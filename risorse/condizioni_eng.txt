DELIVERIES:
Transfer of risks: Deliveries are made at the buyer's risks. The risk shall be transferred in accordance with the Incoterm specified in the SC. If none is specified, Incoterm EXW (latest edition if ICC on the date of the SC) shall apply.
The buyer shall take personal responsabilty for any reservation and any recurse taken against any carrier, agent or forwarding company, even if the product have been delivered carriage-free.
COMPLAINTS:To be admissible, any complaint shall be formulated in writing within 5 days from receiving date.