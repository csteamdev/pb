﻿$PBExportHeader$cs-ipertech.sra
$PBExportComments$Generated Application Object
forward
global type cs-ipertech from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type cs-ipertech from application
string appname = "cs-ipertech"
end type
global cs-ipertech cs-ipertech

on cs-ipertech.create
appname = "cs-ipertech"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on cs-ipertech.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

