﻿$PBExportHeader$uo_banche.sru
forward
global type uo_banche from nonvisualobject
end type
end forward

global type uo_banche from nonvisualobject
end type
global uo_banche uo_banche

forward prototypes
public function integer uof_componi_iban (integer ai_direzione, ref string as_iban, ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_errore)
public function integer uof_componi_iban (integer ai_direzione, long al_row, ref datawindow adw_data, ref string as_errore)
public function integer uof_cerca_banca_clien_for (string as_tipo_anagrafica, string as_cod_cli_for, ref string as_cod_banca_clien_for, ref string as_msg)
end prototypes

public function integer uof_componi_iban (integer ai_direzione, ref string as_iban, ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_errore);//autocomposizione IBAN da cin, abi, cab, cc e viceversa in base all'argomento ai_direzione (0 da IBAN, 1 viceversa)
//torna 0 se tutto OK altrimenti un messaggio di errore/warning (es combinazione abi/cab inesistente in tabella abi cab)

s_cs_xx_parametri				lstr_parametri


lstr_parametri.parametro_s_1_a[1] = as_iban
lstr_parametri.parametro_s_1_a[2] = as_cin
lstr_parametri.parametro_s_1_a[3] = as_abi
lstr_parametri.parametro_s_1_a[4] = as_cab
lstr_parametri.parametro_s_1_a[5] = as_cc

openwithparm(w_autocomponi_iban, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_i_1 = 0 then
	as_iban = lstr_parametri.parametro_s_1_a[1]
	as_cin = lstr_parametri.parametro_s_1_a[2]
	as_abi = lstr_parametri.parametro_s_1_a[3]
	as_cab = lstr_parametri.parametro_s_1_a[4]
	as_cc = lstr_parametri.parametro_s_1_a[5]
	
	if as_abi="" then setnull(as_abi)
	if as_cab="" then setnull(as_cab)
else
	return -1
end if

return 0
end function

public function integer uof_componi_iban (integer ai_direzione, long al_row, ref datawindow adw_data, ref string as_errore);//autocomposizione IBAN da cin, abi, cab, cc e viceversa in base all'argomento ai_direzione (0 da IBAN, 1 viceversa)
//torna 0 se tutto OK altrimenti un messaggio di errore/warning (es combinazione abi/cab inesistente in tabella abi cab)

string				ls_iban, ls_cin, ls_abi, ls_cab, ls_cc
integer			li_ret


ls_iban	= adw_data.getitemstring(al_row, "iban")
ls_cin		= adw_data.getitemstring(al_row, "cin")
ls_abi		= adw_data.getitemstring(al_row, "abi")
ls_cab	= adw_data.getitemstring(al_row, "cab")
ls_cc		= adw_data.getitemstring(al_row, "conto_corrente")

li_ret = uof_componi_iban(ai_direzione, ls_iban, ls_cin, ls_abi, ls_cab, ls_cc, as_errore)

if li_ret = 0 then
	adw_data.setitem(al_row, "iban", ls_iban)
	adw_data.setitem(al_row, "cin", ls_cin)
	adw_data.setitem(al_row, "abi", ls_abi)
	adw_data.setitem(al_row, "cab", ls_cab)
	adw_data.setitem(al_row, "conto_corrente", ls_cc)
end if

return li_ret
end function

public function integer uof_cerca_banca_clien_for (string as_tipo_anagrafica, string as_cod_cli_for, ref string as_cod_banca_clien_for, ref string as_msg);//restitiusce il cod_banca_clien_for corrispondnete ad un abi e ad un cab

//TORNA 			-1 		se errore (in fs_msg)
//						 0		se OK e in fs_cod_banca_clien_for il relativo valore con cui fare il setitem nella dw



//verifica se abi e cab sono presenti in anag_banche_clien_for
//se esiste torna il cod_banca_clien_for
//se non esiste inserisci creando un nuovo codice e torna tale codice

integer ll_i, ll_max, ll_len
string ls_char, ls_appo[], ls_cod_bancaclienfor, ls_max, ls_abi, ls_cab, ls_null, ls_cin, ls_iban, ls_sql, &
		 ls_des_abi, ls_agenzia, ls_indirizzo, ls_des_banca, ls_localita, ls_cap, ls_provincia, ls_telefono, ls_fax
datastore lds_data
long ll_tot

setnull(ls_null)

choose case as_tipo_anagrafica
	case "C"
		SELECT	abi,   
					cab,  
					cin,   
					iban
		 INTO 	:ls_abi,   
					:ls_cab,  
					:ls_cin,   
					:ls_iban  
		 FROM 	anag_clienti
		 where	cod_azienda = :s_cs_xx.cod_azienda and
		 			cod_cliente = :as_cod_cli_for;
		
		
	case "F"
		
		SELECT	abi,   
					cab,  
					cin,   
					iban
		 INTO 	:ls_abi,   
					:ls_cab,  
					:ls_cin,   
					:ls_iban  
		 FROM 	anag_fornitori
		 where	cod_azienda = :s_cs_xx.cod_azienda and
		 			cod_fornitore = :as_cod_cli_for;
		
		
end choose

if (isnull(ls_abi) or len(ls_abi)<=0) then
	return 2 	// non fare nulla
end if

if (isnull(ls_cab) or len(ls_cab)<=0) then
	return 2 	// non fare nulla
end if


ls_sql = 	"select cod_banca_clien_for "+&
			"from anag_banche_clien_for "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_abi='"+ls_abi+"' and cod_cab='"+ls_cab+"' "
					
if not f_crea_datastore(lds_data,ls_sql) then
	as_msg="Errore in lettura creazione datastore"
	return -1
end if

ll_tot = lds_data.retrieve()

if ll_tot>0 then
	//trovato almeno uno, prendi il primo
	ls_cod_bancaclienfor = lds_data.getitemstring(1, 1)
else
	ls_cod_bancaclienfor = ""
end if

if ls_cod_bancaclienfor="" then
	//non trovato, proseguirai con il calcolo del nuovo codice ed inserimento in anag_banche_clien_for
else
	//trovato
	as_cod_banca_clien_for= ls_cod_bancaclienfor
	return 0
end if


//se sei arrivato fin qui crea il codice e inseriscilo
ll_max = 3

if ll_max <=0 then
	return -1
end if

//leggi il max
setnull(ls_cod_bancaclienfor)

//ll_len = len(ls_codice)

select max(substring(cod_banca_clien_for, 1, :ll_max))
into :ls_max
from anag_banche_clien_for
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	as_msg = "Errore durante il lettura MAX codice da anag_banche_clien_for: "+sqlca.sqlerrtext	
	return -1
end if

if isnull(ls_max) or ls_max="" or sqlca.sqlcode=100 then
	ls_cod_bancaclienfor = "001"

else	
	//carica un array con i caratteri della stringa
	for ll_i = 1 to ll_max
		ls_appo[ll_i] = mid(ls_max, ll_i, 1)
	next
	
	for ll_i = ll_max to 1 step -1
		//preleva i caratteri a partire da destra
		ls_char = ls_appo[ll_i]
		
		choose case ls_char
			case "9"
				//fallo diventare "A"
				ls_appo[ll_i] = "A"
				exit
				
			case "Z"
				//metti il carattere corrente a "0"
				ls_appo[ll_i] = "0"
				
				//vai al successivo carattere spostandoti verso sinistra			
				
			case else
				//incrementa il suo codice ascii
				ls_appo[ll_i] = char(asc(ls_char) + 1)
				exit
				
		end choose
	next
	
	ls_cod_bancaclienfor = ""
	for ll_i = 1 to ll_max
		ls_cod_bancaclienfor +=  ls_appo[ll_i]
	next
	
end if

select   des_abi
into     :ls_des_abi
from     tab_abi
where    cod_abi = :ls_abi;
if sqlca.sqlcode <> 0 then
	as_msg = "Errore durante ricerca del codice ABI nella tabella tab_abi~r~n "+sqlca.sqlerrtext	
	return -1
end if


select 	agenzia,
			indirizzo,
			localita,
			cap,
			provincia,
			telefono,
			fax
into     :ls_agenzia,
         :ls_indirizzo,
			:ls_localita, 
			:ls_cap,
			:ls_provincia,
			:ls_telefono,
			:ls_fax
from     tab_abicab
where    cod_abi = :ls_abi and 
         cod_cab = :ls_cab;
if sqlca.sqlcode <> 0 then
	as_msg = "Errore durante ricerca del codice CAB nella tabella tab_abicab~r~n "+sqlca.sqlerrtext	
	return -1
end if


if not isnull(ls_agenzia) and len(trim(ls_agenzia)) > 0 then
	// l'agenzia è presente
	
	if not isnull(ls_localita) and len(trim(ls_localita)) > 0 then
		// presente anche la località
			ls_des_banca = ls_des_abi + "-" + ls_agenzia + "-" + ls_localita
	else
		// NON presente la località
			ls_des_banca = ls_des_abi + "-" + ls_agenzia
	end if
	
else
	// l'agenzia NON è presente

	if not isnull(ls_localita) and len(trim(ls_localita)) > 0 then
		// presente almeno la località
			ls_des_banca = ls_des_abi + "-" + ls_localita
	else
		// NON presente la località
			ls_des_banca = ls_des_abi
	end if
end if

ls_des_banca = left(ls_des_banca, 40)


//fai l'inserimento nella anag_banche_clien_for
INSERT INTO anag_banche_clien_for  
		( cod_azienda,   
		  cod_banca_clien_for,   
		  des_banca,   
		  indirizzo,   
		  localita,   
		  cap,   
		  provincia,   
		  telefono,   
		  fax,   
		  cod_abi,   
		  cod_cab,   
		  flag_blocco,   
		  data_blocco,   
		  cin,   
		  iban )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :ls_cod_bancaclienfor,   
		  :ls_des_banca,   
		  :ls_indirizzo,   
		  :ls_localita,   
		  :ls_cap,   
		  :ls_provincia,   
		  :ls_telefono,   
		  :ls_fax,   
		  :ls_abi,   
		  :ls_cab,   
		  'N',   
		  null,   
		  :ls_cin,   
		  :ls_iban)  ;

if sqlca.sqlcode < 0 then
	as_msg = "Errore durante l'inserimento della banca del cliente in anag_banche_clien_for~r~n "+sqlca.sqlerrtext	
	return -1
end if

as_cod_banca_clien_for=  ls_cod_bancaclienfor

return 0
end function

on uo_banche.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_banche.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

