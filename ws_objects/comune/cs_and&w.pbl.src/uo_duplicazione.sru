﻿$PBExportHeader$uo_duplicazione.sru
forward
global type uo_duplicazione from nonvisualobject
end type
type campi from structure within uo_duplicazione
end type
end forward

type campi from structure
	string		nome
	boolean		chiave
end type

global type uo_duplicazione from nonvisualobject
end type
global uo_duplicazione uo_duplicazione

type variables

end variables

forward prototypes
public function integer uof_duplicazione_tabelle (ref string as_tabella[], ref string as_descrizioni[], string as_origine, string as_destinazione, ref string as_messaggio, ref statictext ast_etichetta, ref hprogressbar apb_progress)
public subroutine uof_correggi_stringa (ref string as_stringa, string as_errato, string as_sostituzione)
public subroutine uof_imposta_campi (string as_tabella, ref campi a_campi[], ref long al_num_campi)
end prototypes

public function integer uof_duplicazione_tabelle (ref string as_tabella[], ref string as_descrizioni[], string as_origine, string as_destinazione, ref string as_messaggio, ref statictext ast_etichetta, ref hprogressbar apb_progress);long   ll_i, ll_j, ll_count, ll_num_campi, ll_progress

string ls_insert, ls_select, ls_controllo, ls_valore[], ls_etichetta

campi  l_campi[]


ll_progress = 0

for ll_i = 1 to upperbound(as_tabella)
	
	if as_tabella[ll_i] <> "parametri_manutenzioni" then
	
		uof_imposta_campi(as_tabella[ll_i],l_campi,ll_num_campi)
		
		ls_select = "select "
		
		for ll_j = 1 to ll_num_campi
			if ll_j > 1 then
				ls_select = ls_select + ","
			end if
			ls_select = ls_select + l_campi[ll_j].nome
		next
		
		ls_select = ls_select + " from " + as_tabella[ll_i] + " where cod_azienda = '" + as_origine + "' "
		
		for ll_j = 1 to upperbound(ls_valore)
			setnull(ls_valore[ll_j])
		next
		
		declare origine dynamic cursor for sqlsa;
		
		prepare sqlsa from :ls_select;
		
		open dynamic origine using descriptor sqlda;
		
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in apertura del cursore origine:~n" + sqlca.sqlerrtext
			return -1
		end if
		
		do while true
			
			fetch origine using descriptor sqlda;
					
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore nella fetch del cursore origine:~n" + sqlca.sqlerrtext
				close origine;
				return -1
			elseif sqlca.sqlcode = 100 then
				close origine;
				exit
			end if
			
			if sqlda.numoutputs <> ll_num_campi then
				as_messaggio = "Errore in lettura della tabella " + as_descrizioni[ll_i] + ":~n" + &
									"Il numero di campi ottenuti non corrisponde al numero di campi specificato per la tabella"
				close origine;
				return -1
			end if
			
			for ll_j = 1 to sqlda.numoutputs
			
				choose case sqlda.outparmtype[ll_j]
						
					case typestring!
						
						ls_valore[ll_j] = sqlda.getdynamicstring(ll_j)
						
					case typeboolean!
						
						ls_valore[ll_j] = sqlda.getdynamicstring(ll_j)
						
						if ls_valore[ll_j] = "" then
							setnull(ls_valore[ll_j])
						end if
						
					case typelong!,typedecimal!,typedouble!,typeinteger!,typereal!,typeuint!,typeulong!
						
						ls_valore[ll_j] = string(sqlda.getdynamicnumber(ll_j))
						
						if ls_valore[ll_j] = "" then
							setnull(ls_valore[ll_j])
						end if
						
						uof_correggi_stringa(ls_valore[ll_j],".","")
						
						uof_correggi_stringa(ls_valore[ll_j],",",".")
						
					case typedatetime!
						
						ls_valore[ll_j] = string(date(sqlda.getdynamicdatetime(ll_j)),s_cs_xx.db_funzioni.formato_data)
						
						if ls_valore[ll_j] = "" then
							setnull(ls_valore[ll_j])
						end if
						
					case typedate!
						
						ls_valore[ll_j] = string(sqlda.getdynamicdate(ll_j),s_cs_xx.db_funzioni.formato_data)
						
						if ls_valore[ll_j] = "" then
							setnull(ls_valore[ll_j])
						end if
						
					case else
						
						as_messaggio = "Errore nella lettura di " + l_campi[ll_j].nome + " dalla tabella " +  &
											as_descrizioni[ll_i] + ":~nTipo di dato non supportato"
											
						close origine;
						
						return -1
				
				end choose
				
				uof_correggi_stringa(ls_valore[ll_j],"'","''")
				
				if sqlda.outparmtype[ll_j] = typestring! or &
					sqlda.outparmtype[ll_j] = typedatetime! or &
					sqlda.outparmtype[ll_j] = typedate! then
					ls_valore[ll_j] = "'" + ls_valore[ll_j] + "'"
				end if
							
			next
			
			ls_etichetta = string(ll_progress) + "%   " + as_descrizioni[ll_i] + " - "
			
			for ll_j = 1 to ll_num_campi
				
				if l_campi[ll_j].chiave = false then
					exit
				end if
				
				ls_etichetta = ls_etichetta + ls_valore[ll_j] + " "
				
			next
			
			ast_etichetta.text = ls_etichetta
			
			ls_controllo = "select count(*) from " + as_tabella[ll_i] + " where cod_azienda = '" + as_destinazione + "' "
			
			for ll_j = 1 to ll_num_campi
				
				if l_campi[ll_j].chiave = false then
					continue
				end if
				
				if isnull(ls_valore[ll_j]) then
					ls_controllo = ls_controllo + "and " + l_campi[ll_j].nome + " is null "
				else
					ls_controllo = ls_controllo + "and " + l_campi[ll_j].nome + " = " + ls_valore[ll_j] + " "
				end if
				
			next
			
			declare controllo dynamic cursor for sqlsa;
			
			prepare sqlsa from :ls_controllo;
			
			open dynamic controllo;
		
			if sqlca.sqlcode <> 0 then
				as_messaggio = "Errore in apertura del cursore controllo tabella "+as_tabella[ll_i]+":~n" + sqlca.sqlerrtext
				return -1
			end if
			
			fetch controllo
			into  :ll_count;
			
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore nella fetch del cursore controllo tabella "+as_tabella[ll_i]+":~n" + sqlca.sqlerrtext
				close controllo;
				return -1
			else
				close controllo;
				if sqlca.sqlcode = 0 and ll_count > 0 then
					continue
				end if
			end if
			
			ls_insert = "insert into " + as_tabella[ll_i] + " (cod_azienda"
			
			for ll_j = 1 to ll_num_campi
				
				ls_insert = ls_insert + "," + l_campi[ll_j].nome
				
			next
			
			ls_insert = ls_insert + ") values ('" + as_destinazione + "'"
			
			for ll_j = 1 to ll_num_campi
				
				if isnull(ls_valore[ll_j]) then
					ls_insert = ls_insert + ",null"
				else
					ls_insert = ls_insert + "," + ls_valore[ll_j]
				end if
				
			next
			
			ls_insert = ls_insert + ")"
			
			execute immediate :ls_insert;
			
			if sqlca.sqlcode <> 0 then
				as_messaggio = "Errore in inserimento record nella tabella " + as_descrizioni[ll_i] + ":~n" + sqlca.sqlerrtext
				rollback;
				return -1
			end if
			
		loop
		
		commit;
	
	else
		//tabella parametri manutenzioni, gestita a parte
		insert into parametri_manutenzioni
				( cod_azienda,
				  flag_gest_manutenzione,
				  tolleranza_tempo,
				  flag_includi_festivi,
				  cod_tipo_fat_ven,
				  cod_tipo_det_ven_rif,
				  cod_tipo_det_ven_regis,
				  cod_tipo_det_ven_ricambi,
				  flag_riferimento,
				  des_riferimento,
				  flag_grid_semplice_programmi,
				  cod_tipo_lista_dist,
				  cod_lista_dist,
				  cod_tipo_lista_rda,
				  cod_lista_rda,
				  cod_tipo_lista_rda_2,
				  cod_lista_rda_2,
				  flag_allinea_scad_prog,
				  flag_allinea_scad_sempl,
				  cod_divisione)
			select		:as_destinazione,
						flag_gest_manutenzione,
						tolleranza_tempo,
						flag_includi_festivi,
						null,
						null,
						null,
						null,
						flag_riferimento,
						des_riferimento,
						flag_grid_semplice_programmi,
						null,
						null,
						null,
						null,
						null,
						null,
						flag_allinea_scad_prog,
						flag_allinea_scad_sempl,
						null
			from parametri_manutenzioni
			where cod_azienda=:as_origine;

			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in inserimento record nella tabella " + as_descrizioni[ll_i] + ":~n" + sqlca.sqlerrtext
				rollback;
				return -1
			end if
			
			commit;
		
	end if
	
	ll_progress = round( (ll_i * 100 / upperbound(as_tabella)) , 0 )
	
	apb_progress.position = ll_progress
	
next

as_messaggio = "Procedura di duplicazione completata con successo"

return 0
end function

public subroutine uof_correggi_stringa (ref string as_stringa, string as_errato, string as_sostituzione);long ll_pos


if isnull(as_stringa) then
	return
end if
			
ll_pos = 1
	
do

	ll_pos = pos(as_stringa,as_errato,ll_pos)
	
	if ll_pos <> 0 then
		as_stringa = replace(as_stringa,ll_pos,len(as_errato),as_sostituzione)
		ll_pos = ll_pos + len(as_sostituzione)
	end if

loop while ll_pos <> 0
end subroutine

public subroutine uof_imposta_campi (string as_tabella, ref campi a_campi[], ref long al_num_campi);long ll_i


for ll_i = 1 to upperbound(a_campi)
	setnull(a_campi[ll_i].nome)
	a_campi[ll_i].chiave = false
next

al_num_campi = 0

choose case as_tabella
					
	case "anag_attrezzature"
	
		a_campi[1].nome = "cod_attrezzatura"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_cat_attrezzature"
		a_campi[3].nome = "descrizione"
		a_campi[4].nome = "fabbricante"
		a_campi[5].nome = "modello"
		a_campi[6].nome = "num_matricola"
		a_campi[7].nome = "data_acquisto"
		a_campi[8].nome = "periodicita_revisione"
		a_campi[9].nome = "unita_tempo"
		a_campi[10].nome = "utilizzo_scadenza"
		a_campi[11].nome = "cod_reparto"
		a_campi[12].nome = "certificata_fabbricante"
		a_campi[13].nome = "certificata_ce"
		a_campi[14].nome = "cod_inventario"
		a_campi[15].nome = "costo_orario_medio"
		a_campi[16].nome = "cod_area_aziendale"
		a_campi[17].nome = "reperibilita"
		a_campi[18].nome = "grado_precisione"
		a_campi[19].nome = "grado_precisione_um"
		a_campi[20].nome = "num_certificato_ente"
		a_campi[21].nome = "data_certificato_ente"
		a_campi[22].nome = "denominazione_ente"
		a_campi[23].nome = "qualita_attrezzatura"
		a_campi[24].nome = "flag_primario"
		a_campi[25].nome = "flag_blocco"
		a_campi[26].nome = "data_blocco"
		a_campi[27].nome = "note"
		a_campi[28].nome = "data_prima_attivazione"
		a_campi[29].nome = "data_installazione"
		a_campi[30].nome = "flag_reg_man"
		a_campi[31].nome = "flag_manuale"
		a_campi[32].nome = "flag_duplicazione"
		a_campi[33].nome = "flag_conta_ore"
		a_campi[34].nome = "costo_acquisto"
		a_campi[35].nome = "freq_guasti"
		a_campi[36].nome = "accessibilita"
		a_campi[37].nome = "disp_ricamnbi"
		a_campi[38].nome = "stato_attuale"
					
		al_num_campi = 38
		
	case "tab_cat_attrezzature"
	
		a_campi[1].nome = "cod_cat_attrezzature"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_cat_attrezzature"
		a_campi[3].nome = "note"
		a_campi[4].nome = "flag_risorsa_umana"
		a_campi[5].nome = "costo_medio_orario"
					
		al_num_campi = 5
		
	case "tab_tipi_manutenzione"
	
		a_campi[1].nome = "cod_attrezzatura"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_tipo_manutenzione"
		a_campi[2].chiave = true
		a_campi[3].nome = "des_tipo_manutenzione"
		a_campi[4].nome = "tempo_previsto"
		a_campi[5].nome = "flag_manutenzione"
		a_campi[6].nome = "flag_ricambio_codificato"
		a_campi[7].nome = "cod_ricambio"
		a_campi[8].nome = "cod_ricambio_alternativo"
		a_campi[9].nome = "des_ricambio_non_codificato"
		a_campi[10].nome = "num_reg_lista"
		a_campi[11].nome = "num_versione"
		a_campi[12].nome = "num_edizione"
		a_campi[13].nome = "modalita_esecuzione"
		a_campi[14].nome = "periodicita"
		a_campi[15].nome = "frequenza"
		a_campi[16].nome = "quan_ricambio"
		a_campi[17].nome = "costo_unitario_ricambio"
		a_campi[18].nome = "cod_primario"
		a_campi[19].nome = "cod_misura"
		a_campi[20].nome = "val_min_taratura"
		a_campi[21].nome = "val_max_taratura"
		a_campi[22].nome = "valore_atteso"
		
		al_num_campi = 22
	
	
	case "parametri_manutenzioni"
		//duplicazione gestita a parte, tutti i campi con FK saranno messi a NULL
	
	
	
	case "parametri_azienda"
	
		a_campi[1].nome = "flag_parametro"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_parametro"
		a_campi[2].chiave = true
		a_campi[3].nome = "des_parametro"
		a_campi[4].nome = "stringa"
		a_campi[5].nome = "flag"
		a_campi[6].nome = "data"
		a_campi[7].nome = "numero"
					
		al_num_campi = 7
		
	case "parametri_omnia"
	
		a_campi[1].nome = "flag_parametro"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_parametro"
		a_campi[2].chiave = true
		a_campi[3].nome = "des_parametro"
		a_campi[4].nome = "stringa"
		a_campi[5].nome = "flag"
		a_campi[6].nome = "data"
		a_campi[7].nome = "numero"
					
		al_num_campi = 7
		
	case "tab_aree_aziendali"
	
		a_campi[1].nome = "cod_area_aziendale"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_divisione"
		a_campi[3].nome = "des_area"
		a_campi[4].nome = "note"
		a_campi[5].nome = "valore_budget"
					
		al_num_campi = 5
		
	case "anag_divisioni"
	
		a_campi[1].nome = "cod_divisione"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_divisione"
		a_campi[3].nome = "compiti"
		a_campi[4].nome = "num_tel_interno"
		a_campi[5].nome = "ind_casella_post"
		a_campi[6].nome = "valore_budget"
					
		al_num_campi = 6
		
	case "anag_reparti"
	
		a_campi[1].nome = "cod_reparto"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_reparto"
		a_campi[3].nome = "nome_responsabile"
		a_campi[4].nome = "cognome_responsabile"
		a_campi[5].nome = "qualifica_responsabile"
		a_campi[6].nome = "indirizzo"
		a_campi[7].nome = "localita"
		a_campi[8].nome = "frazione"
		a_campi[9].nome = "cap"
		a_campi[10].nome = "provincia"
		a_campi[11].nome = "telefono"
		a_campi[12].nome = "fax"
		a_campi[13].nome = "telex"
		a_campi[14].nome = "flag_tipo_reparto"
		a_campi[15].nome = "flag_blocco"
		a_campi[16].nome = "data_blocco"
		a_campi[17].nome = "cod_area_aziendale"
		a_campi[18].nome = "cod_operaio"
					
		al_num_campi = 18
	
	case "cause"
	
		a_campi[1].nome = "cod_causa"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_tipo_causa"
		a_campi[3].nome = "des_causa"
		a_campi[4].nome = "note"
		a_campi[5].nome = "flag_blocco"
		a_campi[6].nome = "data_blocco"
					
		al_num_campi = 6
	
	case "tab_tipi_cause"
	
		a_campi[1].nome = "cod_tipo_causa"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_tipo_causa"
		a_campi[3].nome = "note"
		a_campi[4].nome = "flag_blocco"
		a_campi[5].nome = "data_blocco"
					
		al_num_campi = 5
	
	case "tab_guasti"
	
		a_campi[1].nome = "cod_attrezzatura"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_guasto"
		a_campi[2].chiave = true
		a_campi[3].nome = "des_guasto"
		a_campi[4].nome = "cod_tipo_manutenzione"
		a_campi[5].nome = "punteggio_guasto"
		a_campi[6].nome = "note"
		
		al_num_campi = 6
	
	case "tab_difformita"
	
		a_campi[1].nome = "cod_errore"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_difformita"
		a_campi[3].nome = "cod_trattamento"
		a_campi[4].nome = "cod_area_aziendale"
		a_campi[5].nome = "tipo_difformita"
		a_campi[6].nome = "note"
		a_campi[7].nome = "costo_difformita"
		a_campi[8].nome = "flag_causa_operaio"
					
		al_num_campi = 8
		
	case "tab_trattamenti"
	
		a_campi[1].nome = "cod_trattamento"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_trattamento"
		a_campi[3].nome = "punteggio"
		a_campi[4].nome = "note"
					
		al_num_campi = 4
		
	case "tes_liste_controllo"
	
		a_campi[1].nome = "num_reg_lista"
		a_campi[1].chiave = true
		a_campi[2].nome = "num_versione"
		a_campi[2].chiave = true
		a_campi[3].nome = "num_edizione"
		a_campi[3].chiave = true
		a_campi[4].nome = "des_lista"
		a_campi[5].nome = "cod_area_aziendale"
		a_campi[6].nome = "emesso_il"
		a_campi[7].nome = "autorizzato_il"
		a_campi[8].nome = "valido_per"
		a_campi[9].nome = "flag_uso"
		a_campi[10].nome = "flag_recuperato"
		a_campi[11].nome = "note"
		a_campi[12].nome = "flag_valido"
		a_campi[13].nome = "flag_in_prova"
		a_campi[14].nome = "scadenza_validazione"
		a_campi[15].nome = "modifiche_apportate"
		a_campi[16].nome = "valore_riferimento"
		a_campi[17].nome = "azione_1"
		a_campi[18].nome = "azione_2"
		a_campi[19].nome = "des_etichetta_1"
		a_campi[20].nome = "des_etichetta_2"
		a_campi[21].nome = "des_etichetta_3"
		a_campi[22].nome = "des_etichetta_4"
		a_campi[23].nome = "range_importanza"
		a_campi[24].nome = "flag_questionario"
					
		al_num_campi = 24
		
	case "det_liste_controllo"
	
		a_campi[1].nome = "num_reg_lista"
		a_campi[1].chiave = true
		a_campi[2].nome = "num_versione"
		a_campi[2].chiave = true
		a_campi[3].nome = "num_edizione"
		a_campi[3].chiave = true
		a_campi[4].nome = "prog_riga_liste_contr"
		a_campi[4].chiave = true
		a_campi[5].nome = "des_domanda"
		a_campi[6].nome = "flag_tipo_risposta"
		a_campi[7].nome = "des_check_1"
		a_campi[8].nome = "des_check_2"
		a_campi[9].nome = "check_button"
		a_campi[10].nome = "des_option_1"
		a_campi[11].nome = "des_option_2"
		a_campi[12].nome = "des_option_3"
		a_campi[13].nome = "des_option_4"
		a_campi[14].nome = "des_option_5"
		a_campi[15].nome = "num_opzioni"
		a_campi[16].nome = "option_button"
		a_campi[17].nome = "peso_check_1"
		a_campi[18].nome = "peso_check_2"
		a_campi[19].nome = "peso_option_1"
		a_campi[20].nome = "peso_option_2"
		a_campi[21].nome = "peso_option_3"
		a_campi[22].nome = "peso_option_4"
		a_campi[23].nome = "peso_option_5"
		a_campi[24].nome = "peso_option_libero"
		a_campi[25].nome = "des_option_libero"
		a_campi[26].nome = "campo_libero"
		a_campi[27].nome = "des_libero"
		a_campi[28].nome = "des_subtotale"
		a_campi[29].nome = "val_subtotale"
					
		al_num_campi = 29
		
	case "anag_clienti"
	
		a_campi[1].nome = "cod_cliente"
		a_campi[1].chiave = true
		a_campi[2].nome = "rag_soc_1"
		a_campi[3].nome = "rag_soc_2"
		a_campi[4].nome = "indirizzo"
		a_campi[5].nome = "localita"
		a_campi[6].nome = "frazione"
		a_campi[7].nome = "cap"
		a_campi[8].nome = "provincia"
		a_campi[9].nome = "telefono"
		a_campi[10].nome = "fax"
		a_campi[11].nome = "telex"
		a_campi[12].nome = "cod_fiscale"
		a_campi[13].nome = "partita_iva"
		a_campi[14].nome = "rif_interno"
		a_campi[15].nome = "flag_tipo_cliente"
		a_campi[16].nome = "num_prot_esenzione_iva"
		a_campi[17].nome = "data_esenzione_iva"
		a_campi[18].nome = "flag_sospensione_iva"
		a_campi[19].nome = "giorno_fisso_scadenza"
		a_campi[20].nome = "mese_esclusione_1"
		a_campi[21].nome = "mese_esclusione_2"
		a_campi[22].nome = "data_sostituzione_1"
		a_campi[23].nome = "data_sostituzione_2"
		a_campi[24].nome = "sconto"
		a_campi[25].nome = "fido"
		a_campi[26].nome = "flag_fuori_fido"
		a_campi[27].nome = "conto_corrente"
		a_campi[28].nome = "cod_valuta"
		a_campi[29].nome = "cod_deposito"
		a_campi[30].nome = "flag_riep_boll"
		a_campi[31].nome = "flag_riep_fatt"
		a_campi[32].nome = "flag_blocco"
		a_campi[33].nome = "data_blocco"
		a_campi[34].nome = "casella_mail"
		a_campi[35].nome = "flag_raggruppo_iva_doc"
		a_campi[36].nome = "flag_cauzioni"
		a_campi[37].nome = "flag_stampa_cauzioni"
		a_campi[38].nome = "sito_internet"
		
		al_num_campi = 38
		
	case "anag_fornitori"
	
		a_campi[1].nome = "cod_fornitore"
		a_campi[1].chiave = true
		a_campi[2].nome = "rag_soc_1"
		a_campi[3].nome = "rag_soc_2"
		a_campi[4].nome = "indirizzo"
		a_campi[5].nome = "localita"
		a_campi[6].nome = "frazione"
		a_campi[7].nome = "cap"
		a_campi[8].nome = "provincia"
		a_campi[9].nome = "telefono"
		a_campi[10].nome = "fax"
		a_campi[11].nome = "telex"
		a_campi[12].nome = "cod_fiscale"
		a_campi[13].nome = "partita_iva"
		a_campi[14].nome = "rif_interno"
		a_campi[15].nome = "flag_tipo_fornitore"
		a_campi[16].nome = "num_prot_esenzione_iva"
		a_campi[17].nome = "data_esenzione_iva"
		a_campi[18].nome = "flag_sospensione_iva"
		a_campi[19].nome = "giorno_fisso_scadenza"
		a_campi[20].nome = "mese_esclusione_1"
		a_campi[21].nome = "mese_esclusione_2"
		a_campi[22].nome = "data_sostituzione_1"
		a_campi[23].nome = "data_sostituzione_2"
		a_campi[24].nome = "sconto"
		a_campi[25].nome = "fido"
		a_campi[26].nome = "conto_corrente"
		a_campi[27].nome = "cod_valuta"
		a_campi[28].nome = "cod_deposito"
		a_campi[29].nome = "flag_certificato"
		a_campi[30].nome = "flag_approvato"
		a_campi[31].nome = "flag_strategico"
		a_campi[32].nome = "flag_terzista"
		a_campi[33].nome = "num_contratto"
		a_campi[34].nome = "data_contratto"
		a_campi[35].nome = "flag_ver_ispettiva"
		a_campi[36].nome = "data_ver_ispettiva"
		a_campi[37].nome = "note_ver_ispettiva"
		a_campi[38].nome = "flag_omologato"
		a_campi[39].nome = "flag_procedure_speciali"
		a_campi[40].nome = "peso_val_servizio"
		a_campi[41].nome = "peso_val_qualita"
		a_campi[42].nome = "limite_tolleranza"
		a_campi[43].nome = "flag_blocco"
		a_campi[44].nome = "data_blocco"
		a_campi[45].nome = "flag_tipo_certificazione"
		a_campi[46].nome = "casella_mail"
		a_campi[47].nome = "nome_doc_qualificazione"
		a_campi[48].nome = "sito_internet"
		
		al_num_campi = 48
		
	case "tab_valute"
	
		a_campi[1].nome = "cod_valuta"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_valuta"
		a_campi[3].nome = "cambio_acq"
		a_campi[4].nome = "data_acq"
		a_campi[5].nome = "cambio_ven"
		a_campi[6].nome = "data_ven"
		a_campi[7].nome = "flag_per_diviso"
		a_campi[8].nome = "flag_blocco"
		a_campi[9].nome = "data_blocco"
		a_campi[10].nome = "codice_impresa"
		a_campi[11].nome = "precisione"
		a_campi[12].nome = "formato"
					
		al_num_campi = 12
		
	case "anag_depositi"
	
		a_campi[1].nome = "cod_deposito"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_deposito"
		a_campi[3].nome = "indirizzo"
		a_campi[4].nome = "localita"
		a_campi[5].nome = "frazione"
		a_campi[6].nome = "cap"
		a_campi[7].nome = "provincia"
		a_campi[8].nome = "telefono"
		a_campi[9].nome = "fax"
		a_campi[10].nome = "telex"
		a_campi[11].nome = "flag_blocco"
		a_campi[12].nome = "data_blocco"
		a_campi[13].nome = "numeratore_documento"
								
		al_num_campi = 13
		
	case "anag_prodotti"
	
		a_campi[1].nome = "cod_prodotto"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_comodo"
		a_campi[3].nome = "cod_barre"
		a_campi[4].nome = "des_prodotto"
		a_campi[5].nome = "data_creazione"
		a_campi[6].nome = "cod_deposito"
		a_campi[7].nome = "cod_ubicazione"
		a_campi[8].nome = "flag_decimali"
		a_campi[9].nome = "cod_misura_mag"
		a_campi[10].nome = "cod_misura_peso"
		a_campi[11].nome = "peso_lordo"
		a_campi[12].nome = "peso_netto"
		a_campi[13].nome = "cod_misura_vol"
		a_campi[14].nome = "volume"
		a_campi[15].nome = "pezzi_collo"
		a_campi[16].nome = "flag_classe_abc"
		a_campi[17].nome = "flag_sotto_scorta"
		a_campi[18].nome = "flag_lifo"
		a_campi[19].nome = "saldo_quan_anno_prec"
		a_campi[20].nome = "saldo_quan_inizio_anno"
		a_campi[21].nome = "val_inizio_anno"
		a_campi[22].nome = "saldo_quan_ultima_chiusura"
		a_campi[23].nome = "prog_quan_entrata"
		a_campi[24].nome = "val_quan_entrata"
		a_campi[25].nome = "prog_quan_uscita"
		a_campi[26].nome = "val_quan_uscita"
		a_campi[27].nome = "prog_quan_acquistata"
		a_campi[28].nome = "val_quan_acquistata"
		a_campi[29].nome = "prog_quan_venduta"
		a_campi[30].nome = "val_quan_venduta"
		a_campi[31].nome = "quan_ordinata"
		a_campi[32].nome = "quan_impegnata"
		a_campi[33].nome = "quan_assegnata"
		a_campi[34].nome = "quan_in_spedizione"
		a_campi[35].nome = "quan_anticipi"
		a_campi[36].nome = "costo_standard"
		a_campi[37].nome = "costo_ultimo"
		a_campi[38].nome = "lotto_economico"
		a_campi[39].nome = "livello_riordino"
		a_campi[40].nome = "scorta_minima"
		a_campi[41].nome = "scorta_massima"
		a_campi[42].nome = "indice_rotazione"
		a_campi[43].nome = "consumo_medio"
		a_campi[44].nome = "cod_misura_acq"
		a_campi[45].nome = "fat_conversione_acq"
		a_campi[46].nome = "cod_fornitore"
		a_campi[47].nome = "prezzo_acquisto"
		a_campi[48].nome = "sconto_acquisto"
		a_campi[49].nome = "quan_minima"
		a_campi[50].nome = "inc_ordine"
		a_campi[51].nome = "quan_massima"
		a_campi[52].nome = "tempo_app"
		a_campi[53].nome = "cod_misura_ven"
		a_campi[54].nome = "fat_conversione_ven"	
		a_campi[55].nome = "prezzo_vendita"
		a_campi[56].nome = "sconto"
		a_campi[57].nome = "provvigione"
		a_campi[58].nome = "flag_blocco"
		a_campi[59].nome = "data_blocco"
		a_campi[60].nome = "flag_articolo_fiscale"
		a_campi[61].nome = "flag_cauzione"
		a_campi[62].nome = "cod_misura_lead_time"
		a_campi[63].nome = "lead_time"
		a_campi[64].nome = "cod_reparto"
		a_campi[65].nome = "flag_materia_prima"
		a_campi[66].nome = "flag_escludibile"
		a_campi[67].nome = "flag_stato_rifiuto"
		a_campi[68].nome = "cod_comodo_2"
		
		al_num_campi = 68
		
	case "tab_misure"
	
		a_campi[1].nome = "cod_misura"
		a_campi[1].chiave = true
		a_campi[2].nome = "des_misura"
		a_campi[3].nome = "fat_conversione"
		a_campi[4].nome = "flag_blocco"
		a_campi[5].nome = "data_blocco"
		a_campi[6].nome = "flag_prezzo"
					
		al_num_campi = 6
		
	case "mansionari"
	
		a_campi[1].nome = "cod_resp_divisione"
		a_campi[1].chiave = true
		a_campi[2].nome = "cod_divisione"
		a_campi[3].nome = "cod_area_aziendale"
		a_campi[4].nome = "cod_utente"
		a_campi[5].nome = "ind_casella_post"
		a_campi[6].nome = "nome"
		a_campi[7].nome = "cognome"
		a_campi[8].nome = "indirizzo"
		a_campi[9].nome = "citta"
		a_campi[10].nome = "cap"
		a_campi[11].nome = "prov"
		a_campi[12].nome = "num_telefono"
		a_campi[13].nome = "num_cellulare"
		a_campi[14].nome = "num_livello"
		a_campi[15].nome = "note"
		a_campi[16].nome = "flag_approvazione"
		a_campi[17].nome = "flag_validazione"
		a_campi[18].nome = "flag_lettura"
		a_campi[19].nome = "flag_modifica"
		a_campi[20].nome = "flag_elimina"
		a_campi[21].nome = "autoriz_liv_doc"
		a_campi[22].nome = "responsabilita"
		a_campi[23].nome = "flag_qualificazione"
		a_campi[24].nome = "flag_apertura_nc"
		a_campi[25].nome = "flag_chiusura_nc"
		a_campi[26].nome = "flag_supervisore"
		a_campi[27].nome = "flag_autorizza_reso"
		
		al_num_campi = 27
		
	case "utenti"
	
		a_campi[1].nome = "cod_utente"
		a_campi[1].chiave = true
		a_campi[2].nome = "password"
		a_campi[3].nome = "flag_collegato"
		a_campi[4].nome = "flag_blocco"
		a_campi[5].nome = "data_blocco"
					
		al_num_campi = 5
		
end choose
end subroutine

on uo_duplicazione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_duplicazione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

