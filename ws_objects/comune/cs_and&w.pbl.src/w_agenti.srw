﻿$PBExportHeader$w_agenti.srw
$PBExportComments$Finestra Gestione Anagrafica Agenti
forward
global type w_agenti from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_agenti
end type
type cb_ricerca from commandbutton within w_agenti
end type
type dw_agenti_det from uo_cs_xx_dw within w_agenti
end type
type dw_ricerca from u_dw_search within w_agenti
end type
type dw_agenti_lista from uo_cs_xx_dw within w_agenti
end type
type dw_folder_search from u_folder within w_agenti
end type
end forward

global type w_agenti from w_cs_xx_principale
integer width = 2734
integer height = 1720
string title = "Anagrafica Agenti"
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_agenti_det dw_agenti_det
dw_ricerca dw_ricerca
dw_agenti_lista dw_agenti_lista
dw_folder_search dw_folder_search
end type
global w_agenti w_agenti

event pc_setwindow;call super::pc_setwindow;dw_agenti_lista.set_dw_key("cod_azienda")
dw_agenti_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_agenti_det.set_dw_options(sqlca, &
                             dw_agenti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)

iuo_dw_main = dw_agenti_lista


string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]


l_criteriacolumn[1] = "cod_agente"
l_criteriacolumn[2] = "cod_area_aziendale"
l_criteriacolumn[3] = "flag_blocco"

l_searchtable[1] = "anag_agenti"
l_searchtable[2] = "anag_agenti"
l_searchtable[3] = "anag_agenti"

l_searchcolumn[1] = "cod_agente"
l_searchcolumn[2] = "cod_area_aziendale"
l_searchcolumn[3] = "flag_blocco"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_agenti_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_agenti_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_agenti_lista.change_dw_current()
end event

on w_agenti.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_agenti_det=create dw_agenti_det
this.dw_ricerca=create dw_ricerca
this.dw_agenti_lista=create dw_agenti_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.dw_agenti_det
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.dw_agenti_lista
this.Control[iCurrent+6]=this.dw_folder_search
end on

on w_agenti.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_agenti_det)
destroy(this.dw_ricerca)
destroy(this.dw_agenti_lista)
destroy(this.dw_folder_search)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_agenti_det, &
                 "cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
					  
f_po_loaddddw_dw(dw_ricerca, &
                 "cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  

end event

type cb_reset from commandbutton within w_agenti
integer x = 2286
integer y = 80
integer width = 315
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_agenti
integer x = 2286
integer y = 460
integer width = 315
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_agenti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_agenti_det from uo_cs_xx_dw within w_agenti
integer x = 23
integer y = 620
integer width = 2651
integer height = 980
integer taborder = 20
string dataobject = "d_agenti_det"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_utente"
		guo_ricerca.uof_ricerca_utente(dw_agenti_det, "cod_utente")
		
end choose
end event

type dw_ricerca from u_dw_search within w_agenti
event ue_key pbm_dwnkey
integer x = 229
integer y = 80
integer width = 2030
integer height = 456
integer taborder = 20
string dataobject = "d_agenti_ricerca"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_agente"
		guo_ricerca.uof_ricerca_agente(dw_ricerca,"cod_agente")
end choose
end event

type dw_agenti_lista from uo_cs_xx_dw within w_agenti
integer x = 229
integer y = 40
integer width = 2400
integer height = 540
integer taborder = 10
string dataobject = "d_agenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_folder_search from u_folder within w_agenti
integer x = 23
integer y = 20
integer width = 2647
integer height = 580
integer taborder = 20
end type

