﻿$PBExportHeader$w_autocomponi_iban.srw
forward
global type w_autocomponi_iban from window
end type
type dw_dati from datawindow within w_autocomponi_iban
end type
type cb_annulla from commandbutton within w_autocomponi_iban
end type
type cb_conferma from commandbutton within w_autocomponi_iban
end type
end forward

global type w_autocomponi_iban from window
integer width = 2619
integer height = 1060
boolean titlebar = true
string title = "Autocomposizione IBAN"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_dati dw_dati
cb_annulla cb_annulla
cb_conferma cb_conferma
end type
global w_autocomponi_iban w_autocomponi_iban

forward prototypes
public function integer wf_controlla_abicab (ref string as_abi, ref string as_cab, ref string as_messaggio)
public subroutine wf_componi (ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_bban)
public subroutine wf_componi_2 (ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_bban)
end prototypes

public function integer wf_controlla_abicab (ref string as_abi, ref string as_cab, ref string as_messaggio);integer				li_count


if (isnull(as_abi) or as_abi="")then
	as_messaggio = "Il codice ABI è obbligatorio!"
	return -1
end if

if (isnull(as_cab) or as_cab="") then
	as_messaggio = "Il codice CAB è obbligatorio!"
	return -1
end if

if len(as_abi) < 5 then
	as_abi = right("00000" + as_abi, 5)
end if

setnull(li_count)

select count(*)
into :li_count
from tab_abi
where cod_abi=:as_abi;

if li_count>0 then
else
	as_messaggio = "Il codice ABI '"+as_abi+"' non risulta presente in tabella ABI!"
	return -1
end if


if len(as_cab) < 5 then
	as_cab = right("00000" + as_cab, 5)
end if

setnull(li_count)

select count(*)
into :li_count
from tab_abicab
where 	cod_abi=:as_abi and
			cod_cab=:as_cab;

if li_count>0 then
else
	as_messaggio = "Il codice ABI/CAB '"+as_abi+"/"+as_cab+"' non risulta presente in tabella ABI-CAB!"
	return -1
end if

return 0
end function

public subroutine wf_componi (ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_bban);string				ls_bban


		
if isnull(as_cin) then as_cin = ""
if isnull(as_abi) then as_abi = ""
if isnull(as_cab) then as_cab = ""
if isnull(as_cc) then as_cc = ""

as_abi = right("00000" + as_abi, 5)
as_cab = right("00000" + as_cab, 5)
as_cc = right("000000000000" + as_cc, 12)
		
as_bban = as_cin+as_abi+as_cab+as_cc


return
end subroutine

public subroutine wf_componi_2 (ref string as_cin, ref string as_abi, ref string as_cab, ref string as_cc, ref string as_bban);string				ls_bban


if isnull(as_bban) or as_bban="" then
	as_bban = ""
	as_cin = ""
	as_abi = ""
	as_cab = ""
	as_cc = ""
else
	as_cin = left(as_bban, 1)
	as_abi = right("00000" + mid(as_bban, 2, 5), 5)
	as_cab = right("00000" + mid(as_bban, 7, 5), 5)
	as_cc = right("000000000000" + mid(as_bban, 12, 12), 12)
	
end if


return
end subroutine

on w_autocomponi_iban.create
this.dw_dati=create dw_dati
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.Control[]={this.dw_dati,&
this.cb_annulla,&
this.cb_conferma}
end on

on w_autocomponi_iban.destroy
destroy(this.dw_dati)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
end on

event open;s_cs_xx_parametri				lstr_param
string								ls_iban, ls_cin, ls_abi, ls_cab, ls_cc, ls_bban, ls_nazione, ls_check_digit


lstr_param = message.powerobjectparm


dw_dati.insertrow(0)

ls_iban = lstr_param.parametro_s_1_a[1]
ls_cin = lstr_param.parametro_s_1_a[2]
ls_abi = lstr_param.parametro_s_1_a[3]
ls_cab = lstr_param.parametro_s_1_a[4]
ls_cc = lstr_param.parametro_s_1_a[5]

if isnull(ls_iban) then ls_iban=""
if isnull(ls_cin) then ls_cin=""
if isnull(ls_abi) then ls_abi=""
if isnull(ls_cab) then ls_cab=""
if isnull(ls_cc) then ls_cc=""

//se c'è IBAN parto da quello
if ls_iban<>"" then
	ls_nazione = left(ls_iban, 2)
	ls_check_digit = mid(ls_iban, 3, 2)
	ls_bban = mid(ls_iban, 5, 100)
	wf_componi_2(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)

//se c'è abi e cab ricavo bban
elseif ls_abi<>"" or ls_cab<>"" then
	ls_nazione = left(ls_iban, 2)
	ls_check_digit = mid(ls_iban, 3, 2)
	wf_componi(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)
	
end if


dw_dati.setitem(1, "nazione", ls_nazione)		
dw_dati.setitem(1, "check_digit", ls_check_digit)
dw_dati.setitem(1, "bban", ls_bban)

dw_dati.setitem(1, "cin", ls_cin)
dw_dati.setitem(1, "abi", ls_abi)
dw_dati.setitem(1, "cab", ls_cab)
dw_dati.setitem(1, "conto_corrente", ls_cc)
end event

type dw_dati from datawindow within w_autocomponi_iban
event ue_formatta_abi ( )
event ue_formatta_cab ( )
event ue_formatta_cc ( )
integer x = 55
integer y = 32
integer width = 2482
integer height = 748
integer taborder = 10
string title = "none"
string dataobject = "d_autocomponi_iban"
boolean border = false
boolean livescroll = true
end type

event ue_formatta_abi();

string			ls_valore


ls_valore = getitemstring(1, "abi")
if isnull(ls_valore) then ls_valore = ""

setitem(1, "abi", right("00000" + ls_valore,5))
end event

event ue_formatta_cab();

string			ls_valore


ls_valore = getitemstring(1, "cab")
if isnull(ls_valore) then ls_valore = ""

setitem(1, "cab", right("00000" + ls_valore,5))
end event

event ue_formatta_cc();

string			ls_valore


ls_valore = getitemstring(1, "conto_corrente")
if isnull(ls_valore) then ls_valore = ""

setitem(1, "conto_corrente", right("000000000000" + ls_valore, 12))
end event

event itemchanged;
string				ls_null, ls_bban, ls_data, ls_cin, ls_abi, ls_cab, ls_cc


ls_null = ""

choose case dwo.name
		
	case "bban"
		//ripartisci su cin, abi, cab (prime 1+5+5), il resto è numero cc
		if data<>"" then
			setitem(1, "cin", left(data, 1))
			setitem(1, "abi", mid(data, 2, 5))
			setitem(1, "cab", mid(data, 7, 5))
			setitem(1, "conto_corrente", right("000000000000" + mid(data, 12, 50), 12))
		else
			setitem(1, "cin", ls_null)
			setitem(1, "abi", ls_null)
			setitem(1, "cab", ls_null)
			setitem(1, "conto_corrente", ls_null)
		end if
		
	case "cin"
		//posiziona su BBAN
		ls_cin = data
		ls_abi = getitemstring(1, "abi")
		ls_cab = getitemstring(1, "cab")
		ls_cc = getitemstring(1, "conto_corrente")
		ls_bban = getitemstring(1, "bban")
		
		wf_componi(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)
		setitem(1, "bban", ls_bban)
		
		
	case "abi"
		//posizione su BBAN
		ls_cin = getitemstring(1, "cin")
		ls_abi = data
		ls_cab = getitemstring(1, "cab")
		ls_cc = getitemstring(1, "conto_corrente")
		ls_bban = getitemstring(1, "bban")
		
		wf_componi(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)
		setitem(1, "bban", ls_bban)
		postevent("ue_formatta_abi")
		
		
	case "cab"
		//posizione su BBAN
		ls_cin = getitemstring(1, "cin")
		ls_abi = getitemstring(1, "abi")
		ls_cab = data
		ls_cc = getitemstring(1, "conto_corrente")
		ls_bban = getitemstring(1, "bban")
		
		wf_componi(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)
		setitem(1, "bban", ls_bban)
		postevent("ue_formatta_cab")
		
		
	case "conto_corrente"
		//ultime 12 posizioni
		ls_cin = getitemstring(1, "cin")
		ls_abi = getitemstring(1, "abi")
		ls_cab = getitemstring(1, "cab")
		ls_cc = data
		ls_bban = getitemstring(1, "bban")
		
		wf_componi(ls_cin, ls_abi, ls_cab, ls_cc, ls_bban)
		setitem(1, "bban", ls_bban)
		postevent("ue_formatta_cc")
		
		
end choose
end event

event buttonclicked;string			ls_iban, ls_errore
integer		li_return


choose case dwo.name
	case "b_abicab"
		guo_ricerca.uof_ricerca_abicab(dw_dati,"abi","cab")
		
	case "b_valida"
		accepttext()
		ls_iban = getitemstring(row, "nazione") + getitemstring(row, "check_digit") + getitemstring(row, "bban")
		li_return = guo_functions.uof_valida_iban(ls_iban, ls_errore)
		
		if li_return > 0 then
			//IBAN valido
			setitem(1, "errore", 0)
			object.t_log.text = "IBAN VALIDATO!"
			
		elseif li_return = 0 then
			setitem(1, "errore", 1)
			object.t_log.text = "Inserire un codice IBAN prima di eseguire la validazione!"
		else
			//IBAN non valido
			setitem(1, "errore", 1)
			object.t_log.text = ls_errore

		end if

end choose
end event

type cb_annulla from commandbutton within w_autocomponi_iban
integer x = 1339
integer y = 856
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_cs_xx_parametri			lstr_param

lstr_param.parametro_i_1 = -1
closewithreturn(parent, lstr_param)
end event

type cb_conferma from commandbutton within w_autocomponi_iban
integer x = 823
integer y = 856
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;s_cs_xx_parametri			lstr_param
string							ls_bban, ls_nazione, ls_check_digit, ls_iban, ls_abi, ls_cab, ls_errore
integer						li_ret


dw_dati.accepttext()

ls_abi = dw_dati.getitemstring(1, "abi")
ls_cab = dw_dati.getitemstring(1, "cab")

li_ret = wf_controlla_abicab(ls_abi, ls_cab, ls_errore)

if li_ret <> 0 then
	g_mb.warning(ls_errore)
	return
end if


ls_bban			=  dw_dati.getitemstring(1, "bban")			//cin + abi + cab + cc
ls_nazione		=  dw_dati.getitemstring(1, "nazione")		//primi due caratteri dell'IBAN, es. IT
ls_check_digit	=  dw_dati.getitemstring(1, "check_digit")	//due cifre seguenti (di controllo)

if isnull(ls_bban) then ls_bban = ""
if isnull(ls_nazione) then ls_nazione = ""
if isnull(ls_check_digit) then ls_check_digit = ""

ls_iban = ls_nazione + ls_check_digit + ls_bban

lstr_param.parametro_i_1 = 0
lstr_param.parametro_s_1_a[1] = ls_iban														//iban
lstr_param.parametro_s_1_a[2] = dw_dati.getitemstring(1, "cin")							//cin
lstr_param.parametro_s_1_a[3] = ls_abi														//abi
lstr_param.parametro_s_1_a[4] = ls_cab														//cab
lstr_param.parametro_s_1_a[5] = dw_dati.getitemstring(1, "conto_corrente")			//conto_corrente

closewithreturn(parent, lstr_param)
end event

