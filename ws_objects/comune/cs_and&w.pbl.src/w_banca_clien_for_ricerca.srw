﻿$PBExportHeader$w_banca_clien_for_ricerca.srw
$PBExportComments$Finestra Ricerca Banca delk Cliente / Fornitore
forward
global type w_banca_clien_for_ricerca from w_cs_xx_principale
end type
type st_1 from statictext within w_banca_clien_for_ricerca
end type
type dw_find from u_dw_find within w_banca_clien_for_ricerca
end type
type cb_annulla from uo_cb_close within w_banca_clien_for_ricerca
end type
type cb_ok from commandbutton within w_banca_clien_for_ricerca
end type
type cb_cab from uo_cb_ok within w_banca_clien_for_ricerca
end type
type cb_rag_soc_1 from uo_cb_ok within w_banca_clien_for_ricerca
end type
type cb_abi from uo_cb_ok within w_banca_clien_for_ricerca
end type
type cb_nuovo from commandbutton within w_banca_clien_for_ricerca
end type
type cb_filtra from commandbutton within w_banca_clien_for_ricerca
end type
type dw_ricerca_banca_clien_for from uo_cs_xx_dw within w_banca_clien_for_ricerca
end type
type cb_annulla_ricerca from commandbutton within w_banca_clien_for_ricerca
end type
type dw_ext_nuova_banca from u_dw_search within w_banca_clien_for_ricerca
end type
type dw_folder from u_folder within w_banca_clien_for_ricerca
end type
end forward

global type w_banca_clien_for_ricerca from w_cs_xx_principale
integer width = 2597
integer height = 1468
string title = "Ricerca Banche Clienti Fornitori"
st_1 st_1
dw_find dw_find
cb_annulla cb_annulla
cb_ok cb_ok
cb_cab cb_cab
cb_rag_soc_1 cb_rag_soc_1
cb_abi cb_abi
cb_nuovo cb_nuovo
cb_filtra cb_filtra
dw_ricerca_banca_clien_for dw_ricerca_banca_clien_for
cb_annulla_ricerca cb_annulla_ricerca
dw_ext_nuova_banca dw_ext_nuova_banca
dw_folder dw_folder
end type
global w_banca_clien_for_ricerca w_banca_clien_for_ricerca

on deactivate;call w_cs_xx_principale::deactivate;this.hide()
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[]


dw_ricerca_banca_clien_for.set_dw_key("cod_azienda")
dw_ricerca_banca_clien_for.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_retrieveasneeded + c_selectonrowfocuschange, &
                                c_default)

uo_dw_main = dw_ricerca_banca_clien_for
lw_oggetti[1] = dw_ext_nuova_banca
lw_oggetti[2] = cb_nuovo
lw_oggetti[3] = cb_annulla_ricerca
lw_oggetti[3] = cb_filtra
dw_folder.fu_assigntab(2, "Nuovo/Cerca", lw_oggetti[])
lw_oggetti[1] = dw_ricerca_banca_clien_for
lw_oggetti[2] = st_1
lw_oggetti[3] = dw_find
lw_oggetti[4] = cb_abi
lw_oggetti[5] = cb_cab
lw_oggetti[6] = cb_rag_soc_1
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_ext_nuova_banca.insertrow(0)
dw_ricerca_banca_clien_for.setfocus()
cb_rag_soc_1.postevent("clicked")



l_criteriacolumn[1] = "cod_abi"
l_criteriacolumn[2] = "cod_cab"
l_criteriacolumn[3] = "des_banca_clien_for"

l_searchtable[1] = "anag_banche_clien_for"
l_searchtable[2] = "anag_banche_clien_for"
l_searchtable[3] = "anag_banche_clien_for"

l_searchcolumn[1] = "cod_abi"
l_searchcolumn[2] = "cod_cab"
l_searchcolumn[3] = "des_banca"

dw_ext_nuova_banca.fu_wiredw(l_criteriacolumn[], &
                     dw_ricerca_banca_clien_for, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_ricerca_banca_clien_for.change_dw_current()
end event

on w_banca_clien_for_ricerca.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_find=create dw_find
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cb_cab=create cb_cab
this.cb_rag_soc_1=create cb_rag_soc_1
this.cb_abi=create cb_abi
this.cb_nuovo=create cb_nuovo
this.cb_filtra=create cb_filtra
this.dw_ricerca_banca_clien_for=create dw_ricerca_banca_clien_for
this.cb_annulla_ricerca=create cb_annulla_ricerca
this.dw_ext_nuova_banca=create dw_ext_nuova_banca
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_find
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ok
this.Control[iCurrent+5]=this.cb_cab
this.Control[iCurrent+6]=this.cb_rag_soc_1
this.Control[iCurrent+7]=this.cb_abi
this.Control[iCurrent+8]=this.cb_nuovo
this.Control[iCurrent+9]=this.cb_filtra
this.Control[iCurrent+10]=this.dw_ricerca_banca_clien_for
this.Control[iCurrent+11]=this.cb_annulla_ricerca
this.Control[iCurrent+12]=this.dw_ext_nuova_banca
this.Control[iCurrent+13]=this.dw_folder
end on

on w_banca_clien_for_ricerca.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_find)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cb_cab)
destroy(this.cb_rag_soc_1)
destroy(this.cb_abi)
destroy(this.cb_nuovo)
destroy(this.cb_filtra)
destroy(this.dw_ricerca_banca_clien_for)
destroy(this.cb_annulla_ricerca)
destroy(this.dw_ext_nuova_banca)
destroy(this.dw_folder)
end on

type st_1 from statictext within w_banca_clien_for_ricerca
integer x = 23
integer y = 20
integer width = 754
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Ragione Sociale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_find from u_dw_find within w_banca_clien_for_ricerca
integer x = 800
integer y = 20
integer width = 1143
integer height = 80
integer taborder = 30
end type

type cb_annulla from uo_cb_close within w_banca_clien_for_ricerca
integer x = 1783
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_ok from commandbutton within w_banca_clien_for_ricerca
integer x = 2171
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_banca_clien_for.getitemstring(dw_ricerca_banca_clien_for.getrow(),"cod_banca_clien_for"))
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
else
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_banca_clien_for.getitemstring(dw_ricerca_banca_clien_for.getrow(),"cod_banca_clien_for"))
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
end if	

parent.hide()
end event

type cb_cab from uo_cb_ok within w_banca_clien_for_ricerca
integer x = 1966
integer y = 220
integer width = 343
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CAB"
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_ricerca_banca_clien_for.setsort("cod_cab A")
dw_ricerca_banca_clien_for.sort()

dw_find.fu_wiredw(dw_ricerca_banca_clien_for, "cod_cab")

st_1.text = "Ricerca per CAB:"
dw_find.setfocus()



end event

type cb_rag_soc_1 from uo_cb_ok within w_banca_clien_for_ricerca
integer x = 69
integer y = 220
integer width = 1531
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nominativo Banca"
end type

event clicked;call super::clicked;long ll_righe[]

dw_find.fu_unwiredw()
dw_ricerca_banca_clien_for.setsort("des_banca A")
dw_ricerca_banca_clien_for.sort()
dw_find.fu_wiredw(dw_ricerca_banca_clien_for, "des_banca")
ll_righe[1] = 1
dw_ricerca_banca_clien_for.Set_Selected_Rows(1, ll_righe[], c_IgnoreChanges, c_RefreshChildren, c_RefreshSame)
st_1.text = "Ricerca per Banca:"
dw_find.setfocus()
end event

type cb_abi from uo_cb_ok within w_banca_clien_for_ricerca
event clicked pbm_bnclicked
integer x = 1600
integer y = 220
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ABI"
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_ricerca_banca_clien_for.setsort("cod_abi A")
dw_ricerca_banca_clien_for.sort()

dw_find.fu_wiredw(dw_ricerca_banca_clien_for, "cod_abi")

st_1.text = "Ricerca per ABI:"
dw_find.setfocus()



end event

type cb_nuovo from commandbutton within w_banca_clien_for_ricerca
integer x = 1257
integer y = 840
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo"
end type

event clicked;string ls_cod_abi, ls_cod_cab, ls_des_abi, ls_des_cab, ls_cod_banca, ls_des_banca
long   ll_cont

if dw_ext_nuova_banca.getrow() > 0 then
	dw_ext_nuova_banca.setcolumn(1)
	dw_ext_nuova_banca.setcolumn(2)
	ls_cod_banca = dw_ext_nuova_banca.getitemstring(1, "cod_banca_clien_for")
	ls_des_banca = dw_ext_nuova_banca.getitemstring(1, "des_banca_clien_for")
	ls_cod_abi   = dw_ext_nuova_banca.getitemstring(1, "cod_abi")
	ls_cod_cab   = dw_ext_nuova_banca.getitemstring(1, "cod_cab")
	ls_des_abi   = dw_ext_nuova_banca.getitemstring(1, "des_abi")
	ls_des_cab   = dw_ext_nuova_banca.getitemstring(1, "des_cab")
	
	if len(ls_cod_abi) < 1 then setnull(ls_cod_abi)	
	if len(ls_des_abi) < 1 then setnull(ls_des_abi)	
	if len(ls_cod_abi) < 1 then setnull(ls_cod_cab)	
	if len(ls_des_cab) < 1 then setnull(ls_des_cab)	
	
	if isnull(ls_cod_banca) then
		g_mb.messagebox("Nuova Banca","Attenzione inserire un codice banca",Information!)
		dw_ext_nuova_banca.setcolumn("cod_banca_clien_for")
		return
	end if

	if not isnull(ls_cod_abi) and (isnull(ls_des_abi) or len(ls_des_abi) < 1) then
		g_mb.messagebox("Nuova Banca","Hai digitato un ABI ma non la sua descrizione",Information!)
		dw_ext_nuova_banca.setcolumn("des_abi")
		return
	end if
		
	if not isnull(ls_cod_cab) and (isnull(ls_des_cab) or len(ls_des_cab) < 1) then
		g_mb.messagebox("Nuova Banca","Hai digitato un CAB ma non la sua descrizione",Information!)
		dw_ext_nuova_banca.setcolumn("des_cab")
		return
	end if
		
	if isnull(ls_des_banca) then
		ls_des_banca = trim(left(ls_des_abi, 25))
		ls_des_banca = ls_des_banca + " " + trim(left(ls_des_cab, 39 - len(ls_des_banca)))
		dw_ext_nuova_banca.setitem(1, "des_banca_clien_for", ls_des_banca)
	end if

	ll_cont = 0
	select count(*)  
	into   :ll_cont  
   from   anag_banche_clien_for
   where (anag_banche_clien_for.cod_abi  = :ls_cod_abi ) and
         ( anag_banche_clien_for.cod_cab = :ls_cod_cab )   ;
	if ll_cont > 0 then	
		if g_mb.messagebox("Nuova Banca","Esiste già una banca con le stesse coordinate ABI/CAB: proseguo lo stesso?",Question!,YesNo!,2) = 2 then return
	end if	
	if not isnull(ls_cod_abi) then
		insert into tab_abi  
					 ( cod_abi,   
						des_abi )  
		values    ( :ls_cod_abi,   
						:ls_des_abi )  ;
	end if
	
	if not isnull(ls_cod_abi) then
		insert into tab_abicab  
					 ( cod_abi,   
						cod_cab,   
						indirizzo,   
						localita,   
						frazione,   
						cap,   
						provincia,   
						telefono,   
						fax,   
						telex,   
						agenzia )  
		values    ( :ls_cod_abi,   
						:ls_cod_cab,   
						null,   
						null,   
						null,   
						null,   
						null,   
						null,   
						null,   
						null,   
						:ls_des_cab);
	end if

   insert into anag_banche_clien_for  
				( cod_azienda,   
				  cod_banca_clien_for,   
				  des_banca,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  cod_fiscale,   
				  partita_iva,   
				  cod_abi,   
				  cod_cab,   
				  flag_blocco,   
				  data_blocco )  
	   values ( :s_cs_xx.cod_azienda,   
				  :ls_cod_banca,   
				  :ls_des_banca,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_abi,   
				  :ls_cod_cab,   
				  'N',   
				  null )  ;
		COMMIT using sqlca;
end if


dw_ext_nuova_banca.resetupdate()
cb_filtra.triggerevent("clicked")
end event

type cb_filtra from commandbutton within w_banca_clien_for_ricerca
integer x = 869
integer y = 840
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;//string ls_cod_abi, ls_cod_cab, ls_cod_banca, ls_des_banca, ls_filtro
//
//if dw_ext_nuova_banca.getrow() > 0 then
//	ls_cod_banca = dw_ext_nuova_banca.getitemstring(1, "cod_banca_clien_for")
//	ls_des_banca = dw_ext_nuova_banca.getitemstring(1, "des_banca_clien_for")
//	ls_cod_abi   = dw_ext_nuova_banca.getitemstring(1, "cod_abi")
//	ls_cod_cab   = dw_ext_nuova_banca.getitemstring(1, "cod_cab")
//	
//	if len(ls_cod_abi) < 1 then setnull(ls_cod_abi)	
//	if len(ls_cod_cab) < 1 then setnull(ls_cod_cab)	
//	if len(ls_des_banca) < 1 then setnull(ls_des_banca)	
//
//	setnull(ls_filtro)
//	if not isnull(ls_des_banca) then ls_filtro = "des_banca like '%" + ls_des_banca + "%'"
//	if not isnull(ls_cod_abi) then
//		if isnull(ls_filtro) then
//			ls_filtro = "cod_abi = '" + ls_cod_abi + "'"
//		else
//			ls_filtro = ls_filtro + " AND " + "cod_abi = '" + ls_cod_abi + "'"
//		end if
//	end if
//	if not isnull(ls_cod_cab) then
//		if isnull(ls_filtro) then
//			ls_filtro = "cod_cab = '" + ls_cod_cab + "'"
//		else
//			ls_filtro = ls_filtro + " AND " + "cod_cab = '" + ls_cod_cab + "'"
//		end if
//	end if
//	dw_ricerca_banca_clien_for.setfilter(ls_filtro)
//	dw_ricerca_banca_clien_for.filter()
//end if
//	
//		
dw_ext_nuova_banca.fu_BuildSearch(TRUE)
dw_folder.fu_SelectTab(1)
dw_ricerca_banca_clien_for.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type dw_ricerca_banca_clien_for from uo_cs_xx_dw within w_banca_clien_for_ricerca
integer x = 46
integer y = 220
integer width = 2377
integer height = 1000
integer taborder = 60
string dataobject = "d_ricerca_banca_clien_for"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   cb_ok.postevent(clicked!)
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
datetime ldd_oggi

ldd_oggi = datetime(today())
ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_annulla_ricerca from commandbutton within w_banca_clien_for_ricerca
integer x = 480
integer y = 840
integer width = 366
integer height = 80
integer taborder = 111
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Ric."
end type

event clicked;dw_ext_nuova_banca.fu_Reset()

end event

type dw_ext_nuova_banca from u_dw_search within w_banca_clien_for_ricerca
integer x = 114
integer y = 320
integer width = 1536
integer height = 536
integer taborder = 10
string dataobject = "d_ext_nuova_banca"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_abi, ls_cod_cab, ls_des_abi, ls_des_cab


choose case getcolumnname()
		
	case "cod_abi"
		if not isnull(data) then
			select des_abi
			into   :ls_des_abi
			from   tab_abi
			where  cod_abi = :data;
			if sqlca.sqlcode <> 0 then
				this.setitem(this.getrow(),"des_abi","< ABI non presente in tabella >")
			else
				this.setitem(this.getrow(),"des_abi",ls_des_abi)
			end if
		end if
	case "cod_cab"
		ls_cod_abi = this.getitemstring(this.getrow(),"cod_abi")
		ls_des_abi = this.getitemstring(this.getrow(),"des_abi")
		if not isnull(ls_cod_abi) and not isnull(data) then
			select agenzia
			into   :ls_des_cab
			from   tab_abicab
			where  cod_abi = :ls_cod_abi and cod_cab = :data;
			if sqlca.sqlcode <> 0 then
				this.setitem(this.getrow(),"des_cab","< CAB non presente in tabella >")
			else
				this.setitem(this.getrow(),"des_cab",ls_des_cab)
//				this.setitem(this.getrow(),"des_banca_clien_for", ls_des_abi + " " + ls_des_cab)
			end if
		end if
end choose


end event

type dw_folder from u_folder within w_banca_clien_for_ricerca
integer x = 23
integer y = 120
integer width = 2514
integer height = 1120
integer taborder = 20
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Nuovo/Cerca"
      cb_ok.enabled = false
		cb_annulla.enabled = false
   CASE "Lista"
      cb_ok.enabled = true
		cb_annulla.enabled = true
END CHOOSE

end event

