﻿$PBExportHeader$w_banche.srw
$PBExportComments$Finestra Gestione Banche
forward
global type w_banche from w_cs_xx_principale
end type
type cb_banche_fidi from commandbutton within w_banche
end type
type dw_banche_lista from uo_cs_xx_dw within w_banche
end type
type dw_banche_det from uo_cs_xx_dw within w_banche
end type
end forward

global type w_banche from w_cs_xx_principale
integer width = 2898
integer height = 1976
string title = "Gestione Banche"
cb_banche_fidi cb_banche_fidi
dw_banche_lista dw_banche_lista
dw_banche_det dw_banche_det
end type
global w_banche w_banche

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_banche_lista.set_dw_key("cod_azienda")
dw_banche_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_banche_det.set_dw_options(sqlca, &
                             dw_banche_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_banche_lista
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_banche_det, &
//                 "cod_abi", &
//                 sqlca, &
//                 "tab_abi", &
//                 "cod_abi", &
//                 "des_abi", &
//                 "")

f_po_loaddddw_dw(dw_banche_det, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_banche_det, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_banche.create
int iCurrent
call super::create
this.cb_banche_fidi=create cb_banche_fidi
this.dw_banche_lista=create dw_banche_lista
this.dw_banche_det=create dw_banche_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_banche_fidi
this.Control[iCurrent+2]=this.dw_banche_lista
this.Control[iCurrent+3]=this.dw_banche_det
end on

on w_banche.destroy
call super::destroy
destroy(this.cb_banche_fidi)
destroy(this.dw_banche_lista)
destroy(this.dw_banche_det)
end on

type cb_banche_fidi from commandbutton within w_banche
integer x = 2537
integer y = 1792
integer width = 311
integer height = 72
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fidi"
end type

event clicked;window_open_parm(w_banche_fidi, -1, dw_banche_lista)
end event

type dw_banche_lista from uo_cs_xx_dw within w_banche
integer x = 23
integer y = 20
integer width = 2834
integer height = 500
integer taborder = 10
string dataobject = "d_banche_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;string ls_codice, ls_tabella, ls_descrizione

if i_rownbr > 0 then
   ls_tabella = "anag_banche"
   ls_codice = "cod_banca"
   ls_descrizione = "des_banca"
   f_upd_partita_iva(ls_tabella, ls_codice, ls_descrizione)
end if

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_banche_det from uo_cs_xx_dw within w_banche
integer x = 23
integer y = 540
integer width = 2834
integer height = 1240
integer taborder = 20
string dataobject = "d_banche_det"
borderstyle borderstyle = styleraised!
end type

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
   string ls_cod_abi

   if this.getrow() > 0 then
      ls_cod_abi = this.getitemstring(this.getrow(), "cod_abi")
      f_po_loaddddw_dw(dw_banche_det, &
                       "cod_cab", &
                       sqlca, &
                       "tab_abicab", &
                       "cod_cab", &
                       "agenzia", &
                       "cod_abi = '" + ls_cod_abi + "'")
   end if
end if
end on

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_abi, ls_des_abi, ls_null, ls_indirizzo, ls_localita, ls_frazione, ls_cap, &
          ls_provincia, ls_telefono, ls_fax, ls_telex, ls_agenzia, ls_messaggio


   setnull(ls_null)

   choose case i_colname
      case "cod_abi"
         f_po_loaddddw_dw(dw_banche_det, &
                          "cod_cab", &
                          sqlca, &
                          "tab_abicab", &
                          "cod_cab", &
                          "agenzia", &
                          "cod_abi = '" + i_coltext + "'")

         this.setitem(i_rownbr, "cod_cab", ls_null)
         this.setitem(i_rownbr, "indirizzo", ls_null)
         this.setitem(i_rownbr, "localita", ls_null)
         this.setitem(i_rownbr, "frazione", ls_null)
         this.setitem(i_rownbr, "cap", ls_null)
         this.setitem(i_rownbr, "provincia", ls_null)
         this.setitem(i_rownbr, "telefono", ls_null)
         this.setitem(i_rownbr, "fax", ls_null)
         this.setitem(i_rownbr, "telex", ls_null)
         this.setitem(i_rownbr, "des_banca", ls_null)
      case "cod_cab"
         ls_cod_abi = getitemstring(i_rownbr, "cod_abi")
   
         select tab_abi.des_abi
         into   :ls_des_abi    
         from   tab_abi
         where  tab_abi.cod_abi = :ls_cod_abi;

         if sqlca.sqlcode = 0 then
            select tab_abicab.indirizzo,
                   tab_abicab.localita,
                   tab_abicab.frazione,
                   tab_abicab.cap,
                   tab_abicab.provincia,
                   tab_abicab.telefono,
                   tab_abicab.fax,
                   tab_abicab.telex,
                   tab_abicab.agenzia
            into   :ls_indirizzo,
                   :ls_localita,
                   :ls_frazione,
                   :ls_cap,
                   :ls_provincia,
                   :ls_telefono,
                   :ls_fax,
                   :ls_telex,
                   :ls_agenzia
            from   tab_abicab
            where  tab_abicab.cod_abi = :ls_cod_abi and
                   tab_abicab.cod_cab = :i_coltext;
   
            if sqlca.sqlcode = 0 then
               this.setitem(i_rownbr, "indirizzo", ls_indirizzo)
               this.setitem(i_rownbr, "localita", ls_localita)
               this.setitem(i_rownbr, "frazione", ls_frazione)
               this.setitem(i_rownbr, "cap", ls_cap)
               this.setitem(i_rownbr, "provincia", ls_provincia)
               this.setitem(i_rownbr, "telefono", ls_telefono)
               this.setitem(i_rownbr, "fax", ls_fax)
               this.setitem(i_rownbr, "telex", ls_telex)
               this.setitem(i_rownbr, "des_banca", left (trim(ls_des_abi) + " - " + trim(ls_agenzia), 40))
            end if
         end if
		
		case "cod_banca"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				if f_controlla_codice ("anag_banche","cod_banca","des_banca",i_coltext,ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
			end if
			
   end choose
end if
end event

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;string ls_codice, ls_tabella, ls_descrizione

if i_rownbr > 0 and pcca.window_currentdw = this then
   ls_tabella = "anag_banche"
   ls_codice = "cod_banca"
   ls_descrizione = "des_banca"
   f_upd_partita_iva(ls_tabella, ls_codice, ls_descrizione)
end if


end on

