﻿$PBExportHeader$w_banche_clien_for.srw
$PBExportComments$Finestra Gestione Anagrafica Banche Clienti Fornitori
forward
global type w_banche_clien_for from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_banche_clien_for
end type
type dw_banche_clien_for_lista from uo_cs_xx_dw within w_banche_clien_for
end type
type dw_banche_clien_for_det from uo_cs_xx_dw within w_banche_clien_for
end type
end forward

global type w_banche_clien_for from w_cs_xx_principale
integer width = 2711
integer height = 1736
string title = "Gestione Banche dei Clienti o Fornitori"
cb_1 cb_1
dw_banche_clien_for_lista dw_banche_clien_for_lista
dw_banche_clien_for_det dw_banche_clien_for_det
end type
global w_banche_clien_for w_banche_clien_for

forward prototypes
public subroutine wf_abicab (string fs_cod_abi, string fs_cod_cab)
end prototypes

public subroutine wf_abicab (string fs_cod_abi, string fs_cod_cab);//string ls_des_abi, ls_des_agenzia
//
//if not isnull(fs_cod_abi) then
//	SELECT des_abi
//	INTO   :ls_des_abi  
//	FROM   tab_abi  
//	WHERE  cod_abi = :fs_cod_abi ;
//	if sqlca.sqlcode = 0 then
//		if isnull(fs_cod_cab) then
//			if isnull(ls_des_abi) then ls_des_agenzia = ""
//			dw_banche_clien_for_det.object.st_des_banca.text = ls_des_abi
//		else
//			SELECT agenzia
//			INTO   :ls_des_agenzia
//			FROM   tab_abicab
//			WHERE  cod_abi = :fs_cod_abi and
//			       cod_cab = :fs_cod_cab   ;
//			if sqlca.sqlcode = 0 then
//				if isnull(ls_des_agenzia) then ls_des_agenzia = ""
//				dw_banche_clien_for_det.object.st_des_banca.text = ls_des_abi + " " + ls_des_agenzia
//			else
//				dw_banche_clien_for_det.object.st_des_banca.text = ls_des_abi + "  agenzia ???"
//			end if
//		end if
//	end if
//else
//	dw_banche_clien_for_det.object.st_des_banca.text = ""
//end if
//
//return
//
end subroutine

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_banche_clien_for_lista.set_dw_key("cod_azienda")
dw_banche_clien_for_lista.set_dw_options(sqlca, &
                                         pcca.null_object, &
                                         c_default, &
                                         c_default)
dw_banche_clien_for_det.set_dw_options(sqlca, &
                                       dw_banche_clien_for_lista, &
                                       c_sharedata + c_scrollparent, &
                                       c_default)
iuo_dw_main = dw_banche_clien_for_lista
end on

on w_banche_clien_for.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_banche_clien_for_lista=create dw_banche_clien_for_lista
this.dw_banche_clien_for_det=create dw_banche_clien_for_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_banche_clien_for_lista
this.Control[iCurrent+3]=this.dw_banche_clien_for_det
end on

on w_banche_clien_for.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_banche_clien_for_lista)
destroy(this.dw_banche_clien_for_det)
end on

type cb_1 from commandbutton within w_banche_clien_for
integer x = 2208
integer y = 1492
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "BANCHE"
end type

event clicked;string ls_des_abi, ls_null, ls_indirizzo, ls_localita, ls_frazione, ls_cap, &
		 ls_provincia, ls_telefono, ls_fax, ls_telex, ls_agenzia, ls_des_banca, ls_des_agenzia

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)

open(w_ricerca_banche_italia)

if not isnull(s_cs_xx.parametri.parametro_s_1) then
	dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(),"cod_abi",s_cs_xx.parametri.parametro_s_1)
end if
if not isnull(s_cs_xx.parametri.parametro_s_2) then
	dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(),"cod_cab",s_cs_xx.parametri.parametro_s_2)
	setnull(ls_null)
	
	wf_abicab(s_cs_xx.parametri.parametro_s_1, s_cs_xx.parametri.parametro_s_2)
	
	select tab_abi.des_abi
	into   :ls_des_abi    
	from   tab_abi
	where  tab_abi.cod_abi = :s_cs_xx.parametri.parametro_s_1;
	
	if sqlca.sqlcode = 0 then
		select indirizzo,
				 localita,
				 frazione,
				 cap,
				 provincia,
				 telefono,
				 fax,
				 telex,
				 agenzia
		into   :ls_indirizzo,
				 :ls_localita,
				 :ls_frazione,
				 :ls_cap,
				 :ls_provincia,
				 :ls_telefono,
				 :ls_fax,
				 :ls_telex,
				 :ls_agenzia
		from   tab_abicab
		where  cod_abi = :s_cs_xx.parametri.parametro_s_1 and
				 cod_cab = :s_cs_xx.parametri.parametro_s_2;
	
		if sqlca.sqlcode = 0 then
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "indirizzo", ls_indirizzo)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "localita", ls_localita)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "frazione", ls_frazione)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "cap", ls_cap)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "provincia", ls_provincia)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "telefono", ls_telefono)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "fax", ls_fax)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "telex", ls_telex)
			dw_banche_clien_for_det.setitem(dw_banche_clien_for_det.getrow(), "des_banca", left(trim(ls_des_abi) + " " + trim(ls_agenzia), 40))
		end if
	end if
end if
dw_banche_clien_for_det.accepttext()

end event

type dw_banche_clien_for_lista from uo_cs_xx_dw within w_banche_clien_for
integer x = 23
integer y = 20
integer width = 2629
integer height = 500
integer taborder = 10
string dataobject = "d_banche_clien_for_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_validaterow;call super::pcd_validaterow;string ls_codice, ls_tabella, ls_descrizione

if i_rownbr > 0 then
   ls_tabella = "anag_banche_clien_for"
   ls_codice = "cod_banca_clien_for"
   ls_descrizione = "des_banca"
   f_upd_partita_iva(ls_tabella, ls_codice, ls_descrizione)
end if


end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then

	string ls_cod_abi, ls_cod_cab
	
	ls_cod_abi = getitemstring(getrow(),"cod_abi")
	ls_cod_cab = getitemstring(getrow(),"cod_cab")
	
	wf_abicab(ls_cod_abi, ls_cod_cab)

end if
end event

event pcd_new;call super::pcd_new;cb_1.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_1.enabled = true
end event

event pcd_view;call super::pcd_view;cb_1.enabled = false
end event

type dw_banche_clien_for_det from uo_cs_xx_dw within w_banche_clien_for
integer x = 23
integer y = 540
integer width = 2629
integer height = 1080
integer taborder = 20
string dataobject = "d_banche_clien_for_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_abi, ls_des_abi, ls_null, ls_indirizzo, ls_localita, ls_frazione, ls_cap, &
          ls_provincia, ls_telefono, ls_fax, ls_telex, ls_agenzia, ls_des_agenzia, &
			 ls_cod_cab, ls_cod, ls_des, ls_cod_banca[], ls_des_banca[], ls_messaggio
			 
	long   ll_i, ll_j
	

   setnull(ls_null)

   choose case i_colname
      case "cod_abi"
			wf_abicab(i_coltext, dw_banche_clien_for_det.getitemstring(dw_banche_clien_for_det.getrow(),"cod_cab"))
         this.setitem(i_rownbr, "cod_cab", ls_null)
         this.setitem(i_rownbr, "indirizzo", ls_null)
         this.setitem(i_rownbr, "localita", ls_null)
         this.setitem(i_rownbr, "frazione", ls_null)
         this.setitem(i_rownbr, "cap", ls_null)
         this.setitem(i_rownbr, "provincia", ls_null)
         this.setitem(i_rownbr, "telefono", ls_null)
         this.setitem(i_rownbr, "fax", ls_null)
         this.setitem(i_rownbr, "telex", ls_null)
         this.setitem(i_rownbr, "des_banca", ls_null)
      case "cod_cab"
         ls_cod_abi = getitemstring(i_rownbr, "cod_abi")
			wf_abicab(ls_cod_abi, i_coltext)
			
			declare abicab cursor for
			select  cod_banca_clien_for,
					  des_banca
			from    anag_banche_clien_for
			where   cod_azienda = :s_cs_xx.cod_azienda and
					  cod_abi = :ls_cod_abi and
					  cod_cab = :i_coltext;
					  
			open abicab;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("BANCHE CLIENTI FORNITORI","Errore in controllo ABI-CAB.~nErrore nella open del cursore abicab: " + sqlca.sqlerrtext)
			else
				
				ll_i = 0
			
				do while true
				
					fetch abicab
					into  :ls_cod,
							:ls_des;
							
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("BANCHE CLIENTI FORNITORI","Errore in controllo ABI-CAB.~nErrore nella fetch ddel cursore abicab: " + sqlca.sqlerrtext)
						exit
					elseif sqlca.sqlcode = 100 then
						exit
					end if
					
					ll_i++
					ls_cod_banca[ll_i] = ls_cod
					ls_des_banca[ll_i] = ls_des					
					
				loop
				
				close abicab;
						 
				for ll_j = 1 to ll_i
					g_mb.messagebox("BANCHE CLIENTI FORNITORI","ATTENZIONE!~nABI-CAB già presente per: " + ls_cod_banca[ll_j] + " " + ls_des_banca[ll_j])				
				next				
				
			end if	
	
         select des_abi
         into   :ls_des_abi    
         from   tab_abi
         where  cod_abi = :ls_cod_abi;

         if sqlca.sqlcode = 0 then
            select indirizzo,
                   localita,
                   frazione,
                   cap,
                   provincia,
                   telefono,
                   fax,
                   telex,
                   agenzia
            into   :ls_indirizzo,
                   :ls_localita,
                   :ls_frazione,
                   :ls_cap,
                   :ls_provincia,
                   :ls_telefono,
                   :ls_fax,
                   :ls_telex,
                   :ls_agenzia
            from   tab_abicab
            where  cod_abi = :ls_cod_abi and
                   cod_cab = :i_coltext;
   
            if sqlca.sqlcode = 0 then
               this.setitem(i_rownbr, "indirizzo", ls_indirizzo)
               this.setitem(i_rownbr, "localita", ls_localita)
               this.setitem(i_rownbr, "frazione", ls_frazione)
               this.setitem(i_rownbr, "cap", ls_cap)
               this.setitem(i_rownbr, "provincia", ls_provincia)
               this.setitem(i_rownbr, "telefono", ls_telefono)
               this.setitem(i_rownbr, "fax", ls_fax)
               this.setitem(i_rownbr, "telex", ls_telex)
               this.setitem(i_rownbr, "des_banca", left(trim(ls_des_abi) + " " + trim(ls_agenzia), 40))
            end if
         end if
			
		case "cod_banca_clien_for"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				if f_controlla_codice ("anag_banche_clien_for","cod_banca_clien_for","des_banca",i_coltext,ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
			end if
		
   end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_codice, ls_tabella, ls_descrizione

if i_rownbr > 0 and pcca.window_currentdw = this then
   ls_tabella = "anag_banche_clien_for"
   ls_codice = "cod_banca_clien_for"
   ls_descrizione = "des_banca"
   f_upd_partita_iva(ls_tabella, ls_codice, ls_descrizione)
end if


end event

