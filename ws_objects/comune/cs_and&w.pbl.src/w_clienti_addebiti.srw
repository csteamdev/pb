﻿$PBExportHeader$w_clienti_addebiti.srw
$PBExportComments$Finestra Gestione Addebti del Cliente
forward
global type w_clienti_addebiti from w_cs_xx_principale
end type
type dw_clienti_addebiti_det from uo_cs_xx_dw within w_clienti_addebiti
end type
type dw_clienti_addebiti_lista from uo_cs_xx_dw within w_clienti_addebiti
end type
end forward

global type w_clienti_addebiti from w_cs_xx_principale
integer width = 2121
integer height = 1480
string title = "Addebti per Cliente"
dw_clienti_addebiti_det dw_clienti_addebiti_det
dw_clienti_addebiti_lista dw_clienti_addebiti_lista
end type
global w_clienti_addebiti w_clienti_addebiti

event pc_setwindow;call super::pc_setwindow;dw_clienti_addebiti_lista.set_dw_key("cod_azienda")
dw_clienti_addebiti_lista.set_dw_key("cod_cliente")
dw_clienti_addebiti_lista.set_dw_options(sqlca, &
                                     i_openparm, &
                                     c_scrollparent, &
                                     c_default)
dw_clienti_addebiti_det.set_dw_options(sqlca, &
                                   dw_clienti_addebiti_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_clienti_addebiti_lista

end event

on w_clienti_addebiti.create
int iCurrent
call super::create
this.dw_clienti_addebiti_det=create dw_clienti_addebiti_det
this.dw_clienti_addebiti_lista=create dw_clienti_addebiti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_clienti_addebiti_det
this.Control[iCurrent+2]=this.dw_clienti_addebiti_lista
end on

on w_clienti_addebiti.destroy
call super::destroy
destroy(this.dw_clienti_addebiti_det)
destroy(this.dw_clienti_addebiti_lista)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_clienti_addebiti_det,"cod_tipo_det_ven",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_clienti_addebiti_det,"cod_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_clienti_addebiti_det from uo_cs_xx_dw within w_clienti_addebiti
integer x = 23
integer y = 540
integer width = 2034
integer height = 820
integer taborder = 20
string dataobject = "d_clienti_addebiti_det"
borderstyle borderstyle = styleraised!
end type

type dw_clienti_addebiti_lista from uo_cs_xx_dw within w_clienti_addebiti
integer x = 23
integer y = 20
integer width = 2034
integer height = 500
integer taborder = 10
string dataobject = "d_clienti_addebiti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_cliente

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_cliente")) then
      this.setitem(ll_i, "cod_cliente", ls_cod_cliente)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_cliente


ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

