﻿$PBExportHeader$w_clienti_nuovo.srw
$PBExportComments$Finestra Gestione Clienti
forward
global type w_clienti_nuovo from w_cs_xx_risposta
end type
type dw_clienti_nuovo from uo_std_dw within w_clienti_nuovo
end type
end forward

global type w_clienti_nuovo from w_cs_xx_risposta
integer width = 2112
integer height = 720
string title = "NUOVO CLIENTE"
long backcolor = 15780518
event ue_imposta_dati ( )
dw_clienti_nuovo dw_clienti_nuovo
end type
global w_clienti_nuovo w_clienti_nuovo

type variables
boolean ib_verifica=true
end variables

event ue_imposta_dati();string ls_cod_deposito

if s_cs_xx.cod_utente <> "CS_SYSTEM" and not s_cs_xx.admin   then
	
	dw_clienti_nuovo.settaborder("cod_deposito",0)
	dw_clienti_nuovo.settaborder("cod_capoconto",0)
	
	select cod_deposito
	into :ls_cod_deposito
	from tab_operatori_utenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			flag_default ='S';
			
	if sqlca.sqlcode = 0 then
		dw_clienti_nuovo.setitem(dw_clienti_nuovo.getrow(),"cod_deposito", ls_cod_deposito)
		
		choose case left(ls_cod_deposito,1)
			case "1"
				dw_clienti_nuovo.setitem(dw_clienti_nuovo.getrow(),"cod_capoconto", "01")
			case "2"
				dw_clienti_nuovo.setitem(dw_clienti_nuovo.getrow(),"cod_capoconto", "05")
			case "3"
				dw_clienti_nuovo.setitem(dw_clienti_nuovo.getrow(),"cod_capoconto", "03")
			case "4"
				dw_clienti_nuovo.setitem(dw_clienti_nuovo.getrow(),"cod_capoconto", "04")
		end choose	
	end if
end if	
end event

on w_clienti_nuovo.create
int iCurrent
call super::create
this.dw_clienti_nuovo=create dw_clienti_nuovo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_clienti_nuovo
end on

on w_clienti_nuovo.destroy
call super::destroy
destroy(this.dw_clienti_nuovo)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_clienti_nuovo, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

if s_cs_xx.parametri.impresa then
	string ls_str
	
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MCL';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Verificare l'esistenza del parametro MCL in parametri azienda")
	else
		f_po_loaddddw_dw(dw_clienti_nuovo, &
							  "cod_capoconto", &
							  sqlci, &
							  "sottomastro", &
							  "codice", &
							  "descrizione", &
							  "id_mastro in (select id_mastro from mastro where codice = '"+ ls_str +"')")
	end if
end if

end event

event pc_setwindow;call super::pc_setwindow;dw_clienti_nuovo.insertrow(0)

postevent("ue_imposta_dati")
end event

type dw_clienti_nuovo from uo_std_dw within w_clienti_nuovo
event ue_cerca_codice ( )
event ue_cerca_max_libero ( )
integer x = 23
integer y = 24
integer width = 1993
integer height = 556
integer taborder = 10
string dataobject = "d_clienti_nuovo"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_cerca_codice();string ls_cod_deposito, ls_cod_capoconto, ls_precodice, ls_codice, ls_max, ls_min
long ll_prog, ll_cont

ls_cod_deposito = getitemstring(1, "cod_deposito")
ls_cod_capoconto = getitemstring(1, "cod_capoconto")

if not isnull(ls_cod_deposito) and not isnull(ls_cod_capoconto) then
	
	ls_precodice = left (ls_cod_deposito, 1)
	
	ls_min = ls_precodice + "00000"
	ls_max = ls_precodice + "99999"
	ll_prog = long(ls_min)
	
	do while true
		ll_prog ++
		
		if ll_prog >= long(ls_max) then 
			setnull(ls_codice)
			exit
		end if
		
		ls_codice = string(ll_prog)
		
		select count(cod_cliente)
		into :ll_cont
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_codice ;
		if ll_cont = 0 then
			exit
		end if
		
	loop

	if not isnull(ls_codice) then
		setitem(1,"cod_cliente", ls_codice)
	end if

	
end if
end event

event ue_cerca_max_libero();string ls_cod_deposito, ls_cod_capoconto, ls_precodice, ls_codice, ls_max, ls_min, ls_codice_precedente
long ll_prog, ll_cont

ls_cod_deposito = getitemstring(1, "cod_deposito")
ls_cod_capoconto = getitemstring(1, "cod_capoconto")

if not isnull(ls_cod_deposito) and not isnull(ls_cod_capoconto) then
	
	ls_precodice = left (ls_cod_deposito, 1)
	
	ls_min = ls_precodice + "00000"
	ls_max = ls_precodice + "99999"
	ll_prog = long(ls_max)
	setnull(ls_codice_precedente)
	
	do while true
		
		select max(cod_cliente)
		into   :ls_codice
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente >= ls_min and
				cod_cliente <= ls_max;
				
		ll_prog =long(ls_codice)
		
		ll_prog ++
		
		ls_codice = string(ll_prog)
		
		
		
		
		ll_prog --
		
		if ll_prog >= long(ls_max) then 
			setnull(ls_codice)
			exit
		end if
		
		ls_codice = string(ll_prog)
		
		select count(cod_cliente)
		into :ll_cont
		from anag_clienti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_codice ;
		if ll_cont > 0 then
			ls_codice = ls_codice_precedente 
			exit
		end if
		
		ls_codice_precedente = ls_codice
		
	loop

	if not isnull(ls_codice) then
		setitem(1,"cod_cliente", ls_codice)
	end if
	
end if
end event

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	
	long ll_cont
	
	if dwo.name = "cod_cliente" then
		
		if isnull(data) or len(data) < 1 then
			postevent("ue_cerca_codice")
			
		else
		
			if len(data) <> 6 then 
				ib_verifica=false
				g_mb.messagebox("CLIENTE","Attenzione il codice DEVE essere di 6 caratteri")
				return 1
			end if
			
			select count(*) 
			into :ll_cont
			from anag_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :data;
			
			if ll_cont > 0 then
				ib_verifica=false
				g_mb.messagebox("CLIENTE","Il codice digitato è già stato usato! " )
				return 1
			end if
			
		end if
			
	end if

	
end if
end event

event buttonclicked;call super::buttonclicked;string ls_codice_cliente, ls_cod_deposito

accepttext()
if not ib_verifica then
	ib_verifica=true
	return
end if


choose case dwo.name
	case "b_conferma"
		ls_codice_cliente = getitemstring(1, "cod_cliente")
		
		if len(ls_codice_cliente) <> 6 or isnull(ls_codice_cliente) then
			g_mb.error("Il codice deve essere di 6 caratteri")
			return
		end if
		
		ls_cod_deposito = getitemstring(1, "cod_deposito")
		
		// controllo che il codice cliente sia corerente col deposito
		choose case ls_cod_deposito
			case "100"
				if left(ls_codice_cliente,1) <> "1" then 
					g_mb.error("Attenzione! Il codice cliente non è compatibile col deposito assegnato.")
					return
				end if
					
			case "200"
				if left(ls_codice_cliente,1) <> "2" then 
					g_mb.error("Attenzione! Il codice cliente non è compatibile col deposito assegnato.")
					return
				end if
				
			case "300"
				if left(ls_codice_cliente,1) <> "3" then 
					g_mb.error("Attenzione! Il codice cliente non è compatibile col deposito assegnato.")
					return
				end if
				
			case "400"
				if left(ls_codice_cliente,1) <> "4" then 
					g_mb.error("Attenzione! Il codice cliente non è compatibile col deposito assegnato.")
					return
				end if
				
		end choose
		
		s_cs_xx.parametri.parametro_b_1 = true
		s_cs_xx.parametri.parametro_s_15 = ls_codice_cliente
		s_cs_xx.parametri.parametro_s_14 = getitemstring(1, "cod_deposito")
		s_cs_xx.parametri.parametro_s_13 = getitemstring(1, "cod_capoconto")
		
		close(parent)
		
	case "b_cerca_buco"
		
		triggerevent("ue_cerca_codice")
		
	case "b_max_libero"
		
		triggerevent("ue_cerca_max_libero")
		
end choose
end event

