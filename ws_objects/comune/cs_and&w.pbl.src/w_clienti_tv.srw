﻿$PBExportHeader$w_clienti_tv.srw
forward
global type w_clienti_tv from w_cs_xx_treeview
end type
type dw_clienti_det_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_clienti_det_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_clienti_det_2 dw_clienti_det_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_clienti_det_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_clienti_det_3 dw_clienti_det_3
end type
type det_5 from userobject within tab_dettaglio
end type
type dw_nominativi from uo_dw_nominativi within det_5
end type
type det_5 from userobject within tab_dettaglio
dw_nominativi dw_nominativi
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_col_din from uo_colonne_dinamiche_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_col_din dw_col_din
end type
end forward

global type w_clienti_tv from w_cs_xx_treeview
integer width = 5051
integer height = 2372
string title = "Clienti"
event pc_menu_note ( )
event pc_menu_genera_contatto ( )
event pc_menu_addebiti ( )
event pc_menu_gestisci_contatti ( )
event pc_menu_moduli ( )
event pc_menu_report_commerciale ( )
event pc_menu_report_anagrafica ( )
event pc_menu_report_amministrativo ( )
event pc_menu_blocco_finanziario ( )
event pc_menu_sblocco_finanziario ( )
event pc_menu_destinazioni ( )
end type
global w_clienti_tv w_clienti_tv

type variables
private:
	string is_flag_gestione_anagrafiche
	uo_impresa iuo_impresa
	datastore ids_store
	long il_max_row=255, il_livello
	// icone
	int ICONA_TIPO_ANAGRAFICA,ICONA_DEPOSITO,ICONA_TIPO_CLIENTE,ICONA_PROVINCIA,ICONA_NAZIONE,ICONA_CATEGORIA
	int ICONA_LOCALITA,ICONA_CAP ,ICONA_AREA ,ICONA_ZONA ,ICONA_IVA, ICONA_CLIENTE
	
	
	uo_secure_id iuo_secure

end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_valori_livelli ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_clienti (long al_handle)
public function integer wf_inserisci_tipo_anagrafica (long al_handle)
public function integer wf_inserisci_deposito (long al_handle)
public function integer wf_inserisci_flag_tipo_cliente (long al_handle)
public subroutine wf_treeview_icons ()
public function integer wf_inserisci_provincia (long al_handle)
public function integer wf_inserisci_cap (long al_handle)
public function integer wf_inserisci_localita (long al_handle)
public function integer wf_inserisci_nazione (long al_handle)
public function integer wf_inserisci_categoria (long al_handle)
public function integer wf_inserisci_area (long al_handle)
public function integer wf_inserisci_iva (long al_handle)
public function integer wf_inserisci_zona (long al_handle)
public subroutine wf_imposta_blocco_finanziario (string as_flag_blocco)
public subroutine wf_abilita_componi_iban (long al_row)
end prototypes

event pc_menu_note();window_open_parm(w_clienti_note, -1, tab_dettaglio.det_1.dw_clienti_det_1)

end event

event pc_menu_genera_contatto();tab_dettaglio.det_1.dw_clienti_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_clienti_det_1.getitemstring(tab_dettaglio.det_1.dw_clienti_det_1.getrow(), "cod_cliente")
window_open(w_genera_contatti, 0)
end event

event pc_menu_addebiti();window_open_parm(w_clienti_addebiti, -1, tab_dettaglio.det_1.dw_clienti_det_1)

end event

event pc_menu_gestisci_contatti();s_cs_xx.parametri.parametro_s_1 = "C"  			//tipo_anagrafica
																	/*
																		C	cliente
																		F	fornitore
																		P	fornitore potenziale
																		O	contatto
																	*/
window_open_parm(w_gestione_contatti, -1, tab_dettaglio.det_1.dw_clienti_det_1)
end event

event pc_menu_moduli();tab_dettaglio.det_1.dw_clienti_det_1.accepttext()
if tab_dettaglio.det_1.dw_clienti_det_1.getrow() > 0 then
	
	if not isnull( tab_dettaglio.det_1.dw_clienti_det_1.getitemstring( tab_dettaglio.det_1.dw_clienti_det_1.getrow(), "cod_lingua")) and tab_dettaglio.det_1.dw_clienti_det_1.getitemstring( tab_dettaglio.det_1.dw_clienti_det_1.getrow(), "cod_lingua") <> "" then	
		window_open_parm(w_clienti_tipi_fat_ven, -1, tab_dettaglio.det_1.dw_clienti_det_1)
	else
		g_mb.messagebox( "APICE", "Attenzione: specificare la lingua per poter vedere e valorizzare i moduli!", stopsign!)		
	end if
end if

end event

event pc_menu_report_commerciale();window_open_parm(w_report_comm_cliente, -1, tab_dettaglio.det_1.dw_clienti_det_1)

end event

event pc_menu_report_anagrafica();window_open_parm(w_report_gener_cliente, -1, tab_dettaglio.det_1.dw_clienti_det_1)

end event

event pc_menu_report_amministrativo();window_open_parm(w_report_amm_cliente, -1, tab_dettaglio.det_1.dw_clienti_det_1)

end event

event pc_menu_blocco_finanziario();wf_imposta_blocco_finanziario("S")
end event

event pc_menu_sblocco_finanziario();wf_imposta_blocco_finanziario("N")
end event

event pc_menu_destinazioni();string				ls_cod_cliente
long				ll_row


ll_row = tab_dettaglio.det_1.dw_clienti_det_1.getrow()

if ll_row>0 then
else
	g_mb.warning("Selezionare un cliente!")
	return
end if

ls_cod_cliente =  tab_dettaglio.det_1.dw_clienti_det_1.getitemstring(ll_row, "cod_cliente")

if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
else
	g_mb.warning("Selezionare un cliente!")
	return
end if
	

window_open_parm(w_destinazioni_cliente, -1, tab_dettaglio.det_1.dw_clienti_det_1)


end event

public subroutine wf_imposta_ricerca ();string			ls_rag_soc, ls_localita


is_sql_filtro = ""

ls_rag_soc = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")
ls_localita = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")

//if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) then
	//is_sql_filtro += " AND ( upper(anag_clienti.rag_soc_1) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%'  or  upper(anag_clienti.rag_soc_2) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%' or  upper(anag_clienti.rag_soc_abbreviata) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%' or  upper(anag_clienti.casella_mail) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%' or  upper(anag_clienti.rif_interno) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%' or  upper(anag_clienti.email_amministrazione) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%' or  upper(anag_clienti.telefono) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%'  or  upper(anag_clienti.fax) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%') or  upper(anag_clienti.cod_cliente) like '%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")) +"%'"
if not isnull(ls_rag_soc) then
	 ls_rag_soc = g_str.replace(ls_rag_soc, "'", "''")
	 
	 is_sql_filtro += " AND ("+&
								"upper(anag_clienti.rag_soc_1) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.rag_soc_2) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.rag_soc_abbreviata) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.casella_mail) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.rif_interno) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.email_amministrazione) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.telefono) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_clienti.fax) like '%" + upper(ls_rag_soc) +"%' or "+&
								" upper(anag_clienti.cod_cliente) like '%" + upper(ls_rag_soc) +"%' "+&
								")"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")) then
	is_sql_filtro += " AND cod_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")+"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") <> "X" then
	is_sql_filtro += " AND flag_blocco='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") +"'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco_fin") <> "X" then
	is_sql_filtro += " AND flag_blocco_fin='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco_fin") +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_da")) then
	is_sql_filtro += " AND data_creazione >='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_a")) then
	is_sql_filtro += " AND data_creazione <='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_da")) then
	is_sql_filtro += " AND data_modifica >='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_a")) then
	is_sql_filtro += " AND data_modifica <='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if

//if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")) then
//	is_sql_filtro += " AND upper(localita) like'%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")) +"%'"
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")) then
	 ls_localita = g_str.replace(ls_localita, "'", "''")
	is_sql_filtro += " AND upper(localita) like'%" + upper(ls_localita) +"%'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"provincia")) then
	is_sql_filtro += " AND upper(provincia)='" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"provincia")) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cap")) then
	is_sql_filtro += " AND cap='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cap") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"partita_iva")) then
	is_sql_filtro += " AND upper(partita_iva) like'%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"partita_iva")) +"%'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fiscale")) then
	is_sql_filtro += " AND upper(cod_fiscale) like'%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fiscale")) +"%'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_cliente") <> "X"  then
	is_sql_filtro += " AND flag_tipo_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_cliente") +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_1")) then
	is_sql_filtro += " AND cod_agente_1='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_1") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_2")) then
	is_sql_filtro += " AND cod_agente_2='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_2") +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) then
	is_sql_filtro += " AND cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento")) then
	is_sql_filtro += " AND cod_pagamento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_nazione")) then
	is_sql_filtro += " AND cod_nazione='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_nazione") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area")) then
	is_sql_filtro += " AND cod_area='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_zona")) then
	is_sql_filtro += " AND cod_zona='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_zona") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_anagrafica")) then
	is_sql_filtro += " AND cod_tipo_anagrafica='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_anagrafica") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_categoria")) then
	is_sql_filtro += " AND cod_categoria='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_categoria") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_iva")) then
	is_sql_filtro += " AND cod_iva='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_iva") +"'"
end if


end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Tipo anagrafica", "1")
wf_add_valore_livello("Deposito", "2")
wf_add_valore_livello("Tipo Cliente", "3")
wf_add_valore_livello("Provincia", "4")
wf_add_valore_livello("Nazione", "5")
wf_add_valore_livello("Categoria", "6")
wf_add_valore_livello("Località", "7")
wf_add_valore_livello("CAP", "8")
wf_add_valore_livello("Area", "9")
wf_add_valore_livello("Zona", "Z")
wf_add_valore_livello("Iva Esenzione", "I")

end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "1"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_tipo_anagrafica is null "
			else
				as_sql += " AND cod_tipo_anagrafica = '" + lstr_data.codice + "' "
			end if
			
		case "2"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_deposito is null "
			else
				as_sql += " AND cod_deposito = '" + lstr_data.codice + "' "
			end if
			
		case "3"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND flag_tipo_cliente is null "
			else
				as_sql += " AND flag_tipo_cliente = '" + lstr_data.codice + "' "
			end if
			
		case "4"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND provincia is null "
			else
				as_sql += " AND provincia = '" + lstr_data.codice + "' "
			end if
			
		case "5"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_nazione is null "
			else
				as_sql += " AND cod_nazione = '" + lstr_data.codice + "' "
			end if
			
		case "6"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_categoria is null "
			else
				as_sql += " AND cod_categoria = '" + lstr_data.codice + "' "
			end if
			
		case "7"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND localita is null "
			else
				as_sql += " AND localita = '" + lstr_data.codice + "' "
			end if

		case "8"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cap is null "
			else
				as_sql += " AND cap = '" + lstr_data.codice + "' "
			end if
			
		case "9"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_area is null "
			else
				as_sql += " AND cod_area = '" + lstr_data.codice + "' "
			end if
			
		case "Z"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_zona is null "
			else
				as_sql += " AND cod_zona = '" + lstr_data.codice + "' "
			end if
			
		case "I"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_iva is null "
			else
				as_sql += " AND cod_iva = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "1"
		return wf_inserisci_tipo_anagrafica(al_handle)
		
	case "2"
		return wf_inserisci_deposito(al_handle)
		
	case "3"
		return wf_inserisci_flag_tipo_cliente(al_handle)
		
	case "4"
		return wf_inserisci_provincia(al_handle)
		
	case "5"
		return wf_inserisci_nazione(al_handle)
		
	case "6"
		return wf_inserisci_categoria(al_handle)
		
	case "7"
		return wf_inserisci_localita(al_handle)
		
	case "8"
		return wf_inserisci_cap(al_handle)
		
	case "9"
		return wf_inserisci_area(al_handle)
				
	case "Z"
		return wf_inserisci_zona(al_handle)

	case "I"
		return wf_inserisci_iva(al_handle)
		
	case else
		return wf_inserisci_clienti(al_handle)
		
end choose

return 1
end function

public function integer wf_inserisci_clienti (long al_handle);string ls_sql, ls_error, ls_cod_cliente ,ls_rag_soc_1, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT distinct anag_clienti.cod_cliente, anag_clienti.rag_soc_1 FROM anag_clienti " + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

//imposto l'ordinamento per anno e numero
//ls_sql += " ORDER BY rag_soc_1 ASC"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_cliente = ids_store.getitemstring(ll_i, 1)
	 ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = ls_cod_cliente
	
	if isnull(ls_rag_soc_1) and isnull(ls_rag_soc_1) then
		ls_label = "<Cliente mancante>"
	elseif isnull(ls_rag_soc_1) then
		ls_label = ls_rag_soc_1
	else
		ls_label = ls_cod_cliente + " - " + ls_rag_soc_1
	end if

	ltvi_item = wf_new_item(false, ICONA_CLIENTE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_tipo_anagrafica (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_tipo_anagrafica, tab_tipi_anagrafiche.des_tipo_anagrafica FROM anag_clienti " + &
			" left outer join tab_tipi_anagrafiche on anag_clienti.cod_azienda = tab_tipi_anagrafiche.cod_azienda and anag_clienti.cod_tipo_anagrafica = tab_tipi_anagrafiche.cod_tipo_anagrafica " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "1"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Tipo Anagrafica mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_ANAGRAFICA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_deposito (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_clienti.cod_deposito, anag_depositi.des_deposito FROM anag_clienti " + &
" LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = anag_clienti.cod_azienda AND " + &
" anag_depositi.cod_deposito = anag_clienti.cod_deposito " + &
" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "2"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_flag_tipo_cliente (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct flag_tipo_cliente FROM anag_clienti " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	 choose case ls_codice
		case "S"
			 ls_descrizione = "Società"
		case "F"
			 ls_descrizione = "Persona fisica"
		case "P"
			 ls_descrizione = "Privato"
		case "C"
			 ls_descrizione = "Comunitario"
		case "E"
			 ls_descrizione = "Estero Extra CE"
		case "M"
			 ls_descrizione = "San Marino"
	end choose
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "3"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Tipo cliente mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_CLIENTE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public subroutine wf_treeview_icons ();ICONA_TIPO_ANAGRAFICA =  wf_treeview_add_icon("treeview\cliente.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_TIPO_CLIENTE =  wf_treeview_add_icon("treeview\operatore.png")
ICONA_PROVINCIA =  wf_treeview_add_icon("treeview\target.ico")
ICONA_NAZIONE =  wf_treeview_add_icon("treeview\flag_italy.ico")
ICONA_CATEGORIA =  wf_treeview_add_icon("treeview\Categoria_cliente.png")
ICONA_LOCALITA =  wf_treeview_add_icon("treeview\localita.png")
ICONA_CAP =  wf_treeview_add_icon("treeview\cap.png")
ICONA_AREA =  wf_treeview_add_icon("treeview\area_aziendale.png")
ICONA_ZONA = wf_treeview_add_icon("treeview\zona.png")
ICONA_IVA = wf_treeview_add_icon("treeview\documento_giallo.png")
ICONA_CLIENTE = wf_treeview_add_icon("treeview\cliente.png")
end subroutine

public function integer wf_inserisci_provincia (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(provincia) FROM anag_clienti " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "4"
	lstr_data.codice = ls_codice
	
	ls_label = ls_codice

	ltvi_item = wf_new_item(true, ICONA_PROVINCIA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_cap (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(cap) FROM anag_clienti " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "8"
	lstr_data.codice = ls_codice
	
	ls_label = ls_codice

	ltvi_item = wf_new_item(true, ICONA_CAP)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_localita (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(localita) FROM anag_clienti " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "7"
	lstr_data.codice = ls_codice
	
	ls_label = ls_codice

	ltvi_item = wf_new_item(true, ICONA_LOCALITA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_nazione (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_nazione, tab_nazioni.des_nazione FROM anag_clienti " + &
			" left outer join tab_nazioni on anag_clienti.cod_azienda = tab_nazioni.cod_azienda and anag_clienti.cod_nazione = tab_nazioni.cod_nazione " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "5"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Nazione mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_NAZIONE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_categoria (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_categoria, tab_categorie.des_categoria FROM anag_clienti " + &
			" left outer join tab_categorie on anag_clienti.cod_azienda = tab_categorie.cod_azienda and anag_clienti.cod_categoria = tab_categorie.cod_categoria " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "6"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Categoria cliente mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_CATEGORIA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_area (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_area, tab_aree.des_area FROM anag_clienti " + &
			" left outer join tab_aree on anag_clienti.cod_azienda = tab_aree.cod_azienda and anag_clienti.cod_area = tab_aree.cod_area " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "9"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Area  mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_AREA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_iva (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_iva, tab_ive.des_iva FROM anag_clienti " + &
			" left outer join tab_ive on anag_clienti.cod_azienda = tab_ive.cod_azienda and anag_clienti.cod_iva = tab_ive.cod_iva " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "I"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Area  mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_IVA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_inserisci_zona (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_clienti.cod_zona, tab_zone.des_zona FROM anag_clienti " + &
			" left outer join tab_zone on anag_clienti.cod_azienda = tab_zone.cod_azienda and anag_clienti.cod_zona = tab_zone.cod_zona " + &
			" WHERE anag_clienti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "Z"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Area  mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_ZONA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public subroutine wf_imposta_blocco_finanziario (string as_flag_blocco);/**
 * stefanop
 * 12/02/2014
 *
 * Imposto il blocco finanziario al cliente
 **/

string ls_cod_cliente, ls_cod_cliente_dw, ls_confirm_message
treeviewitem ltvi_item
str_treeview lstr_data
datetime ldt_today

tab_ricerca.selezione.tv_selezione.getitem(il_current_handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "C" then

	ls_cod_cliente = lstr_data.codice
	
	if as_flag_blocco  = "S" then
		ls_confirm_message = "Applicare blocco finanziario al cliente " + ltvi_item.label + " ?"
	else
		ls_confirm_message = "Applicare sblocco finanziario al cliente " + ltvi_item.label + " ?"
	end if
	
	if g_mb.confirm(ls_confirm_message) then

		ldt_today = datetime(today(), 00:00:00)
		
		update anag_clienti
		set 
			flag_blocco_fin=:as_flag_blocco, 
			cod_utente_blocco_fin=:s_cs_xx.cod_utente, 
			data_blocco_fin=:ldt_today
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode = 0 then
			commit;
			
			ls_cod_cliente_dw = tab_dettaglio.det_1.dw_clienti_det_1.getitemstring(1, "cod_cliente")
			if ls_cod_cliente_dw = ls_cod_cliente then
				tab_dettaglio.det_1.dw_clienti_det_1.setitem(1, "flag_blocco_fin", as_flag_blocco)
				tab_dettaglio.det_1.dw_clienti_det_1.setitem(1, "data_blocco_fin", ldt_today)
				tab_dettaglio.det_1.dw_clienti_det_1.resetupdate()
			end if
		else
			g_mb.error("Errore durante il blocco finanziario", sqlca)
			rollback;
		end if
		
	end if
	
end if
end subroutine

public subroutine wf_abilita_componi_iban (long al_row);string			ls_flag_tipo_cliente

ls_flag_tipo_cliente = tab_dettaglio.det_2.dw_clienti_det_2.getitemstring(al_row, "flag_tipo_cliente")

if ls_flag_tipo_cliente="C" or ls_flag_tipo_cliente="E" then
	//per CEE ed Estero niente cin-abi-cab
	tab_dettaglio.det_2.dw_clienti_det_2.object.b_autocomposizione.enabled=false
else
	tab_dettaglio.det_2.dw_clienti_det_2.object.b_autocomposizione.enabled=true
end if

return


end subroutine

on w_clienti_tv.create
int iCurrent
call super::create
end on

on w_clienti_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;is_codice_filtro = "CLI"
il_livello=0

tab_dettaglio.det_1.dw_clienti_det_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_clienti_det_1.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
tab_dettaglio.det_2.dw_clienti_det_2.set_dw_options(sqlca, &
                                tab_dettaglio.det_1.dw_clienti_det_1, &
                                c_sharedata + c_scrollparent, &
                                c_default)
tab_dettaglio.det_3.dw_clienti_det_3.set_dw_options(sqlca, &
                                tab_dettaglio.det_1.dw_clienti_det_1, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main = tab_dettaglio.det_1.dw_clienti_det_1

tab_dettaglio.det_4.dw_col_din.uof_set_dw(tab_dettaglio.det_1.dw_clienti_det_1, {"cod_cliente"})
tab_dettaglio.det_5.dw_nominativi.uof_set_dw(tab_dettaglio.det_1.dw_clienti_det_1, "cod_cliente", tab_dettaglio.det_5.dw_nominativi.CLIENTI)

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select flag_gestione_anagrafiche
	into   :is_flag_gestione_anagrafiche
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode <> 0 or isnull(is_flag_gestione_anagrafiche) or is_flag_gestione_anagrafiche = "" then
		is_flag_gestione_anagrafiche = "S"
	end if
else
	is_flag_gestione_anagrafiche = "S"
end if	

tab_ricerca.ricerca.dw_ricerca.setcolumn("rag_sociale")
tab_ricerca.ricerca.dw_ricerca.setfocus()

tab_dettaglio.det_2.dw_clienti_det_2.object.p_del_banca.filename =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\ole_delete.png"


end event

event close;call super::close;destroy iuo_impresa
end event

event open;call super::open;iuo_impresa = CREATE uo_impresa
iuo_secure = create uo_secure_id
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_deposito='I'")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_nazione", &
	sqlca, &
	"tab_nazioni", &
	"cod_nazione", &
	"des_nazione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

// -------

f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_nazione", &
                 sqlca, &
                 "tab_nazioni", &
                 "cod_nazione", &
                 "des_nazione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_clienti_det_3, &
                 "cod_gruppo_sconto", &
                 sqlca, &
                 "tab_gruppi_sconto", &
                 "cod_gruppo_sconto", &
                 "des_gruppo_sconto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_clienti_det_1, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'C' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_clienti_det_1, &
                 "fatel_idpaese", &
                 sqlca, &
                 "tab_nazioni_iso", &
                 "cod_nazione_iso", &
                 "des_nazione_iso", &
                 "")

if s_cs_xx.parametri.impresa then
	string ls_str
	
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MCL';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Verificare l'esistenza del parametro MCL in parametri azienda")
	else
		f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
							  "cod_capoconto", &
							  sqlci, &
							  "sottomastro", &
							  "codice", &
							  "descrizione", &
							  "id_mastro in (select id_mastro from mastro where codice = '"+ ls_str +"')")
	end if
	
	f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
						  "id_doc_fin_rating_impresa", &
						  sqlci, &
						  "doc_fin_rating", &
						  "id_doc_fin_rating", &
						  "'(' + codice + ') ' + descrizione", &
						  "")
	
	f_po_loaddddw_dw(tab_dettaglio.det_2.dw_clienti_det_2, &
						  "id_doc_fin_voce_fin_impresa", &
						  sqlci, &
						  "doc_fin_voce_fin", &
						  "id_doc_fin_voce_fin", &
						  "'(' + codice + ') ' + descrizione", &
						  "")
end if


end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_clienti_tv
integer x = 1509
integer y = 0
integer width = 3497
integer height = 2232
det_2 det_2
det_3 det_3
det_5 det_5
det_4 det_4
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_5=create det_5
this.det_4=create det_4
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_5,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_5)
destroy(this.det_4)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
event create ( )
event destroy ( )
integer width = 3461
integer height = 2108
string text = "Anagrafica"
dw_clienti_det_1 dw_clienti_det_1
end type

on det_1.create
this.dw_clienti_det_1=create dw_clienti_det_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_clienti_det_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_clienti_det_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_clienti_tv
integer width = 1463
integer height = 2240
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1426
integer height = 2116
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1422
integer height = 2088
string dataobject = "d_clienti_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_sel_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_ricerca.ricerca.dw_ricerca, "cod_cliente")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1426
integer height = 2116
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1403
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_clienti_det_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::rightclicked;call super::rightclicked;string ls_blocco_fin
long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_anag_clienti lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row =tab_dettaglio.det_1.dw_clienti_det_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "C" then
	
	ls_blocco_fin = tab_dettaglio.det_1.dw_clienti_det_1.getitemstring(ll_row, "flag_blocco_fin")

	lm_menu = create m_anag_clienti
	lm_menu.m_bloccofinanziario.visible = ls_blocco_fin = "N"
	lm_menu.m_sbloccofinanziario.visible = ls_blocco_fin = "S"
	
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::key;call super::key;if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if g_mb.confirm("Genero tutte le banche nella tabella banche clienti?") then

		long	ll_handle, ll_ret
		string ls_messaggio,ls_col_abi,ls_col_cab, ls_col_codbancaclienfor, ls_col_cin, ls_col_iban, ls_cod_banca_clien_for, ls_cod_cliente
		treeviewitem ltv_item
		str_treeview lws_record
		uo_banche	luo_banche
		
		ll_handle = tv_selezione.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		luo_banche = create uo_banche
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_selezione.getitem(ll_handle, ltv_item)
			
//			lws_record = ltv_item.data
			
			istr_data = ltv_item.data
			
			ls_cod_cliente = istr_data.codice
	
			ll_ret = luo_banche.uof_cerca_banca_clien_for( "C", ls_cod_cliente, ref ls_cod_banca_clien_for, ref ls_messaggio)
			
			choose case ll_ret
				case 0
				
					update 	anag_clienti
					set 		cod_banca_clien_for = :ls_cod_banca_clien_for
					where 	cod_azienda = :s_cs_xx.cod_azienda and
								cod_cliente = :ls_cod_cliente;
					if sqlca.sqlcode < 0 then
						g_mb.error( g_str.format("Errore in aggiornamento banca del cliente $1. Dettaglio errore:~r~n $2 ", ls_cod_cliente, sqlca.sqlerrtext))
						rollback;
					else
						commit;
					end if
					
				case -1
					g_mb.error(g_str.format("Cliente $1: errore $2", ls_cod_cliente, ls_messaggio)  )
					rollback;
					
			end choose
			
			ll_handle = tv_selezione.finditem( NextTreeItem! ,ll_handle )

		loop
		
		destroy luo_banche
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if

end if
end event

type dw_clienti_det_1 from uo_cs_xx_dw within det_1
integer x = -5
integer y = 16
integer width = 3429
integer height = 2080
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_clienti_det_1_tv"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_messaggio


if i_extendmode then
	choose case getcolumnname()
		case "partita_iva"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				
				if getitemstatus(row, 0, Primary!) <> NewModified! and getitemstatus(row, 0, Primary!) <> New! then
					if g_mb.messagebox("APICE","Partita I.V.A. cambiata: sei sicuro ?",Question!,YesNo!,2) = 2 then
						return 1
					end if
				end if
				
				if f_controlla_partita_iva_doppia (i_coltext, "anag_clienti", "cod_cliente", "rag_soc_1", getitemstring(getrow(),"cod_cliente"), ref ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
				
			end if
			
		case "cod_fiscale"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				
				if getitemstatus(row, 0, Primary!) <> NewModified! and getitemstatus(row, 0, Primary!) <> New! then
					if g_mb.messagebox("APICE","Codice Fiscale cambiato: sei sicuro ?",Question!,YesNo!,2) = 2 then
						return 1
					end if
				end if
				
				if f_controlla_cod_fisc_doppio (i_coltext, "anag_clienti", "cod_cliente", "rag_soc_1", getitemstring(getrow(),"cod_cliente"), ref ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
				
			end if
			
		case "cod_cliente"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				
				if f_controlla_codice ("anag_clienti","cod_cliente","rag_soc_1",i_coltext,ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
				
			end if
			
	end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;
tab_dettaglio.det_5.dw_nominativi.retrieve()

if i_extendmode then
	tab_dettaglio.det_4.dw_col_din.retrieve()
	
	if is_flag_gestione_anagrafiche = "N" then
		tab_dettaglio.det_1.dw_clienti_det_1.object.rag_soc_1.protect = 1
		dw_clienti_det_1.object.rag_soc_2.protect = 1
		dw_clienti_det_1.object.indirizzo.protect = 1
		dw_clienti_det_1.object.cap.protect = 1
		dw_clienti_det_1.object.localita.protect = 1
		dw_clienti_det_1.object.provincia.protect = 1
		dw_clienti_det_1.object.frazione.protect = 1
		dw_clienti_det_1.object.cod_fiscale.protect = 1
		dw_clienti_det_1.object.partita_iva.protect = 1
		dw_clienti_det_1.object.flag_tipo_cliente.protect = 1
		tab_dettaglio.det_2.dw_clienti_det_2.object.cod_capoconto.protect = 1
		tab_dettaglio.det_2.dw_clienti_det_2.object.cod_banca.protect = 1
		tab_dettaglio.det_2.dw_clienti_det_2.object.cod_banca_clien_for.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_valuta.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_agente_1.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_agente_2.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_categoria.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_tipo_listino_prodotto.protect = 1
		tab_dettaglio.det_3.dw_clienti_det_3.object.cod_gruppo_sconto.protect = 1
	end if

	if tab_dettaglio.det_4.dw_col_din.rowcount() > 0 then tab_dettaglio.det_4.dw_col_din.event trigger rowfocuschanged(1)
	
end if
end event

event rowfocuschanging;call super::rowfocuschanging;tab_dettaglio.det_4.dw_col_din.uof_verify_changes()
end event

event ue_key;call super::ue_key;// pressione tasto SHIFT-"C": attivazione registrazione cliente per configuratore ptenda

if key = keyC!  and keyflags = 1 then
	
	if getrow() > 0 then
		window l_window
		str_global l_parametri
		
		l_parametri.cod_cliente = getitemstring(getrow(),"cod_cliente")
		Window_Type_Open_Parm(l_window, "w_conf_attivazione", -1, l_parametri)
	end if
	
end if

end event

event updateend;call super::updateend;long				ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
					ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double			ld_sconto
string				ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
       				ls_rif_interno, ls_flag_tipo_cliente, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
       				ls_cod_tipo_listino_prodotto, ls_flag_fuori_fido, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
       				ls_cod_valuta, ls_cod_categoria, ls_cod_agente_1, ls_cod_agente_2, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
       				ls_cod_deposito, ls_flag_riep_boll, ls_flag_riep_fatt, ls_cod_contatto, ls_cod_cliente, ls_messaggio
datetime 		ldt_data_esenzione_iva, ldt_data_blocco

if tab_dettaglio.det_4.dw_col_din.update() = 1 then
	commit;
else
	rollback;
end if

if rowsinserted > 0 then
	try
		iuo_secure.uof_secure_cliente(getitemstring(getrow(), "cod_azienda"), getitemstring(getrow(), "cod_cliente"))
	catch (SqlException e)
	end try
end if

ll_i = 0
do while ll_i <= rowcount()
	ll_i = getnextmodified(ll_i, Primary!)
	
	if ll_i = 0 then
		exit
	end if

	ls_cod_cliente = getitemstring(ll_i, "cod_cliente")
	
	select anag_contatti.cod_contatto
	into   :ls_cod_contatto
	from   anag_contatti
	where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_contatti.cod_cliente = :ls_cod_cliente;

	if sqlca.sqlcode <> 100 then
		ls_rag_soc_1 = getitemstring(ll_i, "rag_soc_1")
		ls_rag_soc_2 = getitemstring(ll_i, "rag_soc_2")
		ls_indirizzo = getitemstring(ll_i, "indirizzo")
		ls_localita = getitemstring(ll_i, "localita")
		ls_frazione = getitemstring(ll_i, "frazione")
		ls_cap = getitemstring(ll_i, "cap")
		ls_provincia = getitemstring(ll_i, "provincia")
		ls_telefono = getitemstring(ll_i, "telefono")
		ls_fax = getitemstring(ll_i, "fax")
		ls_telex = getitemstring(ll_i, "telex")
		ls_cod_fiscale = getitemstring(ll_i, "cod_fiscale")
		ls_partita_iva = getitemstring(ll_i, "partita_iva")
		ls_rif_interno = getitemstring(ll_i, "rif_interno")
		ls_flag_tipo_cliente = getitemstring(ll_i, "flag_tipo_cliente")
		ls_cod_conto = getitemstring(ll_i, "cod_conto")
		ls_cod_iva = getitemstring(ll_i, "cod_iva")
		ls_num_prot_esenzione_iva = getitemstring(ll_i, "num_prot_esenzione_iva")
		ldt_data_esenzione_iva = getitemdatetime(ll_i, "data_esenzione_iva")
		ls_flag_sospensione_iva = getitemstring(ll_i, "flag_sospensione_iva")
		ls_cod_pagamento = getitemstring(ll_i, "cod_pagamento")
		ll_giorno_fisso_scadenza = getitemnumber(ll_i, "giorno_fisso_scadenza")
		ll_mese_esclusione_1 = getitemnumber(ll_i, "mese_esclusione_1")
		ll_mese_esclusione_2 = getitemnumber(ll_i, "mese_esclusione_2")
		
		ll_ggmm_esclusione_1_da	= getitemnumber(ll_i, "ggmm_esclusione_1_da")
		ll_ggmm_esclusione_1_a		= getitemnumber(ll_i, "ggmm_esclusione_1_a")
		ll_ggmm_esclusione_2_da	= getitemnumber(ll_i, "ggmm_esclusione_2_da")
		ll_ggmm_esclusione_2_a		= getitemnumber(ll_i, "ggmm_esclusione_2_a")
		
		ll_data_sostituzione_1 = getitemnumber(ll_i, "data_sostituzione_1")
		ll_data_sostituzione_2 = getitemnumber(ll_i, "data_sostituzione_2")
		ld_sconto = getitemnumber(ll_i, "sconto")
		ls_cod_tipo_listino_prodotto = getitemstring(ll_i, "cod_tipo_listino_prodotto")
		ll_fido = getitemnumber(ll_i, "fido")
		ls_flag_fuori_fido = getitemstring(ll_i, "flag_fuori_fido")
		ls_cod_banca_clien_for = getitemstring(ll_i, "cod_banca_clien_for")
		ls_conto_corrente = getitemstring(ll_i, "conto_corrente")
		ls_cod_lingua = getitemstring(ll_i, "cod_lingua")
		ls_cod_nazione = getitemstring(ll_i, "cod_nazione")
		ls_cod_area = getitemstring(ll_i, "cod_area")
		ls_cod_zona = getitemstring(ll_i, "cod_zona")
		ls_cod_valuta = getitemstring(ll_i, "cod_valuta")
		ls_cod_categoria = getitemstring(ll_i, "cod_categoria")
		ls_cod_agente_1 = getitemstring(ll_i, "cod_agente_1")
		ls_cod_agente_2 = getitemstring(ll_i, "cod_agente_2")
		ls_cod_imballo = getitemstring(ll_i, "cod_imballo")
		ls_cod_porto = getitemstring(ll_i, "cod_porto")
		ls_cod_resa = getitemstring(ll_i, "cod_resa")
		ls_cod_mezzo = getitemstring(ll_i, "cod_mezzo")
		ls_cod_vettore = getitemstring(ll_i, "cod_vettore")
		ls_cod_inoltro = getitemstring(ll_i, "cod_inoltro")
		ls_cod_deposito = getitemstring(ll_i, "cod_deposito")
		ls_flag_riep_boll = getitemstring(ll_i, "flag_riep_boll")
		ls_flag_riep_fatt = getitemstring(ll_i, "flag_riep_fatt")
	
		update anag_contatti
		set    rag_soc_1 = :ls_rag_soc_1,
				 rag_soc_2 = :ls_rag_soc_2,   
				 indirizzo = :ls_indirizzo,   
				 localita = :ls_localita,   
				 frazione = :ls_frazione,   
				 cap = :ls_cap,   
				 provincia = :ls_provincia,   
				 telefono = :ls_telefono,   
				 fax = :ls_fax,   
				 telex = :ls_telex,   
				 cod_fiscale = :ls_cod_fiscale,   
				 partita_iva = :ls_partita_iva,   
				 rif_interno = :ls_rif_interno,   
				 flag_tipo_cliente = :ls_flag_tipo_cliente,   
				 cod_conto = :ls_cod_conto,   
				 cod_iva = :ls_cod_iva,   
				 num_prot_esenzione_iva = :ls_num_prot_esenzione_iva,   
				 data_esenzione_iva = :ldt_data_esenzione_iva,   
				 flag_sospensione_iva = :ls_flag_sospensione_iva,   
				 cod_pagamento = :ls_cod_pagamento,   
				 giorno_fisso_scadenza = :ll_giorno_fisso_scadenza,   
				 mese_esclusione_1 = :ll_mese_esclusione_1,   
				 mese_esclusione_2 = :ll_mese_esclusione_2,
				 ggmm_esclusione_1_da =:ll_ggmm_esclusione_1_da,
				 ggmm_esclusione_1_a =:ll_ggmm_esclusione_1_a,
				 ggmm_esclusione_2_da =:ll_ggmm_esclusione_2_da,
				 ggmm_esclusione_2_a =:ll_ggmm_esclusione_2_a,
				 data_sostituzione_1 = :ll_data_sostituzione_1,   
				 data_sostituzione_2 = :ll_data_sostituzione_2,   
				 sconto = :ld_sconto,   
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto,   
				 fido = :ll_fido,   
				 flag_fuori_fido = :ls_flag_fuori_fido,   
				 cod_banca_clien_for = :ls_cod_banca_clien_for,   
				 conto_corrente = :ls_conto_corrente,
				 cod_lingua = :ls_cod_lingua,   
				 cod_nazione = :ls_cod_nazione,   
				 cod_area = :ls_cod_area,   
				 cod_zona = :ls_cod_zona,   
				 cod_valuta = :ls_cod_valuta,   
				 cod_categoria = :ls_cod_categoria,   
				 cod_agente_1 = :ls_cod_agente_1,   
				 cod_agente_2 = :ls_cod_agente_2,   
				 cod_imballo = :ls_cod_imballo,   
				 cod_porto = :ls_cod_porto,   
				 cod_resa = :ls_cod_resa,   
				 cod_mezzo = :ls_cod_mezzo,   
				 cod_vettore = :ls_cod_vettore,   
				 cod_inoltro = :ls_cod_inoltro,   
				 cod_deposito = :ls_cod_deposito,   
				 flag_riep_boll = :ls_flag_riep_boll,   
				 flag_riep_fatt = :ls_flag_riep_fatt
		where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_contatti.cod_cliente = :ls_cod_cliente;
	
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Contatto.", &
						  exclamation!, ok!)
			rollback;
			return
		end if
	
		commit;
		
	end if
loop

end event

event updatestart;call super::updatestart; 
string		ls_cod_cliente, ls_messaggio, ls_cod_abi, ls_cod_cab, ls_cod_banca_clien_for, ls_cod_capoconto, ls_cod_abi_1, ls_cod_cab_1, &
			ls_cod_banca, ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_cap, ls_frazione, ls_provincia, &
			ls_partita_iva, ls_cod_fiscale, ls_cin, ls_cin_1, ls_cod_fiscale_copia
integer	li_i
long		ll_cont,li_id_anagrafica, li_id_sog_commerciale

//Donato 03-11-2008 gestione telefono, fax, cellulare, cod_agente_1
string ls_telefono, ls_fax, ls_telex, ls_cod_agente_1

string		ls_msg, ls_col_abi, ls_col_cab, ls_col_codbancaclienfor, ls_col_cin, ls_col_iban
integer		li_ret

if i_extendmode then
	
	tab_dettaglio.det_4.dw_col_din.uof_Delete()
	
	for li_i = 1 to rowcount()
		if getitemstatus(li_i, 0, primary!) = newmodified! OR getitemstatus(li_i, 0, primary!) = datamodified! then      /// nuovo o modificato
			if getitemstatus(li_i, 0, primary!) = datamodified! then		// solo modificato
				setitem(li_i,"data_modifica", datetime(today(),00:00:00) )
			end if
			
			//21/10/2010 ----------------------------------------------------------------------------------------------------------------
			//Gestione Banca per selezione di ABI e CAB
			//occorre recuperare cod_banca_clien_for, o se non esiste per tali abi e cab inserirlo
			//tornando il codice
			
			ls_col_abi = "abi"
			ls_col_cab = "cab"
			ls_col_codbancaclienfor = "cod_banca_clien_for"
			ls_col_cin = "cin"
			ls_col_iban = "iban"
			
			li_ret = f_get_codbancaclienfor(li_i, ls_col_abi, ls_col_cab, ls_col_codbancaclienfor, ls_col_cin, ls_col_iban, tab_dettaglio.det_1.dw_clienti_det_1, ls_msg)
			if li_ret <0 then
				rollback;
				g_mb.messagebox("APICE",ls_msg)
				return 1
			end if			
			//fine modifica ------------------------------------------------------------------------------------------------------------

			if s_cs_xx.parametri.impresa then									// attivato il collegamento con impresa
				ls_cod_capoconto = getitemstring(li_i, "cod_capoconto")
				ls_cod_cliente = getitemstring(li_i, "cod_cliente")
				if isnull(ls_cod_capoconto) and len(ls_cod_cliente) < 4 then
					g_mb.messagebox("APICE","Codice capoconto obbligatorio !")
					return 1
				end if
				
				choose case getitemstring(li_i, "flag_tipo_cliente")
					case "S", "F"			// società o persona fisica (quindi c'è p.iva)
						if isnull(getitemstring(li_i, "partita_iva")) or len(trim(getitemstring(li_i, "partita_iva"))) < 1 then
							g_mb.messagebox("APICE","Se il cliente è una Società o Persona Fisica è obbligatoria la partita IVA, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
					case "P"					// Privato (Quindi c'è il campo codice fiscale)
						if isnull(getitemstring(li_i, "cod_fiscale")) or len(trim(getitemstring(li_i, "cod_fiscale"))) < 1 then
							g_mb.messagebox("APICE","Se il cliente è un privato è obbligatorio il codice fiscale, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
					case "E", "C","M"			// Estero, Cee (Quindi c'è il campo iden/iva - codice fiscale)
						if isnull(getitemstring(li_i, "cod_fiscale")) or len(trim(getitemstring(li_i, "cod_fiscale"))) < 1 then
							g_mb.messagebox("APICE","Se il cliente è un privato è obbligatorio il codice fiscale, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
				end choose
				
				ls_cod_banca_clien_for = getitemstring(li_i, "cod_banca_clien_for")
				ls_cod_banca = getitemstring(li_i, "cod_banca")
				if isnull(ls_cod_banca_clien_for) then
					setnull(ls_cod_abi)
					setnull(ls_cod_cab)
					setnull(ls_cin)
				else
					select cod_abi, cod_cab, cin
					into   :ls_cod_abi, :ls_cod_cab, :ls_cin
					from   anag_banche_clien_for
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_banca_clien_for = :ls_cod_banca_clien_for;
				end if			
				if isnull(ls_cod_banca) then
					setnull(ls_cod_abi_1)
					setnull(ls_cod_cab_1)
					setnull(ls_cin_1)
				else
					select cod_abi, cod_cab, cin
					into   :ls_cod_abi_1, :ls_cod_cab_1, :ls_cin_1
					from   anag_banche
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_banca = :ls_cod_banca;
				end if
				
				//Donato 03-11-2008 modifica per gestione telefono, fax e cellulare
				ls_telefono =getitemstring(li_i, "telefono")
				ls_fax = getitemstring(li_i, "fax")
				ls_telex = getitemstring(li_i, "telex")
				ls_cod_agente_1 = getitemstring(li_i, "cod_agente_1")
				if not isnull( getitemstring(li_i, "cin") ) and len( getitemstring(li_i, "cin") ) > 0 then ls_cin = getitemstring(li_i, "cin")
				
				if iuo_impresa.uof_crea_cli_for("C", &
												ls_cod_cliente, &
												ls_cod_capoconto, &
												getitemstring(li_i, "partita_iva"),&
												getitemstring(li_i, "cod_fiscale"), &
												getitemstring(li_i, "rag_soc_1"),&
												getitemstring(li_i, "rag_soc_2"), &
												getitemstring(li_i, "indirizzo"),&
												getitemstring(li_i, "localita"), &
												getitemstring(li_i, "cap"), &
												getitemstring(li_i, "provincia"), &
												getitemstring(li_i, "frazione"), &
												ls_cod_abi, &
												ls_cod_cab, &
												ls_cin, &
												ls_cod_abi_1, &
												ls_cod_cab_1, &
												ls_cin_1, &
												getitemstring(li_i, "cod_pagamento"), &
												getitemstring(li_i, "cod_valuta"), &
												getitemstring(li_i, "cod_nazione"), &
												getitemnumber(li_i, "ggmm_esclusione_1_da"), &
												getitemnumber(li_i, "ggmm_esclusione_1_a"), &
												getitemnumber(li_i, "ggmm_esclusione_2_da"), &
												getitemnumber(li_i, "ggmm_esclusione_2_a"), &
												getitemnumber(li_i, "data_sostituzione_1"), &
												getitemnumber(li_i, "data_sostituzione_2"), &
												getitemstring(li_i, "flag_raggruppo_scadenze"), &
												ls_telefono, ls_fax, ls_telex, &
												getitemstring(li_i, "email_amministrazione"), &
												ls_cod_agente_1, &
												getitemstring(li_i, "iban"), &
												getitemstring(li_i, "swift"), &
												getitemnumber(li_i, "id_doc_fin_rating_impresa"), &
												getitemnumber(li_i, "id_doc_fin_voce_fin_impresa"), &
												getitemstring(li_i, "flag_tipo_cliente"), &
												ref li_id_anagrafica, &
												ref li_id_sog_commerciale ) = -1 then 
					g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
					return 1
				end if
				
				// -----------------------------------------------------------------------------------------
				// cerco se c'è qualche fornitore con la stessa partita iva o codice fiscale;
				//	se c'è copio le modifiche anagrafiche nel fornitore      EnMe 02/05/2003 ----------------
				
				choose case getitemstring(li_i, "flag_tipo_cliente")
					case "S", "F"			// società o persona fisica (quindi c'è p.iva)
						ls_partita_iva = getitemstring(li_i, "partita_iva")
						if not isnull(ls_partita_iva) and len(trim(ls_partita_iva)) > 0 then
							ll_cont = 0
							select count(*)
							into   :ll_cont
							from   anag_fornitori
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 partita_iva = :ls_partita_iva;
									 
							if ll_cont = 1 then
								
								select cod_fornitore, rag_soc_1
								into   :ls_cod_fornitore, :ls_rag_soc_1
								from   anag_fornitori
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 partita_iva = :ls_partita_iva;
										 
								g_mb.messagebox("APICE","Il fornitore " + ls_cod_fornitore + " " + ls_rag_soc_1 + " ha la stessa partita iva del cliente " + ls_cod_cliente + " " + getitemstring(li_i,"rag_soc_1") + "~r~nIl fornitore sarà aggiornato !",information!)
	
								ls_rag_soc_1 = getitemstring(li_i,"rag_soc_1")
								ls_rag_soc_2 = getitemstring(li_i,"rag_soc_2")
								ls_indirizzo = getitemstring(li_i,"indirizzo")
								ls_localita = getitemstring(li_i,"localita")
								ls_frazione = getitemstring(li_i,"frazione")
								ls_cap = getitemstring(li_i,"cap")
								ls_provincia = getitemstring(li_i,"provincia")
								ls_cod_fiscale_copia = getitemstring(li_i,"cod_fiscale")
								
								update anag_fornitori
								set    rag_soc_1 = :ls_rag_soc_1,
								       rag_soc_2 = :ls_rag_soc_2,
										 indirizzo = :ls_indirizzo,
										 localita  = :ls_localita,
										 frazione  = :ls_frazione,
										 cap       = :ls_cap,
										 provincia = :ls_provincia,
										 cod_fiscale = :ls_cod_fiscale_copia
								where  cod_azienda = :s_cs_xx.cod_azienda and
								       cod_fornitore = :ls_cod_fornitore;
								if sqlca.sqlcode <> 0 then
									g_mb.messagebox("APICE","Errore in aggiornamento del fornitore "+ls_cod_fornitore +" "+ ls_rag_soc_1 +".Dettaglio errore.~r~n" + sqlca.sqlerrtext)
									return 1
								end if
							elseif ll_cont > 1 then
								g_mb.messagebox("APICE","La partita iva di questo cliente corriponde a più di un fornitore; in questo caso le modifiche anagrafiche non saranno trasferite, ma sarà necessario procedere manualmente!~r~nPREMERE INVIO per continuare ...",information!)
							end if
						end if
								 
					case "P", "E", "C","M"
						ls_cod_fiscale = getitemstring(li_i, "cod_fiscale")
						if not isnull(ls_cod_fiscale) and len(trim(ls_cod_fiscale)) > 0 then
							ll_cont = 0
							select count(*)
							into   :ll_cont
							from   anag_fornitori
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_fiscale = :ls_cod_fiscale;
									 
							if ll_cont = 1 then
								
								select cod_fornitore, rag_soc_1
								into   :ls_cod_fornitore, :ls_rag_soc_1
								from   anag_fornitori
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_fiscale = :ls_cod_fiscale;
										 
								g_mb.messagebox("APICE","Il fornitore " + ls_cod_fornitore + " " + ls_rag_soc_1 + " ha lo stesso codice fiscale del cliente " + ls_cod_cliente + " " + getitemstring(li_i,"rag_soc_1") + "~r~nIl fornitore sarà aggiornato !",information!)
	
								ls_rag_soc_1 = getitemstring(li_i,"rag_soc_1")
								ls_rag_soc_2 = getitemstring(li_i,"rag_soc_2")
								ls_indirizzo = getitemstring(li_i,"indirizzo")
								ls_localita = getitemstring(li_i,"localita")
								ls_frazione = getitemstring(li_i,"frazione")
								ls_cap = getitemstring(li_i,"cap")
								ls_provincia = getitemstring(li_i,"provincia")
								
								update anag_fornitori
								set    rag_soc_1 = :ls_rag_soc_1,
								       rag_soc_2 = :ls_rag_soc_2,
										 indirizzo = :ls_indirizzo,
										 localita  = :ls_localita,
										 frazione  = :ls_frazione,
										 cap       = :ls_cap,
										 provincia = :ls_provincia
								where  cod_azienda = :s_cs_xx.cod_azienda and
								       cod_fornitore = :ls_cod_fornitore;
								if sqlca.sqlcode <> 0 then
									g_mb.messagebox("APICE","Errore in aggiornamento del fornitore "+ls_cod_fornitore +" "+ ls_rag_soc_1 +".Dettaglio errore.~r~n" + sqlca.sqlerrtext)
									return 1
								end if
							elseif ll_cont > 1 then
								g_mb.messagebox("APICE","Il codice fiscale di questo cliente corriponde a più di un fornitore; in questo caso le modifiche anagrafiche non saranno trasferite, ma sarà necessario procedere manualmente!~r~nPREMERE INVIO per continuare ...",information!)
							end if
						end if
								 
				end choose
				
				// fine modifica EnMe 02/05/2003 ---------------------------------------------
				
				
			end if
		end if
		
		if getitemstatus(li_i, 0, primary!) = newmodified! and isnull(getitemstring(li_i,"rag_soc_abbreviata")) then
			setitem(li_i,"rag_soc_abbreviata",getitemstring(li_i,"rag_soc_1"))
		end if
		
	next
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_cliente")) then
		tab_dettaglio.det_2.dw_clienti_det_2.object.b_ricerca_abicab.enabled=false
	else
		tab_dettaglio.det_2.dw_clienti_det_2.object.b_ricerca_abicab.enabled=false
	end if
	
	tab_dettaglio.det_2.dw_clienti_det_2.object.b_autocomposizione.enabled=false
	
	tab_dettaglio.det_2.dw_clienti_det_2.object.p_del_banca.enabled=false
end if
end event

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_clienti"
//   ls_codice = "cod_cliente"
//   ls_flag_tipo = "flag_tipo_cliente"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if

string ls_cod_fiscale, ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


if getrow() > 0 then
   f_upd_partita_iva("anag_clienti", "cod_cliente", "rag_soc_1")
   if this.getitemstring(i_rownbr, "flag_tipo_cliente") <> "E" and this.getitemstring(i_rownbr, "flag_tipo_cliente") <> "C" and this.getitemstring(i_rownbr, "flag_tipo_cliente") <> "M" and not isnull(this.getitemstring(i_rownbr, "cod_fiscale")) then
      ls_cod_fiscale = this.getitemstring(i_rownbr, "cod_fiscale")
      if not f_cod_fiscale(ls_cod_fiscale) then
        	g_mb.messagebox("Attenzione", "Inserire un codice fiscale valido!", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
	
	if isnull(getitemstring(getrow(),"cod_iva")) then
		if (tab_dettaglio.det_2.dw_clienti_det_2.getcolumnname() = "num_prot_esenzione_iva" and &
		      not isnull(tab_dettaglio.det_2.dw_clienti_det_2.gettext()) and len(tab_dettaglio.det_2.dw_clienti_det_2.gettext()) > 0) or &
		   (tab_dettaglio.det_2.dw_clienti_det_2.getcolumnname() <> "num_prot_esenzione_iva" and not isnull(this.getitemstring(getrow(), "num_prot_esenzione_iva"))) then
			g_mb.messagebox("APICE","Attenzione! Per indicare un protocollo esenzione è necessario aver indicato ~r~nanche un codice di esenzione.~r~nIl protocollo che è stato inserito e la relativa data verranno cancellati")
		end if
		this.setitem(getrow(), "num_prot_esenzione_iva", ls_null)
		this.setitem(getrow(), "data_esenzione_iva", ldt_null)
	end if

end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_rollback;call super::pcd_rollback;if s_cs_xx.parametri.impresa then
	rollback using sqlci;
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data)  then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.codice)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	tab_dettaglio.det_2.dw_clienti_det_2.object.b_ricerca_abicab.enabled=true
	
	//tab_dettaglio.det_2.dw_clienti_det_2.object.b_autocomposizione.enabled=true
	wf_abilita_componi_iban(getrow())
	
	tab_dettaglio.det_2.dw_clienti_det_2.object.p_del_banca.enabled=true
	
	setitem(getrow(),"data_creazione",datetime(today(),00:00:00))
	setitem(getrow(),"data_modifica",datetime(today(),00:00:00))
	
	// stefanop 22-10-2009: ticket 2009/219
	setitem(getrow(),"flag_accetta_mail","N")
	//-----------------------------------------------
	
	// EnMe 04/01/2012 ------------------------------------
	string ls_codice_automatico
	
	select flag
	into :ls_codice_automatico
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
			flag_parametro= 'F' and
			cod_parametro = 'CCA';
	//Codice Cliente Automatico
			
	if sqlca.sqlcode <> 0 then ls_codice_automatico = "N"
	
	if ls_codice_automatico = "S" then
		dw_clienti_det_1.object.cod_cliente.protect = 1
		dw_clienti_det_1.object.cod_cliente.background.color = 16777215
		
		s_cs_xx.parametri.parametro_b_1 = false
		
		window_open_parm(w_clienti_nuovo, 0, tab_dettaglio.det_1.dw_clienti_det_1)
		
		if not(s_cs_xx.parametri.parametro_b_1) then
			// qualcosa non è andato a buon fine
			tab_dettaglio.det_1.dw_clienti_det_1.resetupdate()
			tab_dettaglio.det_1.dw_clienti_det_1.resetupdate()
			tab_dettaglio.det_2.dw_clienti_det_2.resetupdate()
			tab_dettaglio.det_3.dw_clienti_det_3.resetupdate()
			
			tab_dettaglio.det_1.dw_clienti_det_1.Reset_DW_Modified(c_ResetChildren)
			
			parent.postevent("pc_view")
		else
			setitem(getrow(), "cod_cliente", s_cs_xx.parametri.parametro_s_15)
			setitem(getrow(), "cod_deposito",s_cs_xx.parametri.parametro_s_14)
			setitem(getrow(), "cod_capoconto",s_cs_xx.parametri.parametro_s_13)
			
		end if
		setnull(s_cs_xx.parametri.parametro_s_15)
		setnull(s_cs_xx.parametri.parametro_s_14)
		setnull(s_cs_xx.parametri.parametro_s_13)
	end if
		
		
end if
end event

event pcd_commit;call super::pcd_commit;if s_cs_xx.parametri.impresa then
	commit using sqlci;
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	tab_dettaglio.det_2.dw_clienti_det_2.object.b_ricerca_abicab.enabled=true
	
	//tab_dettaglio.det_2.dw_clienti_det_2.object.b_autocomposizione.enabled=true
	wf_abilita_componi_iban(getrow())
	
	tab_dettaglio.det_2.dw_clienti_det_2.object.p_del_banca.enabled=true
	
end if
end event

event doubleclicked;call super::doubleclicked;string ls_messaggio, ls_destinatari[], ls_allegati[],ls_errore
uo_outlook luo_outlook

choose case dwo.name
	case "casella_mail"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "casella_mail")
		if luo_outlook.uof_invio_outlook(0, "M", "", "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
		
		
	case "email_amministrazione"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "email_amministrazione")
		if luo_outlook.uof_invio_outlook(0, "M", "", "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
		
end choose


end event

type det_2 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3461
integer height = 2108
long backcolor = 12632256
string text = "Condizioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_clienti_det_2 dw_clienti_det_2
end type

on det_2.create
this.dw_clienti_det_2=create dw_clienti_det_2
this.Control[]={this.dw_clienti_det_2}
end on

on det_2.destroy
destroy(this.dw_clienti_det_2)
end on

type dw_clienti_det_2 from uo_cs_xx_dw within det_2
integer y = 20
integer width = 3465
integer height = 2072
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_clienti_det_2_TV"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string				ls_error, ls_iban, ls_cin, ls_abi, ls_cab, ls_cc, ls_errore
int					li_return
uo_banche		luo_banche

if row>0 then
else
	return
end if


choose case dwo.name
	case "b_ricerca_abicab"
		guo_ricerca.uof_ricerca_abicab(dw_clienti_det_2,"abi","cab")
		
	case "b_valida_iban"
		li_return = guo_functions.uof_valida_iban(getitemstring(row, "iban"), ref ls_error)
		
		if li_return > 0 then
			g_mb.success("IBAN valido")
		elseif li_return = 0 then
			g_mb.warning("Inserire un codice IBAN prima di eseguire la validazione")
		else
			g_mb.error(ls_error)
		end if
		
		
	case "b_autocomposizione"
		accepttext()
		luo_banche = create uo_banche
		ls_iban	= getitemstring(row, "iban")
		ls_cin		= getitemstring(row, "cin")
		ls_abi		= getitemstring(row, "abi")
		ls_cab	= getitemstring(row, "cab")
		ls_cc		= getitemstring(row, "conto_corrente")
		li_return = luo_banche.uof_componi_iban(0, ls_iban, ls_cin, ls_abi, ls_cab, ls_cc, ls_errore)		//per ora ls_errore non lo gestisco
		destroy luo_banche
		
		if li_return = 0 then
			setitem(row, "iban", ls_iban)
			setitem(row, "cin", ls_cin)
			setitem(row, "abi", ls_abi)
			setitem(row, "cab", ls_cab)
			setitem(row, "conto_corrente", ls_cc)
		end if
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_null

if i_extendmode then
	
	if isvalid(dwo) and not isnull( getitemstring(getrow(),"iban") )   then
		
		choose case lower(dwo.name)
			case "cin", "abi","cab"
				
				g_mb.warning("Attenzione! riferimenti banca cliente variato; reimpostare IBAN")
				
				setnull(ls_null)
				setitem(getrow(),"iban",ls_null)
				
		end choose
	end if
end if
end event

event clicked;call super::clicked;string				ls_null, ls_cod


if row>0 then
else
	return
end if

setnull(ls_null)

choose case dwo.name

		
	case "p_del_banca"
		
		ls_cod = getitemstring(row, "cod_cliente") 
		if ls_cod="" or isnull(ls_cod) then return
		
		if g_mb.confirm("Eliminare l'associazione della banca RI.BA: per il cliente?") then
			setitem(row, "iban", ls_null)
			setitem(row, "cin", ls_null)
			setitem(row, "abi", ls_null)
			setitem(row, "cab", ls_null)
			setitem(row, "conto_corrente", ls_null)
			setitem(row, "cod_banca_clien_for", ls_null)
			setitem(row, "swift", ls_null)
		end if
		
end choose
end event

type det_3 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3461
integer height = 2108
long backcolor = 12632256
string text = "Tabelle"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_clienti_det_3 dw_clienti_det_3
end type

on det_3.create
this.dw_clienti_det_3=create dw_clienti_det_3
this.Control[]={this.dw_clienti_det_3}
end on

on det_3.destroy
destroy(this.dw_clienti_det_3)
end on

type dw_clienti_det_3 from uo_cs_xx_dw within det_3
integer x = -5
integer y = 36
integer width = 3406
integer height = 2020
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_clienti_det_3_TV"
boolean border = false
long il_limit_campo_note = 255
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_utente"
		guo_ricerca.uof_ricerca_utente(dw_clienti_det_3,"cod_utente")
		
end choose
end event

event pcd_new;call super::pcd_new;object.b_ricerca_utente.enabled = true
end event

event pcd_modify;call super::pcd_modify;object.b_ricerca_utente.enabled = true
end event

event pcd_view;call super::pcd_view;object.b_ricerca_utente.enabled = false
end event

type det_5 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3461
integer height = 2108
long backcolor = 12632256
string text = "Nominativi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_nominativi dw_nominativi
end type

on det_5.create
this.dw_nominativi=create dw_nominativi
this.Control[]={this.dw_nominativi}
end on

on det_5.destroy
destroy(this.dw_nominativi)
end on

type dw_nominativi from uo_dw_nominativi within det_5
integer width = 3360
integer height = 2068
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_CLIENTI_nominativi_TV"
boolean border = false
end type

type det_4 from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3461
integer height = 2108
long backcolor = 12632256
string text = "Colonne Dinamiche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_col_din dw_col_din
end type

on det_4.create
this.dw_col_din=create dw_col_din
this.Control[]={this.dw_col_din}
end on

on det_4.destroy
destroy(this.dw_col_din)
end on

type dw_col_din from uo_colonne_dinamiche_dw within det_4
integer width = 3461
integer height = 2060
integer taborder = 170
boolean bringtotop = true
end type

