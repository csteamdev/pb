﻿$PBExportHeader$w_des_clienti.srw
$PBExportComments$Finestra Gestione Destinazioni Clienti
forward
global type w_des_clienti from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_des_clienti
end type
type cb_ricerca from commandbutton within w_des_clienti
end type
type dw_des_clienti_det from uo_cs_xx_dw within w_des_clienti
end type
type dw_ricerca from u_dw_search within w_des_clienti
end type
type dw_folder_search from u_folder within w_des_clienti
end type
type dw_des_clienti_lista from uo_cs_xx_dw within w_des_clienti
end type
end forward

global type w_des_clienti from w_cs_xx_principale
integer width = 2912
integer height = 1720
string title = "Destinazioni Clienti"
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_des_clienti_det dw_des_clienti_det
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_des_clienti_lista dw_des_clienti_lista
end type
global w_des_clienti w_des_clienti

event pc_setwindow;call super::pc_setwindow;string       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

windowobject lw_oggetti[],l_objects[]

dw_des_clienti_lista.set_dw_key("cod_azienda")

dw_des_clienti_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default + c_NoRetrieveOnOpen, &
                                    c_default)

dw_des_clienti_det.set_dw_options(sqlca, &
                                  dw_des_clienti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)

iuo_dw_main = dw_des_clienti_lista

l_criteriacolumn[1] = "rs_cod_cliente"
l_criteriacolumn[2] = "rs_cod_des_cliente"
l_criteriacolumn[3] = "rs_flag_blocco"
l_criteriacolumn[4] = "rs_cod_deposito"

l_searchtable[1] = "anag_des_clienti"
l_searchtable[2] = "anag_des_clienti"
l_searchtable[3] = "anag_des_clienti"
l_searchtable[4] = "anag_des_clienti"

l_searchcolumn[1] = "cod_cliente"
l_searchcolumn[2] = "cod_des_cliente"
l_searchcolumn[3] = "flag_blocco"
l_searchcolumn[4] = "cod_deposito"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_des_clienti_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)
							
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_des_clienti_lista

dw_folder_search.fu_assigntab(1, "L.", l_objects[])

l_objects[1] = dw_ricerca

l_objects[2] = cb_ricerca

l_objects[3] = cb_reset

//l_objects[4] = cb_ricerca_cliente

dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)

dw_folder_search.fu_selecttab(2)

dw_des_clienti_lista.change_dw_current()

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_des_clienti_det, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_ricerca, &
                 "rs_cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

								
end event

on w_des_clienti.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_des_clienti_det=create dw_des_clienti_det
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_des_clienti_lista=create dw_des_clienti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.dw_des_clienti_det
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.dw_folder_search
this.Control[iCurrent+6]=this.dw_des_clienti_lista
end on

on w_des_clienti.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_des_clienti_det)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_des_clienti_lista)
end on

type cb_reset from commandbutton within w_des_clienti
integer x = 2423
integer y = 80
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_des_clienti
integer x = 2423
integer y = 168
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_BuildSearch(TRUE)

dw_folder_search.fu_SelectTab(1)

dw_des_clienti_lista.change_dw_current()

parent.triggerevent("pc_retrieve")


end event

type dw_des_clienti_det from uo_cs_xx_dw within w_des_clienti
integer x = 23
integer y = 540
integer width = 2834
integer height = 1060
integer taborder = 20
string dataobject = "d_des_clienti_det"
borderstyle borderstyle = styleraised!
end type

event getfocus;call super::getfocus;dw_des_clienti_det.change_dw_current()

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw

s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_des_clienti_det,"cod_cliente")
end choose
end event

event pcd_modify;call super::pcd_modify;dw_des_clienti_det.object.b_ricerca_cliente.enabled = true
end event

event pcd_new;call super::pcd_new;dw_des_clienti_det.object.b_ricerca_cliente.enabled = true
end event

event pcd_view;call super::pcd_view;dw_des_clienti_det.object.b_ricerca_cliente.enabled = false
end event

type dw_ricerca from u_dw_search within w_des_clienti
event ue_key pbm_dwnkey
integer x = 183
integer y = 60
integer width = 2629
integer height = 420
integer taborder = 30
string dataobject = "d_des_clienti_search"
end type

event ue_key;choose case this.getcolumnname()
		
	case "rs_cod_cliente"
		
		if key = keyF1!  and keyflags = 1 then
			
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			
			guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
		end if
		
end choose

if key = keyenter! then cb_ricerca.postevent("clicked")
end event

event itemchanged;call super::itemchanged;string ls_null

if not isnull(dwo) then
	
	if isnull(data) then data = ""
	
	if data = "" then 
		
		setnull(ls_null)
		
		this.setitem( row, "rs_cod_des_cliente", ls_null)
		
		return
		
	end if
	
	choose case dwo.Name
	
		case "rs_cod_cliente"
			
			setnull(ls_null)
			
			f_po_loaddddw_dw( this, &
									"rs_cod_des_cliente", &
									sqlca, &
									"anag_des_clienti", &
									"cod_des_cliente", &
									"rag_soc_1", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + data + "' ")
		 
			this.setitem( row, "rs_cod_des_cliente", ls_null)		
			
	end choose
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
end choose
end event

type dw_folder_search from u_folder within w_des_clienti
integer x = 23
integer y = 20
integer width = 2834
integer height = 500
integer taborder = 20
end type

type dw_des_clienti_lista from uo_cs_xx_dw within w_des_clienti
integer x = 183
integer y = 60
integer width = 2629
integer height = 420
integer taborder = 10
string dataobject = "d_des_clienti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
	
   pcca.error = c_fatal
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		
   end if
	
next
end event

event pcd_new;call super::pcd_new;dw_des_clienti_det.setitem( dw_des_clienti_det.getrow(), "flag_dest_merce_fat", "N")


end event

event pcd_save;call super::pcd_save;//dw_des_clienti_det.Modify("cod_cliente.Background.Color='12632256'")
//
//dw_des_clienti_det.Object.cod_cliente.Protect=1								
//
end event

event pcd_validaterow;call super::pcd_validaterow;if i_rownbr > 0 and i_insave > 0 then	
	i_colnbr = column_nbr("cod_cliente")
	if isnull(get_col_data(i_rownbr, i_colnbr)) or get_col_data(i_rownbr, i_colnbr) = "" then
		g_mb.messagebox("Attenzione", "Codice cliente obbligatorio.", &
						  exclamation!, ok!)
		setcolumn("cod_cliente")
		pcca.error = c_fatal
		return
	end if
	
	i_colnbr = column_nbr("cod_des_cliente")
	if isnull(get_col_data(i_rownbr, i_colnbr)) or get_col_data(i_rownbr, i_colnbr) = "" then
		g_mb.messagebox("Attenzione", "Codice destinazione obbligatorio.", &
						  exclamation!, ok!)
		setcolumn("cod_des_cliente")
		pcca.error = c_fatal
		return
	end if
	
	i_colnbr = column_nbr("rag_soc_1")
	if isnull(get_col_data(i_rownbr, i_colnbr)) or get_col_data(i_rownbr, i_colnbr) = "" then
		g_mb.messagebox("Attenzione", "Codice Ragione sociale obbligatorio.", &
						  exclamation!, ok!)
		setcolumn("rag_soc_1")
		pcca.error = c_fatal
		return
	end if
end if

end event

