﻿$PBExportHeader$w_des_fornitori.srw
$PBExportComments$Finestra Gestione Fornitori
forward
global type w_des_fornitori from w_cs_xx_principale
end type
type dw_des_fornitori_lista from uo_cs_xx_dw within w_des_fornitori
end type
type dw_des_fornitori_det from uo_cs_xx_dw within w_des_fornitori
end type
end forward

global type w_des_fornitori from w_cs_xx_principale
integer width = 2912
integer height = 1568
string title = "Gestione Destinazioni Fornitori"
dw_des_fornitori_lista dw_des_fornitori_lista
dw_des_fornitori_det dw_des_fornitori_det
end type
global w_des_fornitori w_des_fornitori

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_des_fornitori_lista.set_dw_key("cod_azienda")
dw_des_fornitori_lista.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                      c_default, &
                                      c_default)
dw_des_fornitori_det.set_dw_options(sqlca, &
                                    dw_des_fornitori_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_des_fornitori_lista
end on

event pc_setddlb;call super::pc_setddlb;//Giulio: 22/11/2011 il campo è passato da dropdown a testo
//f_po_loaddddw_dw(dw_des_fornitori_det, &
//                 "cod_fornitore", &
//                 sqlca, &
//                 "anag_fornitori", &
//                 "cod_fornitore", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//
//---fine modifica Giulio.

end event

on w_des_fornitori.create
int iCurrent
call super::create
this.dw_des_fornitori_lista=create dw_des_fornitori_lista
this.dw_des_fornitori_det=create dw_des_fornitori_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_des_fornitori_lista
this.Control[iCurrent+2]=this.dw_des_fornitori_det
end on

on w_des_fornitori.destroy
call super::destroy
destroy(this.dw_des_fornitori_lista)
destroy(this.dw_des_fornitori_det)
end on

type dw_des_fornitori_lista from uo_cs_xx_dw within w_des_fornitori
integer x = 23
integer y = 20
integer width = 2834
integer height = 500
integer taborder = 10
string dataobject = "d_des_fornitori_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_des_fornitori_det from uo_cs_xx_dw within w_des_fornitori
integer x = 23
integer y = 540
integer width = 2834
integer height = 900
integer taborder = 20
string dataobject = "d_des_fornitori_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_des_fornitori_det,"cod_fornitore")
end choose
end event

event pcd_delete;call super::pcd_delete;dw_des_fornitori_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_new;call super::pcd_new;dw_des_fornitori_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_des_fornitori_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;dw_des_fornitori_det.object.b_ricerca_fornitore.enabled = false
end event

event pcd_save;call super::pcd_save;dw_des_fornitori_det.object.b_ricerca_fornitore.enabled = false
end event

