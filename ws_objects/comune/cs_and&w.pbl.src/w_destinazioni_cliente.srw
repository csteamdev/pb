﻿$PBExportHeader$w_destinazioni_cliente.srw
forward
global type w_destinazioni_cliente from w_cs_xx_principale
end type
type dw_dettaglio from uo_cs_xx_dw within w_destinazioni_cliente
end type
type dw_lista from uo_cs_xx_dw within w_destinazioni_cliente
end type
end forward

global type w_destinazioni_cliente from w_cs_xx_principale
integer width = 2962
integer height = 1952
string title = "Destinazioni Cliente"
dw_dettaglio dw_dettaglio
dw_lista dw_lista
end type
global w_destinazioni_cliente w_destinazioni_cliente

on w_destinazioni_cliente.create
int iCurrent
call super::create
this.dw_dettaglio=create dw_dettaglio
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dettaglio
this.Control[iCurrent+2]=this.dw_lista
end on

on w_destinazioni_cliente.destroy
call super::destroy
destroy(this.dw_dettaglio)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_cliente")

dw_lista.set_dw_options(		sqlca, &
										i_openparm, &
										c_scrollparent, &
										c_default + &
										c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
										
dw_dettaglio.set_dw_options(	sqlca, &
										dw_lista, &
										c_sharedata + c_scrollparent, &
										c_default)


iuo_dw_main = dw_lista

dw_dettaglio.object.cod_cliente.visible = false
dw_dettaglio.object.b_ricerca_cliente.visible = false
dw_dettaglio.object.cod_cliente_t.visible = false
dw_dettaglio.object.cf_cod_cliente.visible = false
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_dettaglio, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

type dw_dettaglio from uo_cs_xx_dw within w_destinazioni_cliente
integer x = 32
integer y = 752
integer width = 2866
integer height = 1072
integer taborder = 20
string dataobject = "d_des_clienti_det"
borderstyle borderstyle = styleraised!
end type

type dw_lista from uo_cs_xx_dw within w_destinazioni_cliente
integer x = 32
integer y = 20
integer width = 2866
integer height = 704
integer taborder = 10
string dataobject = "d_destinazioni_cliente_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_rag_soc_1
long ll_errore

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
ls_rag_soc_1 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "rag_soc_1")


parent.title = "Destinazioni Cliente  " + ls_cod_cliente + " - " + ls_rag_soc_1

ll_errore = retrieve(s_cs_xx.cod_azienda,ls_cod_cliente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string			ls_cod_cliente

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) or getitemstring(ll_i, "cod_azienda")="" then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(ll_i, "cod_cliente")) or getitemstring(ll_i, "cod_cliente") = "" then
		setitem(ll_i, "cod_cliente", ls_cod_cliente)
	end if

next

end event

event pcd_validaterow;call super::pcd_validaterow;if i_rownbr > 0 and i_insave > 0 then	
		
	i_colnbr = column_nbr("cod_des_cliente")
	if isnull(get_col_data(i_rownbr, i_colnbr)) or get_col_data(i_rownbr, i_colnbr) = "" then
		g_mb.warning( "Codice destinazione obbligatorio!")
		setcolumn("cod_des_cliente")
		pcca.error = c_fatal
		return
	end if
	
	i_colnbr = column_nbr("rag_soc_1")
	if isnull(get_col_data(i_rownbr, i_colnbr)) or get_col_data(i_rownbr, i_colnbr) = "" then
		g_mb.warning("Codice Ragione sociale obbligatorio!")
		setcolumn("rag_soc_1")
		pcca.error = c_fatal
		return
	end if
end if

end event

