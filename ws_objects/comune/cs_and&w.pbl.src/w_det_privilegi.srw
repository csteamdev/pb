﻿$PBExportHeader$w_det_privilegi.srw
forward
global type w_det_privilegi from w_cs_xx_principale
end type
type dw_privilegi from uo_cs_xx_dw within w_det_privilegi
end type
end forward

global type w_det_privilegi from w_cs_xx_principale
integer width = 2318
integer height = 1700
string title = "Privilegi"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
boolean center = true
dw_privilegi dw_privilegi
end type
global w_det_privilegi w_det_privilegi

type variables
private:
	string id_cod_privilegio
end variables

on w_det_privilegi.create
int iCurrent
call super::create
this.dw_privilegi=create dw_privilegi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_privilegi
end on

on w_det_privilegi.destroy
call super::destroy
destroy(this.dw_privilegi)
end on

event pc_setwindow;call super::pc_setwindow;id_cod_privilegio = s_cs_xx.parametri.parametro_s_1
setnull(s_cs_xx.parametri.parametro_s_1)

dw_privilegi.move(20,20)


dw_privilegi.set_dw_key("cod_azienda")
dw_privilegi.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

iuo_dw_main = dw_privilegi
end event

event resize;/* BASTA ANCESTOR **/

dw_privilegi.resize(newwidth - 40, newheight - 40)
end event

type dw_privilegi from uo_cs_xx_dw within w_det_privilegi
integer x = 23
integer y = 20
integer width = 2263
integer height = 1580
integer taborder = 10
string dataobject = "d_det_privilegi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda, id_cod_privilegio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(this.getitemstring(ll_i, "cod_privilegio")) then
      this.setitem(ll_i, "cod_privilegio", id_cod_privilegio)
   end if
	
next
end event

event itemchanged;call super::itemchanged;int li_count = 0, li_i = 0

choose case dwo.name
		
	case "flag_default"
		if data = "S" then
			
			// controllo che sia stato impostato un solo valore di default		
			for li_i = 1 to rowcount()
				
				if getitemstring(li_i, "flag_default") = "S" then li_count++
				
			next
			
			if li_count > 0 then
				g_mb.show("E' possibile impostare un solo valore di Default.")
				return 2
			end if
			// ---
			
		end if
		
end choose
end event

