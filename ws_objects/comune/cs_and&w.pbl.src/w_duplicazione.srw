﻿$PBExportHeader$w_duplicazione.srw
forward
global type w_duplicazione from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_duplicazione
end type
type dw_aziende from datawindow within w_duplicazione
end type
type dw_selezionate from datawindow within w_duplicazione
end type
type cb_aggiungi from commandbutton within w_duplicazione
end type
type cb_tutte from commandbutton within w_duplicazione
end type
type cb_togli from commandbutton within w_duplicazione
end type
type cb_nessuna from commandbutton within w_duplicazione
end type
type cb_prima from commandbutton within w_duplicazione
end type
type cb_su from commandbutton within w_duplicazione
end type
type cb_giu from commandbutton within w_duplicazione
end type
type cb_ultima from commandbutton within w_duplicazione
end type
type cb_annulla from commandbutton within w_duplicazione
end type
type cb_ok from commandbutton within w_duplicazione
end type
type st_3 from statictext within w_duplicazione
end type
type st_stato from statictext within w_duplicazione
end type
type gb_1 from groupbox within w_duplicazione
end type
type gb_3 from groupbox within w_duplicazione
end type
type dw_disponibili from datawindow within w_duplicazione
end type
type gb_4 from groupbox within w_duplicazione
end type
end forward

global type w_duplicazione from w_cs_xx_principale
integer width = 3291
integer height = 1648
string title = "Duplicazione Tabelle"
boolean maxbox = false
boolean resizable = false
event ue_allinea ( )
hpb_1 hpb_1
dw_aziende dw_aziende
dw_selezionate dw_selezionate
cb_aggiungi cb_aggiungi
cb_tutte cb_tutte
cb_togli cb_togli
cb_nessuna cb_nessuna
cb_prima cb_prima
cb_su cb_su
cb_giu cb_giu
cb_ultima cb_ultima
cb_annulla cb_annulla
cb_ok cb_ok
st_3 st_3
st_stato st_stato
gb_1 gb_1
gb_3 gb_3
dw_disponibili dw_disponibili
gb_4 gb_4
end type
global w_duplicazione w_duplicazione

type variables
string is_focus
end variables

forward prototypes
public subroutine wf_allinea_selezionate ()
public function integer wf_duplica_det_attrezzature (ref string as_errore)
end prototypes

event ue_allinea();long    ll_row, ll_i

string  ls_nome

boolean lb_trovato

datawindow ldw_1, ldw_2


choose case is_focus
		
	case "D"
		
		ldw_1 = dw_disponibili
		ldw_2 = dw_selezionate
		
	case "S"
		
		ldw_1 = dw_selezionate
		ldw_2 = dw_disponibili
		
end choose

ll_row = ldw_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	return
end if

ls_nome = ldw_1.getitemstring(ll_row,"nome_tabella")

for ll_i = 1 to ldw_2.rowcount()
	if ldw_2.getitemstring(ll_i,"nome_tabella") = ls_nome then
		ldw_2.setrow(ll_i)
		ldw_2.scrolltorow(ll_i)
		exit
	end if
next
end event

public subroutine wf_allinea_selezionate ();
end subroutine

public function integer wf_duplica_det_attrezzature (ref string as_errore);string					ls_azienda_origine, ls_azienda_destinazione, ls_sql, ls_cod_attrezzatura
datastore			lds_data
long					ll_tot, ll_index, ll_prog_attrezzatura
blob					lb_blob


hpb_1.position = 0
st_stato.text = "Preparo duplicazione documenti attrezz. in corso ..."

ls_azienda_origine = dw_aziende.getitemstring(dw_aziende.getrow(),"origine")
ls_azienda_destinazione = dw_aziende.getitemstring(dw_aziende.getrow(),"destinazione")

//prima importo i mimetype
//poi la tabella tab_protocolli, se vuota
//questo per evitare l'errore di FK
insert into tab_mimetype  
         ( cod_azienda,   
           prog_mimetype,   
           estensione,   
           applicazione,   
           mimetype )
select :ls_azienda_destinazione,   
           prog_mimetype,   
           estensione,   
           applicazione,   
           mimetype
from tab_mimetype
where cod_azienda=:ls_azienda_origine;

if sqlca.sqlcode<0 then
	as_errore = "Errore in pre-popolamento mimetype: "+sqlca.sqlerrtext
	return -1
end if


insert into tab_protocolli  
         ( cod_azienda,   
           num_protocollo,   
           livello_accesso )  
 select	 :ls_azienda_destinazione,
 			num_protocollo,   
			livello_accesso
from tab_protocolli
where cod_azienda=:ls_azienda_origine;

if sqlca.sqlcode<0 then
	as_errore = "Errore in pre-popolamento tab.protocolli: "+sqlca.sqlerrtext
	return -1
end if



//quindi la det_attrezzature
ls_sql = "select		cod_azienda,"+&
						"cod_attrezzatura,"+&
						"prog_attrezzatura,"+&
						"descrizione,"+&
						"num_protocollo,"+&
						"flag_blocco,"+&
						"data_blocco,"+&
						"des_blob,"+&
						"path_documento,"+&
						"prog_mimetype "+&
		"from det_attrezzature "+&
		"where cod_azienda='"+ls_azienda_origine+"' and "+&
         		 "cod_attrezzatura is not null and "+&
         		 "cod_attrezzatura<>'' "+&
		"order by cod_attrezzatura, prog_attrezzatura"

st_stato.text = "Preparo query, attendere ..."

ll_tot = guo_functions.uof_crea_datastore(lds_data,ls_sql, as_errore)
if ll_tot<0 then
	as_errore = "Errore in creazione ds documenti attrezzature: "+sqlca.sqlerrtext
	return -1
end if

hpb_1.maxposition = ll_tot
hpb_1.setstep = 1

for ll_index=1 to ll_tot
	Yield()
	
	ls_azienda_origine = lds_data.getitemstring(ll_index, "cod_azienda")
	ls_cod_attrezzatura = lds_data.getitemstring(ll_index, "cod_attrezzatura")
	ll_prog_attrezzatura = lds_data.getitemnumber(ll_index, "prog_attrezzatura")
	
	st_stato.text = "INS. cod. "+ls_cod_attrezzatura+" ( doc. "+string(ll_index)+" di "+string(ll_tot)+") ..."
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	//inserisco il det attrezzatura con progressivo
	insert into det_attrezzature(
		cod_azienda,   
		cod_attrezzatura,   
		prog_attrezzatura,   
		descrizione,   
		num_protocollo,   
		flag_blocco,   
		data_blocco,   
		des_blob,
		blob,
		path_documento,   
		prog_mimetype )
	select		:ls_azienda_destinazione,   
				:ls_cod_attrezzatura,   
				:ll_prog_attrezzatura,   
				descrizione,   
				num_protocollo,   
				flag_blocco,   
				data_blocco,   
				des_blob,
				null,   
				path_documento,   
				prog_mimetype
	from det_attrezzature
	where 	cod_azienda = :ls_azienda_origine and
				cod_attrezzatura=:ls_cod_attrezzatura and
				prog_attrezzatura = :ll_prog_attrezzatura
	using sqlca;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore INSERT attrezzatura " + ls_cod_attrezzatura + ", progressivo "+string(ll_prog_attrezzatura) + " : "+sqlca.sqlerrtext
		return -1
	end if
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------
	//leggo il blob del documento
	selectblob blob
	into :lb_blob
	from det_attrezzature
	where 	cod_azienda = :ls_azienda_origine and
				cod_attrezzatura=:ls_cod_attrezzatura and
				prog_attrezzatura = :ll_prog_attrezzatura
	using sqlca;
	
	if not isnull(lb_blob) and len(lb_blob)>0 then
		st_stato.text = "UPD. cod. "+ls_cod_attrezzatura+" ( doc. "+string(ll_index)+" di "+string(ll_tot)+") ..."
	
		//aggiorno il blob nell'azienda destinazione
		updateblob det_attrezzature
		set blob = :lb_blob
		where 	cod_azienda = :ls_azienda_destinazione and
					cod_attrezzatura=:ls_cod_attrezzatura and
					prog_attrezzatura = :ll_prog_attrezzatura
		using sqlca;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore UPDATEBLOB attrezzatura " + ls_cod_attrezzatura + ", progressivo "+string(ll_prog_attrezzatura) + " : "+sqlca.sqlerrtext
			return -1
		end if
	end if
	
	hpb_1.stepit()
next

return 0
end function

on w_duplicazione.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dw_aziende=create dw_aziende
this.dw_selezionate=create dw_selezionate
this.cb_aggiungi=create cb_aggiungi
this.cb_tutte=create cb_tutte
this.cb_togli=create cb_togli
this.cb_nessuna=create cb_nessuna
this.cb_prima=create cb_prima
this.cb_su=create cb_su
this.cb_giu=create cb_giu
this.cb_ultima=create cb_ultima
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.st_3=create st_3
this.st_stato=create st_stato
this.gb_1=create gb_1
this.gb_3=create gb_3
this.dw_disponibili=create dw_disponibili
this.gb_4=create gb_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dw_aziende
this.Control[iCurrent+3]=this.dw_selezionate
this.Control[iCurrent+4]=this.cb_aggiungi
this.Control[iCurrent+5]=this.cb_tutte
this.Control[iCurrent+6]=this.cb_togli
this.Control[iCurrent+7]=this.cb_nessuna
this.Control[iCurrent+8]=this.cb_prima
this.Control[iCurrent+9]=this.cb_su
this.Control[iCurrent+10]=this.cb_giu
this.Control[iCurrent+11]=this.cb_ultima
this.Control[iCurrent+12]=this.cb_annulla
this.Control[iCurrent+13]=this.cb_ok
this.Control[iCurrent+14]=this.st_3
this.Control[iCurrent+15]=this.st_stato
this.Control[iCurrent+16]=this.gb_1
this.Control[iCurrent+17]=this.gb_3
this.Control[iCurrent+18]=this.dw_disponibili
this.Control[iCurrent+19]=this.gb_4
end on

on w_duplicazione.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dw_aziende)
destroy(this.dw_selezionate)
destroy(this.cb_aggiungi)
destroy(this.cb_tutte)
destroy(this.cb_togli)
destroy(this.cb_nessuna)
destroy(this.cb_prima)
destroy(this.cb_su)
destroy(this.cb_giu)
destroy(this.cb_ultima)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.st_3)
destroy(this.st_stato)
destroy(this.gb_1)
destroy(this.gb_3)
destroy(this.dw_disponibili)
destroy(this.gb_4)
end on

event pc_setwindow;call super::pc_setwindow;long   ll_i, ll_riga

string ls_tabelle[], ls_descrizioni[]


dw_aziende.insertrow(0)
dw_aziende.insertrow(0)

dw_disponibili.setrowfocusindicator(hand!)
dw_selezionate.setrowfocusindicator(hand!)

dw_disponibili.object.t_1.text = "Tabelle disponibili"
dw_selezionate.object.t_1.text = "Tabelle selezionate"


ls_tabelle[1] = "parametri_manutenzioni"
ls_descrizioni[1] = "Parametri Manutenzioni"
ls_tabelle[2] = "parametri_azienda"
ls_descrizioni[2] = "Parametri Azienda"
ls_tabelle[3] = "parametri_omnia"
ls_descrizioni[3] = "Parametri OMNIA"
ls_tabelle[4] = "tab_tipi_cause"
ls_descrizioni[4] = "Tipi Cause"
ls_tabelle[5] = "cause"
ls_descrizioni[5] = "Cause"
ls_tabelle[6] = "tab_valute"
ls_descrizioni[6] = "Valute"
ls_tabelle[7] = "anag_depositi"
ls_descrizioni[7] = "Depositi"
ls_tabelle[8] = "anag_clienti"
ls_descrizioni[8] = "Anagrafica Clienti"
ls_tabelle[9] = "anag_fornitori"
ls_descrizioni[9] = "Anagrafica Fornitori"
ls_tabelle[10] = "anag_divisioni"
ls_descrizioni[10] = "Divisioni Aziendali"
ls_tabelle[11] = "tab_aree_aziendali"
ls_descrizioni[11] = "Aree Aziendali"
ls_tabelle[12] = "anag_reparti"
ls_descrizioni[12] = "Reparti"
ls_tabelle[13] = "mansionari"
ls_descrizioni[13] = "Mansionari"
ls_tabelle[14] = "tab_trattamenti"
ls_descrizioni[14] = "Trattamenti"
ls_tabelle[15] = "tab_difformita"
ls_descrizioni[15] = "Difetti"
ls_tabelle[16] = "tab_misure"
ls_descrizioni[16] = "Unità di Misura"
ls_tabelle[17] = "anag_prodotti"
ls_descrizioni[17] = "Anagrafica Prodotti"
ls_tabelle[18] = "tab_cat_attrezzature"
ls_descrizioni[18] = "Categorie Attrezzature"
ls_tabelle[19] = "anag_attrezzature"
ls_descrizioni[19] = "Anagrafica Attrezzature"
ls_tabelle[20] = "tes_liste_controllo"
ls_descrizioni[20] = "Liste di Controllo / Customer Satisfaction"
ls_tabelle[21] = "det_liste_controllo"
ls_descrizioni[21] = "Dettagli Liste Controllo / Customer Satisf."
ls_tabelle[22] = "tab_tipi_manutenzione"
ls_descrizioni[22] = "Tipologie Manutenzione"
ls_tabelle[23] = "tab_guasti"
ls_descrizioni[23] = "Guasti"

for ll_i = 1 to upperbound(ls_tabelle)	
	ll_riga = dw_disponibili.insertrow(0)	
	dw_disponibili.setitem(ll_riga,"nome_tabella",ls_tabelle[ll_i])	
	dw_disponibili.setitem(ll_riga,"descrizione",ls_descrizioni[ll_i])
next
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_aziende,"origine",sqlca,"aziende","cod_azienda","rag_soc_1","")
					  
f_po_loaddddw_dw(dw_aziende,"destinazione",sqlca,"aziende","cod_azienda","rag_soc_1","")
end event

type hpb_1 from hprogressbar within w_duplicazione
integer x = 594
integer y = 1428
integer width = 448
integer height = 64
unsignedinteger maxposition = 100
integer setstep = 1
boolean smoothscroll = true
end type

type dw_aziende from datawindow within w_duplicazione
integer y = 20
integer width = 3017
integer height = 200
integer taborder = 10
string dataobject = "d_aziende_duplicazione"
boolean border = false
boolean livescroll = true
end type

type dw_selezionate from datawindow within w_duplicazione
integer x = 1646
integer y = 240
integer width = 1349
integer height = 1120
integer taborder = 10
string dataobject = "d_tabelle_duplicazione"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;is_focus = "S"

parent.postevent("ue_allinea")
end event

type cb_aggiungi from commandbutton within w_duplicazione
integer x = 1440
integer y = 280
integer width = 128
integer height = 264
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


ll_row = dw_disponibili.getrow()

if isnull(ll_row) or ll_row = 0 then
	return -1
end if

if dw_disponibili.getitemstring(ll_row,"selezione") = "S" then
	return -1
end if

ls_nome = dw_disponibili.getitemstring(ll_row,"nome_tabella")

ls_descrizione = dw_disponibili.getitemstring(ll_row,"descrizione")

ll_riga = dw_selezionate.insertrow(0)

dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)

dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)

dw_disponibili.setitem(ll_row,"selezione","S")

is_focus = "D"

parent.postevent("ue_allinea")
end event

type cb_tutte from commandbutton within w_duplicazione
integer x = 1440
integer y = 540
integer width = 128
integer height = 264
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">>"
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


for ll_row = 1 to dw_disponibili.rowcount()

	if dw_disponibili.getitemstring(ll_row,"selezione") = "S" then
		continue
	end if
	
	ls_nome = dw_disponibili.getitemstring(ll_row,"nome_tabella")
	
	ls_descrizione = dw_disponibili.getitemstring(ll_row,"descrizione")
	
	ll_riga = dw_selezionate.insertrow(0)
	
	dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)
	
	dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)
	
	dw_disponibili.setitem(ll_row,"selezione","S")
	
next

is_focus = "D"

parent.postevent("ue_allinea")
end event

type cb_togli from commandbutton within w_duplicazione
integer x = 1440
integer y = 800
integer width = 128
integer height = 264
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;long   ll_i, ll_row

string ls_nome


ll_row = dw_selezionate.getrow()

if isnull(ll_row) or ll_row = 0 then
	return -1
end if

ls_nome = dw_selezionate.getitemstring(ll_row,"nome_tabella")

for ll_i = 1 to dw_disponibili.rowcount()
	if dw_disponibili.getitemstring(ll_i,"nome_tabella") = ls_nome then
		dw_disponibili.setitem(ll_i,"selezione","N")
		dw_selezionate.deleterow(ll_row)
	end if
next

is_focus = "S"

parent.postevent("ue_allinea")
end event

type cb_nessuna from commandbutton within w_duplicazione
integer x = 1440
integer y = 1060
integer width = 128
integer height = 264
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<"
end type

event clicked;long   ll_i


if isnull(dw_selezionate.rowcount()) or dw_selezionate.rowcount() = 0 then
	return -1
end if

dw_selezionate.reset()

for ll_i = 1 to dw_disponibili.rowcount()	
	dw_disponibili.setitem(ll_i,"selezione","N")
next
end event

type cb_prima from commandbutton within w_duplicazione
integer x = 3063
integer y = 280
integer width = 128
integer height = 264
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pr."
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


ll_row = dw_selezionate.getrow()

if isnull(ll_row) or ll_row = 0 or ll_row = 1 then
	return -1
end if

ls_nome = dw_selezionate.getitemstring(ll_row,"nome_tabella")

ls_descrizione = dw_selezionate.getitemstring(ll_row,"descrizione")

dw_selezionate.deleterow(ll_row)

ll_riga = dw_selezionate.insertrow(1)

dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)

dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)

dw_selezionate.setrow(ll_riga)

dw_selezionate.scrolltorow(ll_riga)
end event

type cb_su from commandbutton within w_duplicazione
integer x = 3063
integer y = 540
integer width = 128
integer height = 264
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Su"
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


ll_row = dw_selezionate.getrow()

if isnull(ll_row) or ll_row = 0 or ll_row = 1 then
	return -1
end if

ls_nome = dw_selezionate.getitemstring(ll_row,"nome_tabella")

ls_descrizione = dw_selezionate.getitemstring(ll_row,"descrizione")

dw_selezionate.deleterow(ll_row)

ll_riga = dw_selezionate.insertrow(ll_row - 1)

dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)

dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)

dw_selezionate.setrow(ll_riga)

dw_selezionate.scrolltorow(ll_riga)
end event

type cb_giu from commandbutton within w_duplicazione
integer x = 3063
integer y = 800
integer width = 128
integer height = 264
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Giù"
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


ll_row = dw_selezionate.getrow()

if isnull(ll_row) or ll_row = 0 or ll_row = dw_selezionate.rowcount() then
	return -1
end if

ls_nome = dw_selezionate.getitemstring(ll_row,"nome_tabella")

ls_descrizione = dw_selezionate.getitemstring(ll_row,"descrizione")

dw_selezionate.deleterow(ll_row)

ll_riga = dw_selezionate.insertrow(ll_row + 1)

dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)

dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)

dw_selezionate.setrow(ll_riga)

dw_selezionate.scrolltorow(ll_riga)
end event

type cb_ultima from commandbutton within w_duplicazione
integer x = 3063
integer y = 1060
integer width = 128
integer height = 264
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ult."
end type

event clicked;long   ll_row, ll_riga

string ls_nome, ls_descrizione


ll_row = dw_selezionate.getrow()

if isnull(ll_row) or ll_row = 0 or ll_row = dw_selezionate.rowcount() then
	return -1
end if

ls_nome = dw_selezionate.getitemstring(ll_row,"nome_tabella")

ls_descrizione = dw_selezionate.getitemstring(ll_row,"descrizione")

dw_selezionate.deleterow(ll_row)

ll_riga = dw_selezionate.insertrow(0)

dw_selezionate.setitem(ll_riga,"nome_tabella",ls_nome)

dw_selezionate.setitem(ll_riga,"descrizione",ls_descrizione)

dw_selezionate.setrow(ll_riga)

dw_selezionate.scrolltorow(ll_riga)
end event

type cb_annulla from commandbutton within w_duplicazione
integer x = 2423
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_duplicazione
integer x = 2834
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;long						ll_i, ll_count

string						ls_origine, ls_destinazione, ls_tabelle[], ls_descrizioni[], ls_messaggio, ls_errore

uo_duplicazione		luo_duplicazione

boolean					lb_attrezzature = false

integer					li_ret



ls_origine = dw_aziende.getitemstring(dw_aziende.getrow(),"origine")

if isnull(ls_origine) or ls_origine = "" then
	g_mb.messagebox("OMNIA","Selezionare l'azienda di origine prima di continuare",stopsign!)
	return -1
end if

ls_destinazione = dw_aziende.getitemstring(dw_aziende.getrow(),"destinazione")

if isnull(ls_destinazione) or ls_destinazione = "" then
	g_mb.messagebox("OMNIA","Selezionare l'azienda di destinazione prima di continuare",stopsign!)
	return -1
end if

if ls_origine = ls_destinazione then
	g_mb.messagebox("OMNIA","L'azienda di destinazione e l'azienda di origine non devono essere uguali",stopsign!)
	return -1
end if

if dw_selezionate.rowcount() < 1 then
	g_mb.messagebox("OMNIA","Selezionare almeno una tabella prima di continuare",stopsign!)
	return -1
end if

setpointer(hourglass!)

st_stato.text = "0%   Lettura elenco tabelle"

for ll_i = 1 to dw_selezionate.rowcount()	
	ls_tabelle[ll_i] = dw_selezionate.getitemstring(ll_i,"nome_tabella")
	ls_descrizioni[ll_i] = dw_selezionate.getitemstring(ll_i,"descrizione")
	
	if upper(ls_tabelle[ll_i]) = upper("anag_attrezzature") then
		//se tra i selezionati nella duplicazione c'è la parte relativa alla tabella attrezzature
		//e ci sono documenti associati alle attrezzature nell'azienda di partenza
		//predisponi per la duplicazione (ma solo su conferma dell'operatore)
		select count(*)
		into :ll_count
		from det_attrezzature
		where cod_azienda=:ls_origine;
		
		if ll_count>0 then
			//attiva la richiesta
			lb_attrezzature = true
		end if

	end if

next

st_stato.text = "0%   Avvio duplicazione"

luo_duplicazione = create uo_duplicazione
li_ret = luo_duplicazione.uof_duplicazione_tabelle(ls_tabelle,ls_descrizioni,ls_origine,ls_destinazione,ls_messaggio,st_stato,hpb_1)
destroy luo_duplicazione

if li_ret = 0 then
	ls_messaggio = "Duplicazione effettuata con successo!"
	st_stato.text = ""
	//commit già fatto dalla funzione "uof_duplicazione_tabelle"
	
	if lb_attrezzature then
		ls_messaggio += " Esiste documentazione associata alle attrezzature. Duplico anche quelle?"
		
		if g_mb.confirm(ls_messaggio) then
			//hai scelto di duplicare anche la documentazione attrezzature
			li_ret = wf_duplica_det_attrezzature(ls_errore)
			
			if li_ret < 0 then
				st_stato.text = ""
				rollback;			//il rollback è solo sulla documentazione
				setpointer(arrow!)
				g_mb.warning("Errore in duplicazione documentazione attrezzature : " + ls_errore)
				g_mb.warning("La duplicazione delle altre tabelle è comunque avvenuta (tranne i files allegati alle attrezzature)!")
			else
				//OK anche sulla documentazione attrezzature
				st_stato.text = ""
				commit;
				g_mb.success("Duplicazione effettuata con successo anche con la documentazione relativa alle attrezzature, come richiesto!")
				cb_nessuna.postevent("clicked")
				setpointer(arrow!)
			end if

		else
			//duplicazione effettuata ma hai scelto di NON duplicare la documentazione
			
			//hai finito (COMMIT già fatto, quindi esci)
			g_mb.success("Duplicazione effettuata con successo senza la documentazione relativa alle attrezzature, come richiesto!")
			cb_nessuna.postevent("clicked")
			setpointer(arrow!)
		end if
		
	else
		//hai finito (COMMIT già fatto, quindi esci)
							//"Duplicazione effettuata con successo!"
		g_mb.success(ls_messaggio)
		cb_nessuna.postevent("clicked")
		setpointer(arrow!)
		return
	end if
else
	//errore durante la duplicazione, manda il messaggio ed esci
	st_stato.text = ""
	rollback;
	setpointer(arrow!)
	g_mb.error(ls_messaggio)
	return
end if




end event

type st_3 from statictext within w_duplicazione
integer x = 69
integer y = 1432
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Stato elaborazione:"
boolean focusrectangle = false
end type

type st_stato from statictext within w_duplicazione
integer x = 1051
integer y = 1432
integer width = 1339
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_duplicazione
integer x = 23
integer y = 1360
integer width = 3223
integer height = 180
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
end type

type gb_3 from groupbox within w_duplicazione
integer x = 3017
integer y = 220
integer width = 229
integer height = 1140
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
end type

type dw_disponibili from datawindow within w_duplicazione
integer x = 23
integer y = 240
integer width = 1349
integer height = 1120
integer taborder = 20
string dataobject = "d_tabelle_duplicazione"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;is_focus = "D"

parent.postevent("ue_allinea")
end event

type gb_4 from groupbox within w_duplicazione
integer x = 1394
integer y = 220
integer width = 229
integer height = 1140
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
end type

