﻿$PBExportHeader$w_fil_fornitori.srw
$PBExportComments$Finestra Gestione Filiali Fornitori
forward
global type w_fil_fornitori from w_cs_xx_principale
end type
type dw_fil_fornitori_lista from uo_cs_xx_dw within w_fil_fornitori
end type
type dw_fil_fornitori_det from uo_cs_xx_dw within w_fil_fornitori
end type
end forward

global type w_fil_fornitori from w_cs_xx_principale
int Width=2803
int Height=1549
boolean TitleBar=true
string Title="Gestione Filiali Fornitori"
dw_fil_fornitori_lista dw_fil_fornitori_lista
dw_fil_fornitori_det dw_fil_fornitori_det
end type
global w_fil_fornitori w_fil_fornitori

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_fil_fornitori_lista.set_dw_key("cod_azienda")
dw_fil_fornitori_lista.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                      c_default, &
                                      c_default)
dw_fil_fornitori_det.set_dw_options(sqlca, &
                                    dw_fil_fornitori_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_fil_fornitori_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_fil_fornitori_det, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_fil_fornitori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_fil_fornitori_lista=create dw_fil_fornitori_lista
this.dw_fil_fornitori_det=create dw_fil_fornitori_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_fil_fornitori_lista
this.Control[iCurrent+2]=dw_fil_fornitori_det
end on

on w_fil_fornitori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_fil_fornitori_lista)
destroy(this.dw_fil_fornitori_det)
end on

type dw_fil_fornitori_lista from uo_cs_xx_dw within w_fil_fornitori
int X=23
int Y=21
int Width=2721
int Height=501
int TabOrder=10
string DataObject="d_fil_fornitori_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_fil_fornitori_det from uo_cs_xx_dw within w_fil_fornitori
int X=23
int Y=541
int Width=2721
int Height=881
int TabOrder=20
string DataObject="d_fil_fornitori_det"
BorderStyle BorderStyle=StyleRaised!
end type

