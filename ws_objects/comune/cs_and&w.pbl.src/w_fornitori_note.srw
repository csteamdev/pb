﻿$PBExportHeader$w_fornitori_note.srw
$PBExportComments$Finestra Gestione Fornitori Note
forward
global type w_fornitori_note from w_cs_xx_principale
end type
type dw_fornitori_note_lista from uo_cs_xx_dw within w_fornitori_note
end type
type dw_fornitori_note_det from uo_cs_xx_dw within w_fornitori_note
end type
type cb_note_esterne from commandbutton within w_fornitori_note
end type
end forward

global type w_fornitori_note from w_cs_xx_principale
integer width = 2615
integer height = 1400
string title = "Gestione Note Fornitori"
dw_fornitori_note_lista dw_fornitori_note_lista
dw_fornitori_note_det dw_fornitori_note_det
cb_note_esterne cb_note_esterne
end type
global w_fornitori_note w_fornitori_note

event pc_setwindow;call super::pc_setwindow;dw_fornitori_note_lista.set_dw_key("cod_azienda")
dw_fornitori_note_lista.set_dw_key("cod_fornitore")
dw_fornitori_note_lista.set_dw_options(sqlca, &
                                       i_openparm, &
                                       c_scrollparent, &
                                       c_default)
dw_fornitori_note_det.set_dw_options(sqlca, &
                                     dw_fornitori_note_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)
iuo_dw_main = dw_fornitori_note_lista
cb_note_esterne.enabled = false

end event

on w_fornitori_note.create
int iCurrent
call super::create
this.dw_fornitori_note_lista=create dw_fornitori_note_lista
this.dw_fornitori_note_det=create dw_fornitori_note_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fornitori_note_lista
this.Control[iCurrent+2]=this.dw_fornitori_note_det
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_fornitori_note.destroy
call super::destroy
destroy(this.dw_fornitori_note_lista)
destroy(this.dw_fornitori_note_det)
destroy(this.cb_note_esterne)
end on

event pc_delete;call super::pc_delete;cb_note_esterne.enabled = false

end event

type dw_fornitori_note_lista from uo_cs_xx_dw within w_fornitori_note
integer x = 23
integer y = 20
integer width = 2537
integer height = 500
integer taborder = 10
string dataobject = "d_fornitori_note_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_fornitore


ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_fornitore

ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_fornitore")) then
      this.setitem(ll_i, "cod_fornitore", ls_cod_fornitore)
   end if
next
end on

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false

end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false

end event

event pcd_save;call super::pcd_save;cb_note_esterne.enabled = true

end event

event pcd_view;call super::pcd_view;cb_note_esterne.enabled = true

end event

type dw_fornitori_note_det from uo_cs_xx_dw within w_fornitori_note
integer x = 23
integer y = 540
integer width = 2537
integer height = 640
integer taborder = 20
string dataobject = "d_fornitori_note_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_fornitori_note
event clicked pbm_bnclicked
integer x = 2194
integer y = 1200
integer width = 366
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_fornitore, ls_cod_nota, ls_db, ls_doc
integer li_i, li_risposta
long ll_prog_mimetype

transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

li_i = dw_fornitori_note_lista.getrow()

if li_i < 1 then return

ls_cod_fornitore = dw_fornitori_note_lista.getitemstring(li_i, "cod_fornitore")
ls_cod_nota = dw_fornitori_note_lista.getitemstring(li_i, "cod_nota")

if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then return


select prog_mimetype
into :ll_prog_mimetype
from anag_fornitori_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_fornitore = :ls_cod_fornitore and
	cod_nota = :ls_cod_nota;
	
selectblob blob
into :lbl_blob
from anag_fornitori_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_fornitore = :ls_cod_fornitore and
	cod_nota = :ls_cod_nota;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update anag_fornitori_note
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_nota = :ls_cod_nota;
	else
		updateblob anag_fornitori_note
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_nota = :ls_cod_nota;
			
		update anag_fornitori_note
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore and
			cod_nota = :ls_cod_nota;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
	
end if

/*
// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob anag_fornitori_note.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_fornitori_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_fornitore = :ls_cod_fornitore and 
	           cod_nota = :ls_cod_nota
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else

	selectblob anag_fornitori_note.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_fornitori_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_fornitore = :ls_cod_fornitore and 
	           cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)

	   updateblob anag_fornitori_note
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_fornitore = :ls_cod_fornitore and 
	              cod_nota = :ls_cod_nota
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob anag_fornitori_note
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_fornitore = :ls_cod_fornitore and 
	              cod_nota = :ls_cod_nota;
	
	end if
	
	commit;
end if
*/
end event

