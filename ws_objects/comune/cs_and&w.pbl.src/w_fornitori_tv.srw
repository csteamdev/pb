﻿$PBExportHeader$w_fornitori_tv.srw
forward
global type w_fornitori_tv from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_visite_ispettive_fornitore from datawindow within det_4
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_visite_ispettive_fornitore dw_visite_ispettive_fornitore
dw_4 dw_4
end type
type det_5 from userobject within tab_dettaglio
end type
type dw_check_list_fornitore from datawindow within det_5
end type
type dw_5 from uo_cs_xx_dw within det_5
end type
type det_5 from userobject within tab_dettaglio
dw_check_list_fornitore dw_check_list_fornitore
dw_5 dw_5
end type
type det_6 from userobject within tab_dettaglio
end type
type dw_6 from uo_dw_nominativi within det_6
end type
type det_6 from userobject within tab_dettaglio
dw_6 dw_6
end type
type det_7 from userobject within tab_dettaglio
end type
type dw_7 from uo_colonne_dinamiche_dw within det_7
end type
type det_7 from userobject within tab_dettaglio
dw_7 dw_7
end type
end forward

global type w_fornitori_tv from w_cs_xx_treeview
integer width = 5102
integer height = 2728
string title = "Anagrafica Fornitori"
event pc_menu_note ( )
event pc_menu_riferimenti ( )
event pc_menu_destinazioni ( )
event pc_menu_gestisci_contatti ( )
event pc_menu_for_pot ( )
event pc_menu_report_amm ( )
event pc_menu_report_comm ( )
event pc_menu_report_gen ( )
event pc_menu_report_qualita ( )
event pc_menu_aggiorna_checklist ( )
event pc_menu_aggiorna_visite_isp ( )
event pc_menu_doc_comp ( )
event pc_menu_giudizio_storico ( )
event pc_menu_giudizio_terzi ( )
end type
global w_fornitori_tv w_fornitori_tv

type variables
private:
	string is_flag_gestione_anagrafiche
	uo_impresa iuo_impresa
	datastore ids_store
	long il_max_row=255, il_livello
	// icone
	int ICONA_TIPO_ANAGRAFICA,ICONA_DEPOSITO,ICONA_TIPO_FORNITORE,ICONA_PROVINCIA,ICONA_NAZIONE,ICONA_CATEGORIA
	int ICONA_LOCALITA,ICONA_CAP ,ICONA_AREA ,ICONA_ZONA ,ICONA_IVA, ICONA_FORNITORE
	boolean ib_qualificazione

end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function long wf_inserisci_nazione (long al_handle)
public function long wf_inserisci_deposito (long al_handle)
public function long wf_inserisci_flag_tipo_fornitore (long al_handle)
public function long wf_inserisci_provincia (long al_handle)
public function long wf_inserisci_tipo_anagrafica (long al_handle)
public function long wf_inserisci_categoria (long al_handle)
public function long wf_inserisci_cap (long al_handle)
public function long wf_inserisci_localita (long al_handle)
public function long wf_inserisci_area (long al_handle)
public function long wf_inserisci_zona (long al_handle)
public function long wf_inserisci_iva (long al_handle)
public function long wf_inserisci_fornitori (long al_handle)
public function integer wf_cod_banca_clien_for (string fs_abi, string fs_cab, ref string fs_cod_banca_clien_for, ref string fs_msg)
public subroutine wf_abilita_componi_iban (long al_row)
end prototypes

event pc_menu_note();window_open_parm(w_fornitori_note, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_riferimenti();string ls_cod_fornitore
long   ll_row


ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
else
	return
end if


ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

setnull(s_cs_xx.parametri.parametro_s_1)

select 	riferimenti
into   		:s_cs_xx.parametri.parametro_s_1
from   	anag_fornitori
where  	cod_azienda = :s_cs_xx.cod_azienda and
		   	cod_fornitore = :ls_cod_fornitore;

s_cs_xx.parametri.parametro_i_1 = 5000


window_open(w_ext_note_mss,0)




if not isnull(s_cs_xx.parametri.parametro_s_1) then
	
	
	
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

	update anag_fornitori
	set    riferimenti = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       	cod_fornitore = :ls_cod_fornitore;
				 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento dei riferimenti.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		tab_dettaglio.det_5.dw_5.setitem(ll_row, "riferimenti", s_cs_xx.parametri.parametro_s_1)
		setnull(s_cs_xx.parametri.parametro_s_1)
		tab_dettaglio.det_5.dw_5.resetupdate( )
		commit;
	end if
	
	ll_row = tab_dettaglio.det_1.dw_1.getrow()
	tab_dettaglio.det_1.dw_1.change_dw_current()
	triggerevent("pc_retrieve")
	
	//tab_dettaglio.det_1.dw_1.scrolltorow(ll_row)

end if

return
end event

event pc_menu_destinazioni();string				ls_cod_fornitore
long				ll_row


ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row>0 then
else
	g_mb.warning("Selezionare un fornitore!")
	return
end if

ls_cod_fornitore =  tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

if ls_cod_fornitore<>"" and not isnull(ls_cod_fornitore) then
else
	g_mb.warning("Selezionare un fornitore!")
	return
end if
	

window_open_parm(w_destinazioni_fornitore, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_gestisci_contatti();
s_cs_xx.parametri.parametro_s_1 = "F"  			//tipo_anagrafica
																	/*
																		C	cliente
																		F	fornitore
																		P	fornitore potenziale
																		O	contatto
																	*/
window_open_parm(w_gestione_contatti, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_for_pot();tab_dettaglio.det_1.dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_fornitore")
window_open(w_genera_for_pot, 0)

return
end event

event pc_menu_report_amm();window_open_parm(w_report_amm_fornitore, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_report_comm();window_open_parm(w_report_comm_fornitore, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_report_gen();window_open_parm(w_report_gener_fornitore, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_report_qualita();window_open_parm(w_report_qual_fornitore, -1, tab_dettaglio.det_1.dw_1)

return
end event

event pc_menu_aggiorna_checklist();string ls_cod_fornitore
long ll_row

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row > 0 then
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")
	tab_dettaglio.det_5.dw_check_list_fornitore.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)
end if

return
end event

event pc_menu_aggiorna_visite_isp();string ls_cod_fornitore
long ll_row

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if ll_row > 0 then
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")
	tab_dettaglio.det_4.dw_visite_ispettive_fornitore.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)
end if

return
end event

event pc_menu_doc_comp();blob				lbl_null
string				ls_cod_fornitore, ls_db
long				ll_row, ll_risposta

n_tran			sqlcb


setnull(lbl_null)

ll_row = tab_dettaglio.det_1.dw_1.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("OMNIA","Selezionare una nota prima di continuare",exclamation!)
	return
end if

ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_fornitore")

if isnull(ls_cod_fornitore) then
	g_mb.messagebox("OMNIA","Selezionare un fornitore prima di continuare",exclamation!)
	return
end if

ls_db = f_db()

if ls_db = "MSSQL" then
	
	ll_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob blob_doc_qualif
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_fornitori
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_fornitore = :ls_cod_fornitore
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob blob_doc_qualif
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_fornitori
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		ll_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob anag_fornitori
	   set        blob_doc_qualif = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_fornitore = :ls_cod_fornitore
		using      sqlcb;
				
		if sqlcb.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA","Errore in aggiornamento blob: " + sqlcb.sqlerrtext,stopsign!)
		end if	

		destroy sqlcb;
		
	else
		
	   updateblob anag_fornitori
	   set        blob_doc_qualif = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_fornitore = :ls_cod_fornitore;
		
		if sqlca.sqlcode <> 0 then
		   g_mb.messagebox("OMNIA","Errore in aggiornamento blob: " + sqlca.sqlerrtext,stopsign!)
		end if	
		
	end if
	
   commit;
	
end if

return
end event

event pc_menu_giudizio_storico();string					ls_cod_fornitore
long					ll_row
datetime				ldt_oggi


ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
else
	return
end if


	
//ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring( dw_fornitori_det_1.getrow(), "cod_fornitore")
setnull(s_cs_xx.parametri.parametro_s_1)

//select 	giudizio_storico
//into   		:s_cs_xx.parametri.parametro_s_1
//from   	anag_fornitori
//where  	cod_azienda = :s_cs_xx.cod_azienda and
//		   	cod_fornitore = :ls_cod_fornitore;
s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_5.dw_5.getitemstring(ll_row, "giudizio_storico")

s_cs_xx.parametri.parametro_i_1 = 5000


window_open(w_ext_note_mss,0)


if not isnull(s_cs_xx.parametri.parametro_s_1) then
	
	
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

	update anag_fornitori
	set    giudizio_storico = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       	cod_fornitore = :ls_cod_fornitore;
				 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento del giudizio storico.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		ldt_oggi = datetime(today(), time("00:00:00"))
		
		update anag_fornitori
		set    data_giudizio_storico = :ldt_oggi
		where  cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in aggiornamento data giudizio storico.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		else
			commit;
			tab_dettaglio.det_5.dw_5.setitem(ll_row, "giudizio_storico", s_cs_xx.parametri.parametro_s_1)
			tab_dettaglio.det_5.dw_5.setitem(ll_row, "data_giudizio_storico", ldt_oggi)
			setnull(s_cs_xx.parametri.parametro_s_1)
			tab_dettaglio.det_5.dw_5.resetupdate( )
		end if
	end if
	
	ll_row = tab_dettaglio.det_1.dw_1.getrow()
	tab_dettaglio.det_1.dw_1.change_dw_current()
	triggerevent("pc_retrieve")
	
	//tab_dettaglio.det_1.dw_1.scrolltorow(ll_row)

end if

return
end event

event pc_menu_giudizio_terzi();string						ls_cod_fornitore
long						ll_row
datetime					ldt_oggi


ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

setnull(s_cs_xx.parametri.parametro_s_1)

select 	giudizio_terzi
into   		:s_cs_xx.parametri.parametro_s_1
from   	anag_fornitori
where  	cod_azienda = :s_cs_xx.cod_azienda and
		   	cod_fornitore = :ls_cod_fornitore;

s_cs_xx.parametri.parametro_i_1 = 5000


window_open(w_ext_note_mss,0)



if not isnull(s_cs_xx.parametri.parametro_s_1) then
	
	
	
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_fornitore")

	update anag_fornitori
	set    giudizio_terzi = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       	cod_fornitore = :ls_cod_fornitore;
				 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento del giudizio terzi.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		ldt_oggi = datetime(today(), time("00:00:00"))
		
		update anag_fornitori
		set    data_giudizio_terzi = :ldt_oggi
		where  cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in aggiornamento data giudizio terzi.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		else
			commit;
			
			tab_dettaglio.det_5.dw_5.setitem(ll_row, "giudizio_terzi", s_cs_xx.parametri.parametro_s_1)
			tab_dettaglio.det_5.dw_5.setitem(ll_row, "data_giudizio_terzi", ldt_oggi)
			setnull(s_cs_xx.parametri.parametro_s_1)
			tab_dettaglio.det_5.dw_5.resetupdate( )
		end if
	end if
	
	ll_row = tab_dettaglio.det_1.dw_1.getrow()
	tab_dettaglio.det_1.dw_1.change_dw_current()
	triggerevent("pc_retrieve")
	
	//tab_dettaglio.det_1.dw_1.scrolltorow(ll_row)

end if

return
end event

public subroutine wf_imposta_ricerca ();string			ls_rag_soc, ls_localita


is_sql_filtro = ""

ls_rag_soc = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rag_sociale")
ls_localita = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")


if not isnull(ls_rag_soc) then
	 ls_rag_soc = g_str.replace(ls_rag_soc, "'", "''")
	 
	 is_sql_filtro += " AND ("+&
								"upper(anag_fornitori.rag_soc_1) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.rag_soc_2) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.casella_mail) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.rif_interno) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.email_amministrazione) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.telefono) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.fax) like '%" + upper(ls_rag_soc) +"%' or "+&
								"upper(anag_fornitori.cod_fornitore) like '%" + upper(ls_rag_soc) +"%' "+&
								")"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")) then
	is_sql_filtro += " AND cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")+"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") <> "X" then
	is_sql_filtro += " AND flag_blocco='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_da")) then
	is_sql_filtro += " AND data_creazione >='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_a")) then
	is_sql_filtro += " AND data_creazione <='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_creazione_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_da")) then
	is_sql_filtro += " AND data_modifica >='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_a")) then
	is_sql_filtro += " AND data_modifica <='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1,"data_modifica_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"localita")) then
	ls_localita = g_str.replace(ls_localita, "'", "''")
	is_sql_filtro += " AND upper(localita) like'%" + upper(ls_localita) +"%'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"provincia")) then
	is_sql_filtro += " AND upper(provincia)='" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"provincia")) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cap")) then
	is_sql_filtro += " AND cap='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cap") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"partita_iva")) then
	is_sql_filtro += " AND upper(partita_iva) like'%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"partita_iva")) +"%'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fiscale")) then
	is_sql_filtro += " AND upper(cod_fiscale) like'%" + upper(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fiscale")) +"%'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_fornitore") <> "X"  then
	is_sql_filtro += " AND flag_tipo_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_tipo_fornitore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) then
	is_sql_filtro += " AND cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento")) then
	is_sql_filtro += " AND cod_pagamento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_nazione")) then
	is_sql_filtro += " AND cod_nazione='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_nazione") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area")) then
	is_sql_filtro += " AND cod_area='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_area") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_zona")) then
	is_sql_filtro += " AND cod_zona='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_zona") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_anagrafica")) then
	is_sql_filtro += " AND cod_tipo_anagrafica='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_anagrafica") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_categoria")) then
	is_sql_filtro += " AND cod_categoria='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_categoria") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_iva")) then
	is_sql_filtro += " AND cod_iva='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_iva") +"'"
end if


end subroutine

public subroutine wf_treeview_icons ();ICONA_TIPO_ANAGRAFICA =  wf_treeview_add_icon("treeview\cliente.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_TIPO_FORNITORE =  wf_treeview_add_icon("treeview\operatore.png")
ICONA_PROVINCIA =  wf_treeview_add_icon("treeview\target.ico")
ICONA_NAZIONE =  wf_treeview_add_icon("treeview\flag_italy.ico")
ICONA_CATEGORIA =  wf_treeview_add_icon("treeview\Categoria_cliente.png")
ICONA_LOCALITA =  wf_treeview_add_icon("treeview\localita.png")
ICONA_CAP =  wf_treeview_add_icon("treeview\cap.png")
ICONA_AREA =  wf_treeview_add_icon("treeview\area_aziendale.png")
ICONA_ZONA = wf_treeview_add_icon("treeview\zona.png")
ICONA_IVA = wf_treeview_add_icon("treeview\documento_giallo.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\cliente.png")
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Tipo anagrafica", "1")
wf_add_valore_livello("Deposito", "2")
wf_add_valore_livello("Tipo Fornitore", "3")
wf_add_valore_livello("Provincia", "4")
wf_add_valore_livello("Nazione", "5")
wf_add_valore_livello("Categoria", "6")
wf_add_valore_livello("Località", "7")
wf_add_valore_livello("CAP", "8")
wf_add_valore_livello("Area", "9")
wf_add_valore_livello("Zona", "Z")
wf_add_valore_livello("Iva Esenzione", "I")
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "1"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_tipo_anagrafica is null "
			else
				as_sql += " AND cod_tipo_anagrafica = '" + lstr_data.codice + "' "
			end if
			
		case "2"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_deposito is null "
			else
				as_sql += " AND cod_deposito = '" + lstr_data.codice + "' "
			end if
			
		case "3"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND flag_tipo_fornitore is null "
			else
				as_sql += " AND flag_tipo_fornitore = '" + lstr_data.codice + "' "
			end if
			
		case "4"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND provincia is null "
			else
				as_sql += " AND provincia = '" + lstr_data.codice + "' "
			end if
			
		case "5"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_nazione is null "
			else
				as_sql += " AND cod_nazione = '" + lstr_data.codice + "' "
			end if
			
		case "6"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_categoria is null "
			else
				as_sql += " AND cod_categoria = '" + lstr_data.codice + "' "
			end if
			
		case "7"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND localita is null "
			else
				as_sql += " AND localita = '" + lstr_data.codice + "' "
			end if

		case "8"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cap is null "
			else
				as_sql += " AND cap = '" + lstr_data.codice + "' "
			end if
			
		case "9"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_area is null "
			else
				as_sql += " AND cod_area = '" + lstr_data.codice + "' "
			end if
			
		case "Z"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_zona is null "
			else
				as_sql += " AND cod_zona = '" + lstr_data.codice + "' "
			end if
			
		case "I"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND cod_iva is null "
			else
				as_sql += " AND cod_iva = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "1"
		return wf_inserisci_tipo_anagrafica(al_handle)
		
	case "2"
		return wf_inserisci_deposito(al_handle)
		
	case "3"
		return wf_inserisci_flag_tipo_fornitore(al_handle)
		
	case "4"
		return wf_inserisci_provincia(al_handle)
		
	case "5"
		return wf_inserisci_nazione(al_handle)
		
	case "6"
		return wf_inserisci_categoria(al_handle)
		
	case "7"
		return wf_inserisci_localita(al_handle)
		
	case "8"
		return wf_inserisci_cap(al_handle)
		
	case "9"
		return wf_inserisci_area(al_handle)
				
	case "Z"
		return wf_inserisci_zona(al_handle)

	case "I"
		return wf_inserisci_iva(al_handle)
		
	case else
		return wf_inserisci_fornitori(al_handle)
		
end choose

return 1
end function

public function long wf_inserisci_nazione (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_nazione, tab_nazioni.des_nazione FROM anag_fornitori " + &
			" left outer join tab_nazioni on anag_fornitori.cod_azienda = tab_nazioni.cod_azienda and anag_fornitori.cod_nazione = tab_nazioni.cod_nazione " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "5"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Nazione mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_NAZIONE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_deposito (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_fornitori.cod_deposito, anag_depositi.des_deposito FROM anag_fornitori " + &
" LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = anag_fornitori.cod_azienda AND " + &
" anag_depositi.cod_deposito = anag_fornitori.cod_deposito " + &
" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "2"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_flag_tipo_fornitore (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct flag_tipo_fornitore FROM anag_fornitori " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	 choose case ls_codice
		case "S"
			 ls_descrizione = "Società"//
		case "F"
			 ls_descrizione = "Persona fisica"//
		case "P"
			 ls_descrizione = "Privato"//
		case "C"
			 ls_descrizione = "CEE" //
		case "D"
			 ls_descrizione = "Dogana" //
		case "E"
			 ls_descrizione = "Estero Extra CEE"//
		case "M"
			 ls_descrizione = "San Marino"//
	end choose
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "3"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Tipo fornitore mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_provincia (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(provincia) FROM anag_fornitori " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "4"
	lstr_data.codice = ls_codice
	
	
	if isnull(ls_codice) and isnull(ls_codice) then
		ls_label = "<Provincia mancante>"
	else
		ls_label = ls_codice
	end if
	


	ltvi_item = wf_new_item(true, ICONA_PROVINCIA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_tipo_anagrafica (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_tipo_anagrafica, tab_tipi_anagrafiche.des_tipo_anagrafica FROM anag_fornitori " + &
			" left outer join tab_tipi_anagrafiche on anag_fornitori.cod_azienda = tab_tipi_anagrafiche.cod_azienda and anag_fornitori.cod_tipo_anagrafica = tab_tipi_anagrafiche.cod_tipo_anagrafica " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "1"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Tipo Anagrafica mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_ANAGRAFICA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_categoria (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_categoria, tab_categorie.des_categoria FROM anag_fornitori " + &
			" left outer join tab_categorie on anag_fornitori.cod_azienda = tab_categorie.cod_azienda and anag_fornitori.cod_categoria = tab_categorie.cod_categoria " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "6"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Categoria mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_CATEGORIA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_cap (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(isnull(cap,'')) FROM anag_fornitori " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "8"
	lstr_data.codice = ls_codice
	
	
	if isnull(ls_codice) or trim(ls_codice)="" then
		ls_label = "<CAP  mancante>"
	else
		ls_label = ls_codice
	end if
	
	//ls_label = ls_codice

	ltvi_item = wf_new_item(true, ICONA_CAP)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_localita (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct UPPER(localita) FROM anag_fornitori " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "7"
	lstr_data.codice = ls_codice
	
	if isnull(ls_codice) and isnull(ls_codice) then
		ls_label = "<Località mancante>"
	else
		ls_label = ls_codice
	end if

	ltvi_item = wf_new_item(true, ICONA_LOCALITA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_area (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_area, tab_aree.des_area FROM anag_fornitori " + &
			" left outer join tab_aree on anag_fornitori.cod_azienda = tab_aree.cod_azienda and anag_fornitori.cod_area = tab_aree.cod_area " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "9"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Area  mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_AREA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_zona (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_zona, tab_zone.des_zona FROM anag_fornitori " + &
			" left outer join tab_zone on anag_fornitori.cod_azienda = tab_zone.cod_azienda and anag_fornitori.cod_zona = tab_zone.cod_zona " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "Z"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Zona mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_ZONA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_iva (long al_handle);string ls_sql, ls_error, ls_codice ,ls_descrizione, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = " SELECT distinct anag_fornitori.cod_iva, tab_ive.des_iva FROM anag_fornitori " + &
			" left outer join tab_ive on anag_fornitori.cod_azienda = tab_ive.cod_azienda and anag_fornitori.cod_iva = tab_ive.cod_iva " + &
			" WHERE anag_fornitori.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_codice = ids_store.getitemstring(ll_i, 1)
	 ls_descrizione = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "I"
	lstr_data.codice = ls_codice
	
	if isnull(ls_descrizione) and isnull(ls_descrizione) then
		ls_label = "<Iva mancante>"
	elseif isnull(ls_descrizione) then
		ls_label = ls_descrizione
	else
		ls_label = ls_codice + " - " + ls_descrizione
	end if

	ltvi_item = wf_new_item(true, ICONA_IVA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_fornitori (long al_handle);string ls_sql, ls_error, ls_cod_cliente ,ls_rag_soc_1, ls_label
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT distinct anag_fornitori.cod_fornitore, anag_fornitori.rag_soc_1 FROM anag_fornitori " + " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 A, #2 A")
ids_store.sort()

for ll_i = 1 to ll_rows
	
	 ls_cod_cliente = ids_store.getitemstring(ll_i, 1)
	 ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = ls_cod_cliente
	
	if isnull(ls_rag_soc_1) and isnull(ls_rag_soc_1) then
		ls_label = "<Fornitore mancante>"
	elseif isnull(ls_rag_soc_1) then
		ls_label = ls_rag_soc_1
	else
		ls_label = ls_cod_cliente + " - " + ls_rag_soc_1
	end if

	ltvi_item = wf_new_item(false, ICONA_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_cod_banca_clien_for (string fs_abi, string fs_cab, ref string fs_cod_banca_clien_for, ref string fs_msg);//1verifica se abi e cab sono presenti in anag_banche_clien_for
//se esiste torna il cod_banca_clien_for
//se non esiste inserisci creando un nuovo codice e torna tale codice

integer ll_i, ll_max, ll_len
string ls_char, ls_appo[], ls_codice, ls_max

setnull(ls_codice)

select cod_banca_clien_for
into :ls_codice
from anag_banche_clien_for
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_abi=:fs_abi and cod_cab=:fs_cab;
			
if sqlca.sqlcode<0 then
	fs_msg="Errore in lettura codice da anag_banche_clien_for: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ls_codice) or ls_codice="" or sqlca.sqlcode=100 then
	//non trovato, proseguirai con il calcolo del nuovo codice ed inserimento in anag_banche_clien_for
else
	//trovato
	fs_cod_banca_clien_for = ls_codice
	
	return 1
end if


//se sei arrivato fin qui crea il codice e inseriscilo
ll_max = 3

if ll_max <=0 then
	return -1
end if

//leggi il max
setnull(ls_codice)
ll_len = len(ls_codice)

select max(substring(cod_banca_clien_for, 1, :ll_max))
into :ls_max
from anag_banche_clien_for
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	fs_msg = "Errore durante il lettura MAX codice da anag_banche_clien_for: "+sqlca.sqlerrtext	
	return -1
end if

if isnull(ls_max) or ls_max="" or sqlca.sqlcode=100 then
	fs_cod_banca_clien_for = "001"
	
	return 1
end if


//carica un array con i caratteri della stringa
for ll_i = 1 to ll_max
	ls_appo[ll_i] = mid(ls_max, ll_i, 1)
next

for ll_i = ll_max to 1 step -1
	//preleva i caratteri a partire da destra
	ls_char = ls_appo[ll_i]
	
	choose case ls_char
		case "9"
			//fallo diventare "A"
			ls_appo[ll_i] = "A"
			exit
			
		case "Z"
			//metti il carattere corrente a "0"
			ls_appo[ll_i] = "0"
			
			//vai al successivo carattere spostandoti verso sinistra			
			
		case else
			//incrementa il suo codice ascii
			ls_appo[ll_i] = char(asc(ls_char) + 1)
			exit
			
	end choose
next

fs_cod_banca_clien_for = ""
for ll_i = 1 to ll_max
	fs_cod_banca_clien_for +=  ls_appo[ll_i]
next

return 1
end function

public subroutine wf_abilita_componi_iban (long al_row);string			ls_flag_tipo_fornitore

ls_flag_tipo_fornitore = tab_dettaglio.det_2.dw_2.getitemstring(al_row, "flag_tipo_fornitore")

if ls_flag_tipo_fornitore="C" or ls_flag_tipo_fornitore="E" then
	//per CEE ed Estero niente cin-abi-cab
	tab_dettaglio.det_2.dw_2.object.b_autocomposizione.enabled=false
else
	tab_dettaglio.det_2.dw_2.object.b_autocomposizione.enabled=true
end if

return


end subroutine

on w_fornitori_tv.create
int iCurrent
call super::create
end on

on w_fornitori_tv.destroy
call super::destroy
end on

event open;call super::open;iuo_impresa = CREATE uo_impresa
end event

event close;call super::close;destroy iuo_impresa
end event

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	string ls_str
	
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MFO';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Verificare l'esistenza del parametro MCL in parametri azienda")
	else
		f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
							  "cod_conto", &
							  sqlci, &
							  "sottomastro", &
							  "codice", &
							  "descrizione", &
							  "id_mastro in (select id_mastro from mastro where codice = '" + ls_str + "')")
	end if
	
	f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
						  "id_doc_fin_rating_impresa", &
						  sqlci, &
						  "doc_fin_rating", &
						  "id_doc_fin_rating", &
						  "'(' + codice + ') ' + descrizione", &
						  "")
	
	f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
						  "id_doc_fin_voce_fin_impresa", &
						  sqlci, &
						  "doc_fin_voce_fin", &
						  "id_doc_fin_voce_fin", &
						  "'(' + codice + ') ' + descrizione", &
						  "")
end if

// *** Michela 13/11/2007: se mi trovo in sintexcal allora carico la drop del tipo programma.
//		 ATTENZIONE: se si modificano le dw controllare le personalizzazioni di sintexcal

string	ls_prova

select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then

	f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
						  "cod_tipo_programma", &
						  sqlca, &
						  "tab_tipi_programmi", &
						  "cod_tipo_programma", &
						  "des_tipo_programma", &
						  "")

end if
					  
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_nazione", &
                 sqlca, &
                 "tab_nazioni", &
                 "cod_nazione", &
                 "des_nazione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_4.dw_4, &
                 "cod_piano_campionamento", &
                 sqlca, &
                 "tab_tipi_piani_campionamento", &
                 "cod_piano_campionamento", &
                 "des_piano_campionamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "')")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'F' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//---------------------------------------------------------------------------------
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'F' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_nazione", &
                 sqlca, &
                 "tab_nazioni", &
                 "cod_nazione", &
                 "des_nazione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
								




end event

event pc_setwindow;call super::pc_setwindow;
string			ls_modify
uo_mansionario luo_mansionario


is_codice_filtro = "FOR"
tab_dettaglio.det_5.dw_check_list_fornitore.settransobject(sqlca)
tab_dettaglio.det_5.dw_check_list_fornitore.visible = false

tab_dettaglio.det_4.dw_visite_ispettive_fornitore.settransobject(sqlca)
tab_dettaglio.det_4.dw_visite_ispettive_fornitore.visible = false


luo_mansionario = create uo_mansionario
ib_qualificazione = luo_mansionario.uof_get_privilege(luo_mansionario.qualificazione)
destroy luo_mansionario

if isnull(ib_qualificazione) then ib_qualificazione = false

if not ib_qualificazione then
	tab_dettaglio.det_4.dw_4.object.flag_omologato.visible = 0
	tab_dettaglio.det_4.dw_4.object.st_abil_mans_1.visible = 1
	tab_dettaglio.det_4.dw_4.object.st_abil_mans_2.visible = 1
	
	//la voce Documenti Compilati non deve essere visibile
	//...		cb_doc_comp.visible = false
	tab_dettaglio.det_4.dw_visite_ispettive_fornitore.visible = true
	
else
	tab_dettaglio.det_4.dw_4.object.flag_omologato.visible = 1
	tab_dettaglio.det_4.dw_4.object.st_abil_mans_1.visible = 0
	tab_dettaglio.det_4.dw_4.object.st_abil_mans_2.visible = 0
	
	tab_dettaglio.det_5.dw_check_list_fornitore.visible = true
end if

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")

tab_dettaglio.det_1.dw_1.set_dw_options(	sqlca, &
														pcca.null_object, &
														c_noretrieveonopen, &
														c_default)
														
tab_dettaglio.det_2.dw_2.set_dw_options(	sqlca, &
														tab_dettaglio.det_1.dw_1, &
														c_sharedata + c_scrollparent, &
														c_default)
														
tab_dettaglio.det_3.dw_3.set_dw_options(	sqlca, &
														tab_dettaglio.det_1.dw_1, &
														c_sharedata + c_scrollparent, &
														c_default)
											 
tab_dettaglio.det_4.dw_4.set_dw_options(	sqlca, &
														tab_dettaglio.det_1.dw_1, &
														c_sharedata + c_scrollparent, &
														c_default)
											
tab_dettaglio.det_5.dw_5.set_dw_options(	sqlca, &
														tab_dettaglio.det_1.dw_1, &
														c_sharedata + c_scrollparent, &
														c_default)

iuo_dw_main=tab_dettaglio.det_1.dw_1

//colonne dinamiche
tab_dettaglio.det_7.dw_7.uof_set_dw(tab_dettaglio.det_1.dw_1, {"cod_fornitore"})

//nominativi
tab_dettaglio.det_6.dw_6.uof_set_dw(tab_dettaglio.det_1.dw_1, "cod_fornitore", tab_dettaglio.det_6.dw_6.FORNITORI)

ls_modify = "flag_tipo_certificazione.protect='1~tif(flag_certificato=~~'N~~',1,0)'~t"
ls_modify = ls_modify + "flag_tipo_certificazione.background.color='12632256~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
tab_dettaglio.det_4.dw_4.modify(ls_modify)

tab_dettaglio.det_2.dw_2.object.p_del_banca.filename =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\ole_delete.png"

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select flag_gestione_anagrafiche
	into   :is_flag_gestione_anagrafiche
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode <> 0 or isnull(is_flag_gestione_anagrafiche) or is_flag_gestione_anagrafiche = "" then
		is_flag_gestione_anagrafiche = "S"
	end if
else
	is_flag_gestione_anagrafiche = "S"
end if

tab_dettaglio.det_5.dw_5.object.riferimenti.background.color = 12632256
tab_dettaglio.det_5.dw_5.object.giudizio_storico.background.color = 12632256
tab_dettaglio.det_5.dw_5.object.giudizio_terzi.background.color = 12632256



end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_fornitori_tv
integer x = 1531
integer width = 3511
integer height = 2588
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
det_6 det_6
det_7 det_7
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
this.det_6=create det_6
this.det_7=create det_7
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5,&
this.det_6,&
this.det_7}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
destroy(this.det_6)
destroy(this.det_7)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3474
integer height = 2464
string text = "Principale"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_fornitori_tv
integer width = 1495
integer height = 2588
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1458
integer height = 2464
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1435
integer height = 2448
string dataobject = "d_fornitori_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_sel_cliente"
		guo_ricerca.uof_ricerca_fornitore(tab_ricerca.ricerca.dw_ricerca, "cod_fornitore")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1458
integer height = 2464
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1435
integer height = 2440
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::rightclicked;call super::rightclicked;
long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_anag_fornitori lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row =tab_dettaglio.det_1.dw_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "C" then
	
	lm_menu = create m_anag_fornitori
	
	if not ib_qualificazione then
		lm_menu.m_documentoqualifica.enabled = false
	end if
	
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 5
integer y = 32
integer width = 3419
integer height = 1880
integer taborder = 30
string dataobject = "d_fornitori_det_1_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data)  then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.codice)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()
end event

event doubleclicked;call super::doubleclicked;string ls_messaggio, ls_destinatari[], ls_allegati[],ls_errore
uo_outlook luo_outlook

choose case dwo.name
	case "casella_mail"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "casella_mail")
		if luo_outlook.uof_invio_outlook(0, "M", "", "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
			
end choose

end event

event itemchanged;call super::itemchanged;string ls_messaggio


if i_extendmode then
	choose case getcolumnname()
		case "partita_iva"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				if getitemstatus(row, 0, Primary!) <> NewModified! and getitemstatus(row, 0, Primary!) <> New! then
					if g_mb.messagebox("APICE","Partita I.V.A. cambiata: sei sicuro ?",Question!,YesNo!,2) = 2 then
						return 1
					end if
				end if				
				
				if f_controlla_partita_iva_doppia (i_coltext, "anag_fornitori", "cod_fornitore", "rag_soc_1", getitemstring(getrow(),"cod_fornitore"), ref ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)	
					return 1
				end if
				
			end if
			
		case "cod_fiscale"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				
				if getitemstatus(row, 0, Primary!) <> NewModified! and getitemstatus(row, 0, Primary!) <> New! then
					if g_mb.messagebox("APICE","Codice Fiscale cambiato: sei sicuro ?",Question!,YesNo!,2) = 2 then
						return 1
					end if
				end if
				
				if f_controlla_cod_fisc_doppio (i_coltext, "anag_fornitori", "cod_fornitore", "rag_soc_1", getitemstring(getrow(),"cod_fornitore"), ref ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)	
					return 1
				end if
			end if
			
		case "cod_fornitore"
			
			if not isnull(i_coltext) and len(i_coltext) > 0 then
				
				if f_controlla_codice ("anag_fornitori","cod_fornitore","rag_soc_1",i_coltext,ls_messaggio) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
					return 1
				end if
				
			end if
			
	end choose
end if
end event

event pcd_commit;call super::pcd_commit;if s_cs_xx.parametri.impresa then
	commit using sqlci;
end if
end event

event pcd_modify;call super::pcd_modify;string ls_modify

if i_extendmode then

	
	ls_modify = "flag_tipo_certificazione.protect='0~tif(flag_certificato=~~'N~~',1,0)'~t"
	ls_modify = ls_modify + "flag_tipo_certificazione.background.color='16777215~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
	tab_dettaglio.det_4.dw_4.modify(ls_modify)
	
	tab_dettaglio.det_2.dw_2.object.b_ricerca_abicab.enabled = true
	
	tab_dettaglio.det_2.dw_2.object.p_del_banca.enabled=true
	
	wf_abilita_componi_iban(getrow())
	
end if
end event

event pcd_new;call super::pcd_new;string ls_modify

if i_extendmode then	


	ls_modify = "flag_tipo_certificazione.protect='0~tif(flag_certificato=~~'N~~',1,0)'~t"
	ls_modify = ls_modify + "flag_tipo_certificazione.background.color='16777215~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
	tab_dettaglio.det_4.dw_4.modify(ls_modify)
	tab_dettaglio.det_2.dw_2.object.b_ricerca_abicab.enabled = true

	tab_dettaglio.det_1.dw_1.setitem(getrow(),"data_creazione",datetime(today(),00:00:00))
	tab_dettaglio.det_1.dw_1.setitem(getrow(),"data_modifica",datetime(today(),00:00:00))

	tab_dettaglio.det_2.dw_2.object.p_del_banca.enabled=true

	wf_abilita_componi_iban(getrow())

end if
end event

event pcd_rollback;call super::pcd_rollback;if s_cs_xx.parametri.impresa then
	rollback using sqlci;
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_fiscale, ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


if getrow() > 0 then
   f_upd_partita_iva("anag_fornitori", "cod_fornitore", "rag_soc_1")
   if getitemstring(i_rownbr, "flag_tipo_fornitore") <> "E" and getitemstring(i_rownbr, "flag_tipo_fornitore") <> "C" and getitemstring(i_rownbr, "flag_tipo_fornitore") <> "D"  and getitemstring(i_rownbr, "flag_tipo_fornitore") <> "M"  and not isnull(this.getitemstring(i_rownbr, "cod_fiscale")) then
      ls_cod_fiscale = this.getitemstring(i_rownbr, "cod_fiscale")
      if not f_cod_fiscale(ls_cod_fiscale) then
        	g_mb.messagebox("Attenzione", "Inserire un codice fiscale valido!", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
	
	if isnull(getitemstring(getrow(),"cod_iva")) then
		if (tab_dettaglio.det_2.dw_2.getcolumnname() = "num_prot_esenzione_iva" and &
		      not isnull(tab_dettaglio.det_2.dw_2.gettext()) and len(tab_dettaglio.det_2.dw_2.gettext()) > 0) or &
		   (tab_dettaglio.det_2.dw_2.getcolumnname() <> "num_prot_esenzione_iva" and not isnull(this.getitemstring(getrow(), "num_prot_esenzione_iva"))) then
			g_mb.messagebox("APICE","Attenzione! Per indicare un protocollo esenzione è necessario aver indicato ~r~nanche un codice di esenzione.~r~nIl protocollo che è stato inserito e la relativa data verranno cancellati")
		end if
		this.setitem(getrow(), "num_prot_esenzione_iva", ls_null)
		this.setitem(getrow(), "data_esenzione_iva", ldt_null)
	end if
end if




end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_fornitore

//retrieve dei nominativi
tab_dettaglio.det_6.dw_6.retrieve()

if i_extendmode then
	
	//colonne dinamiche
	tab_dettaglio.det_7.dw_7.retrieve()
	
	if is_flag_gestione_anagrafiche = "N" then
		tab_dettaglio.det_1.dw_1.object.rag_soc_1.protect = 1
		tab_dettaglio.det_1.dw_1.object.rag_soc_2.protect = 1
		tab_dettaglio.det_1.dw_1.object.indirizzo.protect = 1
		tab_dettaglio.det_1.dw_1.object.cap.protect = 1
		tab_dettaglio.det_1.dw_1.object.localita.protect = 1
		tab_dettaglio.det_1.dw_1.object.provincia.protect = 1
		tab_dettaglio.det_1.dw_1.object.frazione.protect = 1
		tab_dettaglio.det_1.dw_1.object.cod_fiscale.protect = 1
		tab_dettaglio.det_1.dw_1.object.partita_iva.protect = 1
		tab_dettaglio.det_1.dw_1.object.flag_tipo_fornitore.protect = 1
		tab_dettaglio.det_2.dw_2.object.cod_conto.protect = 1
		tab_dettaglio.det_2.dw_2.object.cod_banca.protect = 1
		tab_dettaglio.det_2.dw_2.object.cod_banca_clien_for.protect = 1
		tab_dettaglio.det_3.dw_3.object.cod_valuta.protect = 1
		tab_dettaglio.det_3.dw_3.object.cod_categoria.protect = 1
		tab_dettaglio.det_3.dw_3.object.cod_tipo_listino_prodotto.protect = 1
	end if
	
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(getrow(), "cod_fornitore")
		
	tab_dettaglio.det_5.dw_check_list_fornitore.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)
	tab_dettaglio.det_4.dw_visite_ispettive_fornitore.retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)
end if
end event

event rowfocuschanging;call super::rowfocuschanging;tab_dettaglio.det_7.dw_7.uof_verify_changes()
end event

event updateend;call super::updateend;long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double		ld_sconto, ld_peso_val_servizio, ld_peso_val_qualita, ld_limite_tolleranza
string			ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_fornitore, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_cod_fornitore, ls_flag_certificato, ls_flag_approvato, ls_flag_strategico, ls_flag_terzista, &
				ls_num_contratto, ls_flag_ver_ispettiva, ls_note_ver_ispettiva, ls_flag_omologato, ls_flag_procedure_speciali, ls_cod_for_pot, ls_flag_tipo_certificazione, &
				ls_cod_piano_campionamento, ls_messaggio
datetime ldt_data_esenzione_iva, ldt_data_contratto, ldt_data_ver_ispettiva


ll_i = 0

tab_dettaglio.det_7.dw_7.update()

do while ll_i <= tab_dettaglio.det_1.dw_1.rowcount()
	ll_i = tab_dettaglio.det_1.dw_1.getnextmodified(ll_i, Primary!)
	
	if ll_i = 0 then
	return
	end if
	
	ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_fornitore")
	
	select anag_for_pot.cod_for_pot
	into   :ls_cod_for_pot
	from   anag_for_pot
	where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_for_pot.cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode <> 100 then
		ls_rag_soc_1 = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "rag_soc_1")
		ls_rag_soc_2 = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "rag_soc_2")
		ls_indirizzo = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "indirizzo")
		ls_localita = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "localita")
		ls_frazione = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "frazione")
		ls_cap = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cap")
		ls_provincia = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "provincia")
		ls_telefono = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "telefono")
		ls_fax = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "fax")
		ls_telex = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "telex")
		ls_cod_fiscale = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_fiscale")
		ls_partita_iva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "partita_iva")
		ls_rif_interno = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "rif_interno")
		ls_flag_tipo_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_tipo_fornitore")
		ls_cod_conto = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_conto")
		ls_cod_iva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_iva")
		ls_num_prot_esenzione_iva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "num_prot_esenzione_iva")
		ldt_data_esenzione_iva = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_i, "data_esenzione_iva")
		ls_flag_sospensione_iva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_sospensione_iva")
		ls_cod_pagamento = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_pagamento")
		ll_giorno_fisso_scadenza = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "giorno_fisso_scadenza")
		ll_mese_esclusione_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "mese_esclusione_1")
		ll_mese_esclusione_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "mese_esclusione_2")
		
		ll_ggmm_esclusione_1_da	= tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_da")
		ll_ggmm_esclusione_1_a		= tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_a")
		ll_ggmm_esclusione_2_da	= tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_da")
		ll_ggmm_esclusione_2_a		= tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_a")
		
		ll_data_sostituzione_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "data_sostituzione_1")
		ll_data_sostituzione_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "data_sostituzione_2")
		ld_sconto = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "sconto")
		ls_cod_tipo_listino_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_tipo_listino_prodotto")
		ll_fido = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "fido")
		ls_cod_banca_clien_for = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_banca_clien_for")
		ls_conto_corrente = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "conto_corrente")
		ls_cod_lingua = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_lingua")
		ls_cod_nazione = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_nazione")
		ls_cod_area = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_area")
		ls_cod_zona = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_zona")
		ls_cod_valuta = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_valuta")
		ls_cod_categoria = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_categoria")
		ls_cod_imballo = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_imballo")
		ls_cod_porto = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_porto")
		ls_cod_resa = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_resa")
		ls_cod_mezzo = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_mezzo")
		ls_cod_vettore = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_vettore")
		ls_cod_inoltro = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_inoltro")
		ls_cod_deposito = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_deposito")
		ls_flag_certificato = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_certificato")
		ls_flag_approvato = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_approvato")
		ls_flag_strategico = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_strategico")
		ls_flag_terzista = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_terzista")
		ls_num_contratto = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "num_contratto")
		ldt_data_contratto = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_i, "data_contratto")
		ls_flag_ver_ispettiva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_ver_ispettiva")
		ldt_data_ver_ispettiva = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_i, "data_ver_ispettiva")  
		ls_note_ver_ispettiva = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "note_ver_ispettiva")
		ls_flag_omologato = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_omologato")
		ls_flag_procedure_speciali = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_procedure_speciali")
		ld_peso_val_servizio = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "peso_val_servizio")
		ld_peso_val_qualita = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "peso_val_qualita")
		ld_limite_tolleranza = tab_dettaglio.det_1.dw_1.getitemnumber(ll_i, "limite_tolleranza")
		ls_flag_tipo_certificazione = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "flag_tipo_certificazione")
		ls_cod_piano_campionamento = tab_dettaglio.det_1.dw_1.getitemstring(ll_i, "cod_piano_campionamento")

		update anag_for_pot
		set    rag_soc_1 = :ls_rag_soc_1,
				 rag_soc_2 = :ls_rag_soc_2,   
				 indirizzo = :ls_indirizzo,   
				 localita = :ls_localita,   
				 frazione = :ls_frazione,   
				 cap = :ls_cap,   
				 provincia = :ls_provincia,   
				 telefono = :ls_telefono,   
				 fax = :ls_fax,   
				 telex = :ls_telex,   
				 cod_fiscale = :ls_cod_fiscale,   
				 partita_iva = :ls_partita_iva,   
				 rif_interno = :ls_rif_interno,   
				 flag_tipo_fornitore = :ls_flag_tipo_fornitore,   
				 cod_conto = :ls_cod_conto,   
				 cod_iva = :ls_cod_iva,   
				 num_prot_esenzione_iva = :ls_num_prot_esenzione_iva,   
				 data_esenzione_iva = :ldt_data_esenzione_iva,   
				 flag_sospensione_iva = :ls_flag_sospensione_iva,   
				 cod_pagamento = :ls_cod_pagamento,   
				 giorno_fisso_scadenza = :ll_giorno_fisso_scadenza,   
				 mese_esclusione_1 = :ll_mese_esclusione_1,   
				 mese_esclusione_2 = :ll_mese_esclusione_2,   
				 ggmm_esclusione_1_da =:ll_ggmm_esclusione_1_da,
				 ggmm_esclusione_1_a =:ll_ggmm_esclusione_1_a,
				 ggmm_esclusione_2_da =:ll_ggmm_esclusione_2_da,
				 ggmm_esclusione_2_a =:ll_ggmm_esclusione_2_a,
				 data_sostituzione_1 = :ll_data_sostituzione_1,   
				 data_sostituzione_2 = :ll_data_sostituzione_2,   
				 sconto = :ld_sconto,   
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto,   
				 fido = :ll_fido,   
				 cod_banca_clien_for = :ls_cod_banca_clien_for,   
				 conto_corrente = :ls_conto_corrente,
				 cod_lingua = :ls_cod_lingua,   
				 cod_nazione = :ls_cod_nazione,   
				 cod_area = :ls_cod_area,   
				 cod_zona = :ls_cod_zona,   
				 cod_valuta = :ls_cod_valuta,   
				 cod_categoria = :ls_cod_categoria,   
				 cod_imballo = :ls_cod_imballo,   
				 cod_porto = :ls_cod_porto,   
				 cod_resa = :ls_cod_resa,   
				 cod_mezzo = :ls_cod_mezzo,   
				 cod_vettore = :ls_cod_vettore,   
				 cod_inoltro = :ls_cod_inoltro,   
				 cod_deposito = :ls_cod_deposito,
				 flag_certificato = :ls_flag_certificato,
				 flag_approvato = :ls_flag_approvato,
				 flag_strategico = :ls_flag_strategico,
				 flag_terzista = :ls_flag_terzista,
				 num_contratto = :ls_num_contratto,
				 data_contratto = :ldt_data_contratto,
				 flag_ver_ispettiva = :ls_flag_ver_ispettiva,
				 data_ver_ispettiva = :ldt_data_ver_ispettiva,
				 note_ver_ispettiva = :ls_note_ver_ispettiva,
				 flag_omologato = :ls_flag_omologato,
				 flag_procedure_speciali = :ls_flag_procedure_speciali,
				 peso_val_servizio = :ld_peso_val_servizio,
				 peso_val_qualita = :ld_peso_val_qualita,
				 limite_tolleranza = :ld_limite_tolleranza,
				 flag_tipo_certificazione = :ls_flag_tipo_certificazione,
				 cod_piano_campionamento = :ls_cod_piano_campionamento
		where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_for_pot.cod_fornitore = :ls_cod_fornitore;
		
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento fornitori potenziali.", &
						  exclamation!, ok!)
			rollback;
			return
		end if
		
		commit;
   end if
loop
end event

event updatestart;call super::updatestart;string			ls_cod_fornitore, ls_messaggio, ls_cod_abi, ls_cod_cab, ls_cod_banca_clien_for, ls_cod_conto, &
				ls_cod_abi_1, ls_cod_cab_1, ls_cod_banca, ls_partita_iva, ls_cod_fiscale, ls_cod_cliente, ls_rag_soc_1, &
				ls_rag_soc_2, ls_indirizzo, ls_localita, ls_cap, 	ls_frazione, ls_provincia, ls_cin, ls_cin_1, ls_cod_fiscale_copia
long			li_i, ll_cont,li_id_anagrafica, li_id_sog_commerciale

//Donato 03-11-2008 gestione telefono, fax, cellulare e cod_agente_1
string			ls_telefono, ls_fax, ls_telex

string			ls_msg, ls_col_abi, ls_col_cab, ls_col_codbancaclienfor, ls_col_cin, ls_col_iban
integer		li_ret
   
if i_extendmode then
	
	tab_dettaglio.det_7.dw_7.uof_Delete()
	
	for li_i = 1 to rowcount()
		
		if getitemstatus(li_i, 0, primary!) = datamodified! then		// solo modificato
			setitem(li_i,"data_modifica", datetime(today(),00:00:00) )
		end if
		
		if getitemstatus(li_i, 0, primary!) = newmodified! OR getitemstatus(li_i, 0, primary!) = datamodified!then      /// nuovo o modificato fornitore
			
			//21/10/2010 ----------------------------------------------------------------------------------------------------------------
			//Gestione Banca per selezione di ABI e CAB
			//occorre recuperare cod_banca_clien_for, o se non esiste per tali abi e cab inserirlo
			//tornando il codice
			
			ls_col_abi = "abi"
			ls_col_cab = "cab"
			ls_col_codbancaclienfor = "cod_banca_clien_for"
			ls_col_cin = "cin"
			ls_col_iban = "iban"
			
			li_ret = f_get_codbancaclienfor(li_i, ls_col_abi, ls_col_cab, ls_col_codbancaclienfor, ls_col_cin, ls_col_iban, tab_dettaglio.det_1.dw_1, ls_msg)
			if li_ret <0 then
				rollback;
				g_mb.messagebox("APICE",ls_msg)
				return 1
			end if			
			//fine modifica ------------------------------------------------------------------------------------------------------------
			
			
			// EnMe 20-10-2011 verifico presenza di valuta e deposito
			string ls_cod_valuta, ls_cod_deposito
	
			ls_cod_valuta = GetItemString(li_i, "cod_valuta") 
			ls_cod_deposito = GetItemString(li_i, "cod_deposito") 	
	
			if isnull(ls_cod_valuta) then
				g_mb.messagebox("Anagrafica Fornitori", "Manca il codice valuta")
				return 1
			end if
		
			if isnull(ls_cod_deposito) then
				g_mb.messagebox("Anagrafica Fornitori", "Manca il codice deposito")
				return 1
			end if
			// fine modifica EnMe 20-10-2011 --------------------------	
			
		
			if s_cs_xx.parametri.impresa then
				ls_cod_conto = getitemstring(li_i, "cod_conto")
				ls_cod_fornitore = getitemstring(li_i, "cod_fornitore")
				if isnull(ls_cod_conto) and len(ls_cod_fornitore) <= 4 then
					g_mb.messagebox("APICE","Codice capoconto obbligatorio !")
					return 1
				end if
				
				choose case getitemstring(li_i, "flag_tipo_fornitore")
					case "S", "F"			// società o persona fisica (quindi c'è p.iva)
						if isnull(getitemstring(li_i, "partita_iva")) or len(trim(getitemstring(li_i, "partita_iva"))) < 1 then
							g_mb.messagebox("APICE","Se il fornitore è una Società o Persona Fisica è obbligatoria la partita IVA, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
					case "P"					// Privato (Quindi c'è il campo codice fiscale)
						if isnull(getitemstring(li_i, "cod_fiscale")) or len(trim(getitemstring(li_i, "cod_fiscale"))) < 1 then
							g_mb.messagebox("APICE","Se il fornitore è un privato è obbligatorio il codice fiscale, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
					case "E", "C","M"			// Estero, Cee (Quindi c'è il campo iden/iva - codice fiscale)
						if isnull(getitemstring(li_i, "cod_fiscale")) or len(trim(getitemstring(li_i, "cod_fiscale"))) < 1 then
							g_mb.messagebox("APICE","Se il fornitore è un privato è obbligatorio il codice fiscale, altrimenti potrebbero sorgere di problemi nella contabilità Impresa.",Information!)
							return 1
						end if
				end choose
				
				ls_cod_banca_clien_for = getitemstring(li_i, "cod_banca_clien_for")
				ls_cod_banca = getitemstring(li_i, "cod_banca")
				ls_cin = getitemstring(li_i, "cin")
				if isnull(ls_cod_banca_clien_for) then
					setnull(ls_cod_abi)
					setnull(ls_cod_cab)
					setnull(ls_cin)
				else
					select cod_abi, cod_cab, cin
					into   :ls_cod_abi, :ls_cod_cab, :ls_cin
					from   anag_banche_clien_for
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_banca_clien_for = :ls_cod_banca_clien_for;
				end if			
				if isnull(ls_cod_banca) then
					setnull(ls_cod_abi_1)
					setnull(ls_cod_cab_1)
					setnull(ls_cin_1)
				else
					select cod_abi, cod_cab, cin
					into   :ls_cod_abi_1, :ls_cod_cab_1, :ls_cin_1
					from   anag_banche
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_banca = :ls_cod_banca;
				end if
				
				//Donato 03-11-2008 modifica per passaggio parametri aggiuntivi:
				//telefono, fax, cellulare e gestione agenti
				//Donato 03-11-2008 modifica per gestione telefono, fax e cellulare
				//a cod_agente_1 passiamo sempre ""
				ls_telefono =getitemstring(li_i, "telefono")
				ls_fax = getitemstring(li_i, "fax")
				ls_telex = getitemstring(li_i, "telex")
				if not isnull( getitemstring(li_i, "cin") ) and len( getitemstring(li_i, "cin") ) > 0 then ls_cin = getitemstring(li_i, "cin")
				
				if iuo_impresa.uof_crea_cli_for(	"F", &
															ls_cod_fornitore, &
															ls_cod_conto, &
															getitemstring(li_i, "partita_iva"),&
															getitemstring(li_i, "cod_fiscale"), &
															getitemstring(li_i, "rag_soc_1"),&
															getitemstring(li_i, "rag_soc_2"), &
															getitemstring(li_i, "indirizzo"),&
															getitemstring(li_i, "localita"), &
															getitemstring(li_i, "cap"), &
															getitemstring(li_i, "provincia"), &
															getitemstring(li_i, "frazione"), &
															ls_cod_abi, &
															ls_cod_cab, &
															ls_cin, &
															ls_cod_abi_1, &
															ls_cod_cab_1, &
															ls_cin_1, &
															getitemstring(li_i, "cod_pagamento"), &
															getitemstring(li_i, "cod_valuta"), &
															getitemstring(li_i, "cod_nazione"), &
															getitemnumber(li_i, "ggmm_esclusione_1_da"), &
															getitemnumber(li_i, "ggmm_esclusione_1_a"), &
															getitemnumber(li_i, "ggmm_esclusione_2_da"), &
															getitemnumber(li_i, "ggmm_esclusione_2_a"), &
															getitemnumber(li_i, "data_sostituzione_1"), &
															getitemnumber(li_i, "data_sostituzione_2"), &												
															"N", &
															ls_telefono, ls_fax, ls_telex, &
															getitemstring(li_i, "casella_mail"), &
															"", &
															getitemstring(li_i, "iban"), &
															getitemstring(li_i, "swift"), &
															getitemnumber(li_i, "id_doc_fin_rating_impresa"), &
															getitemnumber(li_i, "id_doc_fin_voce_fin_impresa"), &
															getitemstring(li_i, "flag_tipo_fornitore"), &
															ref li_id_anagrafica, &
															ref li_id_sog_commerciale ) = -1 then 
					g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
					return 1
				end if
				
				
				// ---------------------------------------------------------------------------------------
				// cerco se c'è qualche cliente con la stessa partita iva o codice fiscale;
				//	se c'è copio le modifiche anagrafiche nel cliente      EnMe 02/05/2003 ----------------
				
				choose case getitemstring(li_i, "flag_tipo_fornitore")
					case "S", "F"			// società o persona fisica (quindi c'è p.iva)
						ls_partita_iva = getitemstring(li_i, "partita_iva")
						if not isnull(ls_partita_iva) and len(trim(ls_partita_iva)) > 0 then
							ll_cont = 0
							select count(*)
							into   :ll_cont
							from   anag_clienti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 partita_iva = :ls_partita_iva;
									 
							if ll_cont = 1 then
								
								select cod_cliente, rag_soc_1
								into   :ls_cod_cliente, :ls_rag_soc_1
								from   anag_clienti
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 partita_iva = :ls_partita_iva;
										 
								g_mb.messagebox("APICE","Il cliente " + ls_cod_cliente + " " + ls_rag_soc_1 + " ha la stessa partita iva del fornitore " + ls_cod_fornitore + " " + getitemstring(li_i,"rag_soc_1") + "~r~nIl cliente sarà aggiornato !",information!)
	
								ls_rag_soc_1 = getitemstring(li_i,"rag_soc_1")
								ls_rag_soc_2 = getitemstring(li_i,"rag_soc_2")
								ls_indirizzo = getitemstring(li_i,"indirizzo")
								ls_localita = getitemstring(li_i,"localita")
								ls_frazione = getitemstring(li_i,"frazione")
								ls_cap = getitemstring(li_i,"cap")
								ls_provincia = getitemstring(li_i,"provincia")
								ls_cod_fiscale_copia = getitemstring(li_i,"cod_fiscale")
								
								update anag_clienti
								set    rag_soc_1 = :ls_rag_soc_1,
								       rag_soc_2 = :ls_rag_soc_2,
										 indirizzo = :ls_indirizzo,
										 localita  = :ls_localita,
										 frazione  = :ls_frazione,
										 cap       = :ls_cap,
										 provincia = :ls_provincia,
										 cod_fiscale = :ls_cod_fiscale_copia
								where  cod_azienda = :s_cs_xx.cod_azienda and
								       cod_cliente = :ls_cod_cliente;
								if sqlca.sqlcode <> 0 then
									g_mb.messagebox("APICE","Errore in aggiornamento del cliente " + ls_cod_cliente + " " + ls_rag_soc_1 + ".Dettaglio errore.~r~n" + sqlca.sqlerrtext)
									return 1
								end if
							elseif ll_cont > 1 then
								g_mb.messagebox("APICE","La partita iva di questo fornitore corriponde a più di un cliente; in questo caso le modifiche anagrafiche non saranno trasferite, ma sarà necessario procedere manualmente!~r~nPREMERE INVIO per continuare ...",information!)
							end if
						end if
								 
					case "P", "E", "C","M"
						ls_cod_fiscale = getitemstring(li_i, "cod_fiscale")
						if not isnull(ls_cod_fiscale) and len(trim(ls_cod_fiscale)) > 0 then
							ll_cont = 0
							select count(*)
							into   :ll_cont
							from   anag_clienti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_fiscale = :ls_cod_fiscale;
									 
							if ll_cont = 1 then
								
								select cod_cliente, rag_soc_1
								into   :ls_cod_cliente, :ls_rag_soc_1
								from   anag_clienti
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_fiscale = :ls_cod_fiscale;
										 
								g_mb.messagebox("APICE","Il cliente " + ls_cod_cliente + " " + ls_rag_soc_1 + " ha lo stesso codice fiscale del fornitore " + ls_cod_fornitore + " " + getitemstring(li_i,"rag_soc_1") + "~r~nIl cliente sarà aggiornato !",information!)
	
								ls_rag_soc_1 = getitemstring(li_i,"rag_soc_1")
								ls_rag_soc_2 = getitemstring(li_i,"rag_soc_2")
								ls_indirizzo = getitemstring(li_i,"indirizzo")
								ls_localita = getitemstring(li_i,"localita")
								ls_frazione = getitemstring(li_i,"frazione")
								ls_cap = getitemstring(li_i,"cap")
								ls_provincia = getitemstring(li_i,"provincia")
								
								update anag_clienti
								set    rag_soc_1 = :ls_rag_soc_1,
								       rag_soc_2 = :ls_rag_soc_2,
										 indirizzo = :ls_indirizzo,
										 localita  = :ls_localita,
										 frazione  = :ls_frazione,
										 cap       = :ls_cap,
										 provincia = :ls_provincia
								where  cod_azienda = :s_cs_xx.cod_azienda and
								       cod_cliente = :ls_cod_cliente;
								if sqlca.sqlcode <> 0 then
									g_mb.messagebox("APICE","Errore in aggiornamento del cliente " + ls_cod_cliente + " " + ls_rag_soc_1 + ".Dettaglio errore.~r~n" + sqlca.sqlerrtext)
									return 1
								end if
							elseif ll_cont > 1 then
								g_mb.messagebox("APICE","Il codice fiscale di questo fornitore corriponde a più di un cliente; in questo caso le modifiche anagrafiche non saranno trasferite, ma sarà necessario procedere manualmente!~r~nPREMERE INVIO per continuare ...",information!)
							end if
						end if
								 
				end choose
				
				// fine modifica EnMe 02/05/2003 ---------------------------------------------
				
			end if
		end if
	next
end if
end event

event pcd_view;call super::pcd_view;
tab_dettaglio.det_2.dw_2.object.p_del_banca.enabled=false

tab_dettaglio.det_2.dw_2.object.b_autocomposizione.enabled=false

tab_dettaglio.det_2.dw_2.object.b_ricerca_abicab.enabled=false
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Condizioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 5
integer y = 32
integer width = 3342
integer height = 2040
integer taborder = 11
string dataobject = "d_fornitori_det_2"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string				ls_error, ls_iban, ls_cin, ls_abi, ls_cab, ls_cc
int					li_return
uo_banche		luo_banche




choose case dwo.name
	case "b_ricerca_abicab"
		guo_ricerca.uof_ricerca_abicab(tab_dettaglio.det_2.dw_2,"abi","cab")
		
	case "b_valida_iban"
		li_return = guo_functions.uof_valida_iban(getitemstring(row, "iban"), ref ls_error)
		
		if li_return > 0 then
			g_mb.success("IBAN valido")
		elseif li_return = 0 then
			g_mb.warning("Inserire un codice IBAN prima di eseguire la validazione")
		else
			g_mb.error(ls_error)
		end if
	
	
	case "b_autocomposizione"
		accepttext()
		luo_banche = create uo_banche
		ls_iban	= getitemstring(row, "iban")
		ls_cin		= getitemstring(row, "cin")
		ls_abi		= getitemstring(row, "abi")
		ls_cab	= getitemstring(row, "cab")
		ls_cc		= getitemstring(row, "conto_corrente")
		li_return = luo_banche.uof_componi_iban(0, ls_iban, ls_cin, ls_abi, ls_cab, ls_cc, ls_error)		//per ora ls_errore non lo gestisco
		destroy luo_banche
		
		if li_return = 0 then
			setitem(row, "iban", ls_iban)
			setitem(row, "cin", ls_cin)
			setitem(row, "abi", ls_abi)
			setitem(row, "cab", ls_cab)
			setitem(row, "conto_corrente", ls_cc)
		end if
	
end choose
end event

event itemchanged;call super::itemchanged;string ls_null

if i_extendmode then
	
	if isvalid(dwo) and not isnull( getitemstring(getrow(),"iban") )   then
		
		choose case lower(dwo.name)
			case "cin", "abi","cab"
				
				g_mb.warning("Attenzione! riferimenti banca fornitore variato; reimpostare IBAN")
				
				setnull(ls_null)
				setitem(getrow(),"iban",ls_null)
				
		end choose
	end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	tab_dettaglio.det_1.dw_1.setrow(getrow())
end if

end event

event clicked;call super::clicked;string			ls_null,ls_cod

if row>0 then
else
	return
end if

setnull(ls_null)

choose case dwo.name

	case "p_del_banca"
		
		ls_cod = getitemstring(row, "cod_fornitore") 
		if ls_cod="" or isnull(ls_cod) then return
		
		if g_mb.confirm("Eliminare l'associazione della banca appoggio fornitore?") then
			setitem(row, "iban", ls_null)
			setitem(row, "cin", ls_null)
			setitem(row, "abi", ls_null)
			setitem(row, "cab", ls_null)
			setitem(row, "conto_corrente", ls_null)
			setitem(row, "cod_banca_clien_for", ls_null)
			setitem(row, "swift", ls_null)
		end if
		
end choose
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Tabelle"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
integer x = 5
integer y = 32
integer width = 3470
integer height = 1084
integer taborder = 11
string dataobject = "d_fornitori_det_3"
boolean border = false
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	tab_dettaglio.det_1.dw_1.setrow(getrow())
end if

end event

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Qualità"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_visite_ispettive_fornitore dw_visite_ispettive_fornitore
dw_4 dw_4
end type

on det_4.create
this.dw_visite_ispettive_fornitore=create dw_visite_ispettive_fornitore
this.dw_4=create dw_4
this.Control[]={this.dw_visite_ispettive_fornitore,&
this.dw_4}
end on

on det_4.destroy
destroy(this.dw_visite_ispettive_fornitore)
destroy(this.dw_4)
end on

type dw_visite_ispettive_fornitore from datawindow within det_4
integer x = 37
integer y = 1120
integer width = 2601
integer height = 1316
integer taborder = 50
boolean titlebar = true
string title = "Visite Ispettive"
string dataobject = "d_tes_visite_ispettive_forn_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;s_cs_xx_parametri lstr_parametri
long ll_row, ll_anno, ll_num

if not isnull(row) and row > 0 then
	
	ll_anno = getitemnumber(row,"anno_reg_visita")
	ll_num = getitemnumber(row,"num_reg_visita")
	
	if ll_anno > 0 and ll_num > 0 then
		if not isvalid(w_tes_visite_ispettive) then
			lstr_parametri.parametro_ul_1 = ll_anno
			lstr_parametri.parametro_ul_2 = ll_num
		
			Window_Open_Parm(w_tes_visite_ispettive, -1, lstr_parametri)			
			
		else
			w_tes_visite_ispettive.show()
			w_tes_visite_ispettive.wf_carica_singola_visita(ll_anno, ll_num)
		end if
	end if
end if
end event

type dw_4 from uo_cs_xx_dw within det_4
integer x = 5
integer y = 32
integer width = 2647
integer height = 1172
integer taborder = 30
string dataobject = "d_fornitori_det_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
   case "flag_certificato"
      this.setitem(this.getrow(), "flag_tipo_certificazione", "N")
		
   case "flag_omologato"
      if i_coltext = 'S' then
			string ls_ritorno

			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "nome_doc_qualificazione"
			s_cs_xx.parametri.parametro_s_2 = "DQ7"
			s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
			if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
				ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
			
				if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
					this.setitem(this.getrow(), "nome_doc_qualificazione", ls_ritorno)			
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
					end if
				end if      
			else
				f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
			end if
		end if
end choose
end event

type det_5 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Giudizi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_check_list_fornitore dw_check_list_fornitore
dw_5 dw_5
end type

on det_5.create
this.dw_check_list_fornitore=create dw_check_list_fornitore
this.dw_5=create dw_5
this.Control[]={this.dw_check_list_fornitore,&
this.dw_5}
end on

on det_5.destroy
destroy(this.dw_check_list_fornitore)
destroy(this.dw_5)
end on

type dw_check_list_fornitore from datawindow within det_5
integer x = 14
integer y = 1112
integer width = 2560
integer height = 1336
integer taborder = 40
boolean titlebar = true
string title = "Liste di Controllo"
string dataobject = "d_tes_liste_contr_comp_forn_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;s_cs_xx_parametri lstr_parametri
long ll_row
decimal ld_num_reg_lista, ld_num_versione, ld_num_edizione

if not isnull(row) and row > 0 then
	
	ld_num_reg_lista = getitemdecimal(row,"num_reg_lista")
	ld_num_versione = getitemdecimal(row,"num_versione")
	ld_num_edizione = getitemdecimal(row,"num_edizione")
	
	if not isnull(ld_num_reg_lista) and not isnull(ld_num_versione) and &
				not isnull(ld_num_edizione) then
		if not isvalid(w_tes_liste_controllo) then
			lstr_parametri.parametro_ul_1 = ld_num_reg_lista
			lstr_parametri.parametro_ul_2 = ld_num_versione
			lstr_parametri.parametro_ul_3 = ld_num_edizione
		
			Window_Open_Parm(w_tes_liste_controllo, -1, lstr_parametri)			
			
		else
			w_tes_liste_controllo.show()
			w_tes_liste_controllo.wf_carica_singola_lista(ld_num_reg_lista, ld_num_versione, ld_num_edizione)
		end if
	end if
end if
end event

type dw_5 from uo_cs_xx_dw within det_5
integer x = 5
integer y = 32
integer width = 2606
integer height = 1108
integer taborder = 30
string dataobject = "d_fornitori_det_5"
boolean border = false
end type

event pcd_delete;call super::pcd_delete;dw_5.object.riferimenti.background.color = 12632256
dw_5.object.giudizio_storico.background.color = 12632256
dw_5.object.giudizio_terzi.background.color = 12632256
end event

event pcd_modify;call super::pcd_modify;dw_5.object.riferimenti.background.color = 12632256
dw_5.object.giudizio_storico.background.color = 12632256
dw_5.object.giudizio_terzi.background.color = 12632256
end event

event pcd_new;call super::pcd_new;dw_5.object.riferimenti.background.color = 12632256
dw_5.object.giudizio_storico.background.color = 12632256
dw_5.object.giudizio_terzi.background.color = 12632256
end event

event pcd_save;call super::pcd_save;dw_5.object.riferimenti.background.color = 12632256
dw_5.object.giudizio_storico.background.color = 12632256
dw_5.object.giudizio_terzi.background.color = 12632256
end event

event pcd_view;call super::pcd_view;dw_5.object.riferimenti.background.color = 12632256
dw_5.object.giudizio_storico.background.color = 12632256
dw_5.object.giudizio_terzi.background.color = 12632256
end event

type det_6 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Nominativi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_6 dw_6
end type

on det_6.create
this.dw_6=create dw_6
this.Control[]={this.dw_6}
end on

on det_6.destroy
destroy(this.dw_6)
end on

type dw_6 from uo_dw_nominativi within det_6
integer x = 5
integer y = 32
integer width = 3497
integer height = 2408
integer taborder = 40
boolean border = false
borderstyle borderstyle = stylebox!
end type

type det_7 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3474
integer height = 2464
long backcolor = 12632256
string text = "Col. Dinamiche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_7 dw_7
end type

on det_7.create
this.dw_7=create dw_7
this.Control[]={this.dw_7}
end on

on det_7.destroy
destroy(this.dw_7)
end on

type dw_7 from uo_colonne_dinamiche_dw within det_7
integer x = 32
integer y = 32
integer width = 3456
integer height = 2404
integer taborder = 60
end type

