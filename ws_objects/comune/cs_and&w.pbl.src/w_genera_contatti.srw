﻿$PBExportHeader$w_genera_contatti.srw
$PBExportComments$Finestra Generazione Contatti
forward
global type w_genera_contatti from w_cs_xx_risposta
end type
type st_1 from statictext within w_genera_contatti
end type
type cb_1 from uo_cb_close within w_genera_contatti
end type
type em_cod_contatto from editmask within w_genera_contatti
end type
type cb_genera from uo_cb_ok within w_genera_contatti
end type
type ddlb_contatti from dropdownlistbox within w_genera_contatti
end type
type st_clienti_old from statictext within w_genera_contatti
end type
end forward

global type w_genera_contatti from w_cs_xx_risposta
integer width = 1083
integer height = 424
string title = "Generazione Contatto"
st_1 st_1
cb_1 cb_1
em_cod_contatto em_cod_contatto
cb_genera cb_genera
ddlb_contatti ddlb_contatti
st_clienti_old st_clienti_old
end type
global w_genera_contatti w_genera_contatti

event pc_accept;call super::pc_accept;long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double		ld_sconto
string			ls_rag_soc_1_old, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_cliente, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_flag_fuori_fido, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_agente_1, ls_cod_agente_2, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_flag_riep_boll, ls_flag_riep_fatt, ls_cod_contatto, ls_cod_cliente, ls_cod_contatto_old, ls_casella_mail, ls_flag_raggruppo_iva_doc
datetime		ldt_data_esenzione_iva


ls_cod_contatto = trim(em_cod_contatto.text)

if isnull(ls_cod_contatto) or ls_cod_contatto = "" then
  	g_mb.messagebox("Attenzione", "Inserire un codice contatto.", &
              exclamation!, ok!)
   em_cod_contatto.setfocus()
	return
end if

select anag_contatti.rag_soc_1
into   :ls_rag_soc_1_old
from   anag_contatti
where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_contatti.cod_contatto = :ls_cod_contatto;

if sqlca.sqlcode <> 100 then
  	g_mb.messagebox("Attenzione", "Codice contatto già presente in anagrafica: " + ls_rag_soc_1_old, &
              exclamation!, ok!)
   em_cod_contatto.setfocus()
	return
end if

ll_i = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
ls_cod_cliente = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_cliente")

select anag_contatti.cod_contatto
into   :ls_cod_contatto_old
from   anag_contatti
where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_contatti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 100 then
  	g_mb.messagebox("Attenzione", "Codice Cliente già collegato al contatto: " + ls_cod_contatto_old, &
              exclamation!, ok!)
   em_cod_contatto.setfocus()
	return
end if

select  rag_soc_1,
          rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 telefono,
		 fax,
		 telex,
		 cod_fiscale,
		 partita_iva,
		 rif_interno,
		 flag_tipo_cliente,
		 cod_conto,
		 cod_iva,
		 num_prot_esenzione_iva,
		 data_esenzione_iva,
		 flag_sospensione_iva,
		 cod_pagamento,
		 giorno_fisso_scadenza,
		 mese_esclusione_1,
		 mese_esclusione_2,
		 ggmm_esclusione_1_da,
		 ggmm_esclusione_1_a,
		 ggmm_esclusione_2_da,
		 ggmm_esclusione_2_a,
		 data_sostituzione_1,
		 data_sostituzione_2,
		 sconto,
		 cod_tipo_listino_prodotto,
		 fido,
		 flag_fuori_fido,
		 cod_banca_clien_for,
		 conto_corrente,
		 cod_lingua,
		 cod_nazione,
		 cod_area,
		 cod_zona,
		 cod_valuta,
		 cod_categoria,
	 	 cod_agente_1,
		 cod_agente_2,
		 cod_imballo,
		 cod_porto,
		 cod_resa,
		 cod_mezzo,
		 cod_vettore,
		 cod_inoltro,
		 cod_deposito,
		 flag_riep_boll,
		 flag_riep_fatt,
		 casella_mail,
		 flag_raggruppo_iva_doc
into   
		:ls_rag_soc_1,
		:ls_rag_soc_2,
		:ls_indirizzo,
		:ls_localita,
		:ls_frazione,
		:ls_cap,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex,
		:ls_cod_fiscale,
		:ls_partita_iva,
		:ls_rif_interno,
		:ls_flag_tipo_cliente,
		:ls_cod_conto,
		:ls_cod_iva,
		:ls_num_prot_esenzione_iva,
		:ldt_data_esenzione_iva,
		:ls_flag_sospensione_iva,
		:ls_cod_pagamento,
		:ll_giorno_fisso_scadenza,
		:ll_mese_esclusione_1,
		:ll_mese_esclusione_2,
		:ll_ggmm_esclusione_1_da,
		:ll_ggmm_esclusione_1_a,
		:ll_ggmm_esclusione_2_da,
		:ll_ggmm_esclusione_2_a,
		:ll_data_sostituzione_1,
		:ll_data_sostituzione_2,
		:ld_sconto,
		:ls_cod_tipo_listino_prodotto,
		:ll_fido,
		:ls_flag_fuori_fido,
		:ls_cod_banca_clien_for,
		:ls_conto_corrente,
		:ls_cod_lingua,
		:ls_cod_nazione,
		:ls_cod_area ,
		:ls_cod_zona,
		:ls_cod_valuta ,
		:ls_cod_categoria,
		:ls_cod_agente_1,
		:ls_cod_agente_2,
		:ls_cod_imballo ,
		:ls_cod_porto,
		:ls_cod_resa ,
		:ls_cod_mezzo,
		:ls_cod_vettore ,
		:ls_cod_inoltro ,
		:ls_cod_deposito,
		:ls_flag_riep_boll ,
		:ls_flag_riep_fatt ,
		:ls_casella_mail,
		:ls_flag_raggruppo_iva_doc
from anag_clienti
where anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
      anag_clienti.cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode <> 0 then
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione Contatto.", &
              exclamation!, ok!)
	return
end if

insert into anag_contatti
            (cod_azienda,   
             cod_contatto,   
             rag_soc_1,   
             rag_soc_2,   
             indirizzo,   
             localita,   
             frazione,   
             cap,   
             provincia,   
             telefono,   
             fax,   
             telex,   
             cod_fiscale,   
             partita_iva,   
             rif_interno,   
             flag_tipo_cliente,   
             cod_capoconto,   
             cod_conto,   
             cod_iva,   
             num_prot_esenzione_iva,   
             data_esenzione_iva,   
             flag_sospensione_iva,   
             cod_pagamento,   
             giorno_fisso_scadenza,   
             mese_esclusione_1,   
             mese_esclusione_2,
			ggmm_esclusione_1_da,
			ggmm_esclusione_1_a,
			ggmm_esclusione_2_da,
			ggmm_esclusione_2_a,
             data_sostituzione_1,   
             data_sostituzione_2,   
             sconto,   
             cod_tipo_listino_prodotto,   
             fido,   
             flag_fuori_fido,   
             cod_banca_clien_for,   
             conto_corrente,   
             cod_lingua,   
             cod_nazione,   
             cod_area,   
             cod_zona,   
             cod_valuta,   
             cod_categoria,   
             cod_agente_1,   
             cod_agente_2,   
             cod_imballo,   
             cod_porto,   
             cod_resa,   
             cod_mezzo,   
             cod_vettore,   
             cod_inoltro,   
             cod_deposito,   
             flag_riep_boll,   
             flag_riep_fatt,   
             flag_blocco,   
             data_blocco,
             data_ultima_chiamata,   
             flag_per_richiamata,   
             per_richiamata,   
             cod_operaio,   
             cod_cliente,   
             ore_med_trattativa,   
             minuti_med_trattativa,   
             per_trat_chiuse,   
             importo_trat_media,   
             anno_fatturato,   
             importo_fatturato,  
			casella_mail,
			flag_raggruppo_iva_doc)
values      (:s_cs_xx.cod_azienda,
             :ls_cod_contatto,
             :ls_rag_soc_1,   
             :ls_rag_soc_2,   
             :ls_indirizzo,   
             :ls_localita,   
             :ls_frazione,   
             :ls_cap,   
             :ls_provincia,   
             :ls_telefono,   
             :ls_fax,   
             :ls_telex,   
             :ls_cod_fiscale,   
             :ls_partita_iva,   
             :ls_rif_interno,   
             :ls_flag_tipo_cliente,   
             null,   
             :ls_cod_conto,   
             :ls_cod_iva,   
             :ls_num_prot_esenzione_iva,   
             :ldt_data_esenzione_iva,   
             :ls_flag_sospensione_iva,   
             :ls_cod_pagamento,   
             :ll_giorno_fisso_scadenza,   
             :ll_mese_esclusione_1,   
             :ll_mese_esclusione_2,
			:ll_ggmm_esclusione_1_da,
			:ll_ggmm_esclusione_1_a,
			:ll_ggmm_esclusione_2_da,
			:ll_ggmm_esclusione_2_a,
             :ll_data_sostituzione_1,   
             :ll_data_sostituzione_2,   
             :ld_sconto,   
             :ls_cod_tipo_listino_prodotto,   
             :ll_fido,   
             :ls_flag_fuori_fido,   
             :ls_cod_banca_clien_for,   
             :ls_conto_corrente,   
             :ls_cod_lingua,   
             :ls_cod_nazione,   
             :ls_cod_area,   
             :ls_cod_zona,   
             :ls_cod_valuta,   
             :ls_cod_categoria,   
             :ls_cod_agente_1,   
             :ls_cod_agente_2,   
             :ls_cod_imballo,   
             :ls_cod_porto,   
             :ls_cod_resa,   
             :ls_cod_mezzo,   
             :ls_cod_vettore,   
             :ls_cod_inoltro,   
             :ls_cod_deposito,   
             :ls_flag_riep_boll,   
             :ls_flag_riep_fatt,   
             'N',   
             null,
             null,   
             'G',   
             null,   
             null,   
             :ls_cod_cliente,   
             null,   
             null,   
             null,   
             null,   
             null,   
             null,
			:ls_casella_mail,
			:ls_flag_raggruppo_iva_doc);

if sqlca.sqlcode <> 0 then
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione Contatto.", &
              exclamation!, ok!)
   rollback;
	return
end if

commit;
close(this)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_contatti, &
              sqlca, &
              "anag_contatti", &
              "cod_contatto", &
              "cod_contatto", &
              "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
              "")
end event

on w_genera_contatti.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_1=create cb_1
this.em_cod_contatto=create em_cod_contatto
this.cb_genera=create cb_genera
this.ddlb_contatti=create ddlb_contatti
this.st_clienti_old=create st_clienti_old
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.em_cod_contatto
this.Control[iCurrent+4]=this.cb_genera
this.Control[iCurrent+5]=this.ddlb_contatti
this.Control[iCurrent+6]=this.st_clienti_old
end on

on w_genera_contatti.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.em_cod_contatto)
destroy(this.cb_genera)
destroy(this.ddlb_contatti)
destroy(this.st_clienti_old)
end on

type st_1 from statictext within w_genera_contatti
integer x = 23
integer y = 120
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codice Nuovo Contatto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from uo_cb_close within w_genera_contatti
integer x = 137
integer y = 220
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

type em_cod_contatto from editmask within w_genera_contatti
integer x = 686
integer y = 120
integer width = 343
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!"
end type

type cb_genera from uo_cb_ok within w_genera_contatti
integer x = 526
integer y = 220
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

type ddlb_contatti from dropdownlistbox within w_genera_contatti
integer x = 686
integer y = 20
integer width = 343
integer height = 500
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_clienti_old from statictext within w_genera_contatti
integer x = 23
integer y = 20
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codici Contatti Esistenti:"
alignment alignment = right!
boolean focusrectangle = false
end type

