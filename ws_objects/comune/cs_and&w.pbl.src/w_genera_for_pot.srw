﻿$PBExportHeader$w_genera_for_pot.srw
$PBExportComments$Finestra Generazione Fornitori Potenziali
forward
global type w_genera_for_pot from w_cs_xx_risposta
end type
type st_1 from statictext within w_genera_for_pot
end type
type cb_1 from uo_cb_close within w_genera_for_pot
end type
type em_cod_for_pot from editmask within w_genera_for_pot
end type
type cb_genera from uo_cb_ok within w_genera_for_pot
end type
type ddlb_for_pot from dropdownlistbox within w_genera_for_pot
end type
type st_clienti_old from statictext within w_genera_for_pot
end type
end forward

global type w_genera_for_pot from w_cs_xx_risposta
integer width = 1079
integer height = 424
string title = "Generazione Fornitori Potenziali"
st_1 st_1
cb_1 cb_1
em_cod_for_pot em_cod_for_pot
cb_genera cb_genera
ddlb_for_pot ddlb_for_pot
st_clienti_old st_clienti_old
end type
global w_genera_for_pot w_genera_for_pot

event pc_accept;call super::pc_accept;long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double		ld_sconto, ld_peso_val_servizio, ld_peso_val_qualita, ld_limite_tolleranza
string			ls_rag_soc_1_old, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_fornitore, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_cod_for_pot, ls_cod_fornitore, ls_flag_certificato, ls_flag_approvato, ls_flag_strategico, &
				ls_flag_terzista, ls_flag_ver_ispettiva, ls_note_ver_ispettiva, ls_flag_omologato, ls_flag_procedure_speciali, ls_num_contratto, ls_cod_for_pot_old, &
				ls_flag_tipo_certificazione, ls_cod_piano_campionamento, ls_casella_mail, ls_nome_doc_qualificazione 
datetime		ldt_data_esenzione_iva, ldt_data_contratto, ldt_data_ver_ispettiva


ls_cod_for_pot = trim(em_cod_for_pot.text)

if isnull(ls_cod_for_pot) or ls_cod_for_pot = "" then
  	g_mb.messagebox("Attenzione", "Inserire un codice fornitore potenziale.", &
              exclamation!, ok!)
   em_cod_for_pot.setfocus()
	return
end if

select anag_for_pot.rag_soc_1
into   :ls_rag_soc_1_old
from   anag_for_pot
where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_for_pot.cod_for_pot = :ls_cod_for_pot;

if sqlca.sqlcode <> 100 then
  	g_mb.messagebox("Attenzione", "Codice fornitore potenziale già presente in anagrafica: " + ls_rag_soc_1_old, &
              exclamation!, ok!)
   em_cod_for_pot.setfocus()
	return
end if

ll_i = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
ls_cod_fornitore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_fornitore")

select anag_for_pot.cod_for_pot
into   :ls_cod_for_pot_old
from   anag_for_pot
where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_for_pot.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 100 then
  	g_mb.messagebox("Attenzione", "Codice Fornitore già collegato al fornitore potenziale: " + ls_cod_for_pot_old, &
              exclamation!, ok!)
   em_cod_for_pot.setfocus()
	return
end if

ls_rag_soc_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_1")
ls_rag_soc_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_2")
ls_indirizzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "indirizzo")
ls_localita = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "localita")
ls_frazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "frazione")
ls_cap = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cap")
ls_provincia = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "provincia")
ls_telefono = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telefono")
ls_fax = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "fax")
ls_telex = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telex")
ls_cod_fiscale = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_fiscale")
ls_partita_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "partita_iva")
ls_rif_interno = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rif_interno")
ls_flag_tipo_fornitore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_tipo_fornitore")
ls_cod_conto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_conto")
ls_cod_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_iva")
ls_num_prot_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "num_prot_esenzione_iva")
ldt_data_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_esenzione_iva")
ls_flag_sospensione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_sospensione_iva")
ls_cod_pagamento = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_pagamento")
ll_giorno_fisso_scadenza = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "giorno_fisso_scadenza")
ll_mese_esclusione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_1")
ll_mese_esclusione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_2")

ll_ggmm_esclusione_1_da = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_da")
ll_ggmm_esclusione_1_a = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_a")
ll_ggmm_esclusione_2_da = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_da")
ll_ggmm_esclusione_2_a = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_a")

ll_data_sostituzione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_1")
ll_data_sostituzione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_2")
ld_sconto = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "sconto")
ls_cod_tipo_listino_prodotto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_tipo_listino_prodotto")
ll_fido = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "fido")
ls_cod_banca_clien_for = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_banca_clien_for")
ls_conto_corrente = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "conto_corrente")
ls_cod_lingua = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_lingua")
ls_cod_nazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_nazione")
ls_cod_area = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_area")
ls_cod_zona = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_zona")
ls_cod_valuta = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_valuta")
ls_cod_categoria = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_categoria")
ls_cod_imballo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_imballo")
ls_cod_porto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_porto")
ls_cod_resa = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_resa")
ls_cod_mezzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_mezzo")
ls_cod_vettore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_vettore")
ls_cod_inoltro = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_inoltro")
ls_cod_deposito = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_deposito")
ls_flag_certificato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_certificato")
ls_flag_approvato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_approvato")
ls_flag_strategico = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_strategico")
ls_flag_terzista = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_terzista")
ls_num_contratto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "num_contratto")
ldt_data_contratto = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_contratto")
ls_flag_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_ver_ispettiva")
ldt_data_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_ver_ispettiva")  
ls_note_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "note_ver_ispettiva")
ls_flag_omologato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_omologato")
ls_flag_procedure_speciali = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_procedure_speciali")
ld_peso_val_servizio = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "peso_val_servizio")
ld_peso_val_qualita = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "peso_val_qualita")
ld_limite_tolleranza = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "limite_tolleranza")
ls_flag_tipo_certificazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_tipo_certificazione")
ls_cod_piano_campionamento = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_piano_campionamento")
ls_casella_mail = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "casella_mail")
ls_nome_doc_qualificazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "nome_doc_qualificazione")

insert into anag_for_pot  
            (cod_azienda,   
             cod_for_pot,   
             rag_soc_1,   
             rag_soc_2,   
             indirizzo,   
             localita,   
             frazione,   
             cap,   
             provincia,   
             telefono,   
             fax,   
             telex,   
             cod_fiscale,   
             partita_iva,   
             rif_interno,   
             flag_tipo_fornitore,   
             cod_conto,   
             cod_iva,   
             num_prot_esenzione_iva,   
             data_esenzione_iva,   
             flag_sospensione_iva,   
             cod_pagamento,   
             giorno_fisso_scadenza,   
             mese_esclusione_1,   
             mese_esclusione_2,
			ggmm_esclusione_1_da,
		 	ggmm_esclusione_1_a,
		 	ggmm_esclusione_2_da,
		 	ggmm_esclusione_2_a,
             data_sostituzione_1,   
             data_sostituzione_2,   
             sconto,   
             cod_tipo_listino_prodotto,   
             fido,   
             cod_banca_clien_for,   
             conto_corrente,   
             cod_lingua,   
             cod_nazione,   
             cod_area,   
             cod_zona,   
             cod_valuta,   
             cod_categoria,   
             cod_imballo,   
             cod_porto,   
             cod_resa,   
             cod_mezzo,   
             cod_vettore,   
             cod_inoltro,   
             cod_deposito,   
             flag_certificato,   
             flag_approvato,   
             flag_strategico,   
             flag_terzista,   
             num_contratto,   
             data_contratto,   
             flag_ver_ispettiva,   
             data_ver_ispettiva,   
             note_ver_ispettiva,   
             flag_omologato,   
             flag_procedure_speciali,   
             peso_val_servizio,   
             peso_val_qualita,   
             limite_tolleranza,   
             flag_blocco,
             data_blocco,
             data_ultima_chiamata,
             flag_per_richiamata,
             per_richiamata,
             cod_operaio,
             cod_fornitore,
             flag_tipo_certificazione,
             cod_piano_campionamento,
	 		 	 casella_mail,
 				 nome_doc_qualificazione)  
values      (:s_cs_xx.cod_azienda,
             :ls_cod_for_pot,
             :ls_rag_soc_1,   
             :ls_rag_soc_2,   
             :ls_indirizzo,   
             :ls_localita,   
             :ls_frazione,   
             :ls_cap,   
             :ls_provincia,   
             :ls_telefono,   
             :ls_fax,   
             :ls_telex,   
             :ls_cod_fiscale,   
             :ls_partita_iva,   
             :ls_rif_interno,   
             :ls_flag_tipo_fornitore,   
             :ls_cod_conto,   
             :ls_cod_iva,   
             :ls_num_prot_esenzione_iva,   
             :ldt_data_esenzione_iva,   
             :ls_flag_sospensione_iva,   
             :ls_cod_pagamento,   
             :ll_giorno_fisso_scadenza,   
             :ll_mese_esclusione_1,   
             :ll_mese_esclusione_2,
			:ll_ggmm_esclusione_1_da,
			:ll_ggmm_esclusione_1_a,
			:ll_ggmm_esclusione_2_da,
			:ll_ggmm_esclusione_2_a,
             :ll_data_sostituzione_1,   
             :ll_data_sostituzione_2,   
             :ld_sconto,   
             :ls_cod_tipo_listino_prodotto,   
             :ll_fido,   
             :ls_cod_banca_clien_for,   
             :ls_conto_corrente,   
             :ls_cod_lingua,   
             :ls_cod_nazione,   
             :ls_cod_area,   
             :ls_cod_zona,   
             :ls_cod_valuta,   
             :ls_cod_categoria,   
             :ls_cod_imballo,   
             :ls_cod_porto,   
             :ls_cod_resa,   
             :ls_cod_mezzo,   
             :ls_cod_vettore,   
             :ls_cod_inoltro,   
             :ls_cod_deposito,   
             :ls_flag_certificato,   
             :ls_flag_approvato,   
             :ls_flag_strategico,   
             :ls_flag_terzista,   
             :ls_num_contratto,   
             :ldt_data_contratto,   
             :ls_flag_ver_ispettiva,   
             :ldt_data_ver_ispettiva,   
             :ls_note_ver_ispettiva,   
             :ls_flag_omologato,   
             :ls_flag_procedure_speciali,   
             :ld_peso_val_servizio,   
             :ld_peso_val_qualita,   
             :ld_limite_tolleranza,   
             'N',   
             null,
             null,
             'G',
             null,
             null,
             :ls_cod_fornitore,
             :ls_flag_tipo_certificazione,
             :ls_cod_piano_campionamento,
				 :ls_casella_mail,
				 :ls_nome_doc_qualificazione);

if sqlca.sqlcode <> 0 then
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione fornitore potenziale.", &
              exclamation!, ok!)
   rollback;
	return
end if

commit;

close(this)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_for_pot, &
              sqlca, &
              "anag_for_pot", &
              "cod_for_pot", &
              "cod_for_pot", &
              "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
              "")
end event

on w_genera_for_pot.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_1=create cb_1
this.em_cod_for_pot=create em_cod_for_pot
this.cb_genera=create cb_genera
this.ddlb_for_pot=create ddlb_for_pot
this.st_clienti_old=create st_clienti_old
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.em_cod_for_pot
this.Control[iCurrent+4]=this.cb_genera
this.Control[iCurrent+5]=this.ddlb_for_pot
this.Control[iCurrent+6]=this.st_clienti_old
end on

on w_genera_for_pot.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.em_cod_for_pot)
destroy(this.cb_genera)
destroy(this.ddlb_for_pot)
destroy(this.st_clienti_old)
end on

type st_1 from statictext within w_genera_for_pot
integer x = 23
integer y = 120
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codice Nuovo For. Pot.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from uo_cb_close within w_genera_for_pot
integer x = 137
integer y = 220
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

type em_cod_for_pot from editmask within w_genera_for_pot
integer x = 686
integer y = 120
integer width = 343
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!"
end type

type cb_genera from uo_cb_ok within w_genera_for_pot
integer x = 526
integer y = 220
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

type ddlb_for_pot from dropdownlistbox within w_genera_for_pot
integer x = 686
integer y = 20
integer width = 343
integer height = 500
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
end type

type st_clienti_old from statictext within w_genera_for_pot
integer x = 23
integer y = 20
integer width = 654
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codici For. Pot. Esistenti:"
alignment alignment = right!
boolean focusrectangle = false
end type

