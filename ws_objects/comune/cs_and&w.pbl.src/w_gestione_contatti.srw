﻿$PBExportHeader$w_gestione_contatti.srw
forward
global type w_gestione_contatti from w_cs_xx_principale
end type
type dw_folder from u_folder within w_gestione_contatti
end type
type dw_contatti_lista from uo_cs_xx_dw within w_gestione_contatti
end type
type dw_contatti_det_1 from uo_cs_xx_dw within w_gestione_contatti
end type
end forward

global type w_gestione_contatti from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3168
integer height = 1640
string title = "Contatti"
boolean minbox = false
dw_folder dw_folder
dw_contatti_lista dw_contatti_lista
dw_contatti_det_1 dw_contatti_det_1
end type
global w_gestione_contatti w_gestione_contatti

type variables
string is_tipo_anagrafica
end variables

event pc_setwindow;call super::pc_setwindow;
//tipo_anagrafica
/*
C	cliente
F	fornitore
P	fornitore potenziale
O	contatto
*/
is_tipo_anagrafica = s_cs_xx.parametri.parametro_s_1

choose case is_tipo_anagrafica
	case "C"
		this.title = "Contatti - Cliente"
		
	case "F"
		this.title = "Contatti - Fornitore"
		
	case "P"
		this.title = "Contatti - Fornitore Potenziale"
		
	case "O"
		this.title = "Contatti"
		
end choose

setnull(s_cs_xx.parametri.parametro_s_1)

dw_contatti_lista.set_dw_key("cod_azienda")
dw_contatti_lista.set_dw_key("tipo_anagrafica")
dw_contatti_lista.set_dw_key("codice")
dw_contatti_lista.set_dw_key("progressivo")

dw_contatti_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
dw_contatti_det_1.set_dw_options(sqlca, &
                                    dw_contatti_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

windowobject lw_oggetti[]
lw_oggetti[1] = dw_contatti_lista
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[1] = dw_contatti_det_1
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

iuo_dw_main=dw_contatti_lista
end event

on w_gestione_contatti.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_contatti_lista=create dw_contatti_lista
this.dw_contatti_det_1=create dw_contatti_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_contatti_lista
this.Control[iCurrent+3]=this.dw_contatti_det_1
end on

on w_gestione_contatti.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_contatti_lista)
destroy(this.dw_contatti_det_1)
end on

type dw_folder from u_folder within w_gestione_contatti
integer x = 23
integer y = 20
integer width = 3086
integer height = 1500
integer taborder = 40
end type

type dw_contatti_lista from uo_cs_xx_dw within w_gestione_contatti
event ue_key pbm_dwnkey
event ue_postopen ( )
event post_rowfocuschanged ( )
integer x = 69
integer y = 140
integer width = 2994
integer height = 1360
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_gestione_contatti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_codice

ls_codice = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], 2)

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "codice")) or this.getitemstring(ll_i, "codice") = "" then
		this.setitem(ll_i, "codice", ls_codice)
	end if
	
	 if isnull(this.getitemstring(ll_i, "tipo_anagrafica")) or this.getitemstring(ll_i, "tipo_anagrafica") = "" then
      this.setitem(ll_i, "tipo_anagrafica", is_tipo_anagrafica)
   end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_rag_soc_1
long ll_errore, ll_anno_registrazione, ll_num_registrazione

string ls_codice

ls_codice = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], 2) //campo codice

ll_errore = retrieve(s_cs_xx.cod_azienda, is_tipo_anagrafica, ls_codice)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event updatestart;call super::updatestart;long ll_progressivo
string ls_codice


ls_codice = this.getitemstring(getrow(), "codice")

select max(progressivo)
into :ll_progressivo
from tab_nominativi
where cod_azienda=:s_cs_xx.cod_azienda
	and tipo_anagrafica=:is_tipo_anagrafica and codice=:ls_codice
;

if isnull(ll_progressivo) then ll_progressivo = 0
ll_progressivo += 1

this.setitem(getrow(), "progressivo", ll_progressivo)

end event

type dw_contatti_det_1 from uo_cs_xx_dw within w_gestione_contatti
event ue_key pbm_dwnkey
integer x = 69
integer y = 140
integer width = 2994
integer height = 1360
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_gestione_contatti_1"
end type

event pcd_validaterow;call super::pcd_validaterow;//string ls_modify, ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
//       ls_null, ls_match, ls_match_1
//
//
//setnull(ls_null)
//if i_rownbr > 0 then
//   ls_cod_tipo_det_ven = this.getitemstring(i_rownbr,"cod_tipo_det_ven")
//
//   select tab_tipi_det_ven.flag_tipo_det_ven
//   into   :ls_flag_tipo_det_ven
//   from   tab_tipi_det_ven
//   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//
//   if sqlca.sqlcode <> 0 then
//      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
//                 exclamation!, ok!)
//      return
//   end if
//
//   if i_insave > 0 then
//      if ls_flag_tipo_det_ven = "M" then
//         i_colnbr = column_nbr("cod_prodotto")
//         if isnull(get_col_data(i_rownbr, i_colnbr)) then
//          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         i_colnbr = column_nbr("cod_misura")
//         if isnull(get_col_data(i_rownbr, i_colnbr)) then
//          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         i_colnbr = column_nbr("quan_offerta")
//         if double(get_col_data(i_rownbr, i_colnbr)) = 0 then
//          	g_mb.messagebox("Attenzione", "Quantità offerta obbligatoria.", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//      elseif ls_flag_tipo_det_ven = "C" then
//         i_colnbr = column_nbr("cod_prodotto")
//         if isnull(get_col_data(i_rownbr, i_colnbr)) then
//          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//      end if
//   end if
//end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_contatti_lista.setrow(getrow())
end if
end event

