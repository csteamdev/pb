﻿$PBExportHeader$w_nuovo_cliente.srw
$PBExportComments$Finestra Nuovo Fornitore da Ricerca Fornitori
forward
global type w_nuovo_cliente from w_cs_xx_risposta
end type
type dw_nuovo_cliente from uo_cs_xx_dw within w_nuovo_cliente
end type
type cb_ok from commandbutton within w_nuovo_cliente
end type
type cb_annulla from commandbutton within w_nuovo_cliente
end type
end forward

global type w_nuovo_cliente from w_cs_xx_risposta
int Width=2871
int Height=1565
boolean TitleBar=true
string Title="Nuovo Cliente"
dw_nuovo_cliente dw_nuovo_cliente
cb_ok cb_ok
cb_annulla cb_annulla
end type
global w_nuovo_cliente w_nuovo_cliente

on w_nuovo_cliente.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_nuovo_cliente=create dw_nuovo_cliente
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_nuovo_cliente
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=cb_annulla
end on

on w_nuovo_cliente.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_nuovo_cliente)
destroy(this.cb_ok)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;dw_nuovo_cliente.set_dw_key("cod_azienda")
dw_nuovo_cliente.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen + &
										 c_newonopen + &
										 c_nomodify, &										 
                               c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_nuovo_cliente, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_nuovo_cliente, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_nuovo_cliente, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_nuovo_cliente, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_nuovo_cliente from uo_cs_xx_dw within w_nuovo_cliente
int X=23
int Y=21
int Width=2789
int Height=1321
int TabOrder=10
string DataObject="d_nuovo_cliente"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

type cb_ok from commandbutton within w_nuovo_cliente
int X=2423
int Y=1361
int Width=389
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_2 = dw_nuovo_cliente.getitemstring(dw_nuovo_cliente.getrow(),"cod_cliente")
parent.triggerevent("pc_save")
parent.postevent("pc_close")
end event

type cb_annulla from commandbutton within w_nuovo_cliente
int X=2012
int Y=1361
int Width=389
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "ANNULLA"
save_on_close(c_socnosave)
close(parent)
end event

