﻿$PBExportHeader$w_tes_privilegi.srw
forward
global type w_tes_privilegi from w_cs_xx_principale
end type
type dw_privilegi from uo_cs_xx_dw within w_tes_privilegi
end type
end forward

global type w_tes_privilegi from w_cs_xx_principale
integer width = 2775
integer height = 1724
string title = "Privilegi"
windowtype windowtype = child!
boolean center = true
dw_privilegi dw_privilegi
end type
global w_tes_privilegi w_tes_privilegi

on w_tes_privilegi.create
int iCurrent
call super::create
this.dw_privilegi=create dw_privilegi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_privilegi
end on

on w_tes_privilegi.destroy
call super::destroy
destroy(this.dw_privilegi)
end on

event pc_setwindow;call super::pc_setwindow;dw_privilegi.move(20,20)


dw_privilegi.set_dw_key("cod_azienda")
dw_privilegi.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

iuo_dw_main = dw_privilegi
end event

event resize;/* BASTA ANCESTOR **/

dw_privilegi.resize(newwidth - 40, newheight - 40)
end event

type dw_privilegi from uo_cs_xx_dw within w_tes_privilegi
integer x = 23
integer y = 20
integer width = 2697
integer height = 1580
integer taborder = 10
string dataobject = "d_tes_privilegi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event buttonclicked;call super::buttonclicked;if row > 0 and dwo.name = "b_valori_lista" and ib_stato_visualizzazione then
	
	s_cs_xx.parametri.parametro_s_1 = getitemstring(row, "cod_privilegio")
	
	window_open(w_det_privilegi, 0)
	
end if
end event

event pcd_delete;/**
 * !!! TOLTO ANCESTOR, VIENE CHIAMATO SOLO SE E' POSSIBILE ELIMINARE !!!
 *
 * Controllo che il privilegio non sia assegnato a qualche mansionario
 **/
 

string ls_cod_privilegio
long ll_count

ls_cod_privilegio = getitemstring(getrow(), "cod_privilegio")

select count(cod_privilegio)
into :ll_count
from tab_privilegi_mansionari
where	
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_privilegio = :ls_cod_privilegio;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo delle referenze del privilegio.", sqlca)
	return
elseif sqlca.sqlcode = 0 and ll_count > 0 then
	g_mb.error("Il privilegio è assegnato a dei mansionari pertanto non è possibile eliminarlo")
	return
end if

dw_privilegi.object.b_valori_lista.enabled = false
super::event pcd_delete(wparam, lparam)
end event

event pcd_modify;call super::pcd_modify;dw_privilegi.object.b_valori_lista.enabled = false
end event

event pcd_new;call super::pcd_new;dw_privilegi.object.b_valori_lista.enabled = false
end event

event pcd_view;call super::pcd_view;dw_privilegi.object.b_valori_lista.enabled = true
end event

event updatestart;call super::updatestart;/** 
 * Controllo le righe eliminate e se di tipo lista cancello i valori
 **/
 
string ls_cod_privilegio
int li_i

for li_i = 1 to DeletedCount()
	
	if getitemstring(li_i, "flag_tipo", Delete!, false) = "L" then
	
		ls_cod_privilegio = getitemstring(li_i, "cod_privilegio", Delete!, false)
	
		delete from det_privilegi
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_privilegio = :ls_cod_privilegio;
			
		// Serve gestire l'errore?
		
	end if
next
end event

