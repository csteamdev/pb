﻿$PBExportHeader$w_vettori.srw
$PBExportComments$Finestra Gestione Vettori
forward
global type w_vettori from w_cs_xx_principale
end type
type dw_vettori_lista from uo_cs_xx_dw within w_vettori
end type
type dw_vettori_det from uo_cs_xx_dw within w_vettori
end type
end forward

global type w_vettori from w_cs_xx_principale
int Width=2780
int Height=1489
boolean TitleBar=true
string Title="Anagrafica Vettori"
dw_vettori_lista dw_vettori_lista
dw_vettori_det dw_vettori_det
end type
global w_vettori w_vettori

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_vettori_lista.set_dw_key("cod_azienda")
dw_vettori_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)
dw_vettori_det.set_dw_options(sqlca, &
                              dw_vettori_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)
iuo_dw_main = dw_vettori_lista
end on

on w_vettori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_vettori_lista=create dw_vettori_lista
this.dw_vettori_det=create dw_vettori_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_vettori_lista
this.Control[iCurrent+2]=dw_vettori_det
end on

on w_vettori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_vettori_lista)
destroy(this.dw_vettori_det)
end on

type dw_vettori_lista from uo_cs_xx_dw within w_vettori
int X=23
int Y=21
int Width=2698
int Height=501
int TabOrder=10
string DataObject="d_vettori_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_vettori_det from uo_cs_xx_dw within w_vettori
int X=23
int Y=541
int Width=2698
int Height=821
int TabOrder=20
string DataObject="d_vettori_det"
BorderStyle BorderStyle=StyleRaised!
end type

