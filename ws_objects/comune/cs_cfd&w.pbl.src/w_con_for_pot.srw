﻿$PBExportHeader$w_con_for_pot.srw
$PBExportComments$Finestra Controllo Fornitori Potenziali
forward
global type w_con_for_pot from w_cs_xx_principale
end type
type dw_con_for_pot from uo_cs_xx_dw within w_con_for_pot
end type
end forward

global type w_con_for_pot from w_cs_xx_principale
int Width=2204
int Height=469
boolean TitleBar=true
string Title="Gestione Parametri Fornitori Potenziali"
dw_con_for_pot dw_con_for_pot
end type
global w_con_for_pot w_con_for_pot

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_for_pot, &
                 "cod_tipo_trattativa", &
                 sqlca, &
                 "tab_tipi_trattative", &
                 "cod_tipo_trattativa", &
                 "des_tipo_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_trattativa = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_con_for_pot, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_for_pot.set_dw_key("cod_azienda")
dw_con_for_pot.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on w_con_for_pot.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_for_pot=create dw_con_for_pot
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_for_pot
end on

on w_con_for_pot.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_for_pot)
end on

type dw_con_for_pot from uo_cs_xx_dw within w_con_for_pot
int X=23
int Y=21
int Width=2126
int Height=321
string DataObject="d_con_for_pot"
BorderStyle BorderStyle=StyleRaised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

