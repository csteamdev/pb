﻿$PBExportHeader$w_det_trat_for.srw
$PBExportComments$Finestra Gestione Dettaglio Trattative Fornitori
forward
global type w_det_trat_for from w_cs_xx_principale
end type
type dw_det_trat_for_lista from uo_cs_xx_dw within w_det_trat_for
end type
type cb_corrispondenze from commandbutton within w_det_trat_for
end type
type cb_dettagli from commandbutton within w_det_trat_for
end type
type cb_note from commandbutton within w_det_trat_for
end type
type dw_folder from u_folder within w_det_trat_for
end type
type dw_det_trat_for_det from uo_cs_xx_dw within w_det_trat_for
end type
type pb_prod_view from cb_listview within w_det_trat_for
end type
end forward

global type w_det_trat_for from w_cs_xx_principale
integer width = 3465
integer height = 1108
string title = "Gestione Dettaglio Trattative Fornitori"
dw_det_trat_for_lista dw_det_trat_for_lista
cb_corrispondenze cb_corrispondenze
cb_dettagli cb_dettagli
cb_note cb_note
dw_folder dw_folder
dw_det_trat_for_det dw_det_trat_for_det
pb_prod_view pb_prod_view
end type
global w_det_trat_for w_det_trat_for

type variables
boolean ib_nuovo=false, ib_modifica=false

end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_trat_for_det, &
                 "cod_prodotto", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_trat_for_det, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_tipo_det_acq = 'M' or flag_tipo_det_acq = 'C' or flag_tipo_det_acq = 'N')")

end event

event pc_setwindow;call super::pc_setwindow;dw_det_trat_for_lista.set_dw_key("cod_azienda")
dw_det_trat_for_lista.set_dw_key("anno_trattativa")
dw_det_trat_for_lista.set_dw_key("num_trattativa")
dw_det_trat_for_lista.set_dw_options(sqlca, &
                                     i_openparm, &
                                     c_scrollparent, &
                                     c_default)
dw_det_trat_for_det.set_dw_options(sqlca, &
                                   dw_det_trat_for_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)

windowobject lw_oggetti[]
lw_oggetti[1] = dw_det_trat_for_det
lw_oggetti[2] = pb_prod_view
dw_folder.fu_assigntab(1, "Dettaglio", lw_oggetti[])
dw_folder.fu_foldercreate(1, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main=dw_det_trat_for_lista
cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note.enabled = false

end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note.enabled = false

end on

on w_det_trat_for.create
int iCurrent
call super::create
this.dw_det_trat_for_lista=create dw_det_trat_for_lista
this.cb_corrispondenze=create cb_corrispondenze
this.cb_dettagli=create cb_dettagli
this.cb_note=create cb_note
this.dw_folder=create dw_folder
this.dw_det_trat_for_det=create dw_det_trat_for_det
this.pb_prod_view=create pb_prod_view
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_trat_for_lista
this.Control[iCurrent+2]=this.cb_corrispondenze
this.Control[iCurrent+3]=this.cb_dettagli
this.Control[iCurrent+4]=this.cb_note
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_det_trat_for_det
this.Control[iCurrent+7]=this.pb_prod_view
end on

on w_det_trat_for.destroy
call super::destroy
destroy(this.dw_det_trat_for_lista)
destroy(this.cb_corrispondenze)
destroy(this.cb_dettagli)
destroy(this.cb_note)
destroy(this.dw_folder)
destroy(this.dw_det_trat_for_det)
destroy(this.pb_prod_view)
end on

type dw_det_trat_for_lista from uo_cs_xx_dw within w_det_trat_for
integer x = 23
integer y = 20
integer width = 2994
integer height = 500
integer taborder = 70
string dataobject = "d_det_trat_for_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_modify

   if ib_modifica or ib_nuovo then
      ls_cod_tipo_det_acq = dw_det_trat_for_det.getitemstring(dw_det_trat_for_det.getrow(), "cod_tipo_det_acq")
      ld_datawindow = dw_det_trat_for_det
		lp_prod_view = pb_prod_view
      f_tipo_det_trat_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
   end if

   if ib_nuovo then
      ls_modify = "cod_tipo_det_acq.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "cod_tipo_det_acq.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_trat_for_det.modify(ls_modify)
   end if
end if
end event

event pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select tab_tipi_det_acq.flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Acquisto.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_trattata")
         if integer(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità trattata obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemnumber(this.getrow(), "anno_trattativa")) then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note.enabled = true
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note.enabled = false
   end if

   ib_modifica = false
   ib_nuovo = false
   dw_det_trat_for_det.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemnumber(this.getrow(), "anno_trattativa")) then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note.enabled = true
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note.enabled = false
   end if

   ib_modifica = false
   ib_nuovo = false
   dw_det_trat_for_det.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
end if

end event

event pcd_new;call super::pcd_new;if i_extendmode then
   long ll_prog_trattativa, ll_anno_trattativa, ll_num_trattativa
   string ls_cod_tipo_off_acq, ls_cod_tipo_trattativa, ls_cod_tipo_det_acq, ls_null, ls_modify
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view


   setnull(ls_null)
   ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
   ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

   select max(det_trat_for.prog_trattativa)
   into   :ll_prog_trattativa
   from   det_trat_for
   where  det_trat_for.cod_azienda = :s_cs_xx.cod_azienda and
          det_trat_for.anno_trattativa = :ll_anno_trattativa and
          det_trat_for.num_trattativa = :ll_num_trattativa;

   if not isnull(ll_prog_trattativa) then
      this.setitem(this.getrow(), "prog_trattativa", ll_prog_trattativa + 10)
   else
      this.setitem(this.getrow(), "prog_trattativa", 10)
   end if

   ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")

   select tab_tipi_trattative.cod_tipo_off_acq
   into   :ls_cod_tipo_off_acq
   from   tab_tipi_trattative
   where  tab_tipi_trattative.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_trattative.cod_tipo_trattativa = :ls_cod_tipo_trattativa;

   if sqlca.sqlcode = 0 then
      select tab_tipi_off_acq.cod_tipo_det_acq
      into   :ls_cod_tipo_det_acq
      from   tab_tipi_off_acq
      where  tab_tipi_off_acq.cod_azienda = :s_cs_xx.cod_azienda and 
             tab_tipi_off_acq.cod_tipo_off_acq = :ls_cod_tipo_off_acq;
   end if   

   if sqlca.sqlcode = 0 then
      dw_det_trat_for_det.setitem(dw_det_trat_for_det.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
   else
      ls_cod_tipo_det_acq = ls_null
   end if

   dw_det_trat_for_det.setitem(dw_det_trat_for_det.getrow(), "quan_trattata", 1)

   ls_modify = "cod_tipo_det_acq.protect='0'~t"
   dw_det_trat_for_det.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='16777215'~t"
   dw_det_trat_for_det.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_acq) then
      dw_det_trat_for_det.setitem(dw_det_trat_for_det.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      ld_datawindow = dw_det_trat_for_det
		lp_prod_view = pb_prod_view
      f_tipo_det_trat_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "quan_trattata.protect='1'~t"
      dw_det_trat_for_det.modify(ls_modify)
      ls_modify = "quan_trattata.background.color='12632256'~t"
      dw_det_trat_for_det.modify(ls_modify)
   end if

   cb_dettagli.enabled = false
   cb_corrispondenze.enabled = false
   cb_note.enabled = false
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_trattativa, ll_num_trattativa

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then
      this.setitem(ll_i, "anno_trattativa", ll_anno_trattativa)
   end if
   if this.getitemnumber(ll_i, "num_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "num_trattativa")) then
      this.setitem(ll_i, "num_trattativa", ll_num_trattativa)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_trattativa, ll_num_trattativa


ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_trattativa, ll_num_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_modify


   ib_modifica = true

   ls_modify = "cod_tipo_det_acq.protect='1'~t"
   dw_det_trat_for_det.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='12632256'~t"
   dw_det_trat_for_det.modify(ls_modify)
   ls_cod_tipo_det_acq = dw_det_trat_for_det.getitemstring(dw_det_trat_for_det.getrow(), "cod_tipo_det_acq")
   ld_datawindow = dw_det_trat_for_det
	lp_prod_view = pb_prod_view
   f_tipo_det_trat_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)

   cb_dettagli.enabled = false
   cb_corrispondenze.enabled = false
   cb_note.enabled = false
end if
end event

type cb_corrispondenze from commandbutton within w_det_trat_for
integer x = 3040
integer y = 220
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_det_trat_for_corrispondenze, -1, dw_det_trat_for_lista)

end on

type cb_dettagli from commandbutton within w_det_trat_for
integer x = 3040
integer y = 120
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

on clicked;integer li_i
long ll_i, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa, ll_prog_riga
double ld_costo
string ls_cod_tipo_trattativa, ls_cod_fase_trattativa

li_i = dw_det_trat_for_lista.getrow()
ll_anno_trattativa = dw_det_trat_for_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_det_trat_for_lista.getitemnumber(li_i, "num_trattativa")
ll_prog_trattativa = dw_det_trat_for_lista.getitemnumber(li_i, "prog_trattativa")

select tes_trat_for.cod_tipo_trattativa
into   :ls_cod_tipo_trattativa
from   tes_trat_for
where  tes_trat_for.cod_azienda = :s_cs_xx.cod_azienda and
       tes_trat_for.anno_trattativa = :ll_anno_trattativa and 
       tes_trat_for.num_trattativa = :ll_num_trattativa;

declare cu_det_fasi cursor for select det_fasi_trattative.prog_riga from det_fasi_trattative where det_fasi_trattative.cod_azienda = :s_cs_xx.cod_azienda and det_fasi_trattative.anno_trattativa = :ll_anno_trattativa and det_fasi_trattative.num_trattativa = :ll_num_trattativa and det_fasi_trattative.prog_trattativa = :ll_prog_trattativa;

open cu_det_fasi;

ll_i = 0
do while 0 = 0
   ll_i ++
   fetch cu_det_fasi into :ll_prog_riga;

   if sqlca.sqlcode = 100 then

      declare cu_tab_tipi_det cursor for select tab_tipi_det_trattative.cod_fase_trattativa, tab_tipi_det_trattative.prog_riga,tab_tipi_det_trattative.costo from tab_tipi_det_trattative where  tab_tipi_det_trattative.cod_azienda = :s_cs_xx.cod_azienda and tab_tipi_det_trattative.cod_tipo_trattativa = :ls_cod_tipo_trattativa;

      open cu_tab_tipi_det;
      ll_i = 0
      do while 0 = 0
         ll_i ++
         fetch cu_tab_tipi_det into :ls_cod_fase_trattativa, :ll_prog_riga, :ld_costo;

         if sqlca.sqlcode <> 0 then exit

         insert into det_fasi_trat_for
                     (cod_azienda,
                      anno_trattativa,
                      num_trattativa,
                      prog_trattativa,
                      cod_tipo_trattativa,
                      cod_fase_trattativa,
                      prog_riga,
                      des_det_trattativa,
                      flag_controllo,
                      flag_disponibile,
                      data_disponibile,
                      flag_tempo,
                      tempo_speso,
                      note,
                      flag_blocco,
                      data_blocco,
                      cod_operaio,
                      costo)
         values      (:s_cs_xx.cod_azienda,
                      :ll_anno_trattativa,
                      :ll_num_trattativa,
                      :ll_prog_trattativa,
                      :ls_cod_tipo_trattativa,
                      :ls_cod_fase_trattativa,
                      :ll_prog_riga,
                      null,
                      'N',
                      'N',
                      null,
                      'G',
                      null,
                      null,
                      'N',
                      null,
                      null,
                      :ld_costo);
      loop
      close cu_tab_tipi_det;
      exit
   end if
   exit
loop

close cu_det_fasi;

window_open_parm(w_det_fasi_trat_for, -1, dw_det_trat_for_lista)

end on

type cb_note from commandbutton within w_det_trat_for
integer x = 3040
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_det_trat_for_note, -1, dw_det_trat_for_lista)

end on

type dw_folder from u_folder within w_det_trat_for
integer x = 23
integer y = 540
integer width = 3383
integer height = 440
integer taborder = 10
end type

type dw_det_trat_for_det from uo_cs_xx_dw within w_det_trat_for
integer x = 46
integer y = 640
integer width = 3337
integer height = 320
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_det_trat_for_det"
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_prodotto, ls_flag_tipo_det_acq, ls_modify, ls_null, &
          ls_cod_tipo_det_acq, ls_flag_decimali
   double ld_quantita, ld_quan_minima, ld_inc_ordine, ld_quan_massima

   setnull(ls_null)

   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
   choose case i_colname
      case "cod_tipo_det_acq"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_trat_for_det.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_trat_for_det.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_trat_for_det.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_trat_for_det.modify(ls_modify)
         ls_modify = "quan_trattata.protect='0'~t"
         dw_det_trat_for_det.modify(ls_modify)
         ls_modify = "quan_trattata.background.color='16777215'~t"
         dw_det_trat_for_det.modify(ls_modify)
   
         ld_datawindow = dw_det_trat_for_det
			lp_prod_view = pb_prod_view
         f_tipo_det_trat_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
      case "cod_prodotto"
         select anag_prodotti.flag_decimali,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima
         into   :ls_flag_decimali,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;

         ld_quantita = this.getitemnumber(i_rownbr, "quan_trattata")

         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_trattata", ld_quantita)
         end if

         if ld_quantita < ld_quan_minima and ld_quan_minima > 0 then
            this.setitem(i_rownbr, "quan_trattata", ld_quan_minima)
         end if
         if ld_quantita > ld_quan_massima and ld_quan_massima > 0 then
            this.setitem(i_rownbr, "quan_trattata", ld_quan_massima)
         end if
         if ld_inc_ordine > 0 then
            this.setitem(i_rownbr, "quan_trattata", ld_quan_minima)
         end if
      case "quan_trattata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima
         into   :ls_flag_decimali,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_trattata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         if double(i_coltext) < ld_quan_minima and ld_quan_minima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata inferiore alla quantità minima ordinabile: " + string(ld_quan_minima), &
                        exclamation!, ok!)
         end if
         if double(i_coltext) > ld_quan_massima and ld_quan_massima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata superiore alla quantità massima ordinabile: " + string(ld_quan_massima), &
                        exclamation!, ok!)
         end if
         if ld_inc_ordine > 0 then
            if mod(double(i_coltext),ld_inc_ordine) <> 0 then
               g_mb.messagebox("Attenzione", "Proporzione incremento ordine non rispettato: " + string(ld_inc_ordine), &
                           exclamation!, ok!)
            end if
         end if
   end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_trat_for_det,"cod_prodotto")
end choose
end event

type pb_prod_view from cb_listview within w_det_trat_for
integer x = 2309
integer y = 760
integer width = 73
integer height = 80
integer taborder = 2
end type

event getfocus;call super::getfocus;dw_det_trat_for_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

