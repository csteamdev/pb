﻿$PBExportHeader$w_det_trat_for_corrispondenze.srw
$PBExportComments$Finestra Gestione Dettaglio Trattative Fornitori Corrispondenze
forward
global type w_det_trat_for_corrispondenze from w_cs_xx_principale
end type
type dw_det_trat_for_corrispondenze_lista from uo_cs_xx_dw within w_det_trat_for_corrispondenze
end type
type dw_det_trat_for_corrispondenze_det from uo_cs_xx_dw within w_det_trat_for_corrispondenze
end type
type cb_note_esterne from commandbutton within w_det_trat_for_corrispondenze
end type
type cb_controllo from commandbutton within w_det_trat_for_corrispondenze
end type
end forward

global type w_det_trat_for_corrispondenze from w_cs_xx_principale
integer width = 3465
integer height = 1776
string title = "Gestione Corrispondenze Dettaglio Trattative Fornitori"
dw_det_trat_for_corrispondenze_lista dw_det_trat_for_corrispondenze_lista
dw_det_trat_for_corrispondenze_det dw_det_trat_for_corrispondenze_det
cb_note_esterne cb_note_esterne
cb_controllo cb_controllo
end type
global w_det_trat_for_corrispondenze w_det_trat_for_corrispondenze

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_trat_for_corrispondenze_det, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_trat_for_corrispondenze_det, &
                 "num_reg_lista", &
                 sqlca, &
                 "tes_liste_controllo", &
                 "num_reg_lista", &
                 "des_lista", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")
f_po_loaddddw_dw(dw_det_trat_for_corrispondenze_det, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_det_trat_for_corrispondenze_lista.set_dw_key("cod_azienda")
dw_det_trat_for_corrispondenze_lista.set_dw_key("anno_trattativa")
dw_det_trat_for_corrispondenze_lista.set_dw_key("num_trattativa")
dw_det_trat_for_corrispondenze_lista.set_dw_key("prog_trattativa")
dw_det_trat_for_corrispondenze_lista.set_dw_options(sqlca, &
                                                    i_openparm, &
                                                    c_scrollparent, &
                                                    c_default)
dw_det_trat_for_corrispondenze_det.set_dw_options(sqlca, &
                                                  dw_det_trat_for_corrispondenze_lista, &
                                                  c_sharedata + c_scrollparent, &
                                                  c_default)

iuo_dw_main=dw_det_trat_for_corrispondenze_lista

cb_controllo.enabled = false
cb_note_esterne.enabled = false

end event

event pc_delete;call super::pc_delete;cb_controllo.enabled = false
cb_note_esterne.enabled = false

end event

on w_det_trat_for_corrispondenze.create
int iCurrent
call super::create
this.dw_det_trat_for_corrispondenze_lista=create dw_det_trat_for_corrispondenze_lista
this.dw_det_trat_for_corrispondenze_det=create dw_det_trat_for_corrispondenze_det
this.cb_note_esterne=create cb_note_esterne
this.cb_controllo=create cb_controllo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_trat_for_corrispondenze_lista
this.Control[iCurrent+2]=this.dw_det_trat_for_corrispondenze_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_controllo
end on

on w_det_trat_for_corrispondenze.destroy
call super::destroy
destroy(this.dw_det_trat_for_corrispondenze_lista)
destroy(this.dw_det_trat_for_corrispondenze_det)
destroy(this.cb_note_esterne)
destroy(this.cb_controllo)
end on

event pc_new;call super::pc_new;dw_det_trat_for_corrispondenze_det.setitem(dw_det_trat_for_corrispondenze_det.getrow(), "data_corrispondenza", datetime(today()))

end event

type dw_det_trat_for_corrispondenze_lista from uo_cs_xx_dw within w_det_trat_for_corrispondenze
integer x = 23
integer y = 20
integer width = 2994
integer height = 500
integer taborder = 10
string dataobject = "d_det_trat_for_corrispondenze_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
      cb_controllo.enabled = true
   else
      cb_controllo.enabled = false
   end if
end if
end on

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog_corrispondenza, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa
string ls_cod_corrispondenza
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")
ll_prog_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then
      this.setitem(ll_i, "anno_trattativa", ll_anno_trattativa)
   end if
   if this.getitemnumber(ll_i, "num_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "num_trattativa")) then
      this.setitem(ll_i, "num_trattativa", ll_num_trattativa)
   end if
   if this.getitemnumber(ll_i, "prog_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "prog_trattativa")) then
      this.setitem(ll_i, "prog_trattativa", ll_prog_trattativa)
   end if

   if isnull(this.getitemnumber(ll_i, "prog_corrispondenza")) or &
      this.getitemnumber(ll_i, "prog_corrispondenza") = 0 then

      ls_cod_corrispondenza = this.getitemstring(ll_i,"cod_corrispondenza")
      ldt_data_corrispondenza = this.getitemdatetime(ll_i,"data_corrispondenza")
      ldt_ora_corrispondenza = this.getitemdatetime(ll_i,"ora_corrispondenza")

      select max(det_trat_for_corrispondenze.prog_corrispondenza)
      into   :ll_prog_corrispondenza
      from   det_trat_for_corrispondenze
      where  det_trat_for_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
             det_trat_for_corrispondenze.anno_trattativa = :ll_anno_trattativa and
             det_trat_for_corrispondenze.num_trattativa = :ll_num_trattativa and
             det_trat_for_corrispondenze.prog_trattativa = :ll_prog_trattativa and
             det_trat_for_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and
             det_trat_for_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and
             det_trat_for_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza;

      if not isnull(ll_prog_corrispondenza) then
         this.setitem(ll_i, "prog_corrispondenza", ll_prog_corrispondenza + 1)
      else
         this.setitem(ll_i, "prog_corrispondenza", 1)
      end if
   end if
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa


ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")
ll_prog_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_det_trat_for_corrispondenze_det.modify(ls_modify)

   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_det_trat_for_corrispondenze_det.modify(ls_modify)

   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

on updateend;call uo_cs_xx_dw::updateend;integer li_i
long ll_num_reg_lista, ll_prog_liste_con_comp

for li_i = 1 to this.deletedcount()
   ll_num_reg_lista = this.getitemnumber(li_i, "num_reg_lista_comp", delete!, true)
   ll_prog_liste_con_comp = this.getitemnumber(li_i, "prog_liste_con_comp", delete!, true)
   f_cancella_liste_con_comp(ll_num_reg_lista, ll_prog_liste_con_comp)
next

end on

type dw_det_trat_for_corrispondenze_det from uo_cs_xx_dw within w_det_trat_for_corrispondenze
integer x = 23
integer y = 540
integer width = 3383
integer height = 1120
integer taborder = 20
string dataobject = "d_det_trat_for_corrispondenze_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   double ld_costo
   integer li_null, li_num_reg_lista, li_num_versione, li_num_edizione, li_prog_lista
   setnull(li_null)


   choose case i_colname
      case "cod_corrispondenza"
         this.setitem(i_rownbr, "num_reg_lista", li_null)

         select tab_corrispondenze.num_reg_lista
         into   :li_num_reg_lista
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext; 

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "num_reg_lista", li_num_reg_lista)
            select   tes_liste_controllo.num_versione,
                     tes_liste_controllo.num_edizione
            into     :li_num_versione, 
                     :li_num_edizione
            from     tes_liste_controllo
            where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                     tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                     tes_liste_controllo.flag_valido = 'S';

            this.setitem(this.getrow(), "num_versione", li_num_versione)
            this.setitem(this.getrow(), "num_edizione", li_num_edizione)
         end if

         select tab_corrispondenze.costo
         into   :ld_costo
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "costo", ld_costo)
         end if
      case "num_reg_lista"
         
			li_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

			if not isnull(li_prog_lista) then
				g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata dopo aver già eseguito la compilazione")
				return 1
			end if	
			
			li_num_reg_lista = long(i_coltext)
			
			select max(num_edizione)
			into   :li_num_edizione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :li_num_reg_lista and
					 approvato_da is not null;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if
			
			select max(num_versione)
			into   :li_num_versione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :li_num_reg_lista and
					 num_edizione = :li_num_edizione and
					 approvato_da is not null;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if
			
			setnull(li_prog_lista)
			
			setitem(getrow(),"num_edizione",li_num_edizione)
			setitem(getrow(),"num_versione",li_num_versione)
			setitem(getrow(),"prog_liste_con_comp",li_prog_lista)
			
   end choose
end if
end event

type cb_note_esterne from commandbutton within w_det_trat_for_corrispondenze
integer x = 3040
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_corrispondenza,ls_db
integer li_i, li_prog_corrispondenza, li_risposta
long ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_det_trat_for_corrispondenze_lista.getrow()
ll_anno_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(li_i, "num_trattativa")
ll_prog_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(li_i, "prog_trattativa")
ls_cod_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemstring(li_i, "cod_corrispondenza")
ldt_data_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemdatetime(li_i, "data_corrispondenza")
ldt_ora_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemdatetime(li_i, "ora_corrispondenza")
li_prog_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemnumber(li_i, "prog_corrispondenza")

// 15-07-2002 modifiche Michela: controllo enginetype

ls_db = f_db()

if ls_db = "MSSQL" then

	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob det_trat_for_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_trat_for_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           prog_trattativa = :ll_prog_trattativa and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else

	selectblob det_trat_for_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_trat_for_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           prog_trattativa = :ll_prog_trattativa and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if
window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob det_trat_for_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa and 
	              prog_trattativa = :ll_prog_trattativa and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob det_trat_for_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa and 
	              prog_trattativa = :ll_prog_trattativa and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza;
					  
	end if
	
   commit;
end if
end event

type cb_controllo from commandbutton within w_det_trat_for_corrispondenze
integer x = 3040
integer y = 120
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;string ls_cod_corrispondenza

long   ll_prog_corrispondenza, ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista_comp, ll_anno_trattativa, ll_num_trattativa, &
       ll_prog_trattativa
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza
   
ll_i[1] = dw_det_trat_for_corrispondenze_lista.getrow()
ll_num_reg_lista_comp = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"num_reg_lista_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"anno_trattativa")
   ll_num_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"num_trattativa")
   ll_prog_trattativa = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"prog_trattativa")
   ls_cod_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemstring(ll_i[1],"cod_corrispondenza") 
   ldt_data_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemdatetime(ll_i[1],"data_corrispondenza") 
   ldt_ora_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemdatetime(ll_i[1],"ora_corrispondenza") 
   ll_prog_corrispondenza = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"prog_corrispondenza") 
   ll_num_reg_lista_comp = dw_det_trat_for_corrispondenze_lista.getitemnumber(ll_i[1],"num_reg_lista") 
   
   update det_trat_for_corrispondenze
   set    det_trat_for_corrispondenze.num_versione = :ll_num_versione,
          det_trat_for_corrispondenze.num_edizione = :ll_num_edizione,
          det_trat_for_corrispondenze.num_reg_lista_comp = :ll_num_reg_lista_comp,
          det_trat_for_corrispondenze.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  det_trat_for_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
          det_trat_for_corrispondenze.anno_trattativa = :ll_anno_trattativa and 
          det_trat_for_corrispondenze.num_trattativa = :ll_num_trattativa and 
          det_trat_for_corrispondenze.prog_trattativa = :ll_prog_trattativa and 
          det_trat_for_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and 
          det_trat_for_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and 
          det_trat_for_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza and
          det_trat_for_corrispondenze.prog_corrispondenza = :ll_prog_corrispondenza;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_det_trat_for_corrispondenze_lista.triggerevent("pcd_retrieve")
   dw_det_trat_for_corrispondenze_lista.set_selected_rows(1, &
                                                          ll_i[], &
                                                          c_ignorechanges, &
                                                          c_refreshchildren, &
                                                          c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_det_trat_for_corrispondenze_lista)

end event

