﻿$PBExportHeader$w_for_pot.srw
$PBExportComments$Finestra Gestione Fornitori Potenziali
forward
global type w_for_pot from w_cs_xx_principale
end type
type cb_rubrica from commandbutton within w_for_pot
end type
type cb_corrispondenze from commandbutton within w_for_pot
end type
type cb_fornitori from commandbutton within w_for_pot
end type
type cb_note from commandbutton within w_for_pot
end type
type dw_for_pot_lista from uo_cs_xx_dw within w_for_pot
end type
type cb_reset from commandbutton within w_for_pot
end type
type cb_ricerca from commandbutton within w_for_pot
end type
type dw_folder_search from u_folder within w_for_pot
end type
type gb_1 from groupbox within w_for_pot
end type
type st_1 from statictext within w_for_pot
end type
type dw_ricerca from u_dw_search within w_for_pot
end type
type cb_doc_comp from cb_documenti_compilati within w_for_pot
end type
type dw_for_pot_det_4 from uo_cs_xx_dw within w_for_pot
end type
type dw_for_pot_det_3 from uo_cs_xx_dw within w_for_pot
end type
type dw_for_pot_det_5 from uo_cs_xx_dw within w_for_pot
end type
type dw_nominativi from uo_dw_nominativi within w_for_pot
end type
type dw_folder from u_folder within w_for_pot
end type
type dw_col_din from uo_colonne_dinamiche_dw within w_for_pot
end type
type dw_for_pot_det_2 from uo_cs_xx_dw within w_for_pot
end type
type dw_for_pot_det_1 from uo_cs_xx_dw within w_for_pot
end type
end forward

global type w_for_pot from w_cs_xx_principale
integer width = 3575
integer height = 1920
string title = "Gestione Fornitori Potenziali"
cb_rubrica cb_rubrica
cb_corrispondenze cb_corrispondenze
cb_fornitori cb_fornitori
cb_note cb_note
dw_for_pot_lista dw_for_pot_lista
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_folder_search dw_folder_search
gb_1 gb_1
st_1 st_1
dw_ricerca dw_ricerca
cb_doc_comp cb_doc_comp
dw_for_pot_det_4 dw_for_pot_det_4
dw_for_pot_det_3 dw_for_pot_det_3
dw_for_pot_det_5 dw_for_pot_det_5
dw_nominativi dw_nominativi
dw_folder dw_folder
dw_col_din dw_col_din
dw_for_pot_det_2 dw_for_pot_det_2
dw_for_pot_det_1 dw_for_pot_det_1
end type
global w_for_pot w_for_pot

type variables

end variables

forward prototypes
public function integer wf_conta_for_pot ()
end prototypes

public function integer wf_conta_for_pot ();long ll_num_for_pot


select count(anag_for_pot.cod_for_pot)
into :ll_num_for_pot
from anag_for_pot
where anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda;

st_1.text = string(ll_num_for_pot,"###,###")
return 0
end function

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_for_pot_det_2, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_2, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_2, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_2, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_2, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_nazione", &
                 sqlca, &
                 "tab_nazioni", &
                 "cod_nazione", &
                 "des_nazione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_3, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_5, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_for_pot_det_5, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_for_pot_det_4, &
                 "cod_piano_campionamento", &
                 sqlca, &
                 "tab_tipi_piani_campionamento", &
                 "cod_piano_campionamento", &
                 "des_piano_campionamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_piano_campionamento in (select cod_piano_campionamento from tes_piani_campionamento where cod_azienda = '" + s_cs_xx.cod_azienda + "')")


dw_ricerca.fu_loadcode("rs_cod_valuta", &
                        "tab_valute", &
								"cod_valuta", &
								"des_valuta", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_valuta asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_for_pot", &
                        "anag_for_pot", &
								"cod_for_pot", &
								"rag_soc_1", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_for_pot asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_pagamento", &
                        "tab_pagamenti", &
								"cod_pagamento", &
								"des_pagamento", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_pagamento asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_categoria", &
                        "tab_categorie", &
								"cod_categoria", &
								"des_categoria", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_categoria asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_nazione", &
                        "tab_nazioni", &
								"cod_nazione", &
								"des_nazione", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_nazione asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_area", &
                        "tab_aree", &
								"cod_area", &
								"des_area", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_area asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_zona", &
                        "tab_zone", &
								"cod_zona", &
								"des_zona", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_zona asc", "(Tutti)" )
end event

event pc_delete;call super::pc_delete;cb_corrispondenze.enabled = false
cb_rubrica.enabled = false
cb_fornitori.enabled = false
cb_note.enabled = false
cb_doc_comp.enabled = false
end event

event pc_setwindow;call super::pc_setwindow;string ls_modify, ls_flag_qualificazione 
windowobject lw_oggetti[], l_objects[]
ls_flag_qualificazione = 'N'

lw_oggetti[1] = dw_for_pot_det_1
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
lw_oggetti[1] = dw_for_pot_det_2
dw_folder.fu_assigntab(2, "Condizioni", lw_oggetti[])
lw_oggetti[1] = dw_for_pot_det_3
dw_folder.fu_assigntab(3, "Tabelle", lw_oggetti[])
lw_oggetti[1] = dw_for_pot_det_5
dw_folder.fu_assigntab(5, "Contatti", lw_oggetti[])
lw_oggetti[1] = dw_nominativi
dw_folder.fu_assigntab(6, "Nominativi", lw_oggetti[])
lw_oggetti[1] = dw_col_din
dw_folder.fu_assigntab(7, "Col. Dinamiche", lw_oggetti[])

lw_oggetti[1] = dw_for_pot_det_4

dw_nominativi.uof_set_dw(dw_for_pot_lista, "cod_for_pot", dw_nominativi.FOR_POT)

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//select flag_qualificazione
// into :ls_flag_qualificazione  
// from mansionari  
//where cod_azienda = :s_cs_xx.cod_azienda and
//		cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_qualificazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.qualificazione) then ls_flag_qualificazione = 'S'
//--- Fine modifica Giulio

if ls_flag_qualificazione = 'N' then
	dw_for_pot_det_4.object.flag_omologato.visible = 0
	dw_for_pot_det_4.object.nome_doc_qualificazione.visible = 0
	dw_for_pot_det_4.object.st_abil_mans_1.visible = 1
	dw_for_pot_det_4.object.st_abil_mans_2.visible = 1
	cb_doc_comp.visible = false
else
	dw_for_pot_det_4.object.flag_omologato.visible = 1
	dw_for_pot_det_4.object.nome_doc_qualificazione.visible = 1
	dw_for_pot_det_4.object.st_abil_mans_1.visible = 0
	dw_for_pot_det_4.object.st_abil_mans_2.visible = 0
	lw_oggetti[2] = cb_doc_comp
end if

dw_folder.fu_assigntab(4, "Qualità", lw_oggetti[])
dw_folder.fu_foldercreate(7,7)
dw_folder.fu_selecttab(1)

dw_for_pot_lista.set_dw_key("cod_azienda")
dw_for_pot_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_noretrieveonopen, &
                                c_default)
dw_for_pot_det_1.set_dw_options(sqlca, &
                                dw_for_pot_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_for_pot_det_2.set_dw_options(sqlca, &
                                dw_for_pot_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_for_pot_det_3.set_dw_options(sqlca, &
                                dw_for_pot_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_for_pot_det_4.set_dw_options(sqlca, &
                                dw_for_pot_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_for_pot_det_5.set_dw_options(sqlca, &
                                dw_for_pot_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main=dw_for_pot_lista
dw_col_din.uof_set_dw(dw_for_pot_lista, {"cod_for_pot"})

cb_corrispondenze.enabled = false
cb_rubrica.enabled = false
cb_fornitori.enabled = false
cb_note.enabled = false
cb_doc_comp.enabled = false

ls_modify = "flag_tipo_certificazione.protect='1~tif(flag_certificato=~~'N~~',1,0)'~t"
ls_modify = ls_modify + "flag_tipo_certificazione.background.color='12632256~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
dw_for_pot_det_4.modify(ls_modify)

// -----------------------------  aggiunto per ricerca prodotti ----------------------------
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

l_criteriacolumn[1] = "rs_cod_for_pot"
l_criteriacolumn[2] = "rs_partita_iva"
l_criteriacolumn[3] = "rs_cod_categoria"
l_criteriacolumn[4] = "rs_cod_fiscale"
l_criteriacolumn[5] = "rs_cap"
l_criteriacolumn[6] = "rs_localita"
l_criteriacolumn[7] = "rs_provincia"
l_criteriacolumn[8] = "rs_cod_pagamento"
l_criteriacolumn[9] = "rs_flag_tipo_fornitore"
l_criteriacolumn[10] = "rs_cod_nazione"
l_criteriacolumn[11] = "rs_cod_area"
l_criteriacolumn[12] = "rs_cod_zona"
l_criteriacolumn[13] = "rs_cod_valuta"
l_criteriacolumn[14] = "rs_flag_blocco"
l_criteriacolumn[15] = "rd_data_blocco"

l_searchtable[1] = "anag_for_pot"
l_searchtable[2] = "anag_for_pot"
l_searchtable[3] = "anag_for_pot"
l_searchtable[4] = "anag_for_pot"
l_searchtable[5] = "anag_for_pot"
l_searchtable[6] = "anag_for_pot"
l_searchtable[7] = "anag_for_pot"
l_searchtable[8] = "anag_for_pot"
l_searchtable[9] = "anag_for_pot"
l_searchtable[10] = "anag_for_pot"
l_searchtable[11] = "anag_for_pot"
l_searchtable[12] = "anag_for_pot"
l_searchtable[13] = "anag_for_pot"
l_searchtable[14] = "anag_for_pot"
l_searchtable[15] = "anag_for_pot"

l_searchcolumn[1] = "cod_for_pot"
l_searchcolumn[2] = "partita_iva"
l_searchcolumn[3] = "cod_categoria"
l_searchcolumn[4] = "cod_fiscale"
l_searchcolumn[5] = "cap"
l_searchcolumn[6] = "localita"
l_searchcolumn[7] = "provincia"
l_searchcolumn[8] = "cod_pagamento"
l_searchcolumn[9] = "flag_tipo_fornitore"
l_searchcolumn[10] = "cod_nazione"
l_searchcolumn[11] = "cod_area"
l_searchcolumn[12] = "cod_zona"
l_searchcolumn[13] = "cod_valuta"
l_searchcolumn[14] = "flag_blocco"
l_searchcolumn[15] = "data_blocco"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_for_pot_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_for_pot_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_for_pot_lista.change_dw_current()
wf_conta_for_pot()

//string ls_flag_qualificazione
//ls_flag_qualificazione = 'N'
//
//select flag_qualificazione
// into :ls_flag_qualificazione  
// from mansionari  
//where cod_azienda = :s_cs_xx.cod_azienda and
//		cod_utente = :s_cs_xx.cod_utente;
//
//if ls_flag_qualificazione = 'N' then
//	dw_for_pot_det_4.object.flag_omologato.visible = 0
//	dw_for_pot_det_4.object.st_abil_mans.visible = 1
//else
//	dw_for_pot_det_4.object.flag_omologato.visible = 1
//	dw_for_pot_det_4.object.st_abil_mans.visible = 0
//end if
end event

on w_for_pot.create
int iCurrent
call super::create
this.cb_rubrica=create cb_rubrica
this.cb_corrispondenze=create cb_corrispondenze
this.cb_fornitori=create cb_fornitori
this.cb_note=create cb_note
this.dw_for_pot_lista=create dw_for_pot_lista
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_folder_search=create dw_folder_search
this.gb_1=create gb_1
this.st_1=create st_1
this.dw_ricerca=create dw_ricerca
this.cb_doc_comp=create cb_doc_comp
this.dw_for_pot_det_4=create dw_for_pot_det_4
this.dw_for_pot_det_3=create dw_for_pot_det_3
this.dw_for_pot_det_5=create dw_for_pot_det_5
this.dw_nominativi=create dw_nominativi
this.dw_folder=create dw_folder
this.dw_col_din=create dw_col_din
this.dw_for_pot_det_2=create dw_for_pot_det_2
this.dw_for_pot_det_1=create dw_for_pot_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_rubrica
this.Control[iCurrent+2]=this.cb_corrispondenze
this.Control[iCurrent+3]=this.cb_fornitori
this.Control[iCurrent+4]=this.cb_note
this.Control[iCurrent+5]=this.dw_for_pot_lista
this.Control[iCurrent+6]=this.cb_reset
this.Control[iCurrent+7]=this.cb_ricerca
this.Control[iCurrent+8]=this.dw_folder_search
this.Control[iCurrent+9]=this.gb_1
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.dw_ricerca
this.Control[iCurrent+12]=this.cb_doc_comp
this.Control[iCurrent+13]=this.dw_for_pot_det_4
this.Control[iCurrent+14]=this.dw_for_pot_det_3
this.Control[iCurrent+15]=this.dw_for_pot_det_5
this.Control[iCurrent+16]=this.dw_nominativi
this.Control[iCurrent+17]=this.dw_folder
this.Control[iCurrent+18]=this.dw_col_din
this.Control[iCurrent+19]=this.dw_for_pot_det_2
this.Control[iCurrent+20]=this.dw_for_pot_det_1
end on

on w_for_pot.destroy
call super::destroy
destroy(this.cb_rubrica)
destroy(this.cb_corrispondenze)
destroy(this.cb_fornitori)
destroy(this.cb_note)
destroy(this.dw_for_pot_lista)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_folder_search)
destroy(this.gb_1)
destroy(this.st_1)
destroy(this.dw_ricerca)
destroy(this.cb_doc_comp)
destroy(this.dw_for_pot_det_4)
destroy(this.dw_for_pot_det_3)
destroy(this.dw_for_pot_det_5)
destroy(this.dw_nominativi)
destroy(this.dw_folder)
destroy(this.dw_col_din)
destroy(this.dw_for_pot_det_2)
destroy(this.dw_for_pot_det_1)
end on

type cb_rubrica from commandbutton within w_for_pot
integer x = 3154
integer y = 220
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Rubrica"
end type

event clicked;window_open_parm(w_for_pot_rubriche, -1, dw_for_pot_lista)

end event

type cb_corrispondenze from commandbutton within w_for_pot
integer x = 3154
integer y = 120
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_for_pot_corrispondenze, -1, dw_for_pot_lista)

end on

type cb_fornitori from commandbutton within w_for_pot
integer x = 3154
integer y = 320
integer width = 366
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Fornitori"
end type

event clicked;dw_for_pot_lista.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(), "cod_for_pot")
window_open(w_genera_fornitori, 0)

if not isnull(dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(), "cod_fornitore")) then
   cb_fornitori.enabled = false
   f_po_loaddddw_dw(dw_for_pot_det_5, &
                    "cod_fornitore", &
                    sqlca, &
                    "anag_fornitori", &
                    "cod_fornitore", &
                    "rag_soc_1", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if
end event

type cb_note from commandbutton within w_for_pot
integer x = 3154
integer y = 20
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_for_pot_note, -1, dw_for_pot_lista)

end on

type dw_for_pot_lista from uo_cs_xx_dw within w_for_pot
integer x = 137
integer y = 40
integer width = 1509
integer height = 500
integer taborder = 90
string dataobject = "d_for_pot_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_for_pot")) then
      cb_corrispondenze.enabled = true
      cb_rubrica.enabled = true
      cb_note.enabled = true
		cb_doc_comp.enabled = false
      if isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
         cb_fornitori.enabled = true
      else
         cb_fornitori.enabled = false
      end if
   else
      cb_corrispondenze.enabled = false
      cb_rubrica.enabled = false
      cb_fornitori.enabled = false
      cb_note.enabled = false
		cb_doc_comp.enabled = false
   end if
end if
end event

event updateend;call super::updateend;long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double		ld_sconto, ld_peso_val_servizio, ld_peso_val_qualita, ld_limite_tolleranza
string			ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_fornitore, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_cod_fornitore, ls_flag_certificato, ls_flag_approvato, ls_flag_strategico, ls_flag_terzista, &
				ls_num_contratto, ls_flag_ver_ispettiva, ls_note_ver_ispettiva, ls_flag_omologato, ls_flag_procedure_speciali, ls_flag_tipo_certificazione, &
				ls_cod_piano_campionamento
datetime		ldt_data_esenzione_iva, ldt_data_contratto, ldt_data_ver_ispettiva

dw_col_din.update()

ll_i = 0
do while ll_i <= dw_for_pot_lista.rowcount()
	ll_i = dw_for_pot_lista.getnextmodified(ll_i, Primary!)
	
	if ll_i = 0 then
		return
	end if
	
	ls_cod_fornitore = dw_for_pot_lista.getitemstring(ll_i, "cod_fornitore")
	
	if not isnull(ls_cod_fornitore) then
		ls_rag_soc_1 = dw_for_pot_lista.getitemstring(ll_i, "rag_soc_1")
		ls_rag_soc_2 = dw_for_pot_lista.getitemstring(ll_i, "rag_soc_2")
		ls_indirizzo = dw_for_pot_lista.getitemstring(ll_i, "indirizzo")
		ls_localita = dw_for_pot_lista.getitemstring(ll_i, "localita")
		ls_frazione = dw_for_pot_lista.getitemstring(ll_i, "frazione")
		ls_cap = dw_for_pot_lista.getitemstring(ll_i, "cap")
		ls_provincia = dw_for_pot_lista.getitemstring(ll_i, "provincia")
		ls_telefono = dw_for_pot_lista.getitemstring(ll_i, "telefono")
		ls_fax = dw_for_pot_lista.getitemstring(ll_i, "fax")
		ls_telex = dw_for_pot_lista.getitemstring(ll_i, "telex")
		ls_cod_fiscale = dw_for_pot_lista.getitemstring(ll_i, "cod_fiscale")
		ls_partita_iva = dw_for_pot_lista.getitemstring(ll_i, "partita_iva")
		ls_rif_interno = dw_for_pot_lista.getitemstring(ll_i, "rif_interno")
		ls_flag_tipo_fornitore = dw_for_pot_lista.getitemstring(ll_i, "flag_tipo_fornitore")
		ls_cod_conto = dw_for_pot_lista.getitemstring(ll_i, "cod_conto")
		ls_cod_iva = dw_for_pot_lista.getitemstring(ll_i, "cod_iva")
		ls_num_prot_esenzione_iva = dw_for_pot_lista.getitemstring(ll_i, "num_prot_esenzione_iva")
		ldt_data_esenzione_iva = dw_for_pot_lista.getitemdatetime(ll_i, "data_esenzione_iva")
		ls_flag_sospensione_iva = dw_for_pot_lista.getitemstring(ll_i, "flag_sospensione_iva")
		ls_cod_pagamento = dw_for_pot_lista.getitemstring(ll_i, "cod_pagamento")
		ll_giorno_fisso_scadenza = dw_for_pot_lista.getitemnumber(ll_i, "giorno_fisso_scadenza")
		ll_mese_esclusione_1 = dw_for_pot_lista.getitemnumber(ll_i, "mese_esclusione_1")
		ll_mese_esclusione_2 = dw_for_pot_lista.getitemnumber(ll_i, "mese_esclusione_2")
		ll_ggmm_esclusione_1_da = dw_for_pot_lista.getitemnumber(ll_i, "ggmm_esclusione_1_da")
		ll_ggmm_esclusione_1_a = dw_for_pot_lista.getitemnumber(ll_i, "ggmm_esclusione_1_a")
		ll_ggmm_esclusione_2_da = dw_for_pot_lista.getitemnumber(ll_i, "ggmm_esclusione_2_da")
		ll_ggmm_esclusione_2_a = dw_for_pot_lista.getitemnumber(ll_i, "ggmm_esclusione_2_a")
		ll_data_sostituzione_1 = dw_for_pot_lista.getitemnumber(ll_i, "data_sostituzione_1")
		ll_data_sostituzione_2 = dw_for_pot_lista.getitemnumber(ll_i, "data_sostituzione_2")
		ld_sconto = dw_for_pot_lista.getitemnumber(ll_i, "sconto")
		ls_cod_tipo_listino_prodotto = dw_for_pot_lista.getitemstring(ll_i, "cod_tipo_listino_prodotto")
		ll_fido = dw_for_pot_lista.getitemnumber(ll_i, "fido")
		ls_cod_banca_clien_for = dw_for_pot_lista.getitemstring(ll_i, "cod_banca_clien_for")
		ls_conto_corrente = dw_for_pot_lista.getitemstring(ll_i, "conto_corrente")
		ls_cod_lingua = dw_for_pot_lista.getitemstring(ll_i, "cod_lingua")
		ls_cod_nazione = dw_for_pot_lista.getitemstring(ll_i, "cod_nazione")
		ls_cod_area = dw_for_pot_lista.getitemstring(ll_i, "cod_area")
		ls_cod_zona = dw_for_pot_lista.getitemstring(ll_i, "cod_zona")
		ls_cod_valuta = dw_for_pot_lista.getitemstring(ll_i, "cod_valuta")
		ls_cod_categoria = dw_for_pot_lista.getitemstring(ll_i, "cod_categoria")
		ls_cod_imballo = dw_for_pot_lista.getitemstring(ll_i, "cod_imballo")
		ls_cod_porto = dw_for_pot_lista.getitemstring(ll_i, "cod_porto")
		ls_cod_resa = dw_for_pot_lista.getitemstring(ll_i, "cod_resa")
		ls_cod_mezzo = dw_for_pot_lista.getitemstring(ll_i, "cod_mezzo")
		ls_cod_vettore = dw_for_pot_lista.getitemstring(ll_i, "cod_vettore")
		ls_cod_inoltro = dw_for_pot_lista.getitemstring(ll_i, "cod_inoltro")
		ls_cod_deposito = dw_for_pot_lista.getitemstring(ll_i, "cod_deposito")
		ls_flag_certificato = dw_for_pot_lista.getitemstring(ll_i, "flag_certificato")
		ls_flag_approvato = dw_for_pot_lista.getitemstring(ll_i, "flag_approvato")
		ls_flag_strategico = dw_for_pot_lista.getitemstring(ll_i, "flag_strategico")
		ls_flag_terzista = dw_for_pot_lista.getitemstring(ll_i, "flag_terzista")
		ls_num_contratto = dw_for_pot_lista.getitemstring(ll_i, "num_contratto")
		ldt_data_contratto = dw_for_pot_lista.getitemdatetime(ll_i, "data_contratto")
		ls_flag_ver_ispettiva = dw_for_pot_lista.getitemstring(ll_i, "flag_ver_ispettiva")
		ldt_data_ver_ispettiva = dw_for_pot_lista.getitemdatetime(ll_i, "data_ver_ispettiva")  
		ls_note_ver_ispettiva = dw_for_pot_lista.getitemstring(ll_i, "note_ver_ispettiva")
		ls_flag_omologato = dw_for_pot_lista.getitemstring(ll_i, "flag_omologato")
		ls_flag_procedure_speciali = dw_for_pot_lista.getitemstring(ll_i, "flag_procedure_speciali")
		ld_peso_val_servizio = dw_for_pot_lista.getitemnumber(ll_i, "peso_val_servizio")
		ld_peso_val_qualita = dw_for_pot_lista.getitemnumber(ll_i, "peso_val_qualita")
		ld_limite_tolleranza = dw_for_pot_lista.getitemnumber(ll_i, "limite_tolleranza")
		ls_flag_tipo_certificazione = dw_for_pot_lista.getitemstring(ll_i, "flag_tipo_certificazione")
		ls_cod_piano_campionamento = dw_for_pot_lista.getitemstring(ll_i, "cod_piano_campionamento")

		update anag_fornitori
		set    rag_soc_1 = :ls_rag_soc_1,
				 rag_soc_2 = :ls_rag_soc_2,   
				 indirizzo = :ls_indirizzo,   
				 localita = :ls_localita,   
				 frazione = :ls_frazione,   
				 cap = :ls_cap,   
				 provincia = :ls_provincia,   
				 telefono = :ls_telefono,   
				 fax = :ls_fax,   
				 telex = :ls_telex,   
				 cod_fiscale = :ls_cod_fiscale,   
				 partita_iva = :ls_partita_iva,   
				 rif_interno = :ls_rif_interno,   
				 flag_tipo_fornitore = :ls_flag_tipo_fornitore,   
				 cod_conto = :ls_cod_conto,   
				 cod_iva = :ls_cod_iva,   
				 num_prot_esenzione_iva = :ls_num_prot_esenzione_iva,   
				 data_esenzione_iva = :ldt_data_esenzione_iva,   
				 flag_sospensione_iva = :ls_flag_sospensione_iva,   
				 cod_pagamento = :ls_cod_pagamento,   
				 giorno_fisso_scadenza = :ll_giorno_fisso_scadenza,   
				 mese_esclusione_1 = :ll_mese_esclusione_1,   
				 mese_esclusione_2 = :ll_mese_esclusione_2,
				 ggmm_esclusione_1_da =:ll_ggmm_esclusione_1_da,
				 ggmm_esclusione_1_a =:ll_ggmm_esclusione_1_a,
				 ggmm_esclusione_2_da =:ll_ggmm_esclusione_2_da,
				 ggmm_esclusione_2_a =:ll_ggmm_esclusione_2_a,
				 data_sostituzione_1 = :ll_data_sostituzione_1,   
				 data_sostituzione_2 = :ll_data_sostituzione_2,   
				 sconto = :ld_sconto,   
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto,   
				 fido = :ll_fido,   
				 cod_banca_clien_for = :ls_cod_banca_clien_for,   
				 conto_corrente = :ls_conto_corrente,
				 cod_lingua = :ls_cod_lingua,   
				 cod_nazione = :ls_cod_nazione,   
				 cod_area = :ls_cod_area,   
				 cod_zona = :ls_cod_zona,   
				 cod_valuta = :ls_cod_valuta,   
				 cod_categoria = :ls_cod_categoria,   
				 cod_imballo = :ls_cod_imballo,   
				 cod_porto = :ls_cod_porto,   
				 cod_resa = :ls_cod_resa,   
				 cod_mezzo = :ls_cod_mezzo,   
				 cod_vettore = :ls_cod_vettore,   
				 cod_inoltro = :ls_cod_inoltro,   
				 cod_deposito = :ls_cod_deposito,
				 flag_certificato = :ls_flag_certificato,
				 flag_approvato = :ls_flag_approvato,
				 flag_strategico = :ls_flag_strategico,
				 flag_terzista = :ls_flag_terzista,
				 num_contratto = :ls_num_contratto,
				 data_contratto = :ldt_data_contratto,
				 flag_ver_ispettiva = :ls_flag_ver_ispettiva,
				 data_ver_ispettiva = :ldt_data_ver_ispettiva,
				 note_ver_ispettiva = :ls_note_ver_ispettiva,
				 flag_omologato = :ls_flag_omologato,
				 flag_procedure_speciali = :ls_flag_procedure_speciali,
				 peso_val_servizio = :ld_peso_val_servizio,
				 peso_val_qualita = :ld_peso_val_qualita,
				 limite_tolleranza = :ld_limite_tolleranza,
				 flag_tipo_certificazione = :ls_flag_tipo_certificazione,
				 cod_piano_campionamento = :ls_cod_piano_campionamento
		where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
		
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento fornitori.", &
						  exclamation!, ok!)
			rollback;
			return
		end if
		
		commit;
	end if
loop

wf_conta_for_pot()
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_for_pot")) then
      cb_corrispondenze.enabled = true
      cb_rubrica.enabled = true
      cb_note.enabled = true
	cb_doc_comp.enabled = false
      if isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
         cb_fornitori.enabled = true
      else
         cb_fornitori.enabled = false
      end if
   else
      cb_corrispondenze.enabled = false
      cb_rubrica.enabled = false
      cb_fornitori.enabled = false
      cb_note.enabled = false
		cb_doc_comp.enabled = false
   end if
end if
end event

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_for_pot"
//   ls_codice = "cod_for_pot"
//   ls_flag_tipo = "flag_tipo_fornitore"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if

string ls_cod_fiscale, ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


if getrow() > 0 then
   f_upd_partita_iva("anag_for_pot", "cod_for_pot", "rag_soc_1")
   if this.getitemstring(i_rownbr, "flag_tipo_fornitore") <> "E" and &
      not isnull(this.getitemstring(i_rownbr, "cod_fiscale")) then
      ls_cod_fiscale = this.getitemstring(i_rownbr, "cod_fiscale")
      if not f_cod_fiscale(ls_cod_fiscale) then
        	g_mb.messagebox("Attenzione", "Inserire un codice fiscale valido!", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
	
	if isnull(getitemstring(getrow(),"cod_iva")) then
		if (dw_for_pot_det_2.getcolumnname() = "num_prot_esenzione_iva" and &
		      not isnull(dw_for_pot_det_2.gettext()) and len(dw_for_pot_det_2.gettext()) > 0) or &
		   (dw_for_pot_det_2.getcolumnname() <> "num_prot_esenzione_iva" and not isnull(this.getitemstring(getrow(), "num_prot_esenzione_iva"))) then
			g_mb.messagebox("APICE","Attenzione! Per indicare un protocollo esenzione è necessario aver indicato ~r~nanche un codice di esenzione.~r~nIl protocollo che è stato inserito e la relativa data verranno cancellati")
		end if
		this.setitem(getrow(), "num_prot_esenzione_iva", ls_null)
		this.setitem(getrow(), "data_esenzione_iva", ldt_null)
	end if

end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
	pcca.error = c_fatal
elseif ll_errore = 0 then
	dw_col_din.reset()
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   cb_corrispondenze.enabled = false
   cb_rubrica.enabled = false
   cb_fornitori.enabled = false
   cb_note.enabled = false
	cb_doc_comp.enabled = true

   ls_modify = "flag_tipo_certificazione.protect='0~tif(flag_certificato=~~'N~~',1,0)'~t"
   ls_modify = ls_modify + "flag_tipo_certificazione.background.color='16777215~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
   dw_for_pot_det_4.modify(ls_modify)
end if




end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   cb_corrispondenze.enabled = false
   cb_rubrica.enabled = false
   cb_fornitori.enabled = false
   cb_note.enabled = false
	cb_doc_comp.enabled = true

   ls_modify = "flag_tipo_certificazione.protect='0~tif(flag_certificato=~~'N~~',1,0)'~t"
   ls_modify = ls_modify + "flag_tipo_certificazione.background.color='16777215~tif(flag_certificato=~~'N~~',12632256,16777215)'~t"
   dw_for_pot_det_4.modify(ls_modify)
end if




end event

event rowfocuschanged;call super::rowfocuschanged;dw_nominativi.retrieve()

if i_extendmode then
   string ls_cod_for_pot

   if this.getrow() > 0 then
	
		dw_col_din.retrieve()
		
		ls_cod_for_pot = this.getitemstring(this.getrow(), "cod_for_pot")
		if isnull(ls_cod_for_pot) then
			ls_cod_for_pot = ""
		end if
		
		if isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
			cb_fornitori.enabled = true
		else
			cb_fornitori.enabled = false
		end if
	end if
end if
end event

event rowfocuschanging;call super::rowfocuschanging;dw_col_din.uof_verify_changes()
end event

event updatestart;call super::updatestart;dw_col_din.uof_delete()
end event

type cb_reset from commandbutton within w_for_pot
event clicked pbm_bnclicked
integer x = 2743
integer y = 40
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_for_pot
event clicked pbm_bnclicked
integer x = 2743
integer y = 140
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_for_pot_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type dw_folder_search from u_folder within w_for_pot
integer x = 23
integer y = 20
integer width = 3109
integer height = 560
integer taborder = 10
end type

type gb_1 from groupbox within w_for_pot
integer x = 3154
integer y = 420
integer width = 357
integer height = 160
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Nr.For.Pot."
end type

type st_1 from statictext within w_for_pot
integer x = 3177
integer y = 480
integer width = 315
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_ricerca from u_dw_search within w_for_pot
integer x = 137
integer y = 40
integer width = 2583
integer height = 520
integer taborder = 20
string dataobject = "d_for_pot_search"
end type

type cb_doc_comp from cb_documenti_compilati within w_for_pot
event clicked pbm_bnclicked
integer x = 2720
integer y = 1380
integer width = 69
integer height = 80
integer taborder = 2
boolean bringtotop = true
integer weight = 400
fontcharset fontcharset = ansi!
string text = "..."
end type

event clicked;call super::clicked;long ll_riga[]
string ls_ritorno, ls_cod_fornitore, ls_temp

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "nome_doc_qualificazione"
s_cs_xx.parametri.parametro_s_2 = "DQ7"
s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
ls_temp = s_cs_xx.parametri.parametro_s_10 
if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
   ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)

   if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
		ls_cod_fornitore = dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(),"cod_fornitore")
		update anag_for_pot
		set    nome_doc_qualificazione = :ls_ritorno
		where  anag_for_pot.cod_azienda        = :s_cs_xx.cod_azienda and
		       anag_for_pot.cod_fornitore = :ls_cod_fornitore;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
		end if
   end if      
else
   f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
end if
ll_riga[1] = dw_for_pot_lista.getrow()
dw_for_pot_lista.triggerevent("pcd_retrieve")
dw_for_pot_lista.Set_Selected_Rows(1, ll_riga, &
                                        c_CheckForChanges, &
                                        c_RefreshChildren, &
                                        c_RefreshView)
end event

type dw_for_pot_det_4 from uo_cs_xx_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 2789
integer height = 1080
integer taborder = 50
string dataobject = "d_for_pot_det_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
   case "flag_certificato"
      this.setitem(this.getrow(), "flag_tipo_certificazione", "N")
   case "flag_omologato"
      if i_coltext = 'S' then
			string ls_ritorno

			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "nome_doc_qualificazione"
			s_cs_xx.parametri.parametro_s_2 = "DQ7"
			s_cs_xx.parametri.parametro_s_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1)
			if ( isnull(s_cs_xx.parametri.parametro_s_10) ) or ( len(s_cs_xx.parametri.parametro_s_10) < 1 ) then
				ls_ritorno = f_apri_doc_compilato(s_cs_xx.parametri.parametro_s_2)
			
				if ls_ritorno <> "ERRORE" and not isnull(ls_ritorno) then
					this.setitem(this.getrow(), "nome_doc_qualificazione", ls_ritorno)			
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("OMNIA","Si è verificato un errore: il documento potrebbe non essere registrato",Information!)
					end if
				end if      
			else
				f_vedi_doc_compilato(s_cs_xx.parametri.parametro_s_10)
			end if
		end if

end choose
end event

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_fornitori"
//   ls_codice = "cod_fornitore"
//   ls_flag_tipo = "flag_tipo_fornitore"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_for_pot_det_3 from uo_cs_xx_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 1989
integer height = 1080
integer taborder = 60
string dataobject = "d_for_pot_det_3"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_for_pot"
//   ls_codice = "cod_for_pot"
//   ls_flag_tipo = "flag_tipo_fornitore"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_for_pot_det_5 from uo_cs_xx_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 2949
integer height = 320
integer taborder = 40
string dataobject = "d_for_pot_det_5"
end type

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_for_pot"
//   ls_codice = "cod_for_pot"
//   ls_flag_tipo = "flag_tipo_fornitore"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_nominativi from uo_dw_nominativi within w_for_pot
integer x = 46
integer y = 700
integer width = 3003
integer height = 1080
integer taborder = 100
boolean border = false
end type

type dw_folder from u_folder within w_for_pot
integer x = 23
integer y = 600
integer width = 3109
integer height = 1200
integer taborder = 70
end type

type dw_col_din from uo_colonne_dinamiche_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 2789
integer height = 1080
integer taborder = 90
end type

type dw_for_pot_det_2 from uo_cs_xx_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 2857
integer height = 900
integer taborder = 80
string dataobject = "d_for_pot_det_2"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_for_pot"
//   ls_codice = "cod_for_pot"
//   ls_flag_tipo = "flag_tipo_fornitore"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_for_pot_det_1 from uo_cs_xx_dw within w_for_pot
integer x = 46
integer y = 700
integer width = 2789
integer height = 1060
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_for_pot_det_1"
boolean border = false
end type

event doubleclicked;call super::doubleclicked;string ls_messaggio, ls_destinatari[], ls_allegati[],ls_errore
uo_outlook luo_outlook

choose case dwo.name
	case "casella_mail"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "casella_mail")
		if luo_outlook.uof_invio_outlook(0, "M", "", "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
		
end choose
end event

