﻿$PBExportHeader$w_for_pot_note.srw
$PBExportComments$Finestra Gestione Fornitori Potenziali Note
forward
global type w_for_pot_note from w_cs_xx_principale
end type
type cb_note from commandbutton within w_for_pot_note
end type
type dw_for_pot_note_lista from uo_cs_xx_dw within w_for_pot_note
end type
type dw_for_pot_note_det from uo_cs_xx_dw within w_for_pot_note
end type
end forward

global type w_for_pot_note from w_cs_xx_principale
integer width = 2615
integer height = 1840
string title = "Gestione Note Fornitori Potenziali"
cb_note cb_note
dw_for_pot_note_lista dw_for_pot_note_lista
dw_for_pot_note_det dw_for_pot_note_det
end type
global w_for_pot_note w_for_pot_note

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_for_pot_note_lista.set_dw_key("cod_azienda")
dw_for_pot_note_lista.set_dw_key("cod_for_pot")
dw_for_pot_note_lista.set_dw_options(sqlca, &
                                     i_openparm, &
                                     c_scrollparent, &
                                     c_default)
dw_for_pot_note_det.set_dw_options(sqlca, &
                                   dw_for_pot_note_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_for_pot_note_lista
end on

on w_for_pot_note.create
int iCurrent
call super::create
this.cb_note=create cb_note
this.dw_for_pot_note_lista=create dw_for_pot_note_lista
this.dw_for_pot_note_det=create dw_for_pot_note_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_note
this.Control[iCurrent+2]=this.dw_for_pot_note_lista
this.Control[iCurrent+3]=this.dw_for_pot_note_det
end on

on w_for_pot_note.destroy
call super::destroy
destroy(this.cb_note)
destroy(this.dw_for_pot_note_lista)
destroy(this.dw_for_pot_note_det)
end on

type cb_note from commandbutton within w_for_pot_note
integer x = 2149
integer y = 1640
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_db, ls_doc
integer li_i, li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

string ls_cod_for_pot, ls_cod_nota
int li_row

li_row = dw_for_pot_note_lista.getrow()
if li_row < 1 then return

setnull(lbl_null)
ls_cod_for_pot = dw_for_pot_note_lista.getitemstring(li_row, "cod_for_pot")
ls_cod_nota = dw_for_pot_note_lista.getitemstring(li_row, "cod_nota")

if isnull(ls_cod_for_pot) or ls_cod_for_pot = "" then return


select prog_mimetype
into :ll_prog_mimetype
from anag_for_pot_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_for_pot = :ls_cod_for_pot and
	cod_nota = :ls_cod_nota;
	
selectblob blob
into :lbl_blob
from anag_for_pot_note
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_for_pot = :ls_cod_for_pot and
	cod_nota = :ls_cod_nota;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update anag_for_pot_note
		set blob = :lbl_blob
		where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_for_pot = :ls_cod_for_pot and
		cod_nota = :ls_cod_nota;
	else
		updateblob anag_for_pot_note
		set blob = :lbl_blob
		where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_for_pot = :ls_cod_for_pot and
		cod_nota = :ls_cod_nota;
			
		update anag_for_pot_note
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_for_pot = :ls_cod_for_pot and
			cod_nota = :ls_cod_nota;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
	
end if
end event

type dw_for_pot_note_lista from uo_cs_xx_dw within w_for_pot_note
integer x = 23
integer y = 20
integer width = 2537
integer height = 500
integer taborder = 10
string dataobject = "d_for_pot_note_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_for_pot


ls_cod_for_pot = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_for_pot")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_for_pot)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_for_pot

ls_cod_for_pot = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_for_pot")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_for_pot")) then
      this.setitem(ll_i, "cod_for_pot", ls_cod_for_pot)
   end if
next
end on

event pcd_new;call super::pcd_new;cb_note.enabled = false
end event

event pcd_save;call super::pcd_save;cb_note.enabled = true
end event

event pcd_view;call super::pcd_view;cb_note.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_note.enabled = false
end event

type dw_for_pot_note_det from uo_cs_xx_dw within w_for_pot_note
integer x = 23
integer y = 540
integer width = 2537
integer height = 1080
integer taborder = 20
string dataobject = "d_for_pot_note_det"
borderstyle borderstyle = styleraised!
end type

