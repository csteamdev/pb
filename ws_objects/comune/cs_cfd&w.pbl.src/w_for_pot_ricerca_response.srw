﻿$PBExportHeader$w_for_pot_ricerca_response.srw
$PBExportComments$Finestra Ricerca Fornitori Potenziali
forward
global type w_for_pot_ricerca_response from w_cs_xx_risposta
end type
type st_1 from statictext within w_for_pot_ricerca_response
end type
type dw_find from u_dw_find within w_for_pot_ricerca_response
end type
type cb_ok from commandbutton within w_for_pot_ricerca_response
end type
type cb_codice from uo_cb_ok within w_for_pot_ricerca_response
end type
type cb_rag_soc_1 from uo_cb_ok within w_for_pot_ricerca_response
end type
type dw_for_pot_lista from uo_cs_xx_dw within w_for_pot_ricerca_response
end type
type dw_folder from u_folder within w_for_pot_ricerca_response
end type
end forward

global type w_for_pot_ricerca_response from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 1906
integer height = 1468
string title = "Ricerca Fornitori Potenziali"
st_1 st_1
dw_find dw_find
cb_ok cb_ok
cb_codice cb_codice
cb_rag_soc_1 cb_rag_soc_1
dw_for_pot_lista dw_for_pot_lista
dw_folder dw_folder
end type
global w_for_pot_ricerca_response w_for_pot_ricerca_response

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


dw_for_pot_lista.set_dw_key("cod_azienda")
dw_for_pot_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_retrieveasneeded + c_selectonrowfocuschange, &
                                c_default)

dw_for_pot_lista.setsort("cod_for_pot A")
dw_find.fu_wiredw(dw_for_pot_lista, "rag_soc_1")

lw_oggetti[1] = dw_for_pot_lista
lw_oggetti[2] = st_1
lw_oggetti[3] = dw_find
lw_oggetti[4] = cb_codice
lw_oggetti[5] = cb_rag_soc_1
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(1, 4)
dw_folder.fu_selecttab(1)

dw_for_pot_lista.setfocus()
cb_rag_soc_1.postevent("clicked")
end event

on w_for_pot_ricerca_response.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_find=create dw_find
this.cb_ok=create cb_ok
this.cb_codice=create cb_codice
this.cb_rag_soc_1=create cb_rag_soc_1
this.dw_for_pot_lista=create dw_for_pot_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_find
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.cb_codice
this.Control[iCurrent+5]=this.cb_rag_soc_1
this.Control[iCurrent+6]=this.dw_for_pot_lista
this.Control[iCurrent+7]=this.dw_folder
end on

on w_for_pot_ricerca_response.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_find)
destroy(this.cb_ok)
destroy(this.cb_codice)
destroy(this.cb_rag_soc_1)
destroy(this.dw_for_pot_lista)
destroy(this.dw_folder)
end on

type st_1 from statictext within w_for_pot_ricerca_response
integer x = 23
integer y = 20
integer width = 754
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Ragione Sociale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_find from u_dw_find within w_for_pot_ricerca_response
integer x = 800
integer y = 20
integer width = 1051
integer height = 80
integer taborder = 20
end type

type cb_ok from commandbutton within w_for_pot_ricerca_response
integer x = 1486
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(),"cod_for_pot"))
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(),"cod_for_pot"))
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
else
	s_cs_xx.parametri.parametro_s_10 = dw_for_pot_lista.getitemstring(dw_for_pot_lista.getrow(),"cod_for_pot")
end if
close(parent)

end event

type cb_codice from uo_cb_ok within w_for_pot_ricerca_response
integer x = 46
integer y = 220
integer width = 343
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Codice"
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_for_pot_lista.setsort("cod_for_pot A")
dw_for_pot_lista.sort()

dw_find.fu_wiredw(dw_for_pot_lista, "cod_cliente")

st_1.text = "Ricerca per Codice:"
dw_find.setfocus()



end event

type cb_rag_soc_1 from uo_cb_ok within w_for_pot_ricerca_response
integer x = 389
integer y = 220
integer width = 1051
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ragione Sociale"
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_for_pot_lista.setsort("rag_soc_1 A")
dw_for_pot_lista.sort()

dw_find.fu_wiredw(dw_for_pot_lista, "rag_soc_1")

st_1.text = "Ricerca per Ragione Sociale:"
dw_find.setfocus()
end event

type dw_for_pot_lista from uo_cs_xx_dw within w_for_pot_ricerca_response
integer x = 46
integer y = 220
integer width = 1783
integer height = 1000
integer taborder = 50
string dataobject = "d_for_pot_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   cb_ok.postevent(clicked!)
end if
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_folder from u_folder within w_for_pot_ricerca_response
integer x = 23
integer y = 120
integer width = 1829
integer height = 1120
integer taborder = 10
end type

