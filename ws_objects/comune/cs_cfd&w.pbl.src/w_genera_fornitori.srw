﻿$PBExportHeader$w_genera_fornitori.srw
$PBExportComments$Finestra Generazione Fornitori
forward
global type w_genera_fornitori from w_cs_xx_risposta
end type
type st_1 from statictext within w_genera_fornitori
end type
type cb_1 from uo_cb_close within w_genera_fornitori
end type
type em_cod_fornitore from editmask within w_genera_fornitori
end type
type cb_genera from uo_cb_ok within w_genera_fornitori
end type
type ddlb_fornitori from dropdownlistbox within w_genera_fornitori
end type
type st_clienti_old from statictext within w_genera_fornitori
end type
end forward

global type w_genera_fornitori from w_cs_xx_risposta
int Width=1089
int Height=425
boolean TitleBar=true
string Title="Generazione Fornitori"
st_1 st_1
cb_1 cb_1
em_cod_fornitore em_cod_fornitore
cb_genera cb_genera
ddlb_fornitori ddlb_fornitori
st_clienti_old st_clienti_old
end type
global w_genera_fornitori w_genera_fornitori

event pc_accept;call super::pc_accept;long ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, &
     ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido
double ld_sconto, ld_peso_val_servizio, ld_peso_val_qualita, ld_limite_tolleranza
string ls_rag_soc_1_old, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
       ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
       ls_rif_interno, ls_flag_tipo_fornitore, ls_cod_conto, ls_cod_iva, &
       ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
       ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, &
       ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
       ls_cod_valuta, ls_cod_categoria, ls_cod_imballo, &
       ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
       ls_cod_deposito, ls_cod_for_pot, &
       ls_cod_fornitore, ls_flag_certificato, ls_flag_approvato, ls_flag_strategico, &
       ls_flag_terzista, ls_flag_ver_ispettiva, ls_note_ver_ispettiva, ls_flag_omologato, &
       ls_flag_procedure_speciali, ls_num_contratto, ls_flag_tipo_certificazione, &
       ls_cod_piano_campionamento, ls_casella_mail, &
		 ls_nome_doc_qualificazione 
datetime ldt_data_esenzione_iva, ldt_data_contratto, ldt_data_ver_ispettiva


ls_cod_fornitore = trim(em_cod_fornitore.text)

if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then
  	g_mb.messagebox("Attenzione", "Inserire un codice fornitore.", &
              exclamation!, ok!)
   em_cod_fornitore.setfocus()
	return
end if

select anag_fornitori.rag_soc_1
into   :ls_rag_soc_1_old
from   anag_fornitori
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_fornitori.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 100 then
  	g_mb.messagebox("Attenzione", "Codice fornitore già presente in anagrafica: " + ls_rag_soc_1_old, &
              exclamation!, ok!)
   em_cod_fornitore.setfocus()
	return
end if

ll_i = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
ls_cod_for_pot = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_for_pot")
ls_rag_soc_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_1")
ls_rag_soc_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_2")
ls_indirizzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "indirizzo")
ls_localita = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "localita")
ls_frazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "frazione")
ls_cap = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cap")
ls_provincia = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "provincia")
ls_telefono = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telefono")
ls_fax = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "fax")
ls_telex = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telex")
ls_cod_fiscale = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_fiscale")
ls_partita_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "partita_iva")
ls_rif_interno = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rif_interno")
ls_flag_tipo_fornitore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_tipo_fornitore")
ls_cod_conto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_conto")
ls_cod_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_iva")
ls_num_prot_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "num_prot_esenzione_iva")
ldt_data_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_esenzione_iva")
ls_flag_sospensione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_sospensione_iva")
ls_cod_pagamento = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_pagamento")
ll_giorno_fisso_scadenza = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "giorno_fisso_scadenza")
ll_mese_esclusione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_1")
ll_mese_esclusione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_2")
ll_data_sostituzione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_1")
ll_data_sostituzione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_2")
ld_sconto = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "sconto")
ls_cod_tipo_listino_prodotto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_tipo_listino_prodotto")
ll_fido = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "fido")
ls_cod_banca_clien_for = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_banca_clien_for")
ls_conto_corrente = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "conto_corrente")
ls_cod_lingua = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_lingua")
ls_cod_nazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_nazione")
ls_cod_area = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_area")
ls_cod_zona = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_zona")
ls_cod_valuta = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_valuta")
ls_cod_categoria = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_categoria")
ls_cod_imballo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_imballo")
ls_cod_porto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_porto")
ls_cod_resa = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_resa")
ls_cod_mezzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_mezzo")
ls_cod_vettore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_vettore")
ls_cod_inoltro = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_inoltro")
ls_cod_deposito = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_deposito")
ls_flag_certificato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_certificato")
ls_flag_approvato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_approvato")
ls_flag_strategico = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_strategico")
ls_flag_terzista = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_terzista")
ls_num_contratto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "num_contratto")
ldt_data_contratto = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_contratto")
ls_flag_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_ver_ispettiva")
ldt_data_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_ver_ispettiva")  
ls_note_ver_ispettiva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "note_ver_ispettiva")
ls_flag_omologato = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_omologato")
ls_flag_procedure_speciali = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_procedure_speciali")
ld_peso_val_servizio = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "peso_val_servizio")
ld_peso_val_qualita = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "peso_val_qualita")
ld_limite_tolleranza = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "limite_tolleranza")
ls_flag_tipo_certificazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_tipo_certificazione")
ls_cod_piano_campionamento = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_piano_campionamento")
ls_casella_mail = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "casella_mail")
ls_nome_doc_qualificazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "nome_doc_qualificazione")

insert into anag_fornitori  
            (cod_azienda,   
             cod_fornitore,   
             rag_soc_1,   
             rag_soc_2,   
             indirizzo,   
             localita,   
             frazione,   
             cap,   
             provincia,   
             telefono,   
             fax,   
             telex,   
             cod_fiscale,   
             partita_iva,   
             rif_interno,   
             flag_tipo_fornitore,   
             cod_conto,   
             cod_iva,   
             num_prot_esenzione_iva,   
             data_esenzione_iva,   
             flag_sospensione_iva,   
             cod_pagamento,   
             giorno_fisso_scadenza,   
             mese_esclusione_1,   
             mese_esclusione_2,   
             data_sostituzione_1,   
             data_sostituzione_2,   
             sconto,   
             cod_tipo_listino_prodotto,   
             fido,   
             cod_banca_clien_for,   
             conto_corrente,   
             cod_lingua,   
             cod_nazione,   
             cod_area,   
             cod_zona,   
             cod_valuta,   
             cod_categoria,   
             cod_imballo,   
             cod_porto,   
             cod_resa,   
             cod_mezzo,   
             cod_vettore,   
             cod_inoltro,   
             cod_deposito,   
             flag_certificato,   
             flag_approvato,   
             flag_strategico,   
             flag_terzista,   
             num_contratto,   
             data_contratto,   
             flag_ver_ispettiva,   
             data_ver_ispettiva,   
             note_ver_ispettiva,   
             flag_omologato,   
             flag_procedure_speciali,   
             peso_val_servizio,   
             peso_val_qualita,   
             limite_tolleranza,   
             flag_blocco,
             data_blocco,
             flag_tipo_certificazione,
             cod_piano_campionamento,
				 casella_mail,
				 nome_doc_qualificazione)  
values      (:s_cs_xx.cod_azienda,
             :ls_cod_fornitore,
             :ls_rag_soc_1,   
             :ls_rag_soc_2,   
             :ls_indirizzo,   
             :ls_localita,   
             :ls_frazione,   
             :ls_cap,   
             :ls_provincia,   
             :ls_telefono,   
             :ls_fax,   
             :ls_telex,   
             :ls_cod_fiscale,   
             :ls_partita_iva,   
             :ls_rif_interno,   
             :ls_flag_tipo_fornitore,   
             :ls_cod_conto,   
             :ls_cod_iva,   
             :ls_num_prot_esenzione_iva,   
             :ldt_data_esenzione_iva,   
             :ls_flag_sospensione_iva,   
             :ls_cod_pagamento,   
             :ll_giorno_fisso_scadenza,   
             :ll_mese_esclusione_1,   
             :ll_mese_esclusione_2,   
             :ll_data_sostituzione_1,   
             :ll_data_sostituzione_2,   
             :ld_sconto,   
             :ls_cod_tipo_listino_prodotto,   
             :ll_fido,   
             :ls_cod_banca_clien_for,   
             :ls_conto_corrente,   
             :ls_cod_lingua,   
             :ls_cod_nazione,   
             :ls_cod_area,   
             :ls_cod_zona,   
             :ls_cod_valuta,   
             :ls_cod_categoria,   
             :ls_cod_imballo,   
             :ls_cod_porto,   
             :ls_cod_resa,   
             :ls_cod_mezzo,   
             :ls_cod_vettore,   
             :ls_cod_inoltro,   
             :ls_cod_deposito,   
             :ls_flag_certificato,   
             :ls_flag_approvato,   
             :ls_flag_strategico,   
             :ls_flag_terzista,   
             :ls_num_contratto,   
             :ldt_data_contratto,   
             :ls_flag_ver_ispettiva,   
             :ldt_data_ver_ispettiva,   
             :ls_note_ver_ispettiva,   
             :ls_flag_omologato,   
             :ls_flag_procedure_speciali,   
             :ld_peso_val_servizio,   
             :ld_peso_val_qualita,   
             :ld_limite_tolleranza,   
             'N',   
             null,
             :ls_flag_tipo_certificazione,
             :ls_cod_piano_campionamento,
				 :ls_casella_mail,
				 :ls_nome_doc_qualificazione);

if sqlca.sqlcode <> 0 then
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione fornitore.", &
              exclamation!, ok!)
   rollback;
	return
end if

update anag_for_pot  
set    anag_for_pot.cod_fornitore = :ls_cod_fornitore
where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_for_pot.cod_for_pot = :ls_cod_for_pot;

if sqlca.sqlcode = 0 then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(ll_i, "cod_fornitore", ls_cod_fornitore)   
   s_cs_xx.parametri.parametro_uo_dw_1.setitemstatus(ll_i, "cod_fornitore", primary!, notmodified!)
   commit;
else
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione fornitore.", &
              exclamation!, ok!)
   rollback;
	return
end if

close(this)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_fornitori, &
              sqlca, &
              "anag_fornitori", &
              "cod_fornitore", &
              "cod_fornitore", &
              "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
              "")
end event

on w_genera_fornitori.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.cb_1=create cb_1
this.em_cod_fornitore=create em_cod_fornitore
this.cb_genera=create cb_genera
this.ddlb_fornitori=create ddlb_fornitori
this.st_clienti_old=create st_clienti_old
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=em_cod_fornitore
this.Control[iCurrent+4]=cb_genera
this.Control[iCurrent+5]=ddlb_fornitori
this.Control[iCurrent+6]=st_clienti_old
end on

on w_genera_fornitori.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.em_cod_fornitore)
destroy(this.cb_genera)
destroy(this.ddlb_fornitori)
destroy(this.st_clienti_old)
end on

type st_1 from statictext within w_genera_fornitori
int X=23
int Y=121
int Width=641
int Height=61
boolean Enabled=false
string Text="Codice Nuovo Fornitori:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from uo_cb_close within w_genera_fornitori
int X=138
int Y=221
int Width=366
int Height=81
int TabOrder=40
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_cod_fornitore from editmask within w_genera_fornitori
int X=686
int Y=121
int Width=343
int Height=81
int TabOrder=30
Alignment Alignment=Center!
string Mask="!!!!!!"
MaskDataType MaskDataType=StringMask!
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_genera from uo_cb_ok within w_genera_fornitori
int X=526
int Y=221
int Width=366
int Height=81
int TabOrder=10
string Text="&Genera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_fornitori from dropdownlistbox within w_genera_fornitori
int X=686
int Y=21
int Width=343
int Height=501
int TabOrder=20
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_clienti_old from statictext within w_genera_fornitori
int X=23
int Y=21
int Width=654
int Height=61
boolean Enabled=false
string Text="Codici Fornitori Esistenti:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

