﻿$PBExportHeader$w_tes_trat_for.srw
$PBExportComments$Finestra Gestione Testata Trattative Fornitori
forward
global type w_tes_trat_for from w_cs_xx_principale
end type
type dw_tes_trat_for_lista from uo_cs_xx_dw within w_tes_trat_for
end type
type cb_corrispondenze from commandbutton within w_tes_trat_for
end type
type cb_dettagli from commandbutton within w_tes_trat_for
end type
type cb_note_esterne from commandbutton within w_tes_trat_for
end type
type cb_note from commandbutton within w_tes_trat_for
end type
type cb_genera_offerta from commandbutton within w_tes_trat_for
end type
type dw_tes_trat_for_det from uo_cs_xx_dw within w_tes_trat_for
end type
end forward

global type w_tes_trat_for from w_cs_xx_principale
integer width = 2894
integer height = 1744
string title = "Gestione Testata Trattative Fornitori"
dw_tes_trat_for_lista dw_tes_trat_for_lista
cb_corrispondenze cb_corrispondenze
cb_dettagli cb_dettagli
cb_note_esterne cb_note_esterne
cb_note cb_note
cb_genera_offerta cb_genera_offerta
dw_tes_trat_for_det dw_tes_trat_for_det
end type
global w_tes_trat_for w_tes_trat_for

on pc_delete;call w_cs_xx_principale::pc_delete;cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note_esterne.enabled = false
cb_note.enabled = false
cb_genera_offerta.enabled = false
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tes_trat_for_lista.set_dw_key("cod_azienda")
dw_tes_trat_for_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tes_trat_for_det.set_dw_options(sqlca, &
                                   dw_tes_trat_for_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)

iuo_dw_main=dw_tes_trat_for_lista

cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note_esterne.enabled = false
cb_note.enabled = false
cb_genera_offerta.enabled = false
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_trat_for_det, &
                 "cod_tipo_trattativa", &
                 sqlca, &
                 "tab_tipi_trattative", &
                 "cod_tipo_trattativa", &
                 "des_tipo_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_trattativa = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trat_for_det, &
                 "cod_for_pot", &
                 sqlca, &
                 "anag_for_pot", &
                 "cod_for_pot", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trat_for_det, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trat_for_det, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_trat_for_det, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_tes_trat_for.create
int iCurrent
call super::create
this.dw_tes_trat_for_lista=create dw_tes_trat_for_lista
this.cb_corrispondenze=create cb_corrispondenze
this.cb_dettagli=create cb_dettagli
this.cb_note_esterne=create cb_note_esterne
this.cb_note=create cb_note
this.cb_genera_offerta=create cb_genera_offerta
this.dw_tes_trat_for_det=create dw_tes_trat_for_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_trat_for_lista
this.Control[iCurrent+2]=this.cb_corrispondenze
this.Control[iCurrent+3]=this.cb_dettagli
this.Control[iCurrent+4]=this.cb_note_esterne
this.Control[iCurrent+5]=this.cb_note
this.Control[iCurrent+6]=this.cb_genera_offerta
this.Control[iCurrent+7]=this.dw_tes_trat_for_det
end on

on w_tes_trat_for.destroy
call super::destroy
destroy(this.dw_tes_trat_for_lista)
destroy(this.cb_corrispondenze)
destroy(this.cb_dettagli)
destroy(this.cb_note_esterne)
destroy(this.cb_note)
destroy(this.cb_genera_offerta)
destroy(this.dw_tes_trat_for_det)
end on

event pc_new;call super::pc_new;dw_tes_trat_for_det.setitem(dw_tes_trat_for_det.getrow(), "data_registrazione", datetime(today()))

end event

type dw_tes_trat_for_lista from uo_cs_xx_dw within w_tes_trat_for
integer x = 23
integer y = 20
integer width = 2423
integer height = 500
integer taborder = 20
string dataobject = "d_tes_trat_for_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_genera_offerta.enabled = false
   end if
end if
end on

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_trattativa


   select con_for_pot.cod_tipo_trattativa
   into   :ls_cod_tipo_trattativa
   from   con_for_pot
   where  con_for_pot.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_tipo_trattativa", ls_cod_tipo_trattativa)
   end if

   	cb_dettagli.enabled = false
   	cb_corrispondenze.enabled = false
   	cb_note_esterne.enabled = false
  	 cb_note.enabled = false
  	 cb_genera_offerta.enabled = false
	dw_tes_trat_for_det.object.b_ricerca_for_pot.enabled = true
	dw_tes_trat_for_det.object.b_ricerca_fornitore.enabled = true
end if
end event

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note_esterne.enabled = true
      cb_note.enabled = true
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note_esterne.enabled = false
      cb_note.enabled = false
      cb_genera_offerta.enabled = false
   end if
end if
end on

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note_esterne.enabled = true
      cb_note.enabled = true
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note_esterne.enabled = false
      cb_note.enabled = false
      cb_genera_offerta.enabled = false
   end if
	dw_tes_trat_for_det.object.b_ricerca_for_pot.enabled = false
dw_tes_trat_for_det.object.b_ricerca_fornitore.enabled = false
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then

      select parametri_azienda.numero
      into   :ll_anno_registrazione
      from   parametri_azienda
      where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
             parametri_azienda.flag_parametro = 'N' and &
             parametri_azienda.cod_parametro = 'ESC';
      if sqlca.sqlcode = 0 then
         this.setitem(this.getrow(), "anno_trattativa", int(ll_anno_registrazione))
      end if
      select con_for_pot.num_registrazione
      into   :ll_num_registrazione
      from   con_for_pot
      where  con_for_pot.cod_azienda = :s_cs_xx.cod_azienda;
      if sqlca.sqlcode = 0 then
         this.setitem(this.getrow(), "num_trattativa", ll_num_registrazione + 1)
      end if
      update con_for_pot
      set    con_for_pot.num_registrazione = :ll_num_registrazione + 1
      where  con_for_pot.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   cb_dettagli.enabled = false
   cb_corrispondenze.enabled = false
   cb_note_esterne.enabled = false
   cb_note.enabled = false
   cb_genera_offerta.enabled = false
dw_tes_trat_for_det.object.b_ricerca_for_pot.enabled = true
dw_tes_trat_for_det.object.b_ricerca_fornitore.enabled = true
end if
end event

type cb_corrispondenze from commandbutton within w_tes_trat_for
integer x = 2469
integer y = 220
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_tes_trat_for_corrispondenze, -1, dw_tes_trat_for_lista)

end on

type cb_dettagli from commandbutton within w_tes_trat_for
integer x = 2469
integer y = 120
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

on clicked;window_open_parm(w_det_trat_for, -1, dw_tes_trat_for_lista)

end on

type cb_note_esterne from commandbutton within w_tes_trat_for
integer x = 2469
integer y = 320
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "D&ocumento"
end type

event clicked;string ls_db, ls_doc
integer li_i, li_risposta
long ll_anno_trattativa, ll_num_trattativa, ll_prog_mimetype

transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

li_i = dw_tes_trat_for_lista.getrow()
ll_anno_trattativa = dw_tes_trat_for_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_tes_trat_for_lista.getitemnumber(li_i, "num_trattativa")

select prog_mimetype
into :ll_prog_mimetype
from tes_trat_for
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_trattativa and 
	num_trattativa = :ll_num_trattativa;
	
selectblob note_esterne
into :lbl_blob
from tes_trat_for
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_trattativa and 
	num_trattativa = :ll_num_trattativa;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tes_trat_for
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_trattativa = :ll_anno_trattativa and 
			num_trattativa = :ll_num_trattativa;
	else
		updateblob tes_trat_for
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_trattativa = :ll_anno_trattativa and 
			num_trattativa = :ll_num_trattativa;
			
		update tes_trat_for
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_trattativa = :ll_anno_trattativa and 
			num_trattativa = :ll_num_trattativa;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	li_risposta = f_crea_sqlcb(sqlcb)
//
//	selectblob tes_trat_for.note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_trat_for
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_trattativa = :ll_anno_trattativa and 
//	           num_trattativa = :ll_num_trattativa
//	using      sqlcb;
//
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tes_trat_for.note_esterne
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tes_trat_for
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           anno_trattativa = :ll_anno_trattativa and 
//	           num_trattativa = :ll_num_trattativa;
//
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//
//	   updateblob tes_trat_for
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              anno_trattativa = :ll_anno_trattativa and 
//	              num_trattativa = :ll_num_trattativa
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tes_trat_for
//	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              anno_trattativa = :ll_anno_trattativa and 
//	              num_trattativa = :ll_num_trattativa;
//					  
//	end if
//	
//   commit;
//end if
end event

type cb_note from commandbutton within w_tes_trat_for
integer x = 2469
integer y = 20
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_tes_trat_for_note, -1, dw_tes_trat_for_lista)

end on

type cb_genera_offerta from commandbutton within w_tes_trat_for
integer x = 2469
integer y = 420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Offerta"
end type

event clicked;long ll_anno_trattativa, ll_num_trattativa, ll_i
string ls_cod_for_pot, ls_cod_centro_costo, ls_cod_tipo_trattativa, ls_flag_blocco
datetime ldt_data_scadenza, ldt_data_blocco


ll_i = dw_tes_trat_for_lista.getrow()
ls_cod_for_pot = dw_tes_trat_for_lista.getitemstring(ll_i, "cod_for_pot")
ll_anno_trattativa = dw_tes_trat_for_lista.getitemnumber(ll_i, "anno_trattativa")
ll_num_trattativa = dw_tes_trat_for_lista.getitemnumber(ll_i, "num_trattativa")
ldt_data_scadenza = dw_tes_trat_for_lista.getitemdatetime(ll_i, "data_scadenza")
ls_cod_centro_costo = dw_tes_trat_for_lista.getitemstring(ll_i, "cod_centro_costo")
ls_cod_tipo_trattativa = dw_tes_trat_for_lista.getitemstring(ll_i, "cod_tipo_trattativa")
ls_flag_blocco = dw_tes_trat_for_det.getitemstring(ll_i, "flag_blocco")
ldt_data_blocco = dw_tes_trat_for_det.getitemdatetime(ll_i, "data_blocco")

dw_tes_trat_for_det.change_dw_current( )

if ls_flag_blocco = 'S' and (ldt_data_blocco <= datetime(today()) or isnull(ldt_data_blocco)) then
  	g_mb.messagebox("Attenzione", "Trattativa bloccata.", &
              exclamation!, ok!)
   return
else
   f_genera_off_acq(ls_cod_for_pot, ll_anno_trattativa, ll_num_trattativa, ldt_data_scadenza, ls_cod_centro_costo, ls_cod_tipo_trattativa)
end if

if not isnull(dw_tes_trat_for_det.getitemnumber(ll_i, "anno_registrazione")) then
   cb_genera_offerta.enabled=false
end if
end event

type dw_tes_trat_for_det from uo_cs_xx_dw within w_tes_trat_for
integer x = 23
integer y = 540
integer width = 2811
integer height = 1080
integer taborder = 40
string dataobject = "d_tes_trat_for_det"
borderstyle borderstyle = styleraised!
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   string ls_cod_operaio, ls_cod_fornitore, ls_cod_for_pot, &
          ls_null

   setnull(ls_null)

   choose case i_colname
      case "cod_for_pot"
         select anag_for_pot.cod_operaio,
                anag_for_pot.cod_fornitore
         into   :ls_cod_operaio,    
                :ls_cod_fornitore
         from   anag_for_pot
         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_for_pot.cod_for_pot = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
            this.setitem(this.getrow(), "cod_fornitore", ls_cod_fornitore)
         else
            this.setitem(i_rownbr, "cod_operaio", ls_null)
            this.setitem(this.getrow(), "cod_fornitore", ls_null)
         end if
      case "cod_fornitore"
         select anag_for_pot.cod_for_pot, 
                anag_for_pot.cod_operaio
         into   :ls_cod_for_pot,
                :ls_cod_operaio
         from   anag_for_pot
         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_for_pot.cod_fornitore = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_for_pot", ls_cod_for_pot)
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
         else
            this.setitem(i_rownbr, "cod_for_pot", ls_null)
            this.setitem(i_rownbr, "cod_operaio", ls_null)
         end if
   end choose
end if
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_trat_for_det,"cod_fornitore")
	case "b_ricerca_for_pot"
		guo_ricerca.uof_ricerca_fornitore_potenziale(dw_tes_trat_for_det,"cod_for_pot")
end choose
end event

event getfocus;call super::getfocus;dw_tes_trat_for_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

