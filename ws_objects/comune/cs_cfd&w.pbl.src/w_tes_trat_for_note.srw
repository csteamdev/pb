﻿$PBExportHeader$w_tes_trat_for_note.srw
$PBExportComments$Finestra Gestione Testata Trattative Fornitori Note
forward
global type w_tes_trat_for_note from w_cs_xx_principale
end type
type dw_tes_trat_for_note_lista from uo_cs_xx_dw within w_tes_trat_for_note
end type
type dw_tes_trat_for_note_det from uo_cs_xx_dw within w_tes_trat_for_note
end type
end forward

global type w_tes_trat_for_note from w_cs_xx_principale
int Width=2615
int Height=1769
boolean TitleBar=true
string Title="Gestione Note Testata Trattative Fornitori"
dw_tes_trat_for_note_lista dw_tes_trat_for_note_lista
dw_tes_trat_for_note_det dw_tes_trat_for_note_det
end type
global w_tes_trat_for_note w_tes_trat_for_note

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tes_trat_for_note_lista.set_dw_key("cod_azienda")
dw_tes_trat_for_note_lista.set_dw_key("anno_trattativa")
dw_tes_trat_for_note_lista.set_dw_key("num_trattativa")
dw_tes_trat_for_note_lista.set_dw_options(sqlca, &
                                          i_openparm, &
                                          c_scrollparent, &
                                          c_default)
dw_tes_trat_for_note_det.set_dw_options(sqlca, &
                                        dw_tes_trat_for_note_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)
iuo_dw_main = dw_tes_trat_for_note_lista
end on

on w_tes_trat_for_note.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_trat_for_note_lista=create dw_tes_trat_for_note_lista
this.dw_tes_trat_for_note_det=create dw_tes_trat_for_note_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_trat_for_note_lista
this.Control[iCurrent+2]=dw_tes_trat_for_note_det
end on

on w_tes_trat_for_note.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_trat_for_note_lista)
destroy(this.dw_tes_trat_for_note_det)
end on

type dw_tes_trat_for_note_lista from uo_cs_xx_dw within w_tes_trat_for_note
int X=23
int Y=21
int Width=2538
int Height=501
int TabOrder=10
string DataObject="d_tes_trat_for_note_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_trattativa, ll_num_trattativa

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then
      this.setitem(ll_i, "anno_trattativa", ll_anno_trattativa)
   end if
   if this.getitemnumber(ll_i, "num_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "num_trattativa")) then
      this.setitem(ll_i, "num_trattativa", ll_num_trattativa)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_trattativa, ll_num_trattativa

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_trattativa, ll_num_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

type dw_tes_trat_for_note_det from uo_cs_xx_dw within w_tes_trat_for_note
int X=23
int Y=541
int Width=2538
int Height=1101
int TabOrder=20
string DataObject="d_tes_trat_for_note_det"
BorderStyle BorderStyle=StyleRaised!
end type

