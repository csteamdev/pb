﻿$PBExportHeader$w_det_liste_controllo.srw
$PBExportComments$Finestra Dettaglio Liste Controllo
forward
global type w_det_liste_controllo from w_cs_xx_principale
end type
type dw_det_liste_controllo_det_1 from uo_cs_xx_dw within w_det_liste_controllo
end type
type tab_dettaglio from tab within w_det_liste_controllo
end type
type tbp_dati_generici from userobject within tab_dettaglio
end type
type tbp_dati_generici from userobject within tab_dettaglio
end type
type tbp_tipologia from userobject within tab_dettaglio
end type
type tbp_tipologia from userobject within tab_dettaglio
end type
type tab_dettaglio from tab within w_det_liste_controllo
tbp_dati_generici tbp_dati_generici
tbp_tipologia tbp_tipologia
end type
type dw_det_liste_controllo_lista from uo_cs_xx_dw within w_det_liste_controllo
end type
type dw_det_liste_controllo_det_3 from uo_cs_xx_dw within w_det_liste_controllo
end type
type dw_det_liste_controllo_det_2 from uo_cs_xx_dw within w_det_liste_controllo
end type
type dw_det_liste_controllo_det_4 from uo_cs_xx_dw within w_det_liste_controllo
end type
type dw_det_liste_controllo_det_5 from uo_cs_xx_dw within w_det_liste_controllo
end type
end forward

global type w_det_liste_controllo from w_cs_xx_principale
integer width = 2377
integer height = 1704
string title = "Dettaglio Liste di Controllo"
dw_det_liste_controllo_det_1 dw_det_liste_controllo_det_1
tab_dettaglio tab_dettaglio
dw_det_liste_controllo_lista dw_det_liste_controllo_lista
dw_det_liste_controllo_det_3 dw_det_liste_controllo_det_3
dw_det_liste_controllo_det_2 dw_det_liste_controllo_det_2
dw_det_liste_controllo_det_4 dw_det_liste_controllo_det_4
dw_det_liste_controllo_det_5 dw_det_liste_controllo_det_5
end type
global w_det_liste_controllo w_det_liste_controllo

type variables

end variables

forward prototypes
public function integer wf_proteggi_campi (string ws_tipo_campo)
public function integer wf_calcola_progressivo ()
public function integer wf_gestisci_dw (string fs_tipo_campo)
end prototypes

public function integer wf_proteggi_campi (string ws_tipo_campo);//choose case ws_tipo_campo
//	case "T"
//		dw_det_liste_controllo_det_2.modify("check_button.protect=0")
//		dw_det_liste_controllo_det_2.modify("des_check_1.protect=0")
//		dw_det_liste_controllo_det_2.modify("peso_check_1.protect=0")
//		dw_det_liste_controllo_det_2.modify("des_check_2.protect=0")
//		dw_det_liste_controllo_det_2.modify("peso_check_2.protect=0")
//		
//		dw_det_liste_controllo_det_3.modify("option_button.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_1.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_1.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_2.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_2.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_3.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_3.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_4.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_4.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_5.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_5.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_libero.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_libero.protect=0")
//		dw_det_liste_controllo_det_3.modify("num_opzioni.protect=0")
//		
//		dw_det_liste_controllo_det_1.modify("campo_libero.protect=0")
//		dw_det_liste_controllo_det_1.modify("des_subtotale.protect=0")
//		
//	case "L"
//		dw_det_liste_controllo_det_2.modify("check_button.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_2.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_2.protect=1")
//		
//		dw_det_liste_controllo_det_3.modify("option_button.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("num_opzioni.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("campo_libero.protect=0")
//		
//		dw_det_liste_controllo_det_1.modify("des_subtotale.protect=1")
//		
//	case "C"
//		dw_det_liste_controllo_det_2.modify("check_button.protect=0")
//		dw_det_liste_controllo_det_2.modify("des_check_1.protect=0")
//		dw_det_liste_controllo_det_2.modify("peso_check_1.protect=0")
//		dw_det_liste_controllo_det_2.modify("des_check_2.protect=0")
//		dw_det_liste_controllo_det_2.modify("peso_check_2.protect=0")
//		
//		dw_det_liste_controllo_det_3.modify("option_button.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("num_opzioni.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("campo_libero.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("des_subtotale.protect=1")
//		
//	case "O"
//		dw_det_liste_controllo_det_2.modify("check_button.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_2.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_2.protect=1")
//		
//		dw_det_liste_controllo_det_3.modify("option_button.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_1.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_1.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_2.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_2.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_3.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_3.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_4.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_4.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_5.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_5.protect=0")
//		dw_det_liste_controllo_det_3.modify("des_option_libero.protect=0")
//		dw_det_liste_controllo_det_3.modify("peso_option_libero.protect=0")
//		dw_det_liste_controllo_det_3.modify("num_opzioni.protect=0")
//		
//		dw_det_liste_controllo_det_1.modify("campo_libero.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("des_subtotale.protect=1")
//		
//	case "S"
//		dw_det_liste_controllo_det_2.modify("check_button.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_1.protect=1")
//		dw_det_liste_controllo_det_2.modify("des_check_2.protect=1")
//		dw_det_liste_controllo_det_2.modify("peso_check_2.protect=1")
//		
//		dw_det_liste_controllo_det_3.modify("option_button.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_1.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_2.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_3.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_4.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_5.protect=1")
//		dw_det_liste_controllo_det_3.modify("des_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("peso_option_libero.protect=1")
//		dw_det_liste_controllo_det_3.modify("num_opzioni.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("campo_libero.protect=1")
//		
//		dw_det_liste_controllo_det_1.modify("des_subtotale.protect=0")
//
//end choose
return 0

end function

public function integer wf_calcola_progressivo ();long ll_num_registrazione, ll_num_reg, ll_versione, ll_edizione

ll_num_reg = dw_det_liste_controllo_lista.i_parentdw.getitemnumber(dw_det_liste_controllo_lista.i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_versione = dw_det_liste_controllo_lista.i_parentdw.getitemnumber(dw_det_liste_controllo_lista.i_parentdw.i_selectedrows[1], "num_versione")
ll_edizione = dw_det_liste_controllo_lista.i_parentdw.getitemnumber(dw_det_liste_controllo_lista.i_parentdw.i_selectedrows[1], "num_edizione")

select max(det_liste_controllo.prog_riga_liste_contr)
  into :ll_num_registrazione
  from det_liste_controllo
  where (det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda) and (det_liste_controllo.num_reg_lista = :ll_num_reg) and (det_liste_controllo.num_versione = :ll_versione) and (det_liste_controllo.num_edizione = :ll_edizione)  ;

if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
    dw_det_liste_controllo_lista.SetItem(dw_det_liste_controllo_lista.GetRow(),"prog_riga_liste_contr", 10 )
else
   if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
      ll_num_registrazione = 10
   else
      ll_num_registrazione = ll_num_registrazione + 10
   end if
   dw_det_liste_controllo_lista.SetItem(dw_det_liste_controllo_lista.GetRow(),"prog_riga_liste_contr", ll_num_registrazione)
end if
return 0
end function

public function integer wf_gestisci_dw (string fs_tipo_campo);choose case fs_tipo_campo
	case "T" //tutti				
		
		dw_det_liste_controllo_det_2.visible = false
		dw_det_liste_controllo_det_3.visible = false
		dw_det_liste_controllo_det_4.visible = false
		dw_det_liste_controllo_det_5.visible = false
				
		
	case "L" //campo libero --------------------------------------------------------------------------------
		
		if tab_dettaglio.selectedtab = 2 then
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = true
			dw_det_liste_controllo_det_5.visible = false
		else
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		end if
		tab_dettaglio.tbp_tipologia.text = "Campo Libero"
		
	
	case "C" //stile si/no --------------------------------------------------------------------------------
		if tab_dettaglio.selectedtab = 2 then
			dw_det_liste_controllo_det_2.visible = true
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		else
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		end if
		tab_dettaglio.tbp_tipologia.text = "Stile Si/No"
		
	
	case "O" //stile opzione --------------------------------------------------------------------------------
		if tab_dettaglio.selectedtab = 2 then
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = true
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		else
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		end if
		tab_dettaglio.tbp_tipologia.text = "Stile Opzione"
		
	  
	case "S" //subtotale --------------------------------------------------------------------------------
		if tab_dettaglio.selectedtab = 2 then
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = true
		else
			dw_det_liste_controllo_det_2.visible = false
			dw_det_liste_controllo_det_3.visible = false
			dw_det_liste_controllo_det_4.visible = false
			dw_det_liste_controllo_det_5.visible = false
		end if
		tab_dettaglio.tbp_tipologia.text = "Subtotale"
  		
		
end choose

return 0

end function

event pc_setwindow;call super::pc_setwindow;//windowobject l_objects[ ]
//
//l_objects[1] = dw_det_liste_controllo_det_1
//dw_folder.fu_AssignTab(1, "&Dati Generici", l_Objects[])
//l_objects[1] = dw_det_liste_controllo_det_2
//dw_folder.fu_AssignTab(2, "&Stile Si No", l_Objects[])
//l_objects[1] = dw_det_liste_controllo_det_3
//dw_folder.fu_AssignTab(3, "Stile &Opzione", l_Objects[])
//
//dw_folder.fu_FolderCreate(3,4)

dw_det_liste_controllo_lista.set_dw_key("cod_azienda")
dw_det_liste_controllo_lista.set_dw_key("num_reg_lista")
dw_det_liste_controllo_lista.set_dw_key("num_versione")
dw_det_liste_controllo_lista.set_dw_key("num_edizione")

dw_det_liste_controllo_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)

dw_det_liste_controllo_det_1.set_dw_options(sqlca,dw_det_liste_controllo_lista,c_sharedata+c_scrollparent,c_default)
dw_det_liste_controllo_det_2.set_dw_options(sqlca,dw_det_liste_controllo_lista,c_sharedata+c_scrollparent,c_default)
dw_det_liste_controllo_det_3.set_dw_options(sqlca,dw_det_liste_controllo_lista,c_sharedata+c_scrollparent,c_default)
dw_det_liste_controllo_det_4.set_dw_options(sqlca,dw_det_liste_controllo_lista,c_sharedata+c_scrollparent,c_default)
dw_det_liste_controllo_det_5.set_dw_options(sqlca,dw_det_liste_controllo_lista,c_sharedata+c_scrollparent,c_default)

dw_det_liste_controllo_det_2.visible = true
dw_det_liste_controllo_det_3.visible = false
dw_det_liste_controllo_det_4.visible = false
dw_det_liste_controllo_det_5.visible = false

//dw_det_liste_controllo_det_1.ib_proteggi_chiavi = false
uo_dw_main = dw_det_liste_controllo_lista
//dw_folder.fu_SelectTab(1)













end event

on pc_new;call w_cs_xx_principale::pc_new;//ib_in_new = true
end on

on w_det_liste_controllo.create
int iCurrent
call super::create
this.dw_det_liste_controllo_det_1=create dw_det_liste_controllo_det_1
this.tab_dettaglio=create tab_dettaglio
this.dw_det_liste_controllo_lista=create dw_det_liste_controllo_lista
this.dw_det_liste_controllo_det_3=create dw_det_liste_controllo_det_3
this.dw_det_liste_controllo_det_2=create dw_det_liste_controllo_det_2
this.dw_det_liste_controllo_det_4=create dw_det_liste_controllo_det_4
this.dw_det_liste_controllo_det_5=create dw_det_liste_controllo_det_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_liste_controllo_det_1
this.Control[iCurrent+2]=this.tab_dettaglio
this.Control[iCurrent+3]=this.dw_det_liste_controllo_lista
this.Control[iCurrent+4]=this.dw_det_liste_controllo_det_3
this.Control[iCurrent+5]=this.dw_det_liste_controllo_det_2
this.Control[iCurrent+6]=this.dw_det_liste_controllo_det_4
this.Control[iCurrent+7]=this.dw_det_liste_controllo_det_5
end on

on w_det_liste_controllo.destroy
call super::destroy
destroy(this.dw_det_liste_controllo_det_1)
destroy(this.tab_dettaglio)
destroy(this.dw_det_liste_controllo_lista)
destroy(this.dw_det_liste_controllo_det_3)
destroy(this.dw_det_liste_controllo_det_2)
destroy(this.dw_det_liste_controllo_det_4)
destroy(this.dw_det_liste_controllo_det_5)
end on

type dw_det_liste_controllo_det_1 from uo_cs_xx_dw within w_det_liste_controllo
integer x = 64
integer y = 660
integer width = 2194
integer height = 900
integer taborder = 50
string dataobject = "d_det_liste_controllo_det_1"
boolean border = false
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;if i_extendmode then 
   wf_proteggi_campi(i_coltext)
end if

end on

on pcd_view;call uo_cs_xx_dw::pcd_view;//if getrow() > 0 then wf_nascondi_campi(getitemstring(getrow(),"flag_tipo_risposta"), dw_det_liste_controllo_det)

end on

event itemchanged;call super::itemchanged;
choose case i_colname
	case "flag_tipo_risposta"
		wf_gestisci_dw(i_coltext)
		
end choose
end event

type tab_dettaglio from tab within w_det_liste_controllo
integer x = 27
integer y = 556
integer width = 2286
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tbp_dati_generici tbp_dati_generici
tbp_tipologia tbp_tipologia
end type

on tab_dettaglio.create
this.tbp_dati_generici=create tbp_dati_generici
this.tbp_tipologia=create tbp_tipologia
this.Control[]={this.tbp_dati_generici,&
this.tbp_tipologia}
end on

on tab_dettaglio.destroy
destroy(this.tbp_dati_generici)
destroy(this.tbp_tipologia)
end on

event selectionchanged;long ll_row
string ls_flag_tipo_risposta


if newindex = 1 then
	dw_det_liste_controllo_det_1.visible = true
	dw_det_liste_controllo_det_2.visible = false
	dw_det_liste_controllo_det_3.visible = false
	dw_det_liste_controllo_det_4.visible = false
	dw_det_liste_controllo_det_5.visible = false
else
	dw_det_liste_controllo_det_1.visible = false
	
	ll_row = dw_det_liste_controllo_lista.getrow()

	if ll_row > 0 then
		ls_flag_tipo_risposta = dw_det_liste_controllo_lista.getitemstring(ll_row, "flag_tipo_risposta")
	else
		ls_flag_tipo_risposta = "T"
	end if
	
	wf_gestisci_dw(ls_flag_tipo_risposta)
end if
end event

type tbp_dati_generici from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 2249
integer height = -20
long backcolor = 12632256
string text = "Dati Generici"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tbp_tipologia from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 2249
integer height = -20
long backcolor = 12632256
string text = "Stile Si/No"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type dw_det_liste_controllo_lista from uo_cs_xx_dw within w_det_liste_controllo
integer x = 23
integer y = 20
integer width = 2286
integer height = 500
integer taborder = 20
string dataobject = "d_det_liste_controllo_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;if isnull(this.getitemstring(this.getrow(), "des_domanda")) or len(this.getitemstring(this.getrow(), "des_domanda")) < 1 then
   g_mb.messagebox("Dettaglio Lista di Controllo","L'indicazione della domanda è obbligatorio", StopSign!)
   pcca.error = c_valfailed
   return
end if

end event

event pcd_new;call super::pcd_new;//long ll_num_registrazione, ll_num_reg, ll_versione, ll_edizione
//
//ll_num_reg = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
//ll_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
//ll_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
//
//select max(det_liste_controllo.prog_riga_liste_contr)
//  into :ll_num_registrazione
//  from det_liste_controllo
//  where (det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda) and (det_liste_controllo.num_reg_lista = :ll_num_reg) and (det_liste_controllo.num_versione = :ll_versione) and (det_liste_controllo.num_edizione = :ll_edizione)  ;
//
//if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
//    SetItem(GetRow(),"prog_riga_liste_contr", 10 )
//else
//   if isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
//      ll_num_registrazione = 10
//   else
//      ll_num_registrazione = ll_num_registrazione + 10
//   end if
//   dw_det_liste_controllo_det_1.SetItem(GetRow(),"prog_riga_liste_contr", ll_num_registrazione)
//end if
//
//
wf_calcola_progressivo()
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx, ll_num_reg, ll_versione, ll_edizione

ll_num_reg = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")
ll_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      dw_det_liste_controllo_lista.SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_reg_lista"))  or GetItemnumber(l_Idx, "num_reg_lista") < 1 THEN
      dw_det_liste_controllo_lista.SetItem(l_Idx, "num_reg_lista", ll_num_reg)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_versione")) or GetItemnumber(l_Idx, "num_versione") < 1 THEN
      dw_det_liste_controllo_lista.SetItem(l_Idx, "num_versione", ll_versione)
   END IF
   IF IsNull(GetItemnumber(l_Idx, "num_edizione")) or GetItemnumber(l_Idx, "num_edizione") < 1 THEN
      dw_det_liste_controllo_lista.SetItem(l_Idx, "num_edizione", ll_edizione)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_num_reg, ll_edizione, ll_versione


ll_num_reg  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_reg_lista")
ll_edizione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_edizione")
ll_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_versione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_num_reg, ll_versione, ll_edizione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

on updateend;call uo_cs_xx_dw::updateend;wf_proteggi_campi("T")
end on

event rowfocuschanged;call super::rowfocuschanged;long ll_row
string ls_flag_tipo_risposta

ll_row = getrow()

if ll_row > 0 then
	ls_flag_tipo_risposta = getitemstring(ll_row, "flag_tipo_risposta")
else
	ls_flag_tipo_risposta = "T"
end if

wf_gestisci_dw(ls_flag_tipo_risposta)
end event

type dw_det_liste_controllo_det_3 from uo_cs_xx_dw within w_det_liste_controllo
integer x = 64
integer y = 660
integer width = 2194
integer height = 900
integer taborder = 70
string dataobject = "d_det_liste_controllo_det_3"
boolean border = false
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_det_liste_controllo_det_2 from uo_cs_xx_dw within w_det_liste_controllo
integer x = 64
integer y = 660
integer width = 2194
integer height = 900
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_det_liste_controllo_det_2"
boolean border = false
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_det_liste_controllo_det_4 from uo_cs_xx_dw within w_det_liste_controllo
integer x = 64
integer y = 660
integer width = 2194
integer height = 900
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_det_liste_controllo_det_4"
boolean border = false
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

type dw_det_liste_controllo_det_5 from uo_cs_xx_dw within w_det_liste_controllo
integer x = 64
integer y = 660
integer width = 2194
integer height = 900
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_det_liste_controllo_det_5"
boolean border = false
end type

event pcd_new;call super::pcd_new;wf_calcola_progressivo()
end event

