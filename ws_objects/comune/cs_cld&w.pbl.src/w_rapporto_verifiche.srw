﻿$PBExportHeader$w_rapporto_verifiche.srw
$PBExportComments$Finestra RESPONSE Rapporto Veriche su Liste Controllo
forward
global type w_rapporto_verifiche from w_cs_xx_risposta
end type
type mle_1 from multilineedit within w_rapporto_verifiche
end type
type cb_1 from uo_cb_close within w_rapporto_verifiche
end type
end forward

global type w_rapporto_verifiche from w_cs_xx_risposta
int Width=2602
int Height=741
boolean TitleBar=true
string Title="Risultati Verifiche Effettuate su Liste di Controllo"
mle_1 mle_1
cb_1 cb_1
end type
global w_rapporto_verifiche w_rapporto_verifiche

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;mle_1.text = s_cs_xx.parametri.parametro_s_1
end on

on w_rapporto_verifiche.create
int iCurrent
call w_cs_xx_risposta::create
this.mle_1=create mle_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=mle_1
this.Control[iCurrent+2]=cb_1
end on

on w_rapporto_verifiche.destroy
call w_cs_xx_risposta::destroy
destroy(this.mle_1)
destroy(this.cb_1)
end on

type mle_1 from multilineedit within w_rapporto_verifiche
int X=23
int Y=21
int Width=2515
int Height=501
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean AutoHScroll=true
boolean AutoVScroll=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from uo_cb_close within w_rapporto_verifiche
int X=2172
int Y=541
int Width=366
int Height=81
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

