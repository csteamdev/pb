﻿$PBExportHeader$w_tes_liste_cont_comp.srw
$PBExportComments$Finestra Testata Liste di Controllo Compilate
forward
global type w_tes_liste_cont_comp from w_cs_xx_principale
end type
type cb_ricerca from commandbutton within w_tes_liste_cont_comp
end type
type cb_annulla from commandbutton within w_tes_liste_cont_comp
end type
type cb_report from commandbutton within w_tes_liste_cont_comp
end type
type cb_compila from commandbutton within w_tes_liste_cont_comp
end type
type dw_det_1 from uo_cs_xx_dw within w_tes_liste_cont_comp
end type
type dw_ricerca from u_dw_search within w_tes_liste_cont_comp
end type
type dw_folder from u_folder within w_tes_liste_cont_comp
end type
type dw_lista from uo_cs_xx_dw within w_tes_liste_cont_comp
end type
type dw_folder_ricerca from u_folder within w_tes_liste_cont_comp
end type
type dw_det_2 from uo_cs_xx_dw within w_tes_liste_cont_comp
end type
end forward

global type w_tes_liste_cont_comp from w_cs_xx_principale
integer width = 2574
integer height = 2600
string title = "*Compilazione Liste di Controllo"
cb_ricerca cb_ricerca
cb_annulla cb_annulla
cb_report cb_report
cb_compila cb_compila
dw_det_1 dw_det_1
dw_ricerca dw_ricerca
dw_folder dw_folder
dw_lista dw_lista
dw_folder_ricerca dw_folder_ricerca
dw_det_2 dw_det_2
end type
global w_tes_liste_cont_comp w_tes_liste_cont_comp

type variables
boolean ib_in_new=FALSE, ib_in_del=FALSE
string is_sql_sintax
end variables

on w_tes_liste_cont_comp.create
int iCurrent
call super::create
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_compila=create cb_compila
this.dw_det_1=create dw_det_1
this.dw_ricerca=create dw_ricerca
this.dw_folder=create dw_folder
this.dw_lista=create dw_lista
this.dw_folder_ricerca=create dw_folder_ricerca
this.dw_det_2=create dw_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_compila
this.Control[iCurrent+5]=this.dw_det_1
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_lista
this.Control[iCurrent+9]=this.dw_folder_ricerca
this.Control[iCurrent+10]=this.dw_det_2
end on

on w_tes_liste_cont_comp.destroy
call super::destroy
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_compila)
destroy(this.dw_det_1)
destroy(this.dw_ricerca)
destroy(this.dw_folder)
destroy(this.dw_lista)
destroy(this.dw_folder_ricerca)
destroy(this.dw_det_2)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )

f_PO_LoadDDDW_DW(dw_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ricerca,"rd_num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
					  
f_PO_LoadDDDW_DW(dw_ricerca,"rs_compilato_da",sqlca,&
                 "utenti","cod_utente","nome_cognome",&
                 "utenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject		lw_oggetti[], l_objects[]

//preparazione folder del dettaglio
lw_oggetti[1] = dw_det_2
dw_folder.fu_assigntab(2, "&Note e Azioni", lw_oggetti[])

lw_oggetti[1] = dw_det_1
dw_folder.fu_assigntab(1, "&Attributi", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)
//------


//set dw option per lista-dettagli
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_noretrieveonopen, &
                                  c_default)
dw_det_1.set_dw_options(sqlca, &
                                 dw_lista, &
                                 c_sharedata + c_scrollparent, &
                                 c_default)
dw_det_2.set_dw_options(sqlca, &
                                 dw_lista, &
                                 c_sharedata + c_scrollparent, &
                                 c_default)

iuo_dw_main=dw_lista
//----------------------------------

//preparazione folder ricerca-lista
lw_oggetti[1] = dw_lista
dw_folder_ricerca.fu_assigntab(1, "L.", lw_oggetti[])

lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
//lw_oggetti[4] = cb_ricerca_fornitore
//lw_oggetti[5] = cb_ricerca_cliente
dw_folder_ricerca.fu_assigntab(2, "R.", lw_oggetti[])

dw_folder_ricerca.fu_foldercreate(2,2)
dw_folder_ricerca.fu_selecttab(2)
dw_lista.change_dw_current()
//-----------------------------------

end event

type cb_ricerca from commandbutton within w_tes_liste_cont_comp
integer x = 1723
integer y = 740
integer width = 329
integer height = 88
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;//dw_ricerca.fu_BuildSearch(TRUE)
//dw_folder_ricerca.fu_SelectTab(1)
//dw_lista.change_dw_current()
//parent.triggerevent("pc_retrieve")

dw_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_folder_ricerca.fu_selecttab(1)

end event

type cb_annulla from commandbutton within w_tes_liste_cont_comp
integer x = 2071
integer y = 740
integer width = 329
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;decimal ld_null
string ls_null
datetime ldt_datetime, ldt_null

setnull(ld_null)
setnull(ls_null)
setnull(ldt_null)

dw_ricerca.setitem(1, "rd_num_reg_lista", ld_null)

ldt_datetime = datetime(relativedate(today(), 30), time("00:00:00"))
dw_ricerca.setitem(1, "rdt_data_da", ldt_datetime)

ldt_datetime = datetime(today(), time("00:00:00"))
dw_ricerca.setitem(1, "rdt_data_a", ldt_datetime)

dw_ricerca.setitem(1, "rs_compilato_da", ls_null)
dw_ricerca.setitem(1, "rs_cod_fornitore", ls_null)
dw_ricerca.setitem(1, "rs_cod_cliente", ls_null)
dw_ricerca.setitem(1, "rs_cod_area_aziendale", ls_null)
dw_ricerca.setitem(1, "cf_compilato_da", ls_null)
end event

type cb_report from commandbutton within w_tes_liste_cont_comp
integer x = 1728
integer y = 2388
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if not isvalid(w_report_lista_comp) then
   if dw_lista.i_numselected > 0 then
      window_open_parm(w_report_lista_comp, -1, dw_lista)
   end if
end if
end event

type cb_compila from commandbutton within w_tes_liste_cont_comp
integer x = 2117
integer y = 2388
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Compila"
end type

event clicked;long ll_current_row[], l_error, ll_row
string ls_compilato_da
datetime ldt_oggi
decimal ld_num_reg_lista, ld_prog_liste_con_comp

if not isvalid(w_det_liste_con_comp) then
	if dw_lista.i_numselected > 0 then
		
		//metti la data e la firma del compilatore
		ll_row = dw_lista.getrow()
		
		if  ll_row > 0 then
			ls_compilato_da = dw_lista.getitemstring(ll_row, "compilato_da")
			if ls_compilato_da = "" or isnull(ls_compilato_da) then
				ld_num_reg_lista = dw_lista.getitemdecimal(ll_row, "num_reg_lista")
				ld_prog_liste_con_comp = dw_lista.getitemdecimal(ll_row, "prog_liste_con_comp" )
				
				ldt_oggi = datetime(today(), now())
				
				update tes_liste_con_comp
					set 	compilato_da = :s_cs_xx.cod_utente,
							data_registrazione = :ldt_oggi
				where cod_azienda = :s_cs_xx.cod_azienda and
						num_reg_lista = :ld_num_reg_lista  and prog_liste_con_comp = :ld_prog_liste_con_comp;
						
				if sqlca.sqlcode = 0 then
					commit;
				else
					g_mb.messagebox("OMNIA", "Errore durante l'aggiornamento del nome del compilatore!", Exclamation!)
					rollback;
				end if
			end if
		end if
		
		window_open_parm(w_det_liste_con_comp, 0, dw_lista)
		
		ll_current_row[1] = dw_lista.i_selectedrows[1]
		parent.triggerevent("pc_retrieve")
		dw_lista.Set_Selected_Rows(1, ll_current_row[], c_IgnoreChanges, &	
			  																					c_RefreshChildren, c_RefreshSame)
	end if
end if
end event

type dw_det_1 from uo_cs_xx_dw within w_tes_liste_cont_comp
integer x = 55
integer y = 1060
integer width = 2309
integer height = 1292
integer taborder = 30
string dataobject = "d_tes_liste_contr_comp_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;decimal ld_num_reg_lista

if i_extendmode then

string ls_des_lista, ls_cod_area_aziendale, ls_cod_resp_divisione, ls_approvato_da, ls_autorizzato_da
string   ls_validato_da, ls_flag_uso, ls_flag_recuperato, ls_note
long ll_num_versione,ll_num_edizione, ll_valido_per, ll_progressivo, ll_valore_riferimento
datetime ldt_emesso_il, ldt_autorizzato_il

ld_num_reg_lista = dec(data)

   if i_colname="num_reg_lista" then
        SELECT tes_liste_controllo.num_versione,   
         tes_liste_controllo.num_edizione,   
         tes_liste_controllo.des_lista,   
         tes_liste_controllo.cod_area_aziendale,   
         tes_liste_controllo.cod_resp_divisione,   
         tes_liste_controllo.approvato_da,   
         tes_liste_controllo.emesso_il,   
         tes_liste_controllo.autorizzato_da,   
         tes_liste_controllo.autorizzato_il,   
         tes_liste_controllo.valido_per,   
         tes_liste_controllo.validato_da,   
         tes_liste_controllo.flag_uso,   
         tes_liste_controllo.flag_recuperato,
         tes_liste_controllo.note,
         tes_liste_controllo.valore_riferimento
    INTO :ll_num_versione,   
         :ll_num_edizione,   
         :ls_des_lista,   
         :ls_cod_area_aziendale,   
         :ls_cod_resp_divisione,   
         :ls_approvato_da,   
         :ldt_emesso_il,   
         :ls_autorizzato_da,   
         :ldt_autorizzato_il,   
         :ll_valido_per,   
         :ls_validato_da,   
         :ls_flag_uso,   
         :ls_flag_recuperato,
         :ls_note,
         :ll_valore_riferimento 
    FROM tes_liste_controllo  
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( tes_liste_controllo.num_reg_lista = :ld_num_reg_lista ) AND  
         ( tes_liste_controllo.flag_valido = 'S' )   ;

    SELECT max(prog_liste_con_comp)
    INTO   :ll_progressivo  
    FROM   tes_liste_con_comp  
   WHERE   ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
           ( tes_liste_con_comp.num_reg_lista = :ld_num_reg_lista )   ;
   if sqlca.sqlcode <> 0 then
      ll_progressivo = 1
   else
      if isnull(ll_progressivo) or (ll_progressivo < 1) then
         ll_progressivo = 1
      else
         ll_progressivo++
      end if
   end if

   setitem(getrow(),"prog_liste_con_comp", ll_progressivo)
   setitem(getrow(),"num_versione", ll_num_versione)
   setitem(getrow(),"num_edizione", ll_num_edizione)
   setitem(getrow(),"des_lista", ls_des_lista)
   setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
   setitem(getrow(),"approvato_da", ls_approvato_da)
   setitem(getrow(),"emesso_il", ldt_emesso_il)
   setitem(getrow(),"autorizzato_da", ls_autorizzato_da)
   setitem(getrow(),"autorizzato_il", ldt_autorizzato_il)
   setitem(getrow(),"valido_per", ll_valido_per)
   setitem(getrow(),"validato_da", ls_validato_da)
   setitem(getrow(),"flag_uso", ls_flag_uso)
   setitem(getrow(),"flag_recuperato", ls_flag_recuperato)
   setitem(getrow(),"note", ls_note)
   setitem(getrow(),"valore_riferimento", ll_valore_riferimento)
   end if
end if
end event

event pcd_validatecol;call super::pcd_validatecol;//if i_extendmode then
//
//string ls_des_lista, ls_cod_area_aziendale, ls_cod_resp_divisione, ls_approvato_da, ls_autorizzato_da
//string   ls_validato_da, ls_flag_uso, ls_flag_recuperato, ls_note
//long ll_num_versione,ll_num_edizione, ll_valido_per, ll_progressivo, ll_valore_riferimento
//date ld_emesso_il, ld_autorizzato_il
//
//   if i_colname="num_reg_lista" then
//        SELECT tes_liste_controllo.num_versione,   
//         tes_liste_controllo.num_edizione,   
//         tes_liste_controllo.des_lista,   
//         tes_liste_controllo.cod_area_aziendale,   
//         tes_liste_controllo.cod_resp_divisione,   
//         tes_liste_controllo.approvato_da,   
//         tes_liste_controllo.emesso_il,   
//         tes_liste_controllo.autorizzato_da,   
//         tes_liste_controllo.autorizzato_il,   
//         tes_liste_controllo.valido_per,   
//         tes_liste_controllo.validato_da,   
//         tes_liste_controllo.flag_uso,   
//         tes_liste_controllo.flag_recuperato,
//         tes_liste_controllo.note,
//         tes_liste_controllo.valore_riferimento
//    INTO :ll_num_versione,   
//         :ll_num_edizione,   
//         :ls_des_lista,   
//         :ls_cod_area_aziendale,   
//         :ls_cod_resp_divisione,   
//         :ls_approvato_da,   
//         :ld_emesso_il,   
//         :ls_autorizzato_da,   
//         :ld_autorizzato_il,   
//         :ll_valido_per,   
//         :ls_validato_da,   
//         :ls_flag_uso,   
//         :ls_flag_recuperato,
//         :ls_note,
//         :ll_valore_riferimento 
//    FROM tes_liste_controllo  
//   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//         ( tes_liste_controllo.num_reg_lista = :i_coltext ) AND  
//         ( tes_liste_controllo.flag_valido = 'S' )   ;
//
//    SELECT max(prog_liste_con_comp)
//    INTO   :ll_progressivo  
//    FROM   tes_liste_con_comp  
//   WHERE   ( tes_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//           ( tes_liste_con_comp.num_reg_lista = :i_coltext )   ;
//   if sqlca.sqlcode <> 0 then
//      ll_progressivo = 1
//   else
//      if isnull(ll_progressivo) or (ll_progressivo < 1) then
//         ll_progressivo = 1
//      else
//         ll_progressivo++
//      end if
//   end if
//
//   setitem(getrow(),"prog_liste_con_comp", ll_progressivo)
//   setitem(getrow(),"num_versione", ll_num_versione)
//   setitem(getrow(),"num_edizione", ll_num_edizione)
//   setitem(getrow(),"des_lista", ls_des_lista)
//   setitem(getrow(),"cod_area_aziendale", ls_cod_area_aziendale)
//   setitem(getrow(),"approvato_da", ls_approvato_da)
//   setitem(getrow(),"emesso_il", ld_emesso_il)
//   setitem(getrow(),"autorizzato_da", ls_autorizzato_da)
//   setitem(getrow(),"autorizzato_il", ld_autorizzato_il)
//   setitem(getrow(),"valido_per", ll_valido_per)
//   setitem(getrow(),"validato_da", ls_validato_da)
//   setitem(getrow(),"flag_uso", ls_flag_uso)
//   setitem(getrow(),"flag_recuperato", ls_flag_recuperato)
//   setitem(getrow(),"note", ls_note)
//   setitem(getrow(),"valore_riferimento", ll_valore_riferimento)
//   end if
//end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_det_1, "cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_det_1, "cod_fornitore")
end choose
end event

event pcd_save;call super::pcd_save;dw_det_1.object.b_ricerca_cliente.enabled = false
dw_det_1.object.b_ricerca_fornitore.enabled = false
end event

event pcd_new;call super::pcd_new;dw_det_1.object.b_ricerca_cliente.enabled = true
dw_det_1.object.b_ricerca_fornitore.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_det_1.object.b_ricerca_cliente.enabled = true
dw_det_1.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;dw_det_1.object.b_ricerca_cliente.enabled = false
dw_det_1.object.b_ricerca_fornitore.enabled = false
end event

type dw_ricerca from u_dw_search within w_tes_liste_cont_comp
integer x = 46
integer y = 140
integer width = 2418
integer height = 736
integer taborder = 30
string dataobject = "d_tes_liste_contr_comp_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"rs_cod_fornitore")
end choose
end event

type dw_folder from u_folder within w_tes_liste_cont_comp
integer x = 32
integer y = 936
integer width = 2464
integer height = 1432
integer taborder = 20
end type

type dw_lista from uo_cs_xx_dw within w_tes_liste_cont_comp
integer x = 50
integer y = 136
integer width = 2217
integer height = 732
integer taborder = 20
string dataobject = "d_tes_liste_contr_comp_lista"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long  ll_long
decimal ld_num_reg_lista
string ls_cod_fornitore, ls_cod_cliente, ls_cod_area_aziendale, ls_compilato_da
string ls_new_select, ls_where, ls_prova
datetime ldt_data_reg_da, ldt_data_reg_a

dw_ricerca.accepttext()

ld_num_reg_lista = dw_ricerca.getitemdecimal(1, "rd_num_reg_lista")
ldt_data_reg_da = dw_ricerca.getitemdatetime(1, "rdt_data_da")
ldt_data_reg_a = dw_ricerca.getitemdatetime(1, "rdt_data_a")
ls_cod_fornitore = dw_ricerca.getitemstring(1, "rs_cod_fornitore")
ls_cod_cliente = dw_ricerca.getitemstring(1, "rs_cod_cliente")
ls_cod_area_aziendale = dw_ricerca.getitemstring(1, "rs_cod_area_aziendale")
ls_compilato_da = dw_ricerca.getitemstring(1, "rs_compilato_da")

if ld_num_reg_lista <=  0 then setnull(ld_num_reg_lista)
if ls_cod_fornitore = "" then setnull(ls_cod_fornitore)
if ls_cod_cliente = "" then setnull(ls_cod_cliente)
if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)
if ls_compilato_da = "" then setnull(ls_compilato_da)


ls_where = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ld_num_reg_lista) then
	ls_where += " and num_reg_lista = " + string(ld_num_reg_lista) + ""
end if

if not isnull(ldt_data_reg_da) then
	ls_where += " and data_registrazione >= '" + string(ldt_data_reg_da, s_cs_xx.db_funzioni.formato_data) + "' "	
end if

if not isnull(ldt_data_reg_a) then
	ls_where += " and data_registrazione <= '" + string(ldt_data_reg_a, s_cs_xx.db_funzioni.formato_data) + "' "	
end if

if not isnull(ls_cod_fornitore) then
	ls_where += " and cod_fornitore = '" + ls_cod_fornitore + "'"
end if

if not isnull(ls_cod_cliente) then
	ls_where +=" and cod_cliente = '" + ls_cod_cliente + "'"
end if

if not isnull(ls_cod_area_aziendale) then
	ls_where += " and cod_area_aziendale = '" + ls_cod_area_aziendale + "'"
end if

if not isnull(ls_compilato_da) then
	ls_where +=" and compilato_da = '" + ls_compilato_da + "'"
end if

//---------------------
ls_prova = upper(this.getsqlselect())

ll_long = pos(ls_prova, "WHERE")
if ll_long = 0 then
	ls_new_select = this.getsqlselect() + ls_where
else	
	ls_new_select = left(this.getsqlselect(), ll_long - 1) + ls_where
end if

ll_long = this.setsqlselect(ls_new_select)

if ll_long < 0 then 
	g_mb.messagebox("OMNIA","Errore nel filtro di selezione")
	pcca.error = c_fatal
else
	this.resetupdate()
	ll_long = retrieve()

	if ll_long < 0 then
		pcca.error = c_fatal
	end if
end if
end event

event pcd_delete;call super::pcd_delete;ib_in_del = true

end event

event pcd_modify;call super::pcd_modify;cb_compila.enabled = false

end event

event pcd_new;call super::pcd_new;cb_compila.enabled = false
ib_in_new = true

end event

event pcd_saveafter;call super::pcd_saveafter;string ls_sql, ls_des_domanda, ls_flag_tipo_risposta, ls_des_check_1, ls_des_check_2, ls_check_button
string ls_des_option[], ls_option_button, ls_des_option_libero, ls_campo_libero, ls_des_libero, ls_des_subtotale
long   ll_num_opzioni, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp, ll_prog_riga_liste_contr
long   ll_peso_check[2]
long   ll_peso_option[6]



if ib_in_new then
   ll_num_reg_lista = getitemnumber(getrow(),"num_reg_lista") 
   ll_num_versione  = getitemnumber(getrow(),"num_versione") 
   ll_num_edizione  = getitemnumber(getrow(),"num_edizione") 
   ll_prog_liste_con_comp = getitemnumber(getrow(),"prog_liste_con_comp") 

   declare cu_dett dynamic cursor for sqlsa;

   ls_sql = "SELECT det_liste_controllo.prog_riga_liste_contr, "+&
                "det_liste_controllo.des_domanda, "+&
                "det_liste_controllo.flag_tipo_risposta, "+&   
                "det_liste_controllo.des_check_1, "+&   
                "det_liste_controllo.des_check_2, "+&   
                "det_liste_controllo.check_button, "+&   
                "det_liste_controllo.des_option_1, "+&   
                "det_liste_controllo.des_option_2, "+&   
                "det_liste_controllo.des_option_3, "+&   
                "det_liste_controllo.des_option_4, "+&   
                "det_liste_controllo.des_option_5, "+&   
                "det_liste_controllo.num_opzioni, "+&   
                "det_liste_controllo.option_button, "+&   
                "det_liste_controllo.peso_check_1, "+&
                "det_liste_controllo.peso_check_2, "+&
                "det_liste_controllo.peso_option_1, "+&
                "det_liste_controllo.peso_option_2, "+&
                "det_liste_controllo.peso_option_3, "+&
                "det_liste_controllo.peso_option_4, "+&
                "det_liste_controllo.peso_option_5, "+&
                "det_liste_controllo.peso_option_libero, "+&
                "det_liste_controllo.des_option_libero, "+&
                "det_liste_controllo.campo_libero, "+&
                "det_liste_controllo.des_libero, "+&
                "det_liste_controllo.des_subtotale "+&
                "FROM det_liste_controllo "+& 
                "WHERE ( det_liste_controllo.cod_azienda = '"+s_cs_xx.cod_azienda+"' ) AND  "+&
                      "( det_liste_controllo.num_reg_lista = "+string(ll_num_reg_lista)+" ) AND "+& 
                      "( det_liste_controllo.num_versione = "+string(ll_num_versione)+" ) AND "+& 
                      "( det_liste_controllo.num_edizione = "+string(ll_num_edizione)+" )   " 

   prepare sqlsa from :ls_sql ;

   open dynamic cu_dett ;

   do while 1=1
      fetch cu_dett into :ll_prog_riga_liste_contr,
                           :ls_des_domanda,
                           :ls_flag_tipo_risposta, 
                           :ls_des_check_1,
                           :ls_des_check_2,
                           :ls_check_button,
                           :ls_des_option[1],
                           :ls_des_option[2],
                           :ls_des_option[3],
                           :ls_des_option[4],
                           :ls_des_option[5],
                           :ll_num_opzioni,
                           :ls_option_button,
                           :ll_peso_check[1],
                           :ll_peso_check[2],
                           :ll_peso_option[1],
                           :ll_peso_option[2],
                           :ll_peso_option[3],
                           :ll_peso_option[4],
                           :ll_peso_option[5],
                           :ll_peso_option[6],
                           :ls_des_option_libero,
                           :ls_campo_libero,
                           :ls_des_libero,
                           :ls_des_subtotale;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

   INSERT INTO det_liste_con_comp  
         ( cod_azienda,   
           num_reg_lista,   
           prog_liste_con_comp,   
           prog_riga_liste_contr,   
           des_domanda,   
           flag_tipo_risposta,   
           des_check_1,   
           des_check_2,   
           check_button,   
           des_option_1,   
           des_option_2,   
           des_option_3,   
           des_option_4,   
           des_option_5,   
           num_opzioni,   
           option_button,
           peso_check_1, 
           peso_check_2, 
           peso_option_1,           
           peso_option_2,           
           peso_option_3,           
           peso_option_4,           
           peso_option_5,           
           peso_option_libero,           
           des_option_libero,
           campo_libero,
           des_libero,
           des_subtotale,
           val_subtotale )  
   VALUES ( :s_cs_xx.cod_azienda,   
           :ll_num_reg_lista,   
           :ll_prog_liste_con_comp,   
           :ll_prog_riga_liste_contr,   
           :ls_des_domanda,   
           :ls_flag_tipo_risposta,   
           :ls_des_check_1,   
           :ls_des_check_2,   
           :ls_check_button,   
           :ls_des_option[1],   
           :ls_des_option[2],   
           :ls_des_option[3],   
           :ls_des_option[4],   
           :ls_des_option[5],   
           :ll_num_opzioni,   
           :ls_option_button,
           :ll_peso_check[1],
           :ll_peso_check[2],
           :ll_peso_option[1],
           :ll_peso_option[2],
           :ll_peso_option[3],
           :ll_peso_option[4],
           :ll_peso_option[5],
           :ll_peso_option[6],
           :ls_des_option_libero,
           :ls_campo_libero,
           :ls_des_libero,
           :ls_des_subtotale,
           0  )  ;
   loop
   close cu_dett;
   ib_in_new = false
end if

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;cb_compila.enabled = true
end event

event updatestart;call super::updatestart;if i_extendmode then
   string ls_tabella, ls_codice, ls_cod_area
   long   ll_num_reg_lista, ll_prog_liste_con_comp, li_i

   for li_i = 1 to this.deletedcount()
      ll_num_reg_lista = getitemnumber(li_i,"num_reg_lista", delete!, true) 
      ll_prog_liste_con_comp = getitemnumber(li_i,"prog_liste_con_comp", delete!, true) 
      delete from det_liste_con_comp
      where (det_liste_con_comp.cod_azienda = :s_cs_xx.cod_azienda) and
            (det_liste_con_comp.num_reg_lista = :ll_num_reg_lista) and
            (det_liste_con_comp.prog_liste_con_comp = :ll_prog_liste_con_comp) ;
      if sqlca.sqlcode <> 0 then
         g_mb.messagebox("Liste Compilate",sqlca.sqlerrtext)
      end if
   next
end if

end event

type dw_folder_ricerca from u_folder within w_tes_liste_cont_comp
integer x = 18
integer y = 28
integer width = 2478
integer height = 864
integer taborder = 10
end type

type dw_det_2 from uo_cs_xx_dw within w_tes_liste_cont_comp
integer x = 59
integer y = 1060
integer width = 2263
integer height = 1276
integer taborder = 30
string dataobject = "d_tes_liste_contr_comp_det_2"
boolean border = false
end type

event getfocus;call super::getfocus;dw_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

