﻿$PBExportHeader$w_tes_liste_controllo.srw
$PBExportComments$Finestra Gestione Liste
forward
global type w_tes_liste_controllo from w_cs_xx_principale
end type
type gb_1 from groupbox within w_tes_liste_controllo
end type
type cb_dettagli from commandbutton within w_tes_liste_controllo
end type
type cb_verifica from commandbutton within w_tes_liste_controllo
end type
type dw_det_4 from uo_cs_xx_dw within w_tes_liste_controllo
end type
type dw_det_3 from uo_cs_xx_dw within w_tes_liste_controllo
end type
type dw_det_1 from uo_cs_xx_dw within w_tes_liste_controllo
end type
type dw_folder from u_folder within w_tes_liste_controllo
end type
type cb_annulla from commandbutton within w_tes_liste_controllo
end type
type cb_ricerca from commandbutton within w_tes_liste_controllo
end type
type dw_folder_ricerca from u_folder within w_tes_liste_controllo
end type
type dw_ricerca from u_dw_search within w_tes_liste_controllo
end type
type dw_lista from uo_cs_xx_dw within w_tes_liste_controllo
end type
type cb_esegui from commandbutton within w_tes_liste_controllo
end type
type rb_approva from radiobutton within w_tes_liste_controllo
end type
type rb_autorizza from radiobutton within w_tes_liste_controllo
end type
type rb_valida from radiobutton within w_tes_liste_controllo
end type
type rb_nuova_edizione from radiobutton within w_tes_liste_controllo
end type
end forward

global type w_tes_liste_controllo from w_cs_xx_principale
integer width = 2811
integer height = 2128
string title = "Liste di Controllo"
gb_1 gb_1
cb_dettagli cb_dettagli
cb_verifica cb_verifica
dw_det_4 dw_det_4
dw_det_3 dw_det_3
dw_det_1 dw_det_1
dw_folder dw_folder
cb_annulla cb_annulla
cb_ricerca cb_ricerca
dw_folder_ricerca dw_folder_ricerca
dw_ricerca dw_ricerca
dw_lista dw_lista
cb_esegui cb_esegui
rb_approva rb_approva
rb_autorizza rb_autorizza
rb_valida rb_valida
rb_nuova_edizione rb_nuova_edizione
end type
global w_tes_liste_controllo w_tes_liste_controllo

type variables
boolean ib_delete=true, ib_new=false, ib_new_rec=false
long il_num_lista, il_versione, il_edizione, ll_new_lista
long il_tipo_operazione
string is_flag_valido, is_flag_in_prova
string is_tipo_records
date idt_da_data, idt_a_data
end variables

forward prototypes
public function integer wf_approva ()
public function integer wf_autorizza ()
public function integer wf_carica_singola_lista (decimal fd_num_reg_lista, decimal fd_num_versione, decimal fd_num_edizione)
public function integer wf_nuova_edizione ()
public function integer wf_valida ()
end prototypes

public function integer wf_approva ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente
if is_tipo_records <> "C" then
   g_mb.messagebox("Approvazione Lista di Controllo","Si possono approvare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if
if not(isnull(dw_lista.getitemstring(dw_lista.getrow(),"approvato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già APPROVATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_det_1.getrow()
ll_num_reg = dw_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_det_1.getitemnumber(ll_riga_corrente,"num_edizione")

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_approvazione <> "S" then
   g_mb.messagebox("Approvazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di APPROVAZIONE: verificare mansionario", StopSign!)
   return -1
end if 


UPDATE tes_liste_controllo  
SET    approvato_da = :s_cs_xx.cod_utente
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Approvazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

return 0

end function

public function integer wf_autorizza ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_approvato_da, ls_autorizzato_da
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente
datetime ldt_oggi

if is_tipo_records <> "C" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono autorizzare solo le liste in fase di CREAZIONE", StopSign!)
   return -1
end if

if isnull(dw_lista.getitemstring(dw_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if not(isnull(dw_lista.getitemstring(dw_lista.getrow(),"autorizzato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già AUTORIZZATA", StopSign!)
   return -1
end if


setpointer(HourGlass!)

ll_riga_corrente = dw_det_1.getrow()
ll_num_reg = dw_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_det_1.getitemnumber(ll_riga_corrente,"num_edizione")


//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_autorizzazione <> "S" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di AUTORIZZAZIONE: verificare mansionario", StopSign!)
   return -1
end if 

UPDATE tes_liste_controllo  
SET    flag_valido = 'N'
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg );
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

UPDATE tes_liste_controllo  
SET    flag_valido = 'S' ,
       flag_in_prova = 'N'
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

ldt_oggi = datetime(today())    //  data autorizzazione da sistema

UPDATE tes_liste_controllo  
SET    autorizzato_da = :s_cs_xx.cod_utente,
       autorizzato_il = :ldt_oggi
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Autorizzazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

return 0

end function

public function integer wf_carica_singola_lista (decimal fd_num_reg_lista, decimal fd_num_versione, decimal fd_num_edizione);cb_ricerca.triggerevent(clicked!)

if fd_num_reg_lista > 0 and fd_num_versione >= 0 and fd_num_edizione>= 0 then
	dw_ricerca.setitem(dw_ricerca.getrow(),"rd_num_reg_lista", fd_num_reg_lista)
	dw_ricerca.setitem(dw_ricerca.getrow(),"rd_num_versione", fd_num_versione)
	dw_ricerca.setitem(dw_ricerca.getrow(),"rd_num_edizione", fd_num_edizione)
end if

dw_ricerca.accepttext()
cb_ricerca.postevent("clicked")

return 1
end function

public function integer wf_nuova_edizione ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
string ls_null, ls_str
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente, ll_new_edizione, ll_new_versione, ll_max_edizioni, ll_nuova_edizione
datetime ldt_scadenza_validazione, ldt_oggi, ldt_null

setnull(ls_null)
setnull(ldt_null)
if is_tipo_records <> "E" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono creare nuove edizioni solo da liste approvate/autorizzate", StopSign!)
   return -1
end if

if isnull(dw_lista.getitemstring(dw_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if isnull(dw_lista.getitemstring(dw_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora AUTORIZZATA", StopSign!)
   return -1
end if

setpointer(hourglass!)

ll_riga_corrente = dw_det_1.getrow()
ll_num_reg = dw_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_det_1.getitemnumber(ll_riga_corrente,"num_edizione")
ll_nuova_edizione = ll_edizione + 1 

COMMIT;

  SELECT tes_liste_controllo.cod_resp_divisione
    INTO :ls_str
	 FROM tes_liste_controllo  
	WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( tes_liste_controllo.num_versione = :ll_versione ) AND  
			( tes_liste_controllo.num_edizione = :ll_nuova_edizione )   ;
   if sqlca.sqlcode = 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo","INTERROMPO ELABORAZIONE: è già esistente una revisione di questa lista")
      ROLLBACK;
      return -1
   end if



  INSERT INTO tes_liste_controllo  
         ( cod_azienda,   
           num_reg_lista,   
           num_versione,   
           num_edizione,   
           des_lista,   
           cod_area_aziendale,   
           cod_resp_divisione,   
           approvato_da,   
           emesso_il,   
           autorizzato_da,   
           autorizzato_il,   
           valido_per,   
           validato_da,   
           flag_uso,   
           flag_recuperato,   
           note,   
           flag_in_prova,   
           flag_valido,   
           scadenza_validazione )  
     SELECT tes_liste_controllo.cod_azienda,   
            tes_liste_controllo.num_reg_lista,   
            tes_liste_controllo.num_versione,   
            tes_liste_controllo.num_edizione + 1,   
            tes_liste_controllo.des_lista,   
            tes_liste_controllo.cod_area_aziendale,   
            tes_liste_controllo.cod_resp_divisione,   
            tes_liste_controllo.approvato_da,   
            tes_liste_controllo.emesso_il,   
            tes_liste_controllo.autorizzato_da,   
            tes_liste_controllo.autorizzato_il,   
            tes_liste_controllo.valido_per,   
            tes_liste_controllo.validato_da,   
            tes_liste_controllo.flag_uso,   
            tes_liste_controllo.flag_recuperato,   
            tes_liste_controllo.note,   
            tes_liste_controllo.flag_in_prova,   
            tes_liste_controllo.flag_valido,   
            tes_liste_controllo.scadenza_validazione  
       FROM tes_liste_controllo  
      WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
            ( tes_liste_controllo.num_versione = :ll_versione ) AND  
            ( tes_liste_controllo.num_edizione = :ll_edizione )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

   SELECT mansionari.cod_resp_divisione  
   INTO   :ls_cod_resp_divisione  
   FROM   mansionari  
   WHERE  (mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

  setnull(ldt_scadenza_validazione)
  ldt_oggi = datetime(today())
  UPDATE tes_liste_controllo  
     SET flag_valido = 'N' ,
         flag_in_prova = 'S',
         cod_resp_divisione = :ls_cod_resp_divisione,
         valido_per = 0,
         scadenza_validazione = :ldt_scadenza_validazione,
         emesso_il = :ldt_oggi,
			autorizzato_il = :ldt_null
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
         ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
         ( tes_liste_controllo.num_versione = :ll_versione ) AND
         ( tes_liste_controllo.num_edizione = :ll_nuova_edizione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

  UPDATE tes_liste_controllo  
     SET approvato_da  = null,
         autorizzato_da = null,
         validato_da = null
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
         ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
         ( tes_liste_controllo.num_versione = :ll_versione ) AND
         ( tes_liste_controllo.num_edizione = :ll_nuova_edizione);
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if


  INSERT INTO det_liste_controllo  
         ( cod_azienda,   
           num_reg_lista,   
           num_versione,   
           num_edizione,   
           prog_riga_liste_contr,   
           des_domanda,   
           flag_tipo_risposta,   
           des_check_1,   
           des_check_2,   
           check_button,   
           des_option_1,   
           des_option_2,   
           des_option_3,   
           des_option_4,   
           des_option_5,   
           num_opzioni,   
           option_button,
           peso_check_1,
           peso_check_2,
           peso_option_1,
           peso_option_2,
           peso_option_3,
           peso_option_4,
           peso_option_5,
           peso_option_libero,
           des_option_libero,
           campo_libero,
           des_libero,
           des_subtotale,
           val_subtotale)  
     SELECT det_liste_controllo.cod_azienda,   
            det_liste_controllo.num_reg_lista,   
            det_liste_controllo.num_versione,   
            det_liste_controllo.num_edizione + 1,   
            det_liste_controllo.prog_riga_liste_contr,   
            det_liste_controllo.des_domanda,   
            det_liste_controllo.flag_tipo_risposta,   
            det_liste_controllo.des_check_1,   
            det_liste_controllo.des_check_2,   
            det_liste_controllo.check_button,   
            det_liste_controllo.des_option_1,   
            det_liste_controllo.des_option_2,   
            det_liste_controllo.des_option_3,   
            det_liste_controllo.des_option_4,   
            det_liste_controllo.des_option_5,   
            det_liste_controllo.num_opzioni,   
            det_liste_controllo.option_button,  
            det_liste_controllo.peso_check_1,
            det_liste_controllo.peso_check_2,
            det_liste_controllo.peso_option_1,
            det_liste_controllo.peso_option_2,
            det_liste_controllo.peso_option_3,
            det_liste_controllo.peso_option_4,
            det_liste_controllo.peso_option_5,
            det_liste_controllo.peso_option_libero,
            det_liste_controllo.des_option_libero,
            det_liste_controllo.campo_libero,
            det_liste_controllo.des_libero,
            det_liste_controllo.des_subtotale,
            det_liste_controllo.val_subtotale  
       FROM det_liste_controllo  
      WHERE ( det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( det_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
            ( det_liste_controllo.num_versione = :ll_versione ) AND  
            ( det_liste_controllo.num_edizione = :ll_edizione )   ;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
   ROLLBACK;
   return -1
end if


ll_max_edizioni = f_num_max_edi()

if (ll_edizione > ll_max_edizioni ) and (ll_max_edizioni <> 0) then
   ll_new_edizione = 0 
   ll_new_versione = ll_versione + 1

  SELECT tes_liste_controllo.cod_resp_divisione
    INTO :ls_str
	 FROM tes_liste_controllo  
	WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
			( tes_liste_controllo.num_versione = :ll_new_versione ) AND  
			( tes_liste_controllo.num_edizione = :ll_new_edizione )   ;
   if sqlca.sqlcode = 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo","INTERROMPO ELABORAZIONE: è già esistente una revisione di questa lista")
      ROLLBACK;
      return -1
   end if
	
	
	UPDATE tes_liste_controllo  
     SET num_versione = :ll_new_versione,  
         num_edizione = :ll_new_edizione
   WHERE ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
         ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
         ( tes_liste_controllo.num_versione = :ll_versione ) AND  
         ( tes_liste_controllo.num_edizione = :ll_edizione );
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if

   UPDATE det_liste_controllo  
     SET num_versione = :ll_new_versione,  
         num_edizione = :ll_new_edizione
   WHERE ( det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda )  AND  
         ( det_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
         ( det_liste_controllo.num_versione = :ll_versione ) AND  
         ( det_liste_controllo.num_edizione = :ll_edizione );

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Edizione Lista di Controllo",sqlca.sqlerrtext + "  INTERROMPO ELABORAZIONE")
      ROLLBACK;
      return -1
   end if
end if

COMMIT;
return 0
end function

public function integer wf_valida ();string ls_cod_resp_divisione, ls_area_aziendale, ls_flag_approvazione, ls_flag_autorizzazione, ls_flag_validazione
long ll_num_reg, ll_versione, ll_edizione, ll_riga_corrente


if is_tipo_records <> "E" then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Si possono validare solo le liste nelle ultime edizioni", StopSign!)
   return -1
end if

if isnull(dw_lista.getitemstring(dw_lista.getrow(),"approvato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora APPROVATA", StopSign!)
   return -1
end if

if isnull(dw_lista.getitemstring(dw_lista.getrow(),"autorizzato_da")) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo non ancora AUTORIZZATA", StopSign!)
   return -1
end if

if NOT(isnull(dw_lista.getitemstring(dw_lista.getrow(),"validato_da"))) then
   g_mb.messagebox("Autorizzazione Lista di Controllo","Lista di Controllo già VALIDATA", StopSign!)
   return -1
end if

setpointer(hourglass!)
ll_riga_corrente = dw_det_1.getrow()
ll_num_reg = dw_det_1.getitemnumber(ll_riga_corrente,"num_reg_lista")
ll_versione = dw_det_1.getitemnumber(ll_riga_corrente,"num_versione")
ll_edizione = dw_det_1.getitemnumber(ll_riga_corrente,"num_edizione")

COMMIT;

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//SELECT mansionari.cod_resp_divisione,
//       mansionari.cod_area_aziendale,   
//       mansionari.flag_approvazione,   
//       mansionari.flag_autorizzazione,   
//       mansionari.flag_validazione  
//INTO   :ls_cod_resp_divisione,
//       :ls_area_aziendale,   
//       :ls_flag_approvazione,   
//       :ls_flag_autorizzazione,   
//       :ls_flag_validazione  
//FROM   mansionari  
//WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;

SELECT mansionari.cod_resp_divisione,
       mansionari.cod_area_aziendale
INTO   :ls_cod_resp_divisione,
       :ls_area_aziendale
FROM   mansionari  
WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
		 
uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_approvazione = 'N'
ls_flag_autorizzazione = 'N'
ls_flag_validazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.approvazione) then ls_flag_approvazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
if luo_mansionario.uof_get_privilege(luo_mansionario.validazione) then ls_flag_validazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Validazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
end if

if ls_flag_Validazione <> "S" then
   g_mb.messagebox("Validazione Lista di Controllo","L'utente non possiede il PRIVILEGIO di AUTORIZZAZIONE: verificare mansionario", StopSign!)
   return -1
end if 

//UPDATE tes_liste_controllo  
//SET    flag_valido = 'N'
//WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( tes_liste_controllo.num_reg_lista = :ll_num_reg );
//
//UPDATE tes_liste_controllo  
//SET    flag_valido = 'S'
//WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
//       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
//       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;

UPDATE tes_liste_controllo  
SET    validato_da = :s_cs_xx.cod_utente
WHERE  ( tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( tes_liste_controllo.num_reg_lista = :ll_num_reg ) AND  
       ( tes_liste_controllo.num_versione  = :ll_versione ) AND  
       ( tes_liste_controllo.num_edizione  = :ll_edizione )   ;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Validazione Lista di Controllo",sqlca.sqlerrtext)
   return -1
   ROLLBACK;
else
   COMMIT;
end if
return 0
end function

on w_tes_liste_controllo.create
int iCurrent
call super::create
this.gb_1=create gb_1
this.cb_dettagli=create cb_dettagli
this.cb_verifica=create cb_verifica
this.dw_det_4=create dw_det_4
this.dw_det_3=create dw_det_3
this.dw_det_1=create dw_det_1
this.dw_folder=create dw_folder
this.cb_annulla=create cb_annulla
this.cb_ricerca=create cb_ricerca
this.dw_folder_ricerca=create dw_folder_ricerca
this.dw_ricerca=create dw_ricerca
this.dw_lista=create dw_lista
this.cb_esegui=create cb_esegui
this.rb_approva=create rb_approva
this.rb_autorizza=create rb_autorizza
this.rb_valida=create rb_valida
this.rb_nuova_edizione=create rb_nuova_edizione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_1
this.Control[iCurrent+2]=this.cb_dettagli
this.Control[iCurrent+3]=this.cb_verifica
this.Control[iCurrent+4]=this.dw_det_4
this.Control[iCurrent+5]=this.dw_det_3
this.Control[iCurrent+6]=this.dw_det_1
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.cb_ricerca
this.Control[iCurrent+10]=this.dw_folder_ricerca
this.Control[iCurrent+11]=this.dw_ricerca
this.Control[iCurrent+12]=this.dw_lista
this.Control[iCurrent+13]=this.cb_esegui
this.Control[iCurrent+14]=this.rb_approva
this.Control[iCurrent+15]=this.rb_autorizza
this.Control[iCurrent+16]=this.rb_valida
this.Control[iCurrent+17]=this.rb_nuova_edizione
end on

on w_tes_liste_controllo.destroy
call super::destroy
destroy(this.gb_1)
destroy(this.cb_dettagli)
destroy(this.cb_verifica)
destroy(this.dw_det_4)
destroy(this.dw_det_3)
destroy(this.dw_det_1)
destroy(this.dw_folder)
destroy(this.cb_annulla)
destroy(this.cb_ricerca)
destroy(this.dw_folder_ricerca)
destroy(this.dw_ricerca)
destroy(this.dw_lista)
destroy(this.cb_esegui)
destroy(this.rb_approva)
destroy(this.rb_autorizza)
destroy(this.rb_valida)
destroy(this.rb_nuova_edizione)
end on

event pc_setwindow;call super::pc_setwindow;windowobject		lw_oggetti[], l_objects[]
string					l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

//preparazione folder del dettaglio
lw_oggetti[1] = dw_det_1
dw_folder.fu_assigntab(1, "&Dati Lista", lw_oggetti[])

lw_oggetti[1] = dw_det_3
dw_folder.fu_assigntab(2, "&Note e Modifiche", lw_oggetti[])

lw_oggetti[1] = dw_det_4
dw_folder.fu_assigntab(3, "&Punteggi", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

rb_approva.enabled = false
rb_autorizza.enabled = false
rb_valida.enabled = true
rb_nuova_edizione.enabled = true
cb_esegui.enabled = false
//---------

//set dw option per lista-dettagli
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_noretrieveonopen, &
                                  c_default)
dw_det_1.set_dw_options(sqlca, &
                                 dw_lista, &
                                 c_sharedata + c_scrollparent, &
                                 c_default)
dw_det_3.set_dw_options(sqlca, &
                                 dw_lista, &
                                 c_sharedata + c_scrollparent, &
                                 c_default)
dw_det_4.set_dw_options(sqlca, &
                                 dw_lista, &
                                 c_sharedata + c_scrollparent, &
                                 c_default)

iuo_dw_main=dw_lista

is_flag_valido = "S"
is_flag_in_prova = "%"
//----------------------------------

//preparazione folder ricerca-lista
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_annulla
dw_folder_ricerca.fu_assigntab(2, "R.", l_objects[])

l_objects[1] = dw_lista
l_objects[2] = gb_1
l_objects[3] = cb_esegui
l_objects[4] = rb_approva
l_objects[5] = rb_autorizza
l_objects[6] = rb_valida
l_objects[7] = rb_nuova_edizione
dw_folder_ricerca.fu_assigntab(1, "L.", l_objects[])

dw_folder_ricerca.fu_foldercreate(2,2)
dw_folder_ricerca.fu_selecttab(2)
dw_lista.change_dw_current()
//-----------------------------------

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_1,"cod_resp_divisione",sqlca,&
                 "mansionari","cod_resp_divisione","cognome",&
                 "mansionari.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ricerca,"rd_num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )


end event

event open;call super::open;decimal					ld_num_reg_lista, ld_num_versione, ld_num_edizione
s_cs_xx_parametri lstr_parametri

lstr_parametri = Message.PowerObjectParm

if not isnull( lstr_parametri) then
	
	if isvalid( lstr_parametri) then
		if not isnull(lstr_parametri.parametro_ul_1) and not isnull(lstr_parametri.parametro_ul_2) and &
				not isnull(lstr_parametri.parametro_ul_3) then
			
			ld_num_reg_lista = lstr_parametri.parametro_ul_1
			ld_num_versione = lstr_parametri.parametro_ul_2
			ld_num_edizione = lstr_parametri.parametro_ul_3
			wf_carica_singola_lista( ld_num_reg_lista, ld_num_versione, ld_num_edizione)
		end if
	end if
end if
end event

type gb_1 from groupbox within w_tes_liste_controllo
integer x = 2139
integer y = 116
integer width = 571
integer height = 420
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Operazioni"
end type

type cb_dettagli from commandbutton within w_tes_liste_controllo
integer x = 2327
integer y = 1928
integer width = 370
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Dettaglio"
end type

event clicked;if dw_lista.i_numselected < 1 then
   g_mb.messagebox("Dettaglio Liste di Controllo","Selezionare prima la lista sulla quale entrare nel dettaglio",StopSign!)
   return
end if
if not isvalid(w_det_liste_controllo) then
	window_open_parm(w_det_liste_controllo, -1, dw_lista)
end if
end event

type cb_verifica from commandbutton within w_tes_liste_controllo
integer x = 1947
integer y = 1932
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Controllo"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)
f_lista_controllo_verifica_validazioni()
if isnull(s_cs_xx.parametri.parametro_s_1) then
   s_cs_xx.parametri.parametro_s_1 = "Nessuna azione intrapresa"
end if
window_open(w_rapporto_verifiche, 0)
setnull(s_cs_xx.parametri.parametro_s_1)

end event

type dw_det_4 from uo_cs_xx_dw within w_tes_liste_controllo
integer x = 96
integer y = 884
integer width = 2322
integer height = 1004
integer taborder = 60
string dataobject = "d_tes_liste_controllo_det_4"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

datetime ldt_data
choose case i_colname
   case "valido_per"
   if (long(i_coltext) > 0) and (not isnull(i_coltext)) then
      setitem(getrow(),"scadenza_validazione", datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), long(i_coltext))))
      if getitemdatetime(getrow(),"scadenza_validazione") < datetime(today()) then
         g_mb.messagebox("Lista Controllo","Attenzione la data di scadenza validazione è inferiore alla data odierna: aumentare il numero di giorni di validità", Information!)
         pcca.error = c_valfailed
      end if
   else
      setnull(ldt_data)
      setitem(getrow(),"scadenza_validazione", ldt_data)
   end if
end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

   ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")),getitemnumber(getrow(),"valido_per")))
   ldt_oggi = datetime(today())

   if ldt_scad <= ldt_oggi  then
      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(), "valido_per") > 0 then
      setitem(getrow(),"scadenza_validazione", ldt_scad)
   else
      setnull(ldt_oggi)
      setitem(getrow(),"scadenza_validazione", ldt_oggi)
   end if
end if
end event

type dw_det_3 from uo_cs_xx_dw within w_tes_liste_controllo
integer x = 96
integer y = 856
integer width = 2341
integer height = 1020
integer taborder = 50
string dataobject = "d_tes_liste_controllo_det_3"
boolean border = false
end type

type dw_det_1 from uo_cs_xx_dw within w_tes_liste_controllo
integer x = 82
integer y = 856
integer width = 2167
integer height = 908
integer taborder = 40
string dataobject = "d_tes_liste_controllo_det_1"
boolean border = false
end type

event pcd_validatecol;call super::pcd_validatecol;if i_extendmode then

datetime ldt_data
choose case i_colname
   case "valido_per"
   if (long(i_coltext) > 0) and (not isnull(i_coltext)) then
      setitem(getrow(),"scadenza_validazione", datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), long(i_coltext))))
      if getitemdatetime(getrow(),"scadenza_validazione") < datetime(today()) then
         g_mb.messagebox("Lista Controllo","Attenzione la data di scadenza validazione è inferiore alla data odierna: aumentare il numero di giorni di validità", Information!)
         pcca.error = c_valfailed
      end if
   else
      setnull(ldt_data)
      setitem(getrow(),"scadenza_validazione", ldt_data)
   end if
end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

   ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")), getitemnumber(getrow(),"valido_per")))
   ldt_oggi = datetime(today())

   if ldt_scad <= ldt_oggi  then
      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
      pcca.error = c_valfailed
      return
   end if

   if getitemnumber(getrow(), "valido_per") > 0 then
      setitem(getrow(),"scadenza_validazione", ldt_scad)
   else
      setnull(ldt_oggi)
      setitem(getrow(),"scadenza_validazione", ldt_oggi)
   end if
end if
end event

type dw_folder from u_folder within w_tes_liste_controllo
integer x = 37
integer y = 728
integer width = 2706
integer height = 1184
integer taborder = 40
end type

type cb_annulla from commandbutton within w_tes_liste_controllo
integer x = 2395
integer y = 256
integer width = 329
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;decimal ld_null
string ls_null

setnull(ld_null)
setnull(ls_null)

dw_ricerca.setitem(1, "rd_num_reg_lista", ld_null)
dw_ricerca.setitem(1, "rs_cod_area_aziendale", ls_null)
dw_ricerca.setitem(1, "rs_stato", "E")
dw_ricerca.setitem(1, "rd_num_versione", ld_null)
dw_ricerca.setitem(1, "rd_num_edizione", ld_null)

//rb_approva.enabled = false
//rb_autorizza.enabled = false
//rb_valida.enabled = true
//rb_nuova_edizione.enabled = true
//cb_esegui.enabled = false

is_flag_valido = "S"
is_flag_in_prova = "%"

is_tipo_records = "E"

////dw_lista.setfilter("")
////dw_lista.filter()

dw_lista.Change_DW_Current( )
end event

type cb_ricerca from commandbutton within w_tes_liste_controllo
integer x = 2395
integer y = 152
integer width = 329
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;//dw_ricerca.fu_BuildSearch(TRUE)
//dw_folder_ricerca.fu_SelectTab(1)
//dw_lista.change_dw_current()
//parent.triggerevent("pc_retrieve")

dw_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_folder_ricerca.fu_selecttab(1)

end event

type dw_folder_ricerca from u_folder within w_tes_liste_controllo
integer x = 37
integer y = 20
integer width = 2706
integer height = 648
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_tes_liste_controllo
integer x = 59
integer y = 124
integer width = 2322
integer height = 524
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tes_liste_controllo_ricerca"
boolean border = false
end type

type dw_lista from uo_cs_xx_dw within w_tes_liste_controllo
integer x = 59
integer y = 124
integer width = 1989
integer height = 508
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_tes_liste_controllo_lista"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;//long ll_errore
//
//
//ll_errore = retrieve(s_cs_xx.cod_azienda)
//
//if ll_errore < 0 then
//   pcca.error = c_fatal
//end if


long  l_error
decimal ld_num_reg_lista, ld_num_versione, ld_num_edizione
string ls_cod_area_aziendale, ls_stato
string ls_new_select, ls_where, ls_prova

dw_ricerca.accepttext()

ld_num_reg_lista = dw_ricerca.getitemdecimal(1, "rd_num_reg_lista")
ls_cod_area_aziendale = dw_ricerca.getitemstring(1, "rs_cod_area_aziendale")
ls_stato = dw_ricerca.getitemstring(1, "rs_stato")
ld_num_versione = dw_ricerca.getitemdecimal(1, "rd_num_versione")
ld_num_edizione = dw_ricerca.getitemdecimal(1, "rd_num_edizione")

//-----
if ld_num_reg_lista > 0 then
else
	setnull(ld_num_reg_lista)
end if

if ls_cod_area_aziendale = "" then setnull(ls_cod_area_aziendale)
//if isnull(ls_stato) or ls_stato = "" then ls_stato = "E"

if ld_num_versione >= 0 then
else
	setnull(ld_num_versione)
end if

if ld_num_edizione >= 0 then
else
	setnull(ld_num_edizione)
end if
//-----

ls_where = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_area_aziendale) then
	ls_where += " and cod_area_aziendale = '" + ls_cod_area_aziendale + "'"
end if

if not isnull(ld_num_reg_lista) then
	ls_where += " and num_reg_lista  = " + string(ld_num_reg_lista)
end if

if not isnull(ld_num_versione) then
	ls_where += " and num_versione  = " + string(ld_num_versione)
end if

if not isnull(ld_num_edizione) then
	ls_where += " and num_edizione  = " + string(ld_num_edizione)
end if

choose case ls_stato
	case "E" //ultime revisioni
		rb_approva.enabled = false
		rb_autorizza.enabled = false
		rb_valida.enabled = true
		rb_nuova_edizione.enabled = true
		cb_esegui.enabled = false
		
		is_flag_valido = "S"
		is_flag_in_prova = "%"
		is_tipo_records = "E"
		ls_where += " and flag_valido  = '" + is_flag_valido + "'"

	case "C" //in creazione
		rb_approva.enabled = true
		rb_autorizza.enabled = true
		rb_valida.enabled = false
		rb_nuova_edizione.enabled = false
		cb_esegui.enabled = false
		
		is_flag_valido = "N"
		is_flag_in_prova = "S"
		is_tipo_records = "C"
		ls_where += " and flag_valido  = '" + is_flag_valido + "' and flag_in_prova = '" + is_flag_in_prova + "'"

	case "S" //in scadenza
		rb_approva.enabled = false
		rb_autorizza.enabled = false
		rb_valida.enabled = true
		rb_nuova_edizione.enabled = false
		cb_esegui.enabled = false
		
		is_flag_valido = "S"
		is_flag_in_prova = "%"
		
		is_tipo_records = "S"
		//		ls_str = "(scadenza_validazione > date(" + char(34) + "01/01/1900" + char(34) + ")  and isnull(validato_da))"
		ls_where += " and flag_valido  = '" + is_flag_valido + "'" + &
							" and scadenza_validazione > '" + string(datetime(date(1900,1,1),time("00:00:00")), s_cs_xx.db_funzioni.formato_data) + "' " + &
							" and validato_da is null"
//		ls_where += " and flag_valido  = '" + is_flag_valido + &
//							" and validato_da is null"

	case "T" //storico
		rb_approva.enabled = false
		rb_autorizza.enabled = false
		rb_valida.enabled = false
		rb_nuova_edizione.enabled = false
		cb_esegui.enabled = false
		
		is_flag_valido = "N"
		is_flag_in_prova = "N"
		
		is_tipo_records = "T"
		ls_where += " and flag_valido  = '" + is_flag_valido + "' and flag_in_prova = '" + is_flag_in_prova + "'"
		
end choose

//---------------------
ls_prova = upper(this.getsqlselect())

l_Error = pos(ls_prova, "WHERE")
if l_Error = 0 then
	ls_new_select = this.getsqlselect() + ls_where
else	
	ls_new_select = left(this.getsqlselect(), l_Error - 1) + ls_where
end if

ls_new_select += " order by num_reg_lista asc,num_versione asc,num_edizione desc"

l_Error = this.setsqlselect(ls_new_select)

if l_Error < 0 then 
	g_mb.messagebox("OMNIA","Errore nel filtro di selezione")
	pcca.error = c_fatal
else
	this.Change_DW_Current( )
	this.resetupdate()
	l_Error = retrieve()

	if l_Error < 0 then
		pcca.error = c_fatal
	end if
end if




end event

event pcd_delete;call super::pcd_delete;cb_dettagli.enabled = false
ib_delete = true
end event

event pcd_modify;call super::pcd_modify;cb_dettagli.enabled = false

end event

event pcd_new;call super::pcd_new;string ls_cod_resp_divisione

cb_dettagli.enabled = false
ib_new =true


if ib_new then
   SELECT mansionari.cod_resp_divisione  
   INTO   :ls_cod_resp_divisione  
   FROM   mansionari  
   WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
      pcca.error = c_fatal
   end if
   setitem(getrow(), "cod_resp_divisione", ls_cod_resp_divisione)
   setitem(getrow(),"emesso_il", datetime(today()))
end if   

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then

string ls_data
datetime ldt_scad, ldt_oggi
long   ll_valido_per

   if isnull(getitemstring(getrow(), "cod_area_aziendale")) or len(getitemstring(getrow(), "cod_area_aziendale")) < 1  then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione area di riferimento lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemdatetime(getrow(), "emesso_il")) or (getitemdatetime(getrow(), "emesso_il") < datetime(date("01/01/1901")))then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione data di creazione lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "cod_resp_divisione")) or len(getitemstring(getrow(), "cod_resp_divisione")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   if isnull(getitemstring(getrow(), "des_lista")) or len(getitemstring(getrow(), "des_lista")) < 1 then
      g_mb.messagebox("Liste di Controllo","ATTENZIONE: manca indicazione mansionario responsabile della lista", StopSign!)
      pcca.error = c_fatal
      return
   end if

   ls_data = string(getitemdatetime(getrow(),"autorizzato_il"), s_cs_xx.db_funzioni.formato_data)

   if (getitemnumber(getrow(), "valido_per") > 0) and ( isnull(ls_data) or ls_data = "")  then
      g_mb.messagebox("Lista di controllo","Sono stati indicati giorni di validità senza la data di autorizzazione: in questo modo non posso calcolare la scadenza entro la quale bisogna validare la lista",StopSign!)
      pcca.error = c_fatal
      return
   end if

	ll_valido_per = getitemnumber(getrow(),"valido_per") 
	if (ll_valido_per > 0) and not(isnull(ll_valido_per)) then
		ldt_scad = datetime(RelativeDate(date(getitemdatetime(getrow(),"autorizzato_il")),  ll_valido_per))
	   ldt_oggi = datetime(today())

	   if ldt_scad <= ldt_oggi  then
	      g_mb.messagebox("Lista di controllo","La data di scadenza della validazione è già inferiore o uguale a oggi: aumentare giorni di validità",StopSign!)
	      pcca.error = c_valfailed
	      return
	   end if
      setitem(getrow(),"scadenza_validazione", ldt_scad)
	end if

end if			//  di extendmode
end event

event pcd_view;call super::pcd_view;if this.getrow() > 0 then
   il_num_lista = this.getitemnumber(this.getrow(), "num_reg_lista")
   il_versione = this.getitemnumber(this.getrow(), "num_versione")
   il_edizione = this.getitemnumber(this.getrow(), "num_edizione")
end if

cb_dettagli.enabled = true

if ib_new_rec then
	//rb_in_creazione.checked = true
	//rb_in_creazione.postevent("Clicked")
	//ib_new_rec = false
	
	dw_ricerca.setitem(1, "rs_stato", "C")
	ib_new_rec = false
	cb_ricerca.postevent(clicked!)
end if
end event

event updatestart;call super::updatestart;string ls_cod_resp_divisione

if ib_delete = true then
   DELETE FROM det_liste_controllo
          WHERE (det_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda) and
                (det_liste_controllo.num_reg_lista = :il_num_lista) and
                (det_liste_controllo.num_versione = :il_versione) and
                (det_liste_controllo.num_edizione = :il_edizione);
end if

if ib_new then
   SELECT mansionari.cod_resp_divisione  
   INTO   :ls_cod_resp_divisione  
   FROM   mansionari  
   WHERE  ( mansionari.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          ( mansionari.cod_utente = :s_cs_xx.cod_utente )   ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Nuova Lista","Attenzione!! all'utente corrispondono più mansionari, oppure non esiste il mansionario dell'utente", StopSign!)
      pcca.error = c_fatal
      ib_new = false
      return
   end if
   setitem(getrow(), "cod_resp_divisione", ls_cod_resp_divisione)

   SELECT max(tes_liste_controllo.num_reg_lista)  
   INTO   :ll_new_lista  
   FROM   tes_liste_controllo  
   WHERE  tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda   ;

   if (sqlca.sqlcode <> 0) or (isnull(ll_new_lista)) then
      ll_new_lista = 1
   else
      ll_new_lista ++
   end if
   setitem(getrow(),"num_reg_lista", ll_new_lista)
   setitem(getrow(),"num_versione", 1)
   setitem(getrow(),"num_edizione", 0)
   setitem(getrow(),"flag_valido", "N")
   setitem(getrow(),"flag_in_prova", "S")
   ib_new = false
   ib_new_rec = true
end if   

end event

type cb_esegui from commandbutton within w_tes_liste_controllo
integer x = 2139
integer y = 536
integer width = 549
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui Operazione"
end type

event clicked;long ll_riga_corrente


ll_riga_corrente = dw_lista.getrow()
choose case il_tipo_operazione
	case 1
		if wf_approva() = 0 then parent.triggerevent("pc_retrieve")
		dw_lista.setrow(ll_riga_corrente)
		
		//rb_in_creazione.triggerevent("clicked")
		dw_ricerca.setitem(1, "rs_stato", "C")
		cb_ricerca.postevent(clicked!)
		
		setpointer(arrow!)
		
		
	case 2
		if wf_autorizza() = 0 then parent.triggerevent("pc_retrieve")
		dw_lista.setrow(ll_riga_corrente)
		
		//rb_in_creazione.triggerevent("clicked")
		dw_ricerca.setitem(1, "rs_stato", "C")
		cb_ricerca.postevent(clicked!)
		
		setpointer(arrow!) 
		
		
	case 3
		if wf_valida() = 0 then parent.triggerevent("pc_retrieve")
		dw_lista.setrow(ll_riga_corrente)
		
		//rb_ultime_edizioni.triggerevent("clicked")
		dw_ricerca.setitem(1, "rs_stato", "E")
		cb_ricerca.postevent(clicked!)
		
		setpointer(arrow!)
		
		
	case 4
		if wf_nuova_edizione() = 0 then
			g_mb.messagebox("Nuova Edizione Lista di Controllo","La nuova edizione si trova fra le liste IN CREAZIONE",Information!)
		end if   
		setpointer(arrow!)
		
		
end choose
end event

type rb_approva from radiobutton within w_tes_liste_controllo
integer x = 2162
integer y = 196
integer width = 507
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Verifica"
end type

on clicked;il_tipo_operazione = 1
cb_esegui.enabled = true
end on

type rb_autorizza from radiobutton within w_tes_liste_controllo
integer x = 2162
integer y = 276
integer width = 489
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Approva"
end type

on clicked;il_tipo_operazione = 2
cb_esegui.enabled = true
end on

type rb_valida from radiobutton within w_tes_liste_controllo
integer x = 2162
integer y = 356
integer width = 530
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Valida"
end type

on clicked;il_tipo_operazione = 3
cb_esegui.enabled = true
end on

type rb_nuova_edizione from radiobutton within w_tes_liste_controllo
integer x = 2162
integer y = 436
integer width = 535
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nuova Revisione"
end type

on clicked;il_tipo_operazione = 4
cb_esegui.enabled = true
end on

