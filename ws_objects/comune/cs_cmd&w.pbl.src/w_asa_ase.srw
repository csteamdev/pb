﻿$PBExportHeader$w_asa_ase.srw
$PBExportComments$Window Esportazione/Importazione ASA to ASE
forward
global type w_asa_ase from w_cs_xx_principale
end type
type dw_elenco_tabelle from uo_cs_xx_dw within w_asa_ase
end type
type st_2 from statictext within w_asa_ase
end type
type sle_path from singlelineedit within w_asa_ase
end type
type cb_esporta from commandbutton within w_asa_ase
end type
type cb_2 from commandbutton within w_asa_ase
end type
type st_1 from statictext within w_asa_ase
end type
type st_3 from statictext within w_asa_ase
end type
type lb_lista_no from listbox within w_asa_ase
end type
type st_4 from statictext within w_asa_ase
end type
type cb_1 from commandbutton within w_asa_ase
end type
type cb_3 from commandbutton within w_asa_ase
end type
type lb_elenco_vuote from listbox within w_asa_ase
end type
type st_5 from statictext within w_asa_ase
end type
end forward

global type w_asa_ase from w_cs_xx_principale
int Width=3420
int Height=1721
boolean TitleBar=true
string Title="Gestione ABI"
dw_elenco_tabelle dw_elenco_tabelle
st_2 st_2
sle_path sle_path
cb_esporta cb_esporta
cb_2 cb_2
st_1 st_1
st_3 st_3
lb_lista_no lb_lista_no
st_4 st_4
cb_1 cb_1
cb_3 cb_3
lb_elenco_vuote lb_elenco_vuote
st_5 st_5
end type
global w_asa_ase w_asa_ase

type variables
transaction sqlcb
end variables

event pc_setwindow;call super::pc_setwindow;dw_elenco_tabelle.set_dw_options(sqlca, &
                      pcca.null_object, &
                      c_nonew + c_nodelete + c_nomodify + c_multiselect, &
                      c_default)
end event

on w_asa_ase.create
int iCurrent
call w_cs_xx_principale::create
this.dw_elenco_tabelle=create dw_elenco_tabelle
this.st_2=create st_2
this.sle_path=create sle_path
this.cb_esporta=create cb_esporta
this.cb_2=create cb_2
this.st_1=create st_1
this.st_3=create st_3
this.lb_lista_no=create lb_lista_no
this.st_4=create st_4
this.cb_1=create cb_1
this.cb_3=create cb_3
this.lb_elenco_vuote=create lb_elenco_vuote
this.st_5=create st_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_elenco_tabelle
this.Control[iCurrent+2]=st_2
this.Control[iCurrent+3]=sle_path
this.Control[iCurrent+4]=cb_esporta
this.Control[iCurrent+5]=cb_2
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=st_3
this.Control[iCurrent+8]=lb_lista_no
this.Control[iCurrent+9]=st_4
this.Control[iCurrent+10]=cb_1
this.Control[iCurrent+11]=cb_3
this.Control[iCurrent+12]=lb_elenco_vuote
this.Control[iCurrent+13]=st_5
end on

on w_asa_ase.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_elenco_tabelle)
destroy(this.st_2)
destroy(this.sle_path)
destroy(this.cb_esporta)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.lb_lista_no)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.cb_3)
destroy(this.lb_elenco_vuote)
destroy(this.st_5)
end on

type dw_elenco_tabelle from uo_cs_xx_dw within w_asa_ase
int X=23
int Y=421
int Width=2195
int Height=1081
int TabOrder=60
string DataObject="d_elenco_tabelle"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type st_2 from statictext within w_asa_ase
int X=23
int Y=161
int Width=481
int Height=77
boolean Enabled=false
string Text="Cartella di Lavoro:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
long BorderColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_path from singlelineedit within w_asa_ase
int X=503
int Y=161
int Width=1715
int Height=81
int TabOrder=70
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_esporta from commandbutton within w_asa_ase
int X=503
int Y=261
int Width=595
int Height=81
int TabOrder=30
string Text="Esporta da Anywhere"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_nome_tabella
string ls_path
long   ll_riga,ll_selected[]
time   lt_tempo_1

ls_path = sle_path.text

dw_elenco_tabelle.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
	ls_nome_tabella = dw_elenco_tabelle.getitemstring(ll_selected[ll_riga],"table_name")

	run ("BCP " + ls_nome_tabella + " out " + ls_path+"\"+ls_nome_tabella + ".dat -c -t§ -Sopen_1 -Udba -Psql -e " + ls_path + ls_nome_tabella)


	
next
end event

type cb_2 from commandbutton within w_asa_ase
int X=1121
int Y=261
int Width=549
int Height=81
int TabOrder=10
string Text="Importa in Adaptive"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_nome_tabella
string ls_path
long   ll_riga,ll_selected[]
time   lt_tempo_1

ls_path = sle_path.text

dw_elenco_tabelle.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
	ls_nome_tabella = dw_elenco_tabelle.getitemstring(ll_selected[ll_riga],"table_name")

	if fileexists(ls_path + "\" + ls_nome_tabella + ".dat") then
		run ("bcp " + ls_nome_tabella + " in " + ls_path+"\"+ls_nome_tabella + ".dat -c -t§ -SCS_ADAPTIVE -Udba -Psqlsql")
	end if

next
end event

type st_1 from statictext within w_asa_ase
int X=46
int Y=41
int Width=2309
int Height=101
boolean Enabled=false
string Text="Attenzione configurare OpenServerGateway prima di iniziare l'esportazione/importazione"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_asa_ase
int X=23
int Y=361
int Width=549
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco Totale Tabelle"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_lista_no from listbox within w_asa_ase
int X=2241
int Y=161
int Width=1121
int Height=721
int TabOrder=40
boolean BringToTop=true
boolean VScrollBar=true
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_asa_ase
int X=2378
int Y=81
int Width=645
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco Tabelle non esportate"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_asa_ase
int X=1692
int Y=261
int Width=389
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Confronta"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga
string ls_nome_tabella,ls_path

ls_path = sle_path.text

for ll_riga = 1 to dw_elenco_tabelle.rowcount()
	ls_nome_tabella = dw_elenco_tabelle.getitemstring(ll_riga,"table_name")
	if not (fileexists(ls_path + "\" + ls_nome_tabella + ".dat")) then
		lb_lista_no.additem (ls_nome_tabella)
	end if
		
next
end event

type cb_3 from commandbutton within w_asa_ase
int X=23
int Y=1521
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="Conf.2"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_nome_tabella
string ls_path,ls_sql
long   ll_riga,ll_num_record_asa,ll_num_record_ase,ll_conteggio

sqlcb = CREATE transaction

sqlcb.dbms = "ODBC"
sqlcb.dbparm = "ConnectString='DSN=CS_DB_ADAPTIVE;uid=dba;pwd=sqlsql'"

connect using sqlcb;

for ll_riga = 1 to dw_elenco_tabelle.rowcount()
	ls_nome_tabella = dw_elenco_tabelle.getitemstring(ll_riga,"table_name")
	ls_sql = "SELECT count(*) FROM " + ls_nome_tabella 

	DECLARE cr_1 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql ;
	OPEN DYNAMIC cr_1 ;
	FETCH cr_1 INTO :ll_num_record_asa;
	CLOSE cr_1 ;

	DECLARE cr_2 DYNAMIC CURSOR FOR SQLSA;
	PREPARE SQLSA FROM :ls_sql using sqlcb;
	OPEN DYNAMIC cr_2 ;
	FETCH cr_2 INTO :ll_num_record_ase;
	CLOSE cr_2;

	if abs(ll_num_record_asa - ll_num_record_ase) > 0 then 
		ll_conteggio ++
		lb_elenco_vuote.additem (ls_nome_tabella + " " + string(ll_num_record_asa) + " " + string(ll_num_record_ase))
		st_5.text = string(ll_conteggio)		
	end if

next
end event

type lb_elenco_vuote from listbox within w_asa_ase
int X=2241
int Y=901
int Width=1121
int Height=601
int TabOrder=51
boolean BringToTop=true
boolean VScrollBar=true
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_asa_ase
int X=2241
int Y=1521
int Width=526
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="none"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

