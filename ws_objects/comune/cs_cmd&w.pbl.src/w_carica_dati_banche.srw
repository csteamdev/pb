﻿$PBExportHeader$w_carica_dati_banche.srw
$PBExportComments$Finestra Caricamento Banche da DB ArchiSoft
forward
global type w_carica_dati_banche from Window
end type
type mle_1 from multilineedit within w_carica_dati_banche
end type
type cb_cab from commandbutton within w_carica_dati_banche
end type
type st_1 from statictext within w_carica_dati_banche
end type
type cb_abi from commandbutton within w_carica_dati_banche
end type
type ln_1 from line within w_carica_dati_banche
end type
end forward

global type w_carica_dati_banche from Window
int X=1075
int Y=481
int Width=2113
int Height=757
boolean TitleBar=true
string Title="Carica Dati Banche"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
mle_1 mle_1
cb_cab cb_cab
st_1 st_1
cb_abi cb_abi
ln_1 ln_1
end type
global w_carica_dati_banche w_carica_dati_banche

on w_carica_dati_banche.create
this.mle_1=create mle_1
this.cb_cab=create cb_cab
this.st_1=create st_1
this.cb_abi=create cb_abi
this.ln_1=create ln_1
this.Control[]={ this.mle_1,&
this.cb_cab,&
this.st_1,&
this.cb_abi,&
this.ln_1}
end on

on w_carica_dati_banche.destroy
destroy(this.mle_1)
destroy(this.cb_cab)
destroy(this.st_1)
destroy(this.cb_abi)
destroy(this.ln_1)
end on

type mle_1 from multilineedit within w_carica_dati_banche
int X=23
int Y=21
int Width=2035
int Height=261
int TabOrder=1
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Text="Questa finestra serve a caricare i dati ABI e CAB nella apposite tabelle prelevandoli dal DB della contabilità. E' ovvio quindi che deve essere attivo il collegamento con la procedura di contabilità."
long TextColor=33554432
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_cab from commandbutton within w_carica_dati_banche
event clicked pbm_bnclicked
int X=1349
int Y=321
int Width=412
int Height=109
int TabOrder=10
string Text="Aggiorna CAB"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long   ll_cont, ll_i
string ls_cod_abi, ls_cod_cab, ls_dsn, ls_agenzia, ls_provincia, ls_cap, ls_localita, ls_indirizzo
transaction sqlcb

ll_i = 0

select stringa
into   :ls_dsn
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CDS';
if sqlca.sqlcode <> 0 then
	return -1
end if

sqlcb = CREATE transaction
sqlcb.servername = "CS_DB"
sqlcb.logid = ""
sqlcb.logpass = ""
sqlcb.dbms = "Odbc"
sqlcb.database = ""
sqlcb.userid = "pgmr"
sqlcb.dbpass = "admin"
sqlcb.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(sqlcb, TRUE)
IF SQLCB.SQLCode <> 0 THEN
	g_mb.messagebox("Status","Connect Failed "  &
		+ SQLCB.SQLErrText)
	RETURN
END IF

DECLARE cu_abi CURSOR FOR
	SELECT pgmr.id_banca.abibap,
	       pgmr.id_banca.cabbap,
			 pgmr.id_banca.bapiaz,
			 pgmr.id_banca.baindi,
			 pgmr.id_banca.bacap,
			 pgmr.id_banca.baprov
	FROM pgmr.id_banca USING sqlcb;

OPEN cu_abi;
IF SQLCB.SQLCode <> 0 THEN
	g_mb.messagebox("Status","Errore durante apertura del cursore " + SQLCB.SQLErrText)
	RETURN
END IF

DO WHILE SQLCB.SQLCode = 0
	FETCH cu_abi INTO :ls_cod_abi,
	                  :ls_cod_cab,
							:ls_agenzia,
							:ls_indirizzo,
							:ls_cap,
							:ls_provincia;
	if sqlcb.sqlcode <> 0 then exit
	ll_i ++
	st_1.text = ls_cod_abi + "  -  " + ls_cod_cab
  INSERT INTO tab_abicab  
				( cod_abi,   
				  cod_cab,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  agenzia )  
	  VALUES ( :ls_cod_abi,   
				  :ls_cod_cab,   
				  :ls_indirizzo,   
				  :ls_localita,   
				  null,   
				  :ls_cap,   
				  :ls_provincia,   
				  null,   
				  null,   
				  null,   
				  :ls_agenzia )  ;
	if ll_i > 100 then
		st_1.text = "COMMIT IN CORSO . . . . "
		COMMIT using sqlca;
		ll_i = 0
	end if
LOOP

CLOSE cu_abi;

destroy sqlcb
COMMIT using sqlca;
end event

type st_1 from statictext within w_carica_dati_banche
int X=46
int Y=521
int Width=2012
int Height=81
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=67108864
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_abi from commandbutton within w_carica_dati_banche
int X=252
int Y=321
int Width=412
int Height=109
int TabOrder=20
string Text="Aggiorna ABI"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long   ll_cont, ll_i
string ls_cod_abi, ls_des_abi, ls_dsn
transaction sqlcb

ll_i = 0

select stringa
into   :ls_dsn
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CDS';
if sqlca.sqlcode <> 0 then
	return -1
end if

sqlcb = CREATE transaction
sqlcb.servername = "CS_DB"
sqlcb.logid = ""
sqlcb.logpass = ""
sqlcb.dbms = "Odbc"
sqlcb.database = ""
sqlcb.userid = "pgmr"
sqlcb.dbpass = "admin"
sqlcb.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(sqlcb, TRUE)
IF SQLCB.SQLCode <> 0 THEN
	g_mb.messagebox("Status","Connect Failed "  &
		+ SQLCB.SQLErrText)
	RETURN
END IF

DECLARE cu_abi CURSOR FOR
	SELECT pgmr.id_banca.abibap, pgmr.id_banca.badesc
	FROM pgmr.id_banca USING sqlcb;

OPEN cu_abi;
IF SQLCB.SQLCode <> 0 THEN
	g_mb.messagebox("Status","Errore durante apertura del cursore " + SQLCB.SQLErrText)
	RETURN
END IF

DO WHILE SQLCB.SQLCode = 0
	FETCH cu_abi INTO :ls_cod_abi, :ls_des_abi;
	if sqlcb.sqlcode <> 0 then exit
	ll_i ++
	st_1.text = ls_cod_abi + " - " + ls_des_abi
	insert into tab_abi
	            (cod_abi,
					des_abi)
	values      (:ls_cod_abi,
	            :ls_des_abi);
	if ll_i > 100 then
		st_1.text = "COMMIT IN CORSO . . . . "
		COMMIT using sqlca;
		ll_i = 0
	end if
LOOP

CLOSE cu_abi;

destroy sqlcb
COMMIT using sqlca;
end event

type ln_1 from line within w_carica_dati_banche
boolean Enabled=false
int BeginX=46
int BeginY=621
int EndX=2012
int EndY=621
int LineThickness=9
end type

