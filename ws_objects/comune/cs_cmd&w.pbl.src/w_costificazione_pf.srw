﻿$PBExportHeader$w_costificazione_pf.srw
forward
global type w_costificazione_pf from window
end type
type ddlb_1 from dropdownlistbox within w_costificazione_pf
end type
type st_6 from statictext within w_costificazione_pf
end type
type st_5 from statictext within w_costificazione_pf
end type
type st_4 from statictext within w_costificazione_pf
end type
type st_1 from statictext within w_costificazione_pf
end type
type st_tot from statictext within w_costificazione_pf
end type
type st_lav from statictext within w_costificazione_pf
end type
type st_3 from statictext within w_costificazione_pf
end type
type st_2 from statictext within w_costificazione_pf
end type
type sle_2 from singlelineedit within w_costificazione_pf
end type
type sle_1 from singlelineedit within w_costificazione_pf
end type
type st_mp from statictext within w_costificazione_pf
end type
type cb_1 from commandbutton within w_costificazione_pf
end type
type gb_1 from groupbox within w_costificazione_pf
end type
type gb_2 from groupbox within w_costificazione_pf
end type
type gb_3 from groupbox within w_costificazione_pf
end type
end forward

global type w_costificazione_pf from window
integer width = 2688
integer height = 588
boolean titlebar = true
string title = "Costificazione PF"
boolean controlmenu = true
boolean resizable = true
long backcolor = 67108864
ddlb_1 ddlb_1
st_6 st_6
st_5 st_5
st_4 st_4
st_1 st_1
st_tot st_tot
st_lav st_lav
st_3 st_3
st_2 st_2
sle_2 sle_2
sle_1 sle_1
st_mp st_mp
cb_1 cb_1
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
global w_costificazione_pf w_costificazione_pf

on w_costificazione_pf.create
this.ddlb_1=create ddlb_1
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_1=create st_1
this.st_tot=create st_tot
this.st_lav=create st_lav
this.st_3=create st_3
this.st_2=create st_2
this.sle_2=create sle_2
this.sle_1=create sle_1
this.st_mp=create st_mp
this.cb_1=create cb_1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
this.Control[]={this.ddlb_1,&
this.st_6,&
this.st_5,&
this.st_4,&
this.st_1,&
this.st_tot,&
this.st_lav,&
this.st_3,&
this.st_2,&
this.sle_2,&
this.sle_1,&
this.st_mp,&
this.cb_1,&
this.gb_1,&
this.gb_2,&
this.gb_3}
end on

on w_costificazione_pf.destroy
destroy(this.ddlb_1)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_1)
destroy(this.st_tot)
destroy(this.st_lav)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.sle_2)
destroy(this.sle_1)
destroy(this.st_mp)
destroy(this.cb_1)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

type ddlb_1 from dropdownlistbox within w_costificazione_pf
integer x = 206
integer y = 100
integer width = 411
integer height = 240
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Preventivo"
string item[] = {"Preventivo","Consuntivo"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;if ddlb_1.text = "Preventivo" then
	st_2.text = "Prodotto:"
	st_3.text = "Versione:"
else
	st_2.text = "Anno:"
	st_3.text = "Commessa:"
end if
end event

type st_6 from statictext within w_costificazione_pf
integer x = 46
integer y = 108
integer width = 142
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Tipo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_costificazione_pf
integer x = 1851
integer y = 348
integer width = 206
integer height = 68
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Totale:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_costificazione_pf
integer x = 46
integer y = 348
integer width = 297
integer height = 68
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "M. Prime:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_costificazione_pf
integer x = 914
integer y = 348
integer width = 366
integer height = 68
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Lavorazioni:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_tot from statictext within w_costificazione_pf
integer x = 2080
integer y = 340
integer width = 503
integer height = 80
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12639424
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_lav from statictext within w_costificazione_pf
integer x = 1303
integer y = 340
integer width = 503
integer height = 80
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 15793151
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_3 from statictext within w_costificazione_pf
integer x = 1554
integer y = 108
integer width = 338
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Versione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_costificazione_pf
integer x = 640
integer y = 108
integer width = 247
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_2 from singlelineedit within w_costificazione_pf
integer x = 1897
integer y = 108
integer width = 297
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_1 from singlelineedit within w_costificazione_pf
integer x = 891
integer y = 108
integer width = 663
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_mp from statictext within w_costificazione_pf
integer x = 366
integer y = 340
integer width = 503
integer height = 80
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 15780518
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_costificazione_pf
integer x = 2309
integer y = 100
integer width = 274
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESEGUI"
end type

event clicked;long   ll_p1, ll_p2, ll_return

string ls_p1, ls_p2, ls_messaggio

dec{4} ld_costo_mp, ld_costo_lav, ld_costo_tot

uo_costificazione_pf luo_1


luo_1 = create uo_costificazione_pf

title = "Costificazione PF - Elaborazione in corso..."

setpointer(hourglass!)

st_mp.text = "---"
st_lav.text = "---"
st_tot.text = "---"

if ddlb_1.text = "Preventivo" then
	ls_p1 = sle_1.text
	ls_p2 = sle_2.text
	ll_return = luo_1.uof_costificazione_pf(sqlca,s_cs_xx.cod_azienda,ls_p1,ls_p2,1,ld_costo_mp,ld_costo_lav,ld_costo_tot,ls_messaggio)
else
	ll_p1 = long(sle_1.text) // anno_comessa
	ll_p2 = long(sle_2.text) // num_commessa
	ll_return = luo_1.uof_costificazione_pf(sqlca,s_cs_xx.cod_azienda,ll_p1,ll_p2,0,ld_costo_mp,ld_costo_lav,ld_costo_tot,ls_messaggio)
end if

if ll_return = -1 then
	g_mb.messagebox("ERRORE",ls_messaggio)
	st_mp.text = ""
	st_lav.text = ""
	st_tot.text = ""
else
	st_mp.text = string(ld_costo_mp,"[currency]")
	st_lav.text = string(ld_costo_lav,"[currency]")
	st_tot.text = string(ld_costo_tot,"[currency]")
end if

title = "Costificazione PF"

setpointer(arrow!)
end event

type gb_1 from groupbox within w_costificazione_pf
integer x = 2263
integer y = 20
integer width = 366
integer height = 220
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "CALCOLO"
end type

type gb_2 from groupbox within w_costificazione_pf
integer x = 23
integer y = 260
integer width = 2606
integer height = 200
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "RISULTATI"
end type

type gb_3 from groupbox within w_costificazione_pf
integer x = 23
integer y = 20
integer width = 2217
integer height = 220
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "DATI INGRESSO"
end type

