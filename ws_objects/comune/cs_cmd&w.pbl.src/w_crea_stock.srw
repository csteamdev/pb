﻿$PBExportHeader$w_crea_stock.srw
$PBExportComments$Finestra Response Creazione Stock da Accettazione Materiali
forward
global type w_crea_stock from w_cs_xx_risposta
end type
type dw_crea_stock from uo_cs_xx_dw within w_crea_stock
end type
type cb_1 from commandbutton within w_crea_stock
end type
type cb_2 from commandbutton within w_crea_stock
end type
end forward

global type w_crea_stock from w_cs_xx_risposta
int Width=2327
int Height=1141
boolean TitleBar=true
string Title="Creazione Nuovo Stock"
dw_crea_stock dw_crea_stock
cb_1 cb_1
cb_2 cb_2
end type
global w_crea_stock w_crea_stock

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_crea_stock.set_dw_key("cod_azienda")
dw_crea_stock.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen+c_newonopen,c_default)


end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_crea_stock,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_crea_stock,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_crea_stock.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_crea_stock=create dw_crea_stock
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_crea_stock
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
end on

on w_crea_stock.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_crea_stock)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event pc_new;call super::pc_new;dw_crea_stock.setitem(dw_crea_stock.getrow(), "data_stock", datetime(today()))
end event

type dw_crea_stock from uo_cs_xx_dw within w_crea_stock
int X=23
int Y=21
int Width=2241
int Height=901
int TabOrder=10
string DataObject="d_crea_stock"
BorderStyle BorderStyle=StyleLowered!
end type

event updatestart;call super::updatestart;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
datetime ldt_data_stock, ldt_today
long   ll_prog_stock

SELECT max(stock.prog_stock)
INTO   :ll_prog_stock  
FROM   stock  
WHERE  ( stock.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( stock.cod_prodotto = :ls_cod_prodotto ) AND  
       ( stock.cod_deposito = :ls_cod_deposito ) AND  
       ( stock.cod_ubicazione = :ls_cod_ubicazione ) AND  
       ( stock.cod_lotto = :ls_cod_lotto ) AND  
       ( stock.data_stock = :ldt_data_stock ) ;

if sqlca.sqlcode = 0 then
   if isnull(ll_prog_stock) or (ll_prog_stock < 1) then
      ll_prog_stock = 1
   else
      ll_prog_stock++
   end if
   this.setitem(this.getrow(), "prog_stock", ll_prog_stock)
else
   this.setitem(this.getrow(), "prog_stock", 1)
end if

ldt_today = datetime(today())
this.setitem(this.getrow(), "prog_stock", 1)
if (s_cs_xx.parametri.parametro_data_1 < datetime(date(1901-01-01))) or isnull(s_cs_xx.parametri.parametro_data_1) then
   this.setitem(this.getrow(), "data_stock", datetime(today()))
else
   this.setitem(this.getrow(), "data_stock", s_cs_xx.parametri.parametro_data_1)
end if
this.setitem(this.getrow(), "prog_stock", 10)
this.triggerevent("itemchanged")


end event

on pcd_new;call uo_cs_xx_dw::pcd_new;this.setitem(this.getrow(), "cod_prodotto", s_cs_xx.parametri.parametro_s_10)

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type cb_1 from commandbutton within w_crea_stock
int X=1509
int Y=941
int Width=366
int Height=81
int TabOrder=20
string Text="&OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;datetime ldt_data_stock
long ll_prog_stock

w_crea_stock.triggerevent("pc_save")

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_crea_stock.getitemstring(dw_crea_stock.getrow(),"cod_deposito"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_crea_stock.getitemstring(dw_crea_stock.getrow(),"cod_ubicazione"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, dw_crea_stock.getitemstring(dw_crea_stock.getrow(),"cod_lotto"))
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_6, dw_crea_stock.getitemnumber(dw_crea_stock.getrow(),"quan_assegnata"))
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_7, dw_crea_stock.getitemnumber(dw_crea_stock.getrow(),"costo_medio"))

ldt_data_stock = dw_crea_stock.getitemdatetime(dw_crea_stock.getrow(),"data_stock")
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, ldt_data_stock)

ll_prog_stock = dw_crea_stock.getitemnumber(dw_crea_stock.getrow(),"prog_stock")
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ll_prog_stock)

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_crea_stock.triggerevent("pc_close")



end event

type cb_2 from commandbutton within w_crea_stock
int X=1898
int Y=941
int Width=366
int Height=81
int TabOrder=30
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;w_crea_stock.triggerevent("pc_close")
end on

