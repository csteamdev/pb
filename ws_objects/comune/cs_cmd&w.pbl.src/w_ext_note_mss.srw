﻿$PBExportHeader$w_ext_note_mss.srw
forward
global type w_ext_note_mss from w_cs_xx_risposta
end type
type st_1 from statictext within w_ext_note_mss
end type
type cb_2 from commandbutton within w_ext_note_mss
end type
type cb_1 from commandbutton within w_ext_note_mss
end type
type dw_1 from uo_cs_xx_dw within w_ext_note_mss
end type
end forward

global type w_ext_note_mss from w_cs_xx_risposta
integer width = 2674
integer height = 1364
string title = "NOTE"
boolean controlmenu = false
event ue_close ( )
st_1 st_1
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
end type
global w_ext_note_mss w_ext_note_mss

event ue_close();close(this)
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin)
save_on_close(c_socnosave)

dw_1.il_limit_campo_note = s_cs_xx.parametri.parametro_i_1

dw_1.set_dw_options(sqlca, &
							pcca.null_object, &
							c_newonopen + &
							c_nomodify + &
							c_nodelete + &
							c_noenablenewonopen + &
							c_noenablemodifyonopen + &
							c_scrollparent + &
							c_disablecc, &
							c_noresizedw + &
							c_nohighlightselected + &
							c_nocursorrowfocusrect + &
							c_nocursorrowpointer)

if s_cs_xx.parametri.parametro_i_1 > 0 then
	dw_1.object.note.edit.limit = s_cs_xx.parametri.parametro_i_1
end if


end event

on w_ext_note_mss.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_1
end on

on w_ext_note_mss.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

type st_1 from statictext within w_ext_note_mss
integer x = 23
integer y = 1168
integer width = 805
integer height = 84
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_ext_note_mss
integer x = 1856
integer y = 1168
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1)
close(parent)
end event

type cb_1 from commandbutton within w_ext_note_mss
integer x = 2245
integer y = 1168
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;dw_1.accepttext()

s_cs_xx.parametri.parametro_s_1 = dw_1.getitemstring(1,"note")

parent.postevent("ue_close")
end event

type dw_1 from uo_cs_xx_dw within w_ext_note_mss
integer x = 5
integer y = 12
integer width = 2633
integer height = 1156
integer taborder = 10
string dataobject = "d_ext_note_mss"
boolean border = false
end type

event pcd_new;call super::pcd_new;setitem(getrow(),"note",s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_s_1 = ""
resetupdate()
end event

event editchanged;call super::editchanged;dw_1.resetupdate()
st_1.text = string(len(data)) + "/" + string(this.object.note.edit.limit)


end event

event rbuttondown;// stefano 20/04/2010: Ticket 2010/95 MARR
// !!! TOLTO ANCESTOR SCRIPT !!!
// In questa finestra non deve apparire il menu di salvataggio.
// Salva alla chiusura della finestra!
return -1
end event

