﻿$PBExportHeader$w_import_righe_doc_excel.srw
forward
global type w_import_righe_doc_excel from window
end type
type dw_selezione from datawindow within w_import_righe_doc_excel
end type
end forward

global type w_import_righe_doc_excel from window
integer width = 2295
integer height = 864
boolean titlebar = true
string title = "Importazione Righe "
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_selezione dw_selezione
end type
global w_import_righe_doc_excel w_import_righe_doc_excel

type variables
long il_parametri[2]
end variables

on w_import_righe_doc_excel.create
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_selezione}
end on

on w_import_righe_doc_excel.destroy
destroy(this.dw_selezione)
end on

event open;il_parametri[1] = s_cs_xx.parametri.parametro_d_1
il_parametri[2] = s_cs_xx.parametri.parametro_d_2

setnull( s_cs_xx.parametri.parametro_d_1)
setnull( s_cs_xx.parametri.parametro_d_2)

dw_selezione.insertrow(0)

f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

type dw_selezione from datawindow within w_import_righe_doc_excel
integer x = 18
integer y = 20
integer width = 2231
integer height = 740
integer taborder = 10
string title = "none"
string dataobject = "d_import_righe_doc_excel"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string ls_path, docname[], ls_errore, ls_cod_tipo_det_ven
long ll_ret, ll_riga_partenza
uo_generazione_documenti luo_gen_docs

choose case dwo.name
	case "b_sfoglia"
			ll_ret = GetFileOpenName("Select File", ls_path, docname[], "XLS", "Excel (*.xls),*.XLS, All Files (*.*), *.*", "%userprofile%", 18)
			
			if ll_ret = 1 then
				dw_selezione.setitem(1, "path", ls_path)
			end if
			
	case "b_import"
		dw_selezione.accepttext()
		ls_cod_tipo_det_ven = dw_selezione.getitemstring(1, "cod_tipo_det_ven")
		ll_riga_partenza = dw_selezione.getitemnumber(1, "num_riga_partenza")
		ls_path = dw_selezione.getitemstring(1, "path")
		
		if isnull(ls_path) or len(ls_path) < 1 then return

		if isnull(ls_cod_tipo_det_ven) or len(ls_cod_tipo_det_ven) < 1 then return
		
		if isnull(ll_riga_partenza) or ll_riga_partenza < 1 then return
		
		luo_gen_docs = create uo_generazione_documenti
		ll_ret = luo_gen_docs.uof_import_righe_documento_excel(ls_path, ll_riga_partenza, "FAT_VEN", il_parametri[1],il_parametri[2] , ls_cod_tipo_det_ven, ls_errore)
		
		if ll_ret < 0 then
			rollback;
			g_mb.messagebox("Apice", ls_errore)
		else
			commit;
			g_mb.messagebox("Apice", "Caricamento righe eseguito con successo")
		end if
		
		destroy luo_gen_docs
	
end choose
end event

