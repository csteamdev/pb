﻿$PBExportHeader$w_prezzo_listini_um.srw
$PBExportComments$Finestra Gestione Prezzo Listini Nell'Unità di Misura Esterna
forward
global type w_prezzo_listini_um from Window
end type
type cb_annulla from commandbutton within w_prezzo_listini_um
end type
type dw_prezzo_listini_um from datawindow within w_prezzo_listini_um
end type
type cb_chiudi from commandbutton within w_prezzo_listini_um
end type
end forward

global type w_prezzo_listini_um from Window
int X=833
int Y=357
int Width=1719
int Height=745
boolean TitleBar=true
string Title="Prezzo Nell'Unità di Misura Esterna"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_annulla cb_annulla
dw_prezzo_listini_um dw_prezzo_listini_um
cb_chiudi cb_chiudi
end type
global w_prezzo_listini_um w_prezzo_listini_um

event open;double ld_prezzo, ld_fat_conv


if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then
	dw_prezzo_listini_um.modify("st_label_quantita.text= 'Quantità " + s_cs_xx.parametri.parametro_s_1 + "'")
	dw_prezzo_listini_um.modify("st_label_prezzo.text= 'Prezzi al " + s_cs_xx.parametri.parametro_s_1 + "'")
else
	dw_prezzo_listini_um.modify("st_label_prezzo.text= 'Prezzi'")	
	dw_prezzo_listini_um.modify("st_label_quantita.text= 'Quantità'")	
end if

dw_prezzo_listini_um.insertrow(1)
dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "prezzo_1", s_cs_xx.parametri.parametro_d_1 / s_cs_xx.parametri.parametro_d_6)
dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "prezzo_2", s_cs_xx.parametri.parametro_d_2 / s_cs_xx.parametri.parametro_d_6)
dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "prezzo_3", s_cs_xx.parametri.parametro_d_3 / s_cs_xx.parametri.parametro_d_6)
dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "prezzo_4", s_cs_xx.parametri.parametro_d_4 / s_cs_xx.parametri.parametro_d_6)
dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "prezzo_5", s_cs_xx.parametri.parametro_d_5 / s_cs_xx.parametri.parametro_d_6)

if s_cs_xx.parametri.parametro_d_7 <> 99999999.9999 then
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_1", s_cs_xx.parametri.parametro_d_7 * s_cs_xx.parametri.parametro_d_6)
else
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_1", s_cs_xx.parametri.parametro_d_7)
end if
if s_cs_xx.parametri.parametro_d_8 <> 99999999.9999 then
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_2", s_cs_xx.parametri.parametro_d_8 * s_cs_xx.parametri.parametro_d_6)
else
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_2", s_cs_xx.parametri.parametro_d_8)
end if
if s_cs_xx.parametri.parametro_d_9 <> 99999999.9999 then
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_3", s_cs_xx.parametri.parametro_d_9 * s_cs_xx.parametri.parametro_d_6)
else
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_3", s_cs_xx.parametri.parametro_d_9)
end if
if s_cs_xx.parametri.parametro_d_10 <> 99999999.9999 then
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_4", s_cs_xx.parametri.parametro_d_10 * s_cs_xx.parametri.parametro_d_6)
else
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_4", s_cs_xx.parametri.parametro_d_10)
end if
if s_cs_xx.parametri.parametro_d_11 <> 99999999.9999 then
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_5", s_cs_xx.parametri.parametro_d_11 * s_cs_xx.parametri.parametro_d_6)
else
	dw_prezzo_listini_um.setitem(dw_prezzo_listini_um.getrow(), "quantita_5", s_cs_xx.parametri.parametro_d_11)
end if

end event

on w_prezzo_listini_um.create
this.cb_annulla=create cb_annulla
this.dw_prezzo_listini_um=create dw_prezzo_listini_um
this.cb_chiudi=create cb_chiudi
this.Control[]={ this.cb_annulla,&
this.dw_prezzo_listini_um,&
this.cb_chiudi}
end on

on w_prezzo_listini_um.destroy
destroy(this.cb_annulla)
destroy(this.dw_prezzo_listini_um)
destroy(this.cb_chiudi)
end on

type cb_annulla from commandbutton within w_prezzo_listini_um
event clicked pbm_bnclicked
int X=1326
int Y=561
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_d_6 = 0
close(parent)
end event

type dw_prezzo_listini_um from datawindow within w_prezzo_listini_um
int X=23
int Y=21
int Width=1669
int Height=521
int TabOrder=10
string DataObject="d_prezzo_listini_um"
boolean LiveScroll=true
end type

type cb_chiudi from commandbutton within w_prezzo_listini_um
event clicked pbm_bnclicked
int X=938
int Y=561
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;boolean lb_diverso


lb_diverso = false

dw_prezzo_listini_um.setcolumn(1)
if double(dw_prezzo_listini_um.gettext()) <> 99999999.9999 then
	if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_7 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_7 = dw_prezzo_listini_um.getitemnumber(1,"quantita_1") / s_cs_xx.parametri.parametro_d_6
	end if
else
	if double(dw_prezzo_listini_um.gettext()) <> s_cs_xx.parametri.parametro_d_7 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_7 = dw_prezzo_listini_um.getitemnumber(1,"quantita_1")
	end if
end if

dw_prezzo_listini_um.setcolumn(2)
if double(dw_prezzo_listini_um.gettext()) <> 99999999.9999 then
	if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_8 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_8 = dw_prezzo_listini_um.getitemnumber(1,"quantita_2") / s_cs_xx.parametri.parametro_d_6
	end if
else
	if double(dw_prezzo_listini_um.gettext()) <> s_cs_xx.parametri.parametro_d_8 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_8 = dw_prezzo_listini_um.getitemnumber(1,"quantita_2")
	end if
end if

dw_prezzo_listini_um.setcolumn(3)
if double(dw_prezzo_listini_um.gettext()) <> 99999999.9999 then
	if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_9 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_9 = dw_prezzo_listini_um.getitemnumber(1,"quantita_3") / s_cs_xx.parametri.parametro_d_6
	end if
else
	if double(dw_prezzo_listini_um.gettext()) <> s_cs_xx.parametri.parametro_d_9 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_9 = dw_prezzo_listini_um.getitemnumber(1,"quantita_3")
	end if
end if

dw_prezzo_listini_um.setcolumn(4)
if double(dw_prezzo_listini_um.gettext()) <> 99999999.9999 then
	if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_10 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_10 = dw_prezzo_listini_um.getitemnumber(1,"quantita_4") / s_cs_xx.parametri.parametro_d_6
	end if
else
	if double(dw_prezzo_listini_um.gettext()) <> s_cs_xx.parametri.parametro_d_10 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_10 = dw_prezzo_listini_um.getitemnumber(1,"quantita_4")
	end if
end if

dw_prezzo_listini_um.setcolumn(5)
if double(dw_prezzo_listini_um.gettext()) <> 99999999.9999 then
	if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_11 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_11 = dw_prezzo_listini_um.getitemnumber(1,"quantita_5") / s_cs_xx.parametri.parametro_d_6
	end if
else
	if double(dw_prezzo_listini_um.gettext()) <> s_cs_xx.parametri.parametro_d_11 then
		lb_diverso = true
		s_cs_xx.parametri.parametro_d_11 = dw_prezzo_listini_um.getitemnumber(1,"quantita_5")
	end if
end if

dw_prezzo_listini_um.setcolumn(6)
if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_1 then
	lb_diverso = true
	s_cs_xx.parametri.parametro_d_1 = dw_prezzo_listini_um.getitemnumber(1,"prezzo_1") * s_cs_xx.parametri.parametro_d_6
end if

dw_prezzo_listini_um.setcolumn(7)
if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_2 then
	lb_diverso = true
	s_cs_xx.parametri.parametro_d_2 = dw_prezzo_listini_um.getitemnumber(1,"prezzo_2") * s_cs_xx.parametri.parametro_d_6
end if

dw_prezzo_listini_um.setcolumn(8)
if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_3 then
	lb_diverso = true
	s_cs_xx.parametri.parametro_d_3 = dw_prezzo_listini_um.getitemnumber(1,"prezzo_3") * s_cs_xx.parametri.parametro_d_6
end if

dw_prezzo_listini_um.setcolumn(9)
if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_4 then
	lb_diverso = true
	s_cs_xx.parametri.parametro_d_4 = dw_prezzo_listini_um.getitemnumber(1,"prezzo_4") * s_cs_xx.parametri.parametro_d_6
end if

dw_prezzo_listini_um.setcolumn(10)
if double(dw_prezzo_listini_um.gettext()) / s_cs_xx.parametri.parametro_d_6 <> s_cs_xx.parametri.parametro_d_5 then
	lb_diverso = true
	s_cs_xx.parametri.parametro_d_5 = dw_prezzo_listini_um.getitemnumber(1,"prezzo_5") * s_cs_xx.parametri.parametro_d_6
end if

if not lb_diverso then
	s_cs_xx.parametri.parametro_d_6 = 0
end if

close(parent)
end event

