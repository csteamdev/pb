﻿$PBExportHeader$w_prezzo_um.srw
$PBExportComments$Finestra Gestione Prezzo Nell'Unità di Misura Esterna
forward
global type w_prezzo_um from window
end type
type cb_annulla from commandbutton within w_prezzo_um
end type
type dw_prezzo_um from datawindow within w_prezzo_um
end type
type cb_chiudi from commandbutton within w_prezzo_um
end type
end forward

global type w_prezzo_um from window
integer x = 832
integer y = 356
integer width = 1353
integer height = 420
boolean titlebar = true
string title = "Prezzo Nell~'Unità di Misura Esterna"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
cb_annulla cb_annulla
dw_prezzo_um dw_prezzo_um
cb_chiudi cb_chiudi
end type
global w_prezzo_um w_prezzo_um

event open;double ld_prezzo, ld_fat_conv


if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then
	dw_prezzo_um.modify("st_label_prezzo.text= 'Prezzo al " + s_cs_xx.parametri.parametro_s_1 + ":'")
	dw_prezzo_um.modify("st_label_quantita.text= 'Quantità " + s_cs_xx.parametri.parametro_s_1 + ":'")
else
	dw_prezzo_um.modify("st_label_prezzo.text= 'Prezzo:'")	
	dw_prezzo_um.modify("st_label_quantita.text= 'Quantità:'")	
end if

dw_prezzo_um.insertrow(1)
dw_prezzo_um.setitem(dw_prezzo_um.getrow(), "quantita", s_cs_xx.parametri.parametro_d_3 * s_cs_xx.parametri.parametro_d_2)
//dw_prezzo_um.insertrow(2)
dw_prezzo_um.setitem(dw_prezzo_um.getrow(), "prezzo", s_cs_xx.parametri.parametro_d_1 / s_cs_xx.parametri.parametro_d_2)

dw_prezzo_um.setcolumn("prezzo")
dw_prezzo_um.SelectText(1, Len(dw_prezzo_um.GetText()))
end event

on w_prezzo_um.create
this.cb_annulla=create cb_annulla
this.dw_prezzo_um=create dw_prezzo_um
this.cb_chiudi=create cb_chiudi
this.Control[]={this.cb_annulla,&
this.dw_prezzo_um,&
this.cb_chiudi}
end on

on w_prezzo_um.destroy
destroy(this.cb_annulla)
destroy(this.dw_prezzo_um)
destroy(this.cb_chiudi)
end on

type cb_annulla from commandbutton within w_prezzo_um
event clicked pbm_bnclicked
integer x = 960
integer y = 240
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_3 = 0
close(parent)
end event

type dw_prezzo_um from datawindow within w_prezzo_um
integer x = 23
integer y = 20
integer width = 1303
integer height = 200
integer taborder = 10
string dataobject = "d_prezzo_um"
boolean livescroll = true
end type

type cb_chiudi from commandbutton within w_prezzo_um
event clicked pbm_bnclicked
integer x = 571
integer y = 240
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean default = true
end type

event clicked;dw_prezzo_um.setcolumn(1)

if double(dw_prezzo_um.gettext()) / s_cs_xx.parametri.parametro_d_2 <> s_cs_xx.parametri.parametro_d_3 then
	s_cs_xx.parametri.parametro_d_3 = dw_prezzo_um.getitemnumber(1,"quantita") / s_cs_xx.parametri.parametro_d_2
else
	s_cs_xx.parametri.parametro_d_3 = 0
end if

dw_prezzo_um.setcolumn(2)

if double(dw_prezzo_um.gettext()) / s_cs_xx.parametri.parametro_d_2 <> s_cs_xx.parametri.parametro_d_1 then
	s_cs_xx.parametri.parametro_d_1 = dw_prezzo_um.getitemnumber(1,"prezzo") * s_cs_xx.parametri.parametro_d_2
else
	s_cs_xx.parametri.parametro_d_1 = 0
end if

close(parent)
end event

