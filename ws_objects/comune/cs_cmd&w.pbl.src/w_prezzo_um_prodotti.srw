﻿$PBExportHeader$w_prezzo_um_prodotti.srw
$PBExportComments$Finestra Gestione Prezzo Prodotti Nell'Unità di Misura Esterna
forward
global type w_prezzo_um_prodotti from Window
end type
type cb_annulla from commandbutton within w_prezzo_um_prodotti
end type
type dw_prezzo_um_prodotti from datawindow within w_prezzo_um_prodotti
end type
type cb_chiudi from commandbutton within w_prezzo_um_prodotti
end type
end forward

global type w_prezzo_um_prodotti from Window
int X=833
int Y=357
int Width=1354
int Height=345
boolean TitleBar=true
string Title="Prezzo Nell'Unità di Misura Esterna"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_annulla cb_annulla
dw_prezzo_um_prodotti dw_prezzo_um_prodotti
cb_chiudi cb_chiudi
end type
global w_prezzo_um_prodotti w_prezzo_um_prodotti

event open;if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then
	dw_prezzo_um_prodotti.modify("st_label_prezzo.text= 'Prezzo al " + s_cs_xx.parametri.parametro_s_1 + ":'")
else
	dw_prezzo_um_prodotti.modify("st_label_prezzo.text= 'Prezzo:'")	
end if

dw_prezzo_um_prodotti.insertrow(1)
dw_prezzo_um_prodotti.setitem(dw_prezzo_um_prodotti.getrow(), "prezzo", s_cs_xx.parametri.parametro_d_1 / s_cs_xx.parametri.parametro_d_2)

end event

on w_prezzo_um_prodotti.create
this.cb_annulla=create cb_annulla
this.dw_prezzo_um_prodotti=create dw_prezzo_um_prodotti
this.cb_chiudi=create cb_chiudi
this.Control[]={ this.cb_annulla,&
this.dw_prezzo_um_prodotti,&
this.cb_chiudi}
end on

on w_prezzo_um_prodotti.destroy
destroy(this.cb_annulla)
destroy(this.dw_prezzo_um_prodotti)
destroy(this.cb_chiudi)
end on

type cb_annulla from commandbutton within w_prezzo_um_prodotti
event clicked pbm_bnclicked
int X=961
int Y=161
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = 0
s_cs_xx.parametri.parametro_d_3 = 0
close(parent)
end event

type dw_prezzo_um_prodotti from datawindow within w_prezzo_um_prodotti
int X=23
int Y=21
int Width=1303
int Height=121
int TabOrder=10
string DataObject="d_prezzo_um_prodotti"
boolean LiveScroll=true
end type

type cb_chiudi from commandbutton within w_prezzo_um_prodotti
event clicked pbm_bnclicked
int X=572
int Y=161
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_prezzo_um_prodotti.setcolumn(1)

if double(dw_prezzo_um_prodotti.gettext()) / s_cs_xx.parametri.parametro_d_2 <> s_cs_xx.parametri.parametro_d_1 then
	s_cs_xx.parametri.parametro_d_1 = dw_prezzo_um_prodotti.getitemnumber(1,"prezzo") * s_cs_xx.parametri.parametro_d_2
else
	s_cs_xx.parametri.parametro_d_1 = 0
end if

close(parent)
end event

