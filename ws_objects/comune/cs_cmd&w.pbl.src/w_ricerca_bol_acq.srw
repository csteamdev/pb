﻿$PBExportHeader$w_ricerca_bol_acq.srw
$PBExportComments$Finestra Ricerca Bolla di Acquisto
forward
global type w_ricerca_bol_acq from w_cs_xx_risposta
end type
type dw_ricerca_bolla_acq from uo_dw_main within w_ricerca_bol_acq
end type
type cb_1 from uo_cb_accept within w_ricerca_bol_acq
end type
type cb_2 from uo_cb_close within w_ricerca_bol_acq
end type
type cb_azzera from commandbutton within w_ricerca_bol_acq
end type
end forward

global type w_ricerca_bol_acq from w_cs_xx_risposta
int Width=3260
int Height=741
boolean TitleBar=true
string Title="Ricerca Bolle di Acquisto"
dw_ricerca_bolla_acq dw_ricerca_bolla_acq
cb_1 cb_1
cb_2 cb_2
cb_azzera cb_azzera
end type
global w_ricerca_bol_acq w_ricerca_bol_acq

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_ricerca_bolla_acq.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCInsert+&
                                    c_nonew+c_nomodify+c_nodelete,c_default)

end on

on w_ricerca_bol_acq.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_ricerca_bolla_acq=create dw_ricerca_bolla_acq
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_azzera=create cb_azzera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_ricerca_bolla_acq
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
this.Control[iCurrent+4]=cb_azzera
end on

on w_ricerca_bol_acq.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_ricerca_bolla_acq)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_azzera)
end on

type dw_ricerca_bolla_acq from uo_dw_main within w_ricerca_bol_acq
int X=23
int Y=21
int Width=3178
int Height=501
int TabOrder=20
string DataObject="d_ricerca_bolla_acq"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_dw_main::pcd_retrieve;LONG  l_Error

if isnull(s_cs_xx.parametri.parametro_s_10) then s_cs_xx.parametri.parametro_s_10 = "%"
if isnull(s_cs_xx.parametri.parametro_s_11) then s_cs_xx.parametri.parametro_s_11 = "%"

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_s_10 , &
                   s_cs_xx.parametri.parametro_d_1 , &
                   s_cs_xx.parametri.parametro_s_11)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_1.triggerevent("Clicked")
   end if
end if
end on

type cb_1 from uo_cb_accept within w_ricerca_bol_acq
int X=2835
int Y=541
int Width=366
int Height=81
int TabOrder=30
string Text="&OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long ll_prog_stock

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, int(dw_ricerca_bolla_acq.getitemnumber(dw_ricerca_bolla_acq.getrow(),"anno_bolla_acq")))

//---------------------- Modifica Nicola ------------------------------------------

if s_cs_xx.parametri.parametro_d_5 <> 1 then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_ricerca_bolla_acq.getitemstring(dw_ricerca_bolla_acq.getrow(),"num_bolla_acq"))
else 
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, long(dw_ricerca_bolla_acq.getitemstring(dw_ricerca_bolla_acq.getrow(),"num_bolla_acq")))	
end if

//------------------------ Fine Modifica ------------------------------------------


s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, int(dw_ricerca_bolla_acq.getitemnumber(dw_ricerca_bolla_acq.getrow(),"prog_riga_bolla_acq")))

s_cs_xx.parametri.parametro_d_5 = 0

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_bol_acq.triggerevent("pc_close")
end event

type cb_2 from uo_cb_close within w_ricerca_bol_acq
int X=2446
int Y=541
int Width=366
int Height=81
int TabOrder=10
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_azzera from commandbutton within w_ricerca_bol_acq
int X=2058
int Y=541
int Width=366
int Height=81
int TabOrder=40
string Text="Azzera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long ll_null

setnull(ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ll_null)

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_bol_acq.postevent("pc_close")
end on

