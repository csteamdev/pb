﻿$PBExportHeader$w_ricerca_collaudo.srw
$PBExportComments$Finestra Ricerca Dati Collaudo
forward
global type w_ricerca_collaudo from w_cs_xx_risposta
end type
type dw_ricerca_collaudo from uo_dw_main within w_ricerca_collaudo
end type
type cb_ok from uo_cb_accept within w_ricerca_collaudo
end type
type cb_chiudi from uo_cb_close within w_ricerca_collaudo
end type
type cb_azzera from commandbutton within w_ricerca_collaudo
end type
end forward

global type w_ricerca_collaudo from w_cs_xx_risposta
integer width = 2848
integer height = 744
string title = "Ricerca Registrazioni Collaudi"
dw_ricerca_collaudo dw_ricerca_collaudo
cb_ok cb_ok
cb_chiudi cb_chiudi
cb_azzera cb_azzera
end type
global w_ricerca_collaudo w_ricerca_collaudo

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca_collaudo,"cod_test",sqlca,&
                 "tab_test","cod_test","des_test","")

end on

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_ricerca_collaudo.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

end on

on w_ricerca_collaudo.create
int iCurrent
call super::create
this.dw_ricerca_collaudo=create dw_ricerca_collaudo
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
this.cb_azzera=create cb_azzera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_collaudo
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.cb_azzera
end on

on w_ricerca_collaudo.destroy
call super::destroy
destroy(this.dw_ricerca_collaudo)
destroy(this.cb_ok)
destroy(this.cb_chiudi)
destroy(this.cb_azzera)
end on

type dw_ricerca_collaudo from uo_dw_main within w_ricerca_collaudo
integer x = 23
integer y = 20
integer width = 2766
integer height = 500
integer taborder = 20
string dataobject = "d_ricerca_collaudo"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
datetime ldt_da_data, ldt_a_data

ldt_da_data = datetime(date("1900/01/01"))
ldt_a_data = datetime(date("2999/12/31"))

if isnull(s_cs_xx.parametri.parametro_s_10) then
   if isnull(s_cs_xx.parametri.parametro_s_11) then
      l_Error = Retrieve(s_cs_xx.cod_azienda, &
                         "%", &
                         "%", &
                         "%", &
                         "%", &
                         ldt_da_data, &
                         ldt_a_data, &
                         0, &
                         999999)
   else
      l_Error = Retrieve(s_cs_xx.cod_azienda, &
                         "%", &
                         s_cs_xx.parametri.parametro_s_11, &
                         s_cs_xx.parametri.parametro_s_12, &
                         s_cs_xx.parametri.parametro_s_13, &
                         s_cs_xx.parametri.parametro_data_1, &
                         s_cs_xx.parametri.parametro_data_1, &
                         s_cs_xx.parametri.parametro_d_1, &
                         s_cs_xx.parametri.parametro_d_1)
   end if
else
   if isnull(s_cs_xx.parametri.parametro_s_11) then
      l_Error = Retrieve(s_cs_xx.cod_azienda, &
                         s_cs_xx.parametri.parametro_s_10, &
                         "%", &
                         "%", &
                         "%", &
                         ldt_da_data, &
                         ldt_a_data, &
                         0, &
                         999999)
   else
      l_Error = Retrieve(s_cs_xx.cod_azienda, &
                         s_cs_xx.parametri.parametro_s_10, &
                         s_cs_xx.parametri.parametro_s_11, &
                         s_cs_xx.parametri.parametro_s_12, &
                         s_cs_xx.parametri.parametro_s_13, &
                         s_cs_xx.parametri.parametro_data_1, &
                         s_cs_xx.parametri.parametro_data_1, &
                         s_cs_xx.parametri.parametro_d_1, &
                         s_cs_xx.parametri.parametro_d_1)
   end if
end if

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_ok.triggerevent("Clicked")
   end if
end if
end on

type cb_ok from uo_cb_accept within w_ricerca_collaudo
integer x = 2423
integer y = 540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;call super::clicked;if dw_ricerca_collaudo.rowcount() < 1 then
	return -1
end if

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, int(dw_ricerca_collaudo.getitemnumber(dw_ricerca_collaudo.getrow(),"anno_registrazione")))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, int(dw_ricerca_collaudo.getitemnumber(dw_ricerca_collaudo.getrow(),"num_registrazione")))

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_collaudo.triggerevent("pc_close")

end event

type cb_chiudi from uo_cb_close within w_ricerca_collaudo
integer x = 2034
integer y = 540
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cb_azzera from commandbutton within w_ricerca_collaudo
integer x = 1646
integer y = 540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

on clicked;long ll_null

setnull(ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
cb_chiudi.postevent("clicked")
end on

