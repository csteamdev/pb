﻿$PBExportHeader$w_ricerca_commesse.srw
forward
global type w_ricerca_commesse from w_cs_xx_principale
end type
type st_trovati from statictext within w_ricerca_commesse
end type
type st_1 from statictext within w_ricerca_commesse
end type
type dw_stringa_ricerca_prodotti from datawindow within w_ricerca_commesse
end type
type cb_rag_soc from commandbutton within w_ricerca_commesse
end type
type cb_cod_cliente from commandbutton within w_ricerca_commesse
end type
type cb_des_prodotto from commandbutton within w_ricerca_commesse
end type
type cb_cod_prodotto from commandbutton within w_ricerca_commesse
end type
type cb_numero from commandbutton within w_ricerca_commesse
end type
type cb_anno from commandbutton within w_ricerca_commesse
end type
type cb_chiudi from commandbutton within w_ricerca_commesse
end type
type cb_ok from commandbutton within w_ricerca_commesse
end type
type st_ricerca_per from statictext within w_ricerca_commesse
end type
type dw_ricerca_commesse from datawindow within w_ricerca_commesse
end type
end forward

global type w_ricerca_commesse from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3602
integer height = 1828
string title = "Ricerca Commesse"
st_trovati st_trovati
st_1 st_1
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
cb_rag_soc cb_rag_soc
cb_cod_cliente cb_cod_cliente
cb_des_prodotto cb_des_prodotto
cb_cod_prodotto cb_cod_prodotto
cb_numero cb_numero
cb_anno cb_anno
cb_chiudi cb_chiudi
cb_ok cb_ok
st_ricerca_per st_ricerca_per
dw_ricerca_commesse dw_ricerca_commesse
end type
global w_ricerca_commesse w_ricerca_commesse

type variables
integer ii_tipo_ricerca
long il_row
end variables

forward prototypes
public function integer wf_carica ()
end prototypes

public function integer wf_carica ();integer li_anno_commessa,li_anno_reg,li_anno_registrazione
long    ll_num_commessa,ll_num_reg,ll_row,ll_i,ll_y,ll_num_registrazione
string  ls_cod_prodotto,ls_des_prodotto,ls_cod_cliente,ls_rag_soc,ls_ricerca

ls_ricerca = dw_stringa_ricerca_prodotti.gettext()

ll_i = len(ls_ricerca)

for ll_y = 1 to ll_i
	if mid(ls_ricerca, ll_y, 1) = "*" then
		ls_ricerca = replace(ls_ricerca, ll_y, 1, "%")
	end if
next

dw_ricerca_commesse.reset()
dw_ricerca_commesse.setredraw(false)

choose case ii_tipo_ricerca
	case 1			// ricerca per anno commessa
		li_anno_commessa = integer(ls_ricerca)
		
		declare r_anag_commesse cursor for
		select num_commessa,
				 cod_prodotto
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa;
		
		open r_anag_commesse;
		
		do while 1 = 1 
			fetch r_anag_commesse
			into  :ll_num_commessa,
					:ls_cod_prodotto;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_anag_commesse;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_anag_commesse;
				return -1
			end if
			
			select anno_registrazione,
					 num_registrazione
			into   :li_anno_reg,
					 :ll_num_reg
			from   det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_anag_commesse;
				return -1
			end if
			
			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:li_anno_reg
			and    num_registrazione=:ll_num_reg;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_anag_commesse;
				return -1
			end if
			
			select rag_soc_1
			into   :ls_rag_soc
			from   anag_clienti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_cliente=:ls_cod_cliente;
			
			ll_row=dw_ricerca_commesse.insertrow(0)
			st_trovati.text = string(ll_row)
			
			dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
			dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
			dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
			dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)
			setnull(li_anno_reg)
			setnull(ll_num_reg)
			ls_cod_cliente = ""
			ls_rag_soc=""
		loop
		
		close r_anag_commesse;
		
	case 2			// ricerca per numero commessa
		ll_num_commessa = long(ls_ricerca)
		
		declare r2_anag_commesse cursor for
		select anno_commessa,
				 cod_prodotto
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    num_commessa=:ll_num_commessa;
		
		open r2_anag_commesse;
		
		do while 1 = 1 
			fetch r2_anag_commesse
			into  :li_anno_commessa,
					:ls_cod_prodotto;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r2_anag_commesse;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r2_anag_commesse;
				return -1
			end if
			
			select anno_registrazione,
					 num_registrazione
			into   :li_anno_reg,
					 :ll_num_reg
			from   det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r2_anag_commesse;
				return -1
			end if
			
			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:li_anno_reg
			and    num_registrazione=:ll_num_reg;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r2_anag_commesse;
				return -1
			end if
			
			select rag_soc_1
			into   :ls_rag_soc
			from   anag_clienti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_cliente=:ls_cod_cliente;
			
			ll_row=dw_ricerca_commesse.insertrow(0)
			st_trovati.text = string(ll_row)
			
			dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
			dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
			dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
			dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)
			
			setnull(li_anno_reg)
			setnull(ll_num_reg)
			ls_cod_cliente=""
			ls_rag_soc=""
			
		loop
		
		close r2_anag_commesse;	
	
	case 3		// ricerca per  codice prodotto
			
		ls_cod_prodotto = ls_ricerca
		
		if pos(ls_cod_prodotto, "%") = 0 then
			ls_cod_prodotto += "%"
		end if
	
		declare r3_anag_commesse cursor for
		select anno_commessa,
				 num_commessa,
				 cod_prodotto
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto like :ls_cod_prodotto;
	
		open r3_anag_commesse;
		
		do while 1 = 1 
			fetch r3_anag_commesse
			into  :li_anno_commessa,
					:ll_num_commessa,
					:ls_cod_prodotto;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r3_anag_commesse;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select anno_registrazione,
					 num_registrazione
			into   :li_anno_reg,
					 :ll_num_reg
			from   det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r3_anag_commesse;
				return -1
			end if
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				return -1
			end if

			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:li_anno_reg
			and    num_registrazione=:ll_num_reg;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r3_anag_commesse;
				return -1
			end if
			
			select rag_soc_1
			into   :ls_rag_soc
			from   anag_clienti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_cliente=:ls_cod_cliente;
			
			ll_row=dw_ricerca_commesse.insertrow(0)
			st_trovati.text = string(ll_row)
			
			dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
			dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
			dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
			dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)

			setnull(li_anno_reg)
			setnull(ll_num_reg)
			ls_cod_cliente=""
			ls_rag_soc=""
			
		loop
		
		close r3_anag_commesse;	
	

	case 4		// ricerca per descrizionbe prodotto 
	
		if pos(ls_ricerca, "%") = 0 then
			ls_ricerca += "%"
		end if
		
		declare r4_anag_commesse cursor for
		select c.anno_commessa,
				 c.num_commessa,
				 c.cod_prodotto,
				 p.des_prodotto
		from   anag_commesse c, 
				 anag_prodotti p
		where  c.cod_azienda=:s_cs_xx.cod_azienda
		and    p.cod_azienda=:s_cs_xx.cod_azienda
		and    c.cod_prodotto=p.cod_prodotto
		and    p.des_prodotto like :ls_ricerca;
	
		open r4_anag_commesse;
		
		do while 1 = 1 
			fetch r4_anag_commesse
			into  :li_anno_commessa,
					:ll_num_commessa,
					:ls_cod_prodotto,
					:ls_des_prodotto;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r4_anag_commesse;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select anno_registrazione,
					 num_registrazione
			into   :li_anno_reg,
					 :ll_num_reg
			from   det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r4_anag_commesse;
				return -1
			end if
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r4_anag_commesse;
				return -1
			end if

			select cod_cliente
			into   :ls_cod_cliente
			from   tes_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:li_anno_reg
			and    num_registrazione=:ll_num_reg;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r4_anag_commesse;
				return -1
			end if
			
			select rag_soc_1
			into   :ls_rag_soc
			from   anag_clienti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_cliente=:ls_cod_cliente;
			
			ll_row=dw_ricerca_commesse.insertrow(0)
			st_trovati.text = string(ll_row)
			
			dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
			dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
			dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
			dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)
			
			setnull(li_anno_reg)
			setnull(ll_num_reg)
			ls_cod_prodotto=""
			ls_des_prodotto=""
			ls_cod_cliente=""
			ls_rag_soc=""
			
		loop
		
		close r4_anag_commesse;	
	
	case 5			// ricerca per  codice cliente
	
		if pos(ls_ricerca, "%") = 0 then
			ls_ricerca += "%"
		end if
		
		declare r_ordini cursor for
		select t.cod_cliente,
				 d.anno_commessa,
				 d.num_commessa
		from   tes_ord_ven t, 
				 det_ord_ven d
		where  t.cod_azienda=:s_cs_xx.cod_azienda
		and    d.cod_azienda=:s_cs_xx.cod_azienda
		and    d.anno_registrazione = t.anno_registrazione
		and    d.num_registrazione = t.num_registrazione
		and    t.cod_cliente like :ls_ricerca
		and    d.anno_commessa is not null;
	
		open r_ordini;
		
		do while 1 = 1 
			fetch r_ordini
			into  :ls_cod_cliente,
					:li_anno_commessa,
					:ll_num_commessa;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_ordini;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			select cod_prodotto
			into   :ls_cod_prodotto
			from   anag_commesse
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_ordini;
				return -1
			end if

			if sqlca.sqlcode = 100 then continue
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r_ordini;
				return -1
			end if
			
			select rag_soc_1
			into   :ls_rag_soc
			from   anag_clienti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_cliente=:ls_cod_cliente;
			
			ll_row=dw_ricerca_commesse.insertrow(0)
			st_trovati.text = string(ll_row)
			
			dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
			dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
			dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
			dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
			dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)

			ls_cod_prodotto=""
			ls_des_prodotto=""
			ls_cod_cliente=""
			ls_rag_soc=""
			
		loop
		
		close r_ordini;	
		
	case 6			// ricerca per  ragione_sociale

		if pos(ls_ricerca, "%") = 0 then
			ls_ricerca += "%"
		end if
		
		declare r2_ordini cursor for
		select a.rag_soc_1,
				 t.cod_cliente,
				 t.anno_registrazione,
				 t.num_registrazione
		from   tes_ord_ven t, 
				 anag_clienti a
		where  t.cod_azienda=:s_cs_xx.cod_azienda
		and    a.cod_azienda=:s_cs_xx.cod_azienda
		and    t.cod_cliente=a.cod_cliente
		and    a.rag_soc_1 like :ls_ricerca;
			
		open r2_ordini;
		
		do while 1 = 1 
			fetch r2_ordini
			into  :ls_rag_soc,
					:ls_cod_cliente,
					:li_anno_registrazione,
					:ll_num_registrazione;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
				close r2_ordini;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			
			declare r_det_ordini cursor for
			select  cod_prodotto,
					  anno_commessa,
					  num_commessa
			from    det_ord_ven
			where   cod_azienda=:s_cs_xx.cod_azienda
			and     anno_registrazione=:li_anno_registrazione
			and     num_registrazione=:ll_num_registrazione;

			open r_det_ordini;
			
			do while 1 = 1 
				fetch r_det_ordini
				into  :ls_cod_prodotto,
						:li_anno_commessa,
						:ll_num_commessa;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
					close r_det_ordini;
					return -1
				end if
				
				if sqlca.sqlcode = 100 then exit
	
				select cod_prodotto
				into   :ls_cod_prodotto
				from   anag_commesse
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_commessa=:li_anno_commessa
				and    num_commessa=:ll_num_commessa;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
					close r_det_ordini;
					return -1
				end if
	
				if sqlca.sqlcode = 100 then continue
				
				select des_prodotto
				into   :ls_des_prodotto
				from   anag_prodotti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice","Errore sul db: " + sqlca.sqlerrtext,stopsign!)
					close r_det_ordini;
					return -1
				end if
							
				ll_row=dw_ricerca_commesse.insertrow(0)
				st_trovati.text = string(ll_row)
				
				dw_ricerca_commesse.setitem(ll_row,"anno_commessa",li_anno_commessa)
				dw_ricerca_commesse.setitem(ll_row,"num_commessa",ll_num_commessa)
				dw_ricerca_commesse.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
				dw_ricerca_commesse.setitem(ll_row,"des_prodotto",ls_des_prodotto)
				dw_ricerca_commesse.setitem(ll_row,"cod_cliente",ls_cod_cliente)
				dw_ricerca_commesse.setitem(ll_row,"rag_soc",ls_rag_soc)
				
			loop
			
			close r_det_ordini;
			
		loop
		
		close r2_ordini;	
end choose

st_trovati.text = string(ll_row)
dw_ricerca_commesse.setredraw(true)

rollback;		// chiudo cursori e libero eventuali locks per ASE.

return 0
end function

on w_ricerca_commesse.create
int iCurrent
call super::create
this.st_trovati=create st_trovati
this.st_1=create st_1
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.cb_rag_soc=create cb_rag_soc
this.cb_cod_cliente=create cb_cod_cliente
this.cb_des_prodotto=create cb_des_prodotto
this.cb_cod_prodotto=create cb_cod_prodotto
this.cb_numero=create cb_numero
this.cb_anno=create cb_anno
this.cb_chiudi=create cb_chiudi
this.cb_ok=create cb_ok
this.st_ricerca_per=create st_ricerca_per
this.dw_ricerca_commesse=create dw_ricerca_commesse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_trovati
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+4]=this.cb_rag_soc
this.Control[iCurrent+5]=this.cb_cod_cliente
this.Control[iCurrent+6]=this.cb_des_prodotto
this.Control[iCurrent+7]=this.cb_cod_prodotto
this.Control[iCurrent+8]=this.cb_numero
this.Control[iCurrent+9]=this.cb_anno
this.Control[iCurrent+10]=this.cb_chiudi
this.Control[iCurrent+11]=this.cb_ok
this.Control[iCurrent+12]=this.st_ricerca_per
this.Control[iCurrent+13]=this.dw_ricerca_commesse
end on

on w_ricerca_commesse.destroy
call super::destroy
destroy(this.st_trovati)
destroy(this.st_1)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.cb_rag_soc)
destroy(this.cb_cod_cliente)
destroy(this.cb_des_prodotto)
destroy(this.cb_cod_prodotto)
destroy(this.cb_numero)
destroy(this.cb_anno)
destroy(this.cb_chiudi)
destroy(this.cb_ok)
destroy(this.st_ricerca_per)
destroy(this.dw_ricerca_commesse)
end on

event pc_setwindow;call super::pc_setwindow;st_ricerca_per.text = "Ricerca per anno commessa"
ii_tipo_ricerca = 1
dw_stringa_ricerca_prodotti.insertrow(0)

dw_ricerca_commesse.setrowfocusindicator(hand!)
end event

event deactivate;call super::deactivate;hide()
end event

type st_trovati from statictext within w_ricerca_commesse
integer x = 23
integer y = 1620
integer width = 494
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_ricerca_commesse
integer x = 549
integer y = 1620
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "record trovati"
boolean focusrectangle = false
end type

type dw_stringa_ricerca_prodotti from datawindow within w_ricerca_commesse
event ue_key pbm_dwnkey
integer x = 1029
integer y = 20
integer width = 2514
integer height = 100
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;if key = keyenter! then
	wf_carica()
end if
end event

type cb_rag_soc from commandbutton within w_ricerca_commesse
integer x = 2697
integer y = 140
integer width = 777
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ragione Sociale"
end type

event clicked;st_ricerca_per.text = "Ricerca per Ragione Sociale Cliente"
ii_tipo_ricerca = 6
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_cod_cliente from commandbutton within w_ricerca_commesse
integer x = 2263
integer y = 140
integer width = 434
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cod.Cliente"
end type

event clicked;st_ricerca_per.text = "Ricerca per codice cliente"
ii_tipo_ricerca = 5
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_des_prodotto from commandbutton within w_ricerca_commesse
integer x = 1234
integer y = 140
integer width = 1029
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizione Prodotto"
end type

event clicked;st_ricerca_per.text = "Ricerca per descrizione prodotto"
ii_tipo_ricerca = 4
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_cod_prodotto from commandbutton within w_ricerca_commesse
integer x = 709
integer y = 140
integer width = 526
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cod.Prodotto"
end type

event clicked;st_ricerca_per.text = "Ricerca per codice prodotto"
ii_tipo_ricerca = 3
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_numero from commandbutton within w_ricerca_commesse
integer x = 366
integer y = 140
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Num.Comm."
end type

event clicked;st_ricerca_per.text = "Ricerca per numero commessa"
ii_tipo_ricerca = 2
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_anno from commandbutton within w_ricerca_commesse
integer x = 23
integer y = 140
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anno Com."
end type

event clicked;st_ricerca_per.text = "Ricerca per anno commessa"
ii_tipo_ricerca = 1
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_chiudi from commandbutton within w_ricerca_commesse
integer x = 2789
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_ricerca_commesse
integer x = 3177
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_ricerca_commesse.getrow() > 0 then

	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		dw_ricerca_commesse.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, int(dw_ricerca_commesse.getitemnumber(dw_ricerca_commesse.getrow(),"anno_commessa")))
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, long(dw_ricerca_commesse.getitemnumber(dw_ricerca_commesse.getrow(),"num_commessa")))
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
	else
		dw_ricerca_commesse.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(string(dw_ricerca_commesse.getitemnumber(dw_ricerca_commesse.getrow(),"anno_commessa")))
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(string(dw_ricerca_commesse.getitemnumber(dw_ricerca_commesse.getrow(),"num_commessa")))
		s_cs_xx.parametri.parametro_uo_dw_search.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
	end if
	if il_row > 0 and not isnull(il_row) then
		dw_ricerca_commesse.setrow(il_row - 1)
		il_row = 0
	end if
	
	parent.hide()
	
end if
end event

type st_ricerca_per from statictext within w_ricerca_commesse
integer x = 23
integer y = 40
integer width = 983
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ricerca per"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_ricerca_commesse from datawindow within w_ricerca_commesse
event ue_key pbm_dwnkey
integer x = 23
integer y = 140
integer width = 3520
integer height = 1440
integer taborder = 10
string title = "none"
string dataobject = "d_ricerca_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < rowcount() then
			il_row = getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = getrow() - 1
		end if
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)
end event

