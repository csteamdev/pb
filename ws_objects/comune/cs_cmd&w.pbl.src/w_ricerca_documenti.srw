﻿$PBExportHeader$w_ricerca_documenti.srw
$PBExportComments$Finestra Ricerca Bolla di Vendita
forward
global type w_ricerca_documenti from w_cs_xx_risposta
end type
type cb_seleziona from uo_cb_accept within w_ricerca_documenti
end type
type cb_azzera from commandbutton within w_ricerca_documenti
end type
type dw_ricerca from uo_std_dw within w_ricerca_documenti
end type
end forward

global type w_ricerca_documenti from w_cs_xx_risposta
integer width = 4270
integer height = 1752
string title = "Ricerca Fatture di Vendita"
cb_seleziona cb_seleziona
cb_azzera cb_azzera
dw_ricerca dw_ricerca
end type
global w_ricerca_documenti w_ricerca_documenti

type variables
private:
	str_ricerca_documenti istr_documenti
end variables

event pc_setwindow;call super::pc_setwindow;

istr_documenti = message.powerobjectparm

// imposto la corretta datawindow
choose case istr_documenti.tipo_documento
		
	case "BV" // bolle vendita
		dw_ricerca.dataobject = "d_ricerca_bolla_ven"
		title = "Ricerca Bolle di Vendita"
		
	case "FV" // fatture vendita
		dw_ricerca.dataobject = "d_ricerca_fattura_ven"
		title = "Ricerca Fatture di Vendita"
		
	case "OV" // ordine vendita
		dw_ricerca.dataobject = "d_ricerca_ordine_ven"
		title = "Ricerca Ordine di Vendita"
		
end choose

dw_ricerca.settransobject(sqlca)
dw_ricerca.event ue_retrieve()
end event

on w_ricerca_documenti.create
int iCurrent
call super::create
this.cb_seleziona=create cb_seleziona
this.cb_azzera=create cb_azzera
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_seleziona
this.Control[iCurrent+2]=this.cb_azzera
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_ricerca_documenti.destroy
call super::destroy
destroy(this.cb_seleziona)
destroy(this.cb_azzera)
destroy(this.dw_ricerca)
end on

event pc_close;call super::pc_close;message.powerobjectparm = istr_documenti
end event

type cb_seleziona from uo_cb_accept within w_ricerca_documenti
integer x = 3840
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Seleziona"
end type

event clicked;call super::clicked;long ll_row

ll_row = dw_ricerca.getrow()

istr_documenti.cod_cliente = dw_ricerca.getitemstring(ll_row, "cod_cliente")
istr_documenti.cod_prodotto = dw_ricerca.getitemstring(ll_row, "cod_prodotto")
istr_documenti.anno_registrazione = dw_ricerca.getitemnumber(ll_row, "anno_registrazione")
istr_documenti.num_registrazione = dw_ricerca.getitemnumber(ll_row, "num_registrazione")
istr_documenti.prog_riga = dw_ricerca.getitemnumber(ll_row, "prog_riga")
		
choose case istr_documenti.tipo_documento
	
	case "FV", "BV" // fatture vendita, bolle vendita
		istr_documenti.cod_documento = dw_ricerca.getitemstring(ll_row, "cod_documento")
		istr_documenti.anno_documento = dw_ricerca.getitemnumber(ll_row, "anno_documento")
		istr_documenti.numeratore_documento = dw_ricerca.getitemstring(ll_row, "numeratore_documento")
		istr_documenti.num_documento = dw_ricerca.getitemnumber(ll_row, "num_documento")
		

	
end choose

closewithreturn(parent, istr_documenti)
end event

type cb_azzera from commandbutton within w_ricerca_documenti
integer x = 3474
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;string ls_null
long ll_null

setnull(ls_null)
setnull(ll_null)

istr_documenti.cod_cliente = ls_null
istr_documenti.cod_prodotto = ls_null
istr_documenti.anno_registrazione = ll_null
istr_documenti.num_registrazione = ll_null
istr_documenti.cod_documento =ls_null
istr_documenti.anno_documento = ll_null
istr_documenti.numeratore_documento = ls_null
istr_documenti.num_documento = ll_null
istr_documenti.prog_riga = ll_null




closewithreturn(parent, istr_documenti)
end event

type dw_ricerca from uo_std_dw within w_ricerca_documenti
event ue_retrieve ( )
integer x = 23
integer y = 20
integer width = 4183
integer height = 1480
integer taborder = 30
string dataobject = "d_ricerca_fattura_ven"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_retrieve();/**
 * retrieve
 **/
 
string ls_cod_cliente, ls_cod_prodotto

ls_cod_cliente = istr_documenti.cod_cliente
if isnull(ls_cod_cliente) then ls_cod_cliente = "%"

ls_cod_prodotto = istr_documenti.cod_prodotto
if isnull(ls_cod_prodotto) then ls_cod_prodotto = "%"

//Retrieve(s_cs_xx.cod_azienda, &
//	istr_documenti.anno_registrazione , &
//	istr_documenti.num_registrazione , &
//	istr_documenti.cod_documento , &
//	istr_documenti.anno_documento , &
//	istr_documenti.numeratore_documento, &
//	istr_documenti.num_documento, &
//	ls_cod_cliente, &
//	ls_cod_prodotto)

Retrieve(s_cs_xx.cod_azienda, &
	istr_documenti.anno_registrazione , &
	istr_documenti.num_registrazione , &
	istr_documenti.cod_documento , &
	istr_documenti.anno_documento , &
	istr_documenti.numeratore_documento, &
	istr_documenti.num_documento, &
	ls_cod_prodotto, &
	ls_cod_cliente)
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	
	cb_seleziona.event clicked()
	
end if
end event

