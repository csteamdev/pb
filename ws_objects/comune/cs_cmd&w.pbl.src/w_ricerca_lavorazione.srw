﻿$PBExportHeader$w_ricerca_lavorazione.srw
$PBExportComments$Finestra Ricerca Dati Lavorazione
forward
global type w_ricerca_lavorazione from w_cs_xx_risposta
end type
type dw_ricerca_lavorazioni from uo_dw_main within w_ricerca_lavorazione
end type
type cb_chiudi from uo_cb_close within w_ricerca_lavorazione
end type
type cb_azzera from commandbutton within w_ricerca_lavorazione
end type
type cb_1 from commandbutton within w_ricerca_lavorazione
end type
end forward

global type w_ricerca_lavorazione from w_cs_xx_risposta
int Width=2410
int Height=749
boolean TitleBar=true
string Title="Ricerca Lavorazione"
dw_ricerca_lavorazioni dw_ricerca_lavorazioni
cb_chiudi cb_chiudi
cb_azzera cb_azzera
cb_1 cb_1
end type
global w_ricerca_lavorazione w_ricerca_lavorazione

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_ricerca_lavorazioni.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nomodify+c_nodelete,c_default)

end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca_lavorazioni,"tes_fasi_lavorazione_cod_cat_attrezzatur",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on w_ricerca_lavorazione.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_ricerca_lavorazioni=create dw_ricerca_lavorazioni
this.cb_chiudi=create cb_chiudi
this.cb_azzera=create cb_azzera
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_ricerca_lavorazioni
this.Control[iCurrent+2]=cb_chiudi
this.Control[iCurrent+3]=cb_azzera
this.Control[iCurrent+4]=cb_1
end on

on w_ricerca_lavorazione.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_ricerca_lavorazioni)
destroy(this.cb_chiudi)
destroy(this.cb_azzera)
destroy(this.cb_1)
end on

type dw_ricerca_lavorazioni from uo_dw_main within w_ricerca_lavorazione
int X=23
int Y=21
int Width=2332
int Height=501
int TabOrder=20
string DataObject="d_ricerca_lavorazioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_1.triggerevent("Clicked")
   end if
end if
end on

on pcd_retrieve;call uo_dw_main::pcd_retrieve;LONG  l_Error


//l_Error = Retrieve(s_cs_xx.cod_azienda, "%", "%")

if isnull(s_cs_xx.parametri.parametro_s_10) then
   if isnull((s_cs_xx.parametri.parametro_s_11)) then
      l_Error = Retrieve(s_cs_xx.cod_azienda, "%", "%")
   else
      l_Error = Retrieve(s_cs_xx.cod_azienda, "%", s_cs_xx.parametri.parametro_s_11)
   end if
else
   if isnull((s_cs_xx.parametri.parametro_s_11)) then
      l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10, "%")
   else
      l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10, s_cs_xx.parametri.parametro_s_11)
   end if
end if
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type cb_chiudi from uo_cb_close within w_ricerca_lavorazione
int X=1601
int Y=541
int Width=366
int Height=81
int TabOrder=10
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_azzera from commandbutton within w_ricerca_lavorazione
int X=1212
int Y=541
int Width=366
int Height=81
int TabOrder=40
string Text="Azzera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_null
string ls_null

setnull(ll_null)
setnull(ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_lavorazione.postevent("pc_close")

end event

type cb_1 from commandbutton within w_ricerca_lavorazione
int X=1989
int Y=541
int Width=366
int Height=81
int TabOrder=30
string Text="OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if dw_ricerca_lavorazioni.getrow() > 0 then
   s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_lavorazioni.getitemstring(dw_ricerca_lavorazioni.getrow(),"tes_fasi_lavorazione_cod_lavorazione"))
   s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_ricerca_lavorazioni.getitemstring(dw_ricerca_lavorazioni.getrow(),"tes_fasi_lavorazione_cod_reparto"))
   s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, dw_ricerca_lavorazioni.getitemstring(dw_ricerca_lavorazioni.getrow(),"tes_fasi_lavorazione_cod_prodotto"))

   s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
   s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
   w_ricerca_lavorazione.triggerevent("pc_close")
end if
end on

