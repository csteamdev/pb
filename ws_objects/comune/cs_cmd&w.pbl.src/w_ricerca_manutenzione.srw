﻿$PBExportHeader$w_ricerca_manutenzione.srw
$PBExportComments$Finestra Ricerca Dai Manutenzione di un'attrezzatura
forward
global type w_ricerca_manutenzione from w_cs_xx_risposta
end type
type dw_ricerca_manutenzione from uo_dw_main within w_ricerca_manutenzione
end type
type cb_2 from uo_cb_close within w_ricerca_manutenzione
end type
type cb_azzera from commandbutton within w_ricerca_manutenzione
end type
type cb_1 from commandbutton within w_ricerca_manutenzione
end type
end forward

global type w_ricerca_manutenzione from w_cs_xx_risposta
integer width = 2254
integer height = 740
string title = "Ricerca Manutenzione"
dw_ricerca_manutenzione dw_ricerca_manutenzione
cb_2 cb_2
cb_azzera cb_azzera
cb_1 cb_1
end type
global w_ricerca_manutenzione w_ricerca_manutenzione

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_ricerca_manutenzione.set_dw_options(sqlca,pcca.null_object,c_retrieveonopen,c_default)

end on

on w_ricerca_manutenzione.create
int iCurrent
call super::create
this.dw_ricerca_manutenzione=create dw_ricerca_manutenzione
this.cb_2=create cb_2
this.cb_azzera=create cb_azzera
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_manutenzione
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_azzera
this.Control[iCurrent+4]=this.cb_1
end on

on w_ricerca_manutenzione.destroy
call super::destroy
destroy(this.dw_ricerca_manutenzione)
destroy(this.cb_2)
destroy(this.cb_azzera)
destroy(this.cb_1)
end on

type dw_ricerca_manutenzione from uo_dw_main within w_ricerca_manutenzione
integer x = 23
integer y = 20
integer width = 2171
integer height = 500
integer taborder = 20
string dataobject = "d_ricerca_manutenzione"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_dw_main::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_1.triggerevent("Clicked")
   end if
end if
end on

type cb_2 from uo_cb_close within w_ricerca_manutenzione
integer x = 1440
integer y = 540
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

on clicked;call uo_cb_close::clicked;s_cs_xx.parametri.parametro_d_1 = 0
end on

type cb_azzera from commandbutton within w_ricerca_manutenzione
integer x = 1051
integer y = 540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

on clicked;long ll_null

setnull(ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_manutenzione.triggerevent("pc_close")

end on

type cb_1 from commandbutton within w_ricerca_manutenzione
integer x = 1829
integer y = 540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;if dw_ricerca_manutenzione.rowcount() < 1 then
	return -1
end if

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, int(dw_ricerca_manutenzione.getitemnumber(dw_ricerca_manutenzione.getrow(),"num_registrazione")))

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_manutenzione.triggerevent("pc_close")

end event

