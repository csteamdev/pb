﻿$PBExportHeader$w_ricerca_ord_ven.srw
$PBExportComments$Finestra Ricerca Bolla di Vendita
forward
global type w_ricerca_ord_ven from w_cs_xx_risposta
end type
type dw_ricerca_bolla_ven from uo_dw_main within w_ricerca_ord_ven
end type
type cb_1 from uo_cb_accept within w_ricerca_ord_ven
end type
type cb_2 from uo_cb_close within w_ricerca_ord_ven
end type
type cb_azzera from commandbutton within w_ricerca_ord_ven
end type
end forward

global type w_ricerca_ord_ven from w_cs_xx_risposta
integer width = 3465
integer height = 1140
string title = "Ricerca Ordini di Vendita"
dw_ricerca_bolla_ven dw_ricerca_bolla_ven
cb_1 cb_1
cb_2 cb_2
cb_azzera cb_azzera
end type
global w_ricerca_ord_ven w_ricerca_ord_ven

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_ricerca_bolla_ven.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCInsert+&
                                    c_nonew+c_nomodify+c_nodelete,c_default)

end on

on w_ricerca_ord_ven.create
int iCurrent
call super::create
this.dw_ricerca_bolla_ven=create dw_ricerca_bolla_ven
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_azzera=create cb_azzera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_bolla_ven
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_azzera
end on

on w_ricerca_ord_ven.destroy
call super::destroy
destroy(this.dw_ricerca_bolla_ven)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_azzera)
end on

type dw_ricerca_bolla_ven from uo_dw_main within w_ricerca_ord_ven
integer x = 23
integer y = 20
integer width = 3383
integer height = 880
integer taborder = 20
string dataobject = "d_ricerca_ordine_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
double ld_da_bolla, ld_a_bolla

if isnull(s_cs_xx.parametri.parametro_d_2) then
   ld_da_bolla = 1
   ld_a_bolla  = 999999
else
   ld_da_bolla = s_cs_xx.parametri.parametro_d_2
   ld_a_bolla  = s_cs_xx.parametri.parametro_d_2
end if

if isnull(s_cs_xx.parametri.parametro_s_10) then s_cs_xx.parametri.parametro_s_10 = "%"
if isnull(s_cs_xx.parametri.parametro_s_11) then s_cs_xx.parametri.parametro_s_11 = "%"

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_s_10 , &
                   s_cs_xx.parametri.parametro_d_1 , &
                   ld_da_bolla , &
                   ld_a_bolla , &
                   s_cs_xx.parametri.parametro_s_11)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_1.triggerevent("Clicked")
   end if
end if
end on

type cb_1 from uo_cb_accept within w_ricerca_ord_ven
integer x = 3040
integer y = 920
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;call super::clicked;long ll_prog_stock

// stefanop: aggiunto settaggio su un nuovo tipo di dw

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_bolla_ven.getitemstring(dw_ricerca_bolla_ven.getrow(),"cod_cliente"))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"anno_registrazione")))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"num_registrazione")))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, dw_ricerca_bolla_ven.getitemstring(dw_ricerca_bolla_ven.getrow(),"cod_prodotto"))
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"prog_riga")))

	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_bolla_ven.getitemstring(dw_ricerca_bolla_ven.getrow(),"cod_cliente"))
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"anno_registrazione")))
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"num_registrazione")))
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, dw_ricerca_bolla_ven.getitemstring(dw_ricerca_bolla_ven.getrow(),"cod_prodotto"))
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, int(dw_ricerca_bolla_ven.getitemnumber(dw_ricerca_bolla_ven.getrow(),"prog_riga")))

end if


w_ricerca_ord_ven.triggerevent("pc_close")
end event

type cb_2 from uo_cb_close within w_ricerca_ord_ven
integer x = 2651
integer y = 920
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cb_azzera from commandbutton within w_ricerca_ord_ven
integer x = 2263
integer y = 920
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;string ls_null
long ll_null

setnull(ls_null)
setnull(ll_null)

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_null)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ll_null)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, ls_null)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ll_null)
elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_null)
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ll_null)
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, ls_null)
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ll_null)
end if

w_ricerca_ord_ven.postevent("pc_close")
end event

