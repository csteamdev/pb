﻿$PBExportHeader$w_ricerca_stock.srw
$PBExportComments$Finestra Ricerca Dati Stock di un Prodotto
forward
global type w_ricerca_stock from w_cs_xx_risposta
end type
type cb_ok from uo_cb_accept within w_ricerca_stock
end type
type cb_chiudi from uo_cb_close within w_ricerca_stock
end type
type cb_azzera from commandbutton within w_ricerca_stock
end type
type cb_nuovo from commandbutton within w_ricerca_stock
end type
type cb_salva from commandbutton within w_ricerca_stock
end type
type cb_annulla_nuovo from commandbutton within w_ricerca_stock
end type
type dw_crea_stock from uo_dw_main within w_ricerca_stock
end type
type dw_ricerca_stock from uo_dw_main within w_ricerca_stock
end type
type dw_folder from u_folder within w_ricerca_stock
end type
end forward

global type w_ricerca_stock from w_cs_xx_risposta
integer width = 2958
integer height = 1220
string title = "Ricerca Stock"
cb_ok cb_ok
cb_chiudi cb_chiudi
cb_azzera cb_azzera
cb_nuovo cb_nuovo
cb_salva cb_salva
cb_annulla_nuovo cb_annulla_nuovo
dw_crea_stock dw_crea_stock
dw_ricerca_stock dw_ricerca_stock
dw_folder dw_folder
end type
global w_ricerca_stock w_ricerca_stock

type variables

end variables

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]


dw_crea_stock.set_dw_key("cod_azienda")
dw_crea_stock.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_ricerca_stock.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

l_objects[1] = dw_crea_stock
l_objects[2] = cb_salva
l_objects[3] = cb_annulla_nuovo
dw_folder.fu_AssignTab(2, "&Crea Stock", l_Objects[])

l_objects[1] = dw_ricerca_stock
dw_folder.fu_AssignTab(1, "&Ricerca Stock", l_Objects[])

dw_folder.fu_FolderCreate(2,4)
                          // il primo parametri indica quanti fogli dentro al folder
                          // il secondo parametro indica quanti fogli per riga
dw_folder.fu_SelectTab(1)
dw_folder.fu_HideTab(2)

cb_salva.enabled = false
cb_annulla_nuovo.enabled = false
end event

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca_stock,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_ricerca_stock.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
this.cb_azzera=create cb_azzera
this.cb_nuovo=create cb_nuovo
this.cb_salva=create cb_salva
this.cb_annulla_nuovo=create cb_annulla_nuovo
this.dw_crea_stock=create dw_crea_stock
this.dw_ricerca_stock=create dw_ricerca_stock
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_chiudi
this.Control[iCurrent+3]=this.cb_azzera
this.Control[iCurrent+4]=this.cb_nuovo
this.Control[iCurrent+5]=this.cb_salva
this.Control[iCurrent+6]=this.cb_annulla_nuovo
this.Control[iCurrent+7]=this.dw_crea_stock
this.Control[iCurrent+8]=this.dw_ricerca_stock
this.Control[iCurrent+9]=this.dw_folder
end on

on w_ricerca_stock.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_chiudi)
destroy(this.cb_azzera)
destroy(this.cb_nuovo)
destroy(this.cb_salva)
destroy(this.cb_annulla_nuovo)
destroy(this.dw_crea_stock)
destroy(this.dw_ricerca_stock)
destroy(this.dw_folder)
end on

type cb_ok from uo_cb_accept within w_ricerca_stock
integer x = 2537
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;call super::clicked;datetime ldt_data_stock
long ll_prog_stock

if not isnull(s_cs_xx.parametri.parametro_s_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_prodotto"))
end if
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_deposito"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_ubicazione"))
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_4)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, dw_ricerca_stock.getitemstring(dw_ricerca_stock.getrow(),"cod_lotto"))

ldt_data_stock = dw_ricerca_stock.getitemdatetime(dw_ricerca_stock.getrow(),"data_stock")
//s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_5)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ldt_data_stock)

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"prog_stock")
//s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_6)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_6, ll_prog_stock)

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"quan_assegnata")
if (ll_prog_stock > 0) and (not isnull(s_cs_xx.parametri.parametro_s_7)) and (len(s_cs_xx.parametri.parametro_s_7) > 0) then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_7, ll_prog_stock)
end if

ll_prog_stock = dw_ricerca_stock.getitemnumber(dw_ricerca_stock.getrow(),"costo_medio")
if (ll_prog_stock > 0) and (not isnull(s_cs_xx.parametri.parametro_s_8)) and (len(s_cs_xx.parametri.parametro_s_8) > 0) then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_8, ll_prog_stock)
end if

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_stock.triggerevent("pc_close")
end event

type cb_chiudi from uo_cb_close within w_ricerca_stock
integer x = 2149
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cb_azzera from commandbutton within w_ricerca_stock
integer x = 1760
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;string ls_null
datetime ldt_null
long ll_null

setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)
//s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
//s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_4)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_4, ls_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_5)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_5, ldt_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_6)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_6, ll_null)

s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
w_ricerca_stock.postevent("pc_close")
end event

type cb_nuovo from commandbutton within w_ricerca_stock
integer x = 23
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuovo"
end type

on clicked;dw_folder.fu_ShowTab(2)
dw_folder.fu_SelectTab(2)
dw_crea_stock.change_dw_current()
dw_folder.fu_HideTab(1)

dw_crea_stock.triggerevent("pcd_new")
cb_nuovo.enabled = false
cb_salva.enabled = true
cb_annulla_nuovo.enabled = true
cb_ok.enabled = false
cb_chiudi.enabled = false
cb_azzera.enabled = false

f_PO_LoadDDDW_DW(dw_crea_stock,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_crea_stock,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito",&
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

type cb_salva from commandbutton within w_ricerca_stock
integer x = 411
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
long   ll_prog_stock, ll_prog_stock_2
datetime		ldt_data_stock

ls_cod_prodotto = dw_crea_stock.getitemstring(dw_crea_stock.getrow(), "cod_prodotto")
ls_cod_deposito = dw_crea_stock.getitemstring(dw_crea_stock.getrow(), "cod_deposito")
ls_cod_ubicazione = dw_crea_stock.getitemstring(dw_crea_stock.getrow(), "cod_ubicazione")
ls_cod_lotto = dw_crea_stock.getitemstring(dw_crea_stock.getrow(), "cod_lotto")

ldt_data_stock = dw_crea_stock.getitemdatetime(dw_crea_stock.getrow(), "data_stock")
ll_prog_stock_2 = dw_crea_stock.getitemnumber(dw_crea_stock.getrow(), "prog_stock")


SELECT max(stock.prog_stock)
INTO   :ll_prog_stock  
FROM   stock  
WHERE  ( stock.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( stock.cod_prodotto = :ls_cod_prodotto ) AND  
       ( stock.cod_deposito = :ls_cod_deposito ) AND  
       ( stock.cod_ubicazione = :ls_cod_ubicazione ) AND  
       ( stock.cod_lotto = :ls_cod_lotto );

if sqlca.sqlcode = 0 then
	if isnull(ll_prog_stock) or (ll_prog_stock < 1) then
		ll_prog_stock = 1
	else
		ll_prog_stock++
	end if
	
	if ll_prog_stock_2 > ll_prog_stock then ll_prog_stock = ll_prog_stock_2
	
	dw_crea_stock.setitem(dw_crea_stock.getrow(), "prog_stock", ll_prog_stock)
else
	ll_prog_stock = 1
	if ll_prog_stock_2 > ll_prog_stock then ll_prog_stock = ll_prog_stock_2
	dw_crea_stock.setitem(dw_crea_stock.getrow(), "prog_stock", ll_prog_stock)
end if


if isnull(ldt_data_stock) or year(date(ldt_data_stock)) < 1950 then
	dw_crea_stock.setitem(dw_crea_stock.getrow(), "data_stock", datetime(today()))
else
	dw_crea_stock.setitem(dw_crea_stock.getrow(), "data_stock", ldt_data_stock)
end if






dw_crea_stock.triggerevent("pcd_save")

end event

type cb_annulla_nuovo from commandbutton within w_ricerca_stock
integer x = 800
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

on clicked;dw_crea_stock.triggerevent("pcd_view")

end on

type dw_crea_stock from uo_dw_main within w_ricerca_stock
integer x = 69
integer y = 140
integer width = 2240
integer height = 820
integer taborder = 20
string dataobject = "d_crea_stock"
end type

on pcd_setkey;call uo_dw_main::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_view;call uo_dw_main::pcd_view;cb_nuovo.enabled = true
cb_salva.enabled = false
cb_annulla_nuovo.enabled = false
cb_ok.enabled = true
cb_chiudi.enabled = true
cb_azzera.enabled = true
dw_folder.fu_ShowTab(1)
dw_folder.fu_SelectTab(1)
dw_ricerca_stock.change_dw_current()
dw_ricerca_stock.triggerevent("pcd_retrieve")
dw_folder.fu_HideTab(2)

end on

on doubleclicked;call uo_dw_main::doubleclicked;//if i_extendmode then
//   if this.getrow() > 0 then
//      cb_1.triggerevent("Clicked")
//   end if
//end if
end on

on pcd_retrieve;call uo_dw_main::pcd_retrieve;//LONG  l_Error
//
//if isnull(s_cs_xx.parametri.parametro_s_11) then s_cs_xx.parametri.parametro_s_11 = "%"
//if isnull(s_cs_xx.parametri.parametro_s_12) then s_cs_xx.parametri.parametro_s_12 = "%"
//if isnull(s_cs_xx.parametri.parametro_s_13) then s_cs_xx.parametri.parametro_s_13 = "%"
//
//l_Error = Retrieve(s_cs_xx.cod_azienda, &
//                   s_cs_xx.parametri.parametro_s_10 , &
//                   s_cs_xx.parametri.parametro_s_11 , &
//                   s_cs_xx.parametri.parametro_s_12 , &
//                   s_cs_xx.parametri.parametro_s_13)
//
//IF l_Error < 0 THEN
//   PCCA.Error = c_Fatal
//END IF
end on

on pcd_new;call uo_dw_main::pcd_new;this.setitem(this.getrow(),"cod_prodotto", s_cs_xx.parametri.parametro_s_10)
end on

type dw_ricerca_stock from uo_dw_main within w_ricerca_stock
integer x = 69
integer y = 140
integer width = 2766
integer height = 820
integer taborder = 40
string dataobject = "d_ricerca_stock"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

if isnull(s_cs_xx.parametri.parametro_s_10) or len(s_cs_xx.parametri.parametro_s_10) < 1 then s_cs_xx.parametri.parametro_s_10 = "%"
if isnull(s_cs_xx.parametri.parametro_s_11) or len(s_cs_xx.parametri.parametro_s_11) < 1 then s_cs_xx.parametri.parametro_s_11 = "%"
if isnull(s_cs_xx.parametri.parametro_s_12) or len(s_cs_xx.parametri.parametro_s_12) < 1 then s_cs_xx.parametri.parametro_s_12 = "%"
if isnull(s_cs_xx.parametri.parametro_s_13) or len(s_cs_xx.parametri.parametro_s_13) < 1 then s_cs_xx.parametri.parametro_s_13 = "%"

l_Error = retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.parametri.parametro_s_10 , &
                   s_cs_xx.parametri.parametro_s_11 , &
                   s_cs_xx.parametri.parametro_s_12 , &
                   s_cs_xx.parametri.parametro_s_13)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

on doubleclicked;call uo_dw_main::doubleclicked;if i_extendmode then
   if this.getrow() > 0 then
      cb_ok.triggerevent("Clicked")
   end if
end if
end on

type dw_folder from u_folder within w_ricerca_stock
integer x = 23
integer y = 20
integer width = 2880
integer height = 980
integer taborder = 10
end type

