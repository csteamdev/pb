﻿$PBExportHeader$w_seleziona_colore.srw
$PBExportComments$Finestra Standard di Selezione Colore (RGB)
forward
global type w_seleziona_colore from w_cs_xx_risposta
end type
type uo_seleziona_colore from u_seleziona_colore within w_seleziona_colore
end type
type cb_ok from commandbutton within w_seleziona_colore
end type
type cb_chiudi from commandbutton within w_seleziona_colore
end type
end forward

global type w_seleziona_colore from w_cs_xx_risposta
int Width=901
int Height=877
boolean TitleBar=true
string Title="Seleziona Colore"
uo_seleziona_colore uo_seleziona_colore
cb_ok cb_ok
cb_chiudi cb_chiudi
end type
global w_seleziona_colore w_seleziona_colore

on w_seleziona_colore.create
int iCurrent
call w_cs_xx_risposta::create
this.uo_seleziona_colore=create uo_seleziona_colore
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=uo_seleziona_colore
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=cb_chiudi
end on

on w_seleziona_colore.destroy
call w_cs_xx_risposta::destroy
destroy(this.uo_seleziona_colore)
destroy(this.cb_ok)
destroy(this.cb_chiudi)
end on

event pc_setwindow;call super::pc_setwindow;uo_seleziona_colore.uf_set_rgb(s_cs_xx.parametri.parametro_ul_1)
setnull(s_cs_xx.parametri.parametro_ul_1)
end event

type uo_seleziona_colore from u_seleziona_colore within w_seleziona_colore
int X=46
int Y=21
int TabOrder=10
long BackColor=79741120
end type

on uo_seleziona_colore.destroy
call u_seleziona_colore::destroy
end on

type cb_ok from commandbutton within w_seleziona_colore
int X=46
int Y=661
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Ok"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_rgb
ll_rgb = uo_seleziona_colore.uf_get_rgb()
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_rgb)
s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
close(parent)
end event

type cb_chiudi from commandbutton within w_seleziona_colore
int X=458
int Y=661
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Chiudi"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

