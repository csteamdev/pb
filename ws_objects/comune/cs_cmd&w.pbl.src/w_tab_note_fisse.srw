﻿$PBExportHeader$w_tab_note_fisse.srw
$PBExportComments$Finestra Tabella Note Fisse X Documenti X Cliente X Periodo
forward
global type w_tab_note_fisse from w_cs_xx_principale
end type
type cb_lingue from commandbutton within w_tab_note_fisse
end type
type cb_reset from commandbutton within w_tab_note_fisse
end type
type cb_ricerca from commandbutton within w_tab_note_fisse
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_note_fisse
end type
type dw_tab_note_fisse_det from uo_cs_xx_dw within w_tab_note_fisse
end type
type dw_ricerca from u_dw_search within w_tab_note_fisse
end type
type dw_folder_search from u_folder within w_tab_note_fisse
end type
type dw_tab_note_fisse_lista from uo_cs_xx_dw within w_tab_note_fisse
end type
end forward

global type w_tab_note_fisse from w_cs_xx_principale
integer width = 3122
integer height = 2284
string title = "Note Fisse Documenti"
cb_lingue cb_lingue
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_note_esterne cb_note_esterne
dw_tab_note_fisse_det dw_tab_note_fisse_det
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_tab_note_fisse_lista dw_tab_note_fisse_lista
end type
global w_tab_note_fisse w_tab_note_fisse

on w_tab_note_fisse.create
int iCurrent
call super::create
this.cb_lingue=create cb_lingue
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_note_esterne=create cb_note_esterne
this.dw_tab_note_fisse_det=create dw_tab_note_fisse_det
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_tab_note_fisse_lista=create dw_tab_note_fisse_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_lingue
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_ricerca
this.Control[iCurrent+4]=this.cb_note_esterne
this.Control[iCurrent+5]=this.dw_tab_note_fisse_det
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.dw_folder_search
this.Control[iCurrent+8]=this.dw_tab_note_fisse_lista
end on

on w_tab_note_fisse.destroy
call super::destroy
destroy(this.cb_lingue)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_note_esterne)
destroy(this.dw_tab_note_fisse_det)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_tab_note_fisse_lista)
end on

event pc_setwindow;call super::pc_setwindow;string 		 l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[], l_objects[ ],lw_oggetti_1[]


dw_tab_note_fisse_lista.set_dw_key("cod_azienda")
dw_tab_note_fisse_lista.set_dw_key("cod_nota_fissa")
dw_tab_note_fisse_lista.set_dw_options(sqlca,pcca.null_object,c_default + c_noretrieveonopen,c_default)
dw_tab_note_fisse_det.set_dw_options(sqlca,dw_tab_note_fisse_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_note_fisse_lista
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = false


l_criteriacolumn[1] = "rs_cod_nota_fissa"
l_criteriacolumn[2] = "rs_cod_cliente"
l_criteriacolumn[3] = "rs_cod_fornitore"
l_criteriacolumn[4] = "rs_flag_piede_testata"
l_criteriacolumn[5] = "rs_cod_prodotto"

l_searchtable[1] = "tab_note_fisse"
l_searchtable[2] = "tab_note_fisse"
l_searchtable[3] = "tab_note_fisse"
l_searchtable[4] = "tab_note_fisse"
l_searchtable[5] = "tab_note_fisse"

l_searchcolumn[1] = "cod_nota_fissa"
l_searchcolumn[2] = "cod_cliente"
l_searchcolumn[3] = "cod_fornitore"
l_searchcolumn[4] = "flag_piede_testata"
l_searchcolumn[5] = "cod_prodotto"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tab_note_fisse_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)

l_objects[1] = dw_tab_note_fisse_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_tab_note_fisse_lista.change_dw_current()

end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_note_fisse_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_note_fisse_lista.GetItemstring(li_row,"cod_nota_fissa"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = false
end event

event pc_setddlb;call super::pc_setddlb;dw_ricerca.fu_loadcode("rs_cod_nota_fissa", &
                       "tab_note_fisse", &
							  "cod_nota_fissa", &
							  "des_nota_fissa + '(' + cod_nota_fissa + ')'", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cod_nota_fissa asc", "(Tutti)" )

f_po_loaddddw_dw(dw_tab_note_fisse_det, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_lingue from commandbutton within w_tab_note_fisse
integer x = 2674
integer y = 2080
integer width = 370
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Lingue"
end type

event clicked;long ll_row

ll_row = dw_tab_note_fisse_lista.getrow()

if ll_row < 1 then return


window_open_parm(w_tab_note_fisse_lingue, -1, dw_tab_note_fisse_lista)
end event

type cb_reset from commandbutton within w_tab_note_fisse
integer x = 2629
integer y = 140
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_ricerca from commandbutton within w_tab_note_fisse
integer x = 2629
integer y = 40
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_data_blocco
date ldt_data_blocco

dw_ricerca.accepttext()

dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_tab_note_fisse_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type cb_note_esterne from cb_documenti_compilati within w_tab_note_fisse
event clicked pbm_bnclicked
boolean visible = false
integer x = 2286
integer y = 2080
integer width = 370
integer height = 80
integer taborder = 20
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_nota_fissa
string ls_cod_blob, ls_db, ls_doc
integer li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

ls_cod_nota_fissa= dw_tab_note_fisse_lista.getitemstring(dw_tab_note_fisse_lista.getrow(), "cod_nota_fissa")

select prog_mimetype
into :ll_prog_mimetype
from tab_note_fisse
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_nota_fissa = :ls_cod_nota_fissa;
	
selectblob note_esterne
into :lbl_blob
from tab_note_fisse
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_nota_fissa = :ls_cod_nota_fissa;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_note_fisse
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_nota_fissa = :ls_cod_nota_fissa;
	else
		updateblob tab_note_fisse
		set note_esterne = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_nota_fissa = :ls_cod_nota_fissa;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
	update tab_note_fisse
	set prog_mimetype = :ll_prog_mimetype
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_nota_fissa = :ls_cod_nota_fissa;
		
	
end if

setpointer(arrow!)
end event

event getfocus;call super::getfocus;dw_tab_note_fisse_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_tab_note_fisse_det from uo_cs_xx_dw within w_tab_note_fisse
integer y = 560
integer width = 3063
integer height = 1500
integer taborder = 30
string dataobject = "d_tab_note_fisse_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;choose case i_colname
		
	case "cod_tipo_fat_ven"
		
		if not isnull(i_coltext) then
			setitem(row,"flag_fattura_ven","S")
		end if
		
	case "flag_fattura_ven"
		
		string ls_null
		
		setnull(ls_null)
		
		if isnull(i_coltext) or i_coltext = "N" then
			setitem(row,"cod_tipo_fat_ven",ls_null)
		end if
		
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tab_note_fisse_det,"cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tab_note_fisse_det,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_note_fisse_det,"cod_prodotto")
end choose
end event

type dw_ricerca from u_dw_search within w_tab_note_fisse
event ue_key pbm_dwnkey
integer x = 229
integer y = 20
integer width = 2789
integer height = 500
integer taborder = 60
string dataobject = "d_tab_note_fisse_ricerca"
boolean border = false
end type

event ue_key;choose case this.getcolumnname()
	case "rs_cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		end if
end choose

//if key = keyenter! then cb_reset.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"rs_cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type dw_folder_search from u_folder within w_tab_note_fisse
integer width = 3063
integer height = 540
integer taborder = 60
end type

type dw_tab_note_fisse_lista from uo_cs_xx_dw within w_tab_note_fisse
integer x = 137
integer y = 20
integer width = 2903
integer height = 480
integer taborder = 10
string dataobject = "d_tab_note_fisse_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota_fissa")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = false
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = true
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = true

dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_offerta_acq", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_ordine_acq", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_bolla_acq", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_fattura_acq", "S")

dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_offerta_ven", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_ordine_ven", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_bolla_ven", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_fattura_ven", "S")
dw_tab_note_fisse_det.setitem(dw_tab_note_fisse_det.getrow(), "flag_listino_vendita", "N")

end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = true
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota_fissa")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
dw_tab_note_fisse_det.object.b_ricerca_cliente.enabled = false
dw_tab_note_fisse_det.object.b_ricerca_fornitore.enabled = false
end event

event updatestart;call super::updatestart;if i_extendmode then
	long ll_i, ll_cliente, ll_fornitore, ll_prodotto
	
	for ll_i = 1 to this.rowcount()
		if getitemstatus(ll_i,0,Primary!) = NewModified! or getitemstatus(ll_i,0,Primary!) = DataModified! then
			ll_cliente = 0
			ll_fornitore = 0
			ll_prodotto = 0
			if not isnull(getitemstring(ll_i,"cod_cliente")) then ll_cliente = 1
			if not isnull(getitemstring(ll_i,"cod_fornitore")) then ll_fornitore = 1
			if not isnull(getitemstring(ll_i,"cod_prodotto")) then ll_prodotto = 1
			if (ll_cliente + ll_fornitore + ll_prodotto) > 1 then
				g_mb.error("Non è possibile associare condizioni contemporaneamente per cliente/fornitore/prodotto.")
				return 1
			end if
		end if
	next
end if

end event

