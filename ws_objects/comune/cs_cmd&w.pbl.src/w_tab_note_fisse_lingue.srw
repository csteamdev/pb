﻿$PBExportHeader$w_tab_note_fisse_lingue.srw
forward
global type w_tab_note_fisse_lingue from w_cs_xx_principale
end type
type dw_note_fisse_lingue from uo_cs_xx_dw within w_tab_note_fisse_lingue
end type
end forward

global type w_tab_note_fisse_lingue from w_cs_xx_principale
integer width = 2944
integer height = 2128
string title = "Listini Vendita - Traduzioni"
dw_note_fisse_lingue dw_note_fisse_lingue
end type
global w_tab_note_fisse_lingue w_tab_note_fisse_lingue

type variables
private:
	string is_cod_nota_fissa
end variables

on w_tab_note_fisse_lingue.create
int iCurrent
call super::create
this.dw_note_fisse_lingue=create dw_note_fisse_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_note_fisse_lingue
end on

on w_tab_note_fisse_lingue.destroy
call super::destroy
destroy(this.dw_note_fisse_lingue)
end on

event pc_setwindow;call super::pc_setwindow;dw_note_fisse_lingue.set_dw_key("cod_azienda")
dw_note_fisse_lingue.set_dw_key("cod_nota_fissa")
dw_note_fisse_lingue.set_dw_key("cod_lingua")
dw_note_fisse_lingue.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)

iuo_dw_main = dw_note_fisse_lingue

end event

event pc_setddlb;call super::pc_setddlb;

f_po_loaddddw_dw(	dw_note_fisse_lingue, &
							  "cod_lingua", &
							  sqlca, &
							  "tab_lingue", &
							  "cod_lingua", &
							  "des_lingua", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_note_fisse_lingue from uo_cs_xx_dw within w_tab_note_fisse_lingue
integer x = 23
integer y = 20
integer width = 2871
integer height = 1988
integer taborder = 10
string dataobject = "d_tab_note_fisse_lingue"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

is_cod_nota_fissa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_nota_fissa")

parent.title = "Traduzioni per la nota fissa " + is_cod_nota_fissa

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_nota_fissa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()

	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if

	if isnull(this.getitemstring(ll_i, "cod_nota_fissa")) or this.getitemstring(ll_i, "cod_nota_fissa")="" then
		this.setitem(ll_i, "cod_nota_fissa", is_cod_nota_fissa)
	end if
	
next
end event

