﻿$PBExportHeader$w_tipi_costi_mp.srw
forward
global type w_tipi_costi_mp from w_cs_xx_principale
end type
type dw_tipi_costi_mp_det from uo_cs_xx_dw within w_tipi_costi_mp
end type
type dw_tipi_costi_mp_lista from uo_cs_xx_dw within w_tipi_costi_mp
end type
end forward

global type w_tipi_costi_mp from w_cs_xx_principale
integer width = 2345
integer height = 1124
string title = "Costificazione Materie Prime"
boolean maxbox = false
boolean resizable = false
dw_tipi_costi_mp_det dw_tipi_costi_mp_det
dw_tipi_costi_mp_lista dw_tipi_costi_mp_lista
end type
global w_tipi_costi_mp w_tipi_costi_mp

on w_tipi_costi_mp.create
int iCurrent
call super::create
this.dw_tipi_costi_mp_det=create dw_tipi_costi_mp_det
this.dw_tipi_costi_mp_lista=create dw_tipi_costi_mp_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_costi_mp_det
this.Control[iCurrent+2]=this.dw_tipi_costi_mp_lista
end on

on w_tipi_costi_mp.destroy
call super::destroy
destroy(this.dw_tipi_costi_mp_det)
destroy(this.dw_tipi_costi_mp_lista)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_tipi_costi_mp_lista

dw_tipi_costi_mp_lista.change_dw_current()

dw_tipi_costi_mp_lista.set_dw_key("cod_azienda")

dw_tipi_costi_mp_lista.set_dw_key("cod_tipo_costo_mp")

dw_tipi_costi_mp_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

dw_tipi_costi_mp_det.set_dw_options(sqlca,dw_tipi_costi_mp_lista,c_sharedata + c_scrollparent,c_default)
end event

type dw_tipi_costi_mp_det from uo_cs_xx_dw within w_tipi_costi_mp
integer x = 23
integer y = 660
integer width = 2286
integer height = 360
integer taborder = 20
string dataobject = "d_tipi_costi_mp_det"
borderstyle borderstyle = stylelowered!
end type

type dw_tipi_costi_mp_lista from uo_cs_xx_dw within w_tipi_costi_mp
integer x = 23
integer y = 20
integer width = 2286
integer height = 620
integer taborder = 10
string dataobject = "d_tipi_costi_mp_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
next
end event

event updatestart;call super::updatestart;string ls_tipo_costo

long   ll_i, ll_giorni


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_tipo_costo_mp")) then
		continue
	end if
	
	ls_tipo_costo = getitemstring(ll_i,"flag_tipo_costo_prev")
	
	ll_giorni = getitemnumber(ll_i,"num_giorni_ricerca_prev")
	
	if isnull(ls_tipo_costo) then
		g_mb.messagebox("SEP","Impostare un tipo costo per il calcolo preventivo!")
		return 1
	elseif ls_tipo_costo = "A" and (isnull(ll_giorni) or ll_giorni = 0) then
		g_mb.messagebox("SEP","Impostare i giorni di ricerca per il calcolo preventivo!")
		return 1
	end if
	
	ls_tipo_costo = getitemstring(ll_i,"flag_tipo_costo_cons")
	
	ll_giorni = getitemnumber(ll_i,"num_giorni_ricerca_cons")
	
	if isnull(ls_tipo_costo) then
		g_mb.messagebox("SEP","Impostare un tipo costo per il calcolo consuntivo!")
		return 1
	elseif (ls_tipo_costo = "A" or ls_tipo_costo = "B") and (isnull(ll_giorni) or ll_giorni = 0) then
		g_mb.messagebox("SEP","Impostare i giorni di ricerca per il calcolo consuntivo!")
		return 1
	end if
	
next
end event

