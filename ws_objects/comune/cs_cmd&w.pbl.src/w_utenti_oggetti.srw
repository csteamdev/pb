﻿$PBExportHeader$w_utenti_oggetti.srw
$PBExportComments$Window utenti oggetti
forward
global type w_utenti_oggetti from w_cs_xx_principale
end type
type dw_utenti_oggetti_lista from uo_cs_xx_dw within w_utenti_oggetti
end type
type cb_path from commandbutton within w_utenti_oggetti
end type
type cb_documento from commandbutton within w_utenti_oggetti
end type
type dw_utenti_oggetti_det from uo_cs_xx_dw within w_utenti_oggetti
end type
end forward

global type w_utenti_oggetti from w_cs_xx_principale
integer width = 2455
integer height = 1300
string title = "Oggetti per Utente"
dw_utenti_oggetti_lista dw_utenti_oggetti_lista
cb_path cb_path
cb_documento cb_documento
dw_utenti_oggetti_det dw_utenti_oggetti_det
end type
global w_utenti_oggetti w_utenti_oggetti

on w_utenti_oggetti.create
int iCurrent
call super::create
this.dw_utenti_oggetti_lista=create dw_utenti_oggetti_lista
this.cb_path=create cb_path
this.cb_documento=create cb_documento
this.dw_utenti_oggetti_det=create dw_utenti_oggetti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_utenti_oggetti_lista
this.Control[iCurrent+2]=this.cb_path
this.Control[iCurrent+3]=this.cb_documento
this.Control[iCurrent+4]=this.dw_utenti_oggetti_det
end on

on w_utenti_oggetti.destroy
call super::destroy
destroy(this.dw_utenti_oggetti_lista)
destroy(this.cb_path)
destroy(this.cb_documento)
destroy(this.dw_utenti_oggetti_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_utenti_oggetti_lista.set_dw_key("cod_azienda")
dw_utenti_oggetti_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_utenti_oggetti_det.set_dw_options(sqlca, &
                             dw_utenti_oggetti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)

iuo_dw_main = dw_utenti_oggetti_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_utenti_oggetti_det, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "cod_azienda", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_utenti_oggetti_lista from uo_cs_xx_dw within w_utenti_oggetti
integer x = 23
integer y = 20
integer width = 2377
integer height = 660
integer taborder = 20
string dataobject = "d_utenti_oggetti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_path.enabled = true	
	cb_documento.enabled = false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_path.enabled = true	
	cb_documento.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_path.enabled = false
	cb_documento.enabled = true
end if
end event

type cb_path from commandbutton within w_utenti_oggetti
integer x = 2309
integer y = 960
integer width = 69
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "..."
end type

event clicked;string ls_path, ls_nome

integer li_valore

li_valore = GetFileSaveName("Selezionare percorso e nome file", ls_path, ls_nome, "bmp", "BitMap (*.bmp),*.bmp")

IF li_valore = 1 THEN 
	dw_utenti_oggetti_det.setitem(dw_utenti_oggetti_det.getrow(),"path_oggetto",ls_path)
end if

end event

type cb_documento from commandbutton within w_utenti_oggetti
integer x = 2034
integer y = 1100
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_utente, ls_cod_oggetto, ls_db, ls_doc
integer li_i, li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)

li_i = dw_utenti_oggetti_lista.getrow()
ls_cod_utente = dw_utenti_oggetti_lista.getitemstring(li_i, "cod_utente")
ls_cod_oggetto = dw_utenti_oggetti_lista.getitemstring(li_i, "cod_oggetto")

select prog_mimetype
into :ll_prog_mimetype
from utenti_oggetti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :ls_cod_utente and 
	cod_oggetto = :ls_cod_oggetto;
	
selectblob blob_oggetto
into :lbl_blob
from utenti_oggetti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :ls_cod_utente and 
	cod_oggetto = :ls_cod_oggetto;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update utenti_oggetti
		set blob_oggetto = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :ls_cod_utente and 
			cod_oggetto = :ls_cod_oggetto;
	else
		updateblob utenti_oggetti
		set blob_oggetto = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :ls_cod_utente and 
			cod_oggetto = :ls_cod_oggetto;
			
		update utenti_oggetti
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :ls_cod_utente and 
			cod_oggetto = :ls_cod_oggetto;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//
//	selectblob blob_oggetto
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       utenti_oggetti
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_utente = :ls_cod_utente and 
//	           cod_oggetto = :ls_cod_oggetto
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else	
//
//	selectblob blob_oggetto
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       utenti_oggetti
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_utente = :ls_cod_utente and 
//	           cod_oggetto = :ls_cod_oggetto;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob utenti_oggetti
//	   set        blob_oggetto = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_utente = :ls_cod_utente and 
//	              cod_oggetto = :ls_cod_oggetto
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob utenti_oggetti
//	   set        blob_oggetto = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_utente = :ls_cod_utente and 
//	              cod_oggetto = :ls_cod_oggetto;
//	
//	end if
//	
//	commit;
//end if
end event

type dw_utenti_oggetti_det from uo_cs_xx_dw within w_utenti_oggetti
integer x = 23
integer y = 700
integer width = 2377
integer height = 380
integer taborder = 30
string dataobject = "d_utenti_oggetti_det"
borderstyle borderstyle = styleraised!
end type

