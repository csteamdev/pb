﻿$PBExportHeader$cb_divisione_ricerca.sru
$PBExportComments$Tasto per richiamare Ricerca Attrezzature
forward
global type cb_divisione_ricerca from commandbutton
end type
end forward

global type cb_divisione_ricerca from commandbutton
integer width = 73
integer height = 80
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type
global cb_divisione_ricerca cb_divisione_ricerca

event clicked;if not isvalid(w_divisioni_ricerca) then
   window_open(w_divisioni_ricerca, 0)
end if

w_divisioni_ricerca.show()
end event

on cb_divisione_ricerca.create
end on

on cb_divisione_ricerca.destroy
end on

