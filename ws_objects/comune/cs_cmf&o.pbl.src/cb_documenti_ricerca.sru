﻿$PBExportHeader$cb_documenti_ricerca.sru
$PBExportComments$Command Button Documenti Ricerca
forward
global type cb_documenti_ricerca from commandbutton
end type
end forward

global type cb_documenti_ricerca from commandbutton
integer width = 73
integer height = 80
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type
global cb_documenti_ricerca cb_documenti_ricerca

event clicked;if not isvalid(w_documenti_ricerca) then
   window_open(w_documenti_ricerca, 0)
end if

w_documenti_ricerca.show()
end event

on cb_documenti_ricerca.create
end on

on cb_documenti_ricerca.destroy
end on

