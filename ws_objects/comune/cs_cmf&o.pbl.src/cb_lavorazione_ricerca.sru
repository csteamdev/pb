﻿$PBExportHeader$cb_lavorazione_ricerca.sru
$PBExportComments$Bottone Ricerca Fasi Lavorazione
forward
global type cb_lavorazione_ricerca from commandbutton
end type
end forward

global type cb_lavorazione_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_lavorazione_ricerca cb_lavorazione_ricerca

on clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA BOLLE DI VENDITA
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice del prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice del reparto
//
// ------------------------------------------------------------------------------------------
//dw_non_conformita_interna_1.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "cod_lavorazione"
//s_cs_xx.parametri.parametro_s_2 = "cod_reparto"
//s_cs_xx.parametri.parametro_s_3 = "cod_prodotto"
//s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_prodotto")
//s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_reparto")
//if isnull(s_cs_xx.parametri.parametro_s_10) then
//   messagebox("Ricerca Lavorazioni","Prodotto Mancante: la ricerca verrà effettuata su un numero elevato di elementi",Information!)
//end if
//window_open(w_ricerca_lavorazione, 0)
end on

