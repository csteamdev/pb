﻿$PBExportHeader$cb_listview.sru
$PBExportComments$Oggetto Picture Botton per Ricerca Prodotti ListView
forward
global type cb_listview from picturebutton
end type
end forward

global type cb_listview from picturebutton
int Width=74
int Height=81
int TabOrder=1
Alignment HTextAlign=Left!
boolean OriginalSize=true
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_listview cb_listview

event clicked;string ls_tipo_ricerca


SELECT flag
INTO   :ls_tipo_ricerca  
FROM   parametri_azienda
WHERE  ( parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( parametri_azienda.cod_parametro = 'TRP' )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Non è stato impostato il parametro TRP in parametri aziendali", StopSign!)
	return
end if

choose case ls_tipo_ricerca
	case "S"
		if not isvalid(w_prod_listview) then
			window_open(w_prod_listview, 0)
		end if
		w_prod_listview.show()
case "N"
		if not isvalid(w_prod_dw) then
			window_open(w_prod_dw, 0)
		end if
		w_prod_dw.show()
end choose

end event

event getfocus;//dw_prodotti_novita_det_1.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

event constructor;this.picturename=s_cs_xx.volume + s_cs_xx.risorse + "listwiev_3.bmp"
this.disabledname=s_cs_xx.volume + s_cs_xx.risorse + "listwiev_1.bmp"
end event

