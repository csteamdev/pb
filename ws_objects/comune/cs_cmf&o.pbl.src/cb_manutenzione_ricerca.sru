﻿$PBExportHeader$cb_manutenzione_ricerca.sru
$PBExportComments$Bottone Ricerca Manutenzione
forward
global type cb_manutenzione_ricerca from commandbutton
end type
end forward

global type cb_manutenzione_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_manutenzione_ricerca cb_manutenzione_ricerca

on clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA MANUTENZIONI
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice attrezzatura
//
// ------------------------------------------------------------------------------------------

//dw_non_conformita_interna_2.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "num_reg_manutenzione"
//s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_interna_1.getitemstring(dw_non_conformita_interna_1.getrow(),"cod_attrezzatura")
//if isnull(s_cs_xx.parametri.parametro_s_10) then
//   messagebox("Ricerca Manutenzione","Codice attrezzatura mancante",Stopsign!)
//   return
//end if
//
//window_open(w_ricerca_manutenzione, 0)
end on

