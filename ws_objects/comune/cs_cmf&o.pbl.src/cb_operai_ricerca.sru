﻿$PBExportHeader$cb_operai_ricerca.sru
forward
global type cb_operai_ricerca from commandbutton
end type
end forward

global type cb_operai_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_operai_ricerca cb_operai_ricerca

event clicked;if not isvalid(w_operai_ricerca) then
   window_open(w_operai_ricerca, 0)
end if

w_operai_ricerca.show()
end event

