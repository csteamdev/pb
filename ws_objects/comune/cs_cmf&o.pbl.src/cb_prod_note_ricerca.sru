﻿$PBExportHeader$cb_prod_note_ricerca.sru
$PBExportComments$Command Button Prodotti Note Ricerca
forward
global type cb_prod_note_ricerca from commandbutton
end type
end forward

global type cb_prod_note_ricerca from commandbutton
int Width=375
int Height=81
int TabOrder=1
string Text="Note Prodotto"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_prod_note_ricerca cb_prod_note_ricerca

on clicked;setnull(s_cs_xx.parametri.parametro_s_2)
window_open(w_prod_note_ricerca, 0)
if not isnull(s_cs_xx.parametri.parametro_s_2) then
   pcca.window_currentdw.setcolumn("nota_dettaglio")
   pcca.window_currentdw.settext(pcca.window_currentdw.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
end if

end on

