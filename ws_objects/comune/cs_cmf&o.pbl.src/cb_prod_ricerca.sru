﻿$PBExportHeader$cb_prod_ricerca.sru
$PBExportComments$Command Button Prodotti Ricerca
forward
global type cb_prod_ricerca from commandbutton
end type
end forward

global type cb_prod_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_prod_ricerca cb_prod_ricerca

on clicked;if not isvalid(w_prodotti_ricerca) then
   window_open(w_prodotti_ricerca, 0)
end if

w_prodotti_ricerca.show()
end on

