﻿$PBExportHeader$cb_stock_ricerca.sru
$PBExportComments$Bottone Ricerca Stock
forward
global type cb_stock_ricerca from commandbutton
end type
end forward

global type cb_stock_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global cb_stock_ricerca cb_stock_ricerca

event clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

//s_cs_xx.parametri.parametro_s_10 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_prodotto")
//s_cs_xx.parametri.parametro_s_11 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_deposito")
//s_cs_xx.parametri.parametro_s_12 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_ubicazione")
//s_cs_xx.parametri.parametro_s_13 = dw_non_conformita_vendite_det_1.getitemstring(dw_non_conformita_vendite_det_1.getrow(),"cod_lotto")
//setnull(s_cs_xx.parametri.parametro_data_1)
//s_cs_xx.parametri.parametro_d_1 = 0
//
//dw_non_conformita_vendite_det_1.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
//s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
//s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
//s_cs_xx.parametri.parametro_s_5 = "data_stock"
//s_cs_xx.parametri.parametro_s_6 = "prog_stock"
//
//window_open(w_ricerca_stock, 0)
end event

