﻿$PBExportHeader$u_seleziona_colore.sru
$PBExportComments$Oggetto per Selezione Colore (RGB)
forward
global type u_seleziona_colore from UserObject
end type
type st_blue from statictext within u_seleziona_colore
end type
type st_green from statictext within u_seleziona_colore
end type
type st_red from statictext within u_seleziona_colore
end type
type hsb_green from hscrollbar within u_seleziona_colore
end type
type hsb_red from hscrollbar within u_seleziona_colore
end type
type sle_blue from singlelineedit within u_seleziona_colore
end type
type sle_green from singlelineedit within u_seleziona_colore
end type
type sle_red from singlelineedit within u_seleziona_colore
end type
type hsb_blue from hscrollbar within u_seleziona_colore
end type
type rr_risultato from oval within u_seleziona_colore
end type
end forward

global type u_seleziona_colore from UserObject
int Width=773
int Height=613
boolean Border=true
long BackColor=78682240
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
event color_changed pbm_custom01
st_blue st_blue
st_green st_green
st_red st_red
hsb_green hsb_green
hsb_red hsb_red
sle_blue sle_blue
sle_green sle_green
sle_red sle_red
hsb_blue hsb_blue
rr_risultato rr_risultato
end type
global u_seleziona_colore u_seleziona_colore

type variables
int   ii_r, ii_g, ii_b
end variables

forward prototypes
public function long uf_get_rgb ()
public subroutine uf_set_rgb (long fl_rgb)
end prototypes

public function long uf_get_rgb ();return RGB (ii_r, ii_g, ii_b)
end function

public subroutine uf_set_rgb (long fl_rgb);// setta il valore del colore rosso
ii_r = Mod (fl_rgb, 256)
fl_rgb = fl_rgb / 256

// setta il valore del colore verde
ii_g = Mod (fl_rgb, 256)
fl_rgb = fl_rgb / 256

// setta il valore del colore blue
ii_b = Mod (fl_rgb, 256)

// setta il valore del rettangolo di anteprima
rr_risultato.fillcolor = rgb (ii_r, ii_g, ii_b)

// setta la posizione delle scrollbar
hsb_red.position = ii_r
hsb_green.position = ii_g
hsb_blue.position = ii_b

// setta il valore dei colori sulle caselle di testo
sle_red.text = String (ii_r)
sle_green.text = String (ii_g)
sle_blue.text = String (ii_b)
end subroutine

on u_seleziona_colore.create
this.st_blue=create st_blue
this.st_green=create st_green
this.st_red=create st_red
this.hsb_green=create hsb_green
this.hsb_red=create hsb_red
this.sle_blue=create sle_blue
this.sle_green=create sle_green
this.sle_red=create sle_red
this.hsb_blue=create hsb_blue
this.rr_risultato=create rr_risultato
this.Control[]={ this.st_blue,&
this.st_green,&
this.st_red,&
this.hsb_green,&
this.hsb_red,&
this.sle_blue,&
this.sle_green,&
this.sle_red,&
this.hsb_blue,&
this.rr_risultato}
end on

on u_seleziona_colore.destroy
destroy(this.st_blue)
destroy(this.st_green)
destroy(this.st_red)
destroy(this.hsb_green)
destroy(this.hsb_red)
destroy(this.sle_blue)
destroy(this.sle_green)
destroy(this.sle_red)
destroy(this.hsb_blue)
destroy(this.rr_risultato)
end on

type st_blue from statictext within u_seleziona_colore
int X=42
int Y=505
int Width=151
int Height=69
boolean Enabled=false
string Text="Blue:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=16711680
long BackColor=78682240
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_green from statictext within u_seleziona_colore
int X=5
int Y=409
int Width=193
int Height=69
boolean Enabled=false
string Text="Verde:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=65280
long BackColor=78682240
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_red from statictext within u_seleziona_colore
int X=42
int Y=313
int Width=161
int Height=69
boolean Enabled=false
string Text="Rosso:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=255
long BackColor=78682240
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type hsb_green from hscrollbar within u_seleziona_colore
int X=389
int Y=409
int Width=330
int Height=65
int TabOrder=30
boolean Enabled=false
int MaxPosition=255
end type

event pageright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore Verde di 50
/////////////////////////////////////////////////////////////////////

if ii_g > 205 then
	ii_g = 255
else
	ii_g = ii_g + 50
end if
sle_green.text = String (ii_g)
this.position = ii_g

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event lineleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore verde di 10
/////////////////////////////////////////////////////////////////////

if ii_g < 10 then
	ii_g = 0
else
	ii_g = ii_g - 10
end if
sle_green.text = String (ii_g)
this.position = ii_g
rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)

parent.TriggerEvent ('color_changed')
end event

event lineright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore verde di 10
/////////////////////////////////////////////////////////////////////

if ii_g > 245 then
	ii_g = 255
else
	ii_g = ii_g + 10
end if
sle_green.text = String (ii_g)
this.position = ii_g

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event moved;/////////////////////////////////////////////////////////////////////
// Cambia il colore verde in base alla posizione corrente
/////////////////////////////////////////////////////////////////////

ii_g = this.position
sle_green.text = String (ii_g)

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ('color_changed')
end event

event pageleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore Verde di 50
/////////////////////////////////////////////////////////////////////

if ii_g < 50 then
	ii_g = 0
else
	ii_g = ii_g - 50
end if
sle_green.text = String (ii_g)
this.position = ii_g

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

type hsb_red from hscrollbar within u_seleziona_colore
int X=389
int Y=321
int Width=330
int Height=65
int TabOrder=10
boolean Enabled=false
int MaxPosition=255
end type

event lineright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore rosso di 10
/////////////////////////////////////////////////////////////////////

if ii_r > 245 then
	ii_r = 255
else
	ii_r = ii_r + 10
end if
sle_red.text = String (ii_r)
this.position = ii_r

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event pageleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore Rosso di 50
/////////////////////////////////////////////////////////////////////

if ii_r < 50 then
	ii_r = 0
else
	ii_r = ii_r - 50
end if
sle_red.text = String (ii_r)
this.position = ii_r

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event lineleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore rosso di 10
/////////////////////////////////////////////////////////////////////

if ii_r < 10 then
	ii_r = 0
else
	ii_r = ii_r - 10
end if
sle_red.text = String (ii_r)
this.position = ii_r
rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)

parent.TriggerEvent ('color_changed')
end event

event moved;/////////////////////////////////////////////////////////////////////
// Cambia il colore rosso in base alla posizione corrente
/////////////////////////////////////////////////////////////////////

ii_r = this.position
sle_red.text = String (ii_r)

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ('color_changed')
end event

event pageright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore Rosso di 50
/////////////////////////////////////////////////////////////////////

if ii_r > 205 then
	ii_r = 255
else
	ii_r = ii_r + 50
end if
sle_red.text = String (ii_r)
this.position = ii_r

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

type sle_blue from singlelineedit within u_seleziona_colore
int X=225
int Y=505
int Width=147
int Height=69
int TabOrder=60
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
string Text="0"
string Pointer="arrow!"
long TextColor=16777215
long BackColor=16711680
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event getfocus;this.SelectText (1, Len (this.text))
end event

event modified;int li_nuovo_valore

if IsNumber (this.text) then
	li_nuovo_valore = Integer (this.text)
	if li_nuovo_valore < 0 or li_nuovo_valore > 255 then
		Beep (1)
		this.text = String (ii_b)
	else
		ii_b = li_nuovo_valore
		rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
		hsb_blue.position = ii_b
		parent.TriggerEvent ("color_changed")
	end if
else
	Beep (1)
	this.text = String (ii_b)
end if


end event

type sle_green from singlelineedit within u_seleziona_colore
int X=225
int Y=409
int Width=147
int Height=77
int TabOrder=40
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
string Text="0"
string Pointer="arrow!"
long TextColor=16777215
long BackColor=65280
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event getfocus;this.SelectText (1, Len (this.text))
end event

event modified;int li_nuovo_valore

if IsNumber (this.text) then
	li_nuovo_valore = Integer (this.text)
	if li_nuovo_valore < 0 or li_nuovo_valore > 255 then
		Beep (1)
		this.text = String (ii_g)
	else
		ii_g = li_nuovo_valore
		rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
		hsb_green.position = ii_g
		parent.TriggerEvent ("color_changed")
	end if
else
	Beep (1)
	this.text = String (ii_g)
end if


end event

type sle_red from singlelineedit within u_seleziona_colore
int X=225
int Y=313
int Width=147
int Height=77
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
string Text="0"
string Pointer="arrow!"
long TextColor=16777215
long BackColor=255
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event getfocus;this.SelectText (1, Len (this.text))
end event

event modified;int li_nuovo_valore

if IsNumber (this.text) then
	li_nuovo_valore = Integer (this.text)
	if li_nuovo_valore < 0 or li_nuovo_valore > 255 then
		Beep (1)
		this.text = String (ii_r)
	else
		ii_r = li_nuovo_valore
		rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
		hsb_red.position = ii_r
		parent.TriggerEvent ("color_changed")
	end if
else
	Beep (1)
	this.text = String (ii_r)
end if


end event

type hsb_blue from hscrollbar within u_seleziona_colore
int X=385
int Y=505
int Width=330
int Height=65
int TabOrder=50
boolean Enabled=false
int MaxPosition=255
end type

event pageright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore Blue di 50
/////////////////////////////////////////////////////////////////////

if ii_b > 205 then
	ii_b = 255
else
	ii_b = ii_b + 50
end if
sle_blue.text = String (ii_b)
this.position = ii_b

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event lineleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore Blue di 10
/////////////////////////////////////////////////////////////////////

if ii_b < 10 then
	ii_b = 0
else
	ii_b = ii_b - 10
end if
sle_blue.text = String (ii_b)
this.position = ii_b

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ('color_changed')
end event

event lineright;/////////////////////////////////////////////////////////////////////
// Incrementa il colore Blue di 10
/////////////////////////////////////////////////////////////////////

if ii_b > 245 then
	ii_b = 255
else
	ii_b = ii_b + 10
end if
sle_blue.text = String (ii_b)
this.position = ii_b

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

event moved;/////////////////////////////////////////////////////////////////////
// Cambia il colore blue in base alla posizione Corrente
/////////////////////////////////////////////////////////////////////

ii_b = this.position
sle_blue.text = String (ii_b)

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ('color_changed')
end event

event pageleft;/////////////////////////////////////////////////////////////////////
// Decrementa il colore Blue di 50
/////////////////////////////////////////////////////////////////////

if ii_b < 50 then
	ii_b = 0
else
	ii_b = ii_b - 50
end if
sle_blue.text = String (ii_b)
this.position = ii_b

rr_risultato.fillcolor = RGB (ii_r, ii_g, ii_b)
parent.TriggerEvent ("color_changed")
end event

type rr_risultato from oval within u_seleziona_colore
int X=46
int Y=21
int Width=686
int Height=261
boolean Enabled=false
int LineThickness=5
long FillColor=16777215
end type

