﻿$PBExportHeader$uo_calcolo_tempi.sru
$PBExportComments$UO per calcolo tempo tra due date
forward
global type uo_calcolo_tempi from nonvisualobject
end type
end forward

global type uo_calcolo_tempi from nonvisualobject
end type
global uo_calcolo_tempi uo_calcolo_tempi

forward prototypes
public function decimal wf_differenza_date (date data_inizio, time ora_inizio, date data_fine, time ora_fine)
end prototypes

public function decimal wf_differenza_date (date data_inizio, time ora_inizio, date data_fine, time ora_fine);long ll_ora, ll_giorni
decimal differenza

if data_inizio = data_fine then
	ll_ora = SecondsAfter(ora_inizio, ora_fine)
	differenza = ll_ora / 3600
else 	
	ll_ora = SecondsAfter(ora_inizio, ora_fine)
	if ll_ora <= 0 then
		ll_giorni = DaysAfter(data_inizio, data_fine)
		ll_ora = abs(SecondsAfter(ora_inizio, ora_fine))
		differenza = (ll_giorni * 24) - (ll_ora / 3600) 
	elseif ll_ora > 0 then
		ll_giorni = DaysAfter(data_inizio, data_fine)
		ll_ora = abs(SecondsAfter(ora_inizio, ora_fine))
		differenza = (ll_ora / 3600) + (ll_giorni * 24)
	end if
end if	
return differenza
end function

on uo_calcolo_tempi.create
TriggerEvent( this, "constructor" )
end on

on uo_calcolo_tempi.destroy
TriggerEvent( this, "destructor" )
end on

