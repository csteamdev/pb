﻿$PBExportHeader$uo_calendario_prod_new.sru
$PBExportComments$Motore di produzione di Ptenda
forward
global type uo_calendario_prod_new from nonvisualobject
end type
end forward

global type uo_calendario_prod_new from nonvisualobject
end type
global uo_calendario_prod_new uo_calendario_prod_new

type variables

end variables

forward prototypes
public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore)
public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
end prototypes

public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore);//ATTENZIONE QUESTO OGGETTO CON QUESTE DUE FUNZIONI CHE NON FANNO UNA MAZZA, NON è ASSOLUTAMENTE UN ERRORE
//DEVE RESTARE COSI COM'è, IN QUANTO NEL REPOSITORY GIBUS ESISTE TALE OGGETTO COMPLETO CON TUTTE LE SUE FUNZIONI
//IL MOTIVO RISIEDE NEL FATTO CHE QUESTO OGGETTO VIENE USATO IN ALCUNE FINESTRE DELLO STANDARD, MA SOLO IN AMBIENTE GIBUS
//PERTANTO PER EVITARE ERRORI DI RUNTIME O DI COMPILAZIONE O DI GETLATEST SI è PENSATO DI METETRE QUESTO OGGETTO CVOSI COM'è NELLO STANDARD


return 0
end function

public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);//ATTENZIONE QUESTO OGGETTO CON QUESTE DUE FUNZIONI CHE NON FANNO UNA MAZZA, NON è ASSOLUTAMENTE UN ERRORE
//DEVE RESTARE COSI COM'è, IN QUANTO NEL REPOSITORY GIBUS ESISTE TALE OGGETTO COMPLETO CON TUTTE LE SUE FUNZIONI
//IL MOTIVO RISIEDE NEL FATTO CHE QUESTO OGGETTO VIENE USATO IN ALCUNE FINESTRE DELLO STANDARD, MA SOLO IN AMBIENTE GIBUS
//PERTANTO PER EVITARE ERRORI DI RUNTIME O DI COMPILAZIONE O DI GETLATEST SI è PENSATO DI METETRE QUESTO OGGETTO CVOSI COM'è NELLO STANDARD


return 0
end function

on uo_calendario_prod_new.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_calendario_prod_new.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;//ATTENZIONE QUESTO OGGETTO CON QUESTE DUE FUNZIONI CHE NON FANNO UNA MAZZA, NON è ASSOLUTAMENTE UN ERRORE
//DEVE RESTARE COSI COM'è, IN QUANTO NEL REPOSITORY GIBUS ESISTE TALE OGGETTO COMPLETO CON TUTTE LE SUE FUNZIONI
//IL MOTIVO RISIEDE NEL FATTO CHE QUESTO OGGETTO VIENE USATO IN ALCUNE FINESTRE DELLO STANDARD, MA SOLO IN AMBIENTE GIBUS
//PERTANTO PER EVITARE ERRORI DI RUNTIME O DI COMPILAZIONE O DI GETLATEST SI è PENSATO DI METETRE QUESTO OGGETTO CVOSI COM'è NELLO STANDARD
end event

