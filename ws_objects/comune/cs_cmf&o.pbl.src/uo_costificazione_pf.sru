﻿$PBExportHeader$uo_costificazione_pf.sru
forward
global type uo_costificazione_pf from nonvisualobject
end type
end forward

global type uo_costificazione_pf from nonvisualobject
end type
global uo_costificazione_pf uo_costificazione_pf

type variables
transaction tran

string is_azienda
end variables

forward prototypes
public function integer uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, decimal ad_quantita_utilizzo, ref decimal ad_costo_lavorazioni, ref string as_messaggio)
public function integer uof_trova_mat_prime (string prodotto_finito, string cod_versione, ref string materia_prima[], ref decimal quantita_utilizzo[], decimal quantita_utilizzo_prec, long anno_commessa, long num_commessa, ref string as_messaggio)
public function integer uof_costo_mp (string as_cod_prodotto, string as_tipo, ref decimal ad_costo_mp, ref string as_messaggio)
public function integer uof_costo_medio_prod (string as_cod_prodotto, long al_giorni_ricerca, ref decimal ad_costo_medio, ref string as_messaggio)
public function integer uof_costificazione_pf (transaction transaction, string as_cod_azienda, string as_cod_prodotto, string as_cod_versione, decimal ad_quantita, ref decimal ad_costo_mp, ref decimal ad_costo_lavorazioni, ref decimal ad_costo_pf, ref string as_messaggio)
public function integer uof_costificazione_pf (transaction transaction, string as_cod_azienda, long al_anno_commessa, long al_num_commessa, decimal ad_quan_prodotta, ref decimal ad_costo_mp, ref decimal ad_costo_lavorazioni, ref decimal ad_costo_pf, ref string as_messaggio)
public function integer uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, long al_anno_commessa, long al_num_commessa, decimal ad_quantita_produzione, ref decimal ad_costo_lavorazioni, ref string as_messaggio)
end prototypes

public function integer uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, decimal ad_quantita_utilizzo, ref decimal ad_costo_lavorazioni, ref string as_messaggio);long   ll_i, ll_count, ll_rows

string ls_cod_prodotto_figlio[], ls_cod_versione_figlio[], ls_test, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, &
		 ls_cod_cat_attrezzature_2, ls_test_prodotto_f, ls_sql, ls_errore

dec{4} ldd_quan_utilizzo[], ldd_tempo_attrezzaggio, ldd_tempo_attrezzaggio_commessa, ldd_tempo_lavorazione, &
		 ldd_tempo_risorsa_umana, ldd_costo_medio_orario, ldd_costo_medio_orario_2, ldd_tempo_totale
		 
datastore lds_data


ls_sql = g_str.format("select cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo from distinta where cod_azienda = '$1' and cod_prodotto_padre = '$2' and cod_versione = '$3' order by num_sequenza asc  ", is_azienda,as_cod_prodotto, as_cod_versione)
    
ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql, tran, ls_errore)

if ll_rows < 0 then
	as_messaggio = "Errore nel datastore delle lavorazioni: " + ls_errore
	return -1
end if

for ll_count = 1 to ll_rows
	ls_cod_prodotto_figlio[ll_count] = lds_data.getitemstring(ll_count, 1)
	ls_cod_versione_figlio[ll_count] = lds_data.getitemstring(ll_count, 2)
	ldd_quan_utilizzo[ll_count] = lds_data.getitemnumber(ll_count, 3)
	ldd_quan_utilizzo[ll_count] = ldd_quan_utilizzo[ll_count] * ad_quantita_utilizzo
next

destroy lds_data

for ll_i = 1 to ll_count
	
	select cod_azienda
	into   :ls_test
	from   distinta
	where  cod_azienda = :is_azienda 	and 	 
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_i] and
	       cod_versione = :ls_cod_versione_figlio[ll_i]
	using  tran;
	
	if tran.sqlcode = 100 then
		continue
	end if

   SELECT cod_reparto,   
			cod_lavorazione,
			cod_cat_attrezzature,
			tempo_attrezzaggio,
			tempo_attrezzaggio_commessa,
			tempo_lavorazione,
			cod_cat_attrezzature_2,
			 tempo_risorsa_umana
   INTO   :ls_cod_reparto,   
          	:ls_cod_lavorazione,
			 :ls_cod_cat_attrezzature,
 			 :ldd_tempo_attrezzaggio,
		    :ldd_tempo_attrezzaggio_commessa,
			 :ldd_tempo_lavorazione,
			 :ls_cod_cat_attrezzature_2,
			 :ldd_tempo_risorsa_umana
   FROM   tes_fasi_lavorazione
   WHERE  cod_azienda = :is_azienda  	AND    
			 cod_prodotto = :ls_cod_prodotto_figlio[ll_i] and
			 cod_versione = :ls_cod_versione_figlio[ll_i]
	using  tran;

	if tran.sqlcode=0 then
  
		select costo_medio_orario
		into   :ldd_costo_medio_orario
		from   tab_cat_attrezzature
		where  cod_azienda=:is_azienda
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature
		using  tran;
	
		select costo_medio_orario
		into   :ldd_costo_medio_orario_2
		from   tab_cat_attrezzature
		where  cod_azienda=:is_azienda
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2
		using  tran;
		
		ldd_tempo_attrezzaggio = ldd_tempo_attrezzaggio
		ldd_tempo_attrezzaggio_commessa = ldd_tempo_attrezzaggio_commessa
		ldd_tempo_lavorazione = ldd_tempo_lavorazione * ldd_quan_utilizzo[ll_i]
		ldd_tempo_risorsa_umana = ldd_tempo_risorsa_umana * ldd_quan_utilizzo[ll_i]
		
		ldd_tempo_totale = ldd_tempo_attrezzaggio + ldd_tempo_attrezzaggio_commessa + ldd_tempo_lavorazione
		
		ad_costo_lavorazioni = ad_costo_lavorazioni + (ldd_tempo_totale/60) * ldd_costo_medio_orario
		
		ad_costo_lavorazioni = ad_costo_lavorazioni + (ldd_tempo_risorsa_umana/60) * ldd_costo_medio_orario_2
	end if
	
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :is_azienda	and	 
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_i] and
			 cod_versione = :ls_cod_versione_figlio[ll_i]
	using  tran;
	
	if tran.sqlcode = 100 then
		continue
	else	
		if uof_fasi_lavorazione(ls_cod_prodotto_figlio[ll_i], ls_cod_versione_figlio[ll_i],ldd_quan_utilizzo[ll_i],ad_costo_lavorazioni,as_messaggio) = -1 then
			return -1
		end if
	end if
	
next

return 0
end function

public function integer uof_trova_mat_prime (string prodotto_finito, string cod_versione, ref string materia_prima[], ref decimal quantita_utilizzo[], decimal quantita_utilizzo_prec, long anno_commessa, long num_commessa, ref string as_messaggio);long   ll_conteggio, ll_t, ll_max,ll_cont_figli,ll_cont_figli_integrazioni

string ls_cod_prodotto_figlio[], ls_flag_materia_prima[], ls_cod_prodotto_variante, ls_flag_materia_prima_variante,ls_test

dec{4} ld_quantita_utilizzo[], ld_quan_utilizzo_variante


declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
        quan_utilizzo,
		  flag_materia_prima 
from    distinta 
where   cod_azienda = :is_azienda 
and     cod_prodotto_padre = :prodotto_finito
and     cod_versione=:cod_versione
union all
select  cod_prodotto_figlio,
        quan_utilizzo,
		  flag_materia_prima 
from    integrazioni_commessa
where   cod_azienda = :is_azienda 
and     anno_commessa = :anno_commessa
and     num_commessa = :num_commessa
and     cod_prodotto_padre = :prodotto_finito
using   tran;

open righe_distinta_3;

if tran.sqlcode <> 0 then
	as_messaggio = "Errore nella open del cursore righe_distinta_3: " + tran.sqlerrtext
	return -1
end if

ll_conteggio = 1

do while true

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
		   :ld_quantita_utilizzo[ll_conteggio],
			:ls_flag_materia_prima[ll_conteggio];

   if tran.sqlcode < 0 then
		as_messaggio = "Errore nella fetch del cursore righe_distinta_3: " + tran.sqlerrtext
		close righe_distinta_3;
		return -1
	elseif tran.sqlcode = 100 then
		ll_conteggio --
		close righe_distinta_3;
		exit
	end if
	
	if not isnull(anno_commessa) and not isnull(num_commessa) then
		select quan_utilizzo,
				 cod_prodotto,
				 flag_materia_prima
		into   :ld_quan_utilizzo_variante,
				 :ls_cod_prodotto_variante,
				 :ls_flag_materia_prima_variante
		from   varianti_commesse
		where  cod_azienda=:is_azienda
		and    anno_commessa=:anno_commessa
		and    num_commessa=:num_commessa
		and    cod_prodotto_padre=:prodotto_finito
		and    cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_conteggio]
		and    cod_versione =:cod_versione
		using  tran;

		if tran.sqlcode <> 100 and not isnull(ld_quan_utilizzo_variante) then
			ld_quantita_utilizzo[ll_conteggio]=ld_quan_utilizzo_variante
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_variante
		end if
	
	end if
	
	ld_quantita_utilizzo[ll_conteggio]=ld_quantita_utilizzo[ll_conteggio]*quantita_utilizzo_prec
	
	ll_conteggio ++

loop

for ll_t = 1 to ll_conteggio

	if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		ll_cont_figli_integrazioni = 0
		
		SELECT count(*)
		into   :ll_cont_figli
		FROM   distinta  
		WHERE  cod_azienda = :is_azienda
		AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]
		and    cod_versione=:cod_versione
		using  tran;
	
		select count(*)
		into   :ll_cont_figli_integrazioni
		from   integrazioni_commessa
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :anno_commessa and
				 num_commessa = :num_commessa and
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]
		using  tran;
		
		ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni

		if tran.sqlcode=100 then
			ll_max=upperbound(materia_prima)
			ll_max++		
			materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
			quantita_utilizzo[ll_max] =ld_quantita_utilizzo[ll_t]
	
			if not isnull(anno_commessa) and not isnull(num_commessa) then
				select quan_utilizzo,	
						 cod_prodotto
				into   :ld_quan_utilizzo_variante,
						 :ls_cod_prodotto_variante
				from   varianti_commesse
				where  cod_azienda=:is_azienda
				and    anno_commessa=:anno_commessa
				and    num_commessa=:num_commessa
				and    cod_prodotto_padre=:prodotto_finito
				and    cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_t]
				and    cod_versione =:cod_versione
				using  tran;
		
				if tran.sqlcode <> 100 and not isnull(ld_quan_utilizzo_variante) then
					ld_quantita_utilizzo[ll_t]=ld_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t]=ls_cod_prodotto_variante
				end if
	
			end if
			
		else
			if uof_trova_mat_prime(ls_cod_prodotto_figlio[ll_t],cod_versione,materia_prima[],quantita_utilizzo[],ld_quantita_utilizzo[ll_t],anno_commessa,num_commessa,as_messaggio) = -1 then
				return -1
			end if
		end if
	else
		ll_max=upperbound(materia_prima)
		ll_max++		
		materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
		quantita_utilizzo[ll_max] =ld_quantita_utilizzo[ll_t]

		if not isnull(anno_commessa) and not isnull(num_commessa) then
			select quan_utilizzo,	
					 cod_prodotto
			into   :ld_quan_utilizzo_variante,
					 :ls_cod_prodotto_variante
			from   varianti_commesse
			where  cod_azienda=:is_azienda
			and    anno_commessa=:anno_commessa
			and    num_commessa=:num_commessa
			and    cod_prodotto_padre=:prodotto_finito
			and    cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_t]
			and    cod_versione =:cod_versione
			using  tran;
	
			if tran.sqlcode <> 100 and not isnull(ld_quan_utilizzo_variante) then
				ld_quantita_utilizzo[ll_t]=ld_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t]=ls_cod_prodotto_variante
			end if

		end if
	end if
next

return 0
end function

public function integer uof_costo_mp (string as_cod_prodotto, string as_tipo, ref decimal ad_costo_mp, ref string as_messaggio);long   ll_giorni

string ls_cod_costo, ls_flag_costo


// Lettura del tipo costo associato al prodotto
select cod_tipo_costo_mp
into	 :ls_cod_costo
from	 anag_prodotti
where  cod_azienda = :is_azienda and
		 cod_prodotto = :as_cod_prodotto
using  tran;
	
if tran.sqlcode < 0 then
	as_messaggio = "Errore in lettura tipo costo materia prima da anag_prodotti: " + tran.sqlerrtext
	return -1
elseif tran.sqlcode = 100 then
	as_messaggio = "Errore in lettura tipo costo materia prima da anag_prodotti: prodotto non trovato"
	return -1
end if

// Se per il prodotto non è specificato un tipo costo allora si considera il parametro (T)ipo (C)osto (D)efault
if isnull(ls_cod_costo) then
	
	select stringa
	into 	 :ls_cod_costo
	from	 parametri_azienda
	where  cod_azienda = :is_azienda and
			 cod_parametro = 'TCD'
	using  tran;
			 
	if tran.sqlcode < 0 then
		as_messaggio = "Errore in lettura tipo costo default da parametri_azienda: " + tran.sqlerrtext
		return -1
	elseif tran.sqlcode = 100 then
		as_messaggio = "Errore in lettura tipo costo default da parametri_azienda: parametro TCD non trovato"
		return -1
	end if
	
end if

// Lettura dei parametri del tipo costo
choose case as_tipo
	
	// In caso di calcolo preventivo
	case "P"	

		select flag_tipo_costo_prev,
				 num_giorni_ricerca_prev
		into   :ls_flag_costo,
				 :ll_giorni
		from   tab_tipi_costi_mp
		where  cod_azienda = :is_azienda and
				 cod_tipo_costo_mp = :ls_cod_costo
		using  tran;
	
	// In caso di calcolo consuntivo
	case "C"
		
		select flag_tipo_costo_cons,
				 num_giorni_ricerca_cons
		into   :ls_flag_costo,
				 :ll_giorni
		from   tab_tipi_costi_mp
		where  cod_azienda = :is_azienda and
				 cod_tipo_costo_mp = :ls_cod_costo
		using  tran;
		
end choose
		
if tran.sqlcode < 0 then
	as_messaggio = "Errore in lettura parametri tipo costo da tab_tipi_costi_mp: " + tran.sqlerrtext
	return -1
elseif tran.sqlcode = 100 then
	as_messaggio = "Errore in lettura parametri tipo costo da tab_tipi_costi_mp: tipo costo non trovato"
	return -1
end if

choose case ls_flag_costo
	
	// Costo MP = Costo Medio	
	case "A"
		
		if uof_costo_medio_prod(as_cod_prodotto,ll_giorni,ad_costo_mp,as_messaggio) = -1 then
			as_messaggio = "Errore in calcolo costo medio prodotto.~n" + as_messaggio
			return -1
		end if			
	
	// Costo MP = Costo Standard
	case "B"
		
		select costo_standard
		into   :ad_costo_mp
		from   anag_prodotti
		where  cod_azienda = :is_azienda and
				 cod_prodotto = :as_cod_prodotto
		using  tran;
		
	// Costo MP = Costo Ultimo
	case "C"
		
		select costo_ultimo
		into   :ad_costo_mp
		from   anag_prodotti
		where  cod_azienda = :is_azienda and
				 cod_prodotto = :as_cod_prodotto
		using  tran;
		
		if ad_costo_mp = 0 or isnull(ad_costo_mp) then
			// Se con il costo ultimo non trova niente, allora tenta con il costo standard
			select costo_standard
			into   :ad_costo_mp
			from   anag_prodotti
			where  cod_azienda = :is_azienda and
					 cod_prodotto = :as_cod_prodotto
			using  tran;
		end if
		
	// Costo MP = Costo Acquisto
	case "D"
		
		select prezzo_acquisto
		into   :ad_costo_mp
		from   anag_prodotti
		where  cod_azienda = :is_azienda and
				 cod_prodotto = :as_cod_prodotto
		using  tran;
		
end choose

if tran.sqlcode <> 0 then
	as_messaggio = "Errore in lettura costo materia prima da anag_prodotti: " + tran.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_costo_medio_prod (string as_cod_prodotto, long al_giorni_ricerca, ref decimal ad_costo_medio, ref string as_messaggio);datetime ldt_da, ldt_a, ldt_chiusura

dec{4} 	ld_quan_tot, ld_val_tot, ld_quan_movimento, ld_val_movimento

string 	ls_cod_tipo_mov, ls_cod_tipo_mov_det, ls_flag_quan, ls_flag_val, ls_flag_fornitore


ld_val_tot = 0

ld_quan_tot = 0

// Impostazione dell'intervallo di date in cui eseguire la ricerca
ldt_a = datetime(today(),00:00:00)

ldt_da = datetime(relativedate(today(),al_giorni_ricerca * -1),00:00:00)

select data_chiusura_annuale
into   :ldt_chiusura
from   con_magazzino
where  cod_azienda = :is_azienda
using  tran;

if tran.sqlcode <> 0 then
	as_messaggio = "Errore in lettura data chiusura annuale da con_magazzino: " + tran.sqlerrtext
	return -1
end if

// La data di partenza non può essere inferiore alla data dell'ultima chiusura annuale
if ldt_da < ldt_chiusura then
	ldt_da = ldt_chiusura
end if

// Vengono letti tutti i movimenti di magazzino del prodotto specificato compresi nelle date indicate
declare movimenti cursor for
select cod_tipo_movimento,
		 cod_tipo_mov_det,
		 quan_movimento,
		 val_movimento
from   mov_magazzino
where  cod_azienda = :is_azienda and
		 cod_prodotto = :as_cod_prodotto and
		 data_registrazione >= :ldt_da and
		 data_registrazione <= :ldt_a
using  tran;

open movimenti;

if tran.sqlcode <> 0 then
	as_messaggio = "Errore nella open del cursore movimenti: " + tran.sqlerrtext
	return -1
end if

do while true
	
	fetch movimenti
	into  :ls_cod_tipo_mov,
			:ls_cod_tipo_mov_det,
			:ld_quan_movimento,
			:ld_val_movimento;
			
	if tran.sqlcode < 0 then
		as_messaggio = "Errore nella fech del cursore movimenti: " + tran.sqlerrtext
		close movimenti;
		return -1
	elseif tran.sqlcode = 100 then
		close movimenti;
		exit
	end if
	
	select flag_fornitore
	into   :ls_flag_fornitore
	from   det_tipi_movimenti
	where  cod_azienda = :is_azienda and
			 cod_tipo_movimento = :ls_cod_tipo_mov and
			 cod_tipo_mov_det = :ls_cod_tipo_mov_det
	using  tran;
			 
	if tran.sqlcode <> 0 then
		as_messaggio = "Errore in lettura dati movimento da tabella det_tipi_movimenti: " + tran.sqlerrtext
		close movimenti;
		return -1
	end if
	
	if ls_flag_fornitore <> "N" then
		continue
	end if
	
	// Vengono lette le impostazioni del tipo movimento
	select flag_prog_quan_acquistata,
			 flag_val_quan_acquistata
	into   :ls_flag_quan,
			 :ls_flag_val
	from   tab_tipi_movimenti_det
	where  cod_azienda = :is_azienda and
			 cod_tipo_mov_det = :ls_cod_tipo_mov_det
	using  tran;
			 
	if tran.sqlcode <> 0 then
		as_messaggio = "Errore in lettura dati movimento da tab_tipi_mov_det: " + tran.sqlerrtext
		close movimenti;
		return -1
	end if
	
	// In base alle impostazioni del tipo movimento vengono incrementati i totali di quantita e valore
	choose case ls_flag_quan
		case "+"
			ld_quan_tot = ld_quan_tot + ld_quan_movimento
		case "-"
			ld_quan_tot = ld_quan_tot - ld_quan_movimento
	end choose
	
	choose case ls_flag_val
		case "+"
			ld_val_tot = ld_val_tot + (ld_quan_movimento * ld_val_movimento)
		case "-"
			ld_val_tot = ld_val_tot - (ld_quan_movimento * ld_val_movimento)
	end choose
	
loop

// Utilizzando i totali di quantità e valore di tutti i movimenti processati si calcola il costo medio
ad_costo_medio = round( ld_val_tot / ld_quan_tot , 4 )

return 0
end function

public function integer uof_costificazione_pf (transaction transaction, string as_cod_azienda, string as_cod_prodotto, string as_cod_versione, decimal ad_quantita, ref decimal ad_costo_mp, ref decimal ad_costo_lavorazioni, ref decimal ad_costo_pf, ref string as_messaggio);//******************************************************************************
//************************ Funzione di calcolo PREVENTIVO **********************
//******************************************************************************


// Impostazione del codice azienda da usare in tutte le funzioni dell'oggetto
is_azienda = as_cod_azienda

// Impostazione della transazione indipendente da usare in tutte le funzioni dell'oggetto
tran.ServerName = transaction.ServerName
tran.LogId = transaction.LogId
tran.LogPass = transaction.LogPass
tran.DBMS = transaction.DBMS
tran.Database = transaction.Database
tran.UserId = transaction.UserId
tran.DBPass = transaction.DBPass
tran.DBParm = transaction.DBParm
tran.Lock = transaction.Lock

// Connessione al DB con la transazione indipendente
connect using tran;

if tran.sqlcode <> 0 then
	as_messaggio = "Errore durante la connessione al DB: " + tran.sqlerrtext
	return -1
end if


////////////////////////////////////////////////////////////////////////////////
////////////////////// controllo esistenza prodotto finito /////////////////////

string ls_test


setnull(ls_test)

select cod_azienda
into	 :ls_test
from	 distinta_padri
where	 cod_azienda=:is_azienda and
		 cod_prodotto=:as_cod_prodotto and
		 cod_versione=:as_cod_versione
using  tran;

if tran.sqlcode < 0 then
	as_messaggio = "Errore in controllo esistenza prodotto finito.~nErrore nella select di anag_prodotti: " + tran.sqlerrtext
	return -1
end if

if isnull(ls_test) or ls_test = "" then
	as_messaggio = "Errore in controllo esistenza prodotto finito.~n" + &
						"Prodotto: " + as_cod_prodotto + "~nVersione: " + as_cod_versione + "~n" + &
						"Prodotto finito non trovato in tabella distinta_padri"
	return -1
end if

////////////////////// calcolo costo materie prime /////////////////////////////

string ls_mat_prime[]

long   ll_null, ll_i

dec{4} ld_quan_utilizzo[], ld_costo_mp


setnull(ll_null)

if uof_trova_mat_prime(as_cod_prodotto,as_cod_versione,ls_mat_prime[],ld_quan_utilizzo[],ad_quantita,ll_null,ll_null,as_messaggio) <> 0 then
	as_messaggio = "Errore in calcolo costo materie prime.~n" + as_messaggio
	return -1
end if

ad_costo_mp = 0

for ll_i = 1 to upperbound(ls_mat_prime)
	
	if uof_costo_mp(ls_mat_prime[ll_i],"P",ld_costo_mp,as_messaggio) <> 0 then
		as_messaggio = "Errore in lettura costo materia prima.~n" + as_messaggio
		return -1
	end if
	
	ad_costo_mp = ad_costo_mp + ( ld_costo_mp * ld_quan_utilizzo[ll_i] )
	
next

//////////////////////// calcolo costo lavorazioni /////////////////////////////

if uof_fasi_lavorazione(as_cod_prodotto,as_cod_versione,ad_quantita,ad_costo_lavorazioni,as_messaggio) <> 0 then
	as_messaggio = "Errore in calcolo costo lavorazioni.~n" + as_messaggio
	return -1
end if

////////////////////// calcolo totale prodotto finito //////////////////////////

ad_costo_pf = ad_costo_mp + ad_costo_lavorazioni

////////////////////////////////////////////////////////////////////////////////


return 0
end function

public function integer uof_costificazione_pf (transaction transaction, string as_cod_azienda, long al_anno_commessa, long al_num_commessa, decimal ad_quan_prodotta, ref decimal ad_costo_mp, ref decimal ad_costo_lavorazioni, ref decimal ad_costo_pf, ref string as_messaggio);//************************ Funzione di calcolo CONSUNTIVO ***************************
string ls_cod_prodotto, ls_cod_versione, ls_mat_prime[], ls_null,ls_versione_mat_prima[], ls_cod_deposito_prelievo, ls_errore
long   ll_null, ll_i
dec{4} ld_quan_utilizzo[], ld_costo_mp, ld_quan_prodotta
double  ldd_quantita_utilizzo[]



// Impostazione del codice azienda da usare in tutte le funzioni dell'oggetto
is_azienda = as_cod_azienda

// Impostazione della transazione indipendente da usare in tutte le funzioni dell'oggetto
tran.ServerName = transaction.ServerName
tran.LogId = transaction.LogId
tran.LogPass = transaction.LogPass
tran.DBMS = transaction.DBMS
tran.Database = transaction.Database
tran.UserId = transaction.UserId
tran.DBPass = transaction.DBPass
tran.DBParm = transaction.DBParm
tran.Lock = transaction.Lock

// Connessione al DB con la transazione indipendente
connect using tran;

if tran.sqlcode <> 0 then
	as_messaggio = "Errore durante la connessione al DB: " + tran.sqlerrtext
	return -1
end if


////////////////////////////////////////////////////////////////////////////////
////////////////////////// Controllo esistenza commessa ////////////////////////


select cod_prodotto,
		 cod_versione,
		 quan_prodotta
into   :ls_cod_prodotto,
		 :ls_cod_versione,
		 :ld_quan_prodotta
from   anag_commesse
where  cod_azienda = :is_azienda and
		 anno_commessa = :al_anno_commessa and
		 num_commessa = :al_num_commessa
using  tran;

if tran.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati commessa da anag_commesse: " + tran.sqlerrtext
	return -1
elseif tran.sqlcode = 100 then
	as_messaggio = "Errore in controllo esistenza commessa.~n" + &
						"Commessa: " + string(al_anno_commessa) + "/" + string(al_num_commessa) + "~n" + &
						"Commessa specificata non trovata in tabella anag_commesse"
	return -1
end if

if ad_quan_prodotta = 0 or isnull(ad_quan_prodotta) then
	// se la quantità prodotta passata è 0 provo a prenderla dalla commessa
	ad_quan_prodotta = ld_quan_prodotta
end if
////////////////////////// Calcolo costo materie prime /////////////////////////
setnull(ll_null)
setnull(ls_null)
uo_funzioni_1 luo_funzion1
luo_funzion1 = create uo_funzioni_1
luo_funzion1.uof_trova_mat_prime_varianti(al_anno_commessa, al_num_commessa, ls_cod_prodotto, false, ls_mat_prime[], ref ls_versione_mat_prima[], ref ldd_quantita_utilizzo[], ref ls_cod_deposito_prelievo, ref ls_errore)
destroy luo_funzion1

ad_costo_mp = 0

for ll_i = 1 to upperbound(ls_mat_prime)
	ld_quan_utilizzo[ll_i] = dec(ldd_quantita_utilizzo[ll_i])
	if uof_costo_mp(ls_mat_prime[ll_i],"P",ld_costo_mp,as_messaggio) <> 0 then
		as_messaggio = "Errore in lettura costo materia prima.~n" + as_messaggio
		return -1
	end if
	
	ad_costo_mp = ad_costo_mp + ( ld_costo_mp * ld_quan_utilizzo[ll_i] )
	
next

//------ calcolo costo lavorazioni-
	
if uof_fasi_lavorazione(ls_cod_prodotto,ls_cod_versione,al_anno_commessa,al_num_commessa, ad_quan_prodotta,ad_costo_lavorazioni,as_messaggio) = -1 then
	as_messaggio = "Errore in calcolo costo lavorazioni.~n" + as_messaggio
	return -1
end if
	
//------ calcolo totale prodotto finito-
	
ad_costo_pf = ad_costo_mp + ad_costo_lavorazioni
	
return 0
end function

public function integer uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, long al_anno_commessa, long al_num_commessa, decimal ad_quantita_produzione, ref decimal ad_costo_lavorazioni, ref string as_messaggio);long   ll_count, ll_i, ll_rows, ll_num_sequenza[], ll_num_sequenza_variante

string ls_cod_prodotto_figlio[], ls_flag_materia_prima[], ls_cod_prodotto_inserito, ls_cod_prodotto_variante, &
		 ls_flag_materia_prima_variante, ls_test, ls_cod_reparto, ls_cod_lavorazione, ls_cod_cat_attrezzature, &
		 ls_cod_cat_attrezzature_2, ls_test_prodotto_f, ls_sql, ls_errore, ls_cod_versione_figlio[],ls_cod_versione_variante, ls_flag_esterna, ls_flag_uso

dec{4} ld_quan_utilizzo[], ld_quan_utilizzo_variante, ld_costo_medio_orario, ld_costo_medio_orario_2, &
		 ld_tempo_attrezzaggio, ld_tempo_attrezzaggio_commessa, ld_tempo_lavorazione, ld_tempo_risorsa_umana, &
		 ld_quan_prodotta_fase, ld_tempo_totale, ld_costo_orario,ld_costo_pezzo,ld_tempo_movimentazione

datastore lds_data


ls_sql = g_str.format("select   cod_prodotto_figlio,   cod_versione_figlio, quan_utilizzo, flag_materia_prima, num_sequenza  from distinta where cod_azienda = '$1' and cod_prodotto_padre = '$2' and cod_versione = '$3' order by distinta.num_sequenza asc", is_azienda,as_cod_prodotto, as_cod_versione)

ll_rows=guo_functions.uof_crea_datastore( lds_data, ls_sql, tran, ls_errore)

if  ll_rows< 0 then
	as_messaggio = "Errore datastore lavorazioni: " + ls_errore
	return -1
end if

for ll_count = 1 to ll_rows
	ls_cod_prodotto_figlio[ll_count] = lds_data.getitemstring(ll_count,1)
	ls_cod_versione_figlio[ll_count] = lds_data.getitemstring(ll_count,2)
	ld_quan_utilizzo[ll_count] = lds_data.getitemnumber(ll_count,3) * ad_quantita_produzione
	ls_flag_materia_prima[ll_count]  = lds_data.getitemstring(ll_count,4)
	ll_num_sequenza[ll_count]  = lds_data.getitemnumber(ll_count,5)
next
	
for ll_i = 1 to ll_rows
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio[ll_i]
	
	select cod_prodotto,
			cod_versione_variante,
			quan_utilizzo,
			flag_materia_prima
	into	:ls_cod_prodotto_variante,	
			:ls_cod_versione_variante,
			:ld_quan_utilizzo_variante,
			:ls_flag_materia_prima_variante
	from  varianti_commesse
	where  cod_azienda=:is_azienda and
			anno_commessa=:al_anno_commessa and
			num_commessa=:al_num_commessa and
			cod_prodotto_padre=:as_cod_prodotto and
			cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_i] and
			cod_versione=:as_cod_versione and
			cod_versione_figlio = :ls_cod_versione_figlio[ll_i] and
			num_sequenza = :ll_num_sequenza[ll_count]
	using  tran;

   if tran.sqlcode < 0 then
		as_messaggio = "Errore nella select di varianti_commesse: " + tran.sqlerrtext
		return -1
	end if

	if tran.sqlcode <> 100 then
		ls_cod_prodotto_figlio[ll_i] = ls_cod_prodotto_variante
		ls_cod_versione_figlio[ll_i] = ls_cod_versione_variante
		ls_flag_materia_prima[ll_i]  = ls_flag_materia_prima_variante
		ld_quan_utilizzo[ll_i] = ld_quan_utilizzo_variante * ad_quantita_produzione
	end if		

	if ls_flag_materia_prima[ll_i] = 'N' then
		// verifico se c'è una distinta sotto e proseguo solo in caso affernativo
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda=:is_azienda 
		and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio[ll_i] and
				cod_versione = :ls_cod_versione_figlio[ll_i]
		using  tran;
		
		if tran.sqlcode <> 0 then continue
	else
		continue
	end if

//	select cod_azienda
//	into   :ls_test
//	from   distinta
//	where  cod_azienda=:is_azienda 
//	and 	 cod_prodotto_padre=:ls_cod_prodotto_figlio[ll_i]
//	using  tran;
//	
//	if tran.sqlcode=100 then continue
	
   SELECT cod_reparto,   
			cod_lavorazione,
			cod_cat_attrezzature,
			cod_cat_attrezzature_2,
			tempo_attrezzaggio,
			tempo_attrezzaggio_commessa,
			tempo_lavorazione,
			tempo_risorsa_umana,
			flag_esterna,
			costo_orario,
			costo_pezzo,
			tempo_movimentazione,
			flag_uso
   INTO	:ls_cod_reparto,   
			:ls_cod_lavorazione,
			:ls_cod_cat_attrezzature,
			:ls_cod_cat_attrezzature_2,
			:ld_tempo_attrezzaggio,
			:ld_tempo_attrezzaggio_commessa,
			:ld_tempo_lavorazione,
			:ld_tempo_risorsa_umana,
			:ls_flag_esterna,
			:ld_costo_orario,
			:ld_costo_pezzo,
			:ld_tempo_movimentazione,
			:ls_flag_uso
   FROM  tes_fasi_lavorazione
   WHERE cod_azienda = :is_azienda and
			cod_prodotto = :ls_cod_prodotto_figlio[ll_i] and
			cod_versione = :ls_cod_versione_figlio[ll_i]
	using  tran;

	if tran.sqlcode < 0 then
		as_messaggio = "Errore SQL in select di tes_fasi_lavorazione.~r~n " +tran.sqlerrtext
		return -1
	end if
	if tran.sqlcode = 0 then  /// se c'è la fase altrimento vado avanti
	
		/*
	  
		select costo_medio_orario
		into   :ld_costo_medio_orario
		from   tab_cat_attrezzature
		where  cod_azienda=:is_azienda
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature
		using  tran;
	
		select costo_medio_orario
		into   :ld_costo_medio_orario_2
		from   tab_cat_attrezzature
		where  cod_azienda=:is_azienda
		and    cod_cat_attrezzature=:ls_cod_cat_attrezzature_2
		using  tran;
		
		select sum(tempo_attrezzaggio),
				 sum(tempo_attrezzaggio_commessa),
				 sum(tempo_lavorazione),
				 sum(tempo_risorsa_umana),
				 sum(quan_prodotta)
		into   :ld_tempo_attrezzaggio,
				 :ld_tempo_attrezzaggio_commessa,
				 :ld_tempo_lavorazione,
				 :ld_tempo_risorsa_umana,
				 :ld_quan_prodotta_fase
		from   avan_produzione_com
		where  cod_azienda=:is_azienda
		and    anno_commessa=:al_anno_commessa
		and    num_commessa =:al_num_commessa	
		and    cod_prodotto =:ls_cod_prodotto_figlio[ll_i]
		and    cod_lavorazione=:ls_cod_lavorazione
		and    cod_reparto=:ls_cod_reparto
		using  tran;
		*/
	
		if isnull(ld_tempo_attrezzaggio) then ld_tempo_attrezzaggio = 0
		if isnull(ld_tempo_attrezzaggio_commessa) then ld_tempo_attrezzaggio_commessa = 0
		if isnull(ld_tempo_lavorazione) then ld_tempo_lavorazione = 0
		if isnull(ld_tempo_risorsa_umana) then ld_tempo_risorsa_umana = 0
		if isnull(ld_tempo_movimentazione) then ld_tempo_movimentazione = 0
		if isnull(ls_flag_uso) then ls_flag_uso = "O"
		// tutti i tempi in minuti
		
		choose case upper(ls_flag_uso)
			case "O"  	// gestione costo orario
				ld_tempo_totale = ld_tempo_attrezzaggio + ld_tempo_attrezzaggio_commessa + ld_tempo_movimentazione + (ld_tempo_lavorazione * ld_quan_utilizzo[ll_i]) + (ld_tempo_risorsa_umana * ld_quan_utilizzo[ll_i]) 
				ad_costo_lavorazioni = ad_costo_lavorazioni + ( round(ld_tempo_totale/60,2) * ld_costo_orario )
			case "P"		// gestione costo al pezzo (può andare bene per le lavorazioni esterne
				ad_costo_lavorazioni = ld_costo_pezzo * ld_quan_utilizzo[ll_i]
		end choose
	end if		
	
	// Se il prodotto figlio non ha una sua distinta inutle proseguire.
	select 	cod_prodotto_figlio 
	into   		:ls_test_prodotto_f
	from   	distinta
	where  	cod_azienda = :is_azienda and
				cod_prodotto_padre=:ls_cod_prodotto_figlio[ll_i] and
				cod_versione = :ls_cod_versione_figlio[ll_i]
	using  tran;

	if tran.sqlcode = 100 then
		continue
	else
		if uof_fasi_lavorazione(ls_cod_prodotto_figlio[ll_i],ls_cod_versione_figlio[ll_i],al_anno_commessa,al_num_commessa, ld_quan_utilizzo[ll_i],ad_costo_lavorazioni,as_messaggio) = -1 then
			return -1
		end if
	end if
	
next

return 0
end function

event constructor;tran = create transaction
end event

event destructor;disconnect using tran;

destroy tran
end event

on uo_costificazione_pf.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_costificazione_pf.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

