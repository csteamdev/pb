﻿$PBExportHeader$uo_dividi_stringa.sru
$PBExportComments$Funzione per dividere una stringa in tre righe
forward
global type uo_dividi_stringa from nonvisualobject
end type
end forward

global type uo_dividi_stringa from nonvisualobject
end type
global uo_dividi_stringa uo_dividi_stringa

forward prototypes
public subroutine wf_dividi_stringa (ref string fs_stringa)
end prototypes

public subroutine wf_dividi_stringa (ref string fs_stringa);string ls_stringa, ls_stringa_1, ls_stringa_2, ls_stringa_3
long ll_posizione, ll_posizione_1, ll_posizione_2, ll_posizione_3

ls_stringa = fs_stringa

do while 1 = 1 
	if Pos(ls_stringa, "~n~r") = 0 then exit
	ls_stringa = Left(ls_stringa, Pos(ls_stringa, "~n~r") - 1) + right(ls_stringa, len(ls_stringa) - Pos(ls_stringa, "~n~r") - 1)
loop

if len(ls_stringa) > 50 then

	if len(ls_stringa) > 49 then
		ll_posizione = 51
		do while 1 = 1
			ll_posizione = ll_posizione - 1
			if Mid(ls_stringa, ll_posizione, 1) = " " then exit
		loop
	end if			
	ls_stringa_1 = Left(ls_stringa, ll_posizione)
	
	ls_stringa = right(ls_stringa, len(ls_stringa) - ll_posizione)
	
	if len(ls_stringa) > 49 then
		ll_posizione = 51
		do while 1 = 1
			ll_posizione = ll_posizione - 1
			if Mid(ls_stringa, ll_posizione, 1) = " " then exit
		loop
	end if			
	ls_stringa_2 = Left(ls_stringa, ll_posizione)	
	
	ls_stringa_3 = right(ls_stringa, len(ls_stringa) - ll_posizione)
	
	ls_stringa = ls_stringa_1 + "~n~r" + ls_stringa_2 + "~n~r" + ls_stringa_3
	
	fs_stringa = ls_stringa
	
else	

	fs_stringa = ls_stringa
end if	
end subroutine

on uo_dividi_stringa.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_dividi_stringa.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

