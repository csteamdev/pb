﻿$PBExportHeader$uo_excel.sru
forward
global type uo_excel from nonvisualobject
end type
end forward

global type uo_excel from nonvisualobject
end type
global uo_excel uo_excel

type variables
constant string NUMBER_PATTERN = "#.##0,00"
constant string STRING_PATTERN = "@"

// Allineamento testo
constant int TEXT_ALIGN_LEFT = -4131
constant int TEXT_ALIGN_CENTER = -4108
constant int TEXT_ALIGN_RIGHT = -4152

constant int TEXT_ALIGN_TOP = -4160
constant int TEXT_ALIGN_MIDDLE = -4108
constant int TEXT_ALIGN_BOTTOM = -4107

constant int INSERT_DOWN = -4121
constant int INSERT_UP = -4162
constant int INSERT_RIGHT = -4161
constant int INSERT_LEFT = -4161 // TODO da cercare il valore della constante!


 private:
 	OLEObject iole_excel
	string is_error
	//string is_current_sheet // deprecato
	string is_worksheet[]
	string is_path_file
	
	// Se non specificato diversamento apro il file in sola lettura
	boolean ib_read_only = false
	
	// Indica se il file è stato aperto o no
	boolean ib_is_open = false
	boolean ib_is_new = false
	
	// Indica il foglio attuale
	uint il_current_sheet = 1
end variables

forward prototypes
public function boolean uof_open (string as_file_path, boolean ab_ole_visible)
public function string uof_get_error ()
public subroutine uof_get_sheets (ref string as_sheets[])
public function any uof_set (long al_row, string as_column, any aa_value)
public function any uof_set (long al_row, long al_column, any aa_value)
public function string uof_nome_cella (long al_colonna)
public function string uof_get_named_column (integer ai_column)
public function boolean uof_open (string as_file_path, boolean ab_ole_visible, boolean ab_read_only)
public function boolean uof_set_sheet (string as_sheet_name)
public function integer uof_get_num_column (string as_column)
public function string uof_read_string (long al_row, string as_column)
public function long uof_read_long (long al_row, string as_column)
public function decimal uof_read_decimal (long al_row, string as_column)
public function boolean uof_add_sheet (string as_name, boolean ab_fire_select)
public function boolean uof_add_sheet (string as_name)
public function boolean uof_clear_sheet (string as_sheet_name)
public function boolean uof_clear_sheet ()
public function integer uof_saveas (string as_file_path)
public subroutine uof_close ()
public subroutine uof_save ()
public function boolean uof_create (string as_file_path, boolean ab_ole_visibile)
public function string uof_get_current_sheet ()
private function string uof_range (long r, string c)
private function string uof_range (long r, long c)
public subroutine uof_set_autofit (long al_row, long al_column)
public subroutine uof_set_autofit (long al_row, string as_column)
public subroutine uof_set_format (long al_row, long al_column, string as_format)
public subroutine uof_set_format (long al_row, string as_column, string as_format)
private function string uof_range (long r_from, string c_from, long r_to, string c_to)
private function string uof_range (long r, any c)
private function string uof_range (long r_from, any c_from, long r_to, any c_to)
private function oleobject uof_get_range (long r, any c)
private function string uof_range (string c)
public subroutine uof_merge (long al_row_from, any aa_coloumn_from, long al_row_to, any aa_column_to)
public subroutine uof_set_horizontal_align (long al_row, any aa_column, integer ai_text_align)
public subroutine uof_set_vertical_align (long al_row, any aa_column, integer ai_text_align)
public subroutine uof_inser_image (long al_row, any aa_column, string as_file_path)
public subroutine uof_set_bold (long al_row, long al_column)
public subroutine uof_set_bold (long al_row, string as_column)
public subroutine uof_set_italic (long al_row, long al_column)
public subroutine uof_set_italic (long al_row, string as_column)
public subroutine uof_saveas (string as_file_path, integer ai_format)
public function date uof_read_date (long al_row, string as_column)
public function date uof_read_date (long al_row, long al_column)
public function decimal uof_read_decimal (long al_row, long al_column)
public function long uof_read_long (long al_row, long al_column)
public function string uof_read_string (long al_row, long al_column)
public function integer uof_get_used_range (ref oleobject aole_range)
public function boolean uof_set_sheet (unsignedinteger ai_sheet)
public function any uof_read (long al_row, string as_column)
public function integer uof_insert_row (long al_row, integer ai_insert_type)
public function integer uof_insert_column (string as_column, integer ai_insert_type)
public function integer uof_insert_column (integer ai_column, integer ai_insert_type)
public subroutine uof_set_border (integer ai_row, integer ai_col, integer ai_border_top, integer ai_border_right, integer ai_border_bottom, integer ai_border_left)
public subroutine uof_set_background (integer ai_row, integer ai_col, integer ai_color)
public subroutine uof_set_background_rgb (integer ai_row, integer ai_col, integer ai_color)
public subroutine uof_set_wrap_text (long al_row, long al_column)
public subroutine uof_set_font_size (long al_row, long al_column, long al_fontsize)
public subroutine uof_set_font_name (long al_row, long al_column, string as_fontname)
public subroutine uof_set_border (integer ai_row_from, integer ai_col_from, integer ai_row_to, integer ai_col_to, integer ai_border_top, integer ai_border_right, integer ai_border_bottom, integer ai_border_left)
public subroutine uof_set_horizontal_align (long al_row_from, any aa_column_from, integer al_row_to, integer aa_column_to, integer ai_text_align)
public function any uof_read (long al_row, long al_column)
private function oleobject uof_get_range (long r_from, any c_from, long r_to, any c_to)
public function integer uof_disable_warning_messages ()
public subroutine uof_set_rows_height (integer ai_row_from, integer ai_row_to, integer ai_height)
public function integer uof_insert_copied_rows (long al_row, integer ai_insert_type, long r_from, string c_from, long r_to, string c_to)
public subroutine uof_set_autofit (long ai_row_from, long ai_col_from, long ai_row_to, long ai_col_to)
public subroutine uof_set_font_color (long al_row, long al_column, long ai_colorindex)
public subroutine uof_set_font_size (long al_row_from, long al_column_from, integer al_row_to, integer al_column_to, long al_fontsize)
public subroutine uof_set_vertical_align (long al_row_from, any aa_column_from, long al_row_to, any aa_column_to, integer ai_text_align)
end prototypes

public function boolean uof_open (string as_file_path, boolean ab_ole_visible);/**
 * stefanop
 * 22/06/2011
 *
 * Apre il file excel
 *
 * as_file_path string indica il percorso del file da aprire
 * ab_ole_visible boolean indica se deve essere visibile l'OLE
 **/
 
return uof_open(as_file_path, ab_ole_visible, false)
end function

public function string uof_get_error ();return is_error
end function

public subroutine uof_get_sheets (ref string as_sheets[]);/**
 * stefanop
 * 17/05/2012
 *
 * Migliorata la gestione del recupero fogli di lavoro
 **/

long ll_i


if isnull(is_worksheet) or upperbound(is_worksheet) < 1 then
	
	for ll_i = 1  to iole_excel.worksheets.count
		is_worksheet[ll_i] = iole_excel.worksheets(ll_i).name
	next
	
end if

as_sheets = is_worksheet
end subroutine

public function any uof_set (long al_row, string as_column, any aa_value);/**
 * Imposta il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, as_column)
	lole_range.value = aa_value
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + as_column + string(al_row)
end try

return aa_value

end function

public function any uof_set (long al_row, long al_column, any aa_value);/**
 * Imposta il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.value = aa_value
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + string(al_column) + string(al_row)
end try

return aa_value
end function

public function string uof_nome_cella (long al_colonna);/**
 * ATTENZIONE
 *
 * FUNZIONE DEPRECATA
 *
 *  USARE UOF_GET_NAMED_COLUMN
 **/
 
 
string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if al_colonna > 26 then
	ll_resto = mod(al_colonna, 26)
	ll_i = long(al_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	al_colonna = ll_resto 
end if

if al_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[al_colonna]
end if	

return ls_char
end function

public function string uof_get_named_column (integer ai_column);/**
 * stefanop
 * 14/05/2012
 *
 * Dato un numero di colonna lo trasforma in lettere.
 * Ad esempio:
 * 		26 = Z
 *		27 = AA
 *
 * !! Attenzione !!
 * il limite attuale dell'algoritmo è ZZ
 **/
 
string ls_column
int li_j, li_k


li_k = 0
ls_column = ""
	
if ai_column > 26 then
	li_k = int(ai_column / 26)
	
	ls_column += string(char(64 + li_k))
end if
	
li_j = ai_column - (26 * li_k)
ls_column += string(char(64 + li_j))

return ls_column
	
	
end function

public function boolean uof_open (string as_file_path, boolean ab_ole_visible, boolean ab_read_only);/**
 * stefanop
 * 22/06/2011
 *
 * Apre il file excel
 *
 * as_file_path string indica il percorso del file da aprire
 * ab_ole_visible boolean indica se deve essere visibile l'OLE
 **/
 
long ll_errore, ll_count, ll_index

setnull(is_error)

// controllo esistenza file
if not fileexists(as_file_path) then
	setnull(as_file_path)
	is_error = "File non trovato: " + as_file_path
	return false
end if

//inizio
ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	is_error = "Si è verificato un errore durante la chiamata a MsExcel."
	return false
end if

is_path_file = as_file_path

// Se more at: http://msdn.microsoft.com/en-us/library/microsoft.office.interop.excel.workbooks.open(v=office.11).aspx
iole_excel.Application.Workbooks.Open(as_file_path, 2, ab_read_only)
iole_excel.application.visible = ab_ole_visible
ib_read_only = ab_read_only
ib_is_open = true
ib_is_new = false

return true
end function

public function boolean uof_set_sheet (string as_sheet_name);/**
 * Seleziona il foglio di lavoro per eventuali letture o scritture
 **/
  
long ll_index

if isnull(is_worksheet) or upperbound(is_worksheet) < 1 then
	uof_get_sheets(is_worksheet)
end if

if (isnull(as_sheet_name) or as_sheet_name = "") and upperbound(is_worksheet) > 0 then
	// Il parametro è vuoto, seleziono il primo disponibile
	il_current_sheet = 1
	
elseif not guo_functions.uof_in_array(as_sheet_name, is_worksheet, ll_index) then
	// il foglio non è presente nel file excel
	setnull(il_current_sheet)
	return false
	
else
	il_current_sheet = ll_index
end if

return true
end function

public function integer uof_get_num_column (string as_column);/**
 * stefanop & doneto
 * 24/05/2012
 *
 * Data una stringa che rappresenta la colonna ne  ricavo il numero
 **/
 
int li_a, li_b
 
if len(as_column) = 1 then
	return asc(as_column) - 64
elseif len(as_column) > 1 then
	
	li_a = asc(mid(as_column,1,1)) -64
	li_b = asc(mid(as_column,2,1)) -64
	
	return (li_a * 26) + li_b
end if

return -1
end function

public function string uof_read_string (long al_row, string as_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

string ls_return
any la_return

setnull(ls_return)

la_return = uof_read(al_row, as_column)

if not isnull(la_return) then ls_return = string(la_return)

return ls_return
end function

public function long uof_read_long (long al_row, string as_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

long ll_return
any la_return

setnull(ll_return)

la_return = uof_read(al_row, as_column)

if not isnull(la_return) then ll_return = long(la_return)

return ll_return
end function

public function decimal uof_read_decimal (long al_row, string as_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/





decimal ld_return
any la_return

setnull(ld_return)

la_return = uof_read(al_row, as_column)

if not isnull(la_return) then ld_return = long(la_return)

return ld_return
end function

public function boolean uof_add_sheet (string as_name, boolean ab_fire_select);/**
 * stefanop
 * 18/06/2012
 *
 * Aggiunge un sheet al foglio excel corrente
 * Viene aggiunto solo se non è già presente
 **/
 
 
// TODO Controllar eche non esista
long ll_count

ll_count = iole_excel.worksheets.count
ll_count ++

iole_excel.worksheets.Add
iole_excel.worksheets(1).name = as_name

if ab_fire_select then
	il_current_sheet = ll_count
end if

return true
end function

public function boolean uof_add_sheet (string as_name);/**
 * stefanop
 * 18/06/2012
 *
 * Aggiunge un sheet al foglio excel corrente
 * Viene aggiunto solo se non è già presente
 **/
 
return uof_add_sheet(as_name, true)
end function

public function boolean uof_clear_sheet (string as_sheet_name);/**
 * stefanop
 * 18/06/2012
 *
 * Cancella un determinato sheet
 **/
 
iole_excel.application.DisplayAlerts = false
iole_excel.worksheets(as_sheet_name).delete
iole_excel.application.DisplayAlerts = true
 
return true
end function

public function boolean uof_clear_sheet ();/**
 * stefanop
 * 18/06/2012
 *
 * Cancella un determinato sheet
 **/
 
string ls_sheet
int li_i, li_count

iole_excel.application.DisplayAlerts = false
li_count = iole_excel.worksheets.count
if li_count > 1 then
	
	for li_i = 2 to li_count
		//ls_sheet = iole_excel.worksheets(li_i).name
		iole_excel.worksheets(1).delete
	next
	
end if

iole_excel.application.DisplayAlerts = true
 
return true
end function

public function integer uof_saveas (string as_file_path);/**
 * stefanop
 * 18/06/2012
 *
 * Salva il file
 **/

iole_excel.Activeworkbook.SaveAs(as_file_path)

return 0
end function

public subroutine uof_close ();iole_excel.Activeworkbook.Close(is_path_file)
end subroutine

public subroutine uof_save ();/**
 * stefanop
 * 18/06/2012
 *
 * Salva il documento se necessario e possibile
 **/

if not ib_read_only then
	if ib_is_new then
		uof_saveas(is_path_file)
	else
		iole_excel.Activeworkbook.Save()
	end if
else
	iole_excel.ActiveWorkbook.Saved = True
end if
end subroutine

public function boolean uof_create (string as_file_path, boolean ab_ole_visibile);/** 
 * stefanop
 * 18/06/2012
 *
 * Creo il file excel fisico nel disco e apro l'istanza OLE
 **/
 
long ll_errore, ll_count, ll_index

setnull(is_error)

//inizio
ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	is_error = "Si è verificato un errore durante la chiamata a MsExcel."
	return false
end if

is_path_file = as_file_path

// Se more at: http://msdn.microsoft.com/en-us/library/microsoft.office.interop.excel.workbooks.open(v=office.11).aspx
//iole_excel.Application.Workbooks.Open(as_file_path, 2, ab_read_only)
iole_excel.application.visible = ab_ole_visibile
iole_excel.Workbooks.Add

uof_clear_sheet()

ib_read_only = false
ib_is_open = true
ib_is_new = true

return true
end function

public function string uof_get_current_sheet ();/**
 * stefanop
 * 17/05/2012
 *
 * Migliorata la gestione del recupero fogli di lavoro
 **/


if isnull(is_worksheet) or upperbound(is_worksheet) < 1 then
	
	// recupero il primo foglio
	uof_get_sheets(is_worksheet)
		
end if

return is_worksheet[il_current_sheet]
end function

private function string uof_range (long r, string c);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 
if r > 0 then
	return c + string(r)
elseif isnull(c) or c = "" then
	return string(r) + string(r)
else
	return c +  ":" + c
end if
end function

private function string uof_range (long r, long c);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 

 
if r > 0 then
	return uof_range(r, uof_get_named_column(c))
elseif c < 1 then
	return string(r) + string(r)
else
	return uof_range(uof_get_named_column(c))
end if
end function

public subroutine uof_set_autofit (long al_row, long al_column);/**
 * stefanop
 * 19/06/2012
 *
 * Indicata alla cella che può allargasi in base al contenuto
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.EntireColumn.AutoFit
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_autofit (long al_row, string as_column);/**
 * stefanop
 * 19/06/2012
 *
 * Indicata alla cella che può allargasi in base al contenuto
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, as_column)
	lole_range.EntireColumn.AutoFit
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + as_column + string(al_row)
end try
end subroutine

public subroutine uof_set_format (long al_row, long al_column, string as_format);/**
 * stefanop
 * 19/06/2012
 *
 * Imposta il formato di visualizzazione in una deterimanta cella
 **/
 
//string ls_current_sheet
//
//ls_current_sheet = uof_get_current_sheet()
//
//iole_excel.Application.ActiveWorkbook.Worksheets(ls_current_sheet).range(uof_range(al_row, al_column)).NumberFormat = as_format

try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.NumberFormat = as_format
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_format (long al_row, string as_column, string as_format);/**
 * stefanop
 * 19/06/2012
 *
 * Imposta il formato di visualizzazione in una deterimanta cella
General     General
Number      0
Currency    $#,##0.00;[Red]$#,##0.00
Accounting  _($* #,##0.00_);_($* (#,##0.00);_($* "-"??_);_(@_)
Date        m/d/yy
Time        [$-F400]h:mm:ss am/pm
Percentage  0.00%
Fraction    # ?/?
Scientific  0.00E+00
Text        @
Special     ;;
Custom      #,##0_);[Red](#,##0)
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, as_column)
	lole_range.NumberFormat = as_format
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del valore della cella " + string(as_column) + string(al_row)
end try
end subroutine

private function string uof_range (long r_from, string c_from, long r_to, string c_to);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 
 
return c_from + string(r_from) +  ":" + c_to + string(r_to)
end function

private function string uof_range (long r, any c);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 
choose case classname(c)
		
	case "integer", "long"
		if r < 1 then
			return uof_range(uof_get_named_column(c))
		elseif long(c) < 1 then
			return string(r) + ":" + string(r)
		else
			return uof_range(r, uof_get_named_column(c))
		end if
		
	case "string"
		if r < 1 then
			return uof_range(string(c))
		else
			return uof_range(r, string(c))
		end if
		
end choose
 

end function

private function string uof_range (long r_from, any c_from, long r_to, any c_to);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 
string ls_range
 
choose case classname(c_from)
	case "integer", "long"
		ls_range = uof_get_named_column(long(c_from)) + string(r_from)
		
	case "string"
		ls_range = string(c_from) + string(r_from)
		
end choose

choose case classname(c_to)
	case "integer", "long"
		ls_range += ":" + uof_get_named_column(long(c_to)) + string(r_to)
		
	case "string"
		ls_range += ":" + string(c_to) + string(r_to)
end choose
 
 
return ls_range
end function

private function oleobject uof_get_range (long r, any c);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna l'OLE del range passato
 **/

return iole_excel.Application.ActiveWorkbook.Worksheets(uof_get_current_sheet()).range(uof_range(r, c))
end function

private function string uof_range (string c);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna la stirnga formatta per il range; ad esempio 1A:1A che indica la prima cella o 3B:3B
 **/
 
 
return c +  ":" + c
end function

public subroutine uof_merge (long al_row_from, any aa_coloumn_from, long al_row_to, any aa_column_to);/**
 * stefanop
 * 19/06/2012
 *
 * Unisce le celle impostate
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row_from, aa_coloumn_from, al_row_to, aa_column_to)
	lole_range.merge
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore funzione merge"
end try
end subroutine

public subroutine uof_set_horizontal_align (long al_row, any aa_column, integer ai_text_align);/**
 * stefanop
 * 19/06/2012
 *
 * Imposta il formato di visualizzazione in una deterimanta cella
 
VALORI PER ai_text_align
1=xlCenter
2=xlDistributed
3=xlJustify
4=xlLeft
5=xlRight
 **/

 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, aa_column)
	lole_range.HorizontalAlignment = ai_text_align
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante l'impostazione di allineamento testo nella cella " + string(aa_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_vertical_align (long al_row, any aa_column, integer ai_text_align);/**
 * stefanop
 * 19/06/2012
 *
 * Imposta il formato di visualizzazione in una deterimanta cella
BASSO		  		-4107	Bottom
CENTRATO 			-4108	Center
DISTRIBUITO		-4117	Distributed
GIUSTIFICATO		-4130	Justify
TOP					-4160	Top

 **/
 
 
 
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, aa_column)
	lole_range.VerticalAlignment = ai_text_align
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante l'impostazione di allineamento testo nella cella " + string(aa_column) + string(al_row)
end try
end subroutine

public subroutine uof_inser_image (long al_row, any aa_column, string as_file_path);/**
 * stefanop
 * 19/06/2012
 *
 * Inserisco immagine all'interno del foglio excel
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, aa_column)
	lole_range.Select
	
	iole_excel.Application.ActiveSheet.Pictures.Insert(as_file_path).Select
	
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante l'impostazione di allineamento testo nella cella " + string(aa_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_bold (long al_row, long al_column);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.font.bold = true
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio bold della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_bold (long al_row, string as_column);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, as_column)
	lole_range.font.bold = true
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del bold della cella " + string(as_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_italic (long al_row, long al_column);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.font.italic = true
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio corsivo della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_italic (long al_row, string as_column);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, as_column)
	lole_range.font.italic = true
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del corsivo della cella " + string(as_column) + string(al_row)
end try
end subroutine

public subroutine uof_saveas (string as_file_path, integer ai_format);

choose case ai_format
	case 2007, 2010
		ai_format = 50		//valore da passare in caso di MS Excel 2007 o 2010
	case else
		ai_format = 56		//valore da passare in caso di MS Excel 2003
		
end choose

iole_excel.Activeworkbook.SaveAs( as_file_path, ai_format )


end subroutine

public function date uof_read_date (long al_row, string as_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

date ld_return
any la_return

setnull(ld_return)

la_return = uof_read(al_row, as_column)

if not isnull(la_return) then ld_return = date(la_return)

return ld_return
end function

public function date uof_read_date (long al_row, long al_column);/**
 * Legge il valore di una determinata cella (numero riga - numero colonna).
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

return uof_read_date(al_row, uof_nome_cella(al_column))

end function

public function decimal uof_read_decimal (long al_row, long al_column);
/**
 * Legge il valore di una determinata cella (numero riga - numero colonna).
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

return uof_read_decimal(al_row, uof_nome_cella(al_column))
end function

public function long uof_read_long (long al_row, long al_column);
/**
 * Legge il valore di una determinata cella (numero riga - numero colonna).
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

return uof_read_long(al_row, uof_nome_cella(al_column))
end function

public function string uof_read_string (long al_row, long al_column);
/**
 * Legge il valore di una determinata cella (numero riga - numero colonna).
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/

return uof_read_string(al_row, uof_nome_cella(al_column))
end function

public function integer uof_get_used_range (ref oleobject aole_range);/**
 * stefanop
 * 16/07/2014
 *
 * Ottiene la selezione delle celle usate
 **/
 
OLEObject luo_usedrange

try
	aole_range =  iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).UsedRange
catch (runtimeerror ex)
	is_error = "Errore durante la lettura del range usato"
end try
 
return 1
end function

public function boolean uof_set_sheet (unsignedinteger ai_sheet);/**
 * Seleziona il foglio di lavoro per eventuali letture o scritture
 **/
  
if isnull(is_worksheet) or upperbound(is_worksheet) < 1 then
	uof_get_sheets(is_worksheet)
end if

if ai_sheet > upperbound(is_worksheet) then
	is_error = "Il foglio numero " + string(ai_sheet) +" non sembra esistere nel file excel"
	return false
else
	il_current_sheet = ai_sheet
end if

return true
end function

public function any uof_read (long al_row, string as_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/
 
any la_value

setnull(is_error)
 
// controllo che sia impostato un foglio, altrimenti seleziono il primo disponibile
if isnull(il_current_sheet) or il_current_sheet < 1 then
	il_current_sheet = 1
end if

try
	la_value =  iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(as_column + string(al_row) + ":" + as_column + string(al_row)). value
catch (runtimeerror ex)
	is_error = "Errore durante la lettura del valore della cella " + as_column + string(al_row)
	setnull(la_value)
end try
 
return la_value
end function

public function integer uof_insert_row (long al_row, integer ai_insert_type);string ls_range

ls_range = uof_range(string(al_row))

if ai_insert_type <> INSERT_DOWN and ai_insert_type <> INSERT_UP then
	ai_insert_type = INSERT_DOWN
end if

iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(ls_range).Insert(ai_insert_type)

return 1
end function

public function integer uof_insert_column (string as_column, integer ai_insert_type);string ls_range

ls_range = uof_range(as_column)

if ai_insert_type <> INSERT_RIGHT and ai_insert_type <> INSERT_LEFT then
	ai_insert_type = INSERT_RIGHT
end if

iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(ls_range).Insert(ai_insert_type)

return 1
end function

public function integer uof_insert_column (integer ai_column, integer ai_insert_type);string ls_range

ls_range = uof_range(uof_nome_cella(ai_column))

if ai_insert_type <> INSERT_RIGHT and ai_insert_type <> INSERT_LEFT then
	ai_insert_type = INSERT_RIGHT
end if

iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(ls_range).Insert(ai_insert_type)

return 1
end function

public subroutine uof_set_border (integer ai_row, integer ai_col, integer ai_border_top, integer ai_border_right, integer ai_border_bottom, integer ai_border_left);/***
 *
 **/
 
 try
	oleobject lole_range
	lole_range = uof_get_range(ai_row, ai_col)
	lole_range.Borders(1).LineStyle = 1
	lole_range.Borders(1).Weight = ai_border_left
	
	lole_range.Borders(2).LineStyle = 1
	lole_range.Borders(2).Weight = ai_border_right
	
	lole_range.Borders(3).LineStyle = 1
	lole_range.Borders(3).Weight = ai_border_top
	
	lole_range.Borders(4).LineStyle = 1
	lole_range.Borders(4).Weight = ai_border_bottom
	
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del bordo della cella " + string(ai_row) + string(ai_col)
end try
end subroutine

public subroutine uof_set_background (integer ai_row, integer ai_col, integer ai_color);/***
 *
 **/
 
 try
	oleobject lole_range
	lole_range = uof_get_range(ai_row, ai_col)
	lole_range.Interior.ColorIndex = ai_color
	
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del bordo della cella " + string(ai_row) + string(ai_col)
end try
end subroutine

public subroutine uof_set_background_rgb (integer ai_row, integer ai_col, integer ai_color);/***
 *
 **/
 
 try
	oleobject lole_range
	lole_range = uof_get_range(ai_row, ai_col)
	lole_range.Interior.Color = ai_color
	
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio del bordo della cella " + string(ai_row) + string(ai_col)
end try
end subroutine

public subroutine uof_set_wrap_text (long al_row, long al_column);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.wraptext = true
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio bold della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_font_size (long al_row, long al_column, long al_fontsize);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.font.size = al_fontsize
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio bold della cella " + string(al_column) + string(al_row)
end try

/*
try
	oleobject lole_range
	lole_range = uof_get_range(al_row_from, al_column_from,al_row_to, al_column_to)
	lole_range.font.size = al_fontsize
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio bold della cella " + string(al_column_from) + string(al_row_from)
end try
*/
end subroutine

public subroutine uof_set_font_name (long al_row, long al_column, string as_fontname);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.font.name = as_fontname
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio bold della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_border (integer ai_row_from, integer ai_col_from, integer ai_row_to, integer ai_col_to, integer ai_border_top, integer ai_border_right, integer ai_border_bottom, integer ai_border_left);/***
 *
 **/
 
 try
	oleobject lole_range
	lole_range = uof_get_range(ai_row_from, ai_col_from, ai_row_to, ai_col_to)
	lole_range.Borders(1).LineStyle = 1
	lole_range.Borders(1).Weight = ai_border_left
	
	lole_range.Borders(2).LineStyle = 1
	lole_range.Borders(2).Weight = ai_border_right
	
	lole_range.Borders(3).LineStyle = 1
	lole_range.Borders(3).Weight = ai_border_top
	
	lole_range.Borders(4).LineStyle = 1
	lole_range.Borders(4).Weight = ai_border_bottom
	
	destroy lole_range
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante il settaggio del bordo del range $1:$2 - $3:$4",ai_row_from, ai_col_from, ai_row_to, ai_col_to)
end try
end subroutine

public subroutine uof_set_horizontal_align (long al_row_from, any aa_column_from, integer al_row_to, integer aa_column_to, integer ai_text_align);/**
 * EnMe
 * 13/03/2018
 *
 * Imposta il formato di visualizzazione in un range di celle
 
VALORI PER ai_text_align
1=xlCenter
2=xlDistributed
3=xlJustify
4=xlLeft
5=xlRight
 **/
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row_from, aa_column_from,al_row_to, aa_column_to)
	lole_range.HorizontalAlignment = ai_text_align
	destroy lole_range
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante l'impostazione di allineamento testo nel range $1:42 - $3:$4 ", aa_column_from ,al_row_from, aa_column_to ,al_row_to)
end try
end subroutine

public function any uof_read (long al_row, long al_column);/**
 * Legge il valore di una determinata cella.
 * Prima di usare questa funziona ricordarsi di chiamare uof_set_sheet per indicare da quale foglio leggere
 **/
 
any la_value

setnull(is_error)
 
// controllo che sia impostato un foglio, altrimenti seleziono il primo disponibile
if isnull(il_current_sheet) or il_current_sheet < 1 then
	il_current_sheet = 1
end if

try
	la_value =  iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).cells(al_row, al_column). value
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante la lettura del valore della riga $1 colonna $1", al_row, al_column)
	setnull(la_value)
end try
 
return la_value
end function

private function oleobject uof_get_range (long r_from, any c_from, long r_to, any c_to);/**
 * stefanop
 * 19/06/2012
 *
 * Ritorna l'OLE del range passato
 **/

return iole_excel.Application.ActiveWorkbook.Worksheets(uof_get_current_sheet()).range(uof_range(r_from, c_from, r_to, c_to))
end function

public function integer uof_disable_warning_messages ();iole_excel.Application.DisplayAlerts = false
return 0
end function

public subroutine uof_set_rows_height (integer ai_row_from, integer ai_row_to, integer ai_height);/***
 *
 **/
 long ll_i
 try
	
	for ll_i = ai_row_from to ai_row_to
		iole_excel.Application.ActiveSheet.Rows(ll_i).RowHeight = ai_height
	next
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante il settaggio altezza dalla riga $1 alla riga ",ai_row_from, ai_row_to)
end try

return
end subroutine

public function integer uof_insert_copied_rows (long al_row, integer ai_insert_type, long r_from, string c_from, long r_to, string c_to);string ls_range

ls_range = uof_range(string(al_row))

if ai_insert_type <> INSERT_DOWN and ai_insert_type <> INSERT_UP then
	ai_insert_type = INSERT_DOWN
end if


iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(uof_range( r_from,c_from, r_to, c_to)).copy()

iole_excel.Application.ActiveWorkbook.Worksheets(il_current_sheet).range(ls_range).Insert(ai_insert_type)

return 1
end function

public subroutine uof_set_autofit (long ai_row_from, long ai_col_from, long ai_row_to, long ai_col_to);/**
 * stefanop
 * 19/06/2012
 *
 * Indicata alla cella che può allargasi in base al contenuto
 **/
 
 try
	oleobject lole_range
	lole_range = uof_get_range(ai_row_from, ai_col_from, ai_row_to, ai_col_to)
	lole_range.EntireColumn.AutoFit
	destroy lole_range
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante il settaggio del bordo del range $1:$2 - $3:$4",ai_row_from, ai_col_from, ai_row_to, ai_col_to)
end try

end subroutine

public subroutine uof_set_font_color (long al_row, long al_column, long ai_colorindex);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row, al_column)
	lole_range.font.colorindex = ai_colorindex
	destroy lole_range
catch (runtimeerror ex)
	is_error = "Errore durante il settaggio color della cella " + string(al_column) + string(al_row)
end try
end subroutine

public subroutine uof_set_font_size (long al_row_from, long al_column_from, integer al_row_to, integer al_column_to, long al_fontsize);
try
	oleobject lole_range
	lole_range = uof_get_range(al_row_from, al_column_from,al_row_to, al_column_to)
	lole_range.font.size = al_fontsize
	destroy lole_range
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante l'impostazione della dimensione testo nel range $1:42 - $3:$4 ", al_column_from ,al_row_from, al_column_to ,al_row_to)
end try

end subroutine

public subroutine uof_set_vertical_align (long al_row_from, any aa_column_from, long al_row_to, any aa_column_to, integer ai_text_align);/**
 * stefanop
 * 19/06/2012
 *
 * Imposta il formato di visualizzazione in una deterimanta cella
BASSO		  		-4107	Bottom
CENTRATO 			-4108	Center
DISTRIBUITO		-4117	Distributed
GIUSTIFICATO		-4130	Justify
TOP					-4160	Top

 **/
 
 
 
 
try
	oleobject lole_range
	lole_range = uof_get_range(al_row_from, aa_column_from,al_row_to, aa_column_to)
	lole_range.VerticalAlignment = ai_text_align
	destroy lole_range
catch (runtimeerror ex)
	is_error = g_str.format("Errore durante l'impostazione di allineamento testo nel range $1:$2 - $3:$4 ", aa_column_from ,al_row_from, aa_column_to ,al_row_to)
end try
end subroutine

on uo_excel.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_excel.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iole_excel = CREATE OLEObject
end event

event destructor;
if ib_is_open and isvalid(iole_excel) and not isnull(iole_excel) then
	
	uof_save()
	
	uof_close()
	
	iole_excel.Application.Quit()
	
end if

destroy iole_excel;
end event

