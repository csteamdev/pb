﻿$PBExportHeader$uo_formule_calcolo.sru
forward
global type uo_formule_calcolo from nonvisualobject
end type
end forward

global type uo_formule_calcolo from nonvisualobject
end type
global uo_formule_calcolo uo_formule_calcolo

type variables
constant string FORMULA_OK = "FORMULA_@K"
constant string ERRORE_FORMULA = "ERROREFORMUL@"
constant string PATTERN_VARIABLES = "([^0-9;=><\[\]/+,-/*/(/)//^]\w{0,})"				//"([^0-9+,-/*/(/)//^]\w+)"


private:
	boolean ib_ferma_calcolo = false
	string is_formule[], is_valori[], is_tipo[], is_vuoto[], is_cache_cod_variabile[], is_cache_val_variabile[]
	dec{4} id_costanti[], id_vuoto[], idc_internal_result
	
	boolean ib_lista_variabili = false
	string is_lista_variabili[]
	
	

public:
	uo_array		iuo_cache
	integer		ii_anno_reg_ord_ven = 0
	long			il_num_reg_ord_ven = 0
	long			il_prog_riga_ord_ven = 0
	string			is_nome_tabella = ""
	boolean		ib_gibus = false
	
	string			is_cod_prodotto_padre = ""
	string			is_cod_prodotto_figlio = ""
	string			is_cod_versione_padre = ""
	string			is_cod_versione_figlio = ""
	long			il_num_sequenza=0
	
	integer		ii_anno_commessa = 0
	long			il_num_commessa = 0
	
	string			is_tipo_ritorno="1"		//"1" numerico    "2"  stringa
	string			is_contesto = ""
	datastore ids_internal_store
	boolean		ib_log = false

end variables

forward prototypes
public function integer uof_calcola_formula (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore)
public function string uof_variabili (string fs_formula, string fs_cod_formula, ref string fs_errore)
public function integer uof_if_condition (ref string fs_formula, ref string fs_errore)
public function integer uof_lista_variabili (string as_cod_formula, ref string as_lista_variabili[], ref string as_errore)
public function string uof_estrapola (string fs_formula, ref string fs_errore)
public function string uof_formula (string fs_formula, ref string fs_errore)
private function string uof_remove_quote (string as_valore)
public function string uof_get_parametro_funzione (string as_parametro)
public function integer uof_calcola_formula_testo (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public function string uof_variabili_cumulativo (string fs_formula, string fs_formula_2, string fs_cod_formula, decimal fd_risultato, ref string fs_errore)
public subroutine uof_pre_calcolo_formula_db (ref string fs_testo_formula)
public function long uof_estrai_stringhe_espress_reg (string fs_testo_formula, ref string fs_variabili[], ref string fs_errore)
public function integer uof_setvariables_formule_db (string fs_cod_formula, string fs_variabili[], boolean fb_test_mode, ref string fs_testo_formula, ref string fs_testo_formula_2, ref string fs_errore)
public function integer uof_eval_with_datastore (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_setvariables_auto_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore)
public subroutine uof_setvariables_dtaglio (ref string fs_testo_formula, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita)
public function integer uof_elabora_select (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_eval_with_datastore (string fs_testo_formula, ref string fs_risultato, ref string fs_errore)
public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_risultato, ref string fs_errore)
public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref string fs_risultato, ref string fs_errore)
public function integer uof_set_variabile (string fs_cod_variabile, ref string fs_testo_formula, ref string fs_errore)
public function integer uof_check_variabile (string fs_cod_variabile_originale, ref string fs_cod_variabile)
public function integer uof_setvariables_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore)
public function integer uof_get_tabella_variabile (string as_cod_formula, ref string as_tabella_variabile, ref string as_errore)
public subroutine uof_crea_ds_variabili_vuoto (ref datastore ads_variabili)
public function integer uof_calcola_formula_veloce (string as_cod_formula, string as_formula, string as_tipo_ritorno, ref datastore ads_variabili, ref decimal ad_valore, ref string as_valore, ref string as_errore)
public function string uof_controlla_ds_variabili (string as_cod_variabile, datastore ads_variabili, ref string as_valore, ref decimal ad_valore)
public function integer uof_calcola_variabile (string as_tabella_variabile, string as_tipo_ritorno, string as_cod_variabile, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore)
public function integer uof_estrai_varibili_formula (string as_cod_formula, ref string as_variabili[], ref string as_error)
public function integer uof_allinea_variabili_formula (string as_cod_formula, ref string as_error)
public function string uof_get_tipo_variabile (string as_cod_tipo_variabile)
public function boolean uof_is_formula_datastore (string as_cod_formula)
public function string uof_calcola_formula_datastore (string as_cod_formula, ref datastore ads_store, ref string as_error)
public function string uof_calcola_formula_datastore_sql (string as_cod_formula, ref string as_error)
public function string uof_esegui_funzione (string as_funzione, ref string as_errore)
public function integer uof_calcola_formula_dtaglio_old (string fs_testo_formula, boolean fb_test, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita, ref decimal fdc_risultato, ref string fs_errore)
public function integer uof_calcola_formula_dtaglio (string fs_testo_formula, boolean fb_test, long al_anno_ordine, long al_num_ordine, long al_prog_riga_ord_ven, ref decimal ad_risultato, ref string fs_errore)
end prototypes

public function integer uof_calcola_formula (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore);string ls_risultato
datastore lds_data
long ll_tot

if ib_ferma_calcolo then return 1

ls_risultato = uof_formula(fs_espressione, fs_errore)

if ls_risultato = ERRORE_FORMULA then
	ib_ferma_calcolo = true
	return -1
	
elseif ls_risultato = FORMULA_OK then
	fdc_risultato = idc_internal_result
	return 1
end if

guo_functions.uof_replace_string(ls_risultato, ",", ".")


if uof_elabora_select(ls_risultato,fs_select, fdc_risultato, fs_errore)<0 then
	//in fs_errore il messaggio
	ib_ferma_calcolo = true
	
	return -1
else
	//in fdc_risultato/ il risultato
	return 1
end if

return 1
end function

public function string uof_variabili (string fs_formula, string fs_cod_formula, ref string fs_errore);string ls_return, ls_cod_variabile, ls_start_variabile, ls_end_variabile, ls_risultato, ls_tmp
long ll_pos, ll_pos2
date ldt_data
s_cs_xx_parametri lstr_parametri

if ib_ferma_calcolo then return ""

ls_start_variabile = "#"
ls_end_variabile = "#"

do while true
	
	//cerca sempre dall'inizio
	ll_pos = pos(fs_formula, ls_start_variabile, 1)
	
	if isnull(ll_pos) or ll_pos < 1 then
		exit
	else
		ll_pos2 = pos(fs_formula, ls_end_variabile, ll_pos + 1)
	end if
	
	if ll_pos2 < 1 then
		//carattere fine variabile non trovato
		fs_errore = "Carattere "+ls_end_variabile+" fine variabile non trovato in "+fs_formula
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
	end if
	
	//tra ll_pos e ll_pos2 c'è il codice variabile
	ls_cod_variabile = mid(fs_formula, ll_pos, ll_pos2 - ll_pos + 1)
	
	if not ib_lista_variabili then
		//GESTIONE CACHE DELLE VARIABILI -----------------------------------------------------------------------------------------
		//verifico se per caso questa variabile l'ho già incontrata, in tal caso restituisco subito il valore precedente
		if not iuo_cache.haskey(ls_cod_variabile) then
			//la variabile non era in cache, quindi chiedila e poi inseriscila in cache...
																													//es. se #S:F:FORNITORE# o #N:ANNO#
			lstr_parametri.parametro_s_1_a[1] = fs_cod_formula									//codice formula
			lstr_parametri.parametro_s_1_a[2] = ls_cod_variabile									//codice variabile -> #S:F:FORNITORE# o #N:ANNO#
			lstr_parametri.parametro_s_1_a[3] = upper(mid(ls_cod_variabile, 2, 2))			//identificatore di tipo variabile -> S: o N:
			lstr_parametri.parametro_s_1_a[4] = upper(mid(ls_cod_variabile, 4, 2))			//identificatore (eventuale) di selezione campo -> F: o AN(in tal caso viene ignorato)
			lstr_parametri.parametro_b_1 = ib_gibus
			
			openwithparm(w_compila_variabili, lstr_parametri)
			ls_risultato = message.stringparm
			
			if ls_risultato = "ANNULLACALCOLOTEST" then
				//stoppa tutto
				fs_errore = "Calcolo annullato dall'utente!"
				ib_ferma_calcolo = true
				return ERRORE_FORMULA
			end if
			
			//memorizza in cache la variabile appena letta
			iuo_cache.set(ls_cod_variabile, ls_risultato)
		else
			//variabile in cache, in ls_risultato il valore letto
			ls_risultato = iuo_cache.get(ls_cod_variabile)
		end if
	else
		
		if not guo_functions.uof_in_array(ls_cod_variabile, is_lista_variabili) then
			is_lista_variabili[upperbound(is_lista_variabili) + 1 ] = ls_cod_variabile
		end if
		
	end if
	
	if isnull(ls_risultato) then
		ls_risultato = "<NULL>"
	end if
	
	fs_formula = replace(fs_formula, ll_pos, len(ls_cod_variabile), ls_risultato)
loop


return fs_formula
end function

public function integer uof_if_condition (ref string fs_formula, ref string fs_errore);long			ll_inizio_condizione, ll_inizio_vero, ll_inizio_falso,ll_fine_falso, ll_pos, ll_inizio_operatore, ll_lunghezza_condizione, ll_inizio, ll_fine
integer		li_lunghezza_operatore, li_ret
string			ls_car_inizio_if, ls_car_fine_if, ls_sep, ls_condizione, ls_formula_vero, ls_formula_falso, ls_operatore, ls_primo_membro, ls_secondo_membro, ls_soluzione, ls_select
decimal		ld_primo_membro, ld_secondo_membro
boolean		lb_confronto_numerico = true

//es:  IF{condizione;formula_se_vero;formula_se_falso}
ls_car_inizio_if	="{" 
ls_car_fine_if	= "}"
ls_sep			= ";"

ll_inizio_condizione = pos(upper(fs_formula),"IF")

if ll_inizio_condizione = 0 then return 0		//condizione IF non presente

ll_inizio = ll_inizio_condizione

//posizione prima occorenza di }
ll_fine = pos(fs_formula, ls_car_fine_if, ll_inizio)

//posizione dell'inizio della espressione se vero (comincio a cercare dopo IF{...)
ll_inizio_condizione = ll_inizio_condizione + 3
ll_inizio_vero = pos(fs_formula, ls_sep, ll_inizio_condizione )

if ll_inizio_vero>0 then
else
	fs_errore = "Carattere apertura IF "+ls_car_inizio_if+ " non trovato nella formula!"
	return -1
end if


//espressione della condizione IF da verificare: faccio mid da inizio condizione a inizio vero
ls_condizione = mid(fs_formula, ll_inizio_condizione, ll_inizio_vero - ll_inizio_condizione)

if len(ls_condizione)>0 then
else
	fs_errore = "Condizione in IF non trovata nella formula!"
	return -1
end if

//posizione dell'inizio della espressione se falso (comincio a cercare dopo la pos. dell'inizio del vero)
ll_inizio_falso = pos(fs_formula, ls_sep, ll_inizio_vero + 1)

if ll_inizio_falso>0 then
else
	fs_errore = "Carattere  IF "+ls_car_inizio_if+ " non trovato nella formula!"
	return -1
end if

//posizione della fine della espressione se falso (cerco dopo l'inizio del falso)
ll_fine_falso = pos(fs_formula, ls_car_fine_if, ll_inizio_falso + 1)

//formula in caso IF verificata
ls_formula_vero = mid(fs_formula, ll_inizio_vero + 1, ll_inizio_falso - (ll_inizio_vero + 1))

//elimino eventuali spazi all'inizio e alla fine
ls_formula_vero = trim(ls_formula_vero)

//formula in caso IF non verificata
ls_formula_falso = mid(fs_formula, ll_inizio_falso + 1, ll_fine_falso - (ll_inizio_falso + 1 ))

//elimino eventuali spazi all'inizio e alla fine
ls_formula_falso = trim(ls_formula_falso)

//------------------------------------------------------------------------
do while 1=1
	ll_pos = pos( ls_condizione, " " , 1)
	if ll_pos = 0 then exit
	ls_condizione = replace( ls_condizione, ll_pos, 1, "")
loop

//cerco l'operatore =
ll_inizio_operatore = pos(ls_condizione,"=",1)

if ll_inizio_operatore>0 then
	//se ho trovato = verifico se c'è '>=' oppure '<=' oppure '<>'
	if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1) > 0 then
		//rimetto a 0 la posizione dell'operatore, cercherò in seguito >=, <= oppure <>
		ll_inizio_operatore = 0
	else
		li_lunghezza_operatore = 1
		ls_operatore = "="
	end if
end if

if ll_inizio_operatore = 0 then
	//se non ho trovato niente prima cerco '>'
	ll_inizio_operatore = pos(ls_condizione,">",1)
	if ll_inizio_operatore>0 then
		if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1)> 0 then
			ll_inizio_operatore = 0
		else
			li_lunghezza_operatore = 1
			ls_operatore = ">"
		end if
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<",1)
	if ll_inizio_operatore>0 then
		if pos(ls_condizione,">=",1) > 0 or pos(ls_condizione,"<=",1) > 0 or pos(ls_condizione,"<>",1) > 0 then
			ll_inizio_operatore = 0
		else
			li_lunghezza_operatore = 1
			ls_operatore = "<"
		end if
	end if
end if
	
if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,">=",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = ">="
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<=",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = "<="
	end if
end if

if ll_inizio_operatore = 0 then
	ll_inizio_operatore = pos(ls_condizione,"<>",1)
	if ll_inizio_operatore>0 then
		li_lunghezza_operatore = 2
		ls_operatore = "<>"
	end if
end if

if pos(ls_condizione,"=>",1) > 0 or pos(ls_condizione,"=<",1) > 0 or pos(ls_condizione,">>",1)> 0 or pos(ls_condizione,"<<",1)> 0 or pos(ls_condizione,"><",1)> 0 then
	ll_inizio_operatore = 0
end if


//--------------------------------------
if ll_inizio_operatore>0 then	
	ls_primo_membro = mid(ls_condizione,1,ll_inizio_operatore -1)
	ll_lunghezza_condizione = len(ls_condizione)
	ls_secondo_membro = mid(ls_condizione,ll_inizio_operatore+li_lunghezza_operatore,ll_lunghezza_condizione - ll_inizio_operatore)
	
	//****************         							    ATTENZIONE!!!!!         ******************************
	//***************
	//**************     SI E' SCELTO DI FARE UN CONFRONTO TRA VARIABILI NUMERICHE NEI CASI
	//**************     DI >,<,>=,<= MENTRE SI FA UN CONFRONTO TRA STRINGHE NEI CASI 
	//**************     DI = E <> (UGUALE E DIVERSO)
	
	//in primo menbro e/o secondo membro potrebbero esserci espressioni (es. altre formule es: [XXX]+5 o variabili #N:ANNO#-25 ) che devono prima essere risolte/calcolate
	//
	
	//-------------
	//vedo se ho formule nel 1° membro
	ls_primo_membro = uof_estrapola(ls_primo_membro, fs_errore)
	
//	//se ci sono variabili sostituisco
//	ls_primo_membro = uof_variabili(ls_primo_membro, "1°membro IF", fs_errore)
//	if ls_primo_membro="ERROREFORMUL@" then
//		ib_ferma_calcolo = true
//		//return "ERROREFORMUL@"
//		fs_errore = "Errore in primo membro condizione IF"
//		return -1
//	end if
	
	//adesso valuta la espressione numerica del 1° membro se l'operatore implica il confronto tra numeri
	if uof_elabora_select(ls_primo_membro, ls_select, ld_primo_membro, fs_errore)<0 then
		//in fs_errore il messaggio
		//return -1
		
		//disattiva il confronto tra numeri
		lb_confronto_numerico = false
	else
		//in ld_primo_membro il risultato
	end if
	
	
	//-------------
	//vedo se ho formule nel 2° membro
	uof_estrapola(ls_secondo_membro, fs_errore)
	
//	//se ci sono variabili sostituisco
//	ls_secondo_membro = uof_variabili(ls_secondo_membro, "2°membro IF", fs_errore)
//	if ls_secondo_membro="ERROREFORMUL@" then
//		ib_ferma_calcolo = true
//		//return "ERROREFORMUL@"
//		fs_errore = "Errore in secondo membro condizione IF"
//		return -1
//	end if
	
	if lb_confronto_numerico then
		if uof_elabora_select(ls_secondo_membro, ls_select, ld_secondo_membro, fs_errore)<0 then
			//in fs_errore il messaggio
			//return -1
			
			//disattiva il confronto tra numeri
			lb_confronto_numerico = false
		else
			//in ld_primo_membro il risultato
		end if
	end if

	
	//ld_primo_membro = dec(ls_primo_membro)	          
	//ld_secondo_membro = dec(ls_secondo_membro)	    
	
	choose case ls_operatore
		case "="
			if lb_confronto_numerico then
				
				if ld_primo_membro = ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro = ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case ">"
			if lb_confronto_numerico then
				
				if ld_primo_membro > ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro > ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<"
			if lb_confronto_numerico then
				
				if ld_primo_membro < ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro < ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case ">="
			if lb_confronto_numerico then
				
				if ld_primo_membro >= ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro >= ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<="
			if lb_confronto_numerico then
				
				if ld_primo_membro <= ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro <= ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			end if
			
		case "<>"
			if lb_confronto_numerico then
				
				if ld_primo_membro <> ld_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
				
			else
				
				if ls_primo_membro <> ls_secondo_membro then
					ls_soluzione = ls_formula_vero
				else
					ls_soluzione = ls_formula_falso		
				end if
			end if
			
	end choose
end if

if ll_inizio_operatore = 0 then
	fs_errore = "Errore: nella condizione manca l'operatore di condizione (>,<,>=,<=,=) oppure si sono introdotti dei simboli errati (=<,=>)!"
	return -1
end if

//sostituisci la espressione dell'IF con la espressione risolta (soluzione)
fs_formula = mid(fs_formula, 1, ll_inizio - 1) + "(" + ls_soluzione + ")" + mid(fs_formula,ll_fine+1, len(fs_formula))

//se c'è più di un IF
//(es. IF{condizione1;vero;falso} + IF{cond_new;exp1; exp2}  )
if pos(upper(fs_formula),"IF") > 0 then
	//se nella condizione è previsto un altro IF re-itera
	li_ret = uof_if_condition(fs_formula, fs_errore)
	if li_ret < 0 then
		
		return -1
	end if
end if

//fs_formula = ls_soluzione

return 1
end function

public function integer uof_lista_variabili (string as_cod_formula, ref string as_lista_variabili[], ref string as_errore);/**
 * stefanop
 * 15/05/2012
 *
 * Estrae l'aray delle variabili che sono necessarie per la formula passata come parametro
 *
 **/
 
string ls_empty[]

is_lista_variabili = ls_empty
as_lista_variabili = ls_empty

ib_lista_variabili = true

uof_formula(as_cod_formula, as_errore)

ib_lista_variabili = false

if not isnull(as_errore) and as_errore <> "" then
	return -1
end if

as_lista_variabili = is_lista_variabili
is_lista_variabili = ls_empty

return 1
end function

public function string uof_estrapola (string fs_formula, ref string fs_errore);string ls_return, ls_cod_formula, ls_start_formula, ls_end_formula, ls_risultato
long ll_pos, ll_pos2

if ib_ferma_calcolo then return ""

ls_start_formula = "["
ls_end_formula = "]"

do while true
	//cerca sempre dall'inizio
	ll_pos = pos(fs_formula, ls_start_formula, 1)
	
	if ll_pos < 1 then
		exit
	else
		ll_pos2 = pos(fs_formula, ls_end_formula, ll_pos)
	end if
	
	if ll_pos < 1 then
		//carattere ] fine codice formula non trovato
		fs_errore = "Carattere "+ls_end_formula+" fine codice formula non trovato in "+fs_formula
		ib_ferma_calcolo = true
		return ""
	else
		ll_pos2 = pos(fs_formula, ls_end_formula, ll_pos)
	end if
	
	//tra ll_pos e ll_pos2 c'è il codice formula
	ls_cod_formula = mid(fs_formula, ll_pos, ll_pos2 - ll_pos + 1)
	
	ls_risultato = uof_formula(ls_cod_formula, fs_errore)
	
	if ib_ferma_calcolo then return "ERROREFORMUL@"
	
	//metto al posto di ls_cod_formula ls_risultato, nella stessa posizione di cod_formula
	
	fs_formula = replace(fs_formula, ll_pos, len(ls_cod_formula), ls_risultato)
loop


return fs_formula
end function

public function string uof_formula (string fs_formula, ref string fs_errore);string ls_return, ls_flag_tipo, ls_sql_statement, ls_tabella, ls_colonna, ls_composta, ls_format, ls_funzione,ls_column_type
dec{4} ld_numero
datastore lds_data
long ll_ret

if ib_ferma_calcolo then return ""

ls_format = "###########0.0000"

select flag_tipo, numero, sql, tabella, colonna, composta, funzione
into :ls_flag_tipo, :ld_numero, :ls_sql_statement, :ls_tabella, :ls_colonna, :ls_composta, :ls_funzione
from tab_formule_calcolo
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_formula=:fs_formula;

if sqlca.sqlcode<0 then
	ib_ferma_calcolo = true
	fs_errore = "Errore in lettura codice formula: "+sqlca.sqlerrtext
	return ERRORE_FORMULA
	
elseif sqlca.sqlcode=100 then
	ib_ferma_calcolo = true
	fs_errore = "Formula con codice "+fs_formula+" non trovata! "+sqlca.sqlerrtext
	return ERRORE_FORMULA
	
end if

choose case ls_flag_tipo
	case "N"
		//costante numerica
		if isnull(ld_numero) then return "0.0000"
		
		ls_return = string(ld_numero, ls_format)
		return ls_return
		
	case "S"
		//sql puro (al più variabili delimitate da #..#)
		
		//se ci sono variabili sostituisco
		ls_return = uof_variabili(ls_sql_statement, fs_formula, fs_errore)
		if ls_return=ERRORE_FORMULA then
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		
		if ib_lista_variabili then return ls_sql_statement
		
		ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_return, fs_errore)
		if ll_ret<0 then
		
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		

		ls_column_type =lds_data.Describe("#1.ColType")

		choose case left(upper(ls_column_type),3)
			case "CHA"
				ls_return = "'" + lds_data.getitemstring(1, 1) + "'"
				
			case "DEC", "INT", "LON", "NUM", "REAL", "ULO"
				if ll_ret > 0 then
				ld_numero = lds_data.getitemdecimal(1, 1)
				if isnull(ld_numero) then ld_numero = 0.0000
			else
				ld_numero = 0.0000
			end if
				ls_return = string(ld_numero, ls_format)
				
			case else
				fs_errore = "Tipo dati non revisto in ritorno select ("+ls_column_type+")"
				ib_ferma_calcolo = true
				return ERRORE_FORMULA
				
		end choose
		
//		if ll_ret > 0 then
//			ld_numero = lds_data.getitemdecimal(1, 1)
//			if isnull(ld_numero) then ld_numero = 0.0000
//		else
//			ld_numero = 0.0000
//		end if
//		ls_return = string(ld_numero, ls_format)
		
		return ls_return
		
	case "C"
		//composta
		
		//verifica se c'è qualche IF
		if uof_if_condition(ls_composta, fs_errore)<0 then
			//in fs_errore il messaggio
			
			return ERRORE_FORMULA
		end if
		
		//estrapola i codici delle altre formule
		ls_return = uof_estrapola(ls_composta, fs_errore)
		
		//se ci sono variabili sostituisco
		ls_return = uof_variabili(ls_return, fs_formula, fs_errore)
		if ls_return = ERRORE_FORMULA then
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		
		return ls_return
	
	case "T" //valore colonna di una tabella: gestire la wherestring
		fs_errore = "Tipo calcolo su tabella in fase di sviluppo (formula "+fs_formula +")"
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
		
	case "F"
		// Funzione
		if isnull(ls_funzione) or ls_funzione = "" then
			fs_errore = "La funzione non è impostata correttamente, la dichiarazione risulta vuota"
			ib_ferma_calcolo = true
			return ERRORE_FORMULA
		end if
		
		// il risultato viene impostato nella variabile idc_internal_result
		ls_return = uof_esegui_funzione(ls_funzione, fs_errore)
		return ls_return
		
	case "E"
		// Excel
		
	case "D"
		// data store	
		
		
	case else
		fs_errore = "Tipo non previsto per formula "+fs_formula
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
		
end choose



end function

private function string uof_remove_quote (string as_valore);if isnull(as_valore) then
	return as_valore
end if

if left(as_valore, 1) = "'" then
	as_valore = mid(as_valore, 2)
end if

if right(as_valore, 1) = "'" then
	as_valore = mid(as_valore, 1, len(as_valore) -1)
end if

return as_valore
end function

public function string uof_get_parametro_funzione (string as_parametro);/**
 * stefanop
 * 17/05/2012
 *
 * Recupera il valore di un parametro nella cache dei valori per usarlo nella funzione.
 * Il parametro protrebbe essere a sua volta una chiave per un parametro.
 **/
 
 
string ls_result, ls_mid
int li_index

if iuo_cache.haskey(as_parametro, li_index) then
	ls_result = uof_remove_quote(iuo_cache.get(li_index))
	 
	// controllo che non sia a sua volta una variabile
	ls_mid = left(ls_result, 3)
	if ls_mid = "#S:" or ls_mid = "#N:" or ls_mid = "#D:" then
		 ls_result = uof_remove_quote(iuo_cache.get(ls_result))
	end if 
else
	ls_result = ""
end if
 
 return ls_result
end function

public function integer uof_calcola_formula_testo (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);// Calcola una formula passata come testo, senza partire da un codice formula
//questa funzione serve ad Enrico cosi com'è quindi non toccarla .....

string					ls_risultato, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_dsdef, ls_tabella_variabile
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval



ls_dsdef = 'release 6; datawindow() table(column=(type=char(1000) name=a dbname="a") )'

lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
	
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratteri che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI e 
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)

if ll_tot_variables>0 then
	//verifica che i codici estratti rappresentino effettivamente variabili codificate in tab_variabili_formule
	//se si leggi la colonna relativa e valutane il valore (stringa o numero) e sostituiscilo nella formula
	
	for ll_index=1 to ll_tot_variables
	
		select nome_campo_database,
				flag_tipo_dato
		into 	:ls_colonna,
				:ls_flag_tipo_dato
		from tab_variabili_formule
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_variabile=:ls_variabili[ll_index];
	
		if sqlca.sqlcode<0 then
			fs_errore = "Errore lettura variabile: "+ls_variabili[ll_index]+": "+sqlca.sqlerrtext
			return -1
			
		elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
			//nome colonna estratto
			
			if not lb_test_mode then
				//valuta il valore della variabile e sostituiscila nella formula
				//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
				
				//#####################################################################
				//leggi la tabella della variabile
				uof_get_tabella_variabile(ls_variabili[ll_index], ls_tabella_variabile, fs_errore)
				//#####################################################################
				
				//###########################################################################
				if uof_calcola_variabile(	ls_tabella_variabile, ls_flag_tipo_dato, ls_variabili[ll_index], ls_colonna, ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
					return -1
				end if
				//###########################################################################
				
				guo_functions.uof_replace_string(fs_testo_formula, ls_variabili[ll_index], ls_val_variabile)
				
			else
				//modifica il nome della variabile mettendo dei caratteri inizio/fine nome variabile e una lettera identificatrice del tipo
				//successivamente sarà chiamata una funzione (uof_variabili) che si occuperà di richiedere i valori per ciascuna variabile
				
				//es: DIM_2   ->   #N:DIM_2#
				guo_functions.uof_replace_string(fs_testo_formula, ls_variabili[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili[ll_index]+"#")
						
			end if
			
		else
			continue
		end if
	
	next
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if uof_eval_with_datastore(fs_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_errore);string					ls_risultato, ls_testo_formula, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

boolean				lb_test_mode

integer				li_ret



lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
		
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//LETTURA FORMULA
if fs_cod_formula="" or isnull(fs_cod_formula) then
	fs_errore = "Codice formula non specificato!"
	return -1
end if

select testo_formula,
		testo_des_formula
into 	:ls_testo_formula,
		:ls_testo_des_formula
from tab_formule_db
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_formula=:fs_cod_formula;

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura formula: "+fs_cod_formula+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_testo_formula) or ls_testo_formula="" or isnull(ls_testo_des_formula) or ls_testo_des_formula="" then
	fs_errore = "Formula: "+fs_cod_formula+" non trovata oppure non specificata!"
	return -1
end if

//in ls_testo_formula la formula con le variabili (tab_variabili_formule.cod_variabile)
//in ls_testo_des_formula la formula con le variabili sostituite dai nomi di colonna (tab_variabili_formule.nome_campo_database) della tabella comp_det_ord_ven
ls_testo_formula_2 = ls_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(ls_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(ls_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if



//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(fs_cod_formula, ls_variabili[], lb_test_mode, ls_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

if uof_eval_with_datastore(ls_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);string					ls_risultato, ls_variabili[], ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval

integer				li_ret



lb_test_mode = true


ls_testo_formula_2 = fs_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)



//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db("", ls_variabili[], lb_test_mode, fs_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
end if


return 0

end function

public function string uof_variabili_cumulativo (string fs_formula, string fs_formula_2, string fs_cod_formula, decimal fd_risultato, ref string fs_errore);//questa funzione, a differenza della precedente (uof_variabili), mostra la finestra dei valori da dare alle variabili, una volta sola,
//riepilogando quindi le variabili richieste in elenco

/*
lstr_param_v.cod_formula
lstr_param_v.testo_formula
lstr_param_v.risultato
lstr_param_v.close_ok
lstr_param_v.array[]  --->  s_cs_xx_parametri
							
							.parametro_s_1_a[1]   -->  CODICE VARIABILE								(es. #S:nomevariabile#)
							.parametro_s_1_a[2]   -->  IDENTIFICATORE VARIABILE					(es. S: per stringa, D: per numero, D: per datetime)
							.parametro_s_1_a[3]   -->  IDENTIFICATORE EVENTUALE SELEZIONE	(es. F: campo fornitore)
							.parametro_s_1_a[4]   -->  VALORE ASSUNTO DALLA VARIABILE DOPO LA SOSTITUZIONE CON VALORE (espresso in stringa)
*/


string ls_return, ls_cod_variabile, ls_start_variabile, ls_end_variabile, ls_risultato, ls_tmp
long ll_pos, ll_pos2, ll_index
date ldt_data
s_cs_xx_param_v lstr_param_v

if ib_ferma_calcolo then return ""

ls_start_variabile = "#"
ls_end_variabile = "#"
ll_index = 0
ll_pos2 = 0

lstr_param_v.cod_formula = fs_cod_formula			//codice formula
lstr_param_v.testo_formula = fs_formula
lstr_param_v.testo_formula_2 = fs_formula_2

lstr_param_v.tipo_ritorno = is_tipo_ritorno

do while true
	
	ll_pos = pos(fs_formula, ls_start_variabile, ll_pos2 + 1)
	
	if isnull(ll_pos) or ll_pos < 1 then
		exit
	else
		ll_pos2 = pos(fs_formula, ls_end_variabile, ll_pos + 1)
	end if
	
	if ll_pos2 < 1 then
		//carattere fine variabile non trovato
		fs_errore = "Carattere "+ls_end_variabile+" fine variabile non trovato in "+fs_formula
		ib_ferma_calcolo = true
		return ERRORE_FORMULA
	end if
	
	//tra ll_pos e ll_pos2 c'è il codice variabile
	ls_cod_variabile = mid(fs_formula, ll_pos, ll_pos2 - ll_pos + 1)
	

	//GESTIONE CACHE DELLE VARIABILI -----------------------------------------------------------------------------------------
	//verifico se per caso questa variabile l'ho già incontrata, in tal caso restituisco subito il valore precedente
	if not iuo_cache.haskey(ls_cod_variabile) then
		//la variabile non era in cache, quindi chiedila e poi inseriscila in cache...
		
		ll_index += 1																										//es. se #S:F:FORNITORE# o #N:ANNO#
		lstr_param_v.array[ll_index].parametro_s_1_a[1] = ls_cod_variabile								//codice variabile -> #S:F:FORNITORE# o #N:ANNO#
		lstr_param_v.array[ll_index].parametro_s_1_a[2] = upper(mid(ls_cod_variabile, 2, 2))			//identificatore di tipo variabile -> S: o N:
		lstr_param_v.array[ll_index].parametro_s_1_a[3] = upper(mid(ls_cod_variabile, 4, 2))			//identificatore (eventuale) di selezione campo -> F: o AN(in tal caso viene ignorato)
		
		//memorizza in cache la variabile appena letta, ma senza il valore, in quanto ancora non ce l'hai
		iuo_cache.set(ls_cod_variabile, "")

	end if
	
loop


//nella struttura ad array lstr_param_v ho tutte le variabili
//apro una window per permettere all'utente l'inserimento dei rispettivi valori

openwithparm(w_compila_variabili_cumulativo, lstr_param_v)
lstr_param_v = message.powerobjectparm

if not lstr_param_v.close_ok then
	fs_errore = "Calcolo annullato dall'utente!"
	ib_ferma_calcolo = true
	return ERRORE_FORMULA
end if

if is_tipo_ritorno="1" then
	fd_risultato = lstr_param_v.risultato
else
	//torna una stringa come risultato
end if


return fs_formula
end function

public subroutine uof_pre_calcolo_formula_db (ref string fs_testo_formula);//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO
/*
	- ricerca e sostituzione dei seguenti caratteri
			","  con "."   (numeri decimali)
			
			"[" con "("	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			"]" con ")"	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			";" con ","	(compatibilità clausola IF[cond;se_vero;se_falso] con IF(cod,se_ver,se_falso))
			
			"SQR" con "SQRT"  (radice quadrata)
*/

guo_functions.uof_replace_string(fs_testo_formula, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "[", "(")
guo_functions.uof_replace_string(fs_testo_formula, "]", ")")
guo_functions.uof_replace_string(fs_testo_formula, ";", ",")
guo_functions.uof_replace_string(fs_testo_formula, "SQR", "SQRT")
guo_functions.uof_replace_string(fs_testo_formula, "'", "~~'")
guo_functions.uof_replace_string(fs_testo_formula, '"', '~~"')
/* EnMe 30-08-2018 Aggiunto per riuscire a mettere più valori nel case, altrimento la virgola veniva sostituita dal punto
	Quindi il case deve essere fatto così, cioè va usato il carattere backslash come separatore per non creare problemi.
	CASE(TEST WHEN 'COEL0007'\'COEL0009'  THEN 'COEL0116' ELSE 'COEL0132')
*/
guo_functions.uof_replace_string(fs_testo_formula, '\', ',')
end subroutine

public function long uof_estrai_stringhe_espress_reg (string fs_testo_formula, ref string fs_variabili[], ref string fs_errore);//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/


long ll_tot_variables
uo_espress_reg luo_espress_reg

luo_espress_reg = create uo_espress_reg

if luo_espress_reg.set_pattern(PATTERN_VARIABLES, fs_errore) < 0 then
	destroy luo_espress_reg
	return -1
end if

ll_tot_variables = luo_espress_reg.cerca_array( fs_testo_formula, fs_variabili[])
destroy luo_espress_reg

return ll_tot_variables
end function

public function integer uof_setvariables_formule_db (string fs_cod_formula, string fs_variabili[], boolean fb_test_mode, ref string fs_testo_formula, ref string fs_testo_formula_2, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp, ls_prog_riga, ls_tabella_variabile
dec{4}			ld_val_variabile, ldc_risultato
datetime			ldt_val_variabile
date				ldt_date


ll_tot_variables = upperbound(fs_variabili[])

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	ls_temp = upper(fs_variabili[ll_index])
	
	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) or &
			ls_temp="=" or  upper(ls_temp)="LEFT" or  upper(ls_temp)="POS" or upper(ls_temp)="RIGHT" or upper(ls_temp)="AND"	 or upper(ls_temp)="FIND_STRING"	then continue
	
	select nome_campo_database,
			flag_tipo_dato
	into 	:ls_colonna,
			:ls_flag_tipo_dato
	from tab_variabili_formule
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				upper(cod_variabile)=:ls_temp;

	if sqlca.sqlcode<0 then
		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
		//nome colonna estratto
		
		if not fb_test_mode then
			//valuta il valore della variabile e sostituiscila nella formula
			//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
			
			//#####################################################################
			//leggi la tabella della variabile
			uof_get_tabella_variabile(fs_variabili[ll_index], ls_tabella_variabile, fs_errore)
			//#####################################################################
			
			//###########################################################################
			if uof_calcola_variabile(ls_tabella_variabile, ls_flag_tipo_dato, fs_variabili[ll_index], ls_colonna, ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
				return -1
			end if
			//###########################################################################
			
			guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
			
		else
			//modifica il nome della variabile mettendo dei caratteri inizio/fine nome variabile e una lettera identificatrice del tipo
			//successivamente sarà chiamata una funzione (uof_variabili) che si occuperà di richiedere i valori per ciascuna variabile
			
			//es: DIM_2   ->   #N:DIM_2#
			guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], "#"+ls_flag_tipo_dato+":"+fs_variabili[ll_index]+"#")
					
		end if

	else
		continue
	end if

next


//SE SIAMO IN MODALITà TEST VUOL DIRE CHE STAI TESTANDO UNA FORMULA
//QUINDI VIENE CHIAMATA UNA FUNZIONE CHE APRE UNA FINESTRA CHE RICHIEDE ALL'UTENTE DI IMMETTERE I VALORIO DELLE VARIABILI EVENTUALI
//VISUALIZZANDO SUBITO IL RELATIVO RISULTATO
if fb_test_mode then
	//chiedi i valori per ogni variabile
	//ls_testo_formula = uof_variabili(ls_testo_formula, fs_cod_formula, fs_errore)
	fs_testo_formula = uof_variabili_cumulativo(fs_testo_formula, fs_testo_formula_2, fs_cod_formula, ldc_risultato, fs_errore)
	if fs_testo_formula = ERRORE_FORMULA then
		ib_ferma_calcolo = true
		
		return -1
	end if
	
	//ora da qui puoi tranquillamente uscire, quando rientrerai nella funzione chiamante ...
	return 1
end if


return 0
end function

public function integer uof_eval_with_datastore (string fs_testo_formula, ref decimal fdc_risultato, ref string fs_errore);
string			ls_risultato
integer		li_ret


li_ret = uof_eval_with_datastore(fs_testo_formula, ls_risultato, fs_errore)

if li_ret<0 then
	//in fs_errore c'è il messaggio
	return -1
end if

fdc_risultato = dec(ls_risultato)

return 0



//datastore				lds_eval
//string						ls_dsdef, ls_risultato
//
//
//
////---------------------------------------------------------------------------------------------------------------------------------------
////VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
//
//ls_dsdef = 'release 6; datawindow() table(column=(type=char(255) name=a dbname="a") )'
//
//lds_eval = create datastore
//lds_eval.Create(ls_dsdef, fs_errore)
//lds_eval.InsertRow(0)
//
//if Len(fs_errore) > 0 then
//	destroy lds_eval
//	return -1
//else
//	ls_risultato = lds_eval.describe ( 'evaluate( " ' + fs_testo_formula + ' " ,1)' )
//	destroy lds_eval
//	
//	if ls_risultato="!" then
//		fs_errore = "Errore! Verificare la formula!"
//		return -1
//	end if
//	
//	fdc_risultato = dec(ls_risultato)
//end if
//
//
//
//return 0
end function

public function integer uof_setvariables_auto_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date
integer			li_ret


ll_tot_variables = upperbound(fs_variabili[])

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	
	li_ret = uof_set_variabile(fs_variabili[ll_index], fs_testo_formula, fs_errore)
	if li_ret<0 then
		return -1
		
	elseif li_ret=1 then
		continue
		
	end if
	
	
	
//	select nome_campo_database,
//			flag_tipo_dato
//	into 	:ls_colonna,
//			:ls_flag_tipo_dato
//	from tab_variabili_formule
//	where 	cod_azienda=:s_cs_xx.cod_azienda and
//				cod_variabile=:fs_variabili[ll_index];
//
//	ls_temp = upper(fs_variabili[ll_index])
//	
//	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
//	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then continue
//
//	if sqlca.sqlcode<0 then
//		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
//		return -1
//		
//	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
//		
//		choose case ls_flag_tipo_dato
//			case "S"
//				fs_errore = "La funzione uof_setvariables_auto_formule_db NON ammette l'uso di variabili STRINGA!"
//				return -1
//				
//			case "N"
//				ld_val_variabile = 350		//valore fisso
//
//				ls_val_variabile = string(ld_val_variabile, "########0.0000")
//				guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
//				guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
//				
//			case "D"
//				fs_errore = "La funzione uof_setvariables_auto_formule_db NON ammette l'uso di variabili DATETIME!"
//				return -1
//			
//		end choose
//
//		
//	else
//		continue
//	end if

next


return 0
end function

public subroutine uof_setvariables_dtaglio (ref string fs_testo_formula, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita);
//il set di variabili disponibili nelle formule delle distinte di taglio sono un insieme ristretto e costante

guo_functions.uof_replace_string(fs_testo_formula, "quan_utilizzo", 		string(fd_quan_utilizzo, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "x", 						string(fd_dim_x, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "y", 						string(fd_dim_y, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "z", 						string(fd_dim_z, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "dim_t", 				string(fd_dim_t, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "tessuto", 				fs_cod_tessuto)
guo_functions.uof_replace_string(fs_testo_formula, "h_mantovana", 		string(fd_alt_mantovana, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "quan_ordine", 		string(fd_quan_ordine, "########0.0000"))
guo_functions.uof_replace_string(fs_testo_formula, "luce_finita", 			fs_luce_finita)

return
end subroutine

public function integer uof_elabora_select (string fs_espressione, ref string fs_select, ref decimal fdc_risultato, ref string fs_errore);datastore		lds_data
long				ll_tot
any				lany_valore
string				ls_col_type, ls_risultato

fs_select = "select "+fs_espressione

if ib_lista_variabili then return 1

ll_tot = guo_functions.uof_crea_datastore(lds_data, fs_select, fs_errore)

if ll_tot<0 then
	//in fs_errore il messaggio
	
	//gestisci la possibilità di aver passato una variabile non numerica, es. fs_espressione= pippo005
	//select pippo005 darebbe errore
	//torna -1 e dai la possibilità di continuare, perchè se la chiamata viene da elaborazione if
	//viene interpretato dal sistema come confronto tra stringhe e non numeri
	
	return -1
end if

if lds_data.rowcount() > 0 then
	
	ls_col_type = lds_data.describe("#1.coltype")
	/*
	Char
	Date
	DateTime
	Decima
	Int
	Long
	Number
	Real
	Time
	Timestamp
	ULong
	*/
	
	choose case left(upper(ls_col_type), 3)
		case "DEC","INT","LON","NUM","REA","ULO"
			fdc_risultato = lds_data.getitemnumber(1, 1)
			//setnull(fs_risultato)
			
		case "CHA"
			//fs_risultato = lds_data.getitemstring(1, 1)
			setnull(fdc_risultato)
			
		case else
			destroy lds_data;
			return -1
			
	end choose
	
else
	fdc_risultato = 0.000
	//setnull(fs_risultato)
end if

destroy lds_data;

return 1
end function

public function integer uof_eval_with_datastore (string fs_testo_formula, ref string fs_risultato, ref string fs_errore);datastore				lds_eval
string						ls_dsdef, ls_risultato



//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

ls_dsdef = 'release 6; datawindow() table(column=(type=char(255) name=a dbname="a") )'

lds_eval = create datastore
lds_eval.Create(ls_dsdef, fs_errore)
lds_eval.InsertRow(0)

if Len(fs_errore) > 0 then
	destroy lds_eval
	return -1
else
	ls_risultato = lds_eval.describe ( 'evaluate( " ' + fs_testo_formula + ' " ,1)' )
	destroy lds_eval
	
	if ls_risultato="!" then
		fs_errore = "Errore! Verificare la formula!"
		return -1
	end if
	
	fs_risultato = ls_risultato
end if



return 0
end function

public function integer uof_calcola_formula_db (string fs_cod_formula, ref decimal fdc_risultato, ref string fs_risultato, ref string fs_errore);string					ls_risultato, ls_testo_formula, ls_testo_des_formula, ls_variabili[], ls_colonna, ls_flag_tipo_dato, &
						ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

boolean				lb_test_mode

integer				li_ret



lb_test_mode = true
if (ii_anno_reg_ord_ven>0 and il_num_reg_ord_ven>0 and il_prog_riga_ord_ven>0) or &
		(is_cod_prodotto_padre<>"" and is_cod_prodotto_figlio<>"" and &
		is_cod_versione_padre<>"" and is_cod_versione_figlio<>"" and il_num_sequenza>0) then
		
	lb_test_mode = false
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//LETTURA FORMULA
if fs_cod_formula="" or isnull(fs_cod_formula) then
	fs_errore = "Codice formula non specificato!"
	return -1
end if

select testo_formula,
		testo_des_formula,
		flag_tipo
into 	:ls_testo_formula,
		:ls_testo_des_formula,
		:is_tipo_ritorno
from tab_formule_db
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_formula=:fs_cod_formula;

if isnull(is_tipo_ritorno) or is_tipo_ritorno="" then
	is_tipo_ritorno = "1"		//numerico
end if

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura formula: "+fs_cod_formula+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_testo_formula) or ls_testo_formula="" or isnull(ls_testo_des_formula) or ls_testo_des_formula="" then
	fs_errore = "Formula: "+fs_cod_formula+" non trovata oppure non specificata!"
	return -1
end if

//in ls_testo_formula la formula con le variabili (tab_variabili_formule.cod_variabile)
//in ls_testo_des_formula la formula con le variabili sostituite dai nomi di colonna (tab_variabili_formule.nome_campo_database) della tabella comp_det_ord_ven
ls_testo_formula_2 = ls_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(ls_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(ls_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(fs_cod_formula, ls_variabili[], lb_test_mode, ls_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

if is_tipo_ritorno="1" then
	//numerico
	li_ret = uof_eval_with_datastore(ls_testo_formula, fdc_risultato, fs_errore)
	setnull(fs_risultato)
else
	//stringa
	li_ret = uof_eval_with_datastore(ls_testo_formula, fs_risultato, fs_errore)
	setnull(fdc_risultato)
end if

if li_ret < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_testo_per_test (string fs_testo_formula, ref string fs_risultato, ref string fs_errore);string					ls_risultato, ls_variabili[], ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_testo_formula_2
						
dec{4}				ld_val_variabile

datetime				ldt_val_variabile
date					ldt_date
						
long					ll_tot_variables, ll_index

uo_espress_reg	luo_espress_reg

boolean				lb_test_mode

datastore			lds_eval

integer				li_ret



lb_test_mode = true


ls_testo_formula_2 = fs_testo_formula


//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratterei che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)



//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALORE MEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db("", ls_variabili[], lb_test_mode, fs_testo_formula, ls_testo_formula_2, fs_errore)
if li_ret < 0 then
	return -1
end if


return 0

end function

public function integer uof_set_variabile (string fs_cod_variabile, ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date


select nome_campo_database,
		flag_tipo_dato
into 	:ls_colonna,
		:ls_flag_tipo_dato
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_variabile=:fs_cod_variabile;

ls_temp = upper(fs_cod_variabile)

//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then return 1

if sqlca.sqlcode<0 then
	fs_errore = "Errore lettura variabile: "+fs_cod_variabile+": "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
	
	choose case ls_flag_tipo_dato
		case "S"
			fs_errore = "La funzione uof_set_variabile NON ammette l'uso di variabili STRINGA!"
			return -1
			
		case "N"
			ld_val_variabile = 350		//valore fisso

			ls_val_variabile = string(ld_val_variabile, "########0.0000")
			guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
			guo_functions.uof_replace_string(fs_testo_formula, fs_cod_variabile, ls_val_variabile)
			
		case "D"
			fs_errore = "La funzione uof_set_variabile NON ammette l'uso di variabili DATETIME!"
			return -1
		
	end choose

	
else
	return 1
end if



return 0
end function

public function integer uof_check_variabile (string fs_cod_variabile_originale, ref string fs_cod_variabile);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp
dec{4}			ld_val_variabile, fdc_risultato
datetime			ldt_val_variabile
date				ldt_date



ls_temp = upper(fs_cod_variabile_originale)

//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) then return 1


select nome_campo_database,
		flag_tipo_dato
into 	:ls_colonna,
		:ls_flag_tipo_dato
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_variabile=:fs_cod_variabile_originale;

if sqlca.sqlcode<0 then
	//me ne infischio dell'errore
	return 1
	
elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
	fs_cod_variabile = "#"+ls_flag_tipo_dato+":" + fs_cod_variabile_originale+ "#"

else
	return 1
end if



return 0
end function

public function integer uof_setvariables_formule_db (string fs_variabili[], ref string fs_testo_formula, ref string fs_errore);long				ll_tot_variables, ll_index
string				ls_colonna, ls_flag_tipo_dato, ls_val_variabile, ls_temp, ls_prog_riga, ls_tabella_variabile
dec{4}			ld_val_variabile, ldc_risultato
datetime			ldt_val_variabile
date				ldt_date


ll_tot_variables = upperbound(fs_variabili[])

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot_variables
	
	ls_temp = upper(fs_variabili[ll_index])
	
	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
	if isnull(ls_temp) or ls_temp="" or upper(ls_temp)="IF" or ls_temp="'" or pos(ls_temp, "'")>0 or isnumber(ls_temp) or &
			ls_temp="=" or  upper(ls_temp)="LEFT" or  upper(ls_temp)="POS" or upper(ls_temp)="RIGHT" or upper(ls_temp)="AND"	 or upper(ls_temp)="FIND_STRING"	then continue
	
	select nome_campo_database,
			flag_tipo_dato
	into 	:ls_colonna,
			:ls_flag_tipo_dato
	from tab_variabili_formule
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				upper(cod_variabile)=:ls_temp;

	if sqlca.sqlcode<0 then
		fs_errore = "Errore lettura variabile: "+fs_variabili[ll_index]+": "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
		//nome colonna estratto
		
		//#####################################################################
		//leggi la tabella della variabile
		uof_get_tabella_variabile(fs_variabili[ll_index], ls_tabella_variabile, fs_errore)
		//#####################################################################
		
		//valuta il valore della variabile e sostituiscila nella formula
		//ls_flag_tipo_dato  può essere S (stringa) oppure N (numerico decimale) oppure D (datetime)
		
		//###########################################################################
		if uof_calcola_variabile(	ls_tabella_variabile, ls_flag_tipo_dato, fs_variabili[ll_index], ls_colonna, ls_val_variabile, ld_val_variabile, ldt_val_variabile, fs_errore) < 0 then
			return -1
		end if
		//###########################################################################
		guo_functions.uof_replace_string(fs_testo_formula, fs_variabili[ll_index], ls_val_variabile)
			
	else
		continue
	end if

next

return 0
end function

public function integer uof_get_tabella_variabile (string as_cod_formula, ref string as_tabella_variabile, ref string as_errore);//in base al contesto, (ORD_VEN oppure OFF_VEN) la variabile viene calcolata sulla comp_det_ord_ven oppure sulla comp_det_off_ven
//questa funzione è necessaria in quanto la tabella dell variabili da la possibilità di mettere solo comp_det_ord_ven e non quella delle offerte
//l'utente quindi definisce una sola variabile, es. DIM_X e specifica la tabella comp_det_ord_ven
//in fase di calcolo poi si controlla il contesto


select nome_tabella_database
into 	:as_tabella_variabile
from tab_variabili_formule
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_variabile=:as_cod_formula;

if sqlca.sqlcode<0 then
	as_errore = "Errore lettura tabella variabile: "+as_cod_formula+": "+sqlca.sqlerrtext
	return -1
end if


if left(as_tabella_variabile, 9) = "comp_det_" then
	//dipende dal contesto
	if is_contesto="OFF_VEN" then
		as_tabella_variabile = "comp_det_off_ven"
	else
		as_tabella_variabile = "comp_det_ord_ven"
	end if
else
	//dipende effettivamente dalla tabella della variabile (per ora solo tabella distinta)
	//quindi as_tabella_variabile
end if

return 0
end function

public subroutine uof_crea_ds_variabili_vuoto (ref datastore ads_variabili);
//creazione struttura datastore delle variabili, da usare come cache

ads_variabili = create datastore
ads_variabili.dataobject = "d_compila_variabili_veloce"
ads_variabili.settransobject(sqlca)

end subroutine

public function integer uof_calcola_formula_veloce (string as_cod_formula, string as_formula, string as_tipo_ritorno, ref datastore ads_variabili, ref decimal ad_valore, ref string as_valore, ref string as_errore);string			ls_variabili[], ls_var, ls_flag_tipo_dato, ls_colonna, ls_val_var, ls_ret, ls_var_to_validate[], ls_tipovar_to_validate[], ls_valvar_to_validate[], &
				ls_testo_originale_formula, ls_nome_tabella
				
long			ll_tot, ll_index, ll_new, ll_index2

dec{4}		ld_val_var, ld_valvar_to_validate[]

integer		li_ret

boolean		lb_presente

datetime		ldt_val_var

s_cs_xx_parametri			lstr_parametri




ls_testo_originale_formula = as_formula
uof_pre_calcolo_formula_db(as_formula)

//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
ll_tot = uof_estrai_stringhe_espress_reg(as_formula, ls_variabili[], as_errore)
if ll_tot<0 then
	return -1
end if

ll_tot = upperbound(ls_variabili[])
ll_new = 0
//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
for ll_index=1 to ll_tot
	
	ls_var = upper(ls_variabili[ll_index])
	
	//alcuni casi, già li possiamo escludere, per evitare di fare select inutili
	if isnull(ls_var) or ls_var="" or upper(ls_var)="IF" or ls_var="'" or pos(ls_var, "'")>0 or isnumber(ls_var) or &
			ls_var="=" or  upper(ls_var)="LEFT" or  upper(ls_var)="POS" or upper(ls_var)="RIGHT" or ls_var="AND" and upper(ls_var)="CASE" then continue
	
	//se presente in datastore cache
	ls_flag_tipo_dato = uof_controlla_ds_variabili(ls_variabili[ll_index], ads_variabili, ls_val_var, ld_val_var)
	
	choose case ls_flag_tipo_dato
		//--------------------------------------------------------------------------------------
		case "N"
			//trovata variabile in datastore cache e di tipo numero
			if isnull(ld_val_var) then ld_val_var=0
			ls_val_var = string(ld_val_var, "########0.0000")
			guo_functions.uof_replace_string(ls_val_var, ",", ".")
			
			//rimpiazzo valore variabile nella formula
			guo_functions.uof_replace_string(as_formula, ls_variabili[ll_index], ls_val_var)
		
		//--------------------------------------------------------------------------------------
		case "S"
			//trovata variabile in datastore cache e di tipo stringa
			if isnull(ls_val_var) then ls_val_var=""
			ls_val_var = "~'"+ls_val_var+"~'"
			
			//rimpiazzo valore variabile nella formula
			guo_functions.uof_replace_string(as_formula, ls_variabili[ll_index], ls_val_var)
			
		
		//--------------------------------------------------------------------------------------
		case else
			//VARIABILE NON TROVATA IN CACHE
			//prima di predisporla negli arrays per la valdazione controlla se per caso è già presente negli stessi
			lb_presente = false
			for ll_index2=1 to upperbound(ls_var_to_validate[])
				if ls_variabili[ll_index] = ls_var_to_validate[ll_index2] then
					//gia presente
					lb_presente = true
					exit		//da questo ciclo for di controllo presenza
				end if
			next
			
			if not lb_presente then
				//metti negli arrays
				select 	flag_tipo_dato, nome_campo_database, nome_tabella_database
				into 		:ls_flag_tipo_dato, :ls_colonna, :ls_nome_tabella
				from 		tab_variabili_formule
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							upper(cod_variabile)=:ls_var;
			
				if sqlca.sqlcode<0 then
					as_errore = "Errore lettura variabile: "+ls_variabili[ll_index]+": "+sqlca.sqlerrtext
					return -1
					
				elseif sqlca.sqlcode=0 and ls_colonna<>"" and not isnull(ls_colonna) then
					//variabile trovata in tabella
					
					//se su distinta base allora la leggo, la sostituisco nella formula e non la salvo in cache
					if upper(ls_nome_tabella) = "DISTINTA" then
						
						if uof_calcola_variabile(	ls_nome_tabella, ls_flag_tipo_dato, ls_variabili[ll_index], ls_colonna, ls_val_var, ld_val_var, ldt_val_var, as_errore) < 0 then
							return -1
						end if
						guo_functions.uof_replace_string(as_formula, ls_var, ls_val_var)
						continue
						
					else
						//carico in array variabili da validare
						ll_new += 1
						ls_var_to_validate[ll_new]		= ls_variabili[ll_index]
						ls_tipovar_to_validate[ll_new]	= ls_flag_tipo_dato
					end if
			
				else
					//probabile sia un operatore funzionale, quindi non è una variabile da calcolare
					continue
				end if
			end if
		
	end choose
next

//a questo punto, se nell'array ls_var_to_validate[] c'è qualcosa da validare lo faccio con l'apertura di una finestra
ll_tot = upperbound(ls_var_to_validate[])

if ll_tot > 0 then

	for ll_index=1 to ll_tot
		ls_valvar_to_validate[ll_index] = ""
		ld_valvar_to_validate[ll_index] = 0
	next

	lstr_parametri.parametro_s_1 = as_cod_formula
	lstr_parametri.parametro_s_2 = ls_testo_originale_formula
	lstr_parametri.parametro_ds_1 = ads_variabili
	lstr_parametri.parametro_s_1_a[] = ls_var_to_validate[]
	lstr_parametri.parametro_s_2_a[] = ls_tipovar_to_validate[]
	lstr_parametri.parametro_s_3_a[] = ls_valvar_to_validate[]
	lstr_parametri.parametro_d_1_a[] = ld_valvar_to_validate[]
	
	//apro la finestra richiesta variabili
	openwithparm(w_compila_variabili_veloce, lstr_parametri)
	//le nuove variabili sono state già impostate nel datastore (accodate a quelle già presenti)
	
	lstr_parametri = message.powerobjectparm
	
	ads_variabili = lstr_parametri.parametro_ds_1
	ls_var_to_validate[] = lstr_parametri.parametro_s_1_a[]
	ls_tipovar_to_validate[] = lstr_parametri.parametro_s_2_a[]
	ls_valvar_to_validate[] = lstr_parametri.parametro_s_3_a[]
	 ld_valvar_to_validate[] = lstr_parametri.parametro_d_1_a[]
	
	//sostituisco valori variabili nella formula
	for ll_index=1 to upperbound(ls_var_to_validate[])
		ls_var = ls_var_to_validate[ll_index]
		
		if ls_tipovar_to_validate[ll_index] = "N" then
			ls_val_var = string(ld_valvar_to_validate[ll_index], "########0.0000")
			guo_functions.uof_replace_string(ls_val_var, ",", ".")
			//rimpiazzo valore variabile nella formula
			guo_functions.uof_replace_string(as_formula, ls_var, ls_val_var)
			
		else
			//S
			ls_val_var = "~'"+ls_valvar_to_validate[ll_index]+"~'"
			//rimpiazzo valore variabile nella formula
			guo_functions.uof_replace_string(as_formula, ls_var, ls_val_var)
		end if
	next
	
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if as_tipo_ritorno="1" then
	//numerico
	ll_tot = uof_eval_with_datastore(as_formula, ad_valore, as_errore)
	setnull(as_valore)
else
	//stringa
	ll_tot = uof_eval_with_datastore(as_formula, as_valore, as_errore)
	setnull(ad_valore)
end if

if ll_tot < 0 then
	return -1
end if

return 0
end function

public function string uof_controlla_ds_variabili (string as_cod_variabile, datastore ads_variabili, ref string as_valore, ref decimal ad_valore);long			ll_index



for ll_index=1 to ads_variabili.rowcount()
	if as_cod_variabile = ads_variabili.getitemstring(ll_index, 1) then
		//variabile già valorizzata
		if ads_variabili.getitemstring(ll_index, 2)="N" then
			ad_valore = ads_variabili.getitemnumber(ll_index, 4)
			setnull(as_valore)
			return "N"
			
		elseif ads_variabili.getitemstring(ll_index, 2)="S" then
			as_valore = ads_variabili.getitemstring(ll_index, 3)
			setnull(ad_valore)
			return "S"
			
		end if
	end if
next

return ""
end function

public function integer uof_calcola_variabile (string as_tabella_variabile, string as_tipo_ritorno, string as_cod_variabile, string as_colonna, ref string as_valore, ref decimal ad_valore, ref datetime adt_valore, ref string as_errore);string				ls_where, ls_where_2, ls_table, ls_sql
date				ldt_date
datastore		lds_data
long				ll_count



if as_tabella_variabile="comp_det_off_ven" or as_tabella_variabile="comp_det_ord_ven" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if isnull(ii_anno_reg_ord_ven) or ii_anno_reg_ord_ven<=0 then
		as_errore += " l'anno ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_num_reg_ord_ven) or il_num_reg_ord_ven<=0 then
		as_errore += " il numero ordine/offerta, che non è stato passato!"
		return -1
	end if
	
	if isnull(il_prog_riga_ord_ven) or il_prog_riga_ord_ven<=0 then
		as_errore += " il numero riga ordine/offerta, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------
	
elseif as_tabella_variabile="distinta" then
	//preparo per l'eventuale errore
	as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+as_tabella_variabile+" richiede "
	//--------------------------------------------------------------------
	if is_cod_prodotto_padre = "" or isnull(is_cod_prodotto_padre) then
		as_errore += " il cod.prodotto padre, che non è stato passato!"
		return -1
	end if
	
	if is_cod_prodotto_figlio = "" or isnull(is_cod_prodotto_figlio) then
		as_errore += " il cod.prodotto figlio, che non è stato passato!"
		return -1
	end if
	
	if is_cod_versione_padre = "" or isnull(is_cod_versione_padre) then
		as_errore += " la versione del padre, che non è stata passata!"
		return -1
	end if
	
	if is_cod_versione_figlio = "" or isnull(is_cod_versione_figlio) then
		as_errore += " la versione del figlio, che non è stata passata!"
		return -1
	end if
	
	if il_num_sequenza<0 or isnull(il_num_sequenza) then
		as_errore += " il num.sequenza, che non è stato passato!"
		return -1
	end if
	//--------------------------------------------------------------------	
end if

as_errore = ""

choose case as_tabella_variabile
	case "comp_det_off_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
		
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_off_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_offerta" e tabella "comp_det_off_ven allora la tabella deve essere "det_off_ven" e la colonna quan_offerta
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_off_ven"
			as_colonna = "quan_offerta"
		end if
		//------------------------------------
		
						
	//----------------------------------------------------------------------------------------------------------------------
	case "comp_det_ord_ven"
		ls_where =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
						"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
						"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
						
		//chiesto da Alberto il 04/06/2013
		//se la variabile è su "des_prodotto" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven"
		if upper(as_colonna) = "DES_PRODOTTO" then
			as_tabella_variabile = "det_ord_ven"
		end if
		//------------------------------------
		
		//chiesto da Alberto il 13/11/2013
		//se la variabile è su "quan_ordine" e tabella "comp_det_ord_ven allora la tabella deve essere "det_ord_ven" e la colonna quan_ordine
		if upper(as_colonna) = "QUAN_OFFERTA" or upper(as_colonna) = "QUAN_ORDINE" then
			as_tabella_variabile = "det_ord_ven"
			as_colonna = "quan_ordine"
		end if
		//------------------------------------
						
	//----------------------------------------------------------------------------------------------------------------------
	case "distinta"
		//where di base per la tabella distinta
		ls_table = "distinta"
		ls_where =  "cod_prodotto_padre='"+is_cod_prodotto_padre+ "' and " + &
						"cod_versione='"+is_cod_versione_padre+"' and "+&
						"num_sequenza="+string(il_num_sequenza)+" and "+&
						"cod_prodotto_figlio='"+is_cod_prodotto_figlio+"' and "+&
						"cod_versione_figlio='"+is_cod_versione_figlio+"'"
		
		//se esiste una variante (ordine, offerta o commessa, in base al contesto) la variabile va calcolata da questa tabella
		//			infatti, ad es. se una variabile fa riferimento al prodotto figlio, 
		//			questo deve essere il prodotto variante e non il figlio nella distinta base
		choose case is_contesto
			case "OFF_VEN"
				ls_table = "varianti_det_off_ven"
				ls_where_2 = 	"anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_off_ven="+string(il_prog_riga_ord_ven)
				
			case "ORD_VEN"
				ls_table = "varianti_det_ord_ven"
				ls_where_2 =  "anno_registrazione="+string(ii_anno_reg_ord_ven)+ " and " + &
									"num_registrazione="+string(il_num_reg_ord_ven)+" and "+&
									"prog_riga_ord_ven="+string(il_prog_riga_ord_ven)
			
			case "COMMESSE"
				//in questo caso, però devono essere passate le variabili per il calcolo (li_anno_commessa, ll_num_commessa)
				
				if isnull(ii_anno_commessa) or ii_anno_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				if isnull(il_num_commessa) or il_num_commessa<=0 then
					as_errore = "Il calcolo della variabile relativa alla colonna "+as_colonna+" in tabella "+ls_table+" richiede "+&
									" l'anno della commessa, che non è stato passato!"
					return -1
				end if
				
				ls_table = "varianti_commesse"
				ls_where_2 =  "anno_commessa="+string(ii_anno_commessa)+ " and " + &
									"num_commessa="+string(il_num_commessa)
				
		end choose
		
		ls_sql = 	"select count(*) from " + ls_table + " " +&
					"where cod_azienda='"+ s_cs_xx.cod_azienda+"' "
					
		if as_tabella_variabile <> "distinta" then ls_sql += " and cod_prodotto is not null "

		ls_sql += " and " + ls_where 
							
		if g_str.isnotempty( ls_where_2) then ls_sql += " and "+ ls_where_2
		
		if guo_functions.uof_crea_datastore(lds_data, ls_sql)>0 then
		else
			as_errore = "Errore creaz. ds per controllo varianti (uof_calcola_variabile)"
			return -1
		end if
		
		//conteggio varianti presenti
		ll_count = lds_data.getitemnumber(1, 1)
		
		if ll_count>0 then
			//esiste una variante, quindi modifico i termini per fare in modo da leggere dalla tabella delle varianti
			
			as_tabella_variabile = ls_table
			if g_str.isnotempty( ls_where_2) then ls_where += " and "+ ls_where_2
			// Enme 27/03/2017
			//ls_where = ls_where + " and " + ls_where_2
			
			//inoltre se la variabile è relativa alla colonna del prodotto figlio o della versione del prodotto figlio,
			//allora leggo la variante figlio o la versione della variante filgio, come variabile nella formula
			if upper(as_tabella_variabile) <> "DISTINTA" and as_colonna="cod_prodotto_figlio" then
				as_colonna = "cod_prodotto"			//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
//			elseif as_colonna="cod_versione_figlio" then
				//as_colonna = "cod_versione_figlio"	//infatti cosi si chiama la colonna nelle 3 tabelle possibili delle vaianti (ordini, offerte, commesse)
				
			end if
		end if
		
		//se non hai trovato varianti la variabile ls_where e la variabile  as_tabella_variabile è rimasta la stessa ...
		
	//----------------------------------------------------------------------------------------------------------------------
	case "dinamica"
		choose case is_contesto
			case "OFF_VEN"
				as_tabella_variabile = "comp_det_off_ven"
			case "ORD_VEN"
				as_tabella_variabile = "comp_det_ord_ven"
		end choose
		
	//----------------------------------------------------------------------------------------------------------------------
	case else
		as_errore = "Tabella variabile non specificata oppure non tra quelle previste!"
		return -1
		
end choose


choose case as_tabella_variabile
	case "comp_det_off_ven", "comp_det_ord_ven"
		
		choose case as_tipo_ritorno
			case "S"
				ls_sql = "select valore_stringa "
			case "N"
				ls_sql = "select valore_numero "
			case "D"
				ls_sql = "select valore_datetime "
		end choose
		
		if as_tabella_variabile = "comp_det_off_ven" then
			ls_sql += " from det_off_ven_conf_variabili "
		else
			ls_sql += " from det_ord_ven_conf_variabili "
		end if
		
		ls_sql += 	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						" anno_registrazione = " + string(ii_anno_reg_ord_ven) + " and " + &
						" num_registrazione = " + string(il_num_reg_ord_ven) + " and "
						
		if as_tabella_variabile = "comp_det_off_ven" then
			ls_sql += " prog_riga_off_ven = " + string(il_prog_riga_ord_ven) + " and "
		else
			ls_sql += " prog_riga_ord_ven = " + string(il_prog_riga_ord_ven) + " and "
		end if
		
		ls_sql +=	" cod_variabile = '" + as_cod_variabile + "' "


		if guo_functions.uof_crea_datastore(lds_data, ls_sql) < 0 then
			as_errore = "Errore in lettura valore variabile da "+as_tabella_variabile+" (uof_calcola_variabile). " + ls_sql
			return -1
		end if
																							

		choose case as_tipo_ritorno
			case "S"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					as_valore = f_des_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					as_valore = lds_data.getitemstring(1, 1)
				end if
				if isnull(as_valore) then as_valore = ''
				as_valore = "~'"+as_valore+"~'"
				setnull(ad_valore)
				setnull(adt_valore)
			case "N"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					ad_valore = f_dec_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					ad_valore = lds_data.getitemnumber(1, 1)
				end if
				if isnull(ad_valore) then ad_valore = 0
				as_valore = string(ad_valore, "########0.0000")
				guo_functions.uof_replace_string(as_valore, ",", ".")
				setnull(adt_valore)
			case "D"
				if lds_data.rowcount() = 0 then							
				// variabile non trovata, vado a vedere con il vecchio sistema
					adt_valore = f_datetime_tabella(as_tabella_variabile, ls_where, as_colonna)
				else
					adt_valore = lds_data.getitemdatetime(1, 1)
				end if
				ldt_date = date(adt_valore)
				if isnull(ldt_date) then 
					as_valore = "~'"+s_cs_xx.db_funzioni.data_neutra+"~'"
				else				
					as_valore = "~'"+string(ldt_date, s_cs_xx.db_funzioni.formato_data)+"~'"
				end if
				setnull(ad_valore)
		end choose
		
	case else // in tutti gli altri casi
		choose case as_tipo_ritorno
			case "S"
				as_valore = f_des_tabella(as_tabella_variabile, ls_where, as_colonna)
				as_valore = "~'"+as_valore+"~'"
				if isnull(as_valore) then as_valore = ''
				setnull(ad_valore)
				setnull(adt_valore)
				
			case "N"
				ad_valore = f_dec_tabella(as_tabella_variabile, ls_where, as_colonna)
				as_valore = string(ad_valore, "########0.0000")
				if isnull(ad_valore) then ad_valore = 0
				guo_functions.uof_replace_string(as_valore, ",", ".")
				setnull(adt_valore)
				
			case "D"
				adt_valore = f_datetime_tabella(as_tabella_variabile, ls_where, as_colonna)
				ldt_date = date(adt_valore)
				if isnull(ldt_date) then 
					as_valore = "~'"+s_cs_xx.db_funzioni.data_neutra+"~'"
				else				
					as_valore = "~'"+string(ldt_date, s_cs_xx.db_funzioni.formato_data)+"~'"
				end if			
				setnull(ad_valore)
		
				//todo
				//verificare SE FUNZIONA ...
			
		end choose
end choose

return 0
end function

public function integer uof_estrai_varibili_formula (string as_cod_formula, ref string as_variabili[], ref string as_error);/**
 * stefanop
 * 10/07/2014
 *
 * Recupero tutti i codici di variabili validi usanti all'interno della formula
 *
 * Return: -1 errore, >0 il numero di varibili trovate
 **/

string ls_testo_formula, ls_vars[], ls_value
int li_found
long ll_count, li_i

as_variabili = ls_vars

select testo_formula
into :ls_testo_formula
from tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_formula = :as_cod_formula;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la ricerca della formula con codice " + g_str.safe(as_cod_formula) + ".~r~n" + g_str.safe(sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then
	as_error = "Formula con codice " +  g_str.safe(as_cod_formula) + " non trovata."
	return -1
elseif g_str.isempty(ls_testo_formula) then
	// il testo è vuoto quindi niente formule
	return 0
end if

ll_count = uof_estrai_stringhe_espress_reg(ls_testo_formula, ls_vars, as_error)

for li_i = 1 to ll_count
	
	// Controllo se la variabile esiste
	select cod_variabile
	into :ls_value
	from tab_variabili_formule
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_variabile = :ls_vars[li_i];
			 
	if sqlca.sqlcode = 0 and g_str.isnotempty(ls_value) then
		li_found++
		as_variabili[li_found] = ls_value
	end if
	
next

return li_found
end function

public function integer uof_allinea_variabili_formula (string as_cod_formula, ref string as_error);/**
 * stefanop
 * 11/07/2014
 *
 * La funzione controlla quali sono le variabili utilizzate all'interno della formula e salva i risultati
 * nella tabella di appoggio tab_formule_db_variabili.
 *
 * Return -1 errore, >= 0 il numero di variabili trovate
 **/

string ls_variabili[]
int li_count_variabili, li_i

setnull(as_error)

li_count_variabili = uof_estrai_varibili_formula(as_cod_formula, ls_variabili, as_error)
if li_count_variabili <0 then return -1

delete from tab_formule_db_variabili
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_formula = :as_cod_formula;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la cancellazione dei vecchi dati della formula " + g_str.safe(as_cod_formula) + "~r~n" +  sqlca.sqlerrtext
	return -1
end if
	
// inserisco nuove variabili
if li_count_variabili > 0 then
	for li_i = 1 to li_count_variabili
		insert into tab_formule_db_variabili(cod_azienda, cod_formula, cod_variabile)
		values(:s_cs_xx.cod_azienda, :as_cod_formula, :ls_variabili[li_i]);
		
		if sqlca.sqlcode < 0 then
			as_error = "Errore durante l'inserimento della variabile " + g_str.safe(ls_variabili[li_i]) + " per la formula " + g_str.safe(as_cod_formula) + "~r~n" +  sqlca.sqlerrtext
			return -1
		end if
	next
end if

commit;
return li_count_variabili
end function

public function string uof_get_tipo_variabile (string as_cod_tipo_variabile);/**
 * stefanop
 * 08/09/2014
 *
 * Ottiene il codice del tipo variabile
 * S = sql
 * D = datastore
 **/

string ls_flag_tipo

select flag_tipo
into :ls_flag_tipo
from tab_formule_calcolo
where cod_azienda=:s_cs_xx.cod_azienda and
	  	 cod_formula=:as_cod_tipo_variabile;
			
if sqlca.sqlcode < 0 or sqlca.sqlcode = 100 then
	setnull(ls_flag_tipo)
end if

return ls_flag_tipo
end function

public function boolean uof_is_formula_datastore (string as_cod_formula);string ls_flag_tipo_formula

ls_flag_tipo_formula = uof_get_tipo_variabile(as_cod_formula)

return "D" = ls_flag_tipo_formula
end function

public function string uof_calcola_formula_datastore (string as_cod_formula, ref datastore ads_store, ref string as_error);string ls_flag_tipo, ls_sql, ls_return
long ll_rows

ls_sql = uof_calcola_formula_datastore_sql(as_cod_formula, as_error)

if ls_sql = ERRORE_FORMULA then
	return ERRORE_FORMULA
end if

ll_rows = guo_functions.uof_crea_ds(ads_store, ls_sql, sqlca, as_error)

if ll_rows < 0 then
	return ERRORE_FORMULA
else
	return "OK"
end if

end function

public function string uof_calcola_formula_datastore_sql (string as_cod_formula, ref string as_error);string ls_sql

select sql
into :ls_sql
from tab_formule_calcolo
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_formula=:as_cod_formula;

if sqlca.sqlcode<0 then
	ib_ferma_calcolo = true
	as_error = "Errore in lettura codice formula: "+sqlca.sqlerrtext
	return ERRORE_FORMULA
	
elseif sqlca.sqlcode=100 then
	ib_ferma_calcolo = true
	as_error = "Formula con codice "+as_cod_formula+" non trovata! "+sqlca.sqlerrtext
	return ERRORE_FORMULA
end if

ls_sql = uof_variabili(ls_sql, as_cod_formula, as_error)

if g_str.isempty(ls_sql) then
	as_error = "L'sql della formula " + as_cod_formula + " sembra essere vuoto."
	return ERRORE_FORMULA
else
	return ls_sql
end if

end function

public function string uof_esegui_funzione (string as_funzione, ref string as_errore);/**
 * stefanop
 * 17/05/2012
 *
 * Esegui le formule oggetti; ovverro la formula viene calcolata eseguendo una determinata funzione di un oggetto che va a priori
 * calcolata.
 *
 * Esempio di una dichiarazione di funzione: uo_oggetto.uof_funzione(#S:parametro_1#, #N:parametro_2)
 **/

string ls_obj, ls_return, ls_valori[]
int li_pos, li_return
long ll_valori[]
decimal ldc_valori[]
date ldd_valori[]
datetime ldt_valori[]
uo_impresa luo_impresa
uo_excel luo_excel
uo_richieste_stat luo_richieste_stat
uo_magazzino luo_magazzino

li_pos = pos(as_funzione, "(")

if li_pos > 0 then
	ls_obj = mid(as_funzione, 1, li_pos - 1)
end if

//se ci sono variabili sostituisco
ls_return = uof_variabili(as_funzione, as_funzione, as_errore)
if ls_return = ERRORE_FORMULA then return ls_return

if ib_lista_variabili then return as_funzione

setnull(ls_return)

choose case ls_obj
	
	case "Indicatore_MI5", "Indicatore_MI6", "Indicatore_MI7"
		luo_richieste_stat = create uo_richieste_stat
		
		ls_valori[1] = uof_get_parametro_funzione("#S:impianto#")
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_dal#")))
		ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_al#")))
		
		if ls_obj = "Indicatore_MI5" then
		
			li_return = luo_richieste_stat.uof_mi5(ls_valori[1], ldt_valori[1], ldt_valori[2], ref idc_internal_result, ref as_errore) 
			
		elseif  ls_obj = "Indicatore_MI6" then
			li_return = luo_richieste_stat.uof_mi6(ls_valori[1], ldt_valori[1], ldt_valori[2], ref idc_internal_result, ref as_errore)
			
		elseif  ls_obj = "Indicatore_MI7" then
			li_return = luo_richieste_stat.uof_mi7(ls_valori[1], ldt_valori[1], ldt_valori[2], ref idc_internal_result, ref as_errore)
		end if
		
		if li_return < 0 then
			destroy luo_richieste_stat
			return ERRORE_FORMULA
		end if
		
		destroy luo_richieste_stat
	
	
	case "uo_impresa.uof_saldo_conto"
		luo_impresa = create uo_impresa
		
		ls_valori[1] = uof_get_parametro_funzione("#S:cod_conto#")
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_saldo#")))
		
		if luo_impresa.uof_saldo_conto(ls_valori[1], ldt_valori[1], ref idc_internal_result, ref as_errore) < 0 then
			destroy luo_impresa
			return ERRORE_FORMULA
		end if
		
		destroy luo_impresa
		
		
	case "uo_impresa.uof_saldo_conto_periodo"
		luo_impresa = create uo_impresa
		
		ls_valori[1] = uof_get_parametro_funzione("#S:cod_conto#")
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_inizio#")))
		ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_fine#")))
		
		if luo_impresa.uof_saldo_conto_periodo(ls_valori[1], ldt_valori[1], ldt_valori[2], ref idc_internal_result, ref as_errore) < 0 then
			destroy luo_impresa
			return ERRORE_FORMULA
		end if
		
		destroy luo_impresa
		
		
	case "uo_impresa.uof_saldo_conto_cc_periodo"
		luo_impresa = create uo_impresa
		
		ls_valori[1] = uof_get_parametro_funzione("#S:cod_conto#")
		ls_valori[2] = uof_get_parametro_funzione("#S:cod_piano_centro_costo#")
		ls_valori[3] = uof_get_parametro_funzione("#S:cod_centro_costo#")
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_inizio#")))
		ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_fine#")))
		
		if luo_impresa.uof_saldo_conto_cc_periodo(ls_valori[1], ls_valori[2], ls_valori[3], ldt_valori[1], ldt_valori[2],ref idc_internal_result, ref as_errore) < 0 then
			destroy luo_impresa
			return ERRORE_FORMULA
		end if
		
		destroy luo_impresa
		
	case "uo_impresa.uof_saldo_conto_centro_costo"
		luo_impresa = create uo_impresa
		
		ls_valori[1] = uof_get_parametro_funzione("#S:cod_conto#")
		ls_valori[2] = uof_get_parametro_funzione("#S:cod_piano_centro_costo#")
		ls_valori[3] = uof_get_parametro_funzione("#S:cod_centro_costo#")
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_saldo#")))
		
		if luo_impresa.uof_saldo_conto_centro_costo(ls_valori[1], ls_valori[2], ls_valori[3], ldt_valori[1], ref idc_internal_result, ref as_errore) < 0 then
			destroy luo_impresa
			return ERRORE_FORMULA
		end if
		
		destroy luo_impresa
		
	case "uo_excel.uof_read"
		luo_excel = create uo_excel
		
		ls_valori[1] = uof_get_parametro_funzione("#S:file_excel#")
		ls_valori[2] = uof_get_parametro_funzione("#S:worksheet#")
		ls_valori[3] = uof_get_parametro_funzione("#S:colonna_excel#")
		ls_valori[4] = uof_get_parametro_funzione("#N:riga_excel#")
		guo_functions.uof_replace_string(ls_valori[4], ".", ",")
		ldc_valori[1] = dec(ls_valori[4])
		
		try
			if not fileexists(ls_valori[1]) then
				as_errore = "Il file excel non è stato trovato nel percorso: " + ls_valori[1]
				return ERRORE_FORMULA
			end if
			
			if ldc_valori[1] < 1 then 
				as_errore = "Il numero di riga non può essere inferiore ad 1"
				return ERRORE_FORMULA
			end if
			
			if isnumber(ls_valori[3]) then
				as_errore = "Attenzione, per leggere un valore da un foglio excel è necessario indicare " &
							 + "la colonna tramite la lettera e non il numero."
				return ERRORE_FORMULA
			end if
		
			luo_excel.uof_open(ls_valori[1], false, true)
			
			// Seleziono il foglio solo se indicato, altrimenti vado nel primo di default
			if g_str.isnotempty(ls_valori[2]) then
				if not luo_excel.uof_set_sheet(ls_valori[2]) then
					as_errore = "Il foglio di lavoro " + ls_valori[2] + " non è presente all'interno del file " + ls_valori[1]
					return ERRORE_FORMULA
				end if
			end if
			
			idc_internal_result = luo_excel.uof_read(ldc_valori[1], ls_valori[3])
			
		catch(Exception e)
			as_errore = "Errore durante la lettura del file excel.~r~n" + g_str.safe(e.getmessage())
		finally	
			destroy luo_excel
		end try
		
	case "uo_excel.uof_somma_mese_orizzontale"
		luo_excel = create uo_excel
		int li_month_start, li_month_end, li_i, li_col_start
		decimal{4} ldc_sum
		any la_valore
		
		ls_valori[1] = uof_get_parametro_funzione("#S:file_excel#")
		ls_valori[2] = uof_get_parametro_funzione("#S:worksheet#")
		ls_valori[3] = uof_get_parametro_funzione("#S:colonna_inizio_excel#")
		ls_valori[4] = uof_get_parametro_funzione("#N:riga_excel#")
		guo_functions.uof_replace_string(ls_valori[4], ".", ",")
		ldc_valori[1] = dec(ls_valori[4])
		ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_saldo_da#")))
		ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_saldo_a#")))
		
		ldc_sum = 0

		if not fileexists(ls_valori[1]) then
			as_errore = "Il file excel non è stato trovato nel percorso: " + ls_valori[1]
			return ERRORE_FORMULA
		end if
		
		if ldc_valori[1] < 1 then 
			as_errore = "Il numero di riga non può essere inferiore ad 1"
			return ERRORE_FORMULA
		end if
		
		luo_excel.uof_open(ls_valori[1], false, true)
		
		if not luo_excel.uof_set_sheet(ls_valori[2]) then
			as_errore = "Il foglio di lavoro " + ls_valori[2] + " non è presente all'interno del file " + ls_valori[1]
			return ERRORE_FORMULA
		end if
		
		li_month_start = month(date(ldt_valori[1])) - 1
		li_month_end = month(date(ldt_valori[2])) - 1
		li_col_start = luo_excel.uof_get_num_column(ls_valori[3])
		
		for li_i = li_month_start to li_month_end
			
			la_valore = luo_excel.uof_read(ldc_valori[1], luo_excel.uof_nome_cella(li_col_start + li_i))
			
			if not isnull(la_valore) then ldc_sum += la_valore
			
		next
		
		idc_internal_result = ldc_sum
		destroy luo_excel
		
	case "uo_impresa.uof_saldo_partite_aperte"
		luo_impresa = create uo_impresa
		
		try
			ls_valori[1] = uof_get_parametro_funzione("#S:tipo_conto#")
			ls_valori[2] = uof_get_parametro_funzione("#S:cod_cli_for#")
			ls_valori[3] = uof_get_parametro_funzione("#S:flag_ita_ue_extraue#")
			ldd_valori[1] = date(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_riferimento#")))
			idc_internal_result = luo_impresa.uof_saldo_partite_aperte(ls_valori[1], ls_valori[2], ls_valori[3], ldd_valori[1])
			
		catch(RuntimeError e2)
			as_errore = "Errore durante la chiamata alla funzione uo_impresa.uof_saldo_partite_aperte.~r~n" + g_str.safe(e2.getmessage())
			idc_internal_result = -1
			return ERRORE_FORMULA
		finally
			destroy luo_impresa
		end try
		
	case "uo_impresa.uof_top_credit"
		luo_impresa = create uo_impresa
		
		try
			ll_valori[1] = Long(uof_get_parametro_funzione("#N:num_elementi#"))
			ls_valori[1] = uof_get_parametro_funzione("#S:flag_tipo_scadenza#")
			ls_valori[2] = uof_get_parametro_funzione("#S:flag_italia_estero_ue#")
			ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_riferimento#")))
			if luo_impresa.uof_top_credit(ll_valori[1], ls_valori[1], ls_valori[2], ldt_valori[1], ids_internal_store, as_errore) < 0 then
				destroy ids_internal_store
				return ERRORE_FORMULA
			end if
			
		catch(RuntimeError e4)
			as_errore = "Errore durante la chiamata alla funzione uo_impresa.uof_top_credit.~r~n" + g_str.safe(e4.getmessage())
			idc_internal_result = -1
			return ERRORE_FORMULA
		finally
			destroy luo_impresa
		end try		

	case "uo_magazzino.uof_costo_impegnato"
		luo_magazzino = create uo_magazzino
		luo_magazzino.ib_log = ib_log
		
		try
			ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_da#")))
			ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_a#")))
			ls_valori[1] = uof_get_parametro_funzione("#S:flag_aperti_evasi#")
			if luo_magazzino.uof_costo_impegnato_ordini(ldt_valori[1], ldt_valori[2], ls_valori[1], idc_internal_result,as_errore) < 0 then
				return ERRORE_FORMULA
			end if
			
		catch(RuntimeError e1)
			as_errore = "Errore durante la chiamata alla funzione uo_magazzino.uof_costo_impegnato.~r~n" + g_str.safe(e1.getmessage())
			idc_internal_result = -1
			return ERRORE_FORMULA
		finally
			destroy luo_magazzino
		end try
		
		
	case "uo_magazzino.uof_inventario_magazzino"
		luo_magazzino = create uo_magazzino
		
		try
			ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_riferimento#")))
			ls_valori[1] = uof_get_parametro_funzione("#S:cod_prodotto_da#")
			ls_valori[2] = uof_get_parametro_funzione("#S:cod_prodotto_a#")
			ls_valori[3] = uof_get_parametro_funzione("#S:cod_deposito#")
			ls_valori[4] = uof_get_parametro_funzione("#S:flag_fiscale#")
			ls_valori[5] = uof_get_parametro_funzione("#S:flag_lifo#")
			ls_valori[6] = uof_get_parametro_funzione("#S:tipo_valorizzazione#")
			
			if luo_magazzino.uof_inventario_magazzino(ldt_valori[1], ls_valori[1], ls_valori[2], ls_valori[3], ls_valori[4], ls_valori[5], ls_valori[6],  idc_internal_result, as_errore) < 0 then
				return ERRORE_FORMULA
			end if
			
		catch(RuntimeError e3)
			as_errore = "Errore durante la chiamata alla funzione uo_magazzino.uof_inventario_magazzino.~r~n" + g_str.safe(e3.getmessage())
			idc_internal_result = -1
			return ERRORE_FORMULA
		finally
			destroy luo_magazzino
		end try
				
		
	case "uo_magazzino.uof_costo_fatturato"
		luo_magazzino = create uo_magazzino
		luo_magazzino.ib_log = ib_log
		
		try
			ldt_valori[1] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_da#")))
			ldt_valori[2] = datetime(guo_functions.uof_string_to_date(uof_get_parametro_funzione("#D:data_a#")))
			if luo_magazzino.uof_costo_fatturato(ldt_valori[1], ldt_valori[2], idc_internal_result,as_errore) < 0 then
				return ERRORE_FORMULA
			end if
			
		catch(RuntimeError e5)
			as_errore = "Errore durante la chiamata alla funzione uo_magazzino.uof_costo_fatturato.~r~n" + g_str.safe(e5.getmessage())
			idc_internal_result = -1
			return ERRORE_FORMULA
		finally
			destroy luo_magazzino
		end try
		
		
	case else
		as_errore = "La funzione " + ls_obj + " non è codificata all'interno del software"
		return ERRORE_FORMULA
		
end choose

return "FORMULA_@K"
end function

public function integer uof_calcola_formula_dtaglio_old (string fs_testo_formula, boolean fb_test, decimal fd_quan_utilizzo, decimal fd_dim_x, decimal fd_dim_y, decimal fd_dim_z, decimal fd_dim_t, string fs_cod_tessuto, decimal fd_alt_mantovana, decimal fd_quan_ordine, string fs_luce_finita, ref decimal fdc_risultato, ref string fs_errore);string					ls_variabili[], ls_variabili_copy[], ls_testo_formula_originale, ls_flag_tipo_dato, ls_val_variabile, ls_variabili_originali[], ls_temp, ls_testo_formula_2
						
long					ll_tot_variables, ll_index, ll_k

integer				li_ret


ls_testo_formula_originale = fs_testo_formula

if fs_testo_formula='' or isnull(fs_testo_formula) or fs_testo_formula="." then
	fdc_risultato = 0
	return 0
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratteri che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI MEDIANTE ESPRESSIONI REGOLARI SOLO IN CASO DI TEST
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

if fb_test then

	ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
	if ll_tot_variables<0 then
		return -1
	end if

	ll_k = 0
	//togli quelle che non sono variabili previste
	for ll_index=1 to ll_tot_variables
		choose case lower(ls_variabili[ll_index])
			case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine","tessuto","luce_finita"
				ll_k += 1
				ls_variabili_copy[ll_k] = ls_variabili[ll_index]
				ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				
			case else
				li_ret = uof_check_variabile(ls_variabili[ll_index], ls_temp)
				if li_ret=0 then
					ll_k += 1
					ls_variabili_copy[ll_k] = ls_temp
					ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				end if

		end choose
	next

	//---------------------------------------------------------------------------------------------------------------------------------------
	//SOSTITUZIONE VARIABILI E RELATIVO VALORE
	if upperbound(ls_variabili_copy)>0 then
		//ci sono variabili da definire ...
		//modifica il testo formula per essere riconosciuto
		//cicla l'array delle variabili
		for ll_index=1 to upperbound(ls_variabili_copy)
			
			choose case lower(ls_variabili_copy[ll_index])
				case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine"
					ls_flag_tipo_dato = "N"
					//es: h_mantovana   ->   #N:h_mantovana#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case "tessuto","luce_finita"
					ls_flag_tipo_dato = "S"
					//es: h_mantovana   ->   #S:tessuto#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case else
					//sostituzione struttura variabile già effettuata, la metto cosi com'è nella formula
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_index], ls_variabili_copy[ll_index])
					//guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_k], ls_variabili_copy[ll_index])
					
			end choose
			
		next
	end if

	//IN MODALITA' TEST VUOL DIRE CHE STAI TESTANDO UNA FORMULA
	//QUINDI VIENE CHIAMATA UNA FUNZIONE CHE APRE UNA FINESTRA CHE RICHIEDE ALL'UTENTE DI IMMETTERE I VALORIO DELLE VARIABILI EVENTUALI
	//VISUALIZZANDO SUBITO IL RELATIVO RISULTATO

	//chiedi i valori per ogni variabile
	//ls_testo_formula = uof_variabili(ls_testo_formula, fs_cod_formula, fs_errore)
	fs_testo_formula = uof_variabili_cumulativo(fs_testo_formula, ls_testo_formula_originale, "", fdc_risultato, fs_errore)
	if fs_testo_formula = ERRORE_FORMULA then
		ib_ferma_calcolo = true
		
		return -1
	end if
	
	//ora da qui puoi tranquillamente uscire, quando rientrerai nella funzione chiamante ...
	return 1
end if

//#####################################################################################################
//QUESTA PARTE RIMANE PER COMPATIBILITA'
//#####################################################################################################

//se arrivi fin qui non è un test, quindi esegui sostituzione dei valori
//se numerico fai prima in string e poi metti il punto al posto della virgola !!!!!!

ls_val_variabile = string(fd_quan_utilizzo, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_utilizzo", 		ls_val_variabile)

ls_val_variabile = string(fd_dim_x, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "x", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_y, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "y", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_z, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "z", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_t, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "dim_t", 				ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "tessuto", 				"~'"+fs_cod_tessuto+"~'")

ls_val_variabile = string(fd_alt_mantovana, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "h_mantovana", 		ls_val_variabile)

ls_val_variabile = string(fd_quan_ordine, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_ordine", 		ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "luce_finita", 			"~'"+fs_luce_finita+"~'")

//#####################################################################################################


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


ls_testo_formula_2 = fs_testo_formula
//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(ls_variabili[], fs_testo_formula, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if




//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if uof_eval_with_datastore(fs_testo_formula, fdc_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

public function integer uof_calcola_formula_dtaglio (string fs_testo_formula, boolean fb_test, long al_anno_ordine, long al_num_ordine, long al_prog_riga_ord_ven, ref decimal ad_risultato, ref string fs_errore);string					ls_variabili[], ls_variabili_copy[], ls_testo_formula_originale, ls_flag_tipo_dato, ls_val_variabile, ls_variabili_originali[], ls_temp, ls_testo_formula_2, ls_sql, ls_cod_variabile
						
long					ll_tot_variables, ll_index, ll_k, ll_ret, ll_i

integer				li_ret

datastore 			lds_datastore

ls_testo_formula_originale = fs_testo_formula

if fs_testo_formula='' or isnull(fs_testo_formula) or fs_testo_formula="." then
	ad_risultato = 0
	return 0
end if

//---------------------------------------------------------------------------------------------------------------------------------------
//MANIPOLAZIONE FORMULA PRE-CALCOLO (sostituzione di alcuni caratteri che potrebbero dare fastidio (. con , nei decimali, SQRT con SQR, ecc...))
uof_pre_calcolo_formula_db(fs_testo_formula)


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI MEDIANTE ESPRESSIONI REGOLARI SOLO IN CASO DI TEST
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

if fb_test then

	ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
	if ll_tot_variables<0 then
		return -1
	end if

	ll_k = 0
	//togli quelle che non sono variabili previste
	for ll_index=1 to ll_tot_variables
		choose case lower(ls_variabili[ll_index])
			case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine","tessuto","luce_finita"
				ll_k += 1
				ls_variabili_copy[ll_k] = ls_variabili[ll_index]
				ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				
			case else
				li_ret = uof_check_variabile(ls_variabili[ll_index], ls_temp)
				if li_ret=0 then
					ll_k += 1
					ls_variabili_copy[ll_k] = ls_temp
					ls_variabili_originali[ll_k] = ls_variabili[ll_index]
				end if

		end choose
	next

	//---------------------------------------------------------------------------------------------------------------------------------------
	//SOSTITUZIONE VARIABILI E RELATIVO VALORE
	if upperbound(ls_variabili_copy)>0 then
		//ci sono variabili da definire ...
		//modifica il testo formula per essere riconosciuto
		//cicla l'array delle variabili
		for ll_index=1 to upperbound(ls_variabili_copy)
			
			choose case lower(ls_variabili_copy[ll_index])
				case "quan_utilizzo", "x", "y","z","dim_t","h_mantovana","quan_ordine"
					ls_flag_tipo_dato = "N"
					//es: h_mantovana   ->   #N:h_mantovana#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case "tessuto","luce_finita"
					ls_flag_tipo_dato = "S"
					//es: h_mantovana   ->   #S:tessuto#
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_copy[ll_index], "#"+ls_flag_tipo_dato+":"+ls_variabili_copy[ll_index]+"#")
					
				case else
					//sostituzione struttura variabile già effettuata, la metto cosi com'è nella formula
					guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_index], ls_variabili_copy[ll_index])
					//guo_functions.uof_replace_string(fs_testo_formula, ls_variabili_originali[ll_k], ls_variabili_copy[ll_index])
					
			end choose
			
		next
	end if

	//IN MODALITA' TEST VUOL DIRE CHE STAI TESTANDO UNA FORMULA
	//QUINDI VIENE CHIAMATA UNA FUNZIONE CHE APRE UNA FINESTRA CHE RICHIEDE ALL'UTENTE DI IMMETTERE I VALORIO DELLE VARIABILI EVENTUALI
	//VISUALIZZANDO SUBITO IL RELATIVO RISULTATO

	//chiedi i valori per ogni variabile
	//ls_testo_formula = uof_variabili(ls_testo_formula, fs_cod_formula, fs_errore)
	fs_testo_formula = uof_variabili_cumulativo(fs_testo_formula, ls_testo_formula_originale, "", ad_risultato, fs_errore)
	if fs_testo_formula = ERRORE_FORMULA then
		ib_ferma_calcolo = true
		
		return -1
	end if
	
	//ora da qui puoi tranquillamente uscire, quando rientrerai nella funzione chiamante ...
	return 1
end if

//#####################################################################################################
//QUESTA PARTE RIMANE PER COMPATIBILITA'
//#####################################################################################################

//se arrivi fin qui non è un test, quindi esegui sostituzione dei valori
//se numerico fai prima in string e poi metti il punto al posto della virgola !!!!!!
ls_sql = g_str.format("SELECT cod_variabile,  flag_tipo_dato, valore_stringa, valore_numero, valore_datetime FROM dbo.det_ord_ven_conf_variabili  WHERE  cod_azienda = '$1' AND  anno_registrazione = $2 AND  num_registrazione = $3 AND  prog_riga_ord_ven = $4ORDER BY prog_variabile ASC  ",s_cs_xx.cod_azienda, al_anno_ordine, al_num_ordine, al_prog_riga_ord_ven)

ll_ret = guo_functions.uof_crea_datastore( lds_datastore, ls_sql)

for ll_i = 1 to ll_ret
	ls_cod_variabile =  lds_datastore.getitemstring(ll_i, 1)
	
	choose case lds_datastore.getitemstring(ll_i, 2)
		case "S"
			guo_functions.uof_replace_string(fs_testo_formula, ls_cod_variabile, "~'"+lds_datastore.getitemstring(ll_i, 3)+"~'")
			
		case "N"
			ls_val_variabile = string( lds_datastore.getitemnumber(ll_i, 4), "########0.0000")
			guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
			guo_functions.uof_replace_string(fs_testo_formula, ls_cod_variabile, ls_val_variabile)
			
		case "D"
			guo_functions.uof_replace_string(fs_testo_formula, ls_cod_variabile, "~'"+string(lds_datastore.getitemdatetime(ll_i, 5),s_cs_xx.db_funzioni.formato_data)+"~'")
	end choose
next

destroy lds_datastore
/*



ls_val_variabile = string(fd_quan_utilizzo, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_utilizzo", 		ls_val_variabile)

ls_val_variabile = string(fd_dim_x, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "x", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_y, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "y", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_z, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "z", 						ls_val_variabile)

ls_val_variabile = string(fd_dim_t, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "dim_t", 				ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "tessuto", 				"~'"+fs_cod_tessuto+"~'")

ls_val_variabile = string(fd_alt_mantovana, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "h_mantovana", 		ls_val_variabile)

ls_val_variabile = string(fd_quan_ordine, "########0.0000")
guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
guo_functions.uof_replace_string(fs_testo_formula, "quan_ordine", 		ls_val_variabile)

guo_functions.uof_replace_string(fs_testo_formula, "luce_finita", 			"~'"+fs_luce_finita+"~'")
*/
//#####################################################################################################


//---------------------------------------------------------------------------------------------------------------------------------------
//ESTRAZIONE EVENTUALI VARIABILI E RELATIVO VALOREMEDIANTE ESPRESSIONI REGOLARI
/*
	- le variabili non devono iniziare per numeri (0-9), per "+", per ",", per "-", per "*", per "(", per ")", per "/", per "^"
	- le variabili devono essere tipo WORD
	
	Per modificare tale pattern fare uso della costante PATTERN_VARIABLES, definita in questo oggetto
*/

ll_tot_variables = uof_estrai_stringhe_espress_reg(fs_testo_formula, ls_variabili[], fs_errore)
if ll_tot_variables<0 then
	return -1
end if


ls_testo_formula_2 = fs_testo_formula
//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DEL RELATIVO VALORE per CIASCUNA VARIABILE (dopo aver verificato che sia una variabile codificata in tab_variabili_formule)
li_ret = uof_setvariables_formule_db(ls_variabili[], fs_testo_formula, fs_errore)
if li_ret < 0 then
	return -1
	
elseif li_ret=1 then
	//eri in modalità testing, quindi non server + proseguire per il risultato finale
	return 0
	
end if




//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME
if uof_eval_with_datastore(fs_testo_formula, ad_risultato, fs_errore) < 0 then
	return -1
end if

return 0
end function

on uo_formule_calcolo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_formule_calcolo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_cache = create uo_array 

end event

event destructor;destroy iuo_cache;

end event

