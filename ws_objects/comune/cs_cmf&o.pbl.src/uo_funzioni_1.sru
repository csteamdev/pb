﻿$PBExportHeader$uo_funzioni_1.sru
$PBExportComments$User object di funzioni
forward
global type uo_funzioni_1 from nonvisualobject
end type
end forward

global type uo_funzioni_1 from nonvisualobject
end type
global uo_funzioni_1 uo_funzioni_1

type variables
uo_magazzino luo_magazzino
boolean		 ib_flag_non_collegati

// Variabili create per Gibus 14-02-2012
boolean 	ib_forza_deposito_scarico_mp=false
string		is_cod_deposito_scarico_mp

//Donato 09/07/2012
//se valorizzato la data movimenti in avanzamento commessa prenderà tale data
//utile nel processo di ricostruzione movimenti di una commessa
datetime idt_data_creazione_mov
string is_ddt_trasf_multipli_log = ""

// stefanop 16/07/2012
// Liste di distribuzione da usare per l'invio email all'interno dell'oggetto
// Al momento viene usato per inviare le mail in caso di mancato DDT di trasferimento
// durante la chiusura di una commessa
string is_cod_tipo_lista_dist = "COMDDT"
string is_cod_lista_dist = "001"

//Donato 25/07/2012 -------------------------------------------------------------------------------------------------------------
//Variabile normalmente a FALSE. 
//Si attiva a TRUE quando si vuole chiudere una commessa legata a:
//		un ddt di tasferimento interno di un semilavorato (in realtà si tratta di un kit di materie prime non assemblate)
//		con tipo dettaglio pari a TRI
//		su un prodotto con cod_tipo_politica_riordino='002'
//		riferimento anno_registrazione_ord_ven/num_registrazione_ord_ven/prog_riga_ord_ven NULLO
//I movimenti da effettuare sono 		gli scarichi SDE delle materie prime in distinta dal deposito di partenza del ddt
//												i carichi CDE delle materie prime in distinta dal deposito di arrivo del ddt
boolean ib_trasf_interno = false
string is_cod_deposito_CARICO_mp = ""
//--------------------------------------------------------------------------------------------------------------------------------------

//Donato 03/08/2012
//variabile normalmente NULL (nel create object)
//conviene valorizzarla quando si vuole chiamare la funzione uof_genera_commesse e la riga ordine è stata evasa
//es. sistemazioni di magazzino fatte per GIBUS in data di queso commento, 3/8/2012
//infatti viene calcolata una quantità in base alla seguente operazione
//lettura quantità varie dalla det_ord_ven e poi   ld_quan_ordine = (ld_quan_ordine - ld_quan_in_evasione) - ld_quan_evasa
//quindi se la riga è evasa risulterà alla fine ld_quan_ordine=0 e questo poi sballa l'impegnato delle MP
decimal id_quan_ordine
//-------------------------------------------------------------------------------------------------------------------------------------

//Donato 26/06/2014
//se si tratta della chiusura di un ramo semilavorato non è detto che la quantità del semilavorato coincida sempre con la quantita ordinata
//magari per GIBUS è vero (1 struttura X + 1 telo Y fa 1 tenda) ma non è detto per gli altri clienti
//quindi durante l'estrazione delle materie prime, se si tratta di avanzamento di semilavorato, attiva la variabile booleana ib_quan_semilavorato
//e memorizza in id_quan_semilavorato la quantità da caricare del semilavorato (dopo il passaggio le due variabile sono resettate)
decimal id_quan_semilavorato
boolean ib_quan_semilavorato = false

//serve per dare la possibilità, nel caso di avanzamento del semilavorato della commessa a dare la possibilità
//di scaricare le materie prime da un deposito e caricare il semilavorato in un altro deposito
boolean 	ib_forza_deposito_carico_sl=false
string		is_cod_deposito_carico_sl
//-------------------------------------------------------------------------------------------------------------------------------------


// Enrico 14/09/2012
// Variabili usate  solo nella funzione uof_valorizza_formule
// Se non sono valorizzate non succede nulla
dec{4} id_dim_x, id_dim_y


boolean		ib_crea_variante_commessa_sl = false

//se TRUE la funzione uof_avanza_commessa non effettua la chiusura, ma semplicemente la pianifica in buffer_chiusura_commesse
private:
	boolean	ib_posticipa = false
	boolean	ib_pianificato = false
	boolean	ib_non_considerare_flag_parziale = false
	
	boolean	ib_from_ddt_trasf = false
	string		is_dep_from = ""
	string		is_dep_to = ""
	integer	ii_anno_comm_ddttrasf = 0
	long		il_num_comm_ddttrasf = 0



end variables

forward prototypes
public function integer uof_genera_commesse (string fs_cod_tipo_ord_ven, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, long fl_num_commessa_forzato, ref integer fi_anno_commessa, ref long fl_num_commessa, ref string fs_errore)
public function integer uof_cancella_commessa (long fl_anno_commessa, long fl_num_commessa, ref string fs_messaggio)
public function integer uof_set_flag_non_collegati (boolean ab_flag_non_collegati)
private function boolean uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, boolean ab_controlla_flag_stampa)
public function integer uof_trova_reparti_cal (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, string fs_cod_deposito_origine_ordine, ref string fs_cod_reparto[], ref string fs_errore)
public function integer uof_reparti_stabilimenti_cal (long fi_anno_registrazione, long fl_num_registrazione, string fs_cod_prodotto, string fs_cod_versione, string fs_cod_reparto_origine, string fs_cod_deposito_origine_fase, string fs_flag_stampa_sempre, string fs_cod_deposito_origine_ordine, ref string fs_reparti_letti[], ref string fs_errore)
public function integer uof_trova_reparti (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_errore)
public function integer uof_trova_reparti_sfuso (string as_cod_prodotto, string as_cod_deposito_origine, string as_cod_deposito_produzione, ref string as_cod_reparti_produzione[], ref string as_messaggio)
public function integer uof_avanza_commessa (long al_anno_commessa, long al_num_commessa, string fs_cod_semilavorato, ref string as_errore)
public function integer uof_trova_mat_prime_varianti (string prodotto_finito, string cod_versione, ref string fs_cod_deposito_produzione, ref string materia_prima[], ref string versione_prima[], ref double quantita_utilizzo[], double quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, string fs_cod_semilavorato, ref string fs_errore)
private function integer uof_fasi_lavorazione_det (long al_anno_documento, long al_num_documento, long al_prog_riga, string as_cod_prodotto_padre, string as_cod_versione_padre, long al_num_sequenza, string as_cod_prodotto_figlio, string as_cod_versione_figlio, string as_tabella_varianti, ref string as_cod_deposito_origine[], ref string as_cod_deposito_produzione[], ref string as_cod_reparto_produzione[], ref string as_messaggio)
public function integer uof_estrai_reparto_stabilimento (string fs_cod_prodotto_padre, string fs_cod_versione_padre, string fs_tipo_gestione, long fl_anno_ordine, long fl_num_ordine, ref string fs_cod_deposito_produzione, ref string fs_cod_reparto_produzione[], ref string fs_errore)
public function integer uof_gen_varianti_deposito_ord_ven (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore)
public function integer uof_gen_varianti_deposito_commesse (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore)
private function integer uof_fasi_lavorazione_tes (long al_anno_documento, long al_num_documento, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, long al_num_sequenza, string as_tabella_varianti, ref datastore ads_store, ref string as_messaggio)
private function integer uof_trova_fasi_lavorazioni (ref long ordinamento_assoluto, string prodotto_finito, string cod_versione, ref string materia_prima[], ref string versione_prima[], ref decimal quantita_utilizzo[], decimal quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref datastore ads_store, ref string fs_errore)
public function integer uof_trova_fasi_lavorazioni (string prodotto_finito, string cod_versione, decimal quan_ordinata, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref datastore ads_store, ref string fs_errore)
public function integer uof_get_varianti_commesse_allinea_comm (integer fi_anno_commessa, long fl_num_commessa, ref string fs_cod_dep_prod[], ref string fs_cod_sl[], ref string fs_errore)
public function integer uof_riallinea_mov_commessa (integer fi_anno_commessa, long fl_num_commessa, boolean fb_solo_trasferimenti, ref string fs_errore)
public function integer uof_get_commesse_righe_collegate (integer fi_anno_com_principale, long fl_num_com_principale, ref integer fi_anno_com_opt[], ref long fl_num_com_opt[], ref string fs_errore)
public function integer uof_get_data_ddt_trasf (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_deposito_partenza, string fs_cod_deposito_arrivo, ref datetime fdt_data_movimenti, ref string fs_errore)
public function integer uof_riapri_commessa (integer fi_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer uof_se_commessa_con_trasf (integer fi_anno_commessa, long fl_num_commessa, string fs_dep_prod_comm[], ref string fs_errore)
public function integer uof_get_ddt_trasf_duplicati (integer fi_anno_commessa, long fl_num_commessa, ref string fs_errore)
private function integer uof_cancella_commessa_ddt_trasferimento (long al_anno_commessa, long al_num_commessa, ref string as_error)
private function integer uof_controlla_fasi_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore)
public function integer uof_commessa_con_ddt_trasferimento (long al_anno_commessa, long al_num_commessa, ref string as_depositi[])
public function integer uof_trova_depositi_commessa (long al_anno_commessa, long al_num_commessa, ref string as_depositi[])
public function integer uof_check_integrita_ddt_trasf_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore, boolean ab_send_email)
public function integer uof_trova_deposito_chiusura_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_cod_dep_chiusura, ref string as_errore)
public function integer uof_genera_commesse_ddt (long fl_anno_bolla, long fl_num_bolla, long fl_prog_riga_bol_ven, ref integer fi_anno_commessa, ref long fl_num_commessa, ref string fs_errore)
public function integer uof_seleziona_stock_arrivo (string fs_materia_prima[], double fdd_quantita_utilizzo[], string fs_flag_mag_negativo, ref s_stock_produzione fs_stock_prod_dest[], ref string fs_errore)
public function boolean uof_is_ddt_trasferimento (integer fi_anno_bol_ven, long fl_num_bol_ven, long fl_prog_riga_bol_ven, ref integer fi_anno_commessa, ref long fl_num_commessa)
public function integer uof_trova_reparti_e_semilavorati (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_semilavorato[], ref string fs_deposito[], ref string fs_errore)
public function integer uof_deposito_e_semilavorato (string fs_cod_reparto, string fs_cod_semilav, ref string fs_cod_deposito[], ref string fs_cod_semilavorato[])
public function integer uof_get_depositi_prod_allinea_comm (integer fi_anno_commessa, long fl_num_commessa, ref string fs_cod_dep_prod[], ref string fs_cod_sl[], ref string fs_errore)
public function boolean uof_check_dep_prod_succ_var_com (integer fi_indice, string fs_cod_dep_prod_var_com[], string fs_cod_dep_prod, ref string fs_cod_dep_succ)
public function integer uof_valorizza_formule_distinta (string fs_cod_prodotto, string fs_cod_versione, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, string fs_tabella_varianti, ref string fs_errore)
public function integer uof_calcola_peso_riga_ordine (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref decimal ad_peso_unitario, ref string as_errore)
public function integer uof_calcola_peso_testata_ordine (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore)
public subroutine uof_log_sistema (string as_testo, string as_tipo_log)
public subroutine uof_set_posticipato (boolean ab_posticipa)
public subroutine uof_set_pianificato (boolean ab_pianificato)
public function integer uof_pianifica (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato)
public function integer uof_trova_mat_prime_varianti (string prodotto_finito, string cod_versione, long al_num_sequenza, ref string fs_cod_deposito_produzione, ref s_trova_mp_varianti fstr_trova_mp_varianti[], decimal quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, string fs_cod_semilavorato, ref string fs_errore)
public function integer uof_avan_prod_com (boolean ab_prima_volta, integer ai_anno_commessa, long al_num_commessa, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, double ad_quan_in_produzione, integer ai_num_livello_cor, string as_cod_tipo_commessa, ref string as_errore)
public function integer uof_trova_mat_prime_varianti (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, boolean ab_esegui_rollback, ref string fs_materia_prima[], ref string fs_versione_prima[], ref double add_quantita_utilizzo[], ref string as_cod_deposito_prelievo, ref string as_errore)
public function integer uof_scarico_mp_in_conferma_doc (integer ai_anno, long al_numero, string as_tipo_doc, boolean ab_chiedi, ref decimal ad_valore_mp, ref string as_errore)
public function integer uof_get_costo_mp (datetime adt_data_rif, string as_cod_prodotto, string as_cod_fornitore, ref decimal ad_valore, ref string as_errore)
public function integer uof_invia_mp_terzista (integer ai_anno_ddt, long al_num_ddt, ref long al_tot_righe, ref string as_errore)
public subroutine uof_set_nonconsidera_flagparziale (boolean ab_non_considerare_flag_parziale)
public subroutine uof_set_from_ddt_trasf (boolean ab_from_ddt_trasf, string as_deposito_from, string as_deposito_to, integer ai_anno_commessa, long al_num_commessa)
public function integer uof_annulla_attivazione_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore)
public function integer uof_ricerca_quan_mp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_prodotto, string fs_cod_gruppo_variante, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_registrazione, string fs_tab_varianti, ref string fs_cod_materia_prima, ref decimal fd_quan_materia_prima, ref string fs_errore)
public function integer uof_giacenze_stock_prodotto_deposito (string as_cod_prodotto, string as_cod_deposito, datetime adt_data_riferimento_stock, ref datastore ads_data)
public function integer uof_chiudi_commessa_fasi (long al_anno_commessa, long al_num_commessa, string as_reparto_riferimento, boolean ab_forza_chiusura, string as_log, ref string as_errore)
end prototypes

public function integer uof_genera_commesse (string fs_cod_tipo_ord_ven, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, long fl_num_commessa_forzato, ref integer fi_anno_commessa, ref long fl_num_commessa, ref string fs_errore);// Funzione che genera tutte le commesse (possibili) dell'ordine che viene passato
// nome: uof_genera_commesse
// tipo: integer
//       -1 failed
//  		 0 passed
//
//							 
//		Creata il 12/10/2000
//		Autore Diego Ferrari
//
//		Enrico 12/11/2004: aggiunto gestione integrazioni

string   ls_cod_tipo_commessa,ls_cod_deposito_versamento,ls_cod_deposito_prelievo,ls_cod_operatore,ls_cod_tipo_det_ven, & 
			ls_cod_prodotto,ls_flag_tipo_det_ven,ls_flag_materia_prima,ls_des_estesa,ls_formula_tempo, ls_flag_muovi_mp,& 
			ls_flag_esclusione,ls_cod_prodotto_padre,ls_cod_prodotto_figlio,ls_cod_versione,ls_cod_misura, ls_gibus, &
			ls_cod_prodotto_variante, ls_cod_versione_figlio, ls_cod_versione_variante, ls_flag_tipo_impegno_mp,ls_cod_deposito_produzione, &
			ls_flag_tipo_bcl, ls_errore, ls_cod_versione_riga
long     ll_anno_registrazione,ll_num_registrazione,ll_test,ll_num_sequenza, ll_ret
datetime ldt_data_registrazione,ldt_data_consegna
dec{4}   ld_quan_ordine,ld_quan_in_evasione,ld_quan_evasa,ldd_coef_calcolo,ldd_quan_tecnica,ldd_quan_utilizzo, &
			ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t, ld_dim_x, ld_dim_y
uo_funzioni_1 luo_funzioni_1
ll_anno_registrazione = f_anno_esercizio()

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :fs_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore durante la lettura Tabella Tipi Ordini." + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_commessa) then
	fs_errore = "Per questo tipo ordine, " + fs_cod_tipo_ord_ven + " non è specificato il relativo tipo commessa."
	return -1
end if

select cod_deposito_versamento,
		 cod_deposito_prelievo,
		 flag_muovi_mp,
		 flag_tipo_impegno_mp
into   :ls_cod_deposito_versamento,
		 :ls_cod_deposito_prelievo,
		 :ls_flag_muovi_mp,
		 :ls_flag_tipo_impegno_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore durante la lettura Tabella Tipi Commessa." + sqlca.sqlerrtext
	return -1
end if

select cod_operatore
into	 :ls_cod_operatore
from   tes_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :fl_anno_ordine and  
		 num_registrazione = :fl_num_ordine;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura Testata Ordini Vendita. " + sqlca.sqlerrtext
	return -1
end if

ldt_data_registrazione = datetime(today())

select cod_tipo_det_ven,   
		 cod_prodotto,   
		 quan_ordine,   
		 quan_in_evasione,
		 quan_evasa,
		 data_consegna,
		 cod_versione
into   :ls_cod_tipo_det_ven,
		 :ls_cod_prodotto,
		 :ld_quan_ordine,
		 :ld_quan_in_evasione,
		 :ld_quan_evasa,
		 :ldt_data_consegna,
		 :ls_cod_versione
from 	 det_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :fl_anno_ordine and  
		 num_registrazione = :fl_num_ordine and
		 prog_riga_ord_ven =:fl_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la lettura Dettagli Ordini Vendita." + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_versione) then
	fs_errore = "Versione mancante in dettaglio ordine ~r~n" + &
					"ORDINE: " + string(fl_anno_ordine) + " / " + &
									string(fl_num_ordine) + " / " + &
									string(fl_prog_riga_ord_ven) + "~r~n" + &
									" PRODOTTO: " + ls_cod_prodotto

	return -1
end if

ls_cod_versione_riga = ls_cod_versione

//donato 03/08/2012
//forzo la quantità della commessa dall'esterno
if isnull(id_quan_ordine) then
	//caso normale
	ld_quan_ordine = (ld_quan_ordine - ld_quan_in_evasione) - ld_quan_evasa
else
	//stai forzando la quantità della commessa dall'esterno
	ld_quan_ordine = id_quan_ordine
end if
//ld_quan_ordine = (ld_quan_ordine - ld_quan_in_evasione) - ld_quan_evasa

//fine modifica -------------------------------------------------------------------


select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda 
and 	 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli vendite." + sqlca.sqlerrtext
	return -1
end if


if ls_flag_tipo_det_ven = "M" or (ib_flag_non_collegati = true and ls_flag_tipo_det_ven = "C") then
	select max(num_commessa)
	into   :ll_num_registrazione
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_registrazione;
	
	if sqlca.sqlcode <0 then
		fs_errore = "Errore durante la lettura delle Commesse." + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ll_num_registrazione) then
		ll_num_registrazione= 1
	else
		ll_num_registrazione++
	end if

	if not isnull(fl_num_commessa_forzato) then

		select num_commessa
		into   :ll_test
		from   anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_commessa= :ll_anno_registrazione
		and    num_commessa=:fl_num_commessa_forzato;

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore durante il controllo presenza del numero forzato~r~n " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode=0 then
			fs_errore = "Numerazione forzata non valida, il numero risulta essere già esistente.!"
			return -1
		else
			ll_num_registrazione=fl_num_commessa_forzato
		end if
	end if
	
	insert into anag_commesse  
					(cod_azienda,   
					 anno_commessa,   
					 num_commessa,   
					 cod_tipo_commessa,   
					 data_registrazione,
					 cod_operatore,
					 nota_testata,
					 nota_piede,
					 flag_blocco,
					 quan_ordine,   
					 quan_prodotta,
					 data_chiusura,
					 data_consegna,
					 cod_prodotto,
					 quan_assegnata,
					 quan_in_produzione,
					 cod_deposito_versamento,
					 cod_deposito_prelievo,
					 cod_versione)
	values		(:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ls_cod_tipo_commessa,   
					 :ldt_data_registrazione,
					 :ls_cod_operatore,
					 null,
					 null,
					 'N',
					 :ld_quan_ordine,
					 0,
					 null,
					 :ldt_data_consegna,
					 :ls_cod_prodotto,
					 0,
					 0,
					 :ls_cod_deposito_versamento,
					 :ls_cod_deposito_prelievo,
					 :ls_cod_versione);

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore durante la scrittura delle Commesse. Errore DB:" + sqlca.sqlerrtext
		return -1			
	end if

	//inizio cursore generazione varianti commesse
	declare righe_var_det_ord_ven cursor for
	select  cod_prodotto_padre,
			  num_sequenza,
			  cod_prodotto_figlio,
			  cod_versione_figlio,
			  cod_versione,
			  cod_misura,
			  cod_prodotto,
			  cod_versione_variante,
			  quan_tecnica,
			  quan_utilizzo,
			  dim_x,
			  dim_y,
			  dim_z,
			  dim_t,
			  coef_calcolo,
			  flag_esclusione,
			  formula_tempo,
			  des_estesa,
			  flag_materia_prima,
			  cod_deposito_produzione
	from    varianti_det_ord_ven
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     anno_registrazione=:fl_anno_ordine
	and     num_registrazione=:fl_num_ordine
	and     prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	open righe_var_det_ord_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in OPEN cursore righe_var_det_ord_ven (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
		return -1			
	end if

	do while true

		fetch righe_var_det_ord_ven
		into  :ls_cod_prodotto_padre,
				:ll_num_sequenza,
				:ls_cod_prodotto_figlio,
				:ls_cod_versione_figlio,
				:ls_cod_versione,
				:ls_cod_misura,
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ldd_quan_tecnica,
				:ldd_quan_utilizzo,
				:ldd_dim_x,
				:ldd_dim_y,
				:ldd_dim_z,
				:ldd_dim_t,
				:ldd_coef_calcolo,
				:ls_flag_esclusione,
				:ls_formula_tempo,
				:ls_des_estesa,
				:ls_flag_materia_prima,
				:ls_cod_deposito_produzione;

		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in FETCH cursore righe_var_det_ord_ven (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if

		INSERT INTO varianti_commesse
			( cod_azienda,   
			  anno_commessa,   
			  num_commessa,   
			  cod_prodotto_padre,   
			  num_sequenza,   
			  cod_prodotto_figlio,   
			  cod_versione_figlio,   
			  cod_versione,   
			  cod_misura,   
			  cod_prodotto,   
			  cod_versione_variante,   
			  quan_tecnica,   
			  quan_utilizzo,   
			  dim_x,   
			  dim_y,   
			  dim_z,   
			  dim_t,   
			  coef_calcolo,   
			  flag_esclusione,   
			  formula_tempo,   
			  des_estesa,
			  flag_materia_prima,
			  cod_deposito_produzione)  
		VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_cod_prodotto_padre,   
			  :ll_num_sequenza,   
			  :ls_cod_prodotto_figlio,   
			  :ls_cod_versione_figlio,   
			  :ls_cod_versione,   
			  :ls_cod_misura,   
			  :ls_cod_prodotto_variante,   
			  :ls_cod_versione_variante,   
			  :ldd_quan_tecnica,   
			  :ldd_quan_utilizzo,   
			  :ldd_dim_x,   
			  :ldd_dim_y,   
			  :ldd_dim_z,   
			  :ldd_dim_t,   
			  :ldd_coef_calcolo,   
			  :ls_flag_esclusione,   
			  :ls_formula_tempo,   
			  :ls_des_estesa,
			  :ls_flag_materia_prima,
			  :ls_cod_deposito_produzione)  ;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in trasferimento varianti (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if

	loop
	
	close righe_var_det_ord_ven;
	//fine cursore generazione varianti
	
	
	// procedo con il passaggio delle integrazioni. EnMe 12/11/2004
	INSERT INTO integrazioni_commessa  
			( cod_azienda,   
			  anno_commessa,   
			  num_commessa,   
			  progressivo,   
			  cod_prodotto_padre,   
			  num_sequenza,   
			  cod_prodotto_figlio,   
			  cod_versione_figlio,   
			  cod_misura,   
			  quan_tecnica,   
			  quan_utilizzo,   
			  dim_x,   
			  dim_y,   
			  dim_z,   
			  dim_t,   
			  coef_calcolo,   
			  des_estesa,   
			  cod_formula_quan_utilizzo,   
			  flag_escludibile,   
			  flag_materia_prima,   
			  flag_arrotonda,
			  cod_versione_padre,
			  lead_time )  
	SELECT 	cod_azienda,   
				:ll_anno_registrazione,   
				:ll_num_registrazione,   
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_versione_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda,
				cod_versione_padre,
				lead_time
	FROM 	integrazioni_det_ord_ven  
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :fl_anno_ordine and
				num_registrazione = :fl_num_ordine and
				prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in trasferimento integrazioni (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// fine trasferimento integrazioni
	
	
	INSERT INTO varianti_commesse_prod  
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,   
				  cod_versione_figlio,   
				  cod_reparto )  
	SELECT cod_azienda,   
					:ll_anno_registrazione,   
					:ll_num_registrazione,   
					cod_prodotto_padre,   
					num_sequenza,   
					cod_prodotto_figlio,   
					cod_versione,   
					cod_versione_figlio,   
					cod_reparto  
	FROM varianti_det_ord_ven_prod  
	WHERE	cod_azienda = :s_cs_xx.cod_azienda AND  
				anno_registrazione = :fl_anno_ordine AND  
				num_registrazione = :fl_num_ordine AND  
				prog_riga_ord_ven = :fl_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in trasferimento varianti reparti produzione (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	//anticipo qui l'update del numero commessa sulla riga ordine
	//mi serve per la chiamata alla funzione di calcolo fabbisogni (reparto impegnato: SR Impegnato_commessa (GIBUS))
	update det_ord_ven  
	set    anno_commessa = :ll_anno_registrazione,
			 num_commessa = :ll_num_registrazione
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_ordine and  
			 num_registrazione = :fl_num_ordine and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento Dettagli Ordini Vendita." + sqlca.sqlerrtext
		return -1
	end if
	
	
	if ls_flag_muovi_mp = 'S' then
		if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
			if luo_magazzino.uof_impegna_mp_commessa(	true, &
																	false, &
																	ls_cod_prodotto, &
																	ls_cod_versione_riga, &
																	ld_quan_ordine, &
																	ll_anno_registrazione, &
																	ll_num_registrazione, &
																	fs_errore) = -1 then
				fs_errore = "Errore in fase di impegno magazzino~r~n" + fs_errore
				return -1
			end if
		end if
	end if
	
	
	// inizio ciclo di scorrimento della distinta e inserimento varianti
	select flag
	into :ls_gibus
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_parametro = 'GIB';
	if sqlca.sqlcode <> 0 then
		ls_gibus = "N"
	end if
	
	if isnull(ls_gibus) then ls_gibus = "N"
	
	if ls_gibus = "S" and not isnull(ls_cod_versione) then
	
		select flag_tipo_bcl
		into 	:ls_flag_tipo_bcl
		from 	tab_tipi_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_ord_ven = :fs_cod_tipo_ord_ven;
		if sqlca.sqlcode = 0 and ls_flag_tipo_bcl = 'B' then		// SOLO SE SFUSO
	
			luo_funzioni_1 = create uo_funzioni_1
			
			select	 	dim_x, 
						dim_y
			into 		:ld_dim_x,
						:ld_dim_y
			from 	 	det_ord_ven  
			where  	cod_azienda = :s_cs_xx.cod_azienda and  
						 anno_registrazione = :fl_anno_ordine and  
						 num_registrazione = :fl_num_ordine and
						 prog_riga_ord_ven =:fl_prog_riga_ord_ven;
	
			
			luo_funzioni_1.id_dim_x = ld_dim_x
			luo_funzioni_1.id_dim_y = ld_dim_y
			
			ll_ret = luo_funzioni_1.uof_valorizza_formule_distinta(ls_cod_prodotto, ls_cod_versione_riga, ll_anno_registrazione, ll_num_registrazione, 0, "COMMESSE", ls_errore)
			
			if ll_ret < 0 then
				fs_errore = "Errore nella funzione di valorizzazione formule (uof_valorizza_formule_distinta)~r~n" + ls_errore
				return -1
			end if
		
			destroy luo_funzioni_1
		end if
	end if
	
	//fine generazione varianti commesse
	
	fi_anno_commessa = ll_anno_registrazione
	fl_num_commessa = ll_num_registrazione
	
end if				

return 0
end function

public function integer uof_cancella_commessa (long fl_anno_commessa, long fl_num_commessa, ref string fs_messaggio);// Funzione che cancella una commessa.
//
// nome: uof_cancella_commessa
// tipo: integer
//       -1 failed
//  		 0 passed
//
//							 
//		Creata il 25/01/2002
//		Autore ENRICO MENEGOTTO
//
// ******************************************  A T T E N Z I O N E  ***************************************************** //
//
//	QUESTA FUNZIONE NON ESEGUE IL DELETE DA ANAG_COMMESSE CHE DEVE ESSERE FATTO A MANO ALL'ESTERNO DOPO QUESTA FUNZIONE
//
// ********************************************************************************************************************** //

boolean lb_dt1
string  ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old,ls_cod_tipo_commessa,&
		  ls_flag_muovi_mp,ls_parametro, ls_flag_tipo_impegno_mp
dec{4}  ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
		  ldd_quan_impegnata,ldd_quan_in_produzione, &
		  ldd_quan_assegnata_old, ldd_quan_prodotta_old,ldd_quan_in_ordine_old, &
		  ldd_quan_impegnata_old,ldd_quan_in_produzione_old
long    ll_i,ll_anno_commessa,ll_num_commessa,ll_null, ll_num_reg,ll_prog_riga_ord_ven, ll_dettagli
integer li_anno_registrazione

setnull(ll_null)
if fl_anno_commessa = 0 or isnull(fl_anno_commessa) then
	fs_messaggio = "Attenzione !! E' stato passato l'anno commessa = 0 alla funzione uo_cancella_commessa: la funzione non sarà eseguita."
	return -1
end if
if fl_num_commessa = 0 or isnull(fl_num_commessa) then
	fs_messaggio = "Attenzione !! E' stato passato numero commessa = 0 alla funzione uo_cancella_commessa: la funzione non sarà eseguita."
	return -1
end if

select cod_tipo_commessa,
       cod_prodotto,
		 cod_versione,
		 quan_in_produzione,
		 quan_assegnata,
		 quan_prodotta,
		 quan_ordine
into   :ls_cod_tipo_commessa,
		 :ls_cod_prodotto,
		 :ls_cod_versione,
		 :ldd_quan_in_produzione,
		 :ldd_quan_assegnata,
		 :ldd_quan_prodotta,
		 :ldd_quan_prodotta
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_commessa = :fl_anno_commessa and
       num_commessa = :fl_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nr." + string(sqlca.sqlcode) + " in ricerca tipo commessa.~r~n " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_commessa) then
	fs_messaggio = "Attenzione !! NON è stato specificato il tipo commessa nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa) + ": la funzione non sarà eseguita."
	return -1
end if

// stefanop 16/07/2012: controllo che la commessa non abbia nessun DDT di trasferimento collegato
// -------------------------------------------------
if uof_cancella_commessa_ddt_trasferimento(fl_anno_commessa, fl_num_commessa, fs_messaggio) < 0 then
	return -1
end if
// -------------------------------------------------

select flag_muovi_mp,
		 flag_tipo_impegno_mp
into    :ls_flag_muovi_mp,
		:ls_flag_tipo_impegno_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_tipo_commessa = :ls_cod_tipo_commessa;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca flag_muovi_mp in tabella tipi commessa.~r~n " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_muovi_mp ='S' then

	ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta
	//Disimpegno la quantita
	
	//	** disimpegno alla cancellazione solo se ho impegnato qualcosa
	//	**	quindi controllo di che tipo è l'impegno, se all'attivazione allora devo controllare se è attiva.
	if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
		
		
		// EnMe 26/01/2009
		// Fino ad oggi veniva disimpegnato usando la variabile "ldd_quan_in_produzione" 
		// secondo me era errato perchè infatti chi usa l'avanzamento automatico disimpegna con ZERO.
		if luo_magazzino.uof_impegna_mp_commessa(	false, &
																false, &
																ls_cod_prodotto, &
																ls_cod_versione, &
																- ldd_quan_impegnata , &
																fl_anno_commessa, &
																fl_num_commessa, &
																fs_messaggio) = -1 then
			fs_messaggio = "Errore in fase di impegno magazzino.~r~n" + fs_messaggio
			return -1
		end if
		
	else
		
		select count(*)
		into	:ll_dettagli
		from	det_anag_commesse
		where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :fl_anno_commessa and
				num_commessa = :fl_num_commessa;
				
		// ***	se ho dei dettagli allora è stata attivata e quindi ha impegnato le materie prime, altrimenti no
		
		if sqlca.sqlcode = 0 and not isnull(ll_dettagli) and ll_dettagli > 0 then
			
			if luo_magazzino.uof_impegna_mp_commessa(	false, &
																	false, &
																	ls_cod_prodotto, &
																	ls_cod_versione, &
																	- ldd_quan_in_produzione , &
																	fl_anno_commessa, &
																	fl_num_commessa, &
																	fs_messaggio) = -1 then
				fs_messaggio = "Errore in fase di impegno magazzino.~r~n" + fs_messaggio
				return -1
			end if			
			
		end if
		
	end if
	
//	if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, -ldd_quan_impegnata, fl_anno_commessa, fl_num_commessa, &
//						ll_null, "varianti_commesse", False ) = -1 then 
//		fs_messaggio = "Errore durante l'aggiornamento dell'impegnato delle materie prime nel magazzino"
//		return -1
//	end if

end if

delete varianti_commesse_prod
where  cod_azienda   = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa  = :fl_num_commessa;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore durante cancellazione varianti commesse.~r~n " + sqlca.sqlerrtext
	return -1
end if

delete varianti_commesse
where  cod_azienda   = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa  = :fl_num_commessa;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore durante cancellazione varianti commesse.~r~n " + sqlca.sqlerrtext
	return -1
end if

delete integrazioni_commessa
where  cod_azienda   = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa  = :fl_num_commessa;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore durante cancellazione integrazioni commesse.~r~n " + sqlca.sqlerrtext
	return -1
end if

// cancellazione tabella distinte_taglio_calcolate

select stringa
into   :ls_parametro
from   parametri
where  cod_parametro='DT';

if sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore durante la lettura tabella parametri.~r~n " + sqlca.sqlerrtext
	return  -1
end if

guo_functions.uof_get_parametro( "DT1", lb_dt1)

if ls_parametro='S' and not lb_dt1 then
	
	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven
	into   :li_anno_registrazione,
			 :ll_num_reg,
			 :ll_prog_riga_ord_ven
	from   det_ord_ven
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :fl_anno_commessa and
			 num_commessa  = :fl_num_commessa;

	if sqlca.sqlcode < 0 then
		fs_messaggio = "Si è verificato un errore durante la lettura tabella det_ord_ven.~r~n " + sqlca.sqlerrtext
		return  -1
	end if
	
	//Donato 26/07/2012: se la commessa è legata all'ordine di vendita allora esegui la cancellazione delle eventuali distinte taglio calcolate
	if li_anno_registrazione>0 and ll_num_reg>0 and ll_prog_riga_ord_ven>0 then
		delete distinte_taglio_calcolate
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :li_anno_registrazione and
				 num_registrazione  = :ll_num_reg and
				 prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Si è verificato un errore durante la cancellazione tabella distinte_taglio_calcolate.~r~n " + sqlca.sqlerrtext
			return -1
		end if
	end if
end if

update det_ord_ven
set    anno_commessa = :ll_null,
		 num_commessa  = :ll_null
where  cod_azienda   = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa  = :fl_num_commessa;
if sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in fase di cancellazione riferimenti commessa in riga ordine.~r~n " + sqlca.sqlerrtext
	return -1
end if

//Donato 26/07/2012 se la commessa è legata ad un ddt azzera le colonne della commessa sulla riga ddt
update det_bol_ven
set    anno_commessa = :ll_null,
		 num_commessa  = :ll_null
where  cod_azienda   = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa  = :fl_num_commessa;
if sqlca.sqlcode < 0 then
	fs_messaggio = "Si è verificato un errore in fase di cancellazione riferimenti commessa in riga ddt.~r~n " + sqlca.sqlerrtext
	return -1
end if
		 
return 0

end function

public function integer uof_set_flag_non_collegati (boolean ab_flag_non_collegati);ib_flag_non_collegati = ab_flag_non_collegati
return 0
end function

private function boolean uof_fasi_lavorazione (string as_cod_prodotto, string as_cod_versione, boolean ab_controlla_flag_stampa);/**
 * stefanop
 * 30/01/2012
 *
 * Controllo che il prodotto abbia una fase di lavorazione
 **/
 
string ls_flag_stampa_fase

select flag_stampa_fase
into :ls_flag_stampa_fase
from tes_fasi_lavorazione
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto and
		 cod_versione = :as_cod_versione;
		 
if sqlca.sqlcode <> 0 then return false
		
// controllo che ci sia il flag_stampa_fase abiliatto
if ab_controlla_flag_stampa then
	
	return (ls_flag_stampa_fase = "S")
	
end if
		 
return true
		 
end function

public function integer uof_trova_reparti_cal (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, string fs_cod_deposito_origine_ordine, ref string fs_cod_reparto[], ref string fs_errore);

string				ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, & 
					ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase, ls_cod_versione_figlio, &
					ls_cod_versione_inserito, ls_cod_versione_variante, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], ls_vuoto[]
					
long				ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza, ll_y, ll_new

integer			li_risposta

boolean			lb_test, lb_variante_trovata=false

datastore lds_righe_distinta


//Donato 19/12/2011 ------------------------------------------------------------------
//controllo se ho una fase di stampa con reparto a livello root della distinta

if fb_inizio then
	ls_flag_stampa_fase = 'N'
	
	select		cod_reparto,
				flag_stampa_fase,
				cod_deposito_origine,
				flag_stampa_sempre
	into		:ls_cod_reparto,
				:ls_flag_stampa_fase,
				:ls_cod_deposito_origine,
				:ls_flag_stampa_sempre
	from		tes_fasi_lavorazione
	where	cod_azienda = :s_cs_xx.cod_azienda  AND    
				cod_prodotto = :fs_cod_prodotto and
				cod_versione = :fs_cod_versione;

	if ls_flag_stampa_fase = 'S' then 
		lb_test = false
		
		//Donato 19/12/2011
		//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
		//if f_stabilimenti_reparti(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_cod_prodotto, fs_cod_versione, ls_cod_reparto, fs_errore)<0 then
		ls_reparti_letti[] = ls_vuoto[]
		if uof_reparti_stabilimenti_cal(		fi_anno_registrazione, fl_num_registrazione, fs_cod_prodotto, fs_cod_versione, &
											ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, fs_cod_deposito_origine_ordine, ls_reparti_letti[], fs_errore)<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//merge con l'array by ref principale
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])

	end if
	//------------------------------------------------------------------
end if

ll_num_figli = 1

lds_righe_distinta = Create DataStore

//lds_righe_distinta.DataObject = "d_data_store_distinta"
// cambiato datastore EnMe 12/11/2004 (integrazioni)
lds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,&
                                           fs_cod_prodotto,&
														 fs_cod_versione,&
														 fi_anno_registrazione, &
														 fl_num_registrazione,&
														 fl_prog_riga_ord_ven)

ll_num_figli = 1
//for ll_num_figli=1 to ll_num_righe
do while ll_num_figli <= ll_num_righe
	//leggi i figli del ramo della distinta base
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo") = "S" then continue
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	//leggi se vi sono varianti nel ramo di distinta ----------------------------
	lb_variante_trovata = false
	
	select cod_prodotto,
			 cod_versione_variante,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 anno_registrazione = :fi_anno_registrazione	and    
			 num_registrazione = :fl_num_registrazione	and    
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven	and    
			 cod_prodotto_padre = :fs_cod_prodotto			and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione_figlio = :ls_cod_versione_figlio  and
			 cod_versione = :fs_cod_versione					and    
			 num_sequenza = :ll_num_sequenza;
	
   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		//variante trovata: esegui scambio di prodotto/versione con prodotto variante/versione variante
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		
		lb_variante_trovata = true
		
		//TODO: leggi i reparti della variante  dalla varianti_det_ord_ven_prod
		//SE LI TROVA, CONSIDERA QUESTI E NON ALTRO, ALTRIMENTI CONSIDERA QUELLI TROVATI DALLA DISTINTA BASE
		//leggi da varianti_det_ord_ven  il cod_deposito_produzione
		//leggi da varianti_det_ord_ven_prod i rispettivi cod_reparto   (sono quelli di produzione)
		
	end if		

	//###########################################################################
	// se è materia prima salta .....
	if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
		//no MP
		
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		if sqlca.sqlcode=100 then 
			ll_num_figli++
			continue
		end if
		
	else
		//MP
		ll_num_figli++
		continue
	end if
	
	//verifica se la fase è da stampare ----------------------------------------
	ls_flag_stampa_fase = 'N'
	
	select	cod_reparto,
			flag_stampa_fase,
			cod_deposito_origine,
			flag_stampa_sempre
	into	:ls_cod_reparto,
			:ls_flag_stampa_fase,
			:ls_cod_deposito_origine,
			:ls_flag_stampa_sempre
   FROM   tes_fasi_lavorazione
   WHERE  cod_azienda = :s_cs_xx.cod_azienda  AND    
			 cod_prodotto = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;
	
	
	
	//Donato 23/01/2012 ----------------------------------------------------------------------------------------------------------
	//se hai trovato la variante pesca i reparti da varianti_det_ord_ven_prod (CHIAMERAI f_reparti_varianti(....))
	//altrimenti procedi normalmente dalla tes_fasi_lavorazione/tes_fasi_lavorazione_prod
	//per ora questa soluzione non è ancora implementata ...
	//---------------------------------------------------------------------------------------------------------------------------------
	
	//se la fase è da stampare
	if ls_flag_stampa_fase = 'S' then 
		//stampa fase SI
		lb_test = false
		
		//Donato 19/12/2011
		//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
		ls_reparti_letti[] = ls_vuoto[]
		if uof_reparti_stabilimenti_cal(		fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto_inserito, ls_cod_versione_inserito, &
											ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, fs_cod_deposito_origine_ordine, ls_reparti_letti[], fs_errore)<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//merge con l'array by ref principale
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])
	end if
	
	//###########################################################################
	
	//prosegui più all'interno del ramo nella distinta, se c'è ancora altro
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda		and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		ll_num_figli++
		continue
	else
		//li_risposta=f_trova_reparti_cal(fi_anno_registrazione,fl_num_registrazione,fl_prog_riga_ord_ven,ls_cod_prodotto_inserito,ls_cod_versione_inserito,fs_cod_reparto[],ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	ll_num_figli++
loop
//next

destroy lds_righe_distinta

return 0
end function

public function integer uof_reparti_stabilimenti_cal (long fi_anno_registrazione, long fl_num_registrazione, string fs_cod_prodotto, string fs_cod_versione, string fs_cod_reparto_origine, string fs_cod_deposito_origine_fase, string fs_flag_stampa_sempre, string fs_cod_deposito_origine_ordine, ref string fs_reparti_letti[], ref string fs_errore);//imposta il reparto produzione, qualora diverso in base allo stabilimento acquisizione dell'ordine

string 					ls_ordine, ls_cod_deposito_acquisiz, ls_tipo_stampa, ls_cod_tipo_ord_ven, ls_sql, ls_cod_deposito_produzione, ls_cod_giro_consegna, ls_cod_dep_giro, ls_cod_deposito_origine
datastore				lds_data
long						ll_return, ll_index, ll_i


if fi_anno_registrazione>0 and fl_num_registrazione>0 then
	ls_ordine = "Ordine "+string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)
	
	//[1] ########################################################################################
	//leggo tipo ordine e deposito acquisizione ordine
	select cod_tipo_ord_ven, cod_deposito, cod_giro_consegna
	into :ls_cod_tipo_ord_ven, :ls_cod_deposito_acquisiz, :ls_cod_giro_consegna
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fi_anno_registrazione and
				num_registrazione=:fl_num_registrazione;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in lettura tipo ordine e deposito "+ls_ordine+" :"+sqlca.sqlerrtext
		return -1
	end if

	if ls_cod_deposito_acquisiz="" or isnull(ls_cod_deposito_acquisiz) then
		//normalmente come prima, restituisci il reparto della fase lavorazione/anagrafica prodotti
		fs_reparti_letti[1] = fs_cod_reparto_origine
		
		return 0
	end if
	
	
	//salvo il deposito origine ordine
	ls_cod_deposito_origine = ls_cod_deposito_acquisiz
	
	//--------------------------------------------------------------------------------------------------------------------------------
	//se esiste un giro consegna e questo ha un deposito giro diverso da quello testata ordine devi considerare quello
	if g_str.isnotempty(ls_cod_giro_consegna) then
		select cod_deposito
		into :ls_cod_dep_giro
		from tes_giri_consegne
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_giro_consegna=:ls_cod_giro_consegna;
					
		if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_dep_giro<>ls_cod_deposito_acquisiz then ls_cod_deposito_acquisiz=ls_cod_dep_giro	
	end if
	//--------------------------------------------------------------------------------------------------------------------------------
	
	
	//controlla se nella fase c'è scritto di stampare sempre o se deve stampare perchè il deposito origine coincide con quello dell'ordine
	if fs_flag_stampa_sempre="S" or (fs_flag_stampa_sempre="N" and ls_cod_deposito_acquisiz=fs_cod_deposito_origine_fase) then
		fs_reparti_letti[1] = fs_cod_reparto_origine
	end if
	
	//[2] ########################################################################################
	//leggo tipo stampa ordine
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore in lettura flag tipo_stampa bcl "+ls_ordine+": " + sqlca.sqlerrtext
		return -1
	end if
	
else
	if fi_anno_registrazione=-9999 then
		ls_tipo_stampa = "B"
	else
		ls_tipo_stampa = "A"
	end if
	ls_cod_deposito_acquisiz = fs_cod_deposito_origine_ordine
end if

//[3] ########################################################################################
//leggo se ci sono regole/eccezioni impostate, se si modifico la variabile REF fs_cod_rep_produzione, altrimenti esco senza cambiarla
choose case ls_tipo_stampa
	case "A", "C"  //tende da sole, tecniche
		//cerco in tes_fasi_lavorazione_det se esiste qualche regola che si accoppia (deposito origine)
		//NOTA: in fs_cod_prodotto e fs_cod_versione abbiamo il prodotto della riga e la sua versione, o al più il prodotto variante e la sua versione
		
		select		cod_deposito_produzione
		into		:ls_cod_deposito_produzione
		from tes_fasi_lavorazione_det
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:fs_cod_prodotto and
					cod_versione=:fs_cod_versione and
					cod_deposito_origine=:ls_cod_deposito_acquisiz;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura deposito produzione del prodotto '"+fs_cod_prodotto+"' versione '"+fs_cod_versione+"': "+ls_ordine+": " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ls_cod_deposito_produzione) then ls_cod_deposito_produzione=""
			
		//carico i reparti stabiliti dalla tes_fasi_lavorazione_prod
		ls_sql = "select 	cod_reparto_produzione "+&
					"from tes_fasi_lavorazione_prod "+&
					"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_prodotto='"+fs_cod_prodotto+"' and "+&
								"cod_versione='"+fs_cod_versione+"' and "+&
								"cod_deposito_produzione='"+ls_cod_deposito_produzione+"' "
		
	case "B"	//sfuso
		
		//carico i reparti stabiliti dalla anag_prodotti_reparti_depositi, se ce ne sono
		ls_sql = "select 	cod_reparto_produzione "+&
					"from anag_prodotti_reparti_depositi "+&
					"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_prodotto='"+fs_cod_prodotto+"' and "+&
								"cod_deposito_origine='"+ls_cod_deposito_origine+"' "

		case else
//			fs_errore = "Caso tipo ordine non previsto (A,B,C): "+ls_ordine+": " + sqlca.sqlerrtext
//			return -1
			return 1
			
end choose

		//creo datastore, già popolato -------
ll_return = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)

		//verifico i risultati ottenuti --------------
choose case ll_return
	case is < 0
	//in fs_errore il messaggio
	return -1
	
	case is >0
		//trovate eccezioni/reparti
		for ll_index=1 to ll_return
			ll_i = upperbound(fs_reparti_letti[])
			ll_i += 1
			fs_reparti_letti[ll_i] = lds_data.getitemstring(ll_index, "cod_reparto_produzione")
		next
		//fs_cod_rep_produzione = lds_data.getitemstring(1, "cod_reparto_produzione")
	
	return 0
	
case else
	// non sono state trovate reparti/eccezioni, usa il reparto della fase/anagrafica
	if upperbound(fs_reparti_letti) > 0 then
	else
		fs_reparti_letti[1] = fs_cod_reparto_origine
	end if
	
	return 1
	
end choose

//######################################################################################


return 0
end function

public function integer uof_trova_reparti (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_errore);// Funzione che trova i reparti associati alle fasi di lavoro che sono da stampare 
// tipo: integer
//       -1 failed
//  	 0 passed


string			ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, ls_sql, & 
					ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase, ls_cod_versione_figlio, &
					ls_cod_versione_inserito, ls_cod_versione_variante, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], ls_vuoto[]
					
long				ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza, ll_y, ll_new, ll_ret, ll_i, ll_count

integer			li_risposta

boolean			lb_test, lb_variante_trovata=false

datastore lds_righe_distinta, lds_reparti_variante

if fb_inizio then
	//Donato 19/12/2011 ------------------------------------------------------------------
	//controllo se ho una fase di stampa con reparto a livello root della distinta
	
	 ls_flag_stampa_fase = 'N'
		
	select		cod_reparto,
				flag_stampa_fase,
				cod_deposito_origine,
				flag_stampa_sempre
	into		:ls_cod_reparto,
				:ls_flag_stampa_fase,
				:ls_cod_deposito_origine,
				:ls_flag_stampa_sempre
	from		tes_fasi_lavorazione
	where	cod_azienda = :s_cs_xx.cod_azienda  AND    
				cod_prodotto = :fs_cod_prodotto and
				cod_versione = :fs_cod_versione;
	
	if ls_flag_stampa_fase = 'S' then 
		lb_test = false
		
		//Donato 19/12/2011
		//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
		//if f_stabilimenti_reparti(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_cod_prodotto, fs_cod_versione, ls_cod_reparto, fs_errore)<0 then
		ls_reparti_letti[] = ls_vuoto[]
		if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, fs_cod_prodotto, fs_cod_versione, &
											ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], fs_errore)<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//merge con l'array by ref principale
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])
	end if
	//------------------------------------------------------------------
end if

ll_num_figli = 1

lds_righe_distinta = Create DataStore

//lds_righe_distinta.DataObject = "d_data_store_distinta"
// cambiato datastore EnMe 12/11/2004 (integrazioni)
lds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, fi_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven)

ll_ret = lds_righe_distinta.setsort("#12, #4")
ll_ret = lds_righe_distinta.sort()

//SR Vendita_semilavorato
if fb_inizio and upperbound(fs_cod_reparto[])>0 then
	//se il datastore ha restituito come figlio un codice prodotto REPARTO
	//ed esiste una variante con reparto associato allora restituisci nell'array dei reparti solo questo reparto
	//il problema si porrebbe per gli ordini per i quali è stato cambiato deposito produzione
	//in tal caso la funzione estrarrebbe il reparto dello stabilimento 1 (root del semilavorato in distinta)
	//ed inoltre anche quello del codice fittizio REPARTO
	
	for ll_num_figli=1 to ll_num_righe
		ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
		
		if ls_cod_prodotto_figlio <> "REPARTO" then continue
		
		ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
		ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
		
		ls_sql =  " select cod_reparto from varianti_det_ord_ven_prod  " + &
					" where  cod_azienda = '" + s_cs_xx.cod_azienda	+ "' and " + &
					" anno_registrazione = " + string(fi_anno_registrazione) + " and " + &
					" num_registrazione = " +  string(fl_num_registrazione)+ " and " + & 
					" prog_riga_ord_ven = " +  string(fl_prog_riga_ord_ven) + " and " + &
					" cod_prodotto_padre = '" +  fs_cod_prodotto + "' and " + &
					" cod_prodotto_figlio = '" +  ls_cod_prodotto_figlio + "' and " + &
					" cod_versione_figlio = '" +  ls_cod_versione_figlio + "' and " + &
					" cod_versione = '" +  fs_cod_versione + "' and " + &
					" num_sequenza = " +  string(ll_num_sequenza)
		
		ll_ret = guo_functions.uof_crea_datastore(lds_reparti_variante, ls_sql, ls_errore)
		ls_errore = ""
		
		if ll_ret>0 then
			fs_cod_reparto[] = ls_vuoto[]
			
			for ll_num_figli=1 to ll_ret
				fs_cod_reparto[ll_num_figli] = lds_reparti_variante.getitemstring(ll_num_figli, "cod_reparto")
			next
			
			destroy lds_reparti_variante
			
			return 0
			
		else
			destroy lds_reparti_variante
			exit
		end if
		
	next
	
end if




ll_num_figli = 1
//for ll_num_figli=1 to ll_num_righe
do while ll_num_figli <= ll_num_righe
	//leggi i figli del ramo della distinta base
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo") = "S" then 
		ll_num_figli ++
		continue
	end if
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	//leggi se vi sono varianti nel ramo di distinta ----------------------------
	lb_variante_trovata = false
	
	select cod_prodotto,
			 cod_versione_variante,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ls_flag_materia_prima_variante
	from    varianti_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 anno_registrazione = :fi_anno_registrazione	and    
			 num_registrazione = :fl_num_registrazione	and    
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven	and    
			 cod_prodotto_padre = :fs_cod_prodotto			and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione_figlio = :ls_cod_versione_figlio  and
			 cod_versione = :fs_cod_versione					and    
			 num_sequenza = :ll_num_sequenza;
	
   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 0 then
		//variante trovata: esegui scambio di prodotto/versione con prodotto variante/versione variante
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		
		lb_variante_trovata = true
		
		//TODO: leggi i reparti della variante  dalla varianti_det_ord_ven_prod
		//SE LI TROVA, CONSIDERA QUESTI E NON ALTRO, ALTRIMENTI CONSIDERA QUELLI TROVATI DALLA DISTINTA BASE
		//leggi da varianti_det_ord_ven  il cod_deposito_produzione
		//leggi da varianti_det_ord_ven_prod i rispettivi cod_reparto   (sono quelli di produzione)
		ls_reparti_letti[] = ls_vuoto[]
		ls_sql =  " select cod_reparto from varianti_det_ord_ven_prod  " + &
					" where  cod_azienda = '" + s_cs_xx.cod_azienda	+ "' and " + &
					" anno_registrazione = " + string(fi_anno_registrazione) + " and " + &
					" num_registrazione = " +  string(fl_num_registrazione)+ " and " + & 
					" prog_riga_ord_ven = " +  string(fl_prog_riga_ord_ven) + " and " + &
					" cod_prodotto_padre = '" +  fs_cod_prodotto + "' and " + &
					" cod_prodotto_figlio = '" +  ls_cod_prodotto_figlio + "' and " + &
					" cod_versione_figlio = '" +  ls_cod_versione_figlio + "' and " + &
					" cod_versione = '" +  fs_cod_versione + "' and " + &
					" num_sequenza = " +  string(ll_num_sequenza)
		
		guo_functions.uof_crea_datastore(lds_reparti_variante, ls_sql)
		
		ll_ret = lds_reparti_variante.retrieve()
		
		for ll_i = 1 to ll_ret
			ls_reparti_letti[ll_i] = lds_reparti_variante.getitemstring(ll_i, "cod_reparto")
		next
		
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])

		
	else	
	
		//###########################################################################
		// se è materia prima salta .....
		if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
			//no MP
			
			select cod_azienda
			into   :ls_test
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and 	 
					 cod_prodotto_padre = :ls_cod_prodotto_inserito and
					 cod_versione = :ls_cod_versione_inserito;
			
			if sqlca.sqlcode=100 then 
				ll_num_figli++
				continue
			end if
			
		else
			//MP
			ll_num_figli++
			continue
		end if
		
		//verifica se la fase è da stampare ----------------------------------------
		ls_flag_stampa_fase = 'N'
		
		select	cod_reparto,
				flag_stampa_fase,
				cod_deposito_origine,
				flag_stampa_sempre
		into	:ls_cod_reparto,
				:ls_flag_stampa_fase,
				:ls_cod_deposito_origine,
				:ls_flag_stampa_sempre
		FROM   tes_fasi_lavorazione
		WHERE  cod_azienda = :s_cs_xx.cod_azienda  AND    
				 cod_prodotto = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		
		
		//Donato 23/01/2012 ----------------------------------------------------------------------------------------------------------
		//se hai trovato la variante pesca i reparti da varianti_det_ord_ven_prod (CHIAMERAI f_reparti_varianti(....))
		//altrimenti procedi normalmente dalla tes_fasi_lavorazione/tes_fasi_lavorazione_prod
		//per ora questa soluzione non è ancora implementata ...
		//---------------------------------------------------------------------------------------------------------------------------------
		
		//se la fase è da stampare
		if ls_flag_stampa_fase = 'S' then 
			//stampa fase SI
			lb_test = false
			
			//Donato 19/12/2011
			//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
			ls_reparti_letti[] = ls_vuoto[]
			if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto_inserito, ls_cod_versione_inserito, &
												ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], fs_errore)<0 then
				//in fs_errore il messaggio
				return -1
			end if
			
			//merge con l'array by ref principale
			guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])
		end if
	end if
	
	//###########################################################################
	
	//prosegui più all'interno del ramo nella distinta, se c'è ancora altro
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda		and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		ll_num_figli++
		continue
	else
		li_risposta=uof_trova_reparti(false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_prodotto_inserito, ls_cod_versione_inserito, fs_cod_reparto[], ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	ll_num_figli++
loop
//next

destroy lds_righe_distinta

return 0
end function

public function integer uof_trova_reparti_sfuso (string as_cod_prodotto, string as_cod_deposito_origine, string as_cod_deposito_produzione, ref string as_cod_reparti_produzione[], ref string as_messaggio);/**
 * stefanop
 * 03/02/2012
 *
 * Recupero i reparti di produzione per i prodotti di sfuso
 * Ritorno:
 *  -1 - errore
 *   1 - ok
 **/
 
string ls_sql, ls_empty[]
long ll_rows, ll_i
datastore lds_store

as_cod_reparti_produzione = ls_empty

if isnull(as_cod_deposito_produzione) or as_cod_deposito_produzione="" then
	ls_sql = 	"SELECT cod_reparto_produzione FROM anag_prodotti_reparti_depositi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
							"cod_prodotto='" + as_cod_prodotto + "' AND  cod_deposito_origine='" + as_cod_deposito_origine + "' "
else
	ls_sql = 	"SELECT cod_reparto_produzione FROM anag_prodotti_reparti_depositi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
						"cod_prodotto='" + as_cod_prodotto + "' AND  cod_deposito_origine='" + as_cod_deposito_origine + "' AND " + &
						"cod_deposito_produzione='" + as_cod_deposito_produzione + "'"
end if

//ls_sql = 	"SELECT cod_reparto_produzione FROM anag_prodotti_reparti_depositi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
//			"cod_prodotto='" + as_cod_prodotto + "' AND  cod_deposito_origine='" + as_cod_deposito_origine + "' AND " + &
//			"cod_deposito_produzione='" + as_cod_deposito_produzione + "'"
			

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_messaggio)

if ll_rows < 0 then
	return -1
elseif ll_rows = 0 then
	return 1
end if

for ll_i = 1 to ll_rows
	as_cod_reparti_produzione[ll_i] = lds_store.getitemstring(ll_i,1)
next

return 1
end function

public function integer uof_avanza_commessa (long al_anno_commessa, long al_num_commessa, string fs_cod_semilavorato, ref string as_errore);// Funzione che esegue l'avanzamento automatico della commessa passata senza che l'utente intervenga per assegnare gli
// stock di materia prima. Per default saranno presi gli stock più vecchi o più nuovi a seconda del flag_tipo_scarico
// impostato nei tipi commessa e cioè se il flag = V (vecchio) la procedura scarica dal più vecchio, se è uguale a N
// la procedura scarica partendo dal più nuovo intesi come. Il campo che determina l'ordinamento vecchio/nuovo è il
// campo data_stock
// nome: uof_avanza_commessa
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per	
//							
//							 al_anno_commessa	 		 integer			valore
//							 al_num_commessa			 long				valore
//							 as_errore					 string			riferimento
//
//		Creata il 05/01/2010
//		Autore Enrico Menegotto

//introdotta variabile dim istanza booleana "ib_posticipa", normalmente a false
//se attivata dopo il create di questo ogegtto, allora questa funzione non esegue la chiusura/avanzamento della commessa ma effettua una pianificazione della stessa
//successivamente un processo schedulato la elabora e la chiude (vedi oggetto uo_service_commesse)

// 20.01.2020 EnMe nuovo parametro DT1 che se esistente non cancella le distinte di taglio alla fine dell'avanzamento della commessa



boolean 			lb_dt1 

double			ldd_quantita_prodotta_pf, ldd_quan_tecnica, ldd_quan_utilizzo, ldd_quantita_utilizzo_buffer[], &
					ldd_quan_ordine, ldd_quan_prodotta, ldd_giacenza_stock, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, &
					ldd_quan_in_produzione, ldd_quan_impegnata_attuale, ldd_quantita_utilizzo[], ldd_coef_calcolo, ldd_quantita_utilizzo_dest[]
					
long				ll_prog_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[],ll_num_registrazione[], &
					ll_i, ll_num_sequenza,ll_progressivo, ll_null,ll_num_stock, ll_prog_stock_1, ll_num_reg, ll_prog_riga_ord_ven, &
					ll_anno_registrazione_rag[], ll_num_registrazione_rag[], ll_k, ll_num_riga_app, ll_prog_stock_old

string				ls_cod_tipo_commessa,ls_cod_tipo_mov_prel_mat_prime,ls_cod_deposito[], ls_cod_prodotto_finito, & 
					ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_cod_deposito_versamento, & 
					ls_cod_tipo_mov_ver_prod_finiti, ls_flag_tipo_avanzamento, ls_cod_prodotto_padre, ls_cod_deposito_prelievo,&
					ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_prodotto_variante, ls_flag_esclusione, ls_flag_calcola_costo_pf, &
					ls_cod_ubicazione_test, ls_cod_lotto_test, ls_flag_cliente, ls_flag_fornitore, ls_errore, ls_parametro,& 
					ls_flag_materia_prima,ls_des_estesa,ls_lotto,ls_ubicazione,ls_materia_prima[],ls_flag_tipo_ord_scarico, & 
					ls_cod_ubicazione_1,ls_cod_lotto_1,ls_cod_ubicazione_pf,ls_cod_lotto_pf,ls_cod_prodotto,ls_anno_commessa, &
					ls_formula_tempo, ls_flag_mag_negativo, ls_sql, ls_versione_prima[], ls_cod_versione_figlio, ls_cod_versione_variante, &
					ls_cod_prodotto_raggruppato,ls_cod_prodotto_movimento, ls_cod_deposito_produzione, ls_flag_tipo_avanz_err, &
					ls_msg_avanz_err, ls_cod_ubicazione_old, ls_cod_lotto_old
					
integer			li_risposta,li_anno_registrazione

datetime			ldt_data_stock[],ldt_oggi,ldt_data_stock_1, ldt_data_reg_ddt, ldt_data_stock_old

dec{4}			ld_costo_mp, ld_costo_lav, ld_costo_tot,ldd_quan_prelevata, ld_quan_raggruppo, ld_valore_movimento

s_stock_produzione	ls_stock_produzione[]


//variabile usata per leggere lo stock del deposito di destinazione
//caso ib_trasf_interno=TRUE  -> trasferimento interno di materie prime che costituiscono un kit semilavorato
s_stock_produzione ls_stock_prod_dest[]
ld_valore_movimento = 1

uo_magazzino luo_mag
uo_costificazione_pf luo_costo
uo_prodotti luo_prodotti


setnull(ll_null)

select flag
into   :ls_flag_mag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	ls_flag_mag_negativo = "N"
elseif sqlca.sqlcode = 100 or ls_flag_mag_negativo <> "S" or isnull(ls_flag_mag_negativo) then
	ls_flag_mag_negativo = "N"
end if



ls_anno_commessa = string (al_anno_commessa)

select cod_tipo_commessa,
		 cod_prodotto,
		 quan_ordine,
		 quan_prodotta,
		 quan_in_produzione,
		 lotto,
		 ubicazione,
		 cod_versione,
		 cod_deposito_prelievo,
		 flag_tipo_avanzamento
into   :ls_cod_tipo_commessa,
		 :ls_cod_prodotto_finito,
		 :ldd_quan_ordine,
		 :ldd_quan_prodotta,
		 :ldd_quan_in_produzione,
		 :ls_lotto,
		 :ls_ubicazione,
		 :ls_cod_versione,
		 :ls_cod_deposito_prelievo,
		 :ls_flag_tipo_avanz_err
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:al_anno_commessa
and    num_commessa=:al_num_commessa;

if sqlca.sqlcode < 0 then
	as_errore = "Errore sul db: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	//commessa inesistente
	as_errore = "La commessa "+string(al_anno_commessa)+"/"+string(al_num_commessa)+" è inesistente!"
	
	uof_log_sistema("Tentativo di avanzamento di una commessa inesistente ("+string(al_anno_commessa)+"/"+string(al_num_commessa)+")","AVANCOMnotext")
	
	return 0
end if

//Donato 02/07/2012 controllo, se la commessa è già in fase chiusa o non + avanzabile esco
if ls_flag_tipo_avanz_err = "9" or ls_flag_tipo_avanz_err = "8" or ls_flag_tipo_avanz_err = "7" or ls_flag_tipo_avanz_err = "1" or ls_flag_tipo_avanz_err = "2" then
	ls_msg_avanz_err = "Tentativo di avanzam. commessa "+string(al_anno_commessa) +"/"+ string(al_num_commessa)+" già chiusa!"
	
	if not isnull(fs_cod_semilavorato) and fs_cod_semilavorato<>"" then
		ls_msg_avanz_err += " - SL: "+fs_cod_semilavorato
	else
		ls_msg_avanz_err += " - NO SL"
	end if
	
	uof_log_sistema(ls_msg_avanz_err, "ERRCHIUSCOM")
	
	as_errore = ls_msg_avanz_err
	return 0
end if
//----------------------------------------------------------------------------------------------------------------

if ib_trasf_interno and (is_cod_deposito_CARICO_mp="" or isnull(is_cod_deposito_CARICO_mp)) then
	as_errore = "Errore: si sta cercando di chiudere una commessa da ddt trasf. interno (kit di semilavorato)  senza aver indicato il deposito di destinazione!" 
	return -1
end if

if ib_trasf_interno then
	
	select tes_bol_ven.data_registrazione, isnull(tes_bol_ven.data_registrazione, tes_bol_ven.data_bolla)
	into :ldt_data_reg_ddt, :idt_data_creazione_mov
	from det_bol_ven
	join tes_bol_ven on 	tes_bol_ven.cod_azienda=det_bol_ven.cod_azienda and
								tes_bol_ven.anno_registrazione=det_bol_ven.anno_registrazione and
								tes_bol_ven.num_registrazione=det_bol_ven.num_registrazione
	where 	det_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and
				det_bol_ven.anno_commessa=:al_anno_commessa and
				det_bol_ven.num_commessa=:al_num_commessa;
	
	//per fare in modo che i movimenti prendano il corretto valore mov e come data registrazione mov. la data della bolla, 
	//allora ho fatto questa questa select per valorizzare ldt_data_reg_ddt e idt_data_creazione_mov
	
end if


select cod_tipo_mov_prel_mat_prime,
		 cod_tipo_mov_ver_prod_finiti,
		 cod_deposito_versamento,
		 flag_tipo_ord_scarico,
		 flag_calcola_costo_pf
into   :ls_cod_tipo_mov_prel_mat_prime,
		 :ls_cod_tipo_mov_ver_prod_finiti,
		 :ls_cod_deposito_versamento,
		 :ls_flag_tipo_ord_scarico,
		 :ls_flag_calcola_costo_pf
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:ls_cod_tipo_commessa;

if sqlca.sqlcode < 0 then
	as_errore = "Errore sul db: " + sqlca.sqlerrtext
	return -1
end if


// EnMe 14-02-2012; aggiunto la possibilità di forzare un deposito di scarico tramite variabile di istanza esterna che fa una forzatura.
if ib_forza_deposito_scarico_mp then
	if not isnull(is_cod_deposito_scarico_mp) and len(is_cod_deposito_scarico_mp) > 0  then 
		ls_cod_deposito_prelievo = is_cod_deposito_scarico_mp
		ls_cod_deposito_versamento = is_cod_deposito_scarico_mp	
	end if
end if


//solo se pianificato (cioè se ho chiamato la funzione dal processo schedulato)
//o non posticipato (cioè ho chiamato la funzione senza aver attivato la var. ib_posticipa, che è il caso normalemnte usato)
if ib_pianificato or not (ib_posticipa) then

	//stefanop 16/07/2012: controllo se son presenti tutti i DDT di  trasferimento (se sono necessari)
	// Al momento è stato chiesto di non bloccare la procedura ma di inviare una mail ad una lista di
	// distribuzione
	// ----------------------------------------------------------------------------------
	if isnull(fs_cod_semilavorato)  and not ib_trasf_interno then
		
		//se è un OPT, allora evito di controllare
		select 	num_riga_appartenenza
		into		:ll_num_riga_app
		from 		det_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_commessa=:al_anno_commessa and
					num_commessa=:al_num_commessa;
		
		//Se la commessa è relativa ad una riga ordine collegata, per ora evito di controllare ...
		if sqlca.sqlcode = 0 then
			if isnull(ll_num_riga_app) or ll_num_riga_app=0 then
				if uof_check_integrita_ddt_trasf_commessa(al_anno_commessa, al_num_commessa, as_errore, true) <0 then
					 return -1
				end if
			end if
		end if
	end if
end if
// ----------------------------------------------------------------------------------



//a partire da questo punto, se posticipato, pianifica ed esci
//									se non posticipato (caso normale) oppure pianificato esegui
if ib_posticipa then
	//inserisci in tabella pianificazioni chiusure commesse
	
	if uof_pianifica(al_anno_commessa, al_num_commessa, fs_cod_semilavorato) < 0 then
		//non sei riuscito a pianificare.
		//trovi il motivo nella log sistema.
		//adesso vai avanti normalmente, in quanto la commessa deve essere chiusa...
	else
		//esci tranquillamente, se ne occuperà il processo di chiusura commesse pianificato
		as_errore = ""
		return 0
	end if
	
end if


if uof_trova_mat_prime_varianti(	ls_cod_prodotto_finito, &
												ls_cod_versione,&
												ref ls_cod_deposito_prelievo, &
												ref ls_materia_prima[],  &
												ref ls_versione_prima[], &
												ref ldd_quantita_utilizzo[], &
												ldd_quan_ordine, &
												al_anno_commessa, &
												al_num_commessa, &
												ll_null, &
												"varianti_commesse", &
												fs_cod_semilavorato, &
												ls_errore) = -1 then
	as_errore = "Errore sulla lettura distinta base: " + ls_errore 
	return -1
end if
	

ldd_quantita_utilizzo_buffer = ldd_quantita_utilizzo
ldd_quantita_utilizzo_dest = ldd_quantita_utilizzo

for ll_i = 1 to upperbound(ls_materia_prima)
	
	if ldd_quantita_utilizzo[ll_i] = 0 then continue  
	
	declare righe_stock dynamic cursor for sqlsa;
	
	ls_sql = &
	"select cod_ubicazione, " + &
	"		 cod_lotto, " + &
	"		 data_stock, " + &
	"		 prog_stock, " + &
	"		 giacenza_stock " + &
	"from   stock " + &
	"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
	"       cod_deposito = '" + ls_cod_deposito_prelievo + "' and " + &
	"       cod_prodotto = '" + ls_materia_prima[ll_i] + "' and flag_stato_lotto ='D' "
	
	if ls_flag_mag_negativo = "N" then
		ls_sql += " and giacenza_stock > 0 "
	end if
		
	
	ls_sql += " order by data_stock desc "
	
	prepare sqlsa from :ls_sql;
	
	open dynamic righe_stock;
	
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in lettura stock.~nErrore in open cursore righe_stock: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while 1 = 1
		fetch righe_stock
		into  :ls_cod_ubicazione_1,
				:ls_cod_lotto_1,
				:ldt_data_stock_1,
				:ll_prog_stock_1,
				:ldd_giacenza_stock;
				
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul DB durante lettura cursore righe_stock: " + sqlca.sqlerrtext
			close righe_stock;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then
			
			if ldd_quantita_utilizzo[ll_i] > 0 and ls_flag_mag_negativo = "N" then
				ll_num_stock++
				ls_stock_produzione[ll_num_stock].cod_prodotto = ls_materia_prima[ll_i]
				ls_stock_produzione[ll_num_stock].cod_deposito = ls_cod_deposito_prelievo
				ls_stock_produzione[ll_num_stock].cod_ubicazione = ls_cod_ubicazione_old
				ls_stock_produzione[ll_num_stock].cod_lotto = ls_cod_lotto_old
				ls_stock_produzione[ll_num_stock].data_stock = ldt_data_stock_old
				ls_stock_produzione[ll_num_stock].prog_stock = ll_prog_stock_old
				ls_stock_produzione[ll_num_stock].quan_prelievo = ldd_quantita_utilizzo[ll_i]
				ldd_quantita_utilizzo[ll_i] = 0
				exit
			end if
		end if
		
		//leggo lo stock e lo memorizzo nell'array
		if ldd_giacenza_stock >= ldd_quantita_utilizzo[ll_i] or ls_flag_mag_negativo = "S" then
			ll_num_stock++
			ls_stock_produzione[ll_num_stock].cod_prodotto = ls_materia_prima[ll_i]
			ls_stock_produzione[ll_num_stock].cod_deposito = ls_cod_deposito_prelievo
			ls_stock_produzione[ll_num_stock].cod_ubicazione = ls_cod_ubicazione_1
			ls_stock_produzione[ll_num_stock].cod_lotto = ls_cod_lotto_1
			ls_stock_produzione[ll_num_stock].data_stock = ldt_data_stock_1
			ls_stock_produzione[ll_num_stock].prog_stock = ll_prog_stock_1
			ls_stock_produzione[ll_num_stock].quan_prelievo = ldd_quantita_utilizzo[ll_i]
			ldd_quantita_utilizzo[ll_i] = 0
			exit
		else
			ldd_quantita_utilizzo[ll_i] = ldd_quantita_utilizzo[ll_i] - ldd_giacenza_stock
			ll_num_stock++
			ls_stock_produzione[ll_num_stock].cod_prodotto = ls_materia_prima[ll_i]
			ls_stock_produzione[ll_num_stock].cod_deposito = ls_cod_deposito_prelievo
			ls_stock_produzione[ll_num_stock].cod_ubicazione = ls_cod_ubicazione_1
			ls_stock_produzione[ll_num_stock].cod_lotto = ls_cod_lotto_1
			ls_stock_produzione[ll_num_stock].data_stock = ldt_data_stock_1
			ls_stock_produzione[ll_num_stock].prog_stock = ll_prog_stock_1
			ls_stock_produzione[ll_num_stock].quan_prelievo = ldd_giacenza_stock
		end if
		
		ls_cod_ubicazione_old = ls_cod_ubicazione_1
		ls_cod_lotto_old = ls_cod_lotto_1
		ldt_data_stock_old = ldt_data_stock_1
		ll_prog_stock_old = ll_prog_stock_1
	loop
	
	close righe_stock;
	
	// ** Modifica Michele per consentire tramite il parametro aziendale CMN il magazzino negativo 17/04/2003 **
	
	if ldd_quantita_utilizzo[ll_i] > 0 and ls_flag_mag_negativo = "N" then
		as_errore = "Errore: Manca giacenza stock per la materia prima " + ls_materia_prima[ll_i] + ". Domanda all'utente:Come si può pensare di fare scatenare un avanzamento automatico di produzione se poi il magazzino presenta delle mancanze di scorta? Meglio che chi sta compiendo questa operazione controlli bene le giacenze a magazzino di tutte le materie prime coinvolte nello scarico simultaneo."
		return -1
	end if
	
	// ************************************ Fine Modifica ******************************************************
	
next

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

//devi caricare anche gli stock per il deposito destinazione delle materie prime del kit del semilavorato
if ib_trasf_interno then
	
	//stock carico e stock scarico coincidono, a parte che per il deposito
	ls_stock_prod_dest = ls_stock_produzione
	for ll_i = 1 to upperbound(ls_stock_prod_dest[])
		ls_stock_prod_dest[ll_i].cod_deposito = is_cod_deposito_CARICO_mp
	next

	setnull(ls_cod_deposito[2])
	setnull(ls_cod_ubicazione[2])
	setnull(ls_cod_lotto[2])
	setnull(ldt_data_stock[2])
	setnull(ll_prog_stock[2])
	setnull(ls_cod_cliente[2])
	setnull(ls_cod_fornitore[2])
	
end if



setnull(ls_cod_ubicazione_pf)
setnull(ls_cod_lotto_pf)

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Donato 09/07/2012
//se risulta valorizzata tale variabile datetime di istanza, in avanzamento commessa prenderà tale data
//utile nel processo di ricostruzione movimenti di una commessa, viene passata la data_chiusura pre-esistente della commessa
if not isnull(idt_data_creazione_mov) and year(date(idt_data_creazione_mov))>1980 then
	ldt_oggi = datetime(date(idt_data_creazione_mov), 00:00:00)
else
	ldt_oggi = datetime(today())
end if

//fine modifica -----------------------------------------------------------------------------------------------------------------------------------------------------


ldd_quantita_prodotta_pf = ldd_quan_ordine

	
	

// (inizio) TEST DET_TIPI_MOVIMENTI ***********************************************************

declare righe_det_tipi_m cursor for
select  cod_ubicazione,
		  cod_lotto,
		  flag_cliente,
		  flag_fornitore
from    det_tipi_movimenti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_tipo_movimento=:ls_cod_tipo_mov_ver_prod_finiti;

open righe_det_tipi_m;

do while 1=1
	fetch righe_det_tipi_m
	into  :ls_cod_ubicazione_test,
			:ls_cod_lotto_test,
			:ls_flag_cliente,
			:ls_flag_fornitore;

	if sqlca.sqlcode = 100 then exit
		
	if sqlca.sqlcode < 0 then
		as_errore = "Si e' verificato un errore in lettura det_tipi_movimenti: " + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ls_ubicazione) or ls_ubicazione="" then
		if isnull(ls_cod_ubicazione_test) then
			as_errore = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
						  " come versamento di prodotto finito per commesse, poiche' il codice ubicazione non e' stato indicato."
			return -1
		end if

	else
		ls_cod_ubicazione_pf = ls_ubicazione
		
	end if
	
	if isnull(ls_lotto) or ls_lotto="" then
		
		if isnull(ls_cod_lotto_test) then
			as_errore = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
						  " come versamento di prodotto finito per commesse, poiche' il codice lotto non e' stato indicato."
			close righe_det_tipi_m;
			return -1
		end if

		
	else
		ls_cod_lotto_pf = ls_lotto
	end if
	
	if ls_flag_cliente='S' then
		as_errore = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
					  " come versamento di prodotto finito per commesse, poiche' viene richiesto il codice cliente." 
		close righe_det_tipi_m;
		return -1
	end if

	if ls_flag_fornitore='S' then
		as_errore = "Non e' possibile utilizzare il movimento di magazzino "+ ls_cod_tipo_mov_ver_prod_finiti + &
					  " come versamento di prodotto finito per commesse, poiche' viene richiesto il codice fornitore." 
		close righe_det_tipi_m;
		return -1
	end if

loop

close righe_det_tipi_m;

// (fine) TEST DET_TIPI_MOVIMENTI 

// MOVIMENTI MAGAZZINO MATERIE PRIME (inizio)
for ll_i = 1 to UpperBound(ls_stock_produzione)
	
	ls_cod_prodotto = ls_stock_produzione[ll_i].cod_prodotto
	ls_cod_deposito[1] = ls_stock_produzione[ll_i].cod_deposito
	ls_cod_ubicazione[1] = ls_stock_produzione[ll_i].cod_ubicazione
	ls_cod_lotto[1] = ls_stock_produzione[ll_i].cod_lotto
	ldt_data_stock[1] = ls_stock_produzione[ll_i].data_stock
	ll_prog_stock[1] = ls_stock_produzione[ll_i].prog_stock
	ldd_quan_prelevata = ls_stock_produzione[ll_i].quan_prelievo
	
	ldd_quan_prelevata = round(ldd_quan_prelevata,4)
	
	//devo fare anche il carico nel deposito destinazione
	if ib_trasf_interno then
		ls_cod_deposito[2] = ls_stock_prod_dest[ll_i].cod_deposito
		ls_cod_ubicazione[2] = ls_stock_prod_dest[ll_i].cod_ubicazione
		ls_cod_lotto[2] = ls_stock_prod_dest[ll_i].cod_lotto
		ldt_data_stock[2] = ls_stock_prod_dest[ll_i].data_stock
		ll_prog_stock[2] = ls_stock_prod_dest[ll_i].prog_stock
	end if
	
	if f_crea_dest_mov_mag(ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto, ls_cod_deposito[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
	  return -1
	end if

	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									 ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto) = -1 then
	
	  as_errore = "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino."
	  return -1
	end if
	
	if ib_trasf_interno then
		luo_prodotti = create uo_prodotti
		ld_valore_movimento = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_data_reg_ddt, as_errore)
		destroy luo_prodotti
		
		if ld_valore_movimento < 0 then
			return -1
			
		elseif ld_valore_movimento = 0 then
			ld_valore_movimento = 1
			
			uof_log_sistema("Nessun prezzo di trasferimento reperibile (commessa "+string(al_anno_commessa)+"/"+string(al_num_commessa)+"), Prodotto: "+ls_cod_prodotto,"ERRCHIUSCOMprezzo")
		end if
		
	end if
	
	luo_mag = create uo_magazzino

	luo_mag.uof_set_flag_commessa("S")
	
	luo_mag.ib_salta_controllo=true
	luo_mag.ib_avvisa=false

	li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
										  ls_cod_tipo_mov_prel_mat_prime, &
										  "N", &
										  ls_cod_prodotto, &
										  ldd_quan_prelevata, &
										  ld_valore_movimento, &
										  al_num_commessa, &
										  ldt_oggi, &
										  ls_anno_commessa, &
										  ll_anno_reg_des_mov, &
										  ll_num_reg_des_mov, &
										  ls_cod_deposito[], &
										  ls_cod_ubicazione[], &
										  ls_cod_lotto[], &
										  ldt_data_stock[], &
										  ll_prog_stock[], &
										  ls_cod_fornitore[], &
										  ls_cod_cliente[], &
										  ll_anno_registrazione[], &
										  ll_num_registrazione[], &
										  as_errore)
										  
	
	if li_risposta=-1 then
		
		//se per disgrazia la variabile errore è vuota valorizzala cosi ...
		if as_errore="" or isnull(as_errore) then
			as_errore = "Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto &
						 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
						 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
						 + ", prog_stock:" + string(ll_prog_stock[1])
		end if
		
		return -1
	end if
	
	f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
	
	// stefanop 06/12/2011: movimento anche il prodotto raggruppato
	
	if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
										  ls_cod_tipo_mov_prel_mat_prime, &
										  "N", &
										  ls_cod_prodotto, &
										  ldd_quan_prelevata, &
										  1, &
										  al_num_commessa, &
										  ldt_oggi, &
										  ls_anno_commessa, &
										  ll_anno_reg_des_mov, &
										  ll_num_reg_des_mov, &
										  ls_cod_deposito[], &
										  ls_cod_ubicazione[], &
										  ls_cod_lotto[], &
										  ldt_data_stock[], &
										  ll_prog_stock[], &
										  ls_cod_fornitore[], &
										  ls_cod_cliente[], &
										  ll_anno_registrazione_rag[], &
										  ll_num_registrazione_rag[],&
										  long(ls_anno_commessa),&
										  ll_anno_registrazione[1],&
										  ll_num_registrazione[1], &
										  as_errore) = -1 then
		
		//se per disgrazia la variabile errore è vuota valorizzala cosi ...
		if as_errore="" or isnull(as_errore) then
			as_errore = "Errore su movimenti magazzino, prodotto raggruppato: " + ls_cod_prodotto
		end if
					 
		destroy luo_mag
		return -1
	end if					
	// ----
	
	destroy luo_mag

next


//****************************************************************************************************************
// DISIMPEGNO LE MP CONTROLLANDO CONTEMPORANEAMENTE ANCHE LA TABELLA IMPEGNO_MAT_PRIME_COMMESSA

for ll_i = 1 to upperbound(ls_materia_prima)

	ls_cod_prodotto = ls_materia_prima[ll_i]
	ldd_quan_prelevata = ldd_quantita_utilizzo_buffer[ll_i]

	select quan_impegnata_attuale
	into   :ldd_quan_impegnata_attuale
	from   impegno_mat_prime_commessa
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :al_anno_commessa and
			 num_commessa  = :al_num_commessa and
			 cod_prodotto  = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore sul db " + sqlca.sqlerrtext
		return -1
	end if
	
	if ldd_quan_impegnata_attuale >= ldd_quan_prelevata then
		
		update impegno_mat_prime_commessa
		set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quan_prelevata
		where  cod_azienda   = :s_cs_xx.cod_azienda and
		 		 anno_commessa = :al_anno_commessa and
				 num_commessa  = :al_num_commessa and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul db " + sqlca.sqlerrtext
			return -1
		end if

		update anag_prodotti																		
		set 	 quan_impegnata = quan_impegnata - :ldd_quan_prelevata
		where  cod_azienda  = :s_cs_xx.cod_azienda and
			  	 cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul db " + sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ldd_quan_prelevata, "M")
			
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ld_quan_raggruppo
			where  cod_azienda=:s_cs_xx.cod_azienda and
					 cod_prodotto=:ls_cod_prodotto_raggruppato;
			
			if sqlca.sqlcode < 0 then
				as_errore = "Errore sul db " + sqlca.sqlerrtext
				return -1
			end if
			
		end if
		
		
	else
		// tutta la quantità impegnata deve essere disimpegnata
		update impegno_mat_prime_commessa
		set 	 quan_impegnata_attuale = 0
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :al_anno_commessa and
				 num_commessa  = :al_num_commessa and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul db " + sqlca.sqlerrtext
			return -1
		end if

		update anag_prodotti																		
		set 	 quan_impegnata = quan_impegnata - :ldd_quan_impegnata_attuale
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 cod_prodotto  = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul db " + sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then

			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ldd_quan_impegnata_attuale, "M")
			
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ld_quan_raggruppo
			where  cod_azienda=:s_cs_xx.cod_azienda  and
					 cod_prodotto=:ls_cod_prodotto_raggruppato;
			
			if sqlca.sqlcode < 0 then
				as_errore = "Errore sul db " + sqlca.sqlerrtext
				return -1
			end if
			
		end if
	
	end if

	
next
// MOVIMENTI MAGAZZINO MATERIE PRIME (fine)


if not ib_trasf_interno then

	// MOVIMENTI MAGAZZINO PRODOTTO FINITO (inizio)
	
	// 	in caso di presenza semilavorato bisogna:
	//	eseguire un select della variante per trovare il deposito corretto
	// 	Ammetto che cercare la variante in questo modo è un po' una schifezza, ma in teoria non dovrebbero esserci altri casi e dovrei beccare sempre 1 record
	if not isnull(fs_cod_semilavorato) then
		select 	cod_deposito_produzione
		into 		:ls_cod_deposito_produzione
		from 		varianti_commesse
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :al_anno_commessa and
					num_commessa = :al_num_commessa and
					cod_prodotto_figlio = :fs_cod_semilavorato;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore in ricerca della variante del semilavorato " + fs_cod_semilavorato + " nelle varianti commesse " + sqlca.sqlerrtext
			rollback;
			return -1
		end if
	
		ls_cod_deposito_versamento = ls_cod_deposito_produzione
	
		// sostituisco il prodotto da caricare con il semilavorato per fare il versamento nel deposito corretto
		
		ls_cod_prodotto_finito = fs_cod_semilavorato
	end if
	
	
	ls_cod_deposito[1] = ls_cod_deposito_versamento
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	if not isnull(ls_cod_ubicazione_pf) then
		ls_cod_ubicazione[1] = ls_cod_ubicazione_pf
	end if
	
	if not isnull(ls_cod_lotto_pf) then
		ls_cod_lotto[1] = ls_cod_lotto_pf
	end if
	

	if f_crea_dest_mov_mag(ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto_finito, ls_cod_deposito[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
	
	  return -1
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
									 ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto_finito) = -1 then
	
	  as_errore = "Si è verificato un errore in fase di verifica destinazioni movimenti magazzino."
	  return -1
	end if
	
	//non è detto sempre che il semilavoratio prodotto abbia quantità pari a quella ordinata/commissionata (per gibus magari è cosi ma per altri assolutamente no)
	if not isnull( fs_cod_semilavorato) and fs_cod_semilavorato<>"" and ib_quan_semilavorato and id_quan_semilavorato>0 then
		ldd_quantita_prodotta_pf = id_quan_semilavorato
		ib_quan_semilavorato = false
		id_quan_semilavorato = 0
	end if
	
	
	//***************** INIZIO CALCOLO DEL COSTO DEL PRODOTTO FINITO
	if ls_flag_calcola_costo_pf ='S' then
		luo_costo = create uo_costificazione_pf
		
		luo_costo.uof_costificazione_pf( sqlca, s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa, ldd_quantita_prodotta_pf, ref ld_costo_mp, ref ld_costo_lav, ref ld_costo_tot, ref ls_errore)

		// uso il costo preventivo perchè in genere con il configuratore si genera una distinta abbastanza verosimile.
		/* per usare il calcoilo sul consuntivo bisognerebbe usare la rilevazione della produzione.
		li_risposta = luo_costo.uof_costificazione_pf(sqlca,&
																	 s_cs_xx.cod_azienda,&
																	 ls_cod_prodotto_finito,&
																	 ls_cod_versione,&
																	 ldd_quantita_prodotta_pf,&
																	 ld_costo_mp,&
																	 ld_costo_lav,&
																	 ld_costo_tot,&
																 ls_errore) */
		destroy luo_costo
		
		if li_risposta < 0 then
			g_mb.error(g_str.format("Errore in costificazione prodotto finito: $1",ls_errore))
			return -1
		end if
	else
		ld_costo_mp = 0
		ld_costo_lav = 0
		ld_costo_tot = 0
	end if
	
	//***************** FINE CALCOLO DEL COSTO DEL PRODOTTO FINITO
	
	luo_mag = create uo_magazzino
	
	luo_mag.uof_set_flag_commessa("S")
	
	li_risposta = luo_mag.uof_movimenti_mag(ldt_oggi, &
										  ls_cod_tipo_mov_ver_prod_finiti, &
										  "N", &
										  ls_cod_prodotto_finito, &
										  ldd_quantita_prodotta_pf, &
										  (ld_costo_tot/ldd_quantita_prodotta_pf), &
										  al_num_commessa, &
										  ldt_oggi, &
										  ls_anno_commessa, &
										  ll_anno_reg_des_mov, &
										  ll_num_reg_des_mov, &
										  ls_cod_deposito[], &
										  ls_cod_ubicazione[], &
										  ls_cod_lotto[], &
										  ldt_data_stock[], &
										  ll_prog_stock[], &
										  ls_cod_fornitore[], &
										  ls_cod_cliente[], &
										  ll_anno_registrazione[], &
										  ll_num_registrazione[], &
										  as_errore)
										  
	if li_risposta=-1 then
		
		//se per disgrazia la variabile errore è vuota valorizzala cosi ...
		if as_errore="" or isnull(as_errore) then
			as_errore = "Errore su movimenti magazzino, prodotto:" + ls_cod_prodotto &
						 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
						 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
						 + ", prog_stock:" + string(ll_prog_stock[1])
		end if
		
		return -1
	end if
	
	f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
	
	
	// stefanop 06/12/2011: movimento anche il prodotto raggruppato
	if luo_mag.uof_movimenti_mag_ragguppato(ldt_oggi, &
										  ls_cod_tipo_mov_ver_prod_finiti, &
										  "N", &
										  ls_cod_prodotto_finito, &
										  ldd_quantita_prodotta_pf, &
										  (ld_costo_tot/ldd_quantita_prodotta_pf), &
										  al_num_commessa, &
										  ldt_oggi, &
										  ls_anno_commessa, &
										  ll_anno_reg_des_mov, &
										  ll_num_reg_des_mov, &
										  ls_cod_deposito[], &
										  ls_cod_ubicazione[], &
										  ls_cod_lotto[], &
										  ldt_data_stock[], &
										  ll_prog_stock[], &
										  ls_cod_fornitore[], &
										  ls_cod_cliente[], &
										  ll_anno_registrazione_rag[], &
										  ll_num_registrazione_rag[],&
										  long(ls_anno_commessa),&
										  ll_anno_registrazione[1],&
										  ll_num_registrazione[1], &
										  as_errore) = -1 then
								
		//se per disgrazia la variabile errore è vuota valorizzala cosi ...
		if as_errore="" or isnull(as_errore) then
			as_errore = "Errore su movimenti magazzino, prodotto: " + ls_cod_prodotto_movimento &
							 + ", deposito:" + ls_cod_deposito[1] + ", ubicazione:" + ls_cod_ubicazione[1] &
							 + ", cod_lotto:" + ls_cod_lotto[1] + ", data_stock:" + string(ldt_data_stock[1]) &
							 + ", prog_stock:" + string(ll_prog_stock[1])
		end if
		
		destroy luo_mag
		return -1
	end if					
	// ----
	
	
	destroy luo_mag
	// MOVIMENTI MAGAZZINO PRODOTTO FINITO (fine)

end if		// fine if not ib_trasf_interno ...

// ---------------------------->>>>>
// EnMe 10-2-2012: tutta questa parte viene fatta solo se sto effettivamente chiudendo la commessa.
// -------------------------------------
if isnull( fs_cod_semilavorato) then

	ls_flag_tipo_avanzamento = "7" // Chiusa da Chiusura Automatica
	
	update anag_commesse
	set	 quan_prodotta = quan_prodotta + :ldd_quantita_prodotta_pf,
			 flag_tipo_avanzamento = :ls_flag_tipo_avanzamento,
			 data_chiusura = :ldt_oggi,
			 tot_valore_prodotti =:ld_costo_mp,
			 tot_valore_servizi=:ld_costo_lav,
			 tot_valore_pf=: ld_costo_tot
	where	 cod_azienda = :s_cs_xx.cod_azienda  
	and	 anno_commessa = :al_anno_commessa
	and	 num_commessa = :al_num_commessa;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore sul db " + sqlca.sqlerrtext
		return -1
	end if
	
	if not ib_trasf_interno then
	
		// EnMe 22/03/2010 ; per sicurezza azzero tutto l'impegnato della commessa
		// in questa funzione la commessa viene chiusa cpompletamente quindi non vedo il bisogno
		// di lasciare q.ta impegnata allegata alla commessa.
		update impegno_mat_prime_commessa
		set    quan_impegnata_attuale = 0
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :al_anno_commessa and
				 num_commessa  = :al_num_commessa;
				 
		if sqlca.sqlcode < 0 then
			as_errore = "Errore sul db " + sqlca.sqlerrtext
			return -1
		end if
		
		
		
		
		// dopo il carico del prodotto finito a magazzino aggiorno la tabella varianti_stock
		
		
		for ll_i = 1 to upperbound(ls_cod_deposito)
			declare righe_var_com cursor for
			select cod_prodotto_padre,
					 num_sequenza,
					 cod_prodotto_figlio,
					 cod_versione,
					 cod_versione_figlio,
					 cod_prodotto,
					 cod_versione_variante,
					 quan_tecnica,
					 quan_utilizzo,
					 dim_x,
					 dim_y,
					 dim_z,
					 dim_t,
					 coef_calcolo,
					 flag_esclusione,
					 formula_tempo,
					 des_estesa,
					 flag_materia_prima
			from   varianti_commesse
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					 anno_commessa = :al_anno_commessa	and    
					 num_commessa = :al_num_commessa;
			
			open righe_var_com;
			
			do while 1=1
				fetch righe_var_com
				into  :ls_cod_prodotto_padre,
						:ll_num_sequenza,
						:ls_cod_prodotto_figlio,
						:ls_cod_versione,
						:ls_cod_versione_figlio,
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante,
						:ldd_quan_tecnica,
						:ldd_quan_utilizzo,
						:ldd_dim_x,
						:ldd_dim_y,
						:ldd_dim_z,
						:ldd_dim_t,
						:ldd_coef_calcolo,
						:ls_flag_esclusione,
						:ls_formula_tempo,
						:ls_des_estesa,
						:ls_flag_materia_prima;
				
				if sqlca.sqlcode = 100 then exit
				
				if sqlca.sqlcode < 0 then
					as_errore = "Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext
					close righe_var_com;
					return -1
				end if
		
				select max(progressivo)
				into   :ll_progressivo
				from   varianti_stock
				where  cod_azienda = :s_cs_xx.cod_azienda		and    
						 cod_prodotto = :ls_cod_prodotto_finito	and    
						 cod_deposito = :ls_cod_deposito[ll_i]    and    
						 cod_ubicazione = :ls_cod_ubicazione[ll_i] and    
						 cod_lotto = :ls_cod_lotto[ll_i]		and    
						 data_stock = :ldt_data_stock[ll_i]	and    
						 prog_stock = :ll_prog_stock[ll_i]  and    
						 cod_prodotto_padre = :ls_cod_prodotto_padre and  	 
						 num_sequenza = :ll_num_sequenza    and    
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio and  
						 cod_versione_figlio = :ls_cod_versione_figlio and
						 cod_versione = :ls_cod_versione;
				
				if sqlca.sqlcode < 0 then
					as_errore = "Si è verificato un errore in fase di aggiornamento tabella varianti stock:" + sqlca.sqlerrtext
					close righe_var_com;
					return -1
				end if
				
				if isnull(ll_progressivo) or ll_progressivo=0 then
					ll_progressivo = 1
				else
					ll_progressivo++
				end if
				
				INSERT INTO varianti_stock  
					( cod_azienda,   
					  cod_prodotto,   
					  cod_deposito,   
					  cod_ubicazione,   
					  cod_lotto,   
					  data_stock,   
					  prog_stock,   
					  cod_prodotto_padre,   
					  num_sequenza,   
					  cod_prodotto_figlio, 
					  cod_versione_figlio,
					  cod_versione, 
					  progressivo,
					  cod_prodotto_variante,   
					  quan_tecnica,   
					  quan_utilizzo,   
					  dim_x,   
					  dim_y,   
					  dim_z,   
					  dim_t,   
					  coef_calcolo,   
					  flag_esclusione,   
					  formula_tempo,
					  des_estesa,
					  flag_materia_prima)  
				VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_prodotto_finito,   
					  :ls_cod_deposito[ll_i],   
					  :ls_cod_ubicazione[ll_i],   
					  :ls_cod_lotto[ll_i],   
					  :ldt_data_stock[ll_i],   
					  :ll_prog_stock[ll_i],   
					  :ls_cod_prodotto_padre,   
					  :ll_num_sequenza,   
					  :ls_cod_prodotto_figlio, 
					  :ls_cod_versione_figlio,
					  :ls_cod_versione,
					  :ll_progressivo,
					  :ls_cod_prodotto_variante,   
					  :ldd_quan_tecnica,   
					  :ldd_quan_utilizzo,   
					  :ldd_dim_x,   
					  :ldd_dim_y,   
					  :ldd_dim_z,   
					  :ldd_dim_t,   
					  :ldd_coef_calcolo,   
					  :ls_flag_esclusione,   
					  :ls_formula_tempo,
					  :ls_des_estesa,
					  :ls_flag_materia_prima)  ;
				
				if sqlca.sqlcode < 0 then
					as_errore = "Si è verificato un errore in fase di aggiornamento tabella varianti stock: " + sqlca.sqlerrtext
					close righe_var_com;
					return -1
				end if
			loop
			close righe_var_com;
		next
		
		// fine aggiornamento tabella varianti_stock	
		
		
		// cancellazione tabella distinte_taglio_calcolate
		
		guo_functions.uof_get_parametro( "DT", ls_parametro)
		
		guo_functions.uof_get_parametro( "DT1", lb_dt1)
		
		if sqlca.sqlcode < 0 then
			as_errore = "Si è verificato un errore durante la lettura tabella parametri. Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_parametro='S' and not lb_dt1 then
			
			select anno_registrazione,
					 num_registrazione,
					 prog_riga_ord_ven
			into   :li_anno_registrazione,
					 :ll_num_reg,
					 :ll_prog_riga_ord_ven
			from   det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:al_anno_commessa
			and    num_commessa=:al_num_commessa;
		
			if sqlca.sqlcode < 0 then
				as_errore = "Si è verificato un errore durante la lettura tabella det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
		
			delete distinte_taglio_calcolate
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:li_anno_registrazione
			and    num_registrazione=:ll_num_reg
			and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
			
			if sqlca.sqlcode < 0 then
				as_errore = "Si è verificato un errore durante la cancellazione tabella distinte_taglio_calcolate. Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
		
		end if
	
	end if 	//fine if not ib_trasf_interno ....
	
end if

return 0
end function

public function integer uof_trova_mat_prime_varianti (string prodotto_finito, string cod_versione, ref string fs_cod_deposito_produzione, ref string materia_prima[], ref string versione_prima[], ref double quantita_utilizzo[], double quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, string fs_cod_semilavorato, ref string fs_errore);s_trova_mp_varianti  lstr_trova_mp_varianti[]
integer li_risposta
long ll_i



if uof_trova_mat_prime_varianti(prodotto_finito, &
													  cod_versione, & 
													  0,&
													  ref fs_cod_deposito_produzione, &
													  ref lstr_trova_mp_varianti[], &
													  quantita_utilizzo_prec, & 
													  anno_registrazione, &
													  num_registrazione, &
													  prog_registrazione, &
													  tabella_varianti, &
													  fs_cod_semilavorato, &
													  ref fs_errore) = 0 then
													  
	for ll_i = 1 to upperbound(lstr_trova_mp_varianti) 
		materia_prima[ll_i] = lstr_trova_mp_varianti[ll_i].cod_materia_prima
		versione_prima[ll_i] = lstr_trova_mp_varianti[ll_i].cod_versione_mp
		quantita_utilizzo[ll_i] = lstr_trova_mp_varianti[ll_i].quan_utilizzo
	next

else
	return -1
end if

return 0
end function

private function integer uof_fasi_lavorazione_det (long al_anno_documento, long al_num_documento, long al_prog_riga, string as_cod_prodotto_padre, string as_cod_versione_padre, long al_num_sequenza, string as_cod_prodotto_figlio, string as_cod_versione_figlio, string as_tabella_varianti, ref string as_cod_deposito_origine[], ref string as_cod_deposito_produzione[], ref string as_cod_reparto_produzione[], ref string as_messaggio);/**
 * stefanop
 * 30/01/2012
 *
 * Controllo che il prodotto abbia una fase di lavorazione
 **/
 
string ls_sql, ls_error, ls_empty[], ls_cod_deposito, ls_cod_deposito_origine, ls_cod_deposito_giro
long ll_rows, ll_i
datastore lds_store, lds_store_reparto

as_cod_deposito_origine = ls_empty
as_cod_deposito_produzione = ls_empty
as_cod_reparto_produzione = ls_empty

//se l'ordine non è sfuso, controllo se il giro consegna ha il deposito e lo metto nella variabile "ls_cod_deposito_giro"
select tes_giri_consegne.cod_deposito
into :ls_cod_deposito_giro
from tes_ord_ven
join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and
								tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven
join tes_giri_consegne on	tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
									tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna
where	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione = :al_anno_documento and
			tes_ord_ven.num_registrazione = :al_num_documento and
			tab_tipi_ord_ven.flag_tipo_bcl in ('A','C');


// verifico che non ci sia il deposito nella variante
ls_sql = "select cod_deposito_produzione from " + as_tabella_varianti + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione=" + string(al_anno_documento) + &
			" and num_registrazione=" + string(al_num_documento) + " and cod_prodotto_padre='" + as_cod_prodotto_padre + "' and cod_versione='" + as_cod_versione_padre + "' " + &
			" and num_sequenza=" + string(al_num_sequenza) + " and cod_prodotto_figlio='" + as_cod_prodotto_figlio + "' and cod_versione_figlio='" + as_cod_versione_figlio + "' "
			
choose case as_tabella_varianti
	// TODO AGGIUNGERE LE ALTRE TABELLE
	case "varianti_det_ord_ven"
		ls_sql  += " AND prog_riga_ord_ven=" + string(al_prog_riga)
		
		// recupero deposito origine ordine
		select cod_deposito
		into :ls_cod_deposito_origine
		from tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_documento and
				 num_registrazione = :al_num_documento;
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore durante la ricerca ordine (uof_fasi_lavorazione_det).~r~n" + sqlca.sqlerrtext
			return -1
		end if
					
end choose

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_messaggio)

if ll_rows < 0 then 
	return -1
elseif ll_rows > 0 then
		
	ls_cod_deposito = lds_store.getitemstring(1,1)
	
	ls_sql = "select cod_reparto from " + as_tabella_varianti + "_prod where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione=" + string(al_anno_documento) + &
			" and num_registrazione=" + string(al_num_documento) + " and cod_prodotto_padre='" + as_cod_prodotto_padre + "' and cod_versione='" + as_cod_versione_padre + "' " + &
			" and num_sequenza=" + string(al_num_sequenza) + " and cod_prodotto_figlio='" + as_cod_prodotto_figlio + "' and cod_versione_figlio='" + as_cod_versione_figlio + "' "
				
	choose case as_tabella_varianti
		// TODO AGGIUNGERE LE ALTRE TABELLE
		case "varianti_det_ord_ven"
			ls_sql  += " AND prog_riga_ord_ven=" + string(al_prog_riga)
			
	end choose
	
	ll_rows = guo_functions.uof_crea_datastore(lds_store_reparto, ls_sql, as_messaggio)

	if ll_rows < 0 then 
		return -1
	elseif ll_rows = 0 then
		as_messaggio= "Attenzione è stato indicato un deposito nella variante ma non sono stati specificati i relativi reparti. "
		
		choose case as_tabella_varianti
			// TODO AGGIUNGERE LE ALTRE TABELLE
			case "varianti_det_ord_ven"
				as_messaggio += "Varianti Ordini: " + string(al_anno_documento) +  "/"+ string(al_num_documento) + "/" + string(al_prog_riga)
		end choose
		
		return -1
		
	else
		for ll_i = 1 to ll_rows
			as_cod_deposito_origine[ll_i] =ls_cod_deposito_origine
			as_cod_deposito_produzione[ll_i] = ls_cod_deposito
			as_cod_reparto_produzione[ll_i] = lds_store_reparto.getitemstring(ll_i, 1)	
		next
	end if
		
else
	// se non c'erano dati nelle varianti, procedo cercando dati nelle fasi

	ls_sql = " SELECT t.cod_deposito_origine, t.cod_deposito_produzione, d.cod_reparto_produzione " + &
				" FROM tes_fasi_lavorazione_det as t " + &
				" JOIN tes_fasi_lavorazione_prod as d on " + &
					" t.cod_azienda = d.cod_azienda and " + &
					" t.cod_prodotto = d.cod_prodotto and " + &
					" t.cod_deposito_produzione = d.cod_deposito_produzione " + &
				" where " + &
					" t.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					" t.cod_prodotto = '" + as_cod_prodotto_figlio + "' and " + &
					" t.cod_versione = '" + as_cod_versione_figlio + "' "
	
	ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_messaggio)
	
	if ll_rows < 0 then 
		return -1
	else
		
		for ll_i = 1 to ll_rows
			as_cod_deposito_origine[ll_i] = lds_store.getitemstring(ll_i, 1)
			as_cod_deposito_produzione[ll_i] = lds_store.getitemstring(ll_i, 2)
			as_cod_reparto_produzione[ll_i] = lds_store.getitemstring(ll_i, 3)	
		next
		
	end if
end if

return 0
end function

public function integer uof_estrai_reparto_stabilimento (string fs_cod_prodotto_padre, string fs_cod_versione_padre, string fs_tipo_gestione, long fl_anno_ordine, long fl_num_ordine, ref string fs_cod_deposito_produzione, ref string fs_cod_reparto_produzione[], ref string fs_errore);/* EnMe 07/02/2012 Estrae il deposito ed il reparto corretti dalla fase di lavorazione in base al deposito commerciale dell'ordine

Return	 -1 = Errore
			  0 = Trovato, tutto ok
			  1 = Al ramo di distina non corrisponde una fase di lavorazione
*/
string ls_cod_deposito_commerciale, ls_cod_deposito_origine, ls_flag_stampa_sempre,ls_cod_lavorazione, ls_cod_reparto,ls_sql, ls_cod_giro_consegna, ls_cod_deposito_giro
long ll_index, ll_return, ll_i
datastore lds_data

if isnull(fs_cod_prodotto_padre) or len(fs_cod_prodotto_padre) < 1 then 
	setnull(fs_cod_deposito_produzione)
	setnull(fs_cod_reparto_produzione)
	return 0
end if
	
if isnull(fs_cod_versione_padre) or len(fs_cod_versione_padre) < 1 then 
	fs_errore = "Attenzione: per il prodotto " + fs_cod_prodotto_padre + " non è stata indicata la versione (uof_estrai_reparto_stabilimento)"
	return 0
end if

setnull(ls_cod_deposito_commerciale)

choose case fs_tipo_gestione

	case "ORD_VEN"
	
		select cod_deposito, cod_giro_consegna
		into 	:ls_cod_deposito_commerciale, :ls_cod_giro_consegna
		from 	tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :fl_anno_ordine and
				num_registrazione = :fl_num_ordine;
				
	case "OFF_VEN"
		
		select cod_deposito
		into 	:ls_cod_deposito_commerciale
		from 	tes_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :fl_anno_ordine and
				num_registrazione = :fl_num_ordine;

		setnull(ls_cod_giro_consegna)

end choose				

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca ordine di vendita " + string(fl_anno_ordine) + "/" + string(fl_num_ordine) + "(uof_estrai_reparto_stabilimento): " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_deposito_commerciale) then
	fs_errore = "Il deposito commerciale associato all'ordine " + string(fl_anno_ordine) + "/" + string(fl_num_ordine) + " è VUOTO(uof_estrai_reparto_stabilimento)"
	return -1
end if


if g_str.isnotempty(ls_cod_giro_consegna) then
	select cod_deposito
	into :ls_cod_deposito_giro
	from tes_giri_consegne
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_giro_consegna = :ls_cod_giro_consegna;
	
	if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_commerciale then ls_cod_deposito_commerciale = ls_cod_deposito_giro
	
end if


// tes_fasi_lavorazione 
// Leggo i dati chiave
select cod_deposito_origine,
		flag_stampa_sempre
into :ls_cod_deposito_origine,
		:ls_flag_stampa_sempre
from 	tes_fasi_lavorazione
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :fs_cod_prodotto_padre and
		cod_versione = :fs_cod_versione_padre;
choose case sqlca.sqlcode
	case 100
		return  1
	case is < 0
		fs_errore = "Errore in ricerca fase lavorazione " + fs_cod_prodotto_padre + " Vers:" + fs_cod_versione_padre + " (uof_estrai_reparto_stabilimento): " + sqlca.sqlerrtext
		return -1
end choose


// tes_fasi_lavorazione_det
select		cod_deposito_produzione
into		:fs_cod_deposito_produzione
from 		tes_fasi_lavorazione_det
where 	cod_azienda =:s_cs_xx.cod_azienda and
			cod_prodotto =:fs_cod_prodotto_padre and
			cod_versione =:fs_cod_versione_padre and
			cod_deposito_origine =:ls_cod_deposito_commerciale;
			
choose case sqlca.sqlcode
	case 100
		return  1
	case is < 0
		fs_errore = "Errore in ricerca dettaglio fase lavorazione " + fs_cod_prodotto_padre + " Vers:" + fs_cod_versione_padre + " (uof_estrai_reparto_stabilimento): " + sqlca.sqlerrtext
		return -1
end choose


ls_sql = "select cod_reparto_produzione "+&
			"from tes_fasi_lavorazione_prod "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_prodotto='"+fs_cod_prodotto_padre+"' and "+&
						"cod_versione='"+fs_cod_versione_padre+"' and "+&
						"cod_deposito_produzione='"+fs_cod_deposito_produzione+"' "

ll_return = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)

choose case ll_return
	case is < 0
		fs_errore = "Errore in fase di estrazione del reparto produzione da fasi-produzione (uof_estrai_reparto_stabilimento) "
	return -1
	
	case is >0
		//trovate eccezioni/reparti
		for ll_index=1 to ll_return
			ll_i = upperbound(fs_cod_reparto_produzione[])
			ll_i += 1
			fs_cod_reparto_produzione[ll_i] = lds_data.getitemstring(ll_index, "cod_reparto_produzione")
		next
		//fs_cod_rep_produzione = lds_data.getitemstring(1, "cod_reparto_produzione")
	
	return 0
	
case else
	fs_errore = "Attenzione! Per il semilavorato " + fs_cod_prodotto_padre + " non è stato trovato alcun reparto di produzione per lo stabilimento " + fs_cod_deposito_produzione + ": Verificare le tabelle di configurazione!"
	return -1
	
end choose

return 0
end function

public function integer uof_gen_varianti_deposito_ord_ven (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore);//  EnMe 17/02/2012 (non so rendo conto ... Venerdì 17 !!!!)
//	Questa procedura si occupa di passare la distinta di una riga di ordine e generare la variante in corrispondenza dei rami di distinta
//	che hanno una fase di lavorazione con stabilimento e reparto associato

string				ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, ls_sql, & 
					ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase, ls_cod_versione_figlio, &
					ls_cod_versione_inserito, ls_cod_versione_variante, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], ls_vuoto[], &
					ls_cod_reparto_produzione[], ls_cod_deposito_produzione, ls_cod_misura
					
long				ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza, ll_y, ll_new, ll_ret, ll_i, ll_z

integer			li_risposta, li_ret

dec{4} 			ld_quan_utilizzo

boolean			lb_test, lb_variante_trovata=false

datastore lds_righe_distinta, lds_reparti_variante


ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, fi_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven)

ll_num_figli = 1
//for ll_num_figli=1 to ll_num_righe
do while ll_num_figli <= ll_num_righe
	//leggi i figli del ramo della distinta base
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo") = "S" then 
	
		ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
		ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
		ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
		ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
		ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
		
		ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
		ls_cod_versione_inserito = ls_cod_versione_figlio
		
		//leggi se vi sono varianti nel ramo di distinta ----------------------------
		lb_variante_trovata = false
		
		select cod_prodotto,
				 cod_versione_variante,
				 flag_materia_prima
		into   :ls_cod_prodotto_variante,	
				 :ls_cod_versione_variante,
				 :ls_flag_materia_prima_variante
		from    varianti_det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda	and    
				 anno_registrazione = :fi_anno_registrazione	and    
				 num_registrazione = :fl_num_registrazione	and    
				 prog_riga_ord_ven = :fl_prog_riga_ord_ven	and    
				 cod_prodotto_padre = :fs_cod_prodotto			and    
				 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
				 cod_versione_figlio = :ls_cod_versione_figlio  and
				 cod_versione = :fs_cod_versione					and    
				 num_sequenza = :ll_num_sequenza;
		
		if sqlca.sqlcode < 0 then
			fs_errore="Errore nel DB"+ sqlca.sqlerrtext
			return -1
		end if
	
		if sqlca.sqlcode = 0 then  // VARIANTE ESITE
			//variante trovata: esegui scambio di prodotto/versione con prodotto variante/versione variante
			ls_cod_prodotto_inserito = ls_cod_prodotto_variante
			ls_cod_versione_inserito = ls_cod_versione_variante
			ls_flag_materia_prima = ls_flag_materia_prima_variante
			
			lb_variante_trovata = true
			
			ls_cod_reparto_produzione = ls_vuoto
			setnull(ls_cod_deposito_produzione)
			li_ret = uof_estrai_reparto_stabilimento ( ls_cod_prodotto_figlio, ls_cod_versione_figlio, "ORD_VEN", fi_anno_registrazione, fl_num_registrazione, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
			
			if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
				// Aggiorno la varianti_det_ord_Ven con i stabilimento e reparti	
				// Caso poco probabile perchè in genere vado sempre in creazione della variante in questo caso
				update varianti_det_ord_ven
				set cod_deposito_produzione = :ls_cod_deposito_produzione
				where  cod_azienda = :s_cs_xx.cod_azienda	and    
						  anno_registrazione = :fi_anno_registrazione	and    
						  num_registrazione = :fl_num_registrazione	and    
						  prog_riga_ord_ven = :fl_prog_riga_ord_ven	and    
						  cod_prodotto_padre = :fs_cod_prodotto			and    
						  cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
						  cod_versione_figlio = :ls_cod_versione_figlio  and
						  cod_versione = :fs_cod_versione					and    
						  num_sequenza = :ll_num_sequenza;
				if sqlca.sqlcode = -1 then
					fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
				
				delete from varianti_det_ord_ven_prod
				where  cod_azienda = :s_cs_xx.cod_azienda	and    
						  anno_registrazione = :fi_anno_registrazione	and    
						  num_registrazione = :fl_num_registrazione	and    
						  prog_riga_ord_ven = :fl_prog_riga_ord_ven	and    
						  cod_prodotto_padre = :fs_cod_prodotto			and    
						  cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
						  cod_versione_figlio = :ls_cod_versione_figlio  and
						  cod_versione = :fs_cod_versione					and    
						  num_sequenza = :ll_num_sequenza;
				if sqlca.sqlcode = -1 then
					fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
				
				for ll_z = 1 to upperbound(ls_cod_reparto_produzione)
					
					INSERT INTO varianti_det_ord_ven_prod  
							( cod_azienda,   
							  anno_registrazione,   
							  num_registrazione,   
							  prog_riga_ord_ven,   
							  cod_prodotto_padre,   
							  num_sequenza,   
							  cod_prodotto_figlio,   
							  cod_versione,   
							  cod_versione_figlio,   
							  cod_reparto )  
					VALUES ( :s_cs_xx.cod_azienda,
							  :fi_anno_registrazione,   
							  :fl_num_registrazione,   
							  :fl_prog_riga_ord_ven,   
							  :fs_cod_prodotto,   
							  :ll_num_sequenza,   
							  :ls_cod_prodotto_figlio,   
							  :fs_cod_versione,   
							  :ls_cod_versione_figlio,   
							  :ls_cod_reparto_produzione[ll_z] )  ;
	
					if sqlca.sqlcode = -1 then
						fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
						return -1
					end if
				next
	
	
			end if
	
		else	// NON ESISTE VARIANTE
		
			if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
				
				select cod_azienda
				into   :ls_test
				from   distinta
				where  cod_azienda = :s_cs_xx.cod_azienda and 	 
						 cod_prodotto_padre = :ls_cod_prodotto_inserito and
						 cod_versione = :ls_cod_versione_inserito;
				
				if sqlca.sqlcode=100 then 
					// non ci sono figli
					ll_num_figli++
					continue
				end if
				
			else
				//MP
				ll_num_figli++
				continue
			end if
			
			ls_cod_reparto_produzione = ls_vuoto
			setnull(ls_cod_deposito_produzione)
			li_ret = uof_estrai_reparto_stabilimento ( ls_cod_prodotto_figlio, ls_cod_versione_figlio, "ORD_VEN", fi_anno_registrazione, fl_num_registrazione, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
			
			if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
				// inserisco la varianti_det_ord_Ven con i stabilimento e reparti	
				select 	cod_misura_mag
				into 		:ls_cod_misura
				from 		anag_prodotti
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto_figlio;
				
				INSERT INTO varianti_det_ord_ven  
						( cod_azienda,   
						  anno_registrazione,   
						  num_registrazione,   
						  prog_riga_ord_ven,   
						  cod_prodotto_padre,   
						  num_sequenza,   
						  cod_prodotto_figlio,   
						  cod_versione,   
						  cod_versione_figlio,   
						  cod_prodotto,   
						  cod_versione_variante,   
						  cod_misura,   
						  quan_tecnica,   
						  quan_utilizzo,   
						  dim_x,   
						  dim_y,   
						  dim_z,   
						  dim_t,   
						  coef_calcolo,   
						  flag_esclusione,   
						  formula_tempo,   
						  des_estesa,   
						  flag_materia_prima,   
						  lead_time,   
						  cod_deposito_produzione )  
				VALUES ( :s_cs_xx.cod_azienda,   
							  :fi_anno_registrazione,   
							  :fl_num_registrazione,   
							  :fl_prog_riga_ord_ven,   
							  :fs_cod_prodotto,   
							  :ll_num_sequenza,   
							  :ls_cod_prodotto_figlio,   
							  :fs_cod_versione,   
							  :ls_cod_versione_figlio,   
							  :ls_cod_prodotto_figlio,   
							  :ls_cod_versione_figlio,   
							  :ls_cod_misura,
							  :ld_quan_utilizzo,
							  :ld_quan_utilizzo,
							  null,
							  null,
							  null,
							  null,
							  0,
							  'N',
							  null,
							  null,
							  :ls_flag_materia_prima,
							  0,
							  :ls_cod_deposito_produzione);
				if sqlca.sqlcode = -1 then
					fs_errore = "Generazione ordine: creazione variante per deposito (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~n.Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
				
				for ll_z = 1 to upperbound(ls_cod_reparto_produzione)
					
					INSERT INTO varianti_det_ord_ven_prod  
							( cod_azienda,   
							  anno_registrazione,   
							  num_registrazione,   
							  prog_riga_ord_ven,   
							  cod_prodotto_padre,   
							  num_sequenza,   
							  cod_prodotto_figlio,   
							  cod_versione,   
							  cod_versione_figlio,   
							  cod_reparto )  
					VALUES ( :s_cs_xx.cod_azienda,
							  :fi_anno_registrazione,   
							  :fl_num_registrazione,   
							  :fl_prog_riga_ord_ven,   
							  :fs_cod_prodotto,   
							  :ll_num_sequenza,   
							  :ls_cod_prodotto_figlio,   
							  :fs_cod_versione,   
							  :ls_cod_versione_figlio,   
							  :ls_cod_reparto_produzione[ll_z] )  ;
	
					if sqlca.sqlcode = -1 then
						fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
						return -1
					end if
				next
				
				
			end if
			
			
		end if
		
		//###########################################################################
		
		//prosegui più all'interno del ramo nella distinta, se c'è ancora altro
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda		and	 
				 cod_prodotto_padre = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
	
		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			ll_num_figli++
			continue
		else
			li_risposta=uof_gen_varianti_deposito_ord_ven(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_prodotto_inserito, ls_cod_versione_inserito, ls_errore)
			if li_risposta = -1 then
				fs_errore=ls_errore
				return -1
			end if
		end if
	end if
	
	ll_num_figli++
loop
//next

destroy lds_righe_distinta

return 0
end function

public function integer uof_gen_varianti_deposito_commesse (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore);//  EnMe 17/02/2012 (non so rendo conto ... Venerdì 17 !!!!)
//	Questa procedura si occupa di passare la distinta di una riga di ordine e generare la variante in corrispondenza dei rami di distinta
//	che hanno una fase di lavorazione con stabilimento e reparto associato

string				ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, ls_sql, & 
					ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase, ls_cod_versione_figlio, &
					ls_cod_versione_inserito, ls_cod_versione_variante, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], ls_vuoto[], &
					ls_cod_reparto_produzione[], ls_cod_deposito_produzione, ls_cod_misura
					
long				ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza, ll_y, ll_new, ll_ret, ll_i, ll_z, &
					ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven

integer			li_risposta, li_ret

dec{4} 			ld_quan_utilizzo

boolean			lb_test, lb_variante_trovata=false

datastore lds_righe_distinta, lds_reparti_variante


ll_num_figli = 1

// tendo di recuperare l'ordine indicizzando con la commessa
select 	anno_registrazione,
			num_registrazione,
			prog_riga_ord_ven
into 		:ll_anno_ord_ven, 
			:ll_num_ord_ven, 
			:ll_prog_riga_ord_ven
from		det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :fi_anno_registrazione and
			num_commessa = :fl_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in lettura dati riga ordine per la commessa "+string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+": "+sqlca.sqlerrtext)
	return -1
end if

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta_comm"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, fi_anno_registrazione, fl_num_registrazione)

ll_num_figli = 1
//for ll_num_figli=1 to ll_num_righe
do while ll_num_figli <= ll_num_righe
	//leggi i figli del ramo della distinta base
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo") = "S" then continue
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	//leggi se vi sono varianti nel ramo di distinta ----------------------------
	lb_variante_trovata = false
	
	select cod_prodotto,
			 cod_versione_variante,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ls_flag_materia_prima_variante
	from    varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda	and    
			 anno_commessa = :fi_anno_registrazione	and    
			 num_commessa = :fl_num_registrazione	and    
			 cod_prodotto_padre = :fs_cod_prodotto			and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
			 cod_versione_figlio = :ls_cod_versione_figlio  and
			 cod_versione = :fs_cod_versione					and    
			 num_sequenza = :ll_num_sequenza;
	
   if sqlca.sqlcode < 0 then
		fs_errore="Errore in ricerca tabella varianti_commesse ()uo_funzioni_1.uof_gen_varianti_deposito_commesse) "+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 0 then  // VARIANTE ESITE
		//variante trovata: esegui scambio di prodotto/versione con prodotto variante/versione variante
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		
		lb_variante_trovata = true
		
		ls_cod_reparto_produzione = ls_vuoto
		setnull(ls_cod_deposito_produzione)
		li_ret = uof_estrai_reparto_stabilimento ( ls_cod_prodotto_figlio, ls_cod_versione_figlio, "ORD_VEN", ll_anno_ord_ven, ll_num_ord_ven, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
		
		if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
			// Aggiorno la varianti_commesse con i stabilimento e reparti	
			// Caso poco probabile perchè in genere vado sempre in creazione della variante in questo caso
			update varianti_commesse
			set cod_deposito_produzione = :ls_cod_deposito_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					  anno_commessa = :fi_anno_registrazione	and    
					  num_commessa = :fl_num_registrazione	and    
					  cod_prodotto_padre = :fs_cod_prodotto			and    
					  cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
					  cod_versione_figlio = :ls_cod_versione_figlio  and
					  cod_versione = :fs_cod_versione					and    
					  num_sequenza = :ll_num_sequenza;
			if sqlca.sqlcode = -1 then
				fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			
			delete from varianti_commesse_prod
			where  cod_azienda = :s_cs_xx.cod_azienda	and    
					  anno_commessa = :fi_anno_registrazione	and    
					  num_commessa = :fl_num_registrazione	and    
					  cod_prodotto_padre = :fs_cod_prodotto			and    
					  cod_prodotto_figlio = :ls_cod_prodotto_figlio	and    
					  cod_versione_figlio = :ls_cod_versione_figlio  and
					  cod_versione = :fs_cod_versione					and    
					  num_sequenza = :ll_num_sequenza;
			if sqlca.sqlcode = -1 then
				fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			
			for ll_z = 1 to upperbound(ls_cod_reparto_produzione)
				
				INSERT INTO varianti_commesse_prod  
						( cod_azienda,   
						  anno_commessa,   
						  num_commessa,   
						  cod_prodotto_padre,   
						  num_sequenza,   
						  cod_prodotto_figlio,   
						  cod_versione,   
						  cod_versione_figlio,   
						  cod_reparto )  
				VALUES ( :s_cs_xx.cod_azienda,
						  :fi_anno_registrazione,   
						  :fl_num_registrazione,   
						  :fs_cod_prodotto,   
						  :ll_num_sequenza,   
						  :ls_cod_prodotto_figlio,   
						  :fs_cod_versione,   
						  :ls_cod_versione_figlio,   
						  :ls_cod_reparto_produzione[ll_z] )  ;

				if sqlca.sqlcode = -1 then
					fs_errore = "Generazione ordine (uo_funzioni_1.uof_gen_varianti_deposito_ord_ven)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
			next


		end if

	else	// NON ESISTE VARIANTE
	
		if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
			
			select cod_azienda
			into   :ls_test
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and 	 
					 cod_prodotto_padre = :ls_cod_prodotto_inserito and
					 cod_versione = :ls_cod_versione_inserito;
			
			if sqlca.sqlcode=100 then 
				// non ci sono figli
				ll_num_figli++
				continue
			end if
			
		else
			//MP
			ll_num_figli++
			continue
		end if
		
		ls_cod_reparto_produzione = ls_vuoto
		setnull(ls_cod_deposito_produzione)
		li_ret = uof_estrai_reparto_stabilimento ( ls_cod_prodotto_figlio, ls_cod_versione_figlio, "ORD_VEN", ll_anno_ord_ven, ll_num_ord_ven, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
		
		if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
			// inserisco la varianti_commesse con i stabilimento e reparti	
			select 	cod_misura_mag
			into 		:ls_cod_misura
			from 		anag_prodotti
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto_figlio;
			
			INSERT INTO varianti_commesse
					( cod_azienda,   
					  anno_commessa,   
					  num_commessa,   
					  cod_prodotto_padre,   
					  num_sequenza,   
					  cod_prodotto_figlio,   
					  cod_versione,   
					  cod_versione_figlio,   
					  cod_prodotto,   
					  cod_versione_variante,   
					  cod_misura,   
					  quan_tecnica,   
					  quan_utilizzo,   
					  dim_x,   
					  dim_y,   
					  dim_z,   
					  dim_t,   
					  coef_calcolo,   
					  flag_esclusione,   
					  formula_tempo,   
					  des_estesa,   
					  flag_materia_prima,   
					  lead_time,   
					  cod_deposito_produzione )  
			VALUES ( :s_cs_xx.cod_azienda,   
						  :fi_anno_registrazione,   
						  :fl_num_registrazione,   
						  :fs_cod_prodotto,   
						  :ll_num_sequenza,   
						  :ls_cod_prodotto_figlio,   
						  :fs_cod_versione,   
						  :ls_cod_versione_figlio,   
						  :ls_cod_prodotto_figlio,   
						  :ls_cod_versione_figlio,   
						  :ls_cod_misura,
						  :ld_quan_utilizzo,
						  :ld_quan_utilizzo,
						  null,
						  null,
						  null,
						  null,
						  0,
						  'N',
						  null,
						  null,
						  :ls_flag_materia_prima,
						  0,
						  :ls_cod_deposito_produzione);
			if sqlca.sqlcode = -1 then
				fs_errore = "Generazione ordine: creazione variante per deposito (uo_funzioni_1.uof_gen_varianti_deposito_commesse)~r~n.Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			
			for ll_z = 1 to upperbound(ls_cod_reparto_produzione)
				
				INSERT INTO varianti_commesse_prod  
						( cod_azienda,   
						  anno_commessa,   
						  num_commessa,   
						  cod_prodotto_padre,   
						  num_sequenza,   
						  cod_prodotto_figlio,   
						  cod_versione,   
						  cod_versione_figlio,   
						  cod_reparto )  
				VALUES ( :s_cs_xx.cod_azienda,
						  :fi_anno_registrazione,   
						  :fl_num_registrazione,   
						  :fs_cod_prodotto,   
						  :ll_num_sequenza,   
						  :ls_cod_prodotto_figlio,   
						  :fs_cod_versione,   
						  :ls_cod_versione_figlio,   
						  :ls_cod_reparto_produzione[ll_z] )  ;

				if sqlca.sqlcode = -1 then
					fs_errore = "Generazione varianti su commesse (uo_funzioni_1.uof_gen_varianti_deposito_commesse)~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
			next
			
			
		end if
		
		
	end if
	
	//###########################################################################
	
	//prosegui più all'interno del ramo nella distinta, se c'è ancora altro
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda		and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		ll_num_figli++
		continue
	else
		li_risposta=uof_gen_varianti_deposito_commesse(fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto_inserito, ls_cod_versione_inserito, ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	ll_num_figli++
loop
//next

destroy lds_righe_distinta

return 0
end function

private function integer uof_fasi_lavorazione_tes (long al_anno_documento, long al_num_documento, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, long al_num_sequenza, string as_tabella_varianti, ref datastore ads_store, ref string as_messaggio);string ls_cod_deposito_origine, ls_cod_deposito_produzione, ls_sql, ls_cod_giro_consegna, ls_cod_dep_giro
long ll_row, ll_rows, ll_i
datastore lds_store

choose case as_tabella_varianti
	case "varianti_det_ord_ven"
		select cod_deposito, cod_giro_consegna
		into :ls_cod_deposito_origine, :ls_cod_giro_consegna
		from tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda  and
				 anno_registrazione = :al_anno_documento and
				 num_registrazione = :al_num_documento;
		
end choose

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore durante il recupero del deposito di origine."
	return -1
end if

if g_str.isnotempty(ls_cod_giro_consegna) then
	select cod_deposito
	into :ls_cod_dep_giro
	from tes_giri_consegne
	where	cod_azienda=:s_cs_xx.cod_azienda and
				cod_giro_consegna=:ls_cod_giro_consegna;
	
	if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_dep_giro<>ls_cod_deposito_origine then ls_cod_deposito_origine = ls_cod_dep_giro
	
end if



ls_sql = "SELECT tes_fasi_lavorazione_det.cod_deposito_produzione, tes_fasi_lavorazione_prod.cod_reparto_produzione " + &
			"FROM tes_fasi_lavorazione_det " + &
			" JOIN tes_fasi_lavorazione_prod ON " + &
				"tes_fasi_lavorazione_prod.cod_azienda = tes_fasi_lavorazione_det.cod_azienda AND " + &
				"tes_fasi_lavorazione_prod.cod_prodotto = tes_fasi_lavorazione_det.cod_prodotto AND " + &
				"tes_fasi_lavorazione_prod.cod_versione = tes_fasi_lavorazione_det.cod_versione AND " + &
				"tes_fasi_lavorazione_prod.cod_reparto = tes_fasi_lavorazione_det.cod_reparto AND " + &
				"tes_fasi_lavorazione_prod.cod_lavorazione = tes_fasi_lavorazione_det.cod_lavorazione AND " + &
				"tes_fasi_lavorazione_prod.cod_deposito_produzione = tes_fasi_lavorazione_det.cod_deposito_produzione " + &
			"WHERE tes_fasi_lavorazione_det.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
			"tes_fasi_lavorazione_det.cod_prodotto='" + as_cod_prodotto + "' AND "+ &
			"tes_fasi_lavorazione_det.cod_versione='" + as_cod_versione + "' AND " + &
			"tes_fasi_lavorazione_det.cod_deposito_origine ='" + ls_cod_deposito_origine + "' "
				
	
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_messaggio)

for ll_i = 1 to ll_rows
	ll_row = ads_store.insertrow(0)
	ads_store.setitem(ll_row, "ordinamento_assoluto", 10)
	ads_store.setitem(ll_row, "cod_semilavorato", as_cod_prodotto)
	ads_store.setitem(ll_row, "cod_versione", as_cod_versione)
	ads_store.setitem(ll_row, "quan_utilizzo", 0)
	ads_store.setitem(ll_row, "num_sequenza", 10)
	ads_store.setitem(ll_row, "cod_deposito_commerciale", ls_cod_deposito_origine)
	ads_store.setitem(ll_row, "cod_deposito_produzione", lds_store.getitemstring(ll_i, 1))
	ads_store.setitem(ll_row, "cod_reparto_produzione", lds_store.getitemstring(ll_i, 2))
	
next
end function

private function integer uof_trova_fasi_lavorazioni (ref long ordinamento_assoluto, string prodotto_finito, string cod_versione, ref string materia_prima[], ref string versione_prima[], ref decimal quantita_utilizzo[], decimal quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref datastore ads_store, ref string fs_errore);string   	ls_cod_prodotto_figlio[],ls_test,ls_cod_prodotto_variante, ls_sql, ls_flag_materia_prima[], &
			ls_flag_materia_prima_variante,ls_errore, ls_cod_versione_figlio[], ls_cod_versione_variante, &
			ls_flag_ramo_descrittivo[], ls_flag_tipo_riordino, ls_cod_tipo_politica_riordino,ls_des_prodotto, &
			ls_flag_materia_prima_politica, ls_cod_deposito_origine[], ls_cod_deposito_produzione[], ls_cod_reparto_produzione[], &
			ls_filter
long     ll_conteggio,ll_t,ll_max, ll_cont_figli, ll_cont_figli_integrazioni, ll_row, ll_j, ll_num_sequenza[]
integer  li_risposta, ll_ret
decimal  ldd_quantita_utilizzo[],ldd_quan_utilizzo_variante
declare cu_varianti dynamic cursor for sqlsa;
DynamicStagingArea sqlsb

if isnull(prodotto_finito) or len(prodotto_finito) < 1 then
	fs_errore = "Manca l'indicazione del prodotto finito."
	return -1
end if

if isnull(cod_versione) or len(cod_versione) < 1 then
	fs_errore = "Al prodotto finito " + prodotto_finito + " manca l'indicazione del prodotto finito."
	return -1
end if

sqlsb = CREATE DynamicStagingArea
ll_conteggio = 1

//declare righe_distinta_3 cursor for 
declare righe_distinta_3 dynamic cursor for sqlsb;

ls_sql = "select  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, flag_ramo_descrittivo, num_sequenza from distinta " + &
         " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
			" and     cod_versione= '" + cod_versione + "' " 

// sono in un documento, quindi potrebbero esserci integrazioni
if not isnull(anno_registrazione) and not isnull(num_registrazione) then
		choose case tabella_varianti
				
			case "varianti_commesse"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza " + &
							 " FROM integrazioni_commessa " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_commessa = " + string(anno_registrazione)  + &
							 " and     num_commessa = "  + string(num_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and 	  cod_versione_padre = '" + cod_versione + "' "
				
			case "varianti_det_off_ven"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM integrazioni_det_off_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(anno_registrazione)  + &
							 " and     num_registrazione = "  + string(num_registrazione) + &
							 " and     prog_riga_off_ven = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
				
			case "varianti_det_ord_ven"
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM integrazioni_det_ord_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(anno_registrazione)  + &
							 " and     num_registrazione = "  + string(num_registrazione) + &
							 " and     prog_riga_ord_ven = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
			case "varianti_det_trattative"
				
				ls_sql += " union all " + &
				          " SELECT  cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM    integrazioni_det_trattative " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_trattativa = " + string(anno_registrazione)  + &
							 " and     num_trattativa = "  + string(num_registrazione) + &
							 " and     prog_trattativa = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
		end choose
end if

ls_sql += " ORDER BY num_sequenza "

prepare sqlsb from :ls_sql;

open dynamic righe_distinta_3;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nell'operazione OPEN del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
	close righe_distinta_3;
	return -1
end if

//open righe_distinta_3;

do while true

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
		   :ldd_quantita_utilizzo[ll_conteggio],
			:ls_flag_materia_prima[ll_conteggio],
			:ls_flag_ramo_descrittivo[ll_conteggio],
			:ll_num_sequenza[ll_conteggio];

   if sqlca.sqlcode = 100 then 
		close righe_distinta_3;	
		exit
	end if

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nell'operazione FETCH del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
		close righe_distinta_3;
		return -1
	end if
	
	if ls_flag_ramo_descrittivo[ll_conteggio] = "S" then continue
	
	// stefanop: 30/01/2012: controllo se ci sono delle fasi di lavorazioni
	if not uof_fasi_lavorazione(ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio], true) then
		continue
	end if
	// ----
	
	// verifico se mi trovo in presenza di un documento e quindi di una variante
	if not isnull(anno_registrazione) and not isnull(num_registrazione) then
		
		ls_sql = "select quan_utilizzo, " + &
					" cod_prodotto, " + &
					" cod_versione_variante, " + &
					" flag_materia_prima " + &
					" from " + tabella_varianti + &
					" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
		choose case tabella_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
											" and num_commessa=" + string(num_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
											" and num_registrazione=" + string(num_registrazione) + &
											" and prog_riga_off_ven=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
											" and num_registrazione=" + string(num_registrazione) + &
											" and prog_riga_ord_ven=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
											" and num_trattativa=" + string(num_registrazione) + &
											" and prog_trattativa=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + prodotto_finito + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + cod_versione + "'" + &
								" and cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "'"
								
						
		
		prepare sqlsa from :ls_sql;
		
		open dynamic cu_varianti;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
			return -1
		end if

		fetch cu_varianti 
		into :ldd_quan_utilizzo_variante, 
			  :ls_cod_prodotto_variante,
			  :ls_cod_versione_variante,
			  :ls_flag_materia_prima_variante;
				
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode <> 100  then
			ldd_quantita_utilizzo[ll_conteggio]=ldd_quan_utilizzo_variante
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
			ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_variante
		end if
	
		close cu_varianti;
	end if

	ldd_quantita_utilizzo[ll_conteggio]=ldd_quantita_utilizzo[ll_conteggio]*quantita_utilizzo_prec
	
	// recupero deposito e reparto di lavorazione.
	
	if uof_fasi_lavorazione_det(anno_registrazione, num_registrazione, prog_registrazione, prodotto_finito, cod_versione, ll_num_sequenza[ll_conteggio], ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio], tabella_varianti, ref ls_cod_deposito_origine[], ref ls_cod_deposito_produzione[], ref ls_cod_reparto_produzione[], ref fs_errore) < 0 then
		return -1
	end if
	
	for ll_j = 1 to upperbound(ls_cod_deposito_origine)
		
		// controllo che ci sia una sola riga, DISTINCT
		ls_filter = "cod_semilavorato='" + ls_cod_prodotto_figlio[ll_conteggio] + "' AND cod_versione='" + ls_cod_versione_figlio[ll_conteggio] + "' " + &
					" AND cod_deposito_commerciale='" + ls_cod_deposito_origine[ll_j] + "' AND cod_deposito_produzione='" + ls_cod_deposito_produzione[ll_j]+ "' " + &
					" AND cod_reparto_produzione='" +  ls_cod_reparto_produzione[ll_j] + "'"
		
		if ads_store.setfilter(ls_filter) < 0 then
			fs_errore = "Errore durante l'impostazione del filtro nel datastore."
			return -1
		end if
		
		ads_store.filter()
		
		if ads_store.rowcount() < 1 then
			
			ll_row = ads_store.insertrow(0)
			ordinamento_assoluto += 10
			ads_store.setitem(ll_row, "ordinamento_assoluto", ordinamento_assoluto)
			ads_store.setitem(ll_row, "cod_semilavorato", ls_cod_prodotto_figlio[ll_conteggio])
			ads_store.setitem(ll_row, "cod_versione", ls_cod_versione_figlio[ll_conteggio])
			ads_store.setitem(ll_row, "quan_utilizzo", ldd_quantita_utilizzo[ll_conteggio])
			ads_store.setitem(ll_row, "num_sequenza", ll_num_sequenza[ll_conteggio])
			ads_store.setitem(ll_row, "cod_deposito_commerciale", ls_cod_deposito_origine[ll_j])
			ads_store.setitem(ll_row, "cod_deposito_produzione", ls_cod_deposito_produzione[ll_j])
			ads_store.setitem(ll_row, "cod_reparto_produzione", ls_cod_reparto_produzione[ll_j])
			
		end if
		
		ads_store.setfilter("")
		ads_store.filter()
			
	next

	ll_conteggio++	

loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1
	

   if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		
		// controllo se esistono dei sotto-rami di distinta
		SELECT count(*)
		into   :ll_cont_figli
		FROM   distinta  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]	and    
				 cod_versione = :ls_cod_versione_figlio[ll_t];
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// sono in un documento, quindi potrebbero esserci integrazioni
		if not isnull(anno_registrazione) and not isnull(num_registrazione) then
			
				choose case tabella_varianti
						
					case "varianti_commesse"
						
						ll_cont_figli_integrazioni = 0
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_commessa
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_commessa = :anno_registrazione and
								 num_commessa = :num_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
						
					case "varianti_det_off_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_off_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :anno_registrazione and
								 num_registrazione = :num_registrazione and
								 prog_riga_off_ven = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_ord_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_ord_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :anno_registrazione and
								 num_registrazione = :num_registrazione and
								 prog_riga_ord_ven = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_trattative"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_trattative
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_trattativa = :anno_registrazione and
								 num_trattativa  = :num_registrazione and
								 prog_trattativa    = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_prodotto_figlio[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
				end choose
		end if
		
		ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni


		// non ci sono figli in distinta, verifico la variante 
		// (quindi non può essere neanche una integrazione, visto che non è ammessa la variante di una integrazione)
		
		if ll_cont_figli < 1 then
			ll_max=upperbound(materia_prima)
			ll_max++		
			materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
			versione_prima[ll_max] = ls_cod_versione_figlio[ll_t]
			quantita_utilizzo[ll_max] =round(ldd_quantita_utilizzo[ll_t],4)
			
			// Verifico se esiste la fase di lavorazione
			// se esiste carico tutti i dati della fase
			
	
			if not isnull(anno_registrazione) and not isnull(num_registrazione) then
				ls_sql = "select quan_utilizzo, " + &
							" cod_prodotto, cod_versione_variante " + &
							" from " + tabella_varianti + &
							" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
				choose case tabella_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
													" and num_commessa=" + string(num_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
													" and num_registrazione=" + string(num_registrazione) + &
													" and prog_riga_off_ven=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
													" and num_registrazione=" + string(num_registrazione)  + &
													" and prog_riga_ord_ven=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
													" and num_trattativa=" + string(num_registrazione) + &
													" and prog_trattativa=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + prodotto_finito + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and    cod_versione='" + cod_versione + "'"    // + "';"
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
	
				fetch cu_varianti 
				into  :ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante;
			
				
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode <> 100  then
					
					// Verifico se esiste la fase di lavorazione
					// se esiste carico tutti i dati della fase
					
					
					ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * quantita_utilizzo_prec
					ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
					materia_prima[ll_max] = ls_cod_prodotto_variante
					versione_prima[ll_max] = ls_cod_versione_variante
					quantita_utilizzo[ll_max] = round(ldd_quan_utilizzo_variante,4)
				end if
	
				close cu_varianti;
			end if
			
		else
			// presenza di figli sotto al prodotto corrente; procedo con la ricorsione
			
			li_risposta = uof_trova_fasi_lavorazioni(ref ordinamento_assoluto, ls_cod_prodotto_figlio[ll_t], &
																  ls_cod_versione_figlio[ll_t], & 
													  			  ref materia_prima[],& 
																  ref versione_prima[],& 
																  ref quantita_utilizzo[],ldd_quantita_utilizzo[ll_t], & 
													  anno_registrazione,num_registrazione,prog_registrazione, tabella_varianti, ref ads_store, ls_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
	
		end if
		
	else
		// si tratta di una materia prima forzata (flag_materia_prima=S)
		
		ll_max=upperbound(materia_prima)
		ll_max++		
		materia_prima[ll_max] = ls_cod_prodotto_figlio[ll_t]
		versione_prima[ll_max] = ls_cod_versione_figlio[ll_t]
		quantita_utilizzo[ll_max] =round(ldd_quantita_utilizzo[ll_t],4)
		
		// Verifico se esiste la fase di lavorazione
		// se esiste carico tutti i dati della fase
		

		if not isnull(anno_registrazione) and not isnull(num_registrazione) then
			ls_sql = "select quan_utilizzo, " + &
						" cod_prodotto, cod_versione_variante " + &
						" from " + tabella_varianti + &
						" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
			choose case tabella_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
												" and num_commessa=" + string(num_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
												" and num_registrazione=" + string(num_registrazione) + &
												" and prog_riga_off_ven=" + string(prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
												" and num_registrazione=" + string(num_registrazione)  + &
												" and prog_riga_ord_ven=" + string(prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
												" and num_trattativa=" + string(num_registrazione) + &
												" and prog_trattativa=" + string(prog_registrazione) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre='" + prodotto_finito + "'" + &
									" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
									" and    cod_versione='" + cod_versione + "'"    // + "';"
			
			prepare sqlsa from :ls_sql;

			open dynamic cu_varianti;
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if

			fetch cu_varianti 
			into  :ldd_quan_utilizzo_variante, 
					:ls_cod_prodotto_variante,
					:ls_cod_versione_variante;
		
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if
		
			if sqlca.sqlcode <> 100  then
				
				// Verifico se esiste la fase di lavorazione
				// se esiste carico tutti i dati della fase
				
				ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * quantita_utilizzo_prec
				ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
				ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
				materia_prima[ll_max] = ls_cod_prodotto_variante
				versione_prima[ll_max] = ls_cod_versione_variante
				quantita_utilizzo[ll_max] =round(ldd_quan_utilizzo_variante,4)
			end if

			close cu_varianti;
		end if

	end if
next
return 0
end function

public function integer uof_trova_fasi_lavorazioni (string prodotto_finito, string cod_versione, decimal quan_ordinata, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, ref datastore ads_store, ref string fs_errore);/**
 * stefanop
 * 01/02/2012
 *
 * Funzione che reupera in un datastore tutet le fasi di lavorazioni dalla distinta base.
 * Il datastore 'ds_funzioni_1' ha la seguente forma:
 * - cod_semilavorato
 * - quan_utilizzo
 * - cod_deposito_commerciale
 * - cod_deposito_produzione
 * - cod_reparto_produzione
 * - num_sequenza // numero sequenza della distinta
 * - cod_versione
 * - ordinamento_assoluto // ordinamento di inserimento delle righe
 **/

string					ls_materia_prima[], ls_version_prima[], ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_sql, ls_errore, &
							ls_cod_deposito_origine, ls_cod_reparto_produzione, ls_cod_deposito_produzione
							
long						ll_ordinamento_assoluto, ll_ret, ll_num_righe, ll_num_figli, ll_num_sequenza, ll_row

decimal{4}				ld_quantita_utilizzo[]

datastore				lds_righe_distinta, lds_reparti_variante



ads_store = create datastore
ads_store.dataobject = "ds_funzioni_1"
ll_ordinamento_assoluto = 0

// stefanop 23/03/2012: controllo la testa prima di entrare nei figli.
// Analizzo i reparti solo se flag_stampa_fase = 'S'
if uof_fasi_lavorazione(prodotto_finito, cod_versione, true) then

	ll_ret = uof_fasi_lavorazione_tes(anno_registrazione,num_registrazione,prog_registrazione, prodotto_finito, cod_versione, 0,  tabella_varianti, ads_store, fs_errore)
	
	ll_ordinamento_assoluto =10
	
	//--------------------------------------------------------------------------
	if ads_store.rowcount()>0 and tabella_varianti="varianti_det_ord_ven" then
		
		lds_righe_distinta = Create DataStore
		lds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"
		lds_righe_distinta.SetTransObject(sqlca)
		ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, prodotto_finito, cod_versione, anno_registrazione,num_registrazione, prog_registrazione)
		
		for ll_num_figli=1 to ll_num_righe
			ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
		
			if ls_cod_prodotto_figlio <> "REPARTO" then continue
		
			ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli, "cod_versione_figlio")
			ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli, "num_sequenza")
		
			ls_sql =  " select cod_reparto from varianti_det_ord_ven_prod  " + &
						" where  cod_azienda = '" + s_cs_xx.cod_azienda	+ "' and " + &
						" anno_registrazione = " + string(anno_registrazione) + " and " + &
						" num_registrazione = " +  string(num_registrazione)+ " and " + & 
						" prog_riga_ord_ven = " +  string(prog_registrazione) + " and " + &
						" cod_prodotto_padre = '" +  prodotto_finito + "' and " + &
						" cod_prodotto_figlio = '" +  ls_cod_prodotto_figlio + "' and " + &
						" cod_versione_figlio = '" +  ls_cod_versione_figlio + "' and " + &
						" cod_versione = '" +  cod_versione + "' and " + &
						" num_sequenza = " +  string(ll_num_sequenza)
		
			ll_ret = guo_functions.uof_crea_datastore(lds_reparti_variante, ls_sql, ls_errore)
			ls_errore = ""
		
			if ll_ret>0 then
				ls_cod_prodotto_figlio = ads_store.getitemstring(1, "cod_semilavorato")
				ls_cod_versione_figlio = ads_store.getitemstring(1, "cod_versione")
				
				ads_store.reset()
				
				ls_cod_deposito_origine = f_des_tabella("tes_ord_ven", &
																				"anno_registrazione="+string(anno_registrazione)+" and "+&
																				"num_registrazione="+string(num_registrazione), &
																				"cod_deposito")
				for ll_num_figli=1 to ll_ret
					ls_cod_reparto_produzione = lds_reparti_variante.getitemstring(ll_num_figli, "cod_reparto")
					
					ls_cod_deposito_produzione = f_des_tabella("anag_reparti", &
																				"cod_reparto='"+ls_cod_reparto_produzione+"'", &
																				"cod_deposito")
					ll_row = ads_store.insertrow(0)
					ads_store.setitem(ll_row, "ordinamento_assoluto", 10)
					ads_store.setitem(ll_row, "cod_semilavorato", ls_cod_prodotto_figlio)
					ads_store.setitem(ll_row, "cod_versione", ls_cod_versione_figlio)
					ads_store.setitem(ll_row, "quan_utilizzo", 0)
					ads_store.setitem(ll_row, "num_sequenza", 10)
					ads_store.setitem(ll_row, "cod_deposito_commerciale", ls_cod_deposito_origine)
					ads_store.setitem(ll_row, "cod_deposito_produzione", ls_cod_deposito_produzione)
					ads_store.setitem(ll_row, "cod_reparto_produzione", ls_cod_reparto_produzione)
				next
				
				destroy lds_reparti_variante
				ads_store.setsort("ordinamento_assoluto")
				ads_store.sort()
				return 0
			
			else
				destroy lds_reparti_variante
				exit
			end if
		next
		
	end if
	//---------------------------------------------------------------------------
	
end if
// -----

ll_ret = uof_trova_fasi_lavorazioni(ll_ordinamento_assoluto, prodotto_finito, cod_versione, ref ls_materia_prima, ref ls_version_prima, ref ld_quantita_utilizzo, quan_ordinata, anno_registrazione, num_registrazione,prog_registrazione, tabella_varianti, ads_store, fs_errore)

if ll_ret = 0 then
	ads_store.setsort("ordinamento_assoluto")
	ads_store.sort()
end if

return ll_ret
end function

public function integer uof_get_varianti_commesse_allinea_comm (integer fi_anno_commessa, long fl_num_commessa, ref string fs_cod_dep_prod[], ref string fs_cod_sl[], ref string fs_errore);string			ls_deposito, ls_prodotto, ls_vuoto[]
integer		li_index
long			ll_num_sequenza

li_index = 0
fs_cod_dep_prod[] = ls_vuoto[]
fs_cod_sl[] = ls_vuoto[]

declare cu_var_com cursor for  
	select		cod_deposito_produzione, 
				cod_prodotto_figlio, 
				num_sequenza
	from varianti_commesse
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_deposito_produzione is not null and
				anno_commessa=:fi_anno_commessa and num_commessa=:fl_num_commessa
	order by num_sequenza;

open cu_var_com;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in apertura cursore cu_var_com: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_var_com into :ls_deposito, :ls_prodotto, :ll_num_sequenza;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fetch cursore cu_var_com: " + sqlca.sqlerrtext
		close cu_var_com;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	if ls_deposito<>"" and not isnull(ls_deposito) then
		li_index += 1
		fs_cod_dep_prod[li_index] = ls_deposito
		fs_cod_sl[li_index] = ls_prodotto
	end if
	
loop

close cu_var_com;

return 0

end function

public function integer uof_riallinea_mov_commessa (integer fi_anno_commessa, long fl_num_commessa, boolean fb_solo_trasferimenti, ref string fs_errore);integer			li_count, li_anno_mov_cu, li_ret, li_anno_comm_opt[], li_count_2, li_num_reg_ord_ven, li_count_3
long				ll_num_mov_cu, li_num_comm_opt[], ll_num_riga_appartenenza, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_riga_ord_ven
uo_magazzino	luo_mag
string				ls_cod_dep_prod[], ls_cod_sl[], ls_cod_deposito_prod, ls_cod_dep_ordine, ls_null, ls_flag_tipo_bcl, ls_cod_prodotto, ls_flag_tipo_avanzamento, &
					ls_cod_deposito_succ, ls_cod_dep_ordine_hold, ls_vuoto[], ls_cod_giro_consegna, ls_cod_deposito_giro
boolean			lb_scarico_totale
datetime			ldt_chiusura_commessa, ldt_data_ddt_trasf


//da chiamare solo se la commessa è legata ad un ordine di vendita non sfuso

setnull(ls_null)
ib_forza_deposito_scarico_mp=false
setnull(is_cod_deposito_scarico_mp)
lb_scarico_totale = false


//controllo l'esistenza della commessa di produzione ---------------------------------------------------------------------------
select flag_tipo_avanzamento, data_chiusura
into :ls_flag_tipo_avanzamento, :ldt_chiusura_commessa
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:fi_anno_commessa and
			num_commessa=:fl_num_commessa;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo esistenza commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " :"+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "La commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " è inesistente!"
	return 1
	
elseif sqlca.sqlcode=0 and ls_flag_tipo_avanzamento <> "7" then
	fs_errore = "La commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " non è chiusa (stato="+ls_flag_tipo_avanzamento+"): impossibile procedere!"
	return 1
	
end if

//------------------------------------------------------------------------------------------------------------------------------------

select 	tp.flag_tipo_bcl, t.cod_deposito, t.cod_deposito_prod, d.cod_prodotto, 
			d.anno_registrazione, d.num_registrazione, d.prog_riga_ord_ven, d.num_riga_appartenenza,
			t.cod_giro_consegna
into 	:ls_flag_tipo_bcl, :ls_cod_dep_ordine, :ls_cod_deposito_prod, :ls_cod_prodotto,
		:ll_anno_reg_ord_ven, :ll_num_reg_ord_ven, :ll_prog_riga_ord_ven, :ll_num_riga_appartenenza,
		:ls_cod_giro_consegna
from det_ord_ven as d
join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
join tab_tipi_ord_ven as tp on tp.cod_azienda=t.cod_azienda and
								tp.cod_tipo_ord_ven=t.cod_tipo_ord_ven
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			d.anno_commessa>0 and
			d.anno_commessa=:fi_anno_commessa and
			d.num_commessa=:fl_num_commessa;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura flag tipo bcl riga ordine commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " :"+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "La commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " sembra non associata a nessun ordine! "
	return 1
	
elseif ll_num_riga_appartenenza>0 then
	
	select anno_commessa, num_commessa
	into :li_anno_comm_opt[1], :li_num_comm_opt[1]
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg_ord_ven and
				num_registrazione=:ll_num_reg_ord_ven and
				prog_riga_ord_ven=:ll_num_riga_appartenenza;
				
	if sqlca.sqlcode<0 then
		fs_errore = "** Errore in lettura n° commessa della riga principale per ordine ("+string(ll_anno_reg_ord_ven)+"/"+string(ll_num_reg_ord_ven)+"/"+string(ll_num_riga_appartenenza)+&
						") "+sqlca.sqlerrtext
		return -1
	end if
	
	fs_errore =	"La commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ &
					" appartiene ad una riga di ordine collegata: ("+string(ll_anno_reg_ord_ven)+"/"+string(ll_num_reg_ord_ven)+"/"+string(ll_prog_riga_ord_ven)+").~r~n"+&
					"Eseguire l'operazione dalla commessa della riga principale, commessa n° "+string(li_anno_comm_opt[1])+"/"+string(li_num_comm_opt[1]) + " riga ordine "+&
					string(ll_anno_reg_ord_ven)+"/"+string(ll_num_reg_ord_ven)+"/"+string(ll_num_riga_appartenenza)
	return 1
end if


//se esiste un giro consegna con deposito diverso dal quello origine considera questo
if g_str.isnotempty(ls_cod_giro_consegna) then
	select cod_deposito
	into :ls_cod_deposito_giro
	from tes_giri_consegne
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_giro_consegna=:ls_cod_giro_consegna;
				
	if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_dep_ordine then ls_cod_dep_ordine = ls_cod_deposito_giro		
end if





if ls_flag_tipo_bcl="B" then
	//caso SFUSO
	//confronto il deposito di produzione con quello della testata ordine
	if not isnull(ls_cod_deposito_prod) and ls_cod_deposito_prod<>"" and ls_cod_dep_ordine<>ls_cod_deposito_prod then
		if fb_solo_trasferimenti then
			//prosegui solo in questo caso, sovrascrivendo la variabile ls_cod_dep_ordine, utilizzata per lo scarico SMP e carico CPF
			ls_cod_dep_ordine_hold = ls_cod_dep_ordine
			ls_cod_dep_ordine = ls_cod_deposito_prod
		else
			fs_errore = "La commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " relativa ad ordine di sfuso non ha dep. produzione, quindi non è soggetta a trasferimenti "
			return 1
		end if
	else
		//considera per lo scarico il deposito origine ordine
		ls_cod_dep_ordine_hold=""
	end if
		
else
	//leggo i depositi produzione coinvolti in varianti_commessa
	li_ret = uof_get_varianti_commesse_allinea_comm (fi_anno_commessa, fl_num_commessa, ls_cod_dep_prod[], ls_cod_sl[], fs_errore)
	if li_ret<0 then
		//in fs_errore il messaggio
		return -1
	end if
	
	if upperbound(ls_cod_dep_prod[]) > 0 then
	else
		 ls_cod_sl[] = ls_vuoto[]
		//prova estraendo dalla distinta base la serie dei depositi coinvolti nella produzione dei semilavorati, ordinati per sequenza
		li_ret = uof_get_depositi_prod_allinea_comm(fi_anno_commessa, fl_num_commessa, ls_cod_dep_prod[], ls_cod_sl[], fs_errore)
		if li_ret<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//se ancora non hai trovato niente allora segnala
		if upperbound(ls_cod_dep_prod[]) > 0 then
		else
			//no depositi neanche da distinta: segnala!
			fs_errore = "Impossibile allineare i movimenti per la commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ ": non esistono varianti commessa e neppure depositi produzione da distinta base!"
			return 1
			
		end if
	end if
	
	if fb_solo_trasferimenti then
		//devo elaborare solo commesse soggette a trasferimento intragruppo (ELABORAZIONE SCHEDULATA)
		//in ls_cod_dep_prod[] ci sono tutti i depositi delle varianti commesse
		//la variabile "ls_cod_dep_prod[]" è passata byval, perchè non deve essere cambiata all'uscita della funzione "uof_se_commessa_con_trasf"
		li_ret = uof_se_commessa_con_trasf(fi_anno_commessa, fl_num_commessa, ls_cod_dep_prod[], fs_errore)
		
		if li_ret<0 then
			//in fs_errore il messaggio
			return -1
		elseif li_ret=1 then
			//commessa non soggetta a trasferimento, salta (in fs_errore comunque c'è il messaggio)
			return 1
		else
			//torna 0 quindi la commessa è soggetta a trasferimento e quindi in tal caso va elaborata
		end if
		
	end if
	
end if


if fb_solo_trasferimenti then
	if uof_get_ddt_trasf_duplicati(fi_anno_commessa, fl_num_commessa, fs_errore) < 0 then
		//in fs_errore il messaggio di errore
		return -1
	end if
end if

//#################################################################
//se arrivi fin qui vuol dire che hai superato i controlli preliminari e quindi devi procedere
//faccio prima le operazioni comuni ai casi di sfuso e non, e poi distinguo


if uof_riapri_commessa(fi_anno_commessa, fl_num_commessa, fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if


//ri-avanzamento della commessa
if ls_flag_tipo_bcl="B" then
	ib_forza_deposito_scarico_mp=true
	is_cod_deposito_scarico_mp = ls_cod_dep_ordine
	
	if ls_cod_dep_ordine_hold="" then
		//sei nel caso ordine sfuso senza trasferimenti, data movimenti dalla data chiusura commessa
		setnull(ldt_data_ddt_trasf)
	else
		//prova ad assegnare la data dei movimenti che stai per fare leggendola dalla bolla di trasferimento generata dal deposito di produzione
		if uof_get_data_ddt_trasf(fi_anno_commessa, fl_num_commessa, ls_cod_deposito_prod, ls_cod_dep_ordine_hold, ldt_data_ddt_trasf, fs_errore) < 0 then
			//in fs_errore il messaggio di errore
			return -1
		end if
	end if
	
	if isnull(ldt_data_ddt_trasf) then
		//forzo la data movimenti a quello della chiusura della commessa
		idt_data_creazione_mov = ldt_chiusura_commessa
	else
		//forzo la data movimenti a quello della bolla di trasferimento
		idt_data_creazione_mov = ldt_data_ddt_trasf
	end if
	
	li_ret = uof_avanza_commessa(fi_anno_commessa, fl_num_commessa, ls_null, fs_errore)
	if li_ret < 0 then
		fs_errore = "Errore in fase di avanzamento commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+" (ordine sfuso): " + fs_errore
		return -1
	end if
	
	//finito esci dalla funzione
	return 0
	
else
	
	lb_scarico_totale = false
	for li_count=1 to upperbound(ls_cod_dep_prod[])
		
		ls_cod_deposito_prod = ls_cod_dep_prod[li_count]
		ls_cod_prodotto =  ls_cod_sl[li_count]
		
		//verifico se esiste un deposito successivo nell'array, che sia diverso da quello in esame
		if uof_check_dep_prod_succ_var_com(li_count, ls_cod_dep_prod[], ls_cod_deposito_prod, ls_cod_deposito_succ) then
			//trovato: fai uno scarico PARZIALE della commessa
			
			//prova ad assegnare la data dei movimenti che stai per fare leggendola dalla bolla di trasferimento generata dal deposito di produzione
			if uof_get_data_ddt_trasf(fi_anno_commessa, fl_num_commessa, ls_cod_deposito_prod, ls_cod_deposito_succ, ldt_data_ddt_trasf, fs_errore) < 0 then
				//in fs_errore il messaggio di errore
				return -1
			end if
			
			if isnull(ldt_data_ddt_trasf) then
				//forzo la data movimenti a quello della chiusura della commessa
				idt_data_creazione_mov = ldt_chiusura_commessa
			else
				//forzo la data movimenti a quello della bolla di trasferimento
				idt_data_creazione_mov = ldt_data_ddt_trasf
			end if
			
			
			ib_forza_deposito_scarico_mp=false
			setnull(is_cod_deposito_scarico_mp)
			
			li_ret = uof_avanza_commessa(fi_anno_commessa, fl_num_commessa, ls_cod_prodotto, fs_errore)
			if li_ret < 0 then
				fs_errore = "Errore in fase di avanzamento parziale commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+": " + fs_errore
				return -1
			end if
			
		else
			//non trovato: fai uno scarico TOTALE della commessa (del parziale già eventualmente scaricato, ne terrà conto la funzione uof_avanza_commessa)
			ib_forza_deposito_scarico_mp=true
			is_cod_deposito_scarico_mp = ls_cod_deposito_prod
			
			//forzo la data movimenti a quello della chiusura della commessa
			idt_data_creazione_mov = ldt_chiusura_commessa
			
			li_ret = uof_avanza_commessa(fi_anno_commessa, fl_num_commessa, ls_null, fs_errore)
			if li_ret < 0 then
				fs_errore = "Errore in fase di avanzamento totale commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+": " + fs_errore
				return -1
			end if
			
			//verifica le righe ordine collegate, se ce ne sono, e fai l'avanzamento anche per queste
			li_ret = uof_get_commesse_righe_collegate(fi_anno_commessa, fl_num_commessa, li_anno_comm_opt[], li_num_comm_opt[], fs_errore)
			if li_ret < 0 then
				//in fs_errore il messaggio di errore
				return -1
			end if
			
			//se hai commesse delle righe collegate avanzale in questo passaggio
			for li_count_2=1 to upperbound(li_num_comm_opt[])
				
				//controllo l'esistenza della commessa di produzione dell'optional (è maniacale ma lo faccio lo stesso) --------------------------------------------
				select flag_tipo_avanzamento, data_chiusura
				into :ls_flag_tipo_avanzamento, :ldt_chiusura_commessa
				from anag_commesse
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_commessa=:li_anno_comm_opt[li_count_2] and
							num_commessa=:li_num_comm_opt[li_count_2];
				
				if sqlca.sqlcode<0 then
					fs_errore = "Errore in controllo esistenza commessa "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+ " :"+sqlca.sqlerrtext
					return -1
					
				elseif sqlca.sqlcode=100 then
					//qui è opportuno loggare questo messaggio e dare un bel continue ###############
					fs_errore = "La commessa "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+ " è inesistente!"
					return 2
					
				elseif sqlca.sqlcode=0 and ls_flag_tipo_avanzamento <> "7" then
					//qui è opportuno loggare questo messaggio e dare un bel continue ###############
					fs_errore = "La commessa "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+ " non è chiusa (stato="+ls_flag_tipo_avanzamento+"): impossibile procedere!"
					return 2
					
				end if
				
				select 	count(*)
				into 	:li_count_3
				from det_ord_ven as d
				join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and
												t.anno_registrazione=d.anno_registrazione and
												t.num_registrazione=d.num_registrazione
				join tab_tipi_ord_ven as tp on tp.cod_azienda=t.cod_azienda and
												tp.cod_tipo_ord_ven=t.cod_tipo_ord_ven
				where 	d.cod_azienda=:s_cs_xx.cod_azienda and
							d.anno_commessa>0 and
							d.anno_commessa=:li_anno_comm_opt[li_count_2] and
							d.num_commessa=:li_num_comm_opt[li_count_2];

				if sqlca.sqlcode<0 then
					fs_errore = "Errore in controllo esistenza commessa riga collegata: n°commessa: "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+ " :"+sqlca.sqlerrtext
					return -1
					
				elseif sqlca.sqlcode=100 or li_count_3<=0 or isnull(li_count_3) then
					//qui è opportuno loggare questo messaggio e dare un bel continue ###############
					fs_errore = "La commessa "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+ " su riga ordine collegata sembra non associata a nessun ordine! "
					return 2
					
				end if
				
				
				if uof_riapri_commessa(li_anno_comm_opt[li_count_2], li_num_comm_opt[li_count_2], fs_errore) < 0 then
					//in fs_errore il messaggio di errore
					return -1
				end if
				
				
				//scarico le SMP del prodotto OPT nel deposito di produzione e carico il CPF dell'OPT
				ib_forza_deposito_scarico_mp=true
				is_cod_deposito_scarico_mp = ls_cod_deposito_prod
				
				//forzo la data movimenti a quello della chiusura della commessa
				idt_data_creazione_mov = ldt_chiusura_commessa
				
				li_ret = uof_avanza_commessa(li_anno_comm_opt[li_count_2], li_num_comm_opt[li_count_2], ls_null, fs_errore)
				if li_ret < 0 then
					fs_errore = ">>> Errore in fase di avanzamento commessa "+string(li_anno_comm_opt[li_count_2]) + "/" +string(li_num_comm_opt[li_count_2])+&
									" collegata alla commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+" in quanto relativa ad una riga ordine collegata: " + fs_errore
					return -1
				end if
			next
			
			//finito esci dalla funzione
			return 0
			
		end if
	next
end if

return 0
end function

public function integer uof_get_commesse_righe_collegate (integer fi_anno_com_principale, long fl_num_com_principale, ref integer fi_anno_com_opt[], ref long fl_num_com_opt[], ref string fs_errore);integer			li_anno_reg_ordine, li_anno_comm_coll_cu
long				ll_num_reg_ordine, ll_riga_ord_principale, ll_count, ll_num_comm_coll_cu


select 	anno_registrazione, num_registrazione, prog_riga_ord_ven
into 		:li_anno_reg_ordine, :ll_num_reg_ordine, :ll_riga_ord_principale
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa>0 and
			anno_commessa=:fi_anno_com_principale and
			num_commessa=:fl_num_com_principale;

if sqlca.sqlcode<0 then
	fs_errore = "(uof_get_commesse_righe_collegate) Errore in lettura riga ordine legata alla commessa: "+sqlca.sqlerrtext
	return -1
end if

ll_count = 0

declare cu_comm_righe_coll cursor for  
	select anno_commessa, num_commessa
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_reg_ordine and
				num_registrazione=:ll_num_reg_ordine and
				num_riga_appartenenza>0 and num_riga_appartenenza=:ll_riga_ord_principale and
				num_commessa>0 and anno_commessa>0
	order by prog_riga_ord_ven;

open cu_comm_righe_coll;

if sqlca.sqlcode < 0 then
	fs_errore = "(uof_get_commesse_righe_collegate) Errore in apertura cursore cu_comm_righe_coll: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_comm_righe_coll into :li_anno_comm_coll_cu, :ll_num_comm_coll_cu;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fetch cursore cu_comm_righe_coll: " + sqlca.sqlerrtext
		close cu_comm_righe_coll;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	if ll_num_comm_coll_cu>0 then
		ll_count += 1
		fi_anno_com_opt[ll_count] = li_anno_comm_coll_cu
		fl_num_com_opt[ll_count] = ll_num_comm_coll_cu
	end if
	
loop

close cu_comm_righe_coll;

return 0
end function

public function integer uof_get_data_ddt_trasf (integer fi_anno_commessa, long fl_num_commessa, string fs_cod_deposito_partenza, string fs_cod_deposito_arrivo, ref datetime fdt_data_movimenti, ref string fs_errore);//faccio un cursore perchè è probabile, per errore, che ci siano due ddt di trasferimento
// ed in tal caso una select secca mi darebbe errore (select return more than one row)


datetime ldt_data_cu


declare cu_ddt_trasf cursor for  
	select tb.data_bolla
	from det_bol_ven db
	join tes_bol_ven as tb on 	tb.cod_azienda=db.cod_azienda and
										tb.anno_registrazione=db.anno_registrazione and
										tb.num_registrazione=db.num_registrazione
	join det_ord_ven as dord on 	dord.cod_azienda=db.cod_azienda and
											dord.anno_registrazione=db.anno_registrazione_ord_ven and
											dord.num_registrazione=db.num_registrazione_ord_ven and
											dord.prog_riga_ord_ven=db.prog_riga_ord_ven
	where 	db.cod_azienda=:s_cs_xx.cod_azienda and db.cod_tipo_det_ven = 'TRI' and
				tb.cod_deposito=:fs_cod_deposito_partenza and tb.cod_deposito_tras=:fs_cod_deposito_arrivo and
				dord.anno_commessa=:fi_anno_commessa and dord.num_commessa=:fl_num_commessa;

open cu_ddt_trasf;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in apertura cursore cu_ddt_trasf: " + sqlca.sqlerrtext
	return -1
end if

//non faccio nessun ciclo, mi interessa, se esiste, il dato della prima riga
fetch cu_ddt_trasf into :fdt_data_movimenti;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in fetch cursore cu_ddt_trasf: " + sqlca.sqlerrtext
	close cu_ddt_trasf;
	return -1
end if
	
if sqlca.sqlcode = 100 or isnull(fdt_data_movimenti) or year(date(fdt_data_movimenti))<1950 then
	setnull(fdt_data_movimenti)
end if

close cu_ddt_trasf;

return 0
end function

public function integer uof_riapri_commessa (integer fi_anno_commessa, long fl_num_commessa, ref string fs_errore);//Questa funzione esegue le seguenti operazioni
// 		elimino i movimenti CPF ed SMP legati alla commessa di produzion
//		riapro la commessa
//		riapro gli eventuali scarichi parziali in varianti commessa
//
//NOTA BENE: chiamare questa funzione solo dalla uof_riallinea_mov_commessa, 
//in quanto sarebbe incompleta chiamarla in modo a se stante !!!!!

integer			li_anno_mov_cu,li_ret
long				ll_num_mov_cu
uo_magazzino	luo_mag


//elimino i movimenti CPF ed SMP legati alla commessa di produzione -----------------------------------------------------
declare cu_mov_commessa cursor for  
	select		anno_registrazione,   
				num_registrazione  
	from mov_magazzino  
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_documento = :fi_anno_commessa and
				num_documento = :fl_num_commessa and
				cod_tipo_movimento in ('CPF', 'SMP');
				
open cu_mov_commessa;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in apertura cursore cu_mov_commessa:" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_mov_commessa into :li_anno_mov_cu, :ll_num_mov_cu;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH cursore 'cu_mov_commessa' :" + sqlca.sqlerrtext
		close cu_mov_commessa;
		return -1
	end if
	if sqlca.sqlcode = 100 then exit

	//procedi
	luo_mag = create uo_magazzino
	li_ret = luo_mag.uof_elimina_movimenti(li_anno_mov_cu, ll_num_mov_cu, true)
	destroy luo_mag
	
	if li_ret <> 0 then
		fs_errore = "Errore durante la cancellazione del movimento di magazzino " + string(li_anno_mov_cu) + "/" + string(ll_num_mov_cu)
		close cu_mov_commessa;
		return -1
	end if

loop
close cu_mov_commessa;
//------------------------------------------------------------------------------------------------------------------------------------

//riapro la commessa ------------------------------------------------------------------------------------------------------------

update anag_commesse
set 	flag_tipo_avanzamento='0',
		quan_prodotta=0
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:fi_anno_commessa and
			num_commessa=:fl_num_commessa;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in aggiornamento flag tipo avanzamento a ZERO per la commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " :"+sqlca.sqlerrtext
	return -1
end if
//------------------------------------------------------------------------------------------------------------------------------------

//riapro gli eventuali scarichi parziali in varianti commessa -------------------------------------------------------------------
update varianti_commesse
set flag_scarico_parziale='N'
 where 	cod_azienda=:s_cs_xx.cod_azienda and
 			anno_commessa=:fi_anno_commessa and
			num_commessa=:fl_num_commessa and
			cod_deposito_produzione is not null;
 
 if sqlca.sqlcode<0 then
	fs_errore = "Errore in aggiornamento flag tipo avanzamento a ZERO per la commessa "+string(fi_anno_commessa) + "/" +string(fl_num_commessa)+ " :"+sqlca.sqlerrtext
	return -1
end if
//--------------------------------------
end function

public function integer uof_se_commessa_con_trasf (integer fi_anno_commessa, long fl_num_commessa, string fs_dep_prod_comm[], ref string fs_errore);//torna 	0 se soggetta a trasferimento tra stabilimenti interni
//			1 se NON soggetta a trasferimenti
//		   -1 se errore

integer			li_count
string				ls_cod_deposito_origine, ls_temp, ls_dep_var_comm[], ls_temp2, ls_cod_giro_consegna, ls_cod_deposito_giro

ls_dep_var_comm[] = fs_dep_prod_comm[]

select 	t.cod_deposito, t.cod_giro_consegna
into 		:ls_cod_deposito_origine, :ls_cod_giro_consegna
from det_ord_ven as d
join tes_ord_ven as 	t on t.cod_azienda=d.cod_azienda and
							t.anno_registrazione=d.anno_registrazione and
							t.num_registrazione=d.num_registrazione
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			d.anno_commessa>0 and
			d.anno_commessa=:fi_anno_commessa and
			d.num_commessa=:fl_num_commessa;

if sqlca.sqlcode<0 then
	fs_errore = "(uof_se_commessa_con_trasf) Errore in lettura riga ordine legata alla commessa: "+sqlca.sqlerrtext
	return -1
end if

//--------------------------------------------------------------------------------------------------------
//se c'è un giro consegne con deposito diverso da quello origine allora conssidera questo
if g_str.isnotempty(ls_cod_giro_consegna) then

	select cod_deposito
	into :ls_cod_deposito_giro
	from tes_giri_consegne
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_giro_consegna=:ls_cod_giro_consegna;
				
	if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_origine then ls_cod_deposito_origine = ls_cod_deposito_giro
end if
//--------------------------------------------------------------------------------------------------------


//ora verifico se tra depositi varianti e commessa e deposito origine ordine ci sono depositi diversi
//e quindi la commessa richiede trasferimenti intragruppo
//non controllo se ci sono varianti commessa in quanto questa funzione viene chiamata dopo che questo controllo è stato effettuato e superato

li_count = upperbound(ls_dep_var_comm[])

//inserisco nell'array anche il deposito origine ordine
li_count += 1
ls_dep_var_comm[li_count] = ls_cod_deposito_origine

//di sicuro ora nell'array ci sono almeno due componenti
for li_count=1 to upperbound(ls_dep_var_comm[]) - 1
	ls_temp = ls_dep_var_comm[li_count]
	ls_temp2 = ls_dep_var_comm[li_count + 1]
	
	if ls_temp<>ls_temp2 then
		//trovato un deposito differente, quindi la commessa è soggetta a trasferimenti intragruppo
		return 0
	end if
	
next

//se arrivi fin qui vuol dire che non hai trovato depositi diversi, compreso il deposito origine ordine
//quindi la commessa non sarà soggetta a trasferiemnti intragruppo
return 1

end function

public function integer uof_get_ddt_trasf_duplicati (integer fi_anno_commessa, long fl_num_commessa, ref string fs_errore);//verifico la eventuale presenza di ddt di trasferimento legati alla stessa riga di ordine e con la stessa combinazione deposito partenza/deposito arrivo
//se esistono casi come questo è un errore e va segnalato
//da chiamare solo in caso di elaborazione schedulata fb_solo_trasferimenti=TRUE nella funzione uof_riallinea_mov_commessa

integer	li_count, li_anno_bol_ven
long		ll_num_bol_ven, ll_riga_bol_ven
string		ls_cod_deposito_partenza, ls_cod_deposito_arrivo, ls_cod_prodotto


declare cu_ddt_trasf_count cursor for  
	select 	count(*), 
				tb.cod_deposito, 
				tb.cod_deposito_tras
	from det_bol_ven db
	join tes_bol_ven as tb on 	tb.cod_azienda=db.cod_azienda and
										tb.anno_registrazione=db.anno_registrazione and
										tb.num_registrazione=db.num_registrazione
	join det_ord_ven as dord on 	dord.cod_azienda=db.cod_azienda and
											dord.anno_registrazione=db.anno_registrazione_ord_ven and
											dord.num_registrazione=db.num_registrazione_ord_ven and
											dord.prog_riga_ord_ven=db.prog_riga_ord_ven
	where 	db.cod_azienda=:s_cs_xx.cod_azienda and db.cod_tipo_det_ven = 'TRI' and
				dord.anno_commessa=:fi_anno_commessa and dord.num_commessa=:fl_num_commessa
	group by tb.cod_deposito, tb.cod_deposito_tras;

open cu_ddt_trasf_count;

if sqlca.sqlcode < 0 then
	fs_errore = "(uof_get_ddt_trasf_duplicati) Errore in apertura cursore cu_ddt_trasf_count: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_ddt_trasf_count into :li_count, :ls_cod_deposito_partenza, :ls_cod_deposito_arrivo;

	if sqlca.sqlcode < 0 then
		fs_errore = "(uof_get_ddt_trasf_duplicati) Errore in fetch cursore cu_ddt_trasf_count: " + sqlca.sqlerrtext
		close cu_ddt_trasf_count;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	if li_count>1 then
		//presenza di ddt trasf duplicati *
		//creo un altro cursore per segnarmi i numeri di questi ddt di trasferimento
		
		is_ddt_trasf_multipli_log += "~r~n----------------------------------------------------------------------------------------------"
		is_ddt_trasf_multipli_log += "~r~nDDT TRASFERIMENTO CREATI ERRONEAMENTE PIU' VOLTE PER LA COMMESSA "+string(fi_anno_commessa)+"/"+string(fl_num_commessa)
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		declare cu_ddt_trasf cursor for  
			select db.anno_registrazione, db.num_registrazione, db.prog_riga_bol_ven, db.cod_prodotto
			from det_bol_ven db
			join tes_bol_ven as tb on 	tb.cod_azienda=db.cod_azienda and
												tb.anno_registrazione=db.anno_registrazione and
												tb.num_registrazione=db.num_registrazione
			join det_ord_ven as dord on 	dord.cod_azienda=db.cod_azienda and
													dord.anno_registrazione=db.anno_registrazione_ord_ven and
													dord.num_registrazione=db.num_registrazione_ord_ven and
													dord.prog_riga_ord_ven=db.prog_riga_ord_ven
			where 	db.cod_azienda=:s_cs_xx.cod_azienda and db.cod_tipo_det_ven = 'TRI' and
						tb.cod_deposito=:ls_cod_deposito_partenza and tb.cod_deposito_tras=:ls_cod_deposito_arrivo and
						dord.anno_commessa=:fi_anno_commessa and dord.num_commessa=:fl_num_commessa;
		
		open cu_ddt_trasf;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "(uof_get_ddt_trasf_duplicati) Errore in apertura cursore cu_ddt_trasf: " + sqlca.sqlerrtext
			close cu_ddt_trasf_count; //chiudo il cursore + esterno
			return -1
		end if
		
		//ciclo del cursore interno #################################
		do while true
			
			fetch cu_ddt_trasf into :li_anno_bol_ven, :ll_num_bol_ven, :ll_riga_bol_ven, :ls_cod_prodotto;

			if sqlca.sqlcode < 0 then
				fs_errore = "(uof_get_ddt_trasf_duplicati) Errore in fetch cursore cu_ddt_trasf: " + sqlca.sqlerrtext
				close cu_ddt_trasf;
				close cu_ddt_trasf_count; //chiudo anche il cursore + esterno
				return -1
			end if
			
			if sqlca.sqlcode = 100 then exit
			
			//scrivi i riferimenti della bolla di trasferimento duplicata (o triplicata)
			is_ddt_trasf_multipli_log += "~r~n"+string(li_anno_bol_ven)+"/"+string(ll_num_bol_ven)+"/"+string(ll_riga_bol_ven)+" - Prodotto: "+ls_cod_prodotto
		loop
		close cu_ddt_trasf;
		//fine ciclo del cursore interno ##############################
		
		is_ddt_trasf_multipli_log += "~r~n----------------------------------------------------------------------------------------------"
		
		//-----------------------------------------------------------------------------------------------------------------------------------
	end if
loop

close cu_ddt_trasf_count;

return 0
end function

private function integer uof_cancella_commessa_ddt_trasferimento (long al_anno_commessa, long al_num_commessa, ref string as_error);/**
 * stefanop
 * 16/07/2012
 *
 * Questo controllo viene chiamato prima della cancellazione di una commessa.
 * Viene controllato se la commessa ha un DDT di trasferimento collegato, in quel 
 * caso NON è possibile cancellare la commessa e viene segnalato all'operatore.
 *
 * Ritorna:
 * -1 errore
 * 0 tutto ok
 **/
 
long ll_anno_registrazione_bol_ven, ll_num_registrazione_bol_ven, ll_prog_riga_bol_ven

setnull(ll_anno_registrazione_bol_ven)

select distinct det_bol_ven.anno_registrazione, det_bol_ven.num_registrazione, det_bol_ven.prog_riga_bol_ven
into :ll_anno_registrazione_bol_ven, :ll_num_registrazione_bol_ven, :ll_prog_riga_bol_ven
from varianti_commesse
join det_ord_ven on
	det_ord_ven.cod_azienda = varianti_commesse.cod_azienda and
	det_ord_ven.anno_commessa = varianti_commesse.anno_commessa and
	det_ord_ven.num_commessa = varianti_commesse.num_commessa
join det_bol_ven on
	det_bol_ven.cod_azienda = det_ord_ven.cod_azienda and
	det_bol_ven.anno_registrazione_ord_ven = det_ord_ven.anno_registrazione and
	det_bol_ven.num_registrazione_ord_ven = det_ord_ven.num_registrazione and
	det_bol_ven.prog_riga_ord_ven = det_ord_ven.prog_riga_ord_ven
where
	varianti_commesse.cod_azienda = :s_cs_xx.cod_azienda and
	varianti_commesse.anno_commessa = :al_anno_commessa and
	varianti_commesse.num_commessa = :al_num_commessa and
	det_bol_ven.cod_tipo_det_ven IN ('TRI');
	
	
if sqlca.sqlcode = 0 and not isnull(ll_anno_registrazione_bol_ven) and ll_anno_registrazione_bol_ven>0 then
	
	as_error = "Impossibile eliminare la commessa " + string(al_anno_commessa) + "/" + string(al_num_commessa) + &
					" perchè è già presente un DDT di trasferimento " + string(ll_anno_registrazione_bol_ven) + "/" + string(ll_num_registrazione_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + &
					" legato alla rispettiva riga d'ordine"
	return -1
	
elseif sqlca.sqlcode < 0 then
	
end if

return 0

end function

private function integer uof_controlla_fasi_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore);/**
 * stefanop
 * 16/07/2012
 *
 * Controllo che in fase di chiusura della commesa, tutte le fasi precedenti (se ci sono) 
 * abbiamo un DDT di trasferimento collegato, altrimenti annullo l'operazione perchè
 * provocherebbe dei movimenti di magazzino sul deposito errato.
 *
 * Ritorno:
 * -1 errore
 * 0 tutto ok
 **/
 
 
 return 0

 
end function

public function integer uof_commessa_con_ddt_trasferimento (long al_anno_commessa, long al_num_commessa, ref string as_depositi[]);/**
 * stefanop
 * 16/07/2012
 *
 * Recupera i depositi di produzione della commessa e ritorna se è implicato un trasferimento oppure no
 * Ritorna
 * -1 = Errore
 * 0 = se NON ci sono trasferimenti
 * 1 = se ci sono trasferimenti da fare
 **/

 
string ls_cod_deposito
int li_count, li_i, li_j
datastore lds_store

// Recupero depositi di produzione (DISTINCT)
li_count = uof_trova_depositi_commessa(al_anno_commessa, al_num_commessa, ref as_depositi)

if li_count < 0 then
	return -1
elseif li_count=1 then
	//un solo reparto produzione o più reparti ma dello stesso deposito
	return 0
end if


// Inserisco tutti i depositi nell'array
for li_i = 1 to li_count
	ls_cod_deposito = as_depositi[li_i]
	
	for li_j = li_i + 1 to li_count
		
		// trovato un deposito diverso, quindi c'è almeno un trasferimento
		if as_depositi[li_j] <> ls_cod_deposito then
			return 1
		end if
		
	next
next
			
return 0
end function

public function integer uof_trova_depositi_commessa (long al_anno_commessa, long al_num_commessa, ref string as_depositi[]);/**
 * stefanop
 * 16/07/2012
 *
 * Recupera l'array dei depositi di produzioni della commessa in ordine di num sequenza
 **/
 
 
string ls_empty[], ls_sql, ls_cod_dep_ordine, ls_cod_deposito
int li_count, li_i, li_k
datastore lds_store

as_depositi = ls_empty


ls_sql = "SELECT cod_deposito_produzione, num_sequenza "+&
		   "FROM varianti_commesse WHERE cod_azienda='" + s_cs_xx.cod_azienda  +"' " + &
					"AND anno_commessa=" + string(al_anno_commessa) + " AND num_commessa=" + string(al_num_commessa) + &
					" AND cod_deposito_produzione is not null "+&
			" ORDER BY num_sequenza"

li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)
li_k = 0

// Inserisco tutti i depositi nell'array
for li_i = 1 to li_count
	ls_cod_deposito = lds_store.getitemstring(li_i, 1)
	
	if li_k > 0 then
		//se arrivi qui vuol, dire che hai già inserito almeno un deposito nell'array as_depositi[]
		
		//controlla l'ultimo deposito inserito as_depositi[li_k] con quello che vorresti inserire, se coincide allora saltalo
		if ls_cod_deposito = as_depositi[li_k] then continue
	end if
	
	li_k ++
	as_depositi[li_k] = ls_cod_deposito
next

			
return upperbound(as_depositi)
end function

public function integer uof_check_integrita_ddt_trasf_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore, boolean ab_send_email);/**
 * stefanop
 * 16/07/2012
 *
 * Durante la chiusura di una commessa vengono recuperati di depositi di 
 * produzioni incaricati per determinare se è necessario avere almeno un DDT
 * di trasferimento. In quel caso vengono verificati che la commessa abbia
 * tutti i DDT di trasferimento creati, altrimenti invia una mail di notifica
 **/
 
string								ls_depositi[], ls_cod_dep_1, ls_cod_dep_2, ls_oggetto, ls_allegati[], ls_errore_email, ls_dep_che_sta_chiudendo, ls_deposito_produzione, ls_ordine_vendita, &
									ls_cod_deposito_origine, ls_cod_giro_consegna, ls_cod_dep_giro, ls_dep_chiusura_corretto

int									li_return, li_i, li_count_depositi_prod, li_count_ddt, li_anno_reg_ordine

boolean							lb_mancato_ddt

long								ll_num_reg_ord_ven, ll_prog_riga_ord_ven

uo_outlook						luo_mail




lb_mancato_ddt = false

// Recupero i reparti di produzione dalla varianti_commesse;
// Il ritorno della funzione indica se è necessario avere un DDT di trasferimento oppure no
// Ritorno: -1 errore; 0 non ci sono varianti o non è previsto DDT; 1 è previsto DDT di trasferimento
li_return = uof_commessa_con_ddt_trasferimento(al_anno_commessa, al_num_commessa, ref ls_depositi)

if li_return < 0 then return -1

//array dei depositi coinvolti nella produzione (DISTINCT)
li_count_depositi_prod = upperbound(ls_depositi)

//dati riga ordine
select 	det_ord_ven.anno_registrazione,
			det_ord_ven.num_registrazione,
			det_ord_ven.prog_riga_ord_ven,
			tes_ord_ven.cod_deposito,
			tes_ord_ven.cod_giro_consegna
into	:li_anno_reg_ordine,
		:ll_num_reg_ord_ven,
		:ll_prog_riga_ord_ven,
		:ls_cod_deposito_origine,
		:ls_cod_giro_consegna
from det_ord_ven
join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
where 	det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			det_ord_ven.anno_commessa=:al_anno_commessa and
			det_ord_ven.num_commessa=:al_num_commessa;

ls_ordine_vendita = ""
if li_anno_reg_ordine>0 and ll_num_reg_ord_ven>0 and ll_prog_riga_ord_ven>0 then &
				ls_ordine_vendita = "   (rif. riga ordine vendita: "+string(li_anno_reg_ordine)+"/"+string(ll_num_reg_ord_ven)+"/"+string(ll_prog_riga_ord_ven)+")"


if li_count_depositi_prod < 1 then
	
	// nessuna variante commessa trovata, quindi recupero il deposito produzione
	// dalla testa ordine. (SFUSO)
	select isnull(t.cod_deposito_prod, t.cod_deposito)
	into :ls_deposito_produzione
	from det_ord_ven as d
		join tes_ord_ven as t on
				t.cod_azienda = d.cod_azienda and
				t.anno_registrazione = d.anno_registrazione and
				t.num_registrazione = d.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and
			d.anno_commessa = :al_anno_commessa and
			d.num_commessa = :al_num_commessa;
		
	ls_depositi[1] = ls_deposito_produzione
	li_count_depositi_prod = 1
	
	//il deposito chiusura commessa corretto deve essere sempre l'ultimo coinvolto nella produzione
	ls_dep_chiusura_corretto = ls_depositi[li_count_depositi_prod]
	
else
	//la variabile "li_count" (conteggio dei depositi produzione) è maggiore di 0 (trovato almeno un deposito produzione)
	
	//il deposito chiusura commessa corretto deve essere sempre l'ultimo coinvolto nella produzione
	ls_dep_chiusura_corretto = ls_depositi[li_count_depositi_prod]
	
	//per stabilire con certezza che non ci sono trasferimenti occorre per sicurezza considerare anche il deposito origine ordine (ovvero quello del giro consegna)
	//infatti quest'ultimo potrebbe essere diverso dall'ultimo deposito produzione, e quindi occorre verificare che ci sia anche questo ddt
	//if li_return=0 then
		
	//verifico se il giro consegna ha un deposito diverso da quello origine ordine
	if g_str.isnotempty(ls_cod_giro_consegna) then
		select cod_deposito
		into :ls_cod_dep_giro
		from tes_giri_consegne
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_giro_consegna = :ls_cod_giro_consegna;
					
		if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_dep_giro<>ls_cod_deposito_origine then ls_cod_deposito_origine = ls_cod_dep_giro
	end if
	
	//in ls_cod_deposito_origine il deposito adibito alla spedizione
	//è per caso diverso dal deposito produzione trovato?
	//se si allora è previsto il trasferimento
	if ls_cod_deposito_origine<>ls_depositi[li_count_depositi_prod] then
		//previsto trasferimento
		li_return = 1
		
		//metto il deposito spedizione nell'array per poter poi cercare se c'è il ddt di trasferimento
		ls_depositi[li_count_depositi_prod + 1] = ls_cod_deposito_origine
		
		//aggiorno la variabile dell'upperbound
		li_count_depositi_prod = upperbound(ls_depositi)
	end if
	
	//end if
	
	
	// -1 errore
	// 0 non ci sono trasf oppure tutte le var. commessa hanno lo stesso deposito
	// 1 necessario trast
	if li_return = 1 then
		for li_i = 1 to li_count_depositi_prod - 1
			
			ls_cod_dep_1 = ls_depositi[li_i]
			ls_cod_dep_2 = ls_depositi[li_i + 1]
			
			// Controllo che esista l'ordine
			select count(*)
			into :li_count_ddt
			from det_ord_ven
				join det_bol_ven on
					det_bol_ven.anno_registrazione_ord_ven = det_ord_ven.anno_registrazione and
					det_bol_ven.num_registrazione_ord_ven = det_ord_ven.num_registrazione and
					det_bol_ven.prog_riga_ord_ven = det_ord_ven.prog_riga_ord_ven
				join tes_bol_ven on
					tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione and
					tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione
			where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
					det_ord_ven.anno_commessa = :al_anno_commessa and
					det_ord_ven.num_commessa = :al_num_commessa and
					tes_bol_ven.cod_deposito = :ls_cod_dep_1 and
					tes_bol_ven.cod_deposito_tras = :ls_cod_dep_2;
					
			if sqlca.sqlcode < 0 then
				as_errore = "Errore durante il controllo del DDT di trasferimento.~r~n" + sqlca.sqlerrtext
				return -1
			elseif sqlca.sqlcode = 100 or isnull(li_count_ddt) or li_count_ddt < 1 then
				
				//se il ddt di trasferimento mancante è proprio quello che sto creando in questo momento (sulla commessa avanzata la commessa)
				//evita di dare l'alert
				if ib_from_ddt_trasf and ls_cod_dep_1=is_dep_from and ls_cod_dep_2=is_dep_to and al_anno_commessa=ii_anno_comm_ddttrasf and al_num_commessa=il_num_comm_ddttrasf then
					//quello che manca lo sto creando adesso, quindi non mandare la mail, sarebbe un falso positivo ...
				else
					as_errore = "Attenzione manca un DDT di trasferimento dal deposito "+ls_cod_dep_1+" al deposito "+ls_cod_dep_2+". Possibili errori nei movimenti di magazzino"
					ls_oggetto = "Mancato DDT trasferimento (dal deposito "+ls_cod_dep_1+" al deposito "+ls_cod_dep_2+") in chiusura commessa " + string(al_anno_commessa) + "/" + string(al_num_commessa) + ls_ordine_vendita
					lb_mancato_ddt = true
					exit
				end if
			end if
		next
		
	end if
	
end if

//se con gli eventuali ddt di trasferimento previsti sono a posto
// controllo che sto chiudendo la commessa dal deposito corretto, cioè
// l'ultimo delle varianti commesse : presente in "ls_depositi[li_count_depositi_prod]"
//altrimenti andrebbe a fare i movimenti di magazzino nel deposito errato.
if lb_mancato_ddt = false then
	
	if uof_trova_deposito_chiusura_commessa(al_anno_commessa, al_num_commessa, ls_dep_che_sta_chiudendo, as_errore) < 0 then
		return -1
	end if
	
	if ls_dep_chiusura_corretto = ls_dep_che_sta_chiudendo then
	//if ls_depositi[li_count_depositi_prod] = ls_dep_che_sta_chiudendo then
		return 0
	else
		ls_oggetto = 	"Chiusura Commessa " + string(al_anno_commessa) + "/" + string(al_num_commessa) +" da deposito errato ("+ls_dep_che_sta_chiudendo+"):"+&
							" il deposito di chiusura commessa dovrebbe essere "+ls_dep_chiusura_corretto + " (che è il deposito dell'ultimo reparto di produzione)"
		//ls_oggetto = "Chiusura Commessa " + string(al_anno_commessa) + "/" + string(al_num_commessa) +" da deposito errato ("+ls_dep_che_sta_chiudendo+"): il deposito di chiusura commessa dovrebbe essere "+ls_depositi[li_count_depositi_prod] + &
		//				" (che è il deposito dell'ultimo reparto di produzione)"
		
		ls_oggetto += ls_ordine_vendita
		
		as_errore = ls_oggetto
		lb_mancato_ddt=true
	end if
end if

// Invio la mail per notifica se necessario
if lb_mancato_ddt then
	uof_log_sistema(as_errore,"COMDDT")
	
	if ab_send_email then
		
		luo_mail = create uo_outlook
	
		luo_mail.uof_invio_sendmail("A", ls_oggetto, ls_oggetto, is_cod_tipo_lista_dist, is_cod_lista_dist, ls_allegati, ref ls_errore_email)
		
		destroy luo_mail
		
	end if
	
end if
 
return li_return
end function

public function integer uof_trova_deposito_chiusura_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_cod_dep_chiusura, ref string as_errore);string				ls_cod_giro_consegna, ls_cod_deposito_giro


setnull(as_cod_dep_chiusura)

if ib_forza_deposito_scarico_mp then
	// Entro qui quando sto forzando lo scarico in un deposito che può essere diverso da quello della
	// testata d'ordine!
	as_cod_dep_chiusura =  is_cod_deposito_scarico_mp
	return 0
	
else
	// Faccio con una select diretta perchè NON può esistere una commessa referenziata in due righe d'ordine
	select distinct cod_deposito, cod_giro_consegna
	into :as_cod_dep_chiusura, :ls_cod_giro_consegna
	from det_ord_ven
		join tes_ord_ven on
			tes_ord_ven.anno_registrazione = det_ord_ven.anno_registrazione and
			tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione
	where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			det_ord_ven.anno_commessa = :ai_anno_commessa and
			det_ord_ven.num_commessa = :al_num_commessa;
			
	if sqlca.sqlcode = 0 and not isnull(as_cod_dep_chiusura) then
		
		//se l'ordine ha un giro di consegna e questo ha un deposito assegnato (diverso da quello in testata ordine)
		//allora il deposito di chiusura previsto è proprio questo qui
		if g_str.isnotempty(ls_cod_giro_consegna) then
			select cod_deposito
			into :ls_cod_deposito_giro
			from tes_giri_consegne
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_giro_consegna = :ls_cod_giro_consegna;
						
			if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>as_cod_dep_chiusura then as_cod_dep_chiusura = ls_cod_deposito_giro
		end if
		
		return 0
		
	elseif sqlca.sqlcode=100 or as_cod_dep_chiusura="" or isnull(as_cod_dep_chiusura) then
		as_errore ="La commessa " + string(ai_anno_commessa) + "/" + string(al_num_commessa) + " non sembra associata a nessuna riga di ordine!"
		
		return -1

		
	elseif sqlca.sqlcode < 0 then
		
		as_errore = "Errore durante la lettura del deposito ordine per la commessa " + string(ai_anno_commessa) + "/" + string(al_num_commessa)
		return -1
		
	end if
end if
end function

public function integer uof_genera_commesse_ddt (long fl_anno_bolla, long fl_num_bolla, long fl_prog_riga_bol_ven, ref integer fi_anno_commessa, ref long fl_num_commessa, ref string fs_errore);// Funzione che genera la commessa sulla riga del ddt di vendita passato
// nome: uof_genera_commesse_ddt
// tipo: integer
//       -1 failed
//  		 0 passed
//
//							 
//		Creata il 25/07/2012
//		Autore Donato Cassano


string			ls_cod_tipo_bol_ven, ls_cod_tipo_commessa, ls_cod_deposito_versamento, ls_cod_deposito_prelievo, ls_cod_operatore, ls_cod_tipo_det_ven, & 
				ls_cod_prodotto, ls_flag_tipo_det_ven, ls_flag_materia_prima, ls_des_estesa,ls_formula_tempo, ls_flag_muovi_mp,& 
				ls_flag_esclusione, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_misura, &
				ls_cod_prodotto_variante, ls_cod_versione_figlio, ls_cod_versione_variante, ls_flag_tipo_impegno_mp, ls_cod_deposito_produzione
				
long			ll_anno_registrazione,ll_num_registrazione, ll_test, ll_num_sequenza

datetime		ldt_data_registrazione, ldt_data_consegna
dec{4}		ld_quan_bolla, ldd_coef_calcolo, ldd_quan_tecnica, ldd_quan_utilizzo, &
				ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t


//leggo valori che mi servono dalla riga bolla vendita e dalla testata bolla vendita -------------------------------------------------------------
select		tes_bol_ven.cod_tipo_bol_ven,
			tes_bol_ven.cod_operatore,
			tes_bol_ven.data_registrazione,
			det_bol_ven.cod_tipo_det_ven,   
		 	det_bol_ven.cod_prodotto,   
			det_bol_ven.quan_consegnata,
		 	det_bol_ven.cod_versione,
			det_bol_ven.anno_commessa,
			det_bol_ven.num_commessa
into	:ls_cod_tipo_bol_ven,
		:ls_cod_operatore,
		:ldt_data_consegna,
		:ls_cod_tipo_det_ven,
		:ls_cod_prodotto,
		:ld_quan_bolla,
		:ls_cod_versione,
		:fi_anno_commessa,
		:fl_num_commessa
from det_bol_ven
join tes_bol_ven on 	tes_bol_ven.cod_azienda=det_bol_ven.cod_azienda and
							tes_bol_ven.anno_registrazione=det_bol_ven.anno_registrazione and
							tes_bol_ven.num_registrazione=det_bol_ven.num_registrazione
where det_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 det_bol_ven.anno_registrazione = :fl_anno_bolla and  
		 det_bol_ven.num_registrazione = :fl_num_bolla and
		 det_bol_ven.prog_riga_bol_ven=:fl_prog_riga_bol_ven;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in lettura dati Dettaglio/Testata Bolla Vendita "+string(fl_anno_bolla)+string(fl_num_bolla)+"/"+string(fl_prog_riga_bol_ven) + ": " + sqlca.sqlerrtext
	return -1
end if

if fi_anno_commessa>0 and fl_num_commessa>0 then
	select count(*)
	into :ll_test
	from anag_commesse
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:fi_anno_commessa and
				num_commessa=:fl_num_commessa;
	
	if ll_test>0 then
		fs_errore = "Per la riga del ddt "+string(fl_anno_bolla)+string(fl_num_bolla)+"/"+string(fl_prog_riga_bol_ven) + " esiste già una commessa ("+string(fi_anno_commessa)+"/"+string(fl_num_commessa)+")"
		setnull(fi_anno_commessa)
		setnull(fl_num_commessa)
		
		return -1
	end if
	
end if


ll_anno_registrazione = f_anno_esercizio()

//leggo il tipo commessa che sarà creato -----------------------------------------------------------------------------------
select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   tab_tipi_bol_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura cod_tipo_commessa da tab_tipi_bol_ven." + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_commessa) or ls_cod_tipo_commessa="" then
	fs_errore = "Per questo tipo DDT ("+ls_cod_tipo_bol_ven+"), non è specificato il relativo tipo commessa!"
	return -1
end if

//lego quello che mi serve sul tipo commessa che devo creare ------------------------------------------------------
select cod_deposito_versamento,
		 cod_deposito_prelievo,
		 flag_muovi_mp,
		 flag_tipo_impegno_mp
into   :ls_cod_deposito_versamento,
		 :ls_cod_deposito_prelievo,
		 :ls_flag_muovi_mp,
		 :ls_flag_tipo_impegno_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in lettura valori in tabella Tipi Commessa." + sqlca.sqlerrtext
	return -1
end if


ldt_data_registrazione = datetime(today())

if isnull(ls_cod_versione) then
	fs_errore = "Versione mancante in dettaglio bolla " + string(fl_anno_bolla) + "/" +string(fl_num_bolla) + "/" + string(fl_prog_riga_bol_ven) + ": Prodotto: " + ls_cod_prodotto
	return -1
end if
		
select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda 
and 	 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura flag_tipo_det_ven da tipi dettagli vendite." + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_det_ven = "M" or (ib_flag_non_collegati = true and ls_flag_tipo_det_ven = "C") then
	select max(num_commessa)
	into   :ll_num_registrazione
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_registrazione;
	
	if sqlca.sqlcode <0 then
		fs_errore = "Errore in lettura max(num_commessa) da anag_commesse: " + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ll_num_registrazione) then
		ll_num_registrazione= 1
	else
		ll_num_registrazione++
	end if

	//creazione commessa ----------------------------------------------------------------------
	insert into anag_commesse  
					(cod_azienda,   
					 anno_commessa,   
					 num_commessa,   
					 cod_tipo_commessa,   
					 data_registrazione,
					 cod_operatore,
					 nota_testata,
					 nota_piede,
					 flag_blocco,
					 quan_ordine,   
					 quan_prodotta,
					 data_chiusura,
					 data_consegna,
					 cod_prodotto,
					 quan_assegnata,
					 quan_in_produzione,
					 cod_deposito_versamento,
					 cod_deposito_prelievo,
					 cod_versione)
  values			(:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ls_cod_tipo_commessa,   
					 :ldt_data_registrazione,
					 :ls_cod_operatore,
					 null,
					 null,
					 'N',
					 :ld_quan_bolla,
					 0,
					 null,
					 :ldt_data_consegna,
					 :ls_cod_prodotto,
					 0,
					 0,
					 :ls_cod_deposito_versamento,
					 :ls_cod_deposito_prelievo,
					 :ls_cod_versione);

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in inserimento dat Commessa:" + sqlca.sqlerrtext
		return -1			
	end if

/*
	//inizio generazione varianti commesse
	declare righe_var_det_ord_ven cursor for
	select  cod_prodotto_padre,
			  num_sequenza,
			  cod_prodotto_figlio,
			  cod_versione_figlio,
			  cod_versione,
			  cod_misura,
			  cod_prodotto,
			  cod_versione_variante,
			  quan_tecnica,
			  quan_utilizzo,
			  dim_x,
			  dim_y,
			  dim_z,
			  dim_t,
			  coef_calcolo,
			  flag_esclusione,
			  formula_tempo,
			  des_estesa,
			  flag_materia_prima,
			  cod_deposito_produzione
	from    varianti_det_ord_ven
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     anno_registrazione=:fl_anno_ordine
	and     num_registrazione=:fl_num_ordine
	and     prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	open righe_var_det_ord_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in OPEN cursore righe_var_det_ord_ven (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
		return -1			
	end if

	do while true

		fetch righe_var_det_ord_ven
		into  :ls_cod_prodotto_padre,
				:ll_num_sequenza,
				:ls_cod_prodotto_figlio,
				:ls_cod_versione_figlio,
				:ls_cod_versione,
				:ls_cod_misura,
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ldd_quan_tecnica,
				:ldd_quan_utilizzo,
				:ldd_dim_x,
				:ldd_dim_y,
				:ldd_dim_z,
				:ldd_dim_t,
				:ldd_coef_calcolo,
				:ls_flag_esclusione,
				:ls_formula_tempo,
				:ls_des_estesa,
				:ls_flag_materia_prima,
				:ls_cod_deposito_produzione;

		if sqlca.sqlcode = 100 then exit

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in FETCH cursore righe_var_det_ord_ven (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if

	  INSERT INTO varianti_commesse
			( cod_azienda,   
			  anno_commessa,   
			  num_commessa,   
			  cod_prodotto_padre,   
			  num_sequenza,   
			  cod_prodotto_figlio,   
			  cod_versione_figlio,   
			  cod_versione,   
			  cod_misura,   
			  cod_prodotto,   
			  cod_versione_variante,   
			  quan_tecnica,   
			  quan_utilizzo,   
			  dim_x,   
			  dim_y,   
			  dim_z,   
			  dim_t,   
			  coef_calcolo,   
			  flag_esclusione,   
			  formula_tempo,   
			  des_estesa,
			  flag_materia_prima,
			  cod_deposito_produzione)  
  VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_cod_prodotto_padre,   
			  :ll_num_sequenza,   
			  :ls_cod_prodotto_figlio,   
			  :ls_cod_versione_figlio,   
			  :ls_cod_versione,   
			  :ls_cod_misura,   
			  :ls_cod_prodotto_variante,   
			  :ls_cod_versione_variante,   
			  :ldd_quan_tecnica,   
			  :ldd_quan_utilizzo,   
			  :ldd_dim_x,   
			  :ldd_dim_y,   
			  :ldd_dim_z,   
			  :ldd_dim_t,   
			  :ldd_coef_calcolo,   
			  :ls_flag_esclusione,   
			  :ls_formula_tempo,   
			  :ls_des_estesa,
			  :ls_flag_materia_prima,
			  :ls_cod_deposito_produzione)  ;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in trasferimento varianti (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if

	loop	
	
	close righe_var_det_ord_ven;
*/

/*
	// procedo con il passaggio delle integrazioni. EnMe 12/11/2004
	INSERT INTO integrazioni_commessa  
			( cod_azienda,   
			  anno_commessa,   
			  num_commessa,   
			  progressivo,   
			  cod_prodotto_padre,   
			  num_sequenza,   
			  cod_prodotto_figlio,   
			  cod_versione_figlio,   
			  cod_misura,   
			  quan_tecnica,   
			  quan_utilizzo,   
			  dim_x,   
			  dim_y,   
			  dim_z,   
			  dim_t,   
			  coef_calcolo,   
			  des_estesa,   
			  cod_formula_quan_utilizzo,   
			  flag_escludibile,   
			  flag_materia_prima,   
			  flag_arrotonda )  
	  SELECT cod_azienda,   
				:ll_anno_registrazione,   
				:ll_num_registrazione,   
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_versione_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda  
		 FROM integrazioni_det_ord_ven  
		 where cod_azienda = :s_cs_xx.cod_azienda and
		      anno_registrazione = :fl_anno_ordine and
				num_registrazione = :fl_num_ordine and
				prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in trasferimento integrazioni (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if
	
	// fine trasferimento integrazioni

*/

/*
		INSERT INTO varianti_commesse_prod  
				( cod_azienda,   
				  anno_commessa,   
				  num_commessa,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_versione,   
				  cod_versione_figlio,   
				  cod_reparto )  
		SELECT cod_azienda,   
					:ll_anno_registrazione,   
					:ll_num_registrazione,   
					cod_prodotto_padre,   
					num_sequenza,   
					cod_prodotto_figlio,   
					cod_versione,   
					cod_versione_figlio,   
					cod_reparto  
		FROM varianti_det_ord_ven_prod  
		WHERE 	( cod_azienda = :s_cs_xx.cod_azienda ) AND  
					( anno_registrazione = :fl_anno_ordine ) AND  
					( num_registrazione = :fl_num_ordine ) AND  
					( prog_riga_ord_ven = :fl_prog_riga_ord_ven )   ;

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in trasferimento varianti reparti produzione (uo_funzioni_1.uof_genera_commesse).~r~n" + sqlca.sqlerrtext
			return -1
		end if
	
*/
	//fine generazione varianti commesse
	
	if ls_flag_muovi_mp = 'S' then
		//	***	Michela 04/06/2007: se l'impegno è previsto durante l'attivazione, lo faccio in questo punto.se il flag è a Si allora devo impegnare le materie prime alla creazione della commessa
		if not isnull(ls_flag_tipo_impegno_mp) and ls_flag_tipo_impegno_mp = "S" then
			if luo_magazzino.uof_impegna_mp_commessa(	true, &
																	false, &
																	ls_cod_prodotto, &
																	ls_cod_versione, &
																	ld_quan_bolla, &
																	ll_anno_registrazione, &
																	ll_num_registrazione, &
																	fs_errore) = -1 then
				fs_errore = "Errore in fase di impegno magazzino~r~n" + fs_errore
				return -1
			end if
		end if
	end if
	
	
	//aggiorno la riga della bolla con il numero commessa associato ----------------------------------------------------------------
	update det_bol_ven  
	set    anno_commessa = :ll_anno_registrazione,
			 num_commessa = :ll_num_registrazione
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_bolla and  
			 num_registrazione = :fl_num_bolla and
			 prog_riga_bol_ven = :fl_prog_riga_bol_ven;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fase aggiornamento riga bolla con numero commessa:" + sqlca.sqlerrtext
		return -1
	end if
	
	//aggiorno variabili byref ...
	fi_anno_commessa = ll_anno_registrazione
	fl_num_commessa = ll_num_registrazione
	
end if				

return 0
end function

public function integer uof_seleziona_stock_arrivo (string fs_materia_prima[], double fdd_quantita_utilizzo[], string fs_flag_mag_negativo, ref s_stock_produzione fs_stock_prod_dest[], ref string fs_errore);long				ll_i, ll_prog_stock, ll_num_stock
string				ls_cod_ubicazione, ls_cod_lotto, ls_sql
datetime			ldt_data_stock
decimal			ldd_giacenza_stock
boolean			lb_stock_trovato = false
double			ldd_quantita_utilizzo[]

ldd_quantita_utilizzo[] = fdd_quantita_utilizzo[]


for ll_i = 1 to upperbound(fs_materia_prima[])
	if ldd_quantita_utilizzo[ll_i] = 0 then continue  
	declare righe_stock_dest dynamic cursor for sqlsa;
	
	lb_stock_trovato = false
	
	ls_sql = &
	"select 	 cod_ubicazione," + &
				"cod_lotto," + &
				"data_stock," + &
				"prog_stock," + &
				"giacenza_stock " + &
	"from stock " + &
	"where  cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
				"cod_deposito = '" + is_cod_deposito_CARICO_mp + "' and " + &
				"cod_prodotto = '" + fs_materia_prima[ll_i] + "' and "+&
				"flag_stato_lotto ='D' "
	
	if fs_flag_mag_negativo = "N" then
		ls_sql += " and giacenza_stock > 0 "
	end if
	ls_sql += " order by data_stock desc "
	
	prepare sqlsa from :ls_sql;
	open dynamic righe_stock_dest;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in open cursore righe_stock_dest: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while 1 = 1
		fetch righe_stock_dest
		into  	:ls_cod_ubicazione,
				:ls_cod_lotto,
				:ldt_data_stock,
				:ll_prog_stock,
				:ldd_giacenza_stock;
				
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB durante lettura cursore righe_stock: " + sqlca.sqlerrtext
			close righe_stock_dest;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		lb_stock_trovato = true
		
		//leggo lo stock e lo memorizzo nell'array
		if ldd_giacenza_stock >= ldd_quantita_utilizzo[ll_i] or fs_flag_mag_negativo = "S" then
			ll_num_stock++
			fs_stock_prod_dest[ll_num_stock].cod_prodotto = fs_materia_prima[ll_i]
			fs_stock_prod_dest[ll_num_stock].cod_deposito = is_cod_deposito_CARICO_mp
			fs_stock_prod_dest[ll_num_stock].cod_ubicazione = ls_cod_ubicazione
			fs_stock_prod_dest[ll_num_stock].cod_lotto = ls_cod_lotto
			fs_stock_prod_dest[ll_num_stock].data_stock = ldt_data_stock
			fs_stock_prod_dest[ll_num_stock].prog_stock = ll_prog_stock
			fs_stock_prod_dest[ll_num_stock].quan_prelievo = ldd_quantita_utilizzo[ll_i]
			
			//aggiorno quantita utilizzo
			ldd_quantita_utilizzo[ll_i] = 0
			exit
		else
			//aggiorno quantita utilizzo
			ldd_quantita_utilizzo[ll_i] = ldd_quantita_utilizzo[ll_i] - ldd_giacenza_stock
			
			ll_num_stock++
			fs_stock_prod_dest[ll_num_stock].cod_prodotto = fs_materia_prima[ll_i]
			fs_stock_prod_dest[ll_num_stock].cod_deposito = is_cod_deposito_CARICO_mp
			fs_stock_prod_dest[ll_num_stock].cod_ubicazione = ls_cod_ubicazione
			fs_stock_prod_dest[ll_num_stock].cod_lotto = ls_cod_lotto
			fs_stock_prod_dest[ll_num_stock].data_stock = ldt_data_stock
			fs_stock_prod_dest[ll_num_stock].prog_stock = ll_prog_stock
			fs_stock_prod_dest[ll_num_stock].quan_prelievo = ldd_giacenza_stock
		end if
			
	loop
	
	close righe_stock_dest;
	
	
//	if not lb_stock_trovato then
//		fs_errore = "Stock mancante per materia prima "+fs_materia_prima[ll_i]+" nel deposito destinazione "+is_cod_deposito_arrivo_kit+" con flag_stato_lotto ='D' ..."
//		return -1
//	end if
	
	
	if ldd_quantita_utilizzo[ll_i] > 0 and fs_flag_mag_negativo = "N" then
		//Donato: il messaggio all'utente è un po' forte, ma io lo lascio perchè ho fatto semplicemente copia incolla
		fs_errore = "Errore: Manca giacenza stock per la materia prima " + fs_materia_prima[ll_i] + ". Domanda all'utente: Come si può pensare di fare scatenare un avanzamento automatico di produzione se poi il magazzino presenta delle mancanze di scorta? Meglio che chi sta compiendo questa operazione controlli bene le giacenze a magazzino di tutte le materie prime coinvolte nello scarico simultaneo."
		return -1
	end if
	
	// ************************************ Fine Modifica ******************************************************
	
next

return 0
end function

public function boolean uof_is_ddt_trasferimento (integer fi_anno_bol_ven, long fl_num_bol_ven, long fl_prog_riga_bol_ven, ref integer fi_anno_commessa, ref long fl_num_commessa);//torna TRUE se è la riga è
//		- con tipo dettaglio vendita con flag_trasferisci_comp='S'
//		- in un ddt di tipo trasferimento
//		- con prodotto la cui politica ha il flag flag_tipo_riordino='C'  (cioè su commessa)
//		- con riferimento a riga ordine di vendita vuoto

//Donato 05/11/2014
//select modificata
//anzichè andare sul codice politica riordino fisso a 002 meglio andare sul flag_tipo_riordino = 'C' (Commessa Interna)

//Donato 03/12/2014
//select modificata
//anzichè andare sul tipo dettaglio fisso a TRI meglio andare sul flag_trasferisci_comp = 'S'

string ls_cod_azienda

//select d.cod_azienda, d.anno_commessa, d.num_commessa
//into :ls_cod_azienda, :fi_anno_commessa, :fl_num_commessa
//from det_bol_ven as d
//join tes_bol_ven as t on 	t.cod_azienda=d.cod_azienda and
//								t.anno_registrazione=d.anno_registrazione and
//								t.num_registrazione=d.num_registrazione
//join anag_prodotti as p on 	p.cod_azienda=d.cod_azienda and
//									p.cod_prodotto=d.cod_prodotto
//join tab_tipi_bol_ven as tipi on 		tipi.cod_azienda=t.cod_azienda and
//											tipi.cod_tipo_bol_ven=t.cod_tipo_bol_ven
//where 	d.cod_azienda=:s_cs_xx.cod_azienda and
//			d.cod_tipo_det_ven = 'TRI' and
//			tipi.flag_tipo_bol_ven = 'T' and
//			p.cod_tipo_politica_riordino in (	select cod_tipo_politica_riordino
//														from tab_tipi_politiche_riordino
//														where 	cod_azienda=:s_cs_xx.cod_azienda and
//																	flag_tipo_riordino='C') and
//			d.anno_registrazione=:fi_anno_bol_ven and d.num_registrazione=:fl_num_bol_ven and d.prog_riga_bol_ven=:fl_prog_riga_bol_ven and
//			(d.anno_registrazione_ord_ven is null or d.num_registrazione_ord_ven is null or prog_riga_ord_ven is null);

select d.cod_azienda
into :ls_cod_azienda
from det_bol_ven as d
join tes_bol_ven as t on 	t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
join anag_prodotti as p on 	p.cod_azienda=d.cod_azienda and
									p.cod_prodotto=d.cod_prodotto
join tab_tipi_bol_ven as tipi on 		tipi.cod_azienda=t.cod_azienda and
											tipi.cod_tipo_bol_ven=t.cod_tipo_bol_ven
join tab_tipi_det_ven as tipidet on 	tipidet.cod_azienda=d.cod_azienda and
											tipidet.cod_tipo_det_ven=d.cod_tipo_det_ven
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			tipidet.flag_trasferisci_comp = 'S' and
			tipi.flag_tipo_bol_ven = 'T' and
			p.cod_tipo_politica_riordino in (	select cod_tipo_politica_riordino
														from tab_tipi_politiche_riordino
														where 	cod_azienda=:s_cs_xx.cod_azienda and
																	flag_tipo_riordino='C') and
			d.anno_registrazione=:fi_anno_bol_ven and d.num_registrazione=:fl_num_bol_ven and d.prog_riga_bol_ven=:fl_prog_riga_bol_ven and
			(d.anno_registrazione_ord_ven is null or d.num_registrazione_ord_ven is null or prog_riga_ord_ven is null);

setnull(fi_anno_commessa)
setnull(fl_num_commessa)

if ls_cod_azienda<>"" and not isnull(ls_cod_azienda) then return true

return false
end function

public function integer uof_trova_reparti_e_semilavorati (boolean fb_inizio, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_semilavorato[], ref string fs_deposito[], ref string fs_errore);// Funzione che trova i reparti associati alle fasi di lavoro che sono da stampare 
// tipo: integer
//       -1 failed
//  	 0 passed


string				ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, ls_sql, & 
					ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase, ls_cod_versione_figlio, &
					ls_cod_versione_inserito, ls_cod_versione_variante, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], ls_vuoto[]
					
long				ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza, ll_y, ll_new, ll_ret, ll_i

integer			li_risposta, li_counter

boolean			lb_test, lb_variante_trovata=false

datastore lds_righe_distinta, lds_reparti_variante

if fb_inizio then
	//Donato 19/12/2011 ------------------------------------------------------------------
	//controllo se ho una fase di stampa con reparto a livello root della distinta
	
	 ls_flag_stampa_fase = 'N'
		
	select		cod_reparto,
				flag_stampa_fase,
				cod_deposito_origine,
				flag_stampa_sempre
	into		:ls_cod_reparto,
				:ls_flag_stampa_fase,
				:ls_cod_deposito_origine,
				:ls_flag_stampa_sempre
	from		tes_fasi_lavorazione
	where	cod_azienda = :s_cs_xx.cod_azienda  AND    
				cod_prodotto = :fs_cod_prodotto and
				cod_versione = :fs_cod_versione;
	
	if ls_flag_stampa_fase = 'S' then 
		lb_test = false
		
		//Donato 19/12/2011
		//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
		//if f_stabilimenti_reparti(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_cod_prodotto, fs_cod_versione, ls_cod_reparto, fs_errore)<0 then
		ls_reparti_letti[] = ls_vuoto[]
		if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, fs_cod_prodotto, fs_cod_versione, &
											ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], fs_errore)<0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//merge con l'array by ref principale
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])
		
		uof_deposito_e_semilavorato(fs_cod_reparto[1], fs_cod_prodotto, fs_deposito[], fs_semilavorato[])
		
	end if
	//------------------------------------------------------------------
end if


ll_num_figli = 1

lds_righe_distinta = Create DataStore

//lds_righe_distinta.DataObject = "d_data_store_distinta"
// cambiato datastore EnMe 12/11/2004 (integrazioni)
lds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, fi_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven)

ll_num_figli = 1
//for ll_num_figli=1 to ll_num_righe
do while ll_num_figli <= ll_num_righe
	//leggi i figli del ramo della distinta base
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"flag_ramo_descrittivo") = "S" then 
		ll_num_figli ++
		continue
	end if
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	
	//leggi se vi sono varianti nel ramo di distinta ----------------------------
	lb_variante_trovata = false
	
	select cod_prodotto,  cod_versione_variante, flag_materia_prima
	into	:ls_cod_prodotto_variante, :ls_cod_versione_variante, :ls_flag_materia_prima_variante
	from    varianti_det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_registrazione = :fi_anno_registrazione and    
			 num_registrazione = :fl_num_registrazione and    
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven and    
			 cod_prodotto_padre = :fs_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione_figlio = :ls_cod_versione_figlio and
			 cod_versione = :fs_cod_versione	and    
			 num_sequenza = :ll_num_sequenza;
	
   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 0 then
		//variante trovata: esegui scambio di prodotto/versione con prodotto variante/versione variante
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		
		lb_variante_trovata = true
		
		//TODO: leggi i reparti della variante  dalla varianti_det_ord_ven_prod
		//SE LI TROVA, CONSIDERA QUESTI E NON ALTRO, ALTRIMENTI CONSIDERA QUELLI TROVATI DALLA DISTINTA BASE
		//leggi da varianti_det_ord_ven  il cod_deposito_produzione
		//leggi da varianti_det_ord_ven_prod i rispettivi cod_reparto   (sono quelli di produzione)
		ls_reparti_letti[] = ls_vuoto[]
		ls_sql =  " select cod_reparto from varianti_det_ord_ven_prod  " + &
					" where  cod_azienda = '" + s_cs_xx.cod_azienda	+ "' and " + &
					" anno_registrazione = " + string(fi_anno_registrazione) + " and " + &
					" num_registrazione = " +  string(fl_num_registrazione)+ " and " + & 
					" prog_riga_ord_ven = " +  string(fl_prog_riga_ord_ven) + " and " + &
					" cod_prodotto_padre = '" +  fs_cod_prodotto + "' and " + &
					" cod_prodotto_figlio = '" +  ls_cod_prodotto_figlio + "' and " + &
					" cod_versione_figlio = '" +  ls_cod_versione_figlio + "' and " + &
					" cod_versione = '" +  fs_cod_versione + "' and " + &
					" num_sequenza = " +  string(ll_num_sequenza) + &
					"order by num_sequenza asc "
		
		guo_functions.uof_crea_datastore(lds_reparti_variante, ls_sql)
		
		ll_ret = lds_reparti_variante.retrieve()
		
		for ll_i = 1 to ll_ret
			ls_reparti_letti[ll_i] = lds_reparti_variante.getitemstring(ll_i, "cod_reparto")
			uof_deposito_e_semilavorato(ls_reparti_letti[ll_i], ls_cod_prodotto_inserito, fs_deposito[], fs_semilavorato[])
		next
		
		guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])

		
	else	
	
		//###########################################################################
		// se è materia prima salta .....
		if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
			//no MP
			
			select cod_azienda
			into   :ls_test
			from   distinta
			where  cod_azienda = :s_cs_xx.cod_azienda and 	 
					 cod_prodotto_padre = :ls_cod_prodotto_inserito and
					 cod_versione = :ls_cod_versione_inserito;
			
			if sqlca.sqlcode=100 then 
				ll_num_figli++
				continue
			end if
			
		else
			//MP
			ll_num_figli++
			continue
		end if
		
		//verifica se la fase è da stampare ----------------------------------------
		ls_flag_stampa_fase = 'N'
		
		select	cod_reparto,
				flag_stampa_fase,
				cod_deposito_origine,
				flag_stampa_sempre
		into	:ls_cod_reparto,
				:ls_flag_stampa_fase,
				:ls_cod_deposito_origine,
				:ls_flag_stampa_sempre
		FROM   tes_fasi_lavorazione
		WHERE  cod_azienda = :s_cs_xx.cod_azienda  AND    
				 cod_prodotto = :ls_cod_prodotto_inserito and
				 cod_versione = :ls_cod_versione_inserito;
		
		
		
		//Donato 23/01/2012 ----------------------------------------------------------------------------------------------------------
		//se hai trovato la variante pesca i reparti da varianti_det_ord_ven_prod (CHIAMERAI f_reparti_varianti(....))
		//altrimenti procedi normalmente dalla tes_fasi_lavorazione/tes_fasi_lavorazione_prod
		//per ora questa soluzione non è ancora implementata ...
		//---------------------------------------------------------------------------------------------------------------------------------
		
		//se la fase è da stampare
		if ls_flag_stampa_fase = 'S' then 
			//stampa fase SI
			lb_test = false
			
			//Donato 19/12/2011
			//verifica se esiste eccezione: se non trova eccezione modifica la variabile REF ls_cod_reparto, altrimenti la lascia invariata
			ls_reparti_letti[] = ls_vuoto[]
			if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto_inserito, ls_cod_versione_inserito, &
												ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, ls_reparti_letti[], fs_errore)<0 then
				//in fs_errore il messaggio
				return -1
			end if
			
			//merge con l'array by ref principale
			guo_functions.uof_merge_arrays(fs_cod_reparto[], ls_reparti_letti[])
			
			
			for li_counter=1 to upperbound(fs_cod_reparto[])
				uof_deposito_e_semilavorato(fs_cod_reparto[li_counter], ls_cod_prodotto_inserito, fs_deposito[], fs_semilavorato[])
			next
			
			
		end if
	end if
	
	//###########################################################################
	
	//prosegui più all'interno del ramo nella distinta, se c'è ancora altro
	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda		and	 
			 cod_prodotto_padre = :ls_cod_prodotto_inserito and
			 cod_versione = :ls_cod_versione_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		ll_num_figli++
		continue
	else
		li_risposta=uof_trova_reparti_e_semilavorati(false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_prodotto_inserito, ls_cod_versione_inserito, fs_cod_reparto[], &
																fs_semilavorato[], fs_deposito[], ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	ll_num_figli++
loop
//next

destroy lds_righe_distinta

return 0
end function

public function integer uof_deposito_e_semilavorato (string fs_cod_reparto, string fs_cod_semilav, ref string fs_cod_deposito[], ref string fs_cod_semilavorato[]);long				ll_tot
string				ls_cod_dep

if fs_cod_reparto="" or isnull(fs_cod_reparto) then return 0

ls_cod_dep = f_des_tabella("anag_reparti", "cod_reparto = '" + fs_cod_reparto + "'", "cod_deposito")

ll_tot =  upperbound(fs_cod_deposito[])

//fai questo controllo solo se l'array non è vuoto ...
if ll_tot>1 then
	//verifica se l'ultimo deposito inserito nell'array è lo stesso di quello che si vorrebbe inserire
	//se si allora non inserire ed esci dalla funzione
	if fs_cod_deposito[ll_tot]=ls_cod_dep then
		return 0
	end if
end if

//inserisci deposito e relativo semilavorato
fs_cod_semilavorato[ll_tot + 1] = fs_cod_semilav
fs_cod_deposito[ll_tot + 1] = ls_cod_dep

return 0
end function

public function integer uof_get_depositi_prod_allinea_comm (integer fi_anno_commessa, long fl_num_commessa, ref string fs_cod_dep_prod[], ref string fs_cod_sl[], ref string fs_errore);string			ls_deposito, ls_vuoto[], ls_reparti[], ls_cod_prodotto, ls_cod_versione
long			ll_num_reg_ord_ven, ll_riga_ord_ven
integer		li_anno_reg_ord_ven, li_ret

fs_cod_dep_prod[] = ls_vuoto[]
fs_cod_sl[] = ls_vuoto[]

select anno_registrazione, num_registrazione, prog_riga_ord_ven, cod_prodotto, cod_versione
into :li_anno_reg_ord_ven, :ll_num_reg_ord_ven, :ll_riga_ord_ven, :ls_cod_prodotto, :ls_cod_versione
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:fi_anno_commessa and
			num_commessa=:fl_num_commessa;

if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura dati riga ordine per estrazione fasi/depositi/semilavorati: "+sqlca.sqlerrtext
	return -1
end if


li_ret = uof_trova_reparti_e_semilavorati(true, li_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, &
												 ls_reparti[], fs_cod_sl[], fs_cod_dep_prod[], fs_errore)

if li_ret < 0 then
	fs_errore = "Errore in ricerca reparti (uof_trova_reparti_e_semilavorati): " + fs_errore
	return -1
end if

return 0

end function

public function boolean uof_check_dep_prod_succ_var_com (integer fi_indice, string fs_cod_dep_prod_var_com[], string fs_cod_dep_prod, ref string fs_cod_dep_succ);integer li_index
string ls_dep_tmp

fs_cod_dep_succ = ""

for li_index=fi_indice + 1 to upperbound(fs_cod_dep_prod_var_com[])
	ls_dep_tmp = fs_cod_dep_prod_var_com[li_index]
	
	if ls_dep_tmp<>fs_cod_dep_prod then
		//hai trovato un deposito successivo nelle varianti commessa, diverso da quello passato
		fs_cod_dep_succ = ls_dep_tmp
		return true
	end if
	
next


return false
end function

public function integer uof_valorizza_formule_distinta (string fs_cod_prodotto, string fs_cod_versione, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, string fs_tabella_varianti, ref string fs_errore);/*	Funzione che scorre la distinta base e valorizza le formule creando nel caso la variante
// FUNZIONE CHE VALORIZZA TUTTE LE FORMULE PRESENTI DELLA DISTINTA CREANDO LA RELATIVA VARIANTE.
//
// nome: uof_valorizza_formule_distinta
// tipo: integer
//  
//
//		Creata il 14/09/2012
//		Autore ENRICO MENEGOTTO
*/
string  ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
		  ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_flag_materia_prima,ls_cod_versione_figlio[],&
		 ls_cod_prodotto_variante, ls_cod_versione_variante, ls_tabella_comp, ls_formula
dec{4} ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo, ld_risultato
long    ll_conteggio,ll_t,ll_num_sequenza, ll_cont_1
integer li_risposta, li_count
uo_formule_calcolo luo_formule

choose case fs_tabella_varianti
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		
	case "COMMESSE"
		ls_tabella = "varianti_commesse"
		ls_progressivo = ""
		ls_tabella_comp = ""
		
end choose

ll_conteggio = 1

declare cu_dist_val_formule cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_materia_prima
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda  and
		  cod_prodotto_padre = :fs_cod_prodotto and
		  cod_versione=:fs_cod_versione;

open cu_dist_val_formule;

do while true
	fetch cu_dist_val_formule 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		fs_errore = "Errore in fase di fetch cursore cu_dist_val_formule (uo_conf_varianti.uof_valorizza_formule) ~r~n" + SQLCA.SQLErrText
		close cu_dist_val_formule;
		return -1
	end if
	
	// verifico presenza o meno della variante

	choose case fs_tabella_varianti
		case "ORD_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_ord_ven
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 anno_registrazione  = :fl_anno_registrazione and
					 num_registrazione   = :fl_num_registrazione and
					 prog_riga_ord_ven   = :fl_prog_riga and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione and
					 num_sequenza = :ll_num_sequenza;
					 
		case "OFF_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_off_ven
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 anno_registrazione  = :fl_anno_registrazione and
					 num_registrazione   = :fl_num_registrazione and
					 prog_riga_off_ven   = :fl_prog_riga and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione and
					 num_sequenza = :ll_num_sequenza;
					 
		case "COMMESSE"
			select count(*)
			into   :ll_cont_1
			from   varianti_commesse
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 anno_commessa  = :fl_anno_registrazione and
					 num_commessa   = :fl_num_registrazione and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione and
					 num_sequenza = :ll_num_sequenza;
					 
	end choose
	
	// se non c'è la variante uso la formula presente sul ramo di distinta	
	if ll_cont_1 = 0 then						 
		
		// se la formula è vuota passo al ramo di distinta successivo
		if  not isnull(ls_cod_formula_quan_utilizzo) then
		
			if isnull(ls_cod_misura) then 
				ls_cod_misura = "null"
			else
				ls_cod_misura = "'" + ls_cod_misura + "'"
			end if
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
			if isnull(ld_dim_x) then ld_dim_x = 0
			if isnull(ld_dim_y) then ld_dim_y = 0
			if isnull(ld_dim_z) then ld_dim_z = 0
			if isnull(ld_dim_t) then ld_dim_t = 0
			if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
			if isnull(ls_des_estesa) then ls_des_estesa = ""
			if isnull(ls_formula_tempo) then ls_formula_tempo = ""
			if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"
			
			// nuova gestione formule  -----------------------------------
			// leggo il testo della formula
			select testo_formula
			into  :ls_formula
			from  tab_formule_db
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_formula = :ls_cod_formula_quan_utilizzo;
						
			if sqlca.sqlcode = 100 then
				fs_errore = "Attenzione, la formula " + ls_cod_formula_quan_utilizzo + " non esiste !"
				rollback;
				return  -1
			end if
		
			li_risposta = f_sostituzione(ls_formula, "DIM_1", string( id_dim_x) )
			if li_risposta < 0 then
				fs_errore = "Errore in calcolo formula; funzione f_sostituzione() formula " +  ls_cod_formula_quan_utilizzo
				rollback;
				return -1
			end if
		
			li_risposta = f_sostituzione(ls_formula, "DIM_2", string( id_dim_y) )
			if li_risposta < 0 then
				fs_errore = "Errore in calcolo formula; funzione f_sostituzione() formula " +  ls_cod_formula_quan_utilizzo
				rollback;
				return -1
			end if
		
			luo_formule = create  uo_formule_calcolo
		
			//modalità gibus
			luo_formule.ib_gibus = true
		
			//modalità testing
			luo_formule.ii_anno_reg_ord_ven = 0
			luo_formule.il_num_reg_ord_ven = 0
			luo_formule.il_prog_riga_ord_ven = 0
			
			if luo_formule.uof_calcola_formula_testo(ls_formula, ld_risultato, fs_errore) < 0 then
				destroy luo_formule;
				return -1
			end if
			
			destroy luo_formule;
			
//			ld_quan_utilizzo = f_calcola_formula_db(ls_cod_formula_quan_utilizzo, ls_tabella_comp, fl_anno_registrazione, fl_num_registrazione, fl_prog_riga)
//			if ld_quan_utilizzo = -1 then
//				close cu_dist_val_formule;
//				rollback;
//				return -1
//			end if
			
			
			// fine nuova gestione formule ---------------------------------
			ld_quan_utilizzo = round(ld_risultato,4)
		
			ls_sql_ins =" insert into " + ls_tabella + &
							"(cod_azienda,"
							
			if fs_tabella_varianti <> "COMMESSE" then
				ls_sql_ins += 	"anno_registrazione," + &
									"num_registrazione," + &
									ls_progressivo + ","
			else
				ls_sql_ins += 	"anno_commessa," + &
									"num_commessa,"
			end if
			
			ls_sql_ins += "cod_prodotto_padre," + &
							"num_sequenza," + &
							"cod_prodotto_figlio," + &
							"cod_versione_figlio," + &
							"cod_versione," + &
							"cod_misura," + &
							"cod_prodotto," + &
							"cod_versione_variante," + &
							"quan_tecnica," + &
							"quan_utilizzo," + &
							"dim_x," + &
							"dim_y," + &
							"dim_z," + &
							"dim_t," + &
							"coef_calcolo," + &
							"flag_esclusione," + &
							"formula_tempo," + &
							"flag_materia_prima," + &
							"des_estesa)" + &
						" VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
							string(fl_anno_registrazione) + ", " + &   
							string(fl_num_registrazione) + ", "

			if fs_tabella_varianti <> "COMMESSE" then 	ls_sql_ins += string(fl_prog_riga) + ", "
							
			ls_sql_ins += " '" + fs_cod_prodotto + "', " + &
							string(ll_num_sequenza) + ", '" + &   
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', '" + &
							fs_cod_versione + "'," + &
							ls_cod_misura + ", '" + &
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', " + &
							f_double_to_string(ld_quan_tecnica) + ", " + &   
							f_double_to_string(ld_quan_utilizzo) + ", " + &
							f_double_to_string(ld_dim_x) + ", " + &   
							f_double_to_string(ld_dim_y) + ", " + &   
							f_double_to_string(ld_dim_z) + ", " + &   
							f_double_to_string(ld_dim_t) + ", " + &   
							f_double_to_string(ld_coef_calcolo) + ", '" + &   
							"N', '" + &
							ls_formula_tempo + "', '" + &
							ls_flag_materia_prima + "', '" + &
							ls_des_estesa + "')" 
	
			execute immediate :ls_sql_ins;
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore nell'inserimento della Variante (uo_conf_varianti - uof_valorizza_formule)~r~n" + sqlca.sqlerrtext
				close cu_dist_val_formule;
				return -1
			end if
			
		end if

	else	// ho trovato la variante; sicuramente la variante è già stata inserita con le formule valorzzate, ma esso potrebbe essere a sua volta
			// un componente semilavorato che contiene la distinta che quindi devo andare a controllare;
			// infatti potrebbero esserci figli con formula associata
	
		choose case  fs_tabella_varianti
			case "ORD_VEN"
				select cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
						:ls_cod_versione_variante
				from   varianti_det_ord_ven
				where  cod_azienda         = :s_cs_xx.cod_azienda and
						 anno_registrazione  = :fl_anno_registrazione and
						 num_registrazione   = :fl_num_registrazione and
						 prog_riga_ord_ven   = :fl_prog_riga and
						 cod_prodotto_padre  = :fs_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :fs_cod_versione and
						 num_sequenza = :ll_num_sequenza;
						 
			case "OFF_VEN"
				select cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
						:ls_cod_versione_variante
				from   varianti_det_off_ven
				where  cod_azienda         = :s_cs_xx.cod_azienda and
						 anno_registrazione  = :fl_anno_registrazione and
						 num_registrazione   = :fl_num_registrazione and
						 prog_riga_off_ven   = :fl_prog_riga and
						 cod_prodotto_padre  = :fs_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :fs_cod_versione and
						 num_sequenza = :ll_num_sequenza;
					 
			case "COMMESSE"
				select cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
						:ls_cod_versione_variante
				from   varianti_commesse
				where  cod_azienda         = :s_cs_xx.cod_azienda and
						 anno_commessa  = :fl_anno_registrazione and
						 num_commessa   = :fl_num_registrazione and
						 cod_prodotto_padre  = :fs_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :fs_cod_versione and
						 num_sequenza = :ll_num_sequenza;
					 
		end choose

		if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca della variante del ramo di distinta:~r~nPadre " + fs_cod_prodotto + " vers.:" + fs_cod_versione + &
																				"~r~nFiglio " + ls_cod_prodotto_figlio[ll_conteggio] + " vers.:" + ls_cod_versione_figlio[ll_conteggio] + &
																				"~r~n " + sqlca.sqlerrtext
			close cu_dist_val_formule;
			return -1
			
		end if		
			
		ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
		ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
		
	end if
	
	ll_conteggio++	
	
loop

close cu_dist_val_formule;


for ll_t = 1 to ll_conteggio -1
   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
			 cod_versione=:ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		g_mb.messagebox("Errore in ricerca nella tabella distinta. Dettaglio Errore:~r~n",SQLCA.SQLErrText,Information!)		
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_valorizza_formule_distinta(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], fl_anno_registrazione, fl_num_registrazione, fl_prog_riga, fs_tabella_varianti, ref fs_errore )
		if li_risposta < 0 then
			return -1
		end if
	end if
next

return 0
end function

public function integer uof_calcola_peso_riga_ordine (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref decimal ad_peso_unitario, ref string as_errore);string								ls_cod_prodotto, ls_cod_formula
dec{5}							ld_peso_netto, ld_calcolo_peso
uo_formule_calcolo			luo_formule
integer							li_ret
dec{4}							ld_dim_x, ld_dim_y
integer							li_comp_det_ord_ven		// 1 se comp_det_ord_ven, 2 se det_ord_ven

ad_peso_unitario = 0
li_comp_det_ord_ven = 0


//verifico che la riga ordine abbia il prodotto -----------------------------------------------------------
select cod_prodotto
into :ls_cod_prodotto
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;
			
if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura prodotto riga per calcolo peso: "+sqlca.sqlerrtext
	return -1
end if

//no prodotto?  torna peso ZERO -----------------------------------------------------------------------
if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
	ad_peso_unitario = 0
	return 0
end if


//leggo peso netto e formula peso in anagrafica prodotto ------------------------------------------
select 	peso_netto,
			cod_formula
into 	:ld_peso_netto,
		:ls_cod_formula
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto;

if isnull(ld_peso_netto) then ld_peso_netto = 0

//verifico se esiste la riga in complementi ordine vendita
select count(*)
into :li_ret
from comp_det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;
			
if isnull(li_ret) or sqlca.sqlcode <> 0 then li_ret = 0

if ls_cod_formula<>"" and not isnull(ls_cod_formula) then
	//c'è una formula impostata in anagrafica prodotti
	
	//vedo se sono state impostate le variabili dimensionali in det_ord_ven (caso optionals configurabili)
	select dim_x, dim_y
	into :ld_dim_x, :ld_dim_y
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura dim_x e dim_y riga per calcolo peso dalla det_ord_ven: "+sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ld_dim_x) then ld_dim_x=0
	if isnull(ld_dim_y) then ld_dim_y=0
	
	
	if li_ret > 0 then
		//esiste un record in comp_det_ord_ven, calcola la formula leggendo le variabili da qui
		li_comp_det_ord_ven = 1
		
	elseif ld_dim_x>0 or ld_dim_y>0 then
		//optional configurato, prova a calcolare la formula leggendo le variabili dimensionali da det_ord_ven
		li_comp_det_ord_ven = 2
	else
		//nessun record in comp_det_ord_ven e neanche optional configurato
		//torna il peso netto in anagrafica prodotto
		ad_peso_unitario = ld_peso_netto
		return 0
		
	end if
	
	//CALCOLO FORMULA ################################
	if li_comp_det_ord_ven > 0 then
		
		luo_formule = create uo_formule_calcolo
		luo_formule.ii_anno_reg_ord_ven = ai_anno_ordine
		luo_formule.il_num_reg_ord_ven = al_num_ordine
		luo_formule.il_prog_riga_ord_ven = al_riga_ordine
		
		if li_comp_det_ord_ven = 1 then
			luo_formule.is_nome_tabella = "comp_det_ord_ven"
		elseif li_comp_det_ord_ven=2 then
			luo_formule.is_nome_tabella = "det_ord_ven"
		end if
		
		li_ret = luo_formule.uof_calcola_formula_db(ls_cod_formula, ld_calcolo_peso, as_errore)
		destroy luo_formule
		
		if li_ret<0 then
			as_errore = as_errore + " (Codice formula peso: '"+ls_cod_formula+"')"
			return -1
		end if
		
		if isnull(ld_calcolo_peso) or ld_calcolo_peso<=0 then
			//la formula ha calcolato valore nullo: torna con il peso netto impostato in anagrafica (qualunque esso sia!)
			ad_peso_unitario = ld_peso_netto
		else
			ad_peso_unitario = ld_calcolo_peso
		end if
		
		return 0
		
	else
		ad_peso_unitario = 0
		return 0
	end if
	//#############################################
	
else
	//la formula non c'è in anagrafica prodotti: torna il peso netto in anagrafica prodotti
	ad_peso_unitario = ld_peso_netto
	return 0
	
end if

return 0
end function

public function integer uof_calcola_peso_testata_ordine (integer ai_anno_registrazione, long al_num_registrazione, ref string as_errore);//Donato 22/01/2013 Se il tipo ordine lo prevede, calcola il peso netto totale in testata ordine di vendita
dec{5}					ld_peso_netto
string						ls_cod_tipo_ord_ven, ls_flag_salva_peso_tes



//leggo il tipo ordine ----------------------------------------------------------------------------------
select cod_tipo_ord_ven
into :ls_cod_tipo_ord_ven
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione and
			num_registrazione=:al_num_registrazione;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura tipo ordine di vendita: "+sqlca.sqlerrtext
	return -1
	
elseif ls_cod_tipo_ord_ven="" or isnull(ls_cod_tipo_ord_ven) or sqlca.sqlcode=100 then
	return 1
end if


//leggo se il tipo ordine prevede il salvaraggio della somma dei pesi netti in testata ----------
ls_flag_salva_peso_tes = ""

select flag_salva_peso_tes
into :ls_flag_salva_peso_tes
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
			
if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura flag salva peso in testata per tipo ordine vendita: "+sqlca.sqlerrtext
	return -1
	
elseif ls_flag_salva_peso_tes<>"S"  then
	return 1
end if
			
			
//il tipo ordine lo prevede, quindi aggiorna il campo peso netto in testata ------------------
select sum(isnull(quan_ordine, 0) * isnull(peso_netto, 0))
into :ld_peso_netto
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione and
			num_registrazione=:al_num_registrazione;
			
if isnull(ld_peso_netto) then ld_peso_netto = 0

update tes_ord_ven
set peso_netto=:ld_peso_netto
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione and
			num_registrazione=:al_num_registrazione;
			
if sqlca.sqlcode<0 then
	as_errore = "Errore in aggiornamento peso netto testata ordine: "+sqlca.sqlerrtext
	return -1
end if

return 0

end function

public subroutine uof_log_sistema (string as_testo, string as_tipo_log);uo_log_sistema			luo_log

luo_log = create uo_log_sistema

luo_log.uof_write_log_sistema_not_sqlca(as_tipo_log, as_testo)

destroy uo_log_sistema
end subroutine

public subroutine uof_set_posticipato (boolean ab_posticipa);
//la chiusura/avanzamento della commessa viene posticipata solo se è attivo il parametro aziendale PCC

boolean		lb_PCC

guo_functions.uof_get_parametro_azienda( "PCC", lb_PCC)

if lb_PCC then
	ib_posticipa = ab_posticipa
else
	ib_posticipa = false
end if

return
end subroutine

public subroutine uof_set_pianificato (boolean ab_pianificato);
ib_pianificato = ab_pianificato

return
end subroutine

public function integer uof_pianifica (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato);
longlong				ll_progressivo
string					ls_flag_non_collegati, ls_forza_deposito_scarico_mp, ls_trasf_interno, ls_flag_from_ddt_trasf
datetime				ldt_oggi


ls_flag_non_collegati = "N"
ls_forza_deposito_scarico_mp = "N"
ls_trasf_interno = "N"
ls_flag_from_ddt_trasf = "N"

if ib_flag_non_collegati then ls_flag_non_collegati="S"
if ib_forza_deposito_scarico_mp then ls_forza_deposito_scarico_mp="S"
if ib_trasf_interno then ls_trasf_interno="S"
if ib_from_ddt_trasf then ls_flag_from_ddt_trasf="S"

if year(date(idt_data_creazione_mov))<1950 then setnull(idt_data_creazione_mov)

ldt_oggi = datetime(today(), now())


//calcola progressivo
select max(progressivo)
into :ll_progressivo
from buffer_chiusura_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda;
			
if isnull(ll_progressivo) then ll_progressivo=0
ll_progressivo += 1


insert into	buffer_chiusura_commesse (
	cod_azienda,
	progressivo,
	anno_commessa,
	num_commessa,
	cod_semilavorato,
	flag_non_collegati,
	forza_deposito_scarico_mp,
	trasf_interno,
	cod_deposito_scarico_mp,
	ddt_trasf_multimpli_log,
	cod_tipo_lista_dist,
	cod_lista_dist,
	cod_deposito_carico_mp,
	data_creazione_mov,
	quan_ordine	,
	dim_x,
	dim_y,
	cod_utente,
	data_pianificazione,
	flag_from_ddt_trasf,
	cod_deposito_from,
	cod_deposito_to)
values	(	:s_cs_xx.cod_azienda,
				:ll_progressivo,
				:ai_anno_commessa,
				:al_num_commessa,
				:as_cod_semilavorato,
				:ls_flag_non_collegati,
				:ls_forza_deposito_scarico_mp,
				:ls_trasf_interno,
				:is_cod_deposito_scarico_mp,
				:is_ddt_trasf_multipli_log,
				:is_cod_tipo_lista_dist,
				:is_cod_lista_dist,
				:is_cod_deposito_CARICO_mp,
				:idt_data_creazione_mov,
				:id_quan_ordine,
				:id_dim_x,
				:id_dim_y,
				:s_cs_xx.cod_utente,
				:ldt_oggi,
				:ls_flag_from_ddt_trasf,
				:is_dep_from,
				:is_dep_to);

if sqlca.sqlcode<0 then
	//metti in log sistema e torna -1
	if isnull(as_cod_semilavorato) then as_cod_semilavorato=""
	uof_log_sistema("Non sono riuscito a pianificare la chiusura commessa ("&
							+string(ai_anno_commessa)+","+string(al_num_commessa)+","+as_cod_semilavorato+"): "+sqlca.sqlerrtext, "AVANCOM")
	
	return -1
	
else
	
	//segno in tabella che è stata pianificata
	update anag_commesse
	set data_pianificazione=:ldt_oggi
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ai_anno_commessa and
				num_commessa=:al_num_commessa;
	
	uof_log_sistema("Pianificata commessa ("&
							+string(ai_anno_commessa)+","+string(al_num_commessa)+","+as_cod_semilavorato+") dall'utente "+s_cs_xx.cod_utente, "PIANIFICACOM")
	
end if

return 0
end function

public function integer uof_trova_mat_prime_varianti (string prodotto_finito, string cod_versione, long al_num_sequenza, ref string fs_cod_deposito_produzione, ref s_trova_mp_varianti fstr_trova_mp_varianti[], decimal quantita_utilizzo_prec, long anno_registrazione, long num_registrazione, long prog_registrazione, string tabella_varianti, string fs_cod_semilavorato, ref string fs_errore);/*	Funzione che trova le materie prime di un prodotto finito controllando le varianti presenti
 nella tabella <tabella_varianti>

 nome: f_trova_mat_prima
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 prodotto_finito	 		string			valore
							 cod_versione				string			valore
							 materia_prima[]			string			riferimento
							 quantita_utilizzo[]    double			riferimento 
							 quantita_utilizzo_prec double			valore
							 anno_registrazione	 	long				valore
							 num_registrazione	 	long				valore
							 prog_registrazione	 	long				valore
							 tabella_varianti			string			valore

	Creata il 20-07-98 dal pajasso MAURO BORDIN (mancava gestione errore + altra nave di 
    cappelle sui cursori Kazzzzzo! Ho perso 1/2 giornata per sistemarla, Kazzzo!)
    Sistemata da Diego il 22-01-1999
    Visto da Enrico il 16/02/1999
		Aggiunta gestione Integrazioni - Enrico 12/11/2004

   -------- 09/12/1999 ------------------------------------
    ENRICO: aggiunto ROUND(4) ovunque venga aggiornata la variabile quantita_utilizzo[]
            perchè con ASE se scrivo più di 4 decimali fa errore. In futuro bisognerà prevedere
            di aumentare il numero di decimali previsti.
				
	-------- 13/11/2006 -------------------------------------
	Enrico: nuova gestione materie prime con versione del figlio e ramo descrittivo
*/

string		ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto_figlio[], ls_cod_versione_figlio[], &
			ls_flag_materia_prima[],ls_flag_ramo_descrittivo[]

string		ls_test,ls_cod_prodotto_variante, ls_sql,    ls_flag_materia_prima_variante,ls_errore,  ls_cod_versione_variante, &
			ls_cod_semilavorato, ls_flag_scarico_parziale, ls_cod_deposito_produzione, ls_cod_prodotto_padre_variante, ls_cod_versione_padre_variante

long     ll_conteggio,ll_t,ll_max, ll_cont_figli, ll_cont_figli_integrazioni, ll_num_sequenza[]

integer  li_risposta

decimal  ldd_quantita_utilizzo[], ldd_quan_utilizzo_variante

boolean	lb_sl_variante = false


declare cu_varianti dynamic cursor for sqlsa;

DynamicStagingArea sqlsb


if len(trim(fs_cod_semilavorato)) <= 0 then setnull(fs_cod_semilavorato)

if isnull(prodotto_finito) or len(prodotto_finito) < 1 then
	fs_errore = "Manca l'indicazione del prodotto finito."
	return -1
end if

if isnull(cod_versione) or len(cod_versione) < 1 then
	fs_errore = "Al prodotto finito " + prodotto_finito + " manca l'indicazione del prodotto finito."
	return -1
end if

sqlsb = CREATE DynamicStagingArea
ll_conteggio = 1

//declare righe_distinta_3 cursor for 
declare righe_distinta_3 dynamic cursor for sqlsb;

ls_sql = "select  cod_prodotto_padre, cod_versione, cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, flag_ramo_descrittivo, num_sequenza from distinta " + &
         " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
			" and     cod_versione= '" + cod_versione + "' " 

//if al_num_sequenza > 0 and not isnull(al_num_sequenza) then
//	ls_sql += " and num_sequenza = " + string(al_num_sequenza) + " "
//end if

// sono in un documento, quindi potrebbero esserci integrazioni
if not isnull(anno_registrazione) and not isnull(num_registrazione) then
		choose case tabella_varianti
				
			case "varianti_commesse"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza " + &
							 " FROM integrazioni_commessa " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_commessa = " + string(anno_registrazione)  + &
							 " and     num_commessa = "  + string(num_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and 	  cod_versione_padre = '" + cod_versione + "' "
				
			case "varianti_det_off_ven"
				
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM integrazioni_det_off_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(anno_registrazione)  + &
							 " and     num_registrazione = "  + string(num_registrazione) + &
							 " and     prog_riga_off_ven = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
				
			case "varianti_det_ord_ven"
				ls_sql += " union all " + &
				          " SELECT cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM integrazioni_det_ord_ven " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_registrazione = " + string(anno_registrazione)  + &
							 " and     num_registrazione = "  + string(num_registrazione) + &
							 " and     prog_riga_ord_ven = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
			case "varianti_det_trattative"
				
				ls_sql += " union all " + &
				          " SELECT  cod_prodotto_padre, cod_versione_padre, cod_prodotto_figlio, cod_versione_figlio, quan_utilizzo, flag_materia_prima, 'N', num_sequenza  " + &
							 " FROM    integrazioni_det_trattative " + & 
				          " where   cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
							 " and     anno_trattativa = " + string(anno_registrazione)  + &
							 " and     num_trattativa = "  + string(num_registrazione) + &
							 " and     prog_trattativa = "  + string(prog_registrazione) + &
							 " and     cod_prodotto_padre = '" + prodotto_finito + "' " + &
							 " and     cod_versione_padre = '" + cod_versione + "' "
				
		end choose
end if

prepare sqlsb from :ls_sql;

open dynamic righe_distinta_3;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nell'operazione OPEN del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
	close righe_distinta_3;
	return -1
end if

//open righe_distinta_3;

do while true

	fetch righe_distinta_3 
	into  	:ls_cod_prodotto_padre[ll_conteggio],
			:ls_cod_versione_padre[ll_conteggio],
			:ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ldd_quantita_utilizzo[ll_conteggio],
			:ls_flag_materia_prima[ll_conteggio],
			:ls_flag_ramo_descrittivo[ll_conteggio],
			:ll_num_sequenza[ll_conteggio];

   if sqlca.sqlcode = 100 then 
		close righe_distinta_3;	
		exit
	end if

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nell'operazione FETCH del cursore righe_distinta_3 (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
		close righe_distinta_3;
		return -1
	end if
	
	if ls_flag_ramo_descrittivo[ll_conteggio] = "S" then continue
	
	// verifico se mi trovo in presenza di un documento e quindi di una variante
	if not isnull(anno_registrazione) and not isnull(num_registrazione) then
		
		ls_sql = "select cod_prodotto_padre, cod_versione, quan_utilizzo, cod_prodotto, cod_versione_variante, flag_materia_prima "
					
		if tabella_varianti = "varianti_commesse" then
			if ib_non_considerare_flag_parziale then
				ls_sql += ", 'N' "
			else
				ls_sql += ", flag_scarico_parziale " 
			end if
		else
			ls_sql += ", 'N' "
		end if
			
		ls_sql += " from " + tabella_varianti + " where  cod_azienda='" + s_cs_xx.cod_azienda + "' "
		
		choose case tabella_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
											" and num_commessa=" + string(num_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
											" and num_registrazione=" + string(num_registrazione) + &
											" and prog_riga_off_ven=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
											" and num_registrazione=" + string(num_registrazione) + &
											" and prog_riga_ord_ven=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
											" and num_trattativa=" + string(num_registrazione) + &
											" and prog_trattativa=" + string(prog_registrazione) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + prodotto_finito + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + cod_versione + "'" + &
								" and cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio]  + "'" + &
								" and num_sequenza = " + string(ll_num_sequenza[ll_conteggio]) + " "
		
		
		prepare sqlsa from :ls_sql;
		
		open dynamic cu_varianti;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
			return -1
		end if

		fetch cu_varianti 
		into 	:ls_cod_prodotto_padre_variante,
				:ls_cod_versione_padre_variante,
				:ldd_quan_utilizzo_variante, 
				:ls_cod_prodotto_variante,
				:ls_cod_versione_variante,
				:ls_flag_materia_prima_variante,
				:ls_flag_scarico_parziale;
				
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode <> 100  then
			ls_cod_prodotto_padre[ll_conteggio] = ls_cod_prodotto_padre_variante
			ls_cod_versione_padre[ll_conteggio] = ls_cod_versione_padre_variante
			ldd_quantita_utilizzo[ll_conteggio]=ldd_quan_utilizzo_variante
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
			ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
			ls_flag_materia_prima[ll_conteggio] = ls_flag_materia_prima_variante
			if ls_flag_scarico_parziale = "S" then ls_flag_materia_prima[ll_conteggio] = "S"
		end if
	
		close cu_varianti;
	end if

	ldd_quantita_utilizzo[ll_conteggio]=ldd_quantita_utilizzo[ll_conteggio]*quantita_utilizzo_prec
	ll_conteggio++	

loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1
	
   if ls_flag_materia_prima[ll_t] = 'N' then
		
		ll_cont_figli = 0
		
		// controllo se esistono dei sotto-rami di distinta
		SELECT count(*)
		into   :ll_cont_figli
		FROM   distinta  
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND    
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]	and    
				 cod_versione = :ls_cod_versione_figlio[ll_t];
				 //and
				 //num_sequenza = :ll_num_sequenza[ll_t];
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// sono in un documento, quindi potrebbero esserci integrazioni
		if not isnull(anno_registrazione) and not isnull(num_registrazione) then
			
				choose case tabella_varianti
						
					case "varianti_commesse"
						
						ll_cont_figli_integrazioni = 0
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_commessa
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_commessa = :anno_registrazione and
								 num_commessa = :num_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
						
					case "varianti_det_off_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_off_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :anno_registrazione and
								 num_registrazione = :num_registrazione and
								 prog_riga_off_ven = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_ord_ven"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_ord_ven
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_registrazione = :anno_registrazione and
								 num_registrazione = :num_registrazione and
								 prog_riga_ord_ven = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_versione_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
					case "varianti_det_trattative"
						
						select count(*)
						into   :ll_cont_figli_integrazioni
						from   integrazioni_det_trattative
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 anno_trattativa = :anno_registrazione and
								 num_trattativa  = :num_registrazione and
								 prog_trattativa    = :prog_registrazione and
								 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
								 cod_versione_padre = :ls_cod_prodotto_figlio[ll_t] and
								 num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode < 0 then
							fs_errore = "Errore in ricerca figli del prodotto "+ls_cod_prodotto_figlio[ll_t]+ "~r~n" + sqlca.sqlerrtext
							return -1
						end if
					
				end choose
		end if
		
		ll_cont_figli = ll_cont_figli + ll_cont_figli_integrazioni


		// non ci sono figli in distinta, verifico la variante 
		// (quindi non può essere neanche una integrazione, visto che non è ammessa la variante di una integrazione)
		
		if ll_cont_figli < 1 then
			// quindi se non ci sono figli vuol dire che è una materia prima
			
			if isnull(fs_cod_semilavorato) then
				// EnMe 10/2/2012; il fatto che fs_cod_semilavorato = null è il segnale che da qui in giù si può andare ad eseguire lo scarico
	
				if not isnull(anno_registrazione) and not isnull(num_registrazione) then
					ls_sql = "select cod_prodotto_padre, cod_versione, quan_utilizzo, cod_prodotto, cod_versione_variante " + &
								" from " + tabella_varianti + &
								" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
					choose case tabella_varianti
						case "varianti_commesse"
								ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
														" and num_commessa=" + string(num_registrazione) + &
														" and flag_esclusione<>'S'"
						case "varianti_det_off_ven"
								ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
														" and num_registrazione=" + string(num_registrazione) + &
														" and prog_riga_off_ven=" + string(prog_registrazione) + &
														" and flag_esclusione<>'S'"
						case "varianti_det_ord_ven"
								ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
														" and num_registrazione=" + string(num_registrazione)  + &
														" and prog_riga_ord_ven=" + string(prog_registrazione) + &
														" and flag_esclusione<>'S'"
						case "varianti_det_trattative"
								ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
														" and num_trattativa=" + string(num_registrazione) + &
														" and prog_trattativa=" + string(prog_registrazione) + &
														" and flag_esclusione<>'S'"
					end choose
					ls_sql = ls_sql + " and    cod_prodotto_padre='" + prodotto_finito + "'" + &
											" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
											" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
											" and    cod_versione='" + cod_versione + "'"    + &
											" and num_sequenza = " + string(ll_num_sequenza[ll_t]) + " "
					
					prepare sqlsa from :ls_sql;
		
					open dynamic cu_varianti;
					if sqlca.sqlcode < 0 then
						fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
						close cu_varianti;
						return -1
					end if
		
					fetch cu_varianti 
					into    :ls_cod_prodotto_padre_variante,
					 		:ls_cod_versione_padre_variante,
							:ldd_quan_utilizzo_variante, 
							:ls_cod_prodotto_variante,
							:ls_cod_versione_variante;
				
					
					if sqlca.sqlcode < 0 then
						fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
						close cu_varianti;
						return -1
					end if
				
					ll_max=upperbound(fstr_trova_mp_varianti)
					ll_max++		
					fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre[ll_t]
					fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre[ll_t]
					fstr_trova_mp_varianti[ll_max].cod_materia_prima = ls_cod_prodotto_figlio[ll_t]
					fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_figlio[ll_t]
					fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quantita_utilizzo[ll_t]
					
					if sqlca.sqlcode <> 100  then
						ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * quantita_utilizzo_prec
						ls_cod_prodotto_padre[ll_t] = ls_cod_prodotto_padre_variante
						ls_cod_versione_padre[ll_t] = ls_cod_versione_padre_variante
						ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
						ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
						ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
						
						fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre_variante
						fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre_variante
						fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_variante
						fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_variante
						fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quan_utilizzo_variante
						
					end if
		
					close cu_varianti;
				end if
		
			end if
			
		else
			// presenza di figli sotto al prodotto corrente; procedo con la ricorsione
			
			// sono sul semilavorato dal quale devo iniziare gli scarichi.
			// eseguo un update sul ramo di distinta.
			
			if not isnull(fs_cod_semilavorato) and ls_cod_prodotto_figlio[ll_t] =  fs_cod_semilavorato and tabella_varianti = "varianti_commesse" then
				
				if ib_crea_variante_commessa_sl then
					//se la variante commessa non esiste ne creo una uguale come il ramo distinta
					
					//Attenzione, il controllo va effettuato anche con la clausola "cod_prodotto=:ls_cod_prodotto_figlio[ll_t]" in quanto potrebbe esserci una variante
					//diversa da quella del figlio ...
					
					select cod_deposito_produzione
					into :ls_cod_deposito_produzione
					from varianti_commesse
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto_padre =:prodotto_finito and
							cod_versione =:cod_versione and
							cod_prodotto_figlio =:ls_cod_prodotto_figlio[ll_t] and
							cod_versione_figlio =:ls_cod_versione_figlio[ll_t] and
							anno_commessa =:anno_registrazione and
							num_commessa =:num_registrazione and
							num_sequenza = :ll_num_sequenza[ll_t];
					
					if sqlca.sqlcode=0 then
						//variante già presente
					else
						//---------------------------------------------------------------------------------------
						//prima di inserire la variante ...
						//verifica se per caso la variante esiste con il prodotto variante = al semilavorato
						lb_sl_variante = false
						
						select cod_deposito_produzione
						into :ls_cod_deposito_produzione
						from varianti_commesse
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_prodotto_padre =:prodotto_finito and
								cod_versione =:cod_versione and
								cod_prodotto=:ls_cod_prodotto_figlio[ll_t] and
								cod_versione_figlio =:ls_cod_versione_figlio[ll_t] and
								anno_commessa =:anno_registrazione and
								num_commessa =:num_registrazione and
								num_sequenza = :ll_num_sequenza[ll_t];
						
						if sqlca.sqlcode=0 then
							//variante presente
							lb_sl_variante = true
						end if
						//-----------------------------------------------------------------------------------------
						
						if not lb_sl_variante then
							//creo la variante
							insert into varianti_commesse
							(cod_azienda,
							 anno_commessa,
							 num_commessa,
							 cod_prodotto_padre,
							 num_sequenza,
							 cod_prodotto_figlio,
							 cod_versione_figlio,
							 cod_versione,
							 cod_prodotto,
							 cod_misura,
							 quan_tecnica,
							 quan_utilizzo,
							 dim_x,
							 dim_y,
							 dim_z,
							 dim_t,
							 coef_calcolo,
							 formula_tempo,
							 flag_esclusione,
							 des_estesa,
							 flag_materia_prima,
							 lead_time,
							 cod_versione_variante)
							select		:s_cs_xx.cod_azienda,
										 :anno_registrazione,
										 :num_registrazione,
										 :prodotto_finito,
										 :ll_num_sequenza[ll_t],
										 :ls_cod_prodotto_figlio[ll_t],
										 :ls_cod_versione_figlio[ll_t],
										 :cod_versione,
										 :ls_cod_prodotto_figlio[ll_t], 
										 cod_misura,
										 quan_tecnica,
										 quan_utilizzo,
										 dim_x,
										 dim_y,
										 dim_z,
										 dim_t,
										 coef_calcolo,
										 formula_tempo,
										 'N',
										 des_estesa,
										 flag_materia_prima,
										 lead_time,
										 :ls_cod_versione_figlio[ll_t]
							from distinta
							where	cod_azienda = :s_cs_xx.cod_azienda and
										cod_prodotto_padre =:prodotto_finito and
										num_sequenza = :ll_num_sequenza[ll_t] and
										cod_prodotto_figlio =: ls_cod_prodotto_figlio[ll_t] and
										cod_versione =:cod_versione and
										cod_versione_figlio =:ls_cod_versione_figlio[ll_t];
							
							if sqlca.sqlcode < 0 then
								fs_errore = "Errore in inserimento Varianti per commessa:" + sqlca.sqlerrtext
								return -1
							end if
						end if
						//---------------------------------------------------------------------------------------
					end if
				end if
				
				if not lb_sl_variante then
					update varianti_commesse
					set flag_scarico_parziale = 'S'
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto_padre =:prodotto_finito and
							cod_versione =:cod_versione and
							cod_prodotto_figlio =: ls_cod_prodotto_figlio[ll_t] and
							cod_versione_figlio =:ls_cod_versione_figlio[ll_t] and
							anno_commessa =:anno_registrazione and
							num_commessa =:num_registrazione and
							num_sequenza = :ll_num_sequenza[ll_t];
				else
					update varianti_commesse
					set flag_scarico_parziale = 'S'
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto_padre =:prodotto_finito and
							cod_versione =:cod_versione and
							cod_prodotto =: ls_cod_prodotto_figlio[ll_t] and
							cod_versione_figlio =:ls_cod_versione_figlio[ll_t] and
							anno_commessa =:anno_registrazione and
							num_commessa =:num_registrazione and
							num_sequenza = :ll_num_sequenza[ll_t];
				end if
				
				lb_sl_variante = false
				
				if sqlca.sqlcode = -1 then
					fs_errore = "Errore in UPDATE tabella varianti commesse con flag_scarico_parziale. " + sqlca.sqlerrtext
					return -1
				end if
				
				//#######################################################
				//memorizzo la quantita del carico del semilavorato
				ib_quan_semilavorato = true
				id_quan_semilavorato = ldd_quantita_utilizzo[ll_t]
				//#######################################################
				
				// Cambio del deposito di scarico delle MP
				select cod_deposito_produzione
				into :ls_cod_deposito_produzione
				from varianti_commesse
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto_padre =:prodotto_finito and
						cod_versione =:cod_versione and
						cod_prodotto_figlio =: ls_cod_prodotto_figlio[ll_t] and
						cod_versione_figlio =:ls_cod_versione_figlio[ll_t] and
						anno_commessa =:anno_registrazione and
						num_commessa =:num_registrazione and
						num_sequenza = :ll_num_sequenza[ll_t];
				if sqlca.sqlcode = -1 then
					fs_errore = "Errore in UPDATE tabella varianti commesse con flag_scarico_parziale. " + sqlca.sqlerrtext
					return -1
				end if
				
				if sqlca.sqlcode = 0 then
					// se lo ha trovato, sostituisco il deposito di prelievo MP con il nuovo trovato nel ramo variante
					if not isnull(ls_cod_deposito_produzione) then
						fs_cod_deposito_produzione = ls_cod_deposito_produzione
					end if
				end if
				
			end if
			
			if isnull(fs_cod_semilavorato) or  ls_cod_prodotto_figlio[ll_t] =  fs_cod_semilavorato then
				setnull(ls_cod_semilavorato)
			else
				ls_cod_semilavorato = fs_cod_semilavorato
			end if
			
			li_risposta = uof_trova_mat_prime_varianti(ls_cod_prodotto_figlio[ll_t], &
																  ls_cod_versione_figlio[ll_t], & 
																  ll_num_sequenza[ll_t], &
																  ref fs_cod_deposito_produzione, &
													  			  fstr_trova_mp_varianti[], &
																  ldd_quantita_utilizzo[ll_t], & 
													  			  anno_registrazione, &
																  num_registrazione, &
																  prog_registrazione, &
																  tabella_varianti, &
																  ls_cod_semilavorato, &
																  ls_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
			
	
		end if
		
	else
		// si tratta di una materia prima forzata (flag_materia_prima=S)
		
		if isnull(fs_cod_semilavorato) then
			// EnMe 10/2/2012; il fatto che fs_cod_semilavorato = null è il segnale che da qui in giù si può andare ad eseguire lo scarico
			ll_max=upperbound(fstr_trova_mp_varianti[])
			ll_max++		
			
			fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre[ll_t]
			fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre[ll_t]
			fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_figlio[ll_t]
			fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_figlio[ll_t]
			fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quantita_utilizzo[ll_t]
			
			if not isnull(anno_registrazione) and not isnull(num_registrazione) then
				ls_sql = "select  cod_prodotto_padre, cod_versione, quan_utilizzo,cod_prodotto, cod_versione_variante " + &
							" from " + tabella_varianti + &
							" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
				choose case tabella_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(anno_registrazione) + &
													" and num_commessa=" + string(num_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
													" and num_registrazione=" + string(num_registrazione) + &
													" and prog_riga_off_ven=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(anno_registrazione) + &
													" and num_registrazione=" + string(num_registrazione)  + &
													" and prog_riga_ord_ven=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(anno_registrazione) + &
													" and num_trattativa=" + string(num_registrazione) + &
													" and prog_trattativa=" + string(prog_registrazione) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + prodotto_finito + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and    cod_versione='" + cod_versione + "'"    + &
										" and num_sequenza = " + string(ll_num_sequenza[ll_t]) + " "
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione OPEN del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
	
				fetch cu_varianti 
				into    :ls_cod_prodotto_padre_variante,
						:ls_cod_versione_padre_variante,
						:ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante;
			
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore nell'operazione FETCH del cursore cu_varianti (f_trova_mat_prime_varianti)~r~n" + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode <> 100  then
					ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * quantita_utilizzo_prec
					ldd_quantita_utilizzo[ll_t]=ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t] = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t] = ls_cod_versione_variante
					ls_cod_prodotto_padre[ll_t] = ls_cod_prodotto_padre_variante
					ls_cod_versione_padre[ll_t] = ls_cod_versione_padre_variante
					
					fstr_trova_mp_varianti[ll_max].cod_prodotto_padre = ls_cod_prodotto_padre_variante
					fstr_trova_mp_varianti[ll_max].cod_versione_padre = ls_cod_versione_padre_variante
					fstr_trova_mp_varianti[ll_max].cod_materia_prima =  ls_cod_prodotto_variante
					fstr_trova_mp_varianti[ll_max].cod_versione_mp = ls_cod_versione_variante
					fstr_trova_mp_varianti[ll_max].quan_utilizzo = ldd_quan_utilizzo_variante
					
				end if
	
				close cu_varianti;
				
			end if
			
		end if

	end if
	
next

return 0
end function

public function integer uof_avan_prod_com (boolean ab_prima_volta, integer ai_anno_commessa, long al_num_commessa, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, double ad_quan_in_produzione, integer ai_num_livello_cor, string as_cod_tipo_commessa, ref string as_errore);// Funzione che crea i record nella tabella avanzamento_produzione_commessa
// nome: uof_avan_prod_com
// tipo: integer
//       -1 failed
//  	 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//							 fl_anno_commessa	 		 long				valore
//							 fl_num_commessa			 long				valore
//							 fl_prog_riga			    long				valore
//							 fs_cod_prodotto			 string		   valore
//							 fd_quan_in_produzione   double			valore
//							 fi_num_livello_cor   	 integer			valore
//							 fs_errore					 string			riferimento
//
//		Creata il 29-05-97 
//		Autore Diego Ferrari
//    Revisione il 29/04/2002  (aggiunto ordinamento fasi)
//    Autore Diego Ferrari
//		-------------------------------------------------------
//		Rivista per gestione integrazioni EnMe 15/11/2004


string					ls_cod_prodotto_figlio, ls_flag_esterna, & 		   
						ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_reparto, ls_cod_prodotto_inserito, & 
						ls_cod_lavorazione,ls_cod_tipo_mov_reso,ls_cod_tipo_mov_sfrido,ls_test, & 
						ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, &
						ls_cod_versione_figlio, ls_cod_versione_variante, ls_cod_versione_inserito
						
long					ll_num_figli,ll_num_righe,ll_ordinamento, ll_cont_figli, ll_cont_figli_integrazioni

integer				li_risposta

dec{4}				ldd_quan_utilizzo,ldd_tempo_attrezzaggio,ldd_tempo_attrezzaggio_commessa, & 
						ldd_tempo_lavorazione,ldd_quan_utilizzo_variante
						
datastore			lds_righe_distinta



select cod_tipo_mov_reso_semilav,
		 cod_tipo_mov_sfrido_semilav
into   :ls_cod_tipo_mov_reso,
		 :ls_cod_tipo_mov_sfrido
from   tab_tipi_commessa
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_commessa=:as_cod_tipo_commessa;


//###########################################################################
//se c'è una fase direttamente sul prodotto finito della commessa la devo considerare (fai questo solo alla prima chiamata)
string			ls_cod_reparto_pf, ls_cod_lavorazione_pf, ls_flag_esterna_pf

if ab_prima_volta then

	select		cod_reparto,cod_lavorazione,flag_esterna
	into		:ls_cod_reparto_pf, :ls_cod_lavorazione_pf, :ls_flag_esterna_pf
	from   tes_fasi_lavorazione
	where	cod_azienda = :s_cs_xx.cod_azienda AND   
				cod_prodotto = :as_cod_prodotto AND	
				cod_versione = :as_cod_versione;

	if sqlca.sqlcode=0 then
		//esiste la fase di lavorazione direttamente sul prodotto finito della commessa
		
		select max(ordinamento)
		into   :ll_ordinamento
		from   avan_produzione_com
		where  cod_azienda = :s_cs_xx.cod_azienda and    
				 anno_commessa = :ai_anno_commessa and    
				 num_commessa = :al_num_commessa and    
				 prog_riga = :al_prog_riga;
		
		if sqlca.sqlcode < 0 then
			as_errore="Errore nel DB"+ sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ll_ordinamento) or ll_ordinamento=0 then 
			ll_ordinamento=1
		else
			ll_ordinamento++
		end if
		
		//---------------------------------------------
		//inserimento in avan_produzione_com
		INSERT INTO avan_produzione_com
         (	cod_azienda, anno_commessa, num_commessa, prog_riga, cod_prodotto, cod_versione,
			cod_reparto, cod_lavorazione, anno_reg_reso, num_reg_reso, tempo_attrezzaggio, tempo_attrezzaggio_commessa, tempo_lavorazione,   
			quan_in_produzione, quan_prodotta, flag_fine_fase, cod_tipo_mov_reso, quan_utilizzata,
			cod_tipo_mov_sfrido, anno_reg_sfrido, num_reg_sfrido, quan_reso, quan_sfrido, quan_scarto,
			flag_esterna, tempo_movimentazione, flag_bolla_uscita,
			anno_reg_bol_ven, num_reg_bol_ven, prog_riga_bol_ven, ordinamento)  
		VALUES (	:s_cs_xx.cod_azienda, :ai_anno_commessa, :al_num_commessa, :al_prog_riga, :as_cod_prodotto, :as_cod_versione,
					:ls_cod_reparto_pf, :ls_cod_lavorazione_pf, null, null, 0, 0, 0,  
							:ad_quan_in_produzione, 0, 'N', :ls_cod_tipo_mov_reso, 0,   
					:ls_cod_tipo_mov_sfrido, null, null, 0,0, 0,
					:ls_flag_esterna_pf, 0, 'N',
					null, null, null, :ll_ordinamento);

		if sqlca.sqlcode < 0 then
			as_errore="Errore in creazione dati avanzamento produzione: "&
							+ as_cod_prodotto + "/" + as_cod_versione + "/" +  ls_cod_reparto_pf + "/" + ls_cod_lavorazione_pf + ": DETTAGLIO: "&
							+ sqlca.sqlerrtext
			return -1
		end if
		//---------------------------------------------
	
	end if
end if
//###########################################################################





ll_num_figli = 1
lds_righe_distinta = Create DataStore
lds_righe_distinta.DataObject = "d_data_store_distinta"
lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, as_cod_prodotto, as_cod_versione, ai_anno_commessa, al_num_commessa)
	
for ll_num_figli = 1 to ll_num_righe
	
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_versione_figlio")
	ldd_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo") * ad_quan_in_produzione
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	
	if lds_righe_distinta.getitemstring(ll_num_figli,"distinta_flag_ramo_descrittivo") = "S" then continue	

	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio

	select cod_prodotto,
	       cod_versione_variante,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_cod_versione_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :ai_anno_commessa and    
			 num_commessa = :al_num_commessa and    
			 cod_prodotto_padre = :as_cod_prodotto and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :as_cod_versione and	 
			 cod_versione_figlio = :ls_cod_versione_figlio;

   if sqlca.sqlcode < 0 then
		as_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_cod_versione_inserito = ls_cod_versione_variante
		ldd_quan_utilizzo = ldd_quan_utilizzo_variante * ad_quan_in_produzione
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' then
		
		ll_cont_figli = 0
		ll_cont_figli_integrazioni = 0
		
		select count(*)
		into   :ll_cont_figli
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		       cod_prodotto_padre = :ls_cod_prodotto_inserito and    
				 cod_versione = :ls_cod_versione_inserito;
		
		select count(*)
		into   :ll_cont_figli_integrazioni
		from   integrazioni_commessa
		where  cod_azienda = :s_cs_xx.cod_azienda and 	 
		       cod_prodotto_padre = :ls_cod_prodotto_inserito and 
				 cod_versione_padre = :ls_cod_versione_inserito and
				 anno_commessa = :ai_anno_commessa and    
				 num_commessa = :al_num_commessa;
		
		ll_cont_figli = ll_cont_figli_integrazioni + ll_cont_figli
		
		if ll_cont_figli < 1 then continue
		
	else
		
		continue
		
	end if
	

	ll_cont_figli = 0
	ll_cont_figli_integrazioni = 0
	
	select count(*)
	into   :ll_cont_figli
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_inserito and    
			 cod_versione = :ls_cod_versione_inserito;
	
	select count(*)
	into   :ll_cont_figli_integrazioni
	from   integrazioni_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and 	 
	       cod_prodotto_padre = :ls_cod_prodotto_inserito and   
			 cod_versione_padre = :ls_cod_versione_inserito and
			 anno_commessa = :ai_anno_commessa and    
			 num_commessa = :al_num_commessa;
	
	ll_cont_figli = ll_cont_figli_integrazioni + ll_cont_figli

	if ll_cont_figli < 1 then
		continue
	else
		li_risposta = uof_avan_prod_com(		false, ai_anno_commessa, al_num_commessa, al_prog_riga, &
														ls_cod_prodotto_inserito, ls_cod_versione_inserito, ldd_quan_utilizzo, ai_num_livello_cor + 1, as_cod_tipo_commessa, as_errore)
		if li_risposta = -1 then
			return -1
		end if
	end if

   SELECT cod_reparto,   
          cod_lavorazione,
			 flag_esterna
   INTO   :ls_cod_reparto,   
          :ls_cod_lavorazione,
			 :ls_flag_esterna
   FROM   tes_fasi_lavorazione
   WHERE  cod_azienda = :s_cs_xx.cod_azienda AND   
	       cod_prodotto = :ls_cod_prodotto_inserito AND	
			 cod_versione = :ls_cod_versione_inserito;

	if sqlca.sqlcode=100 then
		as_errore = "Manca la fase di lavorazione per il prodotto: " + ls_cod_prodotto_inserito
		return -1
	end if
	
	select max(ordinamento)
	into   :ll_ordinamento
	from   avan_produzione_com
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       anno_commessa = :ai_anno_commessa and    
			 num_commessa = :al_num_commessa and    
			 prog_riga = :al_prog_riga;
	
	if sqlca.sqlcode < 0 then
		as_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ll_ordinamento) or ll_ordinamento=0 then 
		ll_ordinamento=1
	else
		ll_ordinamento++
	end if  
	
	INSERT INTO avan_produzione_com
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           prog_riga,   
           cod_prodotto,   
			  cod_versione,
           cod_reparto,   
           cod_lavorazione,   
           anno_reg_reso,   
           num_reg_reso,   
           tempo_attrezzaggio,   
           tempo_attrezzaggio_commessa,   
           tempo_lavorazione,   
           quan_in_produzione,   
           quan_prodotta,   
           flag_fine_fase,   
           cod_tipo_mov_reso,
			  quan_utilizzata,
			  cod_tipo_mov_sfrido,
			  anno_reg_sfrido,
			  num_reg_sfrido,
			  quan_reso,
			  quan_sfrido,
			  quan_scarto,
			  flag_esterna,
			  tempo_movimentazione,
			  flag_bolla_uscita,
			  anno_reg_bol_ven,
			  num_reg_bol_ven,
			  prog_riga_bol_ven,
			  ordinamento)  
   VALUES ( :s_cs_xx.cod_azienda,   
            :ai_anno_commessa,   
            :al_num_commessa,   
            :al_prog_riga,   
            :ls_cod_prodotto_inserito,  
		   :ls_cod_versione_inserito,
            :ls_cod_reparto,   
            :ls_cod_lavorazione,   
            null,   
            null,   
            0,   
            0,   
            0,   
            :ldd_quan_utilizzo,
				0,
            'N',   
            :ls_cod_tipo_mov_reso,   
            0,   
            :ls_cod_tipo_mov_sfrido, 
				null,
				null,
				0,
				0,
				0,
			   :ls_flag_esterna,
			   0,
				'N',
				null,
				null,
				null,
				:ll_ordinamento)  ;

   if sqlca.sqlcode < 0 then
		as_errore="Errore in creazione dati avanzamento produzione: "&
						+ ls_cod_prodotto_inserito + "/" + ls_cod_versione_inserito + "/" +  ls_cod_reparto + "/" + ls_cod_lavorazione + ": DETTAGLIO: "&
						+ sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public function integer uof_trova_mat_prime_varianti (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, boolean ab_esegui_rollback, ref string fs_materia_prima[], ref string fs_versione_prima[], ref double add_quantita_utilizzo[], ref string as_cod_deposito_prelievo, ref string as_errore);//funzione simile alla uof_trova_mat_prime_varianti (che viene qui comunque richiamata)
//ma questa è riservata per le commesse, richiede solo anno e numero della commessa, ed il semilavorato
//le cui materie prime devono essere estratte
//
//########################################################################################################
//########################################################################################################
//# ATTENZIONE: in questa funzione è necessario un ROLLBACK per evitare di trovarsi la variante commessa del semilavorato con flag_scarico_parziale posto a SI	#
//# purtroppo qualche scriteriato a messo un update in una funzione di estrazione di materie prime ...																				#
//########################################################################################################
//########################################################################################################

//------>>>>>  IL ROLLBACK è possibile forzarlo passando nell'argomento "ab_esegui_rollback" il valore TRUE




string						ls_cod_prodotto_finito, ls_cod_versione, ls_sl

double					ldd_quan_ordine

long						ll_null


//###########################################
//lettura preliminare
select cod_prodotto, quan_ordine, cod_versione
into   :ls_cod_prodotto_finito,:ldd_quan_ordine, :ls_cod_versione
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda and anno_commessa=:ai_anno_commessa and num_commessa=:al_num_commessa;

if sqlca.sqlcode < 0 then
	as_errore = "Errore lettura dati commessa : " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	//commessa inesistente
	as_errore = "La commessa "+string(ai_anno_commessa)+"/"+string(al_num_commessa)+" è inesistente!"
	
	return -1
end if

if as_cod_semilavorato="" or isnull(as_cod_semilavorato) then
	as_errore = "Non è stato indicato il semilavorato!"
	return -1
end if
//----------------------------------------------------------------------------------------------------------------

ls_sl = as_cod_semilavorato

if ls_sl=ls_cod_prodotto_finito then
	setnull(ls_sl)
end if

//lettura MP

if uof_trova_mat_prime_varianti(			ls_cod_prodotto_finito, &
													ls_cod_versione,&
													ref as_cod_deposito_prelievo, &
													ref fs_materia_prima[],  &
													ref fs_versione_prima[], &
													ref add_quantita_utilizzo[], &
													ldd_quan_ordine, &
													ai_anno_commessa, &
													al_num_commessa, &
													ll_null, &
													"varianti_commesse", &
													ls_sl, &
													as_errore) = -1 then
													
	as_errore = "Errore (uof_trova_mat_prime_varianti): " + as_errore 
	return -1
end if

if ab_esegui_rollback then
	//#########################
	//rollback necessario perchè in presenza di semilavorato con variante commessa la procedura gli applica il flag_scarico_parziale a SI !!!!!
	rollback;
	//#########################
end if


if upperbound(fs_materia_prima[])>0 then
else
	as_errore = "Nessuna materia prima trovata per il semilavorato o prodotto " + as_cod_semilavorato + " nella commessa " + string(ai_anno_commessa)+"/"+string(al_num_commessa) + " !"
	return 1
end if

return 0
end function

public function integer uof_scarico_mp_in_conferma_doc (integer ai_anno, long al_numero, string as_tipo_doc, boolean ab_chiedi, ref decimal ad_valore_mp, ref string as_errore);/*
questa funzione elabora le righe del documento da confermare (per ora solo del tipo DDT di acquisto)
prima viene verificato se esiste il parametro flag SPT (deve valere S), inoltre sono considerate solo le righe del documento con anno e numero commessa

In questo caso la funzione effettua uno scarico delle materie prime associate al prodotto della riga del documento
Il deposito dello scarico viene fissato dal deposito del fornitore (anag_depositi where cod_fornitore=:f) del documento (caso bol_acq)

Successivamente occorrerà fuori da questa funzione effettuare il, carico del PF/SL proveniente dal terzista ...

NOTA: Se il prodotto della riga del ddt di ingresso è prorpio il semilavorato della commessa, vuol dire che in realtà
la commessa va chiusa, in quanto sei all'ultimo stadio ...

*/


boolean					lb_SPT, lb_arrivo_sl_commessa, lb_manca_deposito_terzista, lb_chiedi_quale_deposito
long						ll_count, ll_num_commessa, ll_index, ll_prog_riga, ll_index2, ll_prog_stock_1, ll_num_stock, ll_prog_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, &
							ll_anno_registrazione[], ll_num_registrazione[]
uo_magazzino			luo_mag
integer					li_ret, li_anno_commessa
string						ls_cod_prodotto, ls_materia_prima[], ls_versione_prima[], ls_deposito_mp, ls_sql, ls_vuoto[], ls_flag_mag_negativo, &
							ls_cod_ubicazione_1, ls_cod_lotto_1, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], &
							ls_cod_tipo_mov_mp, ls_cod_tipo_commessa, ls_prodotto_commessa, ls_versione_prod_commessa, ls_null

double					ldd_quantita_utilizzo[], ldd_vuoto[], ldd_giacenza_stock
datastore				lds_data
dec{4}					ld_qta_arrivata, ld_quan_prelevata, ld_valore_movimento, ldd_quan_impegnata_attuale, ld_quan_ordine_commessa, ld_qta_prodotta
datetime					ldt_data_stock_1, ldt_data_stock[], ldt_data_mov, ldt_data_registrazione

string						ls_cod_forn, ls_referenza, ls_riga
datastore				lds_stock_produzione
long ll_temp

guo_functions.uof_get_parametro("SPT", lb_SPT)

ad_valore_mp = 0
lb_manca_deposito_terzista = false
lb_chiedi_quale_deposito = false

//se non è abilitato lo scarico parziale ramo commessa in rientro da terzista esci subito
if not lb_SPT then return 0


//controlla se il magazzino può andare in negativo
select flag
into   :ls_flag_mag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	//as_errore = "Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext
	ls_flag_mag_negativo = "N"
elseif sqlca.sqlcode = 100 or ls_flag_mag_negativo <> "S" or isnull(ls_flag_mag_negativo) then
	ls_flag_mag_negativo = "N"
end if

setnull(ls_null)

//casi previsti
//bol_acq
//fat_acq

choose case as_tipo_doc
	case "bol_acq"
		//nel caso ddt acq la data dei movimenti è sempre quella relativa alla data registrazione della testata
		select cod_fornitore, data_registrazione
		into :ls_cod_forn, :ldt_data_mov
		from tes_bol_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_bolla_acq=:ai_anno and
					num_bolla_acq=:al_numero;
					
		if ls_cod_forn<>"" and not isnull(ls_cod_forn) then
			
			ls_sql = 	"SELECT cod_deposito FROM anag_depositi "+&
						"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
								"cod_fornitore='" + ls_cod_forn + "'"

			ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql)

			if ll_count < 0 then
				as_errore = "Errore in lettura deposito del terzista/fornitore: " + sqlca.sqlerrtext
				return -1
				
			elseif ll_count = 0 then
				lb_manca_deposito_terzista = true
				
			elseif ll_count>1 then
				//chiedi quale deposito considerare
				lb_chiedi_quale_deposito = true
				
			else
				//ce n'è solo uno
				ls_deposito_mp = lds_data.getitemstring(1, 1)
			end if
			destroy lds_data
			
		else
			return 0
		end if
		
		if isnull(ldt_data_mov) or year(date(ldt_data_mov))<=1950 then
			ldt_data_mov = datetime(today(), 00:00:00)
		end if
		
		
		select count(*)
		into :ll_count
		from det_bol_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_bolla_acq=:ai_anno and
					num_bolla_acq=:al_numero and
					cod_prodotto is not null and
					anno_commessa>0 and 
					num_commessa>0;
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore conteggio righe con commessa in det_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		//--------------------------------------------------------------------------------
		//se non ci sono righe con commessa esco subito
		if isnull(ll_count) or ll_count=0 then return 0
		
		
		//creo datastore delle righe di dettaglio
		ls_sql = 		"select  prog_riga_bolla_acq,"+&
									"cod_prodotto,"+&
									"quan_arrivata,"+&
									"anno_commessa,"+&
									"num_commessa "+&
							"from det_bol_acq   "+&
							"where   cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
									"anno_bolla_acq = " + string(ai_anno) + " and "+&
									"num_bolla_acq = " + string(al_numero) + " and "+&
									"(anno_registrazione_mov_mag is null or num_registrazione_mov_mag is null) and "+&
									"cod_prodotto is not null and anno_commessa>0 and num_commessa>0 and quan_arrivata>0 "+&
									"order by prog_riga_bolla_acq asc "
	case "fat_acq"
		//nel caso fat acq la data dei movimenti è la data doc altrimenti se null prendi la data registrazione della testata
		select cod_fornitore, data_doc_origine, data_registrazione
		into :ls_cod_forn, :ldt_data_mov, :ldt_data_registrazione
		from tes_fat_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero;
					
		if ls_cod_forn<>"" and not isnull(ls_cod_forn) then
			
			ls_sql = 	"SELECT cod_deposito FROM anag_depositi "+&
						"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
								"cod_fornitore='" + ls_cod_forn + "'"

			ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql)

			if ll_count < 0 then
				as_errore = "Errore in lettura deposito del terzista/fornitore: " + sqlca.sqlerrtext
				return -1
				
			elseif ll_count = 0 then
				lb_manca_deposito_terzista = true
				
			elseif ll_count>1 then
				//chiedi quale deposito considerare
				lb_chiedi_quale_deposito = true
				
			else
				//ce n'è solo uno
				ls_deposito_mp = lds_data.getitemstring(1, 1)
			end if
			destroy lds_data
			
		else
			return 0
		end if
		
		//EVITARE di creare movimenti con data registrazione vuota
		//se data_doc_origine è NULL considera per i movimenti la data registrazione,
		//che puretoppo noi programmatori siamo cosi mikioni da permettere data movimento vuota, pensa un po ...
		if isnull(ldt_data_mov) or year(date(ldt_data_mov)) <=1980 then ldt_data_mov = ldt_data_registrazione
		if isnull(ldt_data_mov) or year(date(ldt_data_mov))<=1950 then
			//se ancora null metti data oggi ...
			ldt_data_mov = datetime(today(), 00:00:00)
		end if
		
		
		select count(*)
		into :ll_count
		from det_fat_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					cod_prodotto is not null and 
					anno_commessa>0 and
					num_commessa>0;
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore conteggio righe con commessa in det_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		//--------------------------------------------------------------------------------
		//se non ci sono righe con commessa esco subito
		if isnull(ll_count) or ll_count=0 then return 0
		
		
		//creo datastore delle righe di dettaglio
		ls_sql = 		"select  prog_riga_fat_acq,"+&
									"cod_prodotto,"+&
									"quan_fatturata,"+&
									"anno_commessa,"+&
									"num_commessa "+&
							"from det_fat_acq   "+&
							"where   cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
									"anno_registrazione = " + string(ai_anno) + " and "+&
									"num_registrazione = " + string(al_numero) + " and "+&
									"(anno_registrazione_mov_mag is null or num_registrazione_mov_mag is null) and "+&
									"cod_prodotto is not null and anno_commessa>0 and num_commessa>0 and quan_fatturata>0 "+&
									"order by prog_riga_fat_acq asc "
		
	

	case else
		return 0
		
end choose
	
	
//###############################################################
//parte comune
//###############################################################
ll_count = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_count < 0 then
	return -1
	
elseif ll_count > 0 then
	//ci sono righe da movimentare del documento con commessa
	if lb_manca_deposito_terzista then
		//se manca il deposito terzista avvisa ed esci
		destroy lds_data
		as_errore = "Manca il deposito del terzista/fornitore (codice "+ls_cod_forn+") in anagrafica depositi!"
		return -1
		
	elseif lb_chiedi_quale_deposito then
		openwithparm(w_sel_deposito_terzista, ls_cod_forn)
		ls_deposito_mp = message.stringparm
		
	end if
end if


//creazione datastore stock produzione (senza dati)
guo_functions.uof_crea_datastore(	lds_stock_produzione, &
												"select cod_prodotto,"+&
														"cod_deposito,"+&
														"cod_ubicazione,"+&
														"cod_lotto,"+&
														"data_stock,"+&
														"prog_stock,"+&
														"giacenza_stock "+&
   												"from stock "+&
												"where cod_azienda='INESISTENTE'", as_errore)



//####################################################
//INIZIO CICLO RIGHE DEL DOCUMENTO
//####################################################
for ll_index=1 to ll_count
	
	//---------------------------------------------------------------------------
	//leggo dati riga documento
	ll_prog_riga = lds_data.getitemnumber(ll_index, 1)
	ls_cod_prodotto = lds_data.getitemstring(ll_index, 2)
	ld_qta_arrivata = lds_data.getitemnumber(ll_index, 3)
	li_anno_commessa = lds_data.getitemnumber(ll_index, 4)
	ll_num_commessa = lds_data.getitemnumber(ll_index, 5)
	
	ls_riga = " (riga documento "+string(ai_anno)+"/"+string(al_numero)+"/"+string(ll_prog_riga)+") "
	
	//-------------------------------------------------------------------------------------------------
	//leggo il tipo movimento da utilizzare per lo scarico delle materie prime e relativo movimento
	select cod_tipo_commessa, cod_prodotto, cod_versione, quan_ordine
	into :ls_cod_tipo_commessa, :ls_prodotto_commessa, :ls_versione_prod_commessa, :ld_quan_ordine_commessa
	from anag_commesse
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:li_anno_commessa and
				num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode < 0 then
		destroy lds_data
		as_errore = "Errore in lettura tipo commessa: " + sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=0 and (ls_cod_tipo_commessa="" or isnull(ls_cod_tipo_commessa)) then
		destroy lds_data
		as_errore = "Manca il tipo per la commessa n."+string(li_anno_commessa)+"/"+string(ll_num_commessa) + ls_riga
		return -1
	end if
	
	select cod_tipo_mov_prel_mat_prime
	into   :ls_cod_tipo_mov_mp
	from   tab_tipi_commessa
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_commessa=:ls_cod_tipo_commessa;
	
	if sqlca.sqlcode < 0 then
		destroy lds_data
		as_errore = "Errore sul db: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	ls_materia_prima[] = ls_vuoto[]
	ls_versione_prima[] = ls_vuoto[]
	ldd_quantita_utilizzo[] = ldd_vuoto[]
	
	lds_stock_produzione.reset()
		
	lb_arrivo_sl_commessa = false
	
	//---------------------------------------------------------------------------
	//lettura MP
	if ls_cod_prodotto = ls_prodotto_commessa then
		lb_arrivo_sl_commessa = true
		li_ret = uof_trova_mat_prime_varianti(	li_anno_commessa, ll_num_commessa, ls_cod_prodotto, false, &
																	ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_deposito_mp, as_errore)
																	
		if ld_quan_ordine_commessa > 0 and ld_quan_ordine_commessa<>ld_qta_arrivata then
			//ricalcolo quantità fabbisogno
			for ll_index2 = 1 to upperbound(ldd_quantita_utilizzo[])
				ldd_quantita_utilizzo[ll_index2] = (ld_qta_arrivata / ld_quan_ordine_commessa) * ldd_quantita_utilizzo[ll_index2]
			next
		end if
																	
																	
	else
		//se la variante sul semilavorato non esiste in varianti_commessa creala ...
		ib_crea_variante_commessa_sl = true
	
		//solo se viene impostato il deposito nella variante allora la variabile ls_deposito_mp viene cambiata con questo
		
		li_ret = uof_trova_mat_prime_varianti(	li_anno_commessa, ll_num_commessa, ls_cod_prodotto, false, &
															ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_deposito_mp, as_errore)
		ib_crea_variante_commessa_sl = false
		if li_ret<>0 then
			destroy lds_data
			return li_ret
		end if
		
		if ib_quan_semilavorato and id_quan_semilavorato>0 then
			for ll_index2 = 1 to upperbound(ldd_quantita_utilizzo[])
				ldd_quantita_utilizzo[ll_index2] = (ld_qta_arrivata / id_quan_semilavorato) * ldd_quantita_utilizzo[ll_index2]
			next
			//reset semafori
			ib_quan_semilavorato = false
			id_quan_semilavorato = 0
		end if
		
	end if
	
//	//le quantità utilizzo in ldd_quantita_utilizzo[] sono quelle di fabbisogno relative alla quantità totale della commessa
//	//in realtà, in base alla quantità di rientro del semilavorato, queste potrebbero differire.
//	//Infatti se in base alla distinta base per 1 PZ il semilavorato SL della riga occorrono ad esempio 1 PZ di mp1 e 2 PZ di mp2
//	//se invece per la commessa occorrono 3 PZ di SL ne scaturisce che lo scarico di mp1 ed mp2 deve essere per le seguenti quantità:
//	//  3 PZ di mp1
//	//	6 PZ di mp2
//	//se invece la giacenza delle materie prime presso il terzista è superiore io potrei ordinare una lavorazione di SL per quantità superiori al fabbisogno
//	//quindi devo scaricare una quantità superiore
//	//CALCOLO DA FARE
//	//  qtaMP scarico = (qtaSL arrivata / qta SL fabbisogno commessa ) * qtaMP fabbisogno commessa
//	//nella maggioranza dei casi qtaSL riga ddt = qta SL fabbisogno commessa quindi non ci sarà variazione ...
//	
// 	//in id_quan_semilavorato c'è il fabbisogno commessa del semilavorato
//	if ib_quan_semilavorato and id_quan_semilavorato>0 then
//		
//		for ll_index2 = 1 to upperbound(ldd_quantita_utilizzo[])
//			ldd_quantita_utilizzo[ll_index2] = (ld_qta_arrivata / id_quan_semilavorato) * ldd_quantita_utilizzo[ll_index2]
//		next
//		
//		//reset semafori
//		ib_quan_semilavorato = false
//		id_quan_semilavorato = 0
//	end if
	
	//---------------------------------------------------------------------------
	//prelievo gli stocks
	ll_num_stock = 0
	for ll_index2 = 1 to upperbound(ls_materia_prima[])

		if ldd_quantita_utilizzo[ll_index2] = 0 then continue  

		declare righe_stock dynamic cursor for sqlsa;

		ls_sql = "select cod_ubicazione,cod_lotto,data_stock,prog_stock,giacenza_stock " + &
					"from stock " + &
					"where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
							"cod_deposito='" + ls_deposito_mp + "' and " + &
							"cod_prodotto='" + ls_materia_prima[ll_index2] + "' and flag_stato_lotto='D' "

		if ls_flag_mag_negativo = "N" then
			ls_sql += " and giacenza_stock > 0 "
		end if
		ls_sql += " order by data_stock desc "

		prepare sqlsa from :ls_sql;
		open dynamic righe_stock;

		if sqlca.sqlcode <> 0 then
			destroy lds_data
			as_errore = "Errore in open cursore righe_stock: " + sqlca.sqlerrtext
			return -1
		end if

		do while 1 = 1
			fetch righe_stock
			into  :ls_cod_ubicazione_1,:ls_cod_lotto_1,:ldt_data_stock_1,:ll_prog_stock_1,	:ldd_giacenza_stock;
					
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore sul DB durante lettura cursore righe_stock: " + sqlca.sqlerrtext
				close righe_stock;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then
				if ls_flag_mag_negativo = "S" then
					//-------------------------------------------------------------------------------------------------------------
					//vuol dire che non ci sono stock. Inserisci uno stock con quantità vuota e assegnalo
					ls_cod_ubicazione_1 = "UB0001"
					ls_cod_lotto_1 = "LT0001"
					ldt_data_stock_1 = datetime(date(2000, 1, 1), 00:00:00)
					ll_prog_stock_1 = 10
					ldd_giacenza_stock = 0
					
					insert into stock
								(cod_azienda,   
								  cod_prodotto,   
								  cod_deposito,   
								  cod_ubicazione,   
								  cod_lotto,   
								  data_stock,   
								  prog_stock,   
								  giacenza_stock,   
								  quan_assegnata, quan_in_spedizione, costo_medio, cod_fornitore, cod_cliente, giacenza_inizio_anno, flag_stato_lotto, data_cambio_stato, data_scadenza)
					  values ( :s_cs_xx.cod_azienda,
								 :ls_materia_prima[ll_index2],   
								 :ls_deposito_mp,   
								 :ls_cod_ubicazione_1,   
								 :ls_cod_lotto_1,   
								 :ldt_data_stock_1,   
								 :ll_prog_stock_1,   
								 :ldd_giacenza_stock,   
								 0, 0, 0, null, null, 0, 'D', null, null);
					if sqlca.sqlcode<0 then
						destroy lds_data
						as_errore = "Errore in creazione stock per il prodotto " + ls_materia_prima[ll_index2] + " sulla commessa"+ls_riga + ": "+sqlca.sqlerrtext
						return -1
					end if
					//-------------------------------------------------------------------------------------------------------------
				else
					exit
				end if
			end if
			
			//leggo lo stock e lo memorizzo nell'array
			if ldd_giacenza_stock >= ldd_quantita_utilizzo[ll_index2] or ls_flag_mag_negativo = "S" then
				ll_num_stock = lds_stock_produzione.insertrow(0)
				lds_stock_produzione.setitem(ll_num_stock, "cod_prodotto", ls_materia_prima[ll_index2])
				lds_stock_produzione.setitem(ll_num_stock, "cod_deposito", ls_deposito_mp)
				lds_stock_produzione.setitem(ll_num_stock, "cod_ubicazione", ls_cod_ubicazione_1)
				lds_stock_produzione.setitem(ll_num_stock, "cod_lotto", ls_cod_lotto_1)
				lds_stock_produzione.setitem(ll_num_stock, "data_stock", ldt_data_stock_1)
				lds_stock_produzione.setitem(ll_num_stock, "prog_stock", ll_prog_stock_1)
				lds_stock_produzione.setitem(ll_num_stock, "giacenza_stock", ldd_quantita_utilizzo[ll_index2])
				// fine modifica ---------------------------------------------------------------------
				
				ldd_quantita_utilizzo[ll_index2] = 0
			
				exit
			else
				ldd_quantita_utilizzo[ll_index2] = ldd_quantita_utilizzo[ll_index2] - ldd_giacenza_stock
				ll_num_stock = lds_stock_produzione.insertrow(0)
				lds_stock_produzione.setitem(ll_num_stock, "cod_prodotto", ls_materia_prima[ll_index2])
				lds_stock_produzione.setitem(ll_num_stock, "cod_deposito", ls_deposito_mp)
				lds_stock_produzione.setitem(ll_num_stock, "cod_ubicazione", ls_cod_ubicazione_1)
				lds_stock_produzione.setitem(ll_num_stock, "cod_lotto", ls_cod_lotto_1)
				lds_stock_produzione.setitem(ll_num_stock, "data_stock", ldt_data_stock_1)
				lds_stock_produzione.setitem(ll_num_stock, "prog_stock", ll_prog_stock_1)
				lds_stock_produzione.setitem(ll_num_stock, "giacenza_stock", ldd_giacenza_stock)
				// fine modifica ---------------------------------------------------------------------
				
				
			end if
				
		loop
		
		close righe_stock;

		//controllo per consentire tramite il parametro aziendale CMN il magazzino negativo 17/04/2003 **
		if ldd_quantita_utilizzo[ll_index2] > 0 and ls_flag_mag_negativo = "N" then
			destroy lds_data
			as_errore = "Errore: Manca giacenza stock per la materia prima " + ls_materia_prima[ll_index2] + " sulla commessa"+ls_riga
			return -1
		end if

	next

	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	//--------------------------------------------------------------------------------------------------
	//eseguo movimenti di scarico della MP dal deposito del fornitore del ddt (terzista)
	//viene fatto un ciclo sugli stock della MP da prelevare
	for ll_index2 = 1 to lds_stock_produzione.rowcount()
		
		
		//valorizzo le variabili dello stock
		ls_cod_deposito[1] = lds_stock_produzione.getitemstring(ll_index2, "cod_deposito")						//ls_stock_produzione[ll_index2].cod_deposito
		ls_cod_ubicazione[1] = lds_stock_produzione.getitemstring(ll_index2, "cod_ubicazione")					//ls_stock_produzione[ll_index2].cod_ubicazione
		ls_cod_lotto[1] = lds_stock_produzione.getitemstring(ll_index2, "cod_lotto")								//ls_stock_produzione[ll_index2].cod_lotto
		ldt_data_stock[1] = lds_stock_produzione.getitemdatetime(ll_index2, "data_stock")						//ls_stock_produzione[ll_index2].data_stock
		ll_prog_stock[1] = lds_stock_produzione.getitemnumber(ll_index2, "prog_stock")							//ls_stock_produzione[ll_index2].prog_stock
		ld_quan_prelevata = round(lds_stock_produzione.getitemdecimal(ll_index2, "giacenza_stock"), 4)		//round(ls_stock_produzione[ll_index2].quan_prelievo, 4)
		
		ld_valore_movimento = 0
		

		//campo referenza del movimento della materia prima
		ls_referenza = string(li_anno_commessa)			//string(ai_anno)  + " C:" + right(string(li_anno_commessa), 2) + "/" + string(ll_num_commessa)
		
		if f_crea_dest_mov_mag(	ls_cod_tipo_mov_mp,&
											lds_stock_produzione.getitemstring(ll_index2, "cod_prodotto"), &
											ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
											ll_anno_reg_des_mov, ll_num_reg_des_mov, as_errore) = -1 then
			destroy lds_data
			return -1
		end if

		if f_verifica_dest_mov_mag (	ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_mp, lds_stock_produzione.getitemstring(ll_index2, "cod_prodotto")) = -1 then
			destroy lds_data
			as_errore = "Errore in verifica destinazioni movimenti magazzino sulla commessa"+ls_riga
			return -1
		end if
		
		//esattamente come avviene in avanzamento di una commessa di produzione:
		//		in mov_magazzino.num_documento metto il numero della commessa
		//		in mov_magazzino.referenza (max 20 caratteri) metto l'anno della commessa
		luo_mag = create uo_magazzino
		
		luo_mag.uof_set_flag_commessa("S")
		
		li_ret = luo_mag.uof_movimenti_mag(ldt_data_mov, &
											  ls_cod_tipo_mov_mp, &
											  "N", &
											  lds_stock_produzione.getitemstring(ll_index2, "cod_prodotto"), &
											  ld_quan_prelevata, &
											  ld_valore_movimento, &
											  ll_num_commessa, &
											  ldt_data_mov, &
											  ls_referenza, &
											  ll_anno_reg_des_mov, &
											  ll_num_reg_des_mov, &
											  ls_cod_deposito[], &
											  ls_cod_ubicazione[], &
											  ls_cod_lotto[], &
											  ldt_data_stock[], &
											  ll_prog_stock[], &
											  ls_cod_fornitore[], &
											  ls_cod_cliente[], &
											  ll_anno_registrazione[], &
											  ll_num_registrazione[], &
											  as_errore)
		destroy luo_mag
		
		if li_ret=-1 then	
			//se la variabile errore è vuota valorizzala cosi ...
			if as_errore="" or isnull(as_errore) then
				as_errore = "Errore su mov. mag, prodotto: " + lds_stock_produzione.getitemstring(ll_index2, "cod_prodotto") &
							 + ", stock: " + ls_cod_deposito[1] + "-" + ls_cod_ubicazione[1] &
							 + "-" + ls_cod_lotto[1] + "-" + string(ldt_data_stock[1]) &
							 + "-" + string(ll_prog_stock[1]) + " sulla commessa"+ls_riga
			end if
			
			destroy lds_data
			return -1
		end if

		f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)
	next
	//--------------------------------------------------------------------------------------------------
	
	
	for ll_index2 = 1 to upperbound(ls_materia_prima)
	
		select quan_impegnata_attuale
		into   :ldd_quan_impegnata_attuale
		from   impegno_mat_prime_commessa
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :li_anno_commessa and
				 num_commessa  = :ll_num_commessa and
				 cod_prodotto  = :ls_materia_prima[ll_index2];
		
		if sqlca.sqlcode < 0 then
			destroy lds_data
			as_errore = "Errore lettura qta impegnata attuale della MP "+ls_materia_prima[ll_index2]+" nella commessa"+ls_riga + " : "+ sqlca.sqlerrtext
			return -1
		end if
		
		if ldd_quan_impegnata_attuale >=  ldd_quantita_utilizzo[ll_index2] then
			
			update impegno_mat_prime_commessa
			set 	 quan_impegnata_attuale= quan_impegnata_attuale - :ldd_quantita_utilizzo[ll_index2]
			where  cod_azienda   = :s_cs_xx.cod_azienda and
					 anno_commessa = :li_anno_commessa and
					 num_commessa  = :ll_num_commessa and
					 cod_prodotto  = :ls_materia_prima[ll_index2];
			
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore disimpegno MP "+ls_materia_prima[ll_index2]+" nella commessa"+ls_riga + " : "+ sqlca.sqlerrtext
				return -1
			end if
	
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ldd_quantita_utilizzo[ll_index2]
			where  cod_azienda  = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_materia_prima[ll_index2];
			
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore aggiornamento impegnato MP "+ls_materia_prima[ll_index2]+" nella commessa"+ls_riga + " : "+ sqlca.sqlerrtext
				return -1
			end if
			
		else
			// tutta la quantità impegnata deve essere disimpegnata
			update impegno_mat_prime_commessa
			set 	 quan_impegnata_attuale = 0
			where  cod_azienda   = :s_cs_xx.cod_azienda and
					 anno_commessa = :li_anno_commessa and
					 num_commessa  = :ll_num_commessa and
					 cod_prodotto  = :ls_materia_prima[ll_index2];
			
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore disimpegno MP "+ls_materia_prima[ll_index2]+" nella commessa"+ls_riga + " : "+ sqlca.sqlerrtext
				return -1
			end if
	
			update anag_prodotti																		
			set 	 quan_impegnata = quan_impegnata - :ldd_quantita_utilizzo[ll_index2]
			where  cod_azienda   = :s_cs_xx.cod_azienda and
					 cod_prodotto  = :ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore aggiornamento impegnato MP "+ls_materia_prima[ll_index2]+" nella commessa"+ls_riga + " : "+ sqlca.sqlerrtext
				return -1
			end if
		end if
	next
	
	//################################################################################
	//se il prodotto della riga del documento è il SL della commessa, 
	//aggiorna la quantità prodotta nella commessa; se poi è stata prodotta tutta la quantità da produrre chiudi pure la commessa
	if lb_arrivo_sl_commessa then

		//leggo la quantità prodotta finora
		select quan_prodotta
		into :ld_qta_prodotta
		from anag_commesse
		where	cod_azienda   = :s_cs_xx.cod_azienda and
					anno_commessa = :li_anno_commessa and
					num_commessa  = :ll_num_commessa;

		if isnull(ld_qta_prodotta) then ld_qta_prodotta = 0
		
		//incremento la quantità prodotta nella commessa
		ld_qta_prodotta += ld_qta_arrivata
		
		//---------------------------------------------------------------------------------------------------------------------------------
		//aggiorna la quantità prodotta nella commessa
		update anag_commesse
		set		quan_prodotta=:ld_qta_prodotta
		where	cod_azienda   = :s_cs_xx.cod_azienda and
					anno_commessa = :li_anno_commessa and
					num_commessa  = :ll_num_commessa;
		
		if sqlca.sqlcode < 0 then
			destroy lds_data
			as_errore = "Errore in aggiornamento q.tà prodotta della commessa"+ls_riga+ " : "+ sqlca.sqlerrtext
			return -1
		end if
		
		//se hai prodotto quello che c'era da produrre o una quantità superiore, chiudi la commessa
		if ld_quan_ordine_commessa - ld_qta_prodotta <= 0 then
			//chiudi la commessa
			update anag_commesse
			set		data_chiusura=:ldt_data_mov,
					flag_tipo_avanzamento = '7'
			where	cod_azienda   = :s_cs_xx.cod_azienda and
						anno_commessa = :li_anno_commessa and
						num_commessa  = :ll_num_commessa;
		
			if sqlca.sqlcode < 0 then
				destroy lds_data
				as_errore = "Errore in chiusura commessa"+ls_riga+ " : "+ sqlca.sqlerrtext
				return -1
			end if
		end if
		
	
	end if
	//##########################################################
next //ciclo righe documento (lds_data)

destroy lds_stock_produzione
destroy lds_data
		
	
end function

public function integer uof_get_costo_mp (datetime adt_data_rif, string as_cod_prodotto, string as_cod_fornitore, ref decimal ad_valore, ref string as_errore);
//	prende il valore unitario della riga del più recente tra 
//									- 	l'ultimo ddt di uscita verso il terzista
//									- 	e l'ultimo ordine di acquisto a fornitore 
//										con deposito destinazione quello del terzista
//se non trova niente con questo usa il costo medio


integer				li_ret, li_anno_ddt, li_anno_ord_acq
uo_magazzino		luo_mag
string					ls_where, ls_error, ls_chiave[], ls_cod_pagamento, ls_sql
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], &
						ld_sc_1, ld_sc_2, ld_sc_3, ld_sc_4, ld_sc_5, ld_sc_6, ld_sc_7, ld_sc_8, ld_sc_9, ld_sc_10, ld_prezzo_vendita, ld_val_sconto,&
						ld_sconto_testata, ld_cambio_ven
datetime				ldt_data_reg_bol_ven, ldt_data_reg_ord_acq
long					ll_num_ddt, ll_riga_ddt, ll_num_ord_acq, ll_riga_acq
datastore			lds_data


//----------------------------------------------------------------------------------------------------------------------------------------------
//ultimi 5 ddt di uscita del prodotto verso il terzista con prezzo (prendo solo l'ultimo)
ls_sql = "select top 5 t.data_registrazione, d.anno_registrazione,d.num_registrazione,d.prog_riga_bol_ven "+&
			"from det_bol_ven as d "+&
			"join tes_bol_ven as t on t.cod_azienda=d.cod_azienda and "+&
											"t.anno_registrazione=d.anno_registrazione and "+&
											"t.num_registrazione=d.num_registrazione "+&
			"where 	d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"d.cod_prodotto='"+as_cod_prodotto+"' and "+&
						"t.cod_fornitore='"+as_cod_fornitore +"' and "+&
						"d.prezzo_vendita is not null and d.prezzo_vendita>0 "+&
			"order by t.data_registrazione desc "


li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)
if li_ret>0 then
	//leggo solo la prima riga
	ldt_data_reg_bol_ven = lds_data.getitemdatetime(1, 1)
	li_anno_ddt = lds_data.getitemnumber(1, 2)
	ll_num_ddt = lds_data.getitemnumber(1, 3)
	ll_riga_ddt = lds_data.getitemnumber(1, 4)
else
	ldt_data_reg_bol_ven = datetime(date(1900,1,1), 00:00:00)
end if
destroy lds_data

//----------------------------------------------------------------------------------------------------------------------------------------------
//ultimi 5 ordini di acquist del prodottocon deposito trasferimento pari a quello del terzista (prendo solo l'ultimo)
ls_sql = "select top 5 t.data_registrazione, d.anno_registrazione,d.num_registrazione,d.prog_riga_ordine_acq "+&
			"from det_ord_acq as d "+&
			"join tes_ord_acq as t on t.cod_azienda=d.cod_azienda and "+&
											"t.anno_registrazione=d.anno_registrazione and "+&
											"t.num_registrazione=d.num_registrazione "+&
			"where 	d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"d.cod_prodotto='"+as_cod_prodotto+"' and "+&
						"t.cod_deposito_trasf in (select cod_deposito from anag_depositi "+&
														"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																	"cod_fornitore='"+as_cod_fornitore +"') "+&
			"order by t.data_registrazione desc "


li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)
if li_ret>0 then
	//leggo solo la prima riga
	ldt_data_reg_ord_acq = lds_data.getitemdatetime(1, 1)
	li_anno_ord_acq = lds_data.getitemnumber(1, 2)
	ll_num_ord_acq = lds_data.getitemnumber(1, 3)
	ll_riga_acq = lds_data.getitemnumber(1, 4)
else
	ldt_data_reg_ord_acq = datetime(date(1900,1,1), 00:00:00)
end if
destroy lds_data

if year(date(ldt_data_reg_ord_acq)) = 1900 and year(date(ldt_data_reg_bol_ven)) = 1900 then
	//nè ordine di acquisto nè ddt di uscita: usa il costo medio
	
	for li_ret = 1 to 14
		ld_quant_val[li_ret] = 0
	next
	
	luo_mag = create uo_magazzino
	li_ret = luo_mag.uof_saldo_prod_date_decimal(as_cod_prodotto, adt_data_rif, ls_where, ld_quant_val[], ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
	destroy luo_mag
	
	if ld_quant_val[12] > 0 then
		ad_valore = ld_quant_val[13] / ld_quant_val[12]
	else
		select costo_medio_ponderato
		into   :ad_valore
		from   lifo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :as_cod_prodotto and
				 anno_lifo = (select max(anno_lifo)
								  from   lifo
								  where  cod_azienda = :s_cs_xx.cod_azienda and
											cod_prodotto = :as_cod_prodotto);
												
		if isnull(ad_valore) or ad_valore<0 then
			ad_valore = 0
		end if
	end if
	
	ad_valore = 0
	return 0
end if

if ldt_data_reg_ord_acq > ldt_data_reg_bol_ven then
	//costo da ordine di acquisto
	select 	d.sconto_1,d.sconto_2,d.sconto_3,d.sconto_4,d.sconto_5,d.sconto_6,d.sconto_7,d.sconto_8,d.sconto_9,d.sconto_10,
				d.prezzo_acquisto, t.cambio_acq, t.sconto, t.cod_pagamento
	into		:ld_sc_1,:ld_sc_2,:ld_sc_3,:ld_sc_4,:ld_sc_5,:ld_sc_6,:ld_sc_7,:ld_sc_8,:ld_sc_9,:ld_sc_10,
				:ld_prezzo_vendita, :ld_cambio_ven, :ld_sconto_testata, :ls_cod_pagamento
	from det_ord_acq as d
	join tes_ord_acq as t on t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione=:li_anno_ord_acq and
				d.num_registrazione=:ll_num_ord_acq and
				d.prog_riga_ordine_acq=:ll_riga_acq;
else
	//costo ddt di uscita
	select 	d.sconto_1,d.sconto_2,d.sconto_3,d.sconto_4,d.sconto_5,d.sconto_6, d.sconto_7,d.sconto_8,d.sconto_9,d.sconto_10,
				d.prezzo_vendita, t.cambio_ven, t.sconto, t.cod_pagamento
	into		:ld_sc_1,:ld_sc_2,:ld_sc_3,:ld_sc_4,:ld_sc_5,:ld_sc_6,:ld_sc_7,:ld_sc_8,:ld_sc_9,:ld_sc_10,
				:ld_prezzo_vendita, :ld_cambio_ven, :ld_sconto_testata, :ls_cod_pagamento
	from det_bol_ven as d
	join tes_bol_ven as t on t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione=:li_anno_ddt and
				d.num_registrazione=:ll_num_ddt and
				d.prog_riga_bol_ven=:ll_riga_ddt;
end if


//applico gli sconti riga da 1 a 10
ld_val_sconto = (ld_prezzo_vendita * ld_sc_1) / 100
ad_valore = ld_prezzo_vendita - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_2) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_3) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_4) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_5) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_6) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_7) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_8) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_9) / 100
ad_valore = ad_valore - ld_val_sconto

ld_val_sconto = (ad_valore * ld_sc_10) / 100
ad_valore = ad_valore - ld_val_sconto

//applico eventuale sconto in testata
ld_val_sconto = (ad_valore * ld_sconto_testata) / 100
ad_valore = ad_valore - ld_val_sconto

//applico eventuale sconto previsto dal pagamento (riutilizzo per questo la variabile ld_sc_10)
select sconto
into   :ld_sc_10
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then ld_sc_10 = 0

ld_val_sconto = (ad_valore * ld_sc_10) / 100
ad_valore = ad_valore - ld_val_sconto

//applico eventuale cambio
ad_valore = ad_valore * ld_cambio_ven

if isnull(ad_valore) then ad_valore = 0

ad_valore = round(ad_valore, 4)
	

return 0
end function

public function integer uof_invia_mp_terzista (integer ai_anno_ddt, long al_num_ddt, ref long al_tot_righe, ref string as_errore);//dato un ddt di uscita di conto lavoro, viene richiesto anno e numero commessa e semilavorato che il terzista deve produrre
//vengono estratte le materie prime del semilavorato, con relative quantità di fabbisogno
//quindi vengono messe in dettaglio ddt

//torna 	-1 se errore critico
//			0 se operazione annullata o altro warning
//			il numero di righe inserite in DDT se tutto OK


string							ls_flag_movimenti, ls_flag_gen_fat, ls_deposito, ls_cod_deposito_tras, ls_cod_tipo_bol_ven, ls_flag_tipo_bol_ven, ls_cod_tipo_det_ven, &
								ls_cod_cliente, ls_cod_fornitore, ls_cod_tipo_listino_prodotto, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, &
								ls_cod_iva_det, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cli[], ls_cod_forn[], ls_cod_iva, ls_cod_misura_mag, &
								ls_cod_misura, ls_des_prodotto, ls_cod_misura_ven, ls_cod_iva_cliente, ls_sql_prezzo, ls_cod_valuta

s_cs_xx_parametri			lstr_parametri
long							ll_index, ll_ordinamento, ll_prog_stock[], ll_ret, ll_prog_riga_bol_ven,ll_anno_reg_des_mov, ll_num_reg_des_mov
dec{4}						ld_cambio_ven, ld_prezzo_vendita, ld_quantita_um, ld_prezzo_um
datetime						ldt_data_registrazione, ldt_data_stock[],ldt_data_esenzione_iva, ldt_data_inizio
dec{5}						ld_fat_conversione
datastore					lds_fatture


//verifica se già confermata o già contabilizzata
select flag_movimenti, flag_gen_fat, cod_deposito, cod_deposito_tras, cod_tipo_bol_ven
into :ls_flag_movimenti, :ls_flag_gen_fat, :ls_deposito, :ls_cod_deposito_tras, :ls_cod_tipo_bol_ven
from tes_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and 
			anno_registrazione=:ai_anno_ddt and 
			num_registrazione=:al_num_ddt;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo ddt già confermato/fatturato: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Sembra che il DDT "+string(ai_anno_ddt)+"/"+string(al_num_ddt)+" non esista!"
	return -1
	
elseif ls_flag_gen_fat="S" then
	as_errore = "Impossibile eseguire questa operazione: il ddt è stato già fatturato!"
	return 1
	
elseif ls_flag_movimenti="S" then
	as_errore = "Impossibile eseguire questa operazione: il ddt è stato già confermato!"
	return 1
	
end if


if ls_cod_tipo_bol_ven="" or isnull(ls_cod_tipo_bol_ven) then
	as_errore = "Non è stato indicato il tipo del DDT!"
	return 1
end if

if ls_deposito="" or isnull(ls_deposito) then
	as_errore = "Nel DDT non è stato indicato il deposito di partenza (campo Deposito)!"
	return 1
end if

if ls_cod_deposito_tras="" or isnull(ls_cod_deposito_tras) then
	as_errore = "Nel DDT non è stato indicato il deposito di destinazione (campo Deposito Arrivo)!"
	return 1
end if

select flag_tipo_bol_ven, cod_tipo_det_ven
into :ls_flag_tipo_bol_ven, :ls_cod_tipo_det_ven
from tab_tipi_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_bol_ven=:ls_cod_tipo_bol_ven;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo tipo ddt : "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_bol_ven="C" then
else
	as_errore = "Il tipo del DDT deve essere Conto Lavorazione!"
	return 1
end if


//apri una finestra per immissione anno e numero commessa di riferimento ed SL
//visualizza le MP coinvolte e relativa giacenza nel deposito partenza e nel deposito destinazione (terzista)
//l'utente deve poter togliere alcune righe
lstr_parametri.parametro_s_1_a[1] = ls_deposito
lstr_parametri.parametro_s_1_a[2] = ls_cod_deposito_tras
lstr_parametri.parametro_s_1_a[3] = ls_cod_tipo_det_ven

openwithparm(w_invio_mp_terzista, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_b_1 then
	//prosegui con l'inserimento nei dettagli (   in lstr_parametri.parametro_s_1_a[] e lstr_parametri.parametro_d_1_a[] ci sono i codici e le quantità)
else
	as_errore = "Operazione annullata dall'operatore!"
	return 1
end if

//tipo dettaglio da usare
ls_cod_tipo_det_ven = lstr_parametri.parametro_s_1

//ulteriori dati dalla testata

//cliente vuoto, il DDT e a fornitore (conto lavoro)
select		cod_cliente, cod_fornitore,
			cod_tipo_listino_prodotto,
			cod_valuta,
			cambio_ven,
			data_registrazione
into	:ls_cod_cliente, :ls_cod_fornitore,
		:ls_cod_tipo_listino_prodotto,
		:ls_cod_valuta,
		:ld_cambio_ven,
		:ldt_data_registrazione
from tes_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and 
			anno_registrazione=:ai_anno_ddt and 
			num_registrazione=:al_num_ddt;

//il tipo dettaglio sarà lo steso per tutte le righe
select flag_tipo_det_ven, cod_tipo_movimento, cod_iva
into   :ls_flag_tipo_det_ven, :ls_cod_tipo_movimento, :ls_cod_iva_det
from   tab_tipi_det_ven
where	cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if ls_flag_tipo_det_ven = "M" then
	ll_ordinamento = 0		
	
	select min(ordinamento)
	into   :ll_ordinamento
	from   det_tipi_movimenti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_movimento = :ls_cod_tipo_movimento;
			 
	if ll_ordinamento <> 0 and not isnull(ll_ordinamento) then
		setnull(ls_cod_deposito[1])
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cli[1])
		setnull(ls_cod_forn[1])
		
		select cod_deposito, cod_ubicazione, cod_lotto, data_stock, prog_stock
		into     :ls_cod_deposito[1], :ls_cod_ubicazione[1], :ls_cod_lotto[1], :ldt_data_stock[1], :ll_prog_stock[1]
		from   det_tipi_movimenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_movimento = :ls_cod_tipo_movimento and
				 ordinamento = :ll_ordinamento;
				 
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore in ricerca dati stock da tipi_movimenti " + sqlca.sqlerrtext
			return -1
		end if
		
		if not isnull(ls_deposito) then ls_cod_deposito[1] = ls_deposito
		
		if not isnull(ls_cod_deposito_tras) then
			ls_cod_deposito[2] = ls_cod_deposito_tras
			ls_cod_ubicazione[2] = ls_cod_ubicazione[1]
			ls_cod_lotto[2] = ls_cod_lotto[1]
			ldt_data_stock[2] = ldt_data_stock[1]
			ll_prog_stock[2] = ll_prog_stock[1]
			setnull(ls_cod_cli[2])
			setnull(ls_cod_forn[2])
		end if			
	end if
else
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])  
end if

al_tot_righe = 0
if upperbound(lstr_parametri.parametro_s_1_a[]) = 0 then
	as_errore = "Nessuna MP da inviare per produrre il semilavorato!"
	return 1
end if

for ll_index=1 to upperbound(lstr_parametri.parametro_s_1_a[])

	//leggo dati materia prima
	select cod_misura_mag, cod_misura_ven, fat_conversione_ven, cod_iva, des_prodotto, cod_misura_ven
	into	:ls_cod_misura_mag, :ls_cod_misura, :ld_fat_conversione, :ls_cod_iva, :ls_des_prodotto, :ls_cod_misura_ven
	from   anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_prodotto = :lstr_parametri.parametro_s_1_a[ll_index];

	if isnull(ls_cod_iva) then ls_cod_iva = ls_cod_iva_det

	if not isnull(ls_cod_cliente) then
		select cod_iva, data_esenzione_iva
		into   :ls_cod_iva_cliente, :ldt_data_esenzione_iva
		from   anag_clienti
		where	cod_azienda = :s_cs_xx.cod_azienda and 
					cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode = 0 then
			if not isnull(ls_cod_iva_cliente) and ls_cod_iva_cliente <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
				ls_cod_iva = ls_cod_iva_cliente
			end if
		end if
		
	elseif not isnull(ls_cod_fornitore) then
		select cod_iva, data_esenzione_iva
		into   :ls_cod_iva_cliente, :ldt_data_esenzione_iva
		from   anag_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_fornitore = :ls_cod_fornitore;
	
		if not isnull(ls_cod_iva_cliente) and ls_cod_iva_cliente <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
				ls_cod_iva = ls_cod_iva_cliente
		end if
	end if	


	//----------------------------------------------------------------------------------------------------------------
	//elaboro il valore unitario del movimento dall'ultima fattura/ddt di acquisto confermati  (esso sarà in [€/UMmag])
	//prendo per semplicità gli utlimi 365 giorni
	ldt_data_inizio = datetime(RelativeDate(date(ldt_data_registrazione), -365) , 00:00:00)
	
	ls_sql_prezzo = "select mov_magazzino.val_movimento,"+&
									"tes_fat_acq.data_registrazione as data_doc_fiscale,det_fat_acq.num_registrazione as num_doc_fiscale,det_fat_acq.prog_riga_fat_acq as riga_doc_fiscale,'F' as tipo_doc_fiscale "+&
						"from tes_fat_acq join det_fat_acq on tes_fat_acq.anno_registrazione=det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione=det_fat_acq.num_registrazione "+&
						"join mov_magazzino on det_fat_acq.anno_registrazione_mov_mag=mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione "+&
						"where tes_fat_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"tes_fat_acq.data_registrazione<='"+string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and "+&
									"tes_fat_acq.data_registrazione>='"+string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+"' and "+&
									"det_fat_acq.flag_agg_mov='S' and " +&
									"det_fat_acq.cod_prodotto='"+lstr_parametri.parametro_s_1_a[ll_index]+"' "+&
						"union "+&
						"select mov_magazzino.val_movimento,"+&
									"tes_bol_acq.data_registrazione as data_doc_fiscale, tes_bol_acq.num_bolla_acq as num_doc_fiscale, det_bol_acq.prog_riga_bolla_acq as riga_doc_fiscale,'B' as tipo_doc_fiscale "+&
						"from det_bol_acq "+&
						"join tes_bol_acq on tes_bol_acq.cod_azienda=det_bol_acq.cod_azienda and tes_bol_acq.anno_bolla_acq=det_bol_acq.anno_bolla_acq and tes_bol_acq.num_bolla_acq=det_bol_acq.num_bolla_acq "+&
						"join mov_magazzino on det_bol_acq.anno_registrazione_mov_mag=mov_magazzino.anno_registrazione and det_bol_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione "+&
						"where tes_bol_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"tes_bol_acq.data_registrazione<='"+string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and "+&
									"tes_bol_acq.data_registrazione>='"+string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+"' and "+&
									"det_bol_acq.cod_prodotto='"+lstr_parametri.parametro_s_1_a[ll_index]+"' "+&
						"order by  data_doc_fiscale DESC, num_doc_fiscale DESC, riga_doc_fiscale DESC "


	ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo, as_errore)
	
	if ll_ret < 0 then
		return -1
	end if
	
	if ll_ret > 0 then
		//lo leggo dalla prima riga del datastore, prima colonna c'è il valore unitario del movimento
		ld_prezzo_vendita = lds_fatture.getitemnumber (1, 1)
	else
		ld_prezzo_vendita = 0
	end if
	destroy lds_fatture
	
	
	//----------------------------------------------------------------------------------------------------------------
	//leggo il max progressivo riga del DDT e lo incremento di 10
	select 	max(prog_riga_bol_ven)
	into 		:ll_prog_riga_bol_ven
	from 		det_bol_ven
	where	cod_azienda = :s_cs_xx.cod_azienda  and
				anno_registrazione = :ai_anno_ddt and
				num_registrazione = :al_num_ddt;

	if ll_prog_riga_bol_ven = 0 or isnull(ll_prog_riga_bol_ven) then
		ll_prog_riga_bol_ven = 10
	else
		ll_prog_riga_bol_ven += 10
	end if
	
	
	//----------------------------------------------------------------------------------------------------------------
	//ld_prezzo_vendita 												è in [€/UMmag]
	//ld_fat_conversione 	vendita									è in [UMven/UMmag]
	//quindi ld_prezzo_vendita / ld_fat_conversione			è in [€/UMven]					ottengo cioè il prezzo in unità misura vendita
	
	//la variabile ld_quantita_um esprime la quantità in unità di vendita [UMven]
	
	
	//controllo presenza di eventuale fattore di conversione se differenza tra UMmag e UMven
	if ls_cod_misura_mag <> ls_cod_misura_ven then
		//le UM mag e ven sono diverse
		if ld_fat_conversione = 1 then
			ld_quantita_um = lstr_parametri.parametro_d_1_a[ll_index]													//	in [UMven]
			ld_prezzo_um = ld_prezzo_vendita																					//	[€/UMven]
		else
			ld_quantita_um = round(lstr_parametri.parametro_d_1_a[ll_index] * ld_fat_conversione, 4)			//	in [UMven]
			ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione,4)												//	[€/UMven]
		end if
	else
		//le UM coincidono quindi nessuna conversione
		ld_quantita_um = lstr_parametri.parametro_d_1_a[ll_index]														//	in [UMven]
		ld_prezzo_um = ld_prezzo_vendita																						//	[€/UMven]
	end if

	//----------------------------------------------------------------------------------------------------------------
	//inserimento riga in DDT
 	insert into det_bol_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_bol_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           progr_stock,   
           data_stock,   
           cod_misura,   
           des_prodotto,   
           quan_consegnata,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           nota_dettaglio,   
           anno_registrazione_ord_ven,   
           num_registrazione_ord_ven,   
           prog_riga_ord_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           anno_reg_bol_acq,   
           num_reg_bol_acq,   
           prog_riga_bol_acq,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione,   
           flag_doc_suc_det,   
           flag_st_note_det,
           quantita_um,   
           prezzo_um,   
           imponibile_iva,   
           imponibile_iva_valuta )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ai_anno_ddt,   
           :al_num_ddt,   
           :ll_prog_riga_bol_ven,   
           :lstr_parametri.parametro_s_1_a[ll_index],   
           :ls_cod_tipo_det_ven,   
           :ls_cod_deposito[1],   
           :ls_cod_ubicazione[1],   
           :ls_cod_lotto[1],   
           :ll_prog_stock[1],   
           :ldt_data_stock[1],   
           :ls_cod_misura_ven,   
           :ls_des_prodotto,   
           :lstr_parametri.parametro_d_1_a[ll_index],   
           :ld_prezzo_vendita,   
           :ld_fat_conversione,   
           0,   
           0,   
           0,   
           0,   
           :ls_cod_iva,   
           :ls_cod_tipo_movimento,   
           null,   
           '',   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           'I',   
           'I',   
           :ld_quantita_um,   
           :ld_prezzo_um,   
           0,   
           0 );
			  
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in inserimento riga DDT: " + sqlca.sqlerrtext
		return -1
	end if

	//----------------------------------------------------------------------------------------------------------------
	//creo le destinazioni movimento
	if ls_flag_tipo_det_ven = "M" then
		if f_crea_dest_mov_magazzino(	ls_cod_tipo_movimento, lstr_parametri.parametro_s_1_a[ll_index], &
													ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cli[], ls_cod_forn[], &
													ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			as_errore= "Si è verificato un errore in fase di creazione destinazione movimenti!"
			return -1
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, lstr_parametri.parametro_s_1_a[ll_index]) = -1 then
			as_errore = "Errore in fase di verifica destinazioni movimenti!"
			return -1
		end if
		
		update 	det_bol_ven
		set 		anno_reg_des_mov = :ll_anno_reg_des_mov,
					num_reg_des_mov  = :ll_num_reg_des_mov
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_ddt and
					num_registrazione  = :al_num_ddt and
					prog_riga_bol_ven  = :ll_prog_riga_bol_ven;
		if sqlca.sqlcode = -1 then
			as_errore = "Errore in fase di aggiornamento anno_reg_des_mov e num_reg_des_mov : " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
	al_tot_righe += 1
next

return 0
end function

public subroutine uof_set_nonconsidera_flagparziale (boolean ab_non_considerare_flag_parziale);
ib_non_considerare_flag_parziale = ab_non_considerare_flag_parziale

return
end subroutine

public subroutine uof_set_from_ddt_trasf (boolean ab_from_ddt_trasf, string as_deposito_from, string as_deposito_to, integer ai_anno_commessa, long al_num_commessa);


if isnull(ab_from_ddt_trasf) then ab_from_ddt_trasf = false

ib_from_ddt_trasf = ab_from_ddt_trasf

if ab_from_ddt_trasf then
	is_dep_from = as_deposito_from
	is_dep_to = as_deposito_to
	ii_anno_comm_ddttrasf = ai_anno_commessa
	il_num_comm_ddttrasf = al_num_commessa
else
	is_dep_from = ""
	is_dep_to = ""
	ii_anno_comm_ddttrasf = 0
	il_num_comm_ddttrasf = 0
end if

return
end subroutine

public function integer uof_annulla_attivazione_commessa (long al_anno_commessa, long al_num_commessa, ref string as_errore);double  ldd_quan_in_produzione, & 
		  ldd_quan_prodotta,ldd_quan_da_assegnare,ldd_quantita_possibile, & 
		 ldd_quan_impegnata
long    ll_prog_riga,ll_prog_stock[1],ll_anno_reg_des_mov, ll_num_reg_des_mov
string  ls_test,ls_cod_deposito_prelievo, ls_cod_tipo_mov_anticipo, ls_flag_tipo_avanzamento, & 
		  ls_cod_tipo_commessa,ls_cod_tipo_movimento,ls_cod_prodotto,ls_cod_deposito[1], & 
		  ls_cod_ubicazione[1],ls_cod_lotto[1],ls_cod_cliente[1],ls_cod_fornitore[1],ls_cod_versione, ls_flag_muovi_mp
long    li_risposta
date 	  ld_data_stock[1]

select 	quan_in_produzione,
			cod_prodotto,
			cod_versione,
			cod_tipo_commessa,
			flag_tipo_avanzamento
into		:ldd_quan_in_produzione,
			:ls_cod_prodotto,
			:ls_cod_versione,
			:ls_cod_tipo_commessa,
			:ls_flag_tipo_avanzamento
from		anag_commesse
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :al_anno_commessa and
			num_commessa = :al_num_commessa;
if sqlca.sqlcode = 100 then
	as_errore = g_str.format("Commessa $1/$2 inesistente",al_anno_commessa,al_num_commessa)
	return -1
end if

if ls_flag_tipo_avanzamento <> "0" and ls_flag_tipo_avanzamento <> "2" and ls_flag_tipo_avanzamento <> "8" then
	as_errore = g_str.format("Commessa $1/$2  già stata lanciata in produzione con un altro metodo, pertanto non è possibile compiere alcuna operazione con questo metodo.",al_anno_commessa,al_num_commessa)
	return -1
end if

// --- enme
delete   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :al_anno_commessa and    
		 num_commessa = :al_num_commessa;

select cod_azienda
into   :ls_test
from   det_orari_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and    
       anno_commessa = :al_anno_commessa and    
		 num_commessa = :al_num_commessa;

if sqlca.sqlcode = 100 then
	
	setpointer(hourglass!)
	
	//	***	Michela 05/06/2007: disimpegno le materie prime della commessa
	
	select flag_muovi_mp
	into	:ls_flag_muovi_mp
	from	tab_tipi_commessa
	where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_commessa = :ls_cod_tipo_commessa;
			
	if sqlca.sqlcode <> 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in ricerca flag_muovi_mp in tabella tipi commessa con tipo_commessa=$3.",al_anno_commessa,al_num_commessa,ls_cod_tipo_commessa)
		return -1
	end if
	
	if ls_flag_muovi_mp ='S' then
	
		luo_magazzino = create uo_magazzino
			
		if luo_magazzino.uof_impegna_mp_commessa(	false, &
																false, &
																ls_cod_prodotto, &
																ls_cod_versione, &
																ldd_quan_in_produzione , &
																al_anno_commessa, &
																al_num_commessa, &
																as_errore) = -1 then
			as_errore = g_str.format("Commessa $1/$2: Errore in fase di disimpegno materie prime~r~n"+as_errore ,al_anno_commessa,al_num_commessa)
			destroy luo_magazzino;
			return -1
		end if
		
		destroy luo_magazzino;
	
	end if		
	
	//	***	fine modifica
	
	delete 	avan_produzione_com
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase cancellazione avanzamento di produzione~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	delete		mat_prime_commessa_stock
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase cancellazione da tabella mat_prime_commessa_stock~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	delete		mp_com_stock_det_orari
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase cancellazione da tabella mp_com_stock_det_orari~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	delete		mat_prime_commessa
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase cancellazione da tabella mat_prime_commessa~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	delete 	det_anag_commesse
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase cancellazione da tabella det_anag_commesse~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	update	anag_commesse
	set    		quan_in_produzione = 0,
			 	flag_tipo_avanzamento = '0'
	where  	cod_azienda = :s_cs_xx.cod_azienda and    
	       		anno_commessa = :al_anno_commessa and    
			 	num_commessa = :al_num_commessa;

	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Commessa $1/$2: Errore in fase aggiornamento tabella anag_commesse~r~n"+sqlca.sqlerrtext  ,al_anno_commessa,al_num_commessa)
		rollback;
		setpointer(arrow!)
		return -1
	end if

	commit;

else
	as_errore = g_str.format("Commessa $1/$2: Ci sono dei dettagli orari in alcune sottocommesse, pertanto non è possibile annullarne l'attivazione." ,al_anno_commessa,al_num_commessa)
	return -1
end if

return 0
end function

public function integer uof_ricerca_quan_mp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_prodotto, string fs_cod_gruppo_variante, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_registrazione, string fs_tab_varianti, ref string fs_cod_materia_prima, ref decimal fd_quan_materia_prima, ref string fs_errore);/* ------------------------------------------------------------------------------------
	Funzione che trova la materia prima e la sua quantità partendo dal gruppo variante
 nella tabella <tabella_varianti>

 nome: uof_ricerca_quantita_mp
 tipo: integer

 return:
 		-1 errore
			 0 trovato materia prima tutto OK
  		 1 trovato nulla
			 2 valido solo all'interno indica l'uscita

	Variabili passate: 		nome					 tipo				passaggio per			commento
							

	Creata il 10-05-2000 Enrico 
	modificata il 31/10/2006 per adeguarla alla nuova gestione distinta base con versioni figli diversi dalla versione padre
 ------------------------------------------------------------------------------------
*/
string  ls_cod_prodotto_figlio[],ls_cod_prodotto_variante, ls_sql, ls_flag_materia_prima, &
		  ls_flag_materia_prima_variante,ls_errore, ls_cod_gruppo_variante, ls_cod_versione_figlio[], &
		  ls_cod_versione_variante
long    ll_conteggio,ll_t,ll_max, ll_i, ll_cont, ll_num_legami, ll_num_sequenza,ll_test
integer li_risposta
dec{4}  ldd_quantita_utilizzo,ldd_quan_utilizzo_variante

ll_conteggio = 1

declare cu_varianti dynamic cursor for sqlsa;

declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
        cod_versione_figlio,
        quan_utilizzo,
		  flag_materia_prima,
		  num_sequenza
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda 
and     cod_prodotto_padre = :fs_cod_prodotto
and     cod_versione=:fs_cod_versione;

open righe_distinta_3;

do while 1 = 1

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
		   :ls_cod_versione_figlio[ll_conteggio],
		   :ldd_quantita_utilizzo,
			:ls_flag_materia_prima,
			:ll_num_sequenza;

   if (sqlca.sqlcode = 100) then 
		close righe_distinta_3;	
		exit
	end if

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_distinta_3;
		return -1
	end if
	
	if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then
		
		ls_sql = "select quan_utilizzo, " + &
					" cod_prodotto, " + &
					" cod_versione_variante, " + &
					" flag_materia_prima " + &
					" from " + fs_tab_varianti + &
					" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
		choose case fs_tab_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
											" and num_commessa=" + string(fl_num_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
											" and num_registrazione=" + string(fl_num_registrazione) + &
											" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
											" and num_registrazione=" + string(fl_num_registrazione) + &
											" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
											" and num_trattativa=" + string(fl_num_registrazione) + &
											" and prog_trattativa=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + fs_cod_versione + "'"
		
		prepare sqlsa from :ls_sql;
		open dynamic cu_varianti;

		if sqlca.sqlcode<>0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if


		fetch cu_varianti 
		into :ldd_quan_utilizzo_variante, 
			  :ls_cod_prodotto_variante,
			  :ls_cod_versione_variante,
			  :ls_flag_materia_prima_variante;
				
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode =0  then
			ldd_quantita_utilizzo = ldd_quan_utilizzo_variante
			ls_flag_materia_prima = ls_flag_materia_prima_variante
			
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 num_sequenza        = :ll_num_sequenza and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione;
					 
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = fs_cod_gruppo_variante then
					fs_cod_materia_prima   = ls_cod_prodotto_figlio[ll_conteggio]
					fd_quan_materia_prima  = ldd_quantita_utilizzo
					close cu_varianti;
					close righe_distinta_3;
					return 0
				end if
			end if
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
		end if
		
		ldd_quantita_utilizzo = ldd_quantita_utilizzo * fd_quan_prodotto
		ll_conteggio++	
	
		close cu_varianti;
		// ----------------- verifico se gruppo variante corrisponde ------------------
	end if
loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1

   if ls_flag_materia_prima = 'N' then
		
		select count(*)
		into   :ll_test
		from   distinta  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
				 cod_versione = :ls_cod_versione_figlio[ll_t];
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		if ll_test = 0 or isnull(ll_test) then				// è una materia prima
			if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then		// verifico se ha varianti
				ls_sql = "select quan_utilizzo, " + &
							" cod_prodotto, " + &
							" cod_versione_variante " + &
							" from " + fs_tab_varianti + &
							" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
				choose case fs_tab_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
													" and num_commessa=" + string(fl_num_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
													" and num_registrazione=" + string(fl_num_registrazione) + &
													" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
													" and num_registrazione=" + string(fl_num_registrazione)  + &
													" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
													" and num_trattativa=" + string(fl_num_registrazione) + &
													" and prog_trattativa=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and    cod_versione='" + fs_cod_versione + "'" 
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
	
				fetch cu_varianti 
				into  :ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante;
			
				
				if sqlca.sqlcode < 0 then 
					fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode <> 100  then
					ldd_quan_utilizzo_variante    = ldd_quan_utilizzo_variante * fd_quan_prodotto
					ldd_quantita_utilizzo         = ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t]  = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t]  = ls_cod_versione_variante
					
				end if
	
				close cu_varianti;
			end if
			
		else		// è una semilavorato; vado ad esaminare ulteriori livelli
			
			li_risposta = uof_ricerca_quan_mp(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ldd_quantita_utilizzo, &
			                                  fs_cod_gruppo_variante, fl_anno_registrazione, fl_num_registrazione, fl_prog_registrazione, fs_tab_varianti, &
														 fs_cod_materia_prima, fd_quan_materia_prima, fs_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
			if li_risposta = 0 then
				return 0
			end if
		end if
	else				// flag mp = "S"
		if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then
			ls_sql = "select quan_utilizzo, " + &
						" cod_prodotto, " + &
						" cod_versione_variante " + &
						" from " + fs_tab_varianti + &
						" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
			choose case fs_tab_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
												" and num_commessa=" + string(fl_num_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
												" and num_registrazione=" + string(fl_num_registrazione) + &
												" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
												" and num_registrazione=" + string(fl_num_registrazione)  + &
												" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
												" and num_trattativa=" + string(fl_num_registrazione) + &
												" and prog_trattativa=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
									" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
									" and    cod_versione='" + fs_cod_versione + "'" 
			
			prepare sqlsa from :ls_sql;

			open dynamic cu_varianti;

			fetch cu_varianti 
			into  :ldd_quan_utilizzo_variante, 
					:ls_cod_prodotto_variante,
					:ls_cod_versione_variante;
		
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if
		
			if sqlca.sqlcode <> 100  then
				ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * fd_quan_prodotto
				ldd_quantita_utilizzo=ldd_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t]=ls_cod_prodotto_variante
				ls_cod_versione_figlio[ll_t]=ls_cod_versione_variante
			end if
			close cu_varianti;
			
			// --- verifico corrispondenza del gruppo variante -----------------------
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto_padre = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_t] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_t] and
					 cod_versione = :fs_cod_versione;
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = fs_cod_gruppo_variante then
					fs_cod_materia_prima = ls_cod_prodotto_figlio[ll_t]
					fd_quan_prodotto = ldd_quantita_utilizzo
					return 0
				end if
			end if
	
		end if
	end if
next
return 1
end function

public function integer uof_giacenze_stock_prodotto_deposito (string as_cod_prodotto, string as_cod_deposito, datetime adt_data_riferimento_stock, ref datastore ads_data);string ls_sql, ls_errore, ls_error, ls_chiave[], ls_vuoto[], ls_where, ls_array_elementi[]
long	ll_ret, ll_i, ll_return, ll_row, ll_t, ll_elemento_prog_stock
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_vuoto[]
datetime ldt_data_rif, ldt_elemento_data_stock
datastore 		lds_data	
uo_magazzino luo_mag

ldt_data_rif = datetime(today(),00:00:00)

ld_quant_val = ld_vuoto
ld_giacenza_stock = ld_vuoto
ld_costo_medio_stock = ld_vuoto
ld_quan_costo_medio_stock = ld_vuoto

ls_chiave = ls_vuoto

luo_mag = create uo_magazzino

if not isnull(as_cod_deposito) then 
	luo_mag.is_considera_depositi_fornitori = "I"
	luo_mag.is_cod_depositi_in = "('" +  as_cod_deposito + "')"
end if

ll_return = luo_mag.uof_saldo_prod_date_decimal ( as_cod_prodotto, ldt_data_rif, "", ref ld_quant_val[], ref ls_error, "S", ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[] )

ads_data = create datastore
ads_data.reset()

ads_data.dataobject = 'd_ds_funzioni_1_giacenza_stock'

if upperbound(ls_chiave) > 0 then
	for ll_t = 1 to upperbound(ls_chiave)

		if len( trim(ls_chiave[1])) = 0 then continue 
		
		
		g_str.explode( ls_chiave[ll_t], "-", ref ls_array_elementi[])
		ldt_elemento_data_stock = datetime( date(ls_array_elementi[4]), 00:00:00)
		ll_elemento_prog_stock = long(ls_array_elementi[5])
		
		if not isnull(adt_data_riferimento_stock) and ldt_elemento_data_stock < adt_data_riferimento_stock then continue

		ll_row = ads_data.insertrow(0)

		ads_data.setitem(ll_row, 1, as_cod_prodotto)
		ads_data.setitem(ll_row, 2, ls_array_elementi[1])
		ads_data.setitem(ll_row, 3, ls_array_elementi[2])
		ads_data.setitem(ll_row, 4, ls_array_elementi[3])
		ads_data.setitem(ll_row, 5, ldt_elemento_data_stock)
		ads_data.setitem(ll_row, 6, ll_elemento_prog_stock)
		ads_data.setitem(ll_row, 7, ld_giacenza_stock[ll_t])
	next
end if		

destroy luo_mag
destroy lds_data

ads_data.setsort("adt_data_stock, al_prog_stock")
ads_data.sort()

return 0
end function

public function integer uof_chiudi_commessa_fasi (long al_anno_commessa, long al_num_commessa, string as_reparto_riferimento, boolean ab_forza_chiusura, string as_log, ref string as_errore);// Funzione che esegue l'avanzamento della commessa con fasi di avanzqamento senza che l'utente intervenga per assegnare gli
// stock di materia prima. (Vedi specifica 201--ProduzioneCommesse rev 2 per eurocablaggi Srl)
// Per default saranno presi gli stock più vecchi
// 
// RITORNO: integer
//       -1 failed
//  	0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per	
//							
//							 al_anno_commessa	 		 integer			valore
//							 al_num_commessa			 long				valore
//							 as_reparto_riferimento		string				valore reparto; questo è il reparto che fa da riferimento per la quantità da mettere sulle fasi aperte (per eurocablaggi è sempre RTC)
//							 ab_forza_chiusura			boolean			S/N in genere le fasi vengono chiuse basandosi sulla quantità prodotta prodotta da as_reparto_riferimento; con questo flag forzo la chiusura con la quantità della commessa
//							 as_errore					 	string				eventuale messaggio di errore
//
//		Creata il 19/11/2019
//		Autore Enrico Menegotto

string			ls_sql, ls_errore, ls_flag_tipo_avanzamento, ls_cod_tipo_commessa, ls_flag_muovi_sl, ls_cod_deposito_sl[], ls_cod_tipo_mov_prel_sl, ls_cod_tipo_mov_ver_sl, ls_flag_muovi_mp , &
				ls_cod_deposito_prelievo,ls_cod_tipo_mov_prel_mat_prime,ls_cod_deposito_versamento,ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, &
				ls_vuoto[], 	ls_cod_ubicazione[], ls_cod_lotto[] ,ls_cod_cliente[], ls_cod_fornitore[], ls_cod_deposito_mp[], ls_cod_prodotto_finito, ls_cod_deposito_versamento_pf, ls_cod_ubicazione_pf, &
				ls_cod_lotto_pf, ls_cod_lavorazione_mp, ls_cod_reparto_mp,ls_cod_versione_finito, ls_flag_fine_fase_reparto_riferimento
				
long			ll_i, ll_ret, ll_prog_riga, ll_vuoto[],  ll_prog_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg_sl, ll_num_reg_sl, &
				ll_risposta, ll_y, ll_cont, ll_err, ll_prog_riga_mp, ll_progressivo_mp, ll_prog_orari_mp, ll_anno_reg_movimento, ll_num_reg_movimento

dec{4} 		ld_quan_prodotta_rtc, ld_quan_scarico_mp, ld_quan_scarico_mp_residuo, ld_quan_impegnata, ld_quan_anticipo, ld_quan_cpf

datetime		ldt_data_stock[],ldt_vuoto[], ldt_data_registrazione_commessa, ldt_data_ricerca, ldt_data_mov_magazzino

datastore	lds_avan_prod, lds_stock, lds_giacenze_stock  //, lds_mp
uo_magazzino luo_mag
uo_log luo_log


luo_log = create uo_log

luo_log.open( as_log, false)

// parametro DCC data chiusura commessa
setnull(ldt_data_mov_magazzino)

select data
into	:ldt_data_mov_magazzino
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
cod_parametro = 'DCC';

if sqlca.sqlcode = 0 then
	if isnull(ldt_data_mov_magazzino) then
		ldt_data_mov_magazzino=datetime(today(),time('00:00:00'))
	elseif ldt_data_mov_magazzino <= datetime(date("01/01/1900"),time('00:00:00')) then
		ldt_data_mov_magazzino=datetime(today(),time('00:00:00'))
	end if
else
	ldt_data_mov_magazzino=datetime(today(),time('00:00:00'))
end if


select 
		A.flag_tipo_avanzamento, 
		A.cod_tipo_commessa, 
		T.flag_muovi_sl, 
		T.cod_deposito_sl, 
		T.cod_tipo_mov_prel_sl, 
		T.cod_tipo_mov_ver_sl,
		T.flag_muovi_mp ,
		T.cod_deposito_prelievo cod_deposito_prelievo_mp,
		T.cod_tipo_mov_prel_mat_prime,
		T.cod_deposito_versamento cod_deposito_versamento_pf,
		T.cod_tipo_mov_ver_prod_finiti,
		A.cod_prodotto,
		A.cod_versione,
		A.ubicazione,
		A.lotto,
		A.cod_deposito_versamento,
		A.data_registrazione
into
		:ls_flag_tipo_avanzamento, 
		:ls_cod_tipo_commessa, 
		:ls_flag_muovi_sl, 
		:ls_cod_deposito_sl[1], 
		:ls_cod_tipo_mov_prel_sl, 
		:ls_cod_tipo_mov_ver_sl,
		:ls_flag_muovi_mp ,
		:ls_cod_deposito_prelievo,
		:ls_cod_tipo_mov_prel_mat_prime,
		:ls_cod_deposito_versamento,
		:ls_cod_tipo_mov_ver_prod_finiti,
		:ls_cod_prodotto_finito,
		:ls_cod_versione_finito,
		:ls_cod_ubicazione_pf,
		:ls_cod_lotto_pf,
		:ls_cod_deposito_versamento_pf,
		:ldt_data_registrazione_commessa
from anag_commesse A
join tab_tipi_commessa T on A.cod_azienda=T.cod_azienda and A.cod_tipo_commessa=T.cod_tipo_commessa
where A.anno_commessa = :al_anno_commessa and A.num_commessa = :al_num_commessa;

if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore in esecuzione SLQ: ",sqlca.sqlerrtext)
	return -1
end if	
if sqlca.sqlcode = 100 then
	as_errore = g_str.format("Commessa richiesta $1/$2 non trovata nel database. ",al_anno_commessa, al_num_commessa)
	return -1
end if	

if ls_flag_tipo_avanzamento <> "2" then
	as_errore =  g_str.format("Attenzione la commessa $1/$2 ha uno stato di avanzamento diverso da '2' e quindi non può essere avanzata con questa procedura.",al_anno_commessa, al_num_commessa)
	return -1
end if

if isnull(as_reparto_riferimento) or len(as_reparto_riferimento) = 0 then
	as_errore =  "L'indicazione del reparto di riferimento è obbligatoria"
	return -1
end if

// per prima cosa cerco la quantità prodotta dal reparto  di riferimento che poi mi servirà come base per la quantità prodotta delle fasi da chiudere
select quan_prodotta, 
		flag_fine_fase
into	:ld_quan_prodotta_rtc,
		:ls_flag_fine_fase_reparto_riferimento
from	avan_produzione_com
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :al_anno_commessa and
			num_commessa = :al_num_commessa and 
			cod_reparto = :as_reparto_riferimento ;
//			and 			flag_fine_fase = 'S' ;
if sqlca.sqlcode < 0 then
	as_errore =  g_str.format("Errore SQL in ricerca della fase con reparto RTC nella commessa $1/$2.",al_anno_commessa, al_num_commessa)
	return -1
end if

// reparto riferimento non trovato oppure quantità reparto = 0 perchè la fase non chiusa. verifico se c'è il flag per forzare la chiusura.
if sqlca.sqlcode = 100 OR ld_quan_prodotta_rtc = 0 then
//	if ab_forza_chiusura then
		// in questo caso la quantità diventa quella per cui la commessa era stata creata, cioè (in teoria) la quantità dell'ordine
		select quan_ordine
		into	:ld_quan_prodotta_rtc
		from	anag_commesse
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :al_anno_commessa and
					num_commessa = :al_num_commessa;
		if sqlca.sqlcode <> 0 then
			as_errore =  g_str.format("Errore SQL in ricerca della commessa $1/$2.",al_anno_commessa, al_num_commessa)
			return -1
		end if
//	else
//		as_errore =  g_str.format("Impossibile trovare la quantità prodotta del reparto di riferimento.",al_anno_commessa, al_num_commessa)
//		return -1
//	end if
	ls_sql = g_str.format("select anno_commessa, num_commessa, cod_prodotto, prog_riga, cod_reparto, quan_prodotta, flag_fine_fase, cod_lavorazione from avan_produzione_com where cod_azienda ='$1' and anno_commessa= '$2' and num_commessa = '$3' ",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa)
else					
	ls_sql = g_str.format("select anno_commessa, num_commessa, cod_prodotto, prog_riga, cod_reparto, quan_prodotta, flag_fine_fase, cod_lavorazione from avan_produzione_com where cod_azienda ='$1' and anno_commessa= '$2' and num_commessa = '$3' and cod_reparto <> '$4' ",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa, as_reparto_riferimento)
end if					
					
//ls_sql = g_str.format("select anno_commessa, num_commessa, cod_prodotto, prog_riga, cod_reparto, quan_prodotta, flag_fine_fase, cod_lavorazione from avan_produzione_com where cod_azienda ='$1' and anno_commessa= '$2' and num_commessa = '$3' ",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa)

ll_ret = guo_functions.uof_crea_datastore( lds_avan_prod, ls_sql, ls_errore)
if ll_ret < 0 then
	as_errore = g_str.format("Errore in creazione datastore: ",ls_errore)
	return -1
end if

for ll_i = 1 to ll_ret
	
	// fase ancora aperta?
	if lds_avan_prod.getitemstring(ll_i, 7) = "N" then
		ls_cod_prodotto = lds_avan_prod.getitemstring(ll_i,3)
		ll_prog_riga = lds_avan_prod.getitemnumber(ll_i,4)
		ls_cod_reparto = lds_avan_prod.getitemstring(ll_i,5)
		ls_cod_lavorazione = lds_avan_prod.getitemstring(ll_i,8)
		
		setnull(ll_anno_reg_sl)
		setnull(ll_num_reg_sl)

		if ls_flag_muovi_sl = "S" then
			
			// *******  inizio movimento di carico del SL
	
			setnull(ls_cod_ubicazione[1] )
			setnull(ls_cod_lotto[1])
			setnull(ldt_data_stock[1] )
			setnull(ll_prog_stock[1] )
			setnull(ls_cod_cliente[1] )
			setnull(ls_cod_fornitore[1] )
						
			ll_anno_reg_sl = 0
			ll_num_reg_sl = 0
			
			if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_sl, ls_cod_prodotto, ls_cod_deposito_sl[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
				as_errore="Si è verificato un errore in creazione movimenti ."
		
				return -1
	
			end if
	
			if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_ver_sl, ls_cod_prodotto) = -1 then
			  as_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino ."
			  return -1
			end if
			
			luo_mag = create uo_magazzino
	
			ll_risposta = luo_mag.uof_movimenti_mag( ldt_data_mov_magazzino, &
													 ls_cod_tipo_mov_ver_sl, &
													 "N", &
													 ls_cod_prodotto, &
													 ld_quan_prodotta_rtc, &
													 1, &
													 al_num_commessa, &
													 ldt_data_mov_magazzino, &
													 string(al_anno_commessa), &
													 ll_anno_reg_des_mov, &
													 ll_num_reg_des_mov, &
													 ls_cod_deposito_sl[], &
													 ls_cod_ubicazione[], &
													 ls_cod_lotto[], &
													 ldt_data_stock[], &
													 ll_prog_stock[], &
													 ls_cod_fornitore[], &
													 ls_cod_cliente[], &
													 ll_anno_registrazione[], &
													 ll_num_registrazione[])
													 
			destroy luo_mag
			
			if ll_risposta=-1 then
				as_errore="Errore su movimenti magazzino carico SL."
				return -1
			end if
			
			ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			if ll_risposta = -1 then
				as_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
				return -1
			end if
	
			ll_anno_reg_sl = ll_anno_registrazione[1]
			ll_num_reg_sl = ll_num_registrazione[1]
			// ******* fine movimento di carico SL
			
			// ******* inizio mobimento di scarico SL
			ll_anno_reg_sl = 0
			ll_num_reg_sl = 0
			
			if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_prel_sl, ls_cod_prodotto, ls_cod_deposito_sl[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				as_errore="Si è verificato un errore in creazione movimenti ."
				return -1
			end if
	
			if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_ver_sl, ls_cod_prodotto) = -1 then
			  as_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino ."
			  return -1
			end if
			
			luo_mag = create uo_magazzino
	
			ll_risposta = luo_mag.uof_movimenti_mag(ldt_data_mov_magazzino, &
													 ls_cod_tipo_mov_prel_sl, &
													 "N", &
													 ls_cod_prodotto, &
													 ld_quan_prodotta_rtc, &
													 1, &
													 al_num_commessa, &
													 ldt_data_mov_magazzino, &
													 string(al_anno_commessa), &
													 ll_anno_reg_des_mov, &
													 ll_num_reg_des_mov, &
													 ls_cod_deposito_sl[], &
													 ls_cod_ubicazione[], &
													 ls_cod_lotto[], &
													 ldt_data_stock[], &
													 ll_prog_stock[], &
													 ls_cod_fornitore[], &
													 ls_cod_cliente[], &
													 ll_anno_registrazione[], &
													 ll_num_registrazione[])
													 
			destroy luo_mag
			
			if ll_risposta=-1 then
				as_errore="Errore su movimenti magazzino carico SL."
				return -1
			end if
			
			ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			if ll_risposta = -1 then
				as_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
				return -1
			end if
	
			ll_anno_reg_sl = ll_anno_registrazione[1]
			ll_num_reg_sl = ll_num_registrazione[1]
			
			destroy luo_mag
			// ******* fine movimento di scarico SL			
		end if
		
		// imposta q.ta produzione = q.ta produzione 
		//update con flag_fine_fase=S
		update 	avan_produzione_com
		set 		flag_fine_fase = 'S',
					quan_prodotta = :ld_quan_prodotta_rtc,
					anno_reg_sl = :ll_anno_reg_sl,
					num_reg_sl = :ll_num_reg_sl
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :al_anno_commessa and
					num_commessa = :al_num_commessa and 
					cod_prodotto = :ls_cod_prodotto and
					prog_riga = :ll_prog_riga and
					cod_reparto = :ls_cod_reparto  and
					cod_lavorazione = :ls_cod_lavorazione;
		if sqlca.sqlcode <> 0 then
			as_errore =  g_str.format("Errore SQL in chiusura fase reparto $3 lavorazione $3 della commessa $1/$2.",al_anno_commessa, al_num_commessa, ls_cod_reparto,ls_cod_lavorazione)
			return -1
		end if
	
	end if
		
next
destroy lds_avan_prod

// Controllo se il reparto di riferimento è stato chiuso
if ls_flag_fine_fase_reparto_riferimento = "N" then
	update 	avan_produzione_com
	set 		flag_fine_fase = 'S'
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :al_anno_commessa and
				num_commessa = :al_num_commessa and 
				cod_reparto = :as_reparto_riferimento ;
	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Errore in update per chiusura reparto di riferimento: $1",ls_errore)
		return -1
	end if
end if

// Se sono arrivato qui, vuol dire che la commessa è stata chiusa con relativo movimenti di semilavorati.
// Procedo ora con le materie prime.

string  ls_cod_dep, ls_cod_semilav
ls_cod_dep = "001"
setnull(ls_cod_semilav)
s_trova_mp_varianti lstr_trova_mp_varianti[], lstr_vuoto[]

lstr_trova_mp_varianti[] = lstr_vuoto[]
ll_ret = uof_trova_mat_prime_varianti( ls_cod_prodotto_finito, ls_cod_versione_finito, 0, ls_cod_dep, lstr_trova_mp_varianti[], 1, al_anno_commessa, al_num_commessa, ld_quan_prodotta_rtc, "varianti_commesse", ls_cod_semilav, ls_errore)
if ll_ret < 0 then
	as_errore = g_str.format("Errore in ricerca MP in commessa: $1 ",ls_errore)
	return -1
end if

ll_ret = upperbound(lstr_trova_mp_varianti[])
//uof_trova_mat_prime_varianti( al_anno_commessa, al_num_commessa, ls_cod_semilav, false, ref ls_mp[], ref ls_ver[], ref ld_qta[], ls_cod_dep, ls_errore)

//ls_sql = g_str.format("select cod_prodotto, quan_impegnata_attuale from impegno_mat_prime_commessa where cod_azienda ='$1' and anno_commessa= '$2' and num_commessa = '$3' ",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa)

//ll_ret = guo_functions.uof_crea_datastore(  lds_mp, ls_sql , ls_errore)
//if ll_ret < 0 then
//	as_errore = g_str.format("Errore in creazione datastore: ",ls_errore)
//	return -1
//end if

for ll_i = 1 to ll_ret
//	ls_cod_prodotto = lds_mp.getitemstring(ll_i, 1)
//	ld_quan_impegnata = lds_mp.getitemnumber(ll_i,2)
	
	ls_cod_prodotto = lstr_trova_mp_varianti[ll_i].cod_materia_prima
	ld_quan_impegnata = lstr_trova_mp_varianti[ll_i].quan_utilizzo * ld_quan_prodotta_rtc

	ls_sql = g_str.format("select cod_prodotto, quan_utilizzata, cod_deposito, cod_ubicazione, cod_lotto, data_stock, prog_stock, prog_riga, prog_orari, cod_lavorazione, cod_reparto, progressivo, anno_registrazione, num_registrazione  from mp_com_stock_det_orari where cod_azienda ='$1' and anno_commessa= '$2' and num_commessa = '$3' and cod_prodotto = '$4' ",s_cs_xx.cod_azienda, al_anno_commessa, al_num_commessa,ls_cod_prodotto)
	
	ll_cont = guo_functions.uof_crea_datastore(  lds_stock, ls_sql , ls_errore)
	if ll_cont < 0 then
		as_errore = g_str.format("Errore in creazione datastore: $1",ls_errore)
		return -1
	end if
	
			
	if ll_cont > 0 then
		for ll_y = 1 to ll_cont
			// per prima cosa verifico se c'è già un movimento di magazzino
			ll_anno_reg_movimento = lds_stock.getitemnumber(ll_y,13)
			ll_num_reg_movimento = lds_stock.getitemnumber(ll_y,14)
			
			// se c'è già agganciato un movimento vuol dire che è già stato scaricato
			if not isnull(ll_anno_reg_movimento) or ll_anno_reg_movimento > 0 then continue
			
			
			// eseguo un movimento di magazzino per ogni lotto che era indicato
			ls_cod_deposito_mp[1] = lds_stock.getitemstring(ll_y,3)
			ls_cod_ubicazione[1] = lds_stock.getitemstring(ll_y,4) 
			ls_cod_lotto[1] = lds_stock.getitemstring(ll_y,5) 
			ldt_data_stock[1]  = lds_stock.getitemdatetime(ll_y,6)
			ll_prog_stock[1]  = lds_stock.getitemnumber(ll_y,7)
			setnull(ls_cod_cliente[1] )
			setnull(ls_cod_fornitore[1] )

						
			ll_anno_reg_sl = 0
			ll_num_reg_sl = 0
			
			if f_crea_dest_mov_magazzino( ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto, ls_cod_deposito_mp[], & 
													  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
													  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
													  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
				as_errore="Si è verificato un errore in creazione movimenti ."
				return -1
			end if
	
			if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_ver_sl, ls_cod_prodotto) = -1 then
			  as_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino ."
			  return -1	
			end if
			
			luo_mag = create uo_magazzino
	
			ll_risposta = luo_mag.uof_movimenti_mag( ldt_data_mov_magazzino, &
													 ls_cod_tipo_mov_prel_mat_prime, &
													 "N", &
													 ls_cod_prodotto, &
													 lds_stock.getitemnumber(ll_y,2), &
													 0, &
													 al_num_commessa, &
													 ldt_data_mov_magazzino, &
													 string(al_anno_commessa), &
													 ll_anno_reg_des_mov, &
													 ll_num_reg_des_mov, &
													 ls_cod_deposito_sl[], &
													 ls_cod_ubicazione[], &
													 ls_cod_lotto[], &
													 ldt_data_stock[], &
													 ll_prog_stock[], &
													 ls_cod_fornitore[], &
													 ls_cod_cliente[], &
													 ll_anno_registrazione[], &
													 ll_num_registrazione[])
													 
			destroy luo_mag
			
			if ll_risposta=-1 then
				as_errore="Errore su movimenti magazzino carico SL."
				return -1
			end if
			
			ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			if ll_risposta = -1 then
				as_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
				return -1
			end if
	
			ll_anno_reg_sl = ll_anno_registrazione[1]
			ll_num_reg_sl = ll_num_registrazione[1]
			// ******* fine movimento di carico SL
			
			destroy luo_mag
			// registro il movimento nella mp_com_stock_det_orari
			
			ll_prog_riga_mp = lds_stock.getitemnumber(ll_y,8)
			ll_prog_orari_mp = lds_stock.getitemnumber(ll_y,9)
			ls_cod_lavorazione_mp = lds_stock.getitemstring(ll_y,10)
			ls_cod_reparto_mp = lds_stock.getitemstring(ll_y,11)
			ll_progressivo_mp =  lds_stock.getitemnumber(ll_y,12)
			
			update 	mp_com_stock_det_orari
			set 		anno_registrazione = :ll_anno_reg_sl,
						num_registrazione = :ll_num_reg_sl
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_commessa = :al_anno_commessa and
						num_commessa = :al_num_commessa and
						cod_prodotto = :ls_cod_prodotto and
						prog_riga = :ll_prog_riga_mp and
						prog_orari = :ll_prog_orari_mp and
						cod_deposito = :ls_cod_deposito_mp[1] and
						cod_ubicazione = :ls_cod_ubicazione[1] and
						cod_lotto = :ls_cod_lotto[1] and
						data_stock = :ldt_data_stock[1] and
						prog_stock = :ll_prog_stock[1] and
						cod_lavorazione = :ls_cod_lavorazione_mp and
						cod_reparto = :ls_cod_reparto_mp and
						progressivo = :ll_progressivo_mp;
				if sqlca.sqlcode < 0 then
					as_errore = g_str.format("Errore in UPDATE tabella mp_com_stock_det_orari. $1", sqlca.sqlerrtext)
					return -1
				end if
			
		next
	else
		// eseguo un movimento di magazzino sulla base della quantità impegnata andando a scaricare dai lotti più vecchi
		
//		ld_quan_scarico_mp_residuo = lds_mp.getitemnumber(ll_i, 2)
		ld_quan_scarico_mp_residuo = lstr_trova_mp_varianti[ll_i].quan_utilizzo * ld_quan_prodotta_rtc
		
		// ricerca i lotti andando indietro di 1 anno rispetto alla data di registrazione della commessa
		luo_log.log( g_str.format("$1 - Inizio Inventario Stock prodotto $2",string(now()), ls_cod_prodotto))
		ldt_data_ricerca = datetime( relativedate(date(ldt_data_registrazione_commessa), -365) ,00:00:00 )
		ll_cont = uof_giacenze_stock_prodotto_deposito( ls_cod_prodotto, ls_cod_deposito_prelievo, ldt_data_ricerca, ref lds_giacenze_stock)
//		ll_cont = uof_giacenze_stock_prodotto_deposito( ls_cod_prodotto, ls_cod_deposito_prelievo, ldt_data_registrazione_commessa, ref lds_giacenze_stock)
		luo_log.log( g_str.format("$1 - Fine Inventario Stock prodotto $2",string(now()), ls_cod_prodotto))
		
		if lds_giacenze_stock.rowcount() = 0 then
			luo_log.log( g_str.format("Per il prodotto $1 non ci sono giacenze di lotti nel magazzino $2 a far data dal $3 ", ls_cod_prodotto, ls_cod_deposito_prelievo, string(ldt_data_ricerca,"dd-mm-yyyy")))
		end if
		// mi passo tutti gli stock
		for ll_y = 1 to lds_giacenze_stock.rowcount()
			
			if ld_quan_scarico_mp_residuo = 0 then exit
			
			if lds_giacenze_stock.getitemnumber(ll_y,7) <= 0 then 
				if ll_y = lds_giacenze_stock.rowcount() then luo_log.log( g_str.format("$1 - Per il prodotto $2 non ci sono residui stock sufficienti",string(now()), ls_cod_prodotto))	
				continue
			end if
			
			if ld_quan_scarico_mp_residuo > lds_giacenze_stock.getitemnumber(ll_y,7) then
				ld_quan_scarico_mp =  lds_giacenze_stock.getitemnumber(ll_y,7)
				ld_quan_scarico_mp_residuo = ld_quan_scarico_mp_residuo - ld_quan_scarico_mp
			else
				ld_quan_scarico_mp = ld_quan_scarico_mp_residuo
				ld_quan_scarico_mp_residuo = 0
			end if
	
			ls_cod_deposito_mp[1] = lds_giacenze_stock.getitemstring(ll_y,2)
			ls_cod_ubicazione[1] = lds_giacenze_stock.getitemstring(ll_y,3)
			ls_cod_lotto[1] =lds_giacenze_stock.getitemstring(ll_y,4)
			ldt_data_stock[1]  = lds_giacenze_stock.getitemdatetime(ll_y,5)
			ll_prog_stock[1]  = lds_giacenze_stock.getitemnumber(ll_y,6)
			setnull(ls_cod_cliente[1] )
			setnull(ls_cod_fornitore[1] )
						
			ll_anno_reg_sl = 0
			ll_num_reg_sl = 0
			
			if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_prel_mat_prime, ls_cod_prodotto, ls_cod_deposito_mp[], & 
										  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
										  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
										  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
				as_errore="Si è verificato un errore in creazione movimenti ."
				return -1
			end if
	
			if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, & 
												ls_cod_tipo_mov_ver_sl, ls_cod_prodotto) = -1 then
			  as_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino ."
			  return -1	
			end if
			
			luo_mag = create uo_magazzino
	
			ll_risposta = luo_mag.uof_movimenti_mag( ldt_data_mov_magazzino, &
													 ls_cod_tipo_mov_prel_mat_prime, &
													 "N", &
													 ls_cod_prodotto, &
													 ld_quan_scarico_mp, &
													 0, &
													 al_num_commessa, &
													 ldt_data_mov_magazzino, &
													 string(al_anno_commessa), &
													 ll_anno_reg_des_mov, &
													 ll_num_reg_des_mov, &
													 ls_cod_deposito_sl[], &
													 ls_cod_ubicazione[], &
													 ls_cod_lotto[], &
													 ldt_data_stock[], &
													 ll_prog_stock[], &
													 ls_cod_fornitore[], &
													 ls_cod_cliente[], &
													 ll_anno_registrazione[], &
													 ll_num_registrazione[])
													 
			destroy luo_mag
			
			if ll_risposta=-1 then
				as_errore="Errore su movimenti magazzino carico SL."
				return -1
			end if
			
			ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
			
			if ll_risposta = -1 then
				as_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
				return -1
			end if
	
			ll_anno_reg_sl = ll_anno_registrazione[1]
			ll_num_reg_sl = ll_num_registrazione[1]
			// ******* fine movimento di carico SL
			
			destroy luo_mag
	
		next		
	end if

	// disimpegno la quantità da anag_prodotti
	update 	anag_prodotti
	set 		quan_impegnata = quan_impegnata - :ld_quan_impegnata
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Errore in UPDATE anag_prodotti durante aggiornamento impegnato prodotto $1 . ",ls_cod_prodotto)
		return -1
	end if	
	
	update 	impegno_mat_prime_commessa
	set 		quan_impegnata_attuale = 0
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_commessa = :al_anno_commessa and
				num_commessa = :al_num_commessa and
				cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Errore in UPDATE impegnato commessa durante aggiornamento impegnato prodotto $3 commessa $1/$2 . ",al_anno_commessa, al_num_commessa, ls_cod_prodotto)
		return -1
	end if	
	
next

// Verifico se ci sono stati dei movimenti di anticipo del prodotto finito
select sum(quan_anticipo)
into	:ld_quan_anticipo
from det_anag_comm_anticipi
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :al_anno_commessa and
		num_commessa = :al_num_commessa;
if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore SQL in sommatoria quantità anticipata commessa $1/$2 . ",al_anno_commessa, al_num_commessa)
	return -1
end if	

if isnull(ld_quan_anticipo) then ld_quan_anticipo = 0

ld_quan_cpf = ld_quan_prodotta_rtc - ld_quan_anticipo

if ld_quan_cpf < 0 then ld_quan_cpf = 0
// eseguo il movimento di carico del prodotto finito solo se non è stata tutta anticipata
	
if ld_quan_cpf > 0 then
	ls_cod_deposito_sl[1] = ls_cod_deposito_versamento_pf
	if isnull(ls_cod_ubicazione_pf) or len(ls_cod_ubicazione_pf) < 1 then
		setnull(ls_cod_ubicazione[1])
	else
		ls_cod_ubicazione[1] = ls_cod_ubicazione_pf
	end if
	if isnull(ls_cod_lotto_pf) or len(ls_cod_lotto_pf) < 1 then
		ls_cod_lotto[1] = g_str.format("$1$2",al_anno_commessa, al_num_commessa)
	else
		ls_cod_lotto[1] = ls_cod_lotto_pf
	end if
	setnull(ldt_data_stock[1] )
	setnull(ll_prog_stock[1] )
	setnull(ls_cod_cliente[1] )
	setnull(ls_cod_fornitore[1] )
	
			
	ll_anno_reg_sl = 0
	ll_num_reg_sl = 0
	
	if f_crea_dest_mov_magazzino(ls_cod_tipo_mov_ver_prod_finiti  , ls_cod_prodotto_finito, ls_cod_deposito_sl[], & 
								  ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], & 
								  ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], &
								  ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
	
		as_errore="Si è verificato un errore in creazione movimenti ."
	
		return -1
	
	end if
	
	if f_verifica_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_mov_ver_prod_finiti, ls_cod_prodotto_finito) = -1 then
	  as_errore="Si è verificato un errore in fase di verifica destinazioni movimenti magazzino ."
	  return -1
	end if
	
	luo_mag = create uo_magazzino
	
	ll_risposta = luo_mag.uof_movimenti_mag( ldt_data_mov_magazzino, &
											 ls_cod_tipo_mov_ver_prod_finiti, &
											 "N", &
											 ls_cod_prodotto_finito, &
											 ld_quan_cpf, &
											 1, &
											 al_num_commessa, &
											 ldt_data_mov_magazzino, &
											 string(al_anno_commessa), &
											 ll_anno_reg_des_mov, &
											 ll_num_reg_des_mov, &
											 ls_cod_deposito_sl[], &
											 ls_cod_ubicazione[], &
											 ls_cod_lotto[], &
											 ldt_data_stock[], &
											 ll_prog_stock[], &
											 ls_cod_fornitore[], &
											 ls_cod_cliente[], &
											 ll_anno_registrazione[], &
											 ll_num_registrazione[])
											 
	destroy luo_mag
	
	if ll_risposta=-1 then
		as_errore="Errore su movimenti magazzino carico SL."
		return -1
	end if
	
	ll_risposta = f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov)
	
	if ll_risposta = -1 then
		as_errore="Si è verificato un errore in fase di cancellazione destinazioni movimenti."
		return -1
	end if
	
	ll_anno_reg_sl = ll_anno_registrazione[1]
	ll_num_reg_sl = ll_num_registrazione[1]
end if
// ******* fine movimento di carico PF

// come ultima cosa eseguo update della commessa effettuandone la chiusura e sistemando le quantità.
update anag_commesse
set quan_prodotta = :ld_quan_prodotta_rtc,
	quan_in_produzione = 0,
	flag_tipo_avanzamento='8'
where cod_azienda = :s_cs_xx.cod_azienda and
anno_commessa = :al_anno_commessa and
num_commessa = :al_num_commessa;
if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore SQL in fase di update anag_commesse con la quantità prodotta.~r~n$1",sqlca.sqlerrtext)
	return -1
end if


return 0
end function

on uo_funzioni_1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_funzioni_1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;luo_magazzino = create uo_magazzino
ib_flag_non_collegati = false
setnull(id_quan_ordine)

setnull(idt_data_creazione_mov)

end event

event destructor;
destroy luo_magazzino

end event

