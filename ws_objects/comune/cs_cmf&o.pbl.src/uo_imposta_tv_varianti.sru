﻿$PBExportHeader$uo_imposta_tv_varianti.sru
forward
global type uo_imposta_tv_varianti from nonvisualobject
end type
end forward

global type uo_imposta_tv_varianti from nonvisualobject
end type
global uo_imposta_tv_varianti uo_imposta_tv_varianti

type variables
boolean			ib_verifica_fasi_avanzamento=false, ib_salta_materie_prime=false, ib_salta_rami_descrittivi=false
string				is_tipo_gestione, is_cod_versione
long				il_anno_registrazione, il_num_registrazione, il_prog_riga_registrazione

//Donato 14/03/2012 Controllo sul livello MAX di espansione del report distinta varianti
//se è impostato a ZERO ignora il controllo livelli (come prima del 14/03/2012)
long il_livello_max = 0
//------------------------------------------------------------------------------------------------

boolean ib_trasf_componenti = false
long il_barcode = 0, il_anno_commessa=0, il_num_commessa=0
decimal id_qta_ordine = 1

string		is_simbolo_scarico_parziale = "(elaborato) "
end variables

forward prototypes
public function integer uof_imposta_report (ref datawindow fdw_report, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, string fs_errore)
public function boolean uof_variante_vincolante (string fs_cod_prodotto_padre, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_padre, string fs_cod_versione_figlio)
public function integer uof_imposta_tv (treeview ftv_db, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, long fl_handle, string fs_ordinamento, ref string fs_errore)
public function long uof_prepara_datastore (ref datastore fds_righe_distinta, string fs_cod_prodotto, string fs_cod_versione, string fs_ordinamento, ref string fs_errore)
public function integer uof_imposta_report_old (ref datawindow fdw_report, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, string fs_errore)
public function long uof_get_data_varianti_gestione (string fs_cod_prodotto_padre, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_padre, string fs_cod_versione_figlio, ref string fs_cod_prodotto_variante, ref string fs_cod_versione_variante, ref decimal fdd_quan_utilizzo_variante, ref string fs_flag_materia_prima_variante, ref string fs_flag_scarico_parziale, ref string fs_errore)
end prototypes

public function integer uof_imposta_report (ref datawindow fdw_report, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, string fs_errore);//	Funzione che imposta l'outliner
//
// nome: uof_imposta_tv
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari
//    Modifica Michela 19/09/2006: aggiunta la gestione della versione del prodotto figlio

string					ls_cod_prodotto, ls_errore, ls_des_prodotto, ls_cod_versione, ls_cod_versione_variante, &
						ls_cod_prodotto_variante, ls_des_prodotto_variante, ls_flag_materia_prima, ls_flag_materia_prima_variante, &
						ls_stato_fase, ls_cod_lavorazione, ls_flag_fase_conclusa, ls_flag_fase_esterna, ls_cod_reparto, ls_flag_scarico_parziale, &
						ls_cod_prodotto_figlio_report, ls_cod_versione_figlio_report, ls_des_prodotto_report, ls_flag_materia_prima_report, &
						ls_cod_misura_mag_report, ls_quantita,ls_cod_misura_mag_variante,ls_cod_misura_mag, ls_valore, ls_valore_2
						
long					ll_num_figli, ll_num_righe, ll_cont_figlio, ll_cont_figlio_comm, ll_cont, ll_j, ll_ret

integer				li_num_priorita, li_risposta

decimal				ldd_quan_utilizzo_variante,ldd_quan_utilizzo_report

boolean				lb_flag_variante

s_chiave_distinta	l_chiave_distinta

datastore			lds_righe_distinta

lb_flag_variante = false
ll_num_figli = 1

ll_num_righe = uof_prepara_datastore(ref lds_righe_distinta, fs_cod_prodotto, fs_cod_versione, "S", ref fs_errore)
if ll_num_righe<0 then
	//c'è stato un errore (msg in fs_errore)
	return -1
end if



for ll_num_figli = 1 to ll_num_righe
	
	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.cod_versione_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_versione_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber( ll_num_figli,"quan_utilizzo")
	
	//##############################################################
	l_chiave_distinta.quan_utilizzo = id_qta_ordine * l_chiave_distinta.quan_utilizzo
	//##############################################################
	
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_padre")
	l_chiave_distinta.cod_versione_padre = lds_righe_distinta.getitemstring( ll_num_figli, "cod_versione")
	l_chiave_distinta.flag_tipo_record = lds_righe_distinta.getitemstring( ll_num_figli,"flag_tipo_record")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring( ll_num_figli,"flag_materia_prima")
	l_chiave_distinta.flag_ramo_descrittivo = lds_righe_distinta.getitemstring( ll_num_figli,"flag_ramo_descrittivo")
	choose case l_chiave_distinta.flag_tipo_record
		case 'D'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
		case 'I'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"progressivo")
	end choose
	
	
	//Donato 01/12/2011
	//nuova funzione che sostituisce il choose case
	ll_ret = uof_get_data_varianti_gestione(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
														l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
														l_chiave_distinta.cod_versione_figlio, &
														ls_cod_prodotto_variante, ls_cod_versione_variante, ldd_quan_utilizzo_variante, &
														ls_flag_materia_prima_variante, ls_flag_scarico_parziale, fs_errore)
	if ll_ret<0 then
		//c'è stato un errore (msg in fs_errore)
		return -1
	end if
	
	setnull(l_chiave_distinta.cod_prodotto_variante)
	setnull(l_chiave_distinta.cod_versione_variante)
	setnull(l_chiave_distinta.quan_utilizzo_variante)
	lb_flag_variante=false

	if ll_ret = 0 then		// trovato variante
	
		l_chiave_distinta.cod_prodotto_variante = ls_cod_prodotto_variante
		l_chiave_distinta.cod_versione_variante = ls_cod_versione_variante
		l_chiave_distinta.quan_utilizzo_variante = ldd_quan_utilizzo_variante
		
		//##############################################################
		l_chiave_distinta.quan_utilizzo_variante = id_qta_ordine * l_chiave_distinta.quan_utilizzo_variante
		//##############################################################
		
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		l_chiave_distinta.flag_tipo_record = "V"

	   select des_prodotto, cod_misura_mag
		into   :ls_des_prodotto_variante, :ls_cod_misura_mag_variante
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :l_chiave_distinta.cod_prodotto_variante;

		ls_des_prodotto = trim(ls_des_prodotto_variante)

	   lb_flag_variante = true
	end if

   select des_prodotto, cod_misura_mag
	into   :ls_des_prodotto, :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;

	ls_des_prodotto = trim(ls_des_prodotto)
	
	
	if lb_flag_variante then
		ls_cod_prodotto_figlio_report = l_chiave_distinta.cod_prodotto_variante
		ls_cod_versione_figlio_report = ls_cod_versione_variante
		ldd_quan_utilizzo_report      = l_chiave_distinta.quan_utilizzo_variante
		ls_des_prodotto_report        = ls_des_prodotto_variante
		ls_flag_materia_prima_report  = ls_flag_materia_prima
		ls_cod_misura_mag_report      = ls_cod_misura_mag_variante
		
		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
				 cod_versione = :l_chiave_distinta.cod_versione_variante;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
		
		case else
			
			fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose
				 
		ll_cont_figlio += ll_cont_figlio_comm
		
	else  // no caso variante.
		
		ls_cod_prodotto_figlio_report = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_figlio_report = l_chiave_distinta.cod_versione_figlio
		ldd_quan_utilizzo_report      = l_chiave_distinta.quan_utilizzo
		ls_des_prodotto_report        = ls_des_prodotto
		ls_cod_misura_mag_report      = ls_cod_misura_mag
		
		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
				 cod_versione = :l_chiave_distinta.cod_versione_figlio;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
						 
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
		case else
			fs_errore = "Il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose

		ll_cont_figlio += ll_cont_figlio_comm
		
	end if
	
	
	if ( isnull(ll_cont_figlio) or ll_cont_figlio < 1 ) OR ( ls_flag_materia_prima = "S" ) then
		// sotto questo ramo non ci sono figli; quindi è una materia prima
		
		// non visualizzo le materie prime
		if ib_salta_materie_prime then continue
		
		ll_j = fdw_report.insertrow(0)
		
		fdw_report.setitem( ll_j, "tipo_riga", 3)
		fdw_report.setitem( ll_j, "num_livello", fi_num_livello_cor)
		
		if int(ldd_quan_utilizzo_report) <> ldd_quan_utilizzo_report then
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0.0###")
		else
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0")
		end if
		
		if ib_trasf_componenti then
			//cod prodotto e quantità separati in due campi diversi, inoltre no versione
			ls_valore = ls_cod_prodotto_figlio_report
			//ls_valore_2 = ls_quantita + " " + ls_cod_misura_mag_report
		else
			//come prima
			ls_valore = ls_cod_prodotto_figlio_report + "  Vers:" + ls_cod_versione_figlio_report + "   " + ls_quantita + " " + ls_cod_misura_mag_report
			ls_valore_2 = ""
		end if
		
		if fi_num_livello_cor>0 then
			fdw_report.setitem(ll_j,"cod_l_"+string(fi_num_livello_cor), ls_valore)
			fdw_report.setitem(ll_j,"des_l_"+string(fi_num_livello_cor), ls_des_prodotto_report)
			
			if ib_trasf_componenti then
				fdw_report.setitem(ll_j,"max_livello", il_livello_max)
				//fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ls_valore_2)
				
				fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ldd_quan_utilizzo_report)
				fdw_report.setitem(ll_j,"um_l_"+string(fi_num_livello_cor), ls_cod_misura_mag_report)
				
			end if
			
		end if
			
		fdw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
		if l_chiave_distinta.flag_tipo_record <> "D" then
			fdw_report.setitem( ll_j, "flag_variante", l_chiave_distinta.flag_tipo_record)
		end if

		continue
		
	else
		// ci sono dei figli, si tratta di un semilavorato
		
		ll_j = fdw_report.insertrow(0)
		
		fdw_report.setitem( ll_j, "tipo_riga", 3)		
		
		fdw_report.setitem( ll_j, "num_livello", fi_num_livello_cor)
		
		if int(ldd_quan_utilizzo_report) <> ldd_quan_utilizzo_report then
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0.0###")
		else
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0")
		end if
		
		if ib_trasf_componenti then
			//cod prodotto e quantità separati in due campi diversi, inoltre no versione
			ls_valore = ls_cod_prodotto_figlio_report
			//ls_valore_2 = ls_quantita + " " + ls_cod_misura_mag_report
		else
			//come prima
			ls_valore = ls_cod_prodotto_figlio_report + "  Vers:" + ls_cod_versione_figlio_report + "   " + ls_quantita + " " + ls_cod_misura_mag_report
			ls_valore_2 = ""
		end if
		
		if fi_num_livello_cor>0 then
			fdw_report.setitem(ll_j,"cod_l_"+string(fi_num_livello_cor), ls_valore)
			fdw_report.setitem(ll_j,"des_l_"+string(fi_num_livello_cor), ls_des_prodotto_report)
			
			if ib_trasf_componenti then
				fdw_report.setitem(ll_j,"max_livello", il_livello_max)
				//fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ls_valore_2)
				
				fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ldd_quan_utilizzo_report)
				fdw_report.setitem(ll_j,"um_l_"+string(fi_num_livello_cor), ls_cod_misura_mag_report)
			end if
			
		end if
			
		fdw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
		if l_chiave_distinta.flag_tipo_record <> "D" then
			fdw_report.setitem( ll_j, "flag_variante", l_chiave_distinta.flag_tipo_record)
		end if

		ll_cont_figlio = 0
	
		if lb_flag_variante = true then
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fi_num_livello_cor + 1) > il_livello_max and il_livello_max > 0 then
				lb_flag_variante = false
				continue
			end if
			//-------------------------------------------------------------------------

			li_risposta=uof_imposta_report(ref fdw_report, l_chiave_distinta.cod_prodotto_variante, l_chiave_distinta.cod_versione_variante, fi_num_livello_cor + 1, ref ls_errore)
			lb_flag_variante = false
		else
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fi_num_livello_cor + 1) > il_livello_max and il_livello_max > 0 then continue
			//-------------------------------------------------------------------------

			li_risposta=uof_imposta_report(ref fdw_report, l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_figlio, fi_num_livello_cor + 1,ref ls_errore)				
		end if
	
	end if
next

destroy(lds_righe_distinta)

return 0
end function

public function boolean uof_variante_vincolante (string fs_cod_prodotto_padre, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_padre, string fs_cod_versione_figlio);/*
cerca prima se il ramo distinta ha il flag variante vincolante a SI
poi prova in gruppi varianti, cioè vede se il ramo ha qualcosa in distinta_gruppi_varianti
	e quindi vede in corrispondenza in gruppi_varianti

valore ritorno
	TRUE 		se siamo in presenza di variante vincolante
	FALSE	altrimenti
*/

long ll_count

//cerca prima in ramo distinta
select count(*)
into :ll_count
from distinta
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:fs_cod_prodotto_padre and
			num_sequenza=:fl_num_sequenza and
			cod_prodotto_figlio=:fs_cod_prodotto_figlio and
			cod_versione=:fs_cod_versione_padre and
			cod_versione_figlio=:fs_cod_versione_figlio and
			flag_variante_vincol='S';

if ll_count>0 then
	//variante vincolante
	return true
end if

//altrimenti prova in corrispondenza di gruppi_varianti (ammesso che ce ne siano)
select count(*)
into :ll_count
from distinta_gruppi_varianti
join gruppi_varianti on 	gruppi_varianti.cod_azienda=distinta_gruppi_varianti.cod_azienda and
								gruppi_varianti.cod_gruppo_variante=distinta_gruppi_varianti.cod_gruppo_variante
where 	distinta_gruppi_varianti.cod_azienda=:s_cs_xx.cod_azienda and
			distinta_gruppi_varianti.cod_prodotto_padre=:fs_cod_prodotto_padre and
			distinta_gruppi_varianti.num_sequenza=:fl_num_sequenza and
			distinta_gruppi_varianti.cod_prodotto_figlio=:fs_cod_prodotto_figlio and
			distinta_gruppi_varianti.cod_versione=:fs_cod_versione_padre and
			distinta_gruppi_varianti.cod_versione_figlio=:fs_cod_versione_figlio and
			gruppi_varianti.flag_variante_vincol='S';

if ll_count>0 then
	//variante vincolante
	return true
end if

return false
















end function

public function integer uof_imposta_tv (treeview ftv_db, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, long fl_handle, string fs_ordinamento, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: uof_imposta_tv
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari
//    Modifica Michela 19/09/2006: aggiunta la gestione della versione del prodotto figlio

//Donato 01/12/2011
//modifica per visualizzare una icona diversa se si tratta di variante VINCOLANTE (pictureindex=11 al posto di pictureindex=5)


string					ls_cod_prodotto, ls_errore, ls_des_prodotto, ls_cod_versione, ls_cod_versione_variante, &
						ls_cod_prodotto_variante, ls_des_prodotto_variante, ls_flag_materia_prima, ls_flag_materia_prima_variante, &
						ls_stato_fase, ls_cod_lavorazione, ls_flag_fase_conclusa, ls_flag_fase_esterna, ls_cod_reparto, ls_flag_scarico_parziale
				
long   				ll_num_figli, ll_handle, ll_num_righe, ll_cont_figlio, ll_cont_figlio_comm, ll_pictureindex, ll_cont, ll_ret

integer 				li_num_priorita, li_risposta
decimal  				ldd_quan_utilizzo_variante

boolean				lb_flag_variante

s_chiave_distinta 	l_chiave_distinta
treeviewitem      	tvi_campo
datastore         	lds_righe_distinta

lb_flag_variante = false
ll_num_figli = 1

ll_num_righe = uof_prepara_datastore(ref lds_righe_distinta, fs_cod_prodotto, fs_cod_versione, fs_ordinamento, ref fs_errore)
if ll_num_righe<0 then
	//c'è stato un errore (msg in fs_errore)
	return -1
end if


for ll_num_figli = 1 to ll_num_righe
	
	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.cod_versione_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_versione_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber( ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_padre")
	l_chiave_distinta.cod_versione_padre = lds_righe_distinta.getitemstring( ll_num_figli, "cod_versione")
	l_chiave_distinta.flag_tipo_record = lds_righe_distinta.getitemstring( ll_num_figli,"flag_tipo_record")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring( ll_num_figli,"flag_materia_prima")
	l_chiave_distinta.flag_ramo_descrittivo = lds_righe_distinta.getitemstring( ll_num_figli,"flag_ramo_descrittivo")
	
	// ***	Michela 05/06/2007: imposto di default a No il fatto che sia una fase esterna e aperta
	
	l_chiave_distinta.flag_fase_esterna = "N"
	l_chiave_distinta.flag_fase_aperta = "N"
	
	if l_chiave_distinta.flag_ramo_descrittivo = "S" and ib_salta_rami_descrittivi then continue
	
	choose case l_chiave_distinta.flag_tipo_record
		case 'D'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
		case 'I'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"progressivo")
	end choose
	
	
	//Donato 01/12/2011
	//nuova funzione che sostituisce il choose case
	ll_ret = uof_get_data_varianti_gestione(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
														l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
														l_chiave_distinta.cod_versione_figlio, &
														ls_cod_prodotto_variante, ls_cod_versione_variante, ldd_quan_utilizzo_variante, &
														ls_flag_materia_prima_variante, ls_flag_scarico_parziale, fs_errore)
	if ll_ret<0 then
		//c'è stato un errore (msg in fs_errore)
		return -1
	end if

	
	if ll_ret = 0 then
		
		l_chiave_distinta.cod_prodotto_variante = ls_cod_prodotto_variante
		l_chiave_distinta.cod_versione_variante = ls_cod_versione_variante
		l_chiave_distinta.quan_utilizzo_variante = ldd_quan_utilizzo_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante

		select des_prodotto
		into   :ls_des_prodotto_variante
		from   anag_prodotti
		where  	cod_azienda = :s_cs_xx.cod_azienda and    
			 		cod_prodotto = :l_chiave_distinta.cod_prodotto_variante;

		ls_des_prodotto = trim(ls_des_prodotto_variante)

		lb_flag_variante = true
		
		// aggiunto da enrico per nuove icone su fasi di lavorazione.
		if ib_verifica_fasi_avanzamento and ls_flag_materia_prima_variante = "N" then
			
			setnull(ls_stato_fase)
			
			select cod_reparto,   
					 cod_lavorazione  
			into  :ls_cod_reparto,   
					:ls_cod_lavorazione  
			from  tes_fasi_lavorazione  
			where cod_azienda = :s_cs_xx.cod_azienda AND  
					cod_prodotto = :ls_cod_prodotto_variante AND  
					cod_versione = :ls_cod_versione_variante  ;
					
			if sqlca.sqlcode = 0 then		// esiste una fase
			
				l_chiave_distinta.cod_lavorazione = ls_cod_lavorazione
				l_chiave_distinta.cod_reparto     = ls_cod_reparto
				l_chiave_distinta.flag_fase_aperta = "A"
				
				ls_stato_fase = "A"		// fase aperta (non Iniziata e non Finita)
				
				// controllo se terminata
				select		flag_fine_fase,   
							flag_esterna  
				into		:ls_flag_fase_conclusa,   
							:ls_flag_fase_esterna  
				from  avan_produzione_com  
				where	cod_azienda = :s_cs_xx.cod_azienda and
							anno_commessa = :il_anno_registrazione and
							num_commessa = : il_num_registrazione and
							cod_prodotto = :ls_cod_prodotto_variante and
							cod_versione = :ls_cod_versione_variante and
							prog_riga = :il_prog_riga_registrazione  and
							cod_reparto = :ls_cod_reparto  and
							cod_lavorazione = :ls_cod_lavorazione;
						 
				if sqlca.sqlcode = 0 then
					
				// se non terminata controllo se iniziata			
				
					if ls_flag_fase_conclusa = "N"  then
						select count(*)
						into   :ll_cont
						from   det_orari_produzione
						where	cod_azienda = :s_cs_xx.cod_azienda and
									anno_commessa = :il_anno_registrazione and 
									num_commessa = : il_num_registrazione and
									cod_prodotto = :ls_cod_prodotto_variante and
									cod_versione = :ls_cod_versione_variante and
									prog_riga = :il_prog_riga_registrazione and
									cod_reparto = :ls_cod_reparto  and
									cod_lavorazione = :ls_cod_lavorazione and
									flag_inizio = 'S' ;
						
						if ll_cont > 0 then
							ls_stato_fase = "I"		// Fase Iniziata
							l_chiave_distinta.flag_fase_aperta = "I"
						end if 
					else
						ls_stato_fase = "F"  // fase conclusa
						l_chiave_distinta.flag_fase_aperta = "F"
					end if
					
				// non esiste la fase di avanzamento nell'avanzamento di produzione
				else
					setnull(ls_stato_fase)
					l_chiave_distinta.flag_fase_aperta = "N"
				end if			
			end if			
		
		end if
		
	else		// non è variante
		
		lb_flag_variante = false
		
		// aggiunto da enrico per nuove icone su fasi di lavorazione.
		if ib_verifica_fasi_avanzamento and ls_flag_materia_prima = "N" then
			
			setnull(ls_stato_fase)
			
			select cod_reparto,   
					 cod_lavorazione  
			into  :ls_cod_reparto,   
					:ls_cod_lavorazione  
			from  tes_fasi_lavorazione  
			where cod_azienda = :s_cs_xx.cod_azienda AND  
					cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio AND  
					cod_versione = :l_chiave_distinta.cod_versione_figlio  ;
			if sqlca.sqlcode = 0 then		// esiste una fase
			
				l_chiave_distinta.cod_lavorazione = ls_cod_lavorazione
				l_chiave_distinta.cod_reparto     = ls_cod_reparto
				l_chiave_distinta.flag_fase_aperta = "A"
				ls_stato_fase = "A"		// fase aperta (non Iniziata e non Finita)
				
				// controllo se terminata
				select		flag_fine_fase,   
							flag_esterna  
				into  :ls_flag_fase_conclusa,   
						 :ls_flag_fase_esterna  
				from  avan_produzione_com  
				where	cod_azienda = :s_cs_xx.cod_azienda and
							 anno_commessa = :il_anno_registrazione and
							 num_commessa = : il_num_registrazione and
							 cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
							 cod_versione = :l_chiave_distinta.cod_versione_figlio and
							 prog_riga = :il_prog_riga_registrazione  and
							 cod_reparto = :ls_cod_reparto  and
							 cod_lavorazione = :ls_cod_lavorazione   ;


				if sqlca.sqlcode = 0 then
					
				// se non terminata controllo se iniziata		
				if ls_flag_fase_esterna = "S" then	
					ls_stato_fase = "E"		// fase aperta ESTERNA (non Iniziata e non Finita)
					l_chiave_distinta.flag_fase_aperta = "A"
					l_chiave_distinta.flag_fase_esterna = "S"
				end if
			
					if ls_flag_fase_conclusa = "N"  then
						
						select count(*)
						into   :ll_cont
						from   det_orari_produzione
						where	cod_azienda = :s_cs_xx.cod_azienda and
									anno_commessa = :il_anno_registrazione and 
									num_commessa = : il_num_registrazione and
									cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio and
									cod_versione = :l_chiave_distinta.cod_versione_figlio and
									prog_riga = :il_prog_riga_registrazione and
									cod_reparto = :ls_cod_reparto  and
									cod_lavorazione = :ls_cod_lavorazione and
									flag_inizio = 'S' ;
						
						if ll_cont > 0 then
							ls_stato_fase = "I"		// Fase Iniziata
							l_chiave_distinta.flag_fase_aperta = "I"
						end if 
					else
						ls_stato_fase = "F"  // fase conclusa
						l_chiave_distinta.flag_fase_aperta = "F"
					end if
					
				// non esiste la fase di avanzamento nell'avanzamento di produzione
				else
					setnull(ls_stato_fase)
					l_chiave_distinta.flag_fase_aperta = "N"
				end if			
			
			end if			
		
		end if
		
	end if

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and    
				cod_prodotto =:l_chiave_distinta.cod_prodotto_figlio;

	ls_des_prodotto = trim(ls_des_prodotto)

	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_distinta
	
	if lb_flag_variante then
		
		if ib_verifica_fasi_avanzamento and not isnull(l_chiave_distinta.cod_lavorazione) and len(l_chiave_distinta.cod_lavorazione) > 0 then
			tvi_campo.label = l_chiave_distinta.cod_prodotto_variante + " / " + l_chiave_distinta.cod_lavorazione + " / " + l_chiave_distinta.cod_reparto + " - " +  ls_des_prodotto_variante + " - Ver " + ls_cod_versione_variante + " Qtà:" + string(l_chiave_distinta.quan_utilizzo_variante)
		else
			tvi_campo.label = l_chiave_distinta.cod_prodotto_variante + "," + ls_des_prodotto_variante + ", ver. " + ls_cod_versione_variante + " qtà:" + string(l_chiave_distinta.quan_utilizzo_variante)
		end if

		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
				 cod_versione = :l_chiave_distinta.cod_versione_variante;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
		
		case else
			
			fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose
				 
		ll_cont_figlio += ll_cont_figlio_comm
		
	else
		
		if ib_verifica_fasi_avanzamento and not isnull(l_chiave_distinta.cod_lavorazione) and len(l_chiave_distinta.cod_lavorazione) > 0 then
			tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + " / " + l_chiave_distinta.cod_lavorazione + " / " + l_chiave_distinta.cod_reparto + " - " +  ls_des_prodotto + " - Ver " + l_chiave_distinta.cod_versione_figlio + " Qtà:" + string(l_chiave_distinta.quan_utilizzo)
		else
			tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto + " - Ver " + l_chiave_distinta.cod_versione_figlio + " Qtà:" + string(l_chiave_distinta.quan_utilizzo)
		end if
		
		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
				 cod_versione = :l_chiave_distinta.cod_versione_figlio;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
						 
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
		
		case else
			
			fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose

		
		ll_cont_figlio += ll_cont_figlio_comm
		
	end if
	
	if ls_flag_materia_prima = 'N' then
	// non c'è il flag materia prima impostato
	
		if isnull(ll_cont_figlio) or ll_cont_figlio < 1 then
			// sotto questo ramo non ci sono figli; quindi è una materia prima
			
			// non visualizzo le materie prime
			if ib_salta_materie_prime then continue
			
			ll_pictureindex = 3		// MATERIA PRIMA
			
			if l_chiave_distinta.flag_tipo_record = "I" then ll_pictureindex = 4  // integrazione
			
			//if lb_flag_variante then ll_pictureindex = 5		// VARIANTE
			if lb_flag_variante then	// VARIANTE
				if uof_variante_vincolante(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
													l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
													l_chiave_distinta.cod_versione_figlio) then
					//vincolante
					ll_pictureindex = 11
				else
					//non vincolante
					ll_pictureindex = 5
				end if
			end if
			
			if ib_verifica_fasi_avanzamento and not isnull(ls_stato_fase) then	// stato fase
				choose case ls_stato_fase
					case "A"
						ll_pictureindex = 6
					case "I"
						ll_pictureindex = 7
					case "F"
						ll_pictureindex = 8
					case "E"  // fase esterna aperta
						ll_pictureindex = 9
				end choose
			end if
			
			if l_chiave_distinta.flag_ramo_descrittivo = "S" then ll_pictureindex = 10
			
			tvi_campo.pictureindex = ll_pictureindex
			tvi_campo.selectedpictureindex = ll_pictureindex
			tvi_campo.overlaypictureindex = ll_pictureindex
			
			//********************************************************************************************
			if is_tipo_gestione = "varianti_commesse" and ls_flag_scarico_parziale="S" then tvi_campo.label = is_simbolo_scarico_parziale + tvi_campo.label
			//********************************************************************************************
			ll_handle=ftv_db.insertitemlast(fl_handle, tvi_campo)
			continue
			
		else
			// ci sono dei figli, si tratta di un semilavorato
			
			ll_pictureindex = 2		// SEMILAVORATO
			
			if l_chiave_distinta.flag_tipo_record = "I" then ll_pictureindex = 4  // integrazione
			
			//if lb_flag_variante then ll_pictureindex = 5 		// VARIANTE
			if lb_flag_variante then
				if uof_variante_vincolante(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
													l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
													l_chiave_distinta.cod_versione_figlio) then
					//vincolante
					ll_pictureindex = 11
				else
					//non vincolante
					ll_pictureindex = 5
				end if
			end if
			
			if ib_verifica_fasi_avanzamento and not isnull(ls_stato_fase) then // stato fase
				choose case ls_stato_fase
					case "A"
						ll_pictureindex = 6
					case "I"
						ll_pictureindex = 7
					case "F"
						ll_pictureindex = 8
					case "E"  // fase esterna aperta
						ll_pictureindex = 9
				end choose
			end if
			
			if l_chiave_distinta.flag_ramo_descrittivo = "S" then ll_pictureindex = 10

			tvi_campo.pictureindex = ll_pictureindex
			tvi_campo.selectedpictureindex = ll_pictureindex
			tvi_campo.overlaypictureindex = ll_pictureindex
			
			//********************************************************************************************
			if is_tipo_gestione = "varianti_commesse" and ls_flag_scarico_parziale="S" then tvi_campo.label = is_simbolo_scarico_parziale + tvi_campo.label
			//********************************************************************************************
			
			ll_handle=ftv_db.insertitemlast(fl_handle, tvi_campo)
			ll_cont_figlio = 0
		
			if lb_flag_variante = true then
				li_risposta=uof_imposta_tv(ref ftv_db, l_chiave_distinta.cod_prodotto_variante, l_chiave_distinta.cod_versione_variante, fi_num_livello_cor + 1,ll_handle, fs_ordinamento, ref ls_errore)
				lb_flag_variante = false
			else
				li_risposta=uof_imposta_tv(ref ftv_db, l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_figlio, fi_num_livello_cor + 1,ll_handle,fs_ordinamento, ref ls_errore)				
			end if
		
		end if
		
	else
	// impostato forzatamente a materia prima con il flag della variante
	
		// non visualizzo le materie prime
		if ib_salta_materie_prime then continue
		
		ll_pictureindex = 3		// INTEGRAZIONE
		
		if l_chiave_distinta.flag_tipo_record = "I" then ll_pictureindex = 4  // integrazione
			
		//if lb_flag_variante then ll_pictureindex = 5		// VARIANTE
		if lb_flag_variante then
			if uof_variante_vincolante(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
												l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
												l_chiave_distinta.cod_versione_figlio) then
				//vincolante
				ll_pictureindex = 11
			else
				//non vincolante
				ll_pictureindex = 5
			end if
		end if
		
		if l_chiave_distinta.flag_ramo_descrittivo = "S" then ll_pictureindex = 10

		tvi_campo.pictureindex = ll_pictureindex
		tvi_campo.selectedpictureindex = ll_pictureindex
		tvi_campo.overlaypictureindex = ll_pictureindex
		
		//********************************************************************************************
		if is_tipo_gestione = "varianti_commesse" and ls_flag_scarico_parziale="S" then tvi_campo.label = is_simbolo_scarico_parziale + tvi_campo.label
		//********************************************************************************************
		
		ll_handle=ftv_db.insertitemlast(fl_handle, tvi_campo)
		
		continue

	end if
next

destroy(lds_righe_distinta)

return 0
end function

public function long uof_prepara_datastore (ref datastore fds_righe_distinta, string fs_cod_prodotto, string fs_cod_versione, string fs_ordinamento, ref string fs_errore);long ll_num_righe
fds_righe_distinta = Create DataStore

choose case is_tipo_gestione
	case "varianti_commesse"
		fds_righe_distinta.DataObject = "d_data_store_distinta_comm"
	case "varianti_ordini"
		fds_righe_distinta.DataObject = "d_data_store_distinta_det_ord_ven"
	case "varianti_offerte"
		fds_righe_distinta.DataObject = "d_data_store_distinta_det_off_ven"
case else
	fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
	return -1
	
end choose

fds_righe_distinta.SetTransObject(sqlca)

choose case is_tipo_gestione
	case "varianti_commesse"
		ll_num_righe = fds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, il_anno_registrazione, il_num_registrazione)
	case "varianti_ordini"
		ll_num_righe = fds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, il_anno_registrazione, il_num_registrazione, il_prog_riga_registrazione)
	case "varianti_offerte"
		ll_num_righe = fds_righe_distinta.Retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fs_cod_versione, il_anno_registrazione, il_num_registrazione, il_prog_riga_registrazione)
//case else
//	fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
//	return -1
end choose

choose case fs_ordinamento
	case "S"  // sequenza
		fds_righe_distinta.setsort("#4 A")
		fds_righe_distinta.sort()
	case "P"
		fds_righe_distinta.setsort("#2 A")
		fds_righe_distinta.sort()
	case "D"
		fds_righe_distinta.setsort("#11 A")
		fds_righe_distinta.sort()
//case else
//	fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
//	return -1
end choose


if isnull(ll_num_righe) then ll_num_righe = 0

return ll_num_righe


end function

public function integer uof_imposta_report_old (ref datawindow fdw_report, string fs_cod_prodotto, string fs_cod_versione, integer fi_num_livello_cor, string fs_errore);//	Funzione che imposta l'outliner
//
// nome: uof_imposta_tv
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari
//    Modifica Michela 19/09/2006: aggiunta la gestione della versione del prodotto figlio

string					ls_cod_prodotto, ls_errore, ls_des_prodotto, ls_cod_versione, ls_cod_versione_variante, ls_flag_scarico_parziale, &
						ls_cod_prodotto_variante, ls_des_prodotto_variante, ls_flag_materia_prima, ls_flag_materia_prima_variante, &
						ls_stato_fase, ls_cod_lavorazione, ls_flag_fase_conclusa, ls_flag_fase_esterna, ls_cod_reparto, &
						ls_cod_prodotto_figlio_report, ls_cod_versione_figlio_report, ls_des_prodotto_report, ls_flag_materia_prima_report, &
						ls_cod_misura_mag_report, ls_quantita,ls_cod_misura_mag_variante,ls_cod_misura_mag, ls_valore, ls_valore_2
						
long					ll_num_figli, ll_num_righe, ll_cont_figlio, ll_cont_figlio_comm, ll_cont, ll_j, ll_ret

integer				li_num_priorita, li_risposta

decimal				ldd_quan_utilizzo_variante,ldd_quan_utilizzo_report

boolean				lb_flag_variante

s_chiave_distinta	l_chiave_distinta

datastore			lds_righe_distinta

lb_flag_variante = false
ll_num_figli = 1

ll_num_righe = uof_prepara_datastore(ref lds_righe_distinta, fs_cod_prodotto, fs_cod_versione, "S", ref fs_errore)
if ll_num_righe<0 then
	//c'è stato un errore (msg in fs_errore)
	return -1
end if


for ll_num_figli = 1 to ll_num_righe
	
	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.cod_versione_figlio = lds_righe_distinta.getitemstring( ll_num_figli,"cod_versione_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber( ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring( ll_num_figli,"cod_prodotto_padre")
	l_chiave_distinta.cod_versione_padre = lds_righe_distinta.getitemstring( ll_num_figli, "cod_versione")
	l_chiave_distinta.flag_tipo_record = lds_righe_distinta.getitemstring( ll_num_figli,"flag_tipo_record")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring( ll_num_figli,"flag_materia_prima")
	l_chiave_distinta.flag_ramo_descrittivo = lds_righe_distinta.getitemstring( ll_num_figli,"flag_ramo_descrittivo")
	choose case l_chiave_distinta.flag_tipo_record
		case 'D'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
		case 'I'
			l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"progressivo")
	end choose
	
	
	//Donato 01/12/2011
	//nuova funzione che sostituisce il choose case
	ll_ret = uof_get_data_varianti_gestione(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, &
														l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_padre, &
														l_chiave_distinta.cod_versione_figlio, &
														ls_cod_prodotto_variante, ls_cod_versione_variante, ldd_quan_utilizzo_variante, &
														ls_flag_materia_prima_variante, ls_flag_scarico_parziale, fs_errore)
	if ll_ret<0 then
		//c'è stato un errore (msg in fs_errore)
		return -1
	end if
		
	setnull(l_chiave_distinta.cod_prodotto_variante)
	setnull(l_chiave_distinta.cod_versione_variante)
	setnull(l_chiave_distinta.quan_utilizzo_variante)
	lb_flag_variante=false

	if ll_ret = 0 then		// trovato variante
	
		l_chiave_distinta.cod_prodotto_variante = ls_cod_prodotto_variante
		l_chiave_distinta.cod_versione_variante = ls_cod_versione_variante
		l_chiave_distinta.quan_utilizzo_variante = ldd_quan_utilizzo_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		l_chiave_distinta.flag_tipo_record = "V"

	   select des_prodotto, cod_misura_mag
		into   :ls_des_prodotto_variante, :ls_cod_misura_mag_variante
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and    
		       cod_prodotto = :l_chiave_distinta.cod_prodotto_variante;

		ls_des_prodotto = trim(ls_des_prodotto_variante)

	   lb_flag_variante = true
	end if

   select des_prodotto, cod_misura_mag
	into   :ls_des_prodotto, :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;

	ls_des_prodotto = trim(ls_des_prodotto)
	
	
	if lb_flag_variante then
		ls_cod_prodotto_figlio_report = l_chiave_distinta.cod_prodotto_variante
		ls_cod_versione_figlio_report = ls_cod_versione_variante
		ldd_quan_utilizzo_report      = l_chiave_distinta.quan_utilizzo_variante
		ls_des_prodotto_report        = ls_des_prodotto_variante
		ls_flag_materia_prima_report  = ls_flag_materia_prima
		ls_cod_misura_mag_report      = ls_cod_misura_mag_variante
		
		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
				 cod_versione = :l_chiave_distinta.cod_versione_variante;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_variante and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_variante;
		
		case else
			
			fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose
				 
		ll_cont_figlio += ll_cont_figlio_comm
		
	else  // no caso variante.
		
		ls_cod_prodotto_figlio_report = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_figlio_report = l_chiave_distinta.cod_versione_figlio
		ldd_quan_utilizzo_report      = l_chiave_distinta.quan_utilizzo
		ls_des_prodotto_report        = ls_des_prodotto
		ls_cod_misura_mag_report      = ls_cod_misura_mag
		
		ll_cont_figlio = 0
		ll_cont_figlio_comm = 0
		
		select count(cod_prodotto_figlio)
		into   :ll_cont_figlio
		from   distinta
		where  cod_azienda = :s_cs_xx.cod_azienda and	 
		       cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
				 cod_versione = :l_chiave_distinta.cod_versione_figlio;
	
		choose case is_tipo_gestione
				
			case "varianti_commesse"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_commessa
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_commessa = :il_anno_registrazione and
						 num_commessa = :il_num_registrazione and 	
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
						 
			case "varianti_ordini"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_ord_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
			case "varianti_offerte"
				
				select count(cod_prodotto_figlio)
				into   :ll_cont_figlio_comm
				from   integrazioni_det_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and 	
						 prog_riga_off_ven = :il_prog_riga_registrazione and
						 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_figlio and
						 cod_versione_padre = :l_chiave_distinta.cod_versione_figlio;
				
		
		case else
			
			fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
			return -1
			
		end choose

		
		ll_cont_figlio += ll_cont_figlio_comm
		
	end if
	
	
	if ( isnull(ll_cont_figlio) or ll_cont_figlio < 1 ) OR ( ls_flag_materia_prima = "S" ) then
		// sotto questo ramo non ci sono figli; quindi è una materia prima
		
		// non visualizzo le materie prime
		if ib_salta_materie_prime then continue
		
		ll_j = fdw_report.insertrow(0)
		
		fdw_report.setitem( ll_j, "tipo_riga", 3)
		
		fdw_report.setitem( ll_j, "num_livello", fi_num_livello_cor)
		
		if int(ldd_quan_utilizzo_report) <> ldd_quan_utilizzo_report then
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0.0###")
		else
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0")
		end if
		
		if ib_trasf_componenti then
			//cod prodotto e quantità separati in due campi diversi, inoltre no versione
			ls_valore = ls_cod_prodotto_figlio_report
			ls_valore_2 = ls_quantita + " " + ls_cod_misura_mag_report
		else
			//come prima
			ls_valore = ls_cod_prodotto_figlio_report + "  Vers:" + ls_cod_versione_figlio_report + "   " + ls_quantita + " " + ls_cod_misura_mag_report
			ls_valore_2 = ""
		end if
		
		if fi_num_livello_cor>0 then
			fdw_report.setitem(ll_j,"cod_l_"+string(fi_num_livello_cor), ls_valore)
			fdw_report.setitem(ll_j,"des_l_"+string(fi_num_livello_cor), ls_des_prodotto_report)
			
			if ib_trasf_componenti then
				fdw_report.setitem(ll_j,"max_livello", il_livello_max)
				fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ls_valore_2)
			end if
			
		end if

		fdw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
		if l_chiave_distinta.flag_tipo_record <> "D" then
			fdw_report.setitem( ll_j, "flag_variante", l_chiave_distinta.flag_tipo_record)
		end if

		continue
		
	else
		// ci sono dei figli, si tratta di un semilavorato
		
		ll_j = fdw_report.insertrow(0)
		
		fdw_report.setitem( ll_j, "tipo_riga", 3)		
		
		fdw_report.setitem( ll_j, "num_livello", fi_num_livello_cor)
		
		if int(ldd_quan_utilizzo_report) <> ldd_quan_utilizzo_report then
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0.0###")
		else
			ls_quantita = string(ldd_quan_utilizzo_report, "#,##0")
		end if
		
		if ib_trasf_componenti then
			//cod prodotto e quantità separati in due campi diversi, inoltre no versione
			ls_valore = ls_cod_prodotto_figlio_report
			ls_valore_2 = ls_quantita + " " + ls_cod_misura_mag_report
		else
			//come prima
			ls_valore = ls_cod_prodotto_figlio_report + "  Vers:" + ls_cod_versione_figlio_report + "   " + ls_quantita + " " + ls_cod_misura_mag_report
			ls_valore_2 = ""
		end if
		
		if fi_num_livello_cor>0 then
			fdw_report.setitem(ll_j,"cod_l_"+string(fi_num_livello_cor), ls_valore)
			fdw_report.setitem(ll_j,"des_l_"+string(fi_num_livello_cor), ls_des_prodotto_report)
			
			if ib_trasf_componenti then
				fdw_report.setitem(ll_j,"max_livello", il_livello_max)
				fdw_report.setitem(ll_j,"qta_l_"+string(fi_num_livello_cor), ls_valore_2)
			end if
			
		end if


		fdw_report.setitem( ll_j, "flag_mp", ls_flag_materia_prima)
		if l_chiave_distinta.flag_tipo_record <> "D" then
			fdw_report.setitem( ll_j, "flag_variante", l_chiave_distinta.flag_tipo_record)
		end if

		ll_cont_figlio = 0
	
		if lb_flag_variante = true then
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fi_num_livello_cor + 1) > il_livello_max and il_livello_max > 0 then
				lb_flag_variante = false
				continue
			end if
			//-------------------------------------------------------------------------

			li_risposta=uof_imposta_report(ref fdw_report, l_chiave_distinta.cod_prodotto_variante, l_chiave_distinta.cod_versione_variante, fi_num_livello_cor + 1, ref ls_errore)
			lb_flag_variante = false
		else
			
			//Donato 14/03/2012 Controllo sul livello di espansione
			//inoltre se il livello impostato è nullo o ZERO ignora il controllo livelli
			if (fi_num_livello_cor + 1) > il_livello_max and il_livello_max > 0 then continue
			//-------------------------------------------------------------------------

			li_risposta=uof_imposta_report(ref fdw_report, l_chiave_distinta.cod_prodotto_figlio, l_chiave_distinta.cod_versione_figlio, fi_num_livello_cor + 1,ref ls_errore)				
		end if
	
	end if
	
next

destroy(lds_righe_distinta)

return 0
end function

public function long uof_get_data_varianti_gestione (string fs_cod_prodotto_padre, long fl_num_sequenza, string fs_cod_prodotto_figlio, string fs_cod_versione_padre, string fs_cod_versione_figlio, ref string fs_cod_prodotto_variante, ref string fs_cod_versione_variante, ref decimal fdd_quan_utilizzo_variante, ref string fs_flag_materia_prima_variante, ref string fs_flag_scarico_parziale, ref string fs_errore);long ll_ret

fs_flag_scarico_parziale = "N"

choose case is_tipo_gestione
		
	case "varianti_commesse"
		select	cod_prodotto,
				cod_versione_variante,
				quan_utilizzo,
				flag_materia_prima,
				flag_scarico_parziale
		into		:fs_cod_prodotto_variante,
					:fs_cod_versione_variante,
					:fdd_quan_utilizzo_variante,
					:fs_flag_materia_prima_variante,
					:fs_flag_scarico_parziale
		from   varianti_commesse
		where	 cod_azienda = :s_cs_xx.cod_azienda and    
					 anno_commessa = :il_anno_registrazione and    
					 num_commessa = :il_num_registrazione and    
					 cod_prodotto_padre = :fs_cod_prodotto_padre and    
					 num_sequenza = :fl_num_sequenza and    
					 cod_prodotto_figlio = :fs_cod_prodotto_figlio and    
					 cod_versione = :fs_cod_versione_padre and
					 cod_versione_figlio = :fs_cod_versione_figlio;
		
	case "varianti_ordini"
		
		select	cod_prodotto,
				cod_versione_variante,
				quan_utilizzo,
				flag_materia_prima
		into		:fs_cod_prodotto_variante,
					:fs_cod_versione_variante,
					:fdd_quan_utilizzo_variante,
					:fs_flag_materia_prima_variante
		from   varianti_det_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and    
					anno_registrazione = :il_anno_registrazione and    
					num_registrazione = :il_num_registrazione and    
					prog_riga_ord_ven = :il_prog_riga_registrazione and    
					cod_prodotto_padre = :fs_cod_prodotto_padre and    
					num_sequenza = :fl_num_sequenza and    
					cod_prodotto_figlio = :fs_cod_prodotto_figlio and    
					cod_versione = :fs_cod_versione_padre and
					cod_versione_figlio = :fs_cod_versione_figlio;
		
	case "varianti_offerte"
		select	cod_prodotto,
				cod_versione_variante,
				quan_utilizzo,
				flag_materia_prima
		into		:fs_cod_prodotto_variante,
					:fs_cod_versione_variante,
					:fdd_quan_utilizzo_variante,
					:fs_flag_materia_prima_variante
		from   varianti_det_off_ven
		where 	cod_azienda = :s_cs_xx.cod_azienda and    
					anno_registrazione = :il_anno_registrazione and    
					num_registrazione = :il_num_registrazione and    
					prog_riga_off_ven = :il_prog_riga_registrazione and    
					cod_prodotto_padre = :fs_cod_prodotto_padre and    
					num_sequenza = :fl_num_sequenza and    
					cod_prodotto_figlio = :fs_cod_prodotto_figlio and    
					cod_versione = :fs_cod_versione_padre and
					cod_versione_figlio = :fs_cod_versione_figlio;

case else
	fs_errore = "il tipo gestione passato come argomento non è fra quelli previsti."
	return -1
	
end choose

ll_ret = sqlca.sqlcode
if ll_ret < 0 then fs_errore = sqlca.sqlerrtext

return ll_ret



end function

on uo_imposta_tv_varianti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_imposta_tv_varianti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

