﻿$PBExportHeader$uo_parametri_blocco.sru
forward
global type uo_parametri_blocco from nonvisualobject
end type
end forward

global type uo_parametri_blocco from nonvisualobject
end type
global uo_parametri_blocco uo_parametri_blocco

type variables
// Bolle
constant string GENERA_BOLLA_VENDITA = "GBV"
constant string INSERIMENTO_BOLLA_VENDITA = "IBV"
constant string STAMPA_BOLLA_VENDITA = "SBV"

// Fatture
constant string GENERA_FATTURA_VENDITA = "GFV"
constant string INSERIMENTO_FATTURA_VENDITA = "IFV"
constant string STAMPA_FATTURA_VENDITA = "SFV"

// Ordine
constant string GENERA_ORDINE_VENDITA = "G0V"
constant string INSERIMENTO_ORDINE_VENDITA = "IOV"
constant string STAMPA_ORDINE_VENDITA = "SOV"


private:
	boolean ib_showMessageBox = true
end variables

forward prototypes
public subroutine uof_show_messagebox (boolean ab_show)
public function integer uof_can (string as_action, string as_cod_cliente, ref string as_message)
end prototypes

public subroutine uof_show_messagebox (boolean ab_show);/**
 * stefanop
 * 12/02/2014
 *
 * Imposta la visualizzazione della messagebox per gli avvisi
 **/
 
ib_showMessageBox = ab_show
end subroutine

public function integer uof_can (string as_action, string as_cod_cliente, ref string as_message);/**
 * stefanop
 * 12/02/2014
 *
 * Controlla l'impostazione di blocco per l'azione indicata.
 *
 * @param String as_action Nome dell'azione; uo_parametri.INSERIMENTO_BOLLE_VENDITA
 * @param String as_cod_cliente Codice cliente
 * @param string  as_error Messaggio di errore
 *
 * @return 
 *		0, tutto ok, nessun messaggio
 *		-1, errore bloccante
 *		-2, warning ma NON blocco
 **/
 
string ls_flag_blocco, ls_msg_blocco, ls_warning_blocco
string ls_flag_blocco_fin, ls_msg_blocco_fin, ls_warning_blocco_fin 
string ls_flag_blocco_cliente, ls_flag_blocco_fin_cliente

// prevent null
if isnull(as_action) then as_action = ""
if isnull(as_cod_cliente) then as_cod_cliente = ""
 
// Controllo l'esistenza dell'azione
select
	flag_blocco, msg_blocco_bloccato, msg_blocco_warning
	flag_blocco_fin, msg_blocco_fin_bloccato, msg_blocco_fin_warning
into
	:ls_flag_blocco, :ls_msg_blocco, :ls_warning_blocco,
	:ls_flag_blocco_fin, :ls_msg_blocco_fin, :ls_warning_blocco_fin
from parametri_blocco
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = :as_action;
		 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante il controllo della validita dell'azione. " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_message = "L'azione di blocco " + as_action + " non esiste"
	return -1
end if

// prevent null
if isnull(ls_flag_blocco) then  ls_flag_blocco = "N"
if isnull(ls_flag_blocco_fin) then  ls_flag_blocco_fin = "N"

// Recupero situazione del cliente
select flag_blocco, flag_blocco_fin
into :ls_flag_blocco_cliente, :ls_flag_blocco_fin_cliente
from anag_clienti 
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :as_cod_cliente;
		 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante la lettura del cliente: " + as_cod_cliente
	return -1
elseif sqlca.sqlcode = 100 then
	as_message ="Cliente " + as_cod_cliente + " non trovato in anagrafica."
	return -1
end if

// prevent null
if isnull(ls_flag_blocco_cliente) then  ls_flag_blocco_cliente = "N"
if isnull(ls_flag_blocco_fin_cliente) then  ls_flag_blocco_fin_cliente = "N"

if (ls_flag_blocco_cliente = "N" and ls_flag_blocco = "N") and (ls_flag_blocco_fin_cliente = "N" and ls_flag_blocco_fin= "N") then
	// nessun blocco o warning, tutto ok
	setnull(as_message)
	return 0
elseif ls_flag_blocco_cliente = "S" or ls_flag_blocco_fin_cliente = "S" then
	// Se è blocatto o bloccato_fin  controllo lo stato dei parametri
	if ls_flag_blocco_cliente = "S" and ls_flag_blocco = "W" then
	end if
	
end if

return -1
end function

on uo_parametri_blocco.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_parametri_blocco.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

