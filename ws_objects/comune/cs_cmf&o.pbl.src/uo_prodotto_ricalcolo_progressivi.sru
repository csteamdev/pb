﻿$PBExportHeader$uo_prodotto_ricalcolo_progressivi.sru
forward
global type uo_prodotto_ricalcolo_progressivi from nonvisualobject
end type
end forward

global type uo_prodotto_ricalcolo_progressivi from nonvisualobject
end type
global uo_prodotto_ricalcolo_progressivi uo_prodotto_ricalcolo_progressivi

type variables
string is_path_origine
end variables

forward prototypes
public function integer uof_aggiorna_assegnato (string fs_cod_prodotto, ref string fs_errore)
public function integer uof_scrivi_log (string fs_testo, string fs_file)
public function integer uof_aggiorna_impegnato (string fs_cod_prodotto, ref string fs_error)
public function integer uof_aggiorna_spedito (string fs_cod_prodotto, ref string fs_errore)
public function integer uof_aggiorna_quan_in_produzione (string fs_cod_prodotto, ref string fs_errore)
public function integer uof_ricalcola_progressivi_prodotto (string fs_cod_prodotto, ref string fs_error)
end prototypes

public function integer uof_aggiorna_assegnato (string fs_cod_prodotto, ref string fs_errore);string							ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_sql, ls_str, ls_cod_misura_mag
long							ll_prog_stock, ll_tot, ll_index
dec{4}						ld_quan_in_evasione
datetime						ldt_data_stock
datastore					lds_data

ls_sql = "SELECT cod_prodotto,"+&
						"cod_deposito,"+&
						"cod_ubicazione,"+&
						"cod_lotto,"+&
						"data_stock,"+&
						"prog_stock,"+&
						"quan_in_evasione "+&
 			"FROM  evas_ord_ven "+&
			"WHERE  cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
		 				"cod_prodotto = '"+fs_cod_prodotto+"' and "+&
		 				"flag_spedito <>'S' "

// enme
update anag_prodotti
set    quan_assegnata = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in azzeramento assegnato in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

update stock
set    quan_assegnata = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in azzeramento assegnato negli stock: " + sqlca.sqlerrtext
	return -1
end if


ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)


if ll_tot<0 then
	fs_errore = "Errore in creazione datastore righe assegnate: " + fs_errore
	return -1
else
	for ll_index=1 to ll_tot
		ls_cod_prodotto			= lds_data.getitemstring(ll_index, 1)
		ls_cod_deposito			= lds_data.getitemstring(ll_index, 2)
		ls_cod_ubicazione			= lds_data.getitemstring(ll_index, 3)
		ls_cod_lotto					= lds_data.getitemstring(ll_index, 4)
		ldt_data_stock				= lds_data.getitemdatetime(ll_index, 5)
		ll_prog_stock				= lds_data.getitemnumber(ll_index, 6)
		ld_quan_in_evasione		= lds_data.getitemdecimal(ll_index, 7)
		
		if isnull(ld_quan_in_evasione) or ld_quan_in_evasione<0 then ld_quan_in_evasione = 0
		
		update anag_prodotti
		set    quan_assegnata = quan_assegnata + :ld_quan_in_evasione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode< 0 then
			destroy lds_data
			fs_errore = "Errore in aggiornamento prodotti: " + sqlca.sqlerrtext
			return -1
		end if
	
		update stock
		set    quan_assegnata = quan_assegnata + :ld_quan_in_evasione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_stock and
				 prog_stock = :ll_prog_stock;
				 
		if sqlca.sqlcode = -1 then
			destroy lds_data
			fs_errore = "Errore in aggiornamento stock: " + sqlca.sqlerrtext
			return -1
		end if
		
	next
end if

select quan_assegnata, cod_misura_mag
into :ld_quan_in_evasione, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto;

if isnull(ld_quan_in_evasione) then ld_quan_in_evasione = 0

destroy lds_data

return 0

end function

public function integer uof_scrivi_log (string fs_testo, string fs_file);integer li_file

li_file = FileOpen(fs_file,LineMode!, Write!, LockWrite!, Append!)
filewrite(li_file, fs_testo)
fileclose(li_file)

return 0
end function

public function integer uof_aggiorna_impegnato (string fs_cod_prodotto, ref string fs_error);uo_magazzino					luo_magazzino
long								ll_i, ll_tot
string								ls_file, ls_str, ls_cod_misura_mag
dec{4}							ld_quan_impegnata_old, ld_quan_impegnata_new

ls_file = is_path_origine + "aggiorna_impegnato.log"

luo_magazzino = CREATE uo_magazzino

	
//leggo l'impegnato prima
select quan_impegnata
into :ld_quan_impegnata_old
from anag_prodotti
where  cod_azienda  = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;
			 
if sqlca.sqlcode<0 then
	fs_error = "Errore lettura q.tà impegnata (prima) per il prodotto "+fs_cod_prodotto
	rollback;
	return -1
elseif sqlca.sqlcode=100 then
	fs_error = "Prodotto "+fs_cod_prodotto+" inesistente in anagrafica!"
	uof_scrivi_log(fs_error, ls_file)
	fs_error = ""
	return 1
end if
	
if luo_magazzino.uof_ricalcola_impegnato( fs_cod_prodotto, fs_error) < 0 then
	destroy luo_magazzino
	rollback;
	
	uof_scrivi_log(fs_error, ls_file)
	return 1
end if
	
//commit del singolo prodotto
commit;
	
//leggo l'impegnato dopo
select quan_impegnata, cod_misura_mag
into :ld_quan_impegnata_new, :ls_cod_misura_mag
from anag_prodotti
where  cod_azienda  = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;

if isnull(ld_quan_impegnata_new) then ld_quan_impegnata_new = 0
	
ls_str = 	"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------~r~n" + &
			string(today(),"dd/mm/yyyy") + " " + string(now(),"hh:mm:ss") + " " + "Aggiornamento Impegnato Prodotto: " + fs_cod_prodotto + "~r~n" + &
			"quan_impegnata = " + string(ld_quan_impegnata_new, "###,###,##0.00")			+ "  -> (era "+string(ld_quan_impegnata_old, "###,###,##0.00")+")" 
uof_scrivi_log(ls_str, ls_file)


fs_error = string(ld_quan_impegnata_new, "###,###,##0.00##") + " " + ls_cod_misura_mag + " " + fs_cod_prodotto

destroy luo_magazzino

return 0
end function

public function integer uof_aggiorna_spedito (string fs_cod_prodotto, ref string fs_errore);string					ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto,ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
						ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_sql, ls_sql2, ls_str, ls_cod_misura_mag
						
long					ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, &
						ll_index, ll_tot

dec{4}				ld_quan_consegnata

datetime				ldt_data_stock

datastore			lds_data



ls_sql = "SELECT		a.cod_tipo_det_ven,"+&
				"a.cod_prodotto,"+&
				"a.cod_deposito,"+&
				"a.cod_ubicazione,"+&
				"a.cod_lotto,"+&
				"a.data_stock,"+&
				"a.progr_stock,"+&
				"a.quan_consegnata,"+&
				"a.anno_registrazione_mov_mag,"+&
				"a.num_registrazione_mov_mag "+&
 "FROM  det_bol_ven as a "+&
" JOIN tes_bol_ven as b ON 	b.cod_azienda=a.cod_azienda and "+&
 									"b.anno_registrazione=a.anno_registrazione and "+&
									 "b.num_registrazione=a.num_registrazione "+&
"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and "+&
			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
			"a.cod_prodotto = '"+fs_cod_prodotto+"' "

ls_sql2 = "SELECT	a.cod_tipo_det_ven,"+&
			"a.cod_prodotto,"+&
			"a.cod_deposito,"+&
			"a.cod_ubicazione,"+&
			"a.cod_lotto,"+&
			"a.data_stock,"+&
			"a.progr_stock,"+&
			"a.quan_fatturata,"+&
			"a.anno_registrazione_mov_mag,"+&
			"a.num_registrazione_mov_mag "+&
 "FROM  det_fat_ven as a "+&
 "JOIN	tes_fat_ven as b ON		b.cod_azienda=a.cod_azienda and "+&
 								"b.anno_registrazione=a.anno_registrazione and "+&
								"b.num_registrazione=a.num_registrazione "+&
"JOIN tab_tipi_fat_ven as t ON 	t.cod_azienda=a.cod_azienda and "+&
										"t.cod_tipo_fat_ven=b.cod_tipo_fat_ven "+&
"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and t.flag_tipo_fat_ven='I' and "+&
			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
			"a.cod_prodotto = '"+fs_cod_prodotto+"' "


//azzero la quantità in spedizione in anagrafica del prodotto
update anag_prodotti
set    quan_in_spedizione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in azzeramento quantita in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

//azzero la quantità in spedizione negli stock del prodotto
update stock
set    quan_in_spedizione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto like :fs_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in azzeramento quantità in spedizione negli stock: " + sqlca.sqlerrtext
	return -1
end if


ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)

if ll_tot<0 then
	fs_errore = "Errore in creazione datastore ddt: " + fs_errore
	return -1
elseif ll_tot > 0 then
	for ll_index=1 to ll_tot
		
		ls_cod_tipo_det_ven					= lds_data.getitemstring(ll_index, 1)
		ls_cod_prodotto						= lds_data.getitemstring(ll_index, 2)
		ls_cod_deposito						= lds_data.getitemstring(ll_index, 3)
		ls_cod_ubicazione						= lds_data.getitemstring(ll_index, 4)
		ls_cod_lotto								= lds_data.getitemstring(ll_index, 5)
		ldt_data_stock							= lds_data.getitemdatetime(ll_index, 6)
		ll_prog_stock							= lds_data.getitemnumber(ll_index, 7)
		ld_quan_consegnata					= lds_data.getitemdecimal(ll_index, 8)
		//ll_anno_registrazione_mov_mag	= lds_data.getitemnumber(ll_index, 9)
		//ll_num_registrazione_mov_mag	= lds_data.getitemnumber(ll_index, 10)
		
		
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
		
			update anag_prodotti
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto ;
					 
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
			
			update stock
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
		end if
		
		
	next
end if

destroy lds_data

ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql2, fs_errore)

if ll_tot<0 then
	fs_errore = "Errore in creazione datastore fatture: " + fs_errore
	return -1
elseif ll_tot > 0 then
	
	for ll_index=1 to ll_tot
		ls_cod_tipo_det_ven							= lds_data.getitemstring(ll_index, 1)
		ls_cod_prodotto								= lds_data.getitemstring(ll_index, 2)
		ls_cod_deposito								= lds_data.getitemstring(ll_index, 3)
		ls_cod_ubicazione								= lds_data.getitemstring(ll_index, 4)
		ls_cod_lotto										= lds_data.getitemstring(ll_index, 5)
		ldt_data_stock									= lds_data.getitemdatetime(ll_index, 6)
		ll_prog_stock									= lds_data.getitemnumber(ll_index, 7)
		ld_quan_consegnata							= lds_data.getitemdecimal(ll_index, 8)
		//ll_anno_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 9)
		//ll_num_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 10)
		
		////riga ddt già movimentata?
		//if not isnull(ll_anno_registrazione_mov_mag) or not isnull(ll_anno_registrazione_mov_mag) then continue
	
		
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
			
			update anag_prodotti
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
			
			update stock
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
		end if
	next
end if


select quan_in_spedizione, cod_misura_mag
into :ld_quan_consegnata, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto;
			
if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0
		
fs_errore = string(ld_quan_consegnata, "###,###,##0.00##") + " " + ls_cod_misura_mag + " " + fs_cod_prodotto

destroy lds_data

return 0

end function

public function integer uof_aggiorna_quan_in_produzione (string fs_cod_prodotto, ref string fs_errore);string						ls_cod_prodotto_raggruppato, ls_sql, ls_cod_misura_mag
		 
dec{4}					ld_quan_in_produzione, ld_quan_raggruppo

datastore				lds_data

long						ll_tot, ll_index



//commesse del prodotto non bloccate e in avanzamento
ls_sql = 	"SELECT quan_in_produzione "+&
			"FROM  anag_commesse "+&
			"WHERE  cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
						"flag_blocco = 'N' and "+&
						"cod_prodotto = '"+fs_cod_prodotto+"' and "+&
						"(flag_tipo_avanzamento='2' or flag_tipo_avanzamento='3') and "+&
						"quan_in_produzione>0 "


if isnull(fs_cod_prodotto) or len(fs_cod_prodotto) < 1 then fs_cod_prodotto = "%"

update anag_prodotti
set    quan_in_produzione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in azzeramento quantita in produzione in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

// enme 08/1/2006 gestione prodotto raggruppato
update anag_prodotti  
	set quan_in_produzione = 0
 where cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_prodotto in 
							(select cod_prodotto_raggruppato 
							 from  anag_prodotti 
							 where cod_azienda = :s_cs_xx.cod_azienda and
							       cod_prodotto like :fs_cod_prodotto and 
							       cod_prodotto_raggruppato is not null);
if sqlca.sqlcode = -1 then
	fs_errore = "Errore in azzeramento quantita in produzione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)
if ll_tot < 0 then
	fs_errore = "Errore in creazione datastpre commesse: " + fs_errore
	return -1
	
elseif ll_tot > 0 then
	
	for ll_index=1 to ll_tot
		yield()
		ld_quan_in_produzione = lds_data.getitemdecimal(ll_index, 1)
	
		//#######################################################################
		//													q_prodotta			q_in_prod		q_ordine
		//#######################################################################
		//Quando creo la commessa:						0						0					8
		//Quando attivo											0						8					8
		//Mentre produco										3						5					8
		//....														6						2					8
		//alla chiusura											8						0					8
		//in ogni caso la quota parte per ogni commessa di prodotto in produzione è nella colonna quan_in_produzione
		
		
		update anag_prodotti
		set    quan_in_produzione = quan_in_produzione + :ld_quan_in_produzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_prodotto;
				 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
	
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
		
			ld_quan_raggruppo = f_converti_qta_raggruppo(fs_cod_prodotto, ld_quan_in_produzione, "M")
		
			update anag_prodotti  
				set quan_in_produzione = quan_in_produzione + :ld_quan_raggruppo 
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in aggiornamento quantità in spedizione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		//commit su ogni prodotto
		
	next
end if

select quan_in_produzione, cod_misura_mag
into :ld_quan_in_produzione, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto;

if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0

fs_errore = string(ld_quan_in_produzione, "###,###,##0.00##") + " " + ls_cod_misura_mag

return 0

end function

public function integer uof_ricalcola_progressivi_prodotto (string fs_cod_prodotto, ref string fs_error);string					ls_file, ls_chiave[], ls_vuoto[], ls_where, ls_error, ls_str
dec{4}   				ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
						ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
						ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
						ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
						ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[], ld_costo_medio_stock[], &
						ld_quan_costo_medio_stock[],ld_saldo_quan_anno_prec_1, ld_vuoto[]
uo_magazzino		luo_magazzino
datetime				ldt_max_data_mov
integer				li_ret_funz

dec{4}				ld_saldo_quan_inizio_anno_old, ld_saldo_quan_ultima_chiusura_old, ld_prog_quan_entrata_old, ld_val_quan_entrata_old, &
						ld_prog_quan_uscita_old, ld_val_quan_uscita_old, ld_prog_quan_acquistata_old, ld_val_quan_acquistata_old, &
						ld_prog_quan_venduta_old, ld_val_quan_venduta_old


ls_file = is_path_origine + "ricalcola_progressivi.log"


	// ****************************** Ricalcolo Progressivi Prodotto *************************************
	// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
	// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
	// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
	
		
ld_quant_val_attuale = ld_vuoto
ls_chiave = ls_vuoto
ld_giacenza = ld_vuoto
ld_costo_medio_stock = ld_vuoto
ld_quan_costo_medio_stock = ld_vuoto
	
ldt_max_data_mov = datetime(today())

luo_magazzino = create uo_magazzino
li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal( fs_cod_prodotto, &
																			ldt_max_data_mov, &
																			ls_where, &
																			ref ld_quant_val_attuale[], &
																			ref ls_error, &
																			'N', &
																			ref ls_chiave[], &
																			ref ld_giacenza[], &
																			ref ld_costo_medio_stock[], &
																			ref ld_quan_costo_medio_stock[])

destroy luo_magazzino

if li_ret_funz < 0 then
	fs_error = "Errore nel calcolo della situazione attuale del prodotto " + fs_cod_prodotto + ", " + ls_error
	uof_scrivi_log(fs_error, ls_file)
	fs_error = ""
	return 1
end if

//leggo prima il pre-esistente
select		saldo_quan_inizio_anno,
			 saldo_quan_ultima_chiusura,
			 prog_quan_entrata,
			 val_quan_entrata,
			 prog_quan_uscita,
			 val_quan_uscita,
			 prog_quan_acquistata,
			 val_quan_acquistata,
			 prog_quan_venduta,
			 val_quan_venduta
into			:ld_saldo_quan_inizio_anno_old,
				:ld_saldo_quan_ultima_chiusura_old,
				:ld_prog_quan_entrata_old,
				:ld_val_quan_entrata_old,
				:ld_prog_quan_uscita_old,
				:ld_val_quan_uscita_old,
				:ld_prog_quan_acquistata_old,
				:ld_val_quan_acquistata_old,
				:ld_prog_quan_venduta_old,
				:ld_val_quan_venduta_old
from anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
			cod_prodotto = :fs_cod_prodotto;
			

if isnull(ld_saldo_quan_inizio_anno_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_saldo_quan_ultima_chiusura_old) then ld_saldo_quan_ultima_chiusura_old=0
if isnull(ld_prog_quan_entrata_old) then ld_prog_quan_entrata_old=0
if isnull(ld_val_quan_entrata_old) then ld_val_quan_entrata_old=0
if isnull(ld_prog_quan_uscita_old) then ld_prog_quan_uscita_old=0
if isnull(ld_val_quan_uscita_old) then ld_val_quan_uscita_old=0
if isnull(ld_prog_quan_acquistata_old) then ld_prog_quan_acquistata_old=0
if isnull(ld_val_quan_acquistata_old) then ld_val_quan_acquistata_old=0
if isnull(ld_prog_quan_venduta_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_val_quan_venduta_old) then ld_val_quan_venduta_old=0

ld_saldo_quan_inizio_anno = ld_quant_val_attuale[1]
ld_prog_quan_entrata = ld_quant_val_attuale[4]
ld_val_quan_entrata = ld_quant_val_attuale[5]
ld_prog_quan_uscita = ld_quant_val_attuale[6]
ld_val_quan_uscita = ld_quant_val_attuale[7]
ld_prog_quan_acquistata = ld_quant_val_attuale[8]
ld_val_quan_acquistata = ld_quant_val_attuale[9]
ld_prog_quan_venduta = ld_quant_val_attuale[10]
ld_val_quan_venduta = ld_quant_val_attuale[11]

update anag_prodotti 
set	 saldo_quan_inizio_anno = :ld_saldo_quan_inizio_anno,
		 saldo_quan_ultima_chiusura = :ld_saldo_quan_inizio_anno,
		 prog_quan_entrata = :ld_prog_quan_entrata,
		 val_quan_entrata = :ld_val_quan_entrata,
		 prog_quan_uscita = :ld_prog_quan_uscita,
		 val_quan_uscita = :ld_val_quan_uscita,
		 prog_quan_acquistata = :ld_prog_quan_acquistata,
		 val_quan_acquistata = :ld_val_quan_acquistata,
		 prog_quan_venduta = :ld_prog_quan_venduta,
		 val_quan_venduta = :ld_val_quan_venduta
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :fs_cod_prodotto;	

if sqlca.sqlcode <> 0 then
	fs_error = "Errore durante aggiornamento progressivi prodotto " + fs_cod_prodotto + ", " + sqlca.sqlerrtext
	
	uof_scrivi_log(fs_error, ls_file)
	fs_error = ""
	rollback;
	return 1
	
end if

//fai il commit ad ogni prodotto elaborato e scrivi nel log il risultato
commit;

//#######################################################################################

ls_str = 	"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------~r~n" + &
			string(today(),"dd/mm/yyyy") + " " + string(now(),"hh:mm:ss") + " " + "Aggiornamento Progressivi Prodotto: " + fs_cod_prodotto + "~r~n" + &
			"saldo_quan_inizio_anno = " + string(ld_saldo_quan_inizio_anno, "###,###,##0.00")			+ "  -> (era "+string(ld_saldo_quan_inizio_anno_old, "###,###,##0.00")+")" + "~r~n" + &
			"saldo_quan_ultima_chiusura =  " + string(ld_saldo_quan_inizio_anno,"###,###,##0.00")	+ "  -> (era "+string(ld_saldo_quan_ultima_chiusura_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_entrata = " + string(ld_prog_quan_entrata,"###,###,##0.00")						+ "  -> (era "+string(ld_prog_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_entrata = " + string(ld_val_quan_entrata,"###,###,##0.00")							+ "  -> (era "+string(ld_val_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_uscita = " + string(ld_prog_quan_uscita,"###,###,##0.00")							+ "  -> (era "+string(ld_prog_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_uscita = " + string(ld_val_quan_uscita,"###,###,##0.00")								+ "  -> (era "+string(ld_val_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_acquistata = " + string(ld_prog_quan_acquistata,"###,###,##0.00")				+ "  -> (era "+string(ld_prog_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_acquistata = " + string(ld_val_quan_acquistata,"###,###,##0.00")					+ "  -> (era "+string(ld_val_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_venduta = " + string(ld_prog_quan_venduta,"###,###,##0.00")						+ "  -> (era "+string(ld_prog_quan_venduta_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_venduta = " + string(ld_val_quan_venduta,"###,###,##0.00")							+  "  -> (era "+string(ld_val_quan_venduta_old, "###,###,##0.00")+")"

uof_scrivi_log(ls_str, ls_file)

return 0
end function

on uo_prodotto_ricalcolo_progressivi.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_prodotto_ricalcolo_progressivi.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

