﻿$PBExportHeader$uo_ricambi.sru
forward
global type uo_ricambi from nonvisualobject
end type
end forward

global type uo_ricambi from nonvisualobject
end type
global uo_ricambi uo_ricambi

forward prototypes
public function integer uof_ricambi_tipologia (string as_cod_attrezzatura, string as_cod_tipo_manut, ref string as_messaggio)
public function integer uof_ricambi_programma (long al_anno, long al_num, ref string as_messaggio)
public function integer uof_ricambi_manutenzione (long al_anno, long al_num, ref string as_messaggio)
public function integer uof_duplica_tipologia (string as_cod_attrezzatura_da, string as_cod_tipo_manut_da, string as_cod_attrezzatura_a, string as_cod_tipo_manut_a, ref string as_messaggio)
end prototypes

public function integer uof_ricambi_tipologia (string as_cod_attrezzatura, string as_cod_tipo_manut, ref string as_messaggio);dec{4} ld_quantita[], ld_prezzo_ricambio

long   ll_null, ll_i

string ls_cod_prodotto, ls_cod_versione, ls_ricambi[], ls_des_ricambio, ls_errore


select cod_ricambio,
		 cod_versione
into   :ls_cod_prodotto,
		 :ls_cod_versione
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura kit ricambio da tab_tipi_manutenzione: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura kit ricambio da tab_tipi_manutenzione: la tipologia di manutenzione indicata non esiste"
	return -1
end if

delete
from   tipi_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :as_cod_attrezzatura and
		 cod_tipo_manutenzione = :as_cod_tipo_manut;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in azzeramento elenco ricambi in tabella tipi_manutenzioni_ricambi: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_prodotto) and not isnull(ls_cod_versione) then
	
	setnull(ll_null)
	
	if f_trova_mat_prima_dec(ls_cod_prodotto,ls_cod_versione,ls_ricambi[],ld_quantita[],1,ll_null,ll_null, ref ls_errore) = -1 then
		as_messaggio = "Errore in lettura elenco ricambi del kit~r~n" + ls_errore
		return -1
	end if
	
	for ll_i = 1 to upperbound(ls_ricambi)
		
		select des_prodotto,
				 prezzo_acquisto
		into   :ls_des_ricambio,
				 :ld_prezzo_ricambio
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_ricambi[ll_i];
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in lettura dati ricambio da anag_prodotti: " + sqlca.sqlerrtext
			return -1
		end if
		
		insert
		into   tipi_manutenzioni_ricambi
				 (cod_azienda,
				 cod_attrezzatura,
				 cod_tipo_manutenzione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio)
		values (:s_cs_xx.cod_azienda,
				 :as_cod_attrezzatura,
				 :as_cod_tipo_manut,
				 :ll_i,
				 :ls_ricambi[ll_i],
				 :ls_des_ricambio,
				 :ld_quantita[ll_i],
				 :ld_prezzo_ricambio);
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in inserimento ricambio in tipi_manutenzioni_ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
end if

return 0
end function

public function integer uof_ricambi_programma (long al_anno, long al_num, ref string as_messaggio);long   ll_null, ll_i, ll_prog_ricambio

dec{4} ld_quantita[], ld_quan_ricambio, ld_prezzo_ricambio

string ls_cod_prodotto, ls_cod_versione, ls_cod_attrezzatura, ls_cod_tipo_manut, ls_ricambi[], ls_des_ricambio, &
       ls_cod_ricambio, ls_errore


select cod_ricambio,
		 cod_versione,
		 cod_attrezzatura,
		 cod_tipo_manutenzione
into   :ls_cod_prodotto,
		 :ls_cod_versione,
		 :ls_cod_attrezzatura,
		 :ls_cod_tipo_manut
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_num;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati programma da programmi_manutenzione: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati programma da programmi_manutenzione: il programma indicato non esiste"
	return -1
end if

delete
from   prog_manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_num;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in azzeramento elenco ricambi in tabella prog_manutenzioni_ricambi: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_prodotto) and not isnull(ls_cod_versione) then
	
	setnull(ll_null)
	
	if f_trova_mat_prima_dec(ls_cod_prodotto,ls_cod_versione,ls_ricambi[],ld_quantita[],1,ll_null,ll_null, ls_errore) = -1 then
		g_mb.messagebox("OMNIA","Errore in lettura elenco ricambi del kit.~r~n" + ls_errore)
		return -1
	end if
	
	for ll_i = 1 to upperbound(ls_ricambi)
		
		select des_prodotto,
				 prezzo_acquisto
		into   :ls_des_ricambio,
				 :ld_prezzo_ricambio
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_ricambi[ll_i];
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in lettura dati ricambio da anag_prodotti: " + sqlca.sqlerrtext)
			return -1
		end if
		
		insert
		into   prog_manutenzioni_ricambi
				 (cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio)
		values (:s_cs_xx.cod_azienda,
				 :al_anno,
				 :al_num,
				 :ll_i,
				 :ls_ricambi[ll_i],
				 :ls_des_ricambio,
				 :ld_quantita[ll_i],
				 :ld_prezzo_ricambio);
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in inserimento ricambio in prog_manutenzioni_ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
else
	
	if not isnull(ls_cod_attrezzatura) and not isnull(ls_cod_tipo_manut) then
	
		declare ricambi cursor for
		select   prog_riga_ricambio,
					cod_prodotto,
					des_prodotto,
					quan_utilizzo,
					prezzo_ricambio
		from     tipi_manutenzioni_ricambi
		where    cod_azienda = :s_cs_xx.cod_azienda and
					cod_attrezzatura = :ls_cod_attrezzatura and
					cod_tipo_manutenzione = :ls_cod_tipo_manut
		order by prog_riga_ricambio;
		
		open ricambi;
		
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
		do while true
			
			fetch ricambi
			into  :ll_prog_ricambio,
					:ls_cod_ricambio,
					:ls_des_ricambio,
					:ld_quan_ricambio,
					:ld_prezzo_ricambio;
					
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
				close ricambi;
				return -1
			elseif sqlca.sqlcode = 100 then
				close ricambi;
				exit
			end if
			
			insert
			into   prog_manutenzioni_ricambi
					 (cod_azienda,
					 anno_registrazione,
					 num_registrazione,
					 prog_riga_ricambio,
					 cod_prodotto,
					 des_prodotto,
					 quan_utilizzo,
					 prezzo_ricambio)
			values (:s_cs_xx.cod_azienda,
					 :al_anno,
					 :al_num,
					 :ll_prog_ricambio,
					 :ls_cod_ricambio,
					 :ls_des_ricambio,
					 :ld_quan_ricambio,
					 :ld_prezzo_ricambio);
					 
			if sqlca.sqlcode <> 0 then
				as_messaggio = "Errore in inserimento ricambio in prog_manutenzioni_ricambi: " + sqlca.sqlerrtext
				return -1
			end if
			
		loop
		
	end if
	
end if
	
return 0
end function

public function integer uof_ricambi_manutenzione (long al_anno, long al_num, ref string as_messaggio);long   ll_anno_prog, ll_num_prog, ll_null, ll_i, ll_prog_ricambio

dec{4} ld_quantita[], ld_prezzo_ricambio, ld_quan_ricambio

string ls_cod_prodotto, ls_cod_versione, ls_cod_attrezzatura, ls_cod_tipo_manut, ls_ricambi[], ls_des_ricambio, &
       ls_cod_ricambio, ls_errore


select cod_ricambio,
		 cod_versione,
		 anno_reg_programma,
		 num_reg_programma,
		 cod_attrezzatura,
		 cod_tipo_manutenzione
into   :ls_cod_prodotto,
		 :ls_cod_versione,
		 :ll_anno_prog,
		 :ll_num_prog,
		 :ls_cod_attrezzatura,
		 :ls_cod_tipo_manut
from   manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_num;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati manutenzione da manutenzioni: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati manutenzione da manutenzioni: la manutenzione indicata non esiste"
	return -1
end if

delete
from   manutenzioni_ricambi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_num;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in azzeramento elenco ricambi in tabella manutenzioni_ricambi: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_prodotto) and not isnull(ls_cod_versione) then
	
	setnull(ll_null)
	
	if f_trova_mat_prima_dec(ls_cod_prodotto,ls_cod_versione,ls_ricambi[],ld_quantita[],1,ll_null,ll_null, ls_errore) = -1 then
		as_messaggio = "Errore in lettura elenco ricambi del kit~r~n" + ls_errore
		return -1
	end if
	
	for ll_i = 1 to upperbound(ls_ricambi)
		
		select des_prodotto,
				 prezzo_acquisto
		into   :ls_des_ricambio,
				 :ld_prezzo_ricambio
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_ricambi[ll_i];
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in lettura dati ricambio da anag_prodotti: " + sqlca.sqlerrtext
			return -1
		end if
		
		insert
		into   manutenzioni_ricambi
				 (cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio,
				 flag_utilizzo,
				 anno_reg_mov_mag,
				 num_reg_mov_mag)
		values (:s_cs_xx.cod_azienda,
				 :al_anno,
				 :al_num,
				 :ll_i,
				 :ls_ricambi[ll_i],
				 :ls_des_ricambio,
				 :ld_quantita[ll_i],
				 :ld_prezzo_ricambio,
				 'S',
				 null,
				 null);
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in inserimento ricambio in manutenzioni_ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
elseif not isnull(ll_anno_prog) and not isnull(ll_num_prog) then
	
	declare ricambi cursor for
	select   prog_riga_ricambio,
				cod_prodotto,
				des_prodotto,
				quan_utilizzo,
				prezzo_ricambio
	from     prog_manutenzioni_ricambi
	where    cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_prog and
				num_registrazione = :ll_num_prog
	order by prog_riga_ricambio;
	
	open ricambi;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		
		fetch ricambi
		into  :ll_prog_ricambio,
				:ls_cod_ricambio,
				:ls_des_ricambio,
				:ld_quan_ricambio,
				:ld_prezzo_ricambio;
				
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
			close ricambi;
			return -1
		elseif sqlca.sqlcode = 100 then
			close ricambi;
			exit
		end if
		
		insert
		into   manutenzioni_ricambi
				 (cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio,
				 flag_utilizzo,
				 anno_reg_mov_mag,
				 num_reg_mov_mag)
		values (:s_cs_xx.cod_azienda,
				 :al_anno,
				 :al_num,
				 :ll_prog_ricambio,
				 :ls_cod_ricambio,
				 :ls_des_ricambio,
				 :ld_quan_ricambio,
				 :ld_prezzo_ricambio,
				 'S',
				 null,
				 null);
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in inserimento ricambio in manutenzioni_ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
	loop
		
elseif not isnull(ls_cod_attrezzatura) and not isnull(ls_cod_tipo_manut) then
		
	declare ricambi_2 cursor for
	select   prog_riga_ricambio,
				cod_prodotto,
				des_prodotto,
				quan_utilizzo,
				prezzo_ricambio
	from     tipi_manutenzioni_ricambi
	where    cod_azienda = :s_cs_xx.cod_azienda and
				cod_attrezzatura = :ls_cod_attrezzatura and
				cod_tipo_manutenzione = :ls_cod_tipo_manut
	order by prog_riga_ricambio;
	
	open ricambi_2;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		
		fetch ricambi_2
		into  :ll_prog_ricambio,
				:ls_cod_ricambio,
				:ls_des_ricambio,
				:ld_quan_ricambio,
				:ld_prezzo_ricambio;
				
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
			close ricambi_2;
			return -1
		elseif sqlca.sqlcode = 100 then
			close ricambi_2;
			exit
		end if
		
		insert
		into   manutenzioni_ricambi
				 (cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio,
				 flag_utilizzo,
				 anno_reg_mov_mag,
				 num_reg_mov_mag)
		values (:s_cs_xx.cod_azienda,
				 :al_anno,
				 :al_num,
				 :ll_prog_ricambio,
				 :ls_cod_ricambio,
				 :ls_des_ricambio,
				 :ld_quan_ricambio,
				 :ld_prezzo_ricambio,
				 'S',
				 null,
				 null);
				 
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in inserimento ricambio in manutenzioni_ricambi: " + sqlca.sqlerrtext
			return -1
		end if
		
	loop
		
end if

return 0
end function

public function integer uof_duplica_tipologia (string as_cod_attrezzatura_da, string as_cod_tipo_manut_da, string as_cod_attrezzatura_a, string as_cod_tipo_manut_a, ref string as_messaggio);long   ll_prog_ricambio

dec{4} ld_quan_ricambio, ld_prezzo_ricambio

string ls_cod_ricambio, ls_des_ricambio


declare ricambi cursor for
select 	cod_prodotto,
		 	des_prodotto,
		 	quan_utilizzo,
		 	prezzo_ricambio
from   	tipi_manutenzioni_ricambi
where  	cod_azienda = :s_cs_xx.cod_azienda and
		 	cod_attrezzatura = :as_cod_attrezzatura_da and
		 	cod_tipo_manutenzione = :as_cod_tipo_manut_da
order by prog_riga_ricambio;
		 
open ricambi;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore nella open del cursore ricambi: " + sqlca.sqlerrtext
	return -1
end if

ll_prog_ricambio = 0

do while true

	fetch ricambi
	into  :ls_cod_ricambio,
			:ls_des_ricambio,
			:ld_quan_ricambio,
			:ld_prezzo_ricambio;
			
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore nella fetch del cursore ricambi: " + sqlca.sqlerrtext
		close ricambi;
		return -1
	elseif sqlca.sqlcode = 100 then
		close ricambi;
		exit
	end if
	
	ll_prog_ricambio ++
	
	insert
	into   tipi_manutenzioni_ricambi
		    (cod_azienda,
		    cod_attrezzatura,
		    cod_tipo_manutenzione,
		    prog_riga_ricambio,
		    cod_prodotto,
		  	 des_prodotto,
		  	 quan_utilizzo,
		    prezzo_ricambio)
	values (:s_cs_xx.cod_azienda,
			 :as_cod_attrezzatura_a,
			 :as_cod_tipo_manut_a,
			 :ll_prog_ricambio,
			 :ls_cod_ricambio,
			 :ls_des_ricambio,
			 :ld_quan_ricambio,
			 :ld_prezzo_ricambio);
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in inserimento ricambio in tipi_manutenzioni_ricambi: " + sqlca.sqlerrtext
		close ricambi;
		return -1
	end if
	
loop

return 0
end function

on uo_ricambi.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ricambi.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

