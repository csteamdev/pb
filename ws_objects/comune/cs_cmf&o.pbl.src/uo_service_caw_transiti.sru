﻿$PBExportHeader$uo_service_caw_transiti.sru
forward
global type uo_service_caw_transiti from uo_service_base
end type
end forward

global type uo_service_caw_transiti from uo_service_base
end type
global uo_service_caw_transiti uo_service_caw_transiti

type variables
private:
	string		INI_FILENAME			= "caw_transiti.ini"

	//default value se non presente in file INI
	string		ODBC_NAME			= "terminali"
	string		PROCESS_NAME 		= "CAW_TRANSITI"
	string		DIREZIONE_IN			= "1"
	string		DIREZIONE_OUT		= "2"
	string		EVENTO_OK				= "utente ok"
	string		ID_EVENTO_OK			= "65"

end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);string					ls_path_log, ls_odbc, ls_sql, ls_last_execution, ls_errore, ls_varco_ds, ls_direzione_ds, ls_barcode_ds, ls_log
datetime				ldt_execution, ldt_last_execution, ldt_data_ds
transaction			lt_tran
datastore			lds_data
long					ll_index, ll_tot



//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=caw_transiti,N
//																		dove
//																				N è la verbosità del log
//##########################################################################################


//PARAMETRI ----------------------------------------------------------------------------------------------------------------------------

//livello di logging (1..5 se non passo niente metto 2)
iuo_log.set_log_level(auo_params.uof_get_int(1, 2))

//FILE DI LOG -----------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log
ls_path_log =  s_cs_xx.volume + s_cs_xx.risorse + "logs\"+string(today(),"yyyy-mm-dd")+"_"+PROCESS_NAME+".log"

//il file di LOG viene ad ogni elaborazione dello stesso giorno modificato e non sovrascritto
iuo_log.set_file( ls_path_log, false)


//-------------------------------------------------------------------------------------------------------------------------------------------
//leggo odbc e altre proprietà dal file ini
INI_FILENAME = s_cs_xx.volume + s_cs_xx.risorse + INI_FILENAME

ODBC_NAME 				= ProfileString (INI_FILENAME, "DATABASE", "ODBC_NAME", 			ODBC_NAME)

PROCESS_NAME	 		=  ProfileString (INI_FILENAME, "OPTIONS", "PROCESS_NAME", 		PROCESS_NAME)
DIREZIONE_IN				=  ProfileString (INI_FILENAME, "OPTIONS", "DIREZIONE_IN", 		DIREZIONE_IN)
DIREZIONE_OUT			=  ProfileString (INI_FILENAME, "OPTIONS", "DIREZIONE_OUT", 		DIREZIONE_OUT)
EVENTO_OK					=  ProfileString (INI_FILENAME, "OPTIONS", "EVENTO_OK", 			EVENTO_OK)
ID_EVENTO_OK				=  ProfileString (INI_FILENAME, "OPTIONS", "STR_IDEVENTO_OK", 	ID_EVENTO_OK)


iuo_log.log("### INIZIO ELABORAZIONE PROCESSO "+PROCESS_NAME+" #######################")


//--------------------------------------------------------------------------------------------------------------------------------------------
//verifico in tabella sync_process quale è stata l'ultima volta che ho eseguito
select last_execution
into : ldt_last_execution
from sync_processes
where name=:PROCESS_NAME
using sqlca;

if sqlca.sqlcode<0 then
	iuo_log.error("Errore in lettura ultima esecuzione processo "+PROCESS_NAME+" da sync_process: " + sqlca.sqlerrtext)
	return -1
	
elseif sqlca.sqlcode=100 then
	//mai eseguito prima, quindi inserisco record con data ultima esecuzione nulla
	insert into sync_processes
		(name, last_execution)
	values
		(:PROCESS_NAME, null)
	using sqlca;
	
	if sqlca.sqlcode<0 then
		iuo_log.error("Errore in inserimento processo "+PROCESS_NAME+" in sync_process: " + sqlca.sqlerrtext)
		return -1
	end if
	
	setnull(ldt_last_execution)
	
end if

if  year(date(ldt_last_execution)) <= 1980 then setnull(ldt_last_execution)



//--------------------------------------------------------------------------------------------------------------------------------------------
//creo transazione esterna
lt_tran = create transaction

lt_tran.dbms = "ODBC"
lt_tran.AutoCommit = False
lt_tran.DBParm = "ConnectString='DSN="+ODBC_NAME+"',CommitOnDisconnect='No',DelimitIdentifier='No'"

connect using lt_tran;
if lt_tran.sqlcode<0 then
	iuo_log.error("Errore in connessione database esterno: " + lt_tran.sqlerrtext)
	
	rollback using sqlca;
	destroy lt_tran
	return -1
end if

iuo_log.log("Connessione database estreno effettuata con successo!")


//------------------------------------------------------------------------------------------------------------------------------------
//data di richiesta dei dati (da utilizzare alla fine dell'esecuzione del processo come data ultima esecuzione in sync_process)
ldt_execution = datetime(today(), now())

//formattazione data			{ts 'yyyy-mm-dd hh:MM:ss'}
//es. {ts '2015-03-18 13:15:00'}

ls_last_execution = "{ts '" 
ls_last_execution += string(year(date(ldt_last_execution))) + "-" + string(month(date(ldt_last_execution)), "00") + "-" + string(day(date(ldt_last_execution)), "00")
ls_last_execution += " " + string(hour(time(ldt_last_execution)), "00")+":" + string(minute(time(ldt_last_execution)), "00")+":" + string(second(time(ldt_last_execution)), "00")
ls_last_execution += "'}"


//------------------------------------------------------------------------------------------------------------------------------------
//preparo la query
ls_sql = 	"select data, varco, direzione, codicebadge "+&
			"from eventi "+&
			"where evento="+ID_EVENTO_OK

 if not isnull(ldt_last_execution) then
	//allora prendo quelle successive all'ultima elaborazione
	ls_sql += " and data > " + ls_last_execution
end if

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, lt_tran, ls_errore)
if ll_tot <0 then
	disconnect using lt_tran;
	destroy lt_tran;
	
	iuo_log.error("Errore in creazione dati per lettura da database esterno: " + ls_errore)
	
	rollback using sqlca;
	return -1
	
elseif ll_tot=0 then
	iuo_log.log("Nessun dato risulta da elaborare!")
end if


//------------------------------------------------------------------------------------------------------------------------------------
//elaboro i dati
for ll_index=1 to ll_tot
	ldt_data_ds			= lds_data.getitemdatetime(ll_index, 1)
	ls_varco_ds			= lds_data.getitemstring(ll_index, 2)
	ls_direzione_ds		= lds_data.getitemstring(ll_index, 3)
	ls_barcode_ds		= lds_data.getitemstring(ll_index, 4)
	
	if ls_direzione_ds = DIREZIONE_IN then
		ls_direzione_ds = "E"
	else
		ls_direzione_ds = "U"
	end if
	
	insert into log_transiti
		(barcode, flag_evento, orario, porta)
	values
		(:ls_barcode_ds, :ls_direzione_ds, :ldt_data_ds, :ls_varco_ds)
	using sqlca;
	
	if sqlca.sqlcode<0 then
		iuo_log.error("Errore in inserimento dati badge: " + sqlca.sqlerrtext)
		
		rollback using sqlca;
		disconnect using lt_tran;
		destroy lt_tran;
		destroy lds_data
		
		return -1
		
	else
		iuo_log.log("Inserito   VARCO: " + ls_varco_ds + " - BARCODE: " + ls_barcode_ds + " - DIREZIONE: " + ls_direzione_ds + " - ORARIO" + string(ldt_data_ds, "dd-MM-yyyy hh:mm:ss"))
	end if
	
next

destroy lds_data
disconnect using lt_tran;
destroy lt_tran;



//salvo data ultima elaborazione del processo
update sync_processes
set last_execution = :ldt_execution
where name=:PROCESS_NAME
using sqlca;

if sqlca.sqlcode<0 then
	iuo_log.error("Errore in aggiornamento data ultima esecuzione processo "+PROCESS_NAME+" in sync_process: " + sqlca.sqlerrtext)
	rollback using sqlca;
	return -1
end if

commit using sqlca;

iuo_log.log("### FINE ELABORAZIONE PROCESSO "+PROCESS_NAME+" (tot. badges elaborati: "+string(ll_tot)+" ) ############")

return 0
end function

on uo_service_caw_transiti.create
call super::create
end on

on uo_service_caw_transiti.destroy
call super::destroy
end on

