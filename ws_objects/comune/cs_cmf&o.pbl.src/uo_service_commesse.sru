﻿$PBExportHeader$uo_service_commesse.sru
forward
global type uo_service_commesse from uo_service_base
end type
end forward

global type uo_service_commesse from uo_service_base
end type
global uo_service_commesse uo_service_commesse

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);string					ls_errore, ls_path_log, ls_sql, ls_testo
long					ll_tot, ll_index, ll_ok, ll_ko, ll_skipped
date					ldt_today
time					ltm_now
integer				li_ret, li_sleep
datastore			lds_data
uo_funzioni_1		luo_funz
boolean				lb_errore, lb_from_ddt_trasf = false
datetime				ldt_oggi


//variabili per il datastore
long				ll_num_commessa_ds

longlong			ll_progressivo_ds

integer			li_anno_commessa_ds

datetime			ldt_data_creazione_mov_ds, ldt_data_pianificazione_ds, ldt_data_elaborazione_ds

dec{4}			ld_quan_ordine_ds, ld_dim_x_ds, ld_dim_y_ds

string				ls_cod_semilavorato_ds, ls_flag_non_collegati_ds, ls_forza_deposito_scarico_mp_ds, &
					ls_trasf_interno_ds, ls_cod_deposito_scarico_mp_ds, ls_ddt_trasf_multimpli_log_ds, ls_cod_tipo_lista_dist_ds, &
					ls_cod_lista_dist_ds, ls_cod_deposito_carico_mp_ds, ls_errore_ds, ls_cod_utente_ds, ls_flag_from_ddt_trasf_ds, &
					ls_cod_deposito_from_ds, ls_cod_deposito_to_ds






//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=commesse,A01,k,n
//																			dove 	k è un numero che indica i secondi di attesa tra l'elaborazione di una commessa e l'altra
//																					n è un numero che indica la verbosità del log (min 1, max 5, default 2)
//##########################################################################################


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//imposto l'azienda (se non passo niente metto A01) ed il livello di logging (1..5 se non passo niente metto 2)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

//sleep
li_sleep = auo_params.uof_get_int(2, 2)
if isnull(li_sleep) or li_sleep<0 then li_sleep=0

//verbosity
iuo_log.set_log_level(auo_params.uof_get_int(3, 2))


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log    commesse_YYYYMMDD.log
ldt_today = today()
ltm_now = now()
ls_path_log = s_cs_xx.volume + s_cs_xx.risorse + "logs\commesse_" + &
								string(year(ldt_today)) + "-" + right("00" + string(month(ldt_today)), 2) + "-" + right("00" + string(day(ldt_today)), 2) + ".log"

//fai l'append nel file del giorno
iuo_log.set_file( ls_path_log, false)
iuo_log.warn("### INIZIO ELABORAZIONE COMMESSE MEDIANTE PROCESSO POSTICIPATO #######################")


//carico quelle pianificate per la prima volta, quindi se la colonna errore è valorizzata la escluderò
ls_sql = "select progressivo,"+&
					"anno_commessa,"+&
					"num_commessa,"+&
					"cod_semilavorato,"+&
					"flag_non_collegati,"+&
					"forza_deposito_scarico_mp,"+&
					"trasf_interno,"+&
					"cod_deposito_scarico_mp,"+&
					"ddt_trasf_multimpli_log,"+&
					"cod_tipo_lista_dist,"+&
					"cod_lista_dist,"+&
					"cod_deposito_carico_mp,"+&
					"data_creazione_mov,"+&
					"quan_ordine,"+&
					"dim_x,"+&
					"dim_y,"+&
					"cod_utente,"+&
					"data_pianificazione,"+&
					"data_elaborazione,"+&
					"flag_from_ddt_trasf,"+&
					"cod_deposito_from,"+&
					"cod_deposito_to,"+&
					"errore "+&
			"from buffer_chiusura_commesse "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' "+&
			"order by progressivo asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	iuo_log.error(ls_errore)
	return -1
end if

iuo_log.warn("Tot. commesse da elaborare: " + string(ll_tot))

 ll_ok = 0
 ll_ko = 0
 ll_skipped = 0

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//se arrivi fin qui vuol dire che ci sono files da elaborare

luo_funz = create uo_funzioni_1
luo_funz.uof_set_pianificato(true)

for ll_index=1 to ll_tot
	ll_progressivo_ds 					= lds_data.getitemnumber(ll_index, "progressivo")
	li_anno_commessa_ds 			= lds_data.getitemnumber(ll_index, "anno_commessa")
	ll_num_commessa_ds 			= lds_data.getitemnumber(ll_index, "num_commessa")
	
	ls_testo = "(prg:"+string(ll_progressivo_ds)+") Elaborazione commessa " + string(li_anno_commessa_ds) + "/" + string(ll_num_commessa_ds)
	
	ls_cod_semilavorato_ds = lds_data.getitemstring(ll_index, "cod_semilavorato")
	if ls_cod_semilavorato_ds<>"" and not isnull(ls_cod_semilavorato_ds) then
		ls_testo +=  " - " + ls_cod_semilavorato_ds
	end if
	
	iuo_log.log("------------------------------------------------------------------------------------------")
	iuo_log.log(ls_testo)
	
	ls_errore_ds 						= lds_data.getitemstring(ll_index, "errore")
	ldt_data_elaborazione_ds 		= lds_data.getitemdatetime(ll_index, "data_elaborazione")
	ldt_data_pianificazione_ds 		= lds_data.getitemdatetime(ll_index, "data_pianificazione")
	ls_cod_utente_ds					= lds_data.getitemstring(ll_index, "cod_utente")
	
//	if ls_errore_ds<>"" and not isnull(ls_errore_ds) then
//		
//		ls_cod_utente_ds = ls_cod_utente_ds + f_des_tabella_senza_azienda("utenti", "cod_utente='"+ls_cod_utente_ds+"'", "nome_cognome")
//		
//		iuo_log.warn("La commessa, pianificata il "+string(ldt_data_pianificazione_ds, "dd/mm/yyyyy")+ " dall'utente "+ls_cod_utente_ds + &
//						" verrà saltata in questa elaborazione in quanto risulta già elaborata in data "+string(ldt_data_elaborazione_ds, "dd/mm/yyyyy")+" con il seguente errore:~r~n" + ls_errore_ds)
//		ll_skipped += 1
//		continue
//	end if
	
	//lettura valori --------------------------------------------------------------------------------------------------------------
	ls_flag_non_collegati_ds					= lds_data.getitemstring(ll_index, "flag_non_collegati")
	ls_forza_deposito_scarico_mp_ds		= lds_data.getitemstring(ll_index, "forza_deposito_scarico_mp")
	ls_trasf_interno_ds						= lds_data.getitemstring(ll_index, "trasf_interno")
	ls_cod_deposito_scarico_mp_ds		= lds_data.getitemstring(ll_index, "cod_deposito_scarico_mp")
	ls_ddt_trasf_multimpli_log_ds			= lds_data.getitemstring(ll_index, "ddt_trasf_multimpli_log")
	ls_cod_tipo_lista_dist_ds					= lds_data.getitemstring(ll_index, "cod_tipo_lista_dist")
	ls_cod_lista_dist_ds						= lds_data.getitemstring(ll_index, "cod_lista_dist")
	ls_cod_deposito_carico_mp_ds			= lds_data.getitemstring(ll_index, "cod_deposito_carico_mp")
	ldt_data_creazione_mov_ds				= lds_data.getitemdatetime(ll_index, "data_creazione_mov")
	ld_quan_ordine_ds						= lds_data.getitemdecimal(ll_index, "quan_ordine")
	ld_dim_x_ds									= lds_data.getitemdecimal(ll_index, "dim_x")
	ld_dim_y_ds									= lds_data.getitemdecimal(ll_index, "dim_y")
	
	ls_flag_from_ddt_trasf_ds				= lds_data.getitemstring(ll_index, "flag_from_ddt_trasf")
	ls_cod_deposito_from_ds				= lds_data.getitemstring(ll_index, "cod_deposito_from")
	ls_cod_deposito_to_ds						= lds_data.getitemstring(ll_index, "cod_deposito_to")
	
	//inizializzo variabili di istanza -------------------------------------------------------------------------------------------
	
	luo_funz.ib_flag_non_collegati = (ls_flag_non_collegati_ds = "S")
	luo_funz.ib_forza_deposito_scarico_mp = (ls_forza_deposito_scarico_mp_ds = "S")
	luo_funz.ib_trasf_interno = (ls_trasf_interno_ds = "S")
	
	luo_funz.is_cod_deposito_scarico_mp = ls_cod_deposito_scarico_mp_ds
	luo_funz.is_ddt_trasf_multipli_log = ls_ddt_trasf_multimpli_log_ds
	luo_funz.is_cod_tipo_lista_dist = ls_cod_tipo_lista_dist_ds
	luo_funz.is_cod_lista_dist = ls_cod_lista_dist_ds
	luo_funz.is_cod_deposito_carico_mp = ls_cod_deposito_carico_mp_ds
	luo_funz.idt_data_creazione_mov = ldt_data_creazione_mov_ds
	luo_funz.id_quan_ordine = ld_quan_ordine_ds
	luo_funz.id_dim_x = ld_dim_x_ds
	luo_funz.id_dim_y = ld_dim_y_ds
	
	
	//se provieni da ddt di trasferimento
	lb_from_ddt_trasf = (ls_flag_from_ddt_trasf_ds = "S")
	if isnull(ls_cod_deposito_from_ds) then ls_cod_deposito_from_ds = ""
	if isnull(ls_cod_deposito_to_ds) then ls_cod_deposito_to_ds = ""
	luo_funz.uof_set_from_ddt_trasf(lb_from_ddt_trasf, ls_cod_deposito_from_ds, ls_cod_deposito_to_ds, li_anno_commessa_ds, ll_num_commessa_ds) 



	ls_errore = ""
	lb_errore = false
	if ls_cod_semilavorato_ds="" then setnull(ls_cod_semilavorato_ds)
	li_ret = luo_funz.uof_avanza_commessa(li_anno_commessa_ds, ll_num_commessa_ds, ls_cod_semilavorato_ds, ls_errore)
	
	if li_ret < 0 then
		iuo_log.error(ls_errore)
		lb_errore = true
		
		ll_ko += 1
		
	else
		if ls_errore<>"" and not isnull(ls_errore) then
			iuo_log.warn(ls_errore)
			lb_errore = true
			
			ll_skipped += 1
			
		else
			ll_ok += 1
			
		end if
		
	end if
	
	if not lb_errore then
		
		update anag_commesse
		set data_pianificazione=null
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_commessa=:li_anno_commessa_ds and
					num_commessa=:ll_num_commessa_ds;
		
		iuo_log.log("Elaborazione terminata con successo!")
		iuo_log.log("------------------------------------------------------------------------------------------")
		
		ls_testo = "Elaborata commessa ("+string(li_anno_commessa_ds)+","+string(ll_num_commessa_ds)
		
		if ls_cod_semilavorato_ds<>"" and not isnull(ls_cod_semilavorato_ds) then
			ls_testo += "," +ls_cod_semilavorato_ds
		end if
		
		ls_testo += ")"
		
		luo_funz.uof_log_sistema(ls_testo, "PIANIFICACOM")
		
		
		//elimina dal buffer
		delete from buffer_chiusura_commesse
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					progressivo=:ll_progressivo_ds;
		
		//qui fai il commit
		commit;
		
	else
		iuo_log.log("------------------------------------------------------------------------------------------")
		//annulla eventuali scritture precedenti
		rollback;
		
		ldt_oggi = datetime(today(), now())
		
		//segna l'errore in tabella buffer
		update buffer_chiusura_commesse
		set 	data_elaborazione=:ldt_oggi,
				errore=:ls_errore
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					progressivo=:ll_progressivo_ds;
		
		commit;
	end if
	
	sleep(li_sleep)
	
next

destroy luo_funz

iuo_log.warn("Elaborazione terminata, Tot.Commesse Elaborate: " + string(ll_tot) + "  -  Con Successo: " + string(ll_ok) + "  -  Saltate: " + string(ll_skipped) + "  -  Con Errori: " + string(ll_ko))
iuo_log.warn("### FINE ELABORAZIONE COMMESSE MEDIANTE PROCESSO POSTICIPATO ########################")

return 0

end function

on uo_service_commesse.create
call super::create
end on

on uo_service_commesse.destroy
call super::destroy
end on

