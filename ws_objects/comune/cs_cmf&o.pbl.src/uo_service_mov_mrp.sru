﻿$PBExportHeader$uo_service_mov_mrp.sru
forward
global type uo_service_mov_mrp from uo_service_base
end type
end forward

global type uo_service_mov_mrp from uo_service_base
end type
global uo_service_mov_mrp uo_service_mov_mrp

type variables
integer ii_num_giorni_mrp=183
end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uof_insert_data (datetime adt_data_rif_mrp, string as_cod_tipo_mrp, string as_cod_prodotto, decimal ad_quan_prevista, decimal ad_quan_elaborata, decimal ad_val_unitario, string as_tipo_doc_origine, integer ai_anno_doc_origine, long al_num_doc_origine, long al_prog_riga_doc_origine, string as_cod_reparto, string as_flag_elaborato, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore)
public function integer uof_insert_data (datetime adt_data_rif_mrp, string as_cod_tipo_mrp, string as_cod_prodotto, decimal ad_quan_prevista, decimal ad_quan_elaborata, decimal ad_val_unitario, string as_tipo_doc_origine, integer ai_anno_doc_origine, long al_num_doc_origine, long al_prog_riga_doc_origine, string as_cod_reparto, string as_flag_elaborato, ref string as_errore)
public function integer uof_insert_impegnato_prodotto (string as_cod_prodotto, string as_cod_tipo_movimento_mrp, ref string as_errore)
public function integer uof_get_giacenza_mrp (string as_cod_prodotto, ref decimal ad_giacenza, ref datetime adt_data_rif_giacenza, ref string as_errore)
public function integer uof_mrp_scheduler (string as_cod_prodotto, ref string as_errore)
public function integer uof_get_giacenza_reparto_mrp (string as_cod_prodotto, string as_cod_reparto, ref decimal ad_giacenza, ref datetime adt_data_rif_giacenza, ref string as_errore)
public function integer uof_insert_ordinato_prodotto (string as_cod_prodotto, string as_cod_tipo_movimento_mrp, ref string as_errore)
public function integer uof_media_consumi (string as_cod_prodotto, string as_cod_reparto, ref decimal ad_media_consumi, ref string as_errore)
public function integer uof_exec_scheduler (string as_cod_prodotto)
public function integer uof_giacenza_prodotto (datetime adt_data_riferimento, string as_cod_prodotto, string as_cod_deposito, ref decimal ad_giacenza, ref string as_errore)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);string					ls_path_log, ls_tipo_operazione, ls_cod_prodotto, ls_errore
date						ldt_today
time						ltm_now




//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=mov_mrp,A01,n
//																			dove n è un numero che indica la verbosità del log (min 1, max 5, default 2)
//##########################################################################################


//-----------------------------------------------------------------------------------------------------------------------------------------------------
//imposto l'azienda (se non passo niente metto A01) ed il livello di logging (1..5 se non passo niente metto 2)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//leggo il parametro
ls_tipo_operazione = auo_params.uof_get_string(2,"XXX")

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//leggo il parametro
ls_cod_prodotto = auo_params.uof_get_string(3,"%")

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log    commesse_YYYYMMDD.log
ldt_today = today()
ltm_now = now()
ls_path_log = s_cs_xx.volume + s_cs_xx.risorse + "logs\mrp_" + &
								string(year(ldt_today)) + "-" + right("00" + string(month(ldt_today)), 2) + "-" + right("00" + string(day(ldt_today)), 2) + ".log"

//fai l'append nel file del giorno
iuo_log.set_file( ls_path_log, false)

choose case upper(ls_tipo_operazione)
	case "MRP"
		// esecuzione completa scheduler
		uof_exec_scheduler(ls_cod_prodotto)
		
end choose

//
//iuo_log.warn("### INIZIO ELABORAZIONE PROCESSO MOVIMENTI MRP #################################")

//iuo_log.warn("### FINE ELABORAZIONE ELABORAZIONE PROCESSO MOVIMENTI MRP ########################")

return 0

end function

public function integer uof_insert_data (datetime adt_data_rif_mrp, string as_cod_tipo_mrp, string as_cod_prodotto, decimal ad_quan_prevista, decimal ad_quan_elaborata, decimal ad_val_unitario, string as_tipo_doc_origine, integer ai_anno_doc_origine, long al_num_doc_origine, long al_prog_riga_doc_origine, string as_cod_reparto, string as_flag_elaborato, ref integer ai_anno_registrazione, ref long al_num_registrazione, ref string as_errore);
string			ls_cod_deposito
datetime		ldt_now

setnull(ai_anno_registrazione)
setnull(al_num_registrazione)
as_errore = ""
ldt_now			  = datetime( today(), now() )	


select numero  
into :ai_anno_registrazione  
from parametri_azienda  
where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_parametro='ESC';

if sqlca.sqlcode = 100 or ai_anno_registrazione<=0 then
     as_errore="Non è stato creato il parametro ESC in parametri azienda"
     return -1

elseif sqlca.sqlcode<0 then
	as_errore="Errore in lettura parametro aziendale ESC: "+sqlca.sqlerrtext
     return -1
	  
end if

select max(num_registrazione)
into :al_num_registrazione
from mov_mrp
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_registrazione;

if isnull(al_num_registrazione) then al_num_registrazione=0
al_num_registrazione += 1

//Attenzione
//le 2 variabili 			ai_anno_registrazione   e    al_num_registrazione
//sono passate By Reference

if as_cod_tipo_mrp="" then setnull(as_cod_tipo_mrp)
if isnull(as_cod_tipo_mrp) then
	as_errore="Il campo cod_tipo_mov_mrp è obbligatorio!"
     return -1
end if


if as_cod_prodotto="" then setnull(as_cod_prodotto)

if isnull(ad_quan_prevista) then ad_quan_prevista=0

if al_prog_riga_doc_origine<=0 then setnull(al_prog_riga_doc_origine)

if as_cod_reparto="" or isnull(as_cod_reparto) then
	as_errore="Il campo cod_reparto è obbligatorio!"
     return -1
end if

setnull(ls_cod_deposito)

select cod_deposito
into :ls_cod_deposito
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:as_cod_reparto;

if sqlca.sqlcode<0 then
	as_errore="Errore in lettura deposito del reparto: "+sqlca.sqlerrtext
     return -1
	  
elseif sqlca.sqlcode=100 or ls_cod_deposito="" or isnull(ls_cod_deposito) then
	as_errore="Reparto "+as_cod_reparto+" non trovato in anagrafica o deposito non associato al reparto "+as_cod_reparto+" !"
     return -1
end if


if isnull(as_flag_elaborato) or (as_flag_elaborato<> 'N' and as_flag_elaborato<>'S') then
	as_flag_elaborato='N'
end if

insert into mov_mrp  
		( 	cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			data_rif_mrp,   
			cod_tipo_mov_mrp,   
			cod_prodotto,   
			quan_prevista,   
			quan_elaborata,   
			val_unitario,   
			tipo_doc_origine,   
			anno_doc_origine,   
			num_doc_origine,   
			prog_riga_doc_origine,   
			cod_deposito,   
			cod_reparto,   
			flag_elaborato,
			data_ora_reg)  
values ( :s_cs_xx.cod_azienda,
			:ai_anno_registrazione,
			:al_num_registrazione,
			:adt_data_rif_mrp,
			:as_cod_tipo_mrp,
			:as_cod_prodotto,
			:ad_quan_prevista,
			:ad_quan_elaborata,
			:ad_val_unitario,
			:as_tipo_doc_origine,
			:ai_anno_doc_origine,
			:al_num_doc_origine,
			:al_prog_riga_doc_origine,
			:ls_cod_deposito,
			:as_cod_reparto,
			:as_flag_elaborato,
			:ldt_now);

if sqlca.sqlcode<0 then
	as_errore="Errore in lettura parametro aziendale ESC: "+sqlca.sqlerrtext
     return -1
	  
end if

//OCIO il commit o il rollback va fatto fuori .....!!!!!


return 0
end function

public function integer uof_insert_data (datetime adt_data_rif_mrp, string as_cod_tipo_mrp, string as_cod_prodotto, decimal ad_quan_prevista, decimal ad_quan_elaborata, decimal ad_val_unitario, string as_tipo_doc_origine, integer ai_anno_doc_origine, long al_num_doc_origine, long al_prog_riga_doc_origine, string as_cod_reparto, string as_flag_elaborato, ref string as_errore);
integer				li_ret, li_anno_registrazione

long					ll_num_registrazione


li_ret = uof_insert_data(	adt_data_rif_mrp, as_cod_tipo_mrp, as_cod_prodotto, ad_quan_prevista, ad_quan_elaborata, &
									ad_val_unitario, as_tipo_doc_origine, ai_anno_doc_origine, al_num_doc_origine, al_prog_riga_doc_origine, &
									as_cod_reparto, as_flag_elaborato, &
									li_anno_registrazione, ll_num_registrazione, as_errore)

if li_ret<0 then
	//nella variabile as_errore è presente il messaggio
	return -1
end if

//OCIO:  commit e rollback vanno fatti fuori


return 0
end function

public function integer uof_insert_impegnato_prodotto (string as_cod_prodotto, string as_cod_tipo_movimento_mrp, ref string as_errore);string 		ls_sql, ls_cod_prodotto,ls_cod_reparto_sfuso, ls_flag_gen_commessa, ls_cod_versione, ls_null, ls_compare, ls_compare_old, ls_flag_mrp, ls_flag_tipo_det_ven
long 			ll_ret, ll_i,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ll_lead_time_cumulato, ll_y, ll_max
dec{4}		ld_quan_impegnata
datetime	ldt_data_pronto_sfuso, ldt_null
datastore 	lds_ordini
s_fabbisogno_commessa lstr_fabbisogni[], lstr_null[]
uo_mrp		luo_mrp

delete 	mov_mrp
where 	cod_prodotto = :as_cod_prodotto and
			cod_tipo_mov_mrp = :as_cod_tipo_movimento_mrp;
			
if sqlca.sqlcode < 0 then
	as_errore = "Errore in delete da tabella mov_mrp (uof_insert_impegnato_prodotto).~r~n" + sqlca.sqlerrtext
	return -1
end if

ls_sql = " select 	det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione, det_ord_ven.prog_riga_ord_ven, det_ord_ven.cod_prodotto, det_ord_ven.cod_reparto_sfuso, det_ord_ven.data_pronto_sfuso, det_ord_ven.flag_gen_commessa, ( isnull(det_ord_ven.quan_ordine,0) - isnull(det_ord_ven.quan_evasa,0) - isnull(det_ord_ven.quan_in_evasione,0) ), cod_versione, tab_tipi_det_ven.flag_tipo_det_ven " + &
			" from det_ord_ven " + &
			" join tab_tipi_det_ven 		on 	tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda and " + &
			"												tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven " + &
			" where 	det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
			"			det_ord_ven.cod_prodotto like '"+ as_cod_prodotto +"' and  " + &
			"			det_ord_ven.flag_blocco = 'N' and  " + &
			"			det_ord_ven.flag_evasione in ('A','P') and " + &
			"			det_ord_ven.flag_mrp ='N'  " + &
			" order by det_ord_ven.anno_registrazione,  " + &
			"			det_ord_ven.num_registrazione, " + &
			"			det_ord_ven.prog_riga_ord_ven "

ll_ret = guo_functions.uof_crea_datastore( lds_ordini, ls_sql )
if ll_ret < 0 then
	as_errore = "Errore nella creazione del datastore righe ordini (uof_insert_impegnato_prodotto)"
	return -1
end if

setnull(ls_null)
setnull(ldt_null)
ls_compare_old = ""

for ll_i = 1 to ll_ret
	
	ll_anno_registrazione = lds_ordini.getitemnumber(ll_i, 1)
	ll_num_registrazione = lds_ordini.getitemnumber(ll_i, 2)
	ll_prog_riga_ord_ven = lds_ordini.getitemnumber(ll_i, 3)
	
	
	ls_compare = string(ll_anno_registrazione,"0000") + string(ll_num_registrazione,"000000") + string(ll_prog_riga_ord_ven,"0000")
	if ls_compare = ls_compare_old then continue
	
	ls_compare_old = ls_compare
	
	ls_cod_prodotto = lds_ordini.getitemstring(ll_i, 4)
	ls_cod_reparto_sfuso = lds_ordini.getitemstring(ll_i, 5)
	ldt_data_pronto_sfuso = lds_ordini.getitemdatetime(ll_i, 6)
	ls_flag_gen_commessa = lds_ordini.getitemstring(ll_i, 7)
	ld_quan_impegnata = lds_ordini.getitemnumber(ll_i, 8)
	ls_cod_versione = lds_ordini.getitemstring(ll_i, 9)
	ls_flag_tipo_det_ven = lds_ordini.getitemstring(ll_i, 10)
	
	if ls_flag_tipo_det_ven <> "M" then continue
	
	if ld_quan_impegnata < 0 then ld_quan_impegnata = 0
	
	luo_mrp = create uo_mrp
	
	delete 	mov_mrp
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_doc_origine = :ll_anno_registrazione and
				num_doc_origine = :ll_num_registrazione and
				prog_riga_doc_origine = :ll_prog_riga_ord_ven and
				cod_tipo_mov_mrp = :as_cod_tipo_movimento_mrp  ;
				
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in delete da tabella mov_mrp (uof_insert_impegnato_prodotto).~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_gen_commessa = "S" then		// il prodotto va fatto su commessa; impegno gli elementi
		lstr_fabbisogni[] =  lstr_null[]
		
		luo_mrp.ib_data_fabbisogno_da_varianti = true
		if luo_mrp.uof_calcolo_fabbisogni_comm( false, "varianti_det_ord_ven",  ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ld_quan_impegnata, 0, ls_null, ldt_null, ll_lead_time_cumulato, lstr_fabbisogni[], as_errore) < 0 then
			as_errore = g_str.format( "Riga ordine $1/$2/$3. Errore in elaborazione fabbisogni prodotto (uof_insert_impegnato_prodotto).$4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,as_errore)
			iuo_log.error(as_errore)
			continue
		end if
		ll_max = upperbound( lstr_fabbisogni[] )
		
		for ll_y = 1 to ll_max
			
			if isnull(lstr_fabbisogni[ll_y].cod_reparto) or len(lstr_fabbisogni[ll_y].cod_reparto) < 1 then continue 
			
			select 	flag_mrp
			into		:ls_flag_mrp
			from 		anag_prodotti
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :lstr_fabbisogni[ll_y].cod_prodotto;
						
			if ls_flag_mrp = "N" or isnull(ls_flag_mrp) then continue
			
			if uof_insert_data( lstr_fabbisogni[ll_y].data_fabbisogno, as_cod_tipo_movimento_mrp, lstr_fabbisogni[ll_y].cod_prodotto, lstr_fabbisogni[ll_y].quan_fabbisogno, lstr_fabbisogni[ll_y].quan_fabbisogno, 0, "ORD_VEN", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, lstr_fabbisogni[ll_y].cod_reparto, "N", ref as_errore) < 0 then
				as_errore = g_str.format( "Riga ordine: $1/$2/$3 Materia Prima:$4. Errore in elaborazione fabbisogni prodotto (uof_insert_data). $5", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, lstr_fabbisogni[ll_y].cod_prodotto, as_errore)
				iuo_log.error(as_errore)
				continue
			end if
			
		next
		
		
		update det_ord_ven
		set flag_mrp = 'S'
		where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione =:ll_num_registrazione and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format( "Riga ordine: $1/$2/$3. Errore in update flag_mrp (uof_insert_impegnato_prodotto). $4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, sqlca.sqlerrtext)
			iuo_log.error(as_errore)
			continue
		end if

		
	else		// il prodotto è uno sfuso e quindi lo impegno nel suo reparto specifico

		ls_flag_mrp = ""
		
		select 	flag_mrp
		into		:ls_flag_mrp
		from 		anag_prodotti
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
					
		if ls_flag_mrp = "N" or isnull(ls_flag_mrp) then continue
		
		
		// ----------------- reparto del prodotto ---------------------------------- //
		
		//Donato 19/12/2011
		//verifica se esiste eccezione
		
		string ls_reparti_letti[],  ls_vuoto[], ls_cod_reparti_trovati[],  ls_errore
		
		select 	cod_reparto
		into   	:ls_cod_reparto_sfuso
		from   	anag_prodotti
		where  	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_prodotto;
			
		ls_reparti_letti[] = ls_vuoto[]
		ls_cod_reparti_trovati[] = ls_vuoto[]
		if f_reparti_stabilimenti(		ll_anno_registrazione, ll_num_registrazione, ls_cod_prodotto, "", 	ls_cod_reparto_sfuso, "", "", ls_reparti_letti[], ls_errore)<0 then
			as_errore = g_str.format( "Riga ordine: $1/$2/$3. Errore in ricerca reparto del prodotto $4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto)
			iuo_log.error(as_errore)
			continue	
		end if
	
		//merge con l'array by ref principale
		guo_functions.uof_merge_arrays(ls_cod_reparti_trovati[], ls_reparti_letti[])
		
		//###############################################################################
		//donato 14/03/2012 modifica per prevenire errori di configurazione prodotti in ordine sfuso (no-reparti)
		//dare messaggio all'utente e chiudere il cursore
		if upperbound(ls_cod_reparti_trovati[]) <= 0 then
			as_errore = g_str.format( "Riga ordine: $1/$2/$3. Nessun reparto trovato per il prodotto. $4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto)
			iuo_log.error(as_errore)
			continue	
		end if
		//###############################################################################
		
		
		//in ls_cod_reparti_trovati[] ci sono i reparti del prodotto sfuso
		//in realtà sarà sempre UNO solo
		
		//faccio questa porkeria qua per trovarmi bene poi più sotto con la funzione uof_inserisci_tes(...)
		//caso SFUSO: considero sempre e solo la prima componente del vettore reparti
		ls_cod_reparto_sfuso = ls_cod_reparti_trovati[1]
		ls_cod_reparti_trovati[] = ls_vuoto[]
		//ls_cod_reparti_trovati[1] = ls_cod_reparto_sfuso
		// ------------------------------------------------------------------------------ //
		
		if uof_insert_data( ldt_data_pronto_sfuso, as_cod_tipo_movimento_mrp, ls_cod_prodotto, ld_quan_impegnata, ld_quan_impegnata, 0, "ORD_VEN", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto_sfuso, "N", ref as_errore) < 0 then
			as_errore = g_str.format( "Riga ordine: $1/$2/$3 Prodotto Sfuso:$4. Errore in elaborazione fabbisogni prodotto (uof_insert_data)", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto)
			iuo_log.error(as_errore)
			continue
		end if
		
		update 	det_ord_ven
		set		flag_mrp = 'S'
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione =:ll_num_registrazione and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format( "Riga ordine: $1/$2/$3. Errore in update flag_mrp (uof_insert_impegnato_prodotto). $4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, sqlca.sqlerrtext)
			iuo_log.error(as_errore)
			continue
		end if
		

	end if
	
	commit;
	destroy luo_mrp
	
	iuo_log.info( g_str.format( "Riga ordine: $1/$2/$3. Elaborata correttamente.", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven) )
	
next

destroy lds_ordini

return 0
end function

public function integer uof_get_giacenza_mrp (string as_cod_prodotto, ref decimal ad_giacenza, ref datetime adt_data_rif_giacenza, ref string as_errore);long			ll_anno_registrazione, ll_num_registrazione
dec{4}		ld_quan_ultimo_inventario,ld_quan_scarico,ld_quan_carico
datetime 	ldt_data_riferimento

select 	mov_mrp.anno_registrazione,mov_mrp.num_registrazione, mov_mrp.data_rif_mrp, mov_mrp.quan_prevista
into 		:ll_anno_registrazione, :ll_num_registrazione, :ldt_data_riferimento, :ld_quan_ultimo_inventario
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '='
having	mov_mrp.data_rif_mrp = max(mov_mrp.data_rif_mrp);

if sqlca.sqlcode = 100 then
	ld_quan_ultimo_inventario = 0
	ll_anno_registrazione  = 0
	ll_num_registrazione = 0
end if

if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo giacenza (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if

select 	sum(isnull(mov_mrp.quan_prevista,0))
into 		:ld_quan_carico
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.anno_registrazione >= :ll_anno_registrazione and
			mov_mrp.num_registrazione >= :ll_num_registrazione and
			mov_mrp.data_rif_mrp >= :ldt_data_riferimento and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '+'  ;
if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo carico (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_quan_carico) then ld_quan_carico = 0


select 	sum(isnull(mov_mrp.quan_prevista,0))
into 		:ld_quan_scarico
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.anno_registrazione >= :ll_anno_registrazione and
			mov_mrp.num_registrazione >= :ll_num_registrazione and
			mov_mrp.data_rif_mrp >= :ldt_data_riferimento and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '-'  ;
if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo scarico (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if
if isnull(ld_quan_scarico) then ld_quan_scarico = 0

ad_giacenza = ld_quan_ultimo_inventario + ld_quan_carico - ld_quan_scarico
adt_data_rif_giacenza = datetime(today(), 00:00:00)

return 0
end function

public function integer uof_mrp_scheduler (string as_cod_prodotto, ref string as_errore);boolean		lb_primo = true, lb_found
string		ls_errore, ls_sql, ls_cod_reparto, ls_cod_deposito, ls_flag_giacenza_totale
long			ll_max=0, ll_rows, ll_i, ll_ind, ll_gg_ins_previsione, ll_num_gg_mrp
dec{4}		ld_giacenza
decimal		ld_media_consumi
datetime	ldt_data_rif_giacenza, ldt_data_inizio, ldt_now, ldt_data_limite
datastore	lds_data
str_scheduler_mrp	lstr_reparto[]



select	num_gg_ins_previsione, 
			num_gg_mrp,
			flag_giacenza_totale
into 		:ll_gg_ins_previsione,
			:ll_num_gg_mrp,
			:ls_flag_giacenza_totale
from 		con_mrp  
where 	cod_azienda = :s_cs_xx.cod_azienda   ;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in select parametri da tabella Parametri MRP (con_mrp)"
	return -1
end if

if isnull(ls_flag_giacenza_totale) then ls_flag_giacenza_totale = "N"

if ll_num_gg_mrp > 0 and not isnull(ll_num_gg_mrp) then ii_num_giorni_mrp = ll_num_gg_mrp
	
ldt_data_inizio = datetime( date("01/01/1900"),00:00:00)
ldt_now			  = datetime( today(), now() )	
ldt_data_limite = datetime( relativedate( today(), ii_num_giorni_mrp), 00:00:00)

delete 	mrp_schedule
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;
if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore in cancellazione schedulazione prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, sqlca.sqlerrtext)
	return -1
end if


delete 	mrp_schedule_day
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;
if sqlca.sqlcode < 0 then
	as_errore = g_str.format("Errore in cancellazione schedulazione prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, sqlca.sqlerrtext)
	return -1
end if

ls_sql = "select cod_reparto from anag_reparti where cod_azienda = '"+s_cs_xx.cod_azienda + "' and flag_mrp='S' "
ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)

if ls_flag_giacenza_totale = "S" then
	ldt_data_rif_giacenza = datetime(today(), 00:00:00)
	if uof_giacenza_prodotto( ldt_data_rif_giacenza, as_cod_prodotto, "%", ld_giacenza, ls_errore ) < 0 then
		as_errore = g_str.format("Errore in calcolo giacenza magazzino mrp prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
		return -1
	end if
end if


for ll_i = 1 to ll_rows
	
	lstr_reparto[ll_i].cod_reparto = lds_data.getitemstring(ll_i,1)
	
	if ls_flag_giacenza_totale = "N" then
		if uof_get_giacenza_reparto_mrp(as_cod_prodotto, lstr_reparto[ll_i].cod_reparto, ld_giacenza, ldt_data_rif_giacenza, ls_errore) < 0 then
			as_errore = g_str.format("Errore in calcolo giacenza magazzino mrp prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
			return -1
		end if
	end if
			
	lstr_reparto[ll_i].giacenza = ld_giacenza
	
	if ls_flag_giacenza_totale = "S" then EXIT
	
next

destroy lds_data


// Carico eventuali dati previsionali sui consumi		
ls_sql = " select 	distinct cod_reparto " + &
			" from mov_mrp " + &
			" where	mov_mrp.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" mov_mrp.cod_prodotto = '" + as_cod_prodotto +"' "
			
ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)

for ll_i = 1 to ll_rows
	
	ls_cod_reparto = lds_data.getitemstring(ll_i,1)
	if isnull(ls_cod_reparto) then continue
	
	// calcolo media consumi (ossia previsione impegnato)
	if uof_media_consumi(as_cod_prodotto, ls_cod_reparto, ld_media_consumi, ls_errore) < 0 then
		as_errore = g_str.format("Errore in media consumi per prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
		return -1
	end if
	
	if ld_media_consumi <= 0 then continue

	lb_found=false
	for ll_ind = 1 to upperbound( lstr_reparto )
		if lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto then
			lstr_reparto[ll_ind].previsione_impegnato = ld_media_consumi
			lb_found=true
			exit
		end if
	next
	
	if not lb_found then
		lstr_reparto[ll_ind].cod_reparto = lds_data.getitemstring(ll_i,1)
		if uof_get_giacenza_reparto_mrp(as_cod_prodotto, lstr_reparto[ll_ind].cod_reparto, ld_giacenza, ldt_data_rif_giacenza, ls_errore) < 0 then
			as_errore = g_str.format("Errore in calcolo giacenza magazzino mrp prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
			return -1
		end if
		
		ll_ind =  upperbound( lstr_reparto ) + 1
		lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto
		lstr_reparto[ll_ind].giacenza = ld_giacenza
		lstr_reparto[ll_ind].impegnato = 0
		lstr_reparto[ll_ind].previsione_impegnato = ld_media_consumi
		
		select cod_deposito
		into		:ls_cod_deposito
		from		anag_reparti
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_reparto = :lstr_reparto[ll_ind].cod_reparto;
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format("Errore inricerca deposito del reparto  $1(uof_mrp_scheduler). $2", lstr_reparto[ll_ind].cod_reparto, sqlca.sqlerrtext)
			return -1
		end if
		lstr_reparto[ll_ind].cod_deposito = ls_cod_deposito
	end if
	
next

destroy lds_data
	
	
do while true
	// Totalizzo impegnato per reparto
	ls_sql = " select 	cod_reparto, sum(isnull(mov_mrp.quan_prevista,0)) " + &
				" from mov_mrp 	join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp " + &
				" where	mov_mrp.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" mov_mrp.cod_prodotto = '" + as_cod_prodotto +"'  and  "+&
				" mov_mrp.data_rif_mrp between '" + string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_rif_giacenza,s_cs_xx.db_funzioni.formato_data) + "' and " + &
				" tab_tipi_mov_mrp.flag_tipo_mov_mrp = 'I'  " + &
				" group by cod_reparto "
				
	ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	
	for ll_i = 1 to ll_rows
		
		ls_cod_reparto = lds_data.getitemstring(ll_i,1)
		if isnull(ls_cod_reparto) then continue
		
		lb_found=false
		for ll_ind = 1 to upperbound( lstr_reparto )
			if lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto then
				lstr_reparto[ll_ind].impegnato = lds_data.getitemnumber(ll_i,2)
				lb_found=true
				exit
			end if
		next
		
		if not lb_found then
			lstr_reparto[ll_ind].cod_reparto = lds_data.getitemstring(ll_i,1)
			if uof_get_giacenza_reparto_mrp(as_cod_prodotto, lstr_reparto[ll_ind].cod_reparto, ld_giacenza, ldt_data_rif_giacenza, ls_errore) < 0 then
				as_errore = g_str.format("Errore in calcolo giacenza magazzino mrp prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
				return -1
			end if
			
			ll_ind =  upperbound( lstr_reparto ) + 1
			lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto
			lstr_reparto[ll_ind].giacenza = ld_giacenza
			lstr_reparto[ll_ind].impegnato = lds_data.getitemnumber(ll_i,2)
			
			select cod_deposito
			into		:ls_cod_deposito
			from		anag_reparti
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_reparto = :lstr_reparto[ll_ind].cod_reparto;
			if sqlca.sqlcode < 0 then
				as_errore = g_str.format("Errore inricerca deposito del reparto  $1(uof_mrp_scheduler). $2", lstr_reparto[ll_ind].cod_reparto, sqlca.sqlerrtext)
				return -1
			end if
			lstr_reparto[ll_ind].cod_deposito = ls_cod_deposito
		end if
		
	next
	
	destroy lds_data

	
	// totalizzo ordinato per reparto
	ls_sql = " select 	cod_reparto, sum(isnull(mov_mrp.quan_prevista,0)) " + &
				" from mov_mrp 	join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp " + &
				" where	mov_mrp.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" mov_mrp.cod_prodotto = '" + as_cod_prodotto +"'  and  "+&
				" mov_mrp.data_rif_mrp between '" + string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "' and '" + string(ldt_data_rif_giacenza,s_cs_xx.db_funzioni.formato_data) + "' and " + &
				" tab_tipi_mov_mrp.flag_tipo_mov_mrp = 'O'  " + &
				" group by cod_reparto "
				
	ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	
	for ll_i = 1 to ll_rows
		
		ls_cod_reparto = lds_data.getitemstring(ll_i,1)
		if isnull(ls_cod_reparto) then continue
		
		lb_found=false
		for ll_ind = 1 to upperbound( lstr_reparto )
			if lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto then
				lstr_reparto[ll_ind].ordinato = lds_data.getitemnumber(ll_i,2)
				lb_found=true
				exit
			end if
		next
		
		if not lb_found then
			lstr_reparto[ll_ind].cod_reparto = lds_data.getitemstring(ll_i,1)
			if uof_get_giacenza_reparto_mrp(as_cod_prodotto, lstr_reparto[ll_ind].cod_reparto, ld_giacenza, ldt_data_rif_giacenza, ls_errore) < 0 then
				as_errore = g_str.format("Errore in calcolo giacenza magazzino mrp prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, ls_errore)
				return -1
			end if
			
			ll_ind =  upperbound( lstr_reparto ) + 1
			lstr_reparto[ll_ind].cod_reparto = ls_cod_reparto
			lstr_reparto[ll_ind].giacenza = ld_giacenza
			lstr_reparto[ll_ind].impegnato = lds_data.getitemnumber(ll_i,2)
			
			select cod_deposito
			into		:ls_cod_deposito
			from		anag_reparti
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_reparto = :lstr_reparto[ll_ind].cod_reparto;
			if sqlca.sqlcode < 0 then
				as_errore = g_str.format("Errore inricerca deposito del reparto  $1(uof_mrp_scheduler). $2", lstr_reparto[ll_ind].cod_reparto, sqlca.sqlerrtext)
				return -1
			end if
			lstr_reparto[ll_ind].cod_deposito = ls_cod_deposito
		end if
		
	next
	
	destroy lds_data

	
	if lb_primo then
		
		insert into mrp_schedule
				(cod_azienda,
				cod_prodotto,
				data_ora_reg)
		values
				(:s_cs_xx.cod_azienda,
				:as_cod_prodotto,
				:ldt_now);
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format("Errore in inserimento in tabella mrp_schedule col prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, sqlca.sqlerrtext)
			return -1
		end if
		lb_primo=false
		
	end if			
	
	for ll_ind = 1 to upperbound( lstr_reparto )
		
		// inserisco la previsione Impegnato se necessario
		if relativedate( today(), ll_gg_ins_previsione) < date(ldt_data_rif_giacenza) then
			if lstr_reparto[ll_ind].impegnato < lstr_reparto[ll_ind].previsione_impegnato then
				lstr_reparto[ll_ind].impegnato = lstr_reparto[ll_ind].previsione_impegnato
			end if
		end if
		
		
		lstr_reparto[ll_ind].giacenza = round(lstr_reparto[ll_ind].giacenza ,4)
		lstr_reparto[ll_ind].impegnato =  round(lstr_reparto[ll_ind].impegnato ,4)
		lstr_reparto[ll_ind].ordinato = round(lstr_reparto[ll_ind].ordinato ,4)
		
		ll_max ++
		insert into mrp_schedule_day  
				( cod_azienda,   
				  cod_prodotto,   
				  progressivo,   
				  data_riferimento,
				  giacenza,   
				  impegnato,   
				  ordinato,
				  cod_reparto,
				  cod_deposito)  
		values ( :s_cs_xx.cod_azienda,
					:as_cod_prodotto,  
					:ll_max,   
					:ldt_data_rif_giacenza,
					:lstr_reparto[ll_ind].giacenza,   
					:lstr_reparto[ll_ind].impegnato,   
					:lstr_reparto[ll_ind].ordinato,
					:lstr_reparto[ll_ind].cod_reparto,
					:ls_cod_deposito)  ;
	
		if sqlca.sqlcode < 0 then
			as_errore = g_str.format("Errore in inserimento in tabella mrp_schedule_day col prodotto $1(uof_mrp_schedule). $2", as_cod_prodotto, sqlca.sqlerrtext)
			return -1
		end if
	
		lstr_reparto[ll_ind].giacenza =lstr_reparto[ll_ind].giacenza  + lstr_reparto[ll_ind].ordinato - lstr_reparto[ll_ind].impegnato
		lstr_reparto[ll_ind].impegnato = 0
		lstr_reparto[ll_ind].ordinato = 0
	next
	
	ldt_data_rif_giacenza = datetime( relativedate( date(ldt_data_rif_giacenza),1), 00:00:00)
	ldt_data_inizio = ldt_data_rif_giacenza
	if ldt_data_rif_giacenza > ldt_data_limite  then exit
			
loop


return 0
end function

public function integer uof_get_giacenza_reparto_mrp (string as_cod_prodotto, string as_cod_reparto, ref decimal ad_giacenza, ref datetime adt_data_rif_giacenza, ref string as_errore);long			ll_anno_registrazione, ll_num_registrazione
dec{4}		ld_quan_ultimo_inventario,ld_quan_scarico,ld_quan_carico
datetime 	ldt_data_riferimento

select 	mov_mrp.anno_registrazione,mov_mrp.num_registrazione, mov_mrp.data_rif_mrp, mov_mrp.quan_prevista
into 		:ll_anno_registrazione, :ll_num_registrazione, :ldt_data_riferimento, :ld_quan_ultimo_inventario
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			mov_mrp.cod_reparto = :as_cod_reparto and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '='
group by mov_mrp.anno_registrazione,mov_mrp.num_registrazione, mov_mrp.data_rif_mrp, mov_mrp.quan_prevista
having	mov_mrp.data_rif_mrp = max(mov_mrp.data_rif_mrp) and mov_mrp.num_registrazione = max(mov_mrp.num_registrazione);

if sqlca.sqlcode = 100 then
	ld_quan_ultimo_inventario = 0
	ll_anno_registrazione  = 0
	ll_num_registrazione = 0
end if

if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo giacenza (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if

select 	sum(isnull(mov_mrp.quan_prevista,0))
into 		:ld_quan_carico
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.anno_registrazione >= :ll_anno_registrazione and
			mov_mrp.num_registrazione >= :ll_num_registrazione and
			mov_mrp.data_rif_mrp >= :ldt_data_riferimento and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			mov_mrp.cod_reparto = :as_cod_reparto and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '+'  ;
if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo carico (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_quan_carico) then ld_quan_carico = 0


select 	sum(isnull(mov_mrp.quan_prevista,0))
into 		:ld_quan_scarico
from		mov_mrp 
			join tab_tipi_mov_mrp on mov_mrp.cod_azienda = tab_tipi_mov_mrp.cod_azienda and
												mov_mrp.cod_tipo_mov_mrp = tab_tipi_mov_mrp.cod_tipo_mov_mrp
where 	mov_mrp.cod_azienda = :s_cs_xx.cod_azienda and
			mov_mrp.anno_registrazione >= :ll_anno_registrazione and
			mov_mrp.num_registrazione >= :ll_num_registrazione and
			mov_mrp.data_rif_mrp >= :ldt_data_riferimento and
			mov_mrp.cod_prodotto = :as_cod_prodotto  and
			mov_mrp.cod_reparto = :as_cod_reparto and
			tab_tipi_mov_mrp.flag_tipo_mov_mrp = '-'  ;
if sqlca.sqlcode < 0 then
	as_errore = "Errore in calcolo scarico (uof_get_giacenza_mrp). " + sqlca.sqlerrtext
	return -1
end if
if isnull(ld_quan_scarico) then ld_quan_scarico = 0

ad_giacenza = ld_quan_ultimo_inventario + ld_quan_carico - ld_quan_scarico
adt_data_rif_giacenza = datetime(today(), 00:00:00)

return 0
end function

public function integer uof_insert_ordinato_prodotto (string as_cod_prodotto, string as_cod_tipo_movimento_mrp, ref string as_errore);string 		ls_sql, ls_cod_prodotto, ls_null,  ls_flag_mrp, ls_flag_tipo_det_acq, ls_cod_reparto, ls_flag_mrp_reparto, ls_flag_mrp_prodotto
long 			ll_ret, ll_i,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_acq, ll_y, ll_max
dec{4}		ld_quan_ordinata
datetime	ldt_data_consegna, ldt_null
datastore 	lds_ordini

delete 	mov_mrp
where 	cod_prodotto = :as_cod_prodotto and
			cod_tipo_mov_mrp = :as_cod_tipo_movimento_mrp;
			
if sqlca.sqlcode < 0 then
	as_errore = "Errore in delete da tabella mov_mrp (uof_insert_ordinato_prodotto).~r~n" + sqlca.sqlerrtext
	return -1
end if

ls_sql = " select 	det_ord_acq.anno_registrazione, det_ord_acq.num_registrazione, det_ord_acq.prog_riga_ordine_acq, det_ord_acq.cod_prodotto, det_ord_acq.data_consegna, ( isnull(det_ord_acq.quan_ordinata,0) - isnull(det_ord_acq.quan_arrivata,0) ), tab_tipi_det_acq.flag_tipo_det_acq, det_ord_acq.cod_reparto, anag_prodotti.flag_mrp " + &
			" from det_ord_acq " + &
			" join tab_tipi_det_acq 		on 	tab_tipi_det_acq.cod_azienda = det_ord_acq.cod_azienda and " + &
			"												tab_tipi_det_acq.cod_tipo_det_acq = det_ord_acq.cod_tipo_det_acq " + &
			" join anag_prodotti			on 	anag_prodotti.cod_azienda = det_ord_acq.cod_azienda and " + &
			"												anag_prodotti.cod_prodotto = det_ord_acq.cod_prodotto " + &
			" where det_ord_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
			"			det_ord_acq.cod_prodotto like '"+ as_cod_prodotto +"' and  " + &
			"			det_ord_acq.flag_blocco = 'N' and  " + &
			"			det_ord_acq.flag_saldo = 'N' and " + &
			"			det_ord_acq.flag_mrp ='N'  " + &
			" order by det_ord_acq.anno_registrazione,  " + &
			"			det_ord_acq.num_registrazione, " + &
			"			det_ord_acq.prog_riga_ordine_acq "

ll_ret = guo_functions.uof_crea_datastore( lds_ordini, ls_sql )
if ll_ret < 0 then
	as_errore = "Errore nella creazione del datastore righe ordini (uof_insert_ordinato_prodotto)"
	iuo_log.error(as_errore)
	return -1
end if

setnull(ls_null)
setnull(ldt_null)

for ll_i = 1 to ll_ret
	
	ll_anno_registrazione = lds_ordini.getitemnumber(ll_i, 1)
	ll_num_registrazione = lds_ordini.getitemnumber(ll_i, 2)
	ll_prog_riga_ord_acq = lds_ordini.getitemnumber(ll_i, 3)
	
	ls_cod_prodotto = lds_ordini.getitemstring(ll_i, 4)
	ldt_data_consegna = lds_ordini.getitemdatetime(ll_i, 5)
	ld_quan_ordinata = lds_ordini.getitemnumber(ll_i, 6)
	ls_flag_tipo_det_acq = lds_ordini.getitemstring(ll_i, 7)
	ls_cod_reparto =  lds_ordini.getitemstring(ll_i, 8)
	ls_flag_mrp_prodotto =  lds_ordini.getitemstring(ll_i, 9)
	
	if ls_flag_mrp_prodotto = "N" or isnull(ls_flag_mrp_prodotto) then continue
	
	if isnull(ls_cod_reparto) then
		iuo_log.error( g_str.format( "Riga ordine acquisto: $1/$2/$3 Il prodotto $4 è soggetto a MRP, ma alla riga d'ordine non è stato associato alcun reparto (uof_insert_ordinato_prodotto)", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, ls_cod_prodotto))
		continue
	end if
	
	select 	flag_mrp
	into		:ls_flag_mrp_reparto
	from 		anag_reparti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_reparto = :ls_cod_reparto;
				
	if ls_flag_mrp_reparto = "N" then continue

	if ls_flag_tipo_det_acq <> "M" then continue
	
	if ld_quan_ordinata < 0 then ld_quan_ordinata = 0
	
	
	delete 	mov_mrp
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_doc_origine = :ll_anno_registrazione and
				num_doc_origine = :ll_num_registrazione and
				prog_riga_doc_origine = :ll_prog_riga_ord_acq and
				cod_tipo_mov_mrp = :as_cod_tipo_movimento_mrp  ;
				
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in delete da tabella mov_mrp (uof_insert_ordinato_prodotto).~r~n" + sqlca.sqlerrtext
		iuo_log.error( g_str.format( "Riga ordine acquisto: $1/$2/$3 Prodotto $4. $5 (uof_insert_ordinato_prodotto) ", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, ls_cod_prodotto, as_errore))
		return -1
	end if
	

	ls_flag_mrp = ""
	
	select 	flag_mrp
	into		:ls_flag_mrp
	from 		anag_prodotti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
				
	if ls_flag_mrp = "N" or isnull(ls_flag_mrp) then continue
	
	if uof_insert_data( ldt_data_consegna, as_cod_tipo_movimento_mrp, ls_cod_prodotto, ld_quan_ordinata, ld_quan_ordinata, 0, "ORD_ACQ", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, ls_cod_reparto, "N", ref as_errore) < 0 then
		as_errore = g_str.format( "Riga ordine acquisto: $1/$2/$3 Prodotto:$4. Errore in elaborazione ordini prodotto (uof_insert_data)", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, ls_cod_prodotto)
		iuo_log.error(as_errore)
		continue
	end if
	
	update 	det_ord_acq
	set		flag_mrp = 'S'
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione =:ll_num_registrazione and
				prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
	if sqlca.sqlcode < 0 then
		as_errore = g_str.format( "Riga ordine acquisto: $1/$2/$3. Errore in update flag_mrp su tabella det_ord_acq(uof_insert_ordinato_prodotto). $4", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, sqlca.sqlerrtext)
		iuo_log.error(as_errore)
		continue
	end if

	commit;
	
	iuo_log.info( g_str.format( "Riga ordine acquisto: $1/$2/$3. Elaborata correttamente.", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq) )
	
next

destroy lds_ordini

return 0
end function

public function integer uof_media_consumi (string as_cod_prodotto, string as_cod_reparto, ref decimal ad_media_consumi, ref string as_errore);// funzione di calcolo consumo per inserimento previsioni
// EnMe 01.08.2013
long ll_num_gg_calcolo_previsione, ll_ret, ll_gg_ins_previsione, ll_num_gg
dec{4} ld_correttore_stagionale, ld_quan_scarico_day, ld_sum_scarico
date ld_giorno_previsione, ld_data_ins_previsione
datetime ldt_data_rif_mrp


SELECT	 num_gg_calcolo_previsione,   
			val_correzione_stagionale,   
			num_gg_ins_previsione  
 INTO 	:ll_num_gg_calcolo_previsione,   
			:ld_correttore_stagionale,   
			:ll_gg_ins_previsione  
 FROM 	con_mrp  
WHERE 	cod_azienda = :s_cs_xx.cod_azienda   ;

if sqlca.sqlcode <> 0 then
	as_errore = "Errore in select parametri da tabella Parametri MRP (con_mrp)"
	return -1
end if

if ll_num_gg_calcolo_previsione =0 or isnull(ll_num_gg_calcolo_previsione) then
	// nessun previsione
	ad_media_consumi = 0
	return 0
	//as_errore = "Errore: il num giorni calcolo previsione è ZERO; impostare un valore nella tabella parametri MRP."
	//return -1
end if

if ld_correttore_stagionale = 0 or isnull(ld_correttore_stagionale) then
	ld_correttore_stagionale = 1
end if

// ------------------------------------------------------------------------------------------------------------ //
// eseguo il calcolo del consumo medio

ld_giorno_previsione = today()
ll_num_gg =  abs(ll_num_gg_calcolo_previsione)
do while true
	
	if ll_num_gg_calcolo_previsione < 0 then // negativo, quindi vado ad esaminare il passato, ovvero gli scarichi
	
		ld_giorno_previsione = relativedate(ld_giorno_previsione, -1)   // cerco gg precedente
		
		select 	sum(quan_elaborata)
		into		:ld_quan_scarico_day
		from  	mov_mrp
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :as_cod_prodotto and
					data_rif_mrp = :ldt_data_rif_mrp and
					cod_reparto = :as_cod_reparto and
					cod_tipo_mov_mrp in ( select cod_tipo_mov_mrp
													 from tab_tipi_mov_mrp
													 where flag_tipo_mov_mrp = '-' );
													 
	else		// vado a fare una previsione sulla base dell'impegnato futuro
	
		ld_giorno_previsione = relativedate(ld_giorno_previsione, 1)   // passo a gg successivo
		
		select 	sum(quan_elaborata)
		into		:ld_quan_scarico_day
		from  	mov_mrp
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :as_cod_prodotto and
					data_rif_mrp = :ldt_data_rif_mrp and
					cod_reparto = :as_cod_reparto and
					cod_tipo_mov_mrp in ( select cod_tipo_mov_mrp
													 from tab_tipi_mov_mrp
													 where flag_tipo_mov_mrp = 'I' );
	end if					
					
					
	choose case daynumber(ld_giorno_previsione)
		case 1 		// se domenica salto
			continue
			
		case 7		// se è sabato lo considero solo se ci sono scarichi
			
			ldt_data_rif_mrp = datetime( ld_giorno_previsione, time("00:00:00:000") )
			if ld_quan_scarico_day > 0 then ll_num_gg --
						
		case else	// tutti gli altri giorni
			
			ldt_data_rif_mrp = datetime( ld_giorno_previsione, time("00:00:00:000") )
			ll_num_gg --
			
	end choose
	
	
	if isnull(ld_quan_scarico_day) then ld_quan_scarico_day = 0
	
	ld_sum_scarico += ld_quan_scarico_day
	
	if ll_num_gg = 0 then exit
	
loop

ad_media_consumi = round( ( ld_sum_scarico / abs(ll_num_gg_calcolo_previsione)) * ld_correttore_stagionale, 4)


return 0

/*		spiegazione della procedura di calcolo previsione EnMe 26/7/2013

Calcolo Previsione.
Si consideri la variabile X come il numero dei giorni per determinare le date per calcolo consumi o previsioni; se X è negativo si tratta di andare indietro nella linea del tempo, de X è positivo andiamo avanti nella linea del tempo. 
Si consideri Y come coefficiente correttore stagionale (fra 0,75 – 1.25)
Si consideri la variabile P1 ottenuta secondo la seguente formula:
Se X < 0 allora P1 = [Media degli scarichi degli ultimi X giorni non festivi (il sabato lo considero solo se ci sono scarichi)] * Y 
Se X > 0 allora P1 = [Media dell’impegnato dei prossimi X giorni (no sabato e domenica)] * Y
Si consideri il parametro Z1 come il numero di giorni dopo la data odierna, dopo i quali lo scheduler  deve eseguire anche l’inserimento della previsione impegnato.
Si consideri il parametro D1 come la data effettiva (calcolata come oggi + Z1 giorni) dopo la quale  lo scheduler  deve eseguire anche l’inserimento della previsione impegnato.
Si consideri D2 come la data che lo schedulatore prende in considerazione volta per volta durante l’elaborazione dei valori giornalieri.
Si consideri I1 come l’impegnato puntuale del giorno.

lo schedulatore opererà in questo modo per calcolare inserire il valore impegnato:
Se D1 > D2 allora controllo se P1 > I1; se è vero allora tengo P1, altrimenti mi tengo I1
Se D1 < D2 allora tengo I1

*/
end function

public function integer uof_exec_scheduler (string as_cod_prodotto);string 		ls_sql, ls_errore
long 			ll_ret, ll_i, ll_err
datastore	lds_data

iuo_log.info( " ")
iuo_log.info( "****************************************************")
iuo_log.info( "Avviamento Pianificato Scheduler M.R.P.")
iuo_log.info( "Copyright Consulting&Software (TM) - CSteam")

ls_sql = " SELECT distinct mov_mrp.cod_prodotto, anag_prodotti.des_prodotto from mov_mrp " + &
			" join anag_prodotti on anag_prodotti.cod_azienda = mov_mrp.cod_azienda and anag_prodotti.cod_prodotto = mov_mrp.cod_prodotto " + &
			" where mov_mrp.cod_azienda = '" + s_cs_xx.cod_azienda + "' and mov_mrp.cod_prodotto like '" + as_cod_prodotto + "' order by mov_mrp.cod_prodotto "

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql)

choose case  ll_ret
	case 0 
		iuo_log.error( "Nulla da elaborare.")
		return 0

	case is < 0
		iuo_log.error( "Errore durante la creazione del datastore dei prodotti da elaborare con MRP. Elaborazione bloccata!")
		return -1
end choose

for ll_i = 1 to ll_ret
	
	if uof_insert_impegnato_prodotto( lds_data.getitemstring(ll_i,1), "IMP", ref ls_errore) < 0 then
		iuo_log.error( "Prodotto saltato:" + lds_data.getitemstring(ll_i,1) + " errore in elaborazione IMPEGNATO: " + ls_errore)
		rollback;
		continue
	end if
	
	if uof_insert_ordinato_prodotto( lds_data.getitemstring(ll_i,1), "ORD", ref ls_errore) < 0 then
		iuo_log.error( "Prodotto saltato:" + lds_data.getitemstring(ll_i,1) + " errore in elaborazione ORDINATO: " + ls_errore)
		rollback;
		continue
	end if

	if uof_mrp_scheduler( lds_data.getitemstring(ll_i,1) ,ref ls_errore) < 0 then
		iuo_log.error( "Prodotto saltato:" + lds_data.getitemstring(ll_i,1) + " errore SCHEDULER: " + ls_errore)
		rollback;
		continue
	end if
	
	commit;
	iuo_log.error( "MRP Prodotto:" + lds_data.getitemstring(ll_i,1) + " elaborato correttamente ")

next

destroy lds_data
return 0
end function

public function integer uof_giacenza_prodotto (datetime adt_data_riferimento, string as_cod_prodotto, string as_cod_deposito, ref decimal ad_giacenza, ref string as_errore);string ls_where, ls_chiave[]
long ll_ret
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_costo_prodotto, ld_costo_medio_continuo_stock[]

uo_magazzino luo_mag
	/**
	 * ld_quant_val : 
	 * [1] =quan_inizio_anno
	 * [2] =val_inizio_anno,  
	 * [3] =quan_ultima_chius, 
	 * [4] =qta_entrata,
	 * [5] =val_entrata,
	 * [6] =qta_uscita, 
	 * [7] =val_uscita, 
	 * [8] =qta_acq,
	 * [9] =val_acq,
	 * [10]=qta_ven,
	 * [11]=val_ven
	 **/
luo_mag = create uo_magazzino
ll_ret = luo_mag.uof_saldo_prod_date_decimal(as_cod_prodotto, adt_data_riferimento, ls_where, ld_quant_val, as_errore, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
if ll_ret < 0 then
	destroy luo_mag
	return -1
end if

ad_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
destroy luo_mag

return 0
end function

on uo_service_mov_mrp.create
call super::create
end on

on uo_service_mov_mrp.destroy
call super::destroy
end on

