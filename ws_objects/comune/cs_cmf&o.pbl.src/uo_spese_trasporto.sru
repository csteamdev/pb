﻿$PBExportHeader$uo_spese_trasporto.sru
forward
global type uo_spese_trasporto from nonvisualobject
end type
end forward

global type uo_spese_trasporto from nonvisualobject
end type
global uo_spese_trasporto uo_spese_trasporto

type variables
private:
	boolean ib_log
	boolean ib_enabled
	uo_log_sistema iuo_log
end variables

forward prototypes
private function long uof_cerca_id_spesa (string as_cod_cliente, string as_cod_prodotto, ref string as_message)
public function integer uof_calcola_spese (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message)
public function integer uof_imposta_spese_ord_ven (integer al_anno, long al_numero, long al_prog_riga, boolean ab_copia_in_riferite, ref string as_message)
public subroutine uof_storico_data_riferimento (ref date adt_data_riferimento, ref integer ai_giorni_riferimento)
private function integer uof_cancella_righe_spese_trasporto (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message)
public function integer uof_azzera_spese_ordine (integer al_anno, long al_numero, string as_flag_azzera_spese_trasp, ref string as_message)
public function integer uof_apri_storico (integer al_anno_documento, long al_numero_documento, string as_tipo_documento)
public function integer uof_azzera_trasporto_documento (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message)
private function integer uof_inserisci_spesa_fat_ven (integer al_anno, long al_numero, decimal ad_imponibile, string as_tipo_dettaglio, string as_cod_prodotto, string as_cod_iva, decimal ad_imponibile_solo_perc, ref string as_message)
private function integer uof_inserisci_spesa_bol_ven (integer al_anno, long al_numero, decimal ad_imponibile, string as_tipo_dettaglio, string as_cod_prodotto, string as_cod_iva, decimal ad_imponibile_solo_perc, ref string as_message)
public function integer uof_totale_spese_trasporto (integer al_anno, long al_numero, string as_tipo_documento, ref decimal ad_totale_spese, ref decimal ad_totale_spese_solo_perc, ref string as_message)
public function integer uof_imposta_trasporto_solo_percentuale (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message)
public function integer uof_check_per_ricalcolo (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message)
public subroutine uof_log (string as_message)
end prototypes

private function long uof_cerca_id_spesa (string as_cod_cliente, string as_cod_prodotto, ref string as_message);/**
 * stefanop
 * 19/02/2014
 *
 * Recupera l'id della riga di spesa.
 * Prima viene data priorità al cliente e poi viene cercata la riga senza cliente
 *
 * @return
 *		> 0, id della riga
 *		0, riga non trovata
 *		-1, errore
 **/
 
string ls_cod_zona, ls_cod_livello_prod[], ls_sql
int li_livello, li_sub_livello, li_ret
long ll_ret_val
datastore lds_store

setnull(as_message)
if isnull(as_cod_cliente) then as_cod_cliente = ""
if isnull(as_cod_prodotto) then as_cod_prodotto = ""

select cod_zona
into :ls_cod_zona
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :as_cod_cliente;
		 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante la lettura del codice zona del cliente " + as_cod_cliente + ". " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_message ="Cliente con codice " + as_cod_cliente + " non trovato."
	return -1
elseif isnull(ls_cod_zona) or ls_cod_zona = "" then
	uof_log("Spesa non calcolata perchè il cliente " + as_cod_cliente + " non ha nessuna zona impostata")
	return 0
end if

select cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3,cod_livello_prod_4, cod_livello_prod_5
into :ls_cod_livello_prod[1], :ls_cod_livello_prod[2], :ls_cod_livello_prod[3], :ls_cod_livello_prod[4], :ls_cod_livello_prod[5]
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante la lettura del prodotto " + as_cod_prodotto
	return -1
elseif sqlca.sqlcode = 100 then
	as_message ="Prodotto " + as_cod_prodotto + " non trovato."
	return -1
end if

// Ciclo tutti i livelli per vedere se recupero i valori corretti
for li_livello = 5 to 1 step -1
	
	if isnull(ls_cod_livello_prod[li_livello]) or ls_cod_livello_prod[li_livello] = "" then 
		// Se il livello è nullo o vuoto allora passo a quello precedente
		continue
	end if
	
	
	ls_sql = "select progressivo from tab_livelli_prod_spese where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_zona='" + ls_cod_zona + "' "

	// aggiungo livello alla query
	for li_sub_livello = 1  to li_livello
		ls_sql += " and cod_livello_prod_" + string(li_sub_livello) + " = '" + ls_cod_livello_prod[li_sub_livello] + "' "
	next
	
	for li_sub_livello = li_livello + 1 to 5
			ls_sql += " and cod_livello_prod_" + string(li_sub_livello) + " is null"
	next
	
	// Eseguo query con il cliente
	li_ret = guo_functions.uof_crea_datastore(lds_store, ls_sql + " and cod_cliente='" + as_cod_cliente + "' ", ref as_message)
	
	if li_ret < 0 then
		return -1
	elseif li_ret = 0 then
		// Eseguo la query senza il cliente
		li_ret = guo_functions.uof_crea_datastore(lds_store, ls_sql, ref as_message)
		
		if li_ret < 0 then
			return -1
		end if
	end if
	
	if li_ret > 0 then
		ll_ret_val = lds_store.getitemnumber(1,1)
		destroy lds_store
		return ll_ret_val
	end if
	
next

return 0
end function

public function integer uof_calcola_spese (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message);/**
 * 
 **/
 
string					ls_sql, ls_sql_select, ls_cod_spesa, ls_cod_tipo_det_ven_spesa, ls_cod_cliente, ls_cod_iva

long					ll_count_spese, ll_i_spesa, ll_i_rows, ll_i

decimal{4}			ld_percentuale_sum, ld_min_fatturabile, ld_imponibile_spesa, ld_porto_franco, ld_porto_franco_max, &
						ld_max_fatturabile, ld_max_fatturabile_min, ld_imponibile_iva, ld_min_fatturabile_piu_alto, ld_imponibile_iva_riga
						
datetime				ldt_today

datastore			lds_cod_spese, lds_store



if not ib_enabled then
	return 0
end if

// Cancello righe precedenti
if uof_cancella_righe_spese_trasporto(al_anno, al_numero, as_tipo_documento, ref as_message) < 0 then
	return -1
end if

// Creo datastore buffer
choose case as_tipo_documento
		
	case "FATVEN"
		ls_sql = "FROM det_fat_ven " +  &
					"JOIN det_ord_ven ON " +  &
					"det_ord_ven.cod_azienda=det_fat_ven.cod_azienda AND " + &
					"det_ord_ven.anno_registrazione= det_fat_ven.anno_reg_ord_ven AND " + &
					"det_ord_ven.num_registrazione = det_fat_ven.num_reg_ord_ven AND " + &
					"det_ord_ven.prog_riga_ord_ven = det_fat_ven.prog_riga_ord_ven " + &
					"JOIN tes_ord_ven ON " + &
					"tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda AND " + &
					"tes_ord_ven.anno_registrazione= det_ord_ven.anno_registrazione AND " + &
					"tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione " + &
					"WHERE det_fat_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' AND det_fat_ven.anno_registrazione=" + string(al_anno) + " AND det_fat_ven.num_registrazione=" + string(al_numero) +  &
					" AND tes_ord_ven.flag_azzera_spese_trasp <> 'S' and det_ord_ven.cod_tipo_det_ven_spesa is not null AND det_ord_ven.cod_prodotto_spesa is not null AND " + &
					"det_ord_ven.percentuale > 0 AND det_fat_ven.imponibile_iva is not null "
		
		ls_sql_select = "SELECT tes_ord_ven.cod_cliente, det_ord_ven.min_fatturabile, (det_fat_ven.imponibile_iva * det_ord_ven.percentuale / 100) as percentuale_riga, " + &
							"det_ord_ven.porto_franco, det_ord_ven.max_fatturabile, det_fat_ven.imponibile_iva, det_ord_ven.cod_tipo_det_ven_spesa  "
							
							
	case "BOLVEN"
		ls_sql = "FROM det_bol_ven " +  &
					"JOIN det_ord_ven ON " +  &
					"det_ord_ven.cod_azienda=det_bol_ven.cod_azienda AND " + &
					"det_ord_ven.anno_registrazione= det_bol_ven.anno_registrazione_ord_ven AND " + &
					"det_ord_ven.num_registrazione = det_bol_ven.num_registrazione_ord_ven AND " + &
					"det_ord_ven.prog_riga_ord_ven = det_bol_ven.prog_riga_ord_ven " + &
					"JOIN tes_ord_ven ON " + &
					"tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda AND " + &
					"tes_ord_ven.anno_registrazione= det_ord_ven.anno_registrazione AND " + &
					"tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione " + &
					"WHERE det_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' AND det_bol_ven.anno_registrazione=" + string(al_anno) + " AND det_bol_ven.num_registrazione=" + string(al_numero) +  &
					" AND tes_ord_ven.flag_azzera_spese_trasp <> 'S' and det_ord_ven.cod_tipo_det_ven_spesa is not null AND det_ord_ven.cod_prodotto_spesa is not null AND " + &
					"det_ord_ven.percentuale > 0 AND det_bol_ven.imponibile_iva is not null "
		
		ls_sql_select = "SELECT tes_ord_ven.cod_cliente, det_ord_ven.min_fatturabile, (det_bol_ven.imponibile_iva * det_ord_ven.percentuale / 100) as percentuale_riga, " + &
							"det_ord_ven.porto_franco, det_ord_ven.max_fatturabile, det_bol_ven.imponibile_iva, det_ord_ven.cod_tipo_det_ven_spesa  "
		
		
end choose

ldt_today =  datetime(today(), now())
ll_count_spese = guo_functions.uof_crea_datastore(lds_cod_spese, "SELECT distinct det_ord_ven.cod_prodotto_spesa " + ls_sql, ref as_message)

if ll_count_spese < 0 then return -1

for ll_i_spesa = 1 to ll_count_spese
	
	ls_cod_spesa = lds_cod_spese.getitemstring(ll_i_spesa, 1)
	
	ll_i_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql_select + ls_sql + " and det_ord_ven.cod_prodotto_spesa='" + ls_cod_spesa + "' ", ref as_message)
	if ll_i_rows < 0 then return -1
	
	ld_min_fatturabile_piu_alto = 0
	ld_imponibile_spesa = 0
	ld_percentuale_sum = 0
	ld_porto_franco_max = 0
	ld_max_fatturabile_min = 9999999
	ld_imponibile_iva = 0
	
	for ll_i = 1 to ll_i_rows
		ls_cod_cliente =  lds_store.getitemstring(ll_i, 1) // codice cliente
		ld_min_fatturabile = lds_store.getitemnumber(ll_i, 2) // min_fatturabile della riga
		ld_percentuale_sum +=  lds_store.getitemnumber(ll_i, 3) // percentuale_riga
		ld_porto_franco = lds_store.getitemnumber(ll_i, 4) // porto franco
		ld_max_fatturabile =  lds_store.getitemnumber(ll_i, 5) // max_fatturabile
		ld_imponibile_iva += lds_store.getitemnumber(ll_i, 6) // imponibile_iva
	
		if ld_min_fatturabile > ld_min_fatturabile_piu_alto then ld_min_fatturabile_piu_alto = ld_min_fatturabile
		if ld_porto_franco > ld_porto_franco_max then ld_porto_franco_max = ld_porto_franco
		if ld_max_fatturabile < ld_max_fatturabile_min then ld_max_fatturabile_min = ld_max_fatturabile
		
		if ll_i = 1 then
			ls_cod_tipo_det_ven_spesa = lds_store.getitemstring(ll_i, 7) // il primo ls_cod_tipo_det_ven_spesa
		end if
	next
	
	if isnull(ld_percentuale_sum) then ld_percentuale_sum = 0
	
	if ld_min_fatturabile_piu_alto > ld_percentuale_sum then
		ld_imponibile_spesa = ld_min_fatturabile_piu_alto
	else
		ld_imponibile_spesa = ld_percentuale_sum
	end if
	
	// Passo 2
	if ld_imponibile_iva > ld_porto_franco_max then
		uof_log("Imponibile iva (" + string(ld_imponibile_iva, "#,###.00") + ") superiore al portofranco (" + string(ld_porto_franco_max, "#,###.00") + "): impostato spesa trasporto a 0. " + as_tipo_documento + " " + string(al_anno) + "/" + string(al_numero))
		ld_imponibile_spesa = 0
	elseif ld_imponibile_spesa > ld_max_fatturabile_min then
		uof_log("Spesa trasporto (" + string(ld_imponibile_spesa, "#,###.00") + ") superiore al max fatturabile (" + string(ld_max_fatturabile_min, "#,###.00") + "): impostato spesa trasporto al max fatturabile. " + as_tipo_documento + " " + string(al_anno) + "/" + string(al_numero))
		ld_imponibile_spesa = ld_max_fatturabile_min
	end if
	
	// Passo 2.1: calcolo iva
	if guo_functions.uof_get_cod_iva("VEN", ldt_today, ls_cod_spesa, ls_cod_cliente, ls_cod_tipo_det_ven_spesa, ref ls_cod_iva, ref as_message) < 0 then
		return -1
	end if
	
	// Passo 3: inserisco riga in fattura/ddt
	choose case as_tipo_documento
		case "FATVEN"
			if uof_inserisci_spesa_fat_ven(al_anno, al_numero, ld_imponibile_spesa, ls_cod_tipo_det_ven_spesa, ls_cod_spesa, ls_cod_iva, ld_percentuale_sum, ref as_message) < 0 then
				return -1
			end if
			
		case "BOLVEN"
			if uof_inserisci_spesa_bol_ven(al_anno, al_numero, ld_imponibile_spesa, ls_cod_tipo_det_ven_spesa, ls_cod_spesa, ls_cod_iva, ld_percentuale_sum, ref as_message) < 0 then
				return -1
			end if
			
	end choose
	
next

destroy lds_store
destroy lds_cod_spese
return 0
end function

public function integer uof_imposta_spese_ord_ven (integer al_anno, long al_numero, long al_prog_riga, boolean ab_copia_in_riferite, ref string as_message);/**
 * stefanop
 * 19/02/2014
 *
 * In base alla riga dell'ordine di vendita viene calcolata ed impostato il valore
 * della spesa di trasporto.
 *
 * @param al_anno Anno del documento
 * @param al_numero Numero del documento
 * @param al_prog_riga Numero Riga del documento
 * @param ab_copia_in_riferite Se posto a TRUE e la riga interessata NON è di riferimento allora
 *			copio i valori della spesa anche in tutte le righe riferite.
 * @param as_message Messaggio di ritorno
 *
 * @return 
 *		0, tutto ok,
 *		-1, errore
 **/
 
string ls_flag_evasione, ls_cod_prodotto, ls_cod_cliente, ls_cod_tipo_det_ven, ls_cod_prodotto_spesa
long ll_id_riga, ll_num_riga_appartenenza
decimal{4} ld_min_fatturabile, ld_max_fatturabile, ld_percentuale, ld_porto_franco

if not ib_enabled then return 0

setnull(ll_id_riga)
 
// Controllo che la riga non sia già stata evasa!
select flag_evasione, cod_prodotto, num_riga_appartenenza
into :ls_flag_evasione, :ls_cod_prodotto, :ll_num_riga_appartenenza
from det_ord_ven
where cod_azienda= :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero and
		 prog_riga_ord_ven = :al_prog_riga;
		 
if ls_flag_evasione = "E" then
	uof_log("Spesa non calcolata perchè riga (" + string(al_anno) + "/" + string(al_numero) + "/" + string(al_prog_riga)  + " è evasa")
	return 0
end if

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
	// esco perchè potrebbe essere una riga di tipo RIF o simile
	uof_log("Spesa non calcolata perchè riga (" + string(al_anno) + "/" + string(al_numero) + "/" + string(al_prog_riga)  + " non ha un prodotto impostato")
	return 0
end if

select cod_cliente
into :ls_cod_cliente 
from tes_ord_ven
where cod_azienda= :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero;
		 
if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente) or ls_cod_cliente = "" then
	as_message = "Ordine o cliente non trovati. " + string(al_anno) + "/" + string(al_numero)
	return -1
end if

// E' una riga di riferimento?
if not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza > 0 then
	
	// la riga padre ha già l'id della spesa impostato?
	select prog_spese_trasp
	into :ll_id_riga
	from det_ord_ven
	where cod_azienda= :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero and
		 prog_riga_ord_ven = :ll_num_riga_appartenenza;
		 
	if sqlca.sqlcode <> 0 then
		as_message = "Errore durante la lettura della riga di riferimento. " + sqlca.sqlerrtext
		return -1
	end if
end if

// Questo controllo serve per non calcolare la spesa
// se sono su una riga di riferimento, infatti il valore
// recuperato è quello della riga padre.
if isnull(ll_id_riga) or ll_id_riga <= 0 then
	ll_id_riga = uof_cerca_id_spesa(ls_cod_cliente, ls_cod_prodotto, ref as_message)
end if

if ll_id_riga < 0 then
	return -1
elseif ll_id_riga > 0 then
	// Ho trovato spesa, aggiorno campi
	select min_fatturabile, max_fatturabile, percentuale, porto_franco, cod_tipo_det_ven, cod_prodotto_spesa
	into :ld_min_fatturabile, :ld_max_fatturabile, :ld_percentuale, :ld_porto_franco, :ls_cod_tipo_det_ven, :ls_cod_prodotto_spesa
	from tab_livelli_prod_spese
	where cod_azienda = :s_cs_xx.cod_azienda and
			 progressivo = :ll_id_riga;
			
elseif ll_id_riga = 0 or isnull(ll_id_riga) then
	// Non ho trovato nessuna spesa, quindi metto a 0
	setnull(ld_min_fatturabile)
	setnull(ld_max_fatturabile)
	setnull(ld_percentuale)
	setnull(ld_porto_franco)
	setnull(ls_cod_tipo_det_ven)
	setnull(ls_cod_prodotto_spesa)
	setnull(ll_id_riga)
	
	uof_log("Spesa non calcolata perchè nessun ID spesa trovato; riga (" + string(al_anno) + "/" + string(al_numero) + "/" + string(al_prog_riga))
end if 

update det_ord_ven
set
	min_fatturabile = :ld_min_fatturabile,
	max_fatturabile = :ld_max_fatturabile,
	percentuale = :ld_percentuale,
	porto_franco = :ld_porto_franco,
	prog_spese_trasp = :ll_id_riga,
	cod_tipo_det_ven_spesa = :ls_cod_tipo_det_ven,
	cod_prodotto_spesa = :ls_cod_prodotto_spesa
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero and
		 prog_riga_ord_ven = :al_prog_riga;
				 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante l'aggiornamento dei valori di spesa per il documento " +  string(al_anno) + "/" + string(al_numero) + "/" + string(al_prog_riga) + ". " + sqlca.sqlerrtext
	return -1
end if

if ab_copia_in_riferite and (isnull(ll_num_riga_appartenenza) or ll_num_riga_appartenenza = 0) then

	update det_ord_ven
	set 
			min_fatturabile = :ld_min_fatturabile,
			max_fatturabile = :ld_max_fatturabile,
			percentuale = :ld_percentuale,
			porto_franco = :ld_porto_franco,
			prog_spese_trasp = :ll_id_riga,
			cod_tipo_det_ven_spesa = :ls_cod_tipo_det_ven,
			cod_prodotto_spesa = :ls_cod_prodotto_spesa
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno and
			 num_registrazione = :al_numero and
			 num_riga_appartenenza = :al_prog_riga;
			 
	if sqlca.sqlcode < 0 then
		as_message = "Errore durante l'aggiornamento dei valori di spesa nelle righe rifferite per il documento " +  string(al_anno) + "/" + string(al_numero) + "/" + string(al_prog_riga) + ". " + sqlca.sqlerrtext
		return -1
	end if
	
end if

return 0
end function

public subroutine uof_storico_data_riferimento (ref date adt_data_riferimento, ref integer ai_giorni_riferimento);/**
 * stefanop
 * 20/02/2014
 *
 * Ottiene la data di riferimento per la visualizzazione dello storico
 **/
 
date ldt_data_rif

ldt_data_rif = today()
 
select numero
into :ai_giorni_riferimento
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'GFC' and
		 flag_parametro = 'N';
		 
if sqlca.sqlcode <> 0 or isnull(ai_giorni_riferimento) or ai_giorni_riferimento <= 0 then ai_giorni_riferimento = 7

adt_data_riferimento = relativedate(ldt_data_rif, -ai_giorni_riferimento)
end subroutine

private function integer uof_cancella_righe_spese_trasporto (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message);/**
 * stefanop & donato
 * 20/02/2014
 *
 * Cancello le righe di trasporto relative al documento
 **/
 
setnull(as_message)
 
choose case as_tipo_documento
		
	case "FATVEN"
		delete from det_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno and
				 num_registrazione = :al_numero and
				 flag_riga_spesa_trasp = 'S';
		
	case "BOLVEN"
		delete from det_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno and
				 num_registrazione = :al_numero and
				 flag_riga_spesa_trasp = 'S';	
		
end choose

if sqlca.sqlcode < 0 then
	as_message = "Errore durante la cancellazione delle righe di trasporto per il documento " + as_tipo_documento + " " + string(al_anno) + "/" + string(al_numero) + ". " + sqlca.sqlerrtext
	return -1
end if


return 0
end function

public function integer uof_azzera_spese_ordine (integer al_anno, long al_numero, string as_flag_azzera_spese_trasp, ref string as_message);/**
 * stefanop
 * 18/02/2014
 *
 * Azzero le spese di trasporto dell'ordine.
 * Azzerro prima tutte le righe e poi imposto il flag in testata
 *
 * @return
 *		1, avvisi generali
 * 		0, tutto ok
 *		-1, errore
 **/
 
string ls_flag_evasione, ls_log

setnull(as_message)

// Controllo che l'ordine non sia già evaso
select flag_evasione
into :ls_flag_evasione
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero;
		 
// prevent default
if isnull(ls_flag_evasione) or ls_flag_evasione = "" then ls_flag_evasione = "A"

if ls_flag_evasione = "E" then
	as_message = "L'ordine " + string(al_anno) + "/" + string(al_numero) + " è già stato evaso"
	return 1
end if

// Testata
update tes_ord_ven
set flag_azzera_spese_trasp = :as_flag_azzera_spese_trasp
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_numero;
		 
if sqlca.sqlcode < 0 then
	as_message = "Errore durante l'aggiornamento della testata dell'ordine. "  + string(al_anno) + "/" + string(al_numero) + ". " + sqlca.sqlerrtext
	rollback;
	return -1
end if

commit;

// Log
if as_flag_azzera_spese_trasp = "S" then
	ls_log = "Azzerate" 
else
	ls_log = "Attivate"
end if

uof_log(ls_log + " spese di trasporto per l'ordine " + string(al_anno) + "/" + string(al_numero))

return 0
end function

public function integer uof_apri_storico (integer al_anno_documento, long al_numero_documento, string as_tipo_documento);/**
 * stefanop
 * 20/02/2014
 *
 * Apre la finestra dello storico delle ultime X fatture fatte dal cliente
 * della fattura passata (se as_tipo_documento=FATVEN) o ddt passato (se as_tipo_documento="BOLVEN").
 **/
 
string						ls_message, ls_cod_cliente
decimal{4}				ld_spesa, ld_spesa_solo_perc
s_cs_xx_parametri		lstr_parametri

if ib_enabled = false then
	return 0
end if

uof_totale_spese_trasporto(al_anno_documento, al_numero_documento, as_tipo_documento, ref ld_spesa, ref ld_spesa_solo_perc, ref ls_message)

if as_tipo_documento="FATVEN" then
	select cod_cliente
	into :ls_cod_cliente
	from tes_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno_documento and
			 num_registrazione = :al_numero_documento;
	
	lstr_parametri.parametro_s_2 = "F"
	
else
	
	select cod_cliente
	into :ls_cod_cliente
	from tes_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno_documento and
			 num_registrazione = :al_numero_documento;
			 
	lstr_parametri.parametro_s_2 = "B"
	
end if

lstr_parametri.parametro_s_1 = ls_cod_cliente
lstr_parametri.parametro_ul_1 = al_anno_documento
lstr_parametri.parametro_ul_2 = al_numero_documento
lstr_parametri.parametro_dec4_1 = ld_spesa
lstr_parametri.parametro_dec4_2 = ld_spesa_solo_perc

pcca.mdi_frame.setmicrohelp("Apertura finestra storico spese trasporto")
openwithparm(w_storico_spese_trasporto, lstr_parametri)

lstr_parametri = message.powerobjectparm

//Donato 30/05/2014.
//SR Migliorie Spese Trasporto  Funzione 3
//se lstr_parametri.parametro_s_1 = "OK" allora hai accettato le spese di trasporto calcolate secondo algoritmo principale

//se invece lstr_parametri.parametro_s_1 = "SOLO%" vuol dire che hai deciso di imputare le spese di trasporto sommando solo le percentuali su imponibile riga

//infine se lstr_parametri.parametro_s_1 = "ZEROSPESE" vuol dire che hai deciso di NON imputare alcuna spesa di trasporto

choose case lstr_parametri.parametro_s_1
	case "SOLO%"
		return 2
	
	case "ZEROSPESE"
		return 1
		
	case else
		//OK
		return 0
		
end choose

	


end function

public function integer uof_azzera_trasporto_documento (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message);//fuinzione chiamata se l'operatore decide di non imputare le spese trasporto nel documento
//Questo viene deciso all'atto della presentazione del ripeilogo spese trasporto calcolate per il documento, 
//con la visualizzazione dello storico spese trasporto nei documenti

string										ls_tipo_doc
uo_calcola_documento_euro		luo_calcolo


// Cancello righe trasporto precedenti
if uof_cancella_righe_spese_trasporto(al_anno, al_numero, as_tipo_documento, ref as_message) < 0 then
	return -1
end if

if as_tipo_documento="FATVEN" then
	ls_tipo_doc = "fat_ven"
else
	ls_tipo_doc = "bol_ven"
end if

//ricalcolo il documento per NON includere nel calcolo le spese di trasporto
luo_calcolo = create uo_calcola_documento_euro
if luo_calcolo.uof_calcola_documento(al_anno, al_numero, ls_tipo_doc, as_message) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

uof_log("Spese trasporto annullate (" + as_tipo_documento + " : "+ string(al_anno) + "/" + string(al_numero) + ")")


return 0


end function

private function integer uof_inserisci_spesa_fat_ven (integer al_anno, long al_numero, decimal ad_imponibile, string as_tipo_dettaglio, string as_cod_prodotto, string as_cod_iva, decimal ad_imponibile_solo_perc, ref string as_message);string ls_des_prodotto, ls_cod_iva
long ll_max

setnull(as_message)

ls_des_prodotto = f_des_tabella("anag_prodotti", "cod_prodotto='" + as_cod_prodotto + "' ", "des_prodotto")
if isnull(ls_des_prodotto) or ls_des_prodotto = "" then ls_des_prodotto = "Spesa Trasporto"

select max(prog_riga_fat_ven)
into :ll_max
from det_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :al_anno and 
		 num_registrazione = :al_numero;
		 
if isnull(ll_max) then ll_max = 0
ll_max = ((ll_max / 10) * 10) + 10

insert into det_fat_ven (
	cod_azienda, 
	anno_registrazione, 
	num_registrazione, 
	prog_riga_fat_ven,
	cod_tipo_det_ven,
	des_prodotto, 
	quan_fatturata, 
	prezzo_vendita, 
	cod_iva, 
	flag_riga_spesa_trasp,
	prezzo_vendita_spesa_perc
) values (
	:s_cs_xx.cod_azienda,
	:al_anno, 
	:al_numero, 
	:ll_max, 
	:as_tipo_dettaglio, 
	:ls_des_prodotto, 
	1, 
	:ad_imponibile, 
	:as_cod_iva,
	'S',
	:ad_imponibile_solo_perc);

if sqlca.sqlcode < 0 then
	as_message = "Errore durante l'inserimento della riga di trasporto nella fattura " + string(al_anno) + "/" + string(al_numero) + ". " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

private function integer uof_inserisci_spesa_bol_ven (integer al_anno, long al_numero, decimal ad_imponibile, string as_tipo_dettaglio, string as_cod_prodotto, string as_cod_iva, decimal ad_imponibile_solo_perc, ref string as_message);string ls_des_prodotto, ls_cod_iva
long ll_max

setnull(as_message)

ls_des_prodotto = f_des_tabella("anag_prodotti", "cod_prodotto='" + as_cod_prodotto + "' ", "des_prodotto")
if isnull(ls_des_prodotto) or ls_des_prodotto = "" then ls_des_prodotto = "Spesa Trasporto"

select max(prog_riga_bol_ven)
into :ll_max
from det_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :al_anno and 
		 num_registrazione = :al_numero;
		 
if isnull(ll_max) then ll_max = 0
ll_max = ((ll_max / 10) * 10) + 10

insert into det_bol_ven (
	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_riga_bol_ven,
	cod_tipo_det_ven,
	des_prodotto,
	quan_consegnata,
	prezzo_vendita,
	cod_iva,
	flag_riga_spesa_trasp,
	prezzo_vendita_spesa_perc)
values(	:s_cs_xx.cod_azienda,
			:al_anno,
			:al_numero,
			:ll_max,
			:as_tipo_dettaglio,
			:ls_des_prodotto,
			1,
			:ad_imponibile,
			:as_cod_iva,
			'S',
			:ad_imponibile_solo_perc);

if sqlca.sqlcode < 0 then
	as_message = "Errore durante l'inserimento della riga di trasporto nel DDT " + string(al_anno) + "/" + string(al_numero) + ". " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_totale_spese_trasporto (integer al_anno, long al_numero, string as_tipo_documento, ref decimal ad_totale_spese, ref decimal ad_totale_spese_solo_perc, ref string as_message);/**
 * stefanop
 * 19/02/2014
 *
 * Calcolo il totale delle spese di trasporto nel documento passato
 **/
 
 string ls_sql, ls_tabella
 
 setnull(ad_totale_spese)
 setnull(as_message)
 
 choose case as_tipo_documento
		
	case "FATVEN"
		select isnull(sum(prezzo_vendita), 0), isnull(sum(prezzo_vendita_spesa_perc), 0)
		into :ad_totale_spese, :ad_totale_spese_solo_perc
		from det_fat_ven
		join tab_tipi_det_ven on
			tab_tipi_det_ven.cod_azienda=det_fat_ven.cod_azienda and
			tab_tipi_det_ven.cod_tipo_det_ven=det_fat_ven.cod_tipo_det_ven
		where det_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
				 det_fat_ven.anno_registrazione = :al_anno and
				 det_fat_ven.num_registrazione = :al_numero and
				 tab_tipi_det_ven.flag_tipo_det_ven='T';
		
	case "BOLVEN"
		select isnull(sum(imponibile_iva), 0), isnull(sum(prezzo_vendita_spesa_perc), 0)
		into :ad_totale_spese, :ad_totale_spese_solo_perc
		from det_bol_ven
		join tab_tipi_det_ven on
			tab_tipi_det_ven.cod_azienda=det_bol_ven.cod_azienda and
			tab_tipi_det_ven.cod_tipo_det_ven=det_bol_ven.cod_tipo_det_ven
		where det_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and
				 det_bol_ven.anno_registrazione = :al_anno and
				 det_bol_ven.num_registrazione = :al_numero and
				 tab_tipi_det_ven.flag_tipo_det_ven='T';
		
end choose
 
if sqlca.sqlcode < 0 then
	setnull(ad_totale_spese)
	as_message = "Errore durante il calcolo delle spese di trasporto del documento " + string(al_anno) + "/" + string(al_numero) + ". " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_imposta_trasporto_solo_percentuale (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message);/**
 * stefanop & donato
 * 20/02/2014
 *
 * Re-imposto le righe del documento relative al trasporto calcolandole in base alle sole percentuali sugli imponibili
 * NOTA: in realtà si tratta di un semplice update, in quanto in fase di creazione tali importi sono stati salvati nella colonna prezzo_vendita_spesa_perc
 **/
 
 uo_calcola_documento_euro			luo_calcolo
 string										ls_tipo_doc
 
 
setnull(as_message)
 
choose case as_tipo_documento
		
	case "FATVEN"
		update det_fat_ven
		set prezzo_vendita = prezzo_vendita_spesa_perc
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno and
				 num_registrazione = :al_numero and
				 flag_riga_spesa_trasp = 'S';

	case "BOLVEN"
		update det_bol_ven
		set prezzo_vendita = prezzo_vendita_spesa_perc
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno and
				 num_registrazione = :al_numero and
				 flag_riga_spesa_trasp = 'S';
		
end choose

if sqlca.sqlcode < 0 then
	as_message = "Errore durante la re-impostazione delle righe di trasporto (calcolo su sole percentuali imponibile) per il documento " + &
						as_tipo_documento + " " + string(al_anno) + "/" + string(al_numero) + ": " + sqlca.sqlerrtext
	return -1
end if

if as_tipo_documento="FATVEN" then
	ls_tipo_doc = "fat_ven"
else
	ls_tipo_doc = "bol_ven"
end if

//ricalcolo il documento
luo_calcolo = create uo_calcola_documento_euro
if luo_calcolo.uof_calcola_documento(al_anno, al_numero, ls_tipo_doc, as_message) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

uof_log("Spese trasporto solo da percentuali su imponibile (" + as_tipo_documento + " : "+ string(al_anno) + "/" + string(al_numero) + ")")

return 0
end function

public function integer uof_check_per_ricalcolo (integer al_anno, long al_numero, string as_tipo_documento, ref string as_message);long			ll_count_righe, ll_count2

string			ls_flag_movimenti, ls_flag_contabilita, ls_flag_gen_fat


choose case as_tipo_documento
	//--------------------------------------------------------------------
	case "FATVEN"
		//*************************************
		//controllo righe collegate a ordine di vendita
		select count(*)
		into :ll_count_righe
		from det_fat_ven
		where 	det_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and 
					det_fat_ven.anno_registrazione=:al_anno and 
					det_fat_ven.num_registrazione=:al_numero and
					det_fat_ven.anno_reg_ord_ven>0;
		
		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo esistenza righe collegate a ordine: "+sqlca.sqlerrtext
			return -1
			
		elseif ll_count_righe=0 or isnull(ll_count_righe) then
			as_message = "Impossibile ricalcolare le spese trasporto: in questa fattura non esistono righe collegate a ordine di vendita!"
			return 1
			
		end if
		
		//*************************************
		//verifica se già confermata o già contabilizzata
		select flag_movimenti, flag_contabilita
		into :ls_flag_movimenti, :ls_flag_contabilita
		from tes_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and 
					anno_registrazione=:al_anno and 
					num_registrazione=:al_numero;
	
		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo fattura già confermata/contabilizzata: "+sqlca.sqlerrtext
			return -1
			
		elseif ls_flag_contabilita="S" then
			as_message = "Impossibile ricalcolare le spese trasporto: questa fattura è stata già contabilizzata!"
			return 1
			
		elseif ls_flag_movimenti="S" then
			as_message = "Impossibile ricalcolare le spese trasporto: questa fattura è stata già confermata!"
			return 1
			
		end if
	
		
		
		//*************************************
		//NOTA BENE: FARE QUESTO CONTROLLO SEMPRE ALLA FINE
		//controllo righe collegate a ordine di vendita 
		//la cui testata ha la colonna flag_azzera_spese_trasp posto a SI
		//dare alert se il conteggio è maggiore di zero, ma far proseguire
		select count(*)
		into :ll_count2
		from det_fat_ven
		join det_ord_ven on 	det_ord_ven.cod_azienda=det_fat_ven.cod_azienda and
									det_ord_ven.anno_registrazione= det_fat_ven.anno_reg_ord_ven and
									det_ord_ven.num_registrazione = det_fat_ven.num_reg_ord_ven and
									det_ord_ven.prog_riga_ord_ven = det_fat_ven.prog_riga_ord_ven
		join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
									tes_ord_ven.anno_registrazione= det_ord_ven.anno_registrazione and
									tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione
		where 	det_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
					det_fat_ven.anno_registrazione=:al_anno and 
					det_fat_ven.num_registrazione=:al_numero and
					det_fat_ven.anno_reg_ord_ven>0 and 
					tes_ord_ven.flag_azzera_spese_trasp ='S';
		
		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo esistenza righe collegate a ordine con flag_azzera_spese_trasp ATTIVATO: "+sqlca.sqlerrtext
			return -1
			
		elseif ll_count2>0 then
			//ci sono righe relative a testata ordine con flag azzera spese impostato a si
			
			if ll_count2=ll_count_righe then
				//tutte le righe si riferiscono a testate ordini con flag azzera spese impostato a si, quindi nessun ricalcolo possibile
				as_message = "In questa fattura tutte le righe sono collegate a ordine di vendita con flag_azzera_spese_trasp ATTIVATO! Il ricalcolo spese trasporto non è possibile!"
				return 1
			else
				//solo alcune righe si riferiscono a testate ordini con flag azzera spese impostato a si, quindi sulle altre il ricalcolo è possibile
				as_message = "In questa Fattura esistono righe collegate a ordine di vendita con flag_azzera_spese_trasp ATTIVATO! Queste righe saranno escluse dal ricalcolo. Continuare?"
				return 2
			end if
		end if
		
	

	//--------------------------------------------------------------------
	case "BOLVEN"
		//*************************************
		//controllo righe collegate a ordine di vendita
		select count(*)
		into :ll_count_righe
		from det_bol_ven
		where 	det_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and 
					det_bol_ven.anno_registrazione=:al_anno and 
					det_bol_ven.num_registrazione=:al_numero and
					det_bol_ven.anno_registrazione_ord_ven>0;
		
		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo esistenza righe collegate a ordine: "+sqlca.sqlerrtext
			return -1
			
		elseif ll_count_righe=0 or isnull(ll_count_righe) then
			as_message = "Impossibile ricalcolare le spese trasporto: in questo D.d.T. non esistono righe collegate a ordine di vendita!"
			return 1
			
		end if
		
		
		//*************************************
		//verifica se già confermata o già contabilizzata
		select flag_movimenti, flag_gen_fat
		into :ls_flag_movimenti, :ls_flag_gen_fat
		from tes_bol_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and 
					anno_registrazione=:al_anno and 
					num_registrazione=:al_numero;
	
		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo ddt già confermato/fatturato: "+sqlca.sqlerrtext
			return -1
			
		elseif ls_flag_gen_fat="S" then
			as_message = "Impossibile ricalcolare le spese trasporto: questo ddt è stato già fatturato!"
			return 1
			
		elseif ls_flag_movimenti="S" then
			as_message = "Impossibile ricalcolare le spese trasporto: questo ddt è stato già confermato!"
			return 1
			
		end if
		
		
		
		//*************************************
		//NOTA BENE: FARE QUESTO CONTROLLO SEMPRE ALLA FINE
		//controllo righe collegate a ordine di vendita 
		//la cui testata ha la colonna flag_azzera_spese_trasp posto a SI
		//dare alert se il conteggio è maggiore di zero, ma far proseguire
		select count(*)
		into :ll_count2
		from det_bol_ven
		join det_ord_ven on 	det_ord_ven.cod_azienda=det_bol_ven.cod_azienda and
									det_ord_ven.anno_registrazione= det_bol_ven.anno_registrazione_ord_ven and
									det_ord_ven.num_registrazione = det_bol_ven.num_registrazione_ord_ven and
									det_ord_ven.prog_riga_ord_ven = det_bol_ven.prog_riga_ord_ven
		join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
									tes_ord_ven.anno_registrazione= det_ord_ven.anno_registrazione and
									tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione
		where 	det_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and
					det_bol_ven.anno_registrazione=:al_anno and 
					det_bol_ven.num_registrazione=:al_numero and
					det_bol_ven.anno_registrazione_ord_ven>0 and 
					tes_ord_ven.flag_azzera_spese_trasp ='S';

		if sqlca.sqlcode<0 then
			as_message = "Errore in controllo esistenza righe collegate a ordine con flag_azzera_spese_trasp ATTIVATO: "+sqlca.sqlerrtext
			return -1
			
		elseif ll_count2>0 then
			//ci sono righe relative a testata ordine con flag azzera spese impostato a si
			
			if ll_count2=ll_count_righe then
				//tutte le righe si riferiscono a testate ordini con flag azzera spese impostato a si, quindi nessun ricalcolo possibile
				as_message = "In questo D.d.T. tutte le righe sono collegate a ordine di vendita con flag_azzera_spese_trasp ATTIVATO! Il ricalcolo spese trasporto non è possibile!"
				return 1
			else
				//solo alcune righe si riferiscono a testate ordini con flag azzera spese impostato a si, quindi sulle altre il ricalcolo è possibile
				as_message = "In questo D.d.T. esistono righe collegate a ordine di vendita con flag_azzera_spese_trasp ATTIVATO! Queste righe saranno escluse dal ricalcolo. Continuare?"
				return 2
			end if
		end if
		
	
	//--------------------------------------------------------------------
	case else
		as_message = "Il documento non è tra quelli previsti (Fattura vendita o D.d.T. di vendita)"
		return 1
	
end choose




return 0
end function

public subroutine uof_log (string as_message);if ib_log then
	
	iuo_log.uof_write_log_sistema_not_sqlca("SPESETRASP", as_message)
	
end if
end subroutine

on uo_spese_trasporto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_spese_trasporto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_log = create uo_log_sistema
guo_functions.uof_get_parametro_azienda("LSP", ib_log)
guo_functions.uof_get_parametro_azienda("SSP", ib_enabled)

if isnull(ib_enabled) then ib_enabled = false
end event

event destructor;destroy iuo_log
end event

