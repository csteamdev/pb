﻿$PBExportHeader$uo_web.sru
$PBExportComments$Oggetto per Collegamento ad Internet
forward
global type uo_web from nonvisualobject
end type
end forward

global type uo_web from nonvisualobject autoinstantiate
end type

type prototypes
 FUNCTION long ShellExecuteA( long hWnd, REF String ls_Operation, REF String ls_File, REF String ls_Parameters, REF String ls_Directory, INT nShowCmd ) library 'shell32'

end prototypes

forward prototypes
public function integer openwebpage (string as_url)
public function integer executewebpage (string as_url)
end prototypes

public function integer openwebpage (string as_url);/********************************************************************
	OpenWebPage
	
	<DESC>	This function will use the registry to findout
				the default browser and will execute that program
				passing the URL as a command line parameter.</DESC>
	
	<RETURN>	Integer:
				<LI> 1, Executed ok
				<LI> -1, Execute failed</RETURN>

	<ACCESS>	Public

	<ARGS>	as_URL: The URL you want to execute</ARGS>

	<USAGE>	nca_WTK.OpenWebPage( 'http://www.pbdr.com' )</USAGE>

********************************************************************/
String ls_DefaultBrowser, ls_Command

SetPointer( HourGlass! )

// Trova il browser prefedinito dell'utente
//RegistryGet( 'HKEY_CLASSES_ROOT\.htm', '', ls_DefaultBrowser )
RegistryGet( 'HKEY_CURRENT_USER\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice', 'ProgId', ls_DefaultBrowser )

// for the default .htm class find the shell open command
RegistryGet( 'HKEY_CLASSES_ROOT\' + ls_DefaultBrowser + '\shell\open\command\', '', ls_Command )

// If the open command supports arg substiution use that else append URL
IF Pos( ls_Command, '%1' ) > 0 THEN
	ls_Command = Replace( ls_Command, Pos( ls_Command, '%1' ), 2, as_URL )
ELSE
	ls_Command += ' ' + as_URL
END IF

// Run the browser for the request URL passing back the RC
RETURN Run( ls_Command )

end function

public function integer executewebpage (string as_url);/********************************************************************
	ExecuteWebPage
	
	<DESC>	This function will execute a URL, if a web browser
				is already open then the URL will be shown
				in that browser window, else a new browser session
				is started. Makes use of a 32bit API call.</DESC>
	
	<RETURN>	Integer:
				<LI> 1, Executed ok
				<LI> -1, Execute failed</RETURN>

	<ACCESS>	Public

	<ARGS>	as_URL: The URL you want to execute</ARGS>

	<USAGE>	nca_WTK.ExecuteWebPage( 'http://www.pbdr.com' )</USAGE>

********************************************************************/
String ls_Command, ls_Dir, ls_Args, ls_URL

SetPointer( HourGlass! )

ls_Command = "open"
ls_Dir = ""
ls_Args = ""
ls_URL = as_URL

RETURN ShellExecuteA( 0, ls_Command, ls_URL, ls_Args, ls_Dir, 5 )
end function

on uo_web.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_web.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
/********************************************************************
	nca_webtoolkit: Web Tookit <EXCLUDE>

	<OBJECT>	This web toolkit is used for interaction with web
				type applications, such as launching a URL in
				a browser from within PB.</OBJECT>

	<USAGE>	This object is autoinstantiate, declare it
				and call the functions you want.</USAGE>

	  Date	Ref	Author			Comments
	12/16/97			Ken Howe			First Version
********************************************************************/
end event

