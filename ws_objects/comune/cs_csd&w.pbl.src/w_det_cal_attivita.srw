﻿$PBExportHeader$w_det_cal_attivita.srw
$PBExportComments$Finestra Dettaglio Calendario Attivita
forward
global type w_det_cal_attivita from w_cs_xx_principale
end type
type dw_det_cal_attivita_lista from uo_cs_xx_dw within w_det_cal_attivita
end type
type dw_det_cal_attivita_det from uo_cs_xx_dw within w_det_cal_attivita
end type
end forward

global type w_det_cal_attivita from w_cs_xx_principale
int Width=2277
int Height=1585
boolean TitleBar=true
string Title="Dettaglio Calendario Attività"
dw_det_cal_attivita_lista dw_det_cal_attivita_lista
dw_det_cal_attivita_det dw_det_cal_attivita_det
end type
global w_det_cal_attivita w_det_cal_attivita

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_cal_attivita_lista.set_dw_key("cod_azienda")
dw_det_cal_attivita_lista.set_dw_key("cod_attivita")
dw_det_cal_attivita_lista.set_dw_key("data_giorno")
dw_det_cal_attivita_lista.set_dw_key("cod_cat_attrezzature")
dw_det_cal_attivita_lista.set_dw_key("cod_attrezzatura")

dw_det_cal_attivita_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_det_cal_attivita_det.set_dw_options(sqlca,dw_det_cal_attivita_lista,c_sharedata+c_scrollparent,c_default)

end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita",&
                 "tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on w_det_cal_attivita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_cal_attivita_lista=create dw_det_cal_attivita_lista
this.dw_det_cal_attivita_det=create dw_det_cal_attivita_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_cal_attivita_lista
this.Control[iCurrent+2]=dw_det_cal_attivita_det
end on

on w_det_cal_attivita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_cal_attivita_lista)
destroy(this.dw_det_cal_attivita_det)
end on

type dw_det_cal_attivita_lista from uo_cs_xx_dw within w_det_cal_attivita
int X=23
int Y=21
int Width=2195
int Height=501
int TabOrder=10
string DataObject="d_det_cal_attivita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore
string   ls_attivita, ls_cat_attrezzature, ls_attrezzatura, ls_gg
datetime ld_data_giorno


ls_attivita         = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_attivita")
ld_data_giorno      = i_parentdw.getitemdatetime(i_parentdw.getrow(), "data_giorno")
ls_cat_attrezzature = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_cat_attrezzature")
ls_attrezzatura     = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_attrezzatura")


ll_errore = retrieve(s_cs_xx.cod_azienda, ld_data_giorno, ls_attivita, ls_cat_attrezzature, ls_attrezzatura)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

string   ls_attivita, ls_cat_attrezzature, ls_attrezzatura
datetime ld_data_giorno

ls_attivita         = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_attivita")
ls_cat_attrezzature = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_cat_attrezzature")
ls_attrezzatura     = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_attrezzatura")
ld_data_giorno      = i_parentdw.getitemdatetime(i_parentdw.getrow(), "data_giorno")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_attivita")) THEN
      SetItem(l_Idx, "cod_attivita", ls_attivita)
   END IF
   IF IsNull(GetItemdatetime(l_Idx, "data_giorno")) THEN
      SetItem(l_Idx, "data_giorno", ld_data_giorno)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_cat_attrezzature")) THEN
      SetItem(l_Idx, "cod_cat_attrezzature", ls_cat_attrezzature)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_attrezzatura")) THEN
      SetItem(l_Idx, "cod_attrezzatura", ls_attrezzatura)
   END IF
NEXT

end event

on pcd_new;call uo_cs_xx_dw::pcd_new;//long ll_num_registrazione
//
//string ls_prodotto, ls_reparto, ls_lavorazione
//
//ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
//ls_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
//ls_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")
//
//select max(det_fasi_lavorazione.prog_riga_fasi_lavorazione)
//  into :ll_num_registrazione
//  from det_fasi_lavorazione
//  where (det_fasi_lavorazione.cod_azienda = :s_cs_xx.cod_azienda) and (det_fasi_lavorazione.cod_prodotto = :ls_prodotto) and (det_fasi_lavorazione.cod_reparto = :ls_reparto) and (det_fasi_lavorazione.cod_lavorazione = :ls_lavorazione)  ;
//
//if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
//    dw_det_fasi_lavorazione_lista.SetItem (dw_det_fasi_lavorazione_lista.GetRow ( ),&
//                                 "prog_riga_fasi_lavorazione", 1 )
//else
//   ll_num_registrazione = ll_num_registrazione + 1
//   dw_det_fasi_lavorazione_lista.SetItem (dw_det_fasi_lavorazione_lista.GetRow ( ),&
//                                 "prog_riga_fasi_lavorazione", ll_num_registrazione)
//end if
//cb_ricerca_prod.enabled=true
end on

type dw_det_cal_attivita_det from uo_cs_xx_dw within w_det_cal_attivita
int X=23
int Y=541
int Width=2195
int Height=921
int TabOrder=20
string DataObject="d_det_cal_attivita_det"
BorderStyle BorderStyle=StyleRaised!
end type

