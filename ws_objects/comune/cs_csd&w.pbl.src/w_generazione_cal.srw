﻿$PBExportHeader$w_generazione_cal.srw
$PBExportComments$Window generazione tab_cal_attivita
forward
global type w_generazione_cal from w_cs_xx_principale
end type
type st_5 from statictext within w_generazione_cal
end type
type lb_1 from listbox within w_generazione_cal
end type
type cb_6 from commandbutton within w_generazione_cal
end type
type st_7 from statictext within w_generazione_cal
end type
type st_9 from statictext within w_generazione_cal
end type
type st_1 from statictext within w_generazione_cal
end type
type st_8 from statictext within w_generazione_cal
end type
type st_11 from statictext within w_generazione_cal
end type
type st_13 from statictext within w_generazione_cal
end type
type st_10 from statictext within w_generazione_cal
end type
type st_3 from statictext within w_generazione_cal
end type
type em_1 from editmask within w_generazione_cal
end type
type st_12 from statictext within w_generazione_cal
end type
type cb_5 from commandbutton within w_generazione_cal
end type
type cb_4 from commandbutton within w_generazione_cal
end type
type cb_1 from uo_cb_close within w_generazione_cal
end type
type st_2 from statictext within w_generazione_cal
end type
type st_4 from statictext within w_generazione_cal
end type
end forward

global type w_generazione_cal from w_cs_xx_principale
int Width=1980
int Height=1565
boolean TitleBar=true
string Title="Generazione Calendari"
st_5 st_5
lb_1 lb_1
cb_6 cb_6
st_7 st_7
st_9 st_9
st_1 st_1
st_8 st_8
st_11 st_11
st_13 st_13
st_10 st_10
st_3 st_3
em_1 em_1
st_12 st_12
cb_5 cb_5
cb_4 cb_4
cb_1 cb_1
st_2 st_2
st_4 st_4
end type
global w_generazione_cal w_generazione_cal

on w_generazione_cal.create
int iCurrent
call w_cs_xx_principale::create
this.st_5=create st_5
this.lb_1=create lb_1
this.cb_6=create cb_6
this.st_7=create st_7
this.st_9=create st_9
this.st_1=create st_1
this.st_8=create st_8
this.st_11=create st_11
this.st_13=create st_13
this.st_10=create st_10
this.st_3=create st_3
this.em_1=create em_1
this.st_12=create st_12
this.cb_5=create cb_5
this.cb_4=create cb_4
this.cb_1=create cb_1
this.st_2=create st_2
this.st_4=create st_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_5
this.Control[iCurrent+2]=lb_1
this.Control[iCurrent+3]=cb_6
this.Control[iCurrent+4]=st_7
this.Control[iCurrent+5]=st_9
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=st_8
this.Control[iCurrent+8]=st_11
this.Control[iCurrent+9]=st_13
this.Control[iCurrent+10]=st_10
this.Control[iCurrent+11]=st_3
this.Control[iCurrent+12]=em_1
this.Control[iCurrent+13]=st_12
this.Control[iCurrent+14]=cb_5
this.Control[iCurrent+15]=cb_4
this.Control[iCurrent+16]=cb_1
this.Control[iCurrent+17]=st_2
this.Control[iCurrent+18]=st_4
end on

on w_generazione_cal.destroy
call w_cs_xx_principale::destroy
destroy(this.st_5)
destroy(this.lb_1)
destroy(this.cb_6)
destroy(this.st_7)
destroy(this.st_9)
destroy(this.st_1)
destroy(this.st_8)
destroy(this.st_11)
destroy(this.st_13)
destroy(this.st_10)
destroy(this.st_3)
destroy(this.em_1)
destroy(this.st_12)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.st_4)
end on

event pc_setwindow;call super::pc_setwindow;em_1.text=string(year(today()))
end event

type st_5 from statictext within w_generazione_cal
int X=732
int Y=21
int Width=965
int Height=61
boolean Enabled=false
string Text="Lista Attività non ancora in Calendario:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_1 from listbox within w_generazione_cal
int X=714
int Y=85
int Width=1212
int Height=1181
int TabOrder=10
boolean VScrollBar=true
boolean MultiSelect=true
boolean ExtendedSelect=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_6 from commandbutton within w_generazione_cal
int X=389
int Y=1361
int Width=366
int Height=81
int TabOrder=20
string Text="&Agg.Lista"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_anno

setpointer(hourglass!)

lb_1.reset()

f_po_loadlb(lb_1, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_attivita not in (select cod_attivita from tab_cal_attivita where cod_azienda='" + s_cs_xx.cod_azienda + "' and anno =" + em_1.text + ")","")

setpointer(arrow!)
end event

type st_7 from statictext within w_generazione_cal
int X=366
int Y=441
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_9 from statictext within w_generazione_cal
int X=23
int Y=441
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Attrezzatura:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_generazione_cal
int X=366
int Y=341
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_generazione_cal
int X=23
int Y=341
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Attività:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_11 from statictext within w_generazione_cal
int X=366
int Y=241
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_13 from statictext within w_generazione_cal
int X=23
int Y=241
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Record:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_generazione_cal
int X=366
int Y=141
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_generazione_cal
int X=23
int Y=141
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Mese:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_1 from editmask within w_generazione_cal
int X=366
int Y=41
int Width=321
int Height=81
int TabOrder=30
Alignment Alignment=Center!
string Mask="yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
double Increment=1
string MinMax="~~"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_12 from statictext within w_generazione_cal
int X=23
int Y=41
int Width=321
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Anno:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_5 from commandbutton within w_generazione_cal
int X=1166
int Y=1361
int Width=366
int Height=81
int TabOrder=40
string Text="&Elim.Cal.Att."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long     ll_numero_righe,ll_larghezza,ll_conteggio,ll_t
integer  li_test
string   ls_cod_attivita,ls_anno
datetime ld_data_inizio,ld_data_fine
string   ls_ar_cod_attivita[]

li_test = g_mb.messagebox("Sep", "Attenzione stai per cancellare tutto il calendario attrezzature per l'anno " + string(year(date(em_1.text + "-01-01"))) + ", sei sicuro?",  Question!, OKCancel!, 1)

if li_test = 2 then return

setpointer(hourglass!)

ld_data_inizio = datetime(date(em_1.text + "-01-01"),time('00:00:00'))
ld_data_fine = datetime(date(em_1.text + "-12-31"),time('00:00:00'))

select count(*) into:ll_numero_righe
from tab_attivita
where cod_azienda = :s_cs_xx.cod_azienda;

ll_larghezza = (st_2.width/ll_numero_righe)

st_2.width = 0

ll_conteggio = 1

declare righe_attivita_1 cursor for select cod_attivita from tab_attivita where cod_azienda = :s_cs_xx.cod_azienda;
open righe_attivita_1;

do while 1 = 1
	fetch righe_attivita_1 into :ls_ar_cod_attivita[ll_conteggio];

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB",sqlca.SQLErrText,Information!)
	end if

	ll_conteggio++
loop

close righe_attivita_1;

for ll_t = 1 to ll_conteggio
	ls_cod_attivita = ls_ar_cod_attivita[ll_t]

	DELETE FROM det_cal_attivita  
	WHERE cod_azienda = :s_cs_xx.cod_azienda
	AND 	cod_attivita = :ls_cod_attivita
	AND	data_giorno between :ld_data_inizio and :ld_data_fine;

	DELETE FROM det_cal_attivita_progetti  
	WHERE cod_azienda = :s_cs_xx.cod_azienda
	AND 	cod_attivita = :ls_cod_attivita
	AND	data_giorno between :ld_data_inizio and :ld_data_fine;

	DELETE FROM tab_cal_attivita  
	WHERE cod_azienda = :s_cs_xx.cod_azienda
	AND   cod_attivita = :ls_cod_attivita
	AND	data_giorno between :ld_data_inizio and :ld_data_fine;	

	st_2.width = ll_larghezza * ll_t

next

commit;

st_2.width = st_4.width

lb_1.reset()

f_po_loadlb(lb_1, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_attivita not in (select cod_attivita from tab_cal_attivita where cod_azienda='" + s_cs_xx.cod_azienda + "' and anno =" + string(year(date(em_1.text))) + ")","")

setpointer(arrow!)

g_mb.messagebox("Sep","Eliminazione calendario completata con successo",information!)
end event

type cb_4 from commandbutton within w_generazione_cal
int X=778
int Y=1361
int Width=366
int Height=81
int TabOrder=50
string Text="&Gen.Cal.Att."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer  li_num_mese,li_num_giorno,li_giorno_settimana,li_num_ore,li_num_giorni,li_num_giorni_bis
integer  li_ar_num_ore[]
integer  li_tm,li_tg,li_t,li_anno,li_settimana,li_giorno
integer  li_ar_num_giorni[],li_ar_num_giorni_bis[]
long     ll_numero_righe,ll_lunghezza,ll_conteggio,ll_altezza,ll_conteggio_2,ll_numero_righe_2
long     ll_t_1,ll_t_2,ll_numero_record,ll_num_attivita
string   ls_cod_attivita,ls_cod_cat_attrezzature,ls_flag_tipo_giorno,ls_anno_dopo,ls_cod_attrezzatura,ls_test
string   ls_ar_flag_tipo_giorno[],ls_codice[]
datetime ld_data_giorno,ld_oggi,ld_anno_dopo
boolean  lb_anno_bis,lb_test
integer  ar_set_num_ore[]
string   ar_set_tipo_giorno[],ls_cod_attivita_prec
string   ls_ar_attivita_attrezzatura[],ls_ar_ferie_flag_tipo_giorno[12,31]
string   ls_ar_cod_cat_attrezzatura[]

setpointer(hourglass!)

ld_oggi = datetime(date(em_1.text + "-01-01"),time('00:00:00'))
li_anno = year(date(ld_oggi))

ll_num_attivita = f_po_selectlb(lb_1,ls_codice[])

if ll_num_attivita = 0 then
	g_mb.messagebox("Sep","Attenzione non è stata selezionata alcuna attività per la generazione del calendario",exclamation!)
	return
end if

for ll_t_1 = 1 to ll_num_attivita
	
	ls_cod_attivita = ls_codice[ll_t_1]

	SELECT cod_cat_attrezzature  
	INTO   :ls_cod_cat_attrezzature  
	FROM   tab_attivita  
	WHERE  tab_attivita.cod_azienda = :s_cs_xx.cod_azienda 
	AND    tab_attivita.cod_attivita = :ls_cod_attivita;
	
	SELECT cod_attrezzatura  
	INTO   :ls_test  
	FROM 	 anag_attrezzature  
	WHERE  anag_attrezzature.cod_azienda = :s_cs_xx.cod_azienda
	AND    anag_attrezzature.cod_cat_attrezzature = :ls_cod_cat_attrezzature;
	
	if isnull(ls_test) or ls_test = "" then
		g_mb.messagebox("Sep","Attenzione! Non esiste nessuna attrezzatura per la categoria "+ ls_cod_cat_attrezzature +", creare almeno un'attrezzatura e rigenerare i calendari",exclamation!)
		continue
	end if

	declare righe_anag_attrezzature cursor for 
	select  cod_attrezzatura 
	from    anag_attrezzature 
	where   anag_attrezzature.cod_azienda=:s_cs_xx.cod_azienda 
	and     anag_attrezzature.cod_cat_attrezzature =:ls_cod_cat_attrezzature 
	and     cod_attrezzatura not in (select cod_attrezzatura 
												from tab_cal_attivita 
												where cod_azienda=:s_cs_xx.cod_azienda 
												and anno=:li_anno);
												
	open    righe_anag_attrezzature;
	
	do while 1 = 1
		fetch righe_anag_attrezzature into :ls_cod_attrezzatura;
	   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Errore nel DB",sqlca.SQLErrText,Information!)
		end if

		ll_conteggio++

		ls_ar_attivita_attrezzatura[ll_conteggio] = ls_cod_attivita + "|" + ls_cod_attrezzatura
		ls_ar_cod_cat_attrezzatura[ll_conteggio] = ls_cod_cat_attrezzature

	loop

	close righe_anag_attrezzature;

next

if ll_conteggio = 0 then return

ll_lunghezza = (st_2.width/ll_conteggio)

st_2.width = 0

	for li_t = 1 to 7
		SELECT tab_cal_settimana.flag_tipo_giorno,
				 tab_cal_settimana.num_ore
		INTO   :ar_set_tipo_giorno[li_t],
			    :ar_set_num_ore[li_t]
		FROM   tab_cal_settimana  
		WHERE  tab_cal_settimana.cod_azienda = :s_cs_xx.cod_azienda
		AND	 tab_cal_settimana.giorno_settimana = :li_t;	
	next

	ls_anno_dopo = "01/01/" + string(li_anno + 1)
	ld_anno_dopo = datetime(date(ls_anno_dopo),time('00:00:00'))

	if daysafter(date("01/01/" + string(li_anno)),date(ld_anno_dopo)) = 366 then
		lb_anno_bis = true
	else
		lb_anno_bis = false
	end if

	for li_tm = 1 to 12
		SELECT tab_parametri_mesi.num_giorni,   
     		    tab_parametri_mesi.num_giorni_bis  
	 	INTO 	 :li_ar_num_giorni[li_tm],   
   	       :li_ar_num_giorni_bis[li_tm]  
		FROM   tab_parametri_mesi  
		WHERE  tab_parametri_mesi.num_mese = :li_tm;
	next 

for li_tm = 1 to 12
	for li_tg = 1 to 31
		SELECT tab_cal_ferie.flag_tipo_giorno
		INTO   :ls_ar_ferie_flag_tipo_giorno[li_tm,li_tg]				
		FROM   tab_cal_ferie
		WHERE  tab_cal_ferie.cod_azienda = :s_cs_xx.cod_azienda 
		AND	 tab_cal_ferie.num_mese = :li_tm 
		AND	 tab_cal_ferie.num_giorno = :li_tg; 				
	next
next

ls_cod_attivita = ""

for ll_t_1 = 1 to ll_conteggio

	ls_cod_attivita_prec = ls_cod_attivita
	ls_cod_attivita = left(ls_ar_attivita_attrezzatura[ll_t_1],pos(ls_ar_attivita_attrezzatura[ll_t_1],"|") - 1)
	st_1.text = ls_cod_attivita
	ls_cod_attrezzatura = mid(ls_ar_attivita_attrezzatura[ll_t_1],pos(ls_ar_attivita_attrezzatura[ll_t_1],"|")+1)
	st_7.text = ls_cod_attrezzatura
	ls_cod_cat_attrezzature	= ls_ar_cod_cat_attrezzatura[ll_t_1] 

	if ls_cod_attivita_prec <> ls_cod_attivita then
		li_giorno = 1	

		declare righe_ore_attivita cursor for select num_ore,flag_tipo_giorno from tab_ore_attivita where cod_azienda = :s_cs_xx.cod_azienda and cod_attivita =:ls_cod_attivita;
		open righe_ore_attivita;

		do while 1 = 1
			fetch righe_ore_attivita into :li_ar_num_ore[li_giorno],:ls_ar_flag_tipo_giorno[li_giorno];

		   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Errore nel DB",sqlca.SQLErrText + string(sqlca.sqlcode),Information!)
			end if
			li_giorno++

		loop
		
		close righe_ore_attivita;

		if li_giorno=1 then 
			g_mb.messagebox("Sep","Attenzione! Tabella ore attivita non completata",Information!)
			exit
		end if
	
	end if

	li_giorno = 0	

	for li_tm = 1 to 12
			
	 	li_num_giorni = li_ar_num_giorni[li_tm]
   	li_num_giorni_bis = li_ar_num_giorni_bis[li_tm]
	
		if lb_anno_bis then	
			li_num_giorni = li_num_giorni_bis
		end if

		st_10.text = string(li_tm)

		for li_tg = 1 to li_num_giorni
			ld_data_giorno = datetime(date(string(li_tg) + "/" + string(li_tm) + "/"+string(li_anno)),time('00:00:00'))
			li_giorno_settimana = daynumber(date(ld_data_giorno)) - 1

			ll_numero_record++

			st_11.text = string(ll_numero_record)				
				
			if li_giorno_settimana = 0 then
				li_giorno_settimana = 7
			end if
		
			li_giorno = li_giorno + 1
			li_settimana = int(li_giorno/7) + 1
		
			li_num_ore = li_ar_num_ore[li_giorno]   
		   ls_flag_tipo_giorno = ls_ar_flag_tipo_giorno[li_giorno]  
    			
			if ls_flag_tipo_giorno = 'N' then 
				ls_flag_tipo_giorno = ls_ar_ferie_flag_tipo_giorno[li_tm,li_tg]	
			end if
				
			if isnull(li_num_ore) then
				li_num_ore = ar_set_num_ore[li_giorno_settimana]
			end if

			choose case ls_flag_tipo_giorno
				case "F"
					li_num_ore = 0
				case "S"
					li_num_ore = li_num_ore/2		
			end choose

			if ls_flag_tipo_giorno = 'N' then
				ls_flag_tipo_giorno = ar_set_tipo_giorno[li_giorno_settimana]
			end if

			INSERT INTO tab_cal_attivita  
  		      ( cod_azienda,   
     		     cod_attivita,   
        		  data_giorno,   
				  cod_cat_attrezzature,
				  cod_attrezzatura,	
	           anno,   
  		        settimana,   
     		     giorno,   
        		  giorno_settimana,   
	           num_ore,   
  		        num_ore_impegnate,   
     		     note )  
  			VALUES 
				( :s_cs_xx.cod_azienda,   
      	     :ls_cod_attivita,   
				  :ld_data_giorno,
				  :ls_cod_cat_attrezzature,
				  :ls_cod_attrezzatura,   
        		  :li_anno,   
   	        :li_settimana,   
  	   	     :li_giorno,   
     	   	  :li_giorno_settimana,   
      	     :li_num_ore,   
  	      	  0,   
         	  null ) ;


			if sqlca.sqlcode<>0 then
   			g_mb.messagebox("Errore nel DB",sqlca.SQLErrText,Information!)
				rollback;
				return
			end if

		next
	next

  commit;	

  st_2.width = ll_lunghezza * ll_t_1

next

lb_1.reset()

f_po_loadlb(lb_1, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_attivita not in (select cod_attivita from tab_cal_attivita where cod_azienda='" + s_cs_xx.cod_azienda + "' and anno =" + string(year(date(em_1.text + "-01-01"))) + ")","")

setpointer(arrow!)

st_2.height = st_4.height

if lb_test = false then
	g_mb.messagebox("Sep","Tabella calendario attività completata con successo.",Information!)
end if
end event

type cb_1 from uo_cb_close within w_generazione_cal
int X=1555
int Y=1361
int Width=366
int Height=81
int TabOrder=60
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_generazione_cal
int X=23
int Y=1281
int Width=1898
int Height=61
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=16711680
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_generazione_cal
int X=23
int Y=1281
int Width=1898
int Height=61
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

