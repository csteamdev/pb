﻿$PBExportHeader$w_parametri_mese.srw
$PBExportComments$Window Parametri Mese
forward
global type w_parametri_mese from w_cs_xx_principale
end type
type dw_lista_parametri_mese from uo_cs_xx_dw within w_parametri_mese
end type
type dw_parametri_mese from uo_cs_xx_dw within w_parametri_mese
end type
end forward

global type w_parametri_mese from w_cs_xx_principale
int Width=2387
int Height=641
boolean TitleBar=true
string Title="Parametri Mesi"
dw_lista_parametri_mese dw_lista_parametri_mese
dw_parametri_mese dw_parametri_mese
end type
global w_parametri_mese w_parametri_mese

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_lista_parametri_mese.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_parametri_mese.set_dw_options(sqlca,dw_lista_parametri_mese,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_lista_parametri_mese
end on

on w_parametri_mese.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_parametri_mese=create dw_lista_parametri_mese
this.dw_parametri_mese=create dw_parametri_mese
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_parametri_mese
this.Control[iCurrent+2]=dw_parametri_mese
end on

on w_parametri_mese.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_parametri_mese)
destroy(this.dw_parametri_mese)
end on

type dw_lista_parametri_mese from uo_cs_xx_dw within w_parametri_mese
int X=23
int Y=21
int Width=938
int Height=501
int TabOrder=10
string DataObject="d_lista_parametri_mese"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_parametri_mese from uo_cs_xx_dw within w_parametri_mese
int X=983
int Y=21
int Height=501
int TabOrder=20
string DataObject="d_parametri_mese"
BorderStyle BorderStyle=StyleRaised!
end type

