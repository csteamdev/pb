﻿$PBExportHeader$w_tab_attivita.srw
$PBExportComments$Gestione Tabella Attività
forward
global type w_tab_attivita from w_cs_xx_principale
end type
type dw_tab_attivita_lista from uo_cs_xx_dw within w_tab_attivita
end type
type dw_tab_attivita_dett from uo_cs_xx_dw within w_tab_attivita
end type
type cb_2 from commandbutton within w_tab_attivita
end type
type cb_g_tutte from commandbutton within w_tab_attivita
end type
end forward

global type w_tab_attivita from w_cs_xx_principale
int Width=2140
boolean TitleBar=true
string Title="Tabella Attività"
dw_tab_attivita_lista dw_tab_attivita_lista
dw_tab_attivita_dett dw_tab_attivita_dett
cb_2 cb_2
cb_g_tutte cb_g_tutte
end type
global w_tab_attivita w_tab_attivita

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_attivita_dett,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature","tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")



end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_attivita_lista.set_dw_key("cod_azienda")
dw_tab_attivita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_attivita_dett.set_dw_options(sqlca,dw_tab_attivita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_attivita_lista
end on

on w_tab_attivita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_attivita_lista=create dw_tab_attivita_lista
this.dw_tab_attivita_dett=create dw_tab_attivita_dett
this.cb_2=create cb_2
this.cb_g_tutte=create cb_g_tutte
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_attivita_lista
this.Control[iCurrent+2]=dw_tab_attivita_dett
this.Control[iCurrent+3]=cb_2
this.Control[iCurrent+4]=cb_g_tutte
end on

on w_tab_attivita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_attivita_lista)
destroy(this.dw_tab_attivita_dett)
destroy(this.cb_2)
destroy(this.cb_g_tutte)
end on

type dw_tab_attivita_lista from uo_cs_xx_dw within w_tab_attivita
int X=23
int Y=21
int Width=1669
int Height=501
int TabOrder=10
string DataObject="d_tab_attivita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_attivita_dett from uo_cs_xx_dw within w_tab_attivita
int X=23
int Y=541
int Width=2058
int Height=781
int TabOrder=20
string DataObject="d_tab_attivita_dett"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_2 from commandbutton within w_tab_attivita
int X=1715
int Y=121
int Width=366
int Height=81
int TabOrder=30
string Text="&G.C. Ore Att."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer  li_num_mese,li_num_giorno,li_giorno_settimana,li_num_ore,li_num_giorni,li_num_giorni_bis
integer  li_tm,li_tg,li_gg
string   ls_cod_attivita,ls_cod_cat_attrezzature,ls_des_attivita,ls_flag_attivita
string   ls_note,ls_flag_tipo_giorno,ls_des_giorno,ls_scadenza,ls_sql
datetime ld_data_giorno,ld_oggi
boolean  lb_test

setpointer(hourglass!)

lb_test = false
ls_cod_attivita=dw_tab_attivita_dett.GetItemString(dw_tab_attivita_dett.getrow(), "cod_attivita")  

DELETE FROM tab_ore_attivita  
WHERE cod_azienda = :s_cs_xx.cod_azienda
AND   cod_attivita = :ls_cod_attivita;

for li_tm=1 to 12
	SELECT tab_parametri_mesi.num_giorni,   
          tab_parametri_mesi.num_giorni_bis  
 	INTO 	 :li_num_giorni,   
          :li_num_giorni_bis  
	FROM tab_parametri_mesi  
	WHERE tab_parametri_mesi.num_mese = :li_tm;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("Sep","Errore nel DB:"+SQLCA.SQLErrText,stopsign!)
		return
		rollback;
	end if
	
	if sqlca.sqlcode=100 then
		g_mb.messagebox("Sep","Verificare la tabella parametri mesi.",stopsign!)
		return
		rollback;
	end if

	if li_tm=2 then
		li_num_giorni=li_num_giorni_bis
	end if	
	for li_tg=1 to li_num_giorni
		INSERT INTO tab_ore_attivita  
         ( cod_azienda,   
           cod_attivita,   
           num_mese,   
           num_giorno,   
           num_ore,   
           flag_tipo_giorno,   
           des_giorno,   
           scadenza )  
 		VALUES ( :s_cs_xx.cod_azienda,   
       		   :ls_cod_attivita,   
           		:li_tm,   
          	   :li_tg,   
               null,   
              	'N',   
               null,   
              	null );

		if sqlca.sqlcode<>0 then
			g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
			rollback;
			return
			lb_test = true
			exit
		end if
	next
next

setpointer(arrow!)

if lb_test = false then
	g_mb.messagebox("Sep","Tabella ore attività completata con successo.",Information!)
	commit;
end if
end event

type cb_g_tutte from commandbutton within w_tab_attivita
int X=1715
int Y=21
int Width=366
int Height=81
int TabOrder=40
string Text="&G.C. Tutte"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer  li_num_mese,li_num_giorno,li_giorno_settimana,li_num_ore,li_num_giorni,li_num_giorni_bis
integer  li_tm,li_tg,li_gg
string   ls_cod_attivita,ls_cod_cat_attrezzature,ls_des_attivita,ls_flag_attivita
string   ls_note,ls_flag_tipo_giorno,ls_des_giorno,ls_scadenza,ls_sql
datetime ld_data_giorno,ld_oggi
long ll_t, ll_numero_righe
boolean lb_test

setpointer(hourglass!)
ll_numero_righe = dw_tab_attivita_lista.rowcount()

for ll_t = 1 to ll_numero_righe
	dw_tab_attivita_lista.SetRow ( ll_t )
	lb_test = false
	ls_cod_attivita=dw_tab_attivita_dett.GetItemString(dw_tab_attivita_dett.getrow(), "cod_attivita")  

	DELETE 
	FROM  tab_ore_attivita  
	WHERE cod_azienda = :s_cs_xx.cod_azienda 
	AND   cod_attivita = :ls_cod_attivita;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
		return
	end if

	for li_tm=1 to 12
		SELECT tab_parametri_mesi.num_giorni,   
      	    tab_parametri_mesi.num_giorni_bis  
	 	INTO 	 :li_num_giorni,   
   	       :li_num_giorni_bis  
		FROM tab_parametri_mesi  
		WHERE tab_parametri_mesi.num_mese = :li_tm;

		if sqlca.sqlcode<0 then
			g_mb.messagebox("Sep","Errore nel DB" + SQLCA.SQLErrText,stopsign!)
			rollback;
			return;
		end if
		
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("Sep","Verificare la tabella parametri mesi.",stopsign!)
			rollback;
			return;
		end if

		if li_tm=2 then
			li_num_giorni=li_num_giorni_bis
		end if	
		
		for li_tg=1 to li_num_giorni
			INSERT INTO tab_ore_attivita  
         	( cod_azienda,   
           	  cod_attivita,   
	           num_mese,   
   	        num_giorno,   
      	     num_ore,   
         	  flag_tipo_giorno,   
	           des_giorno,   
   	        scadenza )  
 			VALUES ( :s_cs_xx.cod_azienda,   
       			   :ls_cod_attivita,   
           			:li_tm,   
          	   	:li_tg,   
	               null,   
   	           	'N',   
      	         null,   
         	     	null );
	
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)
				return
				rollback;
				lb_test = true
				exit
			end if
		next
	next
next

setpointer(arrow!)

if lb_test = false then
	g_mb.messagebox("Sep","Tabella ore attività completata con successo.",Information!)
	commit;
end if
end event

