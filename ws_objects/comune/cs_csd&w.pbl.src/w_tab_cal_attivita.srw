﻿$PBExportHeader$w_tab_cal_attivita.srw
$PBExportComments$Window Calendario Attività
forward
global type w_tab_cal_attivita from w_cs_xx_principale
end type
type dw_tab_cal_attivita_lista from uo_cs_xx_dw within w_tab_cal_attivita
end type
type cb_2 from commandbutton within w_tab_cal_attivita
end type
type dw_tab_cal_attivita_dett from uo_cs_xx_dw within w_tab_cal_attivita
end type
type ddlb_1 from dropdownlistbox within w_tab_cal_attivita
end type
type ddlb_2 from dropdownlistbox within w_tab_cal_attivita
end type
type st_1 from statictext within w_tab_cal_attivita
end type
type st_2 from statictext within w_tab_cal_attivita
end type
type st_3 from statictext within w_tab_cal_attivita
end type
type st_4 from statictext within w_tab_cal_attivita
end type
type st_5 from statictext within w_tab_cal_attivita
end type
type st_6 from statictext within w_tab_cal_attivita
end type
type st_7 from statictext within w_tab_cal_attivita
end type
type st_8 from statictext within w_tab_cal_attivita
end type
type st_9 from statictext within w_tab_cal_attivita
end type
type em_lunedi from editmask within w_tab_cal_attivita
end type
type em_martedi from editmask within w_tab_cal_attivita
end type
type em_mercoledi from editmask within w_tab_cal_attivita
end type
type em_giovedi from editmask within w_tab_cal_attivita
end type
type em_venerdi from editmask within w_tab_cal_attivita
end type
type em_sabato from editmask within w_tab_cal_attivita
end type
type em_domenica from editmask within w_tab_cal_attivita
end type
type st_10 from statictext within w_tab_cal_attivita
end type
type ln_1 from line within w_tab_cal_attivita
end type
type st_11 from statictext within w_tab_cal_attivita
end type
type em_da_data from editmask within w_tab_cal_attivita
end type
type em_a_data from editmask within w_tab_cal_attivita
end type
type st_12 from statictext within w_tab_cal_attivita
end type
type st_13 from statictext within w_tab_cal_attivita
end type
type cb_modifica from commandbutton within w_tab_cal_attivita
end type
type ddlb_cat_attrezzature from dropdownlistbox within w_tab_cal_attivita
end type
type st_14 from statictext within w_tab_cal_attivita
end type
type cb_calcola from commandbutton within w_tab_cal_attivita
end type
type em_lunedi_a from editmask within w_tab_cal_attivita
end type
type em_martedi_a from editmask within w_tab_cal_attivita
end type
type em_mercoledi_a from editmask within w_tab_cal_attivita
end type
type em_giovedi_a from editmask within w_tab_cal_attivita
end type
type em_venerdi_a from editmask within w_tab_cal_attivita
end type
type em_sabato_a from editmask within w_tab_cal_attivita
end type
type em_domenica_a from editmask within w_tab_cal_attivita
end type
type st_15 from statictext within w_tab_cal_attivita
end type
type st_16 from statictext within w_tab_cal_attivita
end type
type ln_2 from line within w_tab_cal_attivita
end type
type ln_3 from line within w_tab_cal_attivita
end type
type ln_4 from line within w_tab_cal_attivita
end type
type ln_5 from line within w_tab_cal_attivita
end type
type ln_6 from line within w_tab_cal_attivita
end type
type ln_7 from line within w_tab_cal_attivita
end type
type ln_8 from line within w_tab_cal_attivita
end type
type mle_1 from multilineedit within w_tab_cal_attivita
end type
end forward

global type w_tab_cal_attivita from w_cs_xx_principale
int Width=3347
int Height=1705
boolean TitleBar=true
string Title="Tabella Calendario Attività"
dw_tab_cal_attivita_lista dw_tab_cal_attivita_lista
cb_2 cb_2
dw_tab_cal_attivita_dett dw_tab_cal_attivita_dett
ddlb_1 ddlb_1
ddlb_2 ddlb_2
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
st_7 st_7
st_8 st_8
st_9 st_9
em_lunedi em_lunedi
em_martedi em_martedi
em_mercoledi em_mercoledi
em_giovedi em_giovedi
em_venerdi em_venerdi
em_sabato em_sabato
em_domenica em_domenica
st_10 st_10
ln_1 ln_1
st_11 st_11
em_da_data em_da_data
em_a_data em_a_data
st_12 st_12
st_13 st_13
cb_modifica cb_modifica
ddlb_cat_attrezzature ddlb_cat_attrezzature
st_14 st_14
cb_calcola cb_calcola
em_lunedi_a em_lunedi_a
em_martedi_a em_martedi_a
em_mercoledi_a em_mercoledi_a
em_giovedi_a em_giovedi_a
em_venerdi_a em_venerdi_a
em_sabato_a em_sabato_a
em_domenica_a em_domenica_a
st_15 st_15
st_16 st_16
ln_2 ln_2
ln_3 ln_3
ln_4 ln_4
ln_5 ln_5
ln_6 ln_6
ln_7 ln_7
ln_8 ln_8
mle_1 mle_1
end type
global w_tab_cal_attivita w_tab_cal_attivita

type variables
string is_cod_attivita
string is_cod_attrezzatura
end variables

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_cal_attivita_dett,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature","tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_cal_attivita_dett,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione","anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_cal_attivita_lista,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita","tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_cal_attivita_dett,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita","tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loadddlb(ddlb_1, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")

f_po_loadddlb(ddlb_cat_attrezzature, &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;string ls_risposta

dw_tab_cal_attivita_lista.set_dw_key("cod_azienda")
dw_tab_cal_attivita_lista.set_dw_options(sqlca, pcca.null_object, c_NoNew, c_default)
dw_tab_cal_attivita_dett.set_dw_options(sqlca, dw_tab_cal_attivita_lista, c_NoNew + c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_tab_cal_attivita_lista

is_cod_attivita="*"
end on

on w_tab_cal_attivita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_cal_attivita_lista=create dw_tab_cal_attivita_lista
this.cb_2=create cb_2
this.dw_tab_cal_attivita_dett=create dw_tab_cal_attivita_dett
this.ddlb_1=create ddlb_1
this.ddlb_2=create ddlb_2
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.st_7=create st_7
this.st_8=create st_8
this.st_9=create st_9
this.em_lunedi=create em_lunedi
this.em_martedi=create em_martedi
this.em_mercoledi=create em_mercoledi
this.em_giovedi=create em_giovedi
this.em_venerdi=create em_venerdi
this.em_sabato=create em_sabato
this.em_domenica=create em_domenica
this.st_10=create st_10
this.ln_1=create ln_1
this.st_11=create st_11
this.em_da_data=create em_da_data
this.em_a_data=create em_a_data
this.st_12=create st_12
this.st_13=create st_13
this.cb_modifica=create cb_modifica
this.ddlb_cat_attrezzature=create ddlb_cat_attrezzature
this.st_14=create st_14
this.cb_calcola=create cb_calcola
this.em_lunedi_a=create em_lunedi_a
this.em_martedi_a=create em_martedi_a
this.em_mercoledi_a=create em_mercoledi_a
this.em_giovedi_a=create em_giovedi_a
this.em_venerdi_a=create em_venerdi_a
this.em_sabato_a=create em_sabato_a
this.em_domenica_a=create em_domenica_a
this.st_15=create st_15
this.st_16=create st_16
this.ln_2=create ln_2
this.ln_3=create ln_3
this.ln_4=create ln_4
this.ln_5=create ln_5
this.ln_6=create ln_6
this.ln_7=create ln_7
this.ln_8=create ln_8
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_cal_attivita_lista
this.Control[iCurrent+2]=cb_2
this.Control[iCurrent+3]=dw_tab_cal_attivita_dett
this.Control[iCurrent+4]=ddlb_1
this.Control[iCurrent+5]=ddlb_2
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=st_2
this.Control[iCurrent+8]=st_3
this.Control[iCurrent+9]=st_4
this.Control[iCurrent+10]=st_5
this.Control[iCurrent+11]=st_6
this.Control[iCurrent+12]=st_7
this.Control[iCurrent+13]=st_8
this.Control[iCurrent+14]=st_9
this.Control[iCurrent+15]=em_lunedi
this.Control[iCurrent+16]=em_martedi
this.Control[iCurrent+17]=em_mercoledi
this.Control[iCurrent+18]=em_giovedi
this.Control[iCurrent+19]=em_venerdi
this.Control[iCurrent+20]=em_sabato
this.Control[iCurrent+21]=em_domenica
this.Control[iCurrent+22]=st_10
this.Control[iCurrent+23]=ln_1
this.Control[iCurrent+24]=st_11
this.Control[iCurrent+25]=em_da_data
this.Control[iCurrent+26]=em_a_data
this.Control[iCurrent+27]=st_12
this.Control[iCurrent+28]=st_13
this.Control[iCurrent+29]=cb_modifica
this.Control[iCurrent+30]=ddlb_cat_attrezzature
this.Control[iCurrent+31]=st_14
this.Control[iCurrent+32]=cb_calcola
this.Control[iCurrent+33]=em_lunedi_a
this.Control[iCurrent+34]=em_martedi_a
this.Control[iCurrent+35]=em_mercoledi_a
this.Control[iCurrent+36]=em_giovedi_a
this.Control[iCurrent+37]=em_venerdi_a
this.Control[iCurrent+38]=em_sabato_a
this.Control[iCurrent+39]=em_domenica_a
this.Control[iCurrent+40]=st_15
this.Control[iCurrent+41]=st_16
this.Control[iCurrent+42]=ln_2
this.Control[iCurrent+43]=ln_3
this.Control[iCurrent+44]=ln_4
this.Control[iCurrent+45]=ln_5
this.Control[iCurrent+46]=ln_6
this.Control[iCurrent+47]=ln_7
this.Control[iCurrent+48]=ln_8
this.Control[iCurrent+49]=mle_1
end on

on w_tab_cal_attivita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_cal_attivita_lista)
destroy(this.cb_2)
destroy(this.dw_tab_cal_attivita_dett)
destroy(this.ddlb_1)
destroy(this.ddlb_2)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_7)
destroy(this.st_8)
destroy(this.st_9)
destroy(this.em_lunedi)
destroy(this.em_martedi)
destroy(this.em_mercoledi)
destroy(this.em_giovedi)
destroy(this.em_venerdi)
destroy(this.em_sabato)
destroy(this.em_domenica)
destroy(this.st_10)
destroy(this.ln_1)
destroy(this.st_11)
destroy(this.em_da_data)
destroy(this.em_a_data)
destroy(this.st_12)
destroy(this.st_13)
destroy(this.cb_modifica)
destroy(this.ddlb_cat_attrezzature)
destroy(this.st_14)
destroy(this.cb_calcola)
destroy(this.em_lunedi_a)
destroy(this.em_martedi_a)
destroy(this.em_mercoledi_a)
destroy(this.em_giovedi_a)
destroy(this.em_venerdi_a)
destroy(this.em_sabato_a)
destroy(this.em_domenica_a)
destroy(this.st_15)
destroy(this.st_16)
destroy(this.ln_2)
destroy(this.ln_3)
destroy(this.ln_4)
destroy(this.ln_5)
destroy(this.ln_6)
destroy(this.ln_7)
destroy(this.ln_8)
destroy(this.mle_1)
end on

type dw_tab_cal_attivita_lista from uo_cs_xx_dw within w_tab_cal_attivita
int X=23
int Y=21
int Width=1989
int Height=581
int TabOrder=230
string DataObject="d_tab_cal_attivita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_attivita, is_cod_attrezzatura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if this.getrow()>0 then
	choose case this.getitemnumber(this.getrow(),"giorno_settimana")
		case 1
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Lunedì'")
		case 2
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Martedì'")
		case 3
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Mercoledì'")
		case 4
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Giovedì'")
		case 5
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Venerdì'")
		case 6
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Sabato'")
		case 7
			dw_tab_cal_attivita_dett.modify("st_nome_giorno.text='Domenica'")
	end choose
end if
end on

type cb_2 from commandbutton within w_tab_cal_attivita
int X=2149
int Y=1281
int Width=366
int Height=81
int TabOrder=220
string Text="&Rip. Cal."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;string ls_cod_attivita

setpointer(hourglass!)

ls_cod_attivita=dw_tab_cal_attivita_lista.getitemstring(dw_tab_cal_attivita_lista.getrow(),"cod_attivita")

UPDATE tab_cal_attivita  
SET    num_ore_impegnate = 0  
WHERE  tab_cal_attivita.cod_azienda = :s_cs_xx.cod_azienda 
AND    tab_cal_attivita.cod_attivita = :ls_cod_attivita;

setpointer(arrow!)
end on

type dw_tab_cal_attivita_dett from uo_cs_xx_dw within w_tab_cal_attivita
int X=23
int Y=621
int Width=1989
int Height=961
int TabOrder=240
string DataObject="d_tab_cal_attivita_dett"
BorderStyle BorderStyle=StyleRaised!
end type

type ddlb_1 from dropdownlistbox within w_tab_cal_attivita
int X=2263
int Y=1401
int Width=1029
int Height=981
int TabOrder=200
boolean VScrollBar=true
int Limit=10
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;string ls_cod_cat_attrezzature

is_cod_attivita=f_po_selectddlb(ddlb_1)

SELECT cod_cat_attrezzature 
INTO   :ls_cod_cat_attrezzature
FROM   tab_attivita
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_attivita = :is_cod_attivita;

ddlb_2.reset()

f_po_loadddlb(ddlb_2, &
                 sqlca, &
                 "anag_attrezzature", &
                 "cod_attrezzatura", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_attrezzature = '" + ls_cod_cat_attrezzature + "'","")


dw_tab_cal_attivita_lista.change_dw_focus(dw_tab_cal_attivita_lista)
parent.triggerevent("pc_retrieve")
end on

type ddlb_2 from dropdownlistbox within w_tab_cal_attivita
int X=2263
int Y=1501
int Width=1029
int Height=941
int TabOrder=210
boolean VScrollBar=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;is_cod_attrezzatura=f_po_selectddlb(ddlb_2)
dw_tab_cal_attivita_lista.change_dw_focus(dw_tab_cal_attivita_lista)
parent.triggerevent("pc_retrieve")
end on

type st_1 from statictext within w_tab_cal_attivita
int X=2035
int Y=1401
int Width=229
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Attività:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_tab_cal_attivita
int X=2035
int Y=1501
int Width=229
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Risorsa:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_tab_cal_attivita
int X=2058
int Y=321
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Mercoledì"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_tab_cal_attivita
int X=2058
int Y=241
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Martedì"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_tab_cal_attivita
int X=2058
int Y=401
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Giovedì"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_tab_cal_attivita
int X=2058
int Y=161
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Lunedì"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_tab_cal_attivita
int X=2058
int Y=481
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Venerdì"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_tab_cal_attivita
int X=2058
int Y=561
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Sabato"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_9 from statictext within w_tab_cal_attivita
int X=2058
int Y=641
int Width=270
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Domenica"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_lunedi from editmask within w_tab_cal_attivita
int X=2446
int Y=161
int Width=229
int Height=61
int TabOrder=10
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_martedi from editmask within w_tab_cal_attivita
int X=2446
int Y=241
int Width=229
int Height=61
int TabOrder=40
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_mercoledi from editmask within w_tab_cal_attivita
int X=2446
int Y=321
int Width=229
int Height=61
int TabOrder=60
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_giovedi from editmask within w_tab_cal_attivita
int X=2446
int Y=401
int Width=229
int Height=61
int TabOrder=80
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_venerdi from editmask within w_tab_cal_attivita
int X=2446
int Y=481
int Width=229
int Height=61
int TabOrder=100
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_sabato from editmask within w_tab_cal_attivita
int X=2446
int Y=561
int Width=229
int Height=61
int TabOrder=120
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_domenica from editmask within w_tab_cal_attivita
int X=2446
int Y=641
int Width=229
int Height=61
int TabOrder=140
boolean BringToTop=true
string Mask="##"
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_tab_cal_attivita
int X=2058
int Y=61
int Width=247
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Giorno"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ln_1 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2332
int BeginY=61
int EndX=2332
int EndY=721
int LineThickness=5
long LineColor=8388608
end type

type st_11 from statictext within w_tab_cal_attivita
int X=2721
int Y=21
int Width=183
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Ore"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_da_data from editmask within w_tab_cal_attivita
int X=2149
int Y=861
int Width=389
int Height=81
int TabOrder=170
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_a_data from editmask within w_tab_cal_attivita
int X=2675
int Y=861
int Width=389
int Height=81
int TabOrder=180
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_12 from statictext within w_tab_cal_attivita
int X=2035
int Y=881
int Width=115
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="da"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_13 from statictext within w_tab_cal_attivita
int X=2583
int Y=881
int Width=92
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="a"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_modifica from commandbutton within w_tab_cal_attivita
int X=2926
int Y=1281
int Width=366
int Height=81
int TabOrder=190
boolean BringToTop=true
string Text="&Modifica"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_cat_attrezzature
datetime ldt_da_data,ldt_a_data
integer li_ore_lunedi,li_ore_martedi,li_ore_mercoledi,li_ore_giovedi,li_ore_venerdi,li_ore_sabato,li_ore_domenica

setpointer(hourglass!)

ls_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)
ldt_da_data = datetime(date(em_da_data.text), 00:00:00)
ldt_a_data = datetime(date(em_a_data.text), 00:00:00)

li_ore_lunedi = integer(em_lunedi.text)
li_ore_martedi = integer(em_martedi.text)
li_ore_mercoledi = integer(em_mercoledi.text)
li_ore_giovedi = integer(em_giovedi.text)
li_ore_venerdi = integer(em_venerdi.text)
li_ore_sabato = integer(em_sabato.text)
li_ore_domenica = integer(em_domenica.text)

update tab_cal_attivita
set 	 num_ore =:li_ore_lunedi
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=1;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if
	
update tab_cal_attivita
set 	 num_ore =:li_ore_martedi
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=2;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if

update tab_cal_attivita
set 	 num_ore =:li_ore_mercoledi
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=3;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if


update tab_cal_attivita
set 	 num_ore =:li_ore_giovedi
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=4;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if

update tab_cal_attivita
set 	 num_ore =:li_ore_venerdi
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=5;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if

update tab_cal_attivita
set 	 num_ore =:li_ore_sabato
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=6;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if

update tab_cal_attivita
set 	 num_ore =:li_ore_domenica
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_cat_attrezzature =:ls_cod_cat_attrezzature
and    data_giorno between :ldt_da_data and :ldt_a_data
and    giorno_settimana=7;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	rollback;
	return
end if


commit;

setpointer(arrow!)

end event

type ddlb_cat_attrezzature from dropdownlistbox within w_tab_cal_attivita
int X=2652
int Y=741
int Width=641
int Height=781
int TabOrder=160
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_14 from statictext within w_tab_cal_attivita
int X=2035
int Y=761
int Width=613
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Categoria Attrezzatura:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_calcola from commandbutton within w_tab_cal_attivita
int X=2538
int Y=1281
int Width=366
int Height=81
int TabOrder=150
boolean BringToTop=true
string Text="&Calcola"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_cat_attrezzature
datetime ldt_da_data,ldt_a_data
integer li_ore_lunedi,li_ore_martedi,li_ore_mercoledi,li_ore_giovedi,li_ore_venerdi,li_ore_sabato,li_ore_domenica
double ldd_ore_lunedi,ldd_ore_martedi,ldd_ore_mercoledi,ldd_ore_giovedi,ldd_ore_venerdi,ldd_ore_sabato,ldd_ore_domenica

setpointer(hourglass!)

ls_cod_cat_attrezzature = f_po_selectddlb(ddlb_cat_attrezzature)
ldt_da_data = datetime(date(em_da_data.text), 00:00:00)
ldt_a_data = datetime(date(em_a_data.text), 00:00:00)

select avg(num_ore)
into   :ldd_ore_lunedi
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=1;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if

select avg(num_ore)
into   :ldd_ore_martedi
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=2;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if


select avg(num_ore)
into   :ldd_ore_mercoledi
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=3;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if


select avg(num_ore)
into   :ldd_ore_giovedi
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=4;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if

select avg(num_ore)
into   :ldd_ore_venerdi
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=5;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if

select avg(num_ore)
into   :ldd_ore_sabato
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=6;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if

select avg(num_ore)
into   :ldd_ore_domenica
from  tab_cal_attivita
where cod_azienda =:s_cs_xx.cod_azienda
and   cod_cat_attrezzature =:ls_cod_cat_attrezzature
and   data_giorno between :ldt_da_data and :ldt_a_data
and   giorno_settimana=7;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul db: " + sqlca.sqlerrtext)
	return
end if

em_lunedi_a.text = string(ldd_ore_lunedi)
em_martedi_a.text = string(ldd_ore_martedi)
em_mercoledi_a.text = string(ldd_ore_mercoledi)
em_giovedi_a.text = string(ldd_ore_giovedi)
em_venerdi_a.text = string(ldd_ore_venerdi)
em_sabato_a.text = string(ldd_ore_sabato)
em_domenica_a.text = string(ldd_ore_domenica)

setpointer(arrow!)
end event

type em_lunedi_a from editmask within w_tab_cal_attivita
int X=2926
int Y=161
int Width=229
int Height=61
int TabOrder=20
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_martedi_a from editmask within w_tab_cal_attivita
int X=2926
int Y=241
int Width=229
int Height=61
int TabOrder=30
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_mercoledi_a from editmask within w_tab_cal_attivita
int X=2926
int Y=321
int Width=229
int Height=61
int TabOrder=50
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_giovedi_a from editmask within w_tab_cal_attivita
int X=2926
int Y=401
int Width=229
int Height=61
int TabOrder=70
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_venerdi_a from editmask within w_tab_cal_attivita
int X=2926
int Y=481
int Width=229
int Height=61
int TabOrder=90
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_sabato_a from editmask within w_tab_cal_attivita
int X=2926
int Y=561
int Width=229
int Height=61
int TabOrder=110
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_domenica_a from editmask within w_tab_cal_attivita
int X=2926
int Y=641
int Width=229
int Height=61
int TabOrder=130
boolean BringToTop=true
string Mask="##"
string DisplayData=""
boolean DisplayOnly=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_15 from statictext within w_tab_cal_attivita
int X=2355
int Y=81
int Width=458
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Nuova Impostazione"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_16 from statictext within w_tab_cal_attivita
int X=2835
int Y=81
int Width=467
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Impostazione Attuale"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ln_2 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=221
int EndX=3269
int EndY=221
int LineThickness=5
long LineColor=8388608
end type

type ln_3 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=301
int EndX=3269
int EndY=301
int LineThickness=5
long LineColor=8388608
end type

type ln_4 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2812
int BeginY=81
int EndX=2812
int EndY=721
int LineThickness=5
long LineColor=8388608
end type

type ln_5 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=381
int EndX=3269
int EndY=381
int LineThickness=5
long LineColor=8388608
end type

type ln_6 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=461
int EndX=3269
int EndY=461
int LineThickness=5
long LineColor=8388608
end type

type ln_7 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=541
int EndX=3269
int EndY=541
int LineThickness=5
long LineColor=8388608
end type

type ln_8 from line within w_tab_cal_attivita
boolean Enabled=false
int BeginX=2035
int BeginY=621
int EndX=3269
int EndY=621
int LineThickness=5
long LineColor=8388608
end type

type mle_1 from multilineedit within w_tab_cal_attivita
int X=2035
int Y=961
int Width=1258
int Height=301
int TabOrder=161
boolean BringToTop=true
boolean Border=false
string Text="Il bottone calcola, consente di ottenere la media delle ore lavorate per giorno della cat.attrezzatura selezionata (colonna Impostazione Attuale). Il bottone modifica imposta le ore sulla cat.attrezzature selezionata in base alla colonna Nuova Impostazione."
long TextColor=255
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

