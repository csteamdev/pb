﻿$PBExportHeader$w_tab_cal_ferie.srw
$PBExportComments$Window Calendario Ferie
forward
global type w_tab_cal_ferie from w_cs_xx_principale
end type
type dw_tab_cal_ferie_lista from uo_cs_xx_dw within w_tab_cal_ferie
end type
end forward

global type w_tab_cal_ferie from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2117
integer height = 804
string title = "Calendario Festività"
dw_tab_cal_ferie_lista dw_tab_cal_ferie_lista
end type
global w_tab_cal_ferie w_tab_cal_ferie

event pc_setwindow;call super::pc_setwindow;dw_tab_cal_ferie_lista.set_dw_key("cod_azienda")
dw_tab_cal_ferie_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tab_cal_ferie_lista
end event

on w_tab_cal_ferie.create
int iCurrent
call super::create
this.dw_tab_cal_ferie_lista=create dw_tab_cal_ferie_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_cal_ferie_lista
end on

on w_tab_cal_ferie.destroy
call super::destroy
destroy(this.dw_tab_cal_ferie_lista)
end on

type dw_tab_cal_ferie_lista from uo_cs_xx_dw within w_tab_cal_ferie
integer x = 23
integer y = 20
integer width = 2034
integer height = 660
integer taborder = 10
string dataobject = "d_tab_cal_ferie_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

