﻿$PBExportHeader$w_tab_cal_settimana.srw
$PBExportComments$Window Calendario Settimana
forward
global type w_tab_cal_settimana from w_cs_xx_principale
end type
type dw_tab_cal_settimana_lista from uo_cs_xx_dw within w_tab_cal_settimana
end type
type dw_tab_cal_settimana_dett from uo_cs_xx_dw within w_tab_cal_settimana
end type
end forward

global type w_tab_cal_settimana from w_cs_xx_principale
int Width=2049
int Height=709
boolean TitleBar=true
string Title="Tabella Calendario Settimana"
dw_tab_cal_settimana_lista dw_tab_cal_settimana_lista
dw_tab_cal_settimana_dett dw_tab_cal_settimana_dett
end type
global w_tab_cal_settimana w_tab_cal_settimana

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_cal_settimana_lista.set_dw_key("cod_azienda")
dw_tab_cal_settimana_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_cal_settimana_dett.set_dw_options(sqlca,dw_tab_cal_settimana_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_cal_settimana_lista
end on

on w_tab_cal_settimana.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_cal_settimana_lista=create dw_tab_cal_settimana_lista
this.dw_tab_cal_settimana_dett=create dw_tab_cal_settimana_dett
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_cal_settimana_lista
this.Control[iCurrent+2]=dw_tab_cal_settimana_dett
end on

on w_tab_cal_settimana.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_cal_settimana_lista)
destroy(this.dw_tab_cal_settimana_dett)
end on

type dw_tab_cal_settimana_lista from uo_cs_xx_dw within w_tab_cal_settimana
int X=23
int Y=21
int Width=618
int Height=561
int TabOrder=10
string DataObject="d_tab_cal_settimana_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_tab_cal_settimana_dett from uo_cs_xx_dw within w_tab_cal_settimana
int X=663
int Y=21
int Width=1326
int Height=561
int TabOrder=20
string DataObject="d_tab_cal_settimana_dett"
BorderStyle BorderStyle=StyleRaised!
end type

