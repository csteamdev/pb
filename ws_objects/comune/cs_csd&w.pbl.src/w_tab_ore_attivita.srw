﻿$PBExportHeader$w_tab_ore_attivita.srw
$PBExportComments$Window Ore Attività
forward
global type w_tab_ore_attivita from w_cs_xx_principale
end type
type dw_tab_ore_attivita_lista from uo_cs_xx_dw within w_tab_ore_attivita
end type
type dw_tab_ore_attivita_dett from uo_cs_xx_dw within w_tab_ore_attivita
end type
type cb_1 from uo_cb_close within w_tab_ore_attivita
end type
type ddlb_1 from dropdownlistbox within w_tab_ore_attivita
end type
end forward

global type w_tab_ore_attivita from w_cs_xx_principale
int Width=2049
int Height=1693
boolean TitleBar=true
string Title="Tabella Calendario Ore Attività"
dw_tab_ore_attivita_lista dw_tab_ore_attivita_lista
dw_tab_ore_attivita_dett dw_tab_ore_attivita_dett
cb_1 cb_1
ddlb_1 ddlb_1
end type
global w_tab_ore_attivita w_tab_ore_attivita

type variables
string is_cod_attivita
end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tab_ore_attivita_lista.set_dw_key("cod_azienda")
dw_tab_ore_attivita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_ore_attivita_dett.set_dw_options(sqlca,dw_tab_ore_attivita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_ore_attivita_lista

is_cod_attivita="*"
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_ore_attivita_lista,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita","tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_ore_attivita_dett,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita","tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loadddlb(ddlb_1, &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")


end on

on w_tab_ore_attivita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_ore_attivita_lista=create dw_tab_ore_attivita_lista
this.dw_tab_ore_attivita_dett=create dw_tab_ore_attivita_dett
this.cb_1=create cb_1
this.ddlb_1=create ddlb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_ore_attivita_lista
this.Control[iCurrent+2]=dw_tab_ore_attivita_dett
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=ddlb_1
end on

on w_tab_ore_attivita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_ore_attivita_lista)
destroy(this.dw_tab_ore_attivita_dett)
destroy(this.cb_1)
destroy(this.ddlb_1)
end on

type dw_tab_ore_attivita_lista from uo_cs_xx_dw within w_tab_ore_attivita
int X=23
int Y=21
int Width=1943
int Height=441
int TabOrder=10
string DataObject="d_tab_ore_attivita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error =  Retrieve(s_cs_xx.cod_azienda, is_cod_attivita)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type dw_tab_ore_attivita_dett from uo_cs_xx_dw within w_tab_ore_attivita
int X=23
int Y=481
int Width=1966
int Height=981
int TabOrder=20
string DataObject="d_tab_ore_attivita_dett"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_1 from uo_cb_close within w_tab_ore_attivita
int X=1623
int Y=1481
int Width=366
int Height=81
int TabOrder=40
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_1 from dropdownlistbox within w_tab_ore_attivita
int X=23
int Y=1481
int Width=1143
int Height=881
int TabOrder=30
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;is_cod_attivita = f_po_selectddlb(ddlb_1)
dw_tab_ore_attivita_lista.change_dw_focus(dw_tab_ore_attivita_lista)
parent.triggerevent("pc_retrieve")
end on

