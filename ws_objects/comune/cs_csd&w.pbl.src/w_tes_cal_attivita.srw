﻿$PBExportHeader$w_tes_cal_attivita.srw
$PBExportComments$Finestra tes_cal_attivita
forward
global type w_tes_cal_attivita from w_cs_xx_principale
end type
type dw_tes_cal_attivita_lista from uo_cs_xx_dw within w_tes_cal_attivita
end type
type cb_dettagli from commandbutton within w_tes_cal_attivita
end type
type dw_tes_cal_attivita_det from uo_cs_xx_dw within w_tes_cal_attivita
end type
end forward

global type w_tes_cal_attivita from w_cs_xx_principale
int Width=2158
int Height=1861
boolean TitleBar=true
string Title="Calendario Attività"
dw_tes_cal_attivita_lista dw_tes_cal_attivita_lista
cb_dettagli cb_dettagli
dw_tes_cal_attivita_det dw_tes_cal_attivita_det
end type
global w_tes_cal_attivita w_tes_cal_attivita

type variables
boolean ib_delete=true
string is_attivita, is_cat_attrezzature, is_attrezzatura
datetime id_data_giorno
end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_tes_cal_attivita_det,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_tes_cal_attivita_det,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tes_cal_attivita_det,"cod_attivita",sqlca,&
                 "tab_attivita","cod_attivita","des_attivita",&
                 "tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;//dw_tes_fasi_lavorazione_lista.ib_proteggi_chiavi = false
//dw_tes_fasi_lavorazione_det_1.ib_proteggi_chiavi = false

dw_tes_cal_attivita_lista.set_dw_key("cod_azienda")
dw_tes_cal_attivita_lista.set_dw_options(sqlca,pcca.null_object, c_default, c_default)
dw_tes_cal_attivita_det.set_dw_options(sqlca,dw_tes_cal_attivita_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_cal_attivita_lista


end on

on w_tes_cal_attivita.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_cal_attivita_lista=create dw_tes_cal_attivita_lista
this.cb_dettagli=create cb_dettagli
this.dw_tes_cal_attivita_det=create dw_tes_cal_attivita_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_cal_attivita_lista
this.Control[iCurrent+2]=cb_dettagli
this.Control[iCurrent+3]=dw_tes_cal_attivita_det
end on

on w_tes_cal_attivita.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_cal_attivita_lista)
destroy(this.cb_dettagli)
destroy(this.dw_tes_cal_attivita_det)
end on

type dw_tes_cal_attivita_lista from uo_cs_xx_dw within w_tes_cal_attivita
int X=23
int Y=21
int Width=2081
int Height=501
int TabOrder=30
string DataObject="d_tes_cal_attivita_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_view;call super::pcd_view;if dw_tes_cal_attivita_lista.getrow() > 0 then
   is_attivita = dw_tes_cal_attivita_lista.getitemstring(dw_tes_cal_attivita_lista.getrow(), "cod_attivita")
   is_cat_attrezzature = dw_tes_cal_attivita_lista.getitemstring(dw_tes_cal_attivita_lista.getrow(), "cod_cat_attrezzature")
   is_attrezzatura = dw_tes_cal_attivita_lista.getitemstring(dw_tes_cal_attivita_lista.getrow(), "cod_attrezzatura")
   id_data_giorno = dw_tes_cal_attivita_lista.getitemdatetime(dw_tes_cal_attivita_lista.getrow(), "data_giorno")
end if

cb_dettagli.enabled = true

end event

on updatestart;call uo_cs_xx_dw::updatestart;//if ib_delete = true then
//   DELETE FROM det_fasi_lavorazione
//          WHERE (det_fasi_lavorazione.cod_azienda = :s_cs_xx.cod_azienda) and
//                (det_fasi_lavorazione.cod_prodotto = :is_prodotto) and
//                (det_fasi_lavorazione.cod_reparto = :is_reparto) and
//                (det_fasi_lavorazione.cod_lavorazione = :is_lavorazione);
//end if
                
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_dettagli.enabled = false
ib_delete = true
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_dettagli.enabled = false

end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_dettagli.enabled = false

end on

type cb_dettagli from commandbutton within w_tes_cal_attivita
int X=1738
int Y=1661
int Width=371
int Height=81
int TabOrder=20
boolean Enabled=false
string Text="Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if not isvalid(w_det_fasi_lavorazione) then
	window_open_parm(w_det_cal_attivita, -1, dw_tes_cal_attivita_lista)
end if
end on

type dw_tes_cal_attivita_det from uo_cs_xx_dw within w_tes_cal_attivita
int X=23
int Y=541
int Width=2081
int Height=1101
int TabOrder=10
string DataObject="d_tes_cal_attivita_det"
BorderStyle BorderStyle=StyleRaised!
end type

