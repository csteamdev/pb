﻿$PBExportHeader$w_con_dipendenti.srw
$PBExportComments$Finestra Gestione Controllo Dipendenti
forward
global type w_con_dipendenti from w_cs_xx_principale
end type
type dw_con_dipendenti from uo_cs_xx_dw within w_con_dipendenti
end type
end forward

global type w_con_dipendenti from w_cs_xx_principale
int Width=1934
int Height=1349
boolean TitleBar=true
string Title="Gestione Parametri Dipendenti"
dw_con_dipendenti dw_con_dipendenti
end type
global w_con_dipendenti w_con_dipendenti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_dipendenti.set_dw_key("cod_azienda")
dw_con_dipendenti.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)

end on

on w_con_dipendenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_dipendenti=create dw_con_dipendenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_dipendenti
end on

on w_con_dipendenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_dipendenti)
end on

type dw_con_dipendenti from uo_cs_xx_dw within w_con_dipendenti
int X=23
int Y=21
int Width=1852
int Height=1201
string DataObject="d_con_dipendenti"
BorderStyle BorderStyle=StyleRaised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

