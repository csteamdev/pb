﻿$PBExportHeader$w_contratti_dip.srw
$PBExportComments$Finestra Gestione Contratti Dipendenti
forward
global type w_contratti_dip from w_cs_xx_principale
end type
type dw_contratti_dip from uo_cs_xx_dw within w_contratti_dip
end type
end forward

global type w_contratti_dip from w_cs_xx_principale
int Width=3667
int Height=1149
boolean TitleBar=true
string Title="Gestione Contratti Dipendenti"
dw_contratti_dip dw_contratti_dip
end type
global w_contratti_dip w_contratti_dip

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_contratti_dip,"cod_attivita", &
                 sqlca,&
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_contratti_dip.set_dw_key("cod_azienda")
dw_contratti_dip.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)
   
end on

on w_contratti_dip.create
int iCurrent
call w_cs_xx_principale::create
this.dw_contratti_dip=create dw_contratti_dip
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_contratti_dip
end on

on w_contratti_dip.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_contratti_dip)
end on

type dw_contratti_dip from uo_cs_xx_dw within w_contratti_dip
int X=23
int Y=21
int Width=3589
int Height=1001
string DataObject="d_contratti_dip"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

