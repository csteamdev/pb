﻿$PBExportHeader$w_converti_corsi_sessioni.srw
forward
global type w_converti_corsi_sessioni from window
end type
type st_2 from statictext within w_converti_corsi_sessioni
end type
type st_1 from statictext within w_converti_corsi_sessioni
end type
type cb_1 from commandbutton within w_converti_corsi_sessioni
end type
end forward

global type w_converti_corsi_sessioni from window
integer width = 1477
integer height = 604
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_2 st_2
st_1 st_1
cb_1 cb_1
end type
global w_converti_corsi_sessioni w_converti_corsi_sessioni

on w_converti_corsi_sessioni.create
this.st_2=create st_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.st_2,&
this.st_1,&
this.cb_1}
end on

on w_converti_corsi_sessioni.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

type st_2 from statictext within w_converti_corsi_sessioni
integer x = 23
integer y = 128
integer width = 1394
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Adeguamento alla nuova struttura per Sessioni"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_converti_corsi_sessioni
integer x = 14
integer y = 16
integer width = 1403
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Conversione Dati Corsi di Formazione"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_converti_corsi_sessioni
integer x = 512
integer y = 280
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;string ls_cod_corso, ls_cod_operaio, ls_cod_fornitore, ls_cod_area_aziendale, ls_note, ls_luogo
datetime ldt_data_inizio_pre, ldt_data_fine_pre, ldt_data_inizio_eff, ldt_data_fine_eff
DECLARE cu_corsi CURSOR FOR  
SELECT cod_corso, 
		cod_operaio,   
		cod_fornitore,   
		cod_area_aziendale,   
		data_inizio_pre,   
		data_fine_pre,   
		data_inizio_eff,   
		data_fine_eff,   
		note,   
		luogo  
 FROM tab_corsi  ;

open cu_corsi;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_corsi.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_corsi into :ls_cod_corso, :ls_cod_operaio, :ls_cod_fornitore, :ls_cod_area_aziendale, :ldt_data_inizio_pre, :ldt_data_fine_pre, :ldt_data_inizio_eff, :ldt_data_fine_eff, :ls_note, :ls_luogo;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_corsi.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	INSERT INTO tab_corsi_sessioni  
			( cod_azienda,   
			  cod_corso,   
			  progressivo,   
			  cod_area_aziendale,   
			  sede_corso,   
			  cod_operaio,   
			  cod_fornitore,   
			  data_inizio_prevista,   
			  data_fine_prevista,   
			  data_inizio_effettiva,   
			  data_fine_effettiva,   
			  note )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_corso,   
			  1,   
			  :ls_cod_area_aziendale,   
			  :ls_luogo,   
			  :ls_cod_operaio,   
			  :ls_cod_fornitore,   
			  :ldt_data_inizio_pre,   
			  :ldt_data_fine_pre,   
			  :ldt_data_inizio_eff,   
			  :ldt_data_fine_eff,   
			  :ls_note )  ;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in INSERT tabella CORSI_SESSIONI.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
			  
loop

commit;

end event

