﻿$PBExportHeader$w_corsi.srw
$PBExportComments$Finestra Gestione Corsi
forward
global type w_corsi from w_cs_xx_principale
end type
type cb_note_1 from uo_ext_note_mss_1 within w_corsi
end type
type cb_allinea from commandbutton within w_corsi
end type
type cb_2 from commandbutton within w_corsi
end type
type cb_1 from commandbutton within w_corsi
end type
type cb_reset from commandbutton within w_corsi
end type
type cb_ricerca from commandbutton within w_corsi
end type
type dw_corsi_det from uo_cs_xx_dw within w_corsi
end type
type cb_corsi_propedeutici from commandbutton within w_corsi
end type
type cb_documenti from commandbutton within w_corsi
end type
type dw_corsi_lista from uo_cs_xx_dw within w_corsi
end type
type dw_folder_search from u_folder within w_corsi
end type
type dw_ricerca from u_dw_search within w_corsi
end type
end forward

global type w_corsi from w_cs_xx_principale
integer width = 2569
integer height = 2180
string title = "Corsi"
cb_note_1 cb_note_1
cb_allinea cb_allinea
cb_2 cb_2
cb_1 cb_1
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_corsi_det dw_corsi_det
cb_corsi_propedeutici cb_corsi_propedeutici
cb_documenti cb_documenti
dw_corsi_lista dw_corsi_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_corsi w_corsi

type variables
boolean ib_codice_progressivo
string is_sql_select
end variables

on pc_delete;call w_cs_xx_principale::pc_delete;cb_corsi_propedeutici.enabled=false
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca, &
                 "rs_cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_po_loaddddw_dw(dw_ricerca, &
                 "rs_cod_corso", &
                 sqlca, &
                 "tab_corsi", &
                 "cod_corso", &
                 "des_corso", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  

//f_po_loaddddw_dw(dw_corsi_det, &
//                 "cod_operaio", &
//                 sqlca, &
//                 "anag_operai", &
//                 "cod_operaio", &
//                 "cognome + ' ' + nome", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_po_loaddddw_dw(dw_corsi_det, &
//                 "cod_fornitore", &
//                 sqlca, &
//                 "anag_fornitori", &
//                 "cod_fornitore", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
//f_po_loaddddw_dw(dw_corsi_det, &
//                 "cod_area_aziendale", &
//                 sqlca, &
//                 "tab_aree_aziendali", &
//                 "cod_area_aziendale", &
//                 "des_area", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  

end event

event pc_setwindow;call super::pc_setwindow;string       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

windowobject lw_oggetti[],l_objects[]

dw_corsi_lista.set_dw_key("cod_azienda")

dw_corsi_lista.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default + c_NoRetrieveOnOpen, &
                              c_default)
dw_corsi_det.set_dw_options(sqlca, &
                            dw_corsi_lista, &
                            c_sharedata + c_scrollparent, &
                            c_default)
iuo_dw_main = dw_corsi_lista

cb_corsi_propedeutici.enabled=false

is_sql_select = dw_corsi_lista.getsqlselect()
							
dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
									 

l_objects[1] = dw_corsi_lista

dw_folder_search.fu_assigntab(1, "L.", l_objects[])

l_objects[1] = dw_ricerca

l_objects[2] = cb_ricerca

l_objects[3] = cb_reset

dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)

dw_folder_search.fu_selecttab(2)

dw_corsi_lista.change_dw_current()


end event

on w_corsi.create
int iCurrent
call super::create
this.cb_note_1=create cb_note_1
this.cb_allinea=create cb_allinea
this.cb_2=create cb_2
this.cb_1=create cb_1
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_corsi_det=create dw_corsi_det
this.cb_corsi_propedeutici=create cb_corsi_propedeutici
this.cb_documenti=create cb_documenti
this.dw_corsi_lista=create dw_corsi_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_note_1
this.Control[iCurrent+2]=this.cb_allinea
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.cb_reset
this.Control[iCurrent+6]=this.cb_ricerca
this.Control[iCurrent+7]=this.dw_corsi_det
this.Control[iCurrent+8]=this.cb_corsi_propedeutici
this.Control[iCurrent+9]=this.cb_documenti
this.Control[iCurrent+10]=this.dw_corsi_lista
this.Control[iCurrent+11]=this.dw_folder_search
this.Control[iCurrent+12]=this.dw_ricerca
end on

on w_corsi.destroy
call super::destroy
destroy(this.cb_note_1)
destroy(this.cb_allinea)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_corsi_det)
destroy(this.cb_corsi_propedeutici)
destroy(this.cb_documenti)
destroy(this.dw_corsi_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

type cb_note_1 from uo_ext_note_mss_1 within w_corsi
integer x = 133
integer y = 1580
integer taborder = 50
boolean bringtotop = true
end type

event clicked;call super::clicked;if not isnull(s_cs_xx.parametri.parametro_s_1) then
	string ls_cod_corso
	long   ll_row
	
	ls_cod_corso = dw_corsi_det.getitemstring(dw_corsi_det.getrow(),"cod_corso")
	
	update tab_corsi
	set    note = :s_cs_xx.parametri.parametro_s_1
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_corso = :ls_cod_corso;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento della nota.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	else
		commit;
	end if
	
	ll_row = dw_corsi_lista.getrow()
	dw_corsi_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	dw_corsi_lista.scrolltorow(ll_row)

end if


end event

event ue_carica_valori;call super::ue_carica_valori;string ls_cod_corso

ls_cod_corso = dw_corsi_det.getitemstring(dw_corsi_det.getrow(),"cod_corso")

select note
into   :s_cs_xx.parametri.parametro_s_1
from   tab_corsi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_corso = :ls_cod_corso;

s_cs_xx.parametri.parametro_i_1 = 2000
end event

type cb_allinea from commandbutton within w_corsi
integer x = 571
integer y = 1984
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allinea"
end type

event clicked;string ls_flag_interno,ls_durata, ls_cod_corso
dec{4} ld_costo

if g_mb.messagebox("OMNIA","Procedo con l'allineamento dei dati nei corsi dei dipendenti?", question!, yesno!,2) = 2 then return

if dw_corsi_lista.getrow() < 1 then return

ls_cod_corso = dw_corsi_lista.getitemstring(dw_corsi_lista.getrow(),"cod_corso")
ls_flag_interno = dw_corsi_lista.getitemstring(dw_corsi_lista.getrow(),"flag_interno")
ls_durata = dw_corsi_lista.getitemstring(dw_corsi_lista.getrow(),"durata")
ld_costo = dw_corsi_lista.getitemnumber(dw_corsi_lista.getrow(),"costo")

update corsi_dipendenti
set    flag_interno = :ls_flag_interno,
       durata = :ls_durata,
		 costo = :ld_costo
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_corso = :ls_cod_corso;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in allineamento corsi dip.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

commit;
end event

type cb_2 from commandbutton within w_corsi
integer x = 2149
integer y = 1984
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sessioni"
end type

event clicked;if not isnull(dw_corsi_lista.getitemstring(dw_corsi_lista.getrow(),"cod_corso")) then
	window_open_parm(w_corsi_sessioni, -1, dw_corsi_lista)
end if

end event

type cb_1 from commandbutton within w_corsi
integer x = 960
integer y = 1984
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;window_open_parm(w_corsi_elenco, -1, dw_corsi_lista)

end event

type cb_reset from commandbutton within w_corsi
integer x = 2126
integer y = 556
integer width = 361
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)

dw_ricerca.setitem(1, "rs_cod_area_aziendale", ls_null)
dw_ricerca.setitem(1, "rs_cod_corso", ls_null)
dw_ricerca.setitem(1, "rdt_data_inizio_prevista", ldt_null)
dw_ricerca.setitem(1, "rdt_data_fine_prevista", ldt_null)
dw_ricerca.setitem(1, "rdt_data_inizio_effettiva", ldt_null)
dw_ricerca.setitem(1, "rdt_data_fine_effettiva", ldt_null)
end event

type cb_ricerca from commandbutton within w_corsi
integer x = 1737
integer y = 556
integer width = 361
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_sql, ls_cod_area_aziendale, ls_cod_corso
datetime ldt_data_inizio_prevista, ldt_data_fine_prevista, ldt_data_inizio_effettiva, ldt_data_fine_effettiva


dw_ricerca.accepttext()

ls_sql = is_sql_select + " WHERE tab_corsi.cod_azienda='" + s_cs_xx.cod_azienda + "' "

ls_cod_area_aziendale = dw_ricerca.getitemstring(1, "rs_cod_area_aziendale")
ls_cod_corso = dw_ricerca.getitemstring(1, "rs_cod_corso")
ldt_data_inizio_prevista = dw_ricerca.getitemdatetime(1, "rdt_data_inizio_prevista")
ldt_data_fine_prevista = dw_ricerca.getitemdatetime(1, "rdt_data_fine_prevista")
ldt_data_inizio_effettiva = dw_ricerca.getitemdatetime(1, "rdt_data_inizio_effettiva")
ldt_data_fine_effettiva = dw_ricerca.getitemdatetime(1, "rdt_data_fine_effettiva")

if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
	ls_sql += " AND tab_corsi.cod_area_aziendale='" + ls_cod_area_aziendale + "' "
end if

if not isnull(ls_cod_corso) and ls_cod_corso <> "" then
	ls_sql += " AND tab_corsi.cod_corso='" + ls_cod_corso + "' "
end if

if not isnull(ldt_data_inizio_prevista)  then
	ls_sql += " AND (tab_corsi.data_inizio_pre >='" + string(ldt_data_inizio_prevista, s_cs_xx.db_funzioni.formato_data) + "' OR "
	ls_sql += "tab_corsi_sessioni.data_inizio_prevista>='" + string(ldt_data_inizio_prevista, s_cs_xx.db_funzioni.formato_data) + "') "
end if

if not isnull(ldt_data_fine_prevista) then
	ls_sql += " AND (tab_corsi.data_fine_pre <='" + string(ldt_data_fine_prevista, s_cs_xx.db_funzioni.formato_data) + "' OR "
	ls_sql += "tab_corsi_sessioni.data_fine_prevista<='" + string(ldt_data_fine_prevista, s_cs_xx.db_funzioni.formato_data) + "') "
end if

if not isnull(ldt_data_inizio_effettiva) then
	ls_sql += " AND (tab_corsi.data_inizio_eff >='" + string(ldt_data_inizio_effettiva, s_cs_xx.db_funzioni.formato_data) + "' OR "
	ls_sql += "tab_corsi_sessioni.data_fine_effettiva>='" + string(ldt_data_inizio_effettiva, s_cs_xx.db_funzioni.formato_data) + "') "
end if

if not isnull(ldt_data_fine_effettiva) then
	ls_sql += " AND (tab_corsi.data_fine_eff <='" + string(ldt_data_fine_effettiva, s_cs_xx.db_funzioni.formato_data) + "' OR "
	ls_sql += "tab_corsi_sessioni.data_fine_effettiva<='" + string(ldt_data_fine_effettiva, s_cs_xx.db_funzioni.formato_data) + "') "
end if

ls_sql += " ORDER BY tab_corsi.cod_corso"

dw_corsi_lista.setsqlselect(ls_sql)
dw_corsi_lista.Modify( "DataWindow.Table.UpdateTable = ~"tab_corsi~"")


dw_corsi_lista.change_dw_current()

parent.triggerevent("pc_retrieve")

dw_folder_search.fu_selecttab(1)
end event

type dw_corsi_det from uo_cs_xx_dw within w_corsi
integer x = 23
integer y = 660
integer width = 2491
integer height = 1288
integer taborder = 20
string dataobject = "d_corsi_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null
   integer ll_i


   choose case i_colname
      case "flag_interno"
         setnull(ls_null)
         ll_i = this.getrow()
			setitem(ll_i, "costo", 0)
   end choose
end if
end event

event buttonclicked;call super::buttonclicked;if row>0 then
	choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_corsi_det,"cod_cliente")	
	end choose
end if
end event

type cb_corsi_propedeutici from commandbutton within w_corsi
integer x = 1737
integer y = 1984
integer width = 384
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Propedeutici"
end type

on clicked;window_open_parm(w_corsi_richiesti, -1, dw_corsi_lista)

end on

type cb_documenti from commandbutton within w_corsi
integer x = 1349
integer y = 1984
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;string      ls_cod_corso, ls_db, ls_gestione_doc, ls_doc
integer     li_i, li_risposta
long        ll_prog_mimetype

transaction sqlcb
blob        lbl_null, lbl_blob

setnull(lbl_null)

li_i = dw_corsi_lista.getrow()
if isnull(li_i) or li_i < 1 then return

ls_cod_corso = dw_corsi_lista.getitemstring(li_i, "cod_corso")

select prog_mimetype
into :ll_prog_mimetype
from tab_corsi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_corso = :ls_cod_corso;
	
selectblob blob
into :lbl_blob
from tab_corsi
where 
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_corso = :ls_cod_corso;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_corsi
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_corso = :ls_cod_corso;
	else
		updateblob tab_corsi
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_corso = :ls_cod_corso;
			
		update tab_corsi
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_corso = :ls_cod_corso;
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
		
end if

//select prog_mimetype
//into :ll_prog_mimetype
//from tab_corsi
//where
//	cod_azienda = :s_cs_xx.cod_azienda and
//	cod_corso = :ls_cod_corso;
//
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//		
//	selectblob blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_corsi
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//				  cod_corso = :ls_cod_corso
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//		s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//		
//	destroy sqlcb;
//		
//else
//	
//	selectblob blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_corsi
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//				  cod_corso = :ls_cod_corso;
//	
//	if sqlca.sqlcode <> 0 then
//		s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//		
//end if
//
//if ( not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) or &
//   ( ( isnull(ll_prog_mimetype) or ll_prog_mimetype < 1) and isnull(s_cs_xx.parametri.parametro_bl_1) ) then
//	
//	
//	// in questo caso o non ho inserito documenti oppure li ho inseriti dalla intranet
//	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
//	
//	window_open(w_ole_documenti, 0)
//	
//	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
//	
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
//		
//		if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//			
//			if ls_db = "MSSQL" then
//					
//				li_risposta = f_crea_sqlcb(sqlcb)
//					
//				updateblob tab_corsi
//				set        blob = :s_cs_xx.parametri.parametro_bl_1
//				where      cod_azienda = :s_cs_xx.cod_azienda and
//							  cod_corso = :ls_cod_corso
//				using      sqlcb;
//				
//				destroy sqlcb;
//				
//			else
//				
//				updateblob tab_corsi
//				set        blob = :s_cs_xx.parametri.parametro_bl_1
//				where      cod_azienda = :s_cs_xx.cod_azienda and
//							  cod_corso = :ls_cod_corso;
//		
//			end if
//			
//			update     tab_corsi
//			set        prog_mimetype = :ll_prog_mimetype
//			where      cod_azienda = :s_cs_xx.cod_azienda and
//						  cod_corso = :ls_cod_corso;
//						  
//			commit;
//			
//		end if
//		
//	end if	
//		
//else		
//		
//	window_open(w_ole, 0)
//		
//	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//			
//		if ls_db = "MSSQL" then
//			
//			li_risposta = f_crea_sqlcb(sqlcb)
//			
//			updateblob tab_corsi
//			set        blob = :s_cs_xx.parametri.parametro_bl_1
//			where      cod_azienda = :s_cs_xx.cod_azienda and
//						  cod_corso = :ls_cod_corso
//			using      sqlcb;
//			
//			destroy    sqlcb;
//			
//		else
//			
//			updateblob tab_corsi
//			set        blob = :s_cs_xx.parametri.parametro_bl_1
//			where      cod_azienda = :s_cs_xx.cod_azienda and
//						  cod_corso = :ls_cod_corso;
//						  
//		end if
//	
//		update     tab_corsi
//		set        path = :s_cs_xx.parametri.parametro_s_1
//		where      cod_azienda = :s_cs_xx.cod_azienda and
//					  cod_corso = :ls_cod_corso;
//	
//		commit;
//		
//	end if
//		
//end if		
		
parent.triggerevent("pc_retrieve")

dw_corsi_lista.scrolltorow(li_i)


end event

type dw_corsi_lista from uo_cs_xx_dw within w_corsi
integer x = 178
integer y = 36
integer width = 2304
integer height = 604
integer taborder = 10
string dataobject = "d_corsi_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and this.getitemstring(this.getrow(), "cod_corso") <> "" then
      cb_corsi_propedeutici.enabled = true
	   cb_documenti.enabled = true
   else
      cb_corsi_propedeutici.enabled = false
		cb_documenti.enabled = false
   end if
	
	dw_corsi_det.object.b_ricerca_cliente.enabled = false
	
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify

   cb_corsi_propedeutici.enabled = false
   cb_documenti.enabled = false
	
	dw_corsi_det.object.b_ricerca_cliente.enabled = true
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify
	long   ll_codice
	
	select numero
	into   :ll_codice
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'PAC';
	if sqlca.sqlcode = 0 then
		dw_corsi_det.Object.cod_corso.Protect=1
		dw_corsi_det.object.cod_corso.background.color = '16777215'
	end if

   cb_corsi_propedeutici.enabled = false
   cb_documenti.enabled = false
	
	dw_corsi_det.object.b_ricerca_cliente.enabled = true
	
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_codice
long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(this.getitemstring(ll_i, "cod_corso")) then
		long   ll_codice
		
		//Giulio: 26/01/2012 parametro aziendale non più in uso, seleziono il massimo dal db 
//		select numero
//		into   :ll_codice
//		from   parametri_azienda
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_parametro = 'PAC'

		select max(cod_corso)
		into :ls_codice
		from tab_corsi
		where cod_azienda = :s_cs_xx.cod_azienda ;
					 
		if sqlca.sqlcode = 0 then
//			ll_codice ++

			ls_codice = guo_functions.uof_progressivo_alfanumerico(ls_codice)
			
			setitem(getrow(),"cod_corso", right(ls_codice, 3))
			
//			update parametri_azienda
//			set    numero = :ll_codice
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_parametro = 'PAC';
		end if
	end if	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and this.getitemstring(this.getrow(), "cod_corso") <> "" then
      cb_corsi_propedeutici.enabled = true
		cb_documenti.enabled = true
   else
      cb_corsi_propedeutici.enabled = false
		cb_documenti.enabled = false
   end if
	
	dw_corsi_det.object.b_ricerca_cliente.enabled = false
	
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	string ls_cod_corso, ls_note
	
	if dw_corsi_lista.getrow() > 0 then
		
		ls_cod_corso = getitemstring(getrow(),"cod_corso")
		
		if not isnull(ls_cod_corso) then
			
			select note
			into   :ls_note
			from   tab_corsi
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_corso = :ls_cod_corso;
					 
			dw_corsi_det.setitem(dw_corsi_det.getrow(),"note",ls_note)
			dw_corsi_det.setitemstatus(dw_corsi_det.getrow(), "note", primary!,NotModified!)
			
		end if
		
	end if
	
end if
end event

event sqlpreview;call super::sqlpreview;if request = PreviewFunctionRetrieve! then

	if sqltype = PreviewSelect! then
		string ls_str, ls_str1, ls_str2, ls_sql, ls_order
		
		date ldt_data_prevista_i, ldt_data_prevista_f, ldt_data_effettiva_i, ldt_data_effettiva_f
		
		setnull(ldt_data_prevista_i)
		setnull(ldt_data_prevista_f)
		setnull(ldt_data_effettiva_i)
		setnull(ldt_data_effettiva_f)
		
		ldt_data_prevista_i = date(dw_ricerca.getitemdatetime( 1, "rdt_data_inizio_prevista"))
		ldt_data_prevista_f = date(dw_ricerca.getitemdatetime( 1, "rdt_data_fine_prevista"))
		ldt_data_effettiva_i = date(dw_ricerca.getitemdatetime( 1, "rdt_data_inizio_effettiva"))
		ldt_data_effettiva_f = date(dw_ricerca.getitemdatetime( 1, "rdt_data_fine_effettiva"))
		
		ls_str1 = ""
		ls_str2 = ""
		
		if not isnull(ldt_data_prevista_i) then
			ls_str1 = ls_str1 + " (tab_corsi_sessioni.data_inizio_prevista >= '" + string(ldt_data_prevista_i, s_cs_xx.db_funzioni.formato_data) + "' )"
		end if
		if not isnull(ldt_data_prevista_f) then
			if ls_str1 <> "" then ls_str1 = ls_str1 + " and "
			ls_str1 = ls_str1 + " (tab_corsi_sessioni.data_fine_prevista <= '" + string(ldt_data_prevista_f, s_cs_xx.db_funzioni.formato_data) + "' )"
		end if
		
		if not isnull(ldt_data_effettiva_i) then
			ls_str2 = ls_str2 + " (tab_corsi_sessioni.data_inizio_effettiva >= '" + string(ldt_data_effettiva_i, s_cs_xx.db_funzioni.formato_data) + "' )"
		end if
		if not isnull(ldt_data_effettiva_f) then
			if ls_str2 <> "" then ls_str2 = ls_str2 + " and "
			ls_str2 = ls_str2 + " (tab_corsi_sessioni.data_fine_effettiva <= '" + string(ldt_data_effettiva_f, s_cs_xx.db_funzioni.formato_data) + "' )"
		end if
		
		if ls_str1 <> "" and ls_str2 <> "" then
			ls_str = "( " + ls_str1 + " ) or ( " + ls_str2 + " )"
		elseif ls_str1 = "" and ls_str2 <> "" then
			ls_str = ls_str2
		elseif ls_str1 <> "" and ls_str2 = "" then
			ls_str = ls_str1
		elseif ls_str1 = "" and ls_str2 = "" then
			ls_str = ""
		end if
		
		if len(ls_str) > 0 then
			
			ls_sql = sqlsyntax
			if pos(lower(ls_sql), "order by") > 0 then
				ls_order = mid(ls_sql, pos(lower(ls_sql), "order by") )
				ls_sql = left(ls_sql, pos(lower(ls_sql), "order by") -1)
			else
				ls_order = ''
			end if
		
		
			ls_sql = ls_sql + " and ( " + ls_str + " ) " + ls_order
			setsqlpreview(ls_sql)
			
		end if
		
	end if

end if
end event

type dw_folder_search from u_folder within w_corsi
integer x = 23
integer y = 20
integer width = 2491
integer height = 640
integer taborder = 20
end type

type dw_ricerca from u_dw_search within w_corsi
event ue_key pbm_dwnkey
integer x = 178
integer y = 36
integer width = 2286
integer height = 460
integer taborder = 30
string dataobject = "d_corsi_ricerca"
boolean border = false
end type

