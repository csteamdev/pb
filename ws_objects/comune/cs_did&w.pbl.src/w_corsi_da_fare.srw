﻿$PBExportHeader$w_corsi_da_fare.srw
$PBExportComments$Window corsi da fare
forward
global type w_corsi_da_fare from w_cs_xx_risposta
end type
type dw_corsi_da_fare from uo_cs_xx_dw within w_corsi_da_fare
end type
type ddlb_mansione from dropdownlistbox within w_corsi_da_fare
end type
type st_1 from statictext within w_corsi_da_fare
end type
type cb_aggiorna from commandbutton within w_corsi_da_fare
end type
type cb_genera from commandbutton within w_corsi_da_fare
end type
type cb_chiudi from commandbutton within w_corsi_da_fare
end type
type mle_1 from multilineedit within w_corsi_da_fare
end type
end forward

global type w_corsi_da_fare from w_cs_xx_risposta
int Width=3580
int Height=1989
boolean TitleBar=true
string Title="Genera Calendario Corsi"
dw_corsi_da_fare dw_corsi_da_fare
ddlb_mansione ddlb_mansione
st_1 st_1
cb_aggiorna cb_aggiorna
cb_genera cb_genera
cb_chiudi cb_chiudi
mle_1 mle_1
end type
global w_corsi_da_fare w_corsi_da_fare

on w_corsi_da_fare.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_corsi_da_fare=create dw_corsi_da_fare
this.ddlb_mansione=create ddlb_mansione
this.st_1=create st_1
this.cb_aggiorna=create cb_aggiorna
this.cb_genera=create cb_genera
this.cb_chiudi=create cb_chiudi
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_corsi_da_fare
this.Control[iCurrent+2]=ddlb_mansione
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_aggiorna
this.Control[iCurrent+5]=cb_genera
this.Control[iCurrent+6]=cb_chiudi
this.Control[iCurrent+7]=mle_1
end on

on w_corsi_da_fare.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_corsi_da_fare)
destroy(this.ddlb_mansione)
destroy(this.st_1)
destroy(this.cb_aggiorna)
destroy(this.cb_genera)
destroy(this.cb_chiudi)
destroy(this.mle_1)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave)

dw_corsi_da_fare.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete + c_modifyonopen,c_default)
end event

event pc_setddlb;call super::pc_setddlb;

ddlb_mansione.AddItem('Tutte le mansioni')

f_po_loadddlb(ddlb_mansione, &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "'","")
end event

type dw_corsi_da_fare from uo_cs_xx_dw within w_corsi_da_fare
int X=23
int Y=21
int Width=3498
int Height=1541
int TabOrder=40
string DataObject="d_corsi_da_fare"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

type ddlb_mansione from dropdownlistbox within w_corsi_da_fare
int X=1029
int Y=1781
int Width=915
int Height=1501
int TabOrder=60
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_corsi_da_fare
int X=23
int Y=1801
int Width=988
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Seleziona corsi da fare per Mansione:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_aggiorna from commandbutton within w_corsi_da_fare
int X=1966
int Y=1781
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_mansione,ls_errore
integer li_risposta

ls_cod_mansione = f_po_selectddlb(ddlb_mansione)

if ls_cod_mansione = 'Tutte le mansioni' then setnull(ls_cod_mansione)

li_risposta = f_corsi_da_fare(ls_cod_mansione,ls_errore)

update corsi_da_fare
set flag_da_fare = 'S'
where cod_azienda =:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if 

if li_risposta < 0 then 
	g_mb.messagebox("GRU",ls_errore,stopsign!)
	return
end if

dw_corsi_da_fare.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_corsi_da_fare.SetTabOrder ( "flag_da_fare", 10)
end event

type cb_genera from commandbutton within w_corsi_da_fare
int X=2355
int Y=1781
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="&Genera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_corso,ls_flag_da_fare
double ldd_costo
long ll_progr,ll_righe,ll_test
integer li_anno

li_anno = s_cs_xx.parametri.parametro_i_1 
ll_progr =s_cs_xx.parametri.parametro_d_1

select count(*)
into   :ll_test
from   det_calendario_corsi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno=:li_anno
and    progr=:ll_progr;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

if ll_test > 0 then
	if g_mb.messagebox("GRU","Attenzione esiste già un calendario, vuoi sovrascriverlo con il nuovo?",question!,yesno!,1) = 1 then
	
		delete det_calendario_corsi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno=:li_anno
		and    progr=:ll_progr;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if

	else
		return
	end if
	
end if

for ll_righe = 1 to dw_corsi_da_fare.rowcount()
	ls_cod_corso = dw_corsi_da_fare.getitemstring(ll_righe,"cod_corso")
	ldd_costo = dw_corsi_da_fare.getitemnumber(ll_righe,"costo")
	ls_flag_da_fare =  dw_corsi_da_fare.getitemstring(ll_righe,"flag_da_fare")
	
	if ls_flag_da_fare = 'S' then
		insert into det_calendario_corsi
		(cod_azienda,
		 anno,
		 progr,
		 cod_corso,
		 costo)
		values
		(:s_cs_xx.cod_azienda,
		 :li_anno,
		 :ll_progr,
		 :ls_cod_corso,
		 :ldd_costo);
		 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
	end if
	
next

commit;

g_mb.messagebox("GRU","Calendario corsi generato con successo.",information!)

	
end event

type cb_chiudi from commandbutton within w_corsi_da_fare
int X=3086
int Y=1781
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

type mle_1 from multilineedit within w_corsi_da_fare
int X=23
int Y=1581
int Width=3498
int Height=181
int TabOrder=30
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean DisplayOnly=true
string Text="Selezionare la mansione o tutte per le quali generare i corsi, cliccare sul bottone aggiorna per ottenere l'elenco dei corsi da fare, impostare la colonna 'Da Fare' a Si solo sui corsi desiderati, cliccare sul bottone genera per generare i corsi selezionati."
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

