﻿$PBExportHeader$w_corsi_dipendenti.srw
$PBExportComments$Finestra Gestione Corsi Dipendenti
forward
global type w_corsi_dipendenti from w_cs_xx_principale
end type
type dw_corsi_dipendenti_det from uo_cs_xx_dw within w_corsi_dipendenti
end type
type dw_corsi_dipendenti_lista from uo_cs_xx_dw within w_corsi_dipendenti
end type
end forward

global type w_corsi_dipendenti from w_cs_xx_principale
integer width = 2633
integer height = 2120
string title = "Corsi per Dipendente"
dw_corsi_dipendenti_det dw_corsi_dipendenti_det
dw_corsi_dipendenti_lista dw_corsi_dipendenti_lista
end type
global w_corsi_dipendenti w_corsi_dipendenti

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_corsi_dipendenti_det, &
                 "cod_corso", &
                 sqlca, &
                 "tab_corsi", &
                 "cod_corso", &
                 "des_corso", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_corsi_dipendenti_det, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

				  
f_po_loaddddw_dw(dw_corsi_dipendenti_det, &
					  "cod_istruttore", &
					  sqlca, &
					  "anag_operai", &
					  "cod_operaio", &
					  "cognome + ' ' + nome", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	

end event

event pc_setwindow;call super::pc_setwindow;dw_corsi_dipendenti_lista.set_dw_key("cod_azienda")
dw_corsi_dipendenti_lista.set_dw_key("cod_operaio")

dw_corsi_dipendenti_lista.set_dw_options(sqlca, &
                                         i_openparm, &
                                         c_scrollparent, &
                                         c_default)
													  
dw_corsi_dipendenti_det.set_dw_options(sqlca, &
                                       dw_corsi_dipendenti_lista, &
                                       c_sharedata + c_scrollparent, &
                                       c_default)
													
iuo_dw_main = dw_corsi_dipendenti_lista
end event

on w_corsi_dipendenti.create
int iCurrent
call super::create
this.dw_corsi_dipendenti_det=create dw_corsi_dipendenti_det
this.dw_corsi_dipendenti_lista=create dw_corsi_dipendenti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_corsi_dipendenti_det
this.Control[iCurrent+2]=this.dw_corsi_dipendenti_lista
end on

on w_corsi_dipendenti.destroy
call super::destroy
destroy(this.dw_corsi_dipendenti_det)
destroy(this.dw_corsi_dipendenti_lista)
end on

type dw_corsi_dipendenti_det from uo_cs_xx_dw within w_corsi_dipendenti
integer x = 23
integer y = 540
integer width = 2555
integer height = 1460
integer taborder = 20
string dataobject = "d_corsi_dipendenti_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	
   string   ls_flag_interno, ls_cod_operaio, ls_cod_fornitore, ls_luogo, ls_durata, ls_note, &
            ls_null, ls_cod_corso
	long     ll_progressivo, ll_null
   decimal  ld_costo
	datetime ldt_inizio_pre, ldt_inizio_eff

   setnull(ls_null)
   setnull(ll_null)
	
   choose case i_colname
			
		case "cod_corso"	
			
			setitem(getrow(),"progressivo", ll_null)
			
			if isnull(i_coltext) or len(i_coltext) < 1 then return
			
         select flag_interno,
                costo,
                durata
         into   :ls_flag_interno,
                :ld_costo,
                :ls_durata
         from   tab_corsi
         where  tab_corsi.cod_azienda = :s_cs_xx.cod_azienda and
                tab_corsi.cod_corso = :i_coltext;
 
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "flag_interno", ls_flag_interno)
            this.setitem(i_rownbr, "costo", ld_costo)
            this.setitem(i_rownbr, "durata", ls_durata)
         end if
			
			f_po_loaddddw_dw(dw_corsi_dipendenti_det, &
								  "progressivo", &
								  sqlca, &
								  "tab_corsi_sessioni", &
								  "progressivo", &
								  "sede_corso + ' ' + cod_area_aziendale ", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_corso = '" + i_coltext + "'")
				
			
      case "progressivo"
			
			if isnull(i_coltext) or len(i_coltext) < 1 then return
			
			ll_progressivo = long(i_coltext)
			ls_cod_corso = getitemstring(getrow(),"cod_corso")
			
			if isnull(ls_cod_corso) or len(ls_cod_corso) < 1 then
				g_mb.messagebox("OMNIA","Selezionare un corso dall'apposita lista.")
				return
			end if
			
			
         select cod_operaio,
                cod_fornitore,
                sede_corso,
                note,
					 data_inizio_prevista,
					 data_inizio_effettiva
         into   :ls_cod_operaio,
                :ls_cod_fornitore,
                :ls_luogo,
                :ls_note,
					 :ldt_inizio_pre,
					 :ldt_inizio_eff
         from   tab_corsi_sessioni
         where  cod_azienda = :s_cs_xx.cod_azienda and
                cod_corso = :ls_cod_corso and
					 progressivo = :ll_progressivo;
 
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_istruttore", ls_cod_operaio)
            this.setitem(i_rownbr, "cod_fornitore", ls_cod_fornitore)
            this.setitem(i_rownbr, "sede_corso", ls_luogo)
            this.setitem(i_rownbr, "note", ls_note)
				this.setitem(i_rownbr, "data_corso", ldt_inizio_eff)
				this.setitem(i_rownbr, "data_pianificazione", ldt_inizio_pre)
         end if
			
      case "flag_interno"
         this.setitem(i_rownbr, "cod_istruttore", ls_null)
         this.setitem(i_rownbr, "cod_fornitore", ls_null)
         this.setitem(i_rownbr, "costo", 0)
   end choose
end if
end event

type dw_corsi_dipendenti_lista from uo_cs_xx_dw within w_corsi_dipendenti
integer x = 23
integer y = 20
integer width = 2551
integer height = 500
integer taborder = 10
string dataobject = "d_corsi_dipendenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   ls_modify = "cod_istruttore.background.color='12632256~tif(flag_interno=~~'N~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "cod_fornitore.background.color='12632256~tif(flag_interno=~~'S~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "costo.background.color='12632256~tif(flag_interno=~~'S~~',12632256,16777215)'~t"
   dw_corsi_dipendenti_det.modify(ls_modify)
end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_operaio


ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_operaio

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if
next
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   ls_modify = "cod_istruttore.background.color='12632256~tif(flag_interno=~~'N~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "cod_fornitore.background.color='12632256~tif(flag_interno=~~'S~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "costo.background.color='12632256~tif(flag_interno=~~'S~~',12632256,16777215)'~t"
   dw_corsi_dipendenti_det.modify(ls_modify)
end if
end event

