﻿$PBExportHeader$w_corsi_elenco.srw
$PBExportComments$Elenco generale corsi
forward
global type w_corsi_elenco from w_cs_xx_principale
end type
type dw_corsi_elenco from uo_cs_xx_dw within w_corsi_elenco
end type
end forward

global type w_corsi_elenco from w_cs_xx_principale
integer width = 3438
integer height = 1700
string title = "Report Addestramenti"
dw_corsi_elenco dw_corsi_elenco
end type
global w_corsi_elenco w_corsi_elenco

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin + c_noenablepopup)

save_on_close(c_socnosave)

dw_corsi_elenco.set_dw_options(sqlca, &
								i_openparm, &
								c_nonew + &
								c_nomodify + &
								c_nodelete + &
								c_noenablenewonopen + &
								c_noenablemodifyonopen + &
								c_scrollparent + &
								c_disablecc, &
								c_noresizedw + &
								c_nohighlightselected + &
								c_nocursorrowfocusrect + &
								c_nocursorrowpointer)
end event

on w_corsi_elenco.create
int iCurrent
call super::create
this.dw_corsi_elenco=create dw_corsi_elenco
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_corsi_elenco
end on

on w_corsi_elenco.destroy
call super::destroy
destroy(this.dw_corsi_elenco)
end on

type dw_corsi_elenco from uo_cs_xx_dw within w_corsi_elenco
integer x = 9
integer y = 8
integer width = 3369
integer height = 1564
integer taborder = 10
string dataobject = "d_corsi_elenco"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;integer  li_i, li_anno
long     ll_righe, ll_num,ll_progressivo
date     ldt_data
string   ls_descrizione, ls_stato, emessa_da, approvata_da, chiusa_da, verificata_da, ls_cod_area_aziendale, &
         ls_path_logo, ls_modify, ls_cod_area, ls_des_area, ls_cod_corso, ls_flag_interno, ls_des_corso, ls_cod_operaio, ls_cognome, ls_nome, ls_des_operaio, ls_cod_fornitore

datetime ldt_inizio_pre, ldt_fine_pre, ldt_inizio_eff, ldt_fine_eff

declare cu_sessioni cursor for
select progressivo, 
       cod_operaio, 
		 cod_fornitore,
		 data_inizio_prevista,
		 data_fine_prevista,
		 data_inizio_effettiva,
		 data_fine_effettiva,
		 cod_area_aziendale
from   tab_corsi_sessioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_corso = :ls_cod_corso
order by cod_corso ASC, progressivo ASC;

if i_parentdw.rowcount() > 0 then	
	
	for li_i = 1 to i_parentdw.rowcount()
		
		ls_cod_corso = i_parentdw.getitemstring( li_i, "cod_corso")
		ls_flag_interno = i_parentdw.getitemstring( li_i, "flag_interno")
		ls_des_corso = i_parentdw.getitemstring( li_i, "des_corso")
		
		open cu_sessioni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA","Errore in OPEN cu_sessioni.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		do while true
			fetch cu_sessioni into :ll_progressivo, 
			                       :ls_cod_operaio, 
										  :ls_cod_fornitore,
										  :ldt_inizio_pre,
										  :ldt_fine_pre,
										  :ldt_inizio_eff,
										  :ldt_fine_eff,
										  :ls_cod_area_aziendale;
		
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore in FETCH cu_sessioni.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		
			if ls_flag_interno = "S" and not isnull(ls_flag_interno) then
				if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" then
					select cognome, 
							 nome
					into   :ls_cognome,
							 :ls_nome
					from   anag_operai
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_operaio = :ls_cod_operaio;
							 
					ls_des_operaio = ls_cognome + " " + ls_nome
				else
					ls_des_operaio = ""
				end if
			else
				if not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
					select rag_soc_1
					into   :ls_des_operaio
					from   anag_fornitori
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_fornitore = :ls_cod_fornitore;
											
				else
					ls_des_operaio = ""
				end if			
			end if
		
		
	
			ll_righe = dw_corsi_elenco.insertrow(li_i)
			dw_corsi_elenco.SetItem( ll_righe, "cod_corso", ls_cod_corso + "/" + string(ll_progressivo))
			dw_corsi_elenco.SetItem( ll_righe, "des_corso", ls_des_corso)
			dw_corsi_elenco.SetItem( ll_righe, "flag_interno", ls_flag_interno)
			dw_corsi_elenco.SetItem( ll_righe, "docente", ls_des_operaio)
			dw_corsi_elenco.SetItem( ll_righe, "inizio_pre", ldt_inizio_pre)
			dw_corsi_elenco.SetItem( ll_righe, "fine_pre", ldt_fine_pre)
			dw_corsi_elenco.SetItem( ll_righe, "inizio_eff", ldt_inizio_eff)
			dw_corsi_elenco.SetItem( ll_righe, "fine_eff", ldt_fine_eff)
		
			select des_area
			into   :ls_des_area
			from   tab_aree_aziendali
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_area_aziendale = :ls_cod_area_aziendale;
					 
			dw_corsi_elenco.setitem( ll_righe, "area", ls_des_area)
		
		loop
		
		close cu_sessioni;
		
	next
	dw_corsi_elenco.setsort(" cod_corso A")
	dw_corsi_elenco.sort()
	return
else
	g_mb.messagebox("OMNIA","Attenzione! La lista di ricerca risulta essere vuota!")
	return 
end if
end event

