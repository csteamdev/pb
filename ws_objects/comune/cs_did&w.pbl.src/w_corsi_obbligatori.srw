﻿$PBExportHeader$w_corsi_obbligatori.srw
$PBExportComments$Finestra Gestione Corsi Obbligatori
forward
global type w_corsi_obbligatori from w_cs_xx_principale
end type
type dw_corsi_obbligatori from uo_cs_xx_dw within w_corsi_obbligatori
end type
type ddlb_cod_mansione from dropdownlistbox within w_corsi_obbligatori
end type
type st_cod_mansione from statictext within w_corsi_obbligatori
end type
type st_1 from statictext within w_corsi_obbligatori
end type
end forward

global type w_corsi_obbligatori from w_cs_xx_principale
int Width=1971
int Height=1269
boolean TitleBar=true
string Title="Gestione Corsi per Mansione"
dw_corsi_obbligatori dw_corsi_obbligatori
ddlb_cod_mansione ddlb_cod_mansione
st_cod_mansione st_cod_mansione
st_1 st_1
end type
global w_corsi_obbligatori w_corsi_obbligatori

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loadddlb(ddlb_cod_mansione, &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
                 "")

f_po_loaddddw_dw(dw_corsi_obbligatori,"cod_corso", &
                 sqlca,&
                 "tab_corsi", &
                 "cod_corso", &
                 "des_corso", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_corsi_obbligatori.set_dw_key("cod_azienda")
dw_corsi_obbligatori.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)
   
end on

on w_corsi_obbligatori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_corsi_obbligatori=create dw_corsi_obbligatori
this.ddlb_cod_mansione=create ddlb_cod_mansione
this.st_cod_mansione=create st_cod_mansione
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_corsi_obbligatori
this.Control[iCurrent+2]=ddlb_cod_mansione
this.Control[iCurrent+3]=st_cod_mansione
this.Control[iCurrent+4]=st_1
end on

on w_corsi_obbligatori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_corsi_obbligatori)
destroy(this.ddlb_cod_mansione)
destroy(this.st_cod_mansione)
destroy(this.st_1)
end on

type dw_corsi_obbligatori from uo_cs_xx_dw within w_corsi_obbligatori
int X=23
int Y=141
int Width=1898
int Height=1001
int TabOrder=10
string DataObject="d_corsi_obbligatori"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_mansione

ls_cod_mansione = f_po_selectddlb(ddlb_cod_mansione)

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_mansione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_mansione

ls_cod_mansione = f_po_selectddlb(ddlb_cod_mansione)


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if isnull(this.getitemstring(ll_i, "cod_mansione")) then
      this.setitem(ll_i, "cod_mansione", ls_cod_mansione)
   end if
next
end on

type ddlb_cod_mansione from dropdownlistbox within w_corsi_obbligatori
int X=686
int Y=21
int Width=1235
int Height=641
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;st_cod_mansione.text=f_po_selectddlb(ddlb_cod_mansione)
dw_corsi_obbligatori.setfocus()
parent.triggerevent("pc_retrieve")

end on

type st_cod_mansione from statictext within w_corsi_obbligatori
int X=321
int Y=21
int Width=343
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
long BorderColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_corsi_obbligatori
int X=23
int Y=21
int Width=279
int Height=81
boolean Enabled=false
string Text="Mansione:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

