﻿$PBExportHeader$w_corsi_richiesti.srw
$PBExportComments$Finestra Gestione Corsi Richiesti
forward
global type w_corsi_richiesti from w_cs_xx_principale
end type
type dw_corsi_richiesti_lista from uo_cs_xx_dw within w_corsi_richiesti
end type
type dw_corsi_richiesti_det from uo_cs_xx_dw within w_corsi_richiesti
end type
end forward

global type w_corsi_richiesti from w_cs_xx_principale
int Width=2935
int Height=1389
boolean TitleBar=true
string Title="Gestione Corsi Propedeutici"
dw_corsi_richiesti_lista dw_corsi_richiesti_lista
dw_corsi_richiesti_det dw_corsi_richiesti_det
end type
global w_corsi_richiesti w_corsi_richiesti

on activate;call w_cs_xx_principale::activate;string ls_cod_corso

ls_cod_corso = dw_corsi_richiesti_lista.i_parentdw.getitemstring(dw_corsi_richiesti_lista.i_parentdw.i_selectedrows[1], "cod_corso")

f_po_loaddddw_dw(dw_corsi_richiesti_det, &
                 "cod_corso_propedeutico", &
                 sqlca, &
                 "tab_corsi", &
                 "cod_corso", &
                 "des_corso", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_corso <> '" + ls_cod_corso + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_corsi_richiesti_lista.set_dw_key("cod_azienda")
dw_corsi_richiesti_lista.set_dw_key("cod_corso")
dw_corsi_richiesti_lista.set_dw_options(sqlca, &
                                        i_openparm, &
                                        c_scrollparent, &
                                        c_default)
dw_corsi_richiesti_det.set_dw_options(sqlca, &
                                      dw_corsi_richiesti_lista, &
                                      c_sharedata + c_scrollparent, &
                                      c_default)
iuo_dw_main = dw_corsi_richiesti_lista
end on

on w_corsi_richiesti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_corsi_richiesti_lista=create dw_corsi_richiesti_lista
this.dw_corsi_richiesti_det=create dw_corsi_richiesti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_corsi_richiesti_lista
this.Control[iCurrent+2]=dw_corsi_richiesti_det
end on

on w_corsi_richiesti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_corsi_richiesti_lista)
destroy(this.dw_corsi_richiesti_det)
end on

type dw_corsi_richiesti_lista from uo_cs_xx_dw within w_corsi_richiesti
int X=23
int Y=21
int Width=2858
int Height=501
int TabOrder=10
string DataObject="d_corsi_richiesti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_corso

ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_corso")) then
      this.setitem(ll_i, "cod_corso", ls_cod_corso)
   end if
next
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   long ll_prog_corsi
   string ls_cod_corso

   ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")

   select max(tab_corsi_richiesti.prog_corsi)
   into   :ll_prog_corsi
   from   tab_corsi_richiesti
   where  tab_corsi_richiesti.cod_azienda = :s_cs_xx.cod_azienda and
          tab_corsi_richiesti.cod_corso = :ls_cod_corso;

   if not isnull(ll_prog_corsi) then
      this.setitem(this.getrow(), "prog_corsi", ll_prog_corsi + 10)
   else
      this.setitem(this.getrow(), "prog_corsi", 10)
   end if
end if
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_corso


ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_corso)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_corsi_richiesti_det from uo_cs_xx_dw within w_corsi_richiesti
int X=23
int Y=541
int Width=2858
int Height=721
int TabOrder=20
string DataObject="d_corsi_richiesti_det"
BorderStyle BorderStyle=StyleRaised!
end type

