﻿$PBExportHeader$w_corsi_sessioni.srw
$PBExportComments$Finestra Gestione Sessioni dei Corsi
forward
global type w_corsi_sessioni from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_corsi_sessioni
end type
type cb_partecipanti from commandbutton within w_corsi_sessioni
end type
type cb_agg_schede from commandbutton within w_corsi_sessioni
end type
type cb_allinea from commandbutton within w_corsi_sessioni
end type
type dw_corsi_sessioni_lista from uo_cs_xx_dw within w_corsi_sessioni
end type
type dw_corsi_sessioni_det from uo_cs_xx_dw within w_corsi_sessioni
end type
end forward

global type w_corsi_sessioni from w_cs_xx_principale
integer width = 2706
integer height = 1828
string title = "Corsi Sessioni"
cb_1 cb_1
cb_partecipanti cb_partecipanti
cb_agg_schede cb_agg_schede
cb_allinea cb_allinea
dw_corsi_sessioni_lista dw_corsi_sessioni_lista
dw_corsi_sessioni_det dw_corsi_sessioni_det
end type
global w_corsi_sessioni w_corsi_sessioni

event pc_setwindow;call super::pc_setwindow;dw_corsi_sessioni_lista.set_dw_key("cod_azienda")
dw_corsi_sessioni_lista.set_dw_key("cod_corso")
dw_corsi_sessioni_lista.set_dw_options(sqlca, &
                                        i_openparm, &
                                        c_scrollparent, &
                                        c_default)
dw_corsi_sessioni_det.set_dw_options(sqlca, &
                                      dw_corsi_sessioni_lista, &
                                      c_sharedata + c_scrollparent, &
                                      c_default)
iuo_dw_main = dw_corsi_sessioni_lista
end event

on w_corsi_sessioni.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_partecipanti=create cb_partecipanti
this.cb_agg_schede=create cb_agg_schede
this.cb_allinea=create cb_allinea
this.dw_corsi_sessioni_lista=create dw_corsi_sessioni_lista
this.dw_corsi_sessioni_det=create dw_corsi_sessioni_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_partecipanti
this.Control[iCurrent+3]=this.cb_agg_schede
this.Control[iCurrent+4]=this.cb_allinea
this.Control[iCurrent+5]=this.dw_corsi_sessioni_lista
this.Control[iCurrent+6]=this.dw_corsi_sessioni_det
end on

on w_corsi_sessioni.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_partecipanti)
destroy(this.cb_agg_schede)
destroy(this.cb_allinea)
destroy(this.dw_corsi_sessioni_lista)
destroy(this.dw_corsi_sessioni_det)
end on

event pc_setddlb;call super::pc_setddlb;if upper(f_db()) = "ORACLE" then
	
	f_po_loaddddw_dw(dw_corsi_sessioni_det, &
						  "cod_operaio", &
						  sqlca, &
						  "anag_operai", &
						  "cod_operaio", &
						  "cognome || ' ' || nome", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
else
					
	f_po_loaddddw_dw(dw_corsi_sessioni_det, &
						  "cod_operaio", &
						  sqlca, &
						  "anag_operai", &
						  "cod_operaio", &
						  "cognome + ' ' + nome", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if				  

f_po_loaddddw_dw(dw_corsi_sessioni_det, &
                 "cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  

end event

type cb_1 from commandbutton within w_corsi_sessioni
integer x = 2290
integer y = 328
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;if dw_corsi_sessioni_lista.getrow() > 0 then
	window_open_parm(w_corsi_sessioni_report_partecipanti, -1, dw_corsi_sessioni_lista)
end if
end event

type cb_partecipanti from commandbutton within w_corsi_sessioni
integer x = 2286
integer y = 224
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Partecipanti"
end type

event clicked;if dw_corsi_sessioni_lista.getrow() > 0 then
	window_open_parm(w_corsi_sessioni_partecipanti, 0, dw_corsi_sessioni_lista)
end if
end event

type cb_agg_schede from commandbutton within w_corsi_sessioni
integer x = 2286
integer y = 124
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Agg.Schede"
end type

event clicked;if dw_corsi_sessioni_lista.getrow() > 0 then
	window_open_parm(w_corsi_sessioni_lista_operai, 0, dw_corsi_sessioni_lista)
end if
end event

type cb_allinea from commandbutton within w_corsi_sessioni
integer x = 2286
integer y = 24
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allinea"
end type

event clicked;string ls_cod_corso, ls_sede_corso, ls_note, ls_cod_operaio, ls_cod_fornitore
long   ll_progressivo
datetime ldt_data_inizio_prevista, ldt_data_inizio_effettiva

if g_mb.messagebox("OMNIA","Procedo con l'allineamento della SESSIONE nei corsi dei dipendenti?", question!, yesno!,2) = 2 then return

if dw_corsi_sessioni_lista.getrow() < 1 then return

ls_cod_corso = dw_corsi_sessioni_lista.getitemstring(dw_corsi_sessioni_lista.getrow(),"cod_corso")
ll_progressivo = dw_corsi_sessioni_lista.getitemnumber(dw_corsi_sessioni_lista.getrow(),"progressivo")
ls_sede_corso = dw_corsi_sessioni_lista.getitemstring(dw_corsi_sessioni_lista.getrow(),"sede_corso")
ls_note = dw_corsi_sessioni_lista.getitemstring(dw_corsi_sessioni_lista.getrow(),"note")
ls_cod_operaio = dw_corsi_sessioni_lista.getitemstring(dw_corsi_sessioni_lista.getrow(),"cod_operaio")
ls_cod_fornitore = dw_corsi_sessioni_lista.getitemstring(dw_corsi_sessioni_lista.getrow(),"cod_fornitore")
ldt_data_inizio_prevista = dw_corsi_sessioni_lista.getitemdatetime(dw_corsi_sessioni_lista.getrow(),"data_inizio_prevista")
ldt_data_inizio_effettiva = dw_corsi_sessioni_lista.getitemdatetime(dw_corsi_sessioni_lista.getrow(),"data_inizio_effettiva")


update corsi_dipendenti
set    sede_corso = :ls_sede_corso,
       note = :ls_note,
		 cod_istruttore = :ls_cod_operaio,
		 cod_fornitore = :ls_cod_fornitore,
		 data_pianificazione = :ldt_data_inizio_prevista,
		 data_corso = :ldt_data_inizio_effettiva
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_corso = :ls_cod_corso and
		 progressivo = :ll_progressivo;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in allineamento corsi dip.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

commit;
end event

type dw_corsi_sessioni_lista from uo_cs_xx_dw within w_corsi_sessioni
integer x = 23
integer y = 20
integer width = 2245
integer height = 500
integer taborder = 10
string dataobject = "d_corsi_sessioni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max
string ls_cod_corso

ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(this.getitemstring(ll_i, "cod_corso")) then
      this.setitem(ll_i, "cod_corso", ls_cod_corso)
   end if

   if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo") = 0 then

		select max(progressivo)
		into   :ll_max
		from   tab_corsi_sessioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_corso = :ls_cod_corso;
		
		if ll_max = 0 or isnull(ll_max) then 
			ll_max = 1
		else
			ll_max ++
		end if

      this.setitem(ll_i, "progressivo", ll_max)

	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_corso

ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_corso)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;//cb_ricerca_fornitore.enabled = true
cb_allinea.enabled = false
cb_partecipanti.enabled = false
cb_agg_schede.enabled = false
end event

event pcd_modify;call super::pcd_modify;//cb_ricerca_fornitore.enabled = true
cb_allinea.enabled = false
cb_partecipanti.enabled = false
cb_agg_schede.enabled = false
end event

event pcd_save;call super::pcd_save;//cb_ricerca_fornitore.enabled = false
cb_allinea.enabled = true
cb_partecipanti.enabled = true
cb_agg_schede.enabled = true
end event

event pcd_view;call super::pcd_view;//cb_ricerca_fornitore.enabled = false
cb_allinea.enabled = true
cb_partecipanti.enabled = true
cb_agg_schede.enabled = true
end event

type dw_corsi_sessioni_det from uo_cs_xx_dw within w_corsi_sessioni
integer x = 23
integer y = 540
integer width = 2629
integer height = 1168
integer taborder = 20
string dataobject = "d_corsi_sessioni_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_corsi_sessioni_det,"cod_fornitore")
end choose
end event

