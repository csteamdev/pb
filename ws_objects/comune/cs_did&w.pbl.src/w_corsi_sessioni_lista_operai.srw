﻿$PBExportHeader$w_corsi_sessioni_lista_operai.srw
forward
global type w_corsi_sessioni_lista_operai from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_corsi_sessioni_lista_operai
end type
type cb_1 from commandbutton within w_corsi_sessioni_lista_operai
end type
type dw_corsi_sessioni_lista_operai from uo_cs_xx_dw within w_corsi_sessioni_lista_operai
end type
end forward

global type w_corsi_sessioni_lista_operai from w_cs_xx_risposta
integer width = 1646
integer height = 1796
string title = "Lista Dipendenti da Associare alla Sessione"
cb_2 cb_2
cb_1 cb_1
dw_corsi_sessioni_lista_operai dw_corsi_sessioni_lista_operai
end type
global w_corsi_sessioni_lista_operai w_corsi_sessioni_lista_operai

on w_corsi_sessioni_lista_operai.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_corsi_sessioni_lista_operai=create dw_corsi_sessioni_lista_operai
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_corsi_sessioni_lista_operai
end on

on w_corsi_sessioni_lista_operai.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_corsi_sessioni_lista_operai)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + c_closenosave + c_autoposition + c_noresizewin)


dw_corsi_sessioni_lista_operai.set_dw_options(sqlca, &
                                        i_openparm, &
                                        c_modifyonopen + c_nonew + c_nodelete + c_scrollparent, &
                                        c_default)


end event

type cb_2 from commandbutton within w_corsi_sessioni_lista_operai
integer x = 837
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;rollback;
close(parent)
end event

type cb_1 from commandbutton within w_corsi_sessioni_lista_operai
integer x = 1225
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string ls_cod_operaio, ls_cod_corso, ls_sede_corso, ls_note,ls_cod_istruttore,ls_cod_fornitore, &
       ls_flag_interno,ls_durata
long ll_i, ll_progressivo, ll_cont
dec{4} ld_costo
datetime ldt_data_inizio_prevista, ldt_data_inizio_effettiva

if g_mb.messagebox("OMNIA","Confermi la creazione della sessione corso per ogni dipendente selezionato?", question!, yesno!,2) = 2 then return

ls_cod_corso = dw_corsi_sessioni_lista_operai.i_parentdw.getitemstring(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"cod_corso")
ll_progressivo = dw_corsi_sessioni_lista_operai.i_parentdw.getitemnumber(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"progressivo")
ls_sede_corso = dw_corsi_sessioni_lista_operai.i_parentdw.getitemstring(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"sede_corso")
ls_note = dw_corsi_sessioni_lista_operai.i_parentdw.getitemstring(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"note")
ls_cod_istruttore = dw_corsi_sessioni_lista_operai.i_parentdw.getitemstring(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"cod_operaio")
ls_cod_fornitore = dw_corsi_sessioni_lista_operai.i_parentdw.getitemstring(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"cod_fornitore")
ldt_data_inizio_prevista = dw_corsi_sessioni_lista_operai.i_parentdw.getitemdatetime(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"data_inizio_prevista")
ldt_data_inizio_effettiva = dw_corsi_sessioni_lista_operai.i_parentdw.getitemdatetime(dw_corsi_sessioni_lista_operai.i_parentdw.i_selectedrows[1],"data_inizio_effettiva")

select flag_interno,
		 costo,
		 durata
into   :ls_flag_interno,
		 :ld_costo,
		 :ls_durata
from   tab_corsi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_corso = :ls_cod_corso;
	

for ll_i = 1 to dw_corsi_sessioni_lista_operai.rowcount()
	if dw_corsi_sessioni_lista_operai.getitemstring(ll_i,"flag_selezione") <> "S" then continue
	
	ls_cod_operaio = dw_corsi_sessioni_lista_operai.getitemstring(ll_i,"cod_operaio")
	
	select count(*)
	into   :ll_cont
	from   corsi_dipendenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_corso = :ls_cod_corso and
			 cod_operaio = :ls_cod_operaio and
			 progressivo = :ll_progressivo;
			 
	if not isnull(ll_cont) and ll_cont > 0 then
		g_mb.messagebox("OMNIA","Attenzione, il dipendente " + ls_cod_operaio + " risulta aver già partecipato alla sessione. ~r~nInserimento saltato!")
		continue
	end if
	
	INSERT INTO corsi_dipendenti  
			( cod_azienda,   
			  cod_operaio,   
			  cod_corso,   
			  progressivo,   
			  sede_corso,   
			  data_corso,   
			  durata,   
			  note,   
			  flag_interno,   
			  cod_istruttore,   
			  cod_fornitore,   
			  costo,   
			  punteggio_conseguito,   
			  giudizio_livello_formazione,   
			  data_pianificazione )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_operaio,   
			  :ls_cod_corso,   
			  :ll_progressivo,   
			  :ls_sede_corso,   
			  :ldt_data_inizio_effettiva,   
			  :ls_durata,   
			  :ls_note,   
			  :ls_flag_interno,   
			  :ls_cod_istruttore,   
			  :ls_cod_fornitore,   
			  :ld_costo,   
			  0,   
			  null,   
			  :ldt_data_inizio_prevista)  ;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA", "Errore in inserimento corsi dipendenti.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

next

commit;

close(parent)
end event

type dw_corsi_sessioni_lista_operai from uo_cs_xx_dw within w_corsi_sessioni_lista_operai
integer x = 18
integer y = 16
integer width = 1577
integer height = 1572
integer taborder = 10
string dataobject = "d_corsi_sessioni_lista_operai"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

