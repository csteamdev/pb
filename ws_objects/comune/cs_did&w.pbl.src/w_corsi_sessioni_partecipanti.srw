﻿$PBExportHeader$w_corsi_sessioni_partecipanti.srw
forward
global type w_corsi_sessioni_partecipanti from w_cs_xx_risposta
end type
type dw_corsi_sessioni_partecipanti from uo_cs_xx_dw within w_corsi_sessioni_partecipanti
end type
end forward

global type w_corsi_sessioni_partecipanti from w_cs_xx_risposta
integer width = 1723
integer height = 2124
string title = "Elenco Partecipanti alla Sessione"
dw_corsi_sessioni_partecipanti dw_corsi_sessioni_partecipanti
end type
global w_corsi_sessioni_partecipanti w_corsi_sessioni_partecipanti

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + c_closenosave + c_autoposition + c_noresizewin)


dw_corsi_sessioni_partecipanti.set_dw_options(sqlca, &
                                        i_openparm, &
                                        c_modifyonopen + c_nonew + c_nodelete, &
                                        c_default)


end event

on w_corsi_sessioni_partecipanti.create
int iCurrent
call super::create
this.dw_corsi_sessioni_partecipanti=create dw_corsi_sessioni_partecipanti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_corsi_sessioni_partecipanti
end on

on w_corsi_sessioni_partecipanti.destroy
call super::destroy
destroy(this.dw_corsi_sessioni_partecipanti)
end on

type dw_corsi_sessioni_partecipanti from uo_cs_xx_dw within w_corsi_sessioni_partecipanti
integer x = 9
integer y = 12
integer width = 1669
integer height = 2000
integer taborder = 10
string dataobject = "d_corsi_sessioni_partecipanti"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_corso
long ll_errore, ll_progressivo

ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_corso")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"progressivo")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_corso, ll_progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if	
end event

