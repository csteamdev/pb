﻿$PBExportHeader$w_corsi_sessioni_report_partecipanti.srw
$PBExportComments$Report Partecipanti alla Sessione selezionata
forward
global type w_corsi_sessioni_report_partecipanti from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_corsi_sessioni_report_partecipanti
end type
end forward

global type w_corsi_sessioni_report_partecipanti from w_cs_xx_principale
integer width = 3877
integer height = 2460
string title = "Report Partecipanti alla Sessione"
dw_report dw_report
end type
global w_corsi_sessioni_report_partecipanti w_corsi_sessioni_report_partecipanti

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin + c_noenablepopup)

save_on_close(c_socnosave)

dw_report.set_dw_options(sqlca, &
								i_openparm, &
								c_nonew + &
								c_nomodify + &
								c_nodelete + &
								c_noenablenewonopen + &
								c_noenablemodifyonopen + &
								c_scrollparent + &
								c_disablecc, &
								c_noresizedw + &
								c_nohighlightselected + &
								c_nocursorrowfocusrect + &
								c_nocursorrowpointer)
								
dw_report.object.datawindow.print.preview = 'Yes'								
end event

on w_corsi_sessioni_report_partecipanti.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_corsi_sessioni_report_partecipanti.destroy
call super::destroy
destroy(this.dw_report)
end on

type dw_report from uo_cs_xx_dw within w_corsi_sessioni_report_partecipanti
integer x = 9
integer y = 8
integer width = 3803
integer height = 2312
integer taborder = 10
string dataobject = "d_report_corsi_sessioni_partecipanti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_progressivo
string ls_cod_corso

ls_cod_corso = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_corso")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_corso, ll_progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

//-----------claudia 01/06/07 ordinare i campi
//MODIFICATO IL REPORT NON VISIBILE IL CAMPO CODICE E ORDINATI I DIPENDENTI PER COGNOME


end event

