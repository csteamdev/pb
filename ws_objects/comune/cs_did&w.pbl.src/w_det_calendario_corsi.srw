﻿$PBExportHeader$w_det_calendario_corsi.srw
$PBExportComments$Window det_calendario_corsi
forward
global type w_det_calendario_corsi from w_cs_xx_principale
end type
type dw_det_calendario_corsi_lista from uo_cs_xx_dw within w_det_calendario_corsi
end type
end forward

global type w_det_calendario_corsi from w_cs_xx_principale
int Width=2684
int Height=1945
boolean TitleBar=true
string Title="Dettaglio Calendario Corsi Aziendali"
dw_det_calendario_corsi_lista dw_det_calendario_corsi_lista
end type
global w_det_calendario_corsi w_det_calendario_corsi

on w_det_calendario_corsi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_calendario_corsi_lista=create dw_det_calendario_corsi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_calendario_corsi_lista
end on

on w_det_calendario_corsi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_calendario_corsi_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_calendario_corsi_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_nomodify + c_nonew +c_nodelete, &
                                    c_default)




end event

type dw_det_calendario_corsi_lista from uo_cs_xx_dw within w_det_calendario_corsi
int X=23
int Y=21
int Width=2606
int Height=1801
int TabOrder=20
string DataObject="d_det_calendario_corsi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG    l_Error,ll_progr
integer li_anno

li_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno")
ll_progr = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progr")

l_Error = Retrieve(s_cs_xx.cod_azienda,li_anno,ll_progr)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

