﻿$PBExportHeader$w_det_struttura_impegni.srw
$PBExportComments$Finestra struttura piano impegni
forward
global type w_det_struttura_impegni from w_cs_xx_principale
end type
type dw_struttura_impegni_lista from uo_cs_xx_dw within w_det_struttura_impegni
end type
end forward

global type w_det_struttura_impegni from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3557
integer height = 1148
string title = "Tabella Struttura Piano Impegni"
dw_struttura_impegni_lista dw_struttura_impegni_lista
end type
global w_det_struttura_impegni w_det_struttura_impegni

event pc_setwindow;call super::pc_setwindow;dw_struttura_impegni_lista.set_dw_key("cod_azienda")
dw_struttura_impegni_lista.set_dw_options(sqlca, i_openparm,c_default,c_default)

iuo_dw_main = dw_struttura_impegni_lista
end event

on w_det_struttura_impegni.create
int iCurrent
call super::create
this.dw_struttura_impegni_lista=create dw_struttura_impegni_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_struttura_impegni_lista
end on

on w_det_struttura_impegni.destroy
call super::destroy
destroy(this.dw_struttura_impegni_lista)
end on

type dw_struttura_impegni_lista from uo_cs_xx_dw within w_det_struttura_impegni
integer x = 23
integer y = 20
integer width = 3474
integer height = 1000
string dataobject = "d_det_struttura_impegni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_prog_struttura
string ls_descrizione

ll_prog_struttura = dw_struttura_impegni_lista.i_parentdw.getitemnumber(dw_struttura_impegni_lista.i_parentdw.i_selectedrows[1], "prog_struttura")
ls_descrizione = dw_struttura_impegni_lista.i_parentdw.getitemstring(dw_struttura_impegni_lista.i_parentdw.i_selectedrows[1], "descrizione")


parent.title = "Dettaglio Struttura " + ls_descrizione

l_Error = Retrieve(s_cs_xx.cod_azienda, ll_prog_struttura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog_struttura


ll_prog_struttura = dw_struttura_impegni_lista.i_parentdw.getitemnumber(dw_struttura_impegni_lista.i_parentdw.i_selectedrows[1], "prog_struttura")

for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "prog_struttura", ll_prog_struttura)
next

end event

event pcd_new;call super::pcd_new;long	ll_prog, ll_prog_struttura

ll_prog_struttura = dw_struttura_impegni_lista.i_parentdw.getitemnumber(dw_struttura_impegni_lista.i_parentdw.i_selectedrows[1], "prog_struttura")

select max( prog_struttura_det)
into	 :ll_prog
from	 det_struttura_impegni
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 prog_struttura = :ll_prog_struttura;
		 
if sqlca.sqlcode = 0 then
	if isnull(ll_prog) then ll_prog = 0
	ll_prog++
	setitem( getrow(), "prog_struttura_det", ll_prog)
end if
end event

event itemchanged;call super::itemchanged;long     ll_ret, ll_ore, ll_minuti
datetime ldt_ora_da, ldt_ora_a, ldt_appo
time		lt_da, lt_a

choose case dwo.name
	case "ore_da", "ore_a"
		
		ldt_ora_da = getitemdatetime( row, "ore_da")
		ldt_ora_a = getitemdatetime( row, "ore_a")
		ldt_appo = datetime( data)
		
		if dwo.name = "ore_da" then
			ldt_ora_da = ldt_appo
		else
			ldt_ora_a = ldt_appo
		end if

		lt_da = time(ldt_ora_da)
		lt_a = time(ldt_ora_a)
		
		if isnull(lt_da) or isnull(lt_a) or lt_a < lt_da then
			ll_minuti = 0
		else
			ll_minuti = SecondsAfter ( lt_da, lt_a)
			ll_minuti = int( ll_minuti / 60)
		end if
		
		this.setitem( row, "totale_tempo", ll_minuti)
		
end choose
end event

event updateend;call super::updateend;// *** finito di aggiornare conto i minuti totali e aggiorno la testata

LONG  l_Error, ll_prog_struttura, ll_totale
string ls_descrizione

ll_prog_struttura = dw_struttura_impegni_lista.i_parentdw.getitemnumber(dw_struttura_impegni_lista.i_parentdw.i_selectedrows[1], "prog_struttura")

select sum(totale_tempo)
into	 :ll_totale
from	 det_struttura_impegni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 prog_struttura = :ll_prog_struttura and
		 flag_verifica_presenza = 'S';
		 
if sqlca.sqlcode = 0 then
	
	if isnull( ll_totale) then ll_totale = 0
	
	update tes_struttura_impegni
	set	 totale_tempo = :ll_totale
	where  cod_azienda  = :s_cs_xx.cod_azienda and
	       prog_struttura = :ll_prog_struttura;
			 
   if sqlca.sqlcode = 0 then

		commit;
		dw_struttura_impegni_lista.i_parentdw.postevent("ue_aggiorna")
			
	else
				
		g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento dei tempi totali della testata:" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return -1
	end if
	
end if



end event

