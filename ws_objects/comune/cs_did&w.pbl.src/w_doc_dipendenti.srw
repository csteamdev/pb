﻿$PBExportHeader$w_doc_dipendenti.srw
$PBExportComments$Finestra Gestione Documentazione Dipendenti
forward
global type w_doc_dipendenti from w_cs_xx_principale
end type
type dw_doc_dipendenti_lista from uo_cs_xx_dw within w_doc_dipendenti
end type
type dw_doc_dipendenti_det from uo_cs_xx_dw within w_doc_dipendenti
end type
end forward

global type w_doc_dipendenti from w_cs_xx_principale
int Width=2615
int Height=1769
boolean TitleBar=true
string Title="Gestione Documentazione Dipendenti"
dw_doc_dipendenti_lista dw_doc_dipendenti_lista
dw_doc_dipendenti_det dw_doc_dipendenti_det
end type
global w_doc_dipendenti w_doc_dipendenti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_doc_dipendenti_lista.set_dw_key("cod_azienda")
dw_doc_dipendenti_lista.set_dw_key("cod_operaio")
dw_doc_dipendenti_lista.set_dw_options(sqlca, &
                                       i_openparm, &
                                       c_scrollparent, &
                                       c_default)
dw_doc_dipendenti_det.set_dw_options(sqlca, &
                                     dw_doc_dipendenti_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)
iuo_dw_main = dw_doc_dipendenti_lista
end on

on w_doc_dipendenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_doc_dipendenti_lista=create dw_doc_dipendenti_lista
this.dw_doc_dipendenti_det=create dw_doc_dipendenti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_doc_dipendenti_lista
this.Control[iCurrent+2]=dw_doc_dipendenti_det
end on

on w_doc_dipendenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_doc_dipendenti_lista)
destroy(this.dw_doc_dipendenti_det)
end on

type dw_doc_dipendenti_lista from uo_cs_xx_dw within w_doc_dipendenti
int X=23
int Y=21
int Width=2538
int Height=501
int TabOrder=10
string DataObject="d_doc_dipendenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_operaio

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_operaio


ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_doc_dipendenti_det from uo_cs_xx_dw within w_doc_dipendenti
int X=23
int Y=541
int Width=2538
int Height=1101
int TabOrder=20
string DataObject="d_doc_dipendenti_det"
BorderStyle BorderStyle=StyleRaised!
end type

