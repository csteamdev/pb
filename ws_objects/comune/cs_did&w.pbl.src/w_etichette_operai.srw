﻿$PBExportHeader$w_etichette_operai.srw
forward
global type w_etichette_operai from w_cs_xx_principale
end type
type dw_etichette_operai from uo_cs_xx_dw within w_etichette_operai
end type
end forward

global type w_etichette_operai from w_cs_xx_principale
integer width = 3415
integer height = 1360
string title = "Etichette Difformita/Difetti"
boolean hscrollbar = true
boolean vscrollbar = true
dw_etichette_operai dw_etichette_operai
end type
global w_etichette_operai w_etichette_operai

on w_etichette_operai.create
int iCurrent
call super::create
this.dw_etichette_operai=create dw_etichette_operai
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_etichette_operai
end on

on w_etichette_operai.destroy
call super::destroy
destroy(this.dw_etichette_operai)
end on

event pc_setwindow;call super::pc_setwindow;integer li_risposta,li_bco
string ls_errore,ls_bfo

dw_etichette_operai.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_scrollparent + &
											c_disablecc, &
                                 c_default)

iuo_dw_main = dw_etichette_operai

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
if li_risposta < 0 then 
	g_mb.messagebox("GRU",ls_errore,stopsign!)
	return -1
end if

dw_etichette_operai.Modify("cf_cod_operaio.Font.Face='" + ls_bfo + "'")
dw_etichette_operai.Modify("cf_cod_operaio.Font.Height= -" + string(li_bco) )
end event

type dw_etichette_operai from uo_cs_xx_dw within w_etichette_operai
integer x = 23
integer y = 20
integer width = 3269
integer height = 1160
string dataobject = "d_etichette_operai"
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

