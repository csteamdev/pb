﻿$PBExportHeader$w_giustificativi.srw
$PBExportComments$Finestra Gestione Giustificativi
forward
global type w_giustificativi from w_cs_xx_principale
end type
type dw_giustificativi from uo_cs_xx_dw within w_giustificativi
end type
end forward

global type w_giustificativi from w_cs_xx_principale
int Width=1911
int Height=1149
boolean TitleBar=true
string Title="Gestione Giustificativi"
dw_giustificativi dw_giustificativi
end type
global w_giustificativi w_giustificativi

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_giustificativi.set_dw_key("cod_azienda")
dw_giustificativi.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)
   
end on

on w_giustificativi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_giustificativi=create dw_giustificativi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_giustificativi
end on

on w_giustificativi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_giustificativi)
end on

type dw_giustificativi from uo_cs_xx_dw within w_giustificativi
int X=23
int Y=21
int Width=1829
int Height=1001
string DataObject="d_giustificativi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

