﻿$PBExportHeader$w_giustificazioni.srw
$PBExportComments$Finestra Gestione Giustificazioni
forward
global type w_giustificazioni from w_cs_xx_principale
end type
type dw_giustificazioni from uo_cs_xx_dw within w_giustificazioni
end type
end forward

global type w_giustificazioni from w_cs_xx_principale
integer width = 2962
integer height = 1148
string title = "Gestione Giustificazioni"
dw_giustificazioni dw_giustificazioni
end type
global w_giustificazioni w_giustificazioni

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_giustificazioni.set_dw_key("cod_azienda")
dw_giustificazioni.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
   
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_giustificazioni, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_giustificazioni, &
                 "cod_giustificativo", &
                 sqlca, &
                 "tab_giustificativi", &
                 "cod_giustificativo", &
                 "des_giustificativo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_giustificazioni.create
int iCurrent
call super::create
this.dw_giustificazioni=create dw_giustificazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_giustificazioni
end on

on w_giustificazioni.destroy
call super::destroy
destroy(this.dw_giustificazioni)
end on

event pc_new;call super::pc_new;dw_giustificazioni.setitem(dw_giustificazioni.getrow(), "data_giustificazione", datetime(today()))
end event

type dw_giustificazioni from uo_cs_xx_dw within w_giustificazioni
integer x = 23
integer y = 20
integer width = 2880
integer height = 1000
string dataobject = "d_giustificazioni"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

