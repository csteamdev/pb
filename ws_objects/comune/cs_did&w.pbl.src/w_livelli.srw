﻿$PBExportHeader$w_livelli.srw
$PBExportComments$Finestra Gestione Livelli
forward
global type w_livelli from w_cs_xx_principale
end type
type dw_livelli from uo_cs_xx_dw within w_livelli
end type
type ddlb_cod_contratto from dropdownlistbox within w_livelli
end type
type st_cod_contratto from statictext within w_livelli
end type
type st_1 from statictext within w_livelli
end type
end forward

global type w_livelli from w_cs_xx_principale
int Width=3301
int Height=1269
boolean TitleBar=true
string Title="Gestione Livelli"
dw_livelli dw_livelli
ddlb_cod_contratto ddlb_cod_contratto
st_cod_contratto st_cod_contratto
st_1 st_1
end type
global w_livelli w_livelli

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loadddlb(ddlb_cod_contratto, &
                 sqlca, &
                 "tab_contratti_dip", &
                 "cod_contratto", &
                 "des_contratto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
                 "")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_livelli.set_dw_key("cod_azienda")
dw_livelli.set_dw_options(sqlca, &
                          pcca.null_object, &
                          c_default, &
                          c_default)
   
end on

on w_livelli.create
int iCurrent
call w_cs_xx_principale::create
this.dw_livelli=create dw_livelli
this.ddlb_cod_contratto=create ddlb_cod_contratto
this.st_cod_contratto=create st_cod_contratto
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_livelli
this.Control[iCurrent+2]=ddlb_cod_contratto
this.Control[iCurrent+3]=st_cod_contratto
this.Control[iCurrent+4]=st_1
end on

on w_livelli.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_livelli)
destroy(this.ddlb_cod_contratto)
destroy(this.st_cod_contratto)
destroy(this.st_1)
end on

type dw_livelli from uo_cs_xx_dw within w_livelli
int X=23
int Y=141
int Width=3223
int Height=1001
int TabOrder=10
string DataObject="d_livelli"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   integer li_mesi_contrattuali
   string ls_cod_contratto

   ls_cod_contratto = f_po_selectddlb(ddlb_cod_contratto)

   select tab_contratti_dip.mesi_contrattuali
   into   :li_mesi_contrattuali
   from   tab_contratti_dip
   where  tab_contratti_dip.cod_azienda = :s_cs_xx.cod_azienda and
          tab_contratti_dip.cod_contratto = :ls_cod_contratto;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "mesi_contrattuali", li_mesi_contrattuali)
   end if
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_contratto

ls_cod_contratto = f_po_selectddlb(ddlb_cod_contratto)


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if isnull(this.getitemstring(ll_i, "cod_contratto")) then
      this.setitem(ll_i, "cod_contratto", ls_cod_contratto)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_contratto

ls_cod_contratto = f_po_selectddlb(ddlb_cod_contratto)

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_contratto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

type ddlb_cod_contratto from dropdownlistbox within w_livelli
int X=663
int Y=21
int Width=1235
int Height=641
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;st_cod_contratto.text=f_po_selectddlb(ddlb_cod_contratto)
dw_livelli.setfocus()
parent.triggerevent("pc_retrieve")

end on

type st_cod_contratto from statictext within w_livelli
int X=298
int Y=21
int Width=343
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
long BorderColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_livelli
int X=23
int Y=21
int Width=270
int Height=81
boolean Enabled=false
string Text="Contratto:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

