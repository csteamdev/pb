﻿$PBExportHeader$w_mansioni.srw
$PBExportComments$Finestra Gestione Mansioni
forward
global type w_mansioni from w_cs_xx_principale
end type
type dw_mansioni from uo_cs_xx_dw within w_mansioni
end type
end forward

global type w_mansioni from w_cs_xx_principale
integer width = 3557
integer height = 1644
string title = "Gestione Mansioni"
dw_mansioni dw_mansioni
end type
global w_mansioni w_mansioni

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_mansioni.set_dw_key("cod_azienda")
dw_mansioni.set_dw_options(sqlca, &
                           pcca.null_object, &
                           c_default, &
                           c_default)
   
end on

on w_mansioni.create
int iCurrent
call super::create
this.dw_mansioni=create dw_mansioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mansioni
end on

on w_mansioni.destroy
call super::destroy
destroy(this.dw_mansioni)
end on

type dw_mansioni from uo_cs_xx_dw within w_mansioni
integer x = 23
integer y = 20
integer width = 3474
integer height = 1480
string dataobject = "d_mansioni"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

