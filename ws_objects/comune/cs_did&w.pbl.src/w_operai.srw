﻿$PBExportHeader$w_operai.srw
$PBExportComments$Finestra Gestione Anagrafica Operai
forward
global type w_operai from w_cs_xx_principale
end type
type cb_vetture from commandbutton within w_operai
end type
type cb_corsi from commandbutton within w_operai
end type
type cb_storico from commandbutton within w_operai
end type
type cb_documenti from commandbutton within w_operai
end type
type cb_note from commandbutton within w_operai
end type
type cb_etichette from commandbutton within w_operai
end type
type cb_ricerca from commandbutton within w_operai
end type
type cb_reset from commandbutton within w_operai
end type
type dw_ricerca from u_dw_search within w_operai
end type
type dw_folder_search from u_folder within w_operai
end type
type dw_operai_lista from uo_cs_xx_dw within w_operai
end type
type dw_operai_det_3 from uo_cs_xx_dw within w_operai
end type
type dw_folder from u_folder within w_operai
end type
type dw_operai_det_1 from uo_cs_xx_dw within w_operai
end type
type dw_operai_det_6 from uo_cs_xx_dw within w_operai
end type
type dw_operai_det_5 from uo_cs_xx_dw within w_operai
end type
type dw_operai_det_2 from uo_cs_xx_dw within w_operai
end type
type dw_operai_det_4 from uo_cs_xx_dw within w_operai
end type
end forward

global type w_operai from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 3506
integer height = 2020
string title = "Gestione Anagrafica Dipendenti"
cb_vetture cb_vetture
cb_corsi cb_corsi
cb_storico cb_storico
cb_documenti cb_documenti
cb_note cb_note
cb_etichette cb_etichette
cb_ricerca cb_ricerca
cb_reset cb_reset
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_operai_lista dw_operai_lista
dw_operai_det_3 dw_operai_det_3
dw_folder dw_folder
dw_operai_det_1 dw_operai_det_1
dw_operai_det_6 dw_operai_det_6
dw_operai_det_5 dw_operai_det_5
dw_operai_det_2 dw_operai_det_2
dw_operai_det_4 dw_operai_det_4
end type
global w_operai w_operai

type variables

end variables

forward prototypes
public subroutine wf_report_scheda_qualifica (string as_cod_operaio)
end prototypes

public subroutine wf_report_scheda_qualifica (string as_cod_operaio);

s_cs_xx.parametri.parametro_s_1_a[1] = as_cod_operaio
window_open(w_report_scheda_qualifica, -1)


return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti1[], lw_oggetti[],l_objects[]
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
string ls_anag_operai_estesa


lw_oggetti1[1] = dw_operai_det_1
lw_oggetti1[2] = cb_corsi
lw_oggetti1[3] = cb_storico
lw_oggetti1[4] = cb_documenti
lw_oggetti1[5] = cb_note
lw_oggetti1[6] = cb_etichette
lw_oggetti1[7] = cb_vetture
dw_folder.fu_assigntab(1, "Principale", lw_oggetti1[])
lw_oggetti[1] = dw_operai_det_2
dw_folder.fu_assigntab(2, "Studi/Esp.Lav.", lw_oggetti[])
lw_oggetti[1] = dw_operai_det_3
dw_folder.fu_assigntab(3, "Dettagli", lw_oggetti[])
lw_oggetti[1] = dw_operai_det_4
dw_folder.fu_assigntab(4, "Inquadramento", lw_oggetti[])
lw_oggetti[1] = dw_operai_det_5
dw_folder.fu_assigntab(5, "Autoriz./Inf.Med.", lw_oggetti[])
lw_oggetti[1] = dw_operai_det_6
dw_folder.fu_assigntab(6, "Visite Med.", lw_oggetti[])
dw_folder.fu_foldercreate(6, 6)
dw_folder.fu_selecttab(1)

dw_operai_lista.set_dw_key("cod_azienda")
dw_operai_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
dw_operai_det_1.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                                       c_default)
dw_operai_det_2.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
dw_operai_det_3.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)

dw_operai_det_4.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
dw_operai_det_5.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
dw_operai_det_6.set_dw_options(sqlca, &
                               dw_operai_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)

iuo_dw_main=dw_operai_lista

cb_corsi.enabled=false
cb_storico.enabled=false
cb_documenti.enabled=false
cb_note.enabled=false




l_criteriacolumn[1] = "cod_operaio"
l_criteriacolumn[2] = "num_matricola"
l_criteriacolumn[3] = "cognome"
l_criteriacolumn[4] = "nome"
l_criteriacolumn[5] = "cod_fiscale"
l_criteriacolumn[6] = "cod_socio"
l_criteriacolumn[7] = "cod_badge"
l_criteriacolumn[8] = "cod_area_aziendale"

l_searchtable[1] = "anag_operai"
l_searchtable[2] = "anag_operai"
l_searchtable[3] = "anag_operai"
l_searchtable[4] = "anag_operai"
l_searchtable[5] = "anag_operai"
l_searchtable[6] = "anag_operai"
l_searchtable[7] = "anag_operai"
l_searchtable[8] = "anag_operai"

l_searchcolumn[1] = "cod_operaio"
l_searchcolumn[2] = "num_matricola"
l_searchcolumn[3] = "cognome"
l_searchcolumn[4] = "nome"
l_searchcolumn[5] = "cod_fiscale"
l_searchcolumn[6] = "cod_socio"
l_searchcolumn[7] = "cod_badge"
l_searchcolumn[8] = "cod_area_aziendale"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_operai_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_operai_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_operai_lista.change_dw_current()

select anag_operai_estesa
into   :ls_anag_operai_estesa
from   utenti
where  cod_utente=:s_cs_xx.cod_utente;

if sqlca.sqlcode <0 then
	g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if ls_anag_operai_estesa = 'N' then
	dw_operai_det_3.Object.cod_abi.visible = 0
	dw_operai_det_3.Object.cod_cab.visible = 0
	
	//Donato 16/01/2012 questi du computed sono stati eliminati dalla dw 3
	//non si sa il perchè ... quindi li commento, perchè sono chiamati solo qui
	
	//dw_operai_det_3.Object.cf_cod_abi.visible = 0
	//dw_operai_det_3.Object.cf_cod_cab.visible = 0
	//-----------------------------------------------------------------------------
	dw_operai_det_3.Object.conto_corrente.visible = 0
				
	dw_operai_det_3.Object.cda_socio.visible = 0
	dw_operai_det_3.Object.cda_no_socio.visible = 0
	dw_operai_det_3.Object.cda_assunzione.visible = 0
	dw_operai_det_3.Object.cda_dimissioni.visible = 0
	
	dw_operai_det_4.Object.data_scatto.visible = 0
	dw_operai_det_4.Object.paga_base.visible = 0
	dw_operai_det_4.Object.contingenza.visible = 0
	dw_operai_det_4.Object.super_minimo.visible = 0
	dw_operai_det_4.Object.premio_produzione.visible = 0
	dw_operai_det_4.Object.scatti_anzianita.visible = 0
	dw_operai_det_4.Object.aggiunte.visible = 0
	dw_operai_det_4.Object.detrazioni.visible = 0
	dw_operai_det_4.Object.cf_totale.visible = 0
	
	dw_operai_det_5.Object.flag_aut_straordinari.visible = 0
	dw_operai_det_5.Object.flag_straordinari.visible = 0
	dw_operai_det_5.Object.num_max_ore_straordinari.visible = 0
	dw_operai_det_5.Object.flag_aut_comp.visible = 0					
	dw_operai_det_5.Object.flag_compensazione.visible = 0
	dw_operai_det_5.Object.num_max_ore_comp.visible = 0
	
end if

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_operai_det_2, &
                 "cod_lingua_1", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_operai_det_2, &
                 "cod_lingua_2", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_operai_det_2, &
                 "cod_lingua_3", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_operai_det_2, &
                 "cod_titolo_studio", &
                 sqlca, &
                 "tab_titoli_studio", &
                 "cod_titolo_studio", &
                 "des_titolo_studio", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_operai_det_3, &
                 "cod_mansione", &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_operai_det_3, &
                 "cod_utente", &
                 sqlca, &
                 "utenti join aziende_utenti on aziende_utenti.cod_utente = utenti.cod_utente ", &
                 "utenti.cod_utente", &
                 "utenti.nome_cognome + ' (azienda: ' + aziende_utenti.cod_azienda + ', cod: ' + aziende_utenti.cod_utente + ') '", &
                 "aziende_utenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_operai_det_4, &
                 "cod_contratto", &
                 sqlca, &
                 "tab_contratti_dip", &
                 "cod_contratto", &
                 "des_contratto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_operai_det_4, &
                 "cod_attivita", &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_operai_det_4, &
                 "cod_reparto", &
                 sqlca, &
                 "tab_reparti_dip", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_operai_det_4, &
                 "cod_qualifica", &
                 sqlca, &
                 "tab_qualifiche", &
                 "cod_qualifica", &
                 "des_qualifica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  
f_po_loaddddw_dw(dw_operai_det_4, &
                 "cod_cat_attrezzature", &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'S'")

f_po_loaddddw_sort(dw_operai_det_4, &
                 "cod_area_aziendale", &
                 sqlca, &
                 "tab_aree_aziendali", &
                 "cod_area_aziendale", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "ordinamento, cod_area_aziendale")

f_po_loaddddw_dw(dw_operai_det_4, &
                 "prog_struttura", &
                 sqlca, &
                 "tes_struttura_impegni", &
                 "prog_struttura", &
                 "descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

dw_ricerca.fu_loadcode("cod_area_aziendale", &
                       "tab_aree_aziendali", &
								"cod_area_aziendale", &
								"des_area", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' order by ordinamento, cod_area_aziendale ", "(Tutti)" )
								
								
//15/06/2011 Donato
//Gestione tipi richieste associate ad operai e risorse esterne
// stefanop: 28/03/2013: spostato nella finestra w_operai_richieste per avere OneToMany
//f_po_loaddddw_dw(dw_operai_det_3,&
//					"tipo_richiesta",&
//					sqlca,&
//					"tab_tipi_richieste",&
//					"cod_tipo_richiesta",&
//					"des_tipo_richiesta",&
//					"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
/*
f_po_loaddddw_sort(dw_operai_det_3, &
						"tipo_richiesta", &
						sqlca, &
						"tab_richieste_tipologie", &
						"cod_tipologia_richiesta", &
						"des_tipologia_richiesta", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
*/
//--------------------------------------------------------------
								
end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_corsi.enabled=false
cb_storico.enabled=false
cb_documenti.enabled=false
cb_note.enabled=false
end on

on w_operai.create
int iCurrent
call super::create
this.cb_vetture=create cb_vetture
this.cb_corsi=create cb_corsi
this.cb_storico=create cb_storico
this.cb_documenti=create cb_documenti
this.cb_note=create cb_note
this.cb_etichette=create cb_etichette
this.cb_ricerca=create cb_ricerca
this.cb_reset=create cb_reset
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_operai_lista=create dw_operai_lista
this.dw_operai_det_3=create dw_operai_det_3
this.dw_folder=create dw_folder
this.dw_operai_det_1=create dw_operai_det_1
this.dw_operai_det_6=create dw_operai_det_6
this.dw_operai_det_5=create dw_operai_det_5
this.dw_operai_det_2=create dw_operai_det_2
this.dw_operai_det_4=create dw_operai_det_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_vetture
this.Control[iCurrent+2]=this.cb_corsi
this.Control[iCurrent+3]=this.cb_storico
this.Control[iCurrent+4]=this.cb_documenti
this.Control[iCurrent+5]=this.cb_note
this.Control[iCurrent+6]=this.cb_etichette
this.Control[iCurrent+7]=this.cb_ricerca
this.Control[iCurrent+8]=this.cb_reset
this.Control[iCurrent+9]=this.dw_ricerca
this.Control[iCurrent+10]=this.dw_folder_search
this.Control[iCurrent+11]=this.dw_operai_lista
this.Control[iCurrent+12]=this.dw_operai_det_3
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.dw_operai_det_1
this.Control[iCurrent+15]=this.dw_operai_det_6
this.Control[iCurrent+16]=this.dw_operai_det_5
this.Control[iCurrent+17]=this.dw_operai_det_2
this.Control[iCurrent+18]=this.dw_operai_det_4
end on

on w_operai.destroy
call super::destroy
destroy(this.cb_vetture)
destroy(this.cb_corsi)
destroy(this.cb_storico)
destroy(this.cb_documenti)
destroy(this.cb_note)
destroy(this.cb_etichette)
destroy(this.cb_ricerca)
destroy(this.cb_reset)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_operai_lista)
destroy(this.dw_operai_det_3)
destroy(this.dw_folder)
destroy(this.dw_operai_det_1)
destroy(this.dw_operai_det_6)
destroy(this.dw_operai_det_5)
destroy(this.dw_operai_det_2)
destroy(this.dw_operai_det_4)
end on

event open;call super::open;if dw_operai_lista.rowcount() > 0 then 
	dw_operai_det_3.object.b_ricerca_abicab.visible = false
end if
end event

type cb_vetture from commandbutton within w_operai
integer x = 3040
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Vetture"
end type

event clicked;window_open_parm(w_operai_vetture, -1, dw_operai_lista)

end event

type cb_corsi from commandbutton within w_operai
integer x = 2651
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Corsi"
end type

on clicked;window_open_parm(w_corsi_dipendenti, -1, dw_operai_lista)

end on

type cb_storico from commandbutton within w_operai
integer x = 2263
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Storico"
end type

on clicked;window_open_parm(w_str_dipendenti, -1, dw_operai_lista)

end on

type cb_documenti from commandbutton within w_operai
integer x = 1874
integer y = 1620
integer width = 375
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documenti"
end type

event clicked;//window_open_parm(w_operai_blob, -1, dw_operai_lista)


if dw_operai_lista.getrow() < 1 then return

s_cs_xx.parametri.parametro_s_1 = dw_operai_lista.getitemstring(dw_operai_lista.getrow(), "cod_operaio")
open(w_operai_ole)
end event

type cb_note from commandbutton within w_operai
integer x = 1486
integer y = 1620
integer width = 375
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Note"
end type

on clicked;window_open_parm(w_doc_dipendenti, -1, dw_operai_lista)
end on

type cb_etichette from commandbutton within w_operai
integer x = 1097
integer y = 1620
integer width = 366
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichette B.C."
end type

event clicked;window_open(w_etichette_operai, -1)
end event

type cb_ricerca from commandbutton within w_operai
integer x = 2971
integer y = 140
integer width = 361
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_operai_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_reset from commandbutton within w_operai
integer x = 2971
integer y = 40
integer width = 361
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type dw_ricerca from u_dw_search within w_operai
integer x = 229
integer y = 40
integer width = 2834
integer height = 460
integer taborder = 80
string dataobject = "d_operai_search"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_operaio"
		guo_ricerca.uof_ricerca_operaio(dw_ricerca,"cod_operaio")
end choose
end event

type dw_folder_search from u_folder within w_operai
integer x = 23
integer y = 20
integer width = 3429
integer height = 520
integer taborder = 10
end type

type dw_operai_lista from uo_cs_xx_dw within w_operai
integer x = 229
integer y = 36
integer width = 3109
integer height = 492
integer taborder = 110
string dataobject = "d_operai_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and this.getitemstring(this.getrow(), "cod_operaio") <> "0" then
      cb_corsi.enabled = true
      cb_storico.enabled = true
      cb_documenti.enabled=true
      cb_note.enabled=true
   	cb_vetture.enabled=true
   else
      cb_corsi.enabled = false
      cb_storico.enabled = false
      cb_documenti.enabled=false
      cb_note.enabled=false
   	cb_vetture.enabled=false
   end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and this.getitemstring(this.getrow(), "cod_operaio") <> "0" then
      cb_corsi.enabled = true
      cb_storico.enabled = true
      cb_documenti.enabled=true
      cb_note.enabled=true
	   cb_vetture.enabled=true
   else
      cb_corsi.enabled = false
      cb_storico.enabled = false
      cb_documenti.enabled=false
      cb_note.enabled=false
   	cb_vetture.enabled=false
   end if
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   string ls_cod_contratto, ls_cod_contratto_1, ls_cod_livello, ls_cod_livello_1, &
          ls_cod_mansione, ls_cod_mansione_1, ls_cod_reparto, ls_cod_reparto_1, &
          ls_cod_qualifica, ls_cod_qualifica_1, ls_cod_attivita, ls_cod_attivita_1, &
          ls_flag_aut_straordinari, ls_flag_aut_straordinari_1, &
          ls_flag_straordinari, ls_flag_straordinari_1, ls_flag_aut_comp, ls_flag_aut_comp_1, &
          ls_flag_compensazione, ls_flag_compensazione_1, ls_cod_operaio, ls_cod_operaio_1, &
          ls_cod_cat_attrezzature, ls_cod_cat_attrezzature_1, ls_ok
   double ll_i, ld_per_par_time, ld_per_par_time_1, ld_paga_base, ld_paga_base_1, &
          ld_contingenza, ld_contingenza_1, ld_super_minimo, ld_super_minimo_1, &
          ld_premio_produzione, ld_premio_produzione_1, ld_scatti_anzianita, ld_scatti_anzianita_1, &
          ld_aggiunte, ld_aggiunte_1, ld_detrazioni, ld_detrazioni_1, &
          ld_num_max_ore_straordinari, ld_num_max_ore_straordinari_1, &
          ld_num_max_ore_comp, ld_num_max_ore_comp_1
   datetime ldt_data_scatto, ldt_data_scatto_1, ldt_oggi

   for ll_i = 1 to this.rowcount()
      ls_cod_contratto = dw_operai_det_4.getitemstring(ll_i,"cod_contratto", PRIMARY!, TRUE)
      ls_cod_livello = dw_operai_det_4.getitemstring(ll_i,"cod_livello", PRIMARY!, TRUE)
      ls_cod_mansione = dw_operai_det_3.getitemstring(ll_i,"cod_mansione", PRIMARY!, TRUE)
      ls_cod_reparto = dw_operai_det_4.getitemstring(ll_i,"cod_reparto", PRIMARY!, TRUE)
      ls_cod_qualifica = dw_operai_det_4.getitemstring(ll_i,"cod_qualifica", PRIMARY!, TRUE)
      ls_cod_cat_attrezzature = dw_operai_det_4.getitemstring(ll_i,"cod_cat_attrezzature", PRIMARY!, TRUE)
      ls_cod_attivita = dw_operai_det_4.getitemstring(ll_i,"cod_attivita", PRIMARY!, TRUE)
      ls_flag_aut_straordinari = dw_operai_det_4.getitemstring(ll_i,"flag_aut_straordinari", PRIMARY!, TRUE)
      ls_flag_straordinari = dw_operai_det_4.getitemstring(ll_i,"flag_straordinari", PRIMARY!, TRUE)
      ls_flag_aut_comp = dw_operai_det_4.getitemstring(ll_i,"flag_aut_comp", PRIMARY!, TRUE)
      ls_flag_compensazione = dw_operai_det_4.getitemstring(ll_i,"flag_compensazione", PRIMARY!, TRUE)
      ld_per_par_time = dw_operai_det_4.getitemnumber(ll_i,"per_par_time", PRIMARY!, TRUE)
      ld_paga_base = dw_operai_det_4.getitemnumber(ll_i,"paga_base", PRIMARY!, TRUE)
      ld_contingenza = dw_operai_det_4.getitemnumber(ll_i,"contingenza", PRIMARY!, TRUE)
      ld_super_minimo = dw_operai_det_4.getitemnumber(ll_i,"super_minimo", PRIMARY!, TRUE)
      ld_premio_produzione = dw_operai_det_4.getitemnumber(ll_i,"premio_produzione", PRIMARY!, TRUE)
      ld_scatti_anzianita = dw_operai_det_4.getitemnumber(ll_i,"scatti_anzianita", PRIMARY!, TRUE)
      ld_aggiunte = dw_operai_det_4.getitemnumber(ll_i,"aggiunte", PRIMARY!, TRUE)
      ld_detrazioni = dw_operai_det_4.getitemnumber(ll_i,"detrazioni", PRIMARY!, TRUE)
      ld_num_max_ore_straordinari = dw_operai_det_4.getitemnumber(ll_i,"num_max_ore_straordinari", PRIMARY!, TRUE)
      ld_num_max_ore_comp = dw_operai_det_4.getitemnumber(ll_i,"num_max_ore_comp", PRIMARY!, TRUE)
      ldt_data_scatto = dw_operai_det_4.getitemdatetime(ll_i,"data_scatto", PRIMARY!, TRUE)
   
      ls_cod_contratto_1 = dw_operai_det_4.getitemstring(ll_i,"cod_contratto")
      ls_cod_livello_1 = dw_operai_det_4.getitemstring(ll_i,"cod_livello")
      ls_cod_mansione_1 = dw_operai_det_3.getitemstring(ll_i,"cod_mansione")
      ls_cod_reparto_1 = dw_operai_det_4.getitemstring(ll_i,"cod_reparto")
      ls_cod_qualifica_1 = dw_operai_det_4.getitemstring(ll_i,"cod_qualifica")
      ls_cod_cat_attrezzature_1 = dw_operai_det_4.getitemstring(ll_i,"cod_cat_attrezzature")
      ls_cod_attivita_1 = dw_operai_det_4.getitemstring(ll_i,"cod_attivita")
      ls_flag_aut_straordinari_1 = dw_operai_det_4.getitemstring(ll_i,"flag_aut_straordinari")
      ls_flag_straordinari_1 = dw_operai_det_4.getitemstring(ll_i,"flag_straordinari")
      ls_flag_aut_comp_1 = dw_operai_det_4.getitemstring(ll_i,"flag_aut_comp")
      ls_flag_compensazione_1 = dw_operai_det_4.getitemstring(ll_i,"flag_compensazione")
      ld_per_par_time_1 = dw_operai_det_4.getitemnumber(ll_i,"per_par_time")
      ld_paga_base_1 = dw_operai_det_4.getitemnumber(ll_i,"paga_base")
      ld_contingenza_1 = dw_operai_det_4.getitemnumber(ll_i,"contingenza")
      ld_super_minimo_1 = dw_operai_det_4.getitemnumber(ll_i,"super_minimo")
      ld_premio_produzione_1 = dw_operai_det_4.getitemnumber(ll_i,"premio_produzione")
      ld_scatti_anzianita_1 = dw_operai_det_4.getitemnumber(ll_i,"scatti_anzianita")
      ld_aggiunte_1 = dw_operai_det_4.getitemnumber(ll_i,"aggiunte")
      ld_detrazioni_1 = dw_operai_det_4.getitemnumber(ll_i,"detrazioni")
      ld_num_max_ore_straordinari_1 = dw_operai_det_4.getitemnumber(ll_i,"num_max_ore_straordinari")
      ld_num_max_ore_comp_1 = dw_operai_det_4.getitemnumber(ll_i,"num_max_ore_comp")
      ldt_data_scatto_1 = dw_operai_det_4.getitemdatetime(ll_i,"data_scatto")
   
      if ls_cod_contratto                         <> ls_cod_contratto_1                          or &
         (isnull(ls_cod_contratto)                and not isnull(ls_cod_contratto_1))            or &
         (not isnull(ls_cod_contratto)            and isnull(ls_cod_contratto_1))                or &
         ls_cod_livello                           <> ls_cod_livello_1                            or &
         (isnull(ls_cod_livello)                  and not isnull(ls_cod_livello_1))              or &
         (not isnull(ls_cod_livello)              and isnull(ls_cod_livello_1))                  or &
         ls_cod_mansione                          <> ls_cod_mansione_1                           or &
         (isnull(ls_cod_mansione)                 and not isnull(ls_cod_mansione_1))             or &
         (not isnull(ls_cod_mansione)             and isnull(ls_cod_mansione_1))                 or &
         ls_cod_reparto                           <> ls_cod_reparto_1                            or &
         (isnull(ls_cod_reparto)                  and not isnull(ls_cod_reparto_1))              or &
         (not isnull(ls_cod_reparto)              and isnull(ls_cod_reparto_1))                  or &
         ls_cod_qualifica                         <> ls_cod_qualifica_1                          or &
         (isnull(ls_cod_qualifica)                and not isnull(ls_cod_qualifica_1))            or &
         (not isnull(ls_cod_qualifica)            and isnull(ls_cod_qualifica_1))                or &
         ls_cod_cat_attrezzature                  <> ls_cod_cat_attrezzature_1                   or &
         (isnull(ls_cod_cat_attrezzature)         and not isnull(ls_cod_cat_attrezzature_1))     or &
         (not isnull(ls_cod_cat_attrezzature)     and isnull(ls_cod_cat_attrezzature_1))         or &
         ls_cod_attivita                          <> ls_cod_attivita_1                           or &
         (isnull(ls_cod_attivita)                 and not isnull(ls_cod_attivita_1))             or &
         (not isnull(ls_cod_attivita)             and isnull(ls_cod_attivita_1))                 or &
         ls_flag_aut_straordinari                 <> ls_flag_aut_straordinari_1                  or &
         (isnull(ls_flag_aut_straordinari)        and not isnull(ls_flag_aut_straordinari_1))    or &
         (not isnull(ls_flag_aut_straordinari)    and isnull(ls_flag_aut_straordinari_1))        or &
         ls_flag_straordinari                     <> ls_flag_straordinari_1                      or &
         (isnull(ls_flag_straordinari)            and not isnull(ls_flag_straordinari_1))        or &
         (not isnull(ls_flag_straordinari)        and isnull(ls_flag_straordinari_1))            or &
         ls_flag_aut_comp                         <> ls_flag_aut_comp_1                          or &
         (isnull(ls_flag_aut_comp)                and not isnull(ls_flag_aut_comp_1))            or &
         (not isnull(ls_flag_aut_comp)            and isnull(ls_flag_aut_comp_1))                or &
         ls_flag_compensazione                    <> ls_flag_compensazione_1                     or &
         (isnull(ls_flag_compensazione)           and not isnull(ls_flag_compensazione_1))       or &
         (not isnull(ls_flag_compensazione)       and isnull(ls_flag_compensazione_1))           or &
         ld_per_par_time                          <> ld_per_par_time_1                           or &
         (isnull(ld_per_par_time)                 and not isnull(ld_per_par_time_1))             or &
         (not isnull(ld_per_par_time)             and isnull(ld_per_par_time_1))                 or &
         ld_paga_base                             <> ld_paga_base_1                              or &
         (isnull(ld_paga_base)                    and not isnull(ld_paga_base_1))                or &
         (not isnull(ld_paga_base)                and isnull(ld_paga_base_1))                    or &
         ld_contingenza                           <> ld_contingenza_1                            or &
         (isnull(ld_contingenza)                  and not isnull(ld_contingenza_1))              or &
         (not isnull(ld_contingenza)              and isnull(ld_contingenza_1))                  or &
         ld_super_minimo                          <> ld_super_minimo_1                           or &
         (isnull(ld_super_minimo)                 and not isnull(ld_super_minimo_1))             or &
         (not isnull(ld_super_minimo)             and isnull(ld_super_minimo_1))                 or &
         ld_premio_produzione                     <> ld_premio_produzione_1                      or &
         (isnull(ld_premio_produzione)            and not isnull(ld_premio_produzione_1))        or &
         (not isnull(ld_premio_produzione)        and isnull(ld_premio_produzione_1))            or &
         ld_scatti_anzianita                      <> ld_scatti_anzianita_1                       or &
         (isnull(ld_scatti_anzianita)             and not isnull(ld_scatti_anzianita_1))         or &
         (not isnull(ld_scatti_anzianita)         and isnull(ld_scatti_anzianita_1))             or &
         ld_aggiunte                              <> ld_aggiunte_1                               or &
         (isnull(ld_aggiunte)                     and not isnull(ld_aggiunte_1))                 or &
         (not isnull(ld_aggiunte)                 and isnull(ld_aggiunte_1))                     or &
         ld_detrazioni                            <> ld_detrazioni_1                             or &
         (isnull(ld_detrazioni)                   and not isnull(ld_detrazioni_1))               or &
         (not isnull(ld_detrazioni)               and isnull(ld_detrazioni_1))                   or &
         ld_num_max_ore_straordinari              <> ld_num_max_ore_straordinari_1               or &
         (isnull(ld_num_max_ore_straordinari)     and not isnull(ld_num_max_ore_straordinari_1)) or &
         (not isnull(ld_num_max_ore_straordinari) and isnull(ld_num_max_ore_straordinari_1))     or &
         ld_num_max_ore_comp                      <> ld_num_max_ore_comp_1                       or &
         (isnull(ld_num_max_ore_comp)             and not isnull(ld_num_max_ore_comp_1))         or &
         (not isnull(ld_num_max_ore_comp)         and isnull(ld_num_max_ore_comp_1))             or &
         ldt_data_scatto                          <> ldt_data_scatto_1                            or &
         (isnull(ldt_data_scatto)                 and not isnull(ldt_data_scatto_1))              or &
         (not isnull(ldt_data_scatto)             and isnull(ldt_data_scatto_1)) then
 
         ls_cod_operaio = this.getitemstring(ll_i,"cod_operaio")
         ldt_oggi = datetime(today())
    
         select str_dipendenti.cod_operaio
         into   :ls_ok
         from   str_dipendenti
         where  str_dipendenti.cod_azienda = :s_cs_xx.cod_azienda and
                str_dipendenti.cod_operaio = :ls_cod_operaio and
                str_dipendenti.data_modifica = :ldt_oggi;
   
         if sqlca.sqlcode = 100 then
            insert into str_dipendenti
                        (cod_azienda,
                         cod_operaio,
                         data_modifica,
                         cod_contratto,
                         cod_livello,
                         cod_mansione,
                         cod_reparto,
                         cod_qualifica,
                         cod_attivita,
                         flag_aut_straordinari,
                         flag_straordinari,
                         flag_aut_comp,
                         flag_compensazione,
                         per_par_time,
                         paga_base,
                         contingenza,
                         super_minimo,
                         premio_produzione,
                         scatti_anzianita,
                         aggiunte,
                         detrazioni,
                         num_max_ore_straordinari,
                         num_max_ore_comp,
                         data_scatto,
                         cod_cat_attrezzature)        
            values       (:s_cs_xx.cod_azienda,
                          :ls_cod_operaio,
                          :ldt_oggi,
                          :ls_cod_contratto,
                          :ls_cod_livello,
                          :ls_cod_mansione,
                          :ls_cod_reparto,
                          :ls_cod_qualifica,
                          :ls_cod_attivita,
                          :ls_flag_aut_straordinari,
                          :ls_flag_straordinari,
                          :ls_flag_aut_comp,
                          :ls_flag_compensazione,
                          :ld_per_par_time,
                          :ld_paga_base,
                          :ld_contingenza,
                          :ld_super_minimo,
                          :ld_premio_produzione,
                          :ld_scatti_anzianita,
                          :ld_aggiunte,
                          :ld_detrazioni,
                          :ld_num_max_ore_straordinari,
                          :ld_num_max_ore_comp,
                          :ldt_data_scatto,
                          :ls_cod_cat_attrezzature);
			elseif sqlca.sqlcode = 0 then
            update str_dipendenti
            set    cod_contratto = :ls_cod_contratto,
                   cod_livello = :ls_cod_livello,
                   cod_mansione = :ls_cod_mansione,
                   cod_reparto = :ls_cod_reparto,
                   cod_qualifica = :ls_cod_qualifica,
                   cod_attivita = :ls_cod_attivita,
                   flag_aut_straordinari = :ls_flag_aut_straordinari,
                   flag_straordinari = :ls_flag_straordinari,
                   flag_aut_comp = :ls_flag_aut_comp,
                   flag_compensazione = :ls_flag_compensazione,
                   per_par_time = :ld_per_par_time,
                   paga_base = :ld_paga_base,
                   contingenza = :ld_contingenza,
                   super_minimo = :ld_super_minimo,
                   premio_produzione = :ld_premio_produzione,
                   scatti_anzianita = :ld_scatti_anzianita,
                   aggiunte = :ld_aggiunte,
                   detrazioni = :ld_detrazioni,
                   num_max_ore_straordinari = :ld_num_max_ore_straordinari,
                   num_max_ore_comp = :ld_num_max_ore_comp,
                   data_scatto = :ldt_data_scatto,
                   cod_cat_attrezzature = :ls_cod_cat_attrezzature
            where	 str_dipendenti.cod_azienda = :s_cs_xx.cod_azienda and
                	 str_dipendenti.cod_operaio = :ls_cod_operaio and
                	 str_dipendenti.data_modifica = :ldt_oggi;
         end if
      end if
   next      
end if

long   ll_j

string ls_operaio, ls_cod_utente, ls_cognome, ls_nome, ls_trovati

for ll_j = 1 to rowcount()
	
	ls_operaio = getitemstring(ll_j,"cod_operaio")
	
	ls_cod_utente = getitemstring(ll_j,"cod_utente")
	
	if isnull(ls_cod_utente) or ls_cod_utente = "" then
		continue
	end if
	
	declare operai cursor for
	select 	cognome,
			 	nome
	from   	anag_operai
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				cod_operaio <> :ls_operaio and
			   cod_utente = :ls_cod_utente
	order by cognome ASC,
			   nome ASC;
				
	open operai;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("GRU","Errore nella open del cursore operai: " + sqlca.sqlerrtext)
		return 1
	end if
	
	ls_trovati = ""
	
	do while true
		
		fetch operai
		into  :ls_cognome,
				:ls_nome;
				
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("GRU","Errore nella fetch del cursore operai: " + sqlca.sqlerrtext)
			close operai;
			return 1
		elseif sqlca.sqlcode = 100 then
			close operai;
			exit
		end if
		
		ls_trovati = ls_trovati + "~n" + ls_cognome + " " + ls_nome
		
	loop
	
	if ls_trovati <> "" then
		ls_trovati = "L'utente " + ls_cod_utente + " è già collegato ai seguenti operai:" + ls_trovati
		g_mb.messagebox("GRU",ls_trovati)
		return 1
	end if
	
next
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   cb_corsi.enabled = false
   cb_storico.enabled = false
   cb_documenti.enabled=false
   cb_note.enabled=false
   cb_vetture.enabled=false
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_num_dipendente, ll_num_matricola


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if this.getitemstring(ll_i, "cod_operaio") = "0" or isnull(this.getitemstring(ll_i, "cod_operaio")) then

      select con_dipendenti.num_dipendente,
             con_dipendenti.num_matricola
      into :ll_num_dipendente,
             :ll_num_matricola
      from   con_dipendenti
      where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;
		
	if sqlca.sqlcode = 0 then
		
		// stefanop 11/04/2012
		// -----------------------------------------------------------
		if guo_functions.uof_is_gibus() then
			this.setitem(this.getrow(), "cod_operaio", right("0000" + string(ll_num_dipendente + 1), 4))
		else
			this.setitem(this.getrow(), "cod_operaio", string(ll_num_dipendente + 1))
		end if
		// -----------------------------------------------------------
		
		this.setitem(this.getrow(), "num_matricola", ll_num_matricola + 1)
	end if
      update con_dipendenti
      set    con_dipendenti.num_dipendente = :ll_num_dipendente + 1,
             con_dipendenti.num_matricola = :ll_num_matricola + 1
      where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_flag_aut_straordinari, ls_flag_straordinari, ls_flag_aut_comp, ls_flag_compensazione
   long ll_num_max_ore_straordinari, ll_num_max_ore_comp

   cb_corsi.enabled = false
   cb_storico.enabled = false
   cb_documenti.enabled=false
   cb_note.enabled=false
   cb_vetture.enabled=false
	
	dw_operai_det_1.setitem(dw_operai_det_1.getrow(),"flag_blocco","N")
	
   select con_dipendenti.flag_aut_straordinari,
          con_dipendenti.flag_straordinari,
          con_dipendenti.num_max_ore_straordinari,
          con_dipendenti.flag_aut_comp,
          con_dipendenti.flag_compensazione,
          con_dipendenti.num_max_ore_comp
   into   :ls_flag_aut_straordinari,
          :ls_flag_straordinari,
          :ll_num_max_ore_straordinari,
          :ls_flag_aut_comp,
          :ls_flag_compensazione,
          :ll_num_max_ore_comp
   from   con_dipendenti
   where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "flag_aut_straordinari", ls_flag_aut_straordinari)
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "flag_straordinari", ls_flag_straordinari)
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "num_max_ore_straordinari", ll_num_max_ore_straordinari)
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "flag_aut_comp", ls_flag_aut_comp)
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "flag_compensazione", ls_flag_compensazione)
      dw_operai_det_5.setitem(dw_operai_det_5.getrow(), "num_max_ore_comp", ll_num_max_ore_comp)
   end if
end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

string ls_cod_operaio

ls_cod_operaio = dw_operai_lista.getitemstring(dw_operai_lista.getrow(), "cod_operaio")	
delete from str_dipendenti
 where cod_azienda = :s_cs_xx.cod_azienda
	and cod_operaio = :ls_cod_operaio;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("GRU", "Errore in fase di cancellazione dati da tabella str_dipendenti " + sqlca.sqlerrtext)
end if	

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse
end event

type dw_operai_det_3 from uo_cs_xx_dw within w_operai
integer x = 46
integer y = 640
integer width = 3383
integer height = 1240
integer taborder = 40
string dataobject = "d_operai_det_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null, ls_cod_area_aziendale

   setnull(ls_null)

   choose case i_colname
			
		case "cod_utente"
			
			if isnull(i_coltext) then
				
				dw_operai_det_4.Object.cod_area_aziendale.tabsequence=160
				dw_operai_det_4.Object.cod_area_aziendale.Border='5'
				dw_operai_det_4.Object.cod_area_aziendale.Background.Mode='0'
				dw_operai_det_4.Object.cod_area_aziendale.Background.color=rgb(255,255,255)
				
			else
				
				select cod_area_aziendale
				into   :ls_cod_area_aziendale
				from   mansionari
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_utente  = :i_coltext;

				if sqlca.sqlcode = 100 or isnull(ls_cod_area_aziendale) then
					
					dw_operai_det_4.Object.cod_area_aziendale.tabsequence=160
					dw_operai_det_4.Object.cod_area_aziendale.Border='5'
					dw_operai_det_4.Object.cod_area_aziendale.Background.Mode='0'
					dw_operai_det_4.Object.cod_area_aziendale.Background.color=rgb(255,255,255)
					
				else
					g_mb.messagebox("OMNIA","Questo utente è associato ad un mansionario; quindi gli sarà associata anche la stessa area aziendale di appartenenza.", Information!)
					
					dw_operai_det_4.setitem(dw_operai_det_4.getrow(),"cod_area_aziendale", ls_cod_area_aziendale )
					dw_operai_det_4.Object.cod_area_aziendale.tabsequence=0
					dw_operai_det_4.Object.cod_area_aziendale.Border='6'
					dw_operai_det_4.Object.cod_area_aziendale.Background.Mode='1'
					
				end if
				
			end if
	
   end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_abicab"
		guo_ricerca.uof_ricerca_abicab(dw_operai_det_3, "cod_abi","cod_cab")
		
	case "b_tipi_richieste"
		window_open_parm(w_operai_richieste, -1, dw_operai_lista)
end choose


end event

event pcd_delete;call super::pcd_delete;dw_operai_det_3.object.b_ricerca_abicab.visible = false
end event

event pcd_modify;call super::pcd_modify;dw_operai_det_3.object.b_ricerca_abicab.visible = true
end event

event pcd_new;call super::pcd_new;dw_operai_det_3.object.b_ricerca_abicab.visible = true
end event

event pcd_view;call super::pcd_view;dw_operai_det_3.object.b_ricerca_abicab.visible = false
end event

event pcd_save;call super::pcd_save;dw_operai_det_3.object.b_ricerca_abicab.visible = false
end event

type dw_folder from u_folder within w_operai
integer x = 23
integer y = 540
integer width = 3429
integer height = 1360
integer taborder = 60
end type

type dw_operai_det_1 from uo_cs_xx_dw within w_operai
integer x = 46
integer y = 640
integer width = 3360
integer height = 1000
integer taborder = 130
string dataobject = "d_operai_det_1"
boolean border = false
end type

type dw_operai_det_6 from uo_cs_xx_dw within w_operai
integer x = 46
integer y = 640
integer width = 3337
integer height = 1080
integer taborder = 50
string dataobject = "d_operai_det_6"
boolean border = false
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   string ls_null

   setnull(ls_null)

   choose case i_colname
      case "cod_abi"
         f_po_loaddddw_dw(dw_operai_det_3, &
                          "cod_cab", &
                          sqlca, &
                          "tab_abicab", &
                          "cod_cab", &
                          "agenzia", &
                          "cod_abi = '" + i_coltext + "'")

         this.setitem(i_rownbr, "cod_cab", ls_null)
   end choose
end if
end on

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//   string ls_cod_abi
//
//   if this.getrow() > 0 then
//      ls_cod_abi = this.getitemstring(this.getrow(), "cod_abi")
//      f_po_loaddddw_dw(dw_operai_det_3, &
//                       "cod_cab", &
//                       sqlca, &
//                       "tab_abicab", &
//                       "cod_cab", &
//                       "agenzia", &
//                       "cod_abi = '" + ls_cod_abi + "'")
//   end if
//end if
end event

type dw_operai_det_5 from uo_cs_xx_dw within w_operai
integer x = 69
integer y = 640
integer width = 3337
integer height = 1080
integer taborder = 20
string dataobject = "d_operai_det_5"
end type

type dw_operai_det_2 from uo_cs_xx_dw within w_operai
integer x = 46
integer y = 640
integer width = 3337
integer height = 1200
integer taborder = 70
string dataobject = "d_operai_det_2"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;call super::buttonclicked;string ls_cod_operaio

if row>0 then
else
	return
end if

ls_cod_operaio = dw_operai_det_1.getitemstring(row, "cod_operaio")

if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
else
	return
end if

choose case dwo.name
	case "b_scheda"
		wf_report_scheda_qualifica(ls_cod_operaio)
end choose
end event

type dw_operai_det_4 from uo_cs_xx_dw within w_operai
integer x = 46
integer y = 640
integer width = 3337
integer height = 1020
integer taborder = 30
string dataobject = "d_operai_det_4"
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_contratto, ls_cod_utente
	long   ll_tab

   if this.getrow() > 0 then
      ls_cod_contratto = getitemstring(getrow(), "cod_contratto")
      ls_cod_utente    = getitemstring(getrow(), "cod_utente")
		
		if not isnull(ls_cod_contratto) then
			
			f_po_loaddddw_dw(dw_operai_det_4, &
								  "cod_livello", &
								  sqlca, &
								  "tab_livelli", &
								  "cod_livello", &
								  "des_livello", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_contratto = '" + ls_cod_contratto + "'")
		end if
		
		ll_tab = long(this.Object.cod_livello.tabsequence)
		
		if isnull(ls_cod_utente) and ll_tab > 0 then
			this.Object.cod_area_aziendale.tabsequence=160
			this.Object.cod_area_aziendale.Border='5'
			this.Object.cod_area_aziendale.Background.Mode='0'
			this.Object.cod_area_aziendale.Background.color=rgb(255,255,255)
		else
			this.Object.cod_area_aziendale.tabsequence=0
			this.Object.cod_area_aziendale.Border='6'
			this.Object.cod_area_aziendale.Background.Mode='1'
		end if
	end if
end if 
end event

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   string ls_cod_attivita, ls_cod_cat_attrezzature, ls_null

   setnull(ls_null)

   choose case i_colname
      case "cod_contratto"
         select tab_contratti_dip.cod_attivita
         into   :ls_cod_attivita
         from   tab_contratti_dip
         where  tab_contratti_dip.cod_azienda = :s_cs_xx.cod_azienda and
                tab_contratti_dip.cod_contratto = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(),"cod_attivita", ls_cod_attivita)
         else
            this.setitem(this.getrow(),"cod_attivita", ls_null)
         end if

         this.setitem(i_rownbr, "cod_livello", ls_null)
         f_po_loaddddw_dw(dw_operai_det_4, &
                          "cod_livello", &
                          sqlca, &
                          "tab_livelli", &
                          "cod_livello", &
                          "des_livello", &
                          "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_contratto = '" + i_coltext + "'")
      case "cod_qualifica"
         select tab_qualifiche.cod_cat_attrezzature
         into   :ls_cod_cat_attrezzature
         from   tab_qualifiche
         where  tab_qualifiche.cod_azienda = :s_cs_xx.cod_azienda and
                tab_qualifiche.cod_qualifica = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(),"cod_cat_attrezzature", ls_cod_cat_attrezzature)
         else
            this.setitem(this.getrow(),"cod_cat_attrezzature", ls_null)
         end if
   end choose
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_cod_utente
	
    ls_cod_utente    = getitemstring(getrow(), "cod_utente")
		
	if isnull(ls_cod_utente) then
		this.Object.cod_area_aziendale.tabsequence=160
		this.Object.cod_area_aziendale.Border='5'
		this.Object.cod_area_aziendale.Background.Mode='0'
		this.Object.cod_area_aziendale.Background.color=rgb(255,255,255)
	else
		this.Object.cod_area_aziendale.tabsequence=0
		this.Object.cod_area_aziendale.Border='6'
		this.Object.cod_area_aziendale.Background.Mode='1'
	end if

	
end if
end event

