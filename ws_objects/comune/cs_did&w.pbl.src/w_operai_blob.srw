﻿$PBExportHeader$w_operai_blob.srw
$PBExportComments$Finestra Gestione Operai Blob
forward
global type w_operai_blob from w_cs_xx_principale
end type
type dw_operai_blob_lista from uo_cs_xx_dw within w_operai_blob
end type
type dw_operai_blob_det from uo_cs_xx_dw within w_operai_blob
end type
type cb_note_esterne from commandbutton within w_operai_blob
end type
end forward

global type w_operai_blob from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2597
integer height = 1384
string title = "Gestione Documenti Dipendenti"
dw_operai_blob_lista dw_operai_blob_lista
dw_operai_blob_det dw_operai_blob_det
cb_note_esterne cb_note_esterne
end type
global w_operai_blob w_operai_blob

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_operai_blob_lista.set_dw_key("cod_azienda")
dw_operai_blob_lista.set_dw_key("cod_operaio")
dw_operai_blob_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
dw_operai_blob_det.set_dw_options(sqlca, &
                                    dw_operai_blob_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main=dw_operai_blob_lista

cb_note_esterne.enabled = false

end on

on w_operai_blob.create
int iCurrent
call super::create
this.dw_operai_blob_lista=create dw_operai_blob_lista
this.dw_operai_blob_det=create dw_operai_blob_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_operai_blob_lista
this.Control[iCurrent+2]=this.dw_operai_blob_det
this.Control[iCurrent+3]=this.cb_note_esterne
end on

on w_operai_blob.destroy
call super::destroy
destroy(this.dw_operai_blob_lista)
destroy(this.dw_operai_blob_det)
destroy(this.cb_note_esterne)
end on

type dw_operai_blob_lista from uo_cs_xx_dw within w_operai_blob
integer x = 23
integer y = 20
integer width = 2126
integer height = 500
integer taborder = 10
string dataobject = "d_operai_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_operaio")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_operaio

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_operaio


ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_operaio")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end on

type dw_operai_blob_det from uo_cs_xx_dw within w_operai_blob
integer x = 23
integer y = 540
integer width = 2514
integer height = 720
integer taborder = 20
string dataobject = "d_operai_blob_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_operai_blob
integer x = 2171
integer y = 440
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string 		ls_cod_operaio, ls_cod_blob, ls_db
integer 		li_i, li_risposta

transaction sqlcb
blob    		lbl_null
boolean 		lb_intranet = false
long	  		ll_prog_mimetype

setnull(lbl_null)

li_i = dw_operai_blob_lista.getrow()
ls_cod_operaio = dw_operai_blob_lista.getitemstring(li_i, "cod_operaio")
ls_cod_blob = dw_operai_blob_lista.getitemstring(li_i, "cod_blob")

// *** Michela 12/12/2007: leggo il progressivo del mimetype per la gestione dei documenti in intranet

select prog_mimetype
into	 :ll_prog_mimetype
from   anag_operai_blob
where	 cod_azienda = :s_cs_xx.cod_azienda and
       cod_operaio = :ls_cod_operaio and
		 cod_blob = :ls_cod_blob;
		 
if sqlca.sqlcode = 0 and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
	lb_intranet = true
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob anag_operai_blob.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_operai_blob
	where      anag_operai_blob.cod_azienda = :s_cs_xx.cod_azienda and
	           anag_operai_blob.cod_operaio = :ls_cod_operaio and 
	           anag_operai_blob.cod_blob = :ls_cod_blob
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob anag_operai_blob.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_operai_blob
	where      anag_operai_blob.cod_azienda = :s_cs_xx.cod_azienda and
	           anag_operai_blob.cod_operaio = :ls_cod_operaio and 
	           anag_operai_blob.cod_blob = :ls_cod_blob;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if


// *** se il blob non è nullo ed esiste il prog mimetype: sono documenti intranet
// *** se il blob non è nullo ma il progressivo si: sono documenti client/server
// *** se il blob è nullo: procedo con l'inserimento intranet

if isnull(s_cs_xx.parametri.parametro_bl_1) or ( not isnull(s_cs_xx.parametri.parametro_bl_1) and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 ) then

	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
	
	window_open(w_ole_documenti, 0)
	
	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob anag_operai_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_operaio = :ls_cod_operaio and
						  cod_blob = :ls_cod_blob
			using      sqlcb;
				
			if sqlcb.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore durante l'aggiornamento del documento sul database:" + sqlcb.sqlerrtext, stopsign!)
				destroy sqlcb;
				return -1
			end if	
	
			destroy sqlcb;
		
		else
		
			updateblob anag_operai_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_operaio = :ls_cod_operaio and 
						  cod_blob = :ls_cod_blob;
					
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA", "Errore durante l'aggiornamento del documento sul database:" + sqlca.sqlerrtext, stopsign!)
				rollback;
				return -1
			end if	
			
		end if
					
		update	anag_operai_blob
		set      prog_mimetype = :ll_prog_mimetype
		where    cod_azienda = :s_cs_xx.cod_azienda and
				   cod_operaio = :ls_cod_operaio and 
					cod_blob = :ls_cod_blob;
					
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "OMNIA", "Errore durante l'aggiornamento del mimetype del documento:" + sqlca.sqlerrtext, stopsign!)
			rollback;
			return -1
		end if
					  
		commit;
		
		
	end if
else
	
	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			updateblob anag_operai_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_operaio = :ls_cod_operaio and 
						  cod_blob = :ls_cod_blob
			using      sqlcb;
			
			destroy sqlcb;
						  
		else
			
			updateblob anag_operai_blob
			set        blob = :s_cs_xx.parametri.parametro_bl_1
			where      cod_azienda = :s_cs_xx.cod_azienda and
						  cod_operaio = :ls_cod_operaio and 
						  cod_blob = :ls_cod_blob;
						  
		end if
		commit;
	end if
	
end if

end event

