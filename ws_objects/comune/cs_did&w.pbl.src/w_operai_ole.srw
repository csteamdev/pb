﻿$PBExportHeader$w_operai_ole.srw
$PBExportComments$Finestra Gestione Operai Blob
forward
global type w_operai_ole from w_ole_v2
end type
end forward

global type w_operai_ole from w_ole_v2
end type
global w_operai_ole w_operai_ole

type variables
private:
	string is_cod_operaio
end variables

forward prototypes
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public subroutine wf_load_documents ()
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
end prototypes

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update anag_operai_blob
set 
	des_blob = :as_new_name,
	note = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :is_cod_operaio and
	cod_blob = :astr_data.progressivo_s;
	
if sqlca.sqlcode <> 0 then
	// errore durante il salvataggio
	return false
else
	// tutto ok
	return true
end if

end function

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);// cancello riga
delete from anag_operai_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :is_cod_operaio and
	cod_blob = :astr_data.progressivo_s;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from anag_operai_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :is_cod_operaio and
	cod_blob = :astr_data.progressivo_s;

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	cod_blob, &
	des_blob, &
	prog_mimetype &
FROM anag_operai_blob &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	cod_operaio ='" + is_cod_operaio + "' "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.progressivo_s = lds_documenti.getitemstring(li_i, "cod_blob")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "des_blob"), lstr_data)
next
end subroutine

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);string ls_progressivo
str_ole lstr_data

// Calcolo progressivo
select max(cod_blob)
into :ls_progressivo
from anag_operai_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :is_cod_operaio;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il calcolo del progressivo per il file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ls_progressivo) or ls_progressivo ="" then
	ls_progressivo = "0"
end if

// sapere chi ha fatto un progressivo di tipo stringa
ls_progressivo = string(integer(ls_progressivo) + 1)
	
// Inserimento nuova riga
insert into anag_operai_blob (
	cod_azienda,
	cod_operaio,
	cod_blob,
	des_blob,
	note,
	prog_mimetype)
values (
	:s_cs_xx.cod_azienda,
	:is_cod_operaio,
	:ls_progressivo,
	:as_file_name,
	:as_file_name,
	:al_prog_mimetype);

if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Aggiornamento campo blob
updateblob anag_operai_blob
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_operaio = :is_cod_operaio and
	cod_blob = :ls_progressivo;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.progressivo_s = ls_progressivo
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true
end function

on w_operai_ole.create
call super::create
end on

on w_operai_ole.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;is_cod_operaio = s_cs_xx.parametri.parametro_s_1
end event

type st_loading from w_ole_v2`st_loading within w_operai_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_operai_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_operai_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_operai_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_operai_ole
end type

