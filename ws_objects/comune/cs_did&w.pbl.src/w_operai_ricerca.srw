﻿$PBExportHeader$w_operai_ricerca.srw
forward
global type w_operai_ricerca from w_cs_xx_principale
end type
type st_1 from statictext within w_operai_ricerca
end type
type dw_find from u_dw_find within w_operai_ricerca
end type
type cb_annulla from uo_cb_close within w_operai_ricerca
end type
type cb_ok from commandbutton within w_operai_ricerca
end type
type dw_operai_ricerca from uo_cs_xx_dw within w_operai_ricerca
end type
end forward

global type w_operai_ricerca from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2510
integer height = 1608
string title = "Ricerca Dipendenti"
st_1 st_1
dw_find dw_find
cb_annulla cb_annulla
cb_ok cb_ok
dw_operai_ricerca dw_operai_ricerca
end type
global w_operai_ricerca w_operai_ricerca

type variables
long il_row
end variables

forward prototypes
public subroutine wf_pos_ricerca ()
end prototypes

public subroutine wf_pos_ricerca ();// s_cs_xx.parametri.parametro_tipo_ricerca  = 1   ----> ricerca per codice operaio
//															  2   ----> ricerca per descrizione operaio = cognome

dw_find.fu_unwiredw()

if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) or s_cs_xx.parametri.parametro_tipo_ricerca <> 2 then
	dw_operai_ricerca.setsort("cod_operaio A")
	dw_operai_ricerca.sort()
	
	dw_find.fu_wiredw(dw_operai_ricerca, "cod_operaio")
	
	st_1.text = "Ricerca per Codice:"
else
	dw_operai_ricerca.setsort("cognome A")
	dw_operai_ricerca.sort()
	
	dw_find.fu_wiredw(dw_operai_ricerca, "cognome")
	
	st_1.text = "Ricerca per COgnome:"
end if	
dw_find.setitem(dw_find.getrow(),"findtext", s_cs_xx.parametri.parametro_pos_ricerca)
setnull(s_cs_xx.parametri.parametro_pos_ricerca)
dw_find.postevent("editchanged")

end subroutine

on deactivate;call w_cs_xx_principale::deactivate;this.hide()
end on

event pc_setwindow;call super::pc_setwindow;//windowobject lw_oggetti[]


dw_operai_ricerca.set_dw_key("cod_azienda")
dw_operai_ricerca.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_retrieveasneeded + c_selectonrowfocuschange, &
                                c_default)

dw_operai_ricerca.setsort("cod_operaio A")
dw_find.fu_wiredw(dw_operai_ricerca, "cognome")

//lw_oggetti[1] = dw_operai_ricerca
//lw_oggetti[2] = st_1
//lw_oggetti[3] = dw_find
//lw_oggetti[4] = cb_codice
//lw_oggetti[5] = cb_cognome
//lw_oggetti[6] = cb_matr
//dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
//dw_folder.fu_foldercreate(1, 4)
//dw_folder.fu_selecttab(1)

dw_operai_ricerca.setfocus()
//cb_cognome.postevent("clicked")
end event

on w_operai_ricerca.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_find=create dw_find
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_operai_ricerca=create dw_operai_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_find
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ok
this.Control[iCurrent+5]=this.dw_operai_ricerca
end on

on w_operai_ricerca.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_find)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_operai_ricerca)
end on

event activate;call super::activate;if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
	wf_pos_ricerca()
else
	dw_find.setfocus()	
end if


end event

type st_1 from statictext within w_operai_ricerca
integer x = 23
integer y = 40
integer width = 754
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per cognome:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_find from u_dw_find within w_operai_ricerca
event ue_key pbm_dwnkey
integer x = 800
integer y = 20
integer width = 1646
integer height = 100
integer taborder = 20
borderstyle borderstyle = stylebox!
end type

event ue_key;call super::ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = dw_operai_ricerca.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		dw_operai_ricerca.triggerevent("pcd_next")
		if il_row < dw_operai_ricerca.rowcount() then
			il_row = dw_operai_ricerca.getrow() + 1
		end if
	Case keyuparrow!
		dw_operai_ricerca.triggerevent("pcd_previous")
		if il_row > 1 then
			il_row = dw_operai_ricerca.getrow() - 1
		end if
END CHOOSE

end event

type cb_annulla from uo_cb_close within w_operai_ricerca
integer x = 1705
integer y = 1416
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_ok from commandbutton within w_operai_ricerca
integer x = 2089
integer y = 1416
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;
if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_operai_ricerca.getitemstring(dw_operai_ricerca.getrow(),"cod_operaio"))
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_operai_ricerca.getitemstring(dw_operai_ricerca.getrow(),"cod_operaio"))
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
elseif not isnull(s_cs_xx.parametri.parametro_dw_1) then
	s_cs_xx.parametri.parametro_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_dw_1.setitem(s_cs_xx.parametri.parametro_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_operai_ricerca.getitemstring(dw_operai_ricerca.getrow(),"cod_operaio"))
	s_cs_xx.parametri.parametro_dw_1.triggerevent(itemchanged!)
end if


parent.hide()

end event

type dw_operai_ricerca from uo_cs_xx_dw within w_operai_ricerca
integer x = 23
integer y = 140
integer width = 2423
integer height = 1260
integer taborder = 50
string dataobject = "d_operai_lista_ric"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   cb_ok.postevent(clicked!)
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
datetime ldd_oggi

ldd_oggi = datetime(today())
ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event ue_key;call super::ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

event clicked;call super::clicked;choose case dwo.name
	case "cb_codice" 
		
		dw_find.fu_unwiredw()
		dw_operai_ricerca.setsort("cod_operaio A")
		dw_operai_ricerca.sort()
		dw_find.fu_wiredw(dw_operai_ricerca, "cod_operaio")
		st_1.text = "Ricerca per Codice:"
		dw_find.setfocus()
		
	case "cb_matricola" 
		
		dw_find.fu_unwiredw()
		dw_operai_ricerca.setsort("num_matricola A")
		dw_operai_ricerca.sort()
		dw_find.fu_wiredw(dw_operai_ricerca, "num_matricola")
		st_1.text = "Ricerca per Matricola:"
		dw_find.setfocus()
		
	case "cb_cognome" 
		
		dw_find.fu_unwiredw()
		dw_operai_ricerca.setsort("cognome A")
		dw_operai_ricerca.sort()
		dw_find.fu_wiredw(dw_operai_ricerca, "cognome")
		st_1.text = "Ricerca per Cognome:"
		dw_find.setfocus()
		
	case "cb_nome" 
		
		dw_find.fu_unwiredw()
		dw_operai_ricerca.setsort("nome A")
		dw_operai_ricerca.sort()
		dw_find.fu_wiredw(dw_operai_ricerca, "nome")
		st_1.text = "Ricerca per Nome:"
		dw_find.setfocus()
		
end choose
	
	




end event

