﻿$PBExportHeader$w_operai_richieste.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_operai_richieste from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_operai_richieste
end type
end forward

global type w_operai_richieste from w_cs_xx_principale
integer width = 2149
integer height = 1644
string title = "Tipi Richieste Abilitate"
dw_lista dw_lista
end type
global w_operai_richieste w_operai_richieste

event open;call super::open;string ls_modify

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default + c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)

end event

on w_operai_richieste.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_operai_richieste.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_lista,"cod_tipo_richiesta",sqlca,&
                 "tab_tipi_richieste","cod_tipo_richiesta","des_tipo_richiesta",&
                 "tab_tipi_richieste.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_lista from uo_cs_xx_dw within w_operai_richieste
integer x = 23
integer y = 20
integer width = 2053
integer height = 1500
integer taborder = 10
string dataobject = "d_operai_richieste"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_operaio
long ll_errore

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_operaio
long ll_i

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")


for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	 if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if

next


end event

