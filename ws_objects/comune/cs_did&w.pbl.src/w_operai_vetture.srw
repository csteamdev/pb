﻿$PBExportHeader$w_operai_vetture.srw
$PBExportComments$Finestra Gestione Corsi Dipendenti
forward
global type w_operai_vetture from w_cs_xx_principale
end type
type dw_operai_vetture from uo_cs_xx_dw within w_operai_vetture
end type
end forward

global type w_operai_vetture from w_cs_xx_principale
integer width = 2725
integer height = 1112
string title = "Corsi per Dipendente"
dw_operai_vetture dw_operai_vetture
end type
global w_operai_vetture w_operai_vetture

event pc_setwindow;call super::pc_setwindow;dw_operai_vetture.set_dw_key("cod_azienda")
dw_operai_vetture.set_dw_key("cod_operaio")

dw_operai_vetture.set_dw_options(sqlca, &
                                         i_openparm, &
                                         c_scrollparent, &
                                         c_default)
													  
													
iuo_dw_main = dw_operai_vetture
end event

on w_operai_vetture.create
int iCurrent
call super::create
this.dw_operai_vetture=create dw_operai_vetture
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_operai_vetture
end on

on w_operai_vetture.destroy
call super::destroy
destroy(this.dw_operai_vetture)
end on

type dw_operai_vetture from uo_cs_xx_dw within w_operai_vetture
integer x = 23
integer y = 20
integer width = 2651
integer height = 980
integer taborder = 10
string dataobject = "d_operai_vetture"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_operaio


ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_operaio

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if
next
end on

