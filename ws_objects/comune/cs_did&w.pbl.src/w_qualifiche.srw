﻿$PBExportHeader$w_qualifiche.srw
$PBExportComments$Finestra Gestione Qualifiche
forward
global type w_qualifiche from w_cs_xx_principale
end type
type dw_qualifiche from uo_cs_xx_dw within w_qualifiche
end type
end forward

global type w_qualifiche from w_cs_xx_principale
int Width=2620
int Height=1149
boolean TitleBar=true
string Title="Gestione Qualifiche"
dw_qualifiche dw_qualifiche
end type
global w_qualifiche w_qualifiche

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_qualifiche, &
                 "cod_cat_attrezzature", &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'S'")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_qualifiche.set_dw_key("cod_azienda")
dw_qualifiche.set_dw_options(sqlca, &
                             pcca.null_object, &
                             c_default, &
                             c_default)
   
end on

on w_qualifiche.create
int iCurrent
call w_cs_xx_principale::create
this.dw_qualifiche=create dw_qualifiche
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_qualifiche
end on

on w_qualifiche.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_qualifiche)
end on

type dw_qualifiche from uo_cs_xx_dw within w_qualifiche
int X=23
int Y=21
int Width=2538
int Height=1001
string DataObject="d_qualifiche"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

