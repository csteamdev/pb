﻿$PBExportHeader$w_report_calendario_corsi.srw
$PBExportComments$Window report calendario corsi
forward
global type w_report_calendario_corsi from w_cs_xx_principale
end type
type dw_report_calendario_corsi from uo_cs_xx_dw within w_report_calendario_corsi
end type
end forward

global type w_report_calendario_corsi from w_cs_xx_principale
integer width = 3255
integer height = 1944
string title = "Report Calendario Corsi"
dw_report_calendario_corsi dw_report_calendario_corsi
end type
global w_report_calendario_corsi w_report_calendario_corsi

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string   ls_cod_corso,ls_des_calendario,ls_autorizzato_da,ls_cod_operaio,ls_des_corso,ls_cognome,ls_nome,ls_cod_mansione,&
		   ls_des_mansione,ls_cod_reparto,ls_des_reparto,ls_cod_fiscale
integer  li_anno
long     ll_progressivo,ll_num_righe
datetime ldt_data_inizio,ldt_data_fine

li_anno = s_cs_xx.parametri.parametro_i_1
ll_progressivo = s_cs_xx.parametri.parametro_d_1

declare righe_corsi cursor for
select  cod_corso
from    det_calendario_corsi
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno=:li_anno
and     progr=:ll_progressivo;

open righe_corsi;

do while 1=1 
	fetch righe_corsi
	into  :ls_cod_corso;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		close righe_corsi;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	select des_corso
	into   :ls_des_corso
	from   tab_corsi
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_corso=:ls_cod_corso;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		close righe_corsi;
		return -1
	end if
	
   ll_num_righe = dw_report_calendario_corsi.rowcount() + 1
	dw_report_calendario_corsi.insertrow(ll_num_righe)
	
	dw_report_calendario_corsi.setitem(ll_num_righe,"corso",ls_des_corso)
	
	declare righe_corsi_dipendenti cursor for
	select  cod_operaio
	from    corsi_dipendenti
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_corso=:ls_cod_corso
	and     data_corso is null;

	open righe_corsi_dipendenti;
	
	do while 1=1 
		fetch righe_corsi_dipendenti
		into  :ls_cod_operaio;
		
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
	
		select nome,
				 cognome,
				 cod_mansione,
				 cod_reparto,
				 cod_fiscale
		into   :ls_nome,
				 :ls_cognome,
				 :ls_cod_mansione,
				 :ls_cod_reparto,
				 :ls_cod_fiscale
		from   anag_operai
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_operaio=:ls_cod_operaio;
		
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if
		
		select des_mansione
		into   :ls_des_mansione
		from   tab_mansioni
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_mansione=:ls_cod_mansione;
	
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if
		
		if isnull(ls_des_mansione) then ls_des_mansione=""
		
		select des_reparto
		into   :ls_des_reparto
		from   tab_reparti_dip
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_reparto=:ls_cod_reparto;
	
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if

		if isnull(ls_des_reparto) then ls_des_reparto=""
		if isnull(ls_cod_fiscale) then ls_cod_fiscale=""
		ll_num_righe = dw_report_calendario_corsi.rowcount() + 1
		dw_report_calendario_corsi.insertrow(ll_num_righe)
		dw_report_calendario_corsi.setitem(ll_num_righe,"partecipanti",ls_cognome + " " + ls_nome + " (" + ls_des_mansione +"  "+ ls_des_reparto+"  " + ls_cod_fiscale + ")")
		
	loop	
	
	close righe_corsi_dipendenti;

	
	declare righe_operai cursor for
	select  cognome,
			  nome,
			  cod_mansione,
			  cod_reparto,
			  cod_fiscale
	from    anag_operai
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_mansione in ( select cod_mansione
									  from   tab_corsi_obbligatori
									  where  cod_azienda=:s_cs_xx.cod_azienda
									  and    cod_corso=:ls_cod_corso);
									  
	open righe_operai;
	
	do while 1 = 1
		fetch righe_operai
		into  :ls_cognome,
				:ls_nome,
				:ls_cod_mansione,
				:ls_cod_reparto,
				:ls_cod_fiscale;
		
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit

		select des_mansione
		into   :ls_des_mansione
		from   tab_mansioni
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_mansione=:ls_cod_mansione;
	
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if
		
		if isnull(ls_des_mansione) then ls_des_mansione=""
		
		select des_reparto
		into   :ls_des_reparto
		from   tab_reparti_dip
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_reparto=:ls_cod_reparto;
	
		if sqlca.sqlcode <0 then
			g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
			close righe_corsi_dipendenti;
			return -1
		end if

		if isnull(ls_des_reparto) then ls_des_reparto=""
		if isnull(ls_cod_fiscale) then ls_cod_fiscale=""

		ll_num_righe = dw_report_calendario_corsi.rowcount() + 1
		dw_report_calendario_corsi.insertrow(ll_num_righe)
		dw_report_calendario_corsi.setitem(ll_num_righe,"partecipanti",ls_cognome + " " + ls_nome + " (" + ls_des_mansione +"  "+ ls_des_reparto+"  " + ls_cod_fiscale + ")")
		
	loop
	
	close righe_operai;
	
	
	
loop

close righe_corsi;

select descrizione,
		 data_inizio,
		 data_fine,
		 autorizzato_da
into   :ls_des_corso,
		 :ldt_data_inizio,
		 :ldt_data_fine,
		 :ls_autorizzato_da
from   tes_calendario_corsi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno=:li_anno
and    progr=:ll_progressivo;

if sqlca.sqlcode <0 then
	g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

dw_report_calendario_corsi.Modify("anno.text='" + string(li_anno) + "'")
dw_report_calendario_corsi.Modify("progressivo.text='" + string(ll_progressivo) + "'")
dw_report_calendario_corsi.Modify("descrizione.text='" + ls_des_corso + "'")
dw_report_calendario_corsi.Modify("data_inizio.text='" + string(date(ldt_data_inizio)) + "'")
dw_report_calendario_corsi.Modify("data_fine.text='" + string(date(ldt_data_fine)) + "'")
dw_report_calendario_corsi.Modify("autorizzato_da.text='" + ls_autorizzato_da + "'")

return 0
end function

on w_report_calendario_corsi.create
int iCurrent
call super::create
this.dw_report_calendario_corsi=create dw_report_calendario_corsi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_calendario_corsi
end on

on w_report_calendario_corsi.destroy
call super::destroy
destroy(this.dw_report_calendario_corsi)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_calendario_corsi.ib_dw_report = true

set_w_options(c_closenosave)
dw_report_calendario_corsi.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_nomodify + c_nonew +c_nodelete, &
                                    c_noresizedw + &
			                           c_nohighlightselected + &
           				               c_nocursorrowpointer +&
                      				   c_nocursorrowfocusrect)



save_on_close(c_socnosave)

end event

type dw_report_calendario_corsi from uo_cs_xx_dw within w_report_calendario_corsi
integer x = 23
integer y = 20
integer width = 3177
integer height = 1800
integer taborder = 20
string dataobject = "d_report_calendario_corsi"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;integer li_risposta

li_risposta = wf_report()

resetupdate()
end event

