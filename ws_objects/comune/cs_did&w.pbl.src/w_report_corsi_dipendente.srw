﻿$PBExportHeader$w_report_corsi_dipendente.srw
$PBExportComments$Window report corsi per dipendente
forward
global type w_report_corsi_dipendente from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_corsi_dipendente
end type
type cb_report from commandbutton within w_report_corsi_dipendente
end type
type cb_selezione from commandbutton within w_report_corsi_dipendente
end type
type dw_selezione from uo_cs_xx_dw within w_report_corsi_dipendente
end type
type dw_report from uo_cs_xx_dw within w_report_corsi_dipendente
end type
type cb_stampa from commandbutton within w_report_corsi_dipendente
end type
end forward

global type w_report_corsi_dipendente from w_cs_xx_principale
integer width = 3557
integer height = 1664
string title = "Report Corsi per Dipendente"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
cb_stampa cb_stampa
end type
global w_report_corsi_dipendente w_report_corsi_dipendente

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();string   ls_cod_operaio, ls_cod_mansione,ls_fatti,ls_da_fare,ls_mansione,ls_richiesti,ls_des_mansione,ls_sql,ls_cod_corso,&
		   ls_des_corso,ls_cognome,ls_nome,ls_report
datetime ldt_data_corso
double   ldd_punteggio_conseguito,ldd_punteggio_max,ldd_livello_soglia
long     ll_num_righe

ls_cod_operaio = dw_selezione.getitemstring(1,"rs_cod_operaio")

if isnull(ls_cod_operaio) then 
	g_mb.messagebox("GRU","Selezionare un operaio!",stopsign!)
	return -1
end if

select cod_mansione,
		 cognome,
		 nome
into   :ls_cod_mansione,
		 :ls_cognome,
		 :ls_nome
from   anag_operai
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_operaio =:ls_cod_operaio;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_cod_mansione) or ls_cod_mansione = "" then
	g_mb.messagebox("Gru","Attenzione! L'operaio selezionato non ha alcuna mansione impostata, pertanto non è possibile produrre alcun report.",stopsign!)
	return -1
end if
	
select des_mansione
into   :ls_des_mansione
from   tab_mansioni
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_mansione=:ls_cod_mansione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
	return -1
end if

ls_fatti = dw_selezione.getitemstring(1,"rs_fatti")
ls_da_fare = dw_selezione.getitemstring(1,"rs_da_fare")
ls_richiesti = dw_selezione.getitemstring(1,"rs_richiesti")
ls_mansione = dw_selezione.getitemstring(1,"rs_mansione")

if ls_fatti = 'N' and ls_da_fare = 'N' then
	g_mb.messagebox("GRU","Nessun report richiesto: non sono stati selezionati ne corsi fatti ne corsi da fare.",information!)
	return -1
end if

if ls_richiesti = 'N' and ls_mansione = 'N' then
	g_mb.messagebox("GRU","Nessun report richiesto: non sono stati selezionati ne corsi richiesti ne corsi per mansione.",information!)
	return -1
end if

dw_report.Modify("operaio.text='"+ ls_cognome +" "+ ls_nome + "'")	
dw_report.Modify("des_mansione.text='"+ ls_des_mansione + "'")	

if ls_richiesti='S' and ls_mansione = 'N' then
	ls_report = "Corsi richiesti"
end if

if ls_richiesti='N' and ls_mansione = 'S' then
	ls_report = "Corsi per mansione"
end if

if ls_richiesti='S' and ls_mansione = 'S' then
	ls_report = "Corsi per mansione e richiesti"
end if

if ls_fatti = 'S' and ls_da_fare = 'N' then
	ls_report = ls_report + " già sostenuti"
end if

if ls_fatti = 'N' and ls_da_fare = 'S' then
	ls_report = ls_report + " da effettuare"
end if

if ls_fatti = 'S' and ls_da_fare = 'S' then
	ls_report = ls_report + " da effettuare e già sostenuti"
end if


if ls_richiesti = 'S' then
	ls_sql = "select cod_corso,data_corso,punteggio_conseguito from corsi_dipendenti" + & 
				" where cod_azienda='"+ s_cs_xx.cod_azienda + "'"
	
	if ls_fatti = 'S' and ls_da_fare = 'N' then
		ls_sql = ls_sql +	" and cod_operaio = '" + ls_cod_operaio +"'" +&
					" and data_corso is not null"
	end if
	
	if ls_fatti = 'N' and ls_da_fare = 'S' then
		ls_sql = ls_sql +	" and cod_operaio = '" + ls_cod_operaio + "'" +&
					" and data_corso is null"

	end if
	
	if ls_fatti = 'S' and ls_da_fare = 'S' then
		ls_sql = ls_sql +	" and cod_operaio = '" + ls_cod_operaio +"'"
		
	end if

	ls_sql = ls_sql +	" and cod_corso not in (select cod_corso from tab_corsi_obbligatori where "+ & 
							" cod_azienda='"+ s_cs_xx.cod_azienda + "' and cod_mansione ='" + ls_cod_mansione +"') order by data_corso"
	
	declare righe_corsi dynamic cursor for sqlsa ;
	
	prepare sqlsa from :ls_sql ;
	
	open dynamic righe_corsi;
	
	do while 1 = 1
		fetch righe_corsi 
		into :ls_cod_corso,
			  :ldt_data_corso,
			  :ldd_punteggio_conseguito;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
			close righe_corsi;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit



		setnull(ls_des_corso)
		setnull(ldd_livello_soglia)
		setnull(ldd_punteggio_max)

		select des_corso,
				 punteggio_max,
				 livello_soglia
		into   :ls_des_corso,
				 :ldd_punteggio_max,
				 :ldd_livello_soglia
		from   tab_corsi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_corso=:ls_cod_corso;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
			close righe_corsi;
			return -1
		end if
		
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		dw_report.setitem(ll_num_righe,"cod_corso",ls_cod_corso)		
		dw_report.setitem(ll_num_righe,"des_corso",ls_des_corso)		
		dw_report.setitem(ll_num_righe,"data_corso",ldt_data_corso)		
		dw_report.setitem(ll_num_righe,"punteggio_max",ldd_punteggio_max)		
		dw_report.setitem(ll_num_righe,"livello_soglia",ldd_livello_soglia)		
		dw_report.setitem(ll_num_righe,"punteggio_conseguito",ldd_punteggio_conseguito)		
		
	loop
	
	CLOSE righe_corsi ;
end if	


if ls_mansione = 'S' then
	
	declare righe_corsi_1 cursor for
   select  cod_corso 
	from    tab_corsi_obbligatori
   where   cod_azienda=:s_cs_xx.cod_azienda 
	and     cod_mansione =:ls_cod_mansione;
	
	open righe_corsi_1;
	
	do while 1 = 1
		fetch righe_corsi_1 
		into :ls_cod_corso;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
			close righe_corsi_1;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit

		setnull(ldt_data_corso)
		setnull(ldd_punteggio_conseguito)
		setnull(ls_des_corso)
		setnull(ldd_livello_soglia)
		setnull(ldd_punteggio_max)

		select des_corso,
				 punteggio_max,
				 livello_soglia
		into   :ls_des_corso,
				 :ldd_punteggio_max,
				 :ldd_livello_soglia
		from   tab_corsi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_corso=:ls_cod_corso;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
			close righe_corsi;
			return -1
		end if
		
		select data_corso,
				 punteggio_conseguito
		into   :ldt_data_corso,
				 :ldd_punteggio_conseguito
		from   corsi_dipendenti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_operaio=:ls_cod_operaio
		and    cod_corso =:ls_cod_corso;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Gru","Errore sul DB:"+sqlca.sqlerrtext,stopsign!)
			close righe_corsi;
			return -1
		end if

		if ls_fatti = 'S' and ls_da_fare = 'N' then
			if isnull(ldt_data_corso) then continue
		end if
		
		if ls_fatti = 'N' and ls_da_fare = 'S' then
			if not isnull(ldt_data_corso) then continue	
		end if
		
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		dw_report.setitem(ll_num_righe,"cod_corso",ls_cod_corso)		
		dw_report.setitem(ll_num_righe,"des_corso",ls_des_corso)		
		dw_report.setitem(ll_num_righe,"data_corso",ldt_data_corso)		
		dw_report.setitem(ll_num_righe,"punteggio_max",ldd_punteggio_max)		
		dw_report.setitem(ll_num_righe,"livello_soglia",ldd_livello_soglia)		
		dw_report.setitem(ll_num_righe,"punteggio_conseguito",ldd_punteggio_conseguito)		
		
	loop
	
	CLOSE righe_corsi_1 ;
end if	

dw_report.Modify("report.text='" + ls_report + "'")	

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_selezione

this.x = 618
this.y = 289
this.width = 2163
this.height = 725

end event

on w_report_corsi_dipendente.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_stampa=create cb_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_stampa
end on

on w_report_corsi_dipendente.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_stampa)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "rs_cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_annulla from commandbutton within w_report_corsi_dipendente
integer x = 1349
integer y = 520
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_corsi_dipendente
integer x = 1737
integer y = 520
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;integer li_risposta

dw_report.reset()
li_risposta = wf_report()

if li_risposta < 0 then return

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()

cb_selezione.show()


end event

type cb_selezione from commandbutton within w_report_corsi_dipendente
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 618
parent.y = 289
parent.width = 2163
parent.height = 725

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_corsi_dipendente
integer x = 23
integer y = 20
integer width = 2080
integer height = 480
integer taborder = 10
string dataobject = "d_sel_report_corsi_dipendente"
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_corsi_dipendente
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 30
boolean enabled = false
string dataobject = "d_report_corsi_dipendente"
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_stampa from commandbutton within w_report_corsi_dipendente
integer x = 2743
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 41
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_report) 
PrintClose(job)
end event

