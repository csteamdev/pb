﻿$PBExportHeader$w_report_piano_corsi.srw
$PBExportComments$Window report piano dei corsi
forward
global type w_report_piano_corsi from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_piano_corsi
end type
type cb_report from commandbutton within w_report_piano_corsi
end type
type cb_selezione from commandbutton within w_report_piano_corsi
end type
type dw_selezione from uo_cs_xx_dw within w_report_piano_corsi
end type
type cb_stampa from commandbutton within w_report_piano_corsi
end type
type dw_report from uo_cs_xx_dw within w_report_piano_corsi
end type
end forward

global type w_report_piano_corsi from w_cs_xx_principale
integer width = 3566
integer height = 1660
string title = "Report Piano dei Corsi"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_piano_corsi w_report_piano_corsi

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
								 c_nonew + &
                         c_nodelete + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect)

iuo_dw_main = dw_report
save_on_close(c_socnosave)
dw_selezione.resetupdate()
this.x = 618
this.y = 289
this.width = 2030
this.height = 829

end event

on w_report_piano_corsi.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.cb_stampa
this.Control[iCurrent+6]=this.dw_report
end on

on w_report_piano_corsi.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "rs_cod_mansione", &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_annulla from commandbutton within w_report_piano_corsi
integer x = 1211
integer y = 620
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_piano_corsi
integer x = 1600
integer y = 620
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;integer li_risposta
string  ls_errore,ls_cod_mansione,ls_rag_soc,ls_des_mansione

dw_report.reset()

ls_cod_mansione = dw_selezione.getitemstring(1,"rs_cod_mansione")

li_risposta = f_corsi_da_fare(ls_cod_mansione,ls_errore)

if li_risposta < 0 then 
	g_mb.messagebox("GRU",ls_errore,stopsign!)
	return
end if

select rag_soc_1
into   :ls_rag_soc
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

dw_report.Modify("azienda.text='" + ls_rag_soc + "'")	

if isnull(ls_cod_mansione) or ls_cod_mansione = "" then
	dw_report.Modify("report.text='Corsi da fare per tutte le mansioni'")	
else
	select des_mansione
	into   :ls_des_mansione
	from   tab_mansioni
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_mansione=:ls_cod_mansione;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		return
	end if
	
	dw_report.Modify("report.text='Corsi da fare per la mansione " + ls_des_mansione + "'")	
end if

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.resetupdate()
dw_report.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_report.show()

cb_selezione.show()


end event

type cb_selezione from commandbutton within w_report_piano_corsi
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 618
parent.y = 289
parent.width = 2030
parent.height = 829

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_piano_corsi
integer x = 23
integer y = 20
integer width = 1943
integer height = 580
integer taborder = 10
string dataobject = "d_sel_report_piano_corsi"
borderstyle borderstyle = stylelowered!
end type

type cb_stampa from commandbutton within w_report_piano_corsi
integer x = 2743
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long job

job = PrintOpen( ) 
PrintDataWindow(job, dw_report) 
PrintClose(job)
end event

type dw_report from uo_cs_xx_dw within w_report_piano_corsi
boolean visible = false
integer x = 23
integer y = 20
integer width = 3497
integer height = 1420
integer taborder = 2
string dataobject = "d_report_piano_corsi"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long  ll_errore

ll_errore = dw_report.retrieve(s_cs_xx.cod_azienda)
end event

