﻿$PBExportHeader$w_ric_assunzione.srw
$PBExportComments$Finestra Gestione Richieste Assunzione
forward
global type w_ric_assunzione from w_cs_xx_principale
end type
type dw_ric_assunzione_lista from uo_cs_xx_dw within w_ric_assunzione
end type
type cb_1 from commandbutton within w_ric_assunzione
end type
type cb_assumi from commandbutton within w_ric_assunzione
end type
type dw_ric_assunzione_det_1 from uo_cs_xx_dw within w_ric_assunzione
end type
type dw_ric_assunzione_det_2 from uo_cs_xx_dw within w_ric_assunzione
end type
type dw_ric_assunzione_det_3 from uo_cs_xx_dw within w_ric_assunzione
end type
type dw_folder from u_folder within w_ric_assunzione
end type
end forward

global type w_ric_assunzione from w_cs_xx_principale
int X=5
int Y=161
int Width=3557
int Height=1889
boolean TitleBar=true
string Title="Gestione Richieste di Assunzione"
dw_ric_assunzione_lista dw_ric_assunzione_lista
cb_1 cb_1
cb_assumi cb_assumi
dw_ric_assunzione_det_1 dw_ric_assunzione_det_1
dw_ric_assunzione_det_2 dw_ric_assunzione_det_2
dw_ric_assunzione_det_3 dw_ric_assunzione_det_3
dw_folder dw_folder
end type
global w_ric_assunzione w_ric_assunzione

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject lw_oggetti[]


lw_oggetti[1] = dw_ric_assunzione_det_1
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
lw_oggetti[1] = dw_ric_assunzione_det_2
dw_folder.fu_assigntab(2, "Studi/Esp.Lav.", lw_oggetti[])
lw_oggetti[1] = dw_ric_assunzione_det_3
dw_folder.fu_assigntab(3, "Note", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_ric_assunzione_lista.set_dw_key("cod_azienda")
dw_ric_assunzione_lista.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_default, &
                                       c_default)
dw_ric_assunzione_det_1.set_dw_options(sqlca, &
                                       dw_ric_assunzione_lista, &
                                       c_sharedata + c_scrollparent, &
                                       c_default)
dw_ric_assunzione_det_2.set_dw_options(sqlca, &
                                       dw_ric_assunzione_lista, &
                                       c_sharedata + c_scrollparent, &
                                       c_default)
dw_ric_assunzione_det_3.set_dw_options(sqlca, &
                                       dw_ric_assunzione_lista, &
                                       c_sharedata + c_scrollparent, &
                                       c_default)

iuo_dw_main=dw_ric_assunzione_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ric_assunzione_det_2, &
                 "cod_lingua_1", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_ric_assunzione_det_2, &
                 "cod_lingua_2", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_ric_assunzione_det_2, &
                 "cod_lingua_3", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_ric_assunzione_det_3, &
                 "cod_mansione", &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_ric_assunzione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_ric_assunzione_lista=create dw_ric_assunzione_lista
this.cb_1=create cb_1
this.cb_assumi=create cb_assumi
this.dw_ric_assunzione_det_1=create dw_ric_assunzione_det_1
this.dw_ric_assunzione_det_2=create dw_ric_assunzione_det_2
this.dw_ric_assunzione_det_3=create dw_ric_assunzione_det_3
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_ric_assunzione_lista
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_assumi
this.Control[iCurrent+4]=dw_ric_assunzione_det_1
this.Control[iCurrent+5]=dw_ric_assunzione_det_2
this.Control[iCurrent+6]=dw_ric_assunzione_det_3
this.Control[iCurrent+7]=dw_folder
end on

on w_ric_assunzione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_ric_assunzione_lista)
destroy(this.cb_1)
destroy(this.cb_assumi)
destroy(this.dw_ric_assunzione_det_1)
destroy(this.dw_ric_assunzione_det_2)
destroy(this.dw_ric_assunzione_det_3)
destroy(this.dw_folder)
end on

type dw_ric_assunzione_lista from uo_cs_xx_dw within w_ric_assunzione
int X=23
int Y=1
int Width=3475
int Height=401
int TabOrder=40
string DataObject="d_ric_assunzione_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then

      this.setitem(this.getrow(), "anno_registrazione", year(today()))

      select con_dipendenti.num_registrazione
      into   :ll_num_registrazione
      from   con_dipendenti
      where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;
      if sqlca.sqlcode = 0 then
         this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione + 1)
      end if
      update con_dipendenti
      set    con_dipendenti.num_registrazione = :ll_num_registrazione + 1
      where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type cb_1 from commandbutton within w_ric_assunzione
int X=3132
int Y=1681
int Width=366
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="&Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_assumi from commandbutton within w_ric_assunzione
int X=2707
int Y=1681
int Width=366
int Height=81
int TabOrder=52
boolean BringToTop=true
string Text="Assumi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_anno_registrazione_ric, ll_num_registrazione_ric, ll_anno_termine, ll_i, &
	  ll_num_dipendente, ll_num_matricola, ll_numero

string ls_cognome, ls_nome, ls_sesso, ldt_data_nascita, ls_data_nascita, ls_provincia_nascita, &
       ls_cittadinanza, ls_indirizzo_residenza, ls_localita_residenza,  ls_frazione_residenza, &
		ls_cap_residenza, ls_provincia_residenza, ls_telefono, ls_fax, ls_cellulare, &
		ls_cod_fiscale, ls_stato_civile, ls_studi, ls_voto_finale, &
		ls_titolo_tesi, ls_studi_particolari, ls_esperienze_lavorative, ls_cod_lingua_1, &
		ls_flag_con_scritto_1,  ls_flag_con_orale_1, ls_con_lingua_1, ls_cod_lingua_2, &
		ls_flag_con_scritto_2, ls_flag_con_orale_2, ls_con_lingua_2, ls_cod_lingua_3, &
		ls_flag_con_scritto_3, ls_flag_con_orale_3, ls_con_lingua_3, ls_cod_mansione,  &
		ls_note, ls_luogo_nascita, ls_cod_operaio, ls_cod_fiscale_operai, &
		ls_flag_assunto_precedenza



ll_i = dw_ric_assunzione_lista.getrow()
ll_anno_registrazione_ric = dw_ric_assunzione_lista.getitemnumber(ll_i, "anno_registrazione")
ll_num_registrazione_ric = dw_ric_assunzione_lista.getitemnumber(ll_i, "num_registrazione")

if isnull(ll_anno_registrazione_ric) or isnull(ll_anno_registrazione_ric) then
	g_mb.messagebox("Errore","Perso riferimento della riga della lista")
	return
end if

SELECT cognome, nome, sesso,   data_nascita,   luogo_nascita,   provincia_nascita,   
		cittadinanza,   indirizzo_residenza,   localita_residenza,   frazione_residenza,   
		cap_residenza,   provincia_residenza,   telefono,   fax,   cellulare,   
		cod_fiscale,    stato_civile,     studi,       anno_termine,   voto_finale,   
		titolo_tesi,  studi_particolari,   esperienze_lavorative,   cod_lingua_1,   
		flag_con_scritto_1,   flag_con_orale_1,   con_lingua_1,   cod_lingua_2,   
		flag_con_scritto_2,   flag_con_orale_2,   con_lingua_2,   cod_lingua_3,   
		flag_con_scritto_3,   flag_con_orale_3,   con_lingua_3,   cod_mansione,   
		note, flag_assunto_precedenza
 INTO :ls_cognome, :ls_nome, :ls_sesso, :ldt_data_nascita, :ls_luogo_nascita, :ls_provincia_nascita,   
		:ls_cittadinanza, :ls_indirizzo_residenza, :ls_localita_residenza,  :ls_frazione_residenza,   
		:ls_cap_residenza, :ls_provincia_residenza, :ls_telefono, :ls_fax, :ls_cellulare,   
		:ls_cod_fiscale, :ls_stato_civile, :ls_studi, :ll_anno_termine, :ls_voto_finale,   
		:ls_titolo_tesi, :ls_studi_particolari, :ls_esperienze_lavorative, :ls_cod_lingua_1,   
		:ls_flag_con_scritto_1,  :ls_flag_con_orale_1, :ls_con_lingua_1, :ls_cod_lingua_2,   
		:ls_flag_con_scritto_2, :ls_flag_con_orale_2, :ls_con_lingua_2, :ls_cod_lingua_3,   
		:ls_flag_con_scritto_3, :ls_flag_con_orale_3, :ls_con_lingua_3, :ls_cod_mansione,   
		:ls_note  , :ls_flag_assunto_precedenza
 FROM ric_assunzione  
WHERE ( ric_assunzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( ric_assunzione.num_registrazione = :ll_num_registrazione_ric ) AND  
		( ric_assunzione.anno_registrazione = :ll_anno_registrazione_ric )   ;

if sqlca.sqlcode <>0 then 
	g_mb.messagebox("Apice", "Errore lettura tabella richieste assunzione " + sqlca.sqlerrtext)
	return
end if

if ls_flag_assunto_precedenza = "S" then //già assunta in precedenza: indicare codice dipendente
	g_mb.messagebox("Avviso", "Questa persona è già assunta ! Per assumerla con un nuovo codice dipendente, deselezionare il campo Assunto")
	return
end if

// presa da w_operai
select con_dipendenti.num_dipendente,
		 con_dipendenti.num_matricola
into   :ll_num_dipendente,
		 :ll_num_matricola
from   con_dipendenti
where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 then
	  ls_cod_operaio = string(ll_num_dipendente + 1)
	  ll_num_matricola = ll_num_matricola + 1
else
	g_mb.messagebox("Apice", "Errore lettura paramentri dipendenti "  + sqlca.sqlerrtext)
	return
end if

// controllo che num_matricola non esista già
select num_matricola
into :ll_numero
from anag_operai
where cod_azienda = :s_cs_xx.cod_azienda
  and num_matricola = :ll_num_matricola;

if sqlca.sqlcode = 0 then	// esiste già:
	select max(num_matricola)
	into :ll_num_matricola
	from anag_operai
	where cod_azienda = :s_cs_xx.cod_azienda;
end if
// fine controllo che num_matricola non esista già

update con_dipendenti
set    con_dipendenti.num_dipendente = :ll_num_dipendente + 1,
		 con_dipendenti.num_matricola = :ll_num_matricola 
where  con_dipendenti.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore aggiornamento tabella parametri dipendenti " + sqlca.sqlerrtext)
	return
end if

update ric_assunzione
set cod_operaio = :ls_cod_operaio,
	 flag_assunto_precedenza ='S'
WHERE ( ric_assunzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( ric_assunzione.num_registrazione = :ll_num_registrazione_ric ) AND  
		( ric_assunzione.anno_registrazione = :ll_anno_registrazione_ric )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore aggiornamento tabella Richieste assunzioni " + sqlca.sqlerrtext)
	rollback;
	return	
end if

INSERT INTO anag_operai  
		( cod_azienda,  cod_operaio, num_matricola, cognome, nome,   
		  sesso, data_nascita, luogo_nascita, provincia_nascita,  cittadinanza,   
		  indirizzo_residenza,   localita_residenza,  frazione_residenza,  cap_residenza,   
		  provincia_residenza,   telefono,   fax,  cellulare,  cod_fiscale,  
		  stato_civile,   studi, anno_termine,   voto_finale,   titolo_tesi,  
		  studi_particolari, 
		  esperienze_lavorative,  cod_lingua_1,   flag_con_scritto_1, 	flag_con_orale_1,   
		  con_lingua_1, 			  cod_lingua_2,   flag_con_scritto_2,  flag_con_orale_2,   
		  con_lingua_2,   		  cod_lingua_3,   flag_con_scritto_3,  flag_con_orale_3,   
		  con_lingua_3,   cod_mansione,   note,   
		  cod_titolo_studio )  
VALUES ( :s_cs_xx.cod_azienda, :ls_cod_operaio,   :ll_num_matricola, :ls_cognome, :ls_nome,   
		  :ls_sesso, :ldt_data_nascita, :ls_luogo_nascita, :ls_provincia_nascita,  :ls_cittadinanza,
		  :ls_indirizzo_residenza,   :ls_localita_residenza,  :ls_frazione_residenza,  :ls_cap_residenza, 
		  :ls_provincia_residenza,   :ls_telefono,   :ls_fax,  :ls_cellulare,  :ls_cod_fiscale,
		  :ls_stato_civile, :ls_studi, :ll_anno_termine, :ls_voto_finale, :ls_titolo_tesi,  
		  :ls_studi_particolari,   
		  :ls_esperienze_lavorative, :ls_cod_lingua_1, :ls_flag_con_scritto_1, :ls_flag_con_orale_1, 
		  :ls_con_lingua_1, :ls_cod_lingua_2,  :ls_flag_con_scritto_2,  :ls_flag_con_orale_2,   
		  :ls_con_lingua_2, :ls_cod_lingua_3,  :ls_flag_con_scritto_3,  :ls_flag_con_orale_3,   
		  :ls_con_lingua_3,   :ls_cod_mansione,   :ls_note,   null );

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore inserimento in anagrafica dipendenti " + sqlca.sqlerrtext)
	rollback;
	return
end if

commit;
g_mb.messagebox("Apice", "Copiatura in anagrafica dipendenti terminata; ~r~n Terminare la compilazione dell'assunzione in anagrafica dipendenti ")

dw_ric_assunzione_lista.triggerevent("pcd_retrieve")
dw_ric_assunzione_lista.ScrollToRow(ll_i) 








end event

type dw_ric_assunzione_det_1 from uo_cs_xx_dw within w_ric_assunzione
int X=46
int Y=521
int Width=3429
int Height=1001
int TabOrder=50
string DataObject="d_ric_assunzione_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_ric_assunzione_det_2 from uo_cs_xx_dw within w_ric_assunzione
int X=46
int Y=521
int Width=3361
int Height=1121
int TabOrder=30
string DataObject="d_ric_assunzione_det_2"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_ric_assunzione_det_3 from uo_cs_xx_dw within w_ric_assunzione
int X=46
int Y=521
int Width=2652
int Height=1101
int TabOrder=10
string DataObject="d_ric_assunzione_det_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_folder from u_folder within w_ric_assunzione
int X=23
int Y=421
int Width=3475
int Height=1241
int TabOrder=20
end type

