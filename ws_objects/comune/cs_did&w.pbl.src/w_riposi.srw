﻿$PBExportHeader$w_riposi.srw
forward
global type w_riposi from w_cs_xx_principale
end type
type dw_riposi_det from uo_cs_xx_dw within w_riposi
end type
type dw_riposi_lista from uo_cs_xx_dw within w_riposi
end type
end forward

global type w_riposi from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1998
integer height = 1360
string title = "Riposi"
dw_riposi_det dw_riposi_det
dw_riposi_lista dw_riposi_lista
end type
global w_riposi w_riposi

forward prototypes
public subroutine wf_tipo_riposo (string fs_flag_tipo_riposo)
end prototypes

public subroutine wf_tipo_riposo (string fs_flag_tipo_riposo);choose case fs_flag_tipo_riposo
	case "F"
		dw_riposi_det.Object.giorno_mese_t.Visible = 1
		dw_riposi_det.Object.giorno_mese.Visible = 1
		dw_riposi_det.Object.esempio_giorno_mese_t.Visible = 1
		
		dw_riposi_det.Object.data_inizio_t.Visible = 0
		dw_riposi_det.Object.data_inizio.Visible = 0
		dw_riposi_det.Object.data_fine_t.Visible = 0
		dw_riposi_det.Object.data_fine.Visible = 0
		
		dw_riposi_det.Object.num_giorno_t.Visible = 0
		dw_riposi_det.Object.num_giorno.Visible = 0
		
	case "L"
		dw_riposi_det.Object.giorno_mese_t.Visible = 0
		dw_riposi_det.Object.giorno_mese.Visible = 0
		dw_riposi_det.Object.esempio_giorno_mese_t.Visible = 0
		
		dw_riposi_det.Object.data_inizio_t.Visible = 1
		dw_riposi_det.Object.data_inizio.Visible = 1
		dw_riposi_det.Object.data_fine_t.Visible = 1
		dw_riposi_det.Object.data_fine.Visible = 1
		
		dw_riposi_det.Object.num_giorno_t.Visible = 0
		dw_riposi_det.Object.num_giorno.Visible = 0
		
	case "G"
		dw_riposi_det.Object.giorno_mese_t.Visible = 0
		dw_riposi_det.Object.giorno_mese.Visible = 0
		dw_riposi_det.Object.esempio_giorno_mese_t.Visible = 0
		
		dw_riposi_det.Object.data_inizio_t.Visible = 0
		dw_riposi_det.Object.data_inizio.Visible = 0
		dw_riposi_det.Object.data_fine_t.Visible = 0
		dw_riposi_det.Object.data_fine.Visible = 0
		
		dw_riposi_det.Object.num_giorno_t.Visible = 1
		dw_riposi_det.Object.num_giorno.Visible = 1
		
		
end choose
		
end subroutine

on w_riposi.create
int iCurrent
call super::create
this.dw_riposi_det=create dw_riposi_det
this.dw_riposi_lista=create dw_riposi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_riposi_det
this.Control[iCurrent+2]=this.dw_riposi_lista
end on

on w_riposi.destroy
call super::destroy
destroy(this.dw_riposi_det)
destroy(this.dw_riposi_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_riposi_lista.set_dw_key("cod_azienda")
dw_riposi_lista.set_dw_key("prog_struttura")


dw_riposi_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
dw_riposi_det.set_dw_options(sqlca, &
                                    dw_riposi_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)



iuo_dw_main = dw_riposi_lista
end event

type dw_riposi_det from uo_cs_xx_dw within w_riposi
event ue_tipo_riposo ( )
integer x = 18
integer y = 528
integer width = 1952
integer height = 732
integer taborder = 20
string dataobject = "d_riposi_det"
boolean border = false
end type

event ue_tipo_riposo();wf_tipo_riposo(getitemstring(getrow(),"flag_tipo_riposo"))
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_tipo_riposo"
		wf_tipo_riposo(data)
end choose
end event

type dw_riposi_lista from uo_cs_xx_dw within w_riposi
integer x = 18
integer y = 16
integer width = 1920
integer height = 500
integer taborder = 10
string dataobject = "d_riposi_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;call super::rowfocuschanged;dw_riposi_det.postevent("ue_tipo_riposo")
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_prog_struttura


ll_prog_struttura = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_struttura")

l_Error = Retrieve(s_cs_xx.cod_azienda,ll_prog_struttura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_prog_struttura, ll_max

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		
   END IF
	
	ll_prog_struttura = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_struttura")
	
   IF IsNull(GetItemnumber(l_Idx, "prog_struttura")) or GetItemnumber(l_Idx, "prog_struttura") < 1 THEN
		
      SetItem(l_Idx, "prog_struttura", ll_prog_struttura)
		
   END IF
	
   IF IsNull(GetItemnumber(l_Idx, "progressivo")) or GetItemnumber(l_Idx, "progressivo") < 1 THEN
		
		select max(progressivo)
		into :ll_max
		from tab_riposi
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_struttura = :ll_prog_struttura;
		
		if ll_max = 0 or isnull(ll_max) then
			ll_max = 1
		else
			ll_max ++
		end if
		
      SetItem(l_Idx, "progressivo", ll_max)
		
   END IF
	
NEXT


end event

