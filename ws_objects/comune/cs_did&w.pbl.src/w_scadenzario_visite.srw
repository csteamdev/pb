﻿$PBExportHeader$w_scadenzario_visite.srw
forward
global type w_scadenzario_visite from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_scadenzario_visite
end type
type cb_report from commandbutton within w_scadenzario_visite
end type
type cb_selezione from commandbutton within w_scadenzario_visite
end type
type dw_selezione from uo_cs_xx_dw within w_scadenzario_visite
end type
type dw_report from uo_cs_xx_dw within w_scadenzario_visite
end type
end forward

global type w_scadenzario_visite from w_cs_xx_principale
int Width=3553
int Height=1885
boolean TitleBar=true
string Title="Scadenzario Visite Mediche"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_scadenzario_visite w_scadenzario_visite

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 1390
this.height = 450

end event

on w_scadenzario_visite.create
int iCurrent
call w_cs_xx_principale::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_annulla
this.Control[iCurrent+2]=cb_report
this.Control[iCurrent+3]=cb_selezione
this.Control[iCurrent+4]=dw_selezione
this.Control[iCurrent+5]=dw_report
end on

on w_scadenzario_visite.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

type cb_annulla from commandbutton within w_scadenzario_visite
int X=572
int Y=221
int Width=366
int Height=81
int TabOrder=30
string Text="&Esci"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_scadenzario_visite
int X=961
int Y=221
int Width=366
int Height=81
int TabOrder=40
string Text="&Report"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;datetime ldt_data_da, ldt_data_a, ldt_data_prossima
string ls_cod_operaio, ls_cognome, ls_nome, ls_nota_med
long ll_riga 

dw_selezione.accepttext()

ldt_data_da = dw_selezione.getitemdatetime(1, "data_da")
ldt_data_a = dw_selezione.getitemdatetime(1, "data_a")

dw_selezione.hide()

parent.x = 10
parent.y = 5
parent.width = 3553
parent.height = 2060

dw_report.show()

// dw_report.retrieve(s_cs_xx.cod_azienda, ldt_data_da, ldt_data_a)

DECLARE cur_visite_1 CURSOR FOR 
select cod_operaio, cognome, nome, data_prossima_1, nota_med_1
from anag_operai
where cod_azienda=:s_cs_xx.cod_azienda
		and data_prossima_1 <=:ldt_data_a
		and data_prossima_1 >=:ldt_data_da
using sqlca;

ll_riga = 1
OPEN cur_visite_1 ;
DO while 0=0
	FETCH cur_visite_1 INTO :ls_cod_operaio, :ls_cognome, :ls_nome, :ldt_data_prossima, :ls_nota_med;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore lettura nel DB ", SQLCA.SQLErrText)
		return
	end if
	dw_report.setitem(ll_riga, "cod_operaio", ls_cod_operaio)
	dw_report.setitem(ll_riga, "cognome", ls_cognome)
	dw_report.setitem(ll_riga, "nome", ls_nome)
	dw_report.setitem(ll_riga, "data_visita", ldt_data_prossima)
	dw_report.setitem(ll_riga, "nota_visita", ls_nota_med)
	
	ll_riga = dw_report.insertrow(0)	
LOOP
CLOSE cur_visite_1 ;

DECLARE cur_visite_2 CURSOR FOR 
select cod_operaio, cognome, nome, data_prossima_2, nota_med_2
from anag_operai
where cod_azienda=:s_cs_xx.cod_azienda
		and data_prossima_2 <=:ldt_data_a
		and data_prossima_2 >=:ldt_data_da
using sqlca;

OPEN cur_visite_2 ;
DO while 0=0
	FETCH cur_visite_2 INTO :ls_cod_operaio, :ls_cognome, :ls_nome, :ldt_data_prossima, :ls_nota_med;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore lettura nel DB ", SQLCA.SQLErrText)
		return
	end if
	dw_report.setitem(ll_riga, "cod_operaio", ls_cod_operaio)
	dw_report.setitem(ll_riga, "cognome", ls_cognome)
	dw_report.setitem(ll_riga, "nome", ls_nome)
	dw_report.setitem(ll_riga, "data_visita", ldt_data_prossima)
	dw_report.setitem(ll_riga, "nota_visita", ls_nota_med)
	
	ll_riga = dw_report.insertrow(0)	
LOOP
CLOSE cur_visite_2 ;

DECLARE cur_visite_3 CURSOR FOR 
select cod_operaio, cognome, nome, data_prossima_3, nota_med_3
from anag_operai
where cod_azienda=:s_cs_xx.cod_azienda
		and data_prossima_3 <=:ldt_data_a
		and data_prossima_3 >=:ldt_data_da
using sqlca;

OPEN cur_visite_3 ;
DO while 0=0
	FETCH cur_visite_3 INTO :ls_cod_operaio, :ls_cognome, :ls_nome, :ldt_data_prossima, :ls_nota_med;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore lettura nel DB ", SQLCA.SQLErrText)
		return
	end if
	dw_report.setitem(ll_riga, "cod_operaio", ls_cod_operaio)
	dw_report.setitem(ll_riga, "cognome", ls_cognome)
	dw_report.setitem(ll_riga, "nome", ls_nome)
	dw_report.setitem(ll_riga, "data_visita", ldt_data_prossima)
	dw_report.setitem(ll_riga, "nota_visita", ls_nota_med)
	
	ll_riga = dw_report.insertrow(0)	
LOOP
CLOSE cur_visite_3 ;


dw_report.SetSort("data_visita A") 
dw_report.sort()

cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_scadenzario_visite
int X=3132
int Y=1681
int Width=366
int Height=81
int TabOrder=50
string Text="Selezione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
dw_selezione.show()
dw_report.reset()

parent.x = 741
parent.y = 885
parent.width = 1390
parent.height = 450

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_scadenzario_visite
int X=23
int Y=21
int Width=1303
int Height=161
int TabOrder=10
string DataObject="d_selezione_scadenzario_visite"
end type

type dw_report from uo_cs_xx_dw within w_scadenzario_visite
int X=23
int Y=21
int Width=3475
int Height=1641
int TabOrder=20
boolean Visible=false
string DataObject="d_report_scadenzario_visite"
boolean HScrollBar=true
boolean VScrollBar=true
boolean HSplitScroll=true
boolean LiveScroll=true
end type

