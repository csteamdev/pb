﻿$PBExportHeader$w_str_dipendenti.srw
$PBExportComments$Finestra Gestione Storico Dipendenti
forward
global type w_str_dipendenti from w_cs_xx_principale
end type
type dw_str_dipendenti_lista from uo_cs_xx_dw within w_str_dipendenti
end type
type dw_str_dipendenti_det from uo_cs_xx_dw within w_str_dipendenti
end type
end forward

global type w_str_dipendenti from w_cs_xx_principale
int Width=3370
int Height=1809
boolean TitleBar=true
string Title="Gestione Storico Dipendenti"
dw_str_dipendenti_lista dw_str_dipendenti_lista
dw_str_dipendenti_det dw_str_dipendenti_det
end type
global w_str_dipendenti w_str_dipendenti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_str_dipendenti_lista.set_dw_key("cod_azienda")
dw_str_dipendenti_lista.set_dw_key("cod_operaio")
dw_str_dipendenti_lista.set_dw_options(sqlca, &
                                       i_openparm, &
                                       c_scrollparent + &
                                       c_nonew + &
                                       c_nomodify + &
                                       c_nodelete + &
                                       c_noenablenewonopen + &
                                       c_noenablemodifyonopen, &
                                       c_default)
dw_str_dipendenti_det.set_dw_options(sqlca, &
                                     dw_str_dipendenti_lista, &
                                     c_sharedata + c_scrollparent + &
                                     c_nonew + &
                                     c_nomodify + &
                                     c_nodelete + &
                                     c_noenablenewonopen + &
                                     c_noenablemodifyonopen, &
                                     c_default)
iuo_dw_main = dw_str_dipendenti_lista
end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_mansione", &
                 sqlca, &
                 "tab_mansioni", &
                 "cod_mansione", &
                 "des_mansione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_contratto", &
                 sqlca, &
                 "tab_contratti_dip", &
                 "cod_contratto", &
                 "des_contratto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_reparto", &
                 sqlca, &
                 "tab_reparti_dip", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_qualifica", &
                 sqlca, &
                 "tab_qualifiche", &
                 "cod_qualifica", &
                 "des_qualifica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_attivita", &
                 sqlca, &
                 "tab_attivita", &
                 "cod_attivita", &
                 "des_attivita", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_str_dipendenti_det, &
                 "cod_cat_attrezzature", &
                 sqlca, &
                 "tab_cat_attrezzature", &
                 "cod_cat_attrezzature", &
                 "des_cat_attrezzature", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'S'")
end on

on w_str_dipendenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_str_dipendenti_lista=create dw_str_dipendenti_lista
this.dw_str_dipendenti_det=create dw_str_dipendenti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_str_dipendenti_lista
this.Control[iCurrent+2]=dw_str_dipendenti_det
end on

on w_str_dipendenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_str_dipendenti_lista)
destroy(this.dw_str_dipendenti_det)
end on

type dw_str_dipendenti_lista from uo_cs_xx_dw within w_str_dipendenti
int X=23
int Y=21
int Width=458
int Height=1661
int TabOrder=10
string DataObject="d_str_dipendenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_operaio

ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_operaio")) then
      this.setitem(ll_i, "cod_operaio", ls_cod_operaio)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_operaio


ls_cod_operaio = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_operaio")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_operaio)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_str_dipendenti_det from uo_cs_xx_dw within w_str_dipendenti
int X=503
int Y=21
int Width=2812
int Height=1661
int TabOrder=20
string DataObject="d_str_dipendenti_det"
BorderStyle BorderStyle=StyleRaised!
end type

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
   string ls_cod_attivita, ls_cod_cat_attrezzature, ls_cod_contratto

   if this.getrow() > 0 then
      ls_cod_contratto = this.getitemstring(this.getrow(), "cod_contratto")
      choose case i_colname
         case "cod_contratto"
            f_po_loaddddw_dw(dw_str_dipendenti_det, &
                             "cod_livello", &
                             sqlca, &
                             "tab_livelli", &
                             "cod_livello", &
                             "des_livello", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_contratto = '" + ls_cod_contratto + "'")
      end choose
   end if
end if 
end on

