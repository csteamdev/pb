﻿$PBExportHeader$w_tab_tipo_attivita.srw
$PBExportComments$Finestra tipo attività
forward
global type w_tab_tipo_attivita from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_tab_tipo_attivita
end type
type dw_tipo_attivita_lista from uo_cs_xx_dw within w_tab_tipo_attivita
end type
end forward

global type w_tab_tipo_attivita from w_cs_xx_principale
integer width = 3186
integer height = 1144
string title = "Tabella Tipi Attività"
cb_dettaglio cb_dettaglio
dw_tipo_attivita_lista dw_tipo_attivita_lista
end type
global w_tab_tipo_attivita w_tab_tipo_attivita

event pc_setwindow;call super::pc_setwindow;dw_tipo_attivita_lista.set_dw_key("cod_azienda")
dw_tipo_attivita_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tipo_attivita_lista
end event

on w_tab_tipo_attivita.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.dw_tipo_attivita_lista=create dw_tipo_attivita_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.dw_tipo_attivita_lista
end on

on w_tab_tipo_attivita.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.dw_tipo_attivita_lista)
end on

type cb_dettaglio from commandbutton within w_tab_tipo_attivita
integer x = 2766
integer y = 940
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettaglio"
end type

event clicked;dw_tipo_attivita_lista.accepttext()

window_open_parm(w_tab_tipo_attivita_det, -1, dw_tipo_attivita_lista)


end event

type dw_tipo_attivita_lista from uo_cs_xx_dw within w_tab_tipo_attivita
integer x = 23
integer y = 20
integer width = 3109
integer height = 900
string dataobject = "d_tab_tipo_attivita_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

