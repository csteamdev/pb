﻿$PBExportHeader$w_tab_tipo_attivita_det.srw
$PBExportComments$Finestra di dettaglio per il tipo attività
forward
global type w_tab_tipo_attivita_det from w_cs_xx_principale
end type
type dw_det_tipo_attivita_det from uo_cs_xx_dw within w_tab_tipo_attivita_det
end type
type dw_det_tipo_attivita from uo_cs_xx_dw within w_tab_tipo_attivita_det
end type
end forward

global type w_tab_tipo_attivita_det from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1906
integer height = 1744
string title = "Tipo Attività - Dettaglio"
dw_det_tipo_attivita_det dw_det_tipo_attivita_det
dw_det_tipo_attivita dw_det_tipo_attivita
end type
global w_tab_tipo_attivita_det w_tab_tipo_attivita_det

type variables
boolean ib_modifica = false
end variables

on w_tab_tipo_attivita_det.create
int iCurrent
call super::create
this.dw_det_tipo_attivita_det=create dw_det_tipo_attivita_det
this.dw_det_tipo_attivita=create dw_det_tipo_attivita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_tipo_attivita_det
this.Control[iCurrent+2]=this.dw_det_tipo_attivita
end on

on w_tab_tipo_attivita_det.destroy
call super::destroy
destroy(this.dw_det_tipo_attivita_det)
destroy(this.dw_det_tipo_attivita)
end on

event pc_setwindow;call super::pc_setwindow;string ls_descrizione

dw_det_tipo_attivita.set_dw_key("cod_azienda")
dw_det_tipo_attivita.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)
dw_det_tipo_attivita_det.set_dw_options(sqlca,dw_det_tipo_attivita,c_sharedata+c_scrollparent,c_default)
											

iuo_dw_main = dw_det_tipo_attivita
end event

type dw_det_tipo_attivita_det from uo_cs_xx_dw within w_tab_tipo_attivita_det
integer y = 600
integer width = 1851
integer height = 1020
integer taborder = 10
string dataobject = "d_tab_tipo_attivita_det_det"
boolean border = false
end type

event clicked;call super::clicked;if ib_modifica then
	if dwo.name = "cf_colore" then
		setitem(row,"colore",f_scegli_colori(getitemnumber(row,"colore")))
	end if
end if
end event

type dw_det_tipo_attivita from uo_cs_xx_dw within w_tab_tipo_attivita_det
integer x = 23
integer y = 20
integer width = 1806
integer height = 580
integer taborder = 20
string dataobject = "d_tab_tipo_attivita_det_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_descrizione, ls_cod_attivita

ls_cod_attivita = dw_det_tipo_attivita.i_parentdw.getitemstring(dw_det_tipo_attivita.i_parentdw.i_selectedrows[1], "cod_attivita")
ls_descrizione = dw_det_tipo_attivita.i_parentdw.getitemstring(dw_det_tipo_attivita.i_parentdw.i_selectedrows[1], "des_attivita")


parent.title = "Dettaglio Attività " + ls_cod_attivita + " " + ls_descrizione

l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_attivita)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_attivita

ls_cod_attivita = dw_det_tipo_attivita.i_parentdw.getitemstring(dw_det_tipo_attivita.i_parentdw.i_selectedrows[1], "cod_attivita")

for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "cod_attivita", ls_cod_attivita)
next


end event

event pcd_new;call super::pcd_new;ib_modifica = true
end event

event pcd_modify;call super::pcd_modify;ib_modifica = true
end event

event pcd_save;call super::pcd_save;ib_modifica = false
end event

event pcd_view;call super::pcd_view;ib_modifica = false
end event

