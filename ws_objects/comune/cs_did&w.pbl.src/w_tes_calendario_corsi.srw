﻿$PBExportHeader$w_tes_calendario_corsi.srw
$PBExportComments$Window tes_calendario_corsi
forward
global type w_tes_calendario_corsi from w_cs_xx_principale
end type
type dw_tes_calendario_corsi_lista from uo_cs_xx_dw within w_tes_calendario_corsi
end type
type dw_tes_calendario_corsi_det from uo_cs_xx_dw within w_tes_calendario_corsi
end type
type cb_dettaglio from commandbutton within w_tes_calendario_corsi
end type
type cb_autorizza from commandbutton within w_tes_calendario_corsi
end type
type cb_genera from commandbutton within w_tes_calendario_corsi
end type
type cb_report from commandbutton within w_tes_calendario_corsi
end type
end forward

global type w_tes_calendario_corsi from w_cs_xx_principale
integer width = 2437
integer height = 1724
string title = "Calendario Dei Corsi Aziendali"
dw_tes_calendario_corsi_lista dw_tes_calendario_corsi_lista
dw_tes_calendario_corsi_det dw_tes_calendario_corsi_det
cb_dettaglio cb_dettaglio
cb_autorizza cb_autorizza
cb_genera cb_genera
cb_report cb_report
end type
global w_tes_calendario_corsi w_tes_calendario_corsi

event pc_setwindow;call super::pc_setwindow;string ls_flag_autorizzazione

dw_tes_calendario_corsi_lista.set_dw_key("cod_azienda")
dw_tes_calendario_corsi_lista.set_dw_key("anno")
dw_tes_calendario_corsi_lista.set_dw_key("progr")

dw_tes_calendario_corsi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_calendario_corsi_det.set_dw_options(sqlca,dw_tes_calendario_corsi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main=dw_tes_calendario_corsi_lista

//Giulio: 09/11/2011 cambio gestione privilegi mansionario
//select flag_autorizzazione	 
//into 	 :ls_flag_autorizzazione
//from 	 mansionari  
//where  cod_azienda = :s_cs_xx.cod_azienda 
//and    cod_utente = :s_cs_xx.cod_utente;

uo_mansionario luo_mansionario
luo_mansionario = create uo_mansionario

ls_flag_autorizzazione = 'N'

if luo_mansionario.uof_get_privilege(luo_mansionario.verifica) then ls_flag_autorizzazione = 'S'
//--- Fine modifica Giulio

if sqlca.sqlcode < 0 then
	g_mb.messagebox("GRU","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return
end if


if ls_flag_autorizzazione = 'S' then 
	cb_autorizza.enabled = true
else
	cb_autorizza.enabled = false
end if

end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_cat_attrezzature",sqlca,&
//                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
//                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//
//f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_attrezzatura",sqlca,&
//                 "anag_attrezzature","cod_attrezzatura","descrizione",&
//                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_det_cal_attivita_det,"cod_attivita",sqlca,&
//                 "tab_attivita","cod_attivita","des_attivita",&
//                 "tab_attivita.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//
end event

on w_tes_calendario_corsi.create
int iCurrent
call super::create
this.dw_tes_calendario_corsi_lista=create dw_tes_calendario_corsi_lista
this.dw_tes_calendario_corsi_det=create dw_tes_calendario_corsi_det
this.cb_dettaglio=create cb_dettaglio
this.cb_autorizza=create cb_autorizza
this.cb_genera=create cb_genera
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_calendario_corsi_lista
this.Control[iCurrent+2]=this.dw_tes_calendario_corsi_det
this.Control[iCurrent+3]=this.cb_dettaglio
this.Control[iCurrent+4]=this.cb_autorizza
this.Control[iCurrent+5]=this.cb_genera
this.Control[iCurrent+6]=this.cb_report
end on

on w_tes_calendario_corsi.destroy
call super::destroy
destroy(this.dw_tes_calendario_corsi_lista)
destroy(this.dw_tes_calendario_corsi_det)
destroy(this.cb_dettaglio)
destroy(this.cb_autorizza)
destroy(this.cb_genera)
destroy(this.cb_report)
end on

type dw_tes_calendario_corsi_lista from uo_cs_xx_dw within w_tes_calendario_corsi
integer x = 23
integer y = 20
integer width = 2354
integer height = 740
integer taborder = 10
string dataobject = "d_tes_calendario_corsi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_progressivo
integer li_anno

li_anno=f_anno_esercizio()

select max(progr)
into   :ll_progressivo
from   tes_calendario_corsi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno=:li_anno;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

if isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 1
else
	ll_progressivo++
end if
		
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
      SetItem(l_Idx, "anno", li_anno)
	   SetItem(l_Idx, "progr", ll_progressivo)
   END IF
NEXT

end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_anno
   long ll_progr,ll_i
		
   for ll_i = 1 to deletedcount()
      li_anno = getitemnumber(ll_i, "anno", delete!, true)
      ll_progr = getitemnumber(ll_i, "progr", delete!, true)
		
		delete det_calendario_corsi
		where cod_azienda=:s_cs_xx.cod_azienda
		and   anno=:li_anno
		and   progr=:ll_progr;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
	next
	
	commit;
end if
end event

type dw_tes_calendario_corsi_det from uo_cs_xx_dw within w_tes_calendario_corsi
integer x = 23
integer y = 780
integer width = 2354
integer height = 720
integer taborder = 20
string dataobject = "d_tes_calendario_corsi_det"
borderstyle borderstyle = styleraised!
end type

type cb_dettaglio from commandbutton within w_tes_calendario_corsi
integer x = 2011
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;if dw_tes_calendario_corsi_lista.rowcount() > 0 then
	if isnull(dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.getrow(),"anno")) then return
	window_open_parm(w_det_calendario_corsi,-1,dw_tes_calendario_corsi_lista)
end if
end event

type cb_autorizza from commandbutton within w_tes_calendario_corsi
integer x = 23
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Autorizza"
end type

event clicked;integer li_anno
long ll_progr
datetime ldt_oggi

li_anno = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.getrow(),"anno")
ll_progr = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.getrow(),"progr")

ldt_oggi = datetime(today(),00:00:00)

dw_tes_calendario_corsi_det.setitem(dw_tes_calendario_corsi_det.getrow(),"autorizzato_da",s_cs_xx.cod_utente)
dw_tes_calendario_corsi_det.setitem(dw_tes_calendario_corsi_det.getrow(),"data_autorizzazione",ldt_oggi)

dw_tes_calendario_corsi_lista.resetupdate()

update tes_calendario_corsi
set    autorizzato_da=:s_cs_xx.cod_utente,
	    data_autorizzazione=:ldt_oggi
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno =:li_anno
and    progr=:ll_progr;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("GRU","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	rollback;
	dw_tes_calendario_corsi_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
	return
end if

commit;

end event

type cb_genera from commandbutton within w_tes_calendario_corsi
integer x = 411
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera Cal."
end type

event clicked;if dw_tes_calendario_corsi_lista.rowcount() > 0 then
	delete from corsi_da_fare;
	s_cs_xx.parametri.parametro_i_1 = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.getrow(),"anno")
	s_cs_xx.parametri.parametro_d_1 = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.getrow(),"progr")
	window_open(w_corsi_da_fare,0)
end if
end event

type cb_report from commandbutton within w_tes_calendario_corsi
integer x = 800
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 41
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.i_selectedrows[1], "anno")
s_cs_xx.parametri.parametro_d_1 = dw_tes_calendario_corsi_lista.getitemnumber(dw_tes_calendario_corsi_lista.i_selectedrows[1], "progr")

window_open(w_report_calendario_corsi,-1)
end event

