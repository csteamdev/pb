﻿$PBExportHeader$w_tes_struttura_impegni.srw
$PBExportComments$Finestra struttura piano impegni
forward
global type w_tes_struttura_impegni from w_cs_xx_principale
end type
type cb_dettagli from commandbutton within w_tes_struttura_impegni
end type
type cb_riposi from commandbutton within w_tes_struttura_impegni
end type
type dw_struttura_impegni_lista from uo_cs_xx_dw within w_tes_struttura_impegni
end type
end forward

global type w_tes_struttura_impegni from w_cs_xx_principale
integer width = 2670
integer height = 1024
string title = "Tabella Struttura Piano Impegni"
cb_dettagli cb_dettagli
cb_riposi cb_riposi
dw_struttura_impegni_lista dw_struttura_impegni_lista
end type
global w_tes_struttura_impegni w_tes_struttura_impegni

type variables
long il_riga
end variables

event pc_setwindow;call super::pc_setwindow;dw_struttura_impegni_lista.set_dw_key("cod_azienda")
dw_struttura_impegni_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_struttura_impegni_lista
end event

on w_tes_struttura_impegni.create
int iCurrent
call super::create
this.cb_dettagli=create cb_dettagli
this.cb_riposi=create cb_riposi
this.dw_struttura_impegni_lista=create dw_struttura_impegni_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettagli
this.Control[iCurrent+2]=this.cb_riposi
this.Control[iCurrent+3]=this.dw_struttura_impegni_lista
end on

on w_tes_struttura_impegni.destroy
call super::destroy
destroy(this.cb_dettagli)
destroy(this.cb_riposi)
destroy(this.dw_struttura_impegni_lista)
end on

type cb_dettagli from commandbutton within w_tes_struttura_impegni
integer x = 2240
integer y = 820
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dettagli"
end type

event clicked;dw_struttura_impegni_lista.accepttext()
il_riga = dw_struttura_impegni_lista.i_selectedrows[1]
window_open_parm(w_det_struttura_impegni, -1, dw_struttura_impegni_lista)

end event

type cb_riposi from commandbutton within w_tes_struttura_impegni
integer x = 23
integer y = 820
integer width = 357
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Riposi"
end type

event clicked;window_open_parm(w_riposi, 0, dw_struttura_impegni_lista)
end event

type dw_struttura_impegni_lista from uo_cs_xx_dw within w_tes_struttura_impegni
event ue_aggiorna ( )
integer x = 23
integer y = 20
integer width = 2583
integer height = 780
string dataobject = "d_tes_struttura_impegni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_aggiorna();long ll_riga

//if il_riga > 0 then
try
	if dw_struttura_impegni_lista.i_selectedrows[1] > 0 then
		
		ll_riga = dw_struttura_impegni_lista.i_selectedrows[1]
		
		dw_struttura_impegni_lista.triggerevent("pcd_retrieve") 
		dw_struttura_impegni_lista.setrow( ll_riga)
		dw_struttura_impegni_lista.scrolltorow( ll_riga)
	end if
catch (throwable err)
end try

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event rowfocuschanged;call super::rowfocuschanged;
if currentrow > 0 then &
		il_riga = currentrow
end event

