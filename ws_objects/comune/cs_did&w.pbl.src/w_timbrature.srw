﻿$PBExportHeader$w_timbrature.srw
$PBExportComments$Finestra Gestione Timbrature
forward
global type w_timbrature from w_cs_xx_principale
end type
type dw_timbrature from uo_cs_xx_dw within w_timbrature
end type
end forward

global type w_timbrature from w_cs_xx_principale
integer width = 2683
integer height = 1148
string title = "Gestione Timbrature"
dw_timbrature dw_timbrature
end type
global w_timbrature w_timbrature

type variables

end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_timbrature, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_timbrature.set_dw_key("cod_azienda")
dw_timbrature.set_dw_options(sqlca, &
                             pcca.null_object, &
                             c_default, &
                             c_default)
   
end on

on w_timbrature.create
int iCurrent
call super::create
this.dw_timbrature=create dw_timbrature
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_timbrature
end on

on w_timbrature.destroy
call super::destroy
destroy(this.dw_timbrature)
end on

event pc_new;call super::pc_new;dw_timbrature.setitem(dw_timbrature.getrow(), "data_movimento", datetime(today()))
end event

type dw_timbrature from uo_cs_xx_dw within w_timbrature
integer x = 23
integer y = 20
integer width = 2606
integer height = 1000
string dataobject = "d_timbrature"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_operaio
long ll_i, ll_prog_movimento
datetime ld_data_movimento

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if isnull(this.getitemnumber(ll_i, "prog_movimento")) or &
      this.getitemnumber(ll_i, "prog_movimento") = 0 then

      ls_cod_operaio = this.getitemstring(ll_i,"cod_operaio")
      ld_data_movimento = this.getitemdatetime(ll_i,"data_movimento")

      select max(timbrature.prog_movimento)
      into   :ll_prog_movimento
      from   timbrature
      where  timbrature.cod_azienda = :s_cs_xx.cod_azienda and
             timbrature.cod_operaio = :ls_cod_operaio and
             timbrature.data_movimento = :ld_data_movimento;

      if not isnull(ll_prog_movimento) then
         this.setitem(ll_i, "prog_movimento", ll_prog_movimento + 10)
      else
         this.setitem(ll_i, "prog_movimento", 10)
      end if
   end if
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

