﻿$PBExportHeader$w_tipi_costo_ns.srw
$PBExportComments$Finestra tipi costi nota spese
forward
global type w_tipi_costo_ns from w_cs_xx_principale
end type
type dw_tipi_costo_ns_lista from uo_cs_xx_dw within w_tipi_costo_ns
end type
end forward

global type w_tipi_costo_ns from w_cs_xx_principale
integer width = 2094
integer height = 1144
string title = "Tipi Costo Nota Spese"
dw_tipi_costo_ns_lista dw_tipi_costo_ns_lista
end type
global w_tipi_costo_ns w_tipi_costo_ns

event pc_setwindow;call super::pc_setwindow;dw_tipi_costo_ns_lista.set_dw_key("cod_azienda")
dw_tipi_costo_ns_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tipi_costo_ns_lista
end event

on w_tipi_costo_ns.create
int iCurrent
call super::create
this.dw_tipi_costo_ns_lista=create dw_tipi_costo_ns_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_costo_ns_lista
end on

on w_tipi_costo_ns.destroy
call super::destroy
destroy(this.dw_tipi_costo_ns_lista)
end on

type dw_tipi_costo_ns_lista from uo_cs_xx_dw within w_tipi_costo_ns
integer x = 23
integer y = 20
integer width = 2011
integer height = 1000
string dataobject = "d_tab_tipi_costo_ns_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

