﻿$PBExportHeader$w_titoli_studio.srw
forward
global type w_titoli_studio from w_cs_xx_principale
end type
type dw_titoli_studio from uo_cs_xx_dw within w_titoli_studio
end type
end forward

global type w_titoli_studio from w_cs_xx_principale
int Width=3470
int Height=1349
dw_titoli_studio dw_titoli_studio
end type
global w_titoli_studio w_titoli_studio

on w_titoli_studio.create
int iCurrent
call w_cs_xx_principale::create
this.dw_titoli_studio=create dw_titoli_studio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_titoli_studio
end on

on w_titoli_studio.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_titoli_studio)
end on

event pc_setwindow;call super::pc_setwindow;dw_titoli_studio.set_dw_key("cod_azienda")
dw_titoli_studio.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)

end event

type dw_titoli_studio from uo_cs_xx_dw within w_titoli_studio
int X=19
int Y=21
int Width=3393
int Height=1205
int TabOrder=1
string DataObject="d_titoli_studio"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

