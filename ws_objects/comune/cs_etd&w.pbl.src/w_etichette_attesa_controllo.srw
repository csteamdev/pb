﻿$PBExportHeader$w_etichette_attesa_controllo.srw
$PBExportComments$Composizione + Stampa Etichette Merce da Controllare
forward
global type w_etichette_attesa_controllo from w_cs_xx_risposta
end type
type dw_etichette_acc_materiali from uo_cs_xx_dw within w_etichette_attesa_controllo
end type
type gb_1 from groupbox within w_etichette_attesa_controllo
end type
type st_nrcopie from statictext within w_etichette_attesa_controllo
end type
type cb_stampa from commandbutton within w_etichette_attesa_controllo
end type
type cb_aggiorna from commandbutton within w_etichette_attesa_controllo
end type
type sle_nrcopie from singlelineedit within w_etichette_attesa_controllo
end type
type gb_3 from groupbox within w_etichette_attesa_controllo
end type
type sle_logo_csteam from singlelineedit within w_etichette_attesa_controllo
end type
type sle_logo_azienda from singlelineedit within w_etichette_attesa_controllo
end type
type sle_titolo_1 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_bar1 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_prodotto from singlelineedit within w_etichette_attesa_controllo
end type
type sle_fornitore from singlelineedit within w_etichette_attesa_controllo
end type
type sle_anno_bolla_acq from singlelineedit within w_etichette_attesa_controllo
end type
type sle_num_bolla_acq from singlelineedit within w_etichette_attesa_controllo
end type
type sle_ret_2 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_ret_3 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_ret_1 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_ret_4 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_bar2 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_titolo_2 from singlelineedit within w_etichette_attesa_controllo
end type
type sle_data_ricezione from singlelineedit within w_etichette_attesa_controllo
end type
type st_1 from statictext within w_etichette_attesa_controllo
end type
type st_3 from statictext within w_etichette_attesa_controllo
end type
type st_4 from statictext within w_etichette_attesa_controllo
end type
type st_5 from statictext within w_etichette_attesa_controllo
end type
type st_prodotto from statictext within w_etichette_attesa_controllo
end type
type st_fornitore from statictext within w_etichette_attesa_controllo
end type
type st_bolla_acq from statictext within w_etichette_attesa_controllo
end type
type st_numero_bolla from statictext within w_etichette_attesa_controllo
end type
type st_ret_1 from statictext within w_etichette_attesa_controllo
end type
type st_ret_2 from statictext within w_etichette_attesa_controllo
end type
type st_ret_3 from statictext within w_etichette_attesa_controllo
end type
type st_ret_4 from statictext within w_etichette_attesa_controllo
end type
type st_15 from statictext within w_etichette_attesa_controllo
end type
type st_20 from statictext within w_etichette_attesa_controllo
end type
type sle_data_bolla_acq from singlelineedit within w_etichette_attesa_controllo
end type
type st_data_bolla from statictext within w_etichette_attesa_controllo
end type
type r_1 from rectangle within w_etichette_attesa_controllo
end type
end forward

global type w_etichette_attesa_controllo from w_cs_xx_risposta
integer width = 2551
integer height = 1280
string title = "Stampa Etichette Materiale da Controllare"
boolean resizable = false
dw_etichette_acc_materiali dw_etichette_acc_materiali
gb_1 gb_1
st_nrcopie st_nrcopie
cb_stampa cb_stampa
cb_aggiorna cb_aggiorna
sle_nrcopie sle_nrcopie
gb_3 gb_3
sle_logo_csteam sle_logo_csteam
sle_logo_azienda sle_logo_azienda
sle_titolo_1 sle_titolo_1
sle_bar1 sle_bar1
sle_prodotto sle_prodotto
sle_fornitore sle_fornitore
sle_anno_bolla_acq sle_anno_bolla_acq
sle_num_bolla_acq sle_num_bolla_acq
sle_ret_2 sle_ret_2
sle_ret_3 sle_ret_3
sle_ret_1 sle_ret_1
sle_ret_4 sle_ret_4
sle_bar2 sle_bar2
sle_titolo_2 sle_titolo_2
sle_data_ricezione sle_data_ricezione
st_1 st_1
st_3 st_3
st_4 st_4
st_5 st_5
st_prodotto st_prodotto
st_fornitore st_fornitore
st_bolla_acq st_bolla_acq
st_numero_bolla st_numero_bolla
st_ret_1 st_ret_1
st_ret_2 st_ret_2
st_ret_3 st_ret_3
st_ret_4 st_ret_4
st_15 st_15
st_20 st_20
sle_data_bolla_acq sle_data_bolla_acq
st_data_bolla st_data_bolla
r_1 r_1
end type
global w_etichette_attesa_controllo w_etichette_attesa_controllo

type variables
uo_serial_communication iuo_serial_communication
end variables

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_etichette_acc_materiali.set_dw_key("cod_azienda")
dw_etichette_acc_materiali.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nomodify+c_nodelete,c_default)

sle_nrcopie.text = "0001"

end on

on w_etichette_attesa_controllo.create
int iCurrent
call super::create
this.dw_etichette_acc_materiali=create dw_etichette_acc_materiali
this.gb_1=create gb_1
this.st_nrcopie=create st_nrcopie
this.cb_stampa=create cb_stampa
this.cb_aggiorna=create cb_aggiorna
this.sle_nrcopie=create sle_nrcopie
this.gb_3=create gb_3
this.sle_logo_csteam=create sle_logo_csteam
this.sle_logo_azienda=create sle_logo_azienda
this.sle_titolo_1=create sle_titolo_1
this.sle_bar1=create sle_bar1
this.sle_prodotto=create sle_prodotto
this.sle_fornitore=create sle_fornitore
this.sle_anno_bolla_acq=create sle_anno_bolla_acq
this.sle_num_bolla_acq=create sle_num_bolla_acq
this.sle_ret_2=create sle_ret_2
this.sle_ret_3=create sle_ret_3
this.sle_ret_1=create sle_ret_1
this.sle_ret_4=create sle_ret_4
this.sle_bar2=create sle_bar2
this.sle_titolo_2=create sle_titolo_2
this.sle_data_ricezione=create sle_data_ricezione
this.st_1=create st_1
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_prodotto=create st_prodotto
this.st_fornitore=create st_fornitore
this.st_bolla_acq=create st_bolla_acq
this.st_numero_bolla=create st_numero_bolla
this.st_ret_1=create st_ret_1
this.st_ret_2=create st_ret_2
this.st_ret_3=create st_ret_3
this.st_ret_4=create st_ret_4
this.st_15=create st_15
this.st_20=create st_20
this.sle_data_bolla_acq=create sle_data_bolla_acq
this.st_data_bolla=create st_data_bolla
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_etichette_acc_materiali
this.Control[iCurrent+2]=this.gb_1
this.Control[iCurrent+3]=this.st_nrcopie
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.cb_aggiorna
this.Control[iCurrent+6]=this.sle_nrcopie
this.Control[iCurrent+7]=this.gb_3
this.Control[iCurrent+8]=this.sle_logo_csteam
this.Control[iCurrent+9]=this.sle_logo_azienda
this.Control[iCurrent+10]=this.sle_titolo_1
this.Control[iCurrent+11]=this.sle_bar1
this.Control[iCurrent+12]=this.sle_prodotto
this.Control[iCurrent+13]=this.sle_fornitore
this.Control[iCurrent+14]=this.sle_anno_bolla_acq
this.Control[iCurrent+15]=this.sle_num_bolla_acq
this.Control[iCurrent+16]=this.sle_ret_2
this.Control[iCurrent+17]=this.sle_ret_3
this.Control[iCurrent+18]=this.sle_ret_1
this.Control[iCurrent+19]=this.sle_ret_4
this.Control[iCurrent+20]=this.sle_bar2
this.Control[iCurrent+21]=this.sle_titolo_2
this.Control[iCurrent+22]=this.sle_data_ricezione
this.Control[iCurrent+23]=this.st_1
this.Control[iCurrent+24]=this.st_3
this.Control[iCurrent+25]=this.st_4
this.Control[iCurrent+26]=this.st_5
this.Control[iCurrent+27]=this.st_prodotto
this.Control[iCurrent+28]=this.st_fornitore
this.Control[iCurrent+29]=this.st_bolla_acq
this.Control[iCurrent+30]=this.st_numero_bolla
this.Control[iCurrent+31]=this.st_ret_1
this.Control[iCurrent+32]=this.st_ret_2
this.Control[iCurrent+33]=this.st_ret_3
this.Control[iCurrent+34]=this.st_ret_4
this.Control[iCurrent+35]=this.st_15
this.Control[iCurrent+36]=this.st_20
this.Control[iCurrent+37]=this.sle_data_bolla_acq
this.Control[iCurrent+38]=this.st_data_bolla
this.Control[iCurrent+39]=this.r_1
end on

on w_etichette_attesa_controllo.destroy
call super::destroy
destroy(this.dw_etichette_acc_materiali)
destroy(this.gb_1)
destroy(this.st_nrcopie)
destroy(this.cb_stampa)
destroy(this.cb_aggiorna)
destroy(this.sle_nrcopie)
destroy(this.gb_3)
destroy(this.sle_logo_csteam)
destroy(this.sle_logo_azienda)
destroy(this.sle_titolo_1)
destroy(this.sle_bar1)
destroy(this.sle_prodotto)
destroy(this.sle_fornitore)
destroy(this.sle_anno_bolla_acq)
destroy(this.sle_num_bolla_acq)
destroy(this.sle_ret_2)
destroy(this.sle_ret_3)
destroy(this.sle_ret_1)
destroy(this.sle_ret_4)
destroy(this.sle_bar2)
destroy(this.sle_titolo_2)
destroy(this.sle_data_ricezione)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_prodotto)
destroy(this.st_fornitore)
destroy(this.st_bolla_acq)
destroy(this.st_numero_bolla)
destroy(this.st_ret_1)
destroy(this.st_ret_2)
destroy(this.st_ret_3)
destroy(this.st_ret_4)
destroy(this.st_15)
destroy(this.st_20)
destroy(this.sle_data_bolla_acq)
destroy(this.st_data_bolla)
destroy(this.r_1)
end on

type dw_etichette_acc_materiali from uo_cs_xx_dw within w_etichette_attesa_controllo
integer x = 1280
integer y = 100
integer width = 1143
integer height = 196
integer taborder = 0
string dataobject = "d_etichette_acc_materiali"
boolean border = false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
   

END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type gb_1 from groupbox within w_etichette_attesa_controllo
integer x = 1257
integer y = 40
integer width = 1189
integer height = 280
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Riferimento Regitrazione Accettazione"
end type

type st_nrcopie from statictext within w_etichette_attesa_controllo
integer x = 1394
integer y = 420
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_etichette_attesa_controllo
integer x = 1920
integer y = 520
integer width = 503
integer height = 100
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Etichetta"
end type

event clicked;string stringa, ls_errore,ls_com
integer li_ritorno,li_risposta

li_ritorno = g_mb.messagebox("Stampa Etichette","Conferma Stampa Etichette",question!,YesNo!)

if li_ritorno <> 1 then RETURN

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "com", ls_com)

w_cs_xx_mdi.setmicrohelp("Stampa Etichette su Stampante Termica in Corso ..... ")


// ------------------------------ STAMPA SU TERMICA ---------------------------------------

stringa = char(2) + "L" + char(13)

stringa = stringa + "111100000100200" + sle_logo_csteam.text + char(13)

stringa = stringa + "221100005600380" + sle_logo_azienda.text + char(13)
stringa = stringa + "231100005500330" + sle_titolo_1.text + char(13)
stringa = stringa + "2A3104005500180" + sle_bar2.text + char(13)
stringa = stringa + "221100004200140" + "Codice Fornitore:" + sle_fornitore.text + char(13)
stringa = stringa + "221100004200100" + "Rif. Doc.(bolla):" + sle_anno_bolla_acq.text + "-" + sle_num_bolla_acq.text +char(13)
stringa = stringa + "221100004200070" + "Data Doc.(bolla):" + sle_data_bolla_acq.text + char(13)
stringa = stringa + "221100004200040" + "Data Ricezione  :" + sle_data_ricezione.text + char(13)

stringa = stringa + "211200005700130" + sle_ret_1.text + char(13)
stringa = stringa + "211200005700100" + sle_ret_2.text + char(13)
stringa = stringa + "211200005700060" + sle_ret_3.text + char(13)

stringa = stringa + "Q" + left(sle_nrcopie.text,4) + char(13)
stringa = stringa + "E" + char(13)

iuo_serial_communication = create uo_serial_communication

if iuo_serial_communication.uof_write_com ( stringa, ls_com,9600, ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

destroy iuo_serial_communication

w_cs_xx_mdi.setmicrohelp("Pronto !")

g_mb.messagebox("Stampa Etichette","Dati trasferiti a Stampante Termica",information!)

setpointer(arrow!)



end event

type cb_aggiorna from commandbutton within w_etichette_attesa_controllo
integer x = 1280
integer y = 520
integer width = 503
integer height = 100
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna Campi"
end type

event clicked;string ls_str_1
// ---------------------  imposto campi etichetta ------------------------------------------

sle_logo_csteam.text = "Consulting&Software"

select aziende.rag_soc_1
into   :ls_str_1
from aziende
where aziende.cod_azienda = :s_cs_xx.cod_azienda ;


sle_logo_azienda.text = ls_str_1
sle_titolo_1.text ="MATERIALE IN ATTESA DI CONTROLLO"
sle_titolo_2.text =""

sle_data_ricezione.text = string(dw_etichette_acc_materiali.getitemdatetime(dw_etichette_acc_materiali.getrow(), "data_eff_consegna") ,"dd-mm-yyyy")
sle_prodotto.text = dw_etichette_acc_materiali.getitemstring(dw_etichette_acc_materiali.getrow(), "cod_prodotto") 
sle_fornitore.text = dw_etichette_acc_materiali.getitemstring(dw_etichette_acc_materiali.getrow(), "cod_fornitore")
sle_anno_bolla_acq.text= string(dw_etichette_acc_materiali.getitemnumber(dw_etichette_acc_materiali.getrow(), "anno_bolla_acq"))
sle_num_bolla_acq.text = dw_etichette_acc_materiali.getitemstring(dw_etichette_acc_materiali.getrow(), "num_bolla_acq" )
sle_bar1.text = "" 
sle_bar2.text = sle_fornitore.text + sle_num_bolla_acq.text + sle_anno_bolla_acq.text

sle_ret_1.text = "Materiali"
sle_ret_2.text = "in attesa'"
sle_ret_3.text = "controllo"
sle_ret_4.text = ""



end event

type sle_nrcopie from singlelineedit within w_etichette_attesa_controllo
integer x = 1897
integer y = 420
integer width = 526
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "0001"
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

on losefocus;if len(sle_nrcopie.text) <> 4 then
   sle_nrcopie.setfocus()
end if
	
end on

type gb_3 from groupbox within w_etichette_attesa_controllo
integer x = 1257
integer y = 340
integer width = 1189
integer height = 300
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa Etichette"
end type

type sle_logo_csteam from singlelineedit within w_etichette_attesa_controllo
boolean visible = false
integer x = 1303
integer y = 640
integer width = 709
integer height = 60
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_logo_azienda from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 56
integer width = 709
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_titolo_1 from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 156
integer width = 709
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_bar1 from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 356
integer width = 709
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_prodotto from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 656
integer width = 709
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_fornitore from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 760
integer width = 709
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_anno_bolla_acq from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 856
integer width = 709
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_num_bolla_acq from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 956
integer width = 709
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_2 from singlelineedit within w_etichette_attesa_controllo
integer x = 1646
integer y = 756
integer width = 800
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_3 from singlelineedit within w_etichette_attesa_controllo
integer x = 1646
integer y = 856
integer width = 800
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_1 from singlelineedit within w_etichette_attesa_controllo
integer x = 1646
integer y = 656
integer width = 800
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_4 from singlelineedit within w_etichette_attesa_controllo
integer x = 1646
integer y = 956
integer width = 800
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_bar2 from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 456
integer width = 709
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_titolo_2 from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 256
integer width = 709
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_data_ricezione from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 556
integer width = 709
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 60
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 160
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 260
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura Piede:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 360
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codice a Barre 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 760
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Fornitore:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_fornitore from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 660
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_bolla_acq from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 860
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Anno Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_numero_bolla from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 960
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Numero Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_1 from statictext within w_etichette_attesa_controllo
integer x = 1280
integer y = 660
integer width = 366
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_2 from statictext within w_etichette_attesa_controllo
integer x = 1280
integer y = 760
integer width = 366
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_3 from statictext within w_etichette_attesa_controllo
integer x = 1280
integer y = 860
integer width = 366
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_4 from statictext within w_etichette_attesa_controllo
integer x = 1280
integer y = 960
integer width = 366
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_15 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 460
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codice a Barre 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 560
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Ricezione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_data_bolla_acq from singlelineedit within w_etichette_attesa_controllo
integer x = 503
integer y = 1056
integer width = 709
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_data_bolla from statictext within w_etichette_attesa_controllo
integer x = 46
integer y = 1060
integer width = 457
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Data Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type r_1 from rectangle within w_etichette_attesa_controllo
integer linethickness = 5
long fillcolor = 79741120
integer x = 23
integer y = 20
integer width = 2491
integer height = 1160
end type

