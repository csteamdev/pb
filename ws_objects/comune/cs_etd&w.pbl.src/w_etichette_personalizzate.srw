﻿$PBExportHeader$w_etichette_personalizzate.srw
$PBExportComments$Composizione + Stampa Etichette Personalizzate
forward
global type w_etichette_personalizzate from w_cs_xx_risposta
end type
type st_4 from statictext within w_etichette_personalizzate
end type
type st_2 from statictext within w_etichette_personalizzate
end type
type em_num_commessa from editmask within w_etichette_personalizzate
end type
type em_anno_commessa from editmask within w_etichette_personalizzate
end type
type st_1 from statictext within w_etichette_personalizzate
end type
type st_3 from statictext within w_etichette_personalizzate
end type
type st_prodotto from statictext within w_etichette_personalizzate
end type
type st_fornitore from statictext within w_etichette_personalizzate
end type
type st_20 from statictext within w_etichette_personalizzate
end type
type st_quantita from statictext within w_etichette_personalizzate
end type
type st_rapporto from statictext within w_etichette_personalizzate
end type
type st_difformita from statictext within w_etichette_personalizzate
end type
type st_nrcopie from statictext within w_etichette_personalizzate
end type
type cb_stampa from commandbutton within w_etichette_personalizzate
end type
type sle_nrcopie from singlelineedit within w_etichette_personalizzate
end type
type gb_3 from groupbox within w_etichette_personalizzate
end type
type sle_logo_csteam from singlelineedit within w_etichette_personalizzate
end type
type sle_titolo_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_2 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_3 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_4 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_5 from singlelineedit within w_etichette_personalizzate
end type
type sle_riga_6 from singlelineedit within w_etichette_personalizzate
end type
type sle_logo_azienda from singlelineedit within w_etichette_personalizzate
end type
type st_ret_1 from statictext within w_etichette_personalizzate
end type
type st_ret_2 from statictext within w_etichette_personalizzate
end type
type st_ret_3 from statictext within w_etichette_personalizzate
end type
type st_ret_4 from statictext within w_etichette_personalizzate
end type
type sle_ret_2 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_3 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_ret_4 from singlelineedit within w_etichette_personalizzate
end type
type sle_avv_1 from singlelineedit within w_etichette_personalizzate
end type
type sle_avv_2 from singlelineedit within w_etichette_personalizzate
end type
type st_ret_20 from statictext within w_etichette_personalizzate
end type
type st_ret_21 from statictext within w_etichette_personalizzate
end type
type cb_2 from commandbutton within w_etichette_personalizzate
end type
type r_1 from rectangle within w_etichette_personalizzate
end type
end forward

global type w_etichette_personalizzate from w_cs_xx_risposta
integer width = 2432
integer height = 1540
string title = "Etichette Personalizzate"
boolean resizable = false
st_4 st_4
st_2 st_2
em_num_commessa em_num_commessa
em_anno_commessa em_anno_commessa
st_1 st_1
st_3 st_3
st_prodotto st_prodotto
st_fornitore st_fornitore
st_20 st_20
st_quantita st_quantita
st_rapporto st_rapporto
st_difformita st_difformita
st_nrcopie st_nrcopie
cb_stampa cb_stampa
sle_nrcopie sle_nrcopie
gb_3 gb_3
sle_logo_csteam sle_logo_csteam
sle_titolo_1 sle_titolo_1
sle_riga_2 sle_riga_2
sle_riga_3 sle_riga_3
sle_riga_1 sle_riga_1
sle_riga_4 sle_riga_4
sle_riga_5 sle_riga_5
sle_riga_6 sle_riga_6
sle_logo_azienda sle_logo_azienda
st_ret_1 st_ret_1
st_ret_2 st_ret_2
st_ret_3 st_ret_3
st_ret_4 st_ret_4
sle_ret_2 sle_ret_2
sle_ret_3 sle_ret_3
sle_ret_1 sle_ret_1
sle_ret_4 sle_ret_4
sle_avv_1 sle_avv_1
sle_avv_2 sle_avv_2
st_ret_20 st_ret_20
st_ret_21 st_ret_21
cb_2 cb_2
r_1 r_1
end type
global w_etichette_personalizzate w_etichette_personalizzate

type variables
uo_serial_communication iuo_serial_communication
end variables

event pc_setwindow;call super::pc_setwindow;string ls_str_1

sle_nrcopie.text = "0001"
em_anno_commessa.text=string(f_anno_esercizio ( ))
end event

on w_etichette_personalizzate.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_2=create st_2
this.em_num_commessa=create em_num_commessa
this.em_anno_commessa=create em_anno_commessa
this.st_1=create st_1
this.st_3=create st_3
this.st_prodotto=create st_prodotto
this.st_fornitore=create st_fornitore
this.st_20=create st_20
this.st_quantita=create st_quantita
this.st_rapporto=create st_rapporto
this.st_difformita=create st_difformita
this.st_nrcopie=create st_nrcopie
this.cb_stampa=create cb_stampa
this.sle_nrcopie=create sle_nrcopie
this.gb_3=create gb_3
this.sle_logo_csteam=create sle_logo_csteam
this.sle_titolo_1=create sle_titolo_1
this.sle_riga_2=create sle_riga_2
this.sle_riga_3=create sle_riga_3
this.sle_riga_1=create sle_riga_1
this.sle_riga_4=create sle_riga_4
this.sle_riga_5=create sle_riga_5
this.sle_riga_6=create sle_riga_6
this.sle_logo_azienda=create sle_logo_azienda
this.st_ret_1=create st_ret_1
this.st_ret_2=create st_ret_2
this.st_ret_3=create st_ret_3
this.st_ret_4=create st_ret_4
this.sle_ret_2=create sle_ret_2
this.sle_ret_3=create sle_ret_3
this.sle_ret_1=create sle_ret_1
this.sle_ret_4=create sle_ret_4
this.sle_avv_1=create sle_avv_1
this.sle_avv_2=create sle_avv_2
this.st_ret_20=create st_ret_20
this.st_ret_21=create st_ret_21
this.cb_2=create cb_2
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.em_num_commessa
this.Control[iCurrent+4]=this.em_anno_commessa
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.st_prodotto
this.Control[iCurrent+8]=this.st_fornitore
this.Control[iCurrent+9]=this.st_20
this.Control[iCurrent+10]=this.st_quantita
this.Control[iCurrent+11]=this.st_rapporto
this.Control[iCurrent+12]=this.st_difformita
this.Control[iCurrent+13]=this.st_nrcopie
this.Control[iCurrent+14]=this.cb_stampa
this.Control[iCurrent+15]=this.sle_nrcopie
this.Control[iCurrent+16]=this.gb_3
this.Control[iCurrent+17]=this.sle_logo_csteam
this.Control[iCurrent+18]=this.sle_titolo_1
this.Control[iCurrent+19]=this.sle_riga_2
this.Control[iCurrent+20]=this.sle_riga_3
this.Control[iCurrent+21]=this.sle_riga_1
this.Control[iCurrent+22]=this.sle_riga_4
this.Control[iCurrent+23]=this.sle_riga_5
this.Control[iCurrent+24]=this.sle_riga_6
this.Control[iCurrent+25]=this.sle_logo_azienda
this.Control[iCurrent+26]=this.st_ret_1
this.Control[iCurrent+27]=this.st_ret_2
this.Control[iCurrent+28]=this.st_ret_3
this.Control[iCurrent+29]=this.st_ret_4
this.Control[iCurrent+30]=this.sle_ret_2
this.Control[iCurrent+31]=this.sle_ret_3
this.Control[iCurrent+32]=this.sle_ret_1
this.Control[iCurrent+33]=this.sle_ret_4
this.Control[iCurrent+34]=this.sle_avv_1
this.Control[iCurrent+35]=this.sle_avv_2
this.Control[iCurrent+36]=this.st_ret_20
this.Control[iCurrent+37]=this.st_ret_21
this.Control[iCurrent+38]=this.cb_2
this.Control[iCurrent+39]=this.r_1
end on

on w_etichette_personalizzate.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_2)
destroy(this.em_num_commessa)
destroy(this.em_anno_commessa)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.st_prodotto)
destroy(this.st_fornitore)
destroy(this.st_20)
destroy(this.st_quantita)
destroy(this.st_rapporto)
destroy(this.st_difformita)
destroy(this.st_nrcopie)
destroy(this.cb_stampa)
destroy(this.sle_nrcopie)
destroy(this.gb_3)
destroy(this.sle_logo_csteam)
destroy(this.sle_titolo_1)
destroy(this.sle_riga_2)
destroy(this.sle_riga_3)
destroy(this.sle_riga_1)
destroy(this.sle_riga_4)
destroy(this.sle_riga_5)
destroy(this.sle_riga_6)
destroy(this.sle_logo_azienda)
destroy(this.st_ret_1)
destroy(this.st_ret_2)
destroy(this.st_ret_3)
destroy(this.st_ret_4)
destroy(this.sle_ret_2)
destroy(this.sle_ret_3)
destroy(this.sle_ret_1)
destroy(this.sle_ret_4)
destroy(this.sle_avv_1)
destroy(this.sle_avv_2)
destroy(this.st_ret_20)
destroy(this.st_ret_21)
destroy(this.cb_2)
destroy(this.r_1)
end on

type st_4 from statictext within w_etichette_personalizzate
integer x = 1371
integer y = 500
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Nr. Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_etichette_personalizzate
integer x = 1371
integer y = 420
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Anno Commessa"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_commessa from editmask within w_etichette_personalizzate
integer x = 1851
integer y = 500
integer width = 389
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "######"
end type

type em_anno_commessa from editmask within w_etichette_personalizzate
integer x = 1851
integer y = 400
integer width = 389
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
end type

type st_1 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 40
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Azienda:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 140
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Titolo Alto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prodotto from statictext within w_etichette_personalizzate
integer x = 46
integer y = 440
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_fornitore from statictext within w_etichette_personalizzate
integer x = 46
integer y = 340
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 240
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_quantita from statictext within w_etichette_personalizzate
integer x = 46
integer y = 540
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_rapporto from statictext within w_etichette_personalizzate
integer x = 46
integer y = 640
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 5:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_difformita from statictext within w_etichette_personalizzate
integer x = 46
integer y = 740
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Riga 6:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_nrcopie from statictext within w_etichette_personalizzate
integer x = 1326
integer y = 120
integer width = 498
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Nr Copie Etichetta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_etichette_personalizzate
integer x = 1829
integer y = 220
integer width = 503
integer height = 100
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Etichetta"
end type

event clicked;string stringa, ls_errore,ls_com
integer li_ritorno,li_risposta

setpointer(hourglass!)
li_ritorno = g_mb.messagebox("Stampa Etichette","Conferma Stampa Etichette",question!,YesNo!)

if li_ritorno <> 1 then RETURN

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilocorrente, "com", ls_com)

w_cs_xx_mdi.setmicrohelp("Stampa Etichette su Stampante Termica in Corso ..... ")

// ------------------------------ STAMPA SU TERMICA ---------------------------------------

stringa = char(2) + "L" + char(13)

stringa = stringa + "111100000100200" + sle_logo_csteam.text + char(13)

stringa = stringa + "221100005600395" + sle_logo_azienda.text + char(13)
stringa = stringa + "222100005900360" + sle_titolo_1.text + char(13)

stringa = stringa + "221100005700330" + sle_riga_1.text  + char(13)

stringa = stringa + "221100005700300" + sle_riga_2.text + char(13)

stringa = stringa + "221100005700270" + sle_riga_3.text + char(13)

stringa = stringa + "221100005700240" + sle_riga_4.text + char(13)

stringa = stringa + "221100005700210" + sle_riga_5.text + char(13)

stringa = stringa + "221100005700175" + sle_riga_6.text + char(13)

stringa = stringa + "211200005700130" + sle_ret_1.text + char(13)
stringa = stringa + "211200005700100" + sle_ret_2.text + char(13)
stringa = stringa + "211200005700070" + sle_ret_3.text + char(13)
stringa = stringa + "211200005700040" + sle_ret_4.text + char(13)

stringa = stringa + "252100004200080" + sle_avv_1.text + char(13)
stringa = stringa + "252100004200010" + sle_avv_2.text + char(13)

stringa = stringa + "Q" + left(sle_nrcopie.text,4) + char(13)
stringa = stringa + "E" + char(13)

iuo_serial_communication = create uo_serial_communication

if uo_serial_communication.uof_write_com ( stringa, ls_com, 9600,ls_errore) = -1 then
	g_mb.messagebox("OMNIA",ls_errore)
	return
end if

destroy iuo_serial_communication

w_cs_xx_mdi.setmicrohelp("Pronto !")

g_mb.messagebox("Stampa Etichette","Dati trasferiti a Stampante Termica",information!)

setpointer(arrow!)


end event

type sle_nrcopie from singlelineedit within w_etichette_personalizzate
integer x = 1829
integer y = 120
integer width = 503
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
string text = "0001"
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

on losefocus;if len(sle_nrcopie.text) <> 4 then
   sle_nrcopie.setfocus()
end if
	
end on

type gb_3 from groupbox within w_etichette_personalizzate
integer x = 1303
integer y = 40
integer width = 1051
integer height = 320
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa Etichette"
end type

type sle_logo_csteam from singlelineedit within w_etichette_personalizzate
boolean visible = false
integer x = 503
integer y = 1080
integer width = 709
integer height = 60
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean autohscroll = false
end type

type sle_titolo_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 140
integer width = 869
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 340
integer width = 869
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_3 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 440
integer width = 869
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 240
integer width = 869
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_4 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 540
integer width = 869
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_5 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 640
integer width = 869
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_riga_6 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 740
integer width = 869
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_logo_azienda from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 40
integer width = 869
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_ret_1 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 840
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_2 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 940
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_3 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 1040
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 3:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_4 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 1140
integer width = 366
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Dicitura CQ 4:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_ret_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 940
integer width = 869
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_3 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1040
integer width = 869
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 840
integer width = 869
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_ret_4 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1140
integer width = 869
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_avv_1 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1240
integer width = 869
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type sle_avv_2 from singlelineedit within w_etichette_personalizzate
integer x = 411
integer y = 1340
integer width = 869
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

type st_ret_20 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 1240
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Avvertenza 1:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_21 from statictext within w_etichette_personalizzate
integer x = 46
integer y = 1340
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Avvertenza 2:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_etichette_personalizzate
integer x = 1943
integer y = 620
integer width = 366
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Dati"
end type

event clicked;string ls_rag_soc_azienda,ls_num_ord_cliente,ls_cod_prodotto,ls_cod_cliente,ls_des_prodotto,ls_rag_soc_1
integer li_anno_commessa,li_anno_registrazione
long  ll_num_commessa,ll_num_registrazione,ll_prog_riga_ord_ven
dec{4} ld_quan_prodotta

li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa= long(em_num_commessa.text)

select rag_soc_1
into   :ls_rag_soc_azienda
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

select quan_prodotta
into   :ld_quan_prodotta
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

select anno_registrazione,
		 num_registrazione,
		 cod_prodotto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_prodotto
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

select cod_cliente,
		 num_ord_cliente
into   :ls_cod_cliente,
		 :ls_num_ord_cliente
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 
		 
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if	 

sle_logo_azienda.text = ls_rag_soc_azienda
sle_titolo_1.text     = "IDENTIFICAZIONE PRODOTTO"
sle_riga_1.text       = "Cliente: " + ls_rag_soc_1
sle_riga_2.text       = "Commessa: " + string(ll_num_commessa) + "   Quantità: " + string(ld_quan_prodotta)
sle_riga_3.text       = "Vs.Ordine: " + ls_num_ord_cliente
sle_riga_4.text       = "Vs.Codice: "
sle_riga_5.text       = "Descriz.: " + ls_des_prodotto
sle_riga_6.text       = "Ns.Codice: " + ls_cod_prodotto
sle_ret_1.text        = "CONTROLLO"
sle_ret_2.text        = "QUALITA'"
sle_ret_3.text        = "MATERIALE"
sle_ret_4.text        = "CONFORME"
sle_avv_1.text        = "PALETTA"
sle_avv_2.text        = ""

end event

type r_1 from rectangle within w_etichette_personalizzate
integer linethickness = 5
long fillcolor = 79741120
integer x = 23
integer y = 20
integer width = 2377
integer height = 1420
end type

