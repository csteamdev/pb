﻿$PBExportHeader$w_compila_variabili.srw
forward
global type w_compila_variabili from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_compila_variabili
end type
type dw_1 from u_dw_search within w_compila_variabili
end type
type cb_ok from commandbutton within w_compila_variabili
end type
end forward

global type w_compila_variabili from w_cs_xx_risposta
integer width = 3378
integer height = 1244
string title = "Richiesto Valore per Formula"
boolean controlmenu = false
event ue_imposta_dw ( )
event ue_imposta_dw_gibus ( )
cb_annulla cb_annulla
dw_1 dw_1
cb_ok cb_ok
end type
global w_compila_variabili w_compila_variabili

type variables
string is_cod_variabile
s_cs_xx_parametri istr_parametri
boolean ib_gibus = false
end variables

forward prototypes
public subroutine wf_decimal_sep (ref string fs_stringa, string fs_old_char, string fs_new_char)
public function integer wf_ricerca (ref string fs_errore)
end prototypes

event ue_imposta_dw();long ll_row
string ls_des_label

setpointer(hourglass!)

ll_row = dw_1.getrow()

dw_1.setitem(ll_row, "cod_formula", istr_parametri.parametro_s_1_a[1])

ls_des_label = "Inserire valore "
//dw_1.object.valore_variabile_t.text = "Valore per variabile     " + istr_parametri.parametro_s_1_a[2]

dw_1.setitem(ll_row, "flag_tipo", istr_parametri.parametro_s_1_a[3])
dw_1.setitem(ll_row, "flag_tipo_sel", istr_parametri.parametro_s_1_a[4])

choose case istr_parametri.parametro_s_1_a[3]
	case "D:"
		dw_1.setcolumn("valore_decimal")
		ls_des_label += " numerico"
		
	case "N:"
		dw_1.setcolumn("valore_data")
		ls_des_label += " data dd/mm/yyyy"
		
	case "S:"
		dw_1.setcolumn("valore_stringa")
		ls_des_label += " stringa"
		
end choose

dw_1.object.valore_variabile_t.text = ls_des_label + " per variabile     " + istr_parametri.parametro_s_1_a[2]

setpointer(arrow!)

dw_1.setfocus()


end event

event ue_imposta_dw_gibus();long ll_row


setpointer(hourglass!)

ll_row = dw_1.getrow()

dw_1.setitem(ll_row, "cod_formula", istr_parametri.parametro_s_1_a[1])

dw_1.setitem(ll_row, "flag_tipo_dato", istr_parametri.parametro_s_1_a[3])

choose case istr_parametri.parametro_s_1_a[3]
	case "N:"
		dw_1.setcolumn("valore_decimal")
		
	case "D:"
		dw_1.setcolumn("valore_data")
		
	case "S:"
		dw_1.setcolumn("valore_stringa")
		
end choose

//#X:----------#
//tolgo i primi 3 caratteri e l'ultimo
dw_1.setitem(ll_row, "nome_variabile",  mid(istr_parametri.parametro_s_1_a[2], 4, len(istr_parametri.parametro_s_1_a[2]) - 4))

setpointer(arrow!)

dw_1.setfocus()


end event

public subroutine wf_decimal_sep (ref string fs_stringa, string fs_old_char, string fs_new_char);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(fs_stringa) and fs_stringa<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(fs_stringa, fs_old_char, start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 fs_stringa = Replace(fs_stringa, start_pos, Len(fs_old_char), fs_new_char)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(fs_stringa, fs_old_char, start_pos+Len(fs_new_char))
	LOOP	
else
	fs_stringa="0.0000"
end if
end subroutine

public function integer wf_ricerca (ref string fs_errore);

choose case istr_parametri.parametro_s_1_a[4]
	case "F:"
		//fornitore
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_fornitore(dw_1, "valore_stringa")
		
		
	case "C:"
		//cliente
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_cliente(dw_1, "valore_stringa")
	
		
	case "P:"
		//prodotto
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(dw_1, "valore_stringa")
	
		
	case "A:"
		//attrezzatura
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_attrezzatura(dw_1,"valore_stringa")
		
		
	case else
		fs_errore = "Tipo non previsto per selezione->  " + istr_parametri.parametro_s_1_a[4]
		return -1
		
end choose

return 1
end function

on w_compila_variabili.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.dw_1=create dw_1
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.cb_ok
end on

on w_compila_variabili.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.dw_1)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;
/*
lstr_parametri.parametro_s_1_a[1] = codice formula											es: [NCA]
lstr_parametri.parametro_s_1_a[2] = codice variabile										es: S:FORNITORE   oppure S:F:FORNITORE
lstr_parametri.parametro_s_1_a[3] = upper(mid(ls_cod_variabile, 2, 2))				es: S:
lstr_parametri.parametro_s_1_a[4] = upper(mid(ls_cod_variabile, 4, 2))				es: F:
istr_parametri.parametro_b_1          TRUE se si tratta di GIBUS (datawindow personalizzata)
*/

setpointer(hourglass!)



istr_parametri = message.powerobjectparm
ib_gibus = istr_parametri.parametro_b_1
istr_parametri.parametro_s_1_a[3] = upper(istr_parametri.parametro_s_1_a[3])

if not ib_gibus then
	dw_1.dataobject = "d_compila_variabili"
	
	istr_parametri.parametro_s_1_a[4] = upper(istr_parametri.parametro_s_1_a[4])

	if pos(istr_parametri.parametro_s_1_a[4], ":") > 0 then
	else
		istr_parametri.parametro_s_1_a[4] = ""
	end if
else
	dw_1.dataobject = "d_compila_variabili_gibus"
	istr_parametri.parametro_s_1_a[4] = ""
end if

dw_1.insertrow(0)

setpointer(arrow!)

if not ib_gibus then
	postevent("ue_imposta_dw")
else
	postevent("ue_imposta_dw_gibus")
end if
end event

type cb_annulla from commandbutton within w_compila_variabili
integer x = 1627
integer y = 1008
integer width = 402
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;
closewithreturn(parent, "ANNULLACALCOLOTEST")
end event

type dw_1 from u_dw_search within w_compila_variabili
integer x = 46
integer y = 28
integer width = 3269
integer height = 936
integer taborder = 20
string dataobject = "d_compila_variabili"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_errore

if row>0 then
	choose case dwo.name
		case "b_ricerca"
			if wf_ricerca(ls_errore) < 0 then
				g_mb.error(ls_errore)
				
				return
			end if
			
	end choose
end if
end event

type cb_ok from commandbutton within w_compila_variabili
integer x = 1216
integer y = 1008
integer width = 402
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;string ls_return
dec{4} ldc_decimal
datetime ldt_data

dw_1.accepttext()

//metto al posto di ls_cod_formula ls_risultato, nella stessa posizione di cod_formula
choose case istr_parametri.parametro_s_1_a[3]
	case "D:"
		ldt_data = dw_1.getitemdatetime(dw_1.getrow(), "valore_data")
		ls_return = "'"+string(ldt_data, s_cs_xx.db_funzioni.formato_data)+"'"
		
	case "N:"
		ldc_decimal = dw_1.getitemdecimal(dw_1.getrow(), "valore_decimal")
		ls_return = string(ldc_decimal, "########0.0000")
		wf_decimal_sep(ls_return, ",", ".")
		
	case "S:"
		ls_return = dw_1.getitemstring(dw_1.getrow(), "valore_stringa")
		ls_return = "'"+ls_return+"'"
		
end choose

closewithreturn(parent, ls_return)
end event

