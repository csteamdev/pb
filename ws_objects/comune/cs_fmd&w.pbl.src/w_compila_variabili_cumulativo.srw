﻿$PBExportHeader$w_compila_variabili_cumulativo.srw
forward
global type w_compila_variabili_cumulativo from w_cs_xx_risposta
end type
type st_tipo_ritorno from statictext within w_compila_variabili_cumulativo
end type
type cb_annulla from commandbutton within w_compila_variabili_cumulativo
end type
type dw_1 from u_dw_search within w_compila_variabili_cumulativo
end type
end forward

global type w_compila_variabili_cumulativo from w_cs_xx_risposta
integer width = 3040
integer height = 2096
string title = "Richiesto Valore per Formula"
boolean controlmenu = false
event ue_imposta_dw ( )
st_tipo_ritorno st_tipo_ritorno
cb_annulla cb_annulla
dw_1 dw_1
end type
global w_compila_variabili_cumulativo w_compila_variabili_cumulativo

type variables
string						is_cod_formula, is_testo_formula, is_testo_formula_original, is_tipo_ritorno
dec{4}					id_risultato
s_cs_xx_param_v 	istr_param_v
end variables

forward prototypes
public subroutine wf_decimal_sep (ref string fs_stringa, string fs_old_char, string fs_new_char)
public function integer wf_ricerca (ref string fs_errore)
public function integer wf_clic_calcola (ref decimal fd_risultato, ref string fs_risultato, ref string fs_errore)
public function integer wf_calcola (ref decimal fd_risultato, ref string fs_risultato, ref string fs_errore)
end prototypes

event ue_imposta_dw();long					ll_row, li_index
dec{4}				ld_risultato
string					ls_errore, ls_risultato

setpointer(hourglass!)

dw_1.reset()

//inseriamo una prima riga senza tipo variabile, 
//serve per visualizzare la formula da calcolare anche in assenza di variabili
ll_row = dw_1.insertrow(0)
dw_1.setitem(ll_row, "cod_formula", is_cod_formula)
dw_1.setitem(ll_row, "flag_tipo_dato",  "")
dw_1.setitem(ll_row, "testo_formula", istr_param_v.testo_formula_2)


for li_index=1 to upperbound(istr_param_v.array[])
	ll_row = dw_1.insertrow(0)
	
	dw_1.setitem(ll_row, "cod_formula", is_cod_formula)
	
	dw_1.setitem(ll_row, "flag_tipo_dato",  istr_param_v.array[li_index].parametro_s_1_a[2])
	
	choose case istr_param_v.array[li_index].parametro_s_1_a[2]
		case "N:"
			dw_1.setcolumn("valore_decimal")
			
		case "D:"
			dw_1.setcolumn("valore_data")
			
		case "S:"
			dw_1.setcolumn("valore_stringa")
			
	end choose
	
	//#X:----------#
	//tolgo i primi 3 caratteri e l'ultimo
	dw_1.setitem(ll_row, "nome_variabile",  mid(istr_param_v.array[li_index].parametro_s_1_a[1], 4, len(istr_param_v.array[li_index].parametro_s_1_a[1]) - 4))
	
next


if upperbound(istr_param_v.array[])>0 then
	dw_1.object.nessuna_variabile_t.visible = false
else
	
	dw_1.object.nessuna_variabile_t.visible = true
	//no variabili presenti: pertanto posso già calcolare la formula
	
	if wf_clic_calcola(ld_risultato, ls_risultato, ls_errore)<0 then
		g_mb.error("Attenzione!", ls_errore)
		setpointer(arrow!)
		return
	end if
end if


setpointer(arrow!)

dw_1.setfocus()


end event

public subroutine wf_decimal_sep (ref string fs_stringa, string fs_old_char, string fs_new_char);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(fs_stringa) and fs_stringa<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(fs_stringa, fs_old_char, start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 fs_stringa = Replace(fs_stringa, start_pos, Len(fs_old_char), fs_new_char)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(fs_stringa, fs_old_char, start_pos+Len(fs_new_char))
	LOOP	
else
	fs_stringa="0.0000"
end if
end subroutine

public function integer wf_ricerca (ref string fs_errore);//
//
//choose case istr_parametri.parametro_s_1_a[4]
//	case "F:"
//		//fornitore
//		guo_ricerca.uof_set_response( )
//		guo_ricerca.uof_ricerca_fornitore(dw_1, "valore_stringa")
//		
//		
//	case "C:"
//		//cliente
//		guo_ricerca.uof_set_response( )
//		guo_ricerca.uof_ricerca_cliente(dw_1, "valore_stringa")
//	
//		
//	case "P:"
//		//prodotto
//		guo_ricerca.uof_set_response( )
//		guo_ricerca.uof_ricerca_prodotto(dw_1, "valore_stringa")
//	
//		
//	case "A:"
//		//attrezzatura
//		guo_ricerca.uof_set_response( )
//		guo_ricerca.uof_ricerca_attrezzatura(dw_1,"valore_stringa")
//		
//		
//	case else
//		fs_errore = "Tipo non previsto per selezione->  " + istr_parametri.parametro_s_1_a[4]
//		return -1
//		
//end choose
//
return 1
end function

public function integer wf_clic_calcola (ref decimal fd_risultato, ref string fs_risultato, ref string fs_errore);

if wf_calcola(fd_risultato, fs_risultato, fs_errore) <0 then
	dw_1.object.risultato_t.text = "Errore!"
	return -1
end if


if is_tipo_ritorno="1" then
	dw_1.object.risultato_t.text = string(fd_risultato, "###,###,##0.0000")
else
	dw_1.object.risultato_t.text = fs_risultato
end if


return 0
end function

public function integer wf_calcola (ref decimal fd_risultato, ref string fs_risultato, ref string fs_errore);
long						ll_index, ll_k
string						ls_valore, ls_cod_variabile, ls_dsdef, ls_risultato
dec{4}					ld_valore
datetime					ldt_valore
date						ldt_date
datastore				lds_eval

ls_dsdef = 'release 6; datawindow() table(column=(type=char(255) name=a dbname="a") )'


//Attenzione!
//La riga numero 1 è una riga fittizia, contiene solo la formula
//le eventuali righe da2 in poi sono le variabili da usare

dw_1.accepttext()

is_testo_formula = is_testo_formula_original
istr_param_v.risultato = 0
id_risultato = 0

//lettura variabili inserite
for ll_index=2 to upperbound(istr_param_v.array[]) + 1
	ll_k = ll_index - 1
	
	choose case istr_param_v.array[ll_k].parametro_s_1_a[2]
		case "D:"
			ldt_valore = dw_1.getitemdatetime(ll_index, "valore_data")
			if isnull(ldt_valore) then ldt_valore = datetime(date(1950,1,1), 00:00:00)
			ldt_date = date(ldt_valore)
			ls_valore = "~'"+string(ldt_valore, s_cs_xx.db_funzioni.formato_data)+"~'"
			
		case "N:"
			ld_valore = dw_1.getitemdecimal(ll_index, "valore_decimal")
			if isnull(ld_valore) then ld_valore=0
			ls_valore = string(ld_valore, "########0.0000")
			wf_decimal_sep(ls_valore, ",", ".")
			
		case "S:"		
			ls_valore = dw_1.getitemstring(ll_index, "valore_stringa")
			if isnull(ls_valore) then ls_valore=""
			
			ls_valore = "~'"+ls_valore+"~'"
			
		case ""
			//riga della formula: no variabili
			continue
			
	end choose
	
	istr_param_v.array[ll_k].parametro_s_1_a[4] = ls_valore
	
	ls_cod_variabile = istr_param_v.array[ll_k].parametro_s_1_a[1]
	
	guo_functions.uof_replace_string(is_testo_formula, ls_cod_variabile, ls_valore)
	
next


//---------------------------------------------------------------------------------------------------------------------------------------
//VALUTAZIONE DELLA FORMULA MEDIANTE COMPUTED FIELD DI UN DATASTORE CREATO RUN-TIME

lds_eval = create datastore
lds_eval.Create(ls_dsdef, fs_errore)
lds_eval.InsertRow(0)

if Len(fs_errore) > 0 then
	destroy lds_eval
	return -1
else
	ls_risultato = lds_eval.describe ( 'evaluate( " ' + is_testo_formula + ' " ,1)' )
	destroy lds_eval
	
	if ls_risultato="!" then
		fs_errore = "Errore! Verificare la formula!"
		return -1
	end if
	
	if is_tipo_ritorno="1" then
		fd_risultato = dec(ls_risultato)
	else
		fs_risultato = ls_risultato
	end if
	
end if


return 0
end function

on w_compila_variabili_cumulativo.create
int iCurrent
call super::create
this.st_tipo_ritorno=create st_tipo_ritorno
this.cb_annulla=create cb_annulla
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_tipo_ritorno
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_1
end on

on w_compila_variabili_cumulativo.destroy
call super::destroy
destroy(this.st_tipo_ritorno)
destroy(this.cb_annulla)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;
/*
istr_param_v.cod_formula
istr_param_v.testo_formula
istr_param_v.risultato
istr_param_v.close_ok
istr_param_v.array[]  --->  s_cs_xx_parametri
							
							.parametro_s_1_a[1]   -->  CODICE VARIABILE								(es. #S:nomevariabile#)
							.parametro_s_1_a[2]   -->  IDENTIFICATORE VARIABILE					(es. S: per stringa, D: per numero, D: per datetime)
							.parametro_s_1_a[3]   -->  IDENTIFICATORE EVENTUALE SELEZIONE	(es. F: campo fornitore)
							.parametro_s_1_a[4]   -->  VALORE ASSUNTO DALLA VARIABILE DOPO LA SOSTITUZIONE CON VALORE (espresso in stringa)
*/

integer li_index

setpointer(hourglass!)


istr_param_v = message.powerobjectparm

is_cod_formula = istr_param_v.cod_formula
is_testo_formula_original = istr_param_v.testo_formula
is_tipo_ritorno = istr_param_v.tipo_ritorno

if is_tipo_ritorno="2" then
	st_tipo_ritorno.text = "Tipologia valore di ritorno: STRINGA"
else
	st_tipo_ritorno.text = "Tipologia valore di ritorno: NUMERICO"
end if


setnull(istr_param_v.risultato)
setnull(id_risultato)

istr_param_v.close_ok = false


for li_index=1 to upperbound(istr_param_v.array[])

	istr_param_v.array[li_index].parametro_s_1_a[2] = upper(istr_param_v.array[li_index].parametro_s_1_a[2])
	istr_param_v.array[li_index].parametro_s_1_a[3] = upper(istr_param_v.array[li_index].parametro_s_1_a[3])
	
	if pos(istr_param_v.array[li_index].parametro_s_1_a[3], ":") > 0 then
	else
		istr_param_v.array[li_index].parametro_s_1_a[3] = ""
	end if
	
next

setpointer(arrow!)

postevent("ue_imposta_dw")
end event

type st_tipo_ritorno from statictext within w_compila_variabili_cumulativo
integer x = 46
integer y = 20
integer width = 2917
integer height = 112
integer textsize = -15
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tipologia valore di ritorno:"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_compila_variabili_cumulativo
boolean visible = false
integer x = 1472
integer y = 1876
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;

istr_param_v.close_ok = false
closewithreturn(parent, istr_param_v)
end event

type dw_1 from u_dw_search within w_compila_variabili_cumulativo
event ue_keydown pbm_dwnkey
integer x = 37
integer y = 160
integer width = 2926
integer height = 1792
integer taborder = 20
string dataobject = "d_compila_variabili_gibus"
boolean vscrollbar = true
boolean border = false
end type

event ue_keydown;long				ll_currow
dec{4}			ld_risultato
string				ls_errore, ls_risultato


if key=KeyEnter! then
	
	ll_currow = getrow()
	if ll_currow = rowcount() then
		
		if wf_clic_calcola(ld_risultato, ls_risultato, ls_errore)<0 then
			g_mb.error("Attenzione!", ls_errore)
			return
		end if
		
	end if
	
end if
end event

event buttonclicked;call super::buttonclicked;string				ls_errore, ls_risultato
dec{4}			ld_risultato

if row>0 then
	choose case dwo.name
		case "b_ricerca"
//			if wf_ricerca(ls_errore) < 0 then
//				g_mb.error(ls_errore)
//				
//				return
//			end if

		case "b_calcola"
			
			if wf_clic_calcola(ld_risultato, ls_risultato, ls_errore)<0 then
				g_mb.error("Attenzione!", ls_errore)
				return
			end if
			
		case "b_chiudi"
			id_risultato = dec(dw_1.object.risultato_t.text)
			istr_param_v.risultato = id_risultato
			istr_param_v.close_ok = true
			
			closewithreturn(parent, istr_param_v)

	end choose
end if




end event

