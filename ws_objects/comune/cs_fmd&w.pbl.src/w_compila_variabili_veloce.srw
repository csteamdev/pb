﻿$PBExportHeader$w_compila_variabili_veloce.srw
forward
global type w_compila_variabili_veloce from window
end type
type st_2 from statictext within w_compila_variabili_veloce
end type
type st_1 from statictext within w_compila_variabili_veloce
end type
type dw_variabili_new from datawindow within w_compila_variabili_veloce
end type
type dw_variabili_prec from datawindow within w_compila_variabili_veloce
end type
type mle_testo_formula from multilineedit within w_compila_variabili_veloce
end type
type st_cod_formula from statictext within w_compila_variabili_veloce
end type
type cb_conferma from commandbutton within w_compila_variabili_veloce
end type
end forward

global type w_compila_variabili_veloce from window
integer width = 4727
integer height = 2848
boolean titlebar = true
string title = "Digitare il valore per le variabili richieste"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_2 st_2
st_1 st_1
dw_variabili_new dw_variabili_new
dw_variabili_prec dw_variabili_prec
mle_testo_formula mle_testo_formula
st_cod_formula st_cod_formula
cb_conferma cb_conferma
end type
global w_compila_variabili_veloce w_compila_variabili_veloce

type variables


datastore				ids_variabili
end variables

on w_compila_variabili_veloce.create
this.st_2=create st_2
this.st_1=create st_1
this.dw_variabili_new=create dw_variabili_new
this.dw_variabili_prec=create dw_variabili_prec
this.mle_testo_formula=create mle_testo_formula
this.st_cod_formula=create st_cod_formula
this.cb_conferma=create cb_conferma
this.Control[]={this.st_2,&
this.st_1,&
this.dw_variabili_new,&
this.dw_variabili_prec,&
this.mle_testo_formula,&
this.st_cod_formula,&
this.cb_conferma}
end on

on w_compila_variabili_veloce.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_variabili_new)
destroy(this.dw_variabili_prec)
destroy(this.mle_testo_formula)
destroy(this.st_cod_formula)
destroy(this.cb_conferma)
end on

event open;s_cs_xx_parametri		lstr_parametri
datastore				lds_data
string						ls_var_to_validate[], ls_tipovar_to_validate[], ls_valvar_to_validate[], ls_count, ls_column_name
dec{4}					ld_valvar_to_validate[]
long						ll_tot, ll_index


lstr_parametri = message.powerobjectparm

st_cod_formula.text 			= lstr_parametri.parametro_s_1
mle_testo_formula.text 		= lstr_parametri.parametro_s_2
ids_variabili 						= lstr_parametri.parametro_ds_1
ls_var_to_validate[] 			= lstr_parametri.parametro_s_1_a[]
ls_tipovar_to_validate[] 		= lstr_parametri.parametro_s_2_a[]
ls_valvar_to_validate[] 		= lstr_parametri.parametro_s_3_a[]
ld_valvar_to_validate[] 		= lstr_parametri.parametro_d_1_a[]


ids_variabili.RowsCopy(1,ids_variabili.rowcount(), Primary!, dw_variabili_prec, 1, Primary!)
dw_variabili_prec.sort()

ll_tot = upperbound(ls_var_to_validate[])

for ll_index=1 to ll_tot
	dw_variabili_new.insertrow(0)
	dw_variabili_new.setitem(ll_index, "cod_variabile", 		ls_var_to_validate[ll_index])
	dw_variabili_new.setitem(ll_index, "flag_tipo_dato", 		ls_tipovar_to_validate[ll_index])
	dw_variabili_new.setitem(ll_index, "valore_s", 				ls_valvar_to_validate[ll_index])
	dw_variabili_new.setitem(ll_index, "valore_d", 				ld_valvar_to_validate[ll_index])
next

dw_variabili_new.sort()

//proteggi variabili già validate
ls_count = dw_variabili_prec.Describe("DataWindow.Column.Count")
for ll_index = 1 to integer(ls_count)
	ls_column_name = dw_variabili_prec.Describe("#"+string(ll_index)+".name")
	dw_variabili_prec.modify(ls_column_name+'.protect=1')
next


end event

type st_2 from statictext within w_compila_variabili_veloce
integer x = 2382
integer y = 768
integer width = 2313
integer height = 112
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "VARIABILI DA IMPOSTARE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_compila_variabili_veloce
integer x = 41
integer y = 768
integer width = 2313
integer height = 112
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "VARIABILI GIA~' IMPOSTATE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_variabili_new from datawindow within w_compila_variabili_veloce
integer x = 2382
integer y = 884
integer width = 2313
integer height = 1848
integer taborder = 10
string title = "none"
string dataobject = "d_compila_variabili_veloce"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_variabili_prec from datawindow within w_compila_variabili_veloce
integer x = 41
integer y = 884
integer width = 2313
integer height = 1848
integer taborder = 40
string title = "none"
string dataobject = "d_compila_variabili_veloce"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

type mle_testo_formula from multilineedit within w_compila_variabili_veloce
integer x = 46
integer y = 164
integer width = 4645
integer height = 592
integer taborder = 30
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Courier New"
long textcolor = 33554432
string text = "testoFormula"
boolean vscrollbar = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_cod_formula from statictext within w_compila_variabili_veloce
integer x = 27
integer y = 28
integer width = 841
integer height = 112
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "CodiceFormula"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_compila_variabili_veloce
integer x = 4123
integer y = 40
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long			ll_index, ll_new, ll_index2
string			ls_var_to_validate[], ls_tipovar_to_validate[], ls_valvar_to_validate[], ls_temp
dec{4}		ld_valvar_to_validate[], ld_temp
s_cs_xx_parametri		lstr_return

dw_variabili_new.accepttext()

//metto le variabili con relativo valore già nel datastore mediante rowscopy da dw  a ds
dw_variabili_new.RowsCopy(1, dw_variabili_new.rowcount(), Primary!, ids_variabili, ids_variabili.rowcount() + 1, Primary!)

//inoltre metto i valori negli array di ritorno
for ll_index=1 to dw_variabili_new.rowcount()
	ls_var_to_validate[ll_index] 			= dw_variabili_new.getitemstring(ll_index, "cod_variabile")
	ls_tipovar_to_validate[ll_index]		= dw_variabili_new.getitemstring(ll_index, "flag_tipo_dato")
	ls_temp									= dw_variabili_new.getitemstring(ll_index, "valore_s")
	ld_temp									= dw_variabili_new.getitemdecimal(ll_index, "valore_d")
	
	if isnull(ls_temp) then ls_temp=""
	if isnull(ld_temp) then ld_temp=0
	
	ls_valvar_to_validate[ll_index]		= ls_temp
	ld_valvar_to_validate[ll_index]		= ld_temp
next

lstr_return.parametro_ds_1 = ids_variabili
lstr_return.parametro_s_1_a[] = ls_var_to_validate[]
lstr_return.parametro_s_2_a[] = ls_tipovar_to_validate[]
lstr_return.parametro_s_3_a[] = ls_valvar_to_validate[]
lstr_return.parametro_d_1_a[] = ld_valvar_to_validate[]

closewithreturn(parent, lstr_return)

end event

