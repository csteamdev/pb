﻿$PBExportHeader$w_formule.srw
$PBExportComments$Window formule
forward
global type w_formule from w_cs_xx_principale
end type
type mle_1 from multilineedit within w_formule
end type
type cb_calcola from commandbutton within w_formule
end type
type st_2 from statictext within w_formule
end type
type st_3 from statictext within w_formule
end type
type st_1 from statictext within w_formule
end type
type ddlb_elenco_campi from dropdownlistbox within w_formule
end type
type st_4 from statictext within w_formule
end type
type dw_lista_formule from uo_cs_xx_dw within w_formule
end type
type dw_lista_risultati from uo_cs_xx_dw within w_formule
end type
type st_nr_corr_tot from statictext within w_formule
end type
end forward

global type w_formule from w_cs_xx_principale
integer width = 2665
integer height = 1724
string title = "Formule"
mle_1 mle_1
cb_calcola cb_calcola
st_2 st_2
st_3 st_3
st_1 st_1
ddlb_elenco_campi ddlb_elenco_campi
st_4 st_4
dw_lista_formule dw_lista_formule
dw_lista_risultati dw_lista_risultati
st_nr_corr_tot st_nr_corr_tot
end type
global w_formule w_formule

type variables
string is_tabella, is_campi[]

end variables

on w_formule.create
int iCurrent
call super::create
this.mle_1=create mle_1
this.cb_calcola=create cb_calcola
this.st_2=create st_2
this.st_3=create st_3
this.st_1=create st_1
this.ddlb_elenco_campi=create ddlb_elenco_campi
this.st_4=create st_4
this.dw_lista_formule=create dw_lista_formule
this.dw_lista_risultati=create dw_lista_risultati
this.st_nr_corr_tot=create st_nr_corr_tot
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_1
this.Control[iCurrent+2]=this.cb_calcola
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.ddlb_elenco_campi
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.dw_lista_formule
this.Control[iCurrent+9]=this.dw_lista_risultati
this.Control[iCurrent+10]=this.st_nr_corr_tot
end on

on w_formule.destroy
call super::destroy
destroy(this.mle_1)
destroy(this.cb_calcola)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.ddlb_elenco_campi)
destroy(this.st_4)
destroy(this.dw_lista_formule)
destroy(this.dw_lista_risultati)
destroy(this.st_nr_corr_tot)
end on

event pc_setwindow;call super::pc_setwindow;string ls_db,ls_nome_colonna
integer li_i,li_risposta

mle_1.enabled=false
is_tabella = s_cs_xx.parametri.parametro_s_15

this.title = "Formule per: " + is_tabella

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

dw_lista_formule.set_dw_key("cod_azienda")
dw_lista_formule.set_dw_key("nome_tabella")
dw_lista_formule.set_dw_key("prog_formula")
dw_lista_formule.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_lista_risultati.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

iuo_dw_main = dw_lista_formule

li_i=1

choose case ls_db
	case "SYBASE_ASA"
		declare righe_colonne cursor for
		select cname
		from	 sys.syscolumns
		where  tname=:is_tabella
		and    coltype = 'NUMERIC';
		
		open righe_colonne;
		
		do while 1=1
			fetch righe_colonne
			into  :ls_nome_colonna;
			
			choose case sqlca.sqlcode
				case is < 0 
					g_mb.messagebox("Apice","Errore sul DB",exclamation!)
				case 100
					exit
			end choose
			ddlb_elenco_campi.additem(ls_nome_colonna)
			is_campi[li_i] = ls_nome_colonna
			li_i++
			
		loop
		
		close righe_colonne;
		
	case "ORACLE"
		declare righe_colonne_1 cursor for
		select CNAME
		from	 SYS.COL
		where  TNAME=:is_tabella
		and    COLTYPE = 'NUMBER';
		
		open righe_colonne_1;
		
		do while 1=1
			fetch righe_colonne_1
			into  :ls_nome_colonna;
			
			choose case sqlca.sqlcode
				case is < 0 
					g_mb.messagebox("Apice","Errore sul DB",exclamation!)
				case 100
					exit
			end choose
			ddlb_elenco_campi.additem(ls_nome_colonna)			
			is_campi[li_i] = ls_nome_colonna
			li_i++
			
		loop
		
		close righe_colonne_1;
		
		case "SYBASE_ASE"
			
		declare righe_colonne_2 cursor for
		select syscolumns.name
		from	 sysobjects,syscolumns
		where  sysobjects.id=syscolumns.id
		and    sysobjects.name=:is_tabella
		and    (syscolumns.type = 55 or syscolumns.type = 106);
		
		open righe_colonne_2;
		
		do while 1=1
			fetch righe_colonne_2
			into  :ls_nome_colonna;
			
			choose case sqlca.sqlcode
				case is < 0 
					g_mb.messagebox("Apice","Errore sul DB",exclamation!)
				case 100
					exit
			end choose
			
			ddlb_elenco_campi.additem(ls_nome_colonna)
			is_campi[li_i] = ls_nome_colonna
			li_i++
			
		loop
		
		close righe_colonne_2;

end choose
end event

type mle_1 from multilineedit within w_formule
integer x = 23
integer y = 1060
integer width = 2583
integer height = 440
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_calcola from commandbutton within w_formule
integer x = 23
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcola"
end type

event clicked;string  ls_formula,ls_campo,ls_sql,ls_new_syntax,ls_errore,ls_errore_creazione,ls_risultato
integer li_risposta,li_numero_campi,li_i
double  ldd_valore_campo,ldd_risultato
long    ll_num_righe,ll_t,ll_prog_formula,ll_prog_risultato
datastore lds_righe_ingresso

setpointer(hourglass!)

ll_prog_formula=dw_lista_formule.getitemnumber(dw_lista_formule.getrow(),"prog_formula")

delete 
from tab_risultati
where cod_azienda=:s_cs_xx.cod_azienda
and   nome_tabella=:is_tabella
and   prog_formula=:ll_prog_formula;

commit;

ll_prog_risultato=1

ll_num_righe=s_cs_xx.parametri.parametro_dw_1.rowcount()
li_numero_campi = upperbound(is_campi[])
st_nr_corr_tot.text = "0/"+string(ll_num_righe)
for ll_t = 1 to ll_num_righe
	ls_formula = mle_1.text
	for li_i = 1 to li_numero_campi
		ls_campo = is_campi[li_i]	
		if pos(ls_formula,ls_campo) <> 0 then
			ldd_valore_campo  = s_cs_xx.parametri.parametro_dw_1.getitemnumber(ll_t,ls_campo)
			if isnull(ldd_valore_campo) then ldd_valore_campo=0
			li_risposta = f_sostituzione(ls_formula, ls_campo, string(ldd_valore_campo))
		end if
	next
	
	li_risposta = f_if(ls_formula)
	
	if li_risposta < 0 then
		g_mb.messagebox("Sep",ls_formula,stopsign!)
		return
	end if
	
	ls_risultato = f_parser_parentesi(ls_formula)
	if ls_risultato = "Errore" then
		g_mb.messagebox("Sep","Attenzione le librerie per il calcolo delle formule trigonometriche non sono installate correttamente. Verificare.",stopsign!)
		return
	end if
	ldd_risultato = double(ls_risultato)
	
   INSERT INTO tab_risultati  
          	 (cod_azienda,   
	           nome_tabella,   
   	        prog_formula,   
      	     prog_risultato,   
         	  risultato )  
  	 VALUES ( :s_cs_xx.cod_azienda,   
   	       :is_tabella,   
      	    :ll_prog_formula,   
         	 :ll_prog_risultato,   
           	 :ldd_risultato )  ;

	ll_prog_risultato++
	st_nr_corr_tot.text = string(ll_t) + "/" + string(ll_num_righe)
next

setpointer(arrow!)
commit;
dw_lista_risultati.triggerevent("pcd_retrieve")
end event

type st_2 from statictext within w_formule
integer x = 571
integer y = 980
integer width = 1989
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = " +  -  *  /  (  ) INT()  SQR()  IF[condizione;val vero;val falso]"
boolean focusrectangle = false
end type

type st_3 from statictext within w_formule
integer x = 23
integer y = 1000
integer width = 549
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Operatori ammessi:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_formule
integer x = 23
integer y = 20
integer width = 379
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Lista Formule:"
boolean focusrectangle = false
end type

type ddlb_elenco_campi from dropdownlistbox within w_formule
integer x = 411
integer y = 1520
integer width = 983
integer height = 1080
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean vscrollbar = true
end type

event selectionchanged;mle_1.text = mle_1.text + " " + ddlb_elenco_campi.text + " "
mle_1.setfocus()
end event

type st_4 from statictext within w_formule
integer x = 1691
integer y = 20
integer width = 754
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Risultati:"
boolean focusrectangle = false
end type

type dw_lista_formule from uo_cs_xx_dw within w_formule
integer x = 23
integer y = 100
integer width = 1646
integer height = 880
integer taborder = 20
string dataobject = "d_lista_formule"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,is_tabella)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_prog_formula

select max(prog_formula)
into   :ll_prog_formula
from   tab_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_tabella=:is_tabella;

if ll_prog_formula=0 or isnull(ll_prog_formula) then
	ll_prog_formula=1
else
	ll_prog_formula++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "nome_tabella", is_tabella)
		SetItem(l_Idx, "prog_formula", ll_prog_formula)
		SetItem(l_Idx, "formula", mle_1.text)
   END IF
NEXT
end event

event pcd_pickedrow;call super::pcd_pickedrow;dw_lista_risultati.triggerevent("pcd_retrieve")
mle_1.text = getitemstring(getrow(),"formula")
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	
	mle_1.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	mle_1.enabled=false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	mle_1.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	mle_1.text=""
	mle_1.enabled=true
	dw_lista_risultati.triggerevent("pcd_retrieve")
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	long ll_prog_formula,ll_i
	
	for ll_i = 1 to deletedcount()

  		ll_prog_formula=getitemnumber(ll_i,"prog_formula",delete!,true)

	   delete from tab_risultati
		where cod_azienda=:s_cs_xx.cod_azienda
		and 	nome_tabella=:is_tabella
		and 	prog_formula=:ll_prog_formula;
		commit;

	next

end if

end event

type dw_lista_risultati from uo_cs_xx_dw within w_formule
integer x = 1691
integer y = 100
integer width = 914
integer height = 880
integer taborder = 10
string dataobject = "d_lista_risultati"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx,ll_prog_formula

select max(prog_formula)
into   :ll_prog_formula
from   tab_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_tabella=:is_tabella;

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "nome_tabella", is_tabella)
		SetItem(l_Idx, "prog_formula", ll_prog_formula)
   END IF
NEXT
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error,ll_prog_formula

if dw_lista_formule.rowcount()>0 then
	ll_prog_formula = dw_lista_formule.getitemnumber(dw_lista_formule.getrow(),"prog_formula")
	l_Error = Retrieve(s_cs_xx.cod_azienda,is_tabella,ll_prog_formula)
	
	IF l_Error < 0 THEN
		PCCA.Error = c_Fatal
	END IF
end if
end event

type st_nr_corr_tot from statictext within w_formule
integer x = 2057
integer y = 1520
integer width = 539
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

