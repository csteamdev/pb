﻿$PBExportHeader$w_gen_formule_x_db.srw
$PBExportComments$Finestra di Generazione Formule X Distinta Base
forward
global type w_gen_formule_x_db from w_cs_xx_risposta
end type
type cb_test from commandbutton within w_gen_formule_x_db
end type
type st_1 from statictext within w_gen_formule_x_db
end type
type mle_1 from multilineedit within w_gen_formule_x_db
end type
type dw_elenco_variabili from uo_cs_xx_dw within w_gen_formule_x_db
end type
type cb_annulla from commandbutton within w_gen_formule_x_db
end type
type st_testo_descrittivo from statictext within w_gen_formule_x_db
end type
type cb_ok from commandbutton within w_gen_formule_x_db
end type
type dw_legenda from datawindow within w_gen_formule_x_db
end type
end forward

global type w_gen_formule_x_db from w_cs_xx_risposta
integer width = 3863
integer height = 2760
string title = "Crea / Modifica Formula"
cb_test cb_test
st_1 st_1
mle_1 mle_1
dw_elenco_variabili dw_elenco_variabili
cb_annulla cb_annulla
st_testo_descrittivo st_testo_descrittivo
cb_ok cb_ok
dw_legenda dw_legenda
end type
global w_gen_formule_x_db w_gen_formule_x_db

type variables
boolean ib_annulla = false
string is_cod_prodotto, is_tipo_ritorno
boolean ib_cod_prodotto = false
end variables

forward prototypes
public function string wf_formula_descr (string fs_formula)
public subroutine wf_set_valore (string fs_valore)
end prototypes

public function string wf_formula_descr (string fs_formula);//string  ls_campo,ls_risultato, ls_des_variabile
//integer li_numero_campi,li_i, li_pos_campo
//
//setpointer(hourglass!)
//
//li_numero_campi = dw_elenco_variabili.rowcount()
//for li_i = 1 to li_numero_campi
//	ls_campo = dw_elenco_variabili.getitemstring(li_i, "tab_variabili_formule_cod_variabile")
//	li_pos_campo = pos(fs_formula,ls_campo)
//	do while li_pos_campo <> 0
//		if ib_cod_prodotto then
//			ls_des_variabile = dw_elenco_variabili.getitemstring(li_i, "tab_des_variabili_des_variabile")
//		else
//			ls_des_variabile = dw_elenco_variabili.getitemstring(li_i, "tab_variabili_formule_nome_campo_databas")
//		end if
//
//		fs_formula = replace(fs_formula, li_pos_campo, len(ls_campo), ls_des_variabile)
//		li_pos_campo = pos(fs_formula,ls_campo)
//	loop
//next
//
//setpointer(arrow!)
//return fs_formula
return f_sost_campi_in_formula(fs_formula)
end function

public subroutine wf_set_valore (string fs_valore);long ll_pos, ll_fine

ll_pos = mle_1.position()

mle_1.ReplaceText (fs_valore )
end subroutine

event pc_setwindow;call super::pc_setwindow;if not isnull(s_cs_xx.parametri.parametro_s_12) and s_cs_xx.parametri.parametro_s_12 <> "" then //Ho il Codice Prodotto

	ib_cod_prodotto = true
	is_cod_prodotto = s_cs_xx.parametri.parametro_s_12
	dw_elenco_variabili.dataobject = "d_elenco_des_variabili"
	st_testo_descrittivo.text = f_sost_des_var_in_formula(s_cs_xx.parametri.parametro_s_15, is_cod_prodotto)
	
else
	ib_cod_prodotto = false
	dw_elenco_variabili.dataobject = "d_elenco_variabili"
	st_testo_descrittivo.text = f_sost_campi_in_formula(s_cs_xx.parametri.parametro_s_15)
	
end if

dw_elenco_variabili.set_dw_key("cod_azienda")
dw_elenco_variabili.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)


//------------------------------------------------
try
	dw_elenco_variabili.setfilter("tab_variabili_formule_nome_campo_databas<>'' and not isnull(tab_variabili_formule_nome_campo_databas)")
	dw_elenco_variabili.filter()
catch (runtimeerror err)
end try
//------------------------------------------------


mle_1.text = s_cs_xx.parametri.parametro_s_15

is_tipo_ritorno = s_cs_xx.parametri.parametro_s_11
s_cs_xx.parametri.parametro_s_11 = ""


move(600, 20)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 50
end event

on w_gen_formule_x_db.create
int iCurrent
call super::create
this.cb_test=create cb_test
this.st_1=create st_1
this.mle_1=create mle_1
this.dw_elenco_variabili=create dw_elenco_variabili
this.cb_annulla=create cb_annulla
this.st_testo_descrittivo=create st_testo_descrittivo
this.cb_ok=create cb_ok
this.dw_legenda=create dw_legenda
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_test
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.mle_1
this.Control[iCurrent+4]=this.dw_elenco_variabili
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.st_testo_descrittivo
this.Control[iCurrent+7]=this.cb_ok
this.Control[iCurrent+8]=this.dw_legenda
end on

on w_gen_formule_x_db.destroy
call super::destroy
destroy(this.cb_test)
destroy(this.st_1)
destroy(this.mle_1)
destroy(this.dw_elenco_variabili)
destroy(this.cb_annulla)
destroy(this.st_testo_descrittivo)
destroy(this.cb_ok)
destroy(this.dw_legenda)
end on

event close;call super::close;if ib_annulla then
	s_cs_xx.parametri.parametro_s_15 = '%'
	s_cs_xx.parametri.parametro_s_14 = '%'
	s_cs_xx.parametri.parametro_s_13 = '%'
else
	s_cs_xx.parametri.parametro_s_15 = mle_1.text
	s_cs_xx.parametri.parametro_s_14 = f_sost_campi_in_formula(mle_1.text)

	if ib_cod_prodotto then
		s_cs_xx.parametri.parametro_s_13 = f_sost_des_var_in_formula(mle_1.text, is_cod_prodotto)
	else
		s_cs_xx.parametri.parametro_s_13 = '%'
	end if
end if
end event

event resize;call super::resize;long ll_offset

long ll_x1						//posizione x della dw delle variabili e dell'editor della formula

long ll_x2						//posizione x della dw legenda e della traduzione in nomi colonna della formula

long ll_y1						//posizione y dw delle variabili e della dw_della legenda

long ll_y2						//posizione y  dell'editor della formula e della traduzione in nomi colonna della formula

long ll_x3, ll_x4, ll_x5			//posizioni x dei tre pulsanti in basso: Test/Annulla/OK

long ll_y3						//posizione y dei tre pulsanti in basso: Test/Annulla/OK

integer li_perc_w, li_perc_h, ll_param

li_perc_w = 50
li_perc_h = 40

ll_offset = 10

ll_param = 3


/*
---------------------------------+---------------------------------
elenco variabili						legenda

---------------------------------+---------------------------------
editor									traduzione
										in nomi colonna
---------------------------------+---------------------------------
											Test    Annulla    OK
---------------------------------+---------------------------------
*/

ll_param = ll_param * ll_offset

ll_x1 = 20
ll_y1 = 20
dw_elenco_variabili.x = ll_x1
dw_elenco_variabili.y = ll_y1
dw_elenco_variabili.width = this.width * (li_perc_w / 100) - 2 * ll_offset
dw_elenco_variabili.height = this.height * (li_perc_h / 100) - 2 * ll_offset

ll_x2 = this.width * (li_perc_w / 100) + ll_offset
dw_legenda.x = ll_x2
dw_legenda.y = dw_elenco_variabili.y
dw_legenda.width = dw_elenco_variabili.width - ll_param
dw_legenda.height = dw_elenco_variabili.height

ll_y2 = this.height * (li_perc_h / 100) + ll_offset
mle_1.x = dw_elenco_variabili.x
mle_1.y = ll_y2
mle_1.width =  dw_elenco_variabili.width
mle_1.height = dw_elenco_variabili.height

st_testo_descrittivo.x = dw_legenda.x
st_testo_descrittivo.y = mle_1.y
st_testo_descrittivo.width = dw_elenco_variabili.width - ll_param
st_testo_descrittivo.height = dw_elenco_variabili.height

ll_x3 = ll_x2
ll_y3 =  2 * this.height * (li_perc_h / 100) + ll_offset

cb_test.x = ll_x3
cb_test.y = ll_y3
cb_test.width = 366
cb_test.height = 80

ll_x4 = ll_x3 + cb_test.width + ll_offset
cb_annulla.x = ll_x4
cb_annulla.y = ll_y3
cb_annulla.width = cb_test.width
cb_annulla.height = cb_test.height

ll_x5 = ll_x4 + cb_annulla.width + ll_offset
cb_ok.x = ll_x5
cb_ok.y = ll_y3
cb_ok.width = cb_test.width
cb_ok.height = cb_test.height











end event

type cb_test from commandbutton within w_gen_formule_x_db
integer x = 1257
integer y = 2524
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Test"
end type

event clicked;uo_formule_calcolo luo_formule
dec{4} ld_risultato
string ls_errore

luo_formule = create  uo_formule_calcolo

//modalità testing
luo_formule.ii_anno_reg_ord_ven = 0
luo_formule.il_num_reg_ord_ven = 0
luo_formule.il_prog_riga_ord_ven = 0

luo_formule.is_tipo_ritorno = is_tipo_ritorno


if luo_formule.uof_calcola_formula_testo_per_test(mle_1.text, ld_risultato, ls_errore) < 0 then
	destroy luo_formule;
	ls_errore = "ERR:~r~n" + ls_errore
	g_mb.error("Errore Test Formula", ls_errore)
	
	return
end if

destroy luo_formule;
end event

type st_1 from statictext within w_gen_formule_x_db
boolean visible = false
integer x = 379
integer y = 2224
integer width = 974
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "VISUALIZZAZIONE DELLE FORMULA CON TRADUZIONE DELLE VARIABILI IN NOMI COLONNE DEL DATABASE"
boolean focusrectangle = false
end type

type mle_1 from multilineedit within w_gen_formule_x_db
event ue_keydown pbm_keydown
integer x = 96
integer y = 1156
integer width = 1701
integer height = 980
integer taborder = 40
boolean bringtotop = true
integer textsize = -14
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Courier New"
long backcolor = 16777215
string text = "DIM_2 * 5"
boolean vscrollbar = true
boolean autovscroll = true
end type

event ue_keydown;if ib_cod_prodotto then
	st_testo_descrittivo.text = f_sost_des_var_in_formula(this.text, is_cod_prodotto)
else
	st_testo_descrittivo.text = f_sost_campi_in_formula(this.text)
end if
end event

event modified;if ib_cod_prodotto then
	st_testo_descrittivo.text = f_sost_des_var_in_formula(this.text, is_cod_prodotto)
else
	st_testo_descrittivo.text = f_sost_campi_in_formula(this.text)
end if
end event

type dw_elenco_variabili from uo_cs_xx_dw within w_gen_formule_x_db
integer x = 23
integer y = 20
integer width = 1522
integer height = 624
integer taborder = 20
string dataobject = ""
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;call super::doubleclicked;string ls_cod_variabile
integer li_position

ls_cod_variabile = this.getitemstring(row, "tab_variabili_formule_cod_variabile")

li_position = mle_1.selectedstart()
mle_1.setfocus()
if isnull(li_position) then 
	mle_1.text = ls_cod_variabile + " "
else
	mle_1.replacetext(" " + ls_cod_variabile + " ")
end if
if ib_cod_prodotto then
	st_testo_descrittivo.text = f_sost_des_var_in_formula(mle_1.text, is_cod_prodotto)
else
	st_testo_descrittivo.text = f_sost_campi_in_formula(mle_1.text)
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

if ib_cod_prodotto then
	l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_prodotto)
else
	l_Error = Retrieve(s_cs_xx.cod_azienda, "%")
end if

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_annulla from commandbutton within w_gen_formule_x_db
event clicked pbm_bnclicked
integer x = 37
integer y = 2524
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
boolean cancel = true
end type

event clicked;ib_annulla = true
close(parent)
end event

type st_testo_descrittivo from statictext within w_gen_formule_x_db
integer x = 2139
integer y = 1200
integer width = 1010
integer height = 440
boolean bringtotop = true
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Courier New"
long backcolor = 79741120
boolean enabled = false
boolean border = true
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_gen_formule_x_db
integer x = 471
integer y = 2524
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
boolean default = true
end type

event clicked;ib_annulla = false
close(parent)
end event

type dw_legenda from datawindow within w_gen_formule_x_db
integer x = 256
integer y = 144
integer width = 2491
integer height = 928
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_legenda_formule_x_db"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;

choose case dwo.name
		
	case 	"potenza_t","int_t","truncate_t","round_t","sqr_t","exp_t","abs_t","mod_t","log_t","logten_t", &
			"pi_t","sin_t","cos_t","tan_t","asin_t","acos_t","atan_t", &
			"len_t",&
			"case_t","if_t"
				
		wf_set_valore(dwo.text)
		
end choose
end event

