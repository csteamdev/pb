﻿$PBExportHeader$w_lista_variabili_veloce.srw
forward
global type w_lista_variabili_veloce from window
end type
type cb_conferma from commandbutton within w_lista_variabili_veloce
end type
type dw_lista from datawindow within w_lista_variabili_veloce
end type
end forward

global type w_lista_variabili_veloce from window
accessiblerole accessiblerole = defaultrole!
integer x = 0
integer y = 0
integer width = 2875
integer height = 2452
boolean enabled = true
boolean titlebar = true
string title = "Lista Variabili valorizzate"
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean hscrollbar = false
boolean vscrollbar = false
boolean resizable = false
boolean border = true
windowtype windowtype = response!
windowstate windowstate = normal!
long backcolor = 12632256
string icon = "AppIcon!"
integer unitsperline = 0
integer linesperpage = 0
integer unitspercolumn = 0
integer columnsperpage = 0
boolean bringtotop = false
boolean toolbarvisible = true
toolbaralignment toolbaralignment = alignattop!
integer toolbarx = 0
integer toolbary = 0
integer toolbarwidth = 0
integer toolbarheight = 0
boolean righttoleft = false
boolean keyboardicon = true
boolean clientedge = false
boolean palettewindow = false
boolean contexthelp = false
boolean center = true
integer transparency = 0
windowanimationstyle openanimation = noanimation!
windowanimationstyle closeanimation = noanimation!
integer animationtime = 200
cb_conferma cb_conferma
dw_lista dw_lista
end type
global w_lista_variabili_veloce w_lista_variabili_veloce

type variables
datastore			ids_variabili
end variables

on w_lista_variabili_veloce.create
this.cb_conferma=create cb_conferma
this.dw_lista=create dw_lista
this.Control[]={this.cb_conferma,&
this.dw_lista}
end on

on w_lista_variabili_veloce.destroy
destroy(this.cb_conferma)
destroy(this.dw_lista)
end on

event open;s_cs_xx_parametri		lstr_parametri



lstr_parametri = message.powerobjectparm

ids_variabili 	= lstr_parametri.parametro_ds_1



ids_variabili.RowsCopy(1, ids_variabili.rowcount(), Primary!, dw_lista, 1, Primary!)
dw_lista.sort()



end event

type cb_conferma from commandbutton within w_lista_variabili_veloce
integer x = 2446
integer y = 28
integer width = 402
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_cs_xx_parametri		lstr_return



dw_lista.accepttext()

//svuoto il datastore
ids_variabili.reset()

//rimetto le variabili con relativo valore nel datastore mediante rowscopy da dw  a ds
dw_lista.RowsCopy(1, dw_lista.rowcount(), Primary!, ids_variabili, ids_variabili.rowcount() + 1, Primary!)

lstr_return.parametro_ds_1 = ids_variabili

closewithreturn(parent, lstr_return)

end event

type dw_lista from datawindow within w_lista_variabili_veloce
integer x = 32
integer y = 128
integer width = 2816
integer height = 2216
integer taborder = 10
string title = "none"
string dataobject = "d_compila_variabili_veloce"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

