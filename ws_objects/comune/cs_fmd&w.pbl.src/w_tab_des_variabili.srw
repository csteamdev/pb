﻿$PBExportHeader$w_tab_des_variabili.srw
$PBExportComments$Finestra Tabella Descrizioni X Variabili X Formule X Distinta Base
forward
global type w_tab_des_variabili from w_cs_xx_principale
end type
type dw_tipi_utenti from datawindow within w_tab_des_variabili
end type
type dw_tab_des_variabili_det from uo_cs_xx_dw within w_tab_des_variabili
end type
type dw_ricerca from u_dw_search within w_tab_des_variabili
end type
type dw_tab_des_variabili_lista from uo_cs_xx_dw within w_tab_des_variabili
end type
end forward

global type w_tab_des_variabili from w_cs_xx_principale
integer width = 5285
integer height = 3004
string title = "Descrizioni Variabili per Formule"
dw_tipi_utenti dw_tipi_utenti
dw_tab_des_variabili_det dw_tab_des_variabili_det
dw_ricerca dw_ricerca
dw_tab_des_variabili_lista dw_tab_des_variabili_lista
end type
global w_tab_des_variabili w_tab_des_variabili

forward prototypes
public function integer wf_carica_tipi_utenti (string as_cod_prodotto, string as_cod_variabile)
end prototypes

public function integer wf_carica_tipi_utenti (string as_cod_prodotto, string as_cod_variabile);string		ls_sql, ls_errore
long		ll_rows, ll_i, ll_riga, ll_find
datastore lds_data

ls_sql = " select cod_tipo_utente, des_tipo_utente from tab_tipi_utenti "
ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
if ll_rows < 0 then
	g_mb.error( g_str.format("Errore in caricamento lista tipi utenti.~r~n$1",ls_errore) )
	return -1
end if

dw_tipi_utenti.reset()
for ll_i = 1 to ll_rows
	ll_riga = dw_tipi_utenti.insertrow(0)
	dw_tipi_utenti.setitem(ll_riga, 1,"N")
	dw_tipi_utenti.setitem(ll_riga, 2, lds_data.getitemstring(ll_i,1))
	dw_tipi_utenti.setitem(ll_riga, 3, lds_data.getitemstring(ll_i,2))
next

if not isnull(as_cod_prodotto) and not isnull(as_cod_prodotto) then
	ls_sql = g_str.format(" select cod_tipo_utente from tab_des_variabili_utenti where cod_azienda = '$1' and cod_prodotto ='$2' and cod_variabile = '$3' ", s_cs_xx.cod_azienda, as_cod_prodotto,as_cod_variabile)
	ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
	for ll_i = 1 to ll_rows
		ll_find = dw_tipi_utenti.find(g_str.format(" cod_tipo_utente = '$1' ", lds_data.getitemstring(ll_i,1)),1, dw_tipi_utenti.rowcount() )
		if ll_find > 0 then dw_tipi_utenti.setitem(ll_find,"flag_selezione","S")
	next
end if
return 0
end function

on w_tab_des_variabili.create
int iCurrent
call super::create
this.dw_tipi_utenti=create dw_tipi_utenti
this.dw_tab_des_variabili_det=create dw_tab_des_variabili_det
this.dw_ricerca=create dw_ricerca
this.dw_tab_des_variabili_lista=create dw_tab_des_variabili_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_utenti
this.Control[iCurrent+2]=this.dw_tab_des_variabili_det
this.Control[iCurrent+3]=this.dw_ricerca
this.Control[iCurrent+4]=this.dw_tab_des_variabili_lista
end on

on w_tab_des_variabili.destroy
call super::destroy
destroy(this.dw_tipi_utenti)
destroy(this.dw_tab_des_variabili_det)
destroy(this.dw_ricerca)
destroy(this.dw_tab_des_variabili_lista)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

dw_tab_des_variabili_lista.set_dw_key("cod_azienda")
dw_tab_des_variabili_lista.set_dw_key("cod_prodotto")
dw_tab_des_variabili_lista.set_dw_key("cod_variabile")
dw_tab_des_variabili_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_tab_des_variabili_det.set_dw_options(sqlca, dw_tab_des_variabili_lista, c_sharedata+c_scrollparent, c_default)

iuo_dw_main = dw_tab_des_variabili_lista

l_criteriacolumn[1] = "cod_prodotto"
l_criteriacolumn[2] = "cod_variabile"

l_searchtable[1] = "tab_des_variabili"
l_searchtable[2] = "tab_des_variabili"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_variabile"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tab_des_variabili_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tab_des_variabili_lista, &
                 "cod_variabile", &
                 sqlca, &
                 "tab_variabili_formule", &
                 "cod_variabile", &
                 "isnull(nome_campo_database, cod_variabile) ", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tab_des_variabili_det, &
                 "cod_formula_validazione", &
                 sqlca, &
                 "tab_formule_db", &
                 "cod_formula", &
                 "des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_sort(dw_tab_des_variabili_lista, &
                 "des_variabile", &
                 sqlca, &
                 "tab_des_variabili", &
                 "count(*)", &
                 "des_variabile", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' group by des_variabile", "count(*) DESC")
					  
dw_ricerca.fu_loadcode("cod_variabile", &
                 "tab_variabili_formule", &
                 "cod_variabile", &
                 "isnull(nome_campo_database, cod_variabile)", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cod_variabile ", "(Tutti)")


f_po_loaddddw_dw(dw_tab_des_variabili_det, &
                 "cod_gruppo_var_addizionale", &
                 sqlca, &
                 "gruppi_varianti", &
                 "cod_gruppo_variante", &
                 "des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type dw_tipi_utenti from datawindow within w_tab_des_variabili
integer x = 4549
integer y = 260
integer width = 686
integer height = 1060
integer taborder = 30
string title = "none"
string dataobject = "d_tab_des_variabili_tipi_utenti"
boolean livescroll = true
end type

event itemchanged;choose case dwo.name
	case "flag_selezione"
		string 	ls_cod_prodotto, ls_cod_variabile, ls_errore, ls_cod_tipo_utente
		long		ll_cont
		transaction	ltrn_local

		if dw_tab_des_variabili_lista.getrow() = 0 then return
		
		ls_cod_prodotto = dw_tab_des_variabili_lista.getitemstring(dw_tab_des_variabili_lista.getrow(),"cod_prodotto")
		ls_cod_variabile = dw_tab_des_variabili_lista.getitemstring(dw_tab_des_variabili_lista.getrow(),"cod_variabile")
		ls_cod_tipo_utente = getitemstring(row,"cod_tipo_utente")
		
		if isnull(ls_cod_prodotto) or isnull(ls_cod_variabile) then return
		
		select 	count(*)
		into		:ll_cont
		from 		tab_des_variabili
		where 	cod_azienda = :s_cs_xx.cod_azienda and 
					cod_prodotto = :ls_cod_prodotto and 
					cod_variabile = :ls_cod_variabile ;
		if ll_cont < 1 then 
			g_mb.error("Prima di selezionare i tipi utenti salvare i dati inseriti nella riga della variabile.")
			return 2
		end if
		
		guo_functions.uof_create_transaction_from( sqlca,ltrn_local, ls_errore)

		
		delete 	tab_des_variabili_utenti
		where 	cod_azienda = :s_cs_xx.cod_azienda and 
					cod_prodotto = :ls_cod_prodotto and 
					cod_variabile = :ls_cod_variabile and
					cod_tipo_utente = :ls_cod_tipo_utente
		using 		ltrn_local;
		
		if data = "S" then
			insert into tab_des_variabili_utenti (
					cod_azienda,
					cod_prodotto,
					cod_variabile,
					cod_tipo_utente)
			values (
					:s_cs_xx.cod_azienda,
					:ls_cod_prodotto,
					:ls_cod_variabile,
					:ls_cod_tipo_utente) using ltrn_local;
		end if	
		commit using ltrn_local;
		
		disconnect using ltrn_local;
		
		destroy ltrn_local
		
end choose

			
			
			
			
			
			
			
			
			
			
			
			
			
end event

type dw_tab_des_variabili_det from uo_cs_xx_dw within w_tab_des_variabili
integer x = 23
integer y = 1340
integer width = 5211
integer height = 1560
integer taborder = 30
string dataobject = "d_tab_des_variabili_det"
boolean border = false
long il_limit_campo_note = 2000
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_tab_des_variabili_lista.setrow(getrow())
end if
end event

type dw_ricerca from u_dw_search within w_tab_des_variabili
integer x = 23
integer y = 20
integer width = 5211
integer height = 220
integer taborder = 10
string dataobject = "d_tab_variabili_filtro"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")

	case "b_ricerca"
			dw_ricerca.accepttext()
			
			dw_ricerca.fu_buildsearch(TRUE)
			dw_tab_des_variabili_lista.change_dw_current()
			parent.triggerevent("pc_retrieve")

	case "b_reset"
			dw_ricerca.fu_reset()
			
	case "b_lingue"
			window_open_parm(w_tab_des_variabili_lingue, -1, dw_tab_des_variabili_lista)
			
	case "b_duplica"
		string ls_cod_prodotto
			ls_cod_prodotto = dw_ricerca.getitemstring(1,"cod_prodotto")
			if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
				g_mb.show( "Indicare un prodotto da duplicare!" )
				return
			end if
			openwithparm(w_tab_des_variabili_duplica, ls_cod_prodotto, parent)
end choose

end event

type dw_tab_des_variabili_lista from uo_cs_xx_dw within w_tab_des_variabili
integer x = 23
integer y = 252
integer width = 4526
integer height = 1068
integer taborder = 20
string dataobject = "d_tab_des_variabili_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
long il_limit_campo_note = 2000
end type

event ue_key;call super::ue_key;if getcolumnname() = "cod_prodotto" and key=KeyF1! and keyflags= 1 then
		guo_ricerca.uof_ricerca_prodotto(dw_tab_des_variabili_lista,"cod_prodotto")
end if

if getcolumnname() = "tab_des_variabili_cod_formula_validazione" and key=KeyF1! and keyflags= 1 then
		guo_ricerca.uof_ricerca_formule_db(dw_tab_des_variabili_lista,"cod_tab_des_variabili_cod_formula_validazione")
end if
end event

event itemfocuschanged;call super::itemfocuschanged;pcca.mdi_frame.setmicrohelp("")
if isvalid(dwo) then
	choose case dwo.name
		case "cod_prodotto" 
			pcca.mdi_frame.setmicrohelp("SHIFT-F1 per la ricerca prodotto")
		case "tab_des_variabili_cod_formula_validazione"
			pcca.mdi_frame.setmicrohelp("SHIFT-F1 per la ricerca della formula")
	end choose
end if
end event

event pcd_modify;call super::pcd_modify;dw_ricerca.object.b_lingue.enabled = "No"
end event

event pcd_new;call super::pcd_new;dw_ricerca.object.b_lingue.enabled = "No"
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;dw_ricerca.object.b_lingue.enabled = "Yes"
end event

event pcd_view;call super::pcd_view;dw_ricerca.object.b_lingue.enabled = "Yes"
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if getrow() > 0 then
		string		ls_cod_prodotto, ls_cod_variabile
		
		ls_cod_prodotto = getitemstring(getrow(), "cod_prodotto")
		ls_cod_variabile = getitemstring(getrow(), "cod_variabile")
		
		wf_carica_tipi_utenti(ls_cod_prodotto, ls_cod_variabile)
	end if
end if
end event

