﻿$PBExportHeader$w_tab_des_variabili_duplica.srw
$PBExportComments$Finestra Tabella Descrizioni X Variabili X Formule X Distinta Base traduzioni in lingua
forward
global type w_tab_des_variabili_duplica from window
end type
type dw_duplica from uo_std_dw within w_tab_des_variabili_duplica
end type
end forward

global type w_tab_des_variabili_duplica from window
integer width = 2642
integer height = 500
boolean titlebar = true
string title = "Duplicazione Variabili"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_duplica dw_duplica
end type
global w_tab_des_variabili_duplica w_tab_des_variabili_duplica

type variables
string	is_cod_prodotto_origine
end variables

on w_tab_des_variabili_duplica.create
this.dw_duplica=create dw_duplica
this.Control[]={this.dw_duplica}
end on

on w_tab_des_variabili_duplica.destroy
destroy(this.dw_duplica)
end on

event open;is_cod_prodotto_origine = Message.StringParm
dw_duplica.insertrow(0)

end event

type dw_duplica from uo_std_dw within w_tab_des_variabili_duplica
integer width = 2583
integer height = 380
integer taborder = 10
string dataobject = "d_tab_des_variabili_duplica"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_duplica,"cod_prodotto")
		
	case "b_duplica"
		string	ls_cod_prodotto, ls_flag_cancella
		
		if g_mb.messagebox("SEP","Procedo con la duplicazione?",Question!, YesNo!, 2) = 2 then return
		
		dw_duplica.accepttext()
		ls_cod_prodotto = dw_duplica.getitemstring(1,"cod_prodotto")
		ls_flag_cancella = dw_duplica.getitemstring(1,"flag_cancella")
		
		if ls_flag_cancella = "S" then
			delete tab_des_variabili_lingue
			where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in cancellazione dati lingue del prodotto $1.~r~n$2",is_cod_prodotto_origine, sqlca.sqlerrtext) )
				rollback;
				return
			end if
			
			delete tab_des_variabili_utenti
			where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in cancellazione dati lingue del prodotto $1.~r~n$2",is_cod_prodotto_origine, sqlca.sqlerrtext) )
				rollback;
				return
			end if
			
			delete tab_des_variabili
			where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in cancellazione dati lingue del prodotto $1.~r~n$2",is_cod_prodotto_origine, sqlca.sqlerrtext) )
				rollback;
				return
			end if
			
		end if
		
		INSERT INTO tab_des_variabili  
				( cod_azienda,   
				  cod_prodotto,   
				  cod_variabile,   
				  des_variabile,   
				  flag_ipertech_apice,   
				  flag_ipertech_online,   
				  cod_formula_validazione,   
				  flag_validazione_immediata,   
				  flag_blocco_valore,   
				  prog_ordinamento,   
				  flag_addizionale,   
				  flag_optional,   
				  sql_valore_default,   
				  sql_origine_lista,   
				  des_commerciale,   
				  flag_off_ven,   
				  flag_bol_ven,   
				  flag_ord_ven,   
				  flag_fat_ven,   
				  cod_gruppo_var_addizionale,   
				  valore_default_disabilitato,
				  formula_abilitazione,
				  flag_nascondi_codice,
				  flag_obbligatorio,
				  flag_etichetta_prod,
				  prog_ordinamento_report_ol,
				  elastic_index,
				  elastic_query,
				  flag_tipo_visualizzazione,
				  formula_valore_default)  
		  SELECT cod_azienda,   
					:ls_cod_prodotto,   
					cod_variabile,   
					des_variabile,   
					flag_ipertech_apice,   
					flag_ipertech_online,   
					cod_formula_validazione,   
					flag_validazione_immediata,   
					flag_blocco_valore,   
					prog_ordinamento,   
					flag_addizionale,   
					flag_optional,   
					sql_valore_default,   
					sql_origine_lista,   
					des_commerciale,   
					flag_off_ven,   
					flag_bol_ven,   
					flag_ord_ven,   
					flag_fat_ven,   
					cod_gruppo_var_addizionale,   
				  	valore_default_disabilitato,
				  	formula_abilitazione,
				  	flag_nascondi_codice,
				  	flag_obbligatorio,
				  	flag_etichetta_prod,
				  	prog_ordinamento_report_ol,
				  	elastic_index,
				  	elastic_query,
				  	flag_tipo_visualizzazione,
					formula_valore_default
			 FROM tab_des_variabili  
			WHERE tab_des_variabili.cod_azienda = :s_cs_xx.cod_azienda  AND  
					 tab_des_variabili.cod_prodotto = :is_cod_prodotto_origine   ;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in INSERT in tabella tab_des_variabili con il prodotto origine $1 e prodotto destinazione.~r~n$2",is_cod_prodotto_origine, ls_cod_prodotto, sqlca.sqlerrtext) )
				rollback;
				return
			end if

			INSERT INTO tab_des_variabili_lingue  
				( cod_azienda,   
				cod_prodotto,   
				cod_variabile,   
				cod_lingua,   
				des_variabile )  
			SELECT cod_azienda,   
				:ls_cod_prodotto,   
				cod_variabile,   
				cod_lingua,   
				des_variabile  
			FROM tab_des_variabili_lingue  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda  AND  
				 cod_prodotto = :is_cod_prodotto_origine    ;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in INSERT in tabella tab_des_variabili_lingue con il prodotto origine $1 e prodotto destinazione.~r~n$2",is_cod_prodotto_origine, ls_cod_prodotto, sqlca.sqlerrtext) )
				rollback;
				return
			end if

			INSERT INTO tab_des_variabili_utenti  
				( cod_azienda,   
				  cod_prodotto,   
				  cod_variabile,   
				  cod_tipo_utente )  
			SELECT cod_azienda,   
					:ls_cod_prodotto,   
					cod_variabile,   
					cod_tipo_utente  
			 FROM tab_des_variabili_utenti  
			WHERE cod_azienda = :s_cs_xx.cod_azienda  AND  
					cod_prodotto = :is_cod_prodotto_origine    ;
			if sqlca.sqlcode < 0 then
				g_mb.error( g_str.format("Errore in INSERT in tabella tab_des_variabili_utenti con il prodotto origine $1 e prodotto destinazione.~r~n$2",is_cod_prodotto_origine, ls_cod_prodotto, sqlca.sqlerrtext) )
				rollback;
				return
			end if
			
			
			commit;
			g_mb.show("Duplicazione eseguita con successo!")
			close(parent)
end choose


end event

