﻿$PBExportHeader$w_tab_des_variabili_lingue.srw
$PBExportComments$Finestra Tabella Descrizioni X Variabili X Formule X Distinta Base traduzioni in lingua
forward
global type w_tab_des_variabili_lingue from w_cs_xx_principale
end type
type dw_des_variabili_lingue from uo_cs_xx_dw within w_tab_des_variabili_lingue
end type
end forward

global type w_tab_des_variabili_lingue from w_cs_xx_principale
integer width = 2674
integer height = 1380
string title = "Traduzione Variabili in Lingue"
dw_des_variabili_lingue dw_des_variabili_lingue
end type
global w_tab_des_variabili_lingue w_tab_des_variabili_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_des_variabili_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_des_variabili_lingue.set_dw_key("cod_azienda")
dw_des_variabili_lingue.set_dw_key("cod_prodotto")
dw_des_variabili_lingue.set_dw_key("cod_variabile")
dw_des_variabili_lingue.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)
iuo_dw_main = dw_des_variabili_lingue
end event

on w_tab_des_variabili_lingue.create
int iCurrent
call super::create
this.dw_des_variabili_lingue=create dw_des_variabili_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_des_variabili_lingue
end on

on w_tab_des_variabili_lingue.destroy
call super::destroy
destroy(this.dw_des_variabili_lingue)
end on

type dw_des_variabili_lingue from uo_cs_xx_dw within w_tab_des_variabili_lingue
integer x = 23
integer y = 20
integer width = 2587
integer height = 1228
string dataobject = "d_tab_des_variabili_lingue"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_prodotto, ls_cod_variabile


ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_variabile = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_variabile")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_cod_variabile)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_variabile, ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_variabile = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_variabile")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
   if isnull(this.getitemstring(ll_i, "cod_variabile")) then
      this.setitem(ll_i, "cod_variabile", ls_cod_variabile)
   end if
next
end event

