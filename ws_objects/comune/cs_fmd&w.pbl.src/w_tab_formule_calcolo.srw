﻿$PBExportHeader$w_tab_formule_calcolo.srw
$PBExportComments$Finestra Gestione Fornitori
forward
global type w_tab_formule_calcolo from w_cs_xx_principale
end type
type mle_log from multilineedit within w_tab_formule_calcolo
end type
type cb_test from commandbutton within w_tab_formule_calcolo
end type
type st_risultato from statictext within w_tab_formule_calcolo
end type
type dw_tab_formule_calcolo_det from uo_cs_xx_dw within w_tab_formule_calcolo
end type
type dw_folder from u_folder within w_tab_formule_calcolo
end type
type dw_folder_search from u_folder within w_tab_formule_calcolo
end type
type dw_tab_formule_calcolo_lista from uo_cs_xx_dw within w_tab_formule_calcolo
end type
type dw_tab_formule_calcolo_sel from u_dw_search within w_tab_formule_calcolo
end type
end forward

global type w_tab_formule_calcolo from w_cs_xx_principale
integer width = 3150
integer height = 2648
string title = "Gestione Formule"
event ue_inserisci_formula ( )
event ue_inserisci_var_stringa ( )
event ue_inserisci_var_forn ( )
event ue_inserisci_var_cli ( )
event ue_inserisci_var_prod ( )
event ue_inserisci_var_attr ( )
event ue_inserisci_var_numero ( )
event ue_inserisci_var_data ( )
event ue_inserisci_if ( )
mle_log mle_log
cb_test cb_test
st_risultato st_risultato
dw_tab_formule_calcolo_det dw_tab_formule_calcolo_det
dw_folder dw_folder
dw_folder_search dw_folder_search
dw_tab_formule_calcolo_lista dw_tab_formule_calcolo_lista
dw_tab_formule_calcolo_sel dw_tab_formule_calcolo_sel
end type
global w_tab_formule_calcolo w_tab_formule_calcolo

type variables
string is_sql_origine1 = ""

string is_clausola_if 			= "IF{condiz ; vero; falso}"

string is_var_numero 			= "#N:varnumero#"
string is_var_stringa 			= "#S:varstringa#"
string is_var_data 			= "#D:vardata#"
string is_var_fornitore		= "#S:F:CODFORN#"
string is_var_cliente			= "#S:C:CODCLI#"
string is_var_prodotto		= "#S:P:CODPROD#"
string is_var_attrezzatura	= "#S:A:CODATTR#"

string is_inizio_formula = "["
string is_fine_formula = "]"
end variables

forward prototypes
public subroutine wf_gestisci_campi_formula (ref datawindow fdw_datawindow, long fl_row, string fs_tipo, boolean fb_lista)
public subroutine wf_context_menu (string fs_colonna, string fs_tipo, long fl_row)
public subroutine wf_set_valore (string fs_valore)
end prototypes

event ue_inserisci_formula();any la_any[]
datawindow ldw_data

setnull(ldw_data)

guo_ricerca.uof_set_response( )
guo_ricerca.uof_ricerca_formule_calcolo(ldw_data, "cod_formula")
guo_ricerca.uof_get_results(la_any)


if upperbound(la_any) > 0 then
	dw_tab_formule_calcolo_det.ReplaceText (string(la_any[1]) )
end if



return
end event

event ue_inserisci_var_stringa();//dw_tab_formule_calcolo_det.ReplaceText (is_var_stringa )
wf_set_valore(is_var_stringa)
return
end event

event ue_inserisci_var_forn();//dw_tab_formule_calcolo_det.ReplaceText (is_var_fornitore )
wf_set_valore(is_var_fornitore)
return
end event

event ue_inserisci_var_cli();//dw_tab_formule_calcolo_det.ReplaceText (is_var_cliente )

wf_set_valore(is_var_cliente)
return
end event

event ue_inserisci_var_prod();//dw_tab_formule_calcolo_det.ReplaceText (is_var_prodotto )
wf_set_valore(is_var_prodotto)
return
end event

event ue_inserisci_var_attr();long ll_pos


//ll_pos = dw_tab_formule_calcolo_det.position()
//dw_tab_formule_calcolo_det.ReplaceText (is_var_attrezzatura )
//
//
//ll_pos += 5
//dw_tab_formule_calcolo_det.selecttext( ll_pos,  len(is_var_attrezzatura) - 6)

wf_set_valore(is_var_attrezzatura)

return
end event

event ue_inserisci_var_numero();//dw_tab_formule_calcolo_det.ReplaceText (is_var_numero )
wf_set_valore(is_var_numero)
return
end event

event ue_inserisci_var_data();//dw_tab_formule_calcolo_det.ReplaceText (is_var_data )
wf_set_valore(is_var_data)

return
end event

event ue_inserisci_if();
//dw_tab_formule_calcolo_det.ReplaceText (is_clausola_if )
wf_set_valore(is_clausola_if)

return
end event

public subroutine wf_gestisci_campi_formula (ref datawindow fdw_datawindow, long fl_row, string fs_tipo, boolean fb_lista);string ls_null
decimal ld_null

if fl_row>0 then
else
	return
end if

setnull(ls_null)
setnull(ld_null)

choose case fs_tipo
	case "N"
		fdw_datawindow.setitem(fl_row, "sql", ls_null)
		fdw_datawindow.setitem(fl_row, "tabella", ls_null)
		fdw_datawindow.setitem(fl_row, "colonna", ls_null)
		fdw_datawindow.setitem(fl_row, "composta", ls_null)	
		
	case "S"
		fdw_datawindow.setitem(fl_row, "numero", ld_null)
		fdw_datawindow.setitem(fl_row, "tabella", ls_null)
		fdw_datawindow.setitem(fl_row, "colonna", ls_null)
		fdw_datawindow.setitem(fl_row, "composta", ls_null)
		
	case "T"
		fdw_datawindow.setitem(fl_row, "numero", ld_null)
		fdw_datawindow.setitem(fl_row, "sql", ls_null)
		fdw_datawindow.setitem(fl_row, "composta", ls_null)		
		
	case "C"
		fdw_datawindow.setitem(fl_row, "numero", ld_null)
		fdw_datawindow.setitem(fl_row, "sql", ls_null)
		fdw_datawindow.setitem(fl_row, "tabella", ls_null)
		fdw_datawindow.setitem(fl_row, "colonna", ls_null)
		
	case "D"
		fdw_datawindow.setitem(fl_row, "numero", ld_null)
		fdw_datawindow.setitem(fl_row, "tabella", ls_null)
		fdw_datawindow.setitem(fl_row, "colonna", ls_null)
		
	case else
		
end choose
		

end subroutine

public subroutine wf_context_menu (string fs_colonna, string fs_tipo, long fl_row);m_formule_calcolo			lm_menu

if fs_tipo<>"C" and fs_tipo<>"S" then
	return
end if

lm_menu = create m_formule_calcolo

if fs_tipo="C" then
	//composta
	lm_menu.m_inserisciformula.enabled = true
	lm_menu.m_inserisciclausolaif.enabled = true
	
elseif fs_tipo="S" then
	//sql
	//disabilita inserimento formula e IF
	lm_menu.m_inserisciformula.enabled = false
	lm_menu.m_inserisciclausolaif.enabled = false
	
end if

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu;

return
end subroutine

public subroutine wf_set_valore (string fs_valore);long ll_pos, ll_fine, ll_pos2

ll_pos = dw_tab_formule_calcolo_det.position()

dw_tab_formule_calcolo_det.ReplaceText (fs_valore )

if upper(left(fs_valore, 2 )) = "IF" then
	ll_pos += 3
	ll_fine = 14 + 3
	
elseif pos(fs_valore, ":", 4)>0 then
	ll_pos += 5
	ll_fine = 5 + 1
	
else
	ll_pos += 3
	ll_fine = 3 + 1
	
end if

dw_tab_formule_calcolo_det.selecttext( ll_pos,  len(fs_valore) - ll_fine)
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[], l_vuoto[]


l_objects = l_vuoto
l_objects[1] = dw_tab_formule_calcolo_det
dw_folder.fu_assigntab(1,"Dettaglio",l_objects)
dw_folder.fu_foldercreate(1,2)
dw_folder.fu_selecttab(1)


dw_tab_formule_calcolo_lista.set_dw_key("cod_azienda")
dw_tab_formule_calcolo_lista.set_dw_options(	sqlca, &
															 pcca.null_object, &
															 c_noretrieveonopen, &
															 c_default)
dw_tab_formule_calcolo_det.set_dw_options(	sqlca, &
															dw_tab_formule_calcolo_lista, &
															c_sharedata + c_scrollparent, &
															c_default)
iuo_dw_main = dw_tab_formule_calcolo_lista


dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight,dw_folder_search.c_foldertabtop)

l_objects[1] = dw_tab_formule_calcolo_sel
dw_folder_search.fu_assigntab(1,"Ricerca",l_objects)

l_objects = l_vuoto
l_objects[1] = dw_tab_formule_calcolo_lista
dw_folder_search.fu_assigntab(2,"Lista", l_objects)

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(1)

dw_tab_formule_calcolo_lista.change_dw_current()



is_sql_origine1 =  dw_tab_formule_calcolo_lista.getsqlselect()

//rimpiazza eventuali doppie virgolette con vuoto  (es. "tabella"."codice" -> tabella.codice)
guo_functions.uof_replace_string(is_sql_origine1, '"', '')

//---------------------------------------
dw_tab_formule_calcolo_det.modify("numero.visible=~"1~tif(flag_tipo='N',1,0)~"")
dw_tab_formule_calcolo_det.modify("numero_t.visible=~"1~tif(flag_tipo='N',1,0)~"")

dw_tab_formule_calcolo_det.modify("sql.visible=~"1~tif(flag_tipo='S' or flag_tipo='D',1,0)~"")
dw_tab_formule_calcolo_det.modify("sql_t.visible=~"1~tif(flag_tipo='S' or flag_tipo='D',1,0)~"")

dw_tab_formule_calcolo_det.modify("colonna.visible=~"1~tif(flag_tipo='T',1,0)~"")
dw_tab_formule_calcolo_det.modify("colonna_t.visible=~"1~tif(flag_tipo='T',1,0)~"")
dw_tab_formule_calcolo_det.modify("tabella.visible=~"1~tif(flag_tipo='T',1,0)~"")
dw_tab_formule_calcolo_det.modify("tabella_t.visible=~"1~tif(flag_tipo='T',1,0)~"")

dw_tab_formule_calcolo_det.modify("composta.visible=~"1~tif(flag_tipo='C',1,0)~"")
dw_tab_formule_calcolo_det.modify("composta_t.visible=~"1~tif(flag_tipo='C',1,0)~"")

dw_tab_formule_calcolo_det.modify("funzione.visible=~"1~tif(flag_tipo='F',1,0)~"")
dw_tab_formule_calcolo_det.modify("funzione_t.visible=~"1~tif(flag_tipo='F',1,0)~"")
//---------------------------------------

end event

on w_tab_formule_calcolo.create
int iCurrent
call super::create
this.mle_log=create mle_log
this.cb_test=create cb_test
this.st_risultato=create st_risultato
this.dw_tab_formule_calcolo_det=create dw_tab_formule_calcolo_det
this.dw_folder=create dw_folder
this.dw_folder_search=create dw_folder_search
this.dw_tab_formule_calcolo_lista=create dw_tab_formule_calcolo_lista
this.dw_tab_formule_calcolo_sel=create dw_tab_formule_calcolo_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_log
this.Control[iCurrent+2]=this.cb_test
this.Control[iCurrent+3]=this.st_risultato
this.Control[iCurrent+4]=this.dw_tab_formule_calcolo_det
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_folder_search
this.Control[iCurrent+7]=this.dw_tab_formule_calcolo_lista
this.Control[iCurrent+8]=this.dw_tab_formule_calcolo_sel
end on

on w_tab_formule_calcolo.destroy
call super::destroy
destroy(this.mle_log)
destroy(this.cb_test)
destroy(this.st_risultato)
destroy(this.dw_tab_formule_calcolo_det)
destroy(this.dw_folder)
destroy(this.dw_folder_search)
destroy(this.dw_tab_formule_calcolo_lista)
destroy(this.dw_tab_formule_calcolo_sel)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tab_formule_calcolo_det, &
                 "funzione", &
                 sqlca, &
                 "tab_formule_calcolo_funzioni", &
                 "id", &
                 "funzione", &
                "")

end event

type mle_log from multilineedit within w_tab_formule_calcolo
integer x = 32
integer y = 2312
integer width = 3045
integer height = 200
integer taborder = 190
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean vscrollbar = true
boolean displayonly = true
boolean hideselection = false
end type

type cb_test from commandbutton within w_tab_formule_calcolo
integer x = 23
integer y = 2224
integer width = 402
integer height = 84
integer taborder = 180
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Test"
end type

event clicked;string								ls_cod_formula, ls_select, ls_errore, ls_tipo_formula
decimal							ld_risultato
long								ll_row
uo_formule_calcolo			luo_formule
s_cs_xx_parametri 			lstr_parametri

ll_row = dw_tab_formule_calcolo_lista.getrow()

if ll_row>0 then
else
	return
end if

ls_cod_formula = dw_tab_formule_calcolo_lista.getitemstring(ll_row, "cod_formula")
ls_tipo_formula = dw_tab_formule_calcolo_lista.getitemstring(ll_row, "flag_tipo")

if g_str.isempty(ls_cod_formula) then
	return
end if

if ls_tipo_formula = "D" then
	lstr_parametri.parametro_s_1 = ls_cod_formula
	window_open_parm(w_tab_formule_calcolo_test, -1, lstr_parametri)
	st_risultato.text = "Aperta finestra di test."
	
else
	luo_formule = create  uo_formule_calcolo

	if luo_formule.uof_calcola_formula(ls_cod_formula, ls_select, ld_risultato, ls_errore) < 0 then
		destroy luo_formule;
		mle_log.text = "ERRORE:~r~n" + ls_errore
		
		return
	end if
	
	destroy luo_formule;
	
	st_risultato.text = string(ld_risultato, "###,###,##0.0000")
	mle_log.text = ls_select
	
end if




end event

type st_risultato from statictext within w_tab_formule_calcolo
integer x = 457
integer y = 2232
integer width = 2624
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type dw_tab_formule_calcolo_det from uo_cs_xx_dw within w_tab_formule_calcolo
integer x = 55
integer y = 1052
integer width = 2958
integer height = 1116
integer taborder = 170
string dataobject = "d_tab_formule_calcolo_det"
boolean border = false
long il_limit_campo_note = 1999
end type

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_formula


if getrow() > 0 then
	ls_cod_formula = getitemstring(i_rownbr, "cod_formula")
	
	if len(ls_cod_formula)<=0 then
		g_mb.error("Attenzione", "Inserire un codice formula!")
		pcca.error = c_fatal
		return
	end if
	
	if left(ls_cod_formula, 1) = is_inizio_formula then
	else
		g_mb.error("Attenzione", "Il codice formula deve iniziare con il carattere '"+is_inizio_formula+"' ")
		pcca.error = c_fatal
		return
	end if
	
	if right(ls_cod_formula, 1) = is_fine_formula then
	else
		g_mb.error("Attenzione", "Il codice formula deve terminare con il carattere '"+is_fine_formula+"' ")
		pcca.error = c_fatal
		return
	end if
	
end if

end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_tipo"
		wf_gestisci_campi_formula(dw_tab_formule_calcolo_det, row, data, false)
	
end choose
end event

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	dw_fornitori_lista.setrow(getrow())
//end if
//

//string ls_tipo_formula
//
//if currentrow>0 then
//	ls_tipo_formula = getitemstring(currentrow, "flag_tipo")
//else
//	ls_tipo_formula = ""
//end if
//
//wf_gestisci_campi_formula(dw_tab_formule_calcolo_det, currentrow, ls_tipo_formula, false)
end event

event rbuttondown;//OCIO ancestor script sovrascritto !!!!!!!


string ls_tipo

if row>0 and (ib_stato_nuovo or ib_stato_modifica) then
	ls_tipo = getitemstring(row, "flag_tipo")
	
	choose case dwo.name
		case "sql"
			if ls_tipo="S" then
				wf_context_menu(dwo.name, ls_tipo, row)
				
				//poi esci dallo script
				return
			end if
			
		case "composta"
			if ls_tipo="C" then
				wf_context_menu(dwo.name, ls_tipo, row)
				
				//poi esci dallo script
				return
			end if
			
	end choose
	
	super::event rbuttondown(xpos, ypos, row, dwo)

else
	super::event rbuttondown(xpos, ypos, row, dwo)
end if



//
//
//
//
////******************************************************************
////  PC Module     : uo_DW_Main
////  Event         : RButtonDown
////  Description   : Processes the Right-button-down event.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
//IF NOT i_InUse THEN
//   i_Window.TriggerEvent("RButtonDown")
//   GOTO Finished
//END IF
//
////----------
////  Make sure that this window is active before we process
////  the right mouse button event.
////----------
//
//IF PCCA.Window_Current <> i_Window THEN
//   i_Window.SetFocus()
//   Change_DW_Focus(THIS)
//END IF
//
//// stefanop: 12/10/2011 Fix per il problema della perdita di fuoco della DW dopo aver aperto la finestra di
//// ricerca !!! Maledetto Framework !!!
//if isnull(PCCA.Window_Current) then
//	PCCA.Window_Current = i_Window
//end if
////----------
//
//
////----------
////  Make sure that the popup menu has been initialized.
////----------
//
//IF NOT i_PopupMenuInit THEN
//   Set_DW_PMenu(i_PopupMenu)
//END IF
//
////----------
////  Make sure that we have a right-button Popup Menu.
////----------
//
//IF i_PopupMenuIsValid THEN
//
//   //----------
//   //  Make sure the POPUP menus are loaded correctly.
//   //----------
//
//   is_EventControl.Control_Mode = c_ControlPopupMenu
//   TriggerEvent("pcd_SetControl")
//
//   //----------
//   //  We need to specify the position of the right-button Popup
//   //  Menu.  If the window that contains this DataWindow is
//   //  within an MDI frame, we need to specify the Popup Menu
//   //  postion relative to the MDI Frame.  If the window is not
//   //  inside the MDI frame, then we specify the Popup Menu's
//   //  position relative to the window.  Note that a Response!-
//   //  style window is not inside the MDI frame, even for a MDI
//   //  application.
//   //----------
//
//   IF PCCA.MDI_Frame_Valid AND &
//      i_Window.WindowType <> Response! THEN
//      i_PopupMenu.PopMenu(PCCA.MDI_Frame.PointerX(), &
//                          PCCA.MDI_Frame.PointerY())
//   ELSE
//      i_PopupMenu.PopMenu(i_Window.PointerX(), &
//                          i_Window.PointerY())
//   END IF
//END IF
//
//Finished:
//
//i_ExtendMode = i_InUse
end event

type dw_folder from u_folder within w_tab_formule_calcolo
integer x = 23
integer y = 944
integer width = 3049
integer height = 1256
integer taborder = 50
end type

type dw_folder_search from u_folder within w_tab_formule_calcolo
integer x = 23
integer y = 20
integer width = 3049
integer height = 900
integer taborder = 20
end type

type dw_tab_formule_calcolo_lista from uo_cs_xx_dw within w_tab_formule_calcolo
integer x = 55
integer y = 152
integer width = 2958
integer height = 732
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_tab_formule_calcolo_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
long il_limit_campo_note = 1999
end type

event pcd_retrieve;call super::pcd_retrieve;long			ll_error, ll_pos
string			ls_cod_formula, ls_flag_tipo, ls_contenuto, ls_sql_new, ls_des_formula


dw_tab_formule_calcolo_sel.accepttext()

ls_cod_formula = dw_tab_formule_calcolo_sel.getitemstring(1,"cod_formula")
ls_flag_tipo = dw_tab_formule_calcolo_sel.getitemstring(1,"flag_tipo")
ls_des_formula = dw_tab_formule_calcolo_sel.getitemstring(1,"des_formula")
ls_contenuto = dw_tab_formule_calcolo_sel.getitemstring(1,"contenuto_formula")

if isnull(ls_cod_formula) then ls_cod_formula=""
if isnull(ls_contenuto) then ls_contenuto=""
if isnull(ls_des_formula) then ls_des_formula=""

ls_sql_new = is_sql_origine1 + " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

if ls_cod_formula<> "" then
	ls_sql_new += "and cod_formula like '%"+ls_cod_formula+"%' "
end if

if ls_flag_tipo<> "X" then
	ls_sql_new += "and flag_tipo='"+ls_flag_tipo+"' "
end if

if ls_des_formula<> "" then
	ls_sql_new += "and des_formula like '%"+ls_des_formula+"%' "
end if

if ls_contenuto<>"" then
	ls_sql_new += "and (sql like '%"+ls_contenuto+"%' or tabella like '%"+ls_contenuto+"%' or colonna like '%"+ls_contenuto+"%' or composta like '%"+ls_contenuto+"%') "
end if

//piazzo ORDER BY
ls_sql_new += "order by tab_formule_calcolo.cod_formula ASC "

setsqlselect(ls_sql_new)
ll_error = retrieve()

if ll_error < 0 then
	pcca.error = c_fatal
end if

dw_folder_search.fu_selecttab(2)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i,"cod_azienda"))then
		this.setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next
end event

event rowfocuschanged;call super::rowfocuschanged;

st_risultato.text = ""
end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_tipo"
		wf_gestisci_campi_formula(dw_tab_formule_calcolo_lista, row, data, true)
	
end choose
end event

event pcd_new;call super::pcd_new;cb_test.enabled = false
end event

event pcd_view;call super::pcd_view;cb_test.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_test.enabled = false
end event

type dw_tab_formule_calcolo_sel from u_dw_search within w_tab_formule_calcolo
integer x = 55
integer y = 152
integer width = 2958
integer height = 732
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_tab_formule_calcolo_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string		ls_null

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_annulla"
		setnull(ls_null)
		dw_tab_formule_calcolo_sel.setitem(1,"cod_formula", ls_null)
		dw_tab_formule_calcolo_sel.setitem(1,"flag_tipo", "X")
		dw_tab_formule_calcolo_sel.setitem(1,"contenuto_formula",ls_null)
		dw_tab_formule_calcolo_sel.setitem(1,"des_formula",ls_null)
		
		
	case "b_cerca"
		dw_tab_formule_calcolo_lista.change_dw_current()
		parent.postevent("pc_retrieve")

end choose
end event

