﻿$PBExportHeader$w_tab_formule_calcolo_test.srw
forward
global type w_tab_formule_calcolo_test from w_cs_xx_principale
end type
type dw_result from datawindow within w_tab_formule_calcolo_test
end type
type st_log from statictext within w_tab_formule_calcolo_test
end type
end forward

global type w_tab_formule_calcolo_test from w_cs_xx_principale
integer width = 2185
integer height = 1584
dw_result dw_result
st_log st_log
end type
global w_tab_formule_calcolo_test w_tab_formule_calcolo_test

on w_tab_formule_calcolo_test.create
int iCurrent
call super::create
this.dw_result=create dw_result
this.st_log=create st_log
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_result
this.Control[iCurrent+2]=this.st_log
end on

on w_tab_formule_calcolo_test.destroy
call super::destroy
destroy(this.dw_result)
destroy(this.st_log)
end on

event pc_setwindow;call super::pc_setwindow;string ls_error, ls_sql, ls_syntax
long ll_rows, ll_columns
uo_formule_calcolo luo_formule
s_cs_xx_parametri lstr_parametri
uo_ds lds_datastore

lstr_parametri = message.powerobjectparm
ll_rows = 0
ll_columns = 0

setnull(message.powerobjectparm)

title = "Test formula " +lstr_parametri.parametro_s_1

luo_formule = create uo_formule_calcolo
ls_sql = luo_formule.uof_calcola_formula_datastore_sql(lstr_parametri.parametro_s_1, ls_error)

if g_str.isnotempty(ls_error) then
	g_mb.error(ls_error)
	
else
	ls_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)
	
	if g_str.isempty(ls_error) then
		dw_result.create(ls_syntax, ls_error)
		
		if g_str.isempty(ls_error) then
			
			setpointer(HourGlass!)
			dw_result.settransobject(sqlca)
			ll_rows = dw_result.retrieve()
			ll_columns = Long(dw_result.Object.DataWindow.Column.Count)
			setpointer(Arrow!)
		else
			g_mb.error(ls_error)
		end if
	else
		g_mb.error(ls_error)
	end if
	
end if

st_log.text= g_str.format("Trovate $1 righe visualizzate in $2 colonne", ll_rows, ll_columns)
destroy luo_formule



end event

event resize;/**

 **/
 
 st_log.width = newwidth - 40
 st_log.y = newheight - st_log.height - 20
 
 
 dw_result.resize(st_log.width, st_log.y - 40)
end event

type dw_result from datawindow within w_tab_formule_calcolo_test
integer x = 18
integer y = 20
integer width = 2103
integer height = 1320
integer taborder = 10
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_log from statictext within w_tab_formule_calcolo_test
integer x = 18
integer y = 1380
integer width = 1349
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
alignment alignment = right!
boolean focusrectangle = false
end type

