﻿$PBExportHeader$w_tab_formule_db.srw
$PBExportComments$Finestra Tabella Formule X Distinta Base
forward
global type w_tab_formule_db from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_tab_formule_db
end type
type cb_test from commandbutton within w_tab_formule_db
end type
type dw_tab_formule_db_lista from uo_cs_xx_dw within w_tab_formule_db
end type
type dw_tab_formule_db_det from uo_cs_xx_dw within w_tab_formule_db
end type
type dw_folder from u_folder within w_tab_formule_db
end type
end forward

global type w_tab_formule_db from w_cs_xx_principale
integer width = 2702
integer height = 2368
string title = "Tabella Formule X Distinta Base"
boolean resizable = false
windowtype windowtype = response!
dw_ricerca dw_ricerca
cb_test cb_test
dw_tab_formule_db_lista dw_tab_formule_db_lista
dw_tab_formule_db_det dw_tab_formule_db_det
dw_folder dw_folder
end type
global w_tab_formule_db w_tab_formule_db

type variables
string is_cod_prodotto, is_sql_base
end variables

on w_tab_formule_db.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.cb_test=create cb_test
this.dw_tab_formule_db_lista=create dw_tab_formule_db_lista
this.dw_tab_formule_db_det=create dw_tab_formule_db_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.cb_test
this.Control[iCurrent+3]=this.dw_tab_formule_db_lista
this.Control[iCurrent+4]=this.dw_tab_formule_db_det
this.Control[iCurrent+5]=this.dw_folder
end on

on w_tab_formule_db.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.cb_test)
destroy(this.dw_tab_formule_db_lista)
destroy(this.dw_tab_formule_db_det)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],  lw_vuoto[]


unsignedlong lul_modalita

choose case s_cs_xx.parametri.parametro_s_1
   case "nuovo"
      lul_modalita = c_newonopen
   case "modifica"
      lul_modalita = c_modifyonopen
   case else
      lul_modalita = c_viewonopen
end choose


dw_tab_formule_db_lista.set_dw_key("cod_azienda")
dw_tab_formule_db_lista.set_dw_key("cod_formula_db")
dw_tab_formule_db_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen + lul_modalita, c_default)
dw_tab_formule_db_det.set_dw_options(sqlca, dw_tab_formule_db_lista, c_sharedata+c_scrollparent, c_default)

iuo_dw_main = dw_tab_formule_db_lista

if not isnull(s_cs_xx.parametri.parametro_s_12) and s_cs_xx.parametri.parametro_s_12 <> "" then
	is_cod_prodotto = s_cs_xx.parametri.parametro_s_12
	setnull(s_cs_xx.parametri.parametro_s_12)
else
	setnull(is_cod_prodotto)
end if

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tab_formule_db_lista
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
//dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tab_formule_db_lista
is_sql_base = dw_tab_formule_db_lista.getsqlselect()

if s_cs_xx.parametri.parametro_s_1="nuovo" or s_cs_xx.parametri.parametro_s_1="modifica" then
	dw_folder.fu_selecttab(2)
else
	dw_folder.fu_selecttab(1)
end if

setnull(s_cs_xx.parametri.parametro_s_1)
end event

event pc_delete;call super::pc_delete;dw_tab_formule_db_det.object.b_componi_formula.enabled = false
end event

type dw_ricerca from u_dw_search within w_tab_formule_db
integer x = 41
integer y = 124
integer width = 1961
integer height = 764
integer taborder = 30
string dataobject = "d_tab_formule_db_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cerca"
		dw_tab_formule_db_lista.change_dw_current( )
		parent.postevent("pc_retrieve")
		
end choose
end event

type cb_test from commandbutton within w_tab_formule_db
integer x = 2203
integer y = 2192
integer width = 457
integer height = 80
integer taborder = 31
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "TEST FORMULA"
end type

event clicked;string								ls_cod_formula, ls_errore, ls_tipo_ritorno
decimal							ld_risultato
long								ll_row
uo_formule_calcolo			luo_formule

ll_row = dw_tab_formule_db_lista.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_formula = dw_tab_formule_db_lista.getitemstring(ll_row, "cod_formula")

if ls_cod_formula<>"" and not isnull(ls_cod_formula) then
else
	return
end if

ls_tipo_ritorno = dw_tab_formule_db_lista.getitemstring(ll_row, "flag_tipo")

luo_formule = create  uo_formule_calcolo

//modalità gibus
luo_formule.ib_gibus = true

//modalità testing
luo_formule.ii_anno_reg_ord_ven = 0
luo_formule.il_num_reg_ord_ven = 0
luo_formule.il_prog_riga_ord_ven = 0
luo_formule.is_tipo_ritorno = ls_tipo_ritorno

if luo_formule.uof_calcola_formula_db(ls_cod_formula, ld_risultato, ls_errore) < 0 then
	destroy luo_formule;
	ls_errore = "ERR:~r~n" + ls_errore
	
	g_mb.error("Errore Test Formula", ls_errore)
	
	return
end if

destroy luo_formule;


end event

type dw_tab_formule_db_lista from uo_cs_xx_dw within w_tab_formule_db
integer x = 37
integer y = 112
integer width = 2633
integer height = 892
integer taborder = 20
string dataobject = "d_tab_formule_db_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
long il_limit_campo_note = 2000
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  	l_Error
string		ls_sql, ls_temp

ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

dw_ricerca.accepttext()

ls_temp = dw_ricerca.getitemstring(1, "cod_formula")
if ls_temp<>"" and not isnull(ls_temp) then
	ls_sql += " and cod_formula like '%"+ls_temp+"%'"
end if

ls_temp = dw_ricerca.getitemstring(1, "des_formula")
if ls_temp<>"" and not isnull(ls_temp) then
	ls_sql += " and des_formula like '%"+ls_temp+"%'"
end if

ls_temp = dw_ricerca.getitemstring(1, "des_testo_formula")
if ls_temp<>"" and not isnull(ls_temp) then
	ls_sql += " and testo_formula like '%"+ls_temp+"%'"
end if

ls_temp = dw_ricerca.getitemstring(1, "flag_tipo")
if ls_temp<>"" and not isnull(ls_temp) and ls_temp<>"T" then
	ls_sql += " and flag_tipo = '"+ls_temp+"'"
end if


setsqlselect(ls_sql)

l_Error = Retrieve()

dw_folder.fu_selecttab(2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;dw_tab_formule_db_det.object.b_componi_formula.enabled = true
end event

event pcd_new;call super::pcd_new;dw_tab_formule_db_det.object.b_componi_formula.enabled = true
end event

event pcd_save;call super::pcd_save;dw_tab_formule_db_det.object.b_componi_formula.enabled = false
end event

event pcd_view;call super::pcd_view;dw_tab_formule_db_det.object.b_componi_formula.enabled = false
end event

type dw_tab_formule_db_det from uo_cs_xx_dw within w_tab_formule_db
integer x = 32
integer y = 1056
integer width = 2629
integer height = 1116
integer taborder = 10
string dataobject = "d_tab_formule_db_det"
borderstyle borderstyle = styleraised!
long il_limit_campo_note = 2000
end type

event buttonclicked;call super::buttonclicked;

if row>0 then
else
	return
end if

dw_tab_formule_db_lista.accepttext()

choose case dwo.name
	case "b_componi_formula"
		//--------------------------------------------------------
		
		if getitemstring(row, "flag_tipo")="" or isnull(getitemstring(row, "flag_tipo")) then
			g_mb.warning("Specificare il valore di ritorno per la formula!")
			return
		end if
		
		s_cs_xx.parametri.parametro_s_15 = getitemstring(row, "testo_formula")
		s_cs_xx.parametri.parametro_s_14 = getitemstring(row, "testo_des_formula")
		s_cs_xx.parametri.parametro_s_12 = is_cod_prodotto
		s_cs_xx.parametri.parametro_s_11 = getitemstring(row, "flag_tipo")
		
		window_open(w_gen_formule_x_db, 0)
		
		if s_cs_xx.parametri.parametro_s_15 <> "%" then //Non ho fatto annulla, Aggiorno il campo Testo_Formula
		
			setitem(row, "testo_formula", s_cs_xx.parametri.parametro_s_15)
			setitem(row, "testo_des_formula", s_cs_xx.parametri.parametro_s_14)
			
		end if
		
		setnull(s_cs_xx.parametri.parametro_s_15) // testo_codici_variabili
		setnull(s_cs_xx.parametri.parametro_s_14) // testo_campi_database
		setnull(s_cs_xx.parametri.parametro_s_13) // testo_des_variabili
		setnull(s_cs_xx.parametri.parametro_s_12) // cod_prodotto
		//--------------------------------------------------------
		
end choose
end event

event itemchanged;call super::itemchanged;choose case string(dwo.name)
		
	case "cod_formula"
		if len(data) < 6 then
			g_mb.warning("Attenzione: è necessario inserire un codice formula di 6 caratteri.")
			return 1
		end if
		
end choose
end event

type dw_folder from u_folder within w_tab_formule_db
integer x = 9
integer y = 8
integer width = 2674
integer height = 1032
integer taborder = 10
end type

