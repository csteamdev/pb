﻿$PBExportHeader$w_tab_variabili.srw
$PBExportComments$Finestra Tabella Variabili X Formule X Distinta Base
forward
global type w_tab_variabili from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_tab_variabili
end type
type dw_tab_variabili_lista from uo_cs_xx_dw within w_tab_variabili
end type
type dw_tab_variabili_det from uo_cs_xx_dw within w_tab_variabili
end type
type dw_folder from u_folder within w_tab_variabili
end type
end forward

global type w_tab_variabili from w_cs_xx_principale
integer width = 2930
integer height = 1948
string title = "Variabili X Formule X Distinta Base"
dw_ricerca dw_ricerca
dw_tab_variabili_lista dw_tab_variabili_lista
dw_tab_variabili_det dw_tab_variabili_det
dw_folder dw_folder
end type
global w_tab_variabili w_tab_variabili

type variables
string				is_sql_base
end variables

on w_tab_variabili.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.dw_tab_variabili_lista=create dw_tab_variabili_lista
this.dw_tab_variabili_det=create dw_tab_variabili_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.dw_tab_variabili_lista
this.Control[iCurrent+3]=this.dw_tab_variabili_det
this.Control[iCurrent+4]=this.dw_folder
end on

on w_tab_variabili.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.dw_tab_variabili_lista)
destroy(this.dw_tab_variabili_det)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],  lw_vuoto[]


dw_tab_variabili_lista.set_dw_key("cod_azienda")
dw_tab_variabili_lista.set_dw_key("cod_variabile")


dw_tab_variabili_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen, c_default)
dw_tab_variabili_det.set_dw_options(sqlca, dw_tab_variabili_lista, c_sharedata+c_scrollparent, c_default)


lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tab_variabili_lista
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tab_variabili_lista
is_sql_base = dw_tab_variabili_lista.getsqlselect()
end event

type dw_ricerca from u_dw_search within w_tab_variabili
integer x = 18
integer y = 128
integer width = 1975
integer height = 692
integer taborder = 30
string dataobject = "d_tab_variabili_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca"
		dw_tab_variabili_lista.change_dw_current()
		parent.postevent("pc_retrieve")
		
		
end choose
end event

type dw_tab_variabili_lista from uo_cs_xx_dw within w_tab_variabili
integer x = 9
integer y = 124
integer width = 2853
integer height = 772
integer taborder = 20
string dataobject = "d_tab_variabili_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long  				l_error
string				ls_sql, ls_temp


dw_ricerca.accepttext()

ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

ls_temp = dw_ricerca.getitemstring(1, "cod_variabile")
if ls_temp<>"" and not isnull(ls_temp) then
	ls_sql += " and cod_variabile like '%"+ls_temp+"%'"
end if

ls_temp = dw_ricerca.getitemstring(1, "nome_tabella_database")
if ls_temp<>"" and not isnull(ls_temp) and ls_temp<>'T' then
	ls_sql += " and nome_tabella_database='"+ls_temp+"'"
end if

ls_temp = dw_ricerca.getitemstring(1, "nome_campo_database")
if ls_temp<>"" and not isnull(ls_temp) then
	ls_sql += " and nome_campo_database like '%"+ls_temp+"%'"
end if

ls_temp = dw_ricerca.getitemstring(1, "flag_tipo_dato")
if ls_temp<>"" and not isnull(ls_temp) and ls_temp<>'T' then
	ls_sql += " and flag_tipo_dato = '"+ls_temp+"'"
end if

setsqlselect(ls_sql)
l_error = Retrieve()

dw_folder.fu_selecttab(2)

IF l_error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;setitem(getrow(),"flag_tipo_dato","S")
end event

type dw_tab_variabili_det from uo_cs_xx_dw within w_tab_variabili
integer x = 23
integer y = 900
integer width = 2834
integer height = 920
integer taborder = 10
string dataobject = "d_tab_variabili_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;/**
 * stefanop
 * 22/05/2014
 * 
 * Aggiugo controlli nella selezione della visualizzazione
 **/
string ls_test

choose case dwo.name
		
	case "flag_tipo_visualizzazione"
		// Se di tipo CheckBox allora il tipo dato DEVE essere di tipo Stringa
		if "C" = data then
			if getitemstring(row, "flag_tipo_dato") <> "S" then
				setitem(row, "flag_tipo_dato", "S")
			end if			
		end if
		
end choose
end event

type dw_folder from u_folder within w_tab_variabili
integer y = 8
integer width = 2875
integer height = 900
integer taborder = 30
end type

