﻿$PBExportHeader$w_variabili_tv.srw
forward
global type w_variabili_tv from w_cs_xx_treeview
end type
type dw_variabili from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_prodotto_det from uo_cs_xx_dw within det_2
end type
type dw_prodotto from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_prodotto_det dw_prodotto_det
dw_prodotto dw_prodotto
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_formule from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_formule dw_formule
end type
end forward

global type w_variabili_tv from w_cs_xx_treeview
integer width = 4535
integer height = 2680
string title = "Variabili"
end type
global w_variabili_tv w_variabili_tv

type variables
constant string LIVELLO_PRODOTTO = "P"
constant string LIVELLO_FORMULA = "F"
constant string LIVELLO_VARIABILE = "N"

private:
	long il_livello
	string is_sql_join
	string is_sql_orderby
	
	// Icone
	int ICONA_VARIABILE, ICONA_PRODOTTO, ICONA_FORMULA
	
	uo_formule_calcolo iuo_formule
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_prodotto (long al_handle)
public function long wf_inserisci_formula (long al_handle)
public function long wf_inserisci_variabile (long al_handle)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public subroutine wf_treeview_icons ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_visualizza_tab (string as_tipo_livello)
end prototypes

public subroutine wf_imposta_ricerca ();string ls_value

is_sql_filtro = ""
is_sql_join = ""
is_sql_orderby = ""
il_livello = 0

ls_value = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_variabile")
if g_str.isnotempty(ls_value) then
	is_sql_filtro += " AND tab_variabili_formule.cod_variabile='" + ls_value + "' "
end if

ls_value = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "tabella_origine")
if g_str.isnotempty(ls_value) then
	is_sql_filtro += " AND tab_variabili_formule.nome_tabella_database='" + ls_value + "' "
end if

ls_value = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "nome_campo_db")
if g_str.isnotempty(ls_value) then
	is_sql_filtro += " AND tab_variabili_formule.nome_campo_database='" + ls_value + "' "
end if

// Prodotto
ls_value = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto")
if g_str.isnotempty(ls_value) or wf_has_livello("P") then
	is_sql_join += " JOIN tab_des_variabili ON tab_des_variabili.cod_azienda = tab_variabili_formule.cod_azienda " &
					+ " AND tab_des_variabili.cod_variabile = tab_variabili_formule.cod_variabile "
					
	is_sql_orderby += " tab_des_variabili.prog_ordinamento asc "
end if

if g_str.isnotempty(ls_value) then
	is_sql_filtro += " AND tab_des_variabili.cod_prodotto='" + ls_value + "' "
end if

// Formula
ls_value = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_formula")

if g_str.isnotempty(ls_value) or wf_has_livello("F") then
	is_sql_join += "LEFT OUTER JOIN tab_formule_db_variabili ON tab_formule_db_variabili.cod_azienda = tab_variabili_formule.cod_azienda &
						AND tab_formule_db_variabili.cod_variabile = tab_variabili_formule.cod_variabile &
						LEFT OUTER JOIN tab_formule_db ON tab_formule_db.cod_azienda = tab_formule_db_variabili.cod_azienda &
						AND tab_formule_db.cod_formula = tab_formule_db_variabili.cod_formula "
end if

if g_str.isnotempty(ls_value) then
	is_sql_filtro += " AND tab_formule_db_variabili.cod_formula='" + ls_value + "' "
end if
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Prodotto", "P")
wf_add_valore_livello("Formula", "F")


end subroutine

public function long wf_inserisci_prodotto (long al_handle);string ls_sql, ls_label, ls_cod_prodotto, ls_des_prodotto, ls_error
long ll_rows, ll_i
datastore lds_store
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_des_variabili.cod_prodotto, anag_prodotti.des_prodotto FROM tab_variabili_formule " &
		+ is_sql_join  + "LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = tab_des_variabili.cod_azienda " &
		+ " AND anag_prodotti.cod_prodotto = tab_des_variabili.cod_prodotto " &
		+ " WHERE tab_variabili_formule.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " ORDER BY tab_des_variabili.cod_prodotto asc "

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

wf_check_limit(ll_rows)

for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = lds_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = lds_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = LIVELLO_PRODOTTO
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_formula (long al_handle);string ls_sql, ls_label, ls_cod_formula, ls_des_formula, ls_error
long ll_rows, ll_i
datastore lds_store
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT tab_formule_db.cod_formula, tab_formule_db.des_formula FROM tab_variabili_formule " &
		+ is_sql_join + " WHERE tab_formule_db.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " ORDER BY tab_formule_db.cod_formula asc"

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

wf_check_limit(ll_rows)

for ll_i = 1 to ll_rows
	
	 ls_cod_formula = lds_store.getitemstring(ll_i, 1)
	 ls_des_formula = lds_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = LIVELLO_FORMULA
	lstr_data.codice = ls_cod_formula
	
	if isnull(ls_cod_formula) and isnull(ls_des_formula) then
		ls_label = "<Variabili non usate>"
	elseif isnull(ls_des_formula) then
		ls_label = ls_cod_formula
	else
		ls_label = ls_cod_formula + " - " + ls_des_formula
	end if

	ltvi_item = wf_new_item(true, ICONA_FORMULA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_variabile (long al_handle);string ls_sql, ls_cod_variabile, ls_error
long ll_rows, ll_i
datastore lds_store
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_variabili_formule.cod_variabile "

if pos(lower(is_sql_join), "tab_des_variabili") > 0 then ls_sql += ", tab_des_variabili.prog_ordinamento "

ls_sql += " FROM tab_variabili_formule "  + is_sql_join + "WHERE tab_variabili_formule.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro 

wf_leggi_parent(al_handle, ls_sql)

if g_str.isnotempty(is_sql_orderby) then
	// Se ho impostato un ordinamento forzato lo applico.
	// Utile nella visualizzazione varibili legate ad un prodotto
	ls_sql += " ORDER BY " + is_sql_orderby
else
	ls_sql += " ORDER BY tab_variabili_formule.cod_variabile asc "
end if

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

// Limite risultati
wf_check_limit(ll_rows)

for ll_i = 1 to ll_rows
	ls_cod_variabile = lds_store.getitemstring(ll_i, 1)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = LIVELLO_VARIABILE
	lstr_data.codice = ls_cod_variabile
	
	ltvi_item = wf_new_item(false, ICONA_VARIABILE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_cod_variabile
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "P"
		return wf_inserisci_prodotto(al_handle)
		
	case "F"
		return wf_inserisci_formula(al_handle)
				
	case else
		return wf_inserisci_variabile(al_handle)
		
end choose
end function

public subroutine wf_treeview_icons ();ICONA_VARIABILE = wf_treeview_add_icon("treeview\tipo_documento.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\cog.png")
ICONA_FORMULA = wf_treeview_add_icon("treeview\cog.png")
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "P"
			if g_str.isempty(lstr_data.codice) then
				as_sql += " AND tab_des_variabili.cod_prodotto is null "
			else
				as_sql += " AND tab_des_variabili.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_formule_db_variabili.cod_formula is null "
			else
				as_sql += " AND tab_formule_db_variabili.cod_formula = '" + lstr_data.codice + "' "
			end if
			
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tab_variabili_formule.cod_variabile is null "
			else
				as_sql += " AND tab_variabili_formule.cod_variabile = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_visualizza_tab (string as_tipo_livello);/**
 * stefanop
 * 11/07/2014
 *
 * Visualliza il tab del livello corrente.
 **/
 
tab_dettaglio.det_1.visible = as_tipo_livello = LIVELLO_VARIABILE
tab_dettaglio.det_2.visible = as_tipo_livello = LIVELLO_PRODOTTO
tab_dettaglio.det_3.visible = as_tipo_livello = LIVELLO_FORMULA

choose case as_tipo_livello
		
	case LIVELLO_VARIABILE
		tab_dettaglio.selecttab(1)
		
	case LIVELLO_PRODOTTO
		tab_dettaglio.selecttab(2)
		
	case LIVELLO_FORMULA
		tab_dettaglio.selecttab(3)
		
end choose
end subroutine

on w_variabili_tv.create
int iCurrent
call super::create
end on

on w_variabili_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;is_codice_filtro = "VAR"

iuo_formule = create uo_formule_calcolo

tab_dettaglio.det_1.dw_variabili.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_variabili.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)

tab_dettaglio.det_2.dw_prodotto.set_dw_key("cod_azienda")
tab_dettaglio.det_2.dw_prodotto.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_prodotto_det.set_dw_options(sqlca, tab_dettaglio.det_2.dw_prodotto, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_3.dw_formule.set_dw_key("cod_azienda")
tab_dettaglio.det_3.dw_formule.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
end event

event close;call super::close;destroy iuo_formule
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_variabili_tv
integer width = 3109
integer height = 2520
det_2 det_2
det_3 det_3
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
end on

event tab_dettaglio::ue_resize;call super::ue_resize;
det_2.dw_prodotto_det.move(20, det_2.height - det_2.dw_prodotto_det.height - 20)
det_2.dw_prodotto.height = det_2.dw_prodotto_det.y - 40
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3072
integer height = 2396
string text = "Variabile"
dw_variabili dw_variabili
end type

on det_1.create
this.dw_variabili=create dw_variabili
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_variabili
end on

on det_1.destroy
call super::destroy
destroy(this.dw_variabili)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_variabili_tv
integer height = 2540
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer height = 2416
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer height = 2400
string dataobject = "d_variabili_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_cerca_variabile"
		guo_ricerca.uof_ricerca_variabili_formule(tab_ricerca.ricerca.dw_ricerca, "cod_variabile")
		
	case "b_cerca_formula"
		guo_ricerca.uof_ricerca_formule_db(tab_ricerca.ricerca.dw_ricerca, "cod_formula")
		
	case "b_cerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(tab_ricerca.ricerca.dw_ricerca, "cod_prodotto")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer height = 2416
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data

wf_visualizza_tab(istr_data.tipo_livello)

choose case istr_data.tipo_livello
		
	case LIVELLO_PRODOTTO
		tab_dettaglio.det_2.dw_prodotto.change_dw_current()
		
		
	case LIVELLO_FORMULA
		tab_dettaglio.det_3.dw_formule.change_dw_current()
		
	case LIVELLO_VARIABILE
		tab_dettaglio.det_1.dw_variabili.change_dw_current()

end choose
	
getwindow().triggerevent("pc_retrieve")

end event

type dw_variabili from uo_cs_xx_dw within det_1
integer x = 5
integer y = 12
integer width = 2240
integer height = 980
integer taborder = 30
string dataobject = "d_tab_variaibili_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;
retrieve(s_cs_xx.cod_azienda, istr_data.codice)
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3072
integer height = 2396
long backcolor = 12632256
string text = "Prodotto"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_prodotto_det dw_prodotto_det
dw_prodotto dw_prodotto
end type

on det_2.create
this.dw_prodotto_det=create dw_prodotto_det
this.dw_prodotto=create dw_prodotto
this.Control[]={this.dw_prodotto_det,&
this.dw_prodotto}
end on

on det_2.destroy
destroy(this.dw_prodotto_det)
destroy(this.dw_prodotto)
end on

type dw_prodotto_det from uo_cs_xx_dw within det_2
integer x = 27
integer y = 1192
integer width = 2309
integer height = 1160
integer taborder = 11
string dataobject = "d_variabili_prodotto_det"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_cerca_variabile"
		guo_ricerca.uof_ricerca_variabili_formule(dw_prodotto_det, "cod_variabile")
		
	case "b_cerca_formula"
		guo_ricerca.uof_ricerca_formule_db(dw_prodotto_det, "cod_formula_validazione")
		
end choose
end event

type dw_prodotto from uo_cs_xx_dw within det_2
integer x = 27
integer y = 12
integer width = 2263
integer height = 620
integer taborder = 11
string dataobject = "d_variabili_prodotto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda, istr_data.codice)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(ll_i, "cod_prodotto")) then
		setitem(ll_i, "cod_prodotto", istr_data.codice)
	end if
next  
end event

event updateend;call super::updateend;
if rowsinserted + rowsdeleted > 0 then
	
	wf_popola_item(il_current_handle)
	
end if
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3072
integer height = 2396
long backcolor = 12632256
string text = "Formule"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_formule dw_formule
end type

on det_3.create
this.dw_formule=create dw_formule
this.Control[]={this.dw_formule}
end on

on det_3.destroy
destroy(this.dw_formule)
end on

type dw_formule from uo_cs_xx_dw within det_3
integer x = 27
integer y = 12
integer width = 2537
integer height = 1460
integer taborder = 11
string dataobject = "d_variabili_formule"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda, istr_data.codice)
end event

event buttonclicked;call super::buttonclicked;string ls_cod_prodotto

choose case dwo.name
	case "b_componi_formula"
		
		//--------------------------------------------------------
		if getitemstring(row, "flag_tipo")="" or isnull(getitemstring(row, "flag_tipo")) then
			g_mb.warning("Specificare il valore di ritorno per la formula!")
			return
		end if
		
		setnull(ls_cod_prodotto)
		s_cs_xx.parametri.parametro_s_15 = getitemstring(row, "testo_formula")
		s_cs_xx.parametri.parametro_s_14 = getitemstring(row, "testo_des_formula")
		s_cs_xx.parametri.parametro_s_12 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_11 = getitemstring(row, "flag_tipo")
		
		window_open(w_gen_formule_x_db, 0)
		
		if s_cs_xx.parametri.parametro_s_15 <> "%" then //Non ho fatto annulla, Aggiorno il campo Testo_Formula
		
			setitem(row, "testo_formula", s_cs_xx.parametri.parametro_s_15)
			setitem(row, "testo_des_formula", s_cs_xx.parametri.parametro_s_14)
			
		end if
		
		setnull(s_cs_xx.parametri.parametro_s_15) // testo_codici_variabili
		setnull(s_cs_xx.parametri.parametro_s_14) // testo_campi_database
		setnull(s_cs_xx.parametri.parametro_s_13) // testo_des_variabili
		setnull(s_cs_xx.parametri.parametro_s_12) // cod_prodotto
		//--------------------------------------------------------
		
end choose
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
next  
end event

event updateend;call super::updateend;string ls_error
int li_i

if rowsinserted + rowsupdated > 0 then
		
	// aggiorno tabella di riferimento
	for li_i = 1 to rowcount()
		if iuo_formule.uof_allinea_variabili_formula(getitemstring(li_i, "cod_formula"), ls_error) < 0 then
			g_mb.error(ls_error)
		end if
	next
	
end if

if rowsdeleted > 0 then
	
	wf_set_deleted_item_status()
	
end if
end event

event updatestart;call super::updatestart;string ls_cod_formula
int li_i

if deletedcount() > 0 then
	
	for li_i = 1 to deletedcount()
		
		ls_cod_formula = getitemstring(li_i, "cod_formula", delete!, true)
		
		delete from tab_formule_db_variabili
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_formula = :ls_cod_formula;
				 
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione delle variabili collegate alla formula.", sqlca)
			return -1
		end if
		
	next
	
end if
end event

