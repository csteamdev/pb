﻿$PBExportHeader$w_comunica_sconti_clienti.srw
forward
global type w_comunica_sconti_clienti from w_cs_xx_principale
end type
type cb_email from commandbutton within w_comunica_sconti_clienti
end type
type htb_1 from htrackbar within w_comunica_sconti_clienti
end type
type st_count from statictext within w_comunica_sconti_clienti
end type
type st_msg from statictext within w_comunica_sconti_clienti
end type
type cb_stampa from commandbutton within w_comunica_sconti_clienti
end type
type cb_esporta from commandbutton within w_comunica_sconti_clienti
end type
type tab_testo_standard from tab within w_comunica_sconti_clienti
end type
type tbp_1 from userobject within tab_testo_standard
end type
type mle_testo_standard1 from multilineedit within tbp_1
end type
type tbp_1 from userobject within tab_testo_standard
mle_testo_standard1 mle_testo_standard1
end type
type tbp_2 from userobject within tab_testo_standard
end type
type mle_sconto_standard1 from multilineedit within tbp_2
end type
type tbp_2 from userobject within tab_testo_standard
mle_sconto_standard1 mle_sconto_standard1
end type
type tbp_3 from userobject within tab_testo_standard
end type
type mle_note_dopo_sconti1 from multilineedit within tbp_3
end type
type tbp_3 from userobject within tab_testo_standard
mle_note_dopo_sconti1 mle_note_dopo_sconti1
end type
type tbp_4 from userobject within tab_testo_standard
end type
type em_altro_6 from editmask within tbp_4
end type
type em_altro_5 from editmask within tbp_4
end type
type em_altro_4 from editmask within tbp_4
end type
type em_altro_3 from editmask within tbp_4
end type
type em_altro_2 from editmask within tbp_4
end type
type em_altro_1 from editmask within tbp_4
end type
type st_altro_6 from statictext within tbp_4
end type
type st_altro_5 from statictext within tbp_4
end type
type st_altro_4 from statictext within tbp_4
end type
type st_altro_3 from statictext within tbp_4
end type
type st_altro_2 from statictext within tbp_4
end type
type st_altro_1 from statictext within tbp_4
end type
type tbp_4 from userobject within tab_testo_standard
em_altro_6 em_altro_6
em_altro_5 em_altro_5
em_altro_4 em_altro_4
em_altro_3 em_altro_3
em_altro_2 em_altro_2
em_altro_1 em_altro_1
st_altro_6 st_altro_6
st_altro_5 st_altro_5
st_altro_4 st_altro_4
st_altro_3 st_altro_3
st_altro_2 st_altro_2
st_altro_1 st_altro_1
end type
type tab_testo_standard from tab within w_comunica_sconti_clienti
tbp_1 tbp_1
tbp_2 tbp_2
tbp_3 tbp_3
tbp_4 tbp_4
end type
type dw_report from uo_cs_xx_dw within w_comunica_sconti_clienti
end type
type dw_folder from u_folder within w_comunica_sconti_clienti
end type
type dw_selezione from uo_cs_xx_dw within w_comunica_sconti_clienti
end type
end forward

global type w_comunica_sconti_clienti from w_cs_xx_principale
integer width = 4402
integer height = 2816
string title = "Estrazione Sconti Clienti"
boolean resizable = false
cb_email cb_email
htb_1 htb_1
st_count st_count
st_msg st_msg
cb_stampa cb_stampa
cb_esporta cb_esporta
tab_testo_standard tab_testo_standard
dw_report dw_report
dw_folder dw_folder
dw_selezione dw_selezione
end type
global w_comunica_sconti_clienti w_comunica_sconti_clienti

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables
string is_localita_azienda, is_path_logo_testata, is_path_logo_atelier
end variables

forward prototypes
public function integer wf_elabora ()
public function integer wf_offerta_standard (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, string fs_cod_prodotto_figlio, ref string fs_variazione)
public function string wf_cliente_atelier (string fs_cod_cliente)
public function integer wf_testo_standard_old (string as_cod_cliente)
public function integer wf_testo_standard (string as_cod_cliente, ref string as_testo_linee_sconto, ref string as_valori_linee_sconto)
public function integer wf_offerte (string fs_cod_cliente, string fs_ultima_condizione, datetime fdt_data_da, datetime fdt_data_a, string fs_oggetto, string fs_rag_soc_1_cliente, string fs_indirizzo_cliente, string fs_cap_cliente, string fs_localita_cliente, string fs_provincia_cliente, string fs_testo_linee_sconto, string fs_valori_linee_sconto)
public function integer wf_linee_sconto (string fs_cod_cliente, string fs_ultima_condizione, datetime fdt_data_da, datetime fdt_data_a, string fs_oggetto, string fs_rag_soc_1_cliente, string fs_indirizzo_cliente, string fs_cap_cliente, string fs_localita_cliente, string fs_provincia_cliente, string fs_testo_linee_sconto, string fs_valori_linee_sconto, string fs_cod_gruppo_sconto)
public function integer wf_sconto_standard_cfz (string fs_cod_cliente, ref decimal fd_sconto_std)
end prototypes

public function integer wf_elabora ();string			ls_cod_cliente_da, ls_cod_cliente_a, ls_solo_ultima, ls_cod_agente, ls_cod_zona, ls_cod_tipo_anagrafica, &
					ls_sql_clienti,ls_cod_deposito, ls_cod_cliente_cu, ls_rag_soc_cu, ls_indirizzo_cu, ls_cap_cu, &
					ls_localita_cu, ls_pr_cu, ls_oggetto, ls_modify, ls_cod_gruppo_sconto, ls_valori_linee_sconti, &
					ls_testo_linee_sconti

long			ll_i_cliente, ll_tot_clienti

datetime 	ldt_data_da, ldt_data_a

datastore	lds_clienti_count





dw_selezione.accepttext()
dw_report.reset()

ls_modify = "intestazione.filename='" +is_path_logo_testata + "'~t"
dw_report.modify(ls_modify)

dw_report.modify("p_atelier.filename='" +is_path_logo_atelier + "'~t")

ls_oggetto = "COMUNICAZIONE SCONTI"

ls_solo_ultima = dw_selezione.getitemstring(1, "flag_estrai_ultima")
if ls_solo_ultima <> "S" then
	ldt_data_da = dw_selezione.getitemdatetime(1, "da_data")
	ldt_data_a = dw_selezione.getitemdatetime(1, "a_data")
	
	if isnull(ldt_data_da) or year(date(ldt_data_da))<=1950 and &
		isnull(ldt_data_a) or year(date(ldt_data_a))<=1950 then
		
		g_mb.error("E' necessario specificare un periodo Validità dal-al")
		return -1
	end if
	
	if ldt_data_da>ldt_data_a then
		g_mb.error("Periodo Validità indicato incongruente!")
		return -1
	end if

else
	setnull(ldt_data_da)
	setnull(ldt_data_a)
end if

ls_cod_agente = dw_selezione.getitemstring(1, "cod_agente")
ls_cod_zona = dw_selezione.getitemstring(1, "cod_zona")
ls_cod_tipo_anagrafica = dw_selezione.getitemstring(1, "cod_tipo_anagrafica")
ls_cod_deposito = dw_selezione.getitemstring(1, "cod_deposito")

ls_sql_clienti = "select cod_cliente, rag_soc_1,indirizzo, cap, localita, provincia, cod_gruppo_sconto "+&
					"from anag_clienti "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda +"' and flag_blocco ='N' and cod_cliente>='"+ls_cod_cliente_da+"' and "+&
								"cod_cliente>='"+ls_cod_cliente_a+"' "

ls_cod_cliente_da = dw_selezione.getitemstring(1, "cod_cliente_da")
if ls_cod_cliente_da<>"" and not isnull(ls_cod_cliente_da) then
	ls_sql_clienti += " and cod_cliente>='"+ls_cod_cliente_da+"' "
end if

ls_cod_cliente_a = dw_selezione.getitemstring(1, "cod_cliente_a")
if ls_cod_cliente_a<>"" and not isnull(ls_cod_cliente_a) then
	ls_sql_clienti += " and cod_cliente<='"+ls_cod_cliente_a+"' "
end if

if ls_cod_agente<>"" and not isnull(ls_cod_agente) then &
		ls_sql_clienti += " and (cod_agente_1='"+ls_cod_agente+"' or cod_agente_2='"+ls_cod_agente+"') "
		
if ls_cod_zona<>"" and not isnull(ls_cod_zona) then &
		ls_sql_clienti += " and cod_zona='"+ls_cod_zona+"' "
		
if ls_cod_tipo_anagrafica<>"" and not isnull(ls_cod_tipo_anagrafica) then &
		ls_sql_clienti += " and cod_tipo_anagrafica='"+ls_cod_tipo_anagrafica+"' "

if ls_cod_deposito <>"" and not isnull(ls_cod_deposito) then &
		ls_sql_clienti += " and cod_deposito='"+ls_cod_deposito+"' "

st_msg.text = ""
st_count.text = ""

f_crea_datastore(lds_clienti_count, ls_sql_clienti)
ll_tot_clienti = lds_clienti_count.retrieve()
ll_i_cliente = 0

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_clienti;
open dynamic cu_clienti;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore clienti. Dettaglio = " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

do while true
	fetch cu_clienti into 
			:ls_cod_cliente_cu,
			:ls_rag_soc_cu,
			:ls_indirizzo_cu,
			:ls_cap_cu,
			:ls_localita_cu,
			:ls_pr_cu,
			:ls_cod_gruppo_sconto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore clienti. Dettaglio = " + sqlca.sqlerrtext)
		close cu_clienti;
		rollback;
		setpointer(Arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if	

	Yield()
	
	if isnull(ls_cod_gruppo_sconto) then continue
	
	ll_i_cliente += 1
	
	st_msg.text = "Elaborazione cliente "+ls_cod_cliente_cu+" "+ls_rag_soc_cu
	st_count.text = string(ll_i_cliente) + " di " + string(ll_tot_clienti)
	
	// ----EnMe 14-02-2014: richiesto da Beatrice; la testata deve essere personalizzata con il secondo livello del confezionato ------
	if wf_testo_standard(ls_cod_cliente_cu, ref ls_testo_linee_sconti, ref ls_valori_linee_sconti) = -1 then continue
	
	//per cliente estrai le condizioni particolari linee di sconto (modelli e sfuso -----------------------------
	if wf_linee_sconto(ls_cod_cliente_cu, ls_solo_ultima, ldt_data_da, ldt_data_a, ls_oggetto, ls_rag_soc_cu, ls_indirizzo_cu, ls_cap_cu, ls_localita_cu, ls_pr_cu, ls_testo_linee_sconti, ls_valori_linee_sconti, ls_cod_gruppo_sconto) < 0 then
		close cu_clienti;
		return -1
	end if
	
	//offerte tessuti -----------------------------------------------------------------------------------------------
	if wf_offerte(ls_cod_cliente_cu, ls_solo_ultima, ldt_data_da, ldt_data_a, ls_oggetto, ls_rag_soc_cu, ls_indirizzo_cu, ls_cap_cu, ls_localita_cu, ls_pr_cu, ls_testo_linee_sconti, ls_valori_linee_sconti) < 0 then
		close cu_clienti;
		return -1
	end if
loop

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "yes"
//dw_report.change_dw_current()

close cu_clienti;
return 1
end function

public function integer wf_offerta_standard (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, string fs_cod_prodotto_figlio, ref string fs_variazione);string ls_sql
datastore lds_cond_std
decimal ld_variazione

ls_sql = "select LPCVC.data_inizio_val,LPCVC.cod_prodotto_listino,"+&
					"LPCVC.cod_prodotto_figlio,LPCVC.flag_sconto_mag_prezzo_1,"+&
					"LPCVC.variazione_1 "+&
			"from listini_prod_comune  LPCVC "+&
			"where LPCVC.cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
					"LPCVC.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"' and "+&
					"LPCVC.cod_valuta='"+fs_cod_valuta+"' and "+&
					"LPCVC.data_inizio_val='"+string( fdt_data_inizio_val, s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"LPCVC.progressivo="+string(fl_progressivo)+" and "+&
					"LPCVC.cod_prodotto_figlio='"+fs_cod_prodotto_figlio+"' "+&
			"order by LPCVC.cod_prodotto_listino asc, LPCVC.cod_prodotto_figlio asc"
			
fs_variazione = ""
ld_variazione = 0
if not f_crea_datastore(lds_cond_std, ls_sql) then
	g_mb.error("Errore in creazione datastore varianti standard")
	return -1
end if

if lds_cond_std.retrieve() > 0 then
	ld_variazione = lds_cond_std.getitemdecimal(1, 5)
	
	if isnull(ld_variazione) then ld_variazione = 0
	
end if

fs_variazione = string(ld_variazione, "#######0.00")

return 1
end function

public function string wf_cliente_atelier (string fs_cod_cliente);string ls_cliente_atelier, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica

//verifica se il tipo anagrafica è ATELIER ---------------------------------------------------------------------------------------------------
ls_cliente_atelier = "N"
ls_cod_tipo_anagrafica =  f_des_tabella("anag_clienti",  "cod_cliente='"+fs_cod_cliente+"'", "cod_tipo_anagrafica")

if ls_cod_tipo_anagrafica<>"" and not isnull(ls_cod_tipo_anagrafica) then
	ls_des_tipo_anagrafica =  f_des_tabella("tab_tipi_anagrafiche",  "cod_tipo_anagrafica='"+ls_cod_tipo_anagrafica+"' and "+&
																	"flag_tipo_anagrafica = 'C'", "des_tipo_anagrafica")
	
	ls_des_tipo_anagrafica = upper(ls_des_tipo_anagrafica)
	if pos(ls_des_tipo_anagrafica, "ATELIER")>0 then
		ls_cliente_atelier = "S"
	end if
end if
// ------------------------------------------------------------------------------------------------------------------------------------------------

return ls_cliente_atelier
end function

public function integer wf_testo_standard_old (string as_cod_cliente);//string 	ls_testo, ls_cod_gruppo_sconto, ls_sql, ls_sql_cliente, ls_gruppo_precedente,ls_cod_livello_prod_2,ls_des_livello_2
//integer 	ll_ret, ll_i
//dec{4}	ld_sconto_1, ld_sconto_2
//datastore lds_data
//
//select cod_gruppo_sconto 
//into :ls_cod_gruppo_sconto
//from anag_clienti
//where cod_azienda = :s_cs_xx.cod_azienda and
//		cod_cliente = :as_cod_cliente;
//
//choose case sqlca.sqlcode
//	case 100
//		g_mb.error("Cliente " + as_cod_cliente + " inesistente in anagrafica ")
//		return -1
//	case -1
//		g_mb.error("Errore in fase di selezione del cliente.~r~n" + sqlca.sqlerrtext)
//		return -1
//end choose
//
//
//
//ls_sql_cliente = &
//" SELECT cod_cliente, data_inizio_val,gruppi_sconto.cod_livello_prod_1,gruppi_sconto.cod_livello_prod_2,gruppi_sconto.cod_livello_prod_3,gruppi_sconto.cod_livello_prod_4, gruppi_sconto.sconto_1,  gruppi_sconto.sconto_2, tab_livelli_prod_2.des_livello, tab_livelli_prod_2.des_agg_livello as des_agg_livello " + &
//" FROM tab_livelli_prod_2    " + &
//" left join gruppi_sconto on 	gruppi_sconto.cod_azienda = tab_livelli_prod_2.cod_azienda and 	 " + &
//" gruppi_sconto.cod_livello_prod_1 = tab_livelli_prod_2.cod_livello_prod_1 and 	 " + &
//" gruppi_sconto.cod_livello_prod_2 = tab_livelli_prod_2.cod_livello_prod_2   " + &
//" where  " + &
//" gruppi_sconto.cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
//" gruppi_sconto.cod_cliente ='"+as_cod_cliente+"'   " + &
//" and gruppi_sconto.cod_livello_prod_1 = '01'   " + &
//" union " + &
//" SELECT cod_cliente, data_inizio_val,gruppi_sconto.cod_livello_prod_1,gruppi_sconto.cod_livello_prod_2,gruppi_sconto.cod_livello_prod_3,gruppi_sconto.cod_livello_prod_4, gruppi_sconto.sconto_1,  gruppi_sconto.sconto_2,  tab_livelli_prod_1.des_livello,  tab_livelli_prod_1.des_agg_livello as des_agg_livello " + &
//" FROM tab_livelli_prod_1    " + &
//" left join gruppi_sconto on 	gruppi_sconto.cod_azienda = tab_livelli_prod_1.cod_azienda and 	 " + &
//" gruppi_sconto.cod_livello_prod_1 = tab_livelli_prod_1.cod_livello_prod_1   " + &
//" where  " + &
//" gruppi_sconto.cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
//" gruppi_sconto.cod_cliente ='"+as_cod_cliente+"'   " + &
//" and gruppi_sconto.cod_livello_prod_1 = '01'   " + &
//" order by des_agg_livello, des_agg_livello, gruppi_sconto.cod_livello_prod_2, data_inizio_val DESC "
//
//
//ls_sql = &
//" SELECT cod_cliente, data_inizio_val,gruppi_sconto.cod_livello_prod_1,gruppi_sconto.cod_livello_prod_2,gruppi_sconto.cod_livello_prod_3,gruppi_sconto.cod_livello_prod_4, gruppi_sconto.sconto_1,  gruppi_sconto.sconto_2, tab_livelli_prod_2.des_livello, tab_livelli_prod_2.des_agg_livello as des_agg_livello " + &
//" FROM tab_livelli_prod_2    " + &
//" left join gruppi_sconto on 	gruppi_sconto.cod_azienda = tab_livelli_prod_2.cod_azienda and 	 " + &
//" gruppi_sconto.cod_livello_prod_1 = tab_livelli_prod_2.cod_livello_prod_1 and 	 " + &
//" gruppi_sconto.cod_livello_prod_2 = tab_livelli_prod_2.cod_livello_prod_2   " + &
//" where  " + &
//" gruppi_sconto.cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
//" gruppi_sconto.cod_cliente is null   " + &
//" and gruppi_sconto.cod_livello_prod_1 = '01'   " + &
//" union " + &
//" SELECT cod_cliente, data_inizio_val,gruppi_sconto.cod_livello_prod_1,gruppi_sconto.cod_livello_prod_2,gruppi_sconto.cod_livello_prod_3,gruppi_sconto.cod_livello_prod_4, gruppi_sconto.sconto_1,  gruppi_sconto.sconto_2,  tab_livelli_prod_1.des_livello,  tab_livelli_prod_1.des_agg_livello as des_agg_livello " + &
//" FROM tab_livelli_prod_1    " + &
//" left join gruppi_sconto on 	gruppi_sconto.cod_azienda = tab_livelli_prod_1.cod_azienda and 	 " + &
//" gruppi_sconto.cod_livello_prod_1 = tab_livelli_prod_1.cod_livello_prod_1   " + &
//" where  " + &
//" gruppi_sconto.cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
//" gruppi_sconto.cod_cliente is null " + &
//" and gruppi_sconto.cod_livello_prod_1 = '01'   " + &
//" order by des_agg_livello, gruppi_sconto.cod_livello_prod_2, data_inizio_val DESC "
//
//
//ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql_cliente)
//
//if ll_ret = 0 then
//	destroy lds_data
//	ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
//	if ll_ret = 0 then 
//		destroy lds_data
//		return 0
//	end if
//end if
//
//ls_testo = ""
//ls_gruppo_precedente = ""
//for ll_i = 1 to ll_ret
//	if ls_gruppo_precedente = lds_data.getitemstring(ll_i, 4) then continue
//	ls_cod_livello_prod_2 = lds_data.getitemstring(ll_i, 4)
//	
//	if isnull(lds_data.getitemstring(ll_i, 5)) then
//		ld_sconto_1 = lds_data.getitemnumber(ll_i, 7)
//		ld_sconto_2 = lds_data.getitemnumber(ll_i, 8)
//	end if
//	
//	if isnull(ls_cod_livello_prod_2) then continue
//	
//	if isnull(lds_data.getitemstring(ll_i, 4)) then
//		// vuol dire che c'è una condizione caricata sul livello 2; altrimenti mi tengo lo sconto del livello precedente.
//		ld_sconto_1 = lds_data.getitemnumber(ll_i, 7)
//		ld_sconto_2 = lds_data.getitemnumber(ll_i, 8)
//	end if
//	
//	select des_agg_livello
//	into :ls_des_livello_2
//	from tab_livelli_prod_2
//	where cod_azienda = :s_cs_xx.cod_azienda and
//			cod_livello_prod_1 = '01' and
//			cod_livello_prod_2 = :ls_cod_livello_prod_2;
//	
//	if ll_i > 0 then ls_testo += "~r~n"
//	ls_testo += ls_des_livello_2
//	if ld_sconto_1 > 0 and not isnull(ld_sconto_1) then
//		ls_testo +=  "            SCONTO " + string (ld_sconto_1, "#0") + "%"
//		if ld_sconto_2 > 0 and not isnull(ld_sconto_2) then
//			ls_testo +=  " + " + string (ld_sconto_2, "#0") + "%"
//		end if
//	end if
//	ls_gruppo_precedente = lds_data.getitemstring(ll_i, 4)
//next
//
//ls_testo += "~r~nFaranno eccezione i seguenti prodotti per i quali Vi riserveremo lo sconto speciale come sotto indicato:"
//mle_sconto_standard.text = ls_testo
//
return 0
end function

public function integer wf_testo_standard (string as_cod_cliente, ref string as_testo_linee_sconto, ref string as_valori_linee_sconto);boolean lb_cliente=false, lb_trovato=false
string 	ls_testo_linee_sconto, ls_valori_linee_sconto, ls_cod_gruppo_sconto, ls_sql, ls_sql_cliente, ls_gruppo_precedente,ls_cod_livello_prod_2,ls_des_livello_2, &
			ls_cod_livello_prod_1,ls_str
integer 	ll_ret, ll_i
dec{4}	ld_sconto_1, ld_sconto_2, ld_sconto_1_liv, ld_sconto_2_liv, ld_sc1, ld_sc2
datetime ldt_data_inizio_val
datastore lds_data

select cod_gruppo_sconto 
into :ls_cod_gruppo_sconto
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :as_cod_cliente;

choose case sqlca.sqlcode
	case 100
		g_mb.error("Cliente " + as_cod_cliente + " inesistente in anagrafica ")
		return -1
	case -1
		g_mb.error("Errore in fase di selezione del cliente.~r~n" + sqlca.sqlerrtext)
		return -1
end choose

setnull(ldt_data_inizio_val)
ls_testo_linee_sconto = ""
ls_valori_linee_sconto = ""

select 	max(data_inizio_val)
into		:ldt_data_inizio_val
from		gruppi_sconto
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_gruppo_sconto = :ls_cod_gruppo_sconto and
			cod_livello_prod_1 = '01' and
			cod_livello_prod_2 is null and
			cod_livello_prod_3 is null and
			cod_livello_prod_4 is null and
			cod_livello_prod_5 is null and
			cod_cliente = :as_cod_cliente;

if isnull(ldt_data_inizio_val) or ldt_data_inizio_val <= datetime(s_cs_xx.db_funzioni.data_neutra) then
	select 	max(data_inizio_val)
	into		:ldt_data_inizio_val
	from		gruppi_sconto
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_gruppo_sconto = :ls_cod_gruppo_sconto and
				cod_livello_prod_1 = '01' and
				cod_livello_prod_2 is null and
				cod_livello_prod_3 is null and
				cod_livello_prod_4 is null and
				cod_livello_prod_5 is null and
				cod_cliente is null;

	if isnull(ldt_data_inizio_val) or ldt_data_inizio_val <= datetime(s_cs_xx.db_funzioni.data_neutra) then
		// non c'è neanche la linea di sconto generale caricata.
		ld_sconto_1 = 0
		ld_sconto_2 = 0
	else
		select 	sconto_1, sconto_2
		into		:ld_sconto_1, :ld_sconto_2
		from		gruppi_sconto
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_gruppo_sconto = :ls_cod_gruppo_sconto and
					data_inizio_val = :ldt_data_inizio_val and
					cod_livello_prod_1 = '01' and
					cod_livello_prod_2 is null and
					cod_livello_prod_3 is null and
					cod_livello_prod_4 is null and
					cod_livello_prod_5 is null and
					cod_cliente is null;
	end if
else

	select 	sconto_1, sconto_2
	into		:ld_sconto_1, :ld_sconto_2
	from		gruppi_sconto
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_gruppo_sconto = :ls_cod_gruppo_sconto and
				data_inizio_val = :ldt_data_inizio_val and
				cod_livello_prod_1 = '01' and
				cod_livello_prod_2 is null and
				cod_livello_prod_3 is null and
				cod_livello_prod_4 is null and
				cod_livello_prod_5 is null and
				cod_cliente = :as_cod_cliente;
				
	lb_cliente = true
	// esiste una linea di sconto sul cliente

end if

ls_sql = " select cod_livello_prod_1, cod_livello_prod_2, des_agg_livello from tab_livelli_prod_2 where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_livello_prod_1 = '01'  and des_agg_livello is not null and des_agg_livello <> '' order by des_agg_livello"


ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
if ll_ret < 0 then
	g_mb.error("Errore in creazione datastore (wf_test_standard_2)")
	return -1
end if

lb_trovato = false

for ll_i = 1 to ll_ret 
	
	ls_cod_livello_prod_1 =  lds_data.getitemstring(ll_i, 1)
	ls_cod_livello_prod_2 =  lds_data.getitemstring(ll_i, 2)
	ls_des_livello_2 = lds_data.getitemstring(ll_i, 3)
	
	if lb_cliente then
		
		select max(data_inizio_val)
		into :ldt_data_inizio_val
		from gruppi_sconto
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_gruppo_sconto = :ls_cod_gruppo_sconto and
					cod_livello_prod_1 = '01' and
					cod_livello_prod_2 = :ls_cod_livello_prod_2 and
					cod_livello_prod_3 is null and
					cod_livello_prod_4 is null and
					cod_livello_prod_5 is null and
					cod_cliente = :as_cod_cliente;
					
		if isnull(ldt_data_inizio_val) or ldt_data_inizio_val <= datetime(s_cs_xx.db_funzioni.data_neutra) then
			lb_trovato=false
		else
			select sconto_1, sconto_2
			into :ld_sconto_1_liv, :ld_sconto_2_liv
			from gruppi_sconto
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_gruppo_sconto = :ls_cod_gruppo_sconto and
						cod_livello_prod_1 = '01' and
						cod_livello_prod_2 = :ls_cod_livello_prod_2 and
						cod_livello_prod_3 is null and
						cod_livello_prod_4 is null and
						cod_livello_prod_5 is null and
						cod_cliente = :as_cod_cliente and
						data_inizio_val =: ldt_data_inizio_val;
			if sqlca.sqlcode = 100 then 
				lb_trovato = false
			elseif sqlca.sqlcode = 0 then 
				lb_trovato = true
			end if
		end if
	else

		select max(data_inizio_val)
		into :ldt_data_inizio_val
		from gruppi_sconto
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_gruppo_sconto = :ls_cod_gruppo_sconto and
					cod_livello_prod_1 = '01' and
					cod_livello_prod_2 = :ls_cod_livello_prod_2 and
					cod_livello_prod_3 is null and
					cod_livello_prod_4 is null and
					cod_livello_prod_5 is null and
					cod_cliente is null;
		if isnull(ldt_data_inizio_val) or ldt_data_inizio_val <= datetime(s_cs_xx.db_funzioni.data_neutra) then
			lb_trovato=false
		else
			select sconto_1, sconto_2
			into :ld_sconto_1_liv, :ld_sconto_2_liv
			from gruppi_sconto
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_gruppo_sconto = :ls_cod_gruppo_sconto and
						cod_livello_prod_1 = '01' and
						cod_livello_prod_2 = :ls_cod_livello_prod_2 and
						cod_livello_prod_3 is null and
						cod_livello_prod_4 is null and
						cod_livello_prod_5 is null and
						cod_cliente is null and
						data_inizio_val = :ldt_data_inizio_val;
			if sqlca.sqlcode = 100 then 
				lb_trovato = false
			elseif sqlca.sqlcode = 0 then 
				lb_trovato = true
			end if
		end if
	end if
	
	if not lb_trovato then
		// vuol dire che non c'è il record allora prendo la provvigione del livello superiore 01
		ld_sc1 = ld_sconto_1
		ld_sc2 = ld_sconto_2
	else
		ld_sc1 = ld_sconto_1_liv
		ld_sc2 = ld_sconto_2_liv
	end if		
		
	if ll_i > 1 then 
		ls_testo_linee_sconto += "~r~n"
		ls_valori_linee_sconto += "~r~n"
	end if
	
	if isnull(ls_des_livello_2) then ls_des_livello_2 = ""
	ls_testo_linee_sconto += "     " + mid(ls_des_livello_2, 4)
	
	if ld_sc1 > 0 and not isnull(ld_sc1) then
		ls_valori_linee_sconto +=  "SCONTO " + string (ld_sc1, "#0") + "%"
		if ld_sc2 > 0 and not isnull(ld_sc2) then
			ls_valori_linee_sconto +=  " + " + string (ld_sc2, "#0") + "%"
		end if
	end if
/*
	if isnull(ls_des_livello_2) then ls_des_livello_2 = ""
	ls_testo += "     " + mid(ls_des_livello_2, 4)
	
	if ld_sc1 > 0 and not isnull(ld_sc1) then
		ls_testo +=  "  SCONTO " + string (ld_sc1, "#0") + "%"
		if ld_sc2 > 0 and not isnull(ld_sc2) then
			ls_testo +=  " + " + string (ld_sc2, "#0") + "%"
		end if
	end if
*/
next

as_testo_linee_sconto = ls_testo_linee_sconto
as_valori_linee_sconto = ls_valori_linee_sconto

return 0
end function

public function integer wf_offerte (string fs_cod_cliente, string fs_ultima_condizione, datetime fdt_data_da, datetime fdt_data_a, string fs_oggetto, string fs_rag_soc_1_cliente, string fs_indirizzo_cliente, string fs_cap_cliente, string fs_localita_cliente, string fs_provincia_cliente, string fs_testo_linee_sconto, string fs_valori_linee_sconto);string 		ls_sql_listini, ls_sql_condizioni, ls_cod_modelli, ls_errore,ls_sconto_std, ls_testo_linee_sconto, ls_valori_linee_sconto
datetime 	ldt_data_lettera, ldt_data_max
//datastore	lds_listini
//datastore	lds_condizioni
long			ll_i_listino, ll_tot_listini, ll_i_condiz, ll_tot_condiz, ll_new

string			ls_stringa_sconto, ls_colonna1, ls_colonna2
dec{4}		ldc_variazione, ld_sconto_confezionato, ld_prezzo_lordo, ld_prezzo_netto, ld_sconto_iniziale

string ls_cod_tipo_listino_cu, ls_cod_valuta_cu, ls_cod_prodotto_cu
string ls_prodotto_corrente, ls_cod_prodotto_figlio, ls_cod_prodotto_listino, ls_variazione_std, flag_sconto_mag_prezzo_1_cu
datetime ldt_data_inizio_val_cu, ldt_data_inizio_val_cu2
long ll_progressivo_cu

string ls_cliente_atelier

ls_cod_modelli = "MOD"

ldt_data_lettera = dw_selezione.getitemdatetime(1, "data_lettera")
if isnull(ldt_data_lettera) or year(date(ldt_data_lettera))<=1950 then ldt_data_lettera = datetime(today(), now())

//verifica se il tipo anagrafica è ATELIER ---------------------------------------------------------------------------------------------------
ls_cliente_atelier = wf_cliente_atelier(fs_cod_cliente)		//torna N o S



st_msg.text = "Elaborazione cliente "+fs_cod_cliente+" "+fs_rag_soc_1_cliente + " -->  OFFERTA TESSUTI"


ls_sql_listini = 	"select distinct "+&
						"L.cod_tipo_listino_prodotto,L.cod_valuta,L.data_inizio_val,L.progressivo,L.cod_prodotto "+&
					"from listini_vendite as L "+&
					"join anag_prodotti as P on P.cod_azienda=L.cod_azienda and P.cod_prodotto=L.cod_prodotto "+&
					"join listini_prod_comune_var_client as LPCVC on LPCVC.cod_azienda=L.cod_azienda and "+&
									"LPCVC.cod_tipo_listino_prodotto=L.cod_tipo_listino_prodotto and LPCVC.cod_valuta=L.cod_valuta and "+&
									"LPCVC.data_inizio_val=L.data_inizio_val and LPCVC.progressivo=L.progressivo "+&
					"where L.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
										"L.cod_prodotto is not null and L.cod_cliente is null and P.cod_cat_mer='"+ls_cod_modelli+"' and "+&
										"LPCVC.cod_cliente='"+fs_cod_cliente+"' "

if not isnull(fdt_data_da) and year(date(fdt_data_da))>1950 and &
			not isnull(fdt_data_a) and year(date(fdt_data_a))>1950 then

	ls_sql_listini += " and L.data_inizio_val>='" + string(fdt_data_da, s_cs_xx.db_funzioni.formato_data)+"' "+&
								" and L.data_inizio_val<='" + string(fdt_data_a, s_cs_xx.db_funzioni.formato_data)+"' "
end if

//10/03/2011 aggiungiamo che se il prodotto è bloccato lo escludiamo dalla elaborazione
ls_sql_listini += " and P.flag_blocco='N' "

ls_sql_listini += "order by L.cod_prodotto,L.data_inizio_val desc,L.progressivo asc "

declare cu_listini dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_listini;
open dynamic cu_listini;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore listini per il cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

//nota_prezzo_varianti
if wf_sconto_standard_cfz(fs_cod_cliente, ld_sconto_confezionato) < 0 then
	//messaggio già dato
	close cu_listini;
	return -1
end if

if ls_sconto_std<>"" and not isnull(ls_sconto_std) then
	ls_sconto_std = "(*) Prezzi a Voi IVA esclusa da scontare "+ls_sconto_std+" %"
else
	ls_sconto_std = "(*) Prezzi a Voi IVA esclusa da scontare"
end if

do while true
	fetch cu_listini into 
		:ls_cod_tipo_listino_cu,
		:ls_cod_valuta_cu,
		:ldt_data_inizio_val_cu,
		:ll_progressivo_cu,
		:ls_cod_prodotto_cu;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore listini per il cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
		close cu_listini;
		rollback;
		setpointer(Arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	if ls_cod_prodotto_cu=ls_prodotto_corrente then continue
	
	//ci sono dei casi in cui il prodotto nella condizione più recente non ha varianti, in tal caso la select ls_sql_listini la escluderebbe
	//e verrebbe elaborata una condizione sbagliata
	
	//Donato 10/03/2011
	//verifico se ho preso effettivamente la più grande ----------------------------------------------------------------------------------
	if not isnull(fdt_data_da) and year(date(fdt_data_da))>1950 and &
			not isnull(fdt_data_a) and year(date(fdt_data_a))>1950 then
		
		//max in un periodo
		select max(data_inizio_val)
		into :ldt_data_max
		from listini_vendite
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu and cod_cliente is null and
			data_inizio_val>=:fdt_data_da and data_inizio_val<=:fdt_data_a;
		
	else
		//flag ultima condizione
		select max(data_inizio_val)
		into :ldt_data_max
		from listini_vendite
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu and cod_cliente is null;
		
	end if
	
	if not isnull(ldt_data_max) and year(date(ldt_data_max))>1950 and ldt_data_max > ldt_data_inizio_val_cu then
		//esiste una condizione maggiore (caso di esistenza listino senza varianti che la select ls_sql_listini non estrae)
		continue
	end if
	//------------------------------------------------------
	
	
	//memorizzo il prodotto
	ls_prodotto_corrente = ls_cod_prodotto_cu
	
	//query delle condizioni
	ls_sql_condizioni =	 "select "+&
										"LPCVC.data_inizio_val,"+&
										"LPCVC.cod_prodotto_listino,"+&
										"LPCVC.cod_prodotto_figlio,"+&
										"LPCVC.flag_sconto_mag_prezzo_1,"+&
										"LPCVC.variazione_1 "+&
									"from listini_prod_comune_var_client  LPCVC "+&
									"where LPCVC.cod_azienda = '"+s_cs_xx.cod_azienda+"' and LPCVC.cod_cliente = '"+fs_cod_cliente+"' and "+&
											"LPCVC.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_cu+"' and "+&
											"LPCVC.cod_valuta='"+ls_cod_valuta_cu+"' and "+&
											"LPCVC.data_inizio_val='"+string(ldt_data_inizio_val_cu, s_cs_xx.db_funzioni.formato_data)+"' and "+&
											"LPCVC.progressivo="+string(ll_progressivo_cu)+" "+&
									"order by LPCVC.cod_prodotto_listino asc, LPCVC.cod_prodotto_figlio asc "

	declare cu_condizioni dynamic cursor for sqlsa;
	prepare sqlsa from :ls_sql_condizioni;
	open dynamic cu_condizioni;
	
	if sqlca.sqlcode <> 0 then 
		g_mb.messagebox("OMNIA","Errore nella open del cursore condizioni tessuti per il cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
		close cu_listini;
		rollback;
		setpointer(Arrow!)
		return -1
	end if
	
	do while true
		fetch cu_condizioni into
					:ldt_data_inizio_val_cu2,
					:ls_cod_prodotto_listino,
					:ls_cod_prodotto_figlio,
					:flag_sconto_mag_prezzo_1_cu,
					:ldc_variazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore nella fetch del cursore condizioni tessuti per il cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
			close cu_condizioni;
			close cu_listini;
			rollback;
			setpointer(Arrow!)
			return -1
		elseif sqlca.sqlcode = 100 then
			exit
		end if	
	
		ls_stringa_sconto = ""
	
		if ldc_variazione>0 then
		else
			continue
		end if

		ls_colonna1 = f_des_tabella("anag_prodotti","cod_prodotto='"+ls_cod_prodotto_listino+"'", "des_prodotto")
		ls_colonna2 = f_des_tabella("anag_prodotti","cod_prodotto='"+ls_cod_prodotto_figlio+"'", "des_prodotto")
		
		//############################################################
		//09/03/2011 su indicazione di Antonella dividiamo per 2 questo importo
		//successivamente ri-modificheremo questa indicazione
		//ldc_variazione = ldc_variazione / 2
		//############################################################
		ls_stringa_sconto = string(ldc_variazione, "#######0.00")
	
		//inserisci una riga ----------------------------------------------
		ll_new = dw_report.insertrow(0)
		
		dw_report.setitem(ll_new, "localita_azienda", is_localita_azienda)
		dw_report.setitem(ll_new, "rag_soc_azienda",f_des_tabella("aziende","cod_azienda='"+s_cs_xx.cod_azienda+"'", "rag_soc_1"))
		dw_report.setitem(ll_new, "oggetto", fs_oggetto)
		dw_report.setitem(ll_new, "data_lettera", ldt_data_lettera)
		
		dw_report.setitem(ll_new, "cod_cliente", fs_cod_cliente)
		dw_report.setitem(ll_new, "rag_soc_1_cliente", fs_rag_soc_1_cliente)
		dw_report.setitem(ll_new, "indirizzo_cliente", fs_indirizzo_cliente)
		dw_report.setitem(ll_new, "cap_cliente", fs_cap_cliente)
		dw_report.setitem(ll_new, "localita_cliente", fs_localita_cliente)
		dw_report.setitem(ll_new, "provincia_cliente", fs_provincia_cliente)
		
		dw_report.setitem(ll_new, "testo_standard", tab_testo_standard.tbp_1.mle_testo_standard1.text)
		dw_report.setitem(ll_new, "sconti_standard", fs_testo_linee_sconto )
		dw_report.setitem(ll_new, "sconti_standard_1", fs_valori_linee_sconto )
		dw_report.setitem(ll_new, "testo_sconti", tab_testo_standard.tbp_2.mle_sconto_standard1.text)
		
		dw_report.setitem(ll_new, "colonna1", ls_colonna1)
		dw_report.setitem(ll_new, "colonna2", ls_colonna2)
		
		//dw_report.setitem(ll_new, "sconto", ls_stringa_sconto)
		
		dw_report.setitem(ll_new, "note_dopo_sconto",tab_testo_standard.tbp_3.mle_note_dopo_sconti1.text)
		
		dw_report.setitem(ll_new, "flag_conf", "B")	//tipo riga: Offerte (A per farlo uscire alla fine ...)
		
		ls_variazione_std = ""
		if wf_offerta_standard(ls_cod_tipo_listino_cu, ls_cod_valuta_cu, ldt_data_inizio_val_cu, &
									ll_progressivo_cu, ls_cod_prodotto_figlio, ls_variazione_std) < 0 then
			//messaggio già dato
			close cu_listini;
			close cu_condizioni;
			rollback;
			return -1
		end if
		
		// prezzo netto cliente
		ld_prezzo_netto = dec(ls_stringa_sconto)
		ld_prezzo_lordo = dec(ls_variazione_std)
		
		
		// EnMe; richiesto da Beatrice 14-02-2014; non piùprezzo listino del prodotto, ma lo sconto
		//dw_report.setitem(ll_new, "sconto", ls_variazione_std)

		if ld_prezzo_lordo <> 0 and ld_sconto_confezionato <> 0 then
			ld_sconto_iniziale = ( 1 - (  (ld_prezzo_netto / ld_prezzo_lordo) * (100 / ld_sconto_confezionato) ) ) * 100
		else
			ld_sconto_iniziale = 0
		end if
		
		dw_report.setitem(ll_new, "nota_prezzo_varianti", ls_sconto_std)
		dw_report.setitem(ll_new, "flag_cliente_atelier", ls_cliente_atelier)

		dw_report.setitem(ll_new, "sconto_particolare", "(= "+ ls_stringa_sconto + " )" )
		
		if ld_sconto_iniziale > 0 then
			dw_report.setitem(ll_new, "sconto", string(ld_sconto_confezionato,"#0") + "+" + string(ld_sconto_iniziale,"#0"))
		else
			dw_report.setitem(ll_new, "sconto", string(ld_sconto_confezionato,"#0"))
		end if			

	loop
	close cu_condizioni;
	
loop
close cu_listini;


return 1
end function

public function integer wf_linee_sconto (string fs_cod_cliente, string fs_ultima_condizione, datetime fdt_data_da, datetime fdt_data_a, string fs_oggetto, string fs_rag_soc_1_cliente, string fs_indirizzo_cliente, string fs_cap_cliente, string fs_localita_cliente, string fs_provincia_cliente, string fs_testo_linee_sconto, string fs_valori_linee_sconto, string fs_cod_gruppo_sconto);boolean		lb_riga_testata=false

string 		ls_sql_livelli, ls_sql_condizioni, ls_errore, ls_esporta_sfuso,  ls_cliente_atelier, &
				ls_cod_livello_1_cu, ls_cod_livello_2_cu, ls_cod_livello_3_cu, ls_cod_livello_4_cu, ls_cod_livello_5_cu, &
				ls_flag_offerta_atelier, ls_stringa_sconto, ls_colonna1, ls_colonna2, ls_flag_sconto_particolare, ls_colonna3, &
				ls_cod_pagamento,ls_cod_lingua_cliente, ls_des_livello, ls_des_livello_lingua, ls_des_agg_livello, ls_des_agg_livello_lingua,&
				ls_des_pagamento, ls_des_pagamento_lingua

long			ll_i_livello, ll_tot_livelli, ll_i_condiz, ll_tot_condiz, ll_new, ll_cont

dec{4}		ldc_sconto, ldc_sconto_cu[]

datetime	ldt_data_lettera


dw_report.Object.DataWindow.HTMLDW = "yes"
//ls_cod_livello_sfuso = "10"
//ldc_sconto_std_sfuso = 50
ls_esporta_sfuso = dw_selezione.getitemstring(1, "flag_esporta_sfuso")

//verifica se il tipo anagrafica è ATELIER ---------------------------------------------------------------------------------------------------
ls_cliente_atelier = wf_cliente_atelier(fs_cod_cliente)		//torna N o S


ldt_data_lettera = dw_selezione.getitemdatetime(1, "data_lettera")
if isnull(ldt_data_lettera) or year(date(ldt_data_lettera))<=1950 then ldt_data_lettera = datetime(today(), now())

st_msg.text = "Elaborazione cliente "+fs_cod_cliente+" "+fs_rag_soc_1_cliente + " -->  linee sconto (CONFEZIONATO e SFUSO)"

// ------------------  inserisco in ogni caso una riga di testata -------------------------------------------
lb_riga_testata = true
ll_new = dw_report.insertrow(0)

dw_report.setitem(ll_new, "localita_azienda", is_localita_azienda)
dw_report.setitem(ll_new, "rag_soc_azienda",f_des_tabella("aziende","cod_azienda='"+s_cs_xx.cod_azienda+"'", "rag_soc_1"))
dw_report.setitem(ll_new, "oggetto", fs_oggetto)
dw_report.setitem(ll_new, "data_lettera", ldt_data_lettera)

dw_report.setitem(ll_new, "cod_cliente", fs_cod_cliente)
ls_cod_pagamento = f_des_tabella("anag_clienti","cod_cliente='"+fs_cod_cliente+"'", "cod_pagamento")
ls_cod_lingua_cliente = f_des_tabella("anag_clienti","cod_cliente='"+fs_cod_cliente+"'", "cod_lingua")

if isnull(ls_cod_lingua_cliente) then
	setnull(ls_des_pagamento_lingua)
	select A.cod_pagamento, 
			B.des_pagamento
	into	:ls_cod_pagamento,
			:ls_des_pagamento
	from	anag_clienti A
	left outer join tab_pagamenti B on A.cod_azienda = B.cod_azienda and A.cod_pagamento = B.cod_pagamento
	where A.cod_azienda = :s_cs_xx.cod_azienda and
			A.cod_cliente = :fs_cod_cliente;
else
	select A.cod_pagamento, 
			B.des_pagamento, 
			C.des_pagamento
	into	:ls_cod_pagamento,
			:ls_des_pagamento,
			:ls_des_pagamento_lingua
	from	anag_clienti A
	left outer join tab_pagamenti B on A.cod_azienda = B.cod_azienda and A.cod_pagamento = B.cod_pagamento
	left outer join tab_pagamenti_lingue C on B.cod_azienda = C.cod_azienda and B.cod_pagamento = B.cod_pagamento  and A.cod_lingua = C.cod_lingua
	where A.cod_azienda = :s_cs_xx.cod_azienda and
			A.cod_cliente = :fs_cod_cliente and
			C.cod_lingua = :ls_cod_lingua_cliente;
end if
			
if isnull(ls_des_pagamento_lingua)	then
	dw_report.setitem(ll_new, "pagamento", g_str.format("PAGAMENTO: $2 ($1)",ls_cod_pagamento, ls_des_pagamento))			
else	
	dw_report.setitem(ll_new, "pagamento", g_str.format("$2 ($1)",ls_cod_pagamento, ls_des_pagamento_lingua))			
end if
		
//dw_report.setitem(ll_new, "pagamento", "PAGAMENTO: " + ls_cod_pagamento + " - " + f_des_tabella("tab_pagamenti","cod_pagamento='"+ls_cod_pagamento+"'", "des_pagamento"))
dw_report.setitem(ll_new, "rag_soc_1_cliente", fs_rag_soc_1_cliente)
dw_report.setitem(ll_new, "indirizzo_cliente", fs_indirizzo_cliente)
dw_report.setitem(ll_new, "cap_cliente", fs_cap_cliente)
dw_report.setitem(ll_new, "localita_cliente", fs_localita_cliente)
dw_report.setitem(ll_new, "provincia_cliente", fs_provincia_cliente)

dw_report.setitem(ll_new, "testo_standard", tab_testo_standard.tbp_1.mle_testo_standard1.text)
dw_report.setitem(ll_new, "sconti_standard", fs_testo_linee_sconto)
dw_report.setitem(ll_new, "sconti_standard_1", fs_valori_linee_sconto)

if ls_flag_offerta_atelier="S" then		//in caso contrario vale il valore di default N
	dw_report.setitem(ll_new, "flag_offerta_atelier", ls_flag_offerta_atelier)
end if

dw_report.setitem(ll_new, "note_dopo_sconto", tab_testo_standard.tbp_3.mle_note_dopo_sconti1.text)
dw_report.setitem(ll_new, "flag_conf", "X")
dw_report.setitem(ll_new, "flag_cliente_atelier", ls_cliente_atelier)

dw_report.object.t_1.text =  tab_testo_standard.tbp_4.em_altro_1.text
dw_report.setitem(ll_new, "oggetto", tab_testo_standard.tbp_4.em_altro_2.text)
dw_report.object.tipo_t.text =  tab_testo_standard.tbp_4.em_altro_3.text
dw_report.object.sconto_t.text =  tab_testo_standard.tbp_4.em_altro_4.text
dw_report.object.saluti_t.text =  tab_testo_standard.tbp_4.em_altro_5.text
dw_report.object.amm_deleg_2_t.text =  tab_testo_standard.tbp_4.em_altro_6.text

// -------------------------------------------------------------------------------------------------------------------

ls_sql_livelli = "select distinct "+&
						"cod_livello_prod_1,cod_livello_prod_2,cod_livello_prod_3,cod_livello_prod_4,cod_livello_prod_5 "+&
					" from gruppi_sconto "+&
					" where cod_azienda='"+s_cs_xx.cod_azienda+"' and cod_cliente='"+fs_cod_cliente+"' and cod_gruppo_sconto = '" + fs_cod_gruppo_sconto + "' " +&
					" and flag_sconto_particolare = 'N' " + &
					" group by cod_livello_prod_1,cod_livello_prod_2,cod_livello_prod_3,cod_livello_prod_4,cod_livello_prod_5 "+&
					" order by cod_livello_prod_1,cod_livello_prod_2,cod_livello_prod_3,cod_livello_prod_4,cod_livello_prod_5 "

declare cu_livelli dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_livelli;
open dynamic cu_livelli;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("OMNIA","Errore nella open del cursore livelli. Dettaglio = " + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

do while true
	fetch cu_livelli into 
				:ls_cod_livello_1_cu,
				:ls_cod_livello_2_cu,
				:ls_cod_livello_3_cu,
				:ls_cod_livello_4_cu,
				:ls_cod_livello_5_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore nella fetch del cursore livelli. Dettaglio = " + sqlca.sqlerrtext)
		close cu_livelli;
		rollback;
		setpointer(Arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	if ls_cod_livello_1_cu="" then setnull(ls_cod_livello_1_cu)
	if ls_cod_livello_2_cu="" then setnull(ls_cod_livello_2_cu)
	if ls_cod_livello_3_cu="" then setnull(ls_cod_livello_3_cu)
	if ls_cod_livello_4_cu="" then setnull(ls_cod_livello_4_cu)
	if ls_cod_livello_5_cu="" then setnull(ls_cod_livello_5_cu)
	
	if isnull(ls_cod_livello_1_cu) then continue
	
	//se non vuoi lo sfuso saltalo
//	if ls_cod_livello_1_cu=ls_cod_livello_sfuso and ls_esporta_sfuso<>"S" then continue
	
//	if isnull(ls_cod_livello_2_cu) and ls_cod_livello_1_cu <> ls_cod_livello_sfuso then continue
	
	ls_errore = "livello1="+ls_cod_livello_1_cu+"~r~n"
	
	if not isnull(ls_cod_livello_2_cu) and ls_cod_livello_2_cu<>"" then ls_errore += " livello2="+ls_cod_livello_2_cu+"~r~n"
	
	//per questi dati considera solo la prima riga (+ recente)
	ls_sql_condizioni = 	"select sconto_1,sconto_2,sconto_3,sconto_4,sconto_5,sconto_6,sconto_7,"+&
							 	" sconto_8,sconto_9,sconto_10,flag_sconto_particolare,flag_offerta_atelier  "+&
								"from   gruppi_sconto "+&
								"where  cod_azienda='"+s_cs_xx.cod_azienda+"' "

	// verifico se ci sono linee di sconto personalizzate del cliente							
	select 	count(*)
	into 		:ll_cont
	from 		gruppi_sconto
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :fs_cod_cliente and
				cod_gruppo_sconto = :fs_cod_gruppo_sconto;
			
	
 	if isnull(ll_cont) or ll_cont = 0 then 
		ls_sql_condizioni += "and cod_cliente is null and "
	else
		ls_sql_condizioni += "and cod_cliente='"+fs_cod_cliente+"' and "
	end if
	ls_sql_condizioni +=	" flag_sconto_particolare = 'N' and " + &
								 	" cod_livello_prod_1='"+ls_cod_livello_1_cu+"' "
	
	if isnull(ls_cod_lingua_cliente) then
		ls_colonna1 = f_des_tabella("tab_livelli_prod_1","cod_livello_prod_1='"+ls_cod_livello_1_cu+"'", "des_livello")
	else
		select 	A.des_livello, B.des_livello
		into		:ls_des_livello, :ls_des_livello_lingua
		from 		tab_livelli_prod_1 A
					left outer join tab_livelli_prod_1_lingue B on A.cod_azienda = B.cod_azienda and A.cod_livello_prod_1 = B.cod_livello_prod_1
		where	A.cod_azienda = :s_cs_xx.cod_azienda  and 
					A.cod_livello_prod_1 = :ls_cod_livello_1_cu and 
					(B.cod_lingua = :ls_cod_lingua_cliente or B.cod_lingua is null)  ;
		if sqlca.sqlcode = 0 then
			if isnull(ls_des_livello_lingua) then
				ls_colonna1 = ls_des_livello
			else
				ls_colonna1 = ls_des_livello_lingua
			end if
		else
			g_mb.warning(g_str.format("Errore nella ricerca della descrizione livello $1", ls_cod_livello_1_cu))
			ls_colonna1 = ""
		end if
	end if	
	ls_colonna3 = ls_cod_livello_1_cu
	
	if not isnull(ls_cod_livello_2_cu) and ls_cod_livello_2_cu<>"" then
		ls_sql_condizioni += " and cod_livello_prod_2='"+ls_cod_livello_2_cu+"'"
		
		if isnull(ls_cod_lingua_cliente) then
			ls_colonna2 = f_des_tabella("tab_livelli_prod_2",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and cod_livello_prod_2='"+ls_cod_livello_2_cu+"'", "des_livello")
		else
			select 	A.des_livello, B.des_livello
			into		:ls_des_livello, :ls_des_livello_lingua
			from 		tab_livelli_prod_2 A
						left outer join tab_livelli_prod_2_lingue B on A.cod_azienda = B.cod_azienda and A.cod_livello_prod_1 = B.cod_livello_prod_1 and A.cod_livello_prod_2 = B.cod_livello_prod_2
			where	A.cod_azienda = :s_cs_xx.cod_azienda  and 
						A.cod_livello_prod_1 = :ls_cod_livello_1_cu and 
						A.cod_livello_prod_2 = :ls_cod_livello_2_cu and 
						(B.cod_lingua = :ls_cod_lingua_cliente or B.cod_lingua is null)  ;
			if sqlca.sqlcode = 0 then
				if isnull(ls_des_livello_lingua) then
					ls_colonna2 = ls_des_livello
				else
					ls_colonna2 = ls_des_livello_lingua
				end if
			else
				g_mb.warning(g_str.format("Errore nella ricerca della descrizione livello $1", ls_cod_livello_1_cu))
				ls_colonna1 = ""
			end if
		end if	
		ls_colonna3 = ls_cod_livello_1_cu + ls_cod_livello_2_cu
	else
		ls_sql_condizioni += " and cod_livello_prod_2 is null "
		ls_colonna2 = ""
	end if
	
	if isnull(ls_cod_livello_3_cu) then
		ls_sql_condizioni += " and cod_livello_prod_3 is null"
		ls_errore += " livello3 NULLO~r~n"
	else
		ls_sql_condizioni += " and cod_livello_prod_3='"+ls_cod_livello_3_cu+"'"
		ls_errore += " livello3="+ls_cod_livello_3_cu+"~r~n"
		
		//trasla
		ls_colonna1 = ls_colonna2
		if isnull(ls_cod_lingua_cliente) then
			ls_colonna2 = f_des_tabella("tab_livelli_prod_3",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"'", "des_livello")
			ls_colonna3 = f_des_tabella("tab_livelli_prod_3",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"'", "des_agg_livello")
		else
			select 	A.des_livello, B.des_livello, A.des_agg_livello, B.des_agg_livello
			into		:ls_des_livello, :ls_des_livello_lingua, :ls_des_agg_livello, :ls_des_agg_livello_lingua
			from 		tab_livelli_prod_3 A
						left outer join tab_livelli_prod_3_lingue B on A.cod_azienda = B.cod_azienda and A.cod_livello_prod_1 = B.cod_livello_prod_1 and A.cod_livello_prod_2 = B.cod_livello_prod_2 and A.cod_livello_prod_3 = B.cod_livello_prod_3
			where	A.cod_azienda = :s_cs_xx.cod_azienda  and 
						A.cod_livello_prod_1 = :ls_cod_livello_1_cu and 
						A.cod_livello_prod_2 = :ls_cod_livello_2_cu and 
						A.cod_livello_prod_3 = :ls_cod_livello_3_cu and 
						(B.cod_lingua = :ls_cod_lingua_cliente or B.cod_lingua is null)  ;
			if sqlca.sqlcode = 0 then
				if isnull(ls_des_livello_lingua) then
					ls_colonna2 = ls_des_livello
					ls_colonna3 = ls_des_agg_livello
				else
					ls_colonna2 = ls_des_livello_lingua
					ls_colonna3 = ls_des_agg_livello_lingua
				end if
			else
				g_mb.warning(g_str.format("Errore nella ricerca della descrizione livello $1", ls_cod_livello_1_cu))
				ls_colonna1 = ""
			end if
		end if	
			
	end if
	if isnull(ls_cod_livello_4_cu) then
		ls_sql_condizioni += " and cod_livello_prod_4 is null"
		ls_errore += " livello4 NULLO~r~n"
	else
		ls_sql_condizioni += " and cod_livello_prod_4='"+ls_cod_livello_4_cu+"'"
		ls_errore += " livello4="+ls_cod_livello_4_cu+"~r~n"
		
		//trasla
		ls_colonna1 = ls_colonna2
		
		if isnull(ls_cod_lingua_cliente) then
			ls_colonna2 = f_des_tabella("tab_livelli_prod_4",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"' and "+&
																		"cod_livello_prod_4='"+ls_cod_livello_4_cu+"'", "des_livello")
			ls_colonna3 = f_des_tabella("tab_livelli_prod_4",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"' and "+&
																		"cod_livello_prod_4='"+ls_cod_livello_4_cu+"'", "des_agg_livello")
		else
			select 	A.des_livello, B.des_livello, A.des_agg_livello, B.des_agg_livello
			into		:ls_des_livello, :ls_des_livello_lingua, :ls_des_agg_livello, :ls_des_agg_livello_lingua
			from 		tab_livelli_prod_4 A
						left outer join tab_livelli_prod_4_lingue B on A.cod_azienda = B.cod_azienda and A.cod_livello_prod_1 = B.cod_livello_prod_1 and A.cod_livello_prod_2 = B.cod_livello_prod_2 and A.cod_livello_prod_3 = B.cod_livello_prod_3 and A.cod_livello_prod_4 = B.cod_livello_prod_4
			where	A.cod_azienda = :s_cs_xx.cod_azienda  and 
						A.cod_livello_prod_1 = :ls_cod_livello_1_cu and 
						A.cod_livello_prod_2 = :ls_cod_livello_2_cu and 
						A.cod_livello_prod_3 = :ls_cod_livello_3_cu and 
						A.cod_livello_prod_4 = :ls_cod_livello_4_cu and 
						(B.cod_lingua = :ls_cod_lingua_cliente or B.cod_lingua is null)  ;
			if sqlca.sqlcode = 0 then
				if isnull(ls_des_livello_lingua) then
					ls_colonna2 = ls_des_livello
					ls_colonna3 = ls_des_agg_livello
				else
					ls_colonna2 = ls_des_livello_lingua
					ls_colonna3 = ls_des_agg_livello_lingua
				end if
			else
				g_mb.warning(g_str.format("Errore nella ricerca della descrizione livello $1", ls_cod_livello_1_cu))
				ls_colonna1 = ""
			end if
		end if	
		
	end if
	if isnull(ls_cod_livello_5_cu) then
		ls_sql_condizioni += " and cod_livello_prod_5 is null"
		ls_errore += " livello5 NULLO~r~n"
	else
		ls_sql_condizioni += " and cod_livello_prod_5='"+ls_cod_livello_5_cu+"'"
		ls_errore += " livello5="+ls_cod_livello_5_cu
		
		//trasla
		ls_colonna1 = ls_colonna2
		if isnull(ls_cod_lingua_cliente) then
			ls_colonna2 = f_des_tabella("tab_livelli_prod_5",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"' and "+&
																		"cod_livello_prod_4='"+ls_cod_livello_4_cu+"' and "+&
																		"cod_livello_prod_5='"+ls_cod_livello_5_cu+"'", "des_livello")
			ls_colonna3 = f_des_tabella("tab_livelli_prod_5",  "cod_livello_prod_1='"+ls_cod_livello_1_cu+"' and "+&
																		"cod_livello_prod_2='"+ls_cod_livello_2_cu+"' and "+&
																		"cod_livello_prod_3='"+ls_cod_livello_3_cu+"' and "+&
																		"cod_livello_prod_4='"+ls_cod_livello_4_cu+"' and "+&
																		"cod_livello_prod_5='"+ls_cod_livello_5_cu+"'", "des_agg_livello")
		else
			select 	A.des_livello, B.des_livello, A.des_agg_livello, B.des_agg_livello
			into		:ls_des_livello, :ls_des_livello_lingua, :ls_des_agg_livello, :ls_des_agg_livello_lingua
			from 		tab_livelli_prod_5 A
						left outer join tab_livelli_prod_5_lingue B on A.cod_azienda = B.cod_azienda and A.cod_livello_prod_1 = B.cod_livello_prod_1 and A.cod_livello_prod_2 = B.cod_livello_prod_2 and A.cod_livello_prod_3 = B.cod_livello_prod_3 and A.cod_livello_prod_4 = B.cod_livello_prod_4 and A.cod_livello_prod_5 = B.cod_livello_prod_5
			where	A.cod_azienda = :s_cs_xx.cod_azienda  and 
						A.cod_livello_prod_1 = :ls_cod_livello_1_cu and 
						A.cod_livello_prod_2 = :ls_cod_livello_2_cu and 
						A.cod_livello_prod_3 = :ls_cod_livello_3_cu and 
						A.cod_livello_prod_4 = :ls_cod_livello_4_cu and 
						A.cod_livello_prod_5 = :ls_cod_livello_5_cu and 
						(B.cod_lingua = :ls_cod_lingua_cliente or B.cod_lingua is null)  ;
			if sqlca.sqlcode = 0 then
				if isnull(ls_des_livello_lingua) then
					ls_colonna2 = ls_des_livello
					ls_colonna3 = ls_des_agg_livello
				else
					ls_colonna2 = ls_des_livello_lingua
					ls_colonna3 = ls_des_agg_livello_lingua
				end if
			else
				g_mb.warning(g_str.format("Errore nella ricerca della descrizione livello $1", ls_cod_livello_1_cu))
				ls_colonna1 = ""
			end if
		end if	
		
	end if
	
	if ls_colonna1= "< CODICE INESISTENTE >" or ls_colonna1= "< UNEXISTING CODE >" or &
			ls_colonna2= "< CODICE INESISTENTE >" or ls_colonna2= "< UNEXISTING CODE >" then
			
		continue
		//ls_colonna2 = ""
	end if
	
	
	if not isnull(fdt_data_da) and year(date(fdt_data_da))>1950 and &
				not isnull(fdt_data_a) and year(date(fdt_data_a))>1950 then
	
		ls_sql_condizioni += " and data_inizio_val>='" + string(fdt_data_da, s_cs_xx.db_funzioni.formato_data)+"' "+&
								   " and data_inizio_val<='" + string(fdt_data_a, s_cs_xx.db_funzioni.formato_data)+"' "
	end if
	
	//order by
	ls_sql_condizioni += " order by data_inizio_val desc "
	
	declare cu_condizioni dynamic cursor for sqlsa;
	prepare sqlsa from :ls_sql_condizioni;
	open dynamic cu_condizioni;
	
	if sqlca.sqlcode <> 0 then 
		g_mb.messagebox("OMNIA","Errore nella open del cursore condiz.cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
		close cu_livelli;
		setpointer(Arrow!)
		return -1
	end if
	
	do while true
		//in questo cursore prendi solo la prima riga ....
		fetch cu_condizioni into 
					:ldc_sconto_cu[1],:ldc_sconto_cu[2],:ldc_sconto_cu[3],:ldc_sconto_cu[4],:ldc_sconto_cu[5],
					:ldc_sconto_cu[6],:ldc_sconto_cu[7],:ldc_sconto_cu[8],:ldc_sconto_cu[9],:ldc_sconto_cu[10],
					:ls_flag_sconto_particolare, :ls_flag_offerta_atelier;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("OMNIA","Errore nella fetch del cursore condiz.cliente "+fs_cod_cliente+". Dettaglio = " + sqlca.sqlerrtext)
			close cu_livelli;
			close cu_condizioni;
			rollback;
			setpointer(Arrow!)
			return -1
		elseif sqlca.sqlcode = 100 then
			exit
		end if	
	
		ls_stringa_sconto = ""
		
		if isnull(ls_flag_offerta_atelier) or ls_flag_offerta_atelier="" then ls_flag_offerta_atelier="N"
		
		for ll_i_condiz=1 to 10
			//leggi lo sconto i-mo (1..10)
			
			if ldc_sconto_cu[ll_i_condiz]>0 and not isnull(ldc_sconto_cu[ll_i_condiz]) then
				if ll_i_condiz>1 then ls_stringa_sconto += " + "
				ls_stringa_sconto += string(ldc_sconto_cu[ll_i_condiz], "##0")
			else
				//sconto nullo
				//se sei sullo sconto 1 esci dal ciclo
				//if ll_i_condiz=1 then exit
				
				exit
			end if
		next
		
		//se non hai caricato niente nello sconto non inserire la riga
		if ls_stringa_sconto="" or isnull(ls_stringa_sconto) then continue
		
		//inserisci una riga solo se ci sono dati altrimenti restano i dati di testata per le condizioni generali
		ll_new = dw_report.insertrow(0)

		dw_report.setitem(ll_new, "localita_azienda", is_localita_azienda)
		dw_report.setitem(ll_new, "rag_soc_azienda",f_des_tabella("aziende","cod_azienda='"+s_cs_xx.cod_azienda+"'", "rag_soc_1"))
		dw_report.setitem(ll_new, "oggetto", fs_oggetto)
		dw_report.setitem(ll_new, "data_lettera", ldt_data_lettera)
		
		dw_report.setitem(ll_new, "cod_cliente", fs_cod_cliente)
		
		select A.cod_pagamento, 
				B.des_pagamento, 
				C.des_pagamento
		into	:ls_cod_pagamento,
				:ls_des_pagamento,
				:ls_des_pagamento_lingua
		from	anag_clienti A
		left outer join tab_pagamenti B on A.cod_azienda = B.cod_azienda and A.cod_pagamento = B.cod_pagamento
		left outer join tab_pagamenti_lingue C on B.cod_azienda = C.cod_azienda and B.cod_pagamento = B.cod_pagamento  and A.cod_lingua = C.cod_lingua
		where A.cod_azienda = :s_cs_xx.cod_azienda and
				A.cod_cliente = :fs_cod_cliente;
		
		if isnull(ls_des_pagamento_lingua)	then
			dw_report.setitem(ll_new, "pagamento", g_str.format("PAGAMENTO: $2 ($1)",ls_cod_pagamento, ls_des_pagamento))			
		else	
			dw_report.setitem(ll_new, "pagamento", g_str.format("$2 ($1)",ls_cod_pagamento, ls_des_pagamento))			
		end if
		
		dw_report.setitem(ll_new, "rag_soc_1_cliente", fs_rag_soc_1_cliente)
		dw_report.setitem(ll_new, "indirizzo_cliente", fs_indirizzo_cliente)
		dw_report.setitem(ll_new, "cap_cliente", fs_cap_cliente)
		dw_report.setitem(ll_new, "localita_cliente", fs_localita_cliente)
		dw_report.setitem(ll_new, "provincia_cliente", fs_provincia_cliente)
		
		dw_report.setitem(ll_new, "testo_standard", tab_testo_standard.tbp_1.mle_testo_standard1.text)
		dw_report.setitem(ll_new, "testo_sconti", tab_testo_standard.tbp_2.mle_sconto_standard1.text)
		dw_report.setitem(ll_new, "sconti_standard", fs_testo_linee_sconto)
		dw_report.setitem(ll_new, "sconti_standard_1", fs_valori_linee_sconto)

		
		// chiesto da Beatrice 14-2-2014; non deve più comparire il reparto, ma solo la categoria
		dw_report.setitem(ll_new, "colonna1", ls_colonna1)
		dw_report.setitem(ll_new, "colonna2", ls_colonna2)
		//dw_report.setitem(ll_new, "colonna1", ls_colonna2)
		//if isnull(ls_cod_livello_2_cu) and ls_cod_livello_1_cu = ls_cod_livello_sfuso then 
		//	dw_report.setitem(ll_new, "colonna1", ls_colonna1)
		//end if

		//dw_report.setitem(ll_new, "colonna2", "")
		dw_report.setitem(ll_new, "colonna3", ls_colonna3)

		if ls_flag_sconto_particolare="S" then
			dw_report.setitem(ll_new, "sconto_particolare", ls_stringa_sconto)
		else
			dw_report.setitem(ll_new, "sconto", ls_stringa_sconto)
		end if
		
		if ls_flag_offerta_atelier="S" then		//in caso contrario vale il valore di default N
			dw_report.setitem(ll_new, "flag_offerta_atelier", ls_flag_offerta_atelier)
		end if
		
		dw_report.setitem(ll_new, "note_dopo_sconto", tab_testo_standard.tbp_3.mle_note_dopo_sconti1.text)

		dw_report.object.t_1.text =  tab_testo_standard.tbp_4.em_altro_1.text
		dw_report.setitem(ll_new, "oggetto", tab_testo_standard.tbp_4.em_altro_2.text)
		dw_report.object.tipo_t.text =  tab_testo_standard.tbp_4.em_altro_3.text
		dw_report.object.sconto_t.text =  tab_testo_standard.tbp_4.em_altro_4.text
		dw_report.object.saluti_t.text =  tab_testo_standard.tbp_4.em_altro_5.text
		dw_report.object.amm_deleg_2_t.text =  tab_testo_standard.tbp_4.em_altro_6.text
		
/*
		if ls_cod_livello_1_cu=ls_cod_livello_sfuso then
			dw_report.setitem(ll_new, "flag_conf", "Z")
		else
			dw_report.setitem(ll_new, "flag_conf", "A")
		end if
*/		
		
		dw_report.setitem(ll_new, "flag_cliente_atelier", ls_cliente_atelier)
		
		exit	//dopo la prima riga esci dal cursore
	loop	
	//------------------------------------------------------------------
	close cu_condizioni;
loop

close cu_livelli;

return 1
end function

public function integer wf_sconto_standard_cfz (string fs_cod_cliente, ref decimal fd_sconto_std);string ls_sql, ls_cod_livello_1,ls_cod_gruppo_sconto
datetime ldt_data_dal, ldt_data_al
datastore lds_data
long ll_totale, ll_i_condiz, ll_cont
decimal ldc_sconto

ls_cod_livello_1 = "01"
ldt_data_dal = dw_selezione.getitemdatetime(1, "da_data")
ldt_data_al = dw_selezione.getitemdatetime(1, "a_data")

select 	cod_gruppo_sconto
into		:ls_cod_gruppo_sconto
from		anag_clienti
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :fs_cod_cliente;
			
if isnull(ls_cod_gruppo_sconto) then
	g_mb.error("Attenzione, il cliente " + fs_cod_cliente + " non ha una linea di sconto associata!" )
	return -1
end if

select 	count(*)
into 		:ll_cont
from 		gruppi_sconto
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_gruppo_sconto = :ls_cod_gruppo_sconto and
			cod_cliente = :fs_cod_cliente;
		
ls_sql = 	"select sconto_1,sconto_2,sconto_3,sconto_4,sconto_5,sconto_6,sconto_7,"+&
					"sconto_8,sconto_9,sconto_10  "+&
			"from gruppi_sconto "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and cod_gruppo_sconto ='" + ls_cod_gruppo_sconto + "' and "
if ll_cont > 0 and not isnull(ll_cont) then ls_sql += " cod_cliente='"+fs_cod_cliente+"' and "

ls_sql +=	"cod_livello_prod_1='"+ls_cod_livello_1+"' and " +&
				"cod_livello_prod_2 is null and "+&
				"cod_livello_prod_3 is null and "+&
				"cod_livello_prod_4 is null and "+&
				"cod_livello_prod_5 is null"
	
if not isnull(ldt_data_dal) and year(date(ldt_data_dal))>1950 and &
			not isnull(ldt_data_al) and year(date(ldt_data_al))>1950 then

	ls_sql += " and data_inizio_val>='" + string(ldt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' "+&
				" and data_inizio_val<='" + string(ldt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' "
end if
	
//order by
ls_sql += " order by data_inizio_val desc "
	
//crea datastore condizioni
if not f_crea_datastore(lds_data, ls_sql) then
	g_mb.error("Errore in creazione datastore sconto standard "+fs_cod_cliente)
	return -1
end if

fd_sconto_std =0
ll_totale = lds_data.retrieve()
if ll_totale>0 then
	
	for ll_i_condiz=1 to 10
		//leggi lo sconto i-mo (1..10)
		ldc_sconto = lds_data.getitemdecimal(1, ll_i_condiz)
		
		if ldc_sconto>0 and not isnull(ldc_sconto) then
			
//			if ll_i_condiz>1 then fs_sconto_std += " + "
//			fs_sconto_std += string(ldc_sconto, "##0")
			
			fd_sconto_std += ldc_sconto - (fd_sconto_std /100 * ldc_sconto)
			
		else
			//sconto nullo
			//se sei sullo sconto 1 escludi...
			if ll_i_condiz=1 then continue
		end if
	next
	
end if

return 1
end function

on w_comunica_sconti_clienti.create
int iCurrent
call super::create
this.cb_email=create cb_email
this.htb_1=create htb_1
this.st_count=create st_count
this.st_msg=create st_msg
this.cb_stampa=create cb_stampa
this.cb_esporta=create cb_esporta
this.tab_testo_standard=create tab_testo_standard
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_email
this.Control[iCurrent+2]=this.htb_1
this.Control[iCurrent+3]=this.st_count
this.Control[iCurrent+4]=this.st_msg
this.Control[iCurrent+5]=this.cb_stampa
this.Control[iCurrent+6]=this.cb_esporta
this.Control[iCurrent+7]=this.tab_testo_standard
this.Control[iCurrent+8]=this.dw_report
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_selezione
end on

on w_comunica_sconti_clienti.destroy
call super::destroy
destroy(this.cb_email)
destroy(this.htb_1)
destroy(this.st_count)
destroy(this.st_msg)
destroy(this.cb_stampa)
destroy(this.cb_esporta)
destroy(this.tab_testo_standard)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path, ls_database, ls_path_logo_1
windowobject lw_oggetti[], lw_vuoto[]
long li_risposta

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
													
dw_report.ib_dw_report = true

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
iuo_dw_main = dw_report

// *** folder													

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_esporta
lw_oggetti[3] = tab_testo_standard
lw_oggetti[4] = st_msg
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti = lw_vuoto

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
lw_oggetti[3] = htb_1
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

//da mettere sui report
select localita
into :is_localita_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

select stringa
into   :ls_path_logo_1
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
		 flag_parametro = 'S' and &
		 cod_parametro = 'LO8';	
if sqlca.sqlcode <> 0 then
	g_mb.warning("Parametro logo LO8 (instazione con LOGO aziendale) non impostato")
end if

is_path_logo_testata =  s_cs_xx.volume  + s_cs_xx.risorse + ls_path_logo_1

//logo se cliente atelier
is_path_logo_atelier =  s_cs_xx.volume + s_cs_xx.risorse + "Gibus_Atelier.jpg"

// imposto testo standard
string ls_testo
string ls_anno

ls_anno = string(year(today()))

tab_testo_standard.tbp_1.mle_testo_standard1.text = "Gentile Cliente,~r~ncon la presente Vi consegniamo i listini prezzi 2014 e relative condizioni a Voi riservate. I listini sono scaricabili in formato PDF da www.marinellotende.com/privata/prices"
tab_testo_standard.tbp_3.mle_note_dopo_sconti1.text = "I prezzi indicati a listino sono da intendersi IVA esclusa.~r~nLe presenti condizioni sono valide fino a nuova comunicazione."
tab_testo_standard.tbp_2.mle_sconto_standard1.text  = "Ricordiamo che per la prima fornitura la modalità di pagamento è : ___________________~r~nLo sconto speciale a Voi riservato è:"


// barcode per codice cliente
string ls_bfo,ls_errore
integer li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
if li_risposta < 0 then 
	g_mb.messagebox("APICE",ls_errore, stopsign!)
	return -1
end if

dw_report.Modify("cf_barcode_cliente.Font.Face='" + ls_bfo + "'")
dw_report.Modify("cf_barcode_cliente.Font.Height= -" + string(li_bco) )
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'C'")
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_deposito = 'I' ")					  
end event

type cb_email from commandbutton within w_comunica_sconti_clienti
integer x = 1554
integer y = 164
integer width = 347
integer height = 92
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Email"
end type

event clicked;dw_report.triggerevent("ue_anteprima_pdf")

end event

type htb_1 from htrackbar within w_comunica_sconti_clienti
integer x = 87
integer y = 148
integer width = 1051
integer height = 104
string pointer = "HyperLink!"
integer maxposition = 100
integer position = 100
integer tickfrequency = 10
end type

event moved;
dw_report.Object.DataWindow.Zoom = scrollpos

end event

type st_count from statictext within w_comunica_sconti_clienti
integer x = 2432
integer y = 2188
integer width = 448
integer height = 172
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_msg from statictext within w_comunica_sconti_clienti
integer x = 69
integer y = 2188
integer width = 2318
integer height = 172
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elaborazione"
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_comunica_sconti_clienti
integer x = 1179
integer y = 164
integer width = 347
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print(true, true)
end event

type cb_esporta from commandbutton within w_comunica_sconti_clienti
integer x = 2551
integer y = 2084
integer width = 347
integer height = 92
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;
setpointer(hourglass!)

if wf_elabora() < 0 then
else
	dw_folder.fu_selecttab(2)
end if

setpointer(arrow!)

dw_report.change_dw_current()
end event

type tab_testo_standard from tab within w_comunica_sconti_clienti
integer x = 50
integer y = 968
integer width = 2866
integer height = 1108
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tbp_1 tbp_1
tbp_2 tbp_2
tbp_3 tbp_3
tbp_4 tbp_4
end type

event selectionchanged;//choose case newindex
//	case 1
//		mle_testo_standard.visible = true
//		mle_sconto_standard.visible = false
//		mle_note_dopo_sconti.visible = false
//		
//	case 2
//		mle_testo_standard.visible = false
//		mle_sconto_standard.visible = true
//		mle_note_dopo_sconti.visible = false
//		
//	case 3
//		mle_testo_standard.visible = false
//		mle_sconto_standard.visible = false
//		mle_note_dopo_sconti.visible = true
//		
//end choose
end event

on tab_testo_standard.create
this.tbp_1=create tbp_1
this.tbp_2=create tbp_2
this.tbp_3=create tbp_3
this.tbp_4=create tbp_4
this.Control[]={this.tbp_1,&
this.tbp_2,&
this.tbp_3,&
this.tbp_4}
end on

on tab_testo_standard.destroy
destroy(this.tbp_1)
destroy(this.tbp_2)
destroy(this.tbp_3)
destroy(this.tbp_4)
end on

type tbp_1 from userobject within tab_testo_standard
integer x = 18
integer y = 108
integer width = 2830
integer height = 984
long backcolor = 12632256
string text = "Testo Standard"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_testo_standard1 mle_testo_standard1
end type

on tbp_1.create
this.mle_testo_standard1=create mle_testo_standard1
this.Control[]={this.mle_testo_standard1}
end on

on tbp_1.destroy
destroy(this.mle_testo_standard1)
end on

type mle_testo_standard1 from multilineedit within tbp_1
integer width = 2821
integer height = 976
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type tbp_2 from userobject within tab_testo_standard
integer x = 18
integer y = 108
integer width = 2830
integer height = 984
long backcolor = 12632256
string text = "Sconti Standard"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_sconto_standard1 mle_sconto_standard1
end type

on tbp_2.create
this.mle_sconto_standard1=create mle_sconto_standard1
this.Control[]={this.mle_sconto_standard1}
end on

on tbp_2.destroy
destroy(this.mle_sconto_standard1)
end on

type mle_sconto_standard1 from multilineedit within tbp_2
integer width = 2821
integer height = 972
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type tbp_3 from userobject within tab_testo_standard
integer x = 18
integer y = 108
integer width = 2830
integer height = 984
long backcolor = 12632256
string text = "Note dopo Sconti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_note_dopo_sconti1 mle_note_dopo_sconti1
end type

on tbp_3.create
this.mle_note_dopo_sconti1=create mle_note_dopo_sconti1
this.Control[]={this.mle_note_dopo_sconti1}
end on

on tbp_3.destroy
destroy(this.mle_note_dopo_sconti1)
end on

type mle_note_dopo_sconti1 from multilineedit within tbp_3
integer width = 2825
integer height = 976
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type tbp_4 from userobject within tab_testo_standard
integer x = 18
integer y = 108
integer width = 2830
integer height = 984
long backcolor = 12632256
string text = "Altro"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
em_altro_6 em_altro_6
em_altro_5 em_altro_5
em_altro_4 em_altro_4
em_altro_3 em_altro_3
em_altro_2 em_altro_2
em_altro_1 em_altro_1
st_altro_6 st_altro_6
st_altro_5 st_altro_5
st_altro_4 st_altro_4
st_altro_3 st_altro_3
st_altro_2 st_altro_2
st_altro_1 st_altro_1
end type

on tbp_4.create
this.em_altro_6=create em_altro_6
this.em_altro_5=create em_altro_5
this.em_altro_4=create em_altro_4
this.em_altro_3=create em_altro_3
this.em_altro_2=create em_altro_2
this.em_altro_1=create em_altro_1
this.st_altro_6=create st_altro_6
this.st_altro_5=create st_altro_5
this.st_altro_4=create st_altro_4
this.st_altro_3=create st_altro_3
this.st_altro_2=create st_altro_2
this.st_altro_1=create st_altro_1
this.Control[]={this.em_altro_6,&
this.em_altro_5,&
this.em_altro_4,&
this.em_altro_3,&
this.em_altro_2,&
this.em_altro_1,&
this.st_altro_6,&
this.st_altro_5,&
this.st_altro_4,&
this.st_altro_3,&
this.st_altro_2,&
this.st_altro_1}
end on

on tbp_4.destroy
destroy(this.em_altro_6)
destroy(this.em_altro_5)
destroy(this.em_altro_4)
destroy(this.em_altro_3)
destroy(this.em_altro_2)
destroy(this.em_altro_1)
destroy(this.st_altro_6)
destroy(this.st_altro_5)
destroy(this.st_altro_4)
destroy(this.st_altro_3)
destroy(this.st_altro_2)
destroy(this.st_altro_1)
end on

type em_altro_6 from editmask within tbp_4
integer x = 722
integer y = 612
integer width = 2085
integer height = 88
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "La Direzione"
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type em_altro_5 from editmask within tbp_4
integer x = 722
integer y = 500
integer width = 2085
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "AugurandoVi un buon lavoro, porgiamo i ns saluti."
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type em_altro_4 from editmask within tbp_4
integer x = 722
integer y = 388
integer width = 2085
integer height = 88
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "SCONTO %"
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type em_altro_3 from editmask within tbp_4
integer x = 722
integer y = 276
integer width = 2085
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "MODELLO"
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type em_altro_2 from editmask within tbp_4
integer x = 722
integer y = 164
integer width = 2085
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "OGGETTO: COMUNICAZIONE SCONTI"
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type em_altro_1 from editmask within tbp_4
integer x = 722
integer y = 52
integer width = 2085
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "SPETT.LE"
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type st_altro_6 from statictext within tbp_4
integer x = 27
integer y = 608
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Firma:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_altro_5 from statictext within tbp_4
integer x = 27
integer y = 492
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Saluti:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_altro_4 from statictext within tbp_4
integer x = 27
integer y = 384
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Instazione % Sconto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_altro_3 from statictext within tbp_4
integer x = 27
integer y = 272
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Intestazione Famiglia:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_altro_2 from statictext within tbp_4
integer x = 27
integer y = 160
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Oggetto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_altro_1 from statictext within tbp_4
integer x = 27
integer y = 48
integer width = 667
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Intestazione Cliente:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_comunica_sconti_clienti
integer x = 82
integer y = 268
integer width = 4270
integer height = 2380
integer taborder = 30
string dataobject = "d_comunica_sconti_clienti"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_comunica_sconti_clienti
integer x = 18
integer y = 12
integer width = 4370
integer height = 2692
integer taborder = 30
boolean border = false
end type

type dw_selezione from uo_cs_xx_dw within w_comunica_sconti_clienti
integer x = 37
integer y = 112
integer width = 2619
integer height = 828
integer taborder = 20
string dataobject = "d_comunica_sconti_clienti_sel"
boolean border = false
end type

event itemchanged;call super::itemchanged;datetime ldt_null
string ls_null

setnull(ldt_null)
setnull(ls_null)

choose case dwo.name
		
	case "cod_cliente_da"
		if data <> "" and not isnull(data) then
			setitem(row, "cod_cliente_a", data)
		end if
		
	case "flag_estrai_ultima"
		if data = "S" then
			//disabilita i campi da data a data validita
			setitem(row,"da_data", ldt_null)
			setitem(row,"a_data", ldt_null)
			
			dw_selezione.object.da_data.protect = 1
			dw_selezione.object.a_data.protect = 1
		else
			//abilita i campi da data a data validita
			dw_selezione.object.da_data.protect = 0
			dw_selezione.object.a_data.protect = 0
		end if
		
	case "da_data", "a_data"
		if data<>"" and not isnull(data) then
			setitem(row, "flag_estrai_ultima", "N")
		end if
		
end choose


end event

event buttonclicked;call super::buttonclicked;if row >0 then
	
//	choose case dwo.name
//		case "b_cod_cliente_da", "b_cod_cliente_a"
//			
//			if dwo.name = "b_cod_cliente_da" then
//				//da cliente
//				s_cs_xx.parametri.parametro_s_1 = "cod_cliente_da"
//			else
//				//a cliente
//				s_cs_xx.parametri.parametro_s_1 = "cod_cliente_a"
//			end if
//		
//			dw_selezione.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			if not isvalid(w_clienti_ricerca) then
//				window_open(w_clienti_ricerca, 0)
//			end if
//			w_clienti_ricerca.show()


choose case dwo.name
	case "b_cod_cliente_da"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente_da")
	case "b_cod_cliente_a"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente_a")
end choose
	
end if
end event

