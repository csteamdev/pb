﻿$PBExportHeader$w_det_list_ven.srw
forward
global type w_det_list_ven from w_cs_xx_principale
end type
type cb_cancella from commandbutton within w_det_list_ven
end type
type st_log from statictext within w_det_list_ven
end type
type cb_importa from commandbutton within w_det_list_ven
end type
type dw_lista from uo_cs_xx_dw within w_det_list_ven
end type
end forward

global type w_det_list_ven from w_cs_xx_principale
integer width = 3086
integer height = 1736
cb_cancella cb_cancella
st_log st_log
cb_importa cb_importa
dw_lista dw_lista
end type
global w_det_list_ven w_det_list_ven

type variables
constant string END_TAG = "#FINE#"

private:
	string is_desktop
	string is_cod_prodotto_listino
	long il_cod_versione
	
end variables

forward prototypes
public function integer wf_importa ()
end prototypes

public function integer wf_importa ();/**
 * stefanop
 * 12/06/2012
 *
 * Importo i dati dal foglio excel
 **/
 
string ls_file, ls_filename[], ls_cod_prodotto_vendita
long ll_row_excel, ll_num_prog_posizione, ll_num_posizione, ll_row
boolean lb_fire_event
uo_excel luo_excel

if GetFileOpenName("Select File", ls_file, ls_filename[], "", "Excel,*.xls;*.xlsx", is_desktop) < 1 then
	return 0
end if

luo_excel = create uo_excel

luo_excel.uof_open(ls_file, false, true)
ll_row_excel = 5

lb_fire_event = true
dw_lista.setredraw(false)

do while true
	
	st_log.text = "Lettura riga " + string(ll_row_excel)
	yield()
	
	ll_num_prog_posizione = luo_excel.uof_read_long(ll_row_excel, "A")
	ll_num_posizione = luo_excel.uof_read_long(ll_row_excel, "B")
	ls_cod_prodotto_vendita = luo_excel.uof_read_string(ll_row_excel, "D")
		
	// Se il prodotto è vuoto salto altrimenti salto se anche gli altri due valori sono vuoti
	if ls_cod_prodotto_vendita = END_TAG then
		exit
	elseif isnull(ls_cod_prodotto_vendita) or trim(ls_cod_prodotto_vendita) = "" or isnull(ll_num_posizione) or ll_num_posizione < 0 then
		ll_row_excel ++
		continue
	end if
	
	if lb_fire_event then
		dw_lista.triggerevent("pcd_new")
		ll_row = dw_lista.getrow()
		lb_fire_event = false
	else
		ll_row = dw_lista.insertrow(0)
	end if
	
	dw_lista.setitem(ll_row, "cod_prodotto_vendita", ls_cod_prodotto_vendita)
	dw_lista.setitem(ll_row, "num_prog_posizione", ll_num_prog_posizione)
	dw_lista.setitem(ll_row, "num_posizione", ll_num_posizione)
	
	ll_row_excel ++
loop

destroy luo_excel
dw_lista.setredraw(true)
st_log.text = "-"

return 1
end function

on w_det_list_ven.create
int iCurrent
call super::create
this.cb_cancella=create cb_cancella
this.st_log=create st_log
this.cb_importa=create cb_importa
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cancella
this.Control[iCurrent+2]=this.st_log
this.Control[iCurrent+3]=this.cb_importa
this.Control[iCurrent+4]=this.dw_lista
end on

on w_det_list_ven.destroy
call super::destroy
destroy(this.cb_cancella)
destroy(this.st_log)
destroy(this.cb_importa)
destroy(this.dw_lista)
end on

event pc_setsize;call super::pc_setsize;dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_prodotto_listino")
dw_lista.set_dw_key("cod_versione")
dw_lista.set_dw_key("prog_list_ven")
dw_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)

iuo_dw_main = dw_lista


is_desktop = guo_functions.uof_get_user_desktop_folder()
end event

type cb_cancella from commandbutton within w_det_list_ven
integer x = 2053
integer y = 1520
integer width = 471
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancella Tutto"
end type

event clicked;if g_mb.confirm("Cancellare tutte le righe?") then
	
	dw_lista.setredraw(false)
	do while dw_lista.getrow() > 0
		dw_lista.deleterow(1)
	loop
	dw_lista.setredraw(true)
	
	parent.triggerevent("pc_save")
	
end if
end event

type st_log from statictext within w_det_list_ven
integer x = 23
integer y = 1540
integer width = 1897
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "-"
boolean focusrectangle = false
end type

type cb_importa from commandbutton within w_det_list_ven
integer x = 2546
integer y = 1520
integer width = 471
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;wf_importa()
end event

type dw_lista from uo_cs_xx_dw within w_det_list_ven
integer x = 23
integer y = 20
integer width = 2994
integer height = 1480
integer taborder = 10
string dataobject = "d_det_list_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
boolean i_allowcontrolfirst = false
end type

event pcd_retrieve;call super::pcd_retrieve;is_cod_prodotto_listino = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_prodotto_listino")
il_cod_versione = i_parentdw.getitemnumber(i_parentdw.getrow(), "cod_versione")

parent.title = "Dettaglio prodotto: " + is_cod_prodotto_listino + " - Versione: " + string(il_cod_versione)

if retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_listino, il_cod_versione) < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i,ll_prog_list_ven

setnull(ll_prog_list_ven)

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_prodotto_listino")) then
		this.setitem(ll_i, "cod_prodotto_listino", is_cod_prodotto_listino)
	end if
	
	if isnull(this.getitemnumber(ll_i, "cod_versione")) then
		this.setitem(ll_i, "cod_versione", il_cod_versione)
	end if
		
	if isnull(this.getitemnumber(ll_i, "prog_list_ven")) then
		
		if isnull(ll_prog_list_ven) or ll_prog_list_ven < 0 then
			
			select max(prog_list_ven)
			into :ll_prog_list_ven
			from det_list_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto_listino = :is_cod_prodotto_listino and
					 cod_versione = :il_cod_versione;
					 
			if isnull(ll_prog_list_ven) or ll_prog_list_ven < 0 then
				ll_prog_list_ven = 0
			end if
			
			ll_prog_list_ven++
		else
			// secondo codice da inserire, NON fare la query altrimenti calcola sempre lo stesso progressivo!!
			ll_prog_list_ven ++
		end if
		
		this.setitem(ll_i, "prog_list_ven", ll_prog_list_ven)
	end if
		
next
end event

