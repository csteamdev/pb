﻿$PBExportHeader$w_export_import_listini_produzione.srw
$PBExportComments$Esportazione ed Importazione tramite file di testo dei listini di produzione (fatto per ptenda)
forward
global type w_export_import_listini_produzione from w_std_principale
end type
type st_11 from statictext within w_export_import_listini_produzione
end type
type st_10 from statictext within w_export_import_listini_produzione
end type
type st_9 from statictext within w_export_import_listini_produzione
end type
type st_8 from statictext within w_export_import_listini_produzione
end type
type st_7 from statictext within w_export_import_listini_produzione
end type
type st_6 from statictext within w_export_import_listini_produzione
end type
type st_5 from statictext within w_export_import_listini_produzione
end type
type st_4 from statictext within w_export_import_listini_produzione
end type
type st_3 from statictext within w_export_import_listini_produzione
end type
type cb_2 from cb_prod_ricerca within w_export_import_listini_produzione
end type
type dw_import from datawindow within w_export_import_listini_produzione
end type
type st_2 from statictext within w_export_import_listini_produzione
end type
type cb_3 from cb_prod_ricerca within w_export_import_listini_produzione
end type
type st_1 from statictext within w_export_import_listini_produzione
end type
type dw_1 from datawindow within w_export_import_listini_produzione
end type
type em_path_errors from editmask within w_export_import_listini_produzione
end type
type pb_path_errors from picturebutton within w_export_import_listini_produzione
end type
type shl_3 from statichyperlink within w_export_import_listini_produzione
end type
type shl_2 from statichyperlink within w_export_import_listini_produzione
end type
type shl_1 from statichyperlink within w_export_import_listini_produzione
end type
type cb_1 from commandbutton within w_export_import_listini_produzione
end type
type pb_path_import from picturebutton within w_export_import_listini_produzione
end type
type pb_path_export from picturebutton within w_export_import_listini_produzione
end type
type em_path_import from editmask within w_export_import_listini_produzione
end type
type em_path_export from editmask within w_export_import_listini_produzione
end type
type cb_export from commandbutton within w_export_import_listini_produzione
end type
end forward

global type w_export_import_listini_produzione from w_std_principale
integer width = 4128
integer height = 2396
string title = "Export / Import Listini Produzione"
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
cb_2 cb_2
dw_import dw_import
st_2 st_2
cb_3 cb_3
st_1 st_1
dw_1 dw_1
em_path_errors em_path_errors
pb_path_errors pb_path_errors
shl_3 shl_3
shl_2 shl_2
shl_1 shl_1
cb_1 cb_1
pb_path_import pb_path_import
pb_path_export pb_path_export
em_path_import em_path_import
em_path_export em_path_export
cb_export cb_export
end type
global w_export_import_listini_produzione w_export_import_listini_produzione

forward prototypes
public function string wf_null (string fs_string)
end prototypes

public function string wf_null (string fs_string);if isnull(fs_string) then
	return ""
else
	return fs_string
end if
end function

on w_export_import_listini_produzione.create
int iCurrent
call super::create
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.cb_2=create cb_2
this.dw_import=create dw_import
this.st_2=create st_2
this.cb_3=create cb_3
this.st_1=create st_1
this.dw_1=create dw_1
this.em_path_errors=create em_path_errors
this.pb_path_errors=create pb_path_errors
this.shl_3=create shl_3
this.shl_2=create shl_2
this.shl_1=create shl_1
this.cb_1=create cb_1
this.pb_path_import=create pb_path_import
this.pb_path_export=create pb_path_export
this.em_path_import=create em_path_import
this.em_path_export=create em_path_export
this.cb_export=create cb_export
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_11
this.Control[iCurrent+2]=this.st_10
this.Control[iCurrent+3]=this.st_9
this.Control[iCurrent+4]=this.st_8
this.Control[iCurrent+5]=this.st_7
this.Control[iCurrent+6]=this.st_6
this.Control[iCurrent+7]=this.st_5
this.Control[iCurrent+8]=this.st_4
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.cb_2
this.Control[iCurrent+11]=this.dw_import
this.Control[iCurrent+12]=this.st_2
this.Control[iCurrent+13]=this.cb_3
this.Control[iCurrent+14]=this.st_1
this.Control[iCurrent+15]=this.dw_1
this.Control[iCurrent+16]=this.em_path_errors
this.Control[iCurrent+17]=this.pb_path_errors
this.Control[iCurrent+18]=this.shl_3
this.Control[iCurrent+19]=this.shl_2
this.Control[iCurrent+20]=this.shl_1
this.Control[iCurrent+21]=this.cb_1
this.Control[iCurrent+22]=this.pb_path_import
this.Control[iCurrent+23]=this.pb_path_export
this.Control[iCurrent+24]=this.em_path_import
this.Control[iCurrent+25]=this.em_path_export
this.Control[iCurrent+26]=this.cb_export
end on

on w_export_import_listini_produzione.destroy
call super::destroy
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.cb_2)
destroy(this.dw_import)
destroy(this.st_2)
destroy(this.cb_3)
destroy(this.st_1)
destroy(this.dw_1)
destroy(this.em_path_errors)
destroy(this.pb_path_errors)
destroy(this.shl_3)
destroy(this.shl_2)
destroy(this.shl_1)
destroy(this.cb_1)
destroy(this.pb_path_import)
destroy(this.pb_path_export)
destroy(this.em_path_import)
destroy(this.em_path_export)
destroy(this.cb_export)
end on

event open;call super::open;string ls_flag

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CGH';
if sqlca.sqlcode = 0 and ls_flag = "N" then
	dw_1.object.flag_tipo_listino.protect = '0'
else
	dw_1.object.flag_tipo_listino.protect = '1'
end if


dw_1.insertrow(0)

end event

type st_11 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 1160
integer width = 1193
integer height = 140
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 16711680
long backcolor = 12632256
string text = "PRESTARE LA MASSIMA ATTENZIONE alla versione della distinta base."
boolean focusrectangle = false
end type

type st_10 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 964
integer width = 1193
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Verificare che le righe MODIFCATE abbiano tutte le colonne compilate incluso il progressivo."
boolean focusrectangle = false
end type

type st_9 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 712
integer width = 1193
integer height = 220
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Verificare che le righe NUOVE abbiano tutte le colonne compilate escluso il progressivo che viene calcolato automaticamente."
boolean focusrectangle = false
end type

type st_8 from statictext within w_export_import_listini_produzione
integer x = 3035
integer y = 576
integer width = 901
integer height = 72
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "C per cancellare la condizione"
boolean focusrectangle = false
end type

type st_7 from statictext within w_export_import_listini_produzione
integer x = 3035
integer y = 492
integer width = 901
integer height = 72
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "N per indicare una nuova condizione"
boolean focusrectangle = false
end type

type st_6 from statictext within w_export_import_listini_produzione
integer x = 3035
integer y = 416
integer width = 901
integer height = 76
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "M per modificare la condizione di listino"
boolean focusrectangle = false
end type

type st_5 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 336
integer width = 1211
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nella colonna A inserire il seguente carattere:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 156
integer width = 1179
integer height = 136
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Modificando le righe e importandolo i listini vengono corretti automaticamente."
boolean focusrectangle = false
end type

type st_3 from statictext within w_export_import_listini_produzione
integer x = 2885
integer y = 20
integer width = 1179
integer height = 136
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Viene generato un file EXCEL con tutti i dati dei listini di produzione."
boolean focusrectangle = false
end type

type cb_2 from cb_prod_ricerca within w_export_import_listini_produzione
integer x = 2418
integer y = 220
integer width = 73
integer height = 76
integer taborder = 20
boolean flatstyle = true
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_dw_1 = dw_1
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_fine"
s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

type dw_import from datawindow within w_export_import_listini_produzione
integer x = 27
integer y = 1372
integer width = 4041
integer height = 896
integer taborder = 30
string title = "none"
string dataobject = "d_export_import_listini_prod_ext"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_export_import_listini_produzione
integer x = 73
integer y = 1220
integer width = 2309
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_3 from cb_prod_ricerca within w_export_import_listini_produzione
integer x = 2418
integer y = 140
integer width = 73
integer height = 76
integer taborder = 30
boolean flatstyle = true
end type

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_dw_1 = dw_1
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_inizio"
s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

type st_1 from statictext within w_export_import_listini_produzione
integer x = 78
integer y = 716
integer width = 2309
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_export_import_listini_produzione
integer x = 23
integer y = 20
integer width = 2706
integer height = 504
integer taborder = 10
string title = "none"
string dataobject = "d_export_import_listini_produzione"
boolean border = false
boolean livescroll = true
end type

type em_path_errors from editmask within w_export_import_listini_produzione
integer x = 69
integer y = 1124
integer width = 2674
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type pb_path_errors from picturebutton within w_export_import_listini_produzione
integer x = 2743
integer y = 1124
integer width = 101
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean flatstyle = true
string picturename = "C:\cs_105\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string ls_path, ls_file

int li_rc


ls_path = em_path_errors.Text

li_rc = GetFileSaveName ( "File LOG Errori",  ls_path, ls_file, "Txt", "File Txt (*.Txt),*.Txt" , "C:\temp")

em_path_errors.Text = ls_path
end event

type shl_3 from statichyperlink within w_export_import_listini_produzione
integer x = 69
integer y = 1064
integer width = 658
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
string pointer = "HyperLink!"
long textcolor = 134217856
long backcolor = 12632256
string text = "Percorso File Log Errori."
boolean focusrectangle = false
end type

type shl_2 from statichyperlink within w_export_import_listini_produzione
integer x = 69
integer y = 924
integer width = 658
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
string pointer = "HyperLink!"
long textcolor = 134217856
long backcolor = 12632256
string text = "Percorso File di Importazione."
boolean focusrectangle = false
end type

type shl_1 from statichyperlink within w_export_import_listini_produzione
integer x = 69
integer y = 564
integer width = 654
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
string pointer = "HyperLink!"
long textcolor = 134217856
long backcolor = 12632256
string text = "Percorso File di Esportazione."
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_export_import_listini_produzione
integer x = 2400
integer y = 1224
integer width = 343
integer height = 80
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Importa"
boolean flatstyle = true
end type

event clicked;string ls_sql, ls_flag_tipo_listino, ls_file, ls_str, ls_tab, ls_des, ls_cod_prodotto_selezione, ls_file_errors, ls_azione, &
		ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_listino, ls_cod_versione, ls_cod_prodotto_padre, &
		ls_cod_prodotto_figlio, ls_cod_gruppo_variante, ls_cod_prodotto, ls_des_listino_produzione, &
		ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, ls_flag_sconto_mag_prezzo_1, ls_flag_sconto_mag_prezzo_2, &
		ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4, ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1,&
		ls_flag_origine_prezzo_2, ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5, &
		ls_tabella

long  ll_file,ll_progressivo, ll_num_sequenza, ll_ret,ll_progr, ll_prog_listino_produzione, ll_cont,ll_rc, ll_i,&
      ll_cont_comune, ll_cont_locale

decimal  ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, ld_provvigione_1, ld_provvigione_2

datetime ldt_inizio, ldt_fine,ldt_data_inizio_val



ls_file 			= em_path_import.text
ls_file_errors = em_path_errors.text

if len(trim(ls_file)) < 1 then
	g_mb.messagebox("APICE","E' obbligatorio specificare file di import")
	return
end if
	
if len(trim(ls_file_errors)) < 1 then
	g_mb.messagebox("APICE","E' obbligatorio specificare file di LOG degli errori")
	return
end if

if not fileexists(ls_file) then
	g_mb.messagebox("APICE","Il file di import non esiste!")
	return
end if

if fileexists(ls_file_errors) then
	if g_mb.messagebox("APICE","Il file esiste già; lo sostituisco ?", Question!, YesNo!, 2) = 2 then 
		return
	end if
	
	if not filedelete(ls_file_errors) then
		g_mb.messagebox("APICE","Impossibile cancellare il file~r~n" + ls_file)
		return
	end if
end if
	
ll_file = fileopen(ls_file, linemode!)
if ll_file < 1 then
	g_mb.messagebox("APICE","Errore in apertura del file~r~n" + ls_file)
	return
end if
	
for ll_i = 1 to dw_import.rowcount()
	
	ls_azione = dw_import.getitemstring(ll_i,1)
	
	if UPPER(ls_azione) = "AZIONE" then continue
	
	ls_cod_tipo_listino_prodotto = dw_import.getitemstring(ll_i,2)
	ls_cod_valuta = dw_import.getitemstring(ll_i,3)
	ldt_data_inizio_val = datetime( date(dw_import.getitemstring(ll_i,4)), 00:00:00)
	ll_progressivo = long(dw_import.getitemstring(ll_i,5))
	ls_cod_prodotto_listino = dw_import.getitemstring(ll_i,6)
	ls_cod_versione = dw_import.getitemstring(ll_i,8)
	ll_prog_listino_produzione = long(dw_import.getitemstring(ll_i,10))
	
	// dati non in PK
	ls_cod_prodotto_padre = dw_import.getitemstring(ll_i,11)
	ls_cod_prodotto_figlio = dw_import.getitemstring(ll_i,13)
	ls_flag_sconto_a_parte = dw_import.getitemstring(ll_i,15)
	ls_flag_tipo_scaglioni = dw_import.getitemstring(ll_i,16)
	
	ld_scaglione_1 = round(dec( dw_import.getitemstring(ll_i,17) ),4)
	ls_flag_sconto_mag_prezzo_1 = dw_import.getitemstring(ll_i,18)
	ld_variazione_1 = round(dec( dw_import.getitemstring(ll_i,19) ),4)
	ls_flag_origine_prezzo_1 = dw_import.getitemstring(ll_i,20)
	
	ld_scaglione_2 = round(dec( dw_import.getitemstring(ll_i,21) ),4)
	ls_flag_sconto_mag_prezzo_2 = dw_import.getitemstring(ll_i,22)
	ld_variazione_2 = round(dec( dw_import.getitemstring(ll_i,23) ),4)
	ls_flag_origine_prezzo_2 = dw_import.getitemstring(ll_i,24)
	
	ld_scaglione_3 = round(dec( dw_import.getitemstring(ll_i,25) ),4)
	ls_flag_sconto_mag_prezzo_3 = dw_import.getitemstring(ll_i,26)
	ld_variazione_3 = round(dec( dw_import.getitemstring(ll_i,27) ),4)
	ls_flag_origine_prezzo_3 = dw_import.getitemstring(ll_i,28)
	
	ld_scaglione_4 = round(dec( dw_import.getitemstring(ll_i,29) ),4)
	ls_flag_sconto_mag_prezzo_4 = dw_import.getitemstring(ll_i,30)
	ld_variazione_4 = round(dec( dw_import.getitemstring(ll_i,31) ),4)
	ls_flag_origine_prezzo_4 = dw_import.getitemstring(ll_i,32)
	
	ld_scaglione_5 = round(dec( dw_import.getitemstring(ll_i,33) ),4)
	ls_flag_sconto_mag_prezzo_5 = dw_import.getitemstring(ll_i,34)
	ld_variazione_5 = round(dec( dw_import.getitemstring(ll_i,35) ),4)
	ls_flag_origine_prezzo_5 = dw_import.getitemstring(ll_i,36)
	
	ld_provvigione_1 = round(dec( dw_import.getitemstring(ll_i,37) ),4)
	ld_provvigione_2 = round(dec( dw_import.getitemstring(ll_i,38) ),4)
	
	select count(*) 
	into   :ll_cont_comune
	from  listini_prod_comune
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo and 
			cod_prodotto_listino = :ls_cod_prodotto_listino and 
			cod_versione = :ls_cod_versione and 
			prog_listino_produzione = :ll_prog_listino_produzione;
			
	select count(*) 
	into   :ll_cont_locale
	from  listini_prod_locale
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo and 
			cod_prodotto_listino = :ls_cod_prodotto_listino and 
			cod_versione = :ls_cod_versione and 
			prog_listino_produzione = :ll_prog_listino_produzione;
			
	choose case upper(ls_azione) 
		case "C"		// cancellazione
			
			// condizione non trovata
			ll_cont = ll_cont_comune + ll_cont_locale
			if ll_cont < 1 then
				dw_import.setitem(ll_i, "errore", "Condizione non trovata")
				continue
			end if
			
			if ll_cont_comune > 0 and not isnull(ll_cont_comune) then
			
				delete listini_prod_comune
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
						cod_valuta = :ls_cod_valuta and 
						data_inizio_val = :ldt_data_inizio_val and 
						progressivo = :ll_progressivo and 
						cod_prodotto_listino = :ls_cod_prodotto_listino and 
						cod_versione = :ls_cod_versione and 
						prog_listino_produzione = :ll_prog_listino_produzione;
			elseif ll_cont_locale > 0 and not isnull(ll_cont_locale) then
			
				delete listini_prod_comune
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
						cod_valuta = :ls_cod_valuta and 
						data_inizio_val = :ldt_data_inizio_val and 
						progressivo = :ll_progressivo and 
						cod_prodotto_listino = :ls_cod_prodotto_listino and 
						cod_versione = :ls_cod_versione and 
						prog_listino_produzione = :ll_prog_listino_produzione;
				
			end if
			if sqlca.sqlcode < 0 then
				dw_import.setitem(ll_i, "errore", "ERRORE IN CANCELLAZIONE."+sqlca.sqlerrtext)
				continue
			end if	
			
			
		case "M" 	// modifica
			// condizione non trovata
			ll_cont = ll_cont_comune + ll_cont_locale
			if ll_cont < 1 then
				dw_import.setitem(ll_i, "errore", "Condizione non trovata")
				continue
			end if
			
			if ll_cont_comune > 0 and not isnull(ll_cont_comune) then
			
				update listini_prod_comune
				set   cod_prodotto_padre = :ls_cod_prodotto_padre,
						cod_prodotto_figlio = :ls_cod_prodotto_figlio,
						flag_sconto_a_parte = :ls_flag_sconto_a_parte,
						flag_tipo_scaglioni = :ls_flag_tipo_scaglioni,
						scaglione_1 = :ld_scaglione_1,
						flag_sconto_mag_prezzo_1 = :ls_flag_sconto_mag_prezzo_1,
						variazione_1 = :ld_variazione_1,
						flag_origine_prezzo_1 =:ls_flag_origine_prezzo_1,
						scaglione_2 = :ld_scaglione_2,
						flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_2,
						variazione_2 = :ld_variazione_2,
						flag_origine_prezzo_2 = :ls_flag_origine_prezzo_2,
						scaglione_3 = :ld_scaglione_3,
						flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_3,
						variazione_3 = :ld_variazione_3,
						flag_origine_prezzo_3 = :ls_flag_origine_prezzo_3,
						scaglione_4  = :ld_scaglione_4 ,
						flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_4,
						variazione_4  = :ld_variazione_4 ,
						flag_origine_prezzo_4  = :ls_flag_origine_prezzo_4 ,
						scaglione_5 = :ld_scaglione_5 ,
						flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_5 ,
						variazione_5 = :ld_variazione_5 ,
						flag_origine_prezzo_5 = :ls_flag_origine_prezzo_5 ,
						provvigione_1 = :ld_provvigione_1 ,
						provvigione_2  = :ld_provvigione_2 
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
						cod_valuta = :ls_cod_valuta and 
						data_inizio_val = :ldt_data_inizio_val and 
						progressivo = :ll_progressivo and 
						cod_prodotto_listino = :ls_cod_prodotto_listino and 
						cod_versione = :ls_cod_versione and 
						prog_listino_produzione = :ll_prog_listino_produzione;
						
						
			elseif ll_cont_locale > 0 and not isnull(ll_cont_locale) then
			
				update listini_prod_comune
				set   cod_prodotto_padre = :ls_cod_prodotto_padre,
						cod_prodotto_figlio = :ls_cod_prodotto_figlio,
						flag_sconto_a_parte = :ls_flag_sconto_a_parte,
						flag_tipo_scaglioni = :ls_flag_tipo_scaglioni,
						scaglione_1 = :ld_scaglione_1,
						flag_sconto_mag_prezzo_1 = :ls_flag_sconto_mag_prezzo_1,
						variazione_1 = :ld_variazione_1,
						flag_origine_prezzo_1 =:ls_flag_origine_prezzo_1,
						scaglione_2 = :ld_scaglione_2,
						flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_2,
						variazione_2 = :ld_variazione_2,
						flag_origine_prezzo_2 = :ls_flag_origine_prezzo_2,
						scaglione_3 = :ld_scaglione_3,
						flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_3,
						variazione_3 = :ld_variazione_3,
						flag_origine_prezzo_3 = :ls_flag_origine_prezzo_3,
						scaglione_4  = :ld_scaglione_4 ,
						flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_4,
						variazione_4  = :ld_variazione_4 ,
						flag_origine_prezzo_4  = :ls_flag_origine_prezzo_4 ,
						scaglione_5 = :ld_scaglione_5 ,
						flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_5 ,
						variazione_5 = :ld_variazione_5 ,
						flag_origine_prezzo_5 = :ls_flag_origine_prezzo_5 ,
						provvigione_1 = :ld_provvigione_1 ,
						provvigione_2  = :ld_provvigione_2 
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
						cod_valuta = :ls_cod_valuta and 
						data_inizio_val = :ldt_data_inizio_val and 
						progressivo = :ll_progressivo and 
						cod_prodotto_listino = :ls_cod_prodotto_listino and 
						cod_versione = :ls_cod_versione and 
						prog_listino_produzione = :ll_prog_listino_produzione;
						
			else
				dw_import.setitem(ll_i, "errore", "L'aggiornamento della nuova condizione non è potuta avvenire inquanto non esiste il listino generale di riferimento.")
				continue
			end if
			
			if sqlca.sqlcode < 0 then
				dw_import.setitem(ll_i, "errore", "ERRORE IN MODIFICA."+sqlca.sqlerrtext)
				continue
			end if	
			
		case "N" 	// Nuovo inserimento
			
			select count(*) 
			into   :ll_cont_comune
			from  listini_ven_comune
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
					cod_valuta = :ls_cod_valuta and 
					data_inizio_val = :ldt_data_inizio_val and 
					progressivo = :ll_progressivo ;
					
			select count(*) 
			into   :ll_cont_locale
			from  listini_ven_locale
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
					cod_valuta = :ls_cod_valuta and 
					data_inizio_val = :ldt_data_inizio_val and 
					progressivo = :ll_progressivo ;
					

			select max(prog_listino_produzione)
			into   :ll_prog_listino_produzione
			from   listini_produzione
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
					cod_valuta = :ls_cod_valuta and 
					data_inizio_val = :ldt_data_inizio_val and 
					progressivo = :ll_progressivo and 
					cod_prodotto_listino = :ls_cod_prodotto_listino and 
					cod_versione = :ls_cod_versione ;
			if isnull(ll_prog_listino_produzione) or ll_prog_listino_produzione < 1 then
				ll_prog_listino_produzione = 1
			else
				ll_prog_listino_produzione ++
			end if
			
			if ll_cont_comune > 0 and not isnull(ll_cont_comune) then
				
				insert into listini_prod_comune
						(cod_azienda,
						cod_tipo_listino_prodotto,
						cod_valuta,
						data_inizio_val ,
						progressivo ,
						cod_prodotto_listino,
						cod_versione ,
						prog_listino_produzione ,
						cod_prodotto_padre,
						cod_prodotto_figlio ,
						flag_sconto_a_parte ,
						flag_tipo_scaglioni ,
						scaglione_1 ,
						flag_sconto_mag_prezzo_1 ,
						variazione_1 ,
						flag_origine_prezzo_1 ,
						scaglione_2 ,
						flag_sconto_mag_prezzo_2 ,
						variazione_2 ,
						flag_origine_prezzo_2 ,
						scaglione_3 ,
						flag_sconto_mag_prezzo_3 ,
						variazione_3 ,
						flag_origine_prezzo_3 ,
						scaglione_4  ,
						flag_sconto_mag_prezzo_4 ,
						variazione_4 ,
						flag_origine_prezzo_4  ,
						scaglione_5 ,
						flag_sconto_mag_prezzo_5 ,
						variazione_5 ,
						flag_origine_prezzo_5 ,
						provvigione_1 ,
						provvigione_2  )
				values(:s_cs_xx.cod_azienda ,
						:ls_cod_tipo_listino_prodotto,
						:ls_cod_valuta ,
						:ldt_data_inizio_val ,
						:ll_progressivo ,
						:ls_cod_prodotto_listino ,
						:ls_cod_versione,
						:ll_prog_listino_produzione,
						:ls_cod_prodotto_padre,
						:ls_cod_prodotto_figlio,
						:ls_flag_sconto_a_parte,
						:ls_flag_tipo_scaglioni,
						:ld_scaglione_1,
						:ls_flag_sconto_mag_prezzo_1,
						:ld_variazione_1,
						:ls_flag_origine_prezzo_1,
						:ld_scaglione_2,
						:ls_flag_sconto_mag_prezzo_2,
						:ld_variazione_2,
						:ls_flag_origine_prezzo_2,
						:ld_scaglione_3,
						:ls_flag_sconto_mag_prezzo_3,
						:ld_variazione_3,
						:ls_flag_origine_prezzo_3,
						:ld_scaglione_4 ,
						:ls_flag_sconto_mag_prezzo_4,
						:ld_variazione_4 ,
						:ls_flag_origine_prezzo_4 ,
						:ld_scaglione_5 ,
						:ls_flag_sconto_mag_prezzo_5 ,
						:ld_variazione_5 ,
						:ls_flag_origine_prezzo_5 ,
						:ld_provvigione_1 ,
						:ld_provvigione_2 );
						
						
			elseif ll_cont_locale > 0 and not isnull(ll_cont_locale) then
			
				insert into listini_prod_locale
						(cod_azienda,
						cod_tipo_listino_prodotto,
						cod_valuta,
						data_inizio_val ,
						progressivo ,
						cod_prodotto_listino,
						cod_versione ,
						prog_listino_produzione ,
						cod_prodotto_padre,
						cod_prodotto_figlio ,
						flag_sconto_a_parte ,
						flag_tipo_scaglioni ,
						scaglione_1 ,
						flag_sconto_mag_prezzo_1 ,
						variazione_1 ,
						flag_origine_prezzo_1 ,
						scaglione_2 ,
						flag_sconto_mag_prezzo_2 ,
						variazione_2 ,
						flag_origine_prezzo_2 ,
						scaglione_3 ,
						flag_sconto_mag_prezzo_3 ,
						variazione_3 ,
						flag_origine_prezzo_3 ,
						scaglione_4  ,
						flag_sconto_mag_prezzo_4 ,
						variazione_4 ,
						flag_origine_prezzo_4  ,
						scaglione_5 ,
						flag_sconto_mag_prezzo_5 ,
						variazione_5 ,
						flag_origine_prezzo_5 ,
						provvigione_1 ,
						provvigione_2  )
				values(:s_cs_xx.cod_azienda ,
						:ls_cod_tipo_listino_prodotto,
						:ls_cod_valuta ,
						:ldt_data_inizio_val ,
						:ll_progressivo ,
						:ls_cod_prodotto_listino ,
						:ls_cod_versione,
						:ll_prog_listino_produzione,
						:ls_cod_prodotto_padre,
						:ls_cod_prodotto_figlio,
						:ls_flag_sconto_a_parte,
						:ls_flag_tipo_scaglioni,
						:ld_scaglione_1,
						:ls_flag_sconto_mag_prezzo_1,
						:ld_variazione_1,
						:ls_flag_origine_prezzo_1,
						:ld_scaglione_2,
						:ls_flag_sconto_mag_prezzo_2,
						:ld_variazione_2,
						:ls_flag_origine_prezzo_2,
						:ld_scaglione_3,
						:ls_flag_sconto_mag_prezzo_3,
						:ld_variazione_3,
						:ls_flag_origine_prezzo_3,
						:ld_scaglione_4 ,
						:ls_flag_sconto_mag_prezzo_4,
						:ld_variazione_4 ,
						:ls_flag_origine_prezzo_4 ,
						:ld_scaglione_5 ,
						:ls_flag_sconto_mag_prezzo_5 ,
						:ld_variazione_5 ,
						:ls_flag_origine_prezzo_5 ,
						:ld_provvigione_1 ,
						:ld_provvigione_2 );
						
			else
				dw_import.setitem(ll_i, "errore", "L'inserimento della nuova condizione non è potuta avvenire inquanto non esiste il listino generale di riferimento.")
				continue
						
			end if			
			
			if sqlca.sqlcode < 0 then
				dw_import.setitem(ll_i, "errore", "ERRORE IN INSERIMENTO CONDIZIONI."+sqlca.sqlerrtext)
				continue
			end if	
			
	end choose
	
next

fileclose(ll_file)

commit;

// scrivo il file di errori

for ll_i = 1 to dw_import.rowcount()
	
	if upper(dw_import.getitemstring(ll_i, "errore")) = "OK" and upper(dw_import.getitemstring(ll_i, "c1")) = "AZIONE" then continue
	
	if upper(dw_import.getitemstring(ll_i, "errore")) = "OK" then
		dw_import.deleterow(ll_i)
		ll_i --
	end if
	
next

if dw_import.rowcount() > 0 then
	if dw_import.saveas(ls_file_errors, text!, false) < 0 then
		messagebox("APICE","Errore durante la creazione del file di errori " + ls_file_errors)
	end if
end if

messagebox("APICE","Elaborazione eseguita con successo!")
end event

type pb_path_import from picturebutton within w_export_import_listini_produzione
integer x = 2743
integer y = 984
integer width = 101
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean flatstyle = true
string picturename = "C:\cs_105\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string ls_path, ls_file

int li_rc


ls_path = em_path_import.Text

li_rc = GetFileSaveName ( "File di Import",  ls_path, ls_file, "Txt", "File Txt (*.Txt),*.Txt" , "C:\temp")

em_path_import.Text = ls_path

dw_import.reset()

dw_import.importfile(Text!, ls_path)
end event

type pb_path_export from picturebutton within w_export_import_listini_produzione
integer x = 2743
integer y = 624
integer width = 101
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean flatstyle = true
boolean originalsize = true
string picturename = "C:\cs_105\framework\RISORSE\MENUVOCA.BMP"
alignment htextalign = left!
end type

event clicked;string ls_path, ls_file

int li_rc


ls_path = em_path_export.Text

li_rc = GetFileSaveName ( "File di Export",  ls_path, ls_file, "Txt", "File Txt (*.Txt),*.Txt" , "C:\temp")

em_path_export.Text = ls_path
end event

type em_path_import from editmask within w_export_import_listini_produzione
integer x = 69
integer y = 984
integer width = 2674
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_path_export from editmask within w_export_import_listini_produzione
integer x = 69
integer y = 624
integer width = 2674
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_export from commandbutton within w_export_import_listini_produzione
integer x = 2400
integer y = 724
integer width = 343
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Esporta"
boolean flatstyle = true
end type

event clicked;string ls_sql, ls_flag_tipo_listino, ls_file, ls_str, ls_tab, ls_des, ls_cod_prodotto_inizio, ls_cod_prodotto_fine, &
		ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_listino, ls_cod_versione, ls_cod_prodotto_padre, &
		ls_cod_prodotto_figlio, ls_cod_gruppo_variante, ls_cod_prodotto, ls_des_listino_produzione, &
		ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, ls_flag_sconto_mag_prezzo_1, ls_flag_sconto_mag_prezzo_2, &
		ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4, ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1,&
		ls_flag_origine_prezzo_2, ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5, &
		ls_cod_cliente, ls_rag_soc_1
long  ll_file,ll_progressivo, ll_num_sequenza, ll_ret,ll_progr, ll_prog_listino_produzione, ll_cont
decimal  ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5, ld_provvigione_1, ld_provvigione_2
datetime ldt_inizio, ldt_fine,ldt_data_inizio_val
DECLARE cu_listino_produzione dynamic CURSOR for sqlsa;



ls_tab = "~t"
dw_1.accepttext()
ldt_inizio = dw_1.getitemdatetime(dw_1.getrow(),"data_val_inizio")
ldt_fine   =dw_1.getitemdatetime(dw_1.getrow(),"data_val_fine")
ls_cod_prodotto_inizio = dw_1.getitemstring(dw_1.getrow(),"cod_prodotto_inizio")
ls_cod_prodotto_fine   = dw_1.getitemstring(dw_1.getrow(),"cod_prodotto_fine")
ls_flag_tipo_listino = dw_1.getitemstring(dw_1.getrow(),"flag_tipo_listino")
 
ls_sql = " SELECT cod_tipo_listino_prodotto,   " + &
         " cod_valuta,   " + &
         " data_inizio_val,   " + &
         " progressivo,   " + &
         " cod_prodotto_listino," + &   
         " cod_versione,   " + &
         " prog_listino_produzione," + &   
         " cod_prodotto_padre,   " + &
         " num_sequenza,   " + &
         " cod_prodotto_figlio, " + &  
         " cod_gruppo_variante,   " + &
         " progr,   " + &
         " cod_prodotto, " + &  
         " des_listino_produzione," + &   
         " flag_sconto_a_parte,   " + &
         " flag_tipo_scaglioni,   " + &
         " scaglione_1,   " + &
         " scaglione_2,   " + &
         " scaglione_3,   " + &
         " scaglione_4,   " + &
         " scaglione_5,   " + &
         " flag_sconto_mag_prezzo_1,   " + &
         " flag_sconto_mag_prezzo_2,   " + &
         " flag_sconto_mag_prezzo_3,   " + &
         " flag_sconto_mag_prezzo_4,   " + &
         " flag_sconto_mag_prezzo_5,   " + &
         " variazione_1,   " + &
         " variazione_2,   " + &
         " variazione_3,   " + &
         " variazione_4,   " + &
         " variazione_5,   " + &
         " flag_origine_prezzo_1,   " + &
         " flag_origine_prezzo_2,   " + &
         " flag_origine_prezzo_3,   " + &
         " flag_origine_prezzo_4,   " + &
         " flag_origine_prezzo_5,   " + &
         " provvigione_1,   " + &
         " provvigione_2 "

choose case upper(ls_flag_tipo_listino)
	case "L"
    ls_sql += " FROM listini_prod_locale "
	case "C"
    ls_sql += " FROM listini_prod_comune "
end choose

ls_sql += " where cod_azienda = '" + s_cs_xx.cod_azienda + "' " 

if not isnull(ldt_inizio) then
	ls_sql += " and data_inizio_val >= '" + string(ldt_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
else
	g_mb.messagebox("APICE","E' obbligatorio specificare una data di inizio")
	return
end if

if not isnull(ldt_fine) then
	ls_sql += " and data_inizio_val <= '" + string(ldt_fine, s_cs_xx.db_funzioni.formato_data) + "' "
else
	g_mb.messagebox("APICE","E' obbligatorio specificare una data di fine")
	return
end if

if not isnull(ls_cod_prodotto_inizio) then
	ls_sql += " and cod_prodotto_listino >= '" + ls_cod_prodotto_inizio + "' "
end if

if not isnull(ls_cod_prodotto_fine) then
	ls_sql += " and cod_prodotto_listino <= '" + ls_cod_prodotto_fine + "' "
end if

ls_file = em_path_export.text

if len(trim(ls_file)) < 1 then
	g_mb.messagebox("APICE","E' obbligatorio specificare file di export")
	return
end if
	
if fileexists(ls_file) then
	if g_mb.messagebox("APICE","Il file esiste già; lo sostituisco ?", Question!, YesNo!, 2) = 2 then 
		return
	end if
	
	if not filedelete(ls_file) then
		g_mb.messagebox("APICE","Impossibile cancellare il file~r~n" + ls_file)
		return
	end if
end if
	
ll_file = fileopen(ls_file, linemode!, write!, LockReadWrite!,Replace!)

if ll_file < 0 then
	g_mb.messagebox("APICE","Errore in creazione del file~r~n" + ls_file + "~r~nVerificare i permessi utente.")
	return 
end if

ll_cont = 0

prepare sqlsa from :ls_sql;
open dynamic cu_listino_produzione;

do while true
	fetch cu_listino_produzione into :ls_cod_tipo_listino_prodotto, :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo, 
			:ls_cod_prodotto_listino, :ls_cod_versione, :ll_prog_listino_produzione, :ls_cod_prodotto_padre, :ll_num_sequenza, 
			:ls_cod_prodotto_figlio, :ls_cod_gruppo_variante, :ll_progr, :ls_cod_prodotto, :ls_des_listino_produzione,
			:ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ld_scaglione_1, :ld_scaglione_2, :ld_scaglione_3, :ld_scaglione_4, :ld_scaglione_5, 
			:ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, :ls_flag_sconto_mag_prezzo_5, 
			:ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, 
			:ls_flag_origine_prezzo_1, :ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5, 
         :ld_provvigione_1, :ld_provvigione_2;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in FETCH cursore listini produzione~r~n" + sqlca.sqlerrtext)
		rollback;
		return 
	end if

	if sqlca.sqlcode = 100 then
		exit
	end if
	
	ls_cod_cliente = ""
	
	select cod_cliente
	into   :ls_cod_cliente
	from   listini_ven_locale
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			 cod_valuta = :ls_cod_valuta and
			 data_inizio_val = :ldt_data_inizio_val and
			 progressivo = :ll_progressivo;
	if sqlca.sqlcode = 100 then
		ls_cod_cliente = ""
	end if
	
	if isnull(ls_cod_cliente) then ls_cod_cliente = ""
	
	if len(ls_cod_cliente) > 0 and not isnull(ls_cod_cliente) then
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cliente = :ls_cod_cliente;
	end if
		
	ll_cont ++
	
	st_1.text = ls_cod_prodotto_padre + "...." + string(ll_cont)
	
	if ll_cont = 1 then		// inserisco riga di testata
		ls_str  = "AZIONE" + ls_tab
		ls_str += "TIPO LISTINO" + ls_tab
		ls_str += "VALUTA" + ls_tab
		ls_str += "DATA VALIDITA" + ls_tab
		ls_str += "PROGRESSIVO" + ls_tab
		ls_str += "PRODOTTO LISTINO" + ls_tab
		ls_str += "DESCRIZIONE PRODOTTO LISTINO" + ls_tab
		ls_str += "VERSIONE" + ls_tab
		ls_str += "DESCRIZIONE VERSIONE" + ls_tab
		ls_str += "PROGRESSIVO PROD" + ls_tab
		ls_str += "PRODOTTO PADRE" + ls_tab
		ls_str += "DESCRIZIONE PRODOTTO PADRE" + ls_tab
		ls_str += "PRODOTTO FIGLIO" + ls_tab
		ls_str += "DESCRIZIONE PRODOTTO FIGLIO" + ls_tab
		ls_str += "SCONTO A PARTE" + ls_tab
		ls_str += "TIPO SCAGLIONE" + ls_tab
		ls_str += "SCAGLIONE_1" + ls_tab
		ls_str += "TIPO VARIAZIONE 1" + ls_tab
		ls_str += "VARIAZIONE 1" + ls_tab
		ls_str += "ORIGINE PREZZO 1" + ls_tab
		ls_str += "SCAGLIONE_2" + ls_tab
		ls_str += "TIPO VARIAZIONE 2" + ls_tab
		ls_str += "VARIAZIONE 2" + ls_tab
		ls_str += "ORIGINE PREZZO 2" + ls_tab
		ls_str += "SCAGLIONE_3" + ls_tab
		ls_str += "TIPO VARIAZIONE 3" + ls_tab
		ls_str += "VARIAZIONE 3" + ls_tab
		ls_str += "ORIGINE PREZZO 3" + ls_tab
		ls_str += "SCAGLIONE_4" + ls_tab
		ls_str += "TIPO VARIAZIONE 4" + ls_tab
		ls_str += "VARIAZIONE 4" + ls_tab
		ls_str += "ORIGINE PREZZO 4" + ls_tab
		ls_str += "SCAGLIONE_5" + ls_tab
		ls_str += "TIPO VARIAZIONE 5" + ls_tab
		ls_str += "VARIAZIONE 5" + ls_tab
		ls_str += "ORIGINE PREZZO 5" + ls_tab
		ls_str += "PROVV AG 1" + ls_tab
		ls_str += "PROVV AG 2"
	
		ll_ret = FileWriteEx (ll_file, ls_str)
		
		if ll_ret < 1 then
			g_mb.messagebox("APICE","Errore in scrittura file " + ls_file)
			fileclose(ll_file)
			filedelete(ls_file)
			exit
		end if

	end if
	
	ls_str  = "M" + ls_tab
	ls_str += wf_null( ls_cod_tipo_listino_prodotto ) + ls_tab
	ls_str += wf_null( ls_cod_valuta ) + ls_tab
	ls_str += wf_null( string(ldt_data_inizio_val, "dd/mm/yyyy") ) + ls_tab
	ls_str += wf_null( string(ll_progressivo) ) + ls_tab
	ls_str += wf_null( ls_cod_prodotto_listino ) + ls_tab
	
	ls_des = f_des_tabella("anag_prodotti", "cod_prodotto ='" + ls_cod_prodotto_listino + "'", "des_prodotto")
	ls_str += wf_null( ls_des ) + ls_tab
	
	ls_str += wf_null( ls_cod_versione )  + ls_tab
	
	ls_des = f_des_tabella("distinta_padri", "cod_prodotto ='" + ls_cod_prodotto_listino + "' and cod_versione = '" + ls_cod_versione + "'", "des_versione")
	ls_str += wf_null( ls_des ) + " " + ls_cod_cliente + " " + ls_rag_soc_1 + ls_tab
	
	ls_str += wf_null( string(ll_prog_listino_produzione) ) + ls_tab
	ls_str += wf_null( ls_cod_prodotto_padre ) + ls_tab
	
	ls_des = f_des_tabella("anag_prodotti", "cod_prodotto ='" + ls_cod_prodotto_padre + "'", "des_prodotto")
	ls_str += wf_null( ls_des ) + ls_tab
	
	ls_str += wf_null( ls_cod_prodotto_figlio ) + ls_tab
	
	ls_des = f_des_tabella("anag_prodotti", "cod_prodotto ='" + ls_cod_prodotto_figlio + "'", "des_prodotto")
	ls_str += wf_null( ls_des ) + ls_tab
	
	ls_str += wf_null( ls_flag_sconto_a_parte ) + ls_tab
	ls_str += wf_null( ls_flag_tipo_scaglioni )  + ls_tab
	ls_str += wf_null( string(ld_scaglione_1, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_sconto_mag_prezzo_1 ) + ls_tab
	ls_str += wf_null( string(ld_variazione_1, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_origine_prezzo_1 ) + ls_tab
	
	ls_str += wf_null( string(ld_scaglione_2, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_sconto_mag_prezzo_2 ) + ls_tab
	ls_str += wf_null( string(ld_variazione_2, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_origine_prezzo_2 ) + ls_tab
	
	ls_str += wf_null( string(ld_scaglione_3, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_sconto_mag_prezzo_3 ) + ls_tab
	ls_str += wf_null( string(ld_variazione_3, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_origine_prezzo_3 ) + ls_tab
	
	ls_str += wf_null( string(ld_scaglione_4, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_sconto_mag_prezzo_4 ) + ls_tab
	ls_str += wf_null( string(ld_variazione_4, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_origine_prezzo_4 ) + ls_tab
	
	ls_str += wf_null( string(ld_scaglione_5, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_sconto_mag_prezzo_5 ) + ls_tab
	ls_str += wf_null( string(ld_variazione_5, "###,###,##0.0000") ) + ls_tab
	ls_str += wf_null( ls_flag_origine_prezzo_5 ) + ls_tab
	
	ls_str += wf_null( string(ld_provvigione_1, "##0.00") ) + ls_tab
	ls_str += wf_null( string(ld_provvigione_2, "##0.00") )
		
	ll_ret = FileWriteEx (ll_file, ls_str)
	
	if ll_ret < 1 then
		g_mb.messagebox("APICE","Errore in scrittura file " + ls_file)
		fileclose(ll_file)
		filedelete(ls_file)
		exit
	end if

loop

close cu_listino_produzione;

fileclose(ll_file)

rollback;

messagebox("APICE","Elaborazione eseguita con successo!")
end event

