﻿$PBExportHeader$w_file_clienti.srw
forward
global type w_file_clienti from window
end type
type cb_1 from commandbutton within w_file_clienti
end type
end forward

global type w_file_clienti from window
integer width = 928
integer height = 388
boolean titlebar = true
string title = "CLIENTI"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
cb_1 cb_1
end type
global w_file_clienti w_file_clienti

on w_file_clienti.create
this.cb_1=create cb_1
this.Control[]={this.cb_1}
end on

on w_file_clienti.destroy
destroy(this.cb_1)
end on

type cb_1 from commandbutton within w_file_clienti
integer x = 251
integer y = 120
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_str[], ls_des_str[], ls_condizioni[], ls_varianti[], dinensioni[], ls_messaggio

uo_report_listini luo_report_listini

luo_report_listini = CREATE uo_report_listini
luo_report_listini.uof_ricerca_clienti(ls_str, ls_des_str, ls_condizioni[], ls_varianti[],dinensioni[],ls_messaggio)
destroy luo_report_listini

end event

