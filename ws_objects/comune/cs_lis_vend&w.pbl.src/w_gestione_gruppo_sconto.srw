﻿$PBExportHeader$w_gestione_gruppo_sconto.srw
$PBExportComments$Finestra Gestione Linee di sconto
forward
global type w_gestione_gruppo_sconto from w_cs_xx_principale
end type
type cb_6 from commandbutton within w_gestione_gruppo_sconto
end type
type cb_5 from commandbutton within w_gestione_gruppo_sconto
end type
type tv_1 from treeview within w_gestione_gruppo_sconto
end type
type cb_codice from commandbutton within w_gestione_gruppo_sconto
end type
type cb_descrizione from commandbutton within w_gestione_gruppo_sconto
end type
type dw_gruppo_sconto from uo_cs_xx_dw within w_gestione_gruppo_sconto
end type
type cb_ricerca from commandbutton within w_gestione_gruppo_sconto
end type
type st_1 from statictext within w_gestione_gruppo_sconto
end type
type cb_1 from commandbutton within w_gestione_gruppo_sconto
end type
type cb_2 from commandbutton within w_gestione_gruppo_sconto
end type
type em_1 from editmask within w_gestione_gruppo_sconto
end type
type st_2 from statictext within w_gestione_gruppo_sconto
end type
type cb_3 from commandbutton within w_gestione_gruppo_sconto
end type
type st_3 from statictext within w_gestione_gruppo_sconto
end type
type em_2 from editmask within w_gestione_gruppo_sconto
end type
type st_4 from statictext within w_gestione_gruppo_sconto
end type
type em_3 from editmask within w_gestione_gruppo_sconto
end type
type cb_4 from commandbutton within w_gestione_gruppo_sconto
end type
type ws_record from structure within w_gestione_gruppo_sconto
end type
end forward

type ws_record from structure
	string		cod_gruppo_sconto
	string		cod_valuta
	datetime		data_inizio_val
	long		progressivo
	string		cod_livello
end type

global type w_gestione_gruppo_sconto from w_cs_xx_principale
integer width = 3483
integer height = 2056
string title = "Gruppi Sconti"
cb_6 cb_6
cb_5 cb_5
tv_1 tv_1
cb_codice cb_codice
cb_descrizione cb_descrizione
dw_gruppo_sconto dw_gruppo_sconto
cb_ricerca cb_ricerca
st_1 st_1
cb_1 cb_1
cb_2 cb_2
em_1 em_1
st_2 st_2
cb_3 cb_3
st_3 st_3
em_2 em_2
st_4 st_4
em_3 em_3
cb_4 cb_4
end type
global w_gestione_gruppo_sconto w_gestione_gruppo_sconto

type variables
long il_handle, il_rootitem[]
string is_tipo_ordinamento, is_cod_cliente, is_cod_valuta, is_cod_agente, is_cod_gruppo
datastore ids_cat_attrezzature, ids_rep_attrezzature, ids_programmi_manutenzione
treeviewitem tvi_campo
end variables

forward prototypes
public function integer wf_inserisci_livello_3 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2)
public function integer wf_inserisci_livello_2 (long fl_handle, string fs_cod_livello_1)
public function integer wf_inserisci_livello_4 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3)
public function integer wf_inserisci_livello_5 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4)
public function integer wf_inserisci_dettaglio (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4, string fs_cod_livello_5)
public subroutine wf_imposta_ricerca (string fs_cod_cliente, string fs_cod_agente, string fs_cod_valuta, string fs_cod_gruppo)
public subroutine wf_imposta_tv ()
public function integer wf_inserisci_livello_1 (long fl_handle)
public function integer wf_inserisci_nuovo_nodo ()
public function long wf_trova_handle (long al_handle_parent, string as_cod_livello)
public function string wf_des_livello (string as_cod_livello, integer ai_num_livello)
end prototypes

public function integer wf_inserisci_livello_3 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2);string ls_sql, ls_cod_livello_prod_3, ls_des_livello, ls_null
long ll_risposta, ll_i
ws_record lstr_record


setnull(ls_null)
declare cu_livello_3 dynamic cursor for sqlsa;
ls_sql = "select cod_livello_prod_3, des_livello from tab_livelli_prod_3 where cod_azienda = '" + s_cs_xx.cod_azienda + "'  and flag_blocco = 'N' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "'"
 
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_livello_prod_3"
else
	ls_sql = ls_sql + " ORDER BY des_livello"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_livello_3;
do while 1=1
   fetch cu_livello_3 into :ls_cod_livello_prod_3, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	setnull(lstr_record.cod_gruppo_sconto)
	setnull(lstr_record.cod_valuta)
	setnull(lstr_record.data_inizio_val)
	setnull(lstr_record.progressivo)
	
	lstr_record.cod_livello = ls_cod_livello_prod_3 // stefanop 28/05/2010: ticket 137
	
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = ls_des_livello + ", " + ls_cod_livello_prod_3
	else
		tvi_campo.label = ls_cod_livello_prod_3 + ", " + ls_des_livello
	end if		
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	wf_inserisci_livello_4(ll_risposta, fs_cod_livello_1, fs_cod_livello_2, ls_cod_livello_prod_3)
	wf_inserisci_dettaglio(ll_risposta, fs_cod_livello_1, fs_cod_livello_2, ls_cod_livello_prod_3, ls_null, ls_null)
loop
close cu_livello_3;

return 0
end function

public function integer wf_inserisci_livello_2 (long fl_handle, string fs_cod_livello_1);string ls_sql, ls_cod_livello_prod_2, ls_des_livello, ls_null
long ll_risposta, ll_i
ws_record lstr_record


setnull(ls_null)
declare cu_livello_2 dynamic cursor for sqlsa;
ls_sql = "select cod_livello_prod_2, des_livello from tab_livelli_prod_2 where cod_azienda = '" + s_cs_xx.cod_azienda + "'  and flag_blocco = 'N' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "'"
 
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_livello_prod_2"
else
	ls_sql = ls_sql + " ORDER BY des_livello"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_livello_2;
do while 1=1
   fetch cu_livello_2 into :ls_cod_livello_prod_2, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	setnull(lstr_record.cod_gruppo_sconto)
	setnull(lstr_record.cod_valuta)
	setnull(lstr_record.data_inizio_val)
	setnull(lstr_record.progressivo)
	
	lstr_record.cod_livello = ls_cod_livello_prod_2 // stefanop 28/05/2010: ticket 137
	
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = ls_des_livello + ", " + ls_cod_livello_prod_2
	else
		tvi_campo.label = ls_cod_livello_prod_2 + ", " + ls_des_livello
	end if		
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	wf_inserisci_livello_3(ll_risposta, fs_cod_livello_1, ls_cod_livello_prod_2)
	wf_inserisci_dettaglio(ll_risposta, fs_cod_livello_1, ls_cod_livello_prod_2, ls_null, ls_null, ls_null)
loop
close cu_livello_2;

return 0
end function

public function integer wf_inserisci_livello_4 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3);string ls_sql, ls_cod_livello_prod_4, ls_des_livello, ls_null
long ll_risposta, ll_i
ws_record lstr_record


setnull(ls_null)
declare cu_livello_4 dynamic cursor for sqlsa;
ls_sql = "select cod_livello_prod_4, des_livello from tab_livelli_prod_4 where cod_azienda = '" + s_cs_xx.cod_azienda + "'  and flag_blocco = 'N' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "' and cod_livello_prod_3 = '" +fs_cod_livello_3 + "'"
 
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_livello_prod_4"
else
	ls_sql = ls_sql + " ORDER BY des_livello"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_livello_4;
do while 1=1
   fetch cu_livello_4 into :ls_cod_livello_prod_4, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	setnull(lstr_record.cod_gruppo_sconto)
	setnull(lstr_record.cod_valuta)
	setnull(lstr_record.data_inizio_val)
	setnull(lstr_record.progressivo)
	
	lstr_record.cod_livello = ls_cod_livello_prod_4 // stefanop 28/05/2010: ticket 137
	
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = ls_des_livello + ", " + ls_cod_livello_prod_4
	else
		tvi_campo.label = ls_cod_livello_prod_4 + ", " + ls_des_livello
	end if		
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	wf_inserisci_livello_5(ll_risposta, fs_cod_livello_1, fs_cod_livello_2, fs_cod_livello_3, ls_cod_livello_prod_4)
	wf_inserisci_dettaglio(ll_risposta, fs_cod_livello_1, fs_cod_livello_2, fs_cod_livello_3, ls_cod_livello_prod_4, ls_null)
loop
close cu_livello_4;

return 0
end function

public function integer wf_inserisci_livello_5 (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4);string ls_sql, ls_cod_livello_prod_5, ls_des_livello, ls_null
long ll_risposta, ll_i
ws_record lstr_record


setnull(ls_null)
declare cu_livello_5 dynamic cursor for sqlsa;
ls_sql = "select cod_livello_prod_5, des_livello from tab_livelli_prod_5 where cod_azienda = '" + s_cs_xx.cod_azienda + "'  and flag_blocco = 'N' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "' and cod_livello_prod_3 = '" +fs_cod_livello_3 + "' and cod_livello_prod_4 = '" +fs_cod_livello_4 + "'"
 
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_livello_prod_5"
else
	ls_sql = ls_sql + " ORDER BY des_livello"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_livello_5;
do while 1=1
   fetch cu_livello_5 into :ls_cod_livello_prod_5, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	setnull(lstr_record.cod_gruppo_sconto)
	setnull(lstr_record.cod_valuta)
	setnull(lstr_record.data_inizio_val)
	setnull(lstr_record.progressivo)
	
	lstr_record.cod_livello = ls_cod_livello_prod_5 // stefanop 28/05/2010: ticket 137
	
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = ls_des_livello + ", " + ls_cod_livello_prod_5
	else
		tvi_campo.label = ls_cod_livello_prod_5 + ", " + ls_des_livello
	end if		
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	wf_inserisci_dettaglio(ll_risposta, fs_cod_livello_1, fs_cod_livello_2, fs_cod_livello_3, fs_cod_livello_4, ls_cod_livello_prod_5)
loop
close cu_livello_5;

return 0
end function

public function integer wf_inserisci_dettaglio (long fl_handle, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4, string fs_cod_livello_5);string ls_sql, ls_cod_valuta, ls_null, ls_cod_gruppo_sconto
long ll_risposta, ll_i, ll_progressivo
datetime ldt_data_inizio_val
ws_record lstr_record


setnull(ls_null)
declare cu_dettaglio dynamic cursor for sqlsa;
ls_sql = "select cod_gruppo_sconto, cod_valuta, data_inizio_val, progressivo from gruppi_sconto where cod_azienda = '" + s_cs_xx.cod_azienda + "'"
if isnull(fs_cod_livello_1) then
	ls_sql = ls_sql + " and cod_livello_prod_1 is null"
else
	ls_sql = ls_sql + " and cod_livello_prod_1 = '" +fs_cod_livello_1 + "'"
end if
if isnull(fs_cod_livello_2) then
	ls_sql = ls_sql + " and cod_livello_prod_2 is null"
else
   ls_sql = ls_sql + " and cod_livello_prod_2 = '" +fs_cod_livello_2 + "'"
end if
if isnull(fs_cod_livello_3) then
	ls_sql = ls_sql + " and cod_livello_prod_3 is null"
else
	ls_sql = ls_sql + " and cod_livello_prod_3 = '" +fs_cod_livello_3 + "'"
end if	
if isnull(fs_cod_livello_4) then
	ls_sql = ls_sql + " and cod_livello_prod_4 is null"
else
	ls_sql = ls_sql + " and cod_livello_prod_4 = '" +fs_cod_livello_4 + "'"
end if
if isnull(fs_cod_livello_5) then
	ls_sql = ls_sql + " and cod_livello_prod_5 is null"
else
	ls_sql = ls_sql + " and cod_livello_prod_5 = '" +fs_cod_livello_5 + "'"
end if
if not isnull(is_cod_cliente) then
	ls_sql = ls_sql + " and cod_cliente = '" +is_cod_cliente + "'"
else
	ls_sql = ls_sql + " and cod_cliente is null "
end if
if not isnull(is_cod_agente) then
	ls_sql = ls_sql + " and cod_agente = '" +is_cod_agente + "'"
else
	ls_sql = ls_sql + " and cod_agente is null "
end if
if not isnull(is_cod_valuta) then
	ls_sql = ls_sql + " and cod_valuta = '" +is_cod_valuta + "'"
end if
if not isnull(is_cod_gruppo) then
	ls_sql = ls_sql + " and cod_gruppo_sconto = '" +is_cod_gruppo + "'"
end if
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_valuta"
else
	ls_sql = ls_sql + " ORDER BY data_inizio_val"
end if

prepare sqlsa from :ls_sql;
open dynamic cu_dettaglio;
do while 1=1
   fetch cu_dettaglio into :ls_cod_gruppo_sconto, :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	lstr_record.cod_gruppo_sconto = ls_cod_gruppo_sconto
	lstr_record.cod_valuta = ls_cod_valuta
	lstr_record.data_inizio_val = ldt_data_inizio_val
	lstr_record.progressivo = ll_progressivo
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = string(ldt_data_inizio_val,"dd/mm/yyyy") + ", " + ls_cod_valuta
	else
		tvi_campo.label = ls_cod_valuta + ", " + string(ldt_data_inizio_val,"dd/mm/yyyy")
	end if		
	tvi_campo.pictureindex = 4
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop
close cu_dettaglio;

return 0
end function

public subroutine wf_imposta_ricerca (string fs_cod_cliente, string fs_cod_agente, string fs_cod_valuta, string fs_cod_gruppo);string ls_str

is_cod_cliente = fs_cod_cliente
is_cod_agente  = fs_cod_agente
is_cod_valuta  = fs_cod_valuta
is_cod_gruppo = fs_cod_gruppo

st_1.text = ""
if not isnull(is_cod_gruppo) then
	st_1.text = st_1.text + "GRUPPO SCONTO " + is_cod_gruppo
end if
if not isnull(is_cod_cliente) then
	st_1.text = "CLIENTE " + is_cod_cliente
	select rag_soc_1
	into   :ls_str
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda  and
	       cod_cliente = :is_cod_cliente ;
	if isnull(ls_str) or sqlca.sqlcode <> 0 then ls_str = " "
	st_1.text = st_1.text + "-" + ls_str
end if
if not isnull(is_cod_agente) then
	st_1.text = st_1.text + "AGENTE " + is_cod_agente
end if
if not isnull(is_cod_valuta) then
	st_1.text = st_1.text + "VALUTA " + is_cod_valuta
end if

wf_imposta_tv()
end subroutine

public subroutine wf_imposta_tv ();string ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i
ws_record lstr_record


ll_i = 0
tv_1.setredraw(false)
tv_1.deleteitem(0)

wf_inserisci_livello_1(ll_2)

tv_1.setredraw(true)
commit;

return
end subroutine

public function integer wf_inserisci_livello_1 (long fl_handle);string ls_sql, ls_cod_livello_prod_1, ls_des_livello, ls_null
long ll_risposta, ll_i, ll_item, ll_vuoto[]
ws_record lstr_record

ll_item = 1
il_rootitem[] = ll_vuoto[]
setnull(ls_null)
declare cu_livello_1 dynamic cursor for sqlsa;
ls_sql = "select cod_livello_prod_1, des_livello from tab_livelli_prod_1 where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' "
if is_tipo_ordinamento = "C" then
	ls_sql = ls_sql + " ORDER BY cod_livello_prod_1"
else
	ls_sql = ls_sql + " ORDER BY des_livello"
end if
prepare sqlsa from :ls_sql;
open dynamic cu_livello_1;
do while 1=1
   fetch cu_livello_1 into :ls_cod_livello_prod_1, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i ++
	tvi_campo.itemhandle = fl_handle
	setnull(lstr_record.cod_gruppo_sconto)
	setnull(lstr_record.cod_valuta)
	setnull(lstr_record.data_inizio_val)
	setnull(lstr_record.progressivo)
	
	lstr_record.cod_livello = ls_cod_livello_prod_1 // stefanop 28/05/2010: ticket 137
	
	tvi_campo.data = lstr_record
	if is_tipo_ordinamento = "C" then
		tvi_campo.label = ls_des_livello + ", " + ls_cod_livello_prod_1
	else
		tvi_campo.label = ls_cod_livello_prod_1 + ", " + ls_des_livello
	end if		
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	il_rootitem[ll_item] = ll_risposta
	ll_item ++
	
	wf_inserisci_livello_2(ll_risposta, ls_cod_livello_prod_1)
	wf_inserisci_dettaglio(ll_risposta, ls_cod_livello_prod_1, ls_null, ls_null, ls_null, ls_null)
loop
close cu_livello_1;

return 0
end function

public function integer wf_inserisci_nuovo_nodo ();/**
 * Stefanop
 * 28/05/2010
 *
 * Funzione che inserisce un nuovo nodo nella corretta posizione dell'albero
 **/
 
string ls_cod_livello, ls_des_livello, ls_cod_valuta
int		li_row, li_livello
long	ll_handle, ll_handle_parent
datetime ldt_data_inizio_val
treeviewitem ltvi_item
ws_record lstr_data

ll_handle_parent = 0
li_livello = 1
li_row = dw_gruppo_sconto.getrow()

// controllo se esiste il livello
do
	ls_cod_livello = dw_gruppo_sconto.getitemstring(li_row, "cod_livello_prod_" + string(li_livello))
	if isnull(ls_cod_livello) or ls_cod_livello = "" then exit
	
	ll_handle = wf_trova_handle(ll_handle_parent, ls_cod_livello)
	if ll_handle < 0 then
		
		// inserisco nuovo livello nell'albero
		ls_des_livello = wf_des_livello(ls_cod_livello, li_livello)
		
		//ltvi_campo.itemhandle = ll_handle
		setnull(lstr_data.cod_gruppo_sconto)
		setnull(lstr_data.cod_valuta)
		setnull(lstr_data.data_inizio_val)
		setnull(lstr_data.progressivo)
		lstr_data.cod_livello = ls_cod_livello
		ltvi_item.data = lstr_data
		
		if is_tipo_ordinamento = "C" then
			ltvi_item.label = ls_des_livello + ", " + ls_cod_livello
		else
			ltvi_item.label = ls_cod_livello + ", " + ls_des_livello
		end if		
		
		tvi_campo.pictureindex = 2
		tvi_campo.selectedpictureindex = 1
		tvi_campo.overlaypictureindex = 1
		tvi_campo.selected = false
		
		ll_handle = tv_1.insertitemlast(ll_handle_parent, ltvi_item)
	end if
	
	ll_handle_parent = ll_handle
	li_livello++
loop while ls_cod_livello <> ""

// inserisco nodo
//ltvi_item.itemhandle = fl_handle
ls_cod_valuta = dw_gruppo_sconto.getitemstring(li_row,"cod_valuta")
ldt_data_inizio_val = dw_gruppo_sconto.getitemdatetime(li_row, "data_inizio_val")
lstr_data.cod_gruppo_sconto = dw_gruppo_sconto.getitemstring(li_row, "cod_gruppo_sconto")
lstr_data.cod_valuta = ls_cod_valuta
lstr_data.data_inizio_val = ldt_data_inizio_val
lstr_data.progressivo = dw_gruppo_sconto.getitemnumber(li_row, "progressivo")
ltvi_item.data = lstr_data
if is_tipo_ordinamento = "C" then
	ltvi_item.label = string(ldt_data_inizio_val,"dd/mm/yyyy") + ", " + ls_cod_valuta
else
	ltvi_item.label = ls_cod_valuta + ", " + string(ldt_data_inizio_val,"dd/mm/yyyy")
end if		
ltvi_item.pictureindex = 4
ltvi_item.selectedpictureindex = 3
ltvi_item.overlaypictureindex = 3

ll_handle = tv_1.insertitemlast(ll_handle_parent, ltvi_item)
if ll_handle > 0 then
	tv_1.setfocus()
	tv_1.selectitem(ll_handle)
end if

return 1
end function

public function long wf_trova_handle (long al_handle_parent, string as_cod_livello);/**
 * Stefanop
 * 28/05/2010
 *
 * Controlla se esiste un determinato codice livello a partire da un handle padre
 **/
 
long ll_handle
treeviewitem ltvi_item
ws_record lstr_data

if al_handle_parent = 0 then
	ll_handle = tv_1.finditem(RootTreeItem!, al_handle_parent)
else
	ll_handle = tv_1.finditem(ChildTreeItem!, al_handle_parent)
end if

do while ll_handle > 0
	
	tv_1.getitem(ll_handle, ltvi_item)
	lstr_data = ltvi_item.data
		
	if lstr_data.cod_livello = as_cod_livello then	return ll_handle

	
	ll_handle = tv_1.finditem(NextTreeItem!, ll_handle)
loop

return -1
end function

public function string wf_des_livello (string as_cod_livello, integer ai_num_livello);string ls_sql
datastore lds_store

ls_sql   = "SELECT des_livello FROM tab_livelli_prod_" + string(ai_num_livello) + " "
ls_sql += "WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND flag_blocco = 'N' "
ls_sql += "AND cod_livello_prod_" + string(ai_num_livello) + "='" + as_cod_livello + "'"

if not f_crea_datastore(ref lds_store, ls_sql) then
	destroy lds_store
	return ""
end if

lds_store.retrieve()
return lds_store.getitemstring(1,1)
end function

on w_gestione_gruppo_sconto.create
int iCurrent
call super::create
this.cb_6=create cb_6
this.cb_5=create cb_5
this.tv_1=create tv_1
this.cb_codice=create cb_codice
this.cb_descrizione=create cb_descrizione
this.dw_gruppo_sconto=create dw_gruppo_sconto
this.cb_ricerca=create cb_ricerca
this.st_1=create st_1
this.cb_1=create cb_1
this.cb_2=create cb_2
this.em_1=create em_1
this.st_2=create st_2
this.cb_3=create cb_3
this.st_3=create st_3
this.em_2=create em_2
this.st_4=create st_4
this.em_3=create em_3
this.cb_4=create cb_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_6
this.Control[iCurrent+2]=this.cb_5
this.Control[iCurrent+3]=this.tv_1
this.Control[iCurrent+4]=this.cb_codice
this.Control[iCurrent+5]=this.cb_descrizione
this.Control[iCurrent+6]=this.dw_gruppo_sconto
this.Control[iCurrent+7]=this.cb_ricerca
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.cb_1
this.Control[iCurrent+10]=this.cb_2
this.Control[iCurrent+11]=this.em_1
this.Control[iCurrent+12]=this.st_2
this.Control[iCurrent+13]=this.cb_3
this.Control[iCurrent+14]=this.st_3
this.Control[iCurrent+15]=this.em_2
this.Control[iCurrent+16]=this.st_4
this.Control[iCurrent+17]=this.em_3
this.Control[iCurrent+18]=this.cb_4
end on

on w_gestione_gruppo_sconto.destroy
call super::destroy
destroy(this.cb_6)
destroy(this.cb_5)
destroy(this.tv_1)
destroy(this.cb_codice)
destroy(this.cb_descrizione)
destroy(this.dw_gruppo_sconto)
destroy(this.cb_ricerca)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.cb_3)
destroy(this.st_3)
destroy(this.em_2)
destroy(this.st_4)
destroy(this.em_3)
destroy(this.cb_4)
end on

event open;call super::open;//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "menuvoca.bmp")
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "menuvocc.bmp")
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "menufina.bmp")
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "menufinc.bmp")

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_apri.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_chiudi.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "window.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "window.bmp")

// stefanop 28/05/2010
//wf_imposta_tv()
end event

event close;call super::close;destroy ids_cat_attrezzature
destroy ids_rep_attrezzature

end event

event pc_setwindow;call super::pc_setwindow;string ls_gruppo

dw_gruppo_sconto.set_dw_key("cod_azienda")
//dw_gruppo_sconto.set_dw_key("cod_gruppo_sconto")
//dw_gruppo_sconto.set_dw_key("cod_valuta")
//dw_gruppo_sconto.set_dw_key("data_inizio_val")
//dw_gruppo_sconto.set_dw_key("progressivo")
dw_gruppo_sconto.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
iuo_dw_main = dw_gruppo_sconto
setnull(is_cod_cliente)
setnull(is_cod_valuta)
setnull(is_cod_agente)
setnull(is_cod_gruppo)

setnull(ls_gruppo)
select stringa 
into   :ls_gruppo
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LSD';
if not isnull(ls_gruppo) then em_1.text = ls_gruppo

cb_ricerca.postevent("clicked")

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_6 from commandbutton within w_gestione_gruppo_sconto
integer x = 2583
integer y = 1744
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cr&ea Cliente"
end type

event clicked;window_open_parm(w_linee_sconto_cliente, -1, dw_gruppo_sconto)

end event

type cb_5 from commandbutton within w_gestione_gruppo_sconto
integer x = 709
integer y = 20
integer width = 338
integer height = 68
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Espandi"
end type

event clicked;long ll_tvi, ll_i

tv_1.setredraw(false)
for ll_i = 1 to upperbound(il_rootitem)
//	ll_tvi = tv_1.FindItem(RootTreeItem!, il_rootitem[ll_i] )
//	tv_1.ExpandAll(ll_tvi)

	tv_1.ExpandAll(il_rootitem[ll_i])
next
if upperbound(il_rootitem) > 0 then tv_1.selectitem(il_rootitem[1])

tv_1.setredraw(true)

end event

type tv_1 from treeview within w_gestione_gruppo_sconto
integer x = 23
integer y = 100
integer width = 1371
integer height = 1828
integer taborder = 120
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 16777215
boolean border = false
boolean hasbuttons = false
boolean disabledragdrop = false
boolean hideselection = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event clicked;if handle<>0 then
	il_handle = handle
	dw_gruppo_sconto.change_dw_current( )
	parent.triggerevent("pc_retrieve")
end if
end event

event selectionchanged;if newhandle>0 then
	il_handle = newhandle
	dw_gruppo_sconto.change_dw_current( )
	parent.triggerevent("pc_retrieve")
end if
end event

type cb_codice from commandbutton within w_gestione_gruppo_sconto
integer x = 23
integer y = 20
integer width = 338
integer height = 68
integer taborder = 110
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Codice"
end type

event clicked;is_tipo_ordinamento = "C"
wf_imposta_tv()
end event

type cb_descrizione from commandbutton within w_gestione_gruppo_sconto
integer x = 366
integer y = 20
integer width = 338
integer height = 68
integer taborder = 90
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Descrizione"
end type

event clicked;is_tipo_ordinamento = "D"
wf_imposta_tv()
end event

type dw_gruppo_sconto from uo_cs_xx_dw within w_gestione_gruppo_sconto
event ue_ricerca_tv pbm_custom36
event ue_reset_tv ( )
event ue_allinea_tv ( long rowsinserted,  long rowsdeleted )
integer x = 1417
integer y = 84
integer width = 2011
integer height = 1644
integer taborder = 80
string dataobject = "d_gruppo_sconto"
boolean border = false
end type

event ue_ricerca_tv;wf_imposta_ricerca(s_cs_xx.parametri.parametro_s_1, s_cs_xx.parametri.parametro_s_2, s_cs_xx.parametri.parametro_s_3, s_cs_xx.parametri.parametro_s_4)

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_4)

end event

event ue_reset_tv();st_1.text = ""
tv_1.setredraw(false)
tv_1.deleteitem(0)
tv_1.setredraw(true)
return
end event

event ue_allinea_tv(long rowsinserted, long rowsdeleted);if rowsinserted > 0 then
	wf_inserisci_nuovo_nodo()
end if

if rowsdeleted > 0 then
	long ll_handle
	
	ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
	if ll_handle > 0 then
		tv_1.deleteitem(ll_handle)
	end if
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
string ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
       ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_cod_gruppo, ls_cod_valuta, ls_str
long   ll_progressivo, ll_cont
datetime ldt_data_inizio_val

setnull(ls_null)
ll_cont = 0
choose case getcolumnname()
	case "cod_agente"
		ls_str = i_coltext
		if not isnull(i_coltext) then
			setitem(getrow(),"sconto_1", 0)
			setitem(getrow(),"sconto_2", 0)
			setitem(getrow(),"sconto_3", 0)
			setitem(getrow(),"sconto_4", 0)
			setitem(getrow(),"sconto_5", 0)
			setitem(getrow(),"sconto_6", 0)
			setitem(getrow(),"sconto_7", 0)
			setitem(getrow(),"sconto_8", 0)
			setitem(getrow(),"sconto_9", 0)
			setitem(getrow(),"sconto_10", 0)
			dw_gruppo_sconto.object.sconto_1.protect = 1
			dw_gruppo_sconto.object.sconto_2.protect = 1
			dw_gruppo_sconto.object.sconto_3.protect = 1
			dw_gruppo_sconto.object.sconto_4.protect = 1
			dw_gruppo_sconto.object.sconto_5.protect = 1
			dw_gruppo_sconto.object.sconto_6.protect = 1
			dw_gruppo_sconto.object.sconto_7.protect = 1
			dw_gruppo_sconto.object.sconto_8.protect = 1
			dw_gruppo_sconto.object.sconto_9.protect = 1
			dw_gruppo_sconto.object.sconto_10.protect = 1
		else
			dw_gruppo_sconto.object.sconto_1.protect = 0
			dw_gruppo_sconto.object.sconto_2.protect = 0
			dw_gruppo_sconto.object.sconto_3.protect = 0
			dw_gruppo_sconto.object.sconto_4.protect = 0
			dw_gruppo_sconto.object.sconto_5.protect = 0
			dw_gruppo_sconto.object.sconto_6.protect = 0
			dw_gruppo_sconto.object.sconto_7.protect = 0
			dw_gruppo_sconto.object.sconto_8.protect = 0
			dw_gruppo_sconto.object.sconto_9.protect = 0
			dw_gruppo_sconto.object.sconto_10.protect = 0
		end if
	case "cod_livello_prod_1"
		// stefanop 28/05/2010: aggiunto flag blocco = 'N'
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_2",sqlca,&
			  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + i_coltext + "') and flag_blocco='N'" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
		
	case "cod_livello_prod_2"
		// stefanop 28/05/2010: aggiunto flag blocco = 'N'
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_3",sqlca,&
			  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + i_coltext + "') and flag_blocco='N'" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
		
	case "cod_livello_prod_3"
		// stefanop 28/05/2010: aggiunto flag blocco = 'N'
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_4",sqlca,&
			  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
			  "(cod_livello_prod_3 = '" + i_coltext + "') and flag_blocco='N'" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
		
	case "cod_livello_prod_4"
		// stefanop 28/05/2010: aggiunto flag blocco = 'N'
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
		ls_cod_livello_prod_3 = getitemstring(getrow(),"cod_livello_prod_3")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_5",sqlca,&
			  "tab_livelli_prod_5","cod_livello_prod_5","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
			  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
			  "(cod_livello_prod_4 = '" + i_coltext + "') and flag_blocco='N'" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 0
	case "cod_gruppo", "cod_valuta", "data_inizio_val", "progressivo"
		ls_cod_gruppo = getitemstring(getrow(),"cod_gruppo_sconto")
		ls_cod_valuta = getitemstring(getrow(),"cod_valuta")
		ldt_data_inizio_val = getitemdatetime(getrow(),"data_inizio_val")
		ll_progressivo = getitemnumber(getrow(),"progressivo")
		select count(*)
		into   :ll_cont
		from   gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_gruppo_sconto = :ls_cod_gruppo and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val and
				 progressivo = :ll_progressivo;
		if ll_cont > 0 then
			g_mb.messagebox("Gruppi di sconto","Condizione già inserita: variare la data di validità")
			return -1
		end if
				 
end choose
end if
end event

event pcd_new;call super::pcd_new;string ls_stringa

// stefanop 28/05/2010: aggiunto flag blocco = 'N'
f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_1",sqlca,&
	  "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
	  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'" )

dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
dw_gruppo_sconto.object.cod_livello_prod_2.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_3.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
cb_ricerca.enabled = false
dw_gruppo_sconto.object.b_ricerca_cliente.enabled = true

setnull(ls_stringa)
select stringa 
into   :ls_stringa
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';
if not isnull(ls_stringa) then setitem(getrow(),"cod_valuta", ls_stringa)

if not isnull(em_1.text) and em_1.text <> "" then setitem(getrow(),"cod_gruppo_sconto", em_1.text)
if not isnull(em_2.text) and em_2.text <> "" then setitem(getrow(),"cod_cliente", em_2.text)
if not isnull(em_3.text) and em_3.text <> "" then setitem(getrow(),"cod_agente", em_3.text)

end event

event updateend;call super::updateend;// stefanop 28/05/2010: ticket 137

// wf_imposta_tv() commento perchè inserisco il nodo manualmente

this.event post ue_allinea_tv(rowsinserted,rowsdeleted)
// ----



end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_gruppo_sconto, ls_cod_valuta
long   l_idx, ll_progressivo
datetime ldt_data_inizio_val

for l_idx = 1 to rowcount()
	// stefanop 28/05/2010: copiato da sopra
	if isnull(getitemstring(l_idx, "cod_azienda")) then
		setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	// ----

	if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		ls_cod_gruppo_sconto = getitemstring(l_idx, "cod_gruppo_sconto")
		ls_cod_valuta = getitemstring(l_idx, "cod_valuta")
		ldt_data_inizio_val = getitemdatetime(l_idx, "data_inizio_val")
		
		select max(progressivo)
		into   :ll_progressivo
		from   gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_gruppo_sconto = :ls_cod_gruppo_sconto and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val;
		if isnull(ll_progressivo) or ll_progressivo = 0 then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if
		setitem(l_idx, "progressivo", ll_progressivo)
	end if
next


end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
ws_record lstr_record

tv_1.getitem(il_handle, tvi_campo)
lstr_record = tvi_campo.data

if not isnull(lstr_record.cod_gruppo_sconto) then
	l_Error = retrieve(s_cs_xx.cod_azienda, lstr_record.cod_gruppo_sconto, lstr_record.cod_valuta, lstr_record.data_inizio_val, lstr_record.progressivo)
	if l_Error < 0 then
		PCCA.Error = c_Fatal
	end if
else
	dw_gruppo_sconto.reset()
end if	
	

end event

event pcd_view;call super::pcd_view;cb_ricerca.enabled = true
dw_gruppo_sconto.object.b_ricerca_cliente.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_ricerca.enabled = false
dw_gruppo_sconto.object.b_ricerca_cliente.enabled = true
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
			 ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_cod_gruppo, ls_cod_valuta
	long   ll_progressivo, ll_cont
	datetime ldt_data_inizio_val
	
	setnull(ls_null)
	ll_cont = 0
	ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
	ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
	ls_cod_livello_prod_3 = getitemstring(getrow(),"cod_livello_prod_3")
	ls_cod_livello_prod_4 = getitemstring(getrow(),"cod_livello_prod_4")
	ls_cod_livello_prod_5 = getitemstring(getrow(),"cod_livello_prod_5")
	
	if not isnull(ls_cod_livello_prod_1) then
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_1",sqlca,&
			  "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
			  "cod_azienda = '" + s_cs_xx.cod_azienda + "'and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "')" )

		if not isnull(ls_cod_livello_prod_2) then
			f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_2",sqlca,&
				  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
				  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
				  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
				  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "')" )
				  
			if not isnull(ls_cod_livello_prod_3) then
				f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_3",sqlca,&
					  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
					  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
					  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
					  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
					  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "')" )
					  
				if not isnull(ls_cod_livello_prod_4) then
					f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_4",sqlca,&
						  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
						  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
						  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
						  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
						  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
						  "(cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "')" )
						  
					if not isnull(ls_cod_livello_prod_5) then
						f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_5",sqlca,&
							  "tab_livelli_prod_5","cod_livello_prod_5","des_livello", &
							  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
							  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
							  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
							  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
							  "(cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "') and  " + &
							  "(cod_livello_prod_5 = '" + ls_cod_livello_prod_5 + "')" )
					end if
				end if
			end if
		end if
	end if

end if

end event

event pcd_validaterow;call super::pcd_validaterow;// stefanop: controllo campo obbligatori
int li_i

for li_i = 1 to rowcount()
	
	if isnull(getitemdatetime(li_i, "data_inizio_val")) then
		g_mb.show("OMNIA", "Impostare una data di inizio validità")
		PCCA.Error = c_Fatal
		setfocus()
		return -1
	end if

next
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_gruppo_sconto,"cod_cliente")
end choose
end event

type cb_ricerca from commandbutton within w_gestione_gruppo_sconto
integer x = 1051
integer y = 20
integer width = 338
integer height = 68
integer taborder = 100
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ricerca"
end type

event clicked;window_open_parm(w_linee_sconto_filtro,-1, dw_gruppo_sconto)


end event

type st_1 from statictext within w_gestione_gruppo_sconto
integer x = 1440
integer y = 20
integer width = 1966
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 16777215
boolean enabled = false
string text = "GESTIONE LINEE DI SCONTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_gestione_gruppo_sconto
integer x = 3017
integer y = 1744
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Agg. Clienti"
end type

type cb_2 from commandbutton within w_gestione_gruppo_sconto
integer x = 3017
integer y = 1840
integer width = 393
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica Cliente"
end type

event clicked;window_open_parm(w_linee_sconto_duplica_cliente, -1, dw_gruppo_sconto)

end event

type em_1 from editmask within w_gestione_gruppo_sconto
integer x = 1851
integer y = 1744
integer width = 251
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = stringmask!
string mask = "###"
string displaydata = ""
end type

type st_2 from statictext within w_gestione_gruppo_sconto
integer x = 1440
integer y = 1744
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Gruppo Default:"
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_gestione_gruppo_sconto
integer x = 2171
integer y = 1744
integer width = 229
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Memo"
end type

event clicked;string ls_gruppo

ls_gruppo = em_1.text
if ls_gruppo = "" or isnull(ls_gruppo) then return

delete from paramatri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and 
      cod_paramentro = 'LSD';

INSERT INTO parametri_azienda  
       ( cod_azienda,   
         flag_parametro,   
         cod_parametro,   
         des_parametro,   
         stringa)  
VALUES ( :s_cs_xx.cod_azienda,   
         'S',   
         'LSD',   
         'Linea Sconto Default',   
         :ls_gruppo)  ;
commit;
end event

type st_3 from statictext within w_gestione_gruppo_sconto
integer x = 1440
integer y = 1844
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Cliente Default:"
boolean focusrectangle = false
end type

type em_2 from editmask within w_gestione_gruppo_sconto
integer x = 1851
integer y = 1844
integer width = 251
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = stringmask!
string mask = "######"
string displaydata = ""
end type

type st_4 from statictext within w_gestione_gruppo_sconto
integer x = 2171
integer y = 1844
integer width = 411
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Agente Default:"
boolean focusrectangle = false
end type

type em_3 from editmask within w_gestione_gruppo_sconto
integer x = 2583
integer y = 1844
integer width = 251
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
maskdatatype maskdatatype = stringmask!
string mask = "######"
string displaydata = "T"
end type

type cb_4 from commandbutton within w_gestione_gruppo_sconto
boolean visible = false
integer x = 3017
integer y = 1844
integer width = 389
integer height = 80
integer taborder = 23
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = em_1.text
s_cs_xx.parametri.parametro_s_3 = em_2.text
s_cs_xx.parametri.parametro_s_4 = em_3.text
window_open(w_gruppi_sconto_nuovo, 0)
wf_imposta_tv()
end event

