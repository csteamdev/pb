﻿$PBExportHeader$w_gruppi_sconti.srw
$PBExportComments$Finestra Gestione tabella linee di sconto
forward
global type w_gruppi_sconti from w_cs_xx_principale
end type
type dw_gruppi_sconti from uo_cs_xx_dw within w_gruppi_sconti
end type
end forward

global type w_gruppi_sconti from w_cs_xx_principale
int Width=2135
int Height=1153
boolean TitleBar=true
string Title="Gruppi Sconti"
dw_gruppi_sconti dw_gruppi_sconti
end type
global w_gruppi_sconti w_gruppi_sconti

event pc_setwindow;call super::pc_setwindow;dw_gruppi_sconti.set_dw_key("cod_azienda")
dw_gruppi_sconti.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)
iuo_dw_main = dw_gruppi_sconti
end event

on w_gruppi_sconti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_gruppi_sconti=create dw_gruppi_sconti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_gruppi_sconti
end on

on w_gruppi_sconti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_gruppi_sconti)
end on

type dw_gruppi_sconti from uo_cs_xx_dw within w_gruppi_sconti
int X=23
int Y=21
int Width=2058
int Height=1001
string DataObject="d_gruppi_sconti"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

