﻿$PBExportHeader$w_gruppi_sconto_nuovo.srw
forward
global type w_gruppi_sconto_nuovo from w_cs_xx_risposta
end type
type dw_gruppo_sconto from uo_cs_xx_dw within w_gruppi_sconto_nuovo
end type
type cb_1 from commandbutton within w_gruppi_sconto_nuovo
end type
type cb_2 from commandbutton within w_gruppi_sconto_nuovo
end type
end forward

global type w_gruppi_sconto_nuovo from w_cs_xx_risposta
integer width = 2094
integer height = 1912
string title = "Nuova Condizione"
dw_gruppo_sconto dw_gruppo_sconto
cb_1 cb_1
cb_2 cb_2
end type
global w_gruppi_sconto_nuovo w_gruppi_sconto_nuovo

on w_gruppi_sconto_nuovo.create
int iCurrent
call super::create
this.dw_gruppo_sconto=create dw_gruppo_sconto
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_gruppo_sconto
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
end on

on w_gruppi_sconto_nuovo.destroy
call super::destroy
destroy(this.dw_gruppo_sconto)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_gruppo_sconto.set_dw_key("cod_azienda")
dw_gruppo_sconto.set_dw_key("cod_gruppo_sconto")
dw_gruppo_sconto.set_dw_key("cod_valuta")
dw_gruppo_sconto.set_dw_key("data_inizio_val")
dw_gruppo_sconto.set_dw_key("progressivo")
dw_gruppo_sconto.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nomodify + c_nodelete + c_newonopen,c_default)


end event

type dw_gruppo_sconto from uo_cs_xx_dw within w_gruppi_sconto_nuovo
event ue_ricerca_tv pbm_custom36
integer x = 23
integer y = 20
integer width = 2011
integer height = 1656
integer taborder = 10
string dataobject = "d_gruppo_sconto"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
string ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
       ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_cod_gruppo, ls_cod_valuta, ls_str
long   ll_progressivo, ll_cont
datetime ldt_data_inizio_val

setnull(ls_null)
ll_cont = 0
choose case getcolumnname()
	case "cod_agente"
		ls_str = i_coltext
		if not isnull(i_coltext) then
			setitem(getrow(),"sconto_1", 0)
			setitem(getrow(),"sconto_2", 0)
			setitem(getrow(),"sconto_3", 0)
			setitem(getrow(),"sconto_4", 0)
			setitem(getrow(),"sconto_5", 0)
			setitem(getrow(),"sconto_6", 0)
			setitem(getrow(),"sconto_7", 0)
			setitem(getrow(),"sconto_8", 0)
			setitem(getrow(),"sconto_9", 0)
			setitem(getrow(),"sconto_10", 0)
			dw_gruppo_sconto.object.sconto_1.protect = 1
			dw_gruppo_sconto.object.sconto_2.protect = 1
			dw_gruppo_sconto.object.sconto_3.protect = 1
			dw_gruppo_sconto.object.sconto_4.protect = 1
			dw_gruppo_sconto.object.sconto_5.protect = 1
			dw_gruppo_sconto.object.sconto_6.protect = 1
			dw_gruppo_sconto.object.sconto_7.protect = 1
			dw_gruppo_sconto.object.sconto_8.protect = 1
			dw_gruppo_sconto.object.sconto_9.protect = 1
			dw_gruppo_sconto.object.sconto_10.protect = 1
		else
			dw_gruppo_sconto.object.sconto_1.protect = 0
			dw_gruppo_sconto.object.sconto_2.protect = 0
			dw_gruppo_sconto.object.sconto_3.protect = 0
			dw_gruppo_sconto.object.sconto_4.protect = 0
			dw_gruppo_sconto.object.sconto_5.protect = 0
			dw_gruppo_sconto.object.sconto_6.protect = 0
			dw_gruppo_sconto.object.sconto_7.protect = 0
			dw_gruppo_sconto.object.sconto_8.protect = 0
			dw_gruppo_sconto.object.sconto_9.protect = 0
			dw_gruppo_sconto.object.sconto_10.protect = 0
		end if
	case "cod_livello_prod_1"
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_2",sqlca,&
			  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + i_coltext + "')" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
	case "cod_livello_prod_2"
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_3",sqlca,&
			  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + i_coltext + "')" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
	case "cod_livello_prod_3"
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_4",sqlca,&
			  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
			  "(cod_livello_prod_3 = '" + i_coltext + "')" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1
	case "cod_livello_prod_4"
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
		ls_cod_livello_prod_3 = getitemstring(getrow(),"cod_livello_prod_3")
		f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_5",sqlca,&
			  "tab_livelli_prod_5","cod_livello_prod_5","des_livello", &
			  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
			  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
			  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
			  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
			  "(cod_livello_prod_4 = '" + i_coltext + "')" )

		dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_2.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_3.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_4.protect = 0
		dw_gruppo_sconto.object.cod_livello_prod_5.protect = 0
	case "cod_gruppo", "cod_valuta", "data_inizio_val", "progressivo"
		ls_cod_gruppo = getitemstring(getrow(),"cod_gruppo_sconto")
		ls_cod_valuta = getitemstring(getrow(),"cod_valuta")
		ldt_data_inizio_val = getitemdatetime(getrow(),"data_inizio_val")
		ll_progressivo = getitemnumber(getrow(),"progressivo")
		select count(*)
		into   :ll_cont
		from   gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_gruppo_sconto = :ls_cod_gruppo and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val and
				 progressivo = :ll_progressivo;
		if ll_cont > 0 then
			g_mb.messagebox("Gruppi di sconto","Condizione già inserita: variare la data di validità")
			return -1
		end if
				 
end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
			 ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_cod_gruppo, ls_cod_valuta
	long   ll_progressivo, ll_cont
	datetime ldt_data_inizio_val
	
	if getrow() > 0 then
		setnull(ls_null)
		ll_cont = 0
		ls_cod_livello_prod_1 = getitemstring(getrow(),"cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(getrow(),"cod_livello_prod_2")
		ls_cod_livello_prod_3 = getitemstring(getrow(),"cod_livello_prod_3")
		ls_cod_livello_prod_4 = getitemstring(getrow(),"cod_livello_prod_4")
		ls_cod_livello_prod_5 = getitemstring(getrow(),"cod_livello_prod_5")
		
		if not isnull(ls_cod_livello_prod_1) then
			f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_1",sqlca,&
				  "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'and  " + &
				  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "')" )
	
			if not isnull(ls_cod_livello_prod_2) then
				f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_2",sqlca,&
					  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
					  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
					  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
					  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "')" )
					  
				if not isnull(ls_cod_livello_prod_3) then
					f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_3",sqlca,&
						  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
						  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
						  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
						  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
						  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "')" )
						  
					if not isnull(ls_cod_livello_prod_4) then
						f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_4",sqlca,&
							  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
							  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
							  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
							  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
							  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
							  "(cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "')" )
							  
						if not isnull(ls_cod_livello_prod_5) then
							f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_5",sqlca,&
								  "tab_livelli_prod_5","cod_livello_prod_5","des_livello", &
								  "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
								  "(cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "') and  " + &
								  "(cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "') and  " + &
								  "(cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "') and  " + &
								  "(cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "') and  " + &
								  "(cod_livello_prod_5 = '" + ls_cod_livello_prod_5 + "')" )
						end if
					end if
				end if
			end if
		end if
	end if
end if

end event

event pcd_new;call super::pcd_new;string ls_stringa

f_PO_LoadDDDW_DW(dw_gruppo_sconto,"cod_livello_prod_1",sqlca,&
	  "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
	  "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

dw_gruppo_sconto.object.cod_livello_prod_1.protect = 0
dw_gruppo_sconto.object.cod_livello_prod_2.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_3.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_4.protect = 1
dw_gruppo_sconto.object.cod_livello_prod_5.protect = 1

setnull(ls_stringa)
select stringa 
into   :ls_stringa
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';
if not isnull(ls_stringa) then setitem(getrow(),"cod_valuta", ls_stringa)

setitem(getrow(),"cod_gruppo_sconto", s_cs_xx.parametri.parametro_s_1)
setitem(getrow(),"cod_cliente", s_cs_xx.parametri.parametro_s_3)
setitem(getrow(),"cod_agente", s_cs_xx.parametri.parametro_s_4)

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_gruppo_sconto, ls_cod_valuta
long   l_idx, ll_progressivo
datetime ldt_data_inizio_val

for l_idx = 1 to rowcount()
   if isnull(getitemstring(l_idx, "cod_azienda")) then
      setitem(l_idx, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for l_idx = 1 to rowcount()
   if isnull(getitemnumber(l_idx, "progressivo")) or getitemnumber(l_idx, "progressivo") = 0 then
		ls_cod_gruppo_sconto = getitemstring(l_idx, "cod_gruppo_sconto")
		ls_cod_valuta = getitemstring(l_idx, "cod_valuta")
		ldt_data_inizio_val = getitemdatetime(l_idx, "data_inizio_val")
      
		select max(progressivo)
		into   :ll_progressivo
		from   gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_gruppo_sconto = :ls_cod_gruppo_sconto and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val;
		if isnull(ll_progressivo) or ll_progressivo = 0 then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if
      setitem(l_idx, "progressivo", ll_progressivo)
   end if
next


end event

event updateend;call super::updateend;close(parent)
end event

type cb_1 from commandbutton within w_gruppi_sconto_nuovo
integer x = 1669
integer y = 1704
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;parent.triggerevent("pc_save")
close(parent)
end event

type cb_2 from commandbutton within w_gruppi_sconto_nuovo
integer x = 1280
integer y = 1704
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_gruppo_sconto.resetupdate()
close(parent)
end event

