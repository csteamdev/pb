﻿$PBExportHeader$w_linee_sconto_duplica_cliente.srw
$PBExportComments$Finestra Creazione gruppo di sconto personalizzato
forward
global type w_linee_sconto_duplica_cliente from w_cs_xx_principale
end type
type dw_linee_sconto_cliente from uo_cs_xx_dw within w_linee_sconto_duplica_cliente
end type
type cb_1 from commandbutton within w_linee_sconto_duplica_cliente
end type
type cb_2 from commandbutton within w_linee_sconto_duplica_cliente
end type
end forward

global type w_linee_sconto_duplica_cliente from w_cs_xx_principale
integer width = 2231
integer height = 728
string title = "Duplicazione Gruppo Cliente"
dw_linee_sconto_cliente dw_linee_sconto_cliente
cb_1 cb_1
cb_2 cb_2
end type
global w_linee_sconto_duplica_cliente w_linee_sconto_duplica_cliente

type variables
long il_errore = 0
end variables

event pc_setwindow;call super::pc_setwindow;dw_linee_sconto_cliente.set_dw_options(sqlca, &
                                i_openparm, &
                                c_newonopen + &
										  c_nodelete + &
										  c_nomodify + &
										  c_disableCC + &
										  c_noretrieveonopen, &
                                c_default)

set_w_options(c_NoEnablePopup)

save_on_close(c_socnosave)
end event

on w_linee_sconto_duplica_cliente.create
int iCurrent
call super::create
this.dw_linee_sconto_cliente=create dw_linee_sconto_cliente
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_linee_sconto_cliente
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
end on

on w_linee_sconto_duplica_cliente.destroy
call super::destroy
destroy(this.dw_linee_sconto_cliente)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_linee_sconto_cliente,"cod_valuta",sqlca,&
//                 "tab_valute","cod_valuta","des_valuta",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_linee_sconto_cliente,"cod_agente",sqlca,&
//                 "anag_agenti","cod_agente","rag_soc_1",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//---------CLAUDIA 22/05/07 MARCO DI PTENDA DICE CHE Dà ERRORE  HO COMMENTATO LO SCRIPT 
//-----------------PERCHè LA DROP COD_GRUPPO_SCONTO IN QUESTA WINDOW NON ESISTE.
//f_PO_LoadDDDW_DW(dw_linee_sconto_cliente,"cod_gruppo_sconto",sqlca,&
//                 "tab_gruppi_sconto","cod_gruppo_sconto","des_gruppo_sconto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//

end event

type dw_linee_sconto_cliente from uo_cs_xx_dw within w_linee_sconto_duplica_cliente
integer x = 14
integer y = 12
integer width = 2158
integer height = 524
integer taborder = 20
string dataobject = "d_linee_sconto_duplica_cliente"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_str

il_errore = 0
if i_extendmode then
	choose case i_colname
		case "cod_cliente"
			select cod_cliente
			into   :ls_str
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cliente = :i_coltext;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Codice Cliente","Il cliente digitato non esiste")
				il_errore = -1
				return -1
			end if
			if not(isnull(i_coltext)) then
				setnull(ls_str)
				setitem(1, "cod_agente", ls_str)
			end if
		case "cod_agente"
			if not(isnull(i_coltext)) then
				setnull(ls_str)
				setitem(1, "cod_cliente", ls_str)
			end if
	end choose
end if
end event

event pcd_new;call super::pcd_new;// claudia 28/05/07 aggiutno perchè non visualizzava il cliente.

dw_linee_sconto_cliente.setitem(dw_linee_sconto_cliente.getrow(),"cod_cliente_origine", dw_linee_sconto_cliente.i_parentdw.getitemstring(1,"cod_cliente"))
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_linee_sconto_cliente,"cod_cliente")
end choose
end event

type cb_1 from commandbutton within w_linee_sconto_duplica_cliente
integer x = 1349
integer y = 520
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;save_on_close(c_socnosave)
close(parent)
end event

type cb_2 from commandbutton within w_linee_sconto_duplica_cliente
integer x = 1751
integer y = 520
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&pplica"
end type

event clicked;string ls_cod_cliente, ls_messaggio, ls_cod_cliente_origine
datetime ldt_data_inizio_val
uo_gruppi_sconti luo_gruppi_sconti

dw_linee_sconto_cliente.accepttext()


ls_cod_cliente_origine = dw_linee_sconto_cliente.i_parentdw.getitemstring(1,"cod_cliente")
ls_cod_cliente = dw_linee_sconto_cliente.getitemstring(1,"cod_cliente")

ldt_data_inizio_val = dw_linee_sconto_cliente.getitemdatetime(1,"data_inizio_val")
if isnull(ls_cod_cliente) or isnull(ldt_data_inizio_val) then
	g_mb.messagebox("Dati incompleti","Completare i dati con cliente, e data inizio validità!")
	return 
end if

if isnull(ls_cod_cliente_origine) or isnull(ldt_data_inizio_val) then
	g_mb.messagebox("Dati incompleti","Completare i dati con cliente di origine, e data inizio validità!")
	return 
end if

delete from gruppi_sconto
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante cancellazione linee di sconto del cliente selezionato. ~r~nDettaglio errore: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if
	
commit;
	
luo_gruppi_sconti = CREATE uo_gruppi_sconti

if luo_gruppi_sconti.fuo_duplica_gruppo_cliente(ls_cod_cliente_origine, ls_cod_cliente, ldt_data_inizio_val, ls_messaggio) <> 0 then
	rollback;
	g_mb.messagebox("Personalizzazione gruppo cliente",ls_messaggio)
else
	commit;
end if

destroy luo_gruppi_sconti

i_openparm.postevent("ue_ricerca_tv")
save_on_close(c_socnosave)
close(parent)

end event

event getfocus;call super::getfocus;dw_linee_sconto_cliente.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

