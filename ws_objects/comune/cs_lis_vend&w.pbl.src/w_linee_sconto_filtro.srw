﻿$PBExportHeader$w_linee_sconto_filtro.srw
$PBExportComments$Finestra Fiiltro su gruppi di sconto
forward
global type w_linee_sconto_filtro from w_cs_xx_principale
end type
type dw_linee_sconto_filtro from uo_cs_xx_dw within w_linee_sconto_filtro
end type
type cb_1 from commandbutton within w_linee_sconto_filtro
end type
type cb_2 from commandbutton within w_linee_sconto_filtro
end type
end forward

global type w_linee_sconto_filtro from w_cs_xx_principale
integer width = 2043
integer height = 620
string title = "Selezione"
dw_linee_sconto_filtro dw_linee_sconto_filtro
cb_1 cb_1
cb_2 cb_2
end type
global w_linee_sconto_filtro w_linee_sconto_filtro

type variables
long il_errore = 0
end variables

event pc_setwindow;call super::pc_setwindow;dw_linee_sconto_filtro.set_dw_options(sqlca, &
                                i_openparm, &
                                c_newonopen + &
										  c_nodelete + &
										  c_nomodify + &
										  c_disableCC + &
										  c_noretrieveonopen, &
                                c_default)

set_w_options(c_NoEnablePopup)

save_on_close(c_socnosave)
end event

on w_linee_sconto_filtro.create
int iCurrent
call super::create
this.dw_linee_sconto_filtro=create dw_linee_sconto_filtro
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_linee_sconto_filtro
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
end on

on w_linee_sconto_filtro.destroy
call super::destroy
destroy(this.dw_linee_sconto_filtro)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_linee_sconto_filtro,"cod_valuta",sqlca,&
                 "tab_valute","cod_valuta","des_valuta",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_linee_sconto_filtro,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_linee_sconto_filtro,"cod_gruppo_sconto",sqlca,&
                 "tab_gruppi_sconto","cod_gruppo_sconto","des_gruppo_sconto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N' ")


end event

type dw_linee_sconto_filtro from uo_cs_xx_dw within w_linee_sconto_filtro
integer x = 23
integer y = 20
integer width = 1966
integer height = 380
integer taborder = 30
string dataobject = "d_selezione_gruppo_sconto"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;string ls_str

il_errore = 0
if i_extendmode then
	choose case i_colname
		case "cod_cliente"
			select cod_cliente
			into   :ls_str
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cliente = :i_coltext;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Codice Cliente","Il cliente digitato non esiste")
				il_errore = -1
				return 1
			end if
			if not(isnull(i_coltext)) and i_coltext <> "" then
				setnull(ls_str)
				setitem(1, "cod_agente", ls_str)
			end if
		case "cod_agente"
			if not(isnull(i_coltext)) and i_coltext <> "" then
				setnull(ls_str)
				setitem(1, "cod_cliente", ls_str)
			end if
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_linee_sconto_filtro,"cod_cliente")
end choose
end event

type cb_1 from commandbutton within w_linee_sconto_filtro
integer x = 1234
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;save_on_close(c_socnosave)
close(parent)
end event

type cb_2 from commandbutton within w_linee_sconto_filtro
integer x = 1623
integer y = 420
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&pplica"
end type

event clicked;string ls_cod_cliente, ls_cod_agente, ls_cod_valuta, ls_cod_gruppo, ls_flag_supervisore

dw_linee_sconto_filtro.setcolumn(1)
dw_linee_sconto_filtro.setcolumn(2)
if il_errore <> 0 then 
	dw_linee_sconto_filtro.setfocus()
	return
end if

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_4)

ls_cod_cliente = dw_linee_sconto_filtro.getitemstring(dw_linee_sconto_filtro.getrow(),"cod_cliente")
ls_cod_agente  = dw_linee_sconto_filtro.getitemstring(dw_linee_sconto_filtro.getrow(),"cod_agente")
ls_cod_valuta  = dw_linee_sconto_filtro.getitemstring(dw_linee_sconto_filtro.getrow(),"cod_valuta")
ls_cod_gruppo  = dw_linee_sconto_filtro.getitemstring(dw_linee_sconto_filtro.getrow(),"cod_gruppo_sconto")

if not isnull(ls_cod_cliente) then
	setnull(ls_cod_agente)
elseif not isnull(ls_cod_agente) then
	setnull(ls_cod_cliente)
else
	setnull(ls_cod_agente)
	setnull(ls_cod_cliente)
end if

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_flag_supervisore = "S"
else
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
end if

if ls_flag_supervisore="N" and isnull(ls_cod_cliente) then
	g_mb.warning("Ricerca Condizioni Base concessa solo ai supervisori")
	i_openparm.postevent("ue_reset_tv")
	save_on_close(c_socnosave)
	close(parent)
	return
end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_cliente
s_cs_xx.parametri.parametro_s_2 = ls_cod_agente
s_cs_xx.parametri.parametro_s_3 = ls_cod_valuta
s_cs_xx.parametri.parametro_s_4 = ls_cod_gruppo

i_openparm.postevent("ue_ricerca_tv")
save_on_close(c_socnosave)
close(parent)
end event

event getfocus;call super::getfocus;dw_linee_sconto_filtro.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

