﻿$PBExportHeader$w_listini_clienti.srw
$PBExportComments$Finestra Gestione Listini Clienti
forward
global type w_listini_clienti from w_cs_xx_principale
end type
type dw_listini_clienti_lista from uo_cs_xx_dw within w_listini_clienti
end type
type dw_listini_clienti_det from uo_cs_xx_dw within w_listini_clienti
end type
end forward

global type w_listini_clienti from w_cs_xx_principale
integer width = 2711
integer height = 1708
string title = "Gestione Listini Clienti"
dw_listini_clienti_lista dw_listini_clienti_lista
dw_listini_clienti_det dw_listini_clienti_det
end type
global w_listini_clienti w_listini_clienti

type variables
string  is_cod_misura_ven
double id_fat_conversione_ven

end variables

event pc_setwindow;call super::pc_setwindow;dw_listini_clienti_lista.set_dw_key("cod_azienda")
dw_listini_clienti_lista.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_default, &
                                        c_default)
dw_listini_clienti_det.set_dw_options(sqlca, &
                                      dw_listini_clienti_lista, &
                                      c_sharedata + c_scrollparent, &
                                      c_default)
iuo_dw_main = dw_listini_clienti_lista

//cb_prezzo_ven.enabled=false
dw_listini_clienti_det.object.b_ricerca_cliente.visible=true
dw_listini_clienti_det.object.b_ricerca_prodotto.visible=true

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_clienti_det, &
                 "cod_prodotto", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_listini_clienti_det, &
                 "cod_cliente", &
                 sqlca, &
                 "anag_clienti", &
                 "cod_cliente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_listini_clienti_det, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_listini_clienti.create
int iCurrent
call super::create
this.dw_listini_clienti_lista=create dw_listini_clienti_lista
this.dw_listini_clienti_det=create dw_listini_clienti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_listini_clienti_lista
this.Control[iCurrent+2]=this.dw_listini_clienti_det
end on

on w_listini_clienti.destroy
call super::destroy
destroy(this.dw_listini_clienti_lista)
destroy(this.dw_listini_clienti_det)
end on

event pc_delete;call super::pc_delete;//cb_prezzo_ven.enabled=false
dw_listini_clienti_det.object.b_ricerca_cliente.enabled=false
dw_listini_clienti_det.object.b_ricerca_prodotto.enabled=false

end event

event pc_new;call super::pc_new;dw_listini_clienti_det.setitem(dw_listini_clienti_det.getrow(), "data_inizio_val", datetime(today()))
end event

type dw_listini_clienti_lista from uo_cs_xx_dw within w_listini_clienti
integer x = 23
integer y = 20
integer width = 2629
integer height = 500
integer taborder = 30
string dataobject = "d_listini_clienti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;f_listini_scaglioni()
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	string ls_cod_prodotto, ls_cod_misura_mag
//
//	
//	if this.getrow() > 0 then
//		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		
//
//      select anag_prodotti.cod_misura_mag,
//				 anag_prodotti.cod_misura_ven,
//             anag_prodotti.fat_conversione_ven
//      into   :ls_cod_misura_mag,
//				 :is_cod_misura_ven,
//             :id_fat_conversione_ven
//      from   anag_prodotti
//      where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
//             anag_prodotti.cod_prodotto = :ls_cod_prodotto;
//
//		if sqlca.sqlcode = 0 then
//	   	dw_listini_clienti_det.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + "'")
//	   	dw_listini_clienti_det.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + "'")
//		   cb_prezzo_ven.text = "Prezzo al " + is_cod_misura_ven
//		else
//			dw_listini_clienti_det.modify("st_prezzo.text='Quantità'")
//		   cb_prezzo_ven.text = "Prezzo"
//		end if
//	end if
//end if
//
end event

event pcd_modify;call super::pcd_modify;//cb_prezzo_ven.enabled=true
dw_listini_clienti_det.object.b_ricerca_cliente.enabled=false
dw_listini_clienti_det.object.b_ricerca_prodotto.enabled=false

end event

event pcd_new;call super::pcd_new;//cb_prezzo_ven.enabled=true
dw_listini_clienti_det.object.b_ricerca_cliente.enabled=true
dw_listini_clienti_det.object.b_ricerca_prodotto.enabled=true

end event

event pcd_save;call super::pcd_save;//cb_prezzo_ven.enabled=false
dw_listini_clienti_det.object.b_ricerca_cliente.enabled=false
dw_listini_clienti_det.object.b_ricerca_prodotto.enabled=false

end event

event pcd_view;call super::pcd_view;//cb_prezzo_ven.enabled=false
dw_listini_clienti_det.object.b_ricerca_cliente.enabled=false
dw_listini_clienti_det.object.b_ricerca_prodotto.enabled=false

end event

type dw_listini_clienti_det from uo_cs_xx_dw within w_listini_clienti
integer x = 23
integer y = 540
integer width = 2629
integer height = 1040
integer taborder = 40
string dataobject = "d_listini_clienti_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;//if i_extendmode then
//	string ls_cod_misura_mag
// 
//   choose case i_colname
//      case "cod_prodotto"
//         select anag_prodotti.cod_misura_mag,
//					 anag_prodotti.cod_misura_ven,
//                anag_prodotti.fat_conversione_ven
//         into   :ls_cod_misura_mag,
//					 :is_cod_misura_ven,
//                :id_fat_conversione_ven
//         from   anag_prodotti
//         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
//                anag_prodotti.cod_prodotto = :i_coltext;
//   
//   		dw_listini_clienti_det.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + "'")
//   		dw_listini_clienti_det.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + "'")
//		   cb_prezzo_ven.text = "Prezzo al " + is_cod_misura_ven
//	end choose
//end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_listini_clienti_det,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_clienti_det,"cod_prodotto")
end choose
end event

