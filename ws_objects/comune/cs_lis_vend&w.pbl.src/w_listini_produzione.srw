﻿$PBExportHeader$w_listini_produzione.srw
$PBExportComments$Finestra Listini della distinta base - (richiamata solo da listini globali azienda)
forward
global type w_listini_produzione from w_cs_xx_principale
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_listini_produzione
end type
type tv_db from treeview within w_listini_produzione
end type
type dw_folder from u_folder within w_listini_produzione
end type
type cb_1 from commandbutton within w_listini_produzione
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
end type

global type w_listini_produzione from w_cs_xx_principale
int Width=3319
int Height=1737
boolean TitleBar=true
string Title="Distinta Base"
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
tv_db tv_db
dw_folder dw_folder
cb_1 cb_1
end type
global w_listini_produzione w_listini_produzione

type variables
long il_handle
string is_cod_versione,is_cod_prodotto_finito


end variables

forward prototypes
public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore)
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
end prototypes

public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: wf_imposta_ol
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari

string  ls_cod_prodotto,ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_versione
long    ll_num_figli,ll_handle,ll_num_righe
integer li_num_priorita,li_risposta
s_chiave_distinta l_chiave_distinta

treeviewitem tvi_campo
datastore lds_righe_distinta

ll_num_figli = 1
ls_cod_versione= is_cod_versione
lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,ls_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	l_chiave_distinta.cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	
   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:l_chiave_distinta.cod_prodotto_figlio;
	ls_des_prodotto=trim(ls_des_prodotto)

	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_distinta
	tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto + " qtà:" + string(l_chiave_distinta.quan_utilizzo)

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda=:s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_figlio;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		tvi_campo.pictureindex = 3
		tvi_campo.selectedpictureindex = 3
		tvi_campo.overlaypictureindex = 3
		ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
		continue
	else
		tvi_campo.pictureindex = 2
		tvi_campo.selectedpictureindex = 2
		tvi_campo.overlaypictureindex = 2
		ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
		setnull(ls_test_prodotto_f)
		li_risposta=wf_imposta_tv(l_chiave_distinta.cod_prodotto_figlio, fi_num_livello_cor + 1,ll_handle,ls_errore)
	end if
	
next

destroy(lds_righe_distinta)

return 0
end function

public function integer wf_inizio ();string ls_errore,ls_des_prodotto
long ll_risposta
treeviewitem tvi_campo

//is_cod_versione = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_versione")
//is_cod_prodotto_finito = s_cs_xx.parametri.parametro_dw_2.getitemstring(s_cs_xx.parametri.parametro_dw_2.getrow(),"cod_prodotto")

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
l_chiave_distinta.cod_prodotto_figlio = ""
tv_db.setredraw(false)
tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta=tv_db.insertitemlast(0, tvi_campo)

wf_imposta_tv(l_chiave_distinta.cod_prodotto_padre,1,1,ls_errore)

tv_db.expandall(1)
tv_db.setredraw(true)
//ll_risposta = tv_db.FindItem(RootTreeItem! , 0)
//tv_db.SelectItem ( ll_risposta )
tv_db.triggerevent("pcd_clicked")

return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

//dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

on w_listini_produzione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.tv_db=create tv_db
this.dw_folder=create dw_folder
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_distinta_gruppi_varianti
this.Control[iCurrent+2]=tv_db
this.Control[iCurrent+3]=dw_folder
this.Control[iCurrent+4]=cb_1
end on

on w_listini_produzione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.tv_db)
destroy(this.dw_folder)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]
windowobject l_objects_4[]


l_objects_1[1] = tv_db
dw_folder.fu_AssignTab(1, "&Struttura", l_Objects_1[])
//l_objects_2[1] = dw_distinta_det
//l_objects_2[2] = cb_ricerca_figlio_dist
//l_objects_2[3] = cb_inserisci
//l_objects_2[4] = lb_elenco_campi
//l_objects_2[5] = cb_verifica
//l_objects_2[6] = st_1
//l_objects_2[7] = st_tempo
//l_objects_2[8] = cb_modifica_testo_formula
//l_objects_2[9] = cb_inserisci_formula
//dw_folder.fu_AssignTab(2, "&Dettaglio", l_Objects_2[])
//l_objects_3[1] = dw_fasi_lavorazione
//l_objects_3[2] = cb_ricerca_sfrido
//l_objects_3[3] = cb_ricerca_lavorazioni
//dw_folder.fu_AssignTab(3, "&Fasi", l_Objects_3[])
l_objects_4[1] = dw_distinta_gruppi_varianti
dw_folder.fu_AssignTab(4, "&Gruppi Varianti", l_Objects_4[])

//dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
												 
//dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_disableCC+c_disableCCinsert,c_default)													 

dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_disableCC+c_disableCCinsert,c_default)													 

//dw_fasi_lavorazione.ib_proteggi_chiavi = false
//dw_distinta_det.ib_proteggi_chiavi = false

uo_dw_main = dw_distinta_gruppi_varianti


dw_folder.fu_FolderCreate(4,4)
dw_folder.fu_SelectTab(1)


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_listini_produzione
event pcd_retrieve pbm_custom60
int X=92
int Y=141
int Width=2103
int Height=1241
int TabOrder=20
string DataObject="d_distinta_gruppi_varianti"
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre,l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_new;call super::pcd_new;LONG  l_Idx
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_prodotto_padre", l_chiave_distinta.cod_prodotto_padre)
		SetItem(l_Idx, "cod_prodotto_figlio", l_chiave_distinta.cod_prodotto_figlio)
		SetItem(l_Idx, "num_sequenza", l_chiave_distinta.num_sequenza)
		SetItem(l_Idx, "cod_versione", is_cod_versione)
   END IF
NEXT
end event

type tv_db from treeview within w_listini_produzione
int X=92
int Y=141
int Width=3132
int Height=1301
int TabOrder=30
string DragIcon="Exclamation!"
BorderStyle BorderStyle=StyleLowered!
boolean DisableDragDrop=false
string PictureName[]={"H:\Cs_xx\RISORSE\pf.bmp",&
"H:\Cs_xx\RISORSE\semil.bmp",&
"H:\Cs_xx\RISORSE\mp.bmp"}
long PictureMaskColor=553648127
long StatePictureMaskColor=553648127
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event constructor;this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\pf.bmp")
this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\semil.bmp")
this.addpicture(s_cs_xx.volume + "\cs_xx\risorse\mp.bmp")

end event

event clicked;if handle<>0 then
	il_handle = handle
//	dw_distinta_det.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
//	dw_fasi_lavorazione.Change_DW_Current( )
//	parent.triggerevent("pc_retrieve")
	dw_distinta_gruppi_varianti.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
end if
end event

type dw_folder from u_folder within w_listini_produzione
int X=23
int Y=21
int Width=3246
int Height=1461
int TabOrder=10
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event po_tabclicked;call super::po_tabclicked;choose case i_selectedtab
	case 1
		tv_db.setfocus()
		tv_db.triggerevent("clicked")

	case 2
//		wf_visual_testo_formula(dw_distinta_det.getitemstring(dw_distinta_det.getrow(), "cod_formula_quan_utilizzo"))
	case 3
		s_chiave_distinta l_chiave_distinta
		treeviewitem tvi_campo
		tv_db.GetItem ( il_handle, tvi_campo )
		l_chiave_distinta = tvi_campo.data
		string ls_test

		select cod_azienda
		into   :ls_test
		from   tes_fasi_lavorazione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:l_chiave_distinta.cod_prodotto_figlio;
		
		if sqlca.sqlcode<>100 or il_handle = 1 then
//			dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_nonew,c_default)

		else
//			dw_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
			
		end if

	case 4		
		if il_handle = 1 then
			dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_nonew + c_disableCC+c_disableCCinsert,c_default)													 
		else
			dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_disableCC+c_disableCCinsert,c_default)													 
		end if

end choose
end event

type cb_1 from commandbutton within w_listini_produzione
int X=2698
int Y=1501
int Width=247
int Height=109
int TabOrder=21
boolean BringToTop=true
string Text="none"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;is_cod_prodotto_finito = "0C0100040001"
is_cod_versione = "1"
wf_inizio()
end event

