﻿$PBExportHeader$w_listini_tree.srw
$PBExportComments$Finestra Manutenzioni
forward
global type w_listini_tree from w_cs_xx_principale
end type
type cb_print_dw from commandbutton within w_listini_tree
end type
type st_split from uo_splitbar within w_listini_tree
end type
type cb_richiama_predefinito from commandbutton within w_listini_tree
end type
type cb_predefinito from commandbutton within w_listini_tree
end type
type cb_cerca from commandbutton within w_listini_tree
end type
type dw_filtro from uo_cs_xx_dw within w_listini_tree
end type
type dw_listino from uo_std_dw within w_listini_tree
end type
type dw_linee_sconto from uo_std_dw within w_listini_tree
end type
type dw_variante_client from uo_std_dw within w_listini_tree
end type
type dw_provvigioni from uo_std_dw within w_listini_tree
end type
type tv_1 from treeview within w_listini_tree
end type
type dw_folder_search from u_folder within w_listini_tree
end type
type dw_variante from uo_std_dw within w_listini_tree
end type
type dw_vuota from uo_std_dw within w_listini_tree
end type
type ws_record from structure within w_listini_tree
end type
end forward

type ws_record from structure
	string		cod_tipo_listino_prodotto
	string		cod_valuta
	datetime		data_inizio_val
	long		progressivo
	string		des_listino
	string		codice
	string		descrizione
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_listini_tree from w_cs_xx_principale
integer width = 5134
integer height = 2284
string title = "Navigazione Listini"
cb_print_dw cb_print_dw
st_split st_split
cb_richiama_predefinito cb_richiama_predefinito
cb_predefinito cb_predefinito
cb_cerca cb_cerca
dw_filtro dw_filtro
dw_listino dw_listino
dw_linee_sconto dw_linee_sconto
dw_variante_client dw_variante_client
dw_provvigioni dw_provvigioni
tv_1 tv_1
dw_folder_search dw_folder_search
dw_variante dw_variante
dw_vuota dw_vuota
end type
global w_listini_tree w_listini_tree

type variables
boolean ib_retrieve=true, ib_fam = false
long il_handle, il_anno_registrazione, il_num_registrazione, il_livello_corrente,il_handle_modificato,il_handle_cancellato, &
     il_anno_reg_ricerca, il_num_reg_ricerca
string is_tipo_manut, is_tipo_ordinamento, is_flag_eseguito, is_sql_filtro, is_area, is_tipo_descrizione
treeviewitem tvi_campo

boolean ib_manutenzioni_schedulate = true

// stefanop 21/12/2009: ottimizzazione eliminazione nodi
boolean ib_tree_deleting = false
string is_window_name = "Navigazione Listini"

	
// EnMe 23/03/2010; variabili per ricerca listino
private string is_cod_prodotto, is_cod_cliente, is_cod_variante
private long il_coloreBK
end variables

forward prototypes
public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_tv ()
public function string wf_leggi_parent (long al_handle)
public function integer wf_inserisci_clienti (long fl_handle, string fs_cod_cliente)
public function integer wf_inserisci_prodotti (long fl_handle, string fs_cod_prodotto)
public function integer wf_carica_dati_livelli (long al_handle)
public function integer wf_inserisci_varianti (long fl_handle, string fs_cod_variante)
public subroutine wf_visualizza_listino (long fl_handle)
public function integer wf_cerca_livello_prodotto (long al_handle, ref string fs_cod_tipo_listino_prodotto, ref string fs_cod_valuta, ref datetime fdt_data_inizio_val, ref long ll_progressivo)
public function integer wf_visualizza_provvigioni_speciali (ref string fs_errore)
public function integer wf_visualizza_linee_sconto (string fs_cod_cliente, ref string fs_errore)
public subroutine wf_set_loading (boolean ab_busy)
public subroutine wf_imposta_dw (ref datawindow adw_datawindow, string as_dataobject)
end prototypes

public subroutine wf_carica_singola_registrazione (long fl_anno_registrazione, long fl_num_registrazione);
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
		case "C"
			as_sql += " and cod_cliente = '" + lstr_item.codice + "' "			
		case "P"
			as_sql += " and cod_prodotto = '" + lstr_item.codice + "' "
		case "V"
			as_sql += " and cod_prodotto_figlio = '" + lstr_item.codice + "' "
//		case "E"
//			as_sql += " and cod_area_aziendale = '" + lstr_item.codice + "' "
//		case "A"
//			as_sql += " and cod_attrezzatura = '" + lstr_item.codice + "' "
//		case "N"
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_filtro.getitemdatetime(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_filtro.getitemstring(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_filtro.getitemnumber(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'LIST';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'LIST';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'LIST');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'LIST';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_cancella_treeview ();ib_tree_deleting = true
tv_1.deleteitem(0)
ib_tree_deleting = false
end subroutine

public subroutine wf_imposta_tv ();string ls_null, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ws_record lstr_record

setnull(ls_null)

tvi_campo.expanded = false
tvi_campo.selected = false
tvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0
setpointer(HourGlass!)

tv_1.setredraw(false)


il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_livello_" + string(il_livello_corrente + 1))
	case "C"
		wf_inserisci_clienti(0, dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_cliente"))
		// caricamento clienti
	case "P"
		wf_inserisci_prodotti(0, dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_prodotto"))
		// caricamento prodotti
	case "V"
		wf_inserisci_varianti(0, "")
		// caricamento varianti
//	case "N" 
//		wf_inserisci_registrazioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public function string wf_leggi_parent (long al_handle);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

if ll_item = 0 then
	return ""
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	return lstr_item.tipo_livello	
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return ""
end function

public function integer wf_inserisci_clienti (long fl_handle, string fs_cod_cliente);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello CLIENTI
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_cliente, ls_rag_soc_1, ls_localita, ls_null, ls_appoggio, ls_sql_livello, ls_padre, &
			 ls_cod_tipo_listino_prodotto_client, ls_cod_valuta_client

datetime  ldt_data_inizio_val_client

long      ll_risposta, ll_i, ll_livello, ll_progressivo_client, ll_ret

ws_record lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1


ls_sql = "SELECT cod_cliente, rag_soc_1 FROM anag_clienti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(fs_cod_cliente)  and len(fs_cod_cliente) > 0 then
	ls_sql += " and cod_cliente = '" + fs_cod_cliente + "' "
end if

ls_sql_livello = ""
ls_padre = ""


wf_leggi_parent(fl_handle,ls_sql_livello)

// modifica viropa
if dw_filtro.getitemstring(1, "flag_visualizza_clienti") = "N" then
	ls_sql += " and ( cod_cliente in (select distinct cod_cliente from listini_vendite where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
	
	
	ll_ret = wf_cerca_livello_prodotto( fl_handle, &
													ref ls_cod_tipo_listino_prodotto_client, &
													ref ls_cod_valuta_client, &
													ref ldt_data_inizio_val_client, &
													ref ll_progressivo_client)
	
	if ll_ret > 0 and not isnull(ls_cod_tipo_listino_prodotto_client) and len(ls_cod_tipo_listino_prodotto_client) > 0 then
		ls_sql += " OR cod_cliente in ( select distinct cod_cliente from listini_prod_comune_var_client where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					" cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino_prodotto_client + "' and " + &
					" cod_valuta = '" + ls_cod_valuta_client + "' and " + &
					" data_inizio_val = '" + string(ldt_data_inizio_val_client, s_cs_xx.db_funzioni.formato_data) + "' and " + &
					" progressivo = " + string(ll_progressivo_client) + ") "
	end if
	
	ls_sql += " ) "
	
else
	// carico tutti i clienti attivi e non solo quelli che hanno una linea di sconto.
	ls_sql += " and flag_blocco = 'N' "
	
end if



if is_tipo_descrizione = "C" then
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY cod_cliente ASC"
	else
		ls_sql = ls_sql + "ORDER BY cod_cliente DESC"
	end if
else
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY rag_soc_1 ASC"
	else
		ls_sql = ls_sql + "ORDER BY rag_soc_1 DESC"
	end if
end if

declare cu_clienti_tree dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_clienti_tree;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_clienti_tree: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_clienti_tree;
	return 0
end if

do while 1=1
   fetch cu_clienti_tree into :ls_cod_cliente, 
										:ls_rag_soc_1,
										:ls_localita;
									
   if (sqlca.sqlcode = 100) then
		close cu_clienti_tree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_clienti_tree (w_listini_tree:wf_inserisci_clienti)~r~n" + sqlca.sqlerrtext)
		close cu_clienti_tree;
		return -1
	end if
	
	lb_dati = true
	setnull(lstr_record.cod_tipo_listino_prodotto  )
	setnull(lstr_record.cod_valuta  )
	setnull(lstr_record.data_inizio_val  )
	
	lstr_record.progressivo = 0
	
	lstr_record.codice = ls_cod_cliente
	lstr_record.descrizione = ls_rag_soc_1 + "(" + ls_localita + ")"
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_listino)
	lstr_record.tipo_livello = "C"
	tvi_campo.data = lstr_record
	
	if is_tipo_descrizione = "D" then
		tvi_campo.label = ls_rag_soc_1 + "(" + ls_localita + ")" + ", " +  ls_cod_cliente
	else
		tvi_campo.label = ls_cod_cliente + ", " + ls_rag_soc_1 + "(" + ls_localita + ")"
	end if

	tvi_campo.pictureindex = 3
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento CLIENTI nel TREEVIEW al livello DIVISIONE")
		close cu_clienti_tree;
		return 0
	end if

loop
close cu_clienti_tree;
return 0
end function

public function integer wf_inserisci_prodotti (long fl_handle, string fs_cod_prodotto);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_prodotto, ls_des_prodotto, ls_null, ls_appoggio, ls_sql_livello
long ll_risposta, ll_i,ll_livello
treeviewitem ltvi_campo
ws_record lstr_record
setnull(ls_null)
//
ll_livello = il_livello_corrente + 1

declare cu_prodotti_tree dynamic cursor for sqlsa;
ls_sql = "SELECT cod_prodotto, des_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_prodotto) then
	ls_sql += " and cod_prodotto = '" + fs_cod_prodotto + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

//if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_prodotto in (select cod_prodotto from listini_vendite where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
//end if

if is_tipo_descrizione = "C" then
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY cod_prodotto ASC"
	else
		ls_sql = ls_sql + "ORDER BY cod_prodotto DESC"
	end if
else
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY des_prodotto ASC"
	else
		ls_sql = ls_sql + "ORDER BY des_prodotto DESC"
	end if
end if

prepare sqlsa from :ls_sql;

open dynamic cu_prodotti_tree;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_prodotti_tree (w_listini_tree:wf_inserisci_prodotti)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1=1
   fetch cu_prodotti_tree into :ls_cod_prodotto, :ls_des_prodotto;
   if (sqlca.sqlcode = 100) then
		close cu_prodotti_tree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_prodotti_tree (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
		close cu_prodotti_tree;
		return -1
	end if
	
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	lb_dati = true

	setnull(lstr_record.cod_tipo_listino_prodotto  )
	setnull(lstr_record.cod_valuta  )
	setnull(lstr_record.data_inizio_val  )
	
	lstr_record.progressivo = 0
	
	lstr_record.codice = ls_cod_prodotto
	lstr_record.descrizione = ls_des_prodotto
	lstr_record.tipo_livello = "P"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle
	setnull(lstr_record.des_listino)

	ltvi_campo.data = lstr_record
	if is_tipo_descrizione = "D" then
		ltvi_campo.label = ls_des_prodotto + ", " + ls_cod_prodotto
	else
		ltvi_campo.label = ls_cod_prodotto + ", " + ls_des_prodotto
	end if
	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento prodotti nel TREEVIEW")
		close cu_prodotti_tree;
		return 0
	end if

	// -- stefanop 08/02/2010
//	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_livello_" + string(ll_livello + 1)) = "N" then
//		if wf_inserisci_registrazioni(ll_risposta, ls_null) = 1 then
//			tv_1.deleteitem(ll_risposta)
//		else
//			ltvi_campo.ExpandedOnce = true
//			tv_1.setitem(ll_risposta, ltvi_campo)
//		end if
//	end if
	// ----
loop
close cu_prodotti_tree;
return 0
end function

public function integer wf_carica_dati_livelli (long al_handle);ws_record lstr_item

long	ll_item

treeviewitem ltv_item


ll_item = al_handle

setnull(is_cod_cliente)
setnull(is_cod_prodotto)
setnull(is_cod_variante)

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
			
		case "C"
			is_cod_cliente = lstr_item.codice
			
		case "P"
			is_cod_prodotto = lstr_item.codice
			
		case "V"
			is_cod_variante = lstr_item.codice
			
	end choose
	
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_varianti (long fl_handle, string fs_cod_variante);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello VARIANTI
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_variante, ls_des_variante, ls_null, ls_appoggio, ls_sql_livello, ls_padre, &
          ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_figlio, ls_des_prodotto, ls_tipo_livello_variante, &
			 ls_cod_tipo_listino_prodotto_client, ls_cod_valuta_client

long      ll_risposta, ll_i, ll_livello, ll_itemparent, ll_progressivo, ll_progressivo_client, ll_ret

datetime  ldt_data_inizio_val, ldt_data_inizio_val_client

treeviewitem ltv_item

ws_record lstr_record
ws_record lstr_record_parent



setnull(ls_null)

ll_livello = il_livello_corrente + 1

wf_carica_dati_livelli( fl_handle)


ls_sql = "SELECT cod_prodotto_figlio, 'V1' as tipo_livello FROM listini_produzione WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(fs_cod_variante)  and len(fs_cod_variante) > 0 then
	ls_sql += " and cod_prodotto_figlio = '" + fs_cod_variante + "' "
end if

ls_sql_livello = ""
ls_padre = ""


// cerco il livello padre
tv_1.getitem(fl_handle,ltv_item)
lstr_record_parent = ltv_item.data

ls_cod_tipo_listino_prodotto = lstr_record_parent.cod_tipo_listino_prodotto
ls_cod_valuta = lstr_record_parent.cod_valuta
ldt_data_inizio_val = lstr_record_parent.data_inizio_val
ll_progressivo = lstr_record_parent.progressivo

if not isnull(ls_cod_tipo_listino_prodotto) and len(ls_cod_tipo_listino_prodotto) > 0 then
	ls_sql += " and cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino_prodotto + "' "
	ls_sql += " and cod_valuta = '" + ls_cod_valuta + "' "
	ls_sql += " and data_inizio_val = '" + string(ldt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_sql += " and progressivo = " + string(ll_progressivo) + " "
end if	

if not isnull(is_cod_prodotto) and not isnull(is_cod_cliente) then
	ll_ret = wf_cerca_livello_prodotto( fl_handle, &
													ref ls_cod_tipo_listino_prodotto_client, &
													ref ls_cod_valuta_client, &
													ref ldt_data_inizio_val_client, &
													ref ll_progressivo_client)
	if ll_ret > 0 then

		ls_sql += " UNION "
		ls_sql += "SELECT cod_prodotto_figlio, 'V2' as tipo_livello FROM listini_prod_comune_var_client WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	
		if not isnull(fs_cod_variante)  and len(fs_cod_variante) > 0 then
			ls_sql += " and cod_prodotto_figlio = '" + fs_cod_variante + "' "
		end if
	
		if not isnull(ls_cod_tipo_listino_prodotto_client) and len(ls_cod_tipo_listino_prodotto_client) > 0 then
			ls_sql += " and cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino_prodotto_client + "' "
			ls_sql += " and cod_valuta = '" + ls_cod_valuta_client + "' "
			ls_sql += " and data_inizio_val = '" + string(ldt_data_inizio_val_client, s_cs_xx.db_funzioni.formato_data) + "' "
			ls_sql += " and progressivo = " + string(ll_progressivo_client) + " "
		end if	
	end if
	
end if

if is_tipo_descrizione = "C" then
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY cod_prodotto_figlio ASC"
	else
		ls_sql = ls_sql + "ORDER BY cod_prodotto_figlio DESC"
	end if
else
	if is_tipo_ordinamento = "C" then
		ls_sql = ls_sql + "ORDER BY cod_prodotto_figlio ASC"
	else
		ls_sql = ls_sql + "ORDER BY cod_prodotto_figlio DESC"
	end if
end if
//
declare cu_varianti_tree dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_varianti_tree;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_varianti_tree: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_varianti_tree;
	return 0
end if

do while true
   fetch cu_varianti_tree into :ls_cod_prodotto_figlio, :ls_tipo_livello_variante;
									
   if (sqlca.sqlcode = 100) then
		close cu_varianti_tree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_varianti_tree (w_listini_tree:wf_inserisci_clienti)~r~n" + sqlca.sqlerrtext)
		close cu_varianti_tree;
		return -1
	end if
	
	lb_dati = true
	
	choose case ls_tipo_livello_variante
		case "V1"
			if isnull(ls_cod_tipo_listino_prodotto) then continue
			
			lstr_record.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
			lstr_record.cod_valuta  = ls_cod_valuta
			lstr_record.data_inizio_val  = ldt_data_inizio_val
			lstr_record.progressivo  = ll_progressivo
			
			

		case "V2"
			lstr_record.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto_client
			lstr_record.cod_valuta  = ls_cod_valuta_client
			lstr_record.data_inizio_val  = ldt_data_inizio_val_client
			lstr_record.progressivo  = ll_progressivo_client
			
		case else
			setnull(lstr_record.cod_tipo_listino_prodotto  )
			setnull(lstr_record.cod_valuta  )
			setnull(lstr_record.data_inizio_val  )
			setnull(lstr_record.progressivo  )
	end choose
	
	//lstr_record.progressivo = 0
	
	select des_prodotto 
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto_figlio;
	
	lstr_record.codice = ls_cod_prodotto_figlio
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	setnull(lstr_record.des_listino)
	lstr_record.tipo_livello = ls_tipo_livello_variante
	tvi_campo.data = lstr_record
	
	if is_tipo_descrizione = "D" then
		tvi_campo.label = ls_des_prodotto + " - " + ls_cod_prodotto_figlio
	else
		tvi_campo.label = ls_cod_prodotto_figlio + " - " + ls_des_prodotto
	end if

	tvi_campo.pictureindex = 3
	tvi_campo.selectedpictureindex = 3
	tvi_campo.overlaypictureindex = 3
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento VARIANTI nel TREEVIEW al livello DIVISIONE")
		close cu_varianti_tree;
		return 0
	end if

loop
close cu_varianti_tree;
return 0
end function

public subroutine wf_visualizza_listino (long fl_handle);boolean lb_trovato=false
string ls_sql, ls_cod_tipo_listino, ls_cod_valuta, ls_tabella_origine, ls_cod_variante, ls_errore
long ll_progressivo, ll_handle, ll_ret
datetime ldt_data_inizio_val
treeviewitem ltv_item
ws_record lws_record


dw_vuota.show()
dw_listino.hide()
dw_variante.hide()
dw_variante_client.hide()
dw_provvigioni.hide()
dw_linee_sconto.hide()

if isnull(fl_handle) or fl_handle <= 0 or ib_tree_deleting then
	return
end if

il_handle = fl_handle
ll_handle = fl_handle
tv_1.getitem(fl_handle,ltv_item)
lws_record = ltv_item.data

wf_carica_dati_livelli(il_handle )

ls_cod_tipo_listino = dw_filtro.getitemstring(1, "rs_cod_tipo_listino")
ls_cod_valuta = dw_filtro.getitemstring(1, "rs_cod_valuta")

choose case lws_record.tipo_livello
		
	case "V1" 
	
		setredraw( false )
		ls_cod_tipo_listino = lws_record.cod_tipo_listino_prodotto
		ls_cod_valuta = lws_record.cod_valuta
		ldt_data_inizio_val = lws_record.data_inizio_val
		ll_progressivo = lws_record.progressivo
		ls_cod_variante = lws_record.codice
		
		//dw_listino.hide()
		//dw_variante.show()
		//dw_variante_client.hide()
		ll_ret = dw_variante.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo, ls_cod_variante)
		if ll_ret > 0 then
			dw_vuota.hide()
			dw_variante.show()
		end if
		setredraw( true )

		return 

	case "V2" 
	
		setredraw( false )
		ls_cod_tipo_listino = lws_record.cod_tipo_listino_prodotto
		ls_cod_valuta = lws_record.cod_valuta
		ldt_data_inizio_val = lws_record.data_inizio_val
		ll_progressivo = lws_record.progressivo
		ls_cod_variante = lws_record.codice
		
		//dw_listino.hide()
		//dw_variante.hide()
		//dw_variante_client.show()
		ll_ret = dw_variante_client.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo, ls_cod_variante)
		if ll_ret > 0 then
			dw_vuota.hide()
			dw_variante_client.show()
		end if
		setredraw( true )

		return 
		
	case else
	
		ls_tabella_origine = "listini_vendite"
	
end choose


ls_sql = "select data_inizio_val, progressivo from   "+ls_tabella_origine+" " + &
			" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino + "' and " + &
			" cod_valuta = '" + ls_cod_valuta + "' "
			

if isnull(is_cod_cliente) then
	ls_sql += " and cod_cliente is null "
else
	ls_sql += " and cod_cliente = '" + is_cod_cliente + "' "
end if

if isnull(is_cod_prodotto) then
	ls_sql += " and cod_prodotto is null "
else
	ls_sql += " and cod_prodotto = '" + is_cod_prodotto + "' "
end if

ls_sql += " order by data_inizio_val DESC, progressivo DESC"

declare cu_listini_vendite dynamic cursor for sqlsa;

prepare sqlsa  from :ls_sql;

open dynamic cu_listini_vendite;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore in OPEN cursore cu_listini_vendite" + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_listini_vendite into :ldt_data_inizio_val, :ll_progressivo;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore in FETCH cursore cu_listini_vendite" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	lb_trovato = true
	exit
	
loop

close cu_listini_vendite;

if lb_trovato then
	lws_record.cod_tipo_listino_prodotto = ls_cod_tipo_listino
	lws_record.cod_valuta = ls_cod_valuta
	lws_record.data_inizio_val = ldt_data_inizio_val
	lws_record.progressivo = ll_progressivo
	ltv_item.data = lws_record
	tv_1.setitem(ll_handle,ltv_item)
	
	setredraw( false )
	//dw_listino.show()
	//dw_variante.hide()
	//dw_variante_client.hide()
	ll_ret = dw_listino.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo)
	if ll_ret > 0 then
			dw_vuota.hide()
			dw_listino.show()
		end if
	setredraw( true )
	
end if

ll_ret = wf_visualizza_provvigioni_speciali(ref ls_errore)

ll_ret = wf_visualizza_linee_sconto( is_cod_cliente, ref ls_errore)

return

end subroutine

public function integer wf_cerca_livello_prodotto (long al_handle, ref string fs_cod_tipo_listino_prodotto, ref string fs_cod_valuta, ref datetime fdt_data_inizio_val, ref long ll_progressivo);long	ll_item

treeviewitem ltv_item

ws_record lstr_item


ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	if lstr_item.tipo_livello = "P" then
		
		fs_cod_tipo_listino_prodotto = lstr_item.cod_tipo_listino_prodotto
		fs_cod_valuta = lstr_item.cod_valuta
		fdt_data_inizio_val = lstr_item.data_inizio_val	
		ll_progressivo = lstr_item.progressivo
		return ll_item
		
	end if
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return -1
end function

public function integer wf_visualizza_provvigioni_speciali (ref string fs_errore);string ls_cod_agente_1, ls_cod_tipo_listino_prodotto, ls_cod_valuta
datetime ldt_data_inizio_val
long ll_progressivo
dec{4} ld_provvigione	


if not isnull(is_cod_cliente) and not isnull(is_cod_prodotto) then

	select cod_agente_1
	into   :ls_cod_agente_1
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :is_cod_cliente;
			 
	if isnull(ls_cod_agente_1) then
		goto reset
	end if
	
	declare cu_provvigione cursor for
	select cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo
	from   provvigioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :is_cod_prodotto and
			 cod_cat_mer is null and
			 cod_cliente = :is_cod_cliente and
			 cod_categoria is null and
			 cod_agente = :ls_cod_agente_1
	order by  data_inizio_val DESC, progressivo DESC;
	
	open cu_provvigione;
	
	do while true
		fetch cu_provvigione into :ls_cod_tipo_listino_prodotto, :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cursore cu_provvigione (wf_visualizza_provvigioni_speciali).~r~n" + sqlca.sqlerrtext
			rollback;
			return -1
		end if
	
		if isnull(ls_cod_tipo_listino_prodotto) then goto reset
		
		exit		// al massimo solo 1 giro
	
	loop
	
	goto provvigioni
	
	
end if


reset:
dw_provvigioni.reset()
return 0


provvigioni:
dw_provvigioni.reset()
dw_provvigioni.insertrow(0)

select variazione_1
into   :ld_provvigione
from   provvigioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta = :ls_cod_valuta and
		 data_inizio_val = :ldt_data_inizio_val and
		 progressivo = :ll_progressivo and
		 cod_agente = :ls_cod_agente_1;

dw_provvigioni.setitem(1, "data_inizio_val", ldt_data_inizio_val)
dw_provvigioni.setitem(1, "cod_agente", ls_cod_agente_1)
dw_provvigioni.setitem(1, "provvigione_1", ld_provvigione)

dw_provvigioni.show()

return 0
end function

public function integer wf_visualizza_linee_sconto (string fs_cod_cliente, ref string fs_errore);string ls_cod_gruppo_sconto, ls_cod_valuta, ls_cod_livello_prod_1, ls_cod_livello_prod_2,ls_cod_livello_prod_3, &
       ls_cod_livello_prod_4,ls_cod_livello_prod_5, ls_des_Sconto, ls_des_provvigione, ls_des_livello, ls_sql, ls_flag_std, &
		 ls_des_gruppo_sconto
long   ll_progressivo, ll_row, ll_rows, ll_i
dec{4} ld_sconto_1,ld_sconto_2, ld_provvigione_1, ld_provvigione_2
datetime ldt_data_inizio_val_max, ldt_data_inizio_val
datastore lds_store

// indica se la linea di sconto è presonalizzata per il cliente (P) o se è standard (S)
ls_flag_std = "S"

if not isnull(is_cod_cliente) then
	
	dw_linee_sconto.reset()
	
	// cerco la linea di sconto dal cliente
	select cod_gruppo_sconto
	into :ls_cod_gruppo_sconto
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :is_cod_cliente;
			
	if sqlca.sqlcode <> 0 or isnull(ls_cod_gruppo_sconto) or ls_cod_gruppo_sconto = "" then
		g_mb.show("Il cliente " + is_cod_cliente + " non ha nessun gruppo sconto associato")
		return -1
	end if
		
	// verifico la presenza di condizioni personalizzate
	select max(data_inizio_val)
	into	 :ldt_data_inizio_val_max
	from   gruppi_sconto
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :is_cod_cliente;
			 
			 
	// impost query SQL
	ls_sql = " &
		select cod_valuta, data_inizio_val, progressivo, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, provvigione_1, provvigione_2 &
		from gruppi_sconto &
		where cod_azienda = '" + s_cs_xx.cod_azienda + "' and &
		cod_gruppo_sconto = '" + ls_cod_gruppo_sconto + "' "
		
	
	// se ci sono condizioni personalizzate le visualizzo
	if not isnull(ldt_data_inizio_val_max) and ldt_data_inizio_val_max > datetime(date("01/01/1900"),time("00:00:00") ) then

		ls_sql += " and cod_cliente='" + is_cod_cliente + "' "
		ls_flag_std = "P"
		
	else
		
		ls_sql += " and cod_cliente is null "
		ls_flag_std = "S"
		
	end if
	// ----
	
	ls_sql += " order by cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5"
	
	if not f_crea_datastore(lds_store, ls_sql) then
		return -1
	end if
		
	ll_rows = lds_store.retrieve()
	
	if ll_rows < 0 then
		destroy lds_store
		g_mb.error("Errore durante la retrieve del datastore")
		return -1
	elseif ll_rows = 0 then
		destroy lds_store
		return 0
	end if
	
	for ll_i = 1 to ll_rows
		
		ls_cod_valuta = lds_store.getitemstring(ll_i, 1)
		ldt_data_inizio_val = lds_store.getitemdatetime(ll_i, 2)
		ll_progressivo = lds_store.getitemnumber(ll_i, 3)
		ls_cod_livello_prod_1 = lds_store.getitemstring(ll_i, 4)
		ls_cod_livello_prod_2 = lds_store.getitemstring(ll_i, 5)
		ls_cod_livello_prod_3 = lds_store.getitemstring(ll_i, 6)
		ls_cod_livello_prod_4 = lds_store.getitemstring(ll_i, 7)
		ls_cod_livello_prod_5 = lds_store.getitemstring(ll_i, 8)
		ld_sconto_1 = lds_store.getitemnumber(ll_i, 9)
		ld_sconto_2 = lds_store.getitemnumber(ll_i, 10)
		ld_provvigione_1 = lds_store.getitemnumber(ll_i, 11)
		ld_provvigione_2 = lds_store.getitemnumber(ll_i, 12)
					
		if isnull(ls_cod_livello_prod_1) then
			continue
		elseif isnull(ls_cod_livello_prod_2) then
				
			select des_livello
			into   :ls_des_livello
			from   tab_livelli_prod_1
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_livello_prod_1 = :ls_cod_livello_prod_1;
				
		elseif isnull(ls_cod_livello_prod_3) then
			
			select des_livello
			into   :ls_des_livello
			from   tab_livelli_prod_2
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_livello_prod_1 = :ls_cod_livello_prod_1 and
					 cod_livello_prod_2 = :ls_cod_livello_prod_2;
				
		elseif isnull(ls_cod_livello_prod_4) then
			
			select des_livello
			into   :ls_des_livello
			from   tab_livelli_prod_3
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_livello_prod_1 = :ls_cod_livello_prod_1 and
					 cod_livello_prod_2 = :ls_cod_livello_prod_2 and
					 cod_livello_prod_3 = :ls_cod_livello_prod_3;
				
		elseif isnull(ls_cod_livello_prod_5) then
			
			select des_livello
			into   :ls_des_livello
			from   tab_livelli_prod_4
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_livello_prod_1 = :ls_cod_livello_prod_1 and
					 cod_livello_prod_2 = :ls_cod_livello_prod_2 and
					 cod_livello_prod_3 = :ls_cod_livello_prod_3 and
					 cod_livello_prod_4 = :ls_cod_livello_prod_4;
				
		else
			
			select des_livello
			into   :ls_des_livello
			from   tab_livelli_prod_5
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_livello_prod_1 = :ls_cod_livello_prod_1 and
					 cod_livello_prod_2 = :ls_cod_livello_prod_2 and
					 cod_livello_prod_3 = :ls_cod_livello_prod_3 and
					 cod_livello_prod_4 = :ls_cod_livello_prod_4 and
					 cod_livello_prod_5 = :ls_cod_livello_prod_5;
			
		end if			
			
			
			
		ll_row = dw_linee_sconto.insertrow(0)
		dw_linee_sconto.setitem(ll_row, "cod_livello_prod_1", ls_cod_livello_prod_1)
		dw_linee_sconto.setitem(ll_row, "cod_livello_prod_2", ls_cod_livello_prod_2)
		dw_linee_sconto.setitem(ll_row, "cod_livello_prod_3", ls_cod_livello_prod_3)
		dw_linee_sconto.setitem(ll_row, "cod_livello_prod_4", ls_cod_livello_prod_4)
		dw_linee_sconto.setitem(ll_row, "cod_livello_prod_5", ls_cod_livello_prod_5)
		dw_linee_sconto.setitem(ll_row, "descrizione", ls_des_livello)
		
		ls_des_sconto = ""
		ls_des_provvigione = ""
		
		if ld_sconto_1 > 0 then
			ls_des_sconto = "Sconto " + string(ld_sconto_1,"#0")
		end if
		
		if ld_sconto_2 > 0 then
			ls_des_sconto += " + " + string(ld_sconto_2,"#0")
		end if

		if ld_provvigione_1 > 0 then
			ls_des_provvigione = "Provv 1:" + string(ld_provvigione_1,"#0") + "%"
		end if
		
		if ld_provvigione_1 > 0 then
			ls_des_provvigione += " Provv 2:" + string(ld_provvigione_1,"#0") + "%"
		end if

			
			
		dw_linee_sconto.setitem(ll_row, "sconto", ls_des_sconto)
		dw_linee_sconto.setitem(ll_row, "provvigione", ls_des_provvigione)
		dw_linee_sconto.setitem(ll_row, "flag_std", ls_flag_std)
			
	next
		
	destroy lds_store
	
	// dettagli linea sconto
	select des_gruppo_sconto
	into :ls_des_gruppo_sconto
	from tab_gruppi_sconto
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_gruppo_sconto = :ls_cod_gruppo_sconto;
		
	
	dw_linee_sconto.object.t_linea_sconto.text = "Linea sconto in anagrafica " + ls_cod_gruppo_sconto + " - " + ls_des_gruppo_sconto
	// ----
	dw_linee_sconto.show()
			
	
	/* VECCHIO SCRIPT
	// se ci sono condizioni personalizzate le visualizzo
	if not isnull(ldt_data_inizio_val_max) and ldt_data_inizio_val_max > datetime(date("01/01/1900"),time("00:00:00") ) then
		
		declare cu_linee_sconto cursor for
		select cod_valuta, data_inizio_val, progressivo, cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, provvigione_1, provvigione_2
		from   gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
				  cod_cliente = :is_cod_cliente and
				  cod_gruppo_sconto = :ls_cod_gruppo_sconto
		order by cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5;
		
		open cu_linee_sconto;
		
		do while true
			
			fetch cu_linee_sconto into :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo, :ls_cod_livello_prod_1, :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4, :ls_cod_livello_prod_5, :ld_sconto_1, :ld_sconto_2, :ld_provvigione_1, :ld_provvigione_2;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in FETCH cursore cu_linee_sconto (wf_visualizza_linee_sconto)~r~n" + sqlca.sqlerrtext
				close cu_linee_sconto;
				return -1
			end if
			
			if     isnull(ls_cod_livello_prod_1) then
				continue
			elseif isnull(ls_cod_livello_prod_2) then
				
				select des_livello
				into   :ls_des_livello
				from   tab_livelli_prod_1
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_livello_prod_1 = :ls_cod_livello_prod_1;
				
			elseif isnull(ls_cod_livello_prod_3) then
				
				select des_livello
				into   :ls_des_livello
				from   tab_livelli_prod_2
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_livello_prod_1 = :ls_cod_livello_prod_1 and
				       cod_livello_prod_2 = :ls_cod_livello_prod_2;
				
			elseif isnull(ls_cod_livello_prod_4) then
				
				select des_livello
				into   :ls_des_livello
				from   tab_livelli_prod_3
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_livello_prod_1 = :ls_cod_livello_prod_1 and
				       cod_livello_prod_2 = :ls_cod_livello_prod_2 and
				       cod_livello_prod_3 = :ls_cod_livello_prod_3;
				
			elseif isnull(ls_cod_livello_prod_5) then
				
				select des_livello
				into   :ls_des_livello
				from   tab_livelli_prod_4
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_livello_prod_1 = :ls_cod_livello_prod_1 and
				       cod_livello_prod_2 = :ls_cod_livello_prod_2 and
				       cod_livello_prod_3 = :ls_cod_livello_prod_3 and
				       cod_livello_prod_4 = :ls_cod_livello_prod_4;
				
			else
				
				select des_livello
				into   :ls_des_livello
				from   tab_livelli_prod_5
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_livello_prod_1 = :ls_cod_livello_prod_1 and
				       cod_livello_prod_2 = :ls_cod_livello_prod_2 and
				       cod_livello_prod_3 = :ls_cod_livello_prod_3 and
				       cod_livello_prod_4 = :ls_cod_livello_prod_4 and
				       cod_livello_prod_5 = :ls_cod_livello_prod_5;
				
			end if			
			
			
			
			ll_row = dw_linee_sconto.insertrow(0)
			dw_linee_sconto.setitem(ll_row, "cod_livello_prod_1", ls_cod_livello_prod_1)
			dw_linee_sconto.setitem(ll_row, "cod_livello_prod_2", ls_cod_livello_prod_2)
			dw_linee_sconto.setitem(ll_row, "cod_livello_prod_3", ls_cod_livello_prod_3)
			dw_linee_sconto.setitem(ll_row, "cod_livello_prod_4", ls_cod_livello_prod_4)
			dw_linee_sconto.setitem(ll_row, "cod_livello_prod_5", ls_cod_livello_prod_5)
			dw_linee_sconto.setitem(ll_row, "descrizione", ls_des_livello)
			
			ls_des_sconto = ""
			ls_des_provvigione = ""
			
			if ld_sconto_1 > 0 then
				ls_des_sconto = "Sconto " + string(ld_sconto_1,"#0")
			end if
			
			if ld_sconto_2 > 0 then
				ls_des_sconto += " + " + string(ld_sconto_2,"#0")
			end if

			if ld_provvigione_1 > 0 then
				ls_des_provvigione = "Provv 1:" + string(ld_provvigione_1,"#0") + "%"
			end if
			
			if ld_provvigione_1 > 0 then
				ls_des_provvigione += " Provv 2:" + string(ld_provvigione_1,"#0") + "%"
			end if

			
			
			dw_linee_sconto.setitem(ll_row, "sconto", ls_des_sconto)
			dw_linee_sconto.setitem(ll_row, "provvigione", ls_des_provvigione)
			
			
		loop
		
		close cu_linee_sconto;
		
		dw_linee_sconto.show()
			
	end if*/
	
end if

return 0
end function

public subroutine wf_set_loading (boolean ab_busy);// stefanop
// imposta gli stati della finestra in occupato, quindi al lavoro o no

if ab_busy then
	this.title = is_window_name + " - caricamento in corso..."
	setpointer(HourGlass!)
else
	this.title = is_window_name
	setpointer(Arrow!)
end if
end subroutine

public subroutine wf_imposta_dw (ref datawindow adw_datawindow, string as_dataobject);adw_datawindow.dataobject = as_dataobject
adw_datawindow.SetTransObject (sqlca)
end subroutine

on w_listini_tree.create
int iCurrent
call super::create
this.cb_print_dw=create cb_print_dw
this.st_split=create st_split
this.cb_richiama_predefinito=create cb_richiama_predefinito
this.cb_predefinito=create cb_predefinito
this.cb_cerca=create cb_cerca
this.dw_filtro=create dw_filtro
this.dw_listino=create dw_listino
this.dw_linee_sconto=create dw_linee_sconto
this.dw_variante_client=create dw_variante_client
this.dw_provvigioni=create dw_provvigioni
this.tv_1=create tv_1
this.dw_folder_search=create dw_folder_search
this.dw_variante=create dw_variante
this.dw_vuota=create dw_vuota
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_print_dw
this.Control[iCurrent+2]=this.st_split
this.Control[iCurrent+3]=this.cb_richiama_predefinito
this.Control[iCurrent+4]=this.cb_predefinito
this.Control[iCurrent+5]=this.cb_cerca
this.Control[iCurrent+6]=this.dw_filtro
this.Control[iCurrent+7]=this.dw_listino
this.Control[iCurrent+8]=this.dw_linee_sconto
this.Control[iCurrent+9]=this.dw_variante_client
this.Control[iCurrent+10]=this.dw_provvigioni
this.Control[iCurrent+11]=this.tv_1
this.Control[iCurrent+12]=this.dw_folder_search
this.Control[iCurrent+13]=this.dw_variante
this.Control[iCurrent+14]=this.dw_vuota
end on

on w_listini_tree.destroy
call super::destroy
destroy(this.cb_print_dw)
destroy(this.st_split)
destroy(this.cb_richiama_predefinito)
destroy(this.cb_predefinito)
destroy(this.cb_cerca)
destroy(this.dw_filtro)
destroy(this.dw_listino)
destroy(this.dw_linee_sconto)
destroy(this.dw_variante_client)
destroy(this.dw_provvigioni)
destroy(this.tv_1)
destroy(this.dw_folder_search)
destroy(this.dw_variante)
destroy(this.dw_vuota)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag_manutenzioni_schedulate
windowobject lw_oggetti[], lw_oggetti1[], lw_oggetti2[]


//  ------------------------  imposto folder di ricerca ---------------------------

set_w_options(c_closenosave)
dw_filtro.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
//dw_filtro.insertrow(0)

lw_oggetti2[1] = tv_1
dw_folder_search.fu_assigntab(2, "Registrazioni", lw_oggetti2[])
lw_oggetti2[1] = dw_filtro
lw_oggetti2[2] = cb_cerca
lw_oggetti2[3] = cb_predefinito
lw_oggetti2[4] = cb_richiama_predefinito
dw_folder_search.fu_assigntab(1, "Filtro", lw_oggetti2[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

// -----------------------------  impostazioni relative al treeview --------------------------
tv_1.deletepictures()
tv_1.PictureHeight = 16
tv_1.PictureWidth = 16

	//---- categoria
// tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_categorie.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")

	//---- reparto
// tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_reparti.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")

	//---- area
// tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_area.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

	//---- attrezzatura
// tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_attrezzature.bmp")
// tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "manut_strumenti.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")

	//---- registrazione
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "man_reg_eseguita.bmp")
//tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "man_reg_non_eseguita.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

	//---- registrazione eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

	//---- registrazione modificata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png")


//---- registrazione periodica
// gestione icone programmazione periodo
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eseguita.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

	//---- registrazione eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

	//---- registrazione modificata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png")


// ------------------------  richiamo le impostazioni predefinite del filtro --------------------------
cb_richiama_predefinito.postevent("clicked")

dw_listino.settransobject(sqlca)
dw_variante.settransobject(sqlca)
dw_variante_client.settransobject(sqlca)


// stefanop split
st_split.uof_register(dw_folder_search, uo_splitbar.LEFT)
st_split.uof_register(dw_listino, uo_splitbar.RIGHT)
st_split.uof_register(dw_provvigioni, uo_splitbar.RIGHT)
st_split.uof_register(dw_linee_sconto, uo_splitbar.RIGHT)
st_split.uof_register(dw_variante_client, uo_splitbar.RIGHT)
st_split.uof_register(dw_variante, uo_splitbar.RIGHT)
st_split.uof_register(dw_vuota, uo_splitbar.RIGHT)

is_window_name = this.title

dw_vuota.settransobject(sqlca)
dw_vuota.insertrow(0)
dw_vuota.hide()
// ----
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_manutenzioni_1,"cod_tipo_manutenzione",sqlca,&
//                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
//                 "tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_filtro, &
                 "rs_cod_tipo_listino", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))")

f_po_loaddddw_dw(dw_filtro, &
                 "rs_cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_filtro, &
                 "rs_cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_filtro, &
                 "rs_cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event resize;/* TOLTO ANCESTOR SCRIPT */


end event

type cb_print_dw from commandbutton within w_listini_tree
integer x = 2011
integer y = 2060
integer width = 480
integer height = 80
integer taborder = 170
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Scheda"
end type

event clicked;if dw_variante.visible then
	dw_variante.print(true, true)
elseif dw_variante_client.visible then
	dw_variante_client.print(true, true)
elseif dw_listino.visible then
	dw_listino.print(true, true)
end if
end event

type st_split from uo_splitbar within w_listini_tree
integer x = 1920
integer y = 20
integer width = 41
integer height = 2120
long backcolor = 15780518
end type

event uoe_enddrag;call super::uoe_enddrag;cb_print_dw.x += ai_delta_x
end event

type cb_richiama_predefinito from commandbutton within w_listini_tree
integer x = 425
integer y = 2052
integer width = 361
integer height = 80
integer taborder = 180
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()

//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
if ib_fam then
	dw_filtro.setitem(1,"cod_area_aziendale",is_area)
end if
//----------------------------- 22/04/2004 Fine -----------------------------------

dw_filtro.postevent("ue_reset")
end event

type cb_predefinito from commandbutton within w_listini_tree
integer x = 37
integer y = 2052
integer width = 379
integer height = 80
integer taborder = 170
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Predef."
end type

event clicked;dw_filtro.accepttext()
wf_memorizza_filtro()
end event

type cb_cerca from commandbutton within w_listini_tree
integer x = 1509
integer y = 2052
integer width = 361
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_filtro.accepttext()

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_cliente")) then
	is_sql_filtro += " and cod_cliente = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_cliente") + "' "
end if

//if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cliente")) and len(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cliente")) > 0 then
//	is_sql_filtro += " and cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_divisione = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione") + "' ) "
//end if

//if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) then
//	is_sql_filtro += " and cod_reparto = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto") + "' "
//end if
//
//if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) then
//	is_sql_filtro += " and cod_cat_attrezzature = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura") + "' "
//end if

// ------------------------------------------------------------

is_tipo_ordinamento = dw_filtro.getitemstring(1,"flag_ordinamento_dati")
is_tipo_descrizione = dw_filtro.getitemstring(1,"flag_visualizza_dati")

ib_retrieve = false

wf_cancella_treeview()

wf_imposta_tv()

dw_folder_search.fu_selecttab(2)

tv_1.setfocus()

ib_retrieve = true
end event

type dw_filtro from uo_cs_xx_dw within w_listini_tree
event ue_key pbm_dwnkey
event ue_reset ( )
integer x = 46
integer y = 120
integer width = 1257
integer height = 1940
integer taborder = 160
string title = "none"
string dataobject = "d_listini_tree_filtro"
boolean border = false
boolean livescroll = true
end type

event ue_key;if key = keyenter! then
	accepttext()
	cb_cerca.triggerevent("clicked")
	postevent("ue_reset")
end if

end event

event ue_reset();dw_filtro.resetupdate()

end event

event itemchanged;string ls_null, ls_text

setnull(ls_null)
ls_text = gettext()
choose case getcolumnname()
	case "flag_livello_1"
		setitem(getrow(),"flag_livello_2", "N")
		setitem(getrow(),"flag_livello_3", "N")
		
		settaborder("flag_livello_2", 0)
		settaborder("flag_livello_3", 0)
		
		if ls_text <> "N" then
			settaborder("flag_livello_2", 80)
		end if
		
	case "flag_livello_2"
		setitem(getrow(),"flag_livello_3", "N")
		settaborder("flag_livello_3", 0)
		if ls_text <> "N" then
			settaborder("flag_livello_3", 90)
		end if
		
	
end choose

postevent("ue_reset")

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_filtro,"rs_cod_cliente")
			
	case "b_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_filtro,"rs_cod_prodotto")
end choose
end event

type dw_listino from uo_std_dw within w_listini_tree
integer x = 1998
integer y = 16
integer width = 2926
integer height = 1812
integer taborder = 130
string dataobject = "d_listini_tree_1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_linee_sconto from uo_std_dw within w_listini_tree
integer x = 2011
integer y = 1400
integer width = 3063
integer height = 640
integer taborder = 130
boolean bringtotop = true
string dataobject = "d_listini_tree_5"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_variante_client from uo_std_dw within w_listini_tree
integer x = 2021
integer y = 24
integer width = 2912
integer height = 1536
integer taborder = 170
string dataobject = "d_listini_tree_3"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_provvigioni from uo_std_dw within w_listini_tree
integer x = 2011
integer y = 1324
integer width = 2912
integer height = 80
integer taborder = 130
boolean bringtotop = true
string dataobject = "d_listini_tree_4"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type tv_1 from treeview within w_listini_tree
integer x = 64
integer y = 132
integer width = 1806
integer height = 2000
integer taborder = 120
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"Custom093!","C:\CS_70\framework\RISORSE\Foglio2_Dim_Orig.bmp","Start!","Picture!","Custom096!","Custom024!"}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;string ls_null, ls_tipo_livello
long ll_livello = 0
treeviewitem ltv_item
ws_record lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 or ib_tree_deleting then
	return 0
end if

getitem(handle,ltv_item)

wf_set_loading(true)

setredraw(false)
wf_visualizza_listino(handle)
setredraw(true)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 3 then
	// controlla cosa c'è al livello successivo ???
	
	ls_tipo_livello = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_livello_" + string(ll_livello + 1))
	
	choose case ls_tipo_livello
		case "C"
			// caricamento divisioni
			if wf_inserisci_clienti(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_cliente")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "P"
			 //caricamento prodotti
			if wf_inserisci_prodotti(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"rs_cod_prodotto")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "V"
			 //caricamento varianti
			if wf_inserisci_varianti(handle, "") = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
	
end if


wf_set_loading(false)

commit;
end event

event selectionchanged;boolean lb_trovato=false
string ls_sql, ls_cod_tipo_listino, ls_cod_valuta
long ll_progressivo, ll_handle
datetime ldt_data_inizio_val
treeviewitem ltv_item
ws_record lws_record

if isnull(newhandle) or newhandle <= 0 or ib_tree_deleting then
	return 0
end if

wf_visualizza_listino(newhandle)


return

end event

event rightclicked;//treeviewitem ltv_item
//ws_record lws_record
//
//getitem(handle,ltv_item)
//lws_record = ltv_item.data
//
//if lws_record.tipo_livello = "M" then
//	
//	// non sono im modifica o nuovo
//	
//		m_manutenzioni_menu lm_menu
//		lm_menu = create m_manutenzioni_menu
//		
//		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
//		destroy lm_menu
//	
//end if
end event

type dw_folder_search from u_folder within w_listini_tree
integer x = 18
integer y = 24
integer width = 1879
integer height = 2136
integer taborder = 40
end type

event resize;call super::resize;// codifico resize per gli elementi figli

tv_1.width = width - 100
dw_filtro.width = width - 100

cb_cerca.x = width - cb_cerca.width - 20
end event

type dw_variante from uo_std_dw within w_listini_tree
integer x = 1998
integer y = 16
integer width = 2935
integer height = 1516
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_listini_tree_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_vuota from uo_std_dw within w_listini_tree
integer x = 1989
integer width = 2926
integer height = 1520
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_listini_tree_vuota"
boolean hscrollbar = true
boolean border = false
boolean livescroll = false
borderstyle borderstyle = stylebox!
end type

