﻿$PBExportHeader$w_listini_ven_prod.srw
$PBExportComments$Variazione Prezzi (Gestione Veloce Listini)
forward
global type w_listini_ven_prod from w_cs_xx_principale
end type
type cbx_tutti from checkbox within w_listini_ven_prod
end type
type cb_duplica_prod from commandbutton within w_listini_ven_prod
end type
type gb_2 from groupbox within w_listini_ven_prod
end type
type cbx_data_odierna from checkbox within w_listini_ven_prod
end type
type cb_salva from commandbutton within w_listini_ven_prod
end type
type cb_cancella from commandbutton within w_listini_ven_prod
end type
type cb_nuovo from commandbutton within w_listini_ven_prod
end type
type cb_copia_list from commandbutton within w_listini_ven_prod
end type
type cb_duplica_da from commandbutton within w_listini_ven_prod
end type
type cb_duplica_in from commandbutton within w_listini_ven_prod
end type
type cb_prodotti from commandbutton within w_listini_ven_prod
end type
type gb_1 from groupbox within w_listini_ven_prod
end type
type cbx_provvigioni from checkbox within w_listini_ven_prod
end type
type cbx_registro from checkbox within w_listini_ven_prod
end type
type cbx_bloccati from checkbox within w_listini_ven_prod
end type
type dw_prodotti_lista from uo_cs_xx_dw within w_listini_ven_prod
end type
type dw_listini_ven_prodotti from datawindow within w_listini_ven_prod
end type
type dw_listini_ven_prodotti2 from datawindow within w_listini_ven_prod
end type
end forward

global type w_listini_ven_prod from w_cs_xx_principale
integer x = 73
integer y = 400
integer width = 3602
integer height = 2024
string title = "Variazione Prezzi"
cbx_tutti cbx_tutti
cb_duplica_prod cb_duplica_prod
gb_2 gb_2
cbx_data_odierna cbx_data_odierna
cb_salva cb_salva
cb_cancella cb_cancella
cb_nuovo cb_nuovo
cb_copia_list cb_copia_list
cb_duplica_da cb_duplica_da
cb_duplica_in cb_duplica_in
cb_prodotti cb_prodotti
gb_1 gb_1
cbx_provvigioni cbx_provvigioni
cbx_registro cbx_registro
cbx_bloccati cbx_bloccati
dw_prodotti_lista dw_prodotti_lista
dw_listini_ven_prodotti dw_listini_ven_prodotti
dw_listini_ven_prodotti2 dw_listini_ven_prodotti2
end type
global w_listini_ven_prod w_listini_ven_prod

type variables
long    il_list_da, il_prod_da

dec{6}  id_prezzo_acq

string  is_nome_file, is_flag, is_std

boolean ib_inmodifica, ib_prod, ib_speed, ib_list

datastore id_listini_prod


end variables

forward prototypes
public function integer wf_registro_listini (string fs_messaggio)
public function integer wf_salvamodifica (long fl_riga_prod, ref string fs_error)
public function integer wf_carica_listini ()
public subroutine wf_ricerca_prodotto ()
end prototypes

public function integer wf_registro_listini (string fs_messaggio);string  ls_data
integer li_file 


li_file = fileopen(is_nome_file,linemode!,Write!,LockWrite!,Append!)

if li_file = -1 then
	g_mb.messagebox("Registro Listini","Errore durante l'apertura del file registro.~nVerificare il percorso indicato nel parametro aziendale FLI (ad esempio c:\temp\listino.txt)")
	return -1
end if

ls_data = string(today(),"dd/mm/yyyy")
filewrite(li_file, ls_data)
filewrite(li_file, fs_messaggio)
fileclose(li_file)

return 0

end function

public function integer wf_salvamodifica (long fl_riga_prod, ref string fs_error);string   ls_cod_tipo_listino_prodotto , ls_cod_valuta , ls_cod_prodotto, ls_des_listino_vendite , &
		   ls_flag_sconto_mag_prezzo_1 , ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, &
		   ls_flag_origine_prezzo_1, ls_str, ls_messaggio, ls_flag_blocco, ls_des_prodotto, &
			ls_cod_comodo, ls_cod_misura_mag, ls_cod_iva, ls_novita, ls_test, ls_valuta_key, ls_tipo_key, &
			ls_modifica
			
dec{6}   ld_provv , ld_lit_x_unita_ven , ld_prezzo_acquisto , ld_ricarico, ld_variazione_1, &
			ld_pz_ven, ld_pezzi_collo

datetime ldt_data_key, ldt_data

long     ll_progressivo, ll_i, ll_num, ll_num_righe, ll_riga

integer  li_risp, li_col

dwitemstatus l_status


dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

if not ib_inmodifica then
	return 0
end if

if ib_list then

	for ll_i = 1 to dw_listini_ven_prodotti.rowcount()
		
		if ll_i = 0 then 
			fs_error = "Errore: perso il riferimento della riga modificata"
			return -1
		end if
		
		ls_cod_tipo_listino_prodotto = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_cod_tipo_listino_prodotto")
		ls_tipo_key = dw_listini_ven_prodotti.getitemstring(ll_i, "tipo_key")
		ls_cod_valuta = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_cod_valuta")
		ls_valuta_key = dw_listini_ven_prodotti.getitemstring(ll_i, "valuta_key")
		ls_des_listino_vendite = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_des_listino_vendite")
		ld_provv = dw_listini_ven_prodotti.getitemnumber(ll_i, "rd_provv")
		ld_ricarico = dw_listini_ven_prodotti.getitemnumber(ll_i, "rd_ricarico")
		ld_variazione_1 = dw_listini_ven_prodotti.getitemnumber(ll_i, "rd_variazione_1")
		ld_lit_x_unita_ven = dw_listini_ven_prodotti.getitemnumber(ll_i, "rd_lit_x_unita_ven")
		ld_prezzo_acquisto = dw_listini_ven_prodotti2.getitemnumber(1, "rd_prezzo_acquisto")
		ls_flag_blocco = dw_listini_ven_prodotti2.getitemstring(1, "rs_flag_blocco")
		ll_progressivo = dw_listini_ven_prodotti.getitemnumber(ll_i, "rl_progressivo")
		ldt_data = dw_listini_ven_prodotti.getitemdatetime(ll_i, "rdt_data_inizio_val")
		ldt_data_key = dw_listini_ven_prodotti.getitemdatetime(ll_i, "data_key")
		ls_cod_prodotto = dw_prodotti_lista.getitemstring(fl_riga_prod, "cod_prodotto")
		ls_flag_sconto_mag_prezzo_1 = dw_listini_ven_prodotti.getitemString(ll_i,"rs_flag_sconto_mag_prezzo_1") 
		ls_modifica = dw_listini_ven_prodotti.getitemString(ll_i,"modifica")
		ls_flag_sconto_a_parte='S'
		ls_flag_tipo_scaglioni='Q'
		
		if cbx_data_odierna.checked then
			ldt_data = datetime(today(),00:00:00)
		elseif ls_modifica <> "S" then
			continue
		end if
		
		if isnull(ls_flag_sconto_mag_prezzo_1) then
			
			fs_error = "Inserire il ricarico o il prezzo di vendita!"
			return -1
			
		else
			
			if ls_flag_sconto_mag_prezzo_1 = 'M' then
				ld_variazione_1 = ld_ricarico
				ls_flag_origine_prezzo_1 = 'A'
			elseif ls_flag_sconto_mag_prezzo_1 = 'P' then		
				ls_flag_origine_prezzo_1 = 'N'			
			end if
			
			if ((ld_provv=0 or isnull(ld_provv)) and ld_variazione_1<>0) then
				ld_provv = 100 * ld_lit_x_unita_ven/ld_variazione_1
			end if		
			
		end if
		
		if ld_provv > 100 then
			li_risp = g_mb.messagebox("Errore Variazione Prezzi in riga " + string(ll_i),"Provvigione maggiore del 100% !",stopsign!)
			fs_error = "Annullamento Salvataggio"
			return -1
		end if
		
		if cbx_provvigioni.checked = true and ld_provv > 0 then
			
			select count(*)
			into   :ll_num
			from   provvigioni
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
					 cod_valuta = :ls_cod_valuta and
					 data_inizio_val = :ldt_data and
					 cod_prodotto = :ls_cod_prodotto		       
			using  sqlca;
			
			if sqlca.sqlcode < 0 then
				fs_error = "Errore nella select di provvigioni: " + sqlca.sqlerrtext
				return -1
			end if	
				
			if (ll_num = 0) or isnull(ll_num) then
				
				select max(progressivo)
				into   :ll_num
				from   provvigioni
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
						 cod_valuta = :ls_cod_valuta and
						 data_inizio_val = :ldt_data;
						 
				if sqlca.sqlcode < 0 then
					fs_error = "Errore nella select di provvigioni: " + sqlca.sqlerrtext
					return -1
				end if
				
				if isnull(ll_num) then
					ll_num = 0
				end if
				
				ll_num++
				
				insert
				into   provvigioni
						 (cod_azienda,
						 cod_tipo_listino_prodotto,
						 cod_valuta,
						 data_inizio_val,
						 progressivo,
						 cod_prodotto,
						 flag_tipo_scaglioni,					 
						 variazione_1,
						 scaglione_1)
				values (:s_cs_xx.cod_azienda,
						 :ls_cod_tipo_listino_prodotto,
						 :ls_cod_valuta,
						 :ldt_data,
						 :ll_num,
						 :ls_cod_prodotto,
						 'Q',
						 :ld_provv,
						 99999999)
				using  sqlca ;
				
				if sqlca.sqlcode < 0 then
					fs_error = "Errore salvataggio provvigioni in tabella provvigioni " + sqlca.SQLErrText
					rollback;
					return -1
				end if
			
			else
				
				update provvigioni
				set    variazione_1 = :ld_provv
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
						 cod_valuta = :ls_cod_valuta and
						 data_inizio_val = :ldt_data and
						 cod_prodotto = :ls_cod_prodotto
				using  sqlca;
				
				if sqlca.sqlcode < 0 then
					fs_error = "Errore salvataggio provvigioni in tabella provvigioni " + sqlca.sqlerrtext
					rollback;
					return -1
				end if
				
			end if
			
		end if
	
		if ll_progressivo > 0 then
			
			delete
			from   listini_vendite 
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 cod_tipo_listino_prodotto = :ls_tipo_key and
					 cod_valuta = :ls_valuta_key and
					 data_inizio_val = :ldt_data_key and
					 progressivo = :ll_progressivo;
				
			if sqlca.sqlcode < 0 then
				fs_error = "Errore in salvataggio listino in listini_vendite " + sqlca.sqlerrtext
				rollback;
				return -1
			end if
			
		end if
			
		setnull(ll_progressivo)
		
		select max(progressivo)
		into   :ll_progressivo
		from   listini_vendite
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data;
				
		if isnull(ll_progressivo) then
			ll_progressivo = 0
		end if
		
		ll_progressivo ++
		
		insert
		into   listini_vendite
				 (cod_azienda,
				 cod_prodotto,
				 cod_tipo_listino_prodotto,
				 cod_valuta,
				 data_inizio_val,
				 progressivo,
				 des_listino_vendite, 
				 flag_sconto_a_parte,
				 flag_tipo_scaglioni,
				 flag_sconto_mag_prezzo_1,
				 scaglione_1,
				 variazione_1,
				 flag_origine_prezzo_1)
		values (:s_cs_xx.cod_azienda,
				 :ls_cod_prodotto,
				 :ls_cod_tipo_listino_prodotto,
				 :ls_cod_valuta,
				 :ldt_data,
				 :ll_progressivo,
				 :ls_des_listino_vendite,
				 :ls_flag_sconto_a_parte,
				 :ls_flag_tipo_scaglioni,
				 :ls_flag_sconto_mag_prezzo_1,
				 99999999,
				 :ld_variazione_1,
				 :ls_flag_origine_prezzo_1)
		using  sqlca;
		
		if sqlca.sqlcode < 0 then
			fs_error = "Errore salvataggio in listini_vendite " + sqlca.sqlerrtext
			rollback;
			return -1
		end if
		
		if cbx_registro.checked then
			
			ls_messaggio = " "	
			l_status = dw_listini_ven_prodotti.getitemstatus(ll_i, 0, Primary!)
			
			if l_status = DataModified! or l_status = NewModified! then
				
				ls_messaggio = ls_messaggio + "~t" + ls_cod_prodotto
				ls_messaggio = ls_messaggio + "~t" + ls_cod_tipo_listino_prodotto
				ls_messaggio = ls_messaggio + "~t" + string(ldt_data)
				ls_messaggio = ls_messaggio + "~t" + ls_cod_valuta
				
				if not isnull(ls_des_listino_vendite) then
					ls_messaggio = ls_messaggio + "~t" + ls_des_listino_vendite
				else
					ls_messaggio = ls_messaggio + "~t"
				end if
				
				if not isnull(ld_prezzo_acquisto) then
					ls_messaggio = ls_messaggio + "~t" + string(ld_prezzo_acquisto)
				else
					ls_messaggio = ls_messaggio + "~t"
				end if
				
				if not isnull(ld_ricarico) and ls_flag_sconto_mag_prezzo_1 ='M' then
					ls_messaggio = ls_messaggio + "~t" + string(ld_ricarico)
				else
					ls_messaggio = ls_messaggio + "~t"
				end if
				
				if not isnull(ld_variazione_1) and ls_flag_sconto_mag_prezzo_1 ='P' then
					ls_messaggio = ls_messaggio + "~t" + string(ld_variazione_1)
				else
					ls_messaggio = ls_messaggio + "~t"
				end if
				
				if cbx_provvigioni.checked = true then
					
					if not isnull(ld_provv) then
						ls_messaggio = ls_messaggio + "~t" + string(ld_provv)
					else
						ls_messaggio = ls_messaggio + "~t"
					end if
					
					if not isnull(ld_lit_x_unita_ven) then
						ls_messaggio = ls_messaggio + "~t" + string(ld_lit_x_unita_ven)
					else
						ls_messaggio = ls_messaggio + "~t"
					end if
					
				end if
				
				li_risp = wf_registro_listini(ls_messaggio)
				
				if li_risp < 0 then
					g_mb.messagebox("Variazioni Prezzi","Errore nell'aggiornamento file Registro listini")
				end if	
				
			end if
			
		end if
		
	next
	
end if

if ib_speed then

	ls_cod_prodotto = dw_prodotti_lista.getitemstring(fl_riga_prod,"cod_prodotto")
	ls_novita = dw_listini_ven_prodotti2.getitemstring(1,"novita")
	
	setnull(ls_test)
		
	select cod_prodotto
	into   :ls_test
	from   anag_prodotti_novita
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		
		g_mb.messagebox("APICE","Errore in lettura novità SPEED da anag_prodotti_novita: " + sqlca.sqlerrtext)
		
	elseif sqlca.sqlcode = 100 then
		
		if ls_novita = "S" then
			
			insert
			into   anag_prodotti_novita
					 (cod_azienda,
					 cod_prodotto)
			values (:s_cs_xx.cod_azienda,
					 :ls_cod_prodotto);
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in inserimento novità SPEED in anag_prodotti_novita: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
		
		end if
		
	elseif sqlca.sqlcode = 0 and not isnull(ls_test) then
		
		if ls_novita = "N" then
			
			delete
			from   anag_prodotti_novita
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in cancellazione novità SPEED da anag_prodotti_novita: " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
		
		end if
		
	end if
	
end if

if ib_prod then

	ls_cod_prodotto = dw_prodotti_lista.getitemstring(fl_riga_prod,"cod_prodotto")
	ld_prezzo_acquisto = dw_listini_ven_prodotti2.getitemnumber(1,"rd_prezzo_acquisto")
	ls_flag_blocco = dw_listini_ven_prodotti2.getitemstring(1,"rs_flag_blocco")
	ls_des_prodotto = dw_listini_ven_prodotti2.getitemstring(1,"descrizione")
	ls_cod_comodo = dw_listini_ven_prodotti2.getitemstring(1,"cod_comodo")
	ls_cod_misura_mag = dw_listini_ven_prodotti2.getitemstring(1,"um_mag")
	ld_pezzi_collo = dw_listini_ven_prodotti2.getitemnumber(1,"pezzi_collo")
	ls_cod_iva = dw_listini_ven_prodotti2.getitemstring(1,"cod_iva")
	
	update anag_prodotti 
	set    prezzo_acquisto = :ld_prezzo_acquisto,
			 flag_blocco = :ls_flag_blocco,
			 des_prodotto = :ls_des_prodotto,
			 cod_comodo = :ls_cod_comodo,
			 cod_misura_mag = :ls_cod_misura_mag,
			 pezzi_collo = :ld_pezzi_collo,
			 cod_iva = :ls_cod_iva
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto
	using  sqlca;
	
	if sqlca.sqlcode < 0 then
		fs_error = "Errore salvataggio in anagrafica prodotti " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	dw_prodotti_lista.setitem(fl_riga_prod,"prezzo_acquisto",ld_prezzo_acquisto)
	dw_prodotti_lista.setitem(fl_riga_prod,"des_prodotto",ls_des_prodotto)
	dw_prodotti_lista.setitem(fl_riga_prod,"flag_blocco",ls_flag_blocco)
	dw_prodotti_lista.setitem(fl_riga_prod,"pezzi_collo",ld_pezzi_collo)
	
	if ls_flag_blocco = "S" and cbx_bloccati.checked = false then
		dw_prodotti_lista.deleterow(fl_riga_prod)
	end if

end if

commit;

wf_carica_listini()

ib_inmodifica = false
ib_list = false
ib_prod = false
ib_speed = false

return 0
end function

public function integer wf_carica_listini ();long     ll_errore, ll_selezionato, ll_num_listini_prod, ll_i, ll_progressivo

string   ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta, &
		   ls_flag_sconto_mag_prezzo_1, ls_flag_origine_prezzo_1, ls_des_listino_vendite, &
		   ls_messaggio, ls_flag_blocco, ls_cod_iva, ls_novita, ls_sql
			
datetime ldt_data_inizio_val, ldt_data_inizio_val_suc, ldt_data_test, ldt_data_succ

dec{6}   ld_prezzo_acquisto, ld_provvigione_1, ld_variazione_1, ld_prezzo_1, &
		   ld_lit_x_unita_ven, ld_prezzo_arrotondato, ld_nullo, ld_ricarico, ld_pezzi_collo
			
string   ls_cod_tipo_listino_suc , ls_cod_valuta_suc, ls_des_prodotto, ls_cod_comodo, &
			ls_cod_misura_mag, ls_valuta_test, ls_valuta_succ, ls_tipo_test, ls_tipo_succ
			
boolean  lb_provv
			
uo_condizioni_cliente luo_1


setnull(ld_nullo)

lb_provv = false

ll_num_listini_prod = dw_listini_ven_prodotti.RowCount()
dw_listini_ven_prodotti.rowsdiscard(1,ll_num_listini_prod,Primary!)
dw_listini_ven_prodotti2.rowsdiscard(1,1,Primary!)
dw_listini_ven_prodotti2.insertrow(0)

ll_selezionato = dw_prodotti_lista.getselectedrow(0)

if ll_selezionato = 0 then
	return -1
end if

ls_cod_prodotto = dw_prodotti_lista.getitemstring(ll_selezionato, "cod_prodotto")

select prezzo_acquisto,
		 des_prodotto,
		 cod_comodo,
		 cod_misura_mag,
		 pezzi_collo,
		 flag_blocco,
		 cod_iva
into   :ld_prezzo_acquisto,
		 :ls_des_prodotto,
		 :ls_cod_comodo,
		 :ls_cod_misura_mag,
		 :ld_pezzi_collo,
		 :ls_flag_blocco,
		 :ls_cod_iva
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in lettura dati prodotto da anag_prodotti: " + sqlca.sqlerrtext)
end if

setnull(ls_novita)

select cod_prodotto
into   :ls_novita
from   anag_prodotti_novita
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura novità SPEED da anag_prodotti_novita: " + sqlca.sqlerrtext)
elseif sqlca.sqlcode = 100 then
	ls_novita = "N"
elseif sqlca.sqlcode = 0 and not isnull(ls_novita) then
	ls_novita = "S"
end if

dw_prodotti_lista.setitem(ll_selezionato,"prezzo_acquisto",ld_prezzo_acquisto)
dw_prodotti_lista.setitem(ll_selezionato,"flag_blocco",ls_flag_blocco)

dw_listini_ven_prodotti2.setItem(1,"rd_prezzo_acquisto",ld_prezzo_acquisto)
dw_listini_ven_prodotti2.setItem(1,"descrizione",ls_des_prodotto)
dw_listini_ven_prodotti2.setItem(1,"cod_comodo",ls_cod_comodo)
dw_listini_ven_prodotti2.setItem(1,"um_mag",ls_cod_misura_mag)
dw_listini_ven_prodotti2.setItem(1,"pezzi_collo",ld_pezzi_collo)
dw_listini_ven_prodotti2.setItem(1,"rs_flag_blocco",ls_flag_blocco)
dw_listini_ven_prodotti2.setItem(1,"cod_iva",ls_cod_iva)
dw_listini_ven_prodotti2.setItem(1,"novita",ls_novita)

dw_prodotti_lista.resetupdate()

ls_sql = &
"select cod_tipo_listino_prodotto, " + &
"       cod_valuta, " + &
"		  data_inizio_val, " + &
"		  progressivo, " + &
"	     cod_prodotto, " + &
"		  flag_sconto_mag_prezzo_1, " + &
"		  variazione_1, " + &
"		  des_listino_vendite " + &
"from   listini_vendite " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"	     cod_prodotto = '" + ls_cod_prodotto + "' and " + &
"		  cod_cliente is null "

if cbx_tutti.checked = false then
	ls_sql = ls_sql + " and data_inizio_val <= '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "'"
end if

declare cur_list dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cur_list;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore cur_list: " + sqlca.sqlerrtext)
	return -1
end if

ll_i = 0

do while true
	
	fetch cur_list
	into  :ls_cod_tipo_listino_prodotto,
			:ls_cod_valuta,
			:ldt_data_inizio_val, 
			:ll_progressivo,
			:ls_cod_prodotto,
			:ls_flag_sconto_mag_prezzo_1,
			:ld_variazione_1,
			:ls_des_listino_vendite;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore cur_list: " + sqlca.sqlerrtext)
		close cur_list;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	choose case ls_flag_sconto_mag_prezzo_1
			
		case 'P'
			
			ld_prezzo_1 = ld_variazione_1
			
			if ld_prezzo_acquisto = 0 then
				ld_ricarico = 0
			else
				ld_ricarico = (ld_variazione_1 - ld_prezzo_acquisto) * 100 / ld_prezzo_acquisto
			end if
			
		case 'M'
			
			ld_prezzo_1 = ld_prezzo_acquisto + (ld_prezzo_acquisto * ld_variazione_1 / 100)
			
			ld_ricarico = ld_variazione_1
			
		case else
			
			continue
			
	end choose
	
	luo_1 = create uo_condizioni_cliente
	
	luo_1.uof_arrotonda_prezzo_decimal(ld_prezzo_1,ls_cod_valuta,ld_prezzo_arrotondato,ls_messaggio)
	
	destroy luo_1
	
	ld_ricarico = round(ld_ricarico,2)
			
	ld_provvigione_1 = 0
	
	select variazione_1
	into :ld_provvigione_1
	from provvigioni
	where cod_azienda=:s_cs_xx.cod_azienda
		and cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto
		and cod_valuta = :ls_cod_valuta
		and data_inizio_val = :ldt_data_inizio_val
		and cod_prodotto=:ls_cod_prodotto;
		
	if not isnull(ld_provvigione_1) and ld_provvigione_1 <> 0 then
		lb_provv = true
	end if
	
	ll_i = dw_listini_ven_prodotti.InsertRow(0) 
	dw_listini_ven_prodotti.SetRow(ll_i)
	dw_listini_ven_prodotti.setItem(ll_i, "rs_cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
	dw_listini_ven_prodotti.setItem(ll_i, "tipo_key", ls_cod_tipo_listino_prodotto)
	dw_listini_ven_prodotti.setItem(ll_i, "rs_cod_valuta", ls_cod_valuta)
	dw_listini_ven_prodotti.setItem(ll_i, "valuta_key", ls_cod_valuta)
	dw_listini_ven_prodotti.setItem(ll_i, "rdt_data_inizio_val", ldt_data_inizio_val)
	dw_listini_ven_prodotti.setItem(ll_i, "data_key", ldt_data_inizio_val)
	dw_listini_ven_prodotti.setItem(ll_i, "rs_cod_prodotto", ls_cod_prodotto)
	dw_listini_ven_prodotti.setItem(ll_i, "rs_des_listino_vendite", ls_des_listino_vendite)
	dw_listini_ven_prodotti.setItem(ll_i, "rs_flag_sconto_mag_prezzo_1", ls_flag_sconto_mag_prezzo_1)
	dw_listini_ven_prodotti.setItem(ll_i,  "rl_progressivo", ll_progressivo)
	dw_listini_ven_prodotti.setItem(ll_i,"rd_variazione_1",ld_prezzo_arrotondato)
	dw_listini_ven_prodotti.setItem(ll_i,"rd_ricarico",ld_ricarico)
	dw_listini_ven_prodotti.setItem(ll_i, "rd_provv", ld_provvigione_1)
	dw_listini_ven_prodotti.setItem(ll_i, "rd_lit_x_unita_ven", ld_nullo)
			
loop

close cur_list;

dw_listini_ven_prodotti.setsort("rs_cod_valuta A, rs_cod_tipo_listino_prodotto A, rdt_data_inizio_val A")
dw_listini_ven_prodotti.sort()

if cbx_tutti.checked = false then
	
	for ll_i = 1 to (dw_listini_ven_prodotti.rowcount() - 1)
		
		ls_valuta_test = dw_listini_ven_prodotti.getitemstring(ll_i,"rs_cod_valuta")
		ls_valuta_succ = dw_listini_ven_prodotti.getitemstring(ll_i + 1,"rs_cod_valuta")
		ls_tipo_test = dw_listini_ven_prodotti.getitemstring(ll_i,"rs_cod_tipo_listino_prodotto")
		ls_tipo_succ = dw_listini_ven_prodotti.getitemstring(ll_i + 1,"rs_cod_tipo_listino_prodotto")
		ldt_data_test = dw_listini_ven_prodotti.getitemdatetime(ll_i,"rdt_data_inizio_val")
		ldt_data_succ = dw_listini_ven_prodotti.getitemdatetime(ll_i + 1,"rdt_data_inizio_val")
		
		if ls_valuta_test = ls_valuta_succ and ls_tipo_test = ls_tipo_succ and ldt_data_test < ldt_data_succ then
			dw_listini_ven_prodotti.deleterow(ll_i)
			ll_i --
		end if
		
	next
	
end if

if dw_listini_ven_prodotti.rowcount() < 1 and is_std <> "S" then
	
	for ll_i = 1 to 5
		cb_nuovo.triggerevent("clicked")
	next
	
	for ll_i = 1 to dw_listini_ven_prodotti.rowcount()
		dw_listini_ven_prodotti.setitem(ll_i,"rs_cod_tipo_listino_prodotto","LI" + string(ll_i - 1))
	next
	
	ib_inmodifica = false
	
end if

cbx_provvigioni.checked = lb_provv

dw_listini_ven_prodotti.resetUpdate()
dw_listini_ven_prodotti2.resetUpdate()

return 0
end function

public subroutine wf_ricerca_prodotto ();guo_ricerca.uof_ricerca_prodotto(dw_prodotti_lista,"cod_prodotto")
end subroutine

event pc_setwindow;call super::pc_setwindow;select flag
into   :is_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'FPR';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro FPR da parametri_azienda: " + sqlca.sqlerrtext)
	is_flag = "P"
elseif sqlca.sqlcode = 100 then
	is_flag = "P"
elseif sqlca.sqlcode = 0 then
	if is_flag = "S" then
		is_flag = "M"
	else
		is_flag = "P"
	end if
end if

select flag
into   :is_std
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'GLS';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro GLS da parametri_azienda: " + sqlca.sqlerrtext)
	is_std = "N"
elseif sqlca.sqlcode = 100 then
	is_std = "N"
end if

if is_std = "S" then
	cbx_data_odierna.checked = false
end if

Set_W_Options(c_NoEnablePopup + c_closenosave)

dw_prodotti_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_disablecc + c_nonew + c_nodelete + &
										 c_nomodify + c_NoShareData, &
                               c_default)

dw_listini_ven_prodotti.setrowfocusindicator(hand!)

iuo_dw_main = dw_prodotti_lista

select stringa
into   :is_nome_file
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'FLI';

if sqlca.sqlcode <> 0 then
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura del parametro FLI da parametri_azienda: " + sqlca.sqlerrtext + &
					  "~nFunzione Registro Listini disabilitata.")
	end if
	cbx_registro.checked = false
	cbx_registro.enabled = false
end if
end event

on w_listini_ven_prod.create
int iCurrent
call super::create
this.cbx_tutti=create cbx_tutti
this.cb_duplica_prod=create cb_duplica_prod
this.gb_2=create gb_2
this.cbx_data_odierna=create cbx_data_odierna
this.cb_salva=create cb_salva
this.cb_cancella=create cb_cancella
this.cb_nuovo=create cb_nuovo
this.cb_copia_list=create cb_copia_list
this.cb_duplica_da=create cb_duplica_da
this.cb_duplica_in=create cb_duplica_in
this.cb_prodotti=create cb_prodotti
this.gb_1=create gb_1
this.cbx_provvigioni=create cbx_provvigioni
this.cbx_registro=create cbx_registro
this.cbx_bloccati=create cbx_bloccati
this.dw_prodotti_lista=create dw_prodotti_lista
this.dw_listini_ven_prodotti=create dw_listini_ven_prodotti
this.dw_listini_ven_prodotti2=create dw_listini_ven_prodotti2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_tutti
this.Control[iCurrent+2]=this.cb_duplica_prod
this.Control[iCurrent+3]=this.gb_2
this.Control[iCurrent+4]=this.cbx_data_odierna
this.Control[iCurrent+5]=this.cb_salva
this.Control[iCurrent+6]=this.cb_cancella
this.Control[iCurrent+7]=this.cb_nuovo
this.Control[iCurrent+8]=this.cb_copia_list
this.Control[iCurrent+9]=this.cb_duplica_da
this.Control[iCurrent+10]=this.cb_duplica_in
this.Control[iCurrent+11]=this.cb_prodotti
this.Control[iCurrent+12]=this.gb_1
this.Control[iCurrent+13]=this.cbx_provvigioni
this.Control[iCurrent+14]=this.cbx_registro
this.Control[iCurrent+15]=this.cbx_bloccati
this.Control[iCurrent+16]=this.dw_prodotti_lista
this.Control[iCurrent+17]=this.dw_listini_ven_prodotti
this.Control[iCurrent+18]=this.dw_listini_ven_prodotti2
end on

on w_listini_ven_prod.destroy
call super::destroy
destroy(this.cbx_tutti)
destroy(this.cb_duplica_prod)
destroy(this.gb_2)
destroy(this.cbx_data_odierna)
destroy(this.cb_salva)
destroy(this.cb_cancella)
destroy(this.cb_nuovo)
destroy(this.cb_copia_list)
destroy(this.cb_duplica_da)
destroy(this.cb_duplica_in)
destroy(this.cb_prodotti)
destroy(this.gb_1)
destroy(this.cbx_provvigioni)
destroy(this.cbx_registro)
destroy(this.cbx_bloccati)
destroy(this.dw_prodotti_lista)
destroy(this.dw_listini_ven_prodotti)
destroy(this.dw_listini_ven_prodotti2)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_ven_prodotti, &
                 "rs_cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_ven_prodotti, &
                 "rs_cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_ven_prodotti2, &
                 "um_mag", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_ven_prodotti2, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event open;call super::open;ib_list = false
ib_prod = false
ib_speed = false

ib_inmodifica=false

id_listini_prod = create datastore
id_listini_prod.dataobject="d_listini_ven_prodotti_det"
id_listini_prod.SetTransObject(sqlca)
end event

event close;call super::close;integer li_risp
dw_listini_ven_prodotti.resetUpdate()
if ib_inmodifica then
	li_risp = g_mb.messagebox("Listini vendita dei Prodotti", "Salvare le modifiche? ", Question!, YesNo!)
	if li_risp = 1 then 
		cb_salva.triggerevent("clicked")
	end if
end if

destroy  id_listini_prod
end event

type cbx_tutti from checkbox within w_listini_ven_prod
integer x = 3090
integer y = 1068
integer width = 421
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Tutti i listini"
boolean lefttext = true
end type

event clicked;long   ll_riga

string ls_error


ll_riga = dw_prodotti_lista.getrow()

if ib_inmodifica = true then
	if g_mb.messagebox("APICE","Ci sono modifiche non salvate: salvarle?",Question!, YesNo!) = 1 then			
		if wf_salvamodifica(ll_riga,ls_error) < 0 then
			g_mb.messagebox("APICE",ls_error)
			return -1
		end if			
	else
		ib_list = false
		ib_prod = false
		ib_speed = false
		ib_inmodifica = false
	end if
end if

wf_carica_listini()
end event

type cb_duplica_prod from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 720
integer width = 411
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica prod."
end type

event clicked;long   ll_riga

string ls_error


ll_riga = dw_prodotti_lista.getrow()

if ib_inmodifica = true then
	if g_mb.messagebox("APICE","Ci sono modifiche non salvate: salvarle?",Question!, YesNo!) = 1 then			
		if wf_salvamodifica(ll_riga,ls_error) < 0 then
			g_mb.messagebox("APICE",ls_error)
			return -1
		end if			
	else
		ib_list = false
		ib_prod = false
		ib_speed = false
		ib_inmodifica = false
	end if
end if

s_cs_xx.parametri.parametro_s_1 = dw_prodotti_lista.getitemstring(ll_riga,"cod_prodotto")

s_cs_xx.parametri.parametro_s_2 = dw_prodotti_lista.getitemstring(ll_riga,"des_prodotto")

window_open(w_duplica_prodotto,0)

if s_cs_xx.parametri.parametro_d_1 = 0 then
	parent.triggerevent("pc_retrieve")
end if
end event

type gb_2 from groupbox within w_listini_ven_prod
integer x = 3063
integer width = 480
integer height = 820
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type cbx_data_odierna from checkbox within w_listini_ven_prod
integer x = 3090
integer y = 1012
integer width = 421
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Data odierna"
boolean checked = true
boolean lefttext = true
end type

type cb_salva from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 48
integer width = 411
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Salva"
end type

event clicked;integer li_ret
string ls_error
long ll_riga_prod

dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

ll_riga_prod = dw_prodotti_lista.getrow()

li_ret = wf_salvamodifica(ll_riga_prod, ls_error)

if li_ret<0 then 	
	g_mb.messagebox("APICE", ls_error)
end if

dw_listini_ven_prodotti.resetUpdate()

dw_prodotti_lista.setfocus()

if is_std = "N" then
	dw_prodotti_lista.object.b_ricerca_prodotto.default = true
end if
end event

type cb_cancella from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 144
integer width = 411
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancella"
end type

event clicked;
 string ls_cod_tipo_listino_prodotto ,ls_cod_valuta, ls_cod_prodotto
 long ll_i, ll_progressivo
 datetime ldt_data_inizio_val
 integer li_risp
 
 dw_listini_ven_prodotti.accepttext()
 dw_listini_ven_prodotti2.accepttext()
 
 ll_i = dw_listini_ven_prodotti.getrow()
 if ll_i <=0 then
	g_mb.messagebox("Gestione Listini","Nessun listino selezionato")
	return
 end if
 ls_cod_tipo_listino_prodotto = dw_listini_ven_prodotti.getitemString(ll_i, "rs_cod_tipo_listino_prodotto")
 ls_cod_prodotto = dw_prodotti_lista.getitemString(dw_prodotti_lista.getselectedrow(0), "cod_prodotto")
 ldt_data_inizio_val = dw_listini_ven_prodotti.getitemDatetime(ll_i, "rdt_data_inizio_val")
 ls_cod_valuta = dw_listini_ven_prodotti.getitemString(ll_i, "rs_cod_valuta")
 ll_progressivo= dw_listini_ven_prodotti.getitemNumber(ll_i, "rl_progressivo")
 
 li_risp = g_mb.messagebox("Variazione Prezzi","Si vuole cancellare il listino " + ls_cod_tipo_listino_prodotto &
 		+ " del " + string(ldt_data_inizio_val) +" ?", Question!, YesNo!)
 if li_risp = 2 then 
	return
 end if
 
 DELETE FROM listini_vendite 
	where cod_azienda = :s_cs_xx.cod_azienda
	 and cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto 
	 and cod_valuta = :ls_cod_valuta
	 and data_inizio_val = :ldt_data_inizio_val
	 and progressivo=: ll_progressivo
	using sqlca;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("Errore cancellazione in listini_vendite " , sqlca.SQLErrText)
		rollback;
		return
	end if
	
 DELETE FROM listini_prodotti 
	where cod_azienda = :s_cs_xx.cod_azienda
	 and cod_prodotto = :ls_cod_prodotto
	 and cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto 
	 and cod_valuta = :ls_cod_valuta
	 and data_inizio_val = :ldt_data_inizio_val
	using sqlca;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("Errore cancellazione provvigioni in listini_prodotti " , sqlca.SQLErrText)
		rollback;
		return
	end if
		
// Messagebox("pc_delete", "selezionata per la cancellazione "+ ls_cod_tipo_listino_prodotto)
	 dw_listini_ven_prodotti.resetUpdate()
	 commit; 
	 
	 wf_carica_listini()
end event

type cb_nuovo from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 240
integer width = 411
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuovo"
end type

event clicked;long     ll_selezionato, ll_i

dec{6}   ld_prezzo_acquisto

string   ls_cod_valuta, ls_cod_prodotto

datetime ldt_oggi


dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

ib_inmodifica=true
ib_list = true

ldt_oggi = datetime(today())

ll_i = dw_listini_ven_prodotti.insertrow(0)

il_list_da = ll_i

dw_listini_ven_prodotti.setitem(ll_i,"rdt_data_inizio_val", ldt_oggi)

setnull(ls_cod_valuta)

select stringa
into   :ls_cod_valuta
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LIR';

dw_listini_ven_prodotti.setitem(ll_i,"rs_cod_valuta", ls_cod_valuta)
dw_listini_ven_prodotti.setitem(ll_i,"rs_flag_sconto_mag_prezzo_1", is_flag)
dw_listini_ven_prodotti.setitem(ll_i,"modifica", "S")

dw_listini_ven_prodotti.setrow(ll_i)
dw_listini_ven_prodotti.scrolltorow(ll_i)
end event

type cb_copia_list from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 336
integer width = 411
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Copia da lis."
end type

event clicked;string ls_cod_tipo_listino_prodotto , ls_cod_valuta , ls_cod_prodotto, ls_des_listino_vendite , &
		ls_flag_sconto_mag_prezzo_1 , ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, &
		ls_flag_origine_prezzo_1, ls_str
dec{6} ld_provv , ld_lit_x_unita_ven , ld_prezzo_acquisto , ld_ricarico, ld_variazione_1, ld_pz_ven
datetime ldt_data_inizio_val
long ll_progressivo, ll_i, ll_num, ll_max_progressivo, ll_num_righe, ll_riga
integer li_risp


dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

ll_num_righe = dw_listini_ven_prodotti.rowcount()
ll_i = dw_listini_ven_prodotti.getrow()
ll_riga = il_list_da
if ll_riga > ll_num_righe then
	g_mb.messagebox("Copia Listini vendite dei Prodotti","Errore: perso il riferimento della riga su cui copiare!")
	return
end if
if ll_i=0 then 
	g_mb.messagebox("Copia Listini vendite dei Prodotti", "Errore: perso il riferimento della riga da copiare!")
	return 
end if

ls_cod_tipo_listino_prodotto = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_cod_valuta")
ls_des_listino_vendite = dw_listini_ven_prodotti.getitemstring(ll_i, "rs_des_listino_vendite")
ld_provv = dw_listini_ven_prodotti.getItemNumber(ll_i, "rd_provv")
ld_ricarico = dw_listini_ven_prodotti.getItemNumber(ll_i, "rd_ricarico")
ld_variazione_1 = dw_listini_ven_prodotti.getItemNumber(ll_i, "rd_variazione_1")
ld_lit_x_unita_ven = dw_listini_ven_prodotti.getItemNumber(ll_i, "rd_lit_x_unita_ven")
ld_prezzo_acquisto = dw_listini_ven_prodotti2.getItemNumber(1, "rd_prezzo_acquisto")
ll_progressivo = dw_listini_ven_prodotti.getItemNumber(ll_i, "rl_progressivo")
ldt_data_inizio_val = dw_listini_ven_prodotti.getItemDatetime(ll_i, "rdt_data_inizio_val")
ls_flag_sconto_mag_prezzo_1 = dw_listini_ven_prodotti.getitemString(ll_i,"rs_flag_sconto_mag_prezzo_1") 
ls_cod_prodotto = dw_prodotti_lista.getitemstring(dw_prodotti_lista.GetSelectedRow(0), "cod_prodotto")

dw_listini_ven_prodotti.setItem(ll_riga, "rs_cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
dw_listini_ven_prodotti.setItem(ll_riga, "rs_cod_valuta", ls_cod_valuta)
dw_listini_ven_prodotti.setItem(ll_riga, "rdt_data_inizio_val", ldt_data_inizio_val)
dw_listini_ven_prodotti.setItem(ll_riga, "rs_cod_prodotto", ls_cod_prodotto)
dw_listini_ven_prodotti.setItem(ll_riga, "rs_des_listino_vendite", ls_des_listino_vendite)
dw_listini_ven_prodotti.setItem(ll_riga, "rs_flag_sconto_mag_prezzo_1", ls_flag_sconto_mag_prezzo_1)
dw_listini_ven_prodotti.setItem(ll_riga, "rd_provv", ld_provv)
dw_listini_ven_prodotti.setItem(ll_riga, "rd_ricarico", ld_ricarico)
dw_listini_ven_prodotti.setItem(ll_riga, "rd_variazione_1", ld_variazione_1)
dw_listini_ven_prodotti.setItem(ll_riga, "rd_lit_x_unita_ven", ld_lit_x_unita_ven)
dw_listini_ven_prodotti.setItem(ll_riga, "modifica", "S")

ib_inmodifica = true
ib_list = true
end event

type cb_duplica_da from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 432
integer width = 411
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica da"
end type

event clicked;integer li_risp
string ls_cod_prodotto, ls_des_prodotto, ls_cod_tipo_listino_prodotto
long ll_i, ll_num_list
dec{6} ld_prezzo_acquisto



dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

ll_i = dw_prodotti_lista.getrow()
ls_cod_prodotto = dw_prodotti_lista.getitemString(ll_i, "cod_prodotto")
ls_des_prodotto = dw_prodotti_lista.getitemString(ll_i, "des_prodotto")

li_risp = g_mb.messagebox("Variazione Prezzi","Si vogliono duplicare i listini del prodotto corrente? ~r~n " &
			+ ls_cod_prodotto + " " + ls_des_prodotto, Question!, YesNo!)
if li_risp=2 then return
id_prezzo_acq = dw_listini_ven_prodotti2.getitemnumber(1, "rd_prezzo_acquisto")
ll_num_list = dw_listini_ven_prodotti.rowcount()
if ll_num_list > 0 then
	ll_i = id_listini_prod.rowcount()
	id_listini_prod.RowsDiscard (1, ll_i, Primary!)
	dw_listini_ven_prodotti.rowscopy(1,ll_num_list, Primary!, id_listini_prod, 1, Primary!)
else
	g_mb.messagebox("Variazione Prezzi", "Questo prodotto non ha listini! ~r~n Selezionare un prodotto con i listini da copiare")
	return
end if
// prova debug

//ll_num_list = id_listini_prod.rowcount()
//for ll_i = 1 to ll_num_list
//	ls_cod_tipo_listino_prodotto = id_listini_prod.getitemString(ll_i,"rs_cod_tipo_listino_prodotto")
//	messagebox(" Duplica da ", ls_cod_tipo_listino_prodotto)
//next 
//
//
end event

type cb_duplica_in from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 528
integer width = 411
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica in"
end type

event clicked;integer li_risp
string ls_cod_prodotto, ls_des_prodotto, ls_cod_prodotto_da
long ll_i, ll_num_list


dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

ll_i = dw_prodotti_lista.getrow()
ls_cod_prodotto = dw_prodotti_lista.getitemString(ll_i, "cod_prodotto")
ls_des_prodotto = dw_prodotti_lista.getitemString(ll_i, "des_prodotto")

li_risp = g_mb.messagebox("Variazione Prezzi","Si vogliono copiare i listini del prodotto " + &
			 ls_cod_prodotto_da + "sui listini del prodotto corrente? ~r~n " &
			+ ls_cod_prodotto + "  " + ls_des_prodotto + &
				" ~r~n Tutti i listini destinazione saranno cancellati", Question!, YesNo!)
if li_risp=2 then return
 
ll_num_list = dw_listini_ven_prodotti.rowcount()
if ll_num_list > 0 then
		DELETE FROM listini_vendite 
		where cod_azienda = :s_cs_xx.cod_azienda
		 and cod_prodotto = :ls_cod_prodotto
		using sqlca;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Errore cancellazione in listini_vendite " , sqlca.SQLErrText)
			rollback;
			return
		end if
		
		DELETE FROM listini_prodotti 
		where cod_azienda = :s_cs_xx.cod_azienda
		 and cod_prodotto = :ls_cod_prodotto
		using sqlca;
		
		if sqlca.sqlcode<0 then
			g_mb.messagebox("Errore cancellazione provvigioni in listini_prodotti " , sqlca.SQLErrText)
			rollback;
			return
		end if
dw_listini_ven_prodotti.RowsDiscard (1, ll_num_list, Primary!)
end if
ll_num_list = id_listini_prod.rowcount()
id_listini_prod.rowscopy(1,ll_num_list, Primary!, dw_listini_ven_prodotti, 1, Primary!)
dw_listini_ven_prodotti2.setitem(1, "rd_prezzo_acquisto", id_prezzo_acq)

for ll_i = 1 to dw_listini_ven_prodotti.rowcount()
	dw_listini_ven_prodotti.setitem(ll_i,"modifica","S")
	dw_listini_ven_prodotti.setitem(ll_i,"rl_progressivo",0)
next

ib_inmodifica = true
ib_list = true
ib_prod = true
end event

type cb_prodotti from commandbutton within w_listini_ven_prod
integer x = 3095
integer y = 624
integer width = 411
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Anag. prodotti"
end type

event clicked;long   ll_riga

string ls_cod_prodotto, ls_error


ll_riga = dw_prodotti_lista.getrow()

if ib_inmodifica = true then
	if g_mb.messagebox("APICE","Ci sono modifiche non salvate: salvarle?",Question!, YesNo!) = 1 then			
		if wf_salvamodifica(ll_riga,ls_error) < 0 then
			g_mb.messagebox("APICE",ls_error)
			return -1
		end if			
	else
		ib_list = false
		ib_prod = false
		ib_speed = false
		ib_inmodifica = false
	end if
end if

dw_listini_ven_prodotti.accepttext()
dw_listini_ven_prodotti2.accepttext()

setnull(ls_cod_prodotto)

ls_cod_prodotto = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(), "cod_prodotto")

if isnull(ls_cod_prodotto) then
	g_mb.messagebox("Gestione Listini","Selezionare prima un prodotto")
	return
end if

if isvalid(w_prodotti) then
	w_prodotti.bringtotop = true
else
	window_open(w_prodotti,-1)
end if

w_prodotti.dw_ricerca.setitem(1,"rs_cod_prodotto", ls_cod_prodotto)
w_prodotti.cb_reset.postevent("clicked")
w_prodotti.postevent("pc_modify")
end event

type gb_1 from groupbox within w_listini_ven_prod
integer x = 3063
integer y = 804
integer width = 480
integer height = 324
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type cbx_provvigioni from checkbox within w_listini_ven_prod
integer x = 3090
integer y = 844
integer width = 421
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Provvigioni"
boolean lefttext = true
end type

type cbx_registro from checkbox within w_listini_ven_prod
integer x = 3090
integer y = 900
integer width = 421
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Registro"
boolean checked = true
boolean lefttext = true
end type

type cbx_bloccati from checkbox within w_listini_ven_prod
integer x = 3090
integer y = 956
integer width = 421
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Bloccati"
boolean lefttext = true
end type

event clicked;long   ll_riga

string ls_error


ll_riga = dw_prodotti_lista.getrow()

if ib_inmodifica = true then
	if g_mb.messagebox("APICE","Ci sono modifiche non salvate: salvarle?",Question!, YesNo!) = 1 then			
		if wf_salvamodifica(ll_riga,ls_error) < 0 then
			g_mb.messagebox("APICE",ls_error)
			return -1
		end if			
	else
		ib_list = false
		ib_prod = false
		ib_speed = false
		ib_inmodifica = false
	end if
end if

parent.triggerevent("pc_retrieve")
end event

type dw_prodotti_lista from uo_cs_xx_dw within w_listini_ven_prod
event ue_tasto pbm_dwnkey
event ue_getfocus ( )
integer x = 23
integer y = 20
integer width = 3017
integer height = 800
integer taborder = 10
string dataobject = "d_listini_ven_prodotti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_tasto;choose case key
	case KeyEnter!   
		dw_listini_ven_prodotti.setfocus()

 	case KeyF1!
 		if KeyDown(KeyShift!) then 
			dw_prodotti_lista.change_dw_current()
			wf_ricerca_prodotto()
		end if
end choose

end event

event ue_getfocus();setfocus()

end event

event pcd_retrieve;call super::pcd_retrieve;long   ll_errore

string ls_select


ls_select = "select cod_prodotto, " + &
            "       des_prodotto, " + &
            "       pezzi_collo, " + &
            "       prezzo_acquisto, " + &
            "		  flag_blocco, " + &
            "		  cod_azienda " + &
            "from   anag_prodotti " + &
				"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if cbx_bloccati.checked = false then
	ls_select = ls_select + "and (flag_blocco = 'N' or flag_blocco = 'S' and data_blocco > '" + &
					string(today(),s_cs_xx.db_funzioni.formato_data) + "') "
end if

ls_select = ls_select + "order by cod_prodotto ASC "

ll_errore = setsqlselect(ls_select)

if ll_errore = -1 then
	pcca.error = c_fatal
end if

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event rowfocuschanging;call super::rowfocuschanging;long   li_risp

string ls_error


if ib_inmodifica = true then
	li_risp = g_mb.messagebox("APICE","Ci sono modifiche non salvate: salvarle?",Question!, YesNo!)
	if li_risp = 1 then
		li_risp = wf_salvamodifica(currentrow,ls_error)
		if li_risp < 0 then
			g_mb.messagebox("APICE",ls_error)
			return 1
		end if
	else
		ib_inmodifica = false
		ib_list = false
		ib_prod = false
		ib_speed = false
	end if
end if

il_prod_da = currentrow
end event

event rowfocuschanged;call super::rowfocuschanged;if (i_extendmode=true) then
	wf_carica_listini()
end if
end event

type dw_listini_ven_prodotti from datawindow within w_listini_ven_prod
event ue_tasto pbm_dwnkey
integer x = 23
integer y = 1140
integer width = 3520
integer height = 760
integer taborder = 30
string dataobject = "d_listini_ven_prodotti_det"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_tasto;choose case key
	case KeyEnter!   
		accepttext()
		cb_salva.postevent("clicked")

 	case KeyF1!
 		if KeyDown(KeyShift!) then 
			dw_prodotti_lista.change_dw_current()
			wf_ricerca_prodotto()
		end if

end choose

end event

event itemchanged;string  ls_cod_valuta, ls_messaggio

dec{6}  ld_prezzo_acq, ld_ricarico, ld_variazione

uo_condizioni_cliente luo_1


ib_inmodifica = true
ib_list = true

setitem(row,"modifica","S")

ld_prezzo_acq = dw_listini_ven_prodotti2.getitemnumber(1,"rd_prezzo_acquisto")

ls_cod_valuta = dw_listini_ven_prodotti.getitemstring(1,"rs_cod_valuta")

choose case dwo.name
		
	case "rd_ricarico"
		
		if dec(data) = 0 then
			ld_variazione = ld_prezzo_acq
		else
			if dec(data) < 0 then
				g_mb.messagebox("APICE","ATTENZIONE: è stato inserito un ricarico negativo~n" + &
							  "Assicurarsi che il dato sia corretto prima di continuare",information!)
			elseif dec(data) > 100 then
				g_mb.messagebox("APICE","ATTENZIONE: è stato inserito un ricarico maggiore del 100%~n" + &
							  "Assicurarsi che il dato sia corretto prima di continuare",information!)
			end if
			ld_variazione = ld_prezzo_acq + ( ld_prezzo_acq * dec(data) / 100 )
		end if
		
		luo_1 = create uo_condizioni_cliente		
		luo_1.uof_arrotonda_prezzo_decimal(ld_variazione,ls_cod_valuta,ld_variazione,ls_messaggio)
		destroy luo_1
		
		setitem(row,"rd_variazione_1",ld_variazione)
		
	case "rd_variazione_1"
		
		if dec(data) < ld_prezzo_acq then
			g_mb.messagebox("APICE","ATTENZIONE: è stato inserito un prezzo inferiore al prezzo di acquisto~n" + &
						  "Assicurarsi che il dato sia corretto prima di continuare",information!)
		end if
		
		// 22/01/04 Michela: se il prezzo di acquisto è 0 non calcolo il ricarico.
		if ld_prezzo_acq <> 0 then
			
			ld_ricarico = (dec(data) - ld_prezzo_acq) * 100 / ld_prezzo_acq		
			ld_ricarico = round(ld_ricarico,2)		
			setitem(row,"rd_ricarico",ld_ricarico)
		
		end if
		
	case "rdt_data_inizio_val"
		
		if cbx_data_odierna.checked then
			g_mb.messagebox("APICE","Attenzione: l'indicativo DATA ODIERNA è attivato!~n" + &
						  "La data di validità verrà salvata con valore " + string(today()))
		end if
		
	case "rd_provv"
		
		cbx_provvigioni.checked = true
		
end choose

resetupdate()

dw_prodotti_lista.resetupdate()
end event

event losefocus;accepttext()
end event

event itemfocuschanged;selecttext(1,len(gettext()))
end event

type dw_listini_ven_prodotti2 from datawindow within w_listini_ven_prod
integer x = 23
integer y = 840
integer width = 3017
integer height = 280
integer taborder = 90
string dataobject = "d_listini_ven_prodotti_det2"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;long   ll_i

dec{6} ld_ricarico, ld_prezzo

string ls_flag_sconto_mag_prezzo, ls_cod_valuta, ls_messaggio

uo_condizioni_cliente luo_arrotonda


ib_inmodifica = true

if dwo.name = "novita" then
	ib_speed = true
else
	ib_prod = true
end if

choose case dwo.name
		
	case "rd_prezzo_acquisto"
	
		for ll_i = 1 to dw_listini_ven_prodotti.rowcount()
			
			ls_flag_sconto_mag_prezzo = dw_listini_ven_prodotti.getitemstring(ll_i,"rs_flag_sconto_mag_prezzo_1")
			
			ls_cod_valuta = dw_listini_ven_prodotti.getitemstring(ll_i,"rs_cod_valuta")
			
			choose case ls_flag_sconto_mag_prezzo
					
				case "M"
					
					ld_ricarico = dw_listini_ven_prodotti.getitemnumber(ll_i,"rd_ricarico")
			
					ld_prezzo = dec(data) + (dec(data) / 100 * ld_ricarico)
					
					luo_arrotonda = create uo_condizioni_cliente
					
					luo_arrotonda.uof_arrotonda_prezzo_decimal(ld_prezzo,ls_cod_valuta,ld_prezzo,ls_messaggio)
					
					destroy luo_arrotonda
					
					dw_listini_ven_prodotti.setitem(ll_i,"rd_variazione_1",ld_prezzo)
					
				case "P"
					
					ld_prezzo = dw_listini_ven_prodotti.getitemnumber(ll_i,"rd_variazione_1")
					
					if dec(data) = 0 then
						ld_ricarico = 0
					else
						ld_ricarico = round( (ld_prezzo - dec(data)) * 100 / dec(data) , 2 )
					end if
					
					dw_listini_ven_prodotti.setitem(ll_i,"rd_ricarico",ld_ricarico)
					
			end choose
			
		next
	
end choose

resetupdate()

dw_prodotti_lista.resetupdate()
end event

event losefocus;accepttext()
end event

event itemfocuschanged;selecttext(1,len(gettext()))
end event

event buttonclicked;choose case dwo.name
		case "b_ricerca_prodotto"
			wf_ricerca_prodotto()
	end choose
end event

