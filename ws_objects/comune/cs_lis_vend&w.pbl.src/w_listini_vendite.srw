﻿$PBExportHeader$w_listini_vendite.srw
$PBExportComments$Finestra Listino Globale azienda
forward
global type w_listini_vendite from w_cs_xx_principale
end type
type st_1 from statictext within w_listini_vendite
end type
type cb_aggiorna from commandbutton within w_listini_vendite
end type
type cb_reset from commandbutton within w_listini_vendite
end type
type cb_ricerca from commandbutton within w_listini_vendite
end type
type cb_listini_produzione from commandbutton within w_listini_vendite
end type
type cb_pers_cli from commandbutton within w_listini_vendite
end type
type r_1 from rectangle within w_listini_vendite
end type
type dw_listini_vendite_lista from uo_cs_xx_dw within w_listini_vendite
end type
type dw_folder_search from u_folder within w_listini_vendite
end type
type dw_listini_vendite_det_1 from uo_cs_xx_dw within w_listini_vendite
end type
type dw_listini_vendite_det_2 from uo_cs_xx_dw within w_listini_vendite
end type
type dw_ricerca from u_dw_search within w_listini_vendite
end type
end forward

global type w_listini_vendite from w_cs_xx_principale
integer width = 3246
integer height = 2536
string title = "Listini Vendite"
event ue_sql ( )
event ue_resetupdate ( )
st_1 st_1
cb_aggiorna cb_aggiorna
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_listini_produzione cb_listini_produzione
cb_pers_cli cb_pers_cli
r_1 r_1
dw_listini_vendite_lista dw_listini_vendite_lista
dw_folder_search dw_folder_search
dw_listini_vendite_det_1 dw_listini_vendite_det_1
dw_listini_vendite_det_2 dw_listini_vendite_det_2
dw_ricerca dw_ricerca
end type
global w_listini_vendite w_listini_vendite

type variables
boolean ib_new=false, ib_modify=false
long il_dim_x[], il_dim_y[], il_x, il_y, il_num_scaglione
string is_flag_tipo_vista="G"

// stefanop 02/04/2010
private:
	string is_msg_no_dimvar = "Attenzione: procedere con la cancellazione del listino?"
	string is_msg_dim_var = "Attenzione: sono presenti dei prezzi per dimensione e prezzi per varianti di produzione, vuoi procedere?"
	string is_msg_prezzi_dim = "Attenzione: sono presenti dei prezzi per dimensioni, vuoi procedere?"
	string is_msg_prezzi_var = "Attenzione: sono presenti dei prezzi per varianti di produzione, vuoi procedere?"
	string is_cod_tipo_listino_prodotto
	string is_cod_valuta
	datetime idt_data_inizio_val
	integer ii_progressivo
	string is_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"
	boolean ib_glc = false
end variables

forward prototypes
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_disabilita_colonne ()
public subroutine wf_termine_scaglione ()
public subroutine wf_abilita_colonne ()
public subroutine wf_sql (string old_table, string new_table)
public function integer wf_cancella_listino (long row)
public function integer wf_cancella (string as_table)
private function integer wf_conta (string as_table)
public function integer wf_crea_transazione (string as_ini_section, ref transaction at_transaction)
private function integer wf_conta (string as_table, ref transaction at_transaction)
public function integer wf_invia_mail (long row)
public function integer wf_leggi_profili (ref string as_profili[])
end prototypes

event ue_sql();choose case is_flag_tipo_vista
		
	case "G" 
		RETURN
		
	case "C"
		wf_sql("listini_vendite", "listini_ven_comune")
		
	case "L"
		wf_sql("listini_vendite", "listini_ven_locale")
		
end choose

end event

event ue_resetupdate();//int li_i
//
//dw_listini_vendite_det_1.resetupdate()
//dw_listini_vendite_det_2.resetupdate()
//dw_listini_vendite_det_3.resetupdate() 
//dw_listini_vendite_lista.resetupdate()
//
//dw_listini_vendite_lista.accepttext()
end event

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_disabilita_colonne ();if dw_listini_vendite_det_1.getrow() > 0 and ( ib_new or ib_modify) then

		dw_listini_vendite_det_1.object.cod_cat_mer.border = 6
		dw_listini_vendite_det_1.object.cod_cat_mer.background.mode = 1
		dw_listini_vendite_det_1.object.cod_cat_mer.tabsequence = 0

		dw_listini_vendite_det_1.object.cod_prodotto.border = 6
		dw_listini_vendite_det_1.object.cod_prodotto.background.mode = 1
		dw_listini_vendite_det_1.object.cod_prodotto.tabsequence = 0

		dw_listini_vendite_det_1.object.cod_cliente.border = 6
		dw_listini_vendite_det_1.object.cod_cliente.background.mode = 1
		dw_listini_vendite_det_1.object.cod_cliente.tabsequence = 0

		dw_listini_vendite_det_1.object.cod_categoria.border = 6
		dw_listini_vendite_det_1.object.cod_categoria.background.mode = 1
		dw_listini_vendite_det_1.object.cod_categoria.tabsequence = 0

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public subroutine wf_termine_scaglione ();string ls_tipo_scaglione
double ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5


ls_tipo_scaglione = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"flag_tipo_scaglioni")
ld_variazione_1 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_1")
ld_variazione_2 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_2")
ld_variazione_3 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_3")
ld_variazione_4 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_4")
ld_variazione_5 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_5")
if ld_variazione_1 = 0 or isnull(ld_variazione_1) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_2 = 0 or isnull(ld_variazione_2) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_3 = 0 or isnull(ld_variazione_3) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_2", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_2", 999999999)
	end if
elseif ld_variazione_4 = 0 or isnull(ld_variazione_4) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_3", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_3", 999999999)
	end if
elseif ld_variazione_5 = 0 or isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_4", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_4", 999999999)
	end if
end if

if ld_variazione_5 <> 0 and not isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_5", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_5", 999999999)
	end if
end if

end subroutine

public subroutine wf_abilita_colonne ();if dw_listini_vendite_det_1.getrow() > 0 and ( ib_new or ib_modify) then

		dw_listini_vendite_det_1.object.cod_cat_mer.border = 5
		dw_listini_vendite_det_1.object.cod_cat_mer.background.mode = 0
		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_listini_vendite_det_1.object.cod_cat_mer.tabsequence = 50

		dw_listini_vendite_det_1.object.cod_prodotto.border = 5
		dw_listini_vendite_det_1.object.cod_prodotto.background.mode = 0
		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_listini_vendite_det_1.object.cod_prodotto.tabsequence = 60

		dw_listini_vendite_det_1.object.cod_cliente.border = 5
		dw_listini_vendite_det_1.object.cod_cliente.background.mode = 0
		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_listini_vendite_det_1.object.cod_cliente.tabsequence = 70

		dw_listini_vendite_det_1.object.cod_categoria.border = 5
		dw_listini_vendite_det_1.object.cod_categoria.background.mode = 0
		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_listini_vendite_det_1.object.cod_categoria.tabsequence = 80

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public subroutine wf_sql (string old_table, string new_table);long start_pos=1, ll_ret = 0
string ls_sql, ll_err

	// ------------  prima DW ----------------
	
ls_sql = dw_listini_vendite_lista.getsqlselect( )
	
start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

	 // Replace old_str with new_str.

	 ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

	 // Find the next occurrence of old_str.

	 start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP


dw_listini_vendite_lista.reset()

ll_err = dw_listini_vendite_lista.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_vendite_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")


// -------------- seconda DW ----------------------------------
ls_sql = dw_listini_vendite_det_1.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_vendite_det_1.reset()

ll_err = dw_listini_vendite_det_1.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_vendite_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")


// -------------- terza DW ----------------------------------
ls_sql = dw_listini_vendite_det_2.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_vendite_det_2.reset()

ll_err = dw_listini_vendite_det_2.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_vendite_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")

end subroutine

public function integer wf_cancella_listino (long row);/**
 * stefanop
 * 02/04/2010
 **/
 
string		ls_message, ls_test, ls_profili[], ls_flag_listini_comuni
boolean 	lb_comune
int			li_risposta, li_i
long		ll_count, ll_count_temp, ll_count_ven_dim, ll_count_prod
transaction lt_transaction

ls_flag_listini_comuni = "N"

select flag
into :ls_flag_listini_comuni
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then ls_flag_listini_comuni = "N"

// carico PK
is_cod_tipo_listino_prodotto = dw_listini_vendite_lista.getitemstring(row, "cod_tipo_listino_prodotto")
is_cod_valuta = dw_listini_vendite_lista.getitemstring(row, "cod_valuta")
idt_data_inizio_val = dw_listini_vendite_lista.getitemdatetime(row, "data_inizio_val")
ii_progressivo = dw_listini_vendite_lista.getitemnumber(row, "progressivo")
// ----


if ls_flag_listini_comuni = "S" then
	// gestione listini comuni/locali per gruppo gibus

	// verifico se comune o locale
	ls_test = dw_listini_vendite_lista.getitemstring(row, "cod_cliente")
	if isnull(ls_test) or ls_test = "" then
		lb_comune = true
	else
		lb_comune = false
	end if
	
	if not lb_comune then // sono nel locale
		ll_count_ven_dim = wf_conta("listini_ven_dim_locale")
		ll_count_prod = wf_conta("listini_prod_locale")
		
		if ll_count_ven_dim > 0 and ll_count_prod > 0 then
			ls_message = is_msg_dim_var
			
		elseif ll_count_ven_dim > 0 then
			ls_message = is_msg_prezzi_dim
			
		elseif ll_count_prod > 0 then
			ls_message = is_msg_prezzi_var
			
		else
			ls_message = is_msg_no_dimvar
			
		end if
		
		if g_mb.confirm("Apice", ls_message) then
			// LISTINI_VEN_DIM_LOCALE
			if ll_count_ven_dim > 0 then
				if wf_cancella("listini_ven_dim_locale") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_PROD_LOCALE
			if ll_count_prod > 0 then
				if wf_cancella("listini_prod_locale") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_VEN_LOCALE
			if wf_cancella("listini_ven_locale") < 0 then
				rollback;
				return -1
			end if
	
			commit;
			
		else // cliccato su no
			return 0
		end if
		
	else // comune
			
		if not g_mb.confirm("Apice", "Attenzione: si sta per cancellare una condizione comune. Tale cancellazione potrebbe provocare anomalie nei listini delle altre aziende.~r~nVuoi Continuare?") then
			return 0
		end if
			
		// LISTINI PROD_COMUNE_VAR_CLIENT 
		ll_count = wf_conta("listini_prod_comune_var_client")
		
//		if ib_glc then
//			// se la gestione listini comune è attiva allora controllo nei database delle altre aziende
//			li_risposta = wf_leggi_profili(ref ls_profili)
//			if li_risposta < 0 then
//				return li_risposta
//			elseif li_risposta > 0 then
//				for li_i = 1 to li_risposta
//					
//					if wf_crea_transazione("database_" + ls_profili[li_i], lt_transaction) < 0 then
//						g_mb.error("Apice","Errore durante la creazione della connessione con il database_" + ls_profili[li_i])
//						return -1
//					end if
//					
//					ll_count_temp = wf_conta("listini_prod_comune_var_client", lt_transaction)
//					disconnect using lt_transaction;
//					
//					if ll_count_temp < 0 then return -1
//					ll_count += ll_count_temp
//				next
//			end if
//		
//		end if
		
//		if ll_count > 0 then
//			g_mb.show("Apice", "Attenzione: sono presenti " + string(ll_count) + " varianti locali per cliente.~r~nNon è possibile continuare con la cancellazione.")
//			return -1
//		end if
		
		// 01/04/2011 su richiesta di A.Bellin è stata abilitata la cancellazione anche in presenza di listini
		// nella tabella listini_ven_comune_var_client
		setnull(ls_message)
		ll_count_ven_dim = wf_conta("listini_ven_dim_comune")
		ll_count_prod = wf_conta("listini_prod_comune")
		
		if ll_count_ven_dim > 0 and ll_count_prod > 0 then
			ls_message = is_msg_dim_var
			
		elseif ll_count_ven_dim > 0 then
			ls_message = is_msg_prezzi_dim
			
		elseif ll_count_prod > 0 then
			ls_message = is_msg_prezzi_var
			
		else
			ls_message = is_msg_no_dimvar
			
		end if
		
		if g_mb.confirm("Apice", ls_message) then
			// LISTINI_VEN_COMUNE_VAR_CLIENT
			if wf_cancella("listini_prod_comune_var_client") < 0 then
				rollback;
				return -1
			end if
			
			// LISTINI_VEN_DIM_COMUNE
			if ll_count_ven_dim > 0 then
				if wf_cancella("listini_ven_dim_comune") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_PROD_COMUNE
			if ll_count_prod > 0 then
				if wf_cancella("listini_prod_comune") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_VEN_COMUNE
			if wf_cancella("listini_ven_comune") < 0 then
				rollback;
				return -1
			end if
			
			//se qualcosa non va a buon fine solo nell'invio mail non annullare tutto ecchecazzo .....!!!!
			
			// Invio mail
			if wf_invia_mail(row) < 0 then
				//rollback;
				//return -1
			end if
			
			commit;
			
		else // cliccato su no
			return 0
		end if
	end if

else
	// nessuna gestione listini comuni o locali, quindi devo cancellare dalle tabelle classiche.
	
		ll_count_ven_dim = wf_conta("listini_vendite_dimensioni")
		ll_count_prod = wf_conta("listini_produzione")
		
		if ll_count_ven_dim > 0 and ll_count_prod > 0 then
			ls_message = is_msg_dim_var
			
		elseif ll_count_ven_dim > 0 then
			ls_message = is_msg_prezzi_dim
			
		elseif ll_count_prod > 0 then
			ls_message = is_msg_prezzi_var
			
		else
			ls_message = is_msg_no_dimvar
			
		end if
		
		if g_mb.confirm("Apice", ls_message) then
			// LISTINI_VEN_DIM_LOCALE
			if ll_count_ven_dim > 0 then
				if wf_cancella("listini_vendite_dimensioni") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_PROD_LOCALE
			if ll_count_prod > 0 then
				if wf_cancella("listini_produzione") < 0 then
					rollback;
					return -1
				end if
			end if
			
			// LISTINI_VEN_LOCALE
			if wf_cancella("listini_vendite") < 0 then
				rollback;
				return -1
			end if
	
			commit;
			
		else // cliccato su no
			return 0
		end if
	
	
	
end if

return 1
end function

public function integer wf_cancella (string as_table);/**
 * Stefano
 * 02/04/2010
 *
 * Cancella records da una tabella
 **/
 
string ls_sql
long ll_count


ls_sql = "DELETE FROM " + as_table + " WHERE "
ls_sql += "cod_azienda='"+ s_cs_xx.cod_azienda + "' AND "
ls_sql += "cod_tipo_listino_prodotto='" + is_cod_tipo_listino_prodotto + "' AND "
ls_sql += "cod_valuta='" + is_cod_valuta + "' AND "
ls_sql += "data_inizio_val='" + string(idt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' AND "
ls_sql += "progressivo=" + string(ii_progressivo)

execute immediate :ls_sql;

if sqlca.sqlcode <> 0 then
	g_mb.error("Apice", sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode = 0 then
	return 1
end if
end function

private function integer wf_conta (string as_table);/**
 * stefano
 * 02/04/2010
 *
 * Conta il numero di records all'interno di una tabella
 **/
 
//string ls_sql
//long 	ll_count
//datastore lds_store
//
//ls_sql = "SELECT COUNT(cod_azienda) FROM " + as_table +" WHERE " 
//ls_sql += "cod_azienda='" + s_cs_xx.cod_azienda + "' AND "
//ls_sql += "cod_tipo_listino_prodotto='" + is_cod_tipo_listino_prodotto + "' AND "
//ls_sql += "cod_valuta='" + is_cod_valuta + "' AND "
//ls_sql += "data_inizio_val='" + string(idt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' AND "
//ls_sql += "progressivo=" + string(ii_progressivo);
//	
//if not f_crea_datastore(lds_store, ls_sql) then
//	g_mb.error("Apice", "Errore durante la creazione del datastore per la tabella " + as_table + " (count)")
//	return -1
//end if
//
//ll_count = lds_store.retrieve()
//if ll_count > 0 then
//	ll_count = lds_store.getitemnumber(1, 1)
//	if isnull(ll_count) then ll_count = 0
//else
//	ll_count = 0
//end if
//
//destroy lds_store

return wf_conta(as_table, ref sqlca)
end function

public function integer wf_crea_transazione (string as_ini_section, ref transaction at_transaction);/**
 * Stefano Pulze
 * 23-11-2009
 *
 * Leggo i paramentri dal registro e imposto la transazione,
 * il codice è copiato dalla intranet (uo_ptenda)
 */

string ls_logpass, ls_option
int li_risposta, ll_ret

at_transaction = create transaction

// -- Leggo i dati dal registro
li_risposta = registryget(is_reg_chiave_root + as_ini_section, "servername", at_transaction.ServerName)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: servername.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "dbms", at_transaction.DBMS)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: dbms.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "database", at_transaction.Database)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazione del database sul registro: database.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "logid", at_transaction.LogId)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: logid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: logpass.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if at_transaction.DBMS <> "ODBC" then
	
	if isnull(ls_logpass) or ls_logpass="" then		
		
		messagebox("Apice", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", StopSign!)
		return -1	
		
	end if	
		
	n_cst_crypto luo_crypto
	luo_crypto = create n_cst_crypto
	ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
	destroy luo_crypto;
	if ll_ret < 0 then
		messagebox("Apice", "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
					"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
					"- alcuni simboli.Modificare la password e riprovare", StopSign!)
		return -1
	end if
	
	at_transaction.LogPass = ls_logpass
	
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "userid", at_transaction.UserId)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: userid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "dbpass", at_transaction.DBPass)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: dbpass.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "dbparm", at_transaction.DBParm)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazione del database sul registro: dbparm.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "option", ls_option)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazione del database sul registro: option.", StopSign!)
	return -1
end if

at_transaction.dbparm = at_transaction.dbparm + ls_option

li_risposta = registryget(is_reg_chiave_root + as_ini_section, "lock", at_transaction.Lock)
if li_risposta = -1 then
	messagebox("Apice", "Mancano le impostazioni del database sul registro: lock.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.2")		 

disconnect using at_transaction;
connect using at_transaction;
if at_transaction.sqlcode <> 0 then
	messagebox("Apice", "Errore durante la connessione al database~r~n" + at_transaction.sqlerrtext, StopSign!)
	return -1
end if

return 0
end function

private function integer wf_conta (string as_table, ref transaction at_transaction);/**
 * stefano
 * 02/04/2010
 *
 * Conta il numero di records all'interno di una tabella
 **/
 
string ls_sql, ls_errore, ls_sintassi
long 	ll_count
datastore lds_store

ls_sql = "SELECT COUNT(cod_azienda) FROM " + as_table +" WHERE " 
ls_sql += "cod_azienda='" + s_cs_xx.cod_azienda + "' AND "
ls_sql += "cod_tipo_listino_prodotto='" + is_cod_tipo_listino_prodotto + "' AND "
ls_sql += "cod_valuta='" + is_cod_valuta + "' AND "
ls_sql += "data_inizio_val='" + string(idt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' AND "
ls_sql += "progressivo=" + string(ii_progressivo);
	
// Datastore
ls_sintassi = at_transaction.syntaxfromsql(ls_sql, "style(type=grid)", ls_errore)
if Len(ls_errore) > 0 then
	g_mb.error("Apice", "Errore durante la creazione della sintassi del datastore:" + ls_errore)
	return -1
end if

lds_store = create datastore

lds_store.Create(ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
	g_mb.error("Apice", "Errore durante la creazione del datastore:" + ls_errore)
	destroy lds_store;
	return -1
end if

lds_store.settransobject(at_transaction)
// ----

ll_count = lds_store.retrieve()
if ll_count > 0 then
	ll_count = lds_store.getitemnumber(1, 1)
	if isnull(ll_count) then ll_count = 0
else
	ll_count = 0
end if

destroy lds_store

return ll_count
end function

public function integer wf_invia_mail (long row);string ls_path, ls_tipo, ls_file_allegato, ls_LDL, ls_sql, ls_appo, ls_email, ls_cod_dest[], &
		ls_destinatari[], ls_oggetto, ls_messaggio, ls_allegati[], ls_msg, ls_cod_tipo_listino, &
		ls_cod_valuta, ls_progressivo, ls_data_inizio_val
long	ll_i, ll_rows, ll_count, ll_indice, li_ret
uo_outlook luo_outlook


ls_tipo = "M"

select stringa
into   :ls_LDL
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LDL' and flag_parametro="S";

if sqlca.sqlcode <> 0 then	
	g_mb.show("Apice", "Attenzione: impostare il parametro stringa LDL (dest1,dest2;...) dei destinatari e-mail: tabella parametri_azienda!")
	return -1
end if

if isnull(ls_LDL) or ls_LDL="" then
	g_mb.show("Apice", "Il valore del parametro 'LDL' non può essere vuoto! (controllare il parametro aziendale LDL)")
	return -1
end if

f_split(ls_LDL, ",", ls_cod_dest)

ll_rows = upperbound(ls_cod_dest)
ll_indice = 0

if ll_rows = 0 then
	g_mb.show("Apice", "L'elaborazione del parametro 'LDL' non ha dato nessun risultato! (controllare il parametro aziendale LDL)")
	return -1
end if

for ll_i=1 to ll_rows
	//leggi il valore	
	
	ls_appo = ls_cod_dest[ll_i]
	
	//verifica che esista in tab_ind_dest
	select count(*)
	into :ll_count
	from tab_ind_dest
	where cod_azienda=:s_cs_xx.cod_azienda and cod_destinatario=:ls_appo;
	
	if ll_count = 1 then
	else
		//problemi
		g_mb.messagebox("APICE","Problemi in lettura destinatario: "+ls_appo, Exclamation!)
		continue
	end if
	
	select indirizzo
	into :ls_email
	from tab_ind_dest
	where cod_azienda=:s_cs_xx.cod_azienda and cod_destinatario=:ls_appo;
	
	if ls_email="" or isnull(ls_email) then
		g_mb.messagebox("APICE","Indirizzo vuoto per il destinatario: "+ls_appo, Exclamation!)
		continue
	end if
	
	ll_indice+=1
	ls_destinatari[ll_indice] = ls_email
	
next

luo_outlook = create uo_outlook

ls_cod_tipo_listino = dw_listini_vendite_lista.getitemstring(row, "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_vendite_lista.getitemstring(row, "cod_valuta")
ls_data_inizio_val = string(dw_listini_vendite_lista.getitemdatetime(row, "data_inizio_val"), "dd/mm/yyyy")
ls_progressivo = string(dw_listini_vendite_lista.getitemnumber(row, "progressivo"))

ls_oggetto = "Eliminato Listino Comune " + ls_cod_tipo_listino + "-" + ls_cod_valuta + "-" + ls_data_inizio_val
ls_messaggio = "E' stato eliminato il listino comune~r~n"
ls_messaggio += "Tipo listino: " + ls_cod_tipo_listino + "~r~n"
ls_messaggio += "Valuta: " + ls_cod_valuta + "~r~n"
ls_messaggio += "Data inizio validità: " + ls_data_inizio_val + "~r~n"
ls_messaggio += "Progressivo: " + ls_progressivo

//li_ret = luo_outlook.uof_invio_outlook_redemption(0,ls_tipo, ls_oggetto, ls_messaggio, ls_destinatari[], &
//																	ls_allegati[], true, ls_msg)
																	
li_ret = luo_outlook.uof_outlook(0, ls_tipo, ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], false, ls_msg)


if li_ret <> 0 then
	g_mb.messagebox("APICE", "Errore in fase di notifica cancellazione listino~r~n" + ls_msg)
	return -1
else
	// tutto bene
end if
																	
destroy luo_outlook

return 1
end function

public function integer wf_leggi_profili (ref string as_profili[]);/** * Stefano
 * 25/05/2010
 *
 * Legge le chiavi profili_ assegnati alla cartella applicazione_ del reigstro di sistema
 **/

string  ls_hkey_user, ls_hkey_machine, ls_numero, ls_profilo, ls_empty[]
int li_risposta, li_profilo

ls_hkey_user = "HKEY_CURRENT_USER\SOFTWARE\Consulting&Software\"
ls_hkey_machine = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"

// -- Leggo profilo
li_risposta = registryget(ls_hkey_user + "profilodefault", "numero", ls_numero)
if li_risposta = -1 then
	
	li_risposta = registryget(ls_hkey_machine + "profilocorrente", "numero", ls_numero)
	if li_risposta = -1 then
		
		li_risposta = registryget(ls_hkey_machine + "profilodefault", "numero", ls_numero)
		if li_risposta = -1 then
			g_mb.error("Apice", "Impossibile recuperare il numero di profilo dal registro")
			return -1
		end if
	end if
end if
// -----

// -- Leggo profili assegnati all'applicazione
as_profili = ls_empty
li_profilo = 0
do
	
	li_profilo++
	li_risposta = registryget(ls_hkey_machine + "applicazione_" + ls_numero, "profilo_" + string(li_profilo), ls_profilo)
	if li_risposta = 1 then
		as_profili[li_profilo] = ls_profilo
	end if
	
loop while li_risposta = 1
// ----

return upperbound(as_profili)
end function

event pc_setwindow;call super::pc_setwindow;datetime ldt_data_chiusura_annuale_prec, ldt_data_chiusura_annuale, &
			ldt_data_chiusura_periodica
string ls_data_chiusura_annuale_prec, ls_data_chiusura_annuale, &
			ls_data_chiusura_periodica, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, &
			l_criteriacolumn[], l_searchtable[], l_searchcolumn[],ls_flag
windowobject lw_oggetti[], l_objects[ ]
unsignedlong lu_controlword,lu_controlword1

ls_flag = "N"
select flag
into 	 :ls_flag
from 	 parametri 
where  cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then ls_flag = "N"

if ls_flag= 'S' then ib_glc = true

if ls_flag = "S" then 
	triggerevent("ue_sql")
end if

if ls_flag = "S" then 
	choose case is_flag_tipo_vista 
		case "G"
				lu_controlword = c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete
				lu_controlword1 = c_sharedata + c_scrollparent  + c_nonew + c_nomodify + c_nodelete

		case else
				lu_controlword = c_noretrieveonopen
				lu_controlword1 = c_sharedata + c_scrollparent
				
				// stefano 06/04/2010: la finestra non deve chiedere di salvare alla chiusura
				// !!! Anche se impostato il resetupdate della lista, continua sempre a chiedere conferma !!!
				set_w_options(c_closenosave)
				save_on_close(c_socnosave)
				// ----
	end choose
else
		lu_controlword = c_noretrieveonopen
		lu_controlword1 = c_sharedata + c_scrollparent
		
		// stefano 06/04/2010: la finestra non deve chiedere di salvare alla chiusura
		// !!! Anche se impostato il resetupdate della lista, continua sempre a chiedere conferma !!!
		set_w_options(c_closenosave)
		save_on_close(c_socnosave)
		// ----
end if		

dw_listini_vendite_lista.set_dw_key("cod_azienda")
dw_listini_vendite_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 lu_controlword, &
                                 c_default)
dw_listini_vendite_det_1.set_dw_options(sqlca, &
                                dw_listini_vendite_lista, &
                                lu_controlword1, &
                                c_default)
dw_listini_vendite_det_2.set_dw_options(sqlca, &
                                dw_listini_vendite_lista, &
                                lu_controlword1 , &
                                c_default)

iuo_dw_main=dw_listini_vendite_lista

//dw_listini_vendite_det_3.settransobject(sqlca)


l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "rs_cod_tipo_listino"
l_criteriacolumn[3] = "rs_cod_valuta"
l_criteriacolumn[4] = "rs_cod_cat_mer"
l_criteriacolumn[5] = "rs_cod_categoria"
l_criteriacolumn[6] = "rs_cod_cliente"

if ls_flag ="S" then
	choose case is_flag_tipo_vista
		case "G"
			Title = "LISTINO VENDITE GLOBALE (VISTA)"
			l_searchtable[1] = "listini_vendite"
			l_searchtable[2] = "listini_vendite"
			l_searchtable[3] = "listini_vendite"
			l_searchtable[4] = "listini_vendite"
			l_searchtable[5] = "listini_vendite"
			l_searchtable[6] = "listini_vendite"
		case "C"
			Title = "LISTINO VENDITE COMUNE (MODIFICA)"
			l_searchtable[1] = "listini_ven_comune"
			l_searchtable[2] = "listini_ven_comune"
			l_searchtable[3] = "listini_ven_comune"
			l_searchtable[4] = "listini_ven_comune"
			l_searchtable[5] = "listini_ven_comune"
			l_searchtable[6] = "listini_ven_comune"
		case "L"
			Title = "LISTINO VENDITE PERSONALIZZATO (MODIFICA)"
			l_searchtable[1] = "listini_ven_locale"
			l_searchtable[2] = "listini_ven_locale"
			l_searchtable[3] = "listini_ven_locale"
			l_searchtable[4] = "listini_ven_locale"
			l_searchtable[5] = "listini_ven_locale"
			l_searchtable[6] = "listini_ven_locale"
	end choose
else
	l_searchtable[1] = "listini_vendite"
	l_searchtable[2] = "listini_vendite"
	l_searchtable[3] = "listini_vendite"
	l_searchtable[4] = "listini_vendite"
	l_searchtable[5] = "listini_vendite"
	l_searchtable[6] = "listini_vendite"
end if	

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_tipo_listino_prodotto"
l_searchcolumn[3] = "cod_valuta"
l_searchcolumn[4] = "cod_cat_mer"
l_searchcolumn[5] = "cod_categoria"
l_searchcolumn[6] = "cod_cliente"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_listini_vendite_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
											 
l_objects[1] = dw_listini_vendite_lista
l_objects[2] = cb_listini_produzione
l_objects[3] = cb_pers_cli
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
l_objects[4] = cb_aggiorna
l_objects[5] = st_1
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_listini_vendite_lista.change_dw_current()
return 0
end event

on w_listini_vendite.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_aggiorna=create cb_aggiorna
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_listini_produzione=create cb_listini_produzione
this.cb_pers_cli=create cb_pers_cli
this.r_1=create r_1
this.dw_listini_vendite_lista=create dw_listini_vendite_lista
this.dw_folder_search=create dw_folder_search
this.dw_listini_vendite_det_1=create dw_listini_vendite_det_1
this.dw_listini_vendite_det_2=create dw_listini_vendite_det_2
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_aggiorna
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_listini_produzione
this.Control[iCurrent+6]=this.cb_pers_cli
this.Control[iCurrent+7]=this.r_1
this.Control[iCurrent+8]=this.dw_listini_vendite_lista
this.Control[iCurrent+9]=this.dw_folder_search
this.Control[iCurrent+10]=this.dw_listini_vendite_det_1
this.Control[iCurrent+11]=this.dw_listini_vendite_det_2
this.Control[iCurrent+12]=this.dw_ricerca
end on

on w_listini_vendite.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_aggiorna)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_listini_produzione)
destroy(this.cb_pers_cli)
destroy(this.r_1)
destroy(this.dw_listini_vendite_lista)
destroy(this.dw_folder_search)
destroy(this.dw_listini_vendite_det_1)
destroy(this.dw_listini_vendite_det_2)
destroy(this.dw_ricerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))")

f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

dw_ricerca.fu_loadcode("rs_cod_tipo_listino", &
                       "tab_tipi_listini_prodotti", &
                       "cod_tipo_listino_prodotto", &
                       "des_tipo_listino_prodotto", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_tipo_listino_prodotto asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_prodotto", &
//                       "anag_prodotti", &
//		                 "cod_prodotto", &
//					        "des_prodotto", &
//					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "(Tutti)" )

dw_ricerca.fu_loadcode("rs_cod_cat_mer", &
                       "tab_cat_mer", &
					        "cod_cat_mer", &
					        "des_cat_mer", &
					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cat_mer asc", "(Tutti)" )
					  
dw_ricerca.fu_loadcode("rs_cod_valuta", &
                       "tab_valute", &
							  "cod_valuta", &
							  "des_valuta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_valuta asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_cliente", &
//                        "anag_clienti", &
//								"cod_cliente", &
//								"rag_soc_1",&
//								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cliente asc", "(Tutti)" )

dw_ricerca.fu_loadcode("rs_cod_categoria", &
                        "tab_categorie", &
								"cod_categoria", &
								"des_categoria", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_categoria asc", "(Tutti)" )

					  
					  
					  
					  
					  
					  
end event

event pc_delete;/**
 * !!! TOLTO EXTEND ANCESTOR SCRIPT !!!!
 * altrimenti mostra due volte il messagio di cancellazione riga
 **/

int li_risposta 
// stefano 02/04/2010
if dw_listini_vendite_lista.rowcount() > 0 and dw_listini_vendite_lista.getrow() > 0 then
	if wf_cancella_listino(dw_listini_vendite_lista.getrow()) > 0 then
		dw_listini_vendite_lista.deleterow(dw_listini_vendite_lista.getrow())	

		event post ue_resetupdate()
	end if
end if
end event

type st_1 from statictext within w_listini_vendite
integer x = 2743
integer y = 460
integer width = 366
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "Times New Roman"
long backcolor = 65535
boolean focusrectangle = false
end type

type cb_aggiorna from commandbutton within w_listini_vendite
integer x = 2743
integer y = 260
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Agg.Dati"
end type

event clicked;string ls_str[], ls_des_str[], ls_condizioni[], ls_varianti[], dinensioni[], ls_messaggio
time lt_inizio, lt_fine

lt_inizio = now()
uo_report_listini luo_report_listini

luo_report_listini = CREATE uo_report_listini
luo_report_listini.st_messaggio = st_1
if luo_report_listini.uof_ricerca_clienti(ls_str, ls_des_str, ls_condizioni[], ls_varianti[],dinensioni[],ls_messaggio) = -1 then
	g_mb.messagebox("Agg.Dati",ls_messaggio)
	rollback;
end if
destroy luo_report_listini
rollback;
lt_fine = now()
g_mb.messagebox("APICE","Elaborazione terminata~r~nDurata dell'eleborazione: " + string(secondsafter(lt_inizio, lt_fine)) + " secondi")




end event

type cb_reset from commandbutton within w_listini_vendite
integer x = 2743
integer y = 160
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_listini_vendite_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_listini_vendite
integer x = 2743
integer y = 60
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_listini_produzione from commandbutton within w_listini_vendite
integer x = 2743
integer y = 60
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Produzione"
end type

event clicked;string ls_cod_prodotto, ls_cod_cat_mer, ls_cod_categoria, ls_cod_cliente,ls_flag
long   ll_cont

ls_cod_prodotto = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_prodotto")
ls_cod_categoria = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_categoria")
ls_cod_cat_mer = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_cat_mer")
ls_cod_cliente = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_cliente")

//if not isnull(ls_cod_categoria) or not isnull(ls_cod_cat_mer) or not isnull(ls_cod_cliente) then
//	messagebox("Listini Vendite","Il listino di produzione è accessibile solo dalle condizioni del prodotto", StopSign!)
//	return
//end if
if isnull(ls_cod_prodotto) then
	g_mb.messagebox("Listini Vendite","Il listino selezionato non è relativo ad un prodotto", StopSign!)
	return
end if

ll_cont = 0
select count(*)
into   :ll_cont
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
if ll_cont = 0 or isnull(ll_cont) then
	g_mb.messagebox("Listini Vendite","Nessuna distinta esistente per il prodotto selezionato")
	return
end if

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

s_cs_xx.parametri.parametro_s_12 = is_flag_tipo_vista
if ls_flag = "S" then 
	
	choose case is_flag_tipo_vista
		case "G"
			if not isnull(ls_cod_cliente) then
				window_open_parm(w_listino_distinta, -1, dw_listini_vendite_lista)
			else
				window_open_parm(w_listino_distinta_tree, -1, dw_listini_vendite_lista)
			end if
		case "L"
//			window_open_parm(w_listino_distinta_tree, -1, dw_listini_vendite_lista)
			window_open_parm(w_listino_distinta_locale, -1, dw_listini_vendite_lista)
		case "C"
			window_open_parm(w_listino_distinta_tree, -1, dw_listini_vendite_lista)
//			window_open_parm(w_listino_distinta_comune, -1, dw_listini_vendite_lista)
	end choose
	
else

	window_open_parm(w_listino_distinta, -1, dw_listini_vendite_lista)
	
end if

end event

type cb_pers_cli from commandbutton within w_listini_vendite
integer x = 2743
integer y = 160
integer width = 366
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica"
end type

event clicked;string ls_cod_categoria, ls_cod_cat_mer, ls_cod_cliente, ls_cod_valuta, ls_cod_tipo_listino_prodotto

ls_cod_tipo_listino_prodotto = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_valuta")
ls_cod_categoria = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_categoria")
ls_cod_cat_mer = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_cat_mer")
ls_cod_cliente = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"cod_cliente")

if isnull(ls_cod_categoria) and isnull(ls_cod_cat_mer)  then
	s_cs_xx.parametri.parametro_s_3 = ls_cod_tipo_listino_prodotto
	s_cs_xx.parametri.parametro_s_2 = ls_cod_valuta	
	s_cs_xx.parametri.parametro_d_1 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"progressivo")
	s_cs_xx.parametri.parametro_data_1 = dw_listini_vendite_det_1.getitemdatetime(dw_listini_vendite_det_1.getrow(),"data_inizio_val")
	window_open(w_richiesta_clie_data, 0)	
else
	g_mb.messagebox("Apice", "Non è possibile duplicare un listino che abbia Codice Categoria, Codice Categoria Merce impostati")
end if	
	
end event

type r_1 from rectangle within w_listini_vendite
integer linethickness = 1
long fillcolor = 255
integer x = 3154
integer y = 20
integer width = 46
integer height = 560
end type

type dw_listini_vendite_lista from uo_cs_xx_dw within w_listini_vendite
integer x = 137
integer y = 40
integer width = 2587
integer height = 516
integer taborder = 120
string dataobject = "d_listini_vendite_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
if l_error < 1 then
	cb_listini_produzione.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT


end event

event pcd_new;call super::pcd_new;string ls_cod_valuta
datetime ldt_oggi


if i_extendmode then
	ldt_oggi = datetime(today())
	dw_listini_vendite_lista.setitem(dw_listini_vendite_lista.getrow(),"data_inizio_val", ldt_oggi)
end if

setnull(ls_cod_valuta)
select stringa
into   :ls_cod_valuta
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';

dw_listini_vendite_det_1.setitem(this.getrow(),"cod_valuta", ls_cod_valuta)

dw_listini_vendite_det_1.object.b_ricerca_cliente.enabled = true
dw_listini_vendite_det_1.object.b_ricerca_prodotto.enabled = true
cb_listini_produzione.enabled = true
dw_listini_vendite_det_2.object.b_range_1.enabled = false
dw_listini_vendite_det_2.object.b_range_2.enabled = false
dw_listini_vendite_det_2.object.b_range_3.enabled = false
dw_listini_vendite_det_2.object.b_range_4.enabled = false
dw_listini_vendite_det_2.object.b_range_5.enabled = false


ib_new = true
end event

event updatestart;call super::updatestart;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_tipo_scaglione
datetime ldt_data_inizio_val
long  ll_max_progressivo, ll_riga = 0, ll_i = 0
double ld_scaglione

setnull(ll_max_progressivo)

for ll_i = 1 to modifiedcount()
	ll_riga = getnextmodified(0, primary!)
	if ll_riga = 0 then exit
	
	wf_termine_scaglione()

	ll_max_progressivo = getitemnumber(ll_riga,"progressivo")
	if isnull(ll_max_progressivo) or ll_max_progressivo = 0 then
		setnull(ll_max_progressivo)
		ls_cod_tipo_listino_prodotto = getitemstring(ll_riga, "cod_tipo_listino_prodotto")
		ls_cod_valuta = getitemstring(ll_riga, "cod_valuta")
		ldt_data_inizio_val = getitemdatetime(ll_riga, "data_inizio_val")
		
		select max(progressivo)
		into  :ll_max_progressivo
		from  listini_vendite
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				cod_valuta = :ls_cod_valuta and
				data_inizio_val = :ldt_data_inizio_val;
		if isnull(ll_max_progressivo) then
			ll_max_progressivo = 1
		else
			ll_max_progressivo ++
		end if
		setitem(ll_riga, "progressivo", ll_max_progressivo)
	end if
next

// stefanop 25/05/2010: commentato perchè cancella i dati da una vista
//for ll_i = 1 to deletedcount()
//	ls_cod_tipo_listino_prodotto = getitemstring(ll_i, "cod_tipo_listino_prodotto", delete!, true)
//	ls_cod_valuta = getitemstring(ll_i, "cod_valuta", delete!, true)
//	ldt_data_inizio_val = getitemdatetime(ll_i, "data_inizio_val", delete!, true)
//	ll_max_progressivo = getitemnumber(ll_i,"progressivo", delete!, true)
//	
//	delete from listini_produzione
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
//			 cod_valuta = :ls_cod_valuta and
//			 data_inizio_val = :ldt_data_inizio_val and
//			 progressivo = :ll_max_progressivo;
//			 
//	delete from listini_vendite_dimensioni
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
//			 cod_valuta = :ls_cod_valuta and
//			 data_inizio_val = :ldt_data_inizio_val and
//			 progressivo = :ll_max_progressivo;
//next
// ----
end event

event pcd_validaterow;call super::pcd_validaterow;if isnull(getitemstring(i_rownbr,"cod_tipo_listino_prodotto")) then
	g_mb.messagebox("APICE","Tipo listino obbligatorio")
	pcca.error = c_fatal
end if

if isnull(getitemstring(i_rownbr,"cod_cliente")) and &
	isnull(getitemstring(i_rownbr,"cod_categoria")) and &
	isnull(getitemstring(i_rownbr,"cod_cat_mer")) and &
	isnull(getitemstring(i_rownbr,"cod_prodotto")) then
	g_mb.messagebox("APICE","E' obbligatorio inserire almeno un cliente, prodotto o una categoria di cliente o prodotto")
	pcca.error = c_fatal
end if

end event

event pcd_modify;call super::pcd_modify;dw_listini_vendite_det_1.object.b_ricerca_cliente.enabled = true
dw_listini_vendite_det_1.object.b_ricerca_prodotto.enabled = true
cb_listini_produzione.enabled = false
dw_listini_vendite_det_2.object.b_range_1.enabled = false
dw_listini_vendite_det_2.object.b_range_2.enabled = false
dw_listini_vendite_det_2.object.b_range_3.enabled = false
dw_listini_vendite_det_2.object.b_range_4.enabled = false
dw_listini_vendite_det_2.object.b_range_5.enabled = false
ib_modify = true

end event

event pcd_view;call super::pcd_view;dw_listini_vendite_det_1.object.b_ricerca_cliente.enabled = false
dw_listini_vendite_det_1.object.b_ricerca_prodotto.enabled = false
cb_listini_produzione.enabled = true
ib_new = false
ib_modify = false
dw_listini_vendite_det_2.object.b_range_1.enabled = true
dw_listini_vendite_det_2.object.b_range_2.enabled = true
dw_listini_vendite_det_2.object.b_range_3.enabled = true
dw_listini_vendite_det_2.object.b_range_4.enabled = true
dw_listini_vendite_det_2.object.b_range_5.enabled = true

end event

event pcd_refresh;call super::pcd_refresh;long riga

riga = getrow()

if i_refreshmode = c_refreshmodify or i_refreshmode = c_refreshRFC then 
	if getitemstatus(getrow(),0,primary!) = newmodified! then
		wf_abilita_colonne()
	else
		wf_disabilita_colonne()
	end if
end if
if i_refreshmode = c_refreshnew then wf_abilita_colonne()
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if this.getrow() > 0 then
		string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto
		datetime ldt_data_inizio_val
		long ll_progressivo,ll_cont
		
		ls_cod_tipo_listino_prodotto = getitemstring(getrow(),"cod_tipo_listino_prodotto")
		ls_cod_valuta =  getitemstring(getrow(),"cod_valuta")
		ldt_data_inizio_val = getitemdatetime(getrow(),"data_inizio_val")
		ll_progressivo = getitemnumber(getrow(),"progressivo")
		ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
		
		select count(*)
		into   :ll_cont
		from   listini_vendite_dimensioni
		WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto AND  
				 cod_valuta = :ls_cod_valuta AND  
				 data_inizio_val = :ldt_data_inizio_val AND  
				 progressivo = :ll_progressivo;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca listini per dimensione.")
			return
		end if
		
		if ll_cont > 0 then
			// colore rosso = presenza di listini per dimensione
			r_1.fillcolor = rgb(255,0,0)
		else
			// colore verde = assenza di listini per dimensione
			r_1.fillcolor = rgb(0,255,0)
		end if
		
//		if not isnull(ls_cod_prodotto) then
//			dw_listini_vendite_det_3.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
//		else
//			dw_listini_vendite_det_3.reset()
//		end if
	end if
end if
end event

type dw_folder_search from u_folder within w_listini_vendite
integer x = 23
integer y = 20
integer width = 3109
integer height = 560
integer taborder = 50
end type

type dw_listini_vendite_det_1 from uo_cs_xx_dw within w_listini_vendite
integer x = 23
integer y = 600
integer width = 2629
integer height = 1140
integer taborder = 180
string dataobject = "d_listini_vendite_det_1"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_listini_vendite_det_1,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_vendite_det_1,"cod_prodotto")
end choose
end event

type dw_listini_vendite_det_2 from uo_cs_xx_dw within w_listini_vendite
integer x = 23
integer y = 1740
integer width = 2619
integer height = 676
integer taborder = 70
string dataobject = "d_listini_vendite_det_2"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "flag_sconto_mag_prezzo_1"
			dw_listini_vendite_det_2.object.variazione_1.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_1", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_1")
		case "flag_sconto_mag_prezzo_2"
			dw_listini_vendite_det_2.object.variazione_2.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_2", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_2")
		case "flag_sconto_mag_prezzo_3"
			dw_listini_vendite_det_2.object.variazione_3.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_3", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_3")
		case "flag_sconto_mag_prezzo_4"
			dw_listini_vendite_det_2.object.variazione_4.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_4", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_4")
		case "flag_sconto_mag_prezzo_5"
			dw_listini_vendite_det_2.object.variazione_5.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_5", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_5")
		case "flag_origine_prezzo_1"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_1") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_2"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_2") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_3"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_3") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_4"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_4") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_5"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_5") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
	end choose
end if

end event

event clicked;call super::clicked;if dwo.name = "scaglione_1" then
	il_num_scaglione = 1
end if
end event

event buttonclicked;call super::buttonclicked;string ls_flag


s_cs_xx.parametri.parametro_s_1 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_2 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_tipo_listino_prodotto")
s_cs_xx.parametri.parametro_s_3 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_valuta")
s_cs_xx.parametri.parametro_data_1 = dw_listini_vendite_lista.getitemdatetime(dw_listini_vendite_lista.getrow(),"data_inizio_val")
s_cs_xx.parametri.parametro_d_1 = dw_listini_vendite_lista.getitemnumber(dw_listini_vendite_lista.getrow(),"progressivo")

choose case dwo.name
	case "b_range_1"
		s_cs_xx.parametri.parametro_d_2 = 1
	case "b_range_2"
		s_cs_xx.parametri.parametro_d_2 = 1
	case "b_range_3"
		s_cs_xx.parametri.parametro_d_2 = 1
	case "b_range_4"
		s_cs_xx.parametri.parametro_d_2 = 1
	case "b_range_5"
		s_cs_xx.parametri.parametro_d_2 = 1
end choose

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

if ls_flag = "S" then 
	choose case is_flag_tipo_vista
		case "G"
			window_open(w_listini_vendite_dimensioni, 0)
		case "L"
			window_open(w_listino_ven_dim_locale, 0)
		case "C"
			window_open(w_listino_ven_dim_comune, 0)
	end choose
else
	window_open(w_listini_vendite_dimensioni, 0)
end if
		
end event

type dw_ricerca from u_dw_search within w_listini_vendite
integer x = 137
integer y = 40
integer width = 2583
integer height = 500
integer taborder = 40
string dataobject = "d_listini_vendite_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
end choose
end event

