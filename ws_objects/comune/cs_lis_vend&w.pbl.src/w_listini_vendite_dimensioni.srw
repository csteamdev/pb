﻿$PBExportHeader$w_listini_vendite_dimensioni.srw
$PBExportComments$Finestra Listino vendite dimensioni
forward
global type w_listini_vendite_dimensioni from window
end type
type cb_5 from commandbutton within w_listini_vendite_dimensioni
end type
type cb_4 from commandbutton within w_listini_vendite_dimensioni
end type
type cb_listino_no_range from commandbutton within w_listini_vendite_dimensioni
end type
type cb_listino_range from commandbutton within w_listini_vendite_dimensioni
end type
type dw_variabili from datawindow within w_listini_vendite_dimensioni
end type
type dw_grid from uo_dw_grid within w_listini_vendite_dimensioni
end type
type cb_stampa from commandbutton within w_listini_vendite_dimensioni
end type
type cb_3 from commandbutton within w_listini_vendite_dimensioni
end type
type cb_2 from commandbutton within w_listini_vendite_dimensioni
end type
type cb_1 from commandbutton within w_listini_vendite_dimensioni
end type
end forward

global type w_listini_vendite_dimensioni from window
integer x = 498
integer y = 480
integer width = 4608
integer height = 2720
boolean titlebar = true
string title = "Dimensioni"
windowtype windowtype = response!
long backcolor = 12632256
event ue_controls ( )
cb_5 cb_5
cb_4 cb_4
cb_listino_no_range cb_listino_no_range
cb_listino_range cb_listino_range
dw_variabili dw_variabili
dw_grid dw_grid
cb_stampa cb_stampa
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
end type
global w_listini_vendite_dimensioni w_listini_vendite_dimensioni

type variables
boolean ib_esiste_listino=false
string is_cod_prodotto, is_cod_tipo_listino_prodotto, is_cod_valuta
datetime idt_data_inizio_val
long il_progressivo, il_num_scaglione, il_dim_x[], il_dim_y[], il_x, il_y, il_range
string is_flag_tipo_vista = "G"
string is_flag_multirange
end variables

forward prototypes
public function integer wf_listino_dimensioni ()
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_carica_dimensioni ()
public function string wf_componi_valore (string fs_flag_sconto_mag_prezzo, decimal ld_valore)
public function integer wf_scomponi_valore (string fs_stringa, ref string fs_flag_sconto_mag_prezzo, ref decimal fd_valore)
public function long wf_cerca_range ()
public function string wf_sql_variabili ()
public function integer wf_crea_listino_dimensioni (string fs_cod_prodotto, string fs_cod_listino, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, long fl_prog_range, ref long fl_dim_1[], ref long fl_dim_2[])
end prototypes

event ue_controls();//cb_1.move(width - cb_1.width - 20, height - cb_1.height - 20)
//cb_2.move(cb_1.x + cb_2.width - 20, cb_1.y)
//cb_3.move(20, cb_1.y)
//cb_stampa.move(cb_3.x + cb_3.width + 20, cb_1.y)
//
//dw_grid.move(20,20)
//dw_grid.resize(width - 20, cb_1.y - 40)
//
////cb_3.move(50,height - 190)
////cb_stampa.move(450,height - 190)
////
////cb_2.move(width - 860,height - 190)
////cb_1.move(width - 460,height - 190)
end event

public function integer wf_listino_dimensioni ();string ls_flag_tipo_dimensione_1, ls_flag_tipo_dimensione_2, ls_sql, ls_flag
long ll_limite_dimensione_1[], ll_limite_dimensione_2[], ll_i, ll_limite, ll_x, ll_y, &
     ll_num_scaglione

select flag_tipo_dimensione_1,
       flag_tipo_dimensione_2
into   :ls_flag_tipo_dimensione_1,
		 :ls_flag_tipo_dimensione_2
from   tab_prodotti_dimensioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :is_cod_prodotto;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Non sono state impostate le dimensioni in tabella prodotti dimensioni",stopsign!)
	return -1
end if

// ---------------------------------ricerco limiti della prima dimensione ------------------------------------
declare cu_limiti dynamic cursor for sqlsa;
ls_sql = "select limite_dimensione from tab_prodotti_dimensioni_det where cod_azienda = '" + s_cs_xx.cod_azienda + &
         "' and cod_prodotto = '" + is_cod_prodotto + &
			"' and flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_1 + "'"
prepare sqlsa from :ls_sql;
open dynamic cu_limiti;

ll_i = 1
do while 1=1
   fetch cu_limiti into :ll_limite;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_limite_dimensione_1[ll_i] = ll_limite 
	ll_i = ll_i + 1
loop
close cu_limiti;
// ---------------------------------ricerco limiti della seconda dimensione ----------------------------------
//declare cu_limiti1 dynamic cursor for sqlsa;
ls_sql = "select limite_dimensione from tab_prodotti_dimensioni_det where cod_azienda = '" + s_cs_xx.cod_azienda + &
         "' and cod_prodotto = '" + is_cod_prodotto + &
			"' and flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_2 + "'"
prepare sqlsa from :ls_sql;
open dynamic cu_limiti;

ll_i = 1
do while 1=1
   fetch cu_limiti into :ll_limite;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_limite_dimensione_2[ll_i] = ll_limite 
	ll_i = ll_i + 1
loop
close cu_limiti;

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

for ll_x = 1 to upperbound(ll_limite_dimensione_1)
	for ll_y = 1 to upperbound(ll_limite_dimensione_2)
		
		if ls_flag = "S" then		//  gestione listini comuni ptenda
			
			choose case is_flag_tipo_vista
				case "G"
					g_mb.messagebox("APICE","La variazione delle condizioni nel listino Generale non è ammessa; contattare l'amministratore.")
					rollback;
					return -1
					
				case "L"
					insert into listini_ven_dim_locale
								 ( cod_azienda,
									cod_tipo_listino_prodotto,
									cod_valuta,
									data_inizio_val,
									progressivo,
									num_scaglione,
									limite_dimensione_1,
									limite_dimensione_2,
									flag_sconto_mag_prezzo,
									variazione)
					values	(  :s_cs_xx.cod_azienda,
									:is_cod_tipo_listino_prodotto,
									:is_cod_valuta,
									:idt_data_inizio_val,
									:il_progressivo,
									:il_num_scaglione,
									:ll_limite_dimensione_1[ll_x],
									:ll_limite_dimensione_2[ll_y],
									'S',
									0 );
					
				case "C"
					insert into listini_ven_dim_comune
								 ( cod_azienda,
									cod_tipo_listino_prodotto,
									cod_valuta,
									data_inizio_val,
									progressivo,
									num_scaglione,
									limite_dimensione_1,
									limite_dimensione_2,
									flag_sconto_mag_prezzo,
									variazione)
					values	(  :s_cs_xx.cod_azienda,
									:is_cod_tipo_listino_prodotto,
									:is_cod_valuta,
									:idt_data_inizio_val,
									:il_progressivo,
									:il_num_scaglione,
									:ll_limite_dimensione_1[ll_x],
									:ll_limite_dimensione_2[ll_y],
									'S',
									0 );
					
			end choose

		else
			
			insert into listini_vendite_dimensioni
						 ( cod_azienda,
							cod_tipo_listino_prodotto,
							cod_valuta,
							data_inizio_val,
							progressivo,
							num_scaglione,
							limite_dimensione_1,
							limite_dimensione_2,
							flag_sconto_mag_prezzo,
							variazione)
			values	(  :s_cs_xx.cod_azienda,
							:is_cod_tipo_listino_prodotto,
							:is_cod_valuta,
							:idt_data_inizio_val,
							:il_progressivo,
							:il_num_scaglione,
							:ll_limite_dimensione_1[ll_x],
							:ll_limite_dimensione_2[ll_y],
							'S',
							0 );
		end if
		
		// controllo errore del DB
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in fase di creazione iniziale delle condizioni nella griglia.")
			rollback;
			return -1
		end if
	next
next
return 0



end function

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_carica_dimensioni ();string  ls_sql, ls_flag_sconto_mag_prezzo, ls_str, ls_return

integer ll_1, ll_2

long    ll_dim_1[], ll_dim_2[], ll_y, ll_x, ll_i, ll_limite_dim_1, ll_limite_dim_2, ldec_riga, ldec_colonna, ll_ret, ll_max_range

dec{4}  ld_variazione

ll_ret = wf_cerca_range()

if ll_ret = -1 then
	// errore
	return
elseif ll_ret = -2 then
	select ISNULL(max(prog_range),0)
	into	:ll_max_range
	from listini_vendite_dimensioni
	where cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
	cod_valuta = :is_cod_valuta and
	data_inizio_val = :idt_data_inizio_val and
	progressivo = :il_progressivo and
	num_scaglione = :il_num_scaglione;
	
	if sqlca.sqlcode = 0 then
		il_range = ll_max_range + 1
	else
		rollback;
		g_mb.error("Errore SQL in caricamento dimensioni prodotto (wf_carica_dimensioni)~r~n" + sqlca.sqlerrtext)
		return
	end if
	// non c'è alcun range bisogna crearlo
end if


if wf_crea_listino_dimensioni(is_cod_prodotto, &
						is_cod_tipo_listino_prodotto, &
						is_cod_valuta, &
						idt_data_inizio_val, &
						il_progressivo, &
						il_range, &
						il_num_scaglione, &
						ll_dim_1[], &
						ll_dim_2[]) < 0 then return

ll_x = upperbound(ll_dim_1)
ll_y = upperbound(ll_dim_2)

dw_grid.uof_init(dw_grid.MODE_VALUE_EDITABLE, ll_y, ll_x)

for ll_1 = 1 to ll_x
	dw_grid.setitem(0, ll_1, string(ll_dim_1[ll_1]))
	dw_grid.modify("#" + string(ll_1 + 1) + ".format=~"~tif(left(x_" + string(ll_1+ 1) +", 1) = '-', '[red][General]', '[General]')~"")
	il_dim_x[ll_1] = ll_dim_1[ll_1]
next

for ll_2 = 1 to ll_y
	dw_grid.setitem(ll_2, 0, string(ll_dim_2[ll_2]))
	il_dim_y[ll_2] = ll_dim_2[ll_2]
next

il_x = ll_x
il_y = ll_y

// ----------------------------------  scrivo foglio calcolo -------------------------------------------------

declare cu_dim dynamic cursor for sqlsa;
ls_sql = " SELECT limite_dimensione_1, limite_dimensione_2, flag_sconto_mag_prezzo, variazione " + &
         " FROM listini_vendite_dimensioni " + &
         " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
               " cod_tipo_listino_prodotto = '" + is_cod_tipo_listino_prodotto + "' AND " + &
               " cod_valuta = '" + is_cod_valuta + "' AND " + &
               " data_inizio_val = '"+ string(idt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' AND " + &
               " progressivo = " + string(il_progressivo) + " AND " + &
               " num_scaglione = " + string(il_num_scaglione) + " AND " + &
               " prog_range = " + string(il_range) + " " + &
			wf_sql_variabili() + &		
			" ORDER BY limite_dimensione_1 ASC , limite_dimensione_2 ASC "

prepare sqlsa from :ls_sql;

open dynamic cu_dim;
if sqlca.sqlcode < 0 then
	rollback;
	g_mb.error("Errore in lettura dimensioni prodotto (wf_carica_dimensioni)~r~n" + sqlca.sqlerrtext)
	return
end if

ll_x = 0
ll_y = 0
ll_i = 0
do while 1=1
   fetch cu_dim into :ll_limite_dim_1, :ll_limite_dim_2, :ls_flag_sconto_mag_prezzo, :ld_variazione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if ll_i <> ll_limite_dim_1 then
		ll_y = ll_y + 1
		ll_i = ll_limite_dim_1
		ll_x = 0
	end if
	ll_x = ll_x + 1
	ldec_riga = ll_x
	ldec_colonna = ll_y
	ls_str = wf_componi_valore(ls_flag_sconto_mag_prezzo, ld_variazione)
	dw_grid.setitem(ldec_riga, ldec_colonna, ls_str)
loop
close cu_dim;
commit;
return
end subroutine

public function string wf_componi_valore (string fs_flag_sconto_mag_prezzo, decimal ld_valore);string ls_return

choose case fs_flag_sconto_mag_prezzo
	case "S"  // sconto
		ls_return = string(ld_valore, "#,##0.00##")
		ls_return = "-" + ls_return + "%"
	case "M"
		ls_return = string(ld_valore, "#,##0.00##")
		ls_return = ls_return + "%"
	case "A"
		//ls_return = string(ld_valore)
		ls_return = "+" + string(ld_valore, "#,##0.00##")
	case "D"
		ls_return = "-" + string(ld_valore, "#,##0.00##")
	case "P"
		//ls_return = "L." + string(ld_valore)
		ls_return = string(ld_valore, "#,##0.00##")
	case else
		ls_return = "errore"
end choose

return ls_return
		
end function

public function integer wf_scomponi_valore (string fs_stringa, ref string fs_flag_sconto_mag_prezzo, ref decimal fd_valore);string ls_str

if left(fs_stringa,1) = "-" and right(fs_stringa,1) = "%" then
	ls_str = mid(fs_stringa,2,len(fs_stringa)-2)
	fs_flag_sconto_mag_prezzo = "S"
	fd_valore = double(ls_str)
	return 0
end if

if right(fs_stringa,1) = "%" then
	ls_str = left(fs_stringa,len(fs_stringa) -1)
	fs_flag_sconto_mag_prezzo = "M"
	fd_valore = double(ls_str)
	return 0
end if

if left(fs_stringa,2) = "+" then
	ls_str = mid(fs_stringa,2)
	fs_flag_sconto_mag_prezzo = "A"
	fd_valore = double(ls_str)	
	return 0
//	ls_str = mid(fs_stringa,3)
//	fs_flag_sconto_mag_prezzo = "P"
//	fd_valore = double(ls_str)
//	return 0
end if

if right(fs_stringa,1) = "-" then
	ls_str = mid(fs_stringa,2)
	fs_flag_sconto_mag_prezzo = "D"
	fd_valore = double(ls_str)
	return 0
end if

ls_str = fs_stringa
fs_flag_sconto_mag_prezzo = "P"
fd_valore = double(ls_str)

//ls_str = fs_stringa
//fs_flag_sconto_mag_prezzo = "A"
//fd_valore = double(ls_str)

return 0

end function

public function long wf_cerca_range ();string ls_sql,  ls_where_variabili, ls_cod_variabile_1, ls_cod_variabile_2, ls_cod_variabile_3, ls_cod_variabile_4, &
		ls_valore_var_1_str,ls_valore_var_2_str,ls_valore_var_3_str,ls_valore_var_4_str, &
		ls_flag_tipo_dato_1,ls_flag_tipo_dato_2,ls_flag_tipo_dato_3,ls_flag_tipo_dato_4, ls_errore
long   ll_range, ll_ret
dec{4} ld_valore_var_1_num,ld_valore_var_2_num,ld_valore_var_3_num,ld_valore_var_4_num
datastore lds_data


ls_where_variabili = wf_sql_variabili()

ls_sql = " select prog_range from	listini_vendite_dimensioni " + &
			" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
		 	" cod_tipo_listino_prodotto = '" + is_cod_tipo_listino_prodotto + "' and    " + &
			" cod_valuta      = '" + is_cod_valuta + "' and    " + &
		 	" data_inizio_val = '" + string(idt_data_inizio_val,s_cs_xx.db_funzioni.formato_data) + "' and    " + &
		 	" progressivo     = "+ string(il_progressivo) +" and    " + &
			" num_scaglione   = "+ string(il_num_scaglione) + &
			ls_where_variabili

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret < 0 then
	// errore SQL
	return -1
elseif ll_ret = 0 then
	// tutto ok ma non c'è alcun range
	return -2
else
	// trovato, procedo con il range corretto
	il_range = lds_data.getitemnumber(1,1)
end if



end function

public function string wf_sql_variabili ();string ls_sql,  ls_where_variabili, ls_cod_variabile_1, ls_cod_variabile_2, ls_cod_variabile_3, ls_cod_variabile_4, &
		ls_valore_var_1_str,ls_valore_var_2_str,ls_valore_var_3_str,ls_valore_var_4_str, &
		ls_flag_tipo_dato_1,ls_flag_tipo_dato_2,ls_flag_tipo_dato_3,ls_flag_tipo_dato_4, ls_errore
long   ll_range, ll_ret
dec{4} ld_valore_var_1_num,ld_valore_var_2_num,ld_valore_var_3_num,ld_valore_var_4_num
datastore lds_data

ls_cod_variabile_1 = dw_variabili.getitemstring(1,"cod_variabile_1")
ls_cod_variabile_2 = dw_variabili.getitemstring(1,"cod_variabile_2")
ls_cod_variabile_3 = dw_variabili.getitemstring(1,"cod_variabile_3")
ls_cod_variabile_4 = dw_variabili.getitemstring(1,"cod_variabile_4")


ls_where_variabili = ""

if not isnull(ls_cod_variabile_1) then
	ls_flag_tipo_dato_1 = f_des_tabella("tab_variabili_formule"," cod_variabile = '" + ls_cod_variabile_1 +"' ","flag_tipo_dato")
	ls_where_variabili += " and cod_variabile_1 ='" + ls_cod_variabile_1 + "' "
	if ls_flag_tipo_dato_1 = "S" then
		if not isnull(dw_variabili.getitemstring(1,"valore_var_1_str")) then
			ls_where_variabili += " and valore_stringa_1 ='" + dw_variabili.getitemstring(1,"valore_var_1_str") + "' "
		else
			ls_where_variabili += " and valore_stringa_1 is null "
		end if
	else
		if not isnull(dw_variabili.getitemnumber(1,"valore_var_1_num")) then
			ls_where_variabili += " and valore_numero_1 =" + f_decimal_string(dw_variabili.getitemnumber(1,"valore_var_1_num"))
		else
			ls_where_variabili += " and valore_numero_1 is null "
		end if
	end if	
end if

if not isnull(ls_cod_variabile_2) then
	ls_flag_tipo_dato_2 = f_des_tabella("tab_variabili_formule"," cod_variabile = '" + ls_cod_variabile_2 +"' ","flag_tipo_dato")
	ls_where_variabili += " and cod_variabile_2 ='" + ls_cod_variabile_2 + "' "
	if ls_flag_tipo_dato_2 = "S" then
		if not isnull(dw_variabili.getitemstring(1,"valore_var_1_str")) then
			ls_where_variabili += " and valore_stringa_2 ='" + dw_variabili.getitemstring(1,"valore_var_2_str") + "' "
		else
			ls_where_variabili += " and valore_stringa_2 is null "
		end if
	else
		if not isnull(dw_variabili.getitemnumber(1,"valore_var_2_num")) then
			ls_where_variabili += " and valore_numero_2 =" + f_decimal_string(dw_variabili.getitemnumber(1,"valore_var_2_num"))
		else
			ls_where_variabili += " and valore_numero_2 is null "
		end if
	end if		
end if

if not isnull(ls_cod_variabile_3) then
	ls_flag_tipo_dato_3 = f_des_tabella("tab_variabili_formule"," cod_variabile = '" + ls_cod_variabile_3 +"' ","flag_tipo_dato")
	ls_where_variabili += " and cod_variabile_3 ='" + ls_cod_variabile_3 + "' "
	if ls_flag_tipo_dato_3 = "S" then
		if not isnull(dw_variabili.getitemstring(1,"valore_var_3_str")) then
			ls_where_variabili += " and valore_stringa_3 ='" + dw_variabili.getitemstring(1,"valore_var_3_str") + "' "
		else		
			ls_where_variabili += " and valore_stringa_3 is null "
		end if
	else
		if not isnull(dw_variabili.getitemnumber(1,"valore_var_3_num")) then
			ls_where_variabili += " and valore_numero_3 =" + f_decimal_string(dw_variabili.getitemnumber(1,"valore_var_3_num"))
		else
			ls_where_variabili += " and valore_numero_3 is null "
		end if
	end if		
end if

if not isnull(ls_cod_variabile_4) then
	ls_flag_tipo_dato_4 = f_des_tabella("tab_variabili_formule"," cod_variabile = '" + ls_cod_variabile_4 +"' ","flag_tipo_dato")
	ls_where_variabili += " and cod_variabile_4 ='" + ls_cod_variabile_4 + "' "
	if ls_flag_tipo_dato_4 = "S" then
		if not isnull(dw_variabili.getitemstring(1,"valore_var_4_str")) then
			ls_where_variabili += " and valore_stringa_4 ='" + dw_variabili.getitemstring(1,"valore_var_4_str") + "' "
		else
			ls_where_variabili += " and valore_stringa_4  is null "
		end if
	else
		if not isnull(dw_variabili.getitemnumber(1,"valore_var_4_num")) then
			ls_where_variabili += " and valore_numero_4 =" + f_decimal_string(dw_variabili.getitemnumber(1,"valore_var_4_num"))
		else
			ls_where_variabili += " and valore_numero_4 is null "
		end if
	end if		
end if


return ls_where_variabili


end function

public function integer wf_crea_listino_dimensioni (string fs_cod_prodotto, string fs_cod_listino, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, long fl_prog_range, ref long fl_dim_1[], ref long fl_dim_2[]);string ls_flag_tipo_dimensione_1, ls_flag_tipo_dimensione_2, ls_sql, ls_flag, &
ls_cod_variabile_1, ls_valore_var_1_str,ls_cod_variabile_2, ls_valore_var_2_str,ls_cod_variabile_3, ls_valore_var_3_str,ls_cod_variabile_4, ls_valore_var_4_str
long   ll_num_intervalli_dim_1, ll_num_intervalli_dim_2,  ll_i, ll_z, ll_x, ll_y, ll_valore
dec{4} ld_valore_var_1_num, ld_valore_var_2_num, ld_valore_var_3_num,ld_valore_var_4_num





select flag_tipo_dimensione_1,
       	flag_tipo_dimensione_2,
		 num_intervalli_dim_1,
		 num_intervalli_dim_2
into   :ls_flag_tipo_dimensione_1,
       	:ls_flag_tipo_dimensione_2,
		 :ll_num_intervalli_dim_1,
		 :ll_num_intervalli_dim_2
from   tab_prodotti_dimensioni
where  tab_prodotti_dimensioni.cod_azienda = :s_cs_xx.cod_azienda and
		 tab_prodotti_dimensioni.cod_prodotto = :fs_cod_prodotto ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Non vi sono dimensioni caricate nella tabella Prodotti Dimensioni")
	return -1
end if

declare cu_dim dynamic cursor for sqlsa;
ls_sql = "SELECT limite_dimensione FROM tab_prodotti_dimensioni_det WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
         "cod_prodotto = '" + fs_cod_prodotto + "' AND " + &
			"flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_1 + "'" 

prepare sqlsa from :ls_sql;

open dynamic cu_dim;
if sqlca.sqlcode < 0 then
	rollback;
	g_mb.error("Errore SQL in OPEN curosor cu_dim (wf_crea_listino_dimensioni)~r~n" + sqlca.sqlerrtext)
	return -1
end if

ll_i = 0
do while 1=1
   fetch cu_dim into :ll_valore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	fl_dim_1[ll_i]	= ll_valore
loop
close cu_dim;
ll_x = ll_i

ls_sql = "SELECT limite_dimensione FROM tab_prodotti_dimensioni_det WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
         "cod_prodotto = '" + fs_cod_prodotto + "' AND " + &
			"flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_2 + "'" 

prepare sqlsa from :ls_sql;

open dynamic cu_dim;
if sqlca.sqlcode < 0 then
	rollback;
	g_mb.error("Errore SQL in OPEN curosor cu_dim (wf_crea_listino_dimensioni)~r~n" + sqlca.sqlerrtext)
	return -1
end if

ll_i = 0
do while 1=1
   fetch cu_dim into :ll_valore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	fl_dim_2[ll_i] = ll_valore
loop
close cu_dim;

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

ls_cod_variabile_1 = dw_variabili.getitemstring(1,"cod_variabile_1")
ls_cod_variabile_2 = dw_variabili.getitemstring(1,"cod_variabile_2")
ls_cod_variabile_3 = dw_variabili.getitemstring(1,"cod_variabile_3")
ls_cod_variabile_4 = dw_variabili.getitemstring(1,"cod_variabile_4")

ls_valore_var_1_str  = dw_variabili.getitemstring(1,"valore_var_1_str")
ls_valore_var_2_str  = dw_variabili.getitemstring(1,"valore_var_2_str")
ls_valore_var_3_str  = dw_variabili.getitemstring(1,"valore_var_3_str")
ls_valore_var_4_str  = dw_variabili.getitemstring(1,"valore_var_4_str")

ld_valore_var_1_num  = dw_variabili.getitemnumber(1,"valore_var_1_num")
ld_valore_var_2_num  = dw_variabili.getitemnumber(1,"valore_var_2_num")
ld_valore_var_3_num  = dw_variabili.getitemnumber(1,"valore_var_3_num")
ld_valore_var_4_num  = dw_variabili.getitemnumber(1,"valore_var_4_num")

if not isnull(ls_cod_variabile_1) and (isnull(ls_cod_variabile_1) or len(ls_cod_variabile_1) = 0) then
	g_mb.error("Attenzione assegnare un valore di ricerca per la variabile "  + ls_cod_variabile_1)
	return -1
end if
	
if not isnull(ls_cod_variabile_2) and (isnull(ls_cod_variabile_2) or len(ls_cod_variabile_2) = 0) then
	g_mb.error("Attenzione assegnare un valore di ricerca per la variabile "  + ls_cod_variabile_2)
	return -1
end if
	
if not isnull(ls_cod_variabile_3) and (isnull(ls_cod_variabile_3) or len(ls_cod_variabile_3) = 0) then
	g_mb.error("Attenzione assegnare un valore di ricerca per la variabile "  + ls_cod_variabile_3)
	return -1
end if
	
if not isnull(ls_cod_variabile_4) and (isnull(ls_cod_variabile_4) or len(ls_cod_variabile_4) = 0) then
	g_mb.error("Attenzione assegnare un valore di ricerca per la variabile "  + ls_cod_variabile_4)
	return -1
end if
	

ll_y = ll_i

for ll_i = 1 to ll_x
	for ll_z = 1 to ll_y
		
		if ls_flag = "S" then
			choose case is_flag_tipo_vista
				case "G"
					g_mb.messagebox("APICE","La variazione delle condizioni nel listino Generale non è ammessa; contattare l'amministratore.")
					rollback;
					return -1
					
				case "L"
					INSERT INTO listini_ven_dim_locale
								 (cod_azienda,   
								  cod_tipo_listino_prodotto,   
								  cod_valuta,   
								  data_inizio_val,   
								  progressivo,   
								  num_scaglione,   
								  prog_range,
								  limite_dimensione_1,   
								  limite_dimensione_2,   
								  flag_sconto_mag_prezzo,   
								  variazione,
								  cod_variabile_1,
								  valore_stringa_1,
								  valore_numero_1,
								  cod_variabile_2,
								  valore_stringa_2,
								  valore_numero_2,
								  cod_variabile_3,
								  valore_stringa_3,
								  valore_numero_3,
								  cod_variabile_4,
								  valore_stringa_4,
								  valore_numero_4
								 )  
					VALUES    (:s_cs_xx.cod_azienda,   
								  :fs_cod_listino,   
								  :fs_cod_valuta,   
								  :fdt_data_inizio_val,   
								  :fl_progressivo,   
								  :fl_prog_range,
								  :fl_num_scaglione,   
								  :fl_dim_1[ll_i],   
								  :fl_dim_2[ll_z],   
								  'S',   
								  0,
								  :ls_cod_variabile_1,
								  :ls_valore_var_1_str,
								  :ld_valore_var_1_num,
								  :ls_cod_variabile_2,
								  :ls_valore_var_2_str,
								  :ld_valore_var_2_num,
								  :ls_cod_variabile_3,
								  :ls_valore_var_3_str,
								  :ld_valore_var_3_num,
								  :ls_cod_variabile_4,
								  :ls_valore_var_4_str,
								  :ld_valore_var_4_num
								  )  ;
							
				case "C"
					INSERT INTO listini_ven_dim_comune
								 (cod_azienda,   
								  cod_tipo_listino_prodotto,   
								  cod_valuta,   
								  data_inizio_val,   
								  progressivo,   
								  num_scaglione,   
								  prog_range,
								  limite_dimensione_1,   
								  limite_dimensione_2,   
								  flag_sconto_mag_prezzo,   
								  variazione,
								  cod_variabile_1,
								  valore_stringa_1,
								  valore_numero_1,
								  cod_variabile_2,
								  valore_stringa_2,
								  valore_numero_2,
								  cod_variabile_3,
								  valore_stringa_3,
								  valore_numero_3,
								  cod_variabile_4,
								  valore_stringa_4,
								  valore_numero_4 )  
					VALUES    (:s_cs_xx.cod_azienda,   
								  :fs_cod_listino,   
								  :fs_cod_valuta,   
								  :fdt_data_inizio_val,   
								  :fl_progressivo,  
								  :fl_prog_range,
								  :fl_num_scaglione,   
								  :fl_dim_1[ll_i],   
								  :fl_dim_2[ll_z],   
								  'S',   
								  0,
								  :ls_cod_variabile_1,
								  :ls_valore_var_1_str,
								  :ld_valore_var_1_num,
								  :ls_cod_variabile_2,
								  :ls_valore_var_2_str,
								  :ld_valore_var_2_num,
								  :ls_cod_variabile_3,
								  :ls_valore_var_3_str,
								  :ld_valore_var_3_num,
								  :ls_cod_variabile_4,
								  :ls_valore_var_4_str,
								  :ld_valore_var_4_num)  ;
							
			end choose
			
		else
			INSERT INTO listini_vendite_dimensioni
						 (cod_azienda,   
						  cod_tipo_listino_prodotto,   
						  cod_valuta,   
						  data_inizio_val,   
						  progressivo,   
						  num_scaglione,   
						  prog_range,
						  limite_dimensione_1,   
						  limite_dimensione_2,   
						  flag_sconto_mag_prezzo,   
						  variazione,
						  cod_variabile_1,
						  valore_stringa_1,
						  valore_numero_1,
						  cod_variabile_2,
						  valore_stringa_2,
						  valore_numero_2,
						  cod_variabile_3,
						  valore_stringa_3,
						  valore_numero_3,
						  cod_variabile_4,
						  valore_stringa_4,
						  valore_numero_4 )  
			VALUES    (:s_cs_xx.cod_azienda,   
						  :fs_cod_listino,   
						  :fs_cod_valuta,   
						  :fdt_data_inizio_val,   
						  :fl_progressivo,   
						  :fl_prog_range,
						  :fl_num_scaglione,   
						  :fl_dim_1[ll_i],   
						  :fl_dim_2[ll_z],   
						  'S',   
						  0,
						  :ls_cod_variabile_1,
						  :ls_valore_var_1_str,
						  :ld_valore_var_1_num,
						  :ls_cod_variabile_2,
						  :ls_valore_var_2_str,
						  :ld_valore_var_2_num,
						  :ls_cod_variabile_3,
						  :ls_valore_var_3_str,
						  :ld_valore_var_3_num,
						  :ls_cod_variabile_4,
						  :ls_valore_var_4_str,
						  :ld_valore_var_4_num)  ;
		end if
	next
next	

return 0		 
end function

on w_listini_vendite_dimensioni.create
this.cb_5=create cb_5
this.cb_4=create cb_4
this.cb_listino_no_range=create cb_listino_no_range
this.cb_listino_range=create cb_listino_range
this.dw_variabili=create dw_variabili
this.dw_grid=create dw_grid
this.cb_stampa=create cb_stampa
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.Control[]={this.cb_5,&
this.cb_4,&
this.cb_listino_no_range,&
this.cb_listino_range,&
this.dw_variabili,&
this.dw_grid,&
this.cb_stampa,&
this.cb_3,&
this.cb_2,&
this.cb_1}
end on

on w_listini_vendite_dimensioni.destroy
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.cb_listino_no_range)
destroy(this.cb_listino_range)
destroy(this.dw_variabili)
destroy(this.dw_grid)
destroy(this.cb_stampa)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
end on

event open;integer rtn
long 	ll_cont, ll_screenwidht, ll_width
string ls_des_prodotto,ls_cod_variabile_1,ls_cod_variabile_2,ls_cod_variabile_3,ls_cod_variabile_4, &
		ls_des_variabile_1,ls_des_variabile_2,ls_des_variabile_3,ls_des_variabile_4, path
environment env


postevent("ue_controls")

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1
is_cod_tipo_listino_prodotto= s_cs_xx.parametri.parametro_s_2
is_cod_valuta= s_cs_xx.parametri.parametro_s_3
il_progressivo = s_cs_xx.parametri.parametro_d_1
idt_data_inizio_val = s_cs_xx.parametri.parametro_data_1
il_num_scaglione = s_cs_xx.parametri.parametro_d_2

select flag_ric_multirange
into	:is_flag_multirange
from	con_vendite
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.error("Attenzione! Il prodotto non è un modello per configuratore.")
	close(this)
end if

if is_flag_multirange="N" then 
	cb_listino_range.visible=false
	cb_listino_no_range.visible=false
	dw_variabili.visible=false
end if

select count(*)
into :ll_cont
from listini_vendite_dimensioni
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto AND  
		 cod_valuta = :is_cod_valuta AND  
		 data_inizio_val = :idt_data_inizio_val AND  
		 progressivo = :il_progressivo and
		 num_scaglione = :il_num_scaglione;
if ll_cont > 0 then
	ib_esiste_listino = true
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :is_cod_prodotto;
		 
title = is_cod_prodotto + " " + ls_des_prodotto		 
		 
rtn = GetEnvironment(env)
ll_screenwidht = env.ScreenWidth

ll_width = UnitsToPixels(this.width, XUnitsToPixels!)
ll_screenwidht = (ll_screenwidht - ll_width) / 2

this.x = ll_screenwidht

dw_variabili.insertrow(0)

if is_flag_multirange="S" then 
	select P.cod_variabile_lis_ven_range_1,
			V1.nome_campo_database + ' (' + isnull(V1.note,'') +')' ,
			P.cod_variabile_lis_ven_range_2,
			V2.nome_campo_database + ' (' + isnull(V2.note,'') +')',
			P.cod_variabile_lis_ven_range_3,
			V3.nome_campo_database + ' (' + isnull(V3.note,'') +')',
			P.cod_variabile_lis_ven_range_4,
			V4.nome_campo_database + ' (' + isnull(V4.note,'') +')'
	into	:ls_cod_variabile_1,
			:ls_des_variabile_1,
			:ls_cod_variabile_2,
			:ls_des_variabile_2,
			:ls_cod_variabile_3,
			:ls_des_variabile_3,
			:ls_cod_variabile_4,
			:ls_des_variabile_4
	from 	tab_flags_configuratore P
			left join tab_variabili_formule V1 on P.cod_azienda=V1.cod_azienda and P.cod_variabile_lis_ven_range_1=V1.cod_variabile
			left join tab_variabili_formule V2 on P.cod_azienda=V2.cod_azienda and P.cod_variabile_lis_ven_range_2=V2.cod_variabile
			left join tab_variabili_formule V3 on P.cod_azienda=V3.cod_azienda and P.cod_variabile_lis_ven_range_3=V3.cod_variabile
			left join tab_variabili_formule V4 on P.cod_azienda=V4.cod_azienda and P.cod_variabile_lis_ven_range_4=V4.cod_variabile
			where P.cod_azienda = :s_cs_xx.cod_azienda and
			P.cod_modello = :is_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore in fase di caricamento variabili range." + sqlca.sqlerrtext)
		return
	end if


	dw_variabili.setitem(1,"cod_variabile_1",ls_cod_variabile_1)
	dw_variabili.setitem(1,"cod_variabile_2",ls_cod_variabile_2)
	dw_variabili.setitem(1,"cod_variabile_3",ls_cod_variabile_3)
	dw_variabili.setitem(1,"cod_variabile_4",ls_cod_variabile_4)
	dw_variabili.setitem(1,"des_variabile_1",ls_des_variabile_1)
	dw_variabili.setitem(1,"des_variabile_2",ls_des_variabile_2)
	dw_variabili.setitem(1,"des_variabile_3",ls_des_variabile_3)
	dw_variabili.setitem(1,"des_variabile_4",ls_des_variabile_4)
	dw_variabili.accepttext()
else
	cb_listino_no_range.event clicked()
end if

end event

event close;if ib_esiste_listino = false then
	delete listini_vendite_dimensioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
			 cod_valuta = :is_cod_valuta and
			 data_inizio_val = :idt_data_inizio_val and
			 progressivo = :il_progressivo;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Cancellazione dimensioni","Errore in cancellazione condizioni x dimensione. Dettaglio Errore " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
	end if
end if

	
end event

event resize;long ll_offset

if is_flag_multirange = "S" then 
	ll_offset = 540
else
	ll_offset = 20
end if


cb_1.move(newwidth - cb_1.width - 20, newheight - cb_1.height - 20)
cb_2.move(cb_1.x - cb_2.width - 20, cb_1.y)
cb_3.move(20, cb_1.y)
cb_stampa.move(cb_3.x + cb_3.width + 20, cb_1.y)

dw_grid.move(20,ll_offset)
dw_grid.resize(newwidth - 20, cb_1.y -ll_offset)
end event

type cb_5 from commandbutton within w_listini_vendite_dimensioni
integer x = 2080
integer y = 2540
integer width = 411
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera Griglia"
end type

event clicked;if g_mb.messagebox("APICE","Imposto a ZERO tutti i valori della griglia?",Question!, YesNo!, 2) = 1 then
	
	long ll_x, ll_y
	
	for ll_x = 1 to  il_x +1
		
		for ll_y = 1 to  il_y + 1
			
			dw_grid.setitem(ll_y, ll_x, "0")
			
		next
	next
	
	
end if
dw_grid.accepttext()

end event

type cb_4 from commandbutton within w_listini_vendite_dimensioni
integer x = 457
integer y = 2540
integer width = 709
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina Range Corrente"
end type

event clicked;if g_mb.messagebox("APICE","Elimino tutte le condizioni associate a questa GRIGLIA?",Question!, YesNo!, 2) = 1 then
	
	g_mb.show( "Funzione in corso di sviluppo ")
	return
	
	
	string ls_flag
	
	ls_flag = "N"
	select flag
	into :ls_flag
	from parametri 
	where 	cod_parametro = 'GLC';
	
	if sqlca.sqlcode <> 0 then 	ls_flag = "N"
	
	if ls_flag ="S" then
		
		choose case is_flag_tipo_vista
			
			case "G"
				
				delete listini_vendite_dimensioni
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
			case "L"
				
				delete listini_ven_dim_locale
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
			case "C"
				
				delete listini_ven_dim_comune
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
		end choose
		
	else
		
		delete listini_vendite_dimensioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
				 cod_valuta = :is_cod_valuta and
				 data_inizio_val = :idt_data_inizio_val and
				 progressivo = :il_progressivo and
				 prog_range = :il_range;
		
	end if
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Cancellazione dimensioni","Errore in cancellazione condizioni x dimensione. Dettaglio Errore " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
	end if
	
end if

close(parent)
end event

type cb_listino_no_range from commandbutton within w_listini_vendite_dimensioni
integer x = 3749
integer y = 420
integer width = 809
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Listino Senza Variabili"
end type

event clicked;dw_variabili.reset()
dw_variabili.insertrow(0)

dw_variabili.accepttext()

cb_listino_range.enabled=false

wf_carica_dimensioni()

end event

type cb_listino_range from commandbutton within w_listini_vendite_dimensioni
integer x = 3109
integer y = 420
integer width = 594
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Listino"
end type

event clicked;dw_variabili.accepttext()

wf_carica_dimensioni()

end event

type dw_variabili from datawindow within w_listini_vendite_dimensioni
integer x = 23
integer y = 20
integer width = 3063
integer height = 520
integer taborder = 30
string title = "none"
string dataobject = "d_listini_vendite_dimensioni_variabili"
boolean border = false
boolean livescroll = true
end type

type dw_grid from uo_dw_grid within w_listini_vendite_dimensioni
integer x = 23
integer y = 540
integer width = 4549
integer height = 1960
integer taborder = 20
end type

type cb_stampa from commandbutton within w_listini_vendite_dimensioni
integer x = 3383
integer y = 2540
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long Job
PrintSetup()
Job = PrintOpen( )

parent.Print(Job, 0, 0)

PrintClose(Job)


end event

type cb_3 from commandbutton within w_listini_vendite_dimensioni
integer x = 23
integer y = 2540
integer width = 411
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina Tutto"
end type

event clicked;if g_mb.messagebox("APICE","Elimino tutte le condizioni associate a questa dimensione?",Question!, YesNo!, 2) = 1 then
	
	string ls_flag
	
	ls_flag = "N"
	select flag
	into :ls_flag
	from parametri 
	where 	cod_parametro = 'GLC';
	
	if sqlca.sqlcode <> 0 then 	ls_flag = "N"
	
	if ls_flag ="S" then
		
		choose case is_flag_tipo_vista
			
			case "G"
				
				delete listini_vendite_dimensioni
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
			case "L"
				
				delete listini_ven_dim_locale
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
			case "C"
				
				delete listini_ven_dim_comune
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
						 cod_valuta = :is_cod_valuta and
						 data_inizio_val = :idt_data_inizio_val and
						 progressivo = :il_progressivo and
						 prog_range = :il_range;
				
		end choose
		
	else
		
		delete listini_vendite_dimensioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
				 cod_valuta = :is_cod_valuta and
				 data_inizio_val = :idt_data_inizio_val and
				 progressivo = :il_progressivo and
				 prog_range = :il_range;
		
	end if
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Cancellazione dimensioni","Errore in cancellazione condizioni x dimensione. Dettaglio Errore " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
	end if
	
end if

close(parent)
end event

type cb_2 from commandbutton within w_listini_vendite_dimensioni
integer x = 3771
integer y = 2540
integer width = 384
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;rollback;
close(parent)
end event

type cb_1 from commandbutton within w_listini_vendite_dimensioni
integer x = 4183
integer y = 2540
integer width = 384
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "SALVA"
end type

event clicked;string ls_stringa, ls_flag_sconto_mag_prezzo, ls_flag
long   ll_return, ll_x, ll_y, ll_valore_dim_x, ll_valore_dim_y
dec{4} ld_valore

if g_mb.messagebox("APICE","Salvo le modifiche?",Question!,YesNo!,2) = 2 then return

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

dw_grid.accepttext()

for ll_x = 1 to il_x
	
	for ll_y = 1 to il_y
		
		ls_stringa = dw_grid.getitemstring(ll_y, ll_x)
		
		ll_return = wf_scomponi_valore(ls_stringa, ls_flag_sconto_mag_prezzo, ld_valore)
		
		ll_valore_dim_x = il_dim_x[ll_x]
		
		ll_valore_dim_y = il_dim_y[ll_y]
		
		if ls_flag ="S" then
		
			choose case is_flag_tipo_vista
				case "G"
					
					update listini_vendite_dimensioni
					set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
							 variazione  = :ld_valore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
							 cod_valuta      = :is_cod_valuta and
							 data_inizio_val = :idt_data_inizio_val and
							 progressivo     = :il_progressivo and
							 num_scaglione   = :il_num_scaglione and
							 limite_dimensione_1 = :il_dim_x[ll_x] and
							 limite_dimensione_2 = :il_dim_y[ll_y] and
							 prog_range = :il_range;
					
				case "L"
					
					update listini_ven_dim_locale
					set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
							 variazione  = :ld_valore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
							 cod_valuta      = :is_cod_valuta and
							 data_inizio_val = :idt_data_inizio_val and
							 progressivo     = :il_progressivo and
							 num_scaglione   = :il_num_scaglione and
							 limite_dimensione_1 = :il_dim_x[ll_x] and
							 limite_dimensione_2 = :il_dim_y[ll_y] and
							 prog_range = :il_range;
					
				case "C"
					
					update listini_ven_dim_comune
					set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
							 variazione  = :ld_valore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
							 cod_valuta      = :is_cod_valuta and
							 data_inizio_val = :idt_data_inizio_val and
							 progressivo     = :il_progressivo and
							 num_scaglione   = :il_num_scaglione and
							 limite_dimensione_1 = :il_dim_x[ll_x] and
							 limite_dimensione_2 = :il_dim_y[ll_y] and
							 prog_range = :il_range;
					
			end choose
		
		else
		
			update listini_vendite_dimensioni
			set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
					 variazione  = :ld_valore
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_listino_prodotto = :is_cod_tipo_listino_prodotto and
					 cod_valuta      = :is_cod_valuta and
					 data_inizio_val = :idt_data_inizio_val and
					 progressivo     = :il_progressivo and
					 num_scaglione   = :il_num_scaglione and
					 limite_dimensione_1 = :il_dim_x[ll_x] and
					 limite_dimensione_2 = :il_dim_y[ll_y] ;
					 
		end if
		
		if sqlca.sqlcode <>0 then
			g_mb.messagebox("Listini","Errore durante aggiornamento listini: " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	next
next

COMMIT; 

ib_esiste_listino = TRUE
close(parent)
end event

