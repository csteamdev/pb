﻿$PBExportHeader$w_listino_distinta.srw
$PBExportComments$Finestra Listini Produzione
forward
global type w_listino_distinta from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_listino_distinta
end type
type cb_2 from commandbutton within w_listino_distinta
end type
type cb_1 from cb_prod_ricerca within w_listino_distinta
end type
type dw_listini_produzione_lista from uo_cs_xx_dw within w_listino_distinta
end type
type cb_standard from commandbutton within w_listino_distinta
end type
type dw_listini_produzione_det_1 from uo_cs_xx_dw within w_listino_distinta
end type
type s_chiave_distinta from structure within w_listino_distinta
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
end type

global type w_listino_distinta from w_cs_xx_principale
integer width = 3803
integer height = 2440
string title = "Listino Componenti"
event ue_sql ( )
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
dw_listini_produzione_lista dw_listini_produzione_lista
cb_standard cb_standard
dw_listini_produzione_det_1 dw_listini_produzione_det_1
end type
global w_listino_distinta w_listino_distinta

type variables
boolean ib_new=false, ib_modify=false
long il_handle, il_num_scaglione
string is_cod_versione,is_cod_prodotto_finito
string is_flag_tipo_vista="G"

end variables

forward prototypes
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_termine_scaglione ()
public subroutine wf_sql (string old_table, string new_table)
end prototypes

event ue_sql();choose case is_flag_tipo_vista
		
	case "G" 
		return
		
	case "C"
		wf_sql("listini_produzione", "listini_prod_comune")
		
	case "L"
		wf_sql("listini_vendite", "listini_prod_locale")
		
end choose

end event

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_termine_scaglione ();string ls_tipo_scaglione
double ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5


ls_tipo_scaglione = dw_listini_produzione_det_1.getitemstring(dw_listini_produzione_det_1.getrow(),"flag_tipo_scaglioni")
ld_variazione_1 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_1")
ld_variazione_2 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_2")
ld_variazione_3 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_3")
ld_variazione_4 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_4")
ld_variazione_5 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_5")
if ld_variazione_1 = 0 or isnull(ld_variazione_1) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_2 = 0 or isnull(ld_variazione_2) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_3 = 0 or isnull(ld_variazione_3) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_2", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_2", 999999999)
	end if
elseif ld_variazione_4 = 0 or isnull(ld_variazione_4) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_3", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_3", 999999999)
	end if
elseif ld_variazione_5 = 0 or isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_4", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_4", 999999999)
	end if
end if

if ld_variazione_5 <> 0 and not isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_5", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_5", 999999999)
	end if
end if

end subroutine

public subroutine wf_sql (string old_table, string new_table);long start_pos=1, ll_ret = 0
string ls_sql, ll_err

// ------------  prima DW ----------------

ls_sql = dw_listini_produzione_lista.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_produzione_lista.reset()

//ll_ret = dw_listini_produzione_lista.setsqlselect(ls_sql)
ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")


// -------------- seconda DW ----------------------------------
ls_sql = dw_listini_produzione_det_1.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_produzione_det_1.reset()

//ll_ret = dw_listini_produzione_lista.setsqlselect(ls_sql)
ll_err = dw_listini_produzione_det_1.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")



end subroutine

on w_listino_distinta.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_listini_produzione_lista=create dw_listini_produzione_lista
this.cb_standard=create cb_standard
this.dw_listini_produzione_det_1=create dw_listini_produzione_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_listini_produzione_lista
this.Control[iCurrent+5]=this.cb_standard
this.Control[iCurrent+6]=this.dw_listini_produzione_det_1
end on

on w_listino_distinta.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_listini_produzione_lista)
destroy(this.cb_standard)
destroy(this.dw_listini_produzione_det_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
long Selected_Rows[]
unsignedlong lu_controlword,lu_controlword1

dw_listini_produzione_lista.set_dw_key("cod_azienda")
dw_listini_produzione_lista.set_dw_key("cod_tipo_listino_prodotto")
dw_listini_produzione_lista.set_dw_key("cod_valuta")
dw_listini_produzione_lista.set_dw_key("data_inizio_val")
dw_listini_produzione_lista.set_dw_key("progressivo")


ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

if ls_flag = "S" then 
	triggerevent("ue_sql")
end if

if ls_flag = "S" then 
	choose case is_flag_tipo_vista 
		case "G"
				lu_controlword = c_default + c_nonew + c_nomodify + c_nodelete
				lu_controlword1 = c_sharedata + c_scrollparent  + c_nonew + c_nomodify + c_nodelete
		case else
				lu_controlword = c_default
				lu_controlword1 = c_sharedata + c_scrollparent
	end choose
else
		lu_controlword = c_noretrieveonopen
		lu_controlword1 = c_sharedata + c_scrollparent
end if		


dw_listini_produzione_lista.set_dw_options(sqlca, &
                                   i_openparm, &
											  lu_controlword, &
											  c_default)
dw_listini_produzione_det_1.set_dw_options(sqlca, &
                                   dw_listini_produzione_lista, &
											 lu_controlword1, &
											  c_default)
uo_dw_main = dw_listini_produzione_lista



Selected_Rows[1] = 1
dw_listini_produzione_lista.Set_Selected_Rows(1, Selected_Rows[],   c_CheckForChanges, &
																						  c_RefreshChildren, &
																						  c_RefreshView)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_produzione_det_1, &
                 "cod_variabile", &
                 sqlca, &
                 "tab_variabili_formule", &
                 "cod_variabile", &
                 "des_variabile", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'" )

end event

type cb_3 from commandbutton within w_listino_distinta
integer x = 411
integer y = 2240
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long   ll_progressivo, ll_cont_comune,ll_cont_locale
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")

if g_mb.messagebox("APICE","Sei sicuto di voler cancellare tutti i dati presneti nella finestra?",Question!,YesNo!,2) = 2 then return

select count(*) 
into   :ll_cont_comune
from  listini_ven_comune
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
		cod_valuta = :ls_cod_valuta and 
		data_inizio_val = :ldt_data_inizio_val and 
		progressivo = :ll_progressivo ;
		
select count(*) 
into   :ll_cont_locale
from  listini_ven_locale
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
		cod_valuta = :ls_cod_valuta and 
		data_inizio_val = :ldt_data_inizio_val and 
		progressivo = :ll_progressivo ;
		
if ll_cont_comune > 0 and not isnull(ll_cont_comune) then
	delete listini_prod_comune
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo ;

elseif ll_cont_locale > 0 and not isnull(ll_cont_locale) then
	delete listini_prod_locale
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo ;
end if

commit;

parent.postevent("pc_retrieve")
end event

type cb_2 from commandbutton within w_listino_distinta
integer y = 2240
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;window_open_parm(w_listino_distinta_report, -1, dw_listini_produzione_lista)
end event

type cb_1 from cb_prod_ricerca within w_listino_distinta
integer x = 2331
integer y = 1000
integer width = 73
integer height = 80
integer taborder = 10
end type

event clicked;call super::clicked;dw_listini_produzione_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_figlio"
end event

type dw_listini_produzione_lista from uo_cs_xx_dw within w_listino_distinta
integer x = 23
integer width = 3726
integer height = 840
integer taborder = 30
string dataobject = "d_listini_produzione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode and this.getrow() > 0 then
	string ls_cod_prodotto
	
	is_cod_versione = this.getitemstring(this.getrow(),"cod_versione")
	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto_listino")
	f_po_loaddddw_dw(dw_listini_produzione_det_1, &
						  "cod_versione", &
						  sqlca, &
						  "distinta_padri", &
						  "cod_versione", &
						  "des_versione", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if

end event

event updatestart;call super::updatestart;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_tipo_scaglione, ls_cod_prodotto_listino, ls_cod_versione
datetime ldt_data_inizio_val
long  ll_max_progressivo, ll_progressivo, ll_i, ll_riga


for ll_i = 1 to modifiedcount()
	ll_riga = getnextmodified(0, primary!)
	if ll_riga = 0 then exit
	
	wf_termine_scaglione()
	
	ll_max_progressivo = getitemnumber(ll_riga,"prog_listino_produzione")
	if isnull(ll_max_progressivo) or ll_max_progressivo = 0 and rowcount() > 0 then
		setnull(ll_max_progressivo)
		ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
		ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
		ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
		ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
		ls_cod_prodotto_listino = getitemstring(ll_riga,"cod_prodotto_listino")
		ls_cod_versione = getitemstring(ll_riga,"cod_versione")
		
		select max(prog_listino_produzione)
		into  :ll_max_progressivo
		from  listini_produzione
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				cod_valuta = :ls_cod_valuta and
				data_inizio_val = :ldt_data_inizio_val and
				progressivo = :ll_progressivo and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :ls_cod_versione;
		if isnull(ll_max_progressivo) then
			ll_max_progressivo = 1
		else
			ll_max_progressivo ++
		end if
		setitem(ll_riga,"prog_listino_produzione", ll_max_progressivo)
	end if
next

end event

event pcd_modify;call super::pcd_modify;cb_1.enabled = true
ib_modify = true

end event

event pcd_new;call super::pcd_new;string ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_progressivo
datetime ldt_data_inizio_val

ib_new = true
cb_1.enabled = true
ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

this.setitem(this.getrow(), "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
this.setitem(this.getrow(), "cod_valuta", ls_cod_valuta)
this.setitem(this.getrow(), "data_inizio_val", ldt_data_inizio_val)
this.setitem(this.getrow(), "progressivo", ll_progressivo)
this.setitem(this.getrow(), "cod_prodotto_listino", ls_cod_prodotto)
f_po_loaddddw_dw(dw_listini_produzione_det_1, &
					  "cod_versione", &
					  sqlca, &
					  "distinta_padri", &
					  "cod_versione", &
					  "des_versione", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "'")

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long l_error, ll_progressivo
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

l_error = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_progressivo, l_Idx
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto_listino")) THEN
      SetItem(l_Idx, "cod_prodotto_listino", is_cod_prodotto_finito)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "data_inizio_val")) THEN
      SetItem(l_Idx, "data_inizio_val", ldt_data_inizio_val)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "progressivo")) or GetItemnumber(l_Idx, "progressivo") = 0 THEN
      SetItem(l_Idx, "progressivo", ll_progressivo)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;cb_1.enabled = false
ib_new = false
ib_modify = false
end event

type cb_standard from commandbutton within w_listino_distinta
integer x = 3337
integer y = 2240
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Standard"
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_listino, ls_cod_versione, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, &
       ls_cod_gruppo_variante, ls_des_listino_produzione, ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, ls_cod_prodotto, ls_flag_sconto_mag_prezzo_1, &
		 ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4,ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1, &
		 ls_flag_origine_prezzo_2,ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5
long ll_progressivo,ll_progressivo_std, ll_prog_listino_produzione, ll_num_sequenza, ll_progr
double ld_minimo_fatt_altezza, ld_minimo_fatt_larghezza, ld_minimo_fatt_profondita, ld_minimo_fatt_superficie, ld_minimo_fatt_volume, &
       ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5
datetime ldt_data_inizio_val, ldt_data_inizio_val_std

ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")
ls_cod_prodotto_listino = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_prodotto")

select data_inizio_val,
       progressivo
into   :ldt_data_inizio_val_std,
       :ll_progressivo_std
from   listini_vendite
where  cod_azienda               = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 cod_prodotto              = :ls_cod_prodotto_listino and
		 cod_cat_mer               is null and
		 cod_categoria             is null and
		 cod_cliente               is null and
		 data_inizio_val           <= :ldt_data_inizio_val
having cod_azienda               = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 cod_prodotto              = :ls_cod_prodotto_listino and
		 cod_cat_mer               is null and
		 cod_categoria             is null and
		 cod_cliente               is null and
		 data_inizio_val           =  max(data_inizio_val);
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Prodotto standard non presente o non trovato!")
	return
end if

delete from listini_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 data_inizio_val           = :ldt_data_inizio_val and
		 progressivo               = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione condizioni correnti. Dettaglio " + sqlca.sqlerrtext)
	rollback;
	return
end if

declare cu_listino_produzione cursor for
  SELECT cod_versione,   
			prog_listino_produzione,   
			cod_prodotto_padre,   
			num_sequenza,   
			cod_prodotto_figlio,   
			cod_gruppo_variante,   
			progr,   
			cod_prodotto,   
			des_listino_produzione,   
			minimo_fatt_altezza,   
			minimo_fatt_larghezza,   
			minimo_fatt_profondita,   
			minimo_fatt_superficie,   
			minimo_fatt_volume,   
			flag_sconto_a_parte,   
			flag_tipo_scaglioni,   
			scaglione_1,   
			scaglione_2,   
			scaglione_3,   
			scaglione_4,   
			scaglione_5,   
			flag_sconto_mag_prezzo_1,   
			flag_sconto_mag_prezzo_2,   
			flag_sconto_mag_prezzo_3,   
			flag_sconto_mag_prezzo_4,   
			flag_sconto_mag_prezzo_5,   
			variazione_1,   
			variazione_2,   
			variazione_3,   
			variazione_4,   
			variazione_5,   
			flag_origine_prezzo_1,   
			flag_origine_prezzo_2,   
			flag_origine_prezzo_3,   
			flag_origine_prezzo_4,   
			flag_origine_prezzo_5  
	 FROM listini_produzione  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto ) AND  
			( cod_valuta = :ls_cod_valuta ) AND  
			( data_inizio_val = :ldt_data_inizio_val_std ) AND  
			( progressivo = :ll_progressivo_std )   ;
open cu_listino_produzione;

do while 1=1
	fetch cu_listino_produzione into :ls_cod_versione, :ll_prog_listino_produzione, :ls_cod_prodotto_padre, :ll_num_sequenza, :ls_cod_prodotto_figlio, &
	      :ls_cod_gruppo_variante, :ll_progr, :ls_cod_prodotto, :ls_des_listino_produzione, :ld_minimo_fatt_altezza, :ld_minimo_fatt_larghezza, :ld_minimo_fatt_profondita, &
			:ld_minimo_fatt_superficie, :ld_minimo_fatt_volume, :ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ld_scaglione_1, :ld_scaglione_2, :ld_scaglione_3, &
			:ld_scaglione_4, :ld_scaglione_5, :ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, &
			:ls_flag_sconto_mag_prezzo_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, :ls_flag_origine_prezzo_1, &
			:ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in ricerca condizioni standard. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if

	INSERT INTO listini_produzione
		 (cod_azienda,   
		  cod_tipo_listino_prodotto,   
		  cod_valuta,   
		  data_inizio_val,   
		  progressivo,   
		  cod_prodotto_listino,   
		  cod_versione,   
		  prog_listino_produzione,   
		  cod_prodotto_padre,   
		  num_sequenza,   
		  cod_prodotto_figlio,   
		  cod_gruppo_variante,   
		  progr,   
		  cod_prodotto,   
		  des_listino_produzione,   
		  minimo_fatt_altezza,   
		  minimo_fatt_larghezza,   
		  minimo_fatt_profondita,   
		  minimo_fatt_superficie,   
		  minimo_fatt_volume,   
		  flag_sconto_a_parte,   
		  flag_tipo_scaglioni,   
		  scaglione_1,   
		  scaglione_2,   
		  scaglione_3,   
		  scaglione_4,   
		  scaglione_5,   
		  flag_sconto_mag_prezzo_1,   
		  flag_sconto_mag_prezzo_2,   
		  flag_sconto_mag_prezzo_3,   
		  flag_sconto_mag_prezzo_4,   
		  flag_sconto_mag_prezzo_5,   
		  variazione_1,   
		  variazione_2,   
		  variazione_3,   
		  variazione_4,   
		  variazione_5,   
		  flag_origine_prezzo_1,   
		  flag_origine_prezzo_2,   
		  flag_origine_prezzo_3,   
		  flag_origine_prezzo_4,   
		  flag_origine_prezzo_5 )  
	values(:s_cs_xx.cod_azienda,
			:ls_cod_tipo_listino_prodotto,
			:ls_cod_valuta,   
			:ldt_data_inizio_val,
			:ll_progressivo,
			:ls_cod_prodotto_listino,   
			:ls_cod_versione,   
			:ll_prog_listino_produzione,   
			:ls_cod_prodotto_padre,   
			:ll_num_sequenza,   
			:ls_cod_prodotto_figlio,   
			:ls_cod_gruppo_variante,   
			:ll_progr,   
			:ls_cod_prodotto,   
			:ls_des_listino_produzione,   
			:ld_minimo_fatt_altezza,   
			:ld_minimo_fatt_larghezza,   
			:ld_minimo_fatt_profondita,   
			:ld_minimo_fatt_superficie,   
			:ld_minimo_fatt_volume,   
			:ls_flag_sconto_a_parte,   
			:ls_flag_tipo_scaglioni,   
			:ld_scaglione_1,   
			:ld_scaglione_2,   
			:ld_scaglione_3,   
			:ld_scaglione_4,   
			:ld_scaglione_5,   
			:ls_flag_sconto_mag_prezzo_1,   
			:ls_flag_sconto_mag_prezzo_2,   
			:ls_flag_sconto_mag_prezzo_3,   
			:ls_flag_sconto_mag_prezzo_4,   
			:ls_flag_sconto_mag_prezzo_5,   
			:ld_variazione_1,   
			:ld_variazione_2,   
			:ld_variazione_3,   
			:ld_variazione_4,   
			:ld_variazione_5,   
			:ls_flag_origine_prezzo_1,   
			:ls_flag_origine_prezzo_2,   
			:ls_flag_origine_prezzo_3,   
			:ls_flag_origine_prezzo_4,   
			:ls_flag_origine_prezzo_5  );
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento condizioni da prodotto standard. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

close cu_listino_produzione;
g_mb.messagebox("APICE","Operazione eseguita con successo!")
commit;

end event

type dw_listini_produzione_det_1 from uo_cs_xx_dw within w_listino_distinta
integer x = 23
integer y = 860
integer width = 3726
integer height = 1360
integer taborder = 40
string dataobject = "d_listini_produzione_det_1"
borderstyle borderstyle = styleraised!
end type

