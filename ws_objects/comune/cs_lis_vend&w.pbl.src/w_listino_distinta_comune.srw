﻿$PBExportHeader$w_listino_distinta_comune.srw
forward
global type w_listino_distinta_comune from w_listino_distinta
end type
end forward

global type w_listino_distinta_comune from w_listino_distinta
end type
global w_listino_distinta_comune w_listino_distinta_comune

type variables
string is_flag_tipo_vista = "C"
end variables

on w_listino_distinta_comune.create
call super::create
end on

on w_listino_distinta_comune.destroy
call super::destroy
end on

type cb_2 from w_listino_distinta`cb_2 within w_listino_distinta_comune
end type

type cb_1 from w_listino_distinta`cb_1 within w_listino_distinta_comune
end type

type dw_listini_produzione_lista from w_listino_distinta`dw_listini_produzione_lista within w_listino_distinta_comune
end type

type cb_standard from w_listino_distinta`cb_standard within w_listino_distinta_comune
end type

type dw_listini_produzione_det_1 from w_listino_distinta`dw_listini_produzione_det_1 within w_listino_distinta_comune
end type

