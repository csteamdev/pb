﻿$PBExportHeader$w_listino_distinta_report.srw
$PBExportComments$Finestra Listini Produzione
forward
global type w_listino_distinta_report from w_cs_xx_principale
end type
type dw_listini_produzione_report from uo_cs_xx_dw within w_listino_distinta_report
end type
type s_chiave_distinta from structure within w_listino_distinta_report
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
end type

global type w_listino_distinta_report from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3867
integer height = 1968
string title = "Listino Componenti"
dw_listini_produzione_report dw_listini_produzione_report
end type
global w_listino_distinta_report w_listino_distinta_report

type variables
boolean ib_new=false, ib_modify=false
long il_handle, il_num_scaglione
string is_cod_versione,is_cod_prodotto_finito


end variables

on w_listino_distinta_report.create
int iCurrent
call super::create
this.dw_listini_produzione_report=create dw_listini_produzione_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_listini_produzione_report
end on

on w_listino_distinta_report.destroy
call super::destroy
destroy(this.dw_listini_produzione_report)
end on

event pc_setwindow;call super::pc_setwindow;
set_w_options(c_noresizewin)
save_on_close(c_socnosave)

dw_listini_produzione_report.set_dw_options(sqlca, &
                                   i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

uo_dw_main = dw_listini_produzione_report

dw_listini_produzione_report.object.datawindow.print.preview = 'Yes'

end event

type dw_listini_produzione_report from uo_cs_xx_dw within w_listino_distinta_report
integer x = 14
integer y = 16
integer width = 3799
integer height = 1828
integer taborder = 30
string dataobject = "d_listini_produzione_report"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long l_error, ll_progressivo
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

l_error = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

