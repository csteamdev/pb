﻿$PBExportHeader$w_listino_distinta_tree.srw
$PBExportComments$Finestra Listini Produzione
forward
global type w_listino_distinta_tree from w_cs_xx_principale
end type
type tv_1 from treeview within w_listino_distinta_tree
end type
type cb_3 from commandbutton within w_listino_distinta_tree
end type
type cb_2 from commandbutton within w_listino_distinta_tree
end type
type cb_1 from cb_prod_ricerca within w_listino_distinta_tree
end type
type dw_listini_produzione_lista from uo_cs_xx_dw within w_listino_distinta_tree
end type
type cb_standard from commandbutton within w_listino_distinta_tree
end type
type dw_listini_produzione_det_1 from uo_cs_xx_dw within w_listino_distinta_tree
end type
type s_chiave_distinta from structure within w_listino_distinta_tree
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
end type

global type w_listino_distinta_tree from w_cs_xx_principale
integer width = 4087
integer height = 2128
string title = "Listino Componenti"
event ue_sql ( )
event ue_duplica_generale_su_cliente ( )
event ue_nuova_condizione_cliente ( )
tv_1 tv_1
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
dw_listini_produzione_lista dw_listini_produzione_lista
cb_standard cb_standard
dw_listini_produzione_det_1 dw_listini_produzione_det_1
end type
global w_listino_distinta_tree w_listino_distinta_tree

type variables
boolean ib_new=false, ib_modify=false
long il_handle, il_num_scaglione
string is_cod_versione, is_cod_prodotto_finito, is_cod_cliente
string is_flag_tipo_vista="G", is_tabella_corrente="listini_produzione"
treeviewitem tvi_campo
string is_title
end variables

forward prototypes
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_termine_scaglione ()
public subroutine wf_sql (string old_table, string new_table)
public subroutine wf_carica_tree ()
end prototypes

event ue_sql();if isnull(is_cod_cliente) then

	choose case is_flag_tipo_vista
		case "G" 
			wf_sql(is_tabella_corrente, "listini_prod_comune")
			
		case "C"
			wf_sql(is_tabella_corrente, "listini_prod_comune")
			
		case "L"
			wf_sql(is_tabella_corrente, "listini_prod_locale")
			
	end choose

else

	choose case is_flag_tipo_vista
		case "G" 
			wf_sql(is_tabella_corrente, "listini_prod_comune_var_client")
			
		case "C"
			wf_sql(is_tabella_corrente, "listini_prod_comune_var_client")
			
		case "L"
			wf_sql(is_tabella_corrente, "listini_prod_comune_var_client")
			
	end choose
	
end if

end event

event ue_duplica_generale_su_cliente();// duplica le condizioni standard su un cliente specifico
string ls_cod_cliente, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_listino, ls_cod_versione, &
		 ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_gruppo_variante
long   ll_ret, ll_progressivo, ll_max_progr, ll_prog_listino_produzione, ll_num_sequenza, ll_progr
datetime ldt_data_inizio_val
any la_any[]
datawindow ldw_data

setredraw(false)

setnull(ldw_data)
guo_ricerca.uof_set_response()
guo_ricerca.uof_ricerca_cliente(ldw_data,"cod_cliente")
guo_ricerca.uof_get_results( la_any)
if(upperbound(la_any)>0) then
	ls_cod_cliente=la_any[1]
else 
	return
end if

setredraw(true)

//setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//setnull(s_cs_xx.parametri.parametro_uo_dw_search)
//s_cs_xx.parametri.parametro_tipo_ricerca = 2
//
//setredraw(false)
//
//window_open(w_clienti_ricerca_response, 0)
//ls_cod_cliente = s_cs_xx.parametri.parametro_s_1 
//setnull(s_cs_xx.parametri.parametro_s_1)

setredraw(true)

if g_mb.messagebox("APICE","Duplico le condizioni base sul cliente codice " + ls_cod_cliente + "?~r~nATTENZIONE: le condizioni precedenti saranno sovrascritte!",Question!, YesNo!,2) = 1 then
	
	ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
	ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
	ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")

	delete  listini_prod_comune_var_client
	where cod_azienda = :s_cs_xx.cod_azienda and
		   cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		   cod_valuta = :ls_cod_valuta and
		   data_inizio_val = :ldt_data_inizio_val and
		   progressivo = :ll_progressivo and 
		   cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in cancellazione condizioni vecchie del cliente~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	select 	max(prog_listino_produzione)
	into   	:ll_max_progr
	from   	listini_prod_comune_var_client
	where   cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			 cod_valuta  = :ls_cod_valuta and
			 data_inizio_val = :ldt_data_inizio_val and
			 progressivo = :ll_progressivo;

	if isnull(ll_max_progr) then ll_max_progr = 0
	

	declare cu_listini_prod cursor for
	select cod_prodotto_listino,   
			cod_versione,   
			prog_listino_produzione,   
			cod_prodotto_padre,   
			num_sequenza,   
			cod_prodotto_figlio,   
			cod_gruppo_variante,   
			progr
	from  listini_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			cod_valuta  = :ls_cod_valuta and
			data_inizio_val = :ldt_data_inizio_val and
			progressivo = :ll_progressivo;

	open cu_listini_prod;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in OPEN curosore (cu_listini_prod)~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	do while true
		fetch cu_listini_prod into :ls_cod_prodotto_listino, :ls_cod_versione, :ll_prog_listino_produzione, 
											:ls_cod_prodotto_padre, :ll_num_sequenza, :ls_cod_prodotto_figlio, 
											:ls_cod_gruppo_variante, :ll_progr;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE", "Errore in FETCH curosore (cu_listini_prod)~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		ll_max_progr ++

		INSERT INTO listini_prod_comune_var_client  
         ( cod_azienda,   
           cod_tipo_listino_prodotto,   
           cod_valuta,   
           data_inizio_val,   
           progressivo,   
           cod_prodotto_listino,   
           cod_versione,   
           prog_listino_produzione,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_gruppo_variante,   
           progr,   
           cod_prodotto,   
           des_listino_produzione,   
           minimo_fatt_altezza,   
           minimo_fatt_larghezza,   
           minimo_fatt_profondita,   
           minimo_fatt_superficie,   
           minimo_fatt_volume,   
           flag_sconto_a_parte,   
           flag_tipo_scaglioni,   
           scaglione_1,   
           scaglione_2,   
           scaglione_3,   
           scaglione_4,   
           scaglione_5,   
           flag_sconto_mag_prezzo_1,   
           flag_sconto_mag_prezzo_2,   
           flag_sconto_mag_prezzo_3,   
           flag_sconto_mag_prezzo_4,   
           flag_sconto_mag_prezzo_5,   
           variazione_1,   
           variazione_2,   
           variazione_3,   
           variazione_4,   
           variazione_5,   
           flag_origine_prezzo_1,   
           flag_origine_prezzo_2,   
           flag_origine_prezzo_3,   
           flag_origine_prezzo_4,   
           flag_origine_prezzo_5,   
           provvigione_1,   
           provvigione_2,   
           cod_cliente )  
     select listini_produzione.cod_azienda,   
            listini_produzione.cod_tipo_listino_prodotto,   
            listini_produzione.cod_valuta,   
            listini_produzione.data_inizio_val,   
            listini_produzione.progressivo,   
            listini_produzione.cod_prodotto_listino,   
            listini_produzione.cod_versione,   
            listini_produzione.prog_listino_produzione,   
            listini_produzione.cod_prodotto_padre,   
            listini_produzione.num_sequenza,   
            listini_produzione.cod_prodotto_figlio,   
            listini_produzione.cod_gruppo_variante,   
            listini_produzione.progr,   
            listini_produzione.cod_prodotto,   
            listini_produzione.des_listino_produzione,   
            listini_produzione.minimo_fatt_altezza,   
            listini_produzione.minimo_fatt_larghezza,   
            listini_produzione.minimo_fatt_profondita,   
            listini_produzione.minimo_fatt_superficie,   
            listini_produzione.minimo_fatt_volume,   
            listini_produzione.flag_sconto_a_parte,   
            listini_produzione.flag_tipo_scaglioni,   
            listini_produzione.scaglione_1,   
            listini_produzione.scaglione_2,   
            listini_produzione.scaglione_3,   
            listini_produzione.scaglione_4,   
            listini_produzione.scaglione_5,   
            listini_produzione.flag_sconto_mag_prezzo_1,   
            listini_produzione.flag_sconto_mag_prezzo_2,   
            listini_produzione.flag_sconto_mag_prezzo_3,   
            listini_produzione.flag_sconto_mag_prezzo_4,   
            listini_produzione.flag_sconto_mag_prezzo_5,   
            listini_produzione.variazione_1,   
            listini_produzione.variazione_2,   
            listini_produzione.variazione_3,   
            listini_produzione.variazione_4,   
            listini_produzione.variazione_5,   
            listini_produzione.flag_origine_prezzo_1,   
            listini_produzione.flag_origine_prezzo_2,   
            listini_produzione.flag_origine_prezzo_3,   
            listini_produzione.flag_origine_prezzo_4,   
            listini_produzione.flag_origine_prezzo_5,   
            listini_produzione.provvigione_1,   
            listini_produzione.provvigione_2,
            :ls_cod_cliente
       from listini_produzione
		 where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				cod_valuta = :ls_cod_valuta and
				data_inizio_val = :ldt_data_inizio_val and
				progressivo = :ll_progressivo and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :ls_cod_versione and
				prog_listino_produzione = :ll_prog_listino_produzione and
				cod_prodotto_padre = :ls_cod_prodotto_padre and
				num_sequenza = :ll_num_sequenza and
				cod_prodotto_figlio = :ls_cod_prodotto_figlio and
				cod_gruppo_variante = :ls_cod_gruppo_variante and
				progr = :ll_progr;
	
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE", "Errore in inserimenti condizioni cliente~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	loop
	
commit;	

end if

return
end event

event ue_nuova_condizione_cliente();// nuova condizione cliente vuota (non duplicata da nulla, inserimento manuale)
string ls_cod_cliente, ls_rag_soc_1
long   ll_risposta
str_listino_distinta_tree lstr_record

any la_any[]
datawindow ldw_data

setredraw(false)

setnull(ldw_data)
guo_ricerca.uof_set_response()
guo_ricerca.uof_ricerca_cliente(ldw_data,"cod_cliente")
guo_ricerca.uof_get_results( la_any)
if(upperbound(la_any)>0) then
	ls_cod_cliente=la_any[1]
else 
	return
end if

setredraw(true)

//setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//setnull(s_cs_xx.parametri.parametro_uo_dw_search)
//s_cs_xx.parametri.parametro_tipo_ricerca = 2
//
//setredraw(false)
//
//window_open(w_clienti_ricerca_response, 0)
//ls_cod_cliente = s_cs_xx.parametri.parametro_s_1 
//setnull(s_cs_xx.parametri.parametro_s_1)
//
//setredraw(true)
//	
select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", "Errore in ricerca ragione sociale cliente.~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

tvi_campo.label = ls_cod_cliente + " - " + ls_rag_soc_1
tvi_campo.children = false

lstr_record.codice = ls_cod_cliente
lstr_record.descrizione = ls_rag_soc_1
lstr_record.flag_livello = "C"
tvi_campo.data = lstr_record

tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1

tvi_campo.selected = false

ll_risposta = tv_1.insertitemlast(1, tvi_campo)

tv_1.selectitem( ll_risposta )

il_handle = ll_risposta

end event

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_termine_scaglione ();string ls_tipo_scaglione
double ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5


ls_tipo_scaglione = dw_listini_produzione_det_1.getitemstring(dw_listini_produzione_det_1.getrow(),"flag_tipo_scaglioni")
ld_variazione_1 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_1")
ld_variazione_2 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_2")
ld_variazione_3 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_3")
ld_variazione_4 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_4")
ld_variazione_5 = dw_listini_produzione_det_1.getitemnumber(dw_listini_produzione_det_1.getrow(),"variazione_5")
if ld_variazione_1 = 0 or isnull(ld_variazione_1) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_2 = 0 or isnull(ld_variazione_2) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_3 = 0 or isnull(ld_variazione_3) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_2", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_2", 999999999)
	end if
elseif ld_variazione_4 = 0 or isnull(ld_variazione_4) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_3", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_3", 999999999)
	end if
elseif ld_variazione_5 = 0 or isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_4", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_4", 999999999)
	end if
end if

if ld_variazione_5 <> 0 and not isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_5", 99999999)
	else
		dw_listini_produzione_det_1.setitem(dw_listini_produzione_det_1.getrow(),"scaglione_5", 999999999)
	end if
end if

end subroutine

public subroutine wf_sql (string old_table, string new_table);long start_pos=1, ll_ret = 0
string ls_sql, ll_err, ls_flag_tipo_vista

///---------------------------------------------------------------
string ls_flag
unsignedlong lu_controlword, lu_controlword1

ls_flag = "N"
select flag
into   :ls_flag
from   parametri 
where  cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"


if ls_flag = "S" then 
	choose case is_flag_tipo_vista 
		case "G"
				lu_controlword = c_default + c_nonew + c_nomodify + c_nodelete  + c_noretrieveonopen
				lu_controlword1 = c_sharedata + c_scrollparent  + c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen
		case else
				lu_controlword = c_default + c_noretrieveonopen
				lu_controlword1 = c_sharedata + c_scrollparent + c_noretrieveonopen
	end choose
else
		lu_controlword = c_noretrieveonopen + c_noretrieveonopen
		lu_controlword1 = c_sharedata + c_scrollparent + c_noretrieveonopen
end if		


dw_listini_produzione_lista.set_dw_options(sqlca, &
                                   i_openparm, &
											  lu_controlword, &
											  c_default)
dw_listini_produzione_det_1.set_dw_options(sqlca, &
                                   dw_listini_produzione_lista, &
											 lu_controlword1, &
											  c_default)
uo_dw_main = dw_listini_produzione_lista



// ------------  prima DW ----------------

ls_sql = dw_listini_produzione_lista.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_produzione_lista.reset()

ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")


// -------------- seconda DW ----------------------------------
ls_sql = dw_listini_produzione_det_1.getsqlselect( )

start_pos = Pos(ls_sql, old_table, start_pos)

DO WHILE start_pos > 0

    // Replace old_str with new_str.

    ls_sql = Replace(ls_sql, start_pos, Len(old_table), new_table)

    // Find the next occurrence of old_str.

    start_pos = Pos(ls_sql, old_table, start_pos+Len(new_table))

LOOP

dw_listini_produzione_det_1.reset()

//ll_ret = dw_listini_produzione_lista.setsqlselect(ls_sql)
ll_err = dw_listini_produzione_det_1.Modify("DataWindow.Table.Select='"+ls_sql+"'")
ll_err = dw_listini_produzione_lista.Modify("DataWindow.Table.UpdateTable='"+new_table+"'")

if new_table = "listini_prod_comune_var_client" then
	dw_listini_produzione_lista.Object.cod_cliente.Key = "Yes"
	dw_listini_produzione_det_1.Object.cod_cliente.Key = "Yes"
else
	dw_listini_produzione_lista.Object.cod_cliente.Key = "No"
	dw_listini_produzione_det_1.Object.cod_cliente.Key = "No"
end if

is_tabella_corrente = new_table

end subroutine

public subroutine wf_carica_tree ();boolean lb_inizio=true
string ls_cod_cliente, ls_rag_soc_1, ls_cod_prodotto, ls_des_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_handle, ll_risposta, ll_progressivo, ll_handle_parent
datetime ldt_data_inizio_val
str_listino_distinta_tree lstr_record

tv_1.deleteitem(0)

setpointer(HourGlass!)

tv_1.setredraw(false)

tv_1.deletepictures()
tv_1.PictureHeight = 16
tv_1.PictureWidth = 16

tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_apri.bmp")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "cartella_chiudi.bmp")


ls_cod_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_prodotto")

select 	des_prodotto
into   	:ls_des_prodotto
from   	anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       	cod_prodotto = :ls_cod_prodotto;

tvi_campo.label = ls_cod_prodotto + " - " + ls_des_prodotto
tvi_campo.children = true

lstr_record.codice = ls_cod_prodotto
lstr_record.descrizione = ls_des_prodotto
lstr_record.flag_livello = "P"
tvi_campo.data = lstr_record

tvi_campo.pictureindex = 2
tvi_campo.selectedpictureindex = 2
tvi_campo.overlaypictureindex = 2

tvi_campo.selected = true
	
ll_handle_parent = tv_1.insertitemlast(ll_handle, tvi_campo)

// ------  Inizio il ciclo di caricamento dei clienti che hanno condizioni personalizzate -----------

ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")

declare cu_clienti_produzione cursor for
select distinct cod_cliente
from   	listini_prod_comune_var_client
where  cod_azienda = :s_cs_xx.cod_azienda and
       	cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		cod_valuta = :ls_cod_valuta and
		data_inizio_val = :ldt_data_inizio_val and
		progressivo = :ll_progressivo and
		cod_cliente is not null;
		 
open cu_clienti_produzione;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore in OPEN cursore (cu_clienti_produzione)~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_clienti_produzione into :ls_cod_cliente;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore in FETCH cursore (cu_clienti_produzione)~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	select rag_soc_1
	into   :ls_rag_soc_1
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in ricerca ragione sociale cliente.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

	tvi_campo.label = ls_cod_cliente + " - " + ls_rag_soc_1
	tvi_campo.children = false

	lstr_record.codice = ls_cod_cliente
	lstr_record.descrizione = ls_rag_soc_1
	lstr_record.flag_livello = "C"
	tvi_campo.data = lstr_record

	tvi_campo.pictureindex = 1
	tvi_campo.selectedpictureindex = 1
	tvi_campo.overlaypictureindex = 1

	tvi_campo.selected = false

	ll_risposta = tv_1.insertitemlast(ll_handle_parent, tvi_campo)

loop

tv_1.setredraw(true)

end subroutine

on w_listino_distinta_tree.create
int iCurrent
call super::create
this.tv_1=create tv_1
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_listini_produzione_lista=create dw_listini_produzione_lista
this.cb_standard=create cb_standard
this.dw_listini_produzione_det_1=create dw_listini_produzione_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tv_1
this.Control[iCurrent+2]=this.cb_3
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.dw_listini_produzione_lista
this.Control[iCurrent+6]=this.cb_standard
this.Control[iCurrent+7]=this.dw_listini_produzione_det_1
end on

on w_listino_distinta_tree.destroy
call super::destroy
destroy(this.tv_1)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_listini_produzione_lista)
destroy(this.cb_standard)
destroy(this.dw_listini_produzione_det_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag
long Selected_Rows[]
unsignedlong lu_controlword,lu_controlword1

dw_listini_produzione_lista.set_dw_key("cod_azienda")
dw_listini_produzione_lista.set_dw_key("cod_tipo_listino_prodotto")
dw_listini_produzione_lista.set_dw_key("cod_valuta")
dw_listini_produzione_lista.set_dw_key("data_inizio_val")
dw_listini_produzione_lista.set_dw_key("progressivo")

is_flag_tipo_vista = s_cs_xx.parametri.parametro_s_12
setnull(s_cs_xx.parametri.parametro_s_12)

ls_flag = "N"
select flag
into :ls_flag
from parametri 
where 	cod_parametro = 'GLC';

if sqlca.sqlcode <> 0 then 	ls_flag = "N"

if ls_flag = "S" then 
	triggerevent("ue_sql")
end if

if ls_flag = "S" then 
	choose case is_flag_tipo_vista 
		case "G"
				lu_controlword = c_default + c_nonew + c_nomodify + c_nodelete  + c_noretrieveonopen
				lu_controlword1 = c_sharedata + c_scrollparent  + c_nonew + c_nomodify + c_nodelete + c_noretrieveonopen
		case else
				lu_controlword = c_default + c_noretrieveonopen
				lu_controlword1 = c_sharedata + c_scrollparent + c_noretrieveonopen
	end choose
else
		lu_controlword = c_noretrieveonopen + c_noretrieveonopen
		lu_controlword1 = c_sharedata + c_scrollparent + c_noretrieveonopen
end if		


dw_listini_produzione_lista.set_dw_options(sqlca, &
                                   i_openparm, &
											  lu_controlword, &
											  c_default)
dw_listini_produzione_det_1.set_dw_options(sqlca, &
                                   dw_listini_produzione_lista, &
											 lu_controlword1, &
											  c_default)
iuo_dw_main = dw_listini_produzione_lista

is_title = title																						  
wf_carica_tree( )
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_produzione_det_1, &
                 "cod_variabile", &
                 sqlca, &
                 "tab_variabili_formule", &
                 "cod_variabile", &
                 "isnull(nome_campo_database, cod_variabile) ", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type tv_1 from treeview within w_listino_distinta_tree
integer x = 5
integer y = 4
integer width = 887
integer height = 2012
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean linesatroot = true
string picturename[] = {"C:\cs_115\framework\RISORSE\MENU_cartella_apri.ico","C:\cs_115\framework\RISORSE\MENU_cartella_chiudi.ico"}
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event selectionchanged;treeviewitem ltv_item
str_listino_distinta_tree lws_record

if isnull(newhandle) or newhandle <= 0 then
	return 0
end if

il_handle = newhandle
getitem(newhandle,ltv_item)
lws_record = ltv_item.data

if il_handle > 1 then
	//non è il prodotto standard ma si tratta di una condizione cliente
	is_cod_cliente = lws_record.codice
	parent.title = is_title + " - " + is_cod_cliente + " " + f_des_tabella("anag_clienti", "cod_cliente ='" + is_cod_cliente + "'", "rag_soc_1")
else
	setnull(is_cod_cliente)
	parent.title = is_title
end if

parent.triggerevent("ue_sql")

dw_listini_produzione_lista.change_dw_current()

parent.postevent("pc_retrieve")

end event

event rightclicked;if is_flag_tipo_vista <> "G" then
	
	treeviewitem ltv_item
	str_listino_distinta_tree lws_record
	
	getitem(handle,ltv_item)
	lws_record = ltv_item.data
	
	il_handle = handle
	
	if lws_record.flag_livello = "P" then
		
		// non sono im modifica o nuovo
		m_listini_distinta_tree lm_menu
		lm_menu = create m_listini_distinta_tree
		
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		destroy lm_menu
		
	end if
	
end if
end event

type cb_3 from commandbutton within w_listino_distinta_tree
integer x = 1321
integer y = 1916
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long   ll_progressivo, ll_cont_comune,ll_cont_locale
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")

if g_mb.messagebox("APICE","Sei sicuto di voler cancellare tutti i dati presneti nella finestra?",Question!,YesNo!,2) = 2 then return

select count(*) 
into   :ll_cont_comune
from  listini_ven_comune
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
		cod_valuta = :ls_cod_valuta and 
		data_inizio_val = :ldt_data_inizio_val and 
		progressivo = :ll_progressivo ;
		
select count(*) 
into   :ll_cont_locale
from  listini_ven_locale
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
		cod_valuta = :ls_cod_valuta and 
		data_inizio_val = :ldt_data_inizio_val and 
		progressivo = :ll_progressivo ;
		
if ll_cont_comune > 0 and not isnull(ll_cont_comune) then
	delete listini_prod_comune
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo ;

elseif ll_cont_locale > 0 and not isnull(ll_cont_locale) then
	delete listini_prod_locale
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and 
			cod_valuta = :ls_cod_valuta and 
			data_inizio_val = :ldt_data_inizio_val and 
			progressivo = :ll_progressivo ;
end if

commit;

parent.postevent("pc_retrieve")
end event

type cb_2 from commandbutton within w_listino_distinta_tree
integer x = 910
integer y = 1916
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;window_open_parm(w_listino_distinta_report, -1, dw_listini_produzione_lista)
end event

type cb_1 from cb_prod_ricerca within w_listino_distinta_tree
integer x = 3529
integer y = 800
integer width = 73
integer height = 80
integer taborder = 10
end type

event clicked;call super::clicked;dw_listini_produzione_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_figlio"
end event

type dw_listini_produzione_lista from uo_cs_xx_dw within w_listino_distinta_tree
integer x = 901
integer y = 20
integer width = 3131
integer height = 516
integer taborder = 30
string dataobject = "d_listini_produzione_tree_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode and this.getrow() > 0 then
	string ls_cod_prodotto
	
	is_cod_versione = this.getitemstring(this.getrow(),"cod_versione")
	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto_listino")
	f_po_loaddddw_dw(dw_listini_produzione_det_1, &
						  "cod_versione", &
						  sqlca, &
						  "distinta_padri", &
						  "cod_versione", &
						  "des_versione", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if

end event

event updatestart;call super::updatestart;string		ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_tipo_scaglione, ls_cod_prodotto_listino, ls_cod_versione,&
				ls_property, ls_value, ls_cod_variante
datetime 	ldt_data_inizio_val
long  		ll_max_progressivo, ll_progressivo, ll_i, ll_riga


for ll_i = 1 to modifiedcount()
	ll_riga = getnextmodified(0, primary!)
	if ll_riga = 0 then exit
	
	wf_termine_scaglione()
	
	ls_property = "DataWindow.Table.UpdateTable"
	ls_value = this.Describe(ls_property)
	
	if ls_value = "listini_prod_comune" or ls_value= "listini_prod_locale" then
		//calcola il max +1 dalla vista
		
		ll_max_progressivo = getitemnumber(ll_riga,"prog_listino_produzione")
		if isnull(ll_max_progressivo) or ll_max_progressivo = 0 and rowcount() > 0 then
			setnull(ll_max_progressivo)
			ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
			ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
			ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
			ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
			ls_cod_prodotto_listino = getitemstring(ll_riga,"cod_prodotto_listino")
			ls_cod_versione = getitemstring(ll_riga,"cod_versione")
			
			select max(prog_listino_produzione)
			into  :ll_max_progressivo
			from  listini_produzione
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
					cod_valuta = :ls_cod_valuta and
					data_inizio_val = :ldt_data_inizio_val and
					progressivo = :ll_progressivo and
					cod_prodotto_listino = :ls_cod_prodotto_listino and
					cod_versione = :ls_cod_versione;
			if isnull(ll_max_progressivo) then
				ll_max_progressivo = 1
			else
				ll_max_progressivo ++
			end if
			setitem(ll_riga,"prog_listino_produzione", ll_max_progressivo)
		end if
		
	else
		//tabella listini_prod_comune_var_client
		//non calcolare il max + 1 dalla vista, ma devo recuperare la PK della condizione variante standard corrispondente
		
		//recupero i valori
		ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
		ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
		ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
		ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
		ls_cod_prodotto_listino = getitemstring(ll_riga,"cod_prodotto_listino")
		ls_cod_versione = getitemstring(ll_riga,"cod_versione")
		
		//recupero il codice della variante
		ls_cod_variante = getitemstring(ll_riga,"cod_prodotto_figlio")
		
		//prendo il progressivo che corrisponde alla condizione della variante standard
		select prog_listino_produzione
		into :ll_max_progressivo
		from listini_prod_comune
		where cod_azienda=:s_cs_xx.cod_azienda 	and cod_tipo_listino_prodotto=:ls_cod_tipo_listino_prodotto and
				cod_valuta=:ls_cod_valuta and data_inizio_val=:ldt_data_inizio_val and
				progressivo=:ll_progressivo and cod_prodotto_listino=:ls_cod_prodotto_listino 	and 
				cod_versione=:ls_cod_versione and cod_prodotto_figlio=:ls_cod_variante;
		
		setitem(ll_riga,"prog_listino_produzione", ll_max_progressivo)
		
		//sperando che il cod_cliente sia valorizzato
	end if	
next

end event

event pcd_modify;call super::pcd_modify;cb_1.enabled = true
ib_modify = true

end event

event pcd_new;call super::pcd_new;string ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_null
long ll_progressivo, ll_handle
datetime ldt_data_inizio_val
str_listino_distinta_tree lstr_record
treeviewitem ltv_item

ib_new = true
cb_1.enabled = true
setnull(ls_null)

tv_1.getitem(il_handle,ltv_item)
lstr_record = ltv_item.data

if lstr_record.flag_livello = "C" then
	this.setitem(this.getrow(), "cod_cliente", lstr_record.codice)
else	
	this.setitem(this.getrow(), "cod_cliente", ls_null)
end if

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta   = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo  = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

this.setitem(this.getrow(), "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
this.setitem(this.getrow(), "cod_valuta", ls_cod_valuta)
this.setitem(this.getrow(), "data_inizio_val", ldt_data_inizio_val)
this.setitem(this.getrow(), "progressivo", ll_progressivo)
this.setitem(this.getrow(), "cod_prodotto_listino", ls_cod_prodotto)

f_po_loaddddw_dw(dw_listini_produzione_det_1, &
					  "cod_versione", &
					  sqlca, &
					  "distinta_padri", &
					  "cod_versione", &
					  "des_versione", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "'")

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_progressivo, l_Idx
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto_listino")) THEN
      SetItem(l_Idx, "cod_prodotto_listino", is_cod_prodotto_finito)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "data_inizio_val")) THEN
      SetItem(l_Idx, "data_inizio_val", ldt_data_inizio_val)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "progressivo")) or GetItemnumber(l_Idx, "progressivo") = 0 THEN
      SetItem(l_Idx, "progressivo", ll_progressivo)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;cb_1.enabled = false
ib_new = false
ib_modify = false
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long l_error, ll_progressivo
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

l_error = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo, is_cod_cliente)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event sqlpreview;call super::sqlpreview;STRING LS_SQL

LS_SQL = sqlsyntax

ls_sql = dataobject

LS_SQL = ""
end event

event pcd_delete;call super::pcd_delete;if lower(is_tabella_corrente) = "listini_prod_comune" then
	g_mb.messagebox("APICE","Attenzione stai cancellando un listino COMUNE che potrebbe influire sul funzionamento delle condizioni degli altri LISTINI NEL DB LOCALE",Information!)
end if
end event

type cb_standard from commandbutton within w_listino_distinta_tree
integer x = 3643
integer y = 1920
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Standard"
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto_listino, ls_cod_versione, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, &
       ls_cod_gruppo_variante, ls_des_listino_produzione, ls_flag_sconto_a_parte, ls_flag_tipo_scaglioni, ls_cod_prodotto, ls_flag_sconto_mag_prezzo_1, &
		 ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3, ls_flag_sconto_mag_prezzo_4,ls_flag_sconto_mag_prezzo_5, ls_flag_origine_prezzo_1, &
		 ls_flag_origine_prezzo_2,ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, ls_flag_origine_prezzo_5
long ll_progressivo,ll_progressivo_std, ll_prog_listino_produzione, ll_num_sequenza, ll_progr
double ld_minimo_fatt_altezza, ld_minimo_fatt_larghezza, ld_minimo_fatt_profondita, ld_minimo_fatt_superficie, ld_minimo_fatt_volume, &
       ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5
datetime ldt_data_inizio_val, ldt_data_inizio_val_std

ls_cod_tipo_listino_prodotto = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = dw_listini_produzione_lista.i_parentdw.getitemdatetime(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = dw_listini_produzione_lista.i_parentdw.getitemnumber(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "progressivo")
ls_cod_prodotto_listino = dw_listini_produzione_lista.i_parentdw.getitemstring(dw_listini_produzione_lista.i_parentdw.i_selectedrows[1], "cod_prodotto")

select data_inizio_val,
       progressivo
into   :ldt_data_inizio_val_std,
       :ll_progressivo_std
from   listini_vendite
where  cod_azienda               = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 cod_prodotto              = :ls_cod_prodotto_listino and
		 cod_cat_mer               is null and
		 cod_categoria             is null and
		 cod_cliente               is null and
		 data_inizio_val           <= :ldt_data_inizio_val
having cod_azienda               = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 cod_prodotto              = :ls_cod_prodotto_listino and
		 cod_cat_mer               is null and
		 cod_categoria             is null and
		 cod_cliente               is null and
		 data_inizio_val           =  max(data_inizio_val);
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Prodotto standard non presente o non trovato!")
	return
end if

delete from listini_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
		 cod_valuta                = :ls_cod_valuta and
		 data_inizio_val           = :ldt_data_inizio_val and
		 progressivo               = :ll_progressivo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione condizioni correnti. Dettaglio " + sqlca.sqlerrtext)
	rollback;
	return
end if

declare cu_listino_produzione cursor for
  SELECT cod_versione,   
			prog_listino_produzione,   
			cod_prodotto_padre,   
			num_sequenza,   
			cod_prodotto_figlio,   
			cod_gruppo_variante,   
			progr,   
			cod_prodotto,   
			des_listino_produzione,   
			minimo_fatt_altezza,   
			minimo_fatt_larghezza,   
			minimo_fatt_profondita,   
			minimo_fatt_superficie,   
			minimo_fatt_volume,   
			flag_sconto_a_parte,   
			flag_tipo_scaglioni,   
			scaglione_1,   
			scaglione_2,   
			scaglione_3,   
			scaglione_4,   
			scaglione_5,   
			flag_sconto_mag_prezzo_1,   
			flag_sconto_mag_prezzo_2,   
			flag_sconto_mag_prezzo_3,   
			flag_sconto_mag_prezzo_4,   
			flag_sconto_mag_prezzo_5,   
			variazione_1,   
			variazione_2,   
			variazione_3,   
			variazione_4,   
			variazione_5,   
			flag_origine_prezzo_1,   
			flag_origine_prezzo_2,   
			flag_origine_prezzo_3,   
			flag_origine_prezzo_4,   
			flag_origine_prezzo_5  
	 FROM listini_produzione  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto ) AND  
			( cod_valuta = :ls_cod_valuta ) AND  
			( data_inizio_val = :ldt_data_inizio_val_std ) AND  
			( progressivo = :ll_progressivo_std )   ;
open cu_listino_produzione;

do while 1=1
	fetch cu_listino_produzione into :ls_cod_versione, :ll_prog_listino_produzione, :ls_cod_prodotto_padre, :ll_num_sequenza, :ls_cod_prodotto_figlio, &
	      :ls_cod_gruppo_variante, :ll_progr, :ls_cod_prodotto, :ls_des_listino_produzione, :ld_minimo_fatt_altezza, :ld_minimo_fatt_larghezza, :ld_minimo_fatt_profondita, &
			:ld_minimo_fatt_superficie, :ld_minimo_fatt_volume, :ls_flag_sconto_a_parte, :ls_flag_tipo_scaglioni, :ld_scaglione_1, :ld_scaglione_2, :ld_scaglione_3, &
			:ld_scaglione_4, :ld_scaglione_5, :ls_flag_sconto_mag_prezzo_1, :ls_flag_sconto_mag_prezzo_2, :ls_flag_sconto_mag_prezzo_3, :ls_flag_sconto_mag_prezzo_4, &
			:ls_flag_sconto_mag_prezzo_5, :ld_variazione_1, :ld_variazione_2, :ld_variazione_3, :ld_variazione_4, :ld_variazione_5, :ls_flag_origine_prezzo_1, &
			:ls_flag_origine_prezzo_2, :ls_flag_origine_prezzo_3, :ls_flag_origine_prezzo_4, :ls_flag_origine_prezzo_5;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in ricerca condizioni standard. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if

	INSERT INTO listini_produzione
		 (cod_azienda,   
		  cod_tipo_listino_prodotto,   
		  cod_valuta,   
		  data_inizio_val,   
		  progressivo,   
		  cod_prodotto_listino,   
		  cod_versione,   
		  prog_listino_produzione,   
		  cod_prodotto_padre,   
		  num_sequenza,   
		  cod_prodotto_figlio,   
		  cod_gruppo_variante,   
		  progr,   
		  cod_prodotto,   
		  des_listino_produzione,   
		  minimo_fatt_altezza,   
		  minimo_fatt_larghezza,   
		  minimo_fatt_profondita,   
		  minimo_fatt_superficie,   
		  minimo_fatt_volume,   
		  flag_sconto_a_parte,   
		  flag_tipo_scaglioni,   
		  scaglione_1,   
		  scaglione_2,   
		  scaglione_3,   
		  scaglione_4,   
		  scaglione_5,   
		  flag_sconto_mag_prezzo_1,   
		  flag_sconto_mag_prezzo_2,   
		  flag_sconto_mag_prezzo_3,   
		  flag_sconto_mag_prezzo_4,   
		  flag_sconto_mag_prezzo_5,   
		  variazione_1,   
		  variazione_2,   
		  variazione_3,   
		  variazione_4,   
		  variazione_5,   
		  flag_origine_prezzo_1,   
		  flag_origine_prezzo_2,   
		  flag_origine_prezzo_3,   
		  flag_origine_prezzo_4,   
		  flag_origine_prezzo_5 )  
	values(:s_cs_xx.cod_azienda,
			:ls_cod_tipo_listino_prodotto,
			:ls_cod_valuta,   
			:ldt_data_inizio_val,
			:ll_progressivo,
			:ls_cod_prodotto_listino,   
			:ls_cod_versione,   
			:ll_prog_listino_produzione,   
			:ls_cod_prodotto_padre,   
			:ll_num_sequenza,   
			:ls_cod_prodotto_figlio,   
			:ls_cod_gruppo_variante,   
			:ll_progr,   
			:ls_cod_prodotto,   
			:ls_des_listino_produzione,   
			:ld_minimo_fatt_altezza,   
			:ld_minimo_fatt_larghezza,   
			:ld_minimo_fatt_profondita,   
			:ld_minimo_fatt_superficie,   
			:ld_minimo_fatt_volume,   
			:ls_flag_sconto_a_parte,   
			:ls_flag_tipo_scaglioni,   
			:ld_scaglione_1,   
			:ld_scaglione_2,   
			:ld_scaglione_3,   
			:ld_scaglione_4,   
			:ld_scaglione_5,   
			:ls_flag_sconto_mag_prezzo_1,   
			:ls_flag_sconto_mag_prezzo_2,   
			:ls_flag_sconto_mag_prezzo_3,   
			:ls_flag_sconto_mag_prezzo_4,   
			:ls_flag_sconto_mag_prezzo_5,   
			:ld_variazione_1,   
			:ld_variazione_2,   
			:ld_variazione_3,   
			:ld_variazione_4,   
			:ld_variazione_5,   
			:ls_flag_origine_prezzo_1,   
			:ls_flag_origine_prezzo_2,   
			:ls_flag_origine_prezzo_3,   
			:ls_flag_origine_prezzo_4,   
			:ls_flag_origine_prezzo_5  );
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento condizioni da prodotto standard. Dettaglio " + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

close cu_listino_produzione;
g_mb.messagebox("APICE","Operazione eseguita con successo!")
commit;

end event

type dw_listini_produzione_det_1 from uo_cs_xx_dw within w_listino_distinta_tree
integer x = 901
integer y = 540
integer width = 3131
integer height = 1360
integer taborder = 40
string dataobject = "d_listini_produzione_tree_det"
borderstyle borderstyle = styleraised!
end type

event rowfocuschanged;call super::rowfocuschanged;dw_listini_produzione_lista.scrolltorow( currentrow)
end event

