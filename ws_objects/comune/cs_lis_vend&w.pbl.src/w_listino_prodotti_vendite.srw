﻿$PBExportHeader$w_listino_prodotti_vendite.srw
$PBExportComments$Finestra Listino prodotti
forward
global type w_listino_prodotti_vendite from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_listino_prodotti_vendite
end type
type cb_ricerca from commandbutton within w_listino_prodotti_vendite
end type
type cb_dim_1 from commandbutton within w_listino_prodotti_vendite
end type
type ole_grid from olecustomcontrol within w_listino_prodotti_vendite
end type
type cb_memorizza from commandbutton within w_listino_prodotti_vendite
end type
type dw_listini_vendite_det_1 from uo_cs_xx_dw within w_listino_prodotti_vendite
end type
type dw_folder from u_folder within w_listino_prodotti_vendite
end type
type dw_ricerca from u_dw_search within w_listino_prodotti_vendite
end type
type dw_folder_search from u_folder within w_listino_prodotti_vendite
end type
type dw_listini_vendite_lista from uo_cs_xx_dw within w_listino_prodotti_vendite
end type
type dw_listini_vendite_det_2 from uo_cs_xx_dw within w_listino_prodotti_vendite
end type
end forward

global type w_listino_prodotti_vendite from w_cs_xx_principale
integer width = 2715
integer height = 1964
string title = "Listini Vendite"
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_dim_1 cb_dim_1
ole_grid ole_grid
cb_memorizza cb_memorizza
dw_listini_vendite_det_1 dw_listini_vendite_det_1
dw_folder dw_folder
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_listini_vendite_lista dw_listini_vendite_lista
dw_listini_vendite_det_2 dw_listini_vendite_det_2
end type
global w_listino_prodotti_vendite w_listino_prodotti_vendite

type variables
boolean ib_new=false, ib_modify=false
long il_dim_x[], il_dim_y[], il_x, il_y, il_num_scaglione
end variables

forward prototypes
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_abilita_colonne ()
public subroutine wf_disabilita_colonne ()
public function integer wf_listino_dimensioni ()
public subroutine wf_carica_dimensioni ()
public function string wf_componi_valore (string fs_flag_sconto_mag_prezzo, double ld_valore)
public function integer wf_scomponi_valore (string fs_stringa, ref string fs_flag_sconto_mag_prezzo, ref double fd_valore)
public subroutine wf_termine_scaglione ()
end prototypes

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_abilita_colonne ();if dw_listini_vendite_det_1.getrow() > 0 and ( ib_new or ib_modify) then

//		dw_listini_vendite_det_1.object.cod_cat_mer.border = 5
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.mode = 0
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
//		dw_listini_vendite_det_1.object.cod_cat_mer.tabsequence = 50

		dw_listini_vendite_det_1.object.cod_prodotto.border = 5
		dw_listini_vendite_det_1.object.cod_prodotto.background.mode = 0
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_listini_vendite_det_1.object.cod_prodotto.tabsequence = 60

//		dw_listini_vendite_det_1.object.cod_cliente.border = 5
//		dw_listini_vendite_det_1.object.cod_cliente.background.mode = 0
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
//		dw_listini_vendite_det_1.object.cod_cliente.tabsequence = 70
//
//		dw_listini_vendite_det_1.object.cod_categoria.border = 5
//		dw_listini_vendite_det_1.object.cod_categoria.background.mode = 0
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.color = rgb(255,255,255)
//		dw_listini_vendite_det_1.object.cod_categoria.tabsequence = 80

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public subroutine wf_disabilita_colonne ();if dw_listini_vendite_det_1.getrow() > 0 and ( ib_new or ib_modify) then

//		dw_listini_vendite_det_1.object.cod_cat_mer.border = 6
//		dw_listini_vendite_det_1.object.cod_cat_mer.background.mode = 1
//		dw_listini_vendite_det_1.object.cod_cat_mer.tabsequence = 0

		dw_listini_vendite_det_1.object.cod_prodotto.border = 6
		dw_listini_vendite_det_1.object.cod_prodotto.background.mode = 1
		dw_listini_vendite_det_1.object.cod_prodotto.tabsequence = 0

//		dw_listini_vendite_det_1.object.cod_cliente.border = 6
//		dw_listini_vendite_det_1.object.cod_cliente.background.mode = 1
//		dw_listini_vendite_det_1.object.cod_cliente.tabsequence = 0
//
//		dw_listini_vendite_det_1.object.cod_categoria.border = 6
//		dw_listini_vendite_det_1.object.cod_categoria.background.mode = 1
//		dw_listini_vendite_det_1.object.cod_categoria.tabsequence = 0

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public function integer wf_listino_dimensioni ();string ls_flag_tipo_dimensione_1, ls_flag_tipo_dimensione_2, ls_cod_prodotto, ls_sql, &
       ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_limite_dimensione_1[], ll_limite_dimensione_2[], ll_i, ll_limite, ll_x, ll_y, &
     ll_progressivo, ll_num_scaglione
datetime ldt_data_inizio_val
						


ls_cod_prodotto = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_prodotto")
select flag_tipo_dimensione_1,
       flag_tipo_dimensione_2
into  :ls_flag_tipo_dimensione_1,
		:ls_flag_tipo_dimensione_2
from  tab_prodotti_dimensioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Non sono state impostate le dimensioni in tabella prodotti dimensioni",stopsign!)
	return -1
end if


// ---------------------------------ricerco limiti della prima dimensione ------------------------------------
declare cu_limiti dynamic cursor for sqlsa;
ls_sql = "SELECT LIMITE_DIMENSIONE FROM TAB_PRODOTTI_DIMENSIONI_DET WHERE COD_AZIENDA = '" + s_cs_xx.cod_azienda + &
         "' AND COD_PRODOTTO = '" + ls_cod_prodotto + &
			"' AND FLAG_TIPO_DIMENSIONE = '" + ls_flag_tipo_dimensione_1 + "'"
prepare sqlsa from :ls_sql;
open dynamic cu_limiti;

ll_i = 1
do while 1=1
   fetch cu_limiti into :ll_limite;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_limite_dimensione_1[ll_i] = ll_limite 
	ll_i = ll_i + 1
loop
close cu_limiti;
// ---------------------------------ricerco limiti della seconda dimensione ----------------------------------
//declare cu_limiti1 dynamic cursor for sqlsa;
ls_sql = "SELECT LIMITE_DIMENSIONE FROM TAB_PRODOTTI_DIMENSIONI_DET WHERE COD_AZIENDA = '" + s_cs_xx.cod_azienda + &
         "' AND COD_PRODOTTO = '" + ls_cod_prodotto + &
			"' AND FLAG_TIPO_DIMENSIONE = '" + ls_flag_tipo_dimensione_2 + "'"
prepare sqlsa from :ls_sql;
open dynamic cu_limiti;

ll_i = 1
do while 1=1
   fetch cu_limiti into :ll_limite;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_limite_dimensione_2[ll_i] = ll_limite 
	ll_i = ll_i + 1
loop
close cu_limiti;

for ll_x = 1 to upperbound(ll_limite_dimensione_1)
	for ll_y = 1 to upperbound(ll_limite_dimensione_2)
		insert into listini_vendite_dimensioni
		          ( cod_azienda,
		            cod_tipo_listino_prodotto,
     		         cod_valuta,
		            data_inizio_val,
		            progressivo,
		            num_scaglione,
		            limite_dimensione_1,
		            limite_dimensione_2,
						flag_sconto_mag_prezzo,
						variazione)
		values	(  :s_cs_xx.cod_azienda,
						:ls_cod_tipo_listino_prodotto,
						:ls_cod_valuta,
						:ldt_data_inizio_val,
						:ll_progressivo,
						:ll_num_scaglione,
						:ll_limite_dimensione_1[ll_x],
						:ll_limite_dimensione_2[ll_y],
						'S',
						0 );
	next
next
return 0



end function

public subroutine wf_carica_dimensioni ();string ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta, &
       ls_sql, ls_flag_sconto_mag_prezzo, ls_str, ls_return
integer ll_1, ll_2
long ll_dim_1[], ll_dim_2[], ll_y, ll_x, ll_i, ll_limite_dim_1, ll_limite_dim_2, ldec_riga, ldec_colonna, &
     ll_progressivo
double ld_variazione
datetime ldt_data_inizio_val

ls_cod_prodotto = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_prodotto")
ls_cod_tipo_listino_prodotto = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_valuta")
ll_progressivo = dw_listini_vendite_lista.getitemnumber(dw_listini_vendite_lista.getrow(),"progressivo")
ldt_data_inizio_val = dw_listini_vendite_lista.getitemdatetime(dw_listini_vendite_lista.getrow(),"data_inizio_val")

f_crea_listino_dimensioni(ls_cod_prodotto, &
                          ls_cod_tipo_listino_prodotto, &
                          ls_cod_valuta, &
                          ldt_data_inizio_val, &
                          ll_progressivo, &
								  il_num_scaglione, &
								  ll_dim_1[], &
								  ll_dim_2[])

ll_x = upperbound(ll_dim_1)
ll_y = upperbound(ll_dim_2)

ole_grid.object.rows = ll_y + 1
ole_grid.object.cols = ll_x + 1

for ll_1 = 1 to ll_x
	ole_grid.object.setcolname(ll_1, string(ll_dim_1[ll_1]))
	il_dim_x[ll_1] = ll_dim_1[ll_1]
next

for ll_2 = 1 to ll_y
  	ole_grid.object.setrowname(ll_2, string(ll_dim_2[ll_2]))
	il_dim_y[ll_2] = ll_dim_2[ll_2]
next

il_x = ll_x
il_y = ll_y

// ----------------------------------  scrivo foglio calcolo -------------------------------------------------

declare cu_dim dynamic cursor for sqlsa;
ls_sql = " SELECT limite_dimensione_1, limite_dimensione_2, flag_sconto_mag_prezzo, variazione " + &
         " FROM listini_vendite_dimensioni " + &
         " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
               " cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino_prodotto + "' AND " + &
               " cod_valuta = '" + ls_cod_valuta + "' AND " + &
               " data_inizio_val = "+ string(ldt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + " AND " + &
               " progressivo = " + string(ll_progressivo) + " AND " + &
               " num_scaglione = " + string(il_num_scaglione) + " " + &
			" ORDER BY limite_dimensione_1 ASC , limite_dimensione_2 ASC "

prepare sqlsa from :ls_sql;

open dynamic cu_dim;

ll_x = 0
ll_y = 0
ll_i = 0
do while 1=1
   fetch cu_dim into :ll_limite_dim_1, :ll_limite_dim_2, :ls_flag_sconto_mag_prezzo, :ld_variazione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if ll_i <> ll_limite_dim_1 then
		ll_y = ll_y + 1
		ll_i = ll_limite_dim_1
		ll_x = 0
	end if
	ll_x = ll_x + 1
	ldec_riga = ll_x
	ldec_colonna = ll_y
	ls_str = wf_componi_valore(ls_flag_sconto_mag_prezzo, ld_variazione)
	ole_grid.object.putcell(ls_str,ldec_riga,ldec_colonna)
loop
close cu_dim;

end subroutine

public function string wf_componi_valore (string fs_flag_sconto_mag_prezzo, double ld_valore);string ls_return

choose case fs_flag_sconto_mag_prezzo
	case "S"  // sconto
		ls_return = string(ld_valore)
		ls_return = "-" + ls_return + "%"
	case "M"
		ls_return = string(ld_valore)
		ls_return = ls_return + "%"
	case "A"
		ls_return = string(ld_valore)
	case "D"
		ls_return = "-" + string(ld_valore)
	case "P"
		ls_return = "L." + string(ld_valore)
	case else
		ls_return = "errore"
end choose

return ls_return
		
end function

public function integer wf_scomponi_valore (string fs_stringa, ref string fs_flag_sconto_mag_prezzo, ref double fd_valore);string ls_str

if left(fs_stringa,1) = "-" and right(fs_stringa,1) = "%" then
	ls_str = mid(fs_stringa,2,len(fs_stringa)-2)
	fs_flag_sconto_mag_prezzo = "S"
	fd_valore = double(ls_str)
	return 0
end if

if right(fs_stringa,1) = "%" then
	ls_str = left(fs_stringa,len(fs_stringa) -1)
	fs_flag_sconto_mag_prezzo = "M"
	fd_valore = double(ls_str)
	return 0
end if

if left(fs_stringa,2) = "L." then
	ls_str = mid(fs_stringa,3)
	fs_flag_sconto_mag_prezzo = "P"
	fd_valore = double(ls_str)
	return 0
end if

if right(fs_stringa,1) = "-" then
	ls_str = mid(fs_stringa,2)
	fs_flag_sconto_mag_prezzo = "D"
	fd_valore = double(ls_str)
	return 0
end if

ls_str = fs_stringa
fs_flag_sconto_mag_prezzo = "A"
fd_valore = double(ls_str)

return 0

end function

public subroutine wf_termine_scaglione ();string ls_tipo_scaglione
double ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5


ls_tipo_scaglione = dw_listini_vendite_det_1.getitemstring(dw_listini_vendite_det_1.getrow(),"flag_tipo_scaglioni")
ld_variazione_1 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_1")
ld_variazione_2 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_2")
ld_variazione_3 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_3")
ld_variazione_4 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_4")
ld_variazione_5 = dw_listini_vendite_det_1.getitemnumber(dw_listini_vendite_det_1.getrow(),"variazione_5")
if ld_variazione_1 = 0 or isnull(ld_variazione_1) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_2 = 0 or isnull(ld_variazione_2) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_3 = 0 or isnull(ld_variazione_3) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_2", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_2", 999999999)
	end if
elseif ld_variazione_4 = 0 or isnull(ld_variazione_4) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_3", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_3", 999999999)
	end if
elseif ld_variazione_5 = 0 or isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_4", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_4", 999999999)
	end if
end if

if ld_variazione_5 <> 0 and not isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_5", 99999999)
	else
		dw_listini_vendite_det_1.setitem(dw_listini_vendite_det_1.getrow(),"scaglione_5", 999999999)
	end if
end if

end subroutine

event pc_setwindow;call super::pc_setwindow;datetime ldt_data_chiusura_annuale_prec, ldt_data_chiusura_annuale, &
       ldt_data_chiusura_periodica
string ls_data_chiusura_annuale_prec, ls_data_chiusura_annuale, &
       ls_data_chiusura_periodica, ls_cod_livello_prod_1, ls_cod_livello_prod_2, &
   	 ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, &
       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[], l_objects[ ]


lw_oggetti[1] = ole_grid
lw_oggetti[2] = cb_memorizza
dw_folder.fu_assigntab(3, "Dimensioni", lw_oggetti[])
lw_oggetti[1] = dw_listini_vendite_det_2
lw_oggetti[2] = cb_dim_1
dw_folder.fu_assigntab(2, "Scaglioni", lw_oggetti[])
lw_oggetti[1] = dw_listini_vendite_det_1
//lw_oggetti[2] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
dw_folder.fu_foldercreate(3,4)
dw_folder.fu_selecttab(1)
dw_folder.fu_hidetab(3)

dw_listini_vendite_lista.set_dw_key("cod_azienda")
dw_listini_vendite_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen, &
                                 c_default)

dw_listini_vendite_det_1.set_dw_options(sqlca, &
                                dw_listini_vendite_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

dw_listini_vendite_det_2.set_dw_options(sqlca, &
                                dw_listini_vendite_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)


iuo_dw_main=dw_listini_vendite_lista


l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "rs_cod_tipo_listino"
l_criteriacolumn[3] = "rs_cod_valuta"
//l_criteriacolumn[4] = "rs_cod_cat_mer"
//l_criteriacolumn[5] = "rs_cod_prodotto"
//l_criteriacolumn[6] = "rs_cod_categoria"
//l_criteriacolumn[7] = "rs_cod_cliente"

l_searchtable[1] = "listini_vendite"
l_searchtable[2] = "listini_vendite"
l_searchtable[3] = "listini_vendite"
//l_searchtable[4] = "listini_vendite"
//l_searchtable[5] = "listini_vendite"
//l_searchtable[6] = "listini_vendite"
//l_searchtable[7] = "listini_vendite"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_tipo_listino_prodotto"
l_searchcolumn[3] = "cod_valuta"
//l_searchcolumn[4] = "cod_cat_mer"
//l_searchcolumn[5] = "cod_prodotto"
//l_searchcolumn[6] = "cod_categoria"
//l_searchcolumn[7] = "cod_cliente"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_listini_vendite_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
l_objects[1] = dw_listini_vendite_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto_1
//l_objects[5] = cb_ricerca_cliente_1
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_listini_vendite_lista.change_dw_current()
//cb_ricerca_prodotto.enabled = false

end event

on w_listino_prodotti_vendite.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_dim_1=create cb_dim_1
this.ole_grid=create ole_grid
this.cb_memorizza=create cb_memorizza
this.dw_listini_vendite_det_1=create dw_listini_vendite_det_1
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_listini_vendite_lista=create dw_listini_vendite_lista
this.dw_listini_vendite_det_2=create dw_listini_vendite_det_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.cb_dim_1
this.Control[iCurrent+4]=this.ole_grid
this.Control[iCurrent+5]=this.cb_memorizza
this.Control[iCurrent+6]=this.dw_listini_vendite_det_1
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_ricerca
this.Control[iCurrent+9]=this.dw_folder_search
this.Control[iCurrent+10]=this.dw_listini_vendite_lista
this.Control[iCurrent+11]=this.dw_listini_vendite_det_2
end on

on w_listino_prodotti_vendite.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_dim_1)
destroy(this.ole_grid)
destroy(this.cb_memorizza)
destroy(this.dw_listini_vendite_det_1)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_listini_vendite_lista)
destroy(this.dw_listini_vendite_det_2)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))")

//f_po_loaddddw_dw(dw_listini_vendite_det_1, &
//                 "cod_cat_mer", &
//                 sqlca, &
//                 "tab_cat_mer", &
//                 "cod_cat_mer", &
//                 "des_cat_mer", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_vendite_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//f_po_loaddddw_dw(dw_listini_vendite_det_1, &
//                 "cod_categoria", &
//                 sqlca, &
//                 "tab_categorie", &
//                 "cod_categoria", &
//                 "des_categoria", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_vendite_det_1, &
					  "cod_prodotto", &
					  sqlca, &
                 "anag_prodotti", &
					  "cod_prodotto", &
					  "des_prodotto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//f_po_loaddddw_dw(dw_listini_vendite_det_1, &
//                 "cod_cliente", &
//                 sqlca, &
//                 "anag_clienti", &
//                 "cod_cliente", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


dw_ricerca.fu_loadcode("rs_cod_tipo_listino", &
                       "tab_tipi_listini_prodotti", &
                       "cod_tipo_listino_prodotto", &
                       "des_tipo_listino_prodotto", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_tipo_listino_prodotto asc", "(Tutti)" )

dw_ricerca.fu_loadcode("rs_cod_prodotto", &
                       "anag_prodotti", &
		                 "cod_prodotto", &
					        "des_prodotto", &
					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_cat_mer", &
//                       "tab_cat_mer", &
//					        "cod_cat_mer", &
//					        "des_cat_mer", &
//					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cat_mer asc", "(Tutti)" )
					  
dw_ricerca.fu_loadcode("rs_cod_valuta", &
                       "tab_valute", &
							  "cod_valuta", &
							  "des_valuta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_valuta asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_cliente", &
//                        "anag_clienti", &
//								"cod_cliente", &
//								"rag_soc_1",&
//								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cliente asc", "(Tutti)" )
//
//dw_ricerca.fu_loadcode("rs_cod_categoria", &
//                        "tab_categorie", &
//								"cod_categoria", &
//								"des_categoria", &
//								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_categoria asc", "(Tutti)" )
//
					  
					  
					  
					  
					  
					  
end event

type cb_reset from commandbutton within w_listino_prodotti_vendite
integer x = 2240
integer y = 160
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_listini_vendite_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_listino_prodotti_vendite
integer x = 2240
integer y = 60
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_dim_1 from commandbutton within w_listino_prodotti_vendite
integer x = 2514
integer y = 860
integer width = 69
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;//s_cs_xx.parametri.parametro_s_1 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_prodotto")
//s_cs_xx.parametri.parametro_s_2 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_tipo_listino_prodotto")
//s_cs_xx.parametri.parametro_s_3 = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_valuta")
//s_cs_xx.parametri.parametro_data_1 = dw_listini_vendite_lista.getitemdatetime(dw_listini_vendite_lista.getrow(),"data_inizio_val")
//s_cs_xx.parametri.parametro_d_1 = dw_listini_vendite_lista.getitemnumber(dw_listini_vendite_lista.getrow(),"progressivo")
//s_cs_xx.parametri.parametro_d_2 = 1
//
//window_open_parm(w_listini_vendite_dimensioni_save, 0, dw_listini_vendite_lista)
//
il_num_scaglione = 1
dw_folder.fu_showtab(3)
dw_folder.fu_selecttab(3)
wf_carica_dimensioni()



end event

type ole_grid from olecustomcontrol within w_listino_prodotti_vendite
integer x = 69
integer y = 740
integer taborder = 120
string binarykey = "w_listino_prodotti_vendite.win"
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event losefocus;//string ls_stringa, ls_flag_sconto_mag_prezzo, ls_cod_tipo_listino_prodotto, ls_cod_valuta
//long   ll_return, ll_x, ll_y, ll_valore_dim_x, ll_valore_dim_y, ll_progressivo
//double ld_valore
//datetime ldt_data_inizio_val
//
//ls_cod_tipo_listino_prodotto = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_tipo_listino_prodotto")
//ls_cod_valuta = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_valuta")
//ll_progressivo = dw_listini_vendite_lista.getitemnumber(dw_listini_vendite_lista.getrow(),"progressivo")
//ldt_data_inizio_val = dw_listini_vendite_lista.getitemdatetime(dw_listini_vendite_lista.getrow(),"data_inizio_val")
//
//for ll_x = 1 to il_x
//	for ll_y = 1 to il_y
//		ls_stringa = ole_grid.object.getcell(ll_y, ll_x)
//		ll_return = wf_scomponi_valore(ls_stringa, ls_flag_sconto_mag_prezzo, ld_valore)
//		ll_valore_dim_x = il_dim_x[ll_x]
//		ll_valore_dim_y = il_dim_y[ll_y]
//		messagebox(ls_flag_sconto_mag_prezzo, ld_valore)
//		update listini_vendite_dimensioni
//		set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
//		       variazione  = :ld_valore
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
//				 cod_valuta      = :ls_cod_valuta and
//				 data_inizio_val = :ldt_data_inizio_val and
//				 progressivo     = :ll_progressivo and
//				 num_scaglione   = :il_num_scaglione and
//				 limite_dimensione_1 = :il_dim_x[ll_x] and
//				 limite_dimensione_2 = :il_dim_y[ll_y] ;
//		commit;
//	next
//next
//
//
end event

type cb_memorizza from commandbutton within w_listino_prodotti_vendite
integer x = 2240
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Memorizza"
end type

event clicked;string ls_stringa, ls_flag_sconto_mag_prezzo, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long   ll_return, ll_x, ll_y, ll_valore_dim_x, ll_valore_dim_y, ll_progressivo
double ld_valore
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_tipo_listino_prodotto")
ls_cod_valuta = dw_listini_vendite_lista.getitemstring(dw_listini_vendite_lista.getrow(),"cod_valuta")
ll_progressivo = dw_listini_vendite_lista.getitemnumber(dw_listini_vendite_lista.getrow(),"progressivo")
ldt_data_inizio_val = dw_listini_vendite_lista.getitemdatetime(dw_listini_vendite_lista.getrow(),"data_inizio_val")

for ll_x = 1 to il_x
	for ll_y = 1 to il_y
		ls_stringa = ole_grid.object.getcell(ll_y, ll_x)
		ll_return = wf_scomponi_valore(ls_stringa, ls_flag_sconto_mag_prezzo, ld_valore)
		ll_valore_dim_x = il_dim_x[ll_x]
		ll_valore_dim_y = il_dim_y[ll_y]
		update listini_vendite_dimensioni
		set    flag_sconto_mag_prezzo = :ls_flag_sconto_mag_prezzo, 
		       variazione  = :ld_valore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				 cod_valuta      = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val and
				 progressivo     = :ll_progressivo and
				 num_scaglione   = :il_num_scaglione and
				 limite_dimensione_1 = :il_dim_x[ll_x] and
				 limite_dimensione_2 = :il_dim_y[ll_y] ;
		commit;
	next
next

ole_grid.object.clear
dw_folder.fu_selecttab(2)

dw_folder.fu_hidetab(3)

end event

type dw_listini_vendite_det_1 from uo_cs_xx_dw within w_listino_prodotti_vendite
integer x = 69
integer y = 720
integer width = 2537
integer height = 1080
integer taborder = 130
string dataobject = "d_listini_prodotti_vendite_1"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_vendite_det_1,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_listino_prodotti_vendite
integer x = 23
integer y = 600
integer width = 2629
integer height = 1240
integer taborder = 50
end type

event po_tabclicked;call super::po_tabclicked;//choose case i_selectedtabname
//	case "Dimensioni"
//		if il_num_scaglione < 0 or il_num_scaglione > 5 then
//			messagebox("Listini","Selezionare prima uno scaglione di quantità")
//			dw_folder_search.fu_selectTab(2)
//		end if
//		wf_carica_dimensioni()
//end choose
end event

type dw_ricerca from u_dw_search within w_listino_prodotti_vendite
integer x = 137
integer y = 60
integer width = 2126
integer height = 280
integer taborder = 30
string dataobject = "d_listini_prodotti_vendite_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type dw_folder_search from u_folder within w_listino_prodotti_vendite
integer x = 23
integer y = 20
integer width = 2629
integer height = 560
integer taborder = 40
end type

type dw_listini_vendite_lista from uo_cs_xx_dw within w_listino_prodotti_vendite
integer x = 137
integer y = 40
integer width = 2309
integer height = 500
integer taborder = 100
string dataobject = "d_listini_prodotti_vendite_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT


end event

event pcd_new;call super::pcd_new;string ls_cod_valuta
datetime ldt_oggi


if i_extendmode then
	ldt_oggi = datetime(today())
	dw_listini_vendite_lista.setitem(dw_listini_vendite_lista.getrow(),"data_inizio_val", ldt_oggi)
end if

setnull(ls_cod_valuta)
select stringa
into   :ls_cod_valuta
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';

dw_listini_vendite_det_1.setitem(this.getrow(),"cod_valuta", ls_cod_valuta)

//cb_ricerca_prodotto.enabled = true
ib_new = true
end event

event updatestart;call super::updatestart;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_tipo_scaglione
datetime ldt_data_inizio_val
long  ll_max_progressivo
double ld_scaglione

wf_termine_scaglione()

setnull(ll_max_progressivo)
ll_max_progressivo = getitemnumber(getrow(),"progressivo")
if isnull(ll_max_progressivo) or ll_max_progressivo = 0 then
	setnull(ll_max_progressivo)
	ls_cod_tipo_listino_prodotto = getitemstring(getrow(),"cod_tipo_listino_prodotto")
	ls_cod_valuta = getitemstring(getrow(),"cod_valuta")
	ldt_data_inizio_val = getitemdatetime(getrow(),"data_inizio_val")
	
	select max(progressivo)
	into  :ll_max_progressivo
	from  listini_vendite
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			cod_valuta = :ls_cod_valuta and
			data_inizio_val = :ldt_data_inizio_val;
	if isnull(ll_max_progressivo) then
		ll_max_progressivo = 1
	else
		ll_max_progressivo ++
	end if
	setitem(getrow(),"progressivo",ll_max_progressivo)
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if isnull(getitemstring(i_rownbr,"cod_tipo_listino_prodotto")) then
	g_mb.messagebox("APICE","Tipo listino obbligatorio")
	pcca.error = c_fatal
end if

if isnull(getitemstring(i_rownbr,"cod_cliente")) and &
	isnull(getitemstring(i_rownbr,"cod_categoria")) and &
	isnull(getitemstring(i_rownbr,"cod_cat_mer")) and &
	isnull(getitemstring(i_rownbr,"cod_prodotto")) then
	g_mb.messagebox("APICE","E' obbligatorio inserire almeno un cliente, prodotto o una categoria di cliente o prodotto")
	pcca.error = c_fatal
end if

end event

event rowfocuschanged;call super::rowfocuschanged;//if this.getrow() > 0 then
//	dw_listini_vendite_det_2.object.variazione_1.editmask.mask = wf_formato(getitemstring(getrow(),"flag_sconto_mag_prezzo_1"))
//	
//	dw_listini_vendite_det_2.object.variazione_2.editmask.mask = wf_formato(getitemstring(getrow(),"flag_sconto_mag_prezzo_2"))
//	
//	dw_listini_vendite_det_2.object.variazione_3.editmask.mask = wf_formato(getitemstring(getrow(),"flag_sconto_mag_prezzo_3"))
//	
//	dw_listini_vendite_det_2.object.variazione_4.editmask.mask = wf_formato(getitemstring(getrow(),"flag_sconto_mag_prezzo_4"))
//	
//	dw_listini_vendite_det_2.object.variazione_5.editmask.mask = wf_formato(getitemstring(getrow(),"flag_sconto_mag_prezzo_5"))
//
////	wf_abilita_colonne()
//end if
end event

event pcd_modify;call super::pcd_modify;//cb_ricerca_prodotto.enabled = false
ib_modify = true

end event

event pcd_view;call super::pcd_view;//cb_ricerca_prodotto.enabled = false
ib_new = false
ib_modify = false
end event

event pcd_refresh;call super::pcd_refresh;long riga

riga = getrow()

if i_refreshmode = c_refreshmodify or i_refreshmode = c_refreshRFC then 
	if getitemstatus(getrow(),0,primary!) = newmodified! then
		wf_abilita_colonne()
	else
		wf_disabilita_colonne()
	end if
end if
if i_refreshmode = c_refreshnew then wf_abilita_colonne()
end event

type dw_listini_vendite_det_2 from uo_cs_xx_dw within w_listino_prodotti_vendite
integer x = 46
integer y = 740
integer width = 2514
integer height = 676
integer taborder = 60
string dataobject = "d_listini_vendite_det_2"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "flag_sconto_mag_prezzo_1"
			dw_listini_vendite_det_2.object.variazione_1.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_1", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_1")
		case "flag_sconto_mag_prezzo_2"
			dw_listini_vendite_det_2.object.variazione_2.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_2", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_2")
		case "flag_sconto_mag_prezzo_3"
			dw_listini_vendite_det_2.object.variazione_3.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_3", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_3")
		case "flag_sconto_mag_prezzo_4"
			dw_listini_vendite_det_2.object.variazione_4.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_4", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_4")
		case "flag_sconto_mag_prezzo_5"
			dw_listini_vendite_det_2.object.variazione_5.editmask.mask = wf_formato(i_coltext)
			setitem(getrow(),"variazione_5", 0)
			dw_listini_vendite_det_2.setcolumn("variazione_5")
		case "flag_origine_prezzo_1"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_1") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_2"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_2") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_3"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_3") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_4"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_4") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
		case "flag_origine_prezzo_5"
			if i_coltext = "L" and getitemstring(getrow(),"flag_sconto_mag_prezzo_5") = "P" then g_mb.messagebox("Lisiti Apice","Errore: è assurdo assegnare prezzo fisso con origine Listini")
	end choose
end if

end event

event clicked;call super::clicked;if dwo.name = "scaglione_1" then
	il_num_scaglione = 1
end if
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
01w_listino_prodotti_vendite.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
11w_listino_prodotti_vendite.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
