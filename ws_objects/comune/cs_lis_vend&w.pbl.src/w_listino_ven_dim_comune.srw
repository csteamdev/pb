﻿$PBExportHeader$w_listino_ven_dim_comune.srw
forward
global type w_listino_ven_dim_comune from w_listini_vendite_dimensioni
end type
end forward

global type w_listino_ven_dim_comune from w_listini_vendite_dimensioni
end type
global w_listino_ven_dim_comune w_listino_ven_dim_comune

type variables
string is_flag_tipo_vista = "C"
end variables

on w_listino_ven_dim_comune.create
call super::create
end on

on w_listino_ven_dim_comune.destroy
call super::destroy
end on

type cb_stampa from w_listini_vendite_dimensioni`cb_stampa within w_listino_ven_dim_comune
end type

type cb_3 from w_listini_vendite_dimensioni`cb_3 within w_listino_ven_dim_comune
end type

type cb_2 from w_listini_vendite_dimensioni`cb_2 within w_listino_ven_dim_comune
end type

type cb_1 from w_listini_vendite_dimensioni`cb_1 within w_listino_ven_dim_comune
end type

