﻿$PBExportHeader$w_listino_vendite_comune.srw
forward
global type w_listino_vendite_comune from w_listini_vendite
end type
end forward

global type w_listino_vendite_comune from w_listini_vendite
integer x = 673
integer y = 265
end type
global w_listino_vendite_comune w_listino_vendite_comune

type variables
STRING is_flag_tipo_vista = "C"
end variables

on w_listino_vendite_comune.create
call super::create
end on

on w_listino_vendite_comune.destroy
call super::destroy
end on

type st_1 from w_listini_vendite`st_1 within w_listino_vendite_comune
end type

type cb_aggiorna from w_listini_vendite`cb_aggiorna within w_listino_vendite_comune
end type

type cb_reset from w_listini_vendite`cb_reset within w_listino_vendite_comune
end type

type cb_ricerca from w_listini_vendite`cb_ricerca within w_listino_vendite_comune
end type

type cb_listini_produzione from w_listini_vendite`cb_listini_produzione within w_listino_vendite_comune
end type

type cb_pers_cli from w_listini_vendite`cb_pers_cli within w_listino_vendite_comune
end type

type r_1 from w_listini_vendite`r_1 within w_listino_vendite_comune
end type

type dw_listini_vendite_lista from w_listini_vendite`dw_listini_vendite_lista within w_listino_vendite_comune
end type

type dw_folder_search from w_listini_vendite`dw_folder_search within w_listino_vendite_comune
end type

type dw_listini_vendite_det_1 from w_listini_vendite`dw_listini_vendite_det_1 within w_listino_vendite_comune
end type

type dw_listini_vendite_det_2 from w_listini_vendite`dw_listini_vendite_det_2 within w_listino_vendite_comune
end type

type dw_ricerca from w_listini_vendite`dw_ricerca within w_listino_vendite_comune
end type

