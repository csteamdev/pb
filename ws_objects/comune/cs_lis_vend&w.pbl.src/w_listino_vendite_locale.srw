﻿$PBExportHeader$w_listino_vendite_locale.srw
forward
global type w_listino_vendite_locale from w_listini_vendite
end type
end forward

global type w_listino_vendite_locale from w_listini_vendite
string is_flag_tipo_vista = "L"
end type
global w_listino_vendite_locale w_listino_vendite_locale

type variables
string is_flag_tipo_vista="C"
end variables

on w_listino_vendite_locale.create
call super::create
end on

on w_listino_vendite_locale.destroy
call super::destroy
end on

type st_1 from w_listini_vendite`st_1 within w_listino_vendite_locale
end type

type cb_aggiorna from w_listini_vendite`cb_aggiorna within w_listino_vendite_locale
end type

type cb_reset from w_listini_vendite`cb_reset within w_listino_vendite_locale
end type

type cb_ricerca from w_listini_vendite`cb_ricerca within w_listino_vendite_locale
end type

type cb_listini_produzione from w_listini_vendite`cb_listini_produzione within w_listino_vendite_locale
end type

type cb_pers_cli from w_listini_vendite`cb_pers_cli within w_listino_vendite_locale
end type

type r_1 from w_listini_vendite`r_1 within w_listino_vendite_locale
end type

type dw_listini_vendite_lista from w_listini_vendite`dw_listini_vendite_lista within w_listino_vendite_locale
end type

type dw_folder_search from w_listini_vendite`dw_folder_search within w_listino_vendite_locale
end type

type dw_listini_vendite_det_1 from w_listini_vendite`dw_listini_vendite_det_1 within w_listino_vendite_locale
end type

type dw_listini_vendite_det_2 from w_listini_vendite`dw_listini_vendite_det_2 within w_listino_vendite_locale
end type

type dw_ricerca from w_listini_vendite`dw_ricerca within w_listino_vendite_locale
end type

