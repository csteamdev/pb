﻿$PBExportHeader$w_listino_vettori.srw
$PBExportComments$Finestra Listino Vettori
forward
global type w_listino_vettori from w_cs_xx_principale
end type
type dw_listino_vettori_lista from uo_cs_xx_dw within w_listino_vettori
end type
type dw_listino_vettori_det from uo_cs_xx_dw within w_listino_vettori
end type
end forward

global type w_listino_vettori from w_cs_xx_principale
int Width=2844
int Height=1997
boolean TitleBar=true
string Title="Listino Vettori"
dw_listino_vettori_lista dw_listino_vettori_lista
dw_listino_vettori_det dw_listino_vettori_det
end type
global w_listino_vettori w_listino_vettori

on w_listino_vettori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_listino_vettori_lista=create dw_listino_vettori_lista
this.dw_listino_vettori_det=create dw_listino_vettori_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_listino_vettori_lista
this.Control[iCurrent+2]=dw_listino_vettori_det
end on

on w_listino_vettori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_listino_vettori_lista)
destroy(this.dw_listino_vettori_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_listino_vettori_lista.set_dw_key("cod_azienda")
dw_listino_vettori_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_listino_vettori_det.set_dw_options(sqlca, &
                             dw_listino_vettori_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)

iuo_dw_main = dw_listino_vettori_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listino_vettori_det, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listino_vettori_det, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

					  

end event

type dw_listino_vettori_lista from uo_cs_xx_dw within w_listino_vettori
int X=23
int Y=21
int Width=2766
int Height=501
string DataObject="d_listino_vettori_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

type dw_listino_vettori_det from uo_cs_xx_dw within w_listino_vettori
int X=23
int Y=541
int Width=2766
int Height=1341
int TabOrder=2
string DataObject="d_listino_vettori_det"
BorderStyle BorderStyle=StyleRaised!
end type

