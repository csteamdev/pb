﻿$PBExportHeader$w_prodotti_cerca2.srw
forward
global type w_prodotti_cerca2 from w_cs_xx_principale
end type
type st_1 from statictext within w_prodotti_cerca2
end type
type cb_ok from commandbutton within w_prodotti_cerca2
end type
type cb_annulla from uo_cb_close within w_prodotti_cerca2
end type
type dw_stringa_ricerca_prodotti from datawindow within w_prodotti_cerca2
end type
type st_2 from statictext within w_prodotti_cerca2
end type
type cb_codice from uo_cb_ok within w_prodotti_cerca2
end type
type cb_des_prodotto from uo_cb_ok within w_prodotti_cerca2
end type
type cb_comodo from uo_cb_ok within w_prodotti_cerca2
end type
type dw_prodotti_lista from datawindow within w_prodotti_cerca2
end type
end forward

global type w_prodotti_cerca2 from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3191
integer height = 1460
string title = "Ricerca Prodotti"
st_1 st_1
cb_ok cb_ok
cb_annulla cb_annulla
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
st_2 st_2
cb_codice cb_codice
cb_des_prodotto cb_des_prodotto
cb_comodo cb_comodo
dw_prodotti_lista dw_prodotti_lista
end type
global w_prodotti_cerca2 w_prodotti_cerca2

type variables
long il_row
string is_tipo_ricerca='C'
end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
end prototypes

public subroutine wf_pos_ricerca ();// s_cs_xx.parametri.parametro_tipo_ricerca  = 1   ----> ricerca per codice prodotto
//															  2   ----> ricerca per descrizione prodotto

dw_prodotti_lista.reset()
if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) or s_cs_xx.parametri.parametro_tipo_ricerca <> 2 then
	is_tipo_ricerca = "C"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca per Codice:"
	dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
else
	is_tipo_ricerca = "D"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca per Descrizione:"
	dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
end if	
setnull(s_cs_xx.parametri.parametro_pos_ricerca)
dw_prodotti_lista.setfocus()


end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);string ls_sql
long ll_i, ll_y

ls_sql = "select cod_prodotto, des_prodotto, cod_comodo from anag_prodotti where cod_azienda = ~~~'" + s_cs_xx.cod_azienda + "~~~' and ( flag_blocco = ~~~'N~~~' or ( flag_blocco = ~~~'S~~~' and data_blocco > ~~~'" + string(today(),s_cs_xx.db_funzioni.formato_data) + "~~~' ))"
ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next

choose case fs_tipo_ricerca
	case "C"  				// tipo ricerca
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and cod_prodotto like ~~~'" + fs_stringa_ricerca + "~~~'"
			else
				ls_sql = ls_sql + " and cod_prodotto = ~~~'" + fs_stringa_ricerca + "~~~'"
			end if			
		end if
	   ls_sql = ls_sql + " order by cod_prodotto"
	case "D"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and des_prodotto like ~~~'" + fs_stringa_ricerca + "~~~'"
			else
				ls_sql = ls_sql + " and des_prodotto = ~~~'" + fs_stringa_ricerca + "~~~'"
			end if			
		end if
	   ls_sql = ls_sql + " order by des_prodotto"
	case "M"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and cod_comodo like ~~~'" + fs_stringa_ricerca + "~~~'"
			else
				ls_sql = ls_sql + " and cod_comodo = ~~~'" + fs_stringa_ricerca + "~~~'"
			end if			
		end if
	   ls_sql = ls_sql + " order by cod_comodo"
end choose
dw_prodotti_lista.Modify("DataWindow.Table.Select='" + ls_sql + "'")
ll_y = dw_prodotti_lista.retrieve()
if ll_y > 0 then
	st_2.text = "TROVATI " + string(ll_y) + " PRODOTTI"
else
	st_2.text = "NESSUN PRODOTTO TROVATO"
	dw_stringa_ricerca_prodotti.setfocus()
end if
	
end subroutine

on deactivate;call w_cs_xx_principale::deactivate;this.hide()
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_prodotti_lista.settransobject(sqlca)
dw_stringa_ricerca_prodotti.insertrow(0)
dw_prodotti_lista.setrowfocusindicator(hand!)
if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
	dw_prodotti_lista.setfocus()
else
	dw_stringa_ricerca_prodotti.setfocus()	
end if

end event

on w_prodotti_cerca2.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.st_2=create st_2
this.cb_codice=create cb_codice
this.cb_des_prodotto=create cb_des_prodotto
this.cb_comodo=create cb_comodo
this.dw_prodotti_lista=create dw_prodotti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cb_codice
this.Control[iCurrent+7]=this.cb_des_prodotto
this.Control[iCurrent+8]=this.cb_comodo
this.Control[iCurrent+9]=this.dw_prodotti_lista
end on

on w_prodotti_cerca2.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.st_2)
destroy(this.cb_codice)
destroy(this.cb_des_prodotto)
destroy(this.cb_comodo)
destroy(this.dw_prodotti_lista)
end on

event activate;call super::activate;if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
else
//	dw_prodotti_lista.reset()
	dw_stringa_ricerca_prodotti.setfocus()
end if


end event

type st_1 from statictext within w_prodotti_cerca2
integer x = 23
integer y = 40
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Descrizione Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_prodotti_cerca2
integer x = 2766
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;//if dw_prodotti_lista.getrow() > 0 then
//
//	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
//		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
//		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
//		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
//		s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
//		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
//	else
//		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
//		s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
//		s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
//	end if
//	
//	if il_row > 0 and not isnull(il_row) then
//		dw_prodotti_lista.setrow(il_row - 1)
//		il_row = 0
//	end if
//	
//	parent.hide()
//end if

long ll_numrow, ll_riga
string ls_cod_prodotto

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	ll_numrow = s_cs_xx.parametri.parametro_uo_dw_1.rowcount()
	ls_cod_prodotto = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto")
	ll_riga = s_cs_xx.parametri.parametro_uo_dw_1.Find("cod_prodotto = '"+ ls_cod_prodotto +"'", 1, ll_numrow)
	s_cs_xx.parametri.parametro_uo_dw_1.ScrollToRow(ll_riga)
	
	// *** Michela 22/11/2007
	
	dwobject la_null
	
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("cod_prodotto")
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn("cod_prodotto")	
	s_cs_xx.parametri.parametro_uo_dw_1.setrow(ll_riga)
	s_cs_xx.parametri.parametro_uo_dw_1.postevent("ue_getfocus")
	// *** fine modifica

end if

	if il_row > 0 and not isnull(il_row) then
		dw_prodotti_lista.setrow(il_row - 1)
		il_row = 0
	end if

parent.hide()

end event

type cb_annulla from uo_cb_close within w_prodotti_cerca2
integer x = 2377
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_stringa_ricerca_prodotti from datawindow within w_prodotti_cerca2
event ue_key pbm_dwnkey
integer x = 731
integer y = 20
integer width = 2400
integer height = 100
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;string ls_stringa
if key = keyenter! then
	ls_stringa = dw_stringa_ricerca_prodotti.gettext()
	wf_dw_select(is_tipo_ricerca, ls_stringa)
	dw_prodotti_lista.setfocus()
end if
end event

type st_2 from statictext within w_prodotti_cerca2
integer x = 23
integer y = 1260
integer width = 2331
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type cb_codice from uo_cb_ok within w_prodotti_cerca2
integer x = 23
integer y = 140
integer width = 594
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Codice Prodotto"
end type

event clicked;call super::clicked;dw_prodotti_lista.reset()
is_tipo_ricerca = "C"
st_1.text = "Ricerca per Codice:"
dw_stringa_ricerca_prodotti.setfocus()

end event

type cb_des_prodotto from uo_cb_ok within w_prodotti_cerca2
integer x = 617
integer y = 140
integer width = 1737
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizione"
end type

event clicked;call super::clicked;dw_prodotti_lista.reset()
is_tipo_ricerca = "D"
st_1.text = "Descrizione Prodotto:"
dw_stringa_ricerca_prodotti.setfocus()
end event

type cb_comodo from uo_cb_ok within w_prodotti_cerca2
integer x = 2354
integer y = 140
integer width = 640
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Codice Comodo"
end type

event clicked;call super::clicked;dw_prodotti_lista.reset()
is_tipo_ricerca = "M"
st_1.text = "Ricerca per Comodo:"
dw_stringa_ricerca_prodotti.setfocus()

end event

type dw_prodotti_lista from datawindow within w_prodotti_cerca2
event ue_key pbm_dwnkey
integer x = 23
integer y = 140
integer width = 3109
integer height = 1100
integer taborder = 70
string dataobject = "d_prodotti_ricerca"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)

end event

