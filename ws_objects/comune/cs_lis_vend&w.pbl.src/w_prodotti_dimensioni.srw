﻿$PBExportHeader$w_prodotti_dimensioni.srw
$PBExportComments$Finestra Dimensnioni dei prodotti
forward
global type w_prodotti_dimensioni from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_prodotti_dimensioni
end type
type cb_ricerca from commandbutton within w_prodotti_dimensioni
end type
type dw_ricerca from u_dw_search within w_prodotti_dimensioni
end type
type dw_folder_search from u_folder within w_prodotti_dimensioni
end type
type dw_prod_dimensioni_det from uo_cs_xx_dw within w_prodotti_dimensioni
end type
type dw_prod_dimensioni_lista from uo_cs_xx_dw within w_prodotti_dimensioni
end type
type dw_grid from uo_dw_grid within w_prodotti_dimensioni
end type
end forward

global type w_prodotti_dimensioni from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 3214
integer height = 2268
string title = "Dimensioni Prodotti"
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
dw_prod_dimensioni_det dw_prod_dimensioni_det
dw_prod_dimensioni_lista dw_prod_dimensioni_lista
dw_grid dw_grid
end type
global w_prodotti_dimensioni w_prodotti_dimensioni

forward prototypes
public subroutine wf_visualizza_dimensioni ()
public subroutine wf_salva_dimensioni ()
end prototypes

public subroutine wf_visualizza_dimensioni ();string ls_cod_prodotto, ls_sql, ls_flag_dimensione, ls_x[], ls_y[]
long ll_riga, ll_i, ll_x, ll_y, &
     ll_num_intervalli_dim_1, ll_num_intervalli_dim_2
long ld_valore_massimo = 999999999
double ld_valore, ld_valore_old

if dw_prod_dimensioni_lista.getrow() < 1 then return
ls_cod_prodotto = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"cod_prodotto")
ls_flag_dimensione = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"flag_tipo_dimensione_1")
ll_num_intervalli_dim_1 = dw_prod_dimensioni_lista.getitemnumber(dw_prod_dimensioni_lista.getrow(),"num_intervalli_dim_1")
if isnull(ls_cod_prodotto) then return

declare cu_limiti dynamic cursor for sqlsa;

ls_sql = "select limite_dimensione from tab_prodotti_dimensioni_det where cod_azienda = '" + s_cs_xx.cod_azienda + &
         "' and cod_prodotto = '" + ls_cod_prodotto + &
			"' and flag_tipo_dimensione = '" + ls_flag_dimensione + "'"

prepare sqlsa from :ls_sql;
open dynamic cu_limiti;
ll_riga = 0
do while 1=1
   fetch cu_limiti into :ld_valore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if ll_riga >= ll_num_intervalli_dim_1 then exit
	ll_riga ++
	ls_x[ll_riga] = string(ld_valore, "#########.####")
loop
close cu_limiti;

ls_cod_prodotto = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"cod_prodotto")
ls_flag_dimensione = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"flag_tipo_dimensione_2")
ll_num_intervalli_dim_2 = dw_prod_dimensioni_lista.getitemnumber(dw_prod_dimensioni_lista.getrow(),"num_intervalli_dim_2")
if isnull(ls_cod_prodotto) then return

ls_sql = "select limite_dimensione from tab_prodotti_dimensioni_det where cod_azienda = '" + s_cs_xx.cod_azienda + &
         "' and cod_prodotto = '" + ls_cod_prodotto + &
			"' and flag_tipo_dimensione = '" + ls_flag_dimensione + "'"

prepare sqlsa from :ls_sql;

open dynamic cu_limiti;

ll_riga = 0

do while 1 = 1
	
   fetch cu_limiti into :ld_valore;
	
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	if ll_riga >= ll_num_intervalli_dim_2 then exit
	
	ll_riga ++
	
	ls_y[ll_riga] = string(ld_valore,"#########.####")
	
loop

close cu_limiti;

dw_grid.uof_init(dw_grid.MODE_HEADER_EDITABLE, ll_num_intervalli_dim_2, ll_num_intervalli_dim_1)

//ole_grid.object.clear()
//
//ole_grid.object.setRowCol(ll_num_intervalli_dim_2 + 1, ll_num_intervalli_dim_1 + 1)


for ll_x = 1 to upperbound(ls_x)
	
	//ole_grid.object.setColName(ll_x, ls_x[ll_x])
	dw_grid.setitem(0, ll_x, ls_x[ll_x])
	
next

for ll_y = 1 to upperbound(ls_y)
	//ole_grid.object.setRowName(ll_y, ls_y[ll_y])
	dw_grid.setitem(ll_y, 0, ls_y[ll_y])
next



end subroutine

public subroutine wf_salva_dimensioni ();string ls_str, ls_cod_prodotto, ls_flag_dimensione_1, ls_flag_dimensione_2
long ll_num_intervalli_dim_1, ll_num_intervalli_dim_2, ll_i
double  ld_numero

ll_num_intervalli_dim_1 = dw_prod_dimensioni_lista.getitemnumber(dw_prod_dimensioni_lista.getrow(),"num_intervalli_dim_1")
ll_num_intervalli_dim_1 = dw_prod_dimensioni_lista.getitemnumber(dw_prod_dimensioni_lista.getrow(),"num_intervalli_dim_1")
ll_num_intervalli_dim_2 = dw_prod_dimensioni_lista.getitemnumber(dw_prod_dimensioni_lista.getrow(),"num_intervalli_dim_2")

ls_cod_prodotto = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"cod_prodotto")
ls_flag_dimensione_1 = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"flag_tipo_dimensione_1")
ls_flag_dimensione_2 = dw_prod_dimensioni_lista.getitemstring(dw_prod_dimensioni_lista.getrow(),"flag_tipo_dimensione_2")

delete from tab_prodotti_dimensioni_det
where  cod_azienda  = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione dimensioni.~r~nDettaglio errore: " + sqlca.sqlerrtext)
	ROLLBACK;
	return
end if

dw_grid.accepttext()
		 
for ll_i = 1 to ll_num_intervalli_dim_1
	if ll_i = ll_num_intervalli_dim_1 then
		ld_numero = 99999999.9999
	else
		//ls_str = ole_grid.object.getcell(0, ll_i)
		ls_str = dw_grid.getitemstring(0, ll_i)
		
		if isnull(ls_str) or len(ls_str) < 0 then g_mb.messagebox("Listini", "Impossibile inserire dimensioni = 0", stopsign!)
		ld_numero = double(ls_str)
	end if

	insert into tab_prodotti_dimensioni_det
	            ( cod_azienda, 
					  cod_prodotto,
					  flag_tipo_dimensione,
					  num_intervallo,
					  limite_dimensione)
	values       (:s_cs_xx.cod_azienda,
	              :ls_cod_prodotto,
					  :ls_flag_dimensione_1,
					  :ll_i,
					  :ld_numero );
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento dimensioni.~r~nDettaglio errore: " + sqlca.sqlerrtext)
		ROLLBACK;
		return
	end if
next

for ll_i = 1 to ll_num_intervalli_dim_2
	if ll_i = ll_num_intervalli_dim_2 then
		ld_numero = 99999999.9999
	else
		//ls_str = ole_grid.object.getcell(ll_i, 0)
		ls_str = dw_grid.getitemstring(ll_i, 0)
		
		if isnull(ls_str) or len(ls_str) < 0 then g_mb.messagebox("Listini", "Impossibile inserire dimensioni = 0", stopsign!)
		ld_numero = double(ls_str)
	end if

	insert into tab_prodotti_dimensioni_det
	            ( cod_azienda, 
					  cod_prodotto,
					  flag_tipo_dimensione,
					  num_intervallo,
					  limite_dimensione)
	values       (:s_cs_xx.cod_azienda,
	              :ls_cod_prodotto,
					  :ls_flag_dimensione_2,
					  :ll_i,
					  :ld_numero );
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento dimensioni.~r~nDettaglio errore: " + sqlca.sqlerrtext)
		ROLLBACK;
		return
	end if
next

return

end subroutine

event pc_setwindow;call super::pc_setwindow;string       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[], l_objects[ ]


dw_prod_dimensioni_lista.set_dw_key("cod_azienda")
dw_prod_dimensioni_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_noretrieveonopen, &
                                c_default)
dw_prod_dimensioni_det.set_dw_options(sqlca, &
                              dw_prod_dimensioni_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)

//dw_ext_input_min_max_1.set_dw_options(sqlca, &
//                                pcca.null_object, &
//                                c_nonew + c_nomodify + c_nodelete + c_disablecc, &
//                                c_default)
//dw_ext_input_min_max_2.set_dw_options(sqlca, &
//                                pcca.null_object, &
//                                c_nonew + c_nomodify + c_nodelete + c_disablecc, &
//                                c_default)

//set_w_options(c_noenablepopup)
iuo_dw_main = dw_prod_dimensioni_lista


// ----------------------------------------------

l_criteriacolumn[1] = "rs_cod_prodotto"

l_searchtable[1] = "tab_prodotti_dimensioni"
l_searchtable[2] = "anag_prodotti"
l_searchtable[3] = "anag_prodotti"
l_searchtable[4] = "anag_prodotti"
l_searchtable[5] = "anag_prodotti"
l_searchtable[6] = "anag_prodotti"

l_searchcolumn[1] = "cod_prodotto"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_prod_dimensioni_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

//dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
//                                  dw_folder_search.c_foldertableft)
l_objects[1] = dw_prod_dimensioni_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_prod_dimensioni_lista.change_dw_current()

//ole_grid.object.mode = 2
end event

on w_prodotti_dimensioni.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_prod_dimensioni_det=create dw_prod_dimensioni_det
this.dw_prod_dimensioni_lista=create dw_prod_dimensioni_lista
this.dw_grid=create dw_grid
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.dw_ricerca
this.Control[iCurrent+4]=this.dw_folder_search
this.Control[iCurrent+5]=this.dw_prod_dimensioni_det
this.Control[iCurrent+6]=this.dw_prod_dimensioni_lista
this.Control[iCurrent+7]=this.dw_grid
end on

on w_prodotti_dimensioni.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_prod_dimensioni_det)
destroy(this.dw_prod_dimensioni_lista)
destroy(this.dw_grid)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_prod_dimensioni_det,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
////                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//dw_ricerca.fu_loadcode("rs_cod_prodotto", &
//                 "anag_prodotti", &
//					  "cod_prodotto", &
//					  "des_prodotto", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "(Tutti)" )
//ole_grid.object.alignment = 2
//ole_grid.object.prefix = ""

end event

type cb_reset from commandbutton within w_prodotti_dimensioni
integer x = 1783
integer y = 300
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_prod_dimensioni_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_prodotti_dimensioni
integer x = 2171
integer y = 300
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type dw_ricerca from u_dw_search within w_prodotti_dimensioni
integer x = 46
integer y = 140
integer width = 2551
integer height = 136
integer taborder = 40
string dataobject = "d_ext_prodotti_dim_search"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type dw_folder_search from u_folder within w_prodotti_dimensioni
integer x = 23
integer y = 20
integer width = 3113
integer height = 636
integer taborder = 30
end type

type dw_prod_dimensioni_det from uo_cs_xx_dw within w_prodotti_dimensioni
integer x = 23
integer y = 680
integer width = 3122
integer height = 320
integer taborder = 80
string dataobject = "d_prod_dimensioni_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_prod_dimensioni_det,"cod_prodotto")
end choose
end event

type dw_prod_dimensioni_lista from uo_cs_xx_dw within w_prodotti_dimensioni
integer x = 137
integer y = 140
integer width = 1737
integer height = 500
integer taborder = 70
string dataobject = "d_prod_dimensioni_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

//wf_visualizza_dimensioni()
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event updateend;call super::updateend;//long ll_prog_1, ll_prog_2, ll_i, ll_valore_massimo, ll_riga
//
//ll_valore_massimo = 999999999
//ll_prog_1 = getitemnumber(getrow(),"num_intervalli_dim_1")
//ll_prog_2 = getitemnumber(getrow(),"num_intervalli_dim_2")
//dw_ext_input_min_max_1.reset()
//dw_ext_input_min_max_2.reset()
//
//for ll_i = 1 to ll_prog_1
//	ll_riga = dw_ext_input_min_max_1.insertrow(0)
//next
//dw_ext_input_min_max_1.setitem(ll_riga, "massimo", ll_valore_massimo)
//for ll_i = 1 to ll_prog_2
//	ll_riga = dw_ext_input_min_max_2.insertrow(0)
//next
//dw_ext_input_min_max_2.setitem(ll_riga,"massimo", ll_valore_massimo)
//
wf_visualizza_dimensioni()
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_prod_dimensioni_det.object.b_ricerca_prodotto.enabled = true
	//ole_grid.object.mode = 1
//	dw_ext_input_min_max_1.settaborder(2,0)
//	dw_ext_input_min_max_2.settaborder(2,0)
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_prod_dimensioni_det.object.b_ricerca_prodotto.enabled = true
	//ole_grid.object.mode = 1
//	dw_ext_input_min_max_1.settaborder(2,0)
//	dw_ext_input_min_max_2.settaborder(2,0)
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_prod_dimensioni_det.object.b_ricerca_prodotto.enabled = false
	//ole_grid.object.mode = 2
end if
end event

event updatestart;call super::updatestart;string ls_cod_prodotto
long ll_i

if getrow() > 0 and not isnull( getitemstring(this.getrow(),"cod_prodotto") )then
	
	if this.getitemstring(this.getrow(),"flag_tipo_dimensione_1") = this.getitemstring(this.getrow(),"flag_tipo_dimensione_2") then
		g_mb.messagebox("APICE","Non è possibile assegnare stesso tipo di dimensione",StopSign!)
		return -1
	end if
	
	if isnull(this.getitemnumber(this.getrow(),"num_intervalli_dim_1")) or this.getitemnumber(this.getrow(),"num_intervalli_dim_1") = 0 then
		g_mb.messagebox("APICE","Gli intervalli della prima dimensione devono esistere",StopSign!)
		return -1
	end if
	
	if isnull(this.getitemnumber(this.getrow(),"num_intervalli_dim_2")) then
		g_mb.messagebox("APICE","Impossibile salvare: gli intervalli della seconda dimensione non sono impostati",StopSign!)
		return -1
	end if
	
end if

for ll_i = 1 to deletedcount()
	
	ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto", Delete!, true)
	
	delete tab_prodotti_dimensioni_det
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
	
next


end event

event rowfocuschanged;call super::rowfocuschanged;wf_visualizza_dimensioni()
end event

event pcd_savebefore;call super::pcd_savebefore;wf_salva_dimensioni()

end event

event pcd_save;call super::pcd_save;dw_prod_dimensioni_det.object.b_ricerca_prodotto.enabled = true
end event

type dw_grid from uo_dw_grid within w_prodotti_dimensioni
integer x = 27
integer y = 1020
integer width = 3122
integer height = 1116
integer taborder = 70
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
00w_prodotti_dimensioni.bin 
2B00000600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe00000006000000000000000000000001000000010000000000001000fffffffe00000000fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
10w_prodotti_dimensioni.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
