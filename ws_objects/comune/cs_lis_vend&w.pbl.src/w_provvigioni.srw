﻿$PBExportHeader$w_provvigioni.srw
$PBExportComments$Finestra Input Condizioni Provvigioni Agenti
forward
global type w_provvigioni from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_provvigioni
end type
type cb_ricerca from commandbutton within w_provvigioni
end type
type dw_provvigioni_dettaglio from uo_cs_xx_dw within w_provvigioni
end type
type cb_sconti from commandbutton within w_provvigioni
end type
type dw_ricerca from u_dw_search within w_provvigioni
end type
type dw_provvigioni_lista from uo_cs_xx_dw within w_provvigioni
end type
type dw_folder_search from u_folder within w_provvigioni
end type
end forward

global type w_provvigioni from w_cs_xx_principale
integer width = 2939
integer height = 2032
string title = "Provvigioni"
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_provvigioni_dettaglio dw_provvigioni_dettaglio
cb_sconti cb_sconti
dw_ricerca dw_ricerca
dw_provvigioni_lista dw_provvigioni_lista
dw_folder_search dw_folder_search
end type
global w_provvigioni w_provvigioni

type variables
boolean ib_new=false, ib_modify=false
long il_dim_x[], il_dim_y[], il_x, il_y, il_num_scaglione
end variables

forward prototypes
public function string wf_formato (string fs_tipo_dato)
public subroutine wf_abilita_colonne ()
public subroutine wf_disabilita_colonne ()
public subroutine wf_termine_scaglione ()
end prototypes

public function string wf_formato (string fs_tipo_dato);choose case fs_tipo_dato
	case "S"
		return "##.00"
	case "M"
		return "###.00"
	case "A"
		return "###,###,###,###.0000"
	case "D"
		return "###,###,###,###.0000"
	case "P"
		return "###,###,###,###.0000"
end choose
return ""
end function

public subroutine wf_abilita_colonne ();if dw_provvigioni_dettaglio.getrow() > 0 and ( ib_new or ib_modify) then

		dw_provvigioni_dettaglio.object.cod_cat_mer.border = 5
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.mode = 0
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_provvigioni_dettaglio.object.cod_cat_mer.tabsequence = 50

		dw_provvigioni_dettaglio.object.cod_prodotto.border = 5
		dw_provvigioni_dettaglio.object.cod_prodotto.background.mode = 0
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_provvigioni_dettaglio.object.cod_prodotto.tabsequence = 60

		dw_provvigioni_dettaglio.object.cod_cliente.border = 5
		dw_provvigioni_dettaglio.object.cod_cliente.background.mode = 0
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_provvigioni_dettaglio.object.cod_cliente.tabsequence = 70

		dw_provvigioni_dettaglio.object.cod_categoria.border = 5
		dw_provvigioni_dettaglio.object.cod_categoria.background.mode = 0
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.color = rgb(255,255,255)
		dw_provvigioni_dettaglio.object.cod_categoria.tabsequence = 80

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public subroutine wf_disabilita_colonne ();if dw_provvigioni_dettaglio.getrow() > 0 and ( ib_new or ib_modify) then

		dw_provvigioni_dettaglio.object.cod_cat_mer.border = 6
		dw_provvigioni_dettaglio.object.cod_cat_mer.background.mode = 1
		dw_provvigioni_dettaglio.object.cod_cat_mer.tabsequence = 0

		dw_provvigioni_dettaglio.object.cod_prodotto.border = 6
		dw_provvigioni_dettaglio.object.cod_prodotto.background.mode = 1
		dw_provvigioni_dettaglio.object.cod_prodotto.tabsequence = 0

		dw_provvigioni_dettaglio.object.cod_cliente.border = 6
		dw_provvigioni_dettaglio.object.cod_cliente.background.mode = 1
		dw_provvigioni_dettaglio.object.cod_cliente.tabsequence = 0

		dw_provvigioni_dettaglio.object.cod_categoria.border = 6
		dw_provvigioni_dettaglio.object.cod_categoria.background.mode = 1
		dw_provvigioni_dettaglio.object.cod_categoria.tabsequence = 0

//		cb_ricerca_cliente.enabled = false
//		cb_ricerca_prodotto.enabled = false
end if
end subroutine

public subroutine wf_termine_scaglione ();string ls_tipo_scaglione
double ld_variazione_1, ld_variazione_2, ld_variazione_3, ld_variazione_4, ld_variazione_5


ls_tipo_scaglione = dw_provvigioni_dettaglio.getitemstring(dw_provvigioni_dettaglio.getrow(),"flag_tipo_scaglioni")
ld_variazione_1 = dw_provvigioni_dettaglio.getitemnumber(dw_provvigioni_dettaglio.getrow(),"variazione_1")
ld_variazione_2 = dw_provvigioni_dettaglio.getitemnumber(dw_provvigioni_dettaglio.getrow(),"variazione_2")
ld_variazione_3 = dw_provvigioni_dettaglio.getitemnumber(dw_provvigioni_dettaglio.getrow(),"variazione_3")
ld_variazione_4 = dw_provvigioni_dettaglio.getitemnumber(dw_provvigioni_dettaglio.getrow(),"variazione_4")
ld_variazione_5 = dw_provvigioni_dettaglio.getitemnumber(dw_provvigioni_dettaglio.getrow(),"variazione_5")
if ld_variazione_1 = 0 or isnull(ld_variazione_1) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_1", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_2 = 0 or isnull(ld_variazione_2) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_1", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_1", 999999999)
	end if
elseif ld_variazione_3 = 0 or isnull(ld_variazione_3) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_2", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_2", 999999999)
	end if
elseif ld_variazione_4 = 0 or isnull(ld_variazione_4) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_3", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_3", 999999999)
	end if
elseif ld_variazione_5 = 0 or isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_4", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_4", 999999999)
	end if
end if

if ld_variazione_5 <> 0 and not isnull(ld_variazione_5) then
	if ls_tipo_scaglione = "Q" then
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_5", 99999999)
	else
		dw_provvigioni_dettaglio.setitem(dw_provvigioni_dettaglio.getrow(),"scaglione_5", 999999999)
	end if
end if

end subroutine

event pc_setwindow;call super::pc_setwindow;datetime ldt_data_chiusura_annuale_prec, ldt_data_chiusura_annuale, &
       ldt_data_chiusura_periodica
string ls_data_chiusura_annuale_prec, ls_data_chiusura_annuale, &
       ls_data_chiusura_periodica, ls_cod_livello_prod_1, ls_cod_livello_prod_2, &
   	 ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, &
       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[], l_objects[ ]


dw_provvigioni_lista.set_dw_key("cod_azienda")
dw_provvigioni_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen, &
                                 c_default)
dw_provvigioni_dettaglio.set_dw_options(sqlca, &
                                dw_provvigioni_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
iuo_dw_main=dw_provvigioni_lista


l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "rs_cod_tipo_listino"
l_criteriacolumn[3] = "rs_cod_valuta"
l_criteriacolumn[4] = "rs_cod_cat_mer"
l_criteriacolumn[5] = "rs_cod_prodotto"
l_criteriacolumn[6] = "rs_cod_categoria"
l_criteriacolumn[7] = "rs_cod_cliente"
l_criteriacolumn[8] = "rs_cod_agente"

l_searchtable[1] = "provvigioni"
l_searchtable[2] = "provvigioni"
l_searchtable[3] = "provvigioni"
l_searchtable[4] = "provvigioni"
l_searchtable[5] = "provvigioni"
l_searchtable[6] = "provvigioni"
l_searchtable[7] = "provvigioni"
l_searchtable[8] = "provvigioni"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_tipo_listino_prodotto"
l_searchcolumn[3] = "cod_valuta"
l_searchcolumn[4] = "cod_cat_mer"
l_searchcolumn[5] = "cod_prodotto"
l_searchcolumn[6] = "cod_categoria"
l_searchcolumn[7] = "cod_cliente"
l_searchcolumn[8] = "cod_agente"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_provvigioni_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
l_objects[1] = dw_provvigioni_lista
l_objects[2] = cb_sconti
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto_1
//l_objects[5] = cb_ricerca_cliente_1
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_provvigioni_lista.change_dw_current()
//cb_ricerca_cliente.enabled = false
//cb_ricerca_prodotto.enabled = false

end event

on w_provvigioni.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_provvigioni_dettaglio=create dw_provvigioni_dettaglio
this.cb_sconti=create cb_sconti
this.dw_ricerca=create dw_ricerca
this.dw_provvigioni_lista=create dw_provvigioni_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.dw_provvigioni_dettaglio
this.Control[iCurrent+4]=this.cb_sconti
this.Control[iCurrent+5]=this.dw_ricerca
this.Control[iCurrent+6]=this.dw_provvigioni_lista
this.Control[iCurrent+7]=this.dw_folder_search
end on

on w_provvigioni.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_provvigioni_dettaglio)
destroy(this.cb_sconti)
destroy(this.dw_ricerca)
destroy(this.dw_provvigioni_lista)
destroy(this.dw_folder_search)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))")

f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
//					  "cod_prodotto", &
//					  sqlca, &
//                 "anag_prodotti", &
//					  "cod_prodotto", &
//					  "des_prodotto", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
//                 "cod_cliente", &
//                 sqlca, &
//                 "anag_clienti", &
//                 "cod_cliente", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


dw_ricerca.fu_loadcode("rs_cod_tipo_listino", &
                       "tab_tipi_listini_prodotti", &
                       "cod_tipo_listino_prodotto", &
                       "des_tipo_listino_prodotto", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_tipo_listino_prodotto asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_prodotto", &
//                       "anag_prodotti", &
//		                 "cod_prodotto", &
//					        "des_prodotto", &
//					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "(Tutti)" )
//
dw_ricerca.fu_loadcode("rs_cod_cat_mer", &
                       "tab_cat_mer", &
					        "cod_cat_mer", &
					        "des_cat_mer", &
					        "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cat_mer asc", "(Tutti)" )
					  
dw_ricerca.fu_loadcode("rs_cod_valuta", &
                       "tab_valute", &
							  "cod_valuta", &
							  "des_valuta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_valuta asc", "(Tutti)" )

//dw_ricerca.fu_loadcode("rs_cod_cliente", &
//                        "anag_clienti", &
//								"cod_cliente", &
//								"rag_soc_1",&
//								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cliente asc", "(Tutti)" )

dw_ricerca.fu_loadcode("rs_cod_categoria", &
                        "tab_categorie", &
								"cod_categoria", &
								"des_categoria", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_categoria asc", "(Tutti)" )

dw_ricerca.fu_loadcode("rs_cod_agente", &
                        "anag_agenti", &
								"cod_agente", &
								"rag_soc_1",&
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_agente asc", "(Tutti)" )

					  
					  
					  
					  
					  
					  
end event

type cb_reset from commandbutton within w_provvigioni
integer x = 2469
integer y = 160
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_provvigioni_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_ricerca from commandbutton within w_provvigioni
integer x = 2469
integer y = 60
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type dw_provvigioni_dettaglio from uo_cs_xx_dw within w_provvigioni
integer x = 69
integer y = 680
integer width = 2766
integer height = 1240
integer taborder = 100
string dataobject = "d_provvigioni_dettaglio"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_provvigioni_dettaglio,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_provvigioni_dettaglio,"cod_prodotto")
end choose
end event

type cb_sconti from commandbutton within w_provvigioni
integer x = 2469
integer y = 60
integer width = 366
integer height = 80
integer taborder = 101
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;if dw_provvigioni_lista.getrow() > 0 then
	window_open_parm(w_provvigioni_sconti, -1, dw_provvigioni_lista)
end if
end event

type dw_ricerca from u_dw_search within w_provvigioni
integer x = 137
integer y = 40
integer width = 2377
integer height = 600
integer taborder = 40
string dataobject = "d_provvigioni_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
end choose
end event

type dw_provvigioni_lista from uo_cs_xx_dw within w_provvigioni
integer x = 137
integer y = 40
integer width = 2217
integer height = 600
integer taborder = 90
string dataobject = "d_provvigioni_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG   l_Error


l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx, ll_max_progressivo
string ls_cod_tipo_listino_prodotto, ls_cod_valuta
datetime ldt_data_inizio_val

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
end event

event pcd_new;call super::pcd_new;string ls_cod_valuta
datetime ldt_oggi


if i_extendmode then
	ldt_oggi = datetime(today())
	dw_provvigioni_lista.setitem(dw_provvigioni_lista.getrow(),"data_inizio_val", ldt_oggi)
end if

setnull(ls_cod_valuta)
select stringa
into   :ls_cod_valuta
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';

dw_provvigioni_dettaglio.setitem(this.getrow(),"cod_valuta", ls_cod_valuta)

dw_provvigioni_dettaglio.object.b_ricerca_cliente.enabled = true
dw_provvigioni_dettaglio.object.b_ricerca_prodotto.enabled = true
ib_new = true
end event

event updatestart;call super::updatestart;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_tipo_scaglione, ls_cod_azienda
datetime ldt_data_inizio_val
long  ll_max_progressivo, ll_i
double ld_scaglione

if rowcount() = 1 and &
		(isnull(getitemstring(getrow(), "cod_azienda")) or getitemstring(getrow(), "cod_azienda")="") then return

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_agente")) then
		g_mb.messagebox("APICE","L'agente è un dato obbligatorio")
		return 1
	end if
next

wf_termine_scaglione()

setnull(ll_max_progressivo)
ll_max_progressivo = getitemnumber(getrow(),"progressivo")
if isnull(ll_max_progressivo) or ll_max_progressivo = 0 and rowcount() > 0 then
	setnull(ll_max_progressivo)
	ls_cod_tipo_listino_prodotto = getitemstring(getrow(),"cod_tipo_listino_prodotto")
	ls_cod_valuta = getitemstring(getrow(),"cod_valuta")
	ldt_data_inizio_val = getitemdatetime(getrow(),"data_inizio_val")
	
	select max(progressivo)
	into  :ll_max_progressivo
	from  provvigioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			cod_valuta = :ls_cod_valuta and
			data_inizio_val = :ldt_data_inizio_val;
	if isnull(ll_max_progressivo) then
		ll_max_progressivo = 1
	else
		ll_max_progressivo ++
	end if
	setitem(getrow(),"progressivo",ll_max_progressivo)
end if
end event

event pcd_validaterow;call super::pcd_validaterow;
if isnull(getitemstring(i_rownbr,"cod_tipo_listino_prodotto")) then
	g_mb.messagebox("APICE","Tipo listino obbligatorio")
	pcca.error = c_fatal
end if

if isnull(getitemstring(i_rownbr,"cod_cliente")) and &
	isnull(getitemstring(i_rownbr,"cod_categoria")) and &
	isnull(getitemstring(i_rownbr,"cod_cat_mer")) and &
	isnull(getitemstring(i_rownbr,"cod_agente")) and &
	isnull(getitemstring(i_rownbr,"cod_prodotto")) then
	g_mb.messagebox("APICE","E' obbligatorio inserire almeno un agente, cliente, prodotto o una categoria di cliente o prodotto")
	pcca.error = c_fatal
end if

end event

event pcd_modify;call super::pcd_modify;dw_provvigioni_dettaglio.object.b_ricerca_cliente.enabled = false
dw_provvigioni_dettaglio.object.b_ricerca_prodotto.enabled = false
ib_modify = true

end event

event pcd_view;call super::pcd_view;dw_provvigioni_dettaglio.object.b_ricerca_cliente.enabled = false
dw_provvigioni_dettaglio.object.b_ricerca_prodotto.enabled = false
ib_new = false
ib_modify = false
end event

event pcd_refresh;call super::pcd_refresh;long riga

riga = getrow()

if i_refreshmode = c_refreshmodify or i_refreshmode = c_refreshRFC then 
	if getitemstatus(getrow(),0,primary!) = newmodified! then
		wf_abilita_colonne()
	else
		wf_disabilita_colonne()
	end if
end if
if i_refreshmode = c_refreshnew then wf_abilita_colonne()
end event

type dw_folder_search from u_folder within w_provvigioni
integer x = 23
integer y = 20
integer width = 2857
integer height = 640
integer taborder = 50
end type

