﻿$PBExportHeader$w_provvigioni_sconti.srw
$PBExportComments$Finestra Provvigioni per Scaglioni di Sconto
forward
global type w_provvigioni_sconti from w_cs_xx_principale
end type
type dw_provvigioni_sconti_lista from uo_cs_xx_dw within w_provvigioni_sconti
end type
end forward

global type w_provvigioni_sconti from w_cs_xx_principale
int Width=1276
int Height=1141
boolean TitleBar=true
string Title="Sconti / Provvigioni"
dw_provvigioni_sconti_lista dw_provvigioni_sconti_lista
end type
global w_provvigioni_sconti w_provvigioni_sconti

event pc_setwindow;call super::pc_setwindow;dw_provvigioni_sconti_lista.set_dw_key("cod_azienda")
dw_provvigioni_sconti_lista.set_dw_key("cod_tipo_listino_prodotto")
dw_provvigioni_sconti_lista.set_dw_key("cod_valuta")
dw_provvigioni_sconti_lista.set_dw_key("data_inizio_val")
dw_provvigioni_sconti_lista.set_dw_key("progressivo")

dw_provvigioni_sconti_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default )

iuo_dw_main = dw_provvigioni_sconti_lista

end event

on w_provvigioni_sconti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_provvigioni_sconti_lista=create dw_provvigioni_sconti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_provvigioni_sconti_lista
end on

on w_provvigioni_sconti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_provvigioni_sconti_lista)
end on

type dw_provvigioni_sconti_lista from uo_cs_xx_dw within w_provvigioni_sconti
int X=23
int Y=21
int Width=1189
int Height=1001
int TabOrder=1
string DataObject="d_provvigioni_sconti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_i, ll_progressivo
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_listino_prodotto")) then
      this.setitem(ll_i, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
   end if
   if isnull(this.getitemstring(ll_i, "cod_valuta")) then
      this.setitem(ll_i, "cod_valuta", ls_cod_valuta)
   end if
   if isnull(this.getitemdatetime(ll_i, "data_inizio_val")) then
      this.setitem(ll_i, "data_inizio_val", ldt_data_inizio_val)
   end if
   if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") = 0 then
      this.setitem(ll_i, "progressivo", ll_progressivo)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_i, ll_progressivo, ll_errore
datetime ldt_data_inizio_val

ls_cod_tipo_listino_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
ldt_data_inizio_val = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_inizio_val")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

