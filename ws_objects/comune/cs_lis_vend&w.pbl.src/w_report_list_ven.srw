﻿$PBExportHeader$w_report_list_ven.srw
forward
global type w_report_list_ven from w_cs_xx_principale
end type
type tab_1 from tab within w_report_list_ven
end type
type selezione from userobject within tab_1
end type
type dw_prodotti from uo_dddw_checkbox within selezione
end type
type cb_1 from commandbutton within selezione
end type
type dw_selezione from uo_std_dw within selezione
end type
type selezione from userobject within tab_1
dw_prodotti dw_prodotti
cb_1 cb_1
dw_selezione dw_selezione
end type
type report from userobject within tab_1
end type
type dw_report from uo_std_dw within report
end type
type report from userobject within tab_1
dw_report dw_report
end type
type esporta from userobject within tab_1
end type
type st_info_1 from statictext within esporta
end type
type cb_3 from commandbutton within esporta
end type
type cb_2 from commandbutton within esporta
end type
type esporta from userobject within tab_1
st_info_1 st_info_1
cb_3 cb_3
cb_2 cb_2
end type
type tab_1 from tab within w_report_list_ven
selezione selezione
report report
esporta esporta
end type
end forward

global type w_report_list_ven from w_cs_xx_principale
integer width = 3863
integer height = 1956
string title = "Report Listini Vendita"
tab_1 tab_1
end type
global w_report_list_ven w_report_list_ven

type variables
constant string COLONNA_POSIZIONE = "N"
constant string COLONNA_CODICE = "O"
constant string COLONNA_DESCRIZIONE = "P"
constant string COLONNA_UM = "Q"
constant string COLONNA_VERN_1 = "R"
constant string COLONNA_VERN_2 = "S"
constant string COLONNA_VERN_3 = "T"
constant string COLONNA_VERN_4 = "U"
constant string COLONNA_VERN_5 = "V"
constant string COLONNA_VERN_6 = "W"
constant string COLONNA_VERN_7 = "X"
constant string COLONNA_VERN_8 = "Y"
constant string COLONNA_VERN_9 = "Z"


constant string COLUMN_SEP = " - "
constant string COLUMN_VUOTA = "- - -"


private:
	uo_condizioni_cliente iuo_condizioni_cliente
	string is_logo_lo7
	string is_temp_file, is_temp_file_pdf
	boolean ib_report_excel = false
	double id_cambio_ven=1
	
	// FILTRI
	string is_cod_tipo_listino, is_cod_valuta, is_cod_cliente, is_cod_lingua, is_cod_lingua_prodotti
	int ii_quantita
	datetime idt_data_riferimento
	
	// EXCEL
	uo_excel iuo_excel
	integer ii_row_offet = 1, ii_row_offet_header = 1
	
	// APPOGGIO
	string is_prefix_nota_fissa_testata, is_prefix_nota_fissa_piede
end variables

forward prototypes
public function integer wf_report_prodotto (string as_cod_prodotto_listino, integer ai_cod_versione)
public subroutine wf_lingua_cliente (string as_cod_cliente)
public subroutine wf_traduci_des_prodotto (string as_cod_prodotto, string as_cod_lingua, ref string as_des_prodotto)
public function integer wf_esporta_excel ()
public function boolean wf_esporta_pdf ()
public function integer wf_report (boolean ab_report_excel)
public function integer wf_get_prodotti (string as_cod_prodotto, ref string as_cod_prodotti[])
public function integer wf_get_verniciature_testata (string as_cod_prodotto_listino, integer ai_cod_versione, ref datastore ads_store)
public function string wf_get_excel_column_vern (integer ai_pos_verniciatura)
public subroutine wf_traduci_listino (string as_cod_prodotto_listino, long al_cod_versione, string as_cod_lingua, ref string as_des_prodotto_listino, ref string as_nota_testata, ref string as_nota_piede)
private subroutine wf_crea_intestazione_excel (string as_cod_prodotto, string as_note_testata)
private subroutine wf_crea_pie_pagina_excel (string as_cod_prodotto, long al_row_excel, string as_note_piede)
public function boolean wf_nomenclatura_valida (string as_cod_prodotto)
public subroutine wf_note_fisse (string as_cod_cliente)
public function string wf_get_column_value (long al_row, integer ai_vern_pos, decimal ad_prezzo_vendita, string as_formato)
end prototypes

public function integer wf_report_prodotto (string as_cod_prodotto_listino, integer ai_cod_versione);/**
 * stefanop
 * 13/06/2012
 *
 * Eseguo report
 **/
 
string		ls_des_prodotto_listino, ls_path_immagine, ls_cod_prodotto_vendita, ls_des_prodotto_vendita, ls_cod_misura_ven, &
			ls_cod_prodotto_vern, ls_messaggio, ls_null, ls_formato, ls_cod_vern, ls_des_prodotto, ls_cod_misura_mag, &
			ls_colonna_excel, ls_cod_prodotti[], ls_empty[], ls_test, ls_column_value, ls_nota_testata, ls_nota_piede, ls_path_legenda_vern, &
			ls_flag_no_visualizza_zero, ls_cod_nomenclatura, ls_flag_blocco,ls_des_misura_lingua
			
int			li_count_verniciature, li_i, li_lenght, li_count, li_prodotto, li_pos, li_vern_count

long		ll_livello_gruppo, ll_row, ll_num_posizione, ll_num_prog_posizione, ll_row_excel, ll_i, ll_prog_stampa

decimal{4} ld_fat_conversione_ven, ld_prezzo_vendita, ld_prezzo_vendita_col

datastore lds_verniciature


ls_flag_no_visualizza_zero = tab_1.selezione.dw_selezione.getitemstring(1, "flag_no_visualizza_zero")


setnull(ls_null)
ll_row_excel = ii_row_offet + ii_row_offet_header

// stefanop 05/12/2012: spec Migliorie listini
// Controllo il flag lingua del prodotto
if wf_nomenclatura_valida(as_cod_prodotto_listino) = false then
	return 0
end if
// ----

select path_immagine, path_legenda, des_prodotto_listino, nota_testata, nota_piede, prog_ordine_stampa
into :ls_path_immagine, :ls_path_legenda_vern, :ls_des_prodotto_listino, :ls_nota_testata, :ls_nota_piede, :ll_prog_stampa
from tes_list_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :as_cod_prodotto_listino and
		 cod_versione = :ai_cod_versione;


//se la lingua impostata è diversa da italiano allora recupero le traduzioni
//(des_prodotto_listino - nota_testata - nota_piede)
//se la traduzione corrispondente nn è stata impostata allora lascio la nomenclatura in italiano
if is_cod_lingua <> "ITA" then
	wf_traduci_listino(	as_cod_prodotto_listino, ai_cod_versione, is_cod_lingua, &
								ref ls_des_prodotto_listino, ref ls_nota_testata, ref ls_nota_piede)
end if

if isnull(ls_nota_testata) or ls_nota_testata = "" then ls_nota_testata = ""
if isnull(ls_nota_piede) or ls_nota_piede = "" then ls_nota_piede = ""

// Aggiungo eventiali note_fisse
ls_nota_testata = is_prefix_nota_fissa_testata + ls_nota_testata
ls_nota_piede = is_prefix_nota_fissa_piede + ls_nota_piede

// Verniciature
destroy lds_verniciature
li_vern_count = wf_get_verniciature_testata(as_cod_prodotto_listino, ai_cod_versione, ref lds_verniciature)
if li_vern_count < 0 then
	g_mb.error("Errore durante il recupero delle verniciature per il prodotto "+  as_cod_prodotto_listino + " versione " + string(ai_cod_versione), sqlca)
	return -1
elseif li_vern_count = 0 then
	g_mb.warning("Attenzione: il prodotto " + string(as_cod_prodotto_listino) + " versione " + string(ai_cod_versione) + " non ha nessuna verniciatura associata!")
	return 0
end if
// ---

//Donato 23/11/2012 SR Miglioria_listini_sfuso rev. 05 del 31/10/2012
//impostare la descrizione del prodotto usando la nuova colonna anag_prodotti_des_breve (vch(255))
//questo valore nn sarà tradotto
select des_breve
into :ls_des_prodotto
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto_listino;

//se la des breve non è stata imposta metti quella standard del prodotto
if ls_des_prodotto="" or isnull(ls_des_prodotto) then

	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_prodotto_listino;
	
	//Descrizione in lingua
	if is_cod_lingua <> "ITA" then wf_traduci_des_prodotto(as_cod_prodotto_listino, is_cod_lingua, ref ls_des_prodotto)
end if
//fine modifica -------------------------------------------------------------------------------------------------------

if ib_report_excel then
	wf_crea_intestazione_excel(as_cod_prodotto_listino, ls_nota_testata)
	iuo_excel.uof_inser_image(ll_row_excel, "A", ls_path_immagine)
	
	iuo_excel.uof_inser_image(ll_row_excel + 45, "A", ls_path_legenda_vern)
end if

ll_livello_gruppo = 1
ll_row = tab_1.report.dw_report.insertrow(0)
tab_1.report.dw_report.setitem(ll_row, "des_prodotto_listino", ls_des_prodotto_listino)
tab_1.report.dw_report.setitem(ll_row, "des_prodotto", ls_des_prodotto)
tab_1.report.dw_report.setitem(ll_row, "cod_prodotto_listino", as_cod_prodotto_listino)
tab_1.report.dw_report.setitem(ll_row, "livello_gruppo", ll_livello_gruppo)
tab_1.report.dw_report.setitem(ll_row, "path_immagine", ls_path_immagine)
tab_1.report.dw_report.setitem(ll_row, "path_legenda", ls_path_legenda_vern)
tab_1.report.dw_report.setitem(ll_row, "prog_stampa", ll_prog_stampa)

ll_livello_gruppo++

// Ciclo dettagli
declare cu_det_list_ven cursor for  
	select cod_prodotto_vendita, num_posizione, num_prog_posizione
	from det_list_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_listino = :as_cod_prodotto_listino and
			 cod_versione = :ai_cod_versione
	order by num_posizione,cod_prodotto_vendita;

open cu_det_list_ven;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore in OPEN cursore 'cu_det_list_ven'", sqlca)
	return -1
end if

do while true
	
	fetch cu_det_list_ven into :ls_cod_prodotto_vendita, :ll_num_posizione, :ll_num_prog_posizione;

	if sqlca.sqlcode < 0 then
		close cu_det_list_ven;
		g_mb.error("Errore in FETCH cursore 'cu_det_list_ven'", sqlca)
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	ls_cod_prodotti = ls_empty
	li_lenght = len(ls_cod_prodotto_vendita)
	
	// non devo mostrare i prodotti che alla sesta cifra sta il carattere Z
	if li_lenght >= 6 and mid(ls_cod_prodotto_vendita, 6, 1) = "Z" then continue
	
	// Se il prodotto finisce con --- allora recupero tutte le dimensioni del prodotto
	if li_lenght > 3 and right(ls_cod_prodotto_vendita, 3) = "---" then
		li_count = wf_get_prodotti(ls_cod_prodotto_vendita, ref ls_cod_prodotti)
	else
		ls_cod_prodotti[1] = ls_cod_prodotto_vendita
	end if

	// Ciclo tutti i prodotti nell'array ls_cod_prodotti che può contenere un solo prodotto oppure più prodotti (dimensioni)
	li_lenght = upperbound(ls_cod_prodotti)
	for li_prodotto = 1 to li_lenght
		
		ls_cod_prodotto_vendita = ls_cod_prodotti[li_prodotto]
		
		if wf_nomenclatura_valida(ls_cod_prodotto_vendita) = false then
			continue
		end if
				
		pcca.mdi_frame.setmicrohelp("Analisi prodotto " + as_cod_prodotto_listino + " - " + ls_cod_prodotto_vendita + " in corso...")
		yield()
		
	
		// ## DESCRIZIONI
		select des_prodotto, cod_misura_ven, cod_misura_ven, fat_conversione_ven, cod_misura_mag
		into :ls_des_prodotto_vendita, :ls_cod_misura_ven, :ls_cod_misura_ven, :ld_fat_conversione_ven, :ls_cod_misura_mag
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_vendita;
				 
				 
		ls_des_misura_lingua = ls_cod_misura_ven
		
		if is_cod_lingua <> "ITA" then 
			wf_traduci_des_prodotto(ls_cod_prodotto_vendita, is_cod_lingua, ref ls_des_prodotto_vendita)
			
			select des_misura
			into	:ls_des_misura_lingua
			from	tab_misure_lingue
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_misura = :ls_cod_misura_ven and
						cod_lingua = :is_cod_lingua;
			
			if sqlca.sqlcode <> 0 then ls_des_misura_lingua = ls_cod_misura_ven
		end if	
		// ## FINE DESCRIZIONI
				 
		ll_row = tab_1.report.dw_report.insertrow(0)
		tab_1.report.dw_report.setitem(ll_row, "des_prodotto_listino", ls_des_prodotto_listino)
		tab_1.report.dw_report.setitem(ll_row, "des_prodotto", ls_des_prodotto)
		tab_1.report.dw_report.setitem(ll_row, "cod_prodotto_listino", as_cod_prodotto_listino)
		tab_1.report.dw_report.setitem(ll_row, "livello_gruppo", ll_livello_gruppo)
		tab_1.report.dw_report.setitem(ll_row, "num_posizione", ll_num_posizione)
		tab_1.report.dw_report.setitem(ll_row, "cod_prodotto_vendita", ls_cod_prodotto_vendita)
		tab_1.report.dw_report.setitem(ll_row, "des_prodotto_vendita", ls_des_prodotto_vendita)
		tab_1.report.dw_report.setitem(ll_row, "cod_misura_ven", ls_des_misura_lingua)
		tab_1.report.dw_report.setitem(ll_row, "prog_stampa", ll_prog_stampa)
		tab_1.report.dw_report.setitem(ll_row, "nota_testata", ls_nota_testata)
		tab_1.report.dw_report.setitem(ll_row, "nota_piede", ls_nota_piede)
				
		int li_conteggio_verniciature = 0
		for li_i = 1 to lds_verniciature.rowcount()
			li_pos = lds_verniciature.getitemnumber(li_i, 1)
			ls_column_value = tab_1.report.dw_report.getitemstring(ll_row, "sigla_vern_" + string(li_pos))
			
			if not isnull(ls_column_value) and ls_column_value <> "" then
				// stefanop: 25/07/2014: richiesto da Elisa Berti - andare a capo su due righe
				if mod(li_conteggio_verniciature, 2) = 0 then
					ls_column_value += " " +  lds_verniciature.getitemstring(li_i, 2)
				else
					ls_column_value += COLUMN_SEP +  lds_verniciature.getitemstring(li_i, 2)
				end if
				// --
			else
				ls_column_value = lds_verniciature.getitemstring(li_i, 2)
				li_conteggio_verniciature = 0
			end if
			
			li_conteggio_verniciature++
			
			tab_1.report.dw_report.setitem(ll_row, "sigla_vern_" + string(li_pos), ls_column_value)
			
			if ib_report_excel then
				iuo_excel.uof_set_bold(ii_row_offet + 2, wf_get_excel_column_vern(li_pos))
				iuo_excel.uof_set(ii_row_offet + 2, wf_get_excel_column_vern(li_pos), ls_column_value)
			end if
		next
		
		if ib_report_excel then
			
			ll_row_excel ++
			iuo_excel.uof_set(ll_row_excel, COLONNA_POSIZIONE, ll_num_posizione)
			iuo_excel.uof_set(ll_row_excel, COLONNA_CODICE, ls_cod_prodotto_vendita)
			iuo_excel.uof_set(ll_row_excel, COLONNA_DESCRIZIONE, ls_des_prodotto_vendita)
			iuo_excel.uof_set(ll_row_excel, COLONNA_UM, ls_des_misura_lingua)
			
			iuo_excel.uof_set_autofit(ll_row_excel, COLONNA_CODICE)
			iuo_excel.uof_set_autofit(ll_row_excel, COLONNA_DESCRIZIONE)
			iuo_excel.uof_set_autofit(ll_row_excel, COLONNA_UM)
			
		end if
		
		// ciclo i codici verniciatura della testa e verifico l'esistenza del prodotto
		for li_i = 1 to lds_verniciature.rowcount()
						
			li_pos = lds_verniciature.getitemnumber(li_i, 1)			
			ls_cod_prodotto_vern = left(ls_cod_prodotto_vendita, 5) + lds_verniciature.getitemstring(li_i, 2) + mid(ls_cod_prodotto_vendita, 7)
			
			// Verifico se il prodotto esiste e non è bloccato
			select flag_blocco, cod_nomenclatura
			into :ls_flag_blocco, :ls_cod_nomenclatura
			from anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_vern;
					 
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il controllo dell'esistenza del prodotto", sqlca)
				return -1
			elseif sqlca.sqlcode = 100 or ls_flag_blocco = "S" then
				setnull(ld_prezzo_vendita)
				ls_column_value = wf_get_column_value(ll_row, li_pos, ld_prezzo_vendita, COLUMN_VUOTA)
				tab_1.report.dw_report.setitem(ll_row, "vern_" + string(li_pos), ls_column_value)
				continue
			elseif not isnull(ls_cod_nomenclatura) and ls_cod_nomenclatura <> is_cod_lingua_prodotti then
				// stefanop: 10/03/2015: Richiesto da Beatrice di controllare le nomenclature anche nelle verniciature
				setnull(ld_prezzo_vendita)
				ls_column_value = wf_get_column_value(ll_row, li_pos, ld_prezzo_vendita, COLUMN_VUOTA)
				tab_1.report.dw_report.setitem(ll_row, "vern_" + string(li_pos), ls_column_value)
				continue
				// ----
			end if
			// ----
			
			
			// ## PREZZI
			iuo_condizioni_cliente.ib_setitem = false
			iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto_vern
			iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione_ven
			iuo_condizioni_cliente.ib_setitem = false
			iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = is_cod_tipo_listino
			iuo_condizioni_cliente.str_parametri.cod_valuta = is_cod_valuta
			iuo_condizioni_cliente.str_parametri.cambio_ven = id_cambio_ven
			iuo_condizioni_cliente.str_parametri.data_riferimento = idt_data_riferimento
			iuo_condizioni_cliente.str_parametri.cod_cliente = is_cod_cliente
			iuo_condizioni_cliente.str_parametri.dim_1 = 0
			iuo_condizioni_cliente.str_parametri.dim_2 = 0
			iuo_condizioni_cliente.str_parametri.quantita = ii_quantita
			iuo_condizioni_cliente.str_parametri.valore = 0
			iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_null
			iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_null
			
			iuo_condizioni_cliente.ib_setitem_provvigioni = false
			iuo_condizioni_cliente.wf_condizioni_cliente()
			
			if upperbound(iuo_condizioni_cliente.str_output.variazioni) > 0 then
				ld_prezzo_vendita = iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
				
				for ll_i = 1 to upperbound(iuo_condizioni_cliente.str_output.sconti)
					if iuo_condizioni_cliente.str_output.sconti[ll_i] > 0 then
						ld_prezzo_vendita = ld_prezzo_vendita - ( ( ld_prezzo_vendita * iuo_condizioni_cliente.str_output.sconti[ll_i] ) / 100 )
					else
						exit
					end if
				next

				if iuo_condizioni_cliente.uof_arrotonda_prezzo( ld_prezzo_vendita, is_cod_valuta, ref ld_prezzo_vendita, ls_messaggio) = -1 then
					g_mb.error("Errore in fase di arrotondamento prezzo vendita. " + ls_messaggio)
					rollback;
					return -1
				end if
			else
				setnull(ld_prezzo_vendita)
			end if
				
			if ls_cod_misura_ven <> ls_cod_misura_mag then
				if ld_fat_conversione_ven <> 1 and not isnull(ld_prezzo_vendita) then
					ld_prezzo_vendita = round(ld_prezzo_vendita / ld_fat_conversione_ven, 4)
				end if
			end if
						
			if ld_prezzo_vendita > 0 then
				ls_formato =  "###,##0.00"
				
			elseif ld_prezzo_vendita <= 0 and ls_flag_no_visualizza_zero = "S" then
				//ls_formato = "#"
				ls_formato = COLUMN_VUOTA
			else
				ls_formato = "0"
				
			end if
			// ## FINE PREZZI
			
			ls_column_value = wf_get_column_value(ll_row, li_pos, ld_prezzo_vendita, ls_formato)
			
			/*ls_column_value = tab_1.report.dw_report.getitemstring(ll_row, "vern_" + string(li_pos))
	
			// La colonna è vuota, non ha nessun valore precedentemente inserito
			if not isnull(ls_column_value) and ls_column_value <> "" and ls_column_value <> "#" and ls_column_value <> COLUMN_VUOTA then
				ld_prezzo_vendita_col = dec(tab_1.report.dw_report.getitemstring(ll_row, "vern_" + string(li_pos)))
				
				// nella colonna c'è già un prezzo e sto cercando di inserine un'altro
				if ld_prezzo_vendita_col <> ld_prezzo_vendita then
					ls_column_value = "#"
				end if
				
			// la colonna è vuota ed il prezzo non esiste
			elseif (isnull(ls_column_value) or ls_column_value = "" ) and isnull(ld_prezzo_vendita) then
				ls_column_value = COLUMN_VUOTA
				
			else
				ls_column_value = string(ld_prezzo_vendita, ls_formato)
			end if
			*/
			
			//tab_1.report.dw_report.setitem(ll_row, "vern_" + string(li_pos), string(ld_prezzo_vendita, ls_formato))
			tab_1.report.dw_report.setitem(ll_row, "vern_" + string(li_pos), ls_column_value)
			
			if ib_report_excel then
				if ls_column_value <> "#" then
					
					//scrivi su cella excel solo se si tratta di valore positivo
					//oppure, se zero, solo se espressamente richiesto
					if (ld_prezzo_vendita<=0 and ls_flag_no_visualizza_zero <> "S") or ld_prezzo_vendita>0 then
						iuo_excel.uof_set(ll_row_excel, wf_get_excel_column_vern(li_pos), ld_prezzo_vendita)
						iuo_excel.uof_set_format(ll_row_excel, wf_get_excel_column_vern(li_pos), iuo_excel.NUMBER_PATTERN)
					end if
					
				else
					iuo_excel.uof_set(ll_row_excel, wf_get_excel_column_vern(li_pos), ls_column_value)
				end if
			end if
		next
		
	next

loop

close cu_det_list_ven;

//in ll_row_excel c'è il valore dell'ultima riga scritta
if ib_report_excel then
	wf_crea_pie_pagina_excel(as_cod_prodotto_listino, ll_row_excel, ls_nota_piede)
end if

return 0
end function

public subroutine wf_lingua_cliente (string as_cod_cliente);string ls_cod_lingua

select cod_lingua
into :ls_cod_lingua
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :as_cod_cliente;
		 
if sqlca.sqlcode <> 0 and not isnull(ls_cod_lingua) and ls_cod_lingua <> "" then
	tab_1.selezione.dw_selezione.setitem(1, "cod_lingua", ls_cod_lingua)
end if
end subroutine

public subroutine wf_traduci_des_prodotto (string as_cod_prodotto, string as_cod_lingua, ref string as_des_prodotto);/**
 **/
 
string ls_des_prodotto_lingua

select des_prodotto
into :ls_des_prodotto_lingua
from anag_prodotti_lingue
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto and
		 cod_lingua = :as_cod_lingua;
		 
// cambio la lingua solo se è valida
if sqlca.sqlcode = 0 and not isnull(ls_des_prodotto_lingua) and ls_des_prodotto_lingua <> "" then
	as_des_prodotto = ls_des_prodotto_lingua
end if
end subroutine

public function integer wf_esporta_excel ();/**
 * stefanop
 * 18/06/2012
 *
 * Esporta la DW in un Excel e ne salva il percorso nella variabile di istanza is_temp_file
 **/

 int			li_ret
 string		ls_path, ls_nome
 
is_temp_file =guo_functions.uof_get_user_desktop_folder()
ls_path = is_temp_file + "Listino Vendita " + string(today(), "dd-mm-yy") + ".xlsx"

li_ret = GetFileSaveName("Selezionare percorso e nome file XLSX", ls_path, ls_nome, "xlsx", "XLSX (*.xlsx),*.xlsx", ls_path)

IF li_ret <> 1 THEN 
	return -1
end if

is_temp_file = ls_path

wf_report(true)

return 1
end function

public function boolean wf_esporta_pdf ();/**
 * stefanop
 * 18/06/2012
 *
 * Esporta la DW in un PDF e ne salva il percorso nella variabile di istanza is_temp_file
 **/
 integer		li_ret
 string		ls_path, ls_nome
 
is_temp_file = guo_functions.uof_get_user_desktop_folder()

//is_temp_file += "Listino Vendita " + string(today(), "dd-mm-yyyy") + ".pdf"

ls_path = is_temp_file + "Listino Vendita " + string(today(), "dd-mm-yy") + ".pdf"

li_ret = GetFileSaveName("Selezionare percorso e nome file PDF", ls_path, ls_nome, "pdf", "PDF (*.pdf),*.pdf", ls_path)
//li_rc = GetFileSaveName ( "File LOG Errori",  ls_path, ls_file, "Txt", "File Txt (*.Txt),*.Txt" , "C:\temp")

IF li_ret <> 1 THEN 
	return false
end if

is_temp_file = ls_path

//return tab_1.report.dw_report.saveas(is_temp_file, PDF!, true) = 1
return tab_1.report.dw_report.saveas(ls_path, PDF!, true) = 1
end function

public function integer wf_report (boolean ab_report_excel);/**
 * stefanop
 * 13/06/2012
 *
 * Eseguo report
 **/
 
string ls_null, ls_cod_prodotti[], ls_dataobject
long ll_i

setpointer(Hourglass!)

ib_report_excel = ab_report_excel
tab_1.selezione.dw_selezione.accepttext()
tab_1.report.dw_report.setredraw(false)
tab_1.report.dw_report.reset()

// EnMe 04-04-13 forzo il dataobject per resettare le eventuali descrizioni in lingua delle label impostate (vedi sotto)
ls_dataobject = tab_1.report.dw_report.dataobject
tab_1.report.dw_report.dataobject = ls_dataobject


setnull(ls_null)

// ## FILTRI
is_cod_tipo_listino = tab_1.selezione.dw_selezione.getitemstring(1, "cod_tipo_listino_prodotto")
is_cod_valuta = tab_1.selezione.dw_selezione.getitemstring(1, "cod_valuta")
is_cod_cliente = tab_1.selezione.dw_selezione.getitemstring(1, "cod_cliente")
is_cod_lingua = tab_1.selezione.dw_selezione.getitemstring(1, "cod_lingua")
ii_quantita = tab_1.selezione.dw_selezione.getitemnumber(1, "quantita")
idt_data_riferimento = tab_1.selezione.dw_selezione.getitemdatetime(1, "data_riferimento")
is_cod_lingua_prodotti = tab_1.selezione.dw_selezione.getitemstring(1, "cod_lingua_prodotti")
is_prefix_nota_fissa_testata = ""
is_prefix_nota_fissa_piede = ""
id_cambio_ven = f_cambio_val_ven(is_cod_valuta, idt_data_riferimento)


if isnull(is_cod_tipo_listino) or is_cod_tipo_listino="" then
	setpointer(Arrow!)
	g_mb.warning("Selezionare il Tipo Listino prima di lanciare il report!")
	return -1
end if

if isnull(is_cod_valuta) or is_cod_valuta="" then
	setpointer(Arrow!)
	g_mb.warning("Selezionare la Valuta prima di lanciare il report!")
	return -1
end if

if ii_quantita < 1 then ii_quantita = 1
if isnull(is_cod_lingua) or is_cod_lingua = "" then is_cod_lingua = "ITA"

// Preparo le note fisse
wf_note_fisse(is_cod_cliente)

// ## SE EXCEL PREPARO IL FILE
if ib_report_excel then
	iuo_excel = create uo_excel
	iuo_excel.uof_create(is_temp_file, false)
end if


// ## REPORT

tab_1.selezione.dw_prodotti.setsort("#5 A")
tab_1.selezione.dw_prodotti.sort()

tab_1.selezione.dw_prodotti.uof_get_selected_column_as_array("tes_list_ven_cod_prodotto_listino",ref ls_cod_prodotti)
if upperbound(ls_cod_prodotti) > 0 then
	
	for ll_i = 1 to upperbound(ls_cod_prodotti)
	
		if wf_report_prodotto(ls_cod_prodotti[ll_i], 1) < 0 then
			
			exit
			
		end if
		
	next
		
else
	setpointer(Arrow!)
	g_mb.warning("Selezionare almeno un prodotto prima di lanciare il report!")
	return -1
end if


tab_1.report.dw_report.object.p_logo.filename = s_cs_xx.volume + is_logo_lo7

tab_1.report.dw_report.groupcalc()
tab_1.report.dw_report.setredraw(true)
tab_1.report.dw_report.uof_aggiorna_text( is_cod_lingua )

setpointer(Arrow!)

if ib_report_excel then
	
	//iuo_excel.uof_saveas( is_temp_file)
	destroy iuo_excel
	
	g_mb.success("Esportazione excel eseguita con successo!")
else
	tab_1.selecttab(2)
end if

tab_1.selezione.dw_prodotti.setsort("#2 A, #5 A")
tab_1.selezione.dw_prodotti.sort()

pcca.mdi_frame.setmicrohelp("Analisi completata")
 
return 1
end function

public function integer wf_get_prodotti (string as_cod_prodotto, ref string as_cod_prodotti[]);/**
 * stefanop
 * 14/06/2012
 *
 * Recupero tutte le dimensioni del prodotto.
 * Ad esempio A24591---
 **/
 
string ls_sql, ls_cod_prod_like, ls_empty[]
int li_count, li_i
datastore lds_store

as_cod_prodotti = ls_empty

// A24591--- => A24591%
ls_cod_prod_like = mid(as_cod_prodotto, 1, len(as_cod_prodotto) - 3) + "%"

ls_sql = "SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_prodotto like '" + ls_cod_prod_like + "%' " + &
		 	" AND flag_blocco <> 'S' AND cod_prodotto <> '" + as_cod_prodotto + "' ORDER BY right(cod_prodotto, 3) ASC"

li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for li_i = 1 to li_count
	as_cod_prodotti[li_i] = lds_store.getitemstring(li_i, 1)
next

destroy lds_store

return li_count
end function

public function integer wf_get_verniciature_testata (string as_cod_prodotto_listino, integer ai_cod_versione, ref datastore ads_store);/**
 * stefanop
 * 20/6/2012
 *
 * Recupero i codici delle verniciature da recuperare; sono impostate nella testata della tes_list_ven_vern
 * Uso il datastore perchè protei avere più verniciatura in una stessa posizione.
 * 
 * Il datastore avrà le seguenti colonne
 * 1. Numero posizione
 * 2. Codice breve verniciatura
 **/
 
 
string ls_sql

ls_sql = "SELECT tes_list_ven_vern.num_posizione_griglia, tab_verniciatura.cod_veloce " + &
			"FROM tes_list_ven_vern " + &
			"	JOIN tab_verniciatura ON " + &
			"	tab_verniciatura.cod_azienda = tes_list_ven_vern.cod_azienda AND " + &
			"	tab_verniciatura.cod_verniciatura = tes_list_ven_vern.cod_verniciatura " +  &
			" WHERE tes_list_ven_vern.cod_azienda='" + s_cs_xx.cod_azienda + "'  AND "+ &
			"	cod_prodotto_listino='" + as_cod_prodotto_listino + "' AND " + &
			"	cod_versione=" + string(ai_cod_versione) + &
			" ORDER BY num_posizione_griglia"
			
return guo_functions.uof_crea_datastore(ads_store, ls_sql)
end function

public function string wf_get_excel_column_vern (integer ai_pos_verniciatura);choose case ai_pos_verniciatura
	case 1
		return COLONNA_VERN_1
	case 2
		return COLONNA_VERN_2
	case 3
		return COLONNA_VERN_3
	case 4
		return COLONNA_VERN_4
	case 5
		return COLONNA_VERN_5
	case 6
		return COLONNA_VERN_6
	case 7
		return COLONNA_VERN_7
	case 8
		return COLONNA_VERN_8
	case 9
		return COLONNA_VERN_9
end choose
end function

public subroutine wf_traduci_listino (string as_cod_prodotto_listino, long al_cod_versione, string as_cod_lingua, ref string as_des_prodotto_listino, ref string as_nota_testata, ref string as_nota_piede);
string ls_descrizione, ls_flag_tipo_campo
string ls_sql, ls_errore
datastore lds_data
long ll_index, ll_tot

ls_sql =  "select descrizione_lingua, flag_tipo_campo "+&
					"from tes_list_ven_lingue "+&
					"where cod_azienda = '"+s_cs_xx.cod_azienda+" ' and "+&
							 "cod_prodotto_listino = '"+as_cod_prodotto_listino+"' and "+&
							 "cod_versione = "+string(al_cod_versione)+" and "+&
							 "cod_lingua = '"+as_cod_lingua+"' and "+&
							 "descrizione_lingua<>'' and descrizione_lingua is not null "

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
for ll_index=1 to ll_tot
	choose case lds_data.getitemstring(ll_index, 2)
		case "D"
			//des_prodotto_listino tradotto
			as_des_prodotto_listino = lds_data.getitemstring(ll_index, 1)
			
		case "T"
			//nota_testata tradotta
			as_nota_testata = lds_data.getitemstring(ll_index, 1)
			
		case "P"
			//nota_piede tradotta
			as_nota_piede = lds_data.getitemstring(ll_index, 1)
			
	end choose
next
end subroutine

private subroutine wf_crea_intestazione_excel (string as_cod_prodotto, string as_note_testata);/**
 * stefanop
 * 18/06/2012
 *
 * Creo intestazione foglio excel
 **/
 
long ll_row 

iuo_excel.uof_add_sheet(as_cod_prodotto, true)

if isnull(as_note_testata) then as_note_testata = ""
iuo_excel.uof_merge(1, COLONNA_POSIZIONE, 1, COLONNA_VERN_9)
iuo_excel.uof_set_italic(1, COLONNA_POSIZIONE)
iuo_excel.uof_set(1, COLONNA_POSIZIONE, as_note_testata)

ll_row = ii_row_offet + 1

iuo_excel.uof_merge(ll_row, COLONNA_POSIZIONE, ll_row + 1, COLONNA_POSIZIONE)
iuo_excel.uof_merge(ll_row, COLONNA_CODICE, ll_row + 1, COLONNA_CODICE)
iuo_excel.uof_merge(ll_row, COLONNA_DESCRIZIONE, ll_row + 1, COLONNA_DESCRIZIONE)
iuo_excel.uof_merge(ll_row, COLONNA_UM, ll_row + 1, COLONNA_UM)
iuo_excel.uof_merge(ll_row, COLONNA_VERN_1, ll_row, COLONNA_VERN_9)

iuo_excel.uof_set_bold(ll_row, COLONNA_POSIZIONE)
iuo_excel.uof_set_bold(ll_row, COLONNA_CODICE)
iuo_excel.uof_set_bold(ll_row, COLONNA_DESCRIZIONE)
iuo_excel.uof_set_bold(ll_row, COLONNA_UM)
iuo_excel.uof_set_bold(ll_row, COLONNA_VERN_1)

iuo_excel.uof_set(ll_row, COLONNA_POSIZIONE, "POS")
iuo_excel.uof_set(ll_row, COLONNA_CODICE, "CODICE")
iuo_excel.uof_set(ll_row, COLONNA_DESCRIZIONE, "DESCRIZIONE")
iuo_excel.uof_set(ll_row, COLONNA_UM, "U.M")
iuo_excel.uof_set(ll_row, COLONNA_VERN_1, "VERNICIATURA")


// Text align
iuo_excel.uof_set_horizontal_align(0, COLONNA_POSIZIONE, iuo_excel.TEXT_ALIGN_CENTER)
iuo_excel.uof_set_horizontal_align(ll_row, COLONNA_DESCRIZIONE, iuo_excel.TEXT_ALIGN_CENTER)
iuo_excel.uof_set_horizontal_align(0, COLONNA_UM, iuo_excel.TEXT_ALIGN_CENTER)

iuo_excel.uof_set_vertical_align(ll_row, 0, iuo_excel.TEXT_ALIGN_MIDDLE)
iuo_excel.uof_set_vertical_align(ll_row + 1, 0, iuo_excel.TEXT_ALIGN_MIDDLE)


// indicata quante righe occupa l'intestazione
ii_row_offet_header = 2


end subroutine

private subroutine wf_crea_pie_pagina_excel (string as_cod_prodotto, long al_row_excel, string as_note_piede); 
long ll_row 

al_row_excel = al_row_excel + 1


if isnull(as_note_piede) then as_note_piede = ""
iuo_excel.uof_merge(al_row_excel, COLONNA_POSIZIONE, al_row_excel, COLONNA_VERN_9)
iuo_excel.uof_set_italic(al_row_excel, COLONNA_POSIZIONE)
iuo_excel.uof_set(al_row_excel, COLONNA_POSIZIONE, as_note_piede)

iuo_excel.uof_set_horizontal_align(al_row_excel, COLONNA_POSIZIONE, iuo_excel.text_align_left)

end subroutine

public function boolean wf_nomenclatura_valida (string as_cod_prodotto);/**
 * stefanop
 * 05/12/2012
 * 
 * Specifica Migliorie listini
 * Controllo il flag lingua del prodotto
 **/
 
string ls_cod_nomenclatura
 
if not isnull(is_cod_lingua_prodotti)then
	select cod_nomenclatura
	into :ls_cod_nomenclatura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo del codice nomenclatura del prodotto " + as_cod_prodotto, sqlca)
		return false
	elseif sqlca.sqlcode = 100 then
		g_mb.warning("Attenzione, il prodotto " + as_cod_prodotto + " non è stato trovato nel database")
		return false
	elseif not isnull(ls_cod_nomenclatura) and ls_cod_nomenclatura <> is_cod_lingua_prodotti then
		// La nomenclatura NON deve essere diversa da quella selezionata dall'utente nel filtro.
		// Se è vuota invece va bene
		return false
	else
		// la nomenclatura è nulla quindi mi va più che bene
		return true
	end if
	
else
	return true
end if
end function

public subroutine wf_note_fisse (string as_cod_cliente);/** 
 * stefanop
 * 05/11/2012
 *
 * Calcolo le note fisse per i clienti
 **/
 
string ls_sql, ls_cod_nota_fissa, ls_flag_piede_testata, ls_nota, ls_nota_formattata, ls_descrizione_lingua
int li_i, li_count
datastore lds_store
uo_string_replace luo_replace
 
is_prefix_nota_fissa_testata = ""
is_prefix_nota_fissa_piede = ""
	
if not isnull(as_cod_cliente) and as_cod_cliente <> "" then
	luo_replace = create uo_string_replace
	
	ls_sql = "SELECT cod_nota_fissa, nota_fissa, flag_piede_testata FROM tab_note_fisse WHERE cod_azienda='$1' AND flag_rep_list_ven='S'"
	li_count = guo_functions.uof_crea_datastore(lds_store, g_str.format(ls_sql, s_cs_xx.cod_azienda))
	
	for li_i = 1 to li_count
		
		ls_cod_nota_fissa =  lds_store.getitemstring(li_i, "cod_nota_fissa")
		ls_flag_piede_testata = lds_store.getitemstring(li_i, "flag_piede_testata")
		ls_nota = lds_store.getitemstring(li_i, "nota_fissa")
		
		// Serve la traduzione?
		if is_cod_lingua <> "ITA" then
			
			select descrizione_lingua
			into :ls_descrizione_lingua
			from tab_note_fisse_lingue
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_nota_fissa = :ls_cod_nota_fissa and
					 cod_lingua = :is_cod_lingua;
					 
			if sqlca.sqlcode = 0 and not isnull(ls_descrizione_lingua) and ls_descrizione_lingua <> "" then
				
				ls_nota = ls_descrizione_lingua
				
			end if
			
		end if
		// ---
		
		if isnull(ls_nota) or ls_nota = "" then continue
		
		ls_nota_formattata = luo_replace.uof_replace(ls_nota, "anag_clienti", g_str.format("cod_azienda='$1' and cod_cliente='$2'", s_cs_xx.cod_azienda, is_cod_cliente))
		
		if isnull(ls_nota_formattata) or ls_nota_formattata = "" then continue
		
		if ls_flag_piede_testata = "T" then
			// Testata
			is_prefix_nota_fissa_testata += ls_nota_formattata + "~r~n"
			
		elseif ls_flag_piede_testata = "P" then
			
			// piede
			is_prefix_nota_fissa_piede += ls_nota_formattata + "~r~n"
		end if
		
	next
	
	destroy luo_replace
	
end if

end subroutine

public function string wf_get_column_value (long al_row, integer ai_vern_pos, decimal ad_prezzo_vendita, string as_formato);/**
 * stefanop
 * 12/12/2012
 *
 * Controllo cosa devo visualizzare se nella stessa colonna delle verniciatura ci sono due valori da visualizzare
 **/
 
string ls_column_value
decimal ld_prezzo_vendita_col

ls_column_value = tab_1.report.dw_report.getitemstring(al_row, "vern_" + string(ai_vern_pos))

// La colonna è vuota, non ha nessun valore precedentemente inserito
if not isnull(ls_column_value) and ls_column_value <> "" and ls_column_value <> "#" and ls_column_value <> COLUMN_VUOTA then
	ld_prezzo_vendita_col = dec(ls_column_value)
	
	// nella colonna c'è già un prezzo e sto cercando di inserine un'altro
	if ld_prezzo_vendita_col <> ad_prezzo_vendita then
		ls_column_value = "#"
	end if
	
// la colonna è vuota ed il prezzo non esiste
elseif (isnull(ls_column_value) or ls_column_value = "" ) and isnull(ad_prezzo_vendita) then
	ls_column_value = COLUMN_VUOTA
	
elseif not isnull(ad_prezzo_vendita) then
	
	ls_column_value = string(ad_prezzo_vendita, as_formato)
end if

return ls_column_value
end function

on w_report_list_ven.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_report_list_ven.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql

guo_functions.uof_get_parametro_azienda("LO7", is_logo_lo7)

tab_1.move(20,20)
tab_1.selezione.dw_selezione.move(0,0)
tab_1.report.dw_report.move(0,0)

tab_1.selezione.dw_selezione.insertrow(0)
tab_1.selezione.dw_selezione.setitem(1, "data_riferimento", datetime(today(), 00:00:00))
tab_1.report.dw_report.ib_dw_report = true
tab_1.report.dw_report.object.datawindow.print.preview = 'yes'

tab_1.selecttab(1)

iuo_condizioni_cliente = create uo_condizioni_cliente

ls_sql = 	"SELECT b.cod_prodotto_listino as cod_prodotto_listino, p.des_prodotto as des_prodotto, b.cod_versione, b.prog_ordine_stampa " + &
			"FROM tes_list_ven as b "+&
			"JOIN (SELECT distinct cod_prodotto_listino, max(cod_versione) as cod_versione "+&
						"FROM tes_list_ven " + &
						"WHERE cod_azienda='A01' AND flag_versione_attiva='S' "+&
						"GROUP BY cod_prodotto_listino " + &
					") as a on a.cod_prodotto_listino = b.cod_prodotto_listino AND a.cod_versione = b.cod_versione "+ &
			"JOIN anag_prodotti as p ON p.cod_azienda = b.cod_azienda AND p.cod_prodotto = b.cod_prodotto_listino " + &
			"ORDER BY b.cod_prodotto_listino, b.prog_ordine_stampa"
	
tab_1.selezione.dw_prodotti.uof_set_column_by_sql(ls_sql)
tab_1.selezione.dw_prodotti.uof_set_parent_dw(tab_1.selezione.dw_selezione, "cod_prodotti", "tes_list_ven_cod_prodotto_listino")

tab_1.selezione.dw_prodotti.uof_set_column_width(1, 200)
tab_1.selezione.dw_prodotti.uof_set_column_width(2, 800)
tab_1.selezione.dw_prodotti.uof_set_column_width(4, 200)

tab_1.selezione.dw_prodotti.uof_set_column_name(1, "Cod.")
tab_1.selezione.dw_prodotti.uof_set_column_name(2, "Des. Prodotto")
tab_1.selezione.dw_prodotti.uof_set_column_name(3, "Vers.")
tab_1.selezione.dw_prodotti.uof_set_column_name(4, "Stampa")

tab_1.selezione.dw_prodotti.uof_set_column_align(1, "center")
tab_1.selezione.dw_prodotti.uof_set_column_align(3, "center")
tab_1.selezione.dw_prodotti.uof_set_column_align(4, "center")


end event

event resize;tab_1.resize(newwidth -40, newheight - 40)

tab_1.event ue_resize()
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.selezione.dw_selezione, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))")


f_po_loaddddw_dw(tab_1.selezione.dw_selezione, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
					  
f_po_loaddddw_dw(tab_1.selezione.dw_selezione, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
					  
f_po_loaddddw_dw(tab_1.selezione.dw_selezione, &
                 "cod_lingua_prodotti", &
                 sqlca, &
                 "tab_nomenclature", &
                 "cod_nomenclatura", &
                 "des_nomenclatura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event close;call super::close;destroy iuo_condizioni_cliente
end event

type tab_1 from tab within w_report_list_ven
event ue_resize ( )
event create ( )
event destroy ( )
integer x = 23
integer y = 24
integer width = 3794
integer height = 1820
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
selezione selezione
report report
esporta esporta
end type

event ue_resize();report.dw_report.resize(report.width, report.height)
end event

on tab_1.create
this.selezione=create selezione
this.report=create report
this.esporta=create esporta
this.Control[]={this.selezione,&
this.report,&
this.esporta}
end on

on tab_1.destroy
destroy(this.selezione)
destroy(this.report)
destroy(this.esporta)
end on

type selezione from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3758
integer height = 1696
long backcolor = 12632256
string text = "1. Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_prodotti dw_prodotti
cb_1 cb_1
dw_selezione dw_selezione
end type

on selezione.create
this.dw_prodotti=create dw_prodotti
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_prodotti,&
this.cb_1,&
this.dw_selezione}
end on

on selezione.destroy
destroy(this.dw_prodotti)
destroy(this.cb_1)
destroy(this.dw_selezione)
end on

type dw_prodotti from uo_dddw_checkbox within selezione
integer x = 521
integer y = 752
integer width = 2011
integer height = 824
integer taborder = 50
integer ii_width_percent = 0
end type

type cb_1 from commandbutton within selezione
integer x = 1787
integer y = 608
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;wf_report(false)
end event

type dw_selezione from uo_std_dw within selezione
integer x = 5
integer y = 8
integer width = 2537
integer height = 740
integer taborder = 40
string dataobject = "d_report_list_ven_selezione"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_1.selezione.dw_selezione, "cod_cliente")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_cod_lingua

choose case dwo.name
	case "cod_prodotti"
		//tab_1.selezione.dw_prodotti.event post uoe_read_from_parent()
		
	case "cod_cliente"
		if not isnull(data) and data <> "" then
			
			wf_lingua_cliente(data)
			
		end if
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name
	case "cod_prodotti"
		tab_1.selezione.dw_prodotti.show()
		
end choose
end event

type report from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3758
integer height = 1696
long backcolor = 12632256
string text = "2. Report"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_report dw_report
end type

on report.create
this.dw_report=create dw_report
this.Control[]={this.dw_report}
end on

on report.destroy
destroy(this.dw_report)
end on

type dw_report from uo_std_dw within report
integer x = 50
integer y = 208
integer width = 3657
integer height = 1440
integer taborder = 30
string dataobject = "d_report_list_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

type esporta from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3758
integer height = 1696
long backcolor = 12632256
string text = "3. Esportazione  - Invio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_info_1 st_info_1
cb_3 cb_3
cb_2 cb_2
end type

on esporta.create
this.st_info_1=create st_info_1
this.cb_3=create cb_3
this.cb_2=create cb_2
this.Control[]={this.st_info_1,&
this.cb_3,&
this.cb_2}
end on

on esporta.destroy
destroy(this.st_info_1)
destroy(this.cb_3)
destroy(this.cb_2)
end on

type st_info_1 from statictext within esporta
integer x = 50
integer y = 52
integer width = 1531
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "L~'esportazione dei documenti salva il file nel proprio desktop."
boolean focusrectangle = false
end type

type cb_3 from commandbutton within esporta
integer x = 73
integer y = 192
integer width = 503
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta Excel"
end type

event clicked;wf_esporta_excel()

end event

type cb_2 from commandbutton within esporta
integer x = 1079
integer y = 192
integer width = 503
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta PDF"
end type

event clicked;if wf_esporta_pdf() then
	g_mb.success("Esportazione PDF eseguita con successo!")
	run(is_temp_file)
	
end if
end event

