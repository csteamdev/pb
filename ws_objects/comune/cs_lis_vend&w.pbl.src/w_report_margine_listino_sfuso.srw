﻿$PBExportHeader$w_report_margine_listino_sfuso.srw
forward
global type w_report_margine_listino_sfuso from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_margine_listino_sfuso
end type
type dw_folder from u_folder within w_report_margine_listino_sfuso
end type
type dw_ricerca from u_dw_search within w_report_margine_listino_sfuso
end type
end forward

global type w_report_margine_listino_sfuso from w_cs_xx_principale
integer width = 3822
integer height = 2464
string title = "Report Marginalità Listino"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_report_margine_listino_sfuso w_report_margine_listino_sfuso

type variables


string								is_cod_prod_modello = ""

uo_condizioni_cliente			iuo_condizioni
end variables

forward prototypes
public function integer wf_reset ()
public function integer wf_report (ref string as_errore)
public function double wf_cambio_ven (string as_cod_valuta, datetime adt_data_rif)
public function decimal wf_condizioni (ref datastore ads_data, string as_cod_prodotto, datetime adt_data_rif, string as_cod_tipo_listino, string as_cod_valuta, decimal ad_fat_conv_ven, double ad_data_cambio)
end prototypes

public function integer wf_reset ();string					ls_null


setnull(ls_null)

dw_ricerca.setitem(1, "data_riferimento", datetime(today(), 00:00:00))
dw_ricerca.setitem(1, "cod_tipo_listino", ls_null)
dw_ricerca.setitem(1, "cod_valuta", ls_null)
dw_ricerca.setitem(1, "cod_prodotto_inizio", ls_null)
dw_ricerca.setitem(1, "cod_prodotto_fine", ls_null)
dw_ricerca.setitem(1, "sconto", 0)
dw_ricerca.setitem(1, "minimo", 40)
dw_ricerca.setitem(1, "massimo", 60)

dw_ricerca.setitem(1, "cod_livello_prod_1", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_2", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_3", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_4", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_5", ls_null)

return 0
end function

public function integer wf_report (ref string as_errore);
string						ls_sql, ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_tipo_listino, ls_cod_valuta, ls_cod_prodotto_ds, &
							ls_des_prodotto_ds, ls_tipo_costo, ls_filtro, ls_flag_solo_da_listino, ls_cod_livello_prod_1, ls_cod_livello_prod_2, &
							ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5
							
dec{4}					ld_sconto, ld_massimo, ld_minimo, ld_costo_standard_ds, ld_cambio_ven, ld_prezzo_lordo, ld_prezzo_netto,&
							ld_costo_acquisto, ld_margine
							
dec{2}					ld_perc_marg
dec{5}					ld_fat_conversione_ven_ds

datetime					ldt_data_rif
datastore				lds_listino, lds_data
long						ll_index, ll_tot, ll_new, ll_gg_a_ritroso
uo_trova_prezzi		luo_prezzi


dw_ricerca.accepttext()
dw_report.reset()
ls_filtro = ""

ldt_data_rif = dw_ricerca.getitemdatetime(1, "data_riferimento")
if isnull(ldt_data_rif) or year(date(ldt_data_rif))<1950 then
	as_errore = "La data riferimento è obbligatoria!"
	return 1
end if
ls_filtro += " - Data Rif : "+string(ldt_data_rif, "dd/mm/yyyy")


ls_cod_tipo_listino = dw_ricerca.getitemstring(1, "cod_tipo_listino")
if isnull(ls_cod_tipo_listino) or ls_cod_tipo_listino="" then
	as_errore = "Indicare il tipo listino da utilizzare!"
	return 1
end if
ls_filtro += " - Tipo Listino : " + ls_cod_tipo_listino


ls_cod_valuta = dw_ricerca.getitemstring(1, "cod_valuta")
if isnull(ls_cod_valuta) or ls_cod_valuta="" then
	as_errore = "Indicare la valuta del listino!"
	return 1
end if
ls_filtro += " - Valuta : " + ls_cod_valuta


ls_cod_livello_prod_1 = dw_ricerca.getitemstring(1, "cod_livello_prod_1")
ls_cod_livello_prod_2 = dw_ricerca.getitemstring(1, "cod_livello_prod_2")
ls_cod_livello_prod_3 = dw_ricerca.getitemstring(1, "cod_livello_prod_3")
ls_cod_livello_prod_4 = dw_ricerca.getitemstring(1, "cod_livello_prod_4")
ls_cod_livello_prod_5 = dw_ricerca.getitemstring(1, "cod_livello_prod_5")


ls_cod_prodotto_inizio = dw_ricerca.getitemstring(1, "cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_ricerca.getitemstring(1, "cod_prodotto_fine")

ls_flag_solo_da_listino = dw_ricerca.getitemstring(1, "flag_solo_da_listino")


ld_sconto = dw_ricerca.getitemdecimal(1, "sconto")
if isnull(ld_sconto) then ld_sconto = 0
ls_filtro += " - Sconto pre-impostato : " + string(ld_sconto, "##0.00")


ll_gg_a_ritroso = dw_ricerca.getitemnumber(1, "gg_a_ritroso")
if ll_gg_a_ritroso>0 then
else
	as_errore = "Indicare N° giorni indietro dalla data riferimento per la ricerca dell'ultimo costo acquisto su fattura!"
	return 1
end if

ld_minimo = dw_ricerca.getitemnumber(1, "minimo")
ld_massimo = dw_ricerca.getitemnumber(1, "massimo")

if isnull(ld_minimo) then ld_minimo = 0
if isnull(ld_massimo) then ld_massimo = 0

if ld_massimo<=ld_minimo then
	as_errore = "Il valore del Massimo % deve essere superiore al valore del Minimo % !"
	return 1
end if
ls_filtro += " - Min/Max per colori : " + string(ld_minimo, "##0") + "/" + string(ld_massimo, "##0")


//mi serve un datastore di appoggio per il recupero del prezzo di listino di ogni prodotto
lds_listino = create datastore
lds_listino.dataobject = "d_det_ord_ven_lista"
lds_listino.settransobject(sqlca)


ls_sql = 	"select a.cod_prodotto, a.des_prodotto, a.costo_standard, a.fat_conversione_ven "+&
			"from anag_prodotti as a "

if ls_flag_solo_da_listino="S" then
	ls_sql += "join listini_ven_comune as l on l.cod_azienda=a.cod_azienda and "+&
													"l.cod_prodotto=a.cod_prodotto "
end if

ls_sql +=	"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' "

if ls_flag_solo_da_listino="S" then
	ls_sql += " and l.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino+"' and l.cod_valuta='"+ls_cod_valuta+"' "
end if

if is_cod_prod_modello<>"" then
	ls_sql += " and (a.cod_cat_mer<>'"+is_cod_prod_modello+"' or a.cod_cat_mer is null)"
end if


//################################################
//livelli
if ls_cod_livello_prod_1<>"" and not isnull(ls_cod_livello_prod_1) then
	//-------------------------------------------------------------------------------------------
	ls_sql += "and a.cod_livello_prod_1='"+ls_cod_livello_prod_1+"' "
	
	ls_filtro += " - Livelli ( "+f_des_tabella("tab_livelli_prod_1","cod_livello_prod_1='" +   ls_cod_livello_prod_1  + "'","des_livello")
	//-------------------------------------------------------------------------------------------
	if ls_cod_livello_prod_2<>"" and not isnull(ls_cod_livello_prod_2) then
		ls_sql += "and a.cod_livello_prod_2='"+ls_cod_livello_prod_2+"' "
		
		ls_filtro += ", "+f_des_tabella("tab_livelli_prod_2","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"'","des_livello")
		//----------------------------------------------------------------------------------------
		if ls_cod_livello_prod_3<>"" and not isnull(ls_cod_livello_prod_3) then
			ls_sql += "and a.cod_livello_prod_3='"+ls_cod_livello_prod_3+"' "
			
			ls_filtro += ", "+f_des_tabella("tab_livelli_prod_3","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"'","des_livello")
			//-------------------------------------------------------------------------------------
			if ls_cod_livello_prod_4<>"" and not isnull(ls_cod_livello_prod_4) then
				ls_sql += "and a.cod_livello_prod_4='"+ls_cod_livello_prod_4+"' "
				
				ls_filtro += ", "+f_des_tabella("tab_livelli_prod_4","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"' and "+&
																				 "cod_livello_prod_4='"+ls_cod_livello_prod_4+"'","des_livello")
				//----------------------------------------------------------------------------------
				if ls_cod_livello_prod_5<>"" and not isnull(ls_cod_livello_prod_5) then
					ls_sql += "and a.cod_livello_prod_5='"+ls_cod_livello_prod_5+"' "
					
					ls_filtro += ", "+f_des_tabella("tab_livelli_prod_5","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"' and "+&
																				 "cod_livello_prod_4='"+ls_cod_livello_prod_4+"' and "+&
																				 "cod_livello_prod_5='"+ls_cod_livello_prod_5+"'","des_livello")
				end if
			end if
		end if
	end if
	
	ls_filtro+= " )"
	
end if
//################################################


if ls_cod_prodotto_inizio<>"" and not isnull(ls_cod_prodotto_inizio) then
	ls_sql += " and a.cod_prodotto>='"+ls_cod_prodotto_inizio+"' "
	ls_filtro += " - da Prod. : " + ls_cod_prodotto_inizio
end if

if ls_cod_prodotto_fine<>"" and not isnull(ls_cod_prodotto_fine) then
	ls_sql += " and a.cod_prodotto<='"+ls_cod_prodotto_fine+"' "
	ls_filtro += " - fino a Prod. : " + ls_cod_prodotto_fine
end if

dw_report.object.filtro_t.text = ls_filtro


dw_ricerca.object.t_log.text = "Preparazione dati in corso ..."

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot<0 then
	destroy lds_listino
	return -1
end if

ld_cambio_ven = wf_cambio_ven(ls_cod_valuta, ldt_data_rif)

luo_prezzi = create uo_trova_prezzi

for ll_index=1 to ll_tot
	if mod(ll_index, 10) = 0 then yield()
	
	ls_cod_prodotto_ds				= lds_data.getitemstring(ll_index, 1)
	ls_des_prodotto_ds				= lds_data.getitemstring(ll_index, 2)
	
	ld_costo_standard_ds				= lds_data.getitemnumber(ll_index, 3)
	if isnull(ld_costo_standard_ds) then ld_costo_standard_ds = 0
	
	ld_fat_conversione_ven_ds		= lds_data.getitemnumber(ll_index, 4)
	
	//leggo il prezzo di listino: il prezzo sarà espresso in 			[valuta/UMmag]
	dw_ricerca.object.t_log.text = "(" + string(ll_index) + " di " + string(ll_tot) + ") Elaborazione articolo " + ls_cod_prodotto_ds + " in corso ... (listino vendita)"
	ld_prezzo_lordo = wf_condizioni(lds_listino, ls_cod_prodotto_ds, ldt_data_rif, ls_cod_tipo_listino, ls_cod_valuta, ld_fat_conversione_ven_ds, ld_cambio_ven)
	if ld_sconto > 0 then
		ld_prezzo_netto = ld_prezzo_lordo * (1 - ld_sconto/100)
	else
		ld_prezzo_netto = ld_prezzo_lordo
	end if
	
	ls_tipo_costo = "U"
	
	//valutare ultimo costo acquisto: il prezzo sarà espresso in 			[valuta/UMmag]
	dw_ricerca.object.t_log.text = "(" + string(ll_index) + " di " + string(ll_tot) + ") Elaborazione articolo " + ls_cod_prodotto_ds + " in corso ... (ultimo costo acquisto)"
	if luo_prezzi.uof_ultimo_acquisto("F", ldt_data_rif, ll_gg_a_ritroso, ls_cod_prodotto_ds, "", ld_costo_acquisto, as_errore) < 0 then
		destroy luo_prezzi
		destroy lds_listino
		destroy lds_data
		
		return -1
	end if
	
	if ld_costo_acquisto=0 or isnull(ld_costo_acquisto) then
		ld_costo_acquisto = ld_costo_standard_ds
		ls_tipo_costo = "S"
	end if
	
	ld_margine = ld_prezzo_netto - ld_costo_acquisto
	if ld_prezzo_netto <> 0 then
		ld_perc_marg = (ld_margine / ld_prezzo_netto) * 100
	else
		ld_perc_marg = 0
	end if
	
	ll_new = dw_report.insertrow(0)
	dw_report.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_ds)
	dw_report.setitem(ll_new, "des_prodotto", ls_des_prodotto_ds)
	dw_report.setitem(ll_new, "prezzo_lordo_v", ld_prezzo_lordo)
	dw_report.setitem(ll_new, "sconto", ld_sconto)
	dw_report.setitem(ll_new, "prezzo_netto_v", ld_prezzo_netto)
	dw_report.setitem(ll_new, "costo_acquisto", ld_costo_acquisto)
	dw_report.setitem(ll_new, "tipo_costo_acquisto", ls_tipo_costo)
	dw_report.setitem(ll_new, "margine", ld_margine)
	dw_report.setitem(ll_new, "margine_percentuale", ld_perc_marg)
	dw_report.setitem(ll_new, "minimo", ld_minimo)
	dw_report.setitem(ll_new, "massimo", ld_massimo)
	
	
next

destroy luo_prezzi
destroy lds_listino
destroy lds_data


return 0
end function

public function double wf_cambio_ven (string as_cod_valuta, datetime adt_data_rif);double				ld_cambio_ven
long					ll_tot
datastore			lds_data
string					ls_sql, ls_errore



ls_sql =	"select cambio_ven "+&
			"from tab_cambi  "+&
			"where	cod_azienda='"+s_cs_xx.cod_azienda+"' and  "+&
						"cod_valuta='"+as_cod_valuta+"' and  "+&
						"data_cambio<='"+string(adt_data_rif, s_cs_xx.db_funzioni.formato_data)+"' "+&
			"order by cod_valuta, data_cambio desc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot>0 then
	ld_cambio_ven = lds_data.getitemnumber(1, 1)
else
	//errore o nessuna riga
	select cambio_ven
	into   :ld_cambio_ven    
	from   tab_valute
	where	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_valuta = :as_cod_valuta;
end if
	
if isnull(ld_cambio_ven) then ld_cambio_ven = 0

return ld_cambio_ven

end function

public function decimal wf_condizioni (ref datastore ads_data, string as_cod_prodotto, datetime adt_data_rif, string as_cod_tipo_listino, string as_cod_valuta, decimal ad_fat_conv_ven, double ad_data_cambio);string				ls_null
dec{4}			ld_prezzo
long				ll_row

setnull(ls_null)

iuo_condizioni.str_parametri.lds_oggetto = ads_data
iuo_condizioni.str_parametri.cod_tipo_listino_prodotto = as_cod_tipo_listino
iuo_condizioni.str_parametri.cod_valuta = as_cod_valuta
iuo_condizioni.str_parametri.data_riferimento = adt_data_rif
iuo_condizioni.str_parametri.cod_cliente = ls_null
iuo_condizioni.str_parametri.cod_prodotto = as_cod_prodotto
iuo_condizioni.str_parametri.dim_1 = 0
iuo_condizioni.str_parametri.dim_2 = 0
iuo_condizioni.str_parametri.quantita = 1
iuo_condizioni.str_parametri.valore = 0
iuo_condizioni.str_parametri.cod_agente_1 = ls_null
iuo_condizioni.str_parametri.cod_agente_2 = ls_null
iuo_condizioni.str_parametri.fat_conversione_ven = ad_fat_conv_ven
iuo_condizioni.str_parametri.colonna_quantita = "quan_ordine"
iuo_condizioni.str_parametri.colonna_prezzo = "prezzo_vendita"


ll_row = ads_data.insertrow(0)
ads_data.setrow(ll_row)
ads_data.setitem(1, iuo_condizioni.str_parametri.colonna_quantita, 1)

iuo_condizioni.wf_condizioni_cliente_2()	

//leggo il valore
ld_prezzo = ads_data.getitemdecimal(1, iuo_condizioni.str_parametri.colonna_prezzo)

//resetto il datastore
ads_data.reset()

if isnull(ld_prezzo) then ld_prezzo = 0

return ld_prezzo
end function

on w_report_margine_listino_sfuso.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_report_margine_listino_sfuso.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string					ls_path_logo, ls_modify, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5
windowobject		l_objects[ ]

dw_report.ib_dw_report = true
iuo_condizioni = create uo_condizioni_cliente

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
iuo_dw_main = dw_report


l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])

l_objects[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_ricerca.postevent("ue_imposta_ricerca")

dw_report.object.datawindow.print.preview = "yes"


select		label_livello_prod_1, label_livello_prod_2, label_livello_prod_3, label_livello_prod_4, con_magazzino.label_livello_prod_5
into		:ls_cod_livello_prod_1, :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4, :ls_cod_livello_prod_5
from con_magazzino
where con_magazzino.cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ls_cod_livello_prod_1) or ls_cod_livello_prod_1="" then ls_cod_livello_prod_1 = "Livello Prod. 1"
if isnull(ls_cod_livello_prod_2) or ls_cod_livello_prod_2="" then ls_cod_livello_prod_2 = "Livello Prod. 2"
if isnull(ls_cod_livello_prod_3) or ls_cod_livello_prod_3="" then ls_cod_livello_prod_3 = "Livello Prod. 3"
if isnull(ls_cod_livello_prod_4) or ls_cod_livello_prod_4="" then ls_cod_livello_prod_4 = "Livello Prod. 4"
if isnull(ls_cod_livello_prod_5) or ls_cod_livello_prod_5="" then ls_cod_livello_prod_5 = "Livello Prod. 5"

dw_ricerca.modify("st_cod_livello_prod_1.text='" + ls_cod_livello_prod_1 + ":'")
dw_ricerca.modify("st_cod_livello_prod_2.text='" + ls_cod_livello_prod_2 + ":'")
dw_ricerca.modify("st_cod_livello_prod_3.text='" + ls_cod_livello_prod_3 + ":'")
dw_ricerca.modify("st_cod_livello_prod_4.text='" + ls_cod_livello_prod_4 + ":'")
dw_ricerca.modify("st_cod_livello_prod_5.text='" + ls_cod_livello_prod_5 + ":'")



end event

event close;call super::close;

destroy		iuo_condizioni
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca, &
                 "cod_tipo_listino", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))") 
					  
f_po_loaddddw_dw(dw_ricerca, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &                 
			 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_1", sqlca, "tab_livelli_prod_1", "cod_livello_prod_1", "des_livello", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + "))")
end event

type dw_report from uo_cs_xx_dw within w_report_margine_listino_sfuso
integer x = 23
integer y = 116
integer width = 3753
integer height = 2244
integer taborder = 30
string dataobject = "d_report_margine_listino_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_margine_listino_sfuso
integer x = 9
integer y = 8
integer width = 3762
integer height = 2360
integer taborder = 10
boolean border = false
end type

type dw_ricerca from u_dw_search within w_report_margine_listino_sfuso
event ue_imposta_ricerca ( )
integer x = 23
integer y = 116
integer width = 3040
integer height = 1812
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_margine_listino_sfuso_sel"
boolean border = false
end type

event ue_imposta_ricerca();

setitem(1, "data_riferimento", datetime(today(), 00:00:00))


select 	cod_cat_mer 
into		:is_cod_prod_modello
from 		tab_cat_mer_tabelle 
where 	cod_azienda = :s_cs_xx.cod_azienda and
      		nome_tabella = 'tab_modelli';
if sqlca.sqlcode <> 0 or isnull(is_cod_prod_modello) then
	is_cod_prod_modello = ""
end if

if is_cod_prod_modello <> "" then
	dw_ricerca.object.t_sfuso.text = "Disponibili per l'elaborazione tutti gli articoli in anagrafica prodotti (con Cat.Merceologica VUOTA o diversa da '"+is_cod_prod_modello+"') !"
else
	dw_ricerca.object.t_sfuso.text = "Disponibili per l'elaborazione tutti gli articoli in anagrafica prodotti !"
end if


return
end event

event buttonclicked;call super::buttonclicked;string				ls_errore
integer			li_ret


choose case dwo.name
	case "b_annulla"
		wf_reset()
		return
	
	case "b_report"
		li_ret = wf_report(ls_errore)
		
		choose case li_ret
			case -1
				dw_ricerca.object.t_log.text = "Errore!"
				g_mb.error(ls_errore)
				return
		
			case 1
				dw_ricerca.object.t_log.text = "Alert!"
				g_mb.warning(ls_errore)
				return
				
			case else
				dw_ricerca.object.t_log.text = "Pronto!"
				dw_folder.fu_selecttab(2)
				dw_report.change_dw_current()
				
				return
				
		end choose
		
	case "b_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_inizio")
		
	case "b_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_fine")
	
end choose
end event

event itemchanged;call super::itemchanged;string			ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3,  ls_cod_livello_prod_4


setnull(ls_null)


choose case dwo.name
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_prodotto_inizio"
		
		if data<>"" and not isnull(data) then
			if isnull(getitemstring(1, "cod_prodotto_fine")) or getitemstring(1, "cod_prodotto_fine")="" then
				setitem(1, "cod_prodotto_fine", data)
			end if
		end if
		
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_1"
		setitem(1, "cod_livello_prod_2", ls_null)
		setitem(1, "cod_livello_prod_3", ls_null)
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_2", sqlca, "tab_livelli_prod_2", "cod_livello_prod_2", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  						"cod_livello_prod_1='" + data + "'")

		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_3", sqlca, "tab_livelli_prod_3", "cod_livello_prod_3", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  						"cod_livello_prod_1='" + data + "' and cod_livello_prod_2=''")
													  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca, "tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + data + "' and cod_livello_prod_2='' and cod_livello_prod_3=''")

		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
														"cod_livello_prod_1='" + data + "' and cod_livello_prod_2='' and cod_livello_prod_3 = '' and cod_livello_prod_4=''")
  
  	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_2"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		
		setitem(1, "cod_livello_prod_3", ls_null)
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_3", sqlca, "tab_livelli_prod_3", "cod_livello_prod_3", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
														"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "'")
														
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca, "tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "' and cod_livello_prod_3=''")
														  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "' and cod_livello_prod_3='' and cod_livello_prod_4=''")
							  
							  
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_3"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(1, "cod_livello_prod_2")
		
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca,"tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3='" + data + "'")
														  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + ls_cod_livello_prod_2 + "' and "+&
														"cod_livello_prod_3='" + data + "' and cod_livello_prod_4=''")
							  
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_4"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(1, "cod_livello_prod_2")
		ls_cod_livello_prod_3 = getitemstring(1, "cod_livello_prod_3")
		
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and "+&
														"cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + data + "'")
	
end choose
end event

