﻿$PBExportHeader$w_report_margine_ordini_sfuso.srw
forward
global type w_report_margine_ordini_sfuso from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_margine_ordini_sfuso
end type
type dw_ricerca from u_dw_search within w_report_margine_ordini_sfuso
end type
end forward

global type w_report_margine_ordini_sfuso from w_cs_xx_principale
integer width = 5275
integer height = 2828
string title = "Report Marginalità Vendite"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
dw_report dw_report
dw_ricerca dw_ricerca
end type
global w_report_margine_ordini_sfuso w_report_margine_ordini_sfuso

type variables


integer					ii_anno_doc
long						il_num_doc
string						is_tipo_documento
end variables

forward prototypes
public function integer wf_reset ()
public function integer wf_report (ref string as_errore)
end prototypes

public function integer wf_reset ();

dw_ricerca.setitem(1, "anno_ordine", ii_anno_doc)
dw_ricerca.setitem(1, "num_ordine", il_num_doc)
dw_ricerca.setitem(1, "data_fine", datetime(today(), 00:00:00))

dw_ricerca.setitem(1, "minimo", 40)
dw_ricerca.setitem(1, "massimo", 60)
dw_ricerca.setitem(1, "gg_a_ritroso", 730)



return 0
end function

public function integer wf_report (ref string as_errore);
uo_trova_prezzi		luo_prezzi

long						ll_gg_a_ritroso, ll_num_doc, ll_index, ll_tot, ll_new, ll_num_documento

integer					li_anno_doc, li_anno_documento, li_ret

datetime					ldt_data_fine

dec{4}					ld_massimo, ld_minimo, ld_precisione_valuta

string						ls_sql, ls_tabella_tes, ls_tabella_det, ls_col_prog_riga, ls_col_qta, ls_tipo_costo, ls_tipo, ls_cod_documento, ls_numeratore_documento, ls_numerazione, ls_tipo_doc,&
							ls_cod_valuta

datastore				lds_data

long						ll_riga_ds
string						ls_cod_prodotto_ds, ls_des_prodotto_ds, ls_des_prodotto_p_ds, ls_cod_misura_ds, ls_cod_cliente_ds, ls_rag_soc_1_ds
dec{4}					ld_qta_ds, ld_val_unitario_riga, ld_costo_acquisto, ld_imponibile_riga_ds, ld_imponibile_riga_valuta_ds, ld_sconti_riga[]
dec{5}					ld_cambio_ven_ds, ld_costo_standard_ds

uo_calcola_documento_euro			luo_calcola_doc

datawindow				ldw_null



dw_ricerca.accepttext()
dw_report.reset()

li_anno_doc = dw_ricerca.getitemnumber(1, "anno_ordine")
ll_num_doc = dw_ricerca.getitemnumber(1, "num_ordine")

ll_gg_a_ritroso = dw_ricerca.getitemnumber(1, "gg_a_ritroso")
ldt_data_fine = dw_ricerca.getitemdatetime(1, "data_fine")

if isnull(ldt_data_fine) or year(date(ldt_data_fine))<1950 then
	as_errore = "Impostare una data di riferimento per la ricerca dell'ultimo costo acquisto!"
	return 1
end if

ld_minimo = dw_ricerca.getitemnumber(1, "minimo")
ld_massimo = dw_ricerca.getitemnumber(1, "massimo")

if isnull(ld_minimo) then ld_minimo = 0
if isnull(ld_massimo) then ld_massimo = 0

if ld_massimo<=ld_minimo then
	as_errore = "Il valore del Massimo % deve essere superiore al valore del Minimo % !"
	return 1
end if

ls_tipo = dw_ricerca.getitemstring(1, "tipo_doc")

ls_numerazione = ""

if ls_tipo="O" then
	ls_tabella_det = "det_ord_ven"
	ls_tabella_tes = "tes_ord_ven"
	ls_col_prog_riga 	= "prog_riga_ord_ven"
	ls_col_qta			= "quan_ordine"
	
	dw_report.object.titolo_t.text = "REPORT MARGINALITA ORDINE"
	dw_report.object.doc_t.text = "NR. ORDINE:"
	
	ls_tipo_doc = "ord_ven"
	
	select cod_valuta, cambio_ven
	into :ls_cod_valuta, :ld_cambio_ven_ds
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_doc and
				num_registrazione=:ll_num_doc;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura dati da tes_ord_ven: "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=100 then
		as_errore = "Ordine inesistente!"
		return 1
	
	end if
	
else
	ls_tabella_det = "det_off_ven"
	ls_tabella_tes = "tes_off_ven"
	ls_col_prog_riga 	= "prog_riga_off_ven"
	ls_col_qta			= "quan_offerta"
	
	dw_report.object.titolo_t.text = "REPORT MARGINALITA OFFERTA"
	dw_report.object.doc_t.text = "NR. OFFERTA:"
	
	ls_tipo_doc = "off_ven"
	
	select		cod_documento, anno_documento, numeratore_documento, num_documento, cod_valuta, cambio_ven
	into		:ls_cod_documento, :li_anno_documento, :ls_numeratore_documento, :ll_num_documento, :ls_cod_valuta, :ld_cambio_ven_ds
	from tes_off_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_doc and
				num_registrazione=:ll_num_doc;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura dati da tes_off_ven: "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=100 then
		as_errore = "Offerta inesistente!"
		return 1
	
	end if
	
	if g_str.isnotempty(ls_cod_documento) and li_anno_documento>0 and g_str.isnotempty(ls_numeratore_documento) and ll_num_documento>0 then
		ls_numerazione = ls_cod_documento + " " + string(li_anno_documento) + "/" + ls_numeratore_documento + " " + string(ll_num_documento)
	end if
end if




// Viene letto il valore di precisione per la valuta corrente
select precisione
into   :ld_precisione_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode = -1 then
	as_errore = "Errore nella select di tab_valute: " + sqlca.sqlerrtext
	return -1
end if

luo_calcola_doc = create uo_calcola_documento_euro

luo_calcola_doc.il_anno_registrazione = li_anno_doc
luo_calcola_doc.il_num_registrazione = ll_num_doc
luo_calcola_doc.is_tipo_documento = ls_tipo_doc
luo_calcola_doc.is_cod_valuta = ls_cod_valuta
luo_calcola_doc.id_cambio = ld_cambio_ven_ds
luo_calcola_doc.is_colonna_quantita = ls_col_qta
luo_calcola_doc.is_colonna_prog_riga = ls_col_prog_riga
luo_calcola_doc.id_precisione = ld_precisione_valuta


ls_tipo = is_tipo_documento

ls_sql = "select 	d."+ls_col_prog_riga+","+&
						"d.cod_prodotto,"+&
						"d.des_prodotto,"+&
						"p.des_prodotto,"+&
						"d."+ls_col_qta+","+&
						"p.cod_misura_mag,"+&
						"t.cod_cliente,"+&
						"c.rag_soc_1,"+&
						"p.costo_standard "+&
		"from "+ls_tabella_det+" as d "+&
		"join anag_prodotti as p on p.cod_azienda = d.cod_azienda and "+&
         								"p.cod_prodotto = d.cod_prodotto "+&
		"join "+ls_tabella_tes+" as t on t.cod_azienda = d.cod_azienda and "+&
         								"t.anno_registrazione = d.anno_registrazione and "+&
        									"t.num_registrazione = d.num_registrazione "+&
		"join anag_clienti as c on c.cod_azienda = c.cod_azienda and "+&
         								"c.cod_cliente = t.cod_cliente "+&
		"where	d.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"d.anno_registrazione="+string(li_anno_doc)+"and "+&
					"d.num_registrazione="+string(ll_num_doc)+" and "+&
					"d.cod_prodotto is not null and d."+ls_col_qta+" >0 "

ls_sql += " order by d."+ls_col_prog_riga + " asc "

pcca.mdi_frame.setmicrohelp("Elaborazione query in corso. Attendere ...")

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot <0 then
	destroy luo_calcola_doc
	return -1
	
elseif ll_tot=0 then
	as_errore = "Nessuna riga da mostare per questo documento!"
	destroy luo_calcola_doc
	destroy lds_data
	return 1
end if

luo_prezzi = create uo_trova_prezzi

for ll_index=1 to ll_tot
	ll_riga_ds					= lds_data.getitemnumber(ll_index, 1)
	ls_cod_prodotto_ds		= lds_data.getitemstring(ll_index, 2)
	ls_des_prodotto_ds		= lds_data.getitemstring(ll_index, 3)
	ls_des_prodotto_p_ds		= lds_data.getitemstring(ll_index, 4)
	
	if g_str.isempty(ls_des_prodotto_ds) then ls_des_prodotto_ds = ls_des_prodotto_p_ds
	
	ld_qta_ds 					= lds_data.getitemnumber(ll_index, 5)
	ls_cod_misura_ds			= lds_data.getitemstring(ll_index, 6)
	ls_cod_cliente_ds			= lds_data.getitemstring(ll_index, 7)
	ls_rag_soc_1_ds			= lds_data.getitemstring(ll_index, 8)
	ld_costo_standard_ds		= lds_data.getitemnumber(ll_index, 9)
	
	if luo_calcola_doc.uof_get_imponibile_riga("table", ll_riga_ds, ldw_null, "", 0, 0, ld_sconti_riga[], 0, "", ld_imponibile_riga_ds, ld_imponibile_riga_valuta_ds, as_errore) < 0 then
		destroy luo_calcola_doc
		destroy luo_prezzi
		destroy lds_data
		return -1
	end if
	
	ld_val_unitario_riga = ld_imponibile_riga_ds / ld_qta_ds
	
	
	if luo_prezzi.uof_ultimo_acquisto("F", ldt_data_fine, ll_gg_a_ritroso, ls_cod_prodotto_ds, "", ld_costo_acquisto, as_errore) < 0 then
		destroy luo_calcola_doc
		destroy luo_prezzi
		destroy lds_data
		return -1
	end if
	
	if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0
	
	if ld_costo_acquisto=0 then
		//il costo da considerare è quello standard
		ls_tipo_costo = "S"
		ld_costo_acquisto = ld_costo_standard_ds
	else
		//trovato un costo ultimo (è in "ld_costo_acquisto")
		ls_tipo_costo = "U"
	end if
	
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "cod_cliente", ls_cod_cliente_ds)
	dw_report.setitem(ll_new, "rag_soc_1", ls_rag_soc_1_ds)
	dw_report.setitem(ll_new, "anno_doc", li_anno_doc)
	dw_report.setitem(ll_new, "num_doc", ll_num_doc)
	dw_report.setitem(ll_new, "riga_doc", ll_riga_ds)
	dw_report.setitem(ll_new, "numerazione", ls_numerazione)
	dw_report.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_ds)
	dw_report.setitem(ll_new, "des_prodotto", ls_des_prodotto_ds)
	dw_report.setitem(ll_new, "quantita", ld_qta_ds)
	dw_report.setitem(ll_new, "um", ls_cod_misura_ds)
	dw_report.setitem(ll_new, "val_unitario", ld_val_unitario_riga)
	dw_report.setitem(ll_new, "costo_unitario", ld_costo_acquisto)
	dw_report.setitem(ll_new, "tipo_costo", ls_tipo_costo)
	dw_report.setitem(ll_new, "minimo", ld_minimo)
	dw_report.setitem(ll_new, "massimo", ld_massimo)
	
	dw_report.setitem(ll_new, "valore_totale", ld_imponibile_riga_ds)
	dw_report.setitem(ll_new, "costo_totale", ld_costo_acquisto * ld_qta_ds)
	dw_report.setitem(ll_new, "differenza", dw_report.getitemnumber(ll_new, "valore_totale") - dw_report.getitemnumber(ll_new, "costo_totale"))
	
	if dw_report.getitemnumber(ll_new, "valore_totale") > 0 then
		dw_report.setitem(ll_new, "margine", (dw_report.getitemnumber(ll_new, "differenza") / dw_report.getitemnumber(ll_new, "valore_totale")) * 100)
	else
		dw_report.setitem(ll_new, "margine", 0)
	end if
	
next

destroy luo_calcola_doc
destroy luo_prezzi
destroy lds_data


dw_report.object.datawindow.print.preview = "yes"

return 0
end function

on w_report_margine_ordini_sfuso.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_ricerca
end on

on w_report_margine_ordini_sfuso.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;
s_cs_xx_parametri			lstr_param
integer						li_anno_ordine
long							ll_num_ordine

lstr_param = message.powerobjectparm

ii_anno_doc = lstr_param.parametro_d_1
il_num_doc = lstr_param.parametro_d_2
is_tipo_documento = lstr_param.parametro_s_1

if is_tipo_documento = "O" then
	this.title = "Report Marginalità Ordine Vendita"
else
	this.title = "Report Marginalità Offerta Vendita"
end if

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
iuo_dw_main = dw_report

dw_ricerca.postevent("ue_report")






end event

type dw_report from uo_cs_xx_dw within w_report_margine_ordini_sfuso
integer x = 23
integer y = 504
integer width = 5225
integer height = 2212
integer taborder = 30
string dataobject = "d_report_margine_ordini_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_ricerca from u_dw_search within w_report_margine_ordini_sfuso
event ue_report ( )
integer x = 23
integer y = 8
integer width = 4027
integer height = 476
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_margine_ordini_sfuso_sel"
boolean border = false
end type

event ue_report();string				ls_errore
integer			li_ret

dw_ricerca.setitem(1, "tipo_doc", is_tipo_documento)

if is_tipo_documento = "O" then
	dw_report.object.titolo_t.text = "REPORT MARGINALITA ORDINE"
else
	dw_report.object.titolo_t.text = "REPORT MARGINALITA OFFERTA"
end if


dw_ricerca.setitem(1, "anno_ordine", ii_anno_doc)
dw_ricerca.setitem(1, "num_ordine", il_num_doc)
dw_ricerca.setitem(1, "data_fine", datetime(today(), 00:00:00))

dw_ricerca.setitem(1, "minimo", 40)
dw_ricerca.setitem(1, "massimo", 60)
dw_ricerca.setitem(1, "gg_a_ritroso", 730)

li_ret = wf_report(ls_errore)

pcca.mdi_frame.setmicrohelp("Pronto!")

choose case li_ret
	case -1
		g_mb.error(ls_errore)
		return

	case 1
		g_mb.warning(ls_errore)
		return
		
	case else
		dw_report.change_dw_current()
		
		return
end choose

return
end event

event buttonclicked;call super::buttonclicked;string				ls_errore
integer			li_ret


choose case dwo.name
	case "b_annulla"
		wf_reset()
		return
	
	case "b_report"
		li_ret = wf_report(ls_errore)
		
		choose case li_ret
			case -1
				g_mb.error(ls_errore)
				return
		
			case 1
				g_mb.warning(ls_errore)
				return
				
			case else
				dw_report.change_dw_current()
				
				return
		end choose
		

end choose
end event

