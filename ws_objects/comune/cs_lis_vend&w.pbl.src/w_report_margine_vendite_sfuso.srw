﻿$PBExportHeader$w_report_margine_vendite_sfuso.srw
forward
global type w_report_margine_vendite_sfuso from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_margine_vendite_sfuso
end type
type dw_folder from u_folder within w_report_margine_vendite_sfuso
end type
type dw_ricerca from u_dw_search within w_report_margine_vendite_sfuso
end type
end forward

global type w_report_margine_vendite_sfuso from w_cs_xx_principale
integer width = 5275
integer height = 2464
string title = "Report Marginalità Vendite"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_report_margine_vendite_sfuso w_report_margine_vendite_sfuso

type variables


string								is_cod_prod_modello = ""

end variables

forward prototypes
public function integer wf_reset ()
public function integer wf_report (ref string as_errore)
public function double wf_cambio_ven (string as_cod_valuta, datetime adt_data_rif)
public function decimal wf_condizioni (ref datastore ads_data, string as_cod_prodotto, datetime adt_data_rif, string as_cod_tipo_listino, string as_cod_valuta, decimal ad_fat_conv_ven, double ad_data_cambio)
public subroutine wf_reimposta_date ()
public function string wf_query (string as_flag_raggruppo)
end prototypes

public function integer wf_reset ();string					ls_null


setnull(ls_null)

wf_reimposta_date()

dw_ricerca.setitem(1, "cod_prodotto_inizio", ls_null)
dw_ricerca.setitem(1, "cod_prodotto_fine", ls_null)
dw_ricerca.setitem(1, "flag_raggruppa", "N")
dw_ricerca.setitem(1, "minimo", 40)
dw_ricerca.setitem(1, "massimo", 60)

dw_ricerca.setitem(1, "cod_livello_prod_1", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_2", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_3", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_4", ls_null)
dw_ricerca.setitem(1, "cod_livello_prod_5", ls_null)

return 0
end function

public function integer wf_report (ref string as_errore);
string						ls_sql, ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_prodotto_ds, ls_des_prodotto_ds, ls_tipo_costo, ls_filtro, ls_flag_raggruppa, ls_um_mag_ds, &
							ls_cod_documento_ddt_ds, ls_numeratore_doc_ddt_ds,ls_cod_documento_fat_ds, ls_numeratore_doc_fat_ds, ls_documento, ls_cod_prodotto_old_ds, &
							ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_tipo_costo_hold
							
dec{4}					ld_massimo, ld_minimo, ld_costo_standard_ds, ld_costo_acquisto, ld_differenza, ld_quantita_ds, ld_costo_acquisto_hold
dec{2}					ld_perc_marg
dec{5}					ld_val_medio_ds, ld_val_totale, ld_costo_totale

long						ll_index, ll_tot, ll_new, ll_num_mov_ds, ll_num_documento_ddt_ds,ll_num_documento_fat_ds, ll_gg_a_ritroso

integer					li_anno_mov_ds, li_anno_documento_ddt_ds, li_anno_documento_fat_ds

datetime					ldt_data_inizio, ldt_data_fine, ldt_data_documento_ddt_ds, ldt_data_documento_fat_ds

datastore				lds_data

uo_trova_prezzi		luo_prezzi


dw_ricerca.accepttext()
dw_report.reset()
ls_filtro = ""
ll_gg_a_ritroso = dw_ricerca.getitemnumber(1, "gg_a_ritroso")


ldt_data_inizio = dw_ricerca.getitemdatetime(1, "data_inizio")
ldt_data_fine = dw_ricerca.getitemdatetime(1, "data_fine")

if not isnull(ldt_data_inizio) and year(date(ldt_data_inizio))>1950 and &
	not isnull(ldt_data_fine) and year(date(ldt_data_fine))>1950 then
	
	if date(ldt_data_fine) < date(ldt_data_inizio) then
		as_errore = "La Data Fine non può essere inferiore alla Data Inizio!"
		return 1
	end if
end if

ls_flag_raggruppa = dw_ricerca.getitemstring(1, "flag_raggruppa")
ls_cod_prodotto_inizio = dw_ricerca.getitemstring(1, "cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_ricerca.getitemstring(1, "cod_prodotto_fine")

ls_cod_livello_prod_1 = dw_ricerca.getitemstring(1, "cod_livello_prod_1")
ls_cod_livello_prod_2 = dw_ricerca.getitemstring(1, "cod_livello_prod_2")
ls_cod_livello_prod_3 = dw_ricerca.getitemstring(1, "cod_livello_prod_3")
ls_cod_livello_prod_4 = dw_ricerca.getitemstring(1, "cod_livello_prod_4")
ls_cod_livello_prod_5 = dw_ricerca.getitemstring(1, "cod_livello_prod_5")

ld_minimo = dw_ricerca.getitemnumber(1, "minimo")
ld_massimo = dw_ricerca.getitemnumber(1, "massimo")

if isnull(ld_minimo) then ld_minimo = 0
if isnull(ld_massimo) then ld_massimo = 0

if ld_massimo<=ld_minimo then
	as_errore = "Il valore del Massimo % deve essere superiore al valore del Minimo % !"
	return 1
end if

/*
se raggruppato
				1		cod_prodotto
				2		des_prodotto
				3		costo standard
				4		UMmag
				5		tot qta
				6 		val.medio
altrimenti
				1		cod_prodotto,"+&
				2		des_prodotto,"+&
				3		costo standard
				4		UMmag
				5		quan_movimento
				6		val_movimento
				
				7		anno_registrazione del movimento
				8		num_registrazione del movimento
				9		stringa 'DDT n°' 
				10		cod_documento				del DDT (eventuale)
				11		anno_documento				del DDT
				12		"numeratore_documento		del DDT
				13		"num_documento				del DDT
				14		data_ddt							del DDT
				15		stringa 'Fattura n°' 
				16		cod_documento				della FATTURA (eventuale)
				17		anno_documento				della FATTURA
				18		numeratore_documento		della FATTURA
				19		num_documento				della FATTURA
				20		data_fattura						della FATTURA
*/

ls_sql = wf_query(ls_flag_raggruppa)
	
ls_sql +=	"where mov_magazzino.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"(det_bol_ven.anno_registrazione>0 or det_fat_ven.anno_registrazione>0) "

//considero solo i ddt uscita che sono vendita
ls_sql += " and tes_bol_ven.cod_tipo_bol_ven in (select cod_tipo_bol_ven "+&
																"from tab_tipi_bol_ven "+&
																"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																			"flag_tipo_bol_ven='V'"+&
																") "

if is_cod_prod_modello<>"" then
	ls_sql += "and (anag_prodotti.cod_cat_mer<>'"+is_cod_prod_modello+"' or anag_prodotti.cod_cat_mer is null) "
end if

if not isnull(ldt_data_inizio) and year(date(ldt_data_inizio))>1950 then
	ldt_data_inizio = datetime(date(ldt_data_inizio), 00:00:00)
	
	ls_sql += "and mov_magazzino.data_registrazione>='" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_filtro += " - Data Inizio : "+string(ldt_data_inizio, "dd/mm/yyyy")
end if

//################################################
//livelli
if ls_cod_livello_prod_1<>"" and not isnull(ls_cod_livello_prod_1) then
	//-------------------------------------------------------------------------------------------
	ls_sql += "and anag_prodotti.cod_livello_prod_1='"+ls_cod_livello_prod_1+"' "
	
	ls_filtro += " - Livelli ( "+f_des_tabella("tab_livelli_prod_1","cod_livello_prod_1='" +   ls_cod_livello_prod_1  + "'","des_livello")
	//-------------------------------------------------------------------------------------------
	if ls_cod_livello_prod_2<>"" and not isnull(ls_cod_livello_prod_2) then
		ls_sql += "and anag_prodotti.cod_livello_prod_2='"+ls_cod_livello_prod_2+"' "
		
		ls_filtro += ", "+f_des_tabella("tab_livelli_prod_2","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"'","des_livello")
		//----------------------------------------------------------------------------------------
		if ls_cod_livello_prod_3<>"" and not isnull(ls_cod_livello_prod_3) then
			ls_sql += "and anag_prodotti.cod_livello_prod_3='"+ls_cod_livello_prod_3+"' "
			
			ls_filtro += ", "+f_des_tabella("tab_livelli_prod_3","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"'","des_livello")
			//-------------------------------------------------------------------------------------
			if ls_cod_livello_prod_4<>"" and not isnull(ls_cod_livello_prod_4) then
				ls_sql += "and anag_prodotti.cod_livello_prod_4='"+ls_cod_livello_prod_4+"' "
				
				ls_filtro += ", "+f_des_tabella("tab_livelli_prod_4","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"' and "+&
																				 "cod_livello_prod_4='"+ls_cod_livello_prod_4+"'","des_livello")
				//----------------------------------------------------------------------------------
				if ls_cod_livello_prod_5<>"" and not isnull(ls_cod_livello_prod_5) then
					ls_sql += "and anag_prodotti.cod_livello_prod_5='"+ls_cod_livello_prod_5+"' "
					
					ls_filtro += ", "+f_des_tabella("tab_livelli_prod_5","cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and "+&
																				 "cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
																				 "cod_livello_prod_3='"+ls_cod_livello_prod_3+"' and "+&
																				 "cod_livello_prod_4='"+ls_cod_livello_prod_4+"' and "+&
																				 "cod_livello_prod_5='"+ls_cod_livello_prod_5+"'","des_livello")
				end if
			end if
		end if
	end if
	
	ls_filtro+= " )"
	
end if
//################################################

if not isnull(ldt_data_fine) and year(date(ldt_data_fine))>1950 then
	ldt_data_fine = datetime(date(ldt_data_fine), 23:59:59)
	
	ls_sql += "and mov_magazzino.data_registrazione<='" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
	ls_filtro += " - Data Fine : "+string(ldt_data_fine, "dd/mm/yyyy")
end if

if ls_cod_prodotto_inizio<>"" and not isnull(ls_cod_prodotto_inizio) then
	ls_sql += "and mov_magazzino.cod_prodotto>='"+ls_cod_prodotto_inizio+"' "
	ls_filtro += " - da Prodotto : " + ls_cod_prodotto_inizio
end if

if ls_cod_prodotto_fine<>"" and not isnull(ls_cod_prodotto_fine) then
	ls_sql += "and mov_magazzino.cod_prodotto<='"+ls_cod_prodotto_fine+"' "
	ls_filtro += " - fino a Prodotto : " + ls_cod_prodotto_fine
end if

if ls_flag_raggruppa="S" then
	ls_sql += 		"and mov_magazzino.quan_movimento>0 and mov_magazzino.val_movimento>0 "+&
				"group by mov_magazzino.cod_prodotto, anag_prodotti.des_prodotto, anag_prodotti.costo_standard, anag_prodotti.cod_misura_mag "+&
				"order by 1 "
	
	ls_filtro += " - Raggruppamento per Prodotto : Si"
	
	//----------------------------------------------------------------------------------------------
	//impostazione del dataobject
	dw_report.dataobject = "d_report_margine_vendite_group_sfuso"
	//----------------------------------------------------------------------------------------------
else
	ls_sql += " order by 1,14,10,11,12,13,20,16,17,18,19 "
	
	ls_filtro += " - Raggruppamento per Prodotto : No"
	
	//----------------------------------------------------------------------------------------------
	//impostazione del dataobject
	dw_report.dataobject = "d_report_margine_vendite_sfuso"
	//----------------------------------------------------------------------------------------------
end if

ls_filtro += " - Min/Max per colori : " + string(ld_minimo, "##0") + "/" + string(ld_massimo, "##0")

dw_report.object.filtro_t.text = ls_filtro
dw_report.object.datawindow.print.preview = "yes"

dw_ricerca.object.t_log.text = "Preparazione dati in corso ..."

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot<0 then
	return -1
end if

luo_prezzi = create uo_trova_prezzi
ls_cod_prodotto_old_ds = ""

for ll_index=1 to ll_tot
	if mod(ll_index, 10) = 0 then yield()
	
	ls_cod_prodotto_ds				= lds_data.getitemstring(ll_index, 1)
	dw_ricerca.object.t_log.text = "(" + string(ll_index) + " di " + string(ll_tot) + ") Elaborazione articolo " + ls_cod_prodotto_ds + " in corso ..."	
	
	ls_des_prodotto_ds				= lds_data.getitemstring(ll_index, 2)
	
	ld_costo_standard_ds				= lds_data.getitemnumber(ll_index, 3)
	if isnull(ld_costo_standard_ds) then ld_costo_standard_ds = 0
	
	ls_um_mag_ds				= lds_data.getitemstring(ll_index, 4)
	ld_quantita_ds				= lds_data.getitemnumber(ll_index, 5)
	ld_val_medio_ds			= lds_data.getitemnumber(ll_index, 6)
	
	
	if ls_flag_raggruppa<>"S" then
		
		// se il valore del movimento magazzino è vuoto prendo la fattura e poi il DDT
		if isnull(ld_val_medio_ds) or ld_val_medio_ds = 0 then
			// se anche il valore nella fattura è zero, allora provo con la fattura
			if not isnull(lds_data.getitemnumber(ll_index, 24)) and  lds_data.getitemnumber(ll_index, 24) > 0 and lds_data.getitemnumber(ll_index, 23) > 0 then
				ld_val_medio_ds = round(lds_data.getitemnumber(ll_index, 24) / lds_data.getitemnumber(ll_index, 23),4)
			elseif not isnull(lds_data.getitemnumber(ll_index, 22)) and  lds_data.getitemnumber(ll_index, 22) > 0 and lds_data.getitemnumber(ll_index, 21) > 0 then
				ld_val_medio_ds = round(lds_data.getitemnumber(ll_index, 22) / lds_data.getitemnumber(ll_index, 21),4)
			else
			end if
		end if
		
		li_anno_mov_ds					= lds_data.getitemnumber(ll_index, 7)			//anno_registrazione del movimento
		ll_num_mov_ds						= lds_data.getitemnumber(ll_index, 8)			//num_registrazione del movimento
		//lds_data.getitemstring(ll_index, 9)				//stringa 'DDT n°' 
		ls_cod_documento_ddt_ds		= lds_data.getitemstring(ll_index, 10)			//cod_documento				del DDT (eventuale)
		li_anno_documento_ddt_ds		= lds_data.getitemnumber(ll_index, 11)			//anno_documento				del DDT
		ls_numeratore_doc_ddt_ds		= lds_data.getitemstring(ll_index, 12)			//numeratore_documento		del DDT
		ll_num_documento_ddt_ds		= lds_data.getitemnumber(ll_index, 13)			//num_documento				del DDT
		ldt_data_documento_ddt_ds		= lds_data.getitemdatetime(ll_index, 14)		//data_documento				del DDT
		//lds_data.getitemstring(ll_index, 15)			//stringa 'Fattura n°' 
		ls_cod_documento_fat_ds		= lds_data.getitemstring(ll_index, 16)			//cod_documento				della FATTURA (eventuale)
		li_anno_documento_fat_ds		= lds_data.getitemnumber(ll_index, 17)			//anno_documento				della FATTURA
		ls_numeratore_doc_fat_ds		= lds_data.getitemstring(ll_index, 18)			//numeratore_documento		della FATTURA
		ll_num_documento_fat_ds		= lds_data.getitemnumber(ll_index, 19)			//num_documento				della FATTURA
		ldt_data_documento_fat_ds		= lds_data.getitemdatetime(ll_index, 20)		//data_documento				della FATTURA
	end if
	
	//valutare ultimo costo acquisto
	dw_ricerca.object.t_log.text = "(" + string(ll_index) + " di " + string(ll_tot) + ") Elaborazione articolo " + ls_cod_prodotto_ds + " in corso ... (ultimo costo acquisto)"
	
	if ls_cod_prodotto_old_ds<>ls_cod_prodotto_ds then
		//prodotto diverso da quello elaborato precedentemente (oppure è il primo della lista)
		
		ls_cod_prodotto_old_ds = ls_cod_prodotto_ds
	
		if luo_prezzi.uof_ultimo_acquisto("F", ldt_data_fine, ll_gg_a_ritroso, ls_cod_prodotto_ds, "", ld_costo_acquisto, as_errore) < 0 then
			destroy luo_prezzi
			destroy lds_data
			
			return -1
		end if
		
		if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0
		

		if ld_costo_acquisto=0 then
			//il costo da considerare è quello standard
			ls_tipo_costo = "S"
			ld_costo_acquisto = ld_costo_standard_ds
		else
			//trovato un costo ultimo (è in "ld_costo_acquisto")
			ls_tipo_costo = "U"
		end if

	else
		//ultimo costo di acquisto è quello di prima, visto che il prodotto è lo stesso (OCIO, l'ordinamento deve essere per codice prodotto nel caso non raggruppato) ...
		//ovviamente anche il tipo costo è quello di prima
		
		ls_tipo_costo = ls_tipo_costo_hold
		ld_costo_acquisto = ld_costo_acquisto_hold
	end if
	
	//salvo il costo ultimo acquisto ed il tipo costo dell'ultimo prodotto
	ls_tipo_costo_hold = ls_tipo_costo
	ld_costo_acquisto_hold = ld_costo_acquisto
	
	
	ll_new = dw_report.insertrow(0)
	dw_report.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_ds)
	dw_report.setitem(ll_new, "des_prodotto", ls_des_prodotto_ds)
	
	dw_report.setitem(ll_new, "cod_um", ls_um_mag_ds)
	dw_report.setitem(ll_new, "qta_venduta", ld_quantita_ds)
	dw_report.setitem(ll_new, "valore_unitario", ld_val_medio_ds)
	
	dw_report.setitem(ll_new, "minimo", ld_minimo)
	dw_report.setitem(ll_new, "massimo", ld_massimo)
	
	
	if ls_flag_raggruppa<>"S" then
		dw_report.setitem(ll_new, "anno_movimento", li_anno_mov_ds)
		dw_report.setitem(ll_new, "num_movimento", ll_num_mov_ds)
		
		ls_documento = ""
		if not isnull(ls_cod_documento_ddt_ds) and ls_cod_documento_ddt_ds<>"" then
			//c'è un ddt di vendita
			ls_documento += "DDT. " + ls_cod_documento_ddt_ds + " " + string(li_anno_documento_ddt_ds) + "/" + &
									ls_numeratore_doc_ddt_ds + " " + string(ll_num_documento_ddt_ds) + " " + string(ldt_data_documento_ddt_ds, "dd/mm/yy")
		end if
		
		if not isnull(ls_cod_documento_fat_ds) and ls_cod_documento_fat_ds<>"" then
			//c'è una fattura di vendita
			if ls_documento<>"" and not isnull(ls_documento) then
				ls_documento += "~r~n"
			end if
			
			ls_documento += "FAT " + ls_cod_documento_fat_ds + " " + string(li_anno_documento_fat_ds) + "/" + &
									ls_numeratore_doc_fat_ds + " " + string(ll_num_documento_fat_ds) + " " + string(ldt_data_documento_fat_ds, "dd/mm/yy")
		end if
		
		dw_report.setitem(ll_new, "documento", ls_documento)
	end if
	
	ld_val_totale = ld_val_medio_ds * ld_quantita_ds
	dw_report.setitem(ll_new, "valore_totale", ld_val_totale)
	
	dw_report.setitem(ll_new, "costo_unitario", ld_costo_acquisto)
	dw_report.setitem(ll_new, "tipo_costo", ls_tipo_costo)
	
	ld_costo_totale = ld_costo_acquisto * ld_quantita_ds
	dw_report.setitem(ll_new, "costo_totale", ld_costo_totale)
	
	ld_differenza = ld_val_totale - ld_costo_totale
	dw_report.setitem(ll_new, "differenza", ld_differenza)
	
	if ld_val_totale <> 0 then
		ld_perc_marg = (ld_differenza / ld_val_totale) * 100
	else
		ld_perc_marg = 0
	end if
	dw_report.setitem(ll_new, "margine", ld_perc_marg)
	
next

destroy luo_prezzi
destroy lds_data


return 0
end function

public function double wf_cambio_ven (string as_cod_valuta, datetime adt_data_rif);double				ld_cambio_ven
long					ll_tot
datastore			lds_data
string					ls_sql, ls_errore



ls_sql =	"select cambio_ven "+&
			"from tab_cambi  "+&
			"where	cod_azienda='"+s_cs_xx.cod_azienda+"' and  "+&
						"cod_valuta='"+as_cod_valuta+"' and  "+&
						"data_cambio<='"+string(adt_data_rif, s_cs_xx.db_funzioni.formato_data)+"' "+&
			"order by cod_valuta, data_cambio desc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot>0 then
	ld_cambio_ven = lds_data.getitemnumber(1, 1)
else
	//errore o nessuna riga
	select cambio_ven
	into   :ld_cambio_ven    
	from   tab_valute
	where	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_valuta = :as_cod_valuta;
end if
	
if isnull(ld_cambio_ven) then ld_cambio_ven = 0

return ld_cambio_ven

end function

public function decimal wf_condizioni (ref datastore ads_data, string as_cod_prodotto, datetime adt_data_rif, string as_cod_tipo_listino, string as_cod_valuta, decimal ad_fat_conv_ven, double ad_data_cambio);string				ls_null
dec{4}			ld_prezzo
long				ll_row

//setnull(ls_null)
//
//iuo_condizioni.str_parametri.lds_oggetto = ads_data
//iuo_condizioni.str_parametri.cod_tipo_listino_prodotto = as_cod_tipo_listino
//iuo_condizioni.str_parametri.cod_valuta = as_cod_valuta
//iuo_condizioni.str_parametri.data_riferimento = adt_data_rif
//iuo_condizioni.str_parametri.cod_cliente = ls_null
//iuo_condizioni.str_parametri.cod_prodotto = as_cod_prodotto
//iuo_condizioni.str_parametri.dim_1 = 0
//iuo_condizioni.str_parametri.dim_2 = 0
//iuo_condizioni.str_parametri.quantita = 1
//iuo_condizioni.str_parametri.valore = 0
//iuo_condizioni.str_parametri.cod_agente_1 = ls_null
//iuo_condizioni.str_parametri.cod_agente_2 = ls_null
//iuo_condizioni.str_parametri.fat_conversione_ven = ad_fat_conv_ven
//iuo_condizioni.str_parametri.colonna_quantita = "quan_ordine"
//iuo_condizioni.str_parametri.colonna_prezzo = "prezzo_vendita"
//
//
//ll_row = ads_data.insertrow(0)
//ads_data.setrow(ll_row)
//ads_data.setitem(1, iuo_condizioni.str_parametri.colonna_quantita, 1)
//
//iuo_condizioni.wf_condizioni_cliente_2()	
//
////leggo il valore
//ld_prezzo = ads_data.getitemdecimal(1, iuo_condizioni.str_parametri.colonna_prezzo)
//
////resetto il datastore
//ads_data.reset()
//
//if isnull(ld_prezzo) then ld_prezzo = 0

return ld_prezzo
end function

public subroutine wf_reimposta_date ();date				ldt_data
integer			li_month, li_last_day


ldt_data = today()

li_last_day = guo_functions.uof_get_ultimo_giorno_mese(ldt_data)


dw_ricerca.setitem(1, "data_inizio", datetime(date(year(ldt_data), month(ldt_data), 1), 00:00:00))
dw_ricerca.setitem(1, "data_fine", datetime(date(year(ldt_data), month(ldt_data), li_last_day), 00:00:00))

return
end subroutine

public function string wf_query (string as_flag_raggruppo);string				ls_query

if as_flag_raggruppo = "S" then
	ls_query = &
	"select	mov_magazzino.cod_prodotto,"+&
				"anag_prodotti.des_prodotto,"+&
				"anag_prodotti.costo_standard,"+&
				"anag_prodotti.cod_misura_mag,"+&
				"sum(mov_magazzino.quan_movimento) as tot_quantita,"+&
				"sum(mov_magazzino.val_movimento * mov_magazzino.quan_movimento)/sum(mov_magazzino.quan_movimento) "
else
	ls_query = &
	"select	mov_magazzino.cod_prodotto as cod_prodotto,"+&
				"anag_prodotti.des_prodotto as des_prodotto,"+&
				"anag_prodotti.costo_standard,"+&
				"anag_prodotti.cod_misura_mag as cod_misura_mag,"+&
				"mov_magazzino.quan_movimento as quan_movimento,"+&
				"mov_magazzino.val_movimento as val_movimento,"+&
				"mov_magazzino.anno_registrazione,"+&
				"mov_magazzino.num_registrazione,"+&
				"'DDT n°' as des_ddt,"+&
				"tes_bol_ven.cod_documento as cod_documento_ddt,"+&
				"tes_bol_ven.anno_documento as anno_documento_ddt,"+&
				"tes_bol_ven.numeratore_documento as numeratore_documento_ddt,"+&
				"tes_bol_ven.num_documento as num_documento_ddt,"+&
				"tes_bol_ven.data_bolla as data_ddt,"+&
				"'Fattura n°' as des_fat,"+&
				"tes_fat_ven.cod_documento as cod_documento_fat,"+&
				"tes_fat_ven.anno_documento as anno_documento_fat,"+&
				"tes_fat_ven.numeratore_documento as numeratore_documento_fat,"+&
				"tes_fat_ven.num_documento as num_documento_fat,"+&
				"tes_fat_ven.data_fattura as data_fattura, " + &
				"det_bol_ven.quan_consegnata as quan_ddt, " + &
				"det_bol_ven.imponibile_iva as imponibile_iva_ddt, " + &
				"det_fat_ven.quan_fatturata as quan_fat, " + &
				"det_fat_ven.imponibile_iva as imponibile_iva_fat "
end if

ls_query += "from mov_magazzino "+&
				"join anag_prodotti on	anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
				"left outer join det_bol_ven on	det_bol_ven.cod_azienda=mov_magazzino.cod_azienda and det_bol_ven.anno_registrazione_mov_mag=mov_magazzino.anno_registrazione and det_bol_ven.num_registrazione_mov_mag=mov_magazzino.num_registrazione "+&
				"left outer join tes_bol_ven on tes_bol_ven.cod_azienda=det_bol_ven.cod_azienda and tes_bol_ven.anno_registrazione=det_bol_ven.anno_registrazione and tes_bol_ven.num_registrazione=det_bol_ven.num_registrazione "+&
				"left outer join det_fat_ven on det_fat_ven.cod_azienda=det_bol_ven.cod_azienda and "+&
												"det_fat_ven.anno_registrazione_bol_ven=det_bol_ven.anno_registrazione and "+&
												"det_fat_ven.num_registrazione_bol_ven=det_bol_ven.num_registrazione and "+&
												"det_fat_ven.prog_riga_bol_ven = det_bol_ven.prog_riga_bol_ven and "+&
												"det_fat_ven.cod_prodotto = det_bol_ven.cod_prodotto "+&
				"left join tes_fat_ven on tes_fat_ven.cod_azienda=det_fat_ven.cod_azienda and tes_fat_ven.anno_registrazione=det_fat_ven.anno_registrazione and tes_fat_ven.num_registrazione=det_fat_ven.num_registrazione "


return ls_query
end function

on w_report_margine_vendite_sfuso.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_report_margine_vendite_sfuso.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string			ls_path_logo, ls_modify, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5
windowobject l_objects[ ]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &						 
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
iuo_dw_main = dw_report


l_objects[1] = dw_report
dw_folder.fu_assigntab(2, "Report", l_objects[])

l_objects[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_ricerca.postevent("ue_imposta_ricerca")

select		label_livello_prod_1, label_livello_prod_2, label_livello_prod_3, label_livello_prod_4, con_magazzino.label_livello_prod_5
into		:ls_cod_livello_prod_1, :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4, :ls_cod_livello_prod_5
from con_magazzino
where con_magazzino.cod_azienda = :s_cs_xx.cod_azienda;

if isnull(ls_cod_livello_prod_1) or ls_cod_livello_prod_1="" then ls_cod_livello_prod_1 = "Livello Prod. 1"
if isnull(ls_cod_livello_prod_2) or ls_cod_livello_prod_2="" then ls_cod_livello_prod_2 = "Livello Prod. 2"
if isnull(ls_cod_livello_prod_3) or ls_cod_livello_prod_3="" then ls_cod_livello_prod_3 = "Livello Prod. 3"
if isnull(ls_cod_livello_prod_4) or ls_cod_livello_prod_4="" then ls_cod_livello_prod_4 = "Livello Prod. 4"
if isnull(ls_cod_livello_prod_5) or ls_cod_livello_prod_5="" then ls_cod_livello_prod_5 = "Livello Prod. 5"

dw_ricerca.modify("st_cod_livello_prod_1.text='" + ls_cod_livello_prod_1 + ":'")
dw_ricerca.modify("st_cod_livello_prod_2.text='" + ls_cod_livello_prod_2 + ":'")
dw_ricerca.modify("st_cod_livello_prod_3.text='" + ls_cod_livello_prod_3 + ":'")
dw_ricerca.modify("st_cod_livello_prod_4.text='" + ls_cod_livello_prod_4 + ":'")
dw_ricerca.modify("st_cod_livello_prod_5.text='" + ls_cod_livello_prod_5 + ":'")




end event

event pc_setddlb;call super::pc_setddlb;
f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_1", sqlca, "tab_livelli_prod_1", "cod_livello_prod_1", "des_livello", &
                 "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + "))")
end event

type dw_report from uo_cs_xx_dw within w_report_margine_vendite_sfuso
integer x = 23
integer y = 116
integer width = 5202
integer height = 2244
integer taborder = 30
string dataobject = "d_report_margine_vendite_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_margine_vendite_sfuso
integer x = 9
integer y = 8
integer width = 5243
integer height = 2360
integer taborder = 10
boolean border = false
end type

type dw_ricerca from u_dw_search within w_report_margine_vendite_sfuso
event ue_imposta_ricerca ( )
integer x = 23
integer y = 116
integer width = 3296
integer height = 1728
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_margine_vendite_sfuso_sel"
boolean border = false
end type

event ue_imposta_ricerca();

wf_reimposta_date()


select 	cod_cat_mer 
into		:is_cod_prod_modello
from 		tab_cat_mer_tabelle 
where 	cod_azienda = :s_cs_xx.cod_azienda and
      		nome_tabella = 'tab_modelli';
if sqlca.sqlcode <> 0 or isnull(is_cod_prod_modello) then
	is_cod_prod_modello = ""
end if

if is_cod_prod_modello <> "" then
	dw_ricerca.object.t_sfuso.text = "Disponibili per l'elaborazione tutti gli articoli in anagrafica prodotti e listino vendite (con Cat.Merceologica VUOTA o diversa da '"+is_cod_prod_modello+"') !"
else
	dw_ricerca.object.t_sfuso.text = "Disponibili per l'elaborazione tutti gli articoli in anagrafica prodotti e listino vendite!"
end if


return
end event

event buttonclicked;call super::buttonclicked;string				ls_errore
integer			li_ret


choose case dwo.name
	case "b_annulla"
		wf_reset()
		return
	
	case "b_report"
		li_ret = wf_report(ls_errore)
		
		choose case li_ret
			case -1
				dw_ricerca.object.t_log.text = "Errore!"
				g_mb.error(ls_errore)
				return
		
			case 1
				dw_ricerca.object.t_log.text = "Alert!"
				g_mb.warning(ls_errore)
				return
				
			case else
				dw_ricerca.object.t_log.text = "Pronto!"
				dw_folder.fu_selecttab(2)
				dw_report.change_dw_current()
				
				return
		end choose
		
	case "b_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_inizio")
		
	case "b_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_fine")
	
end choose
end event

event itemchanged;call super::itemchanged;string					ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3


choose case dwo.name
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_prodotto_inizio"
		
		if data<>"" and not isnull(data) then
			if isnull(getitemstring(1, "cod_prodotto_fine")) or getitemstring(1, "cod_prodotto_fine")="" then
				setitem(1, "cod_prodotto_fine", data)
			end if
		end if
	
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_1"
		setitem(1, "cod_livello_prod_2", ls_null)
		setitem(1, "cod_livello_prod_3", ls_null)
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_2", sqlca, "tab_livelli_prod_2", "cod_livello_prod_2", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  						"cod_livello_prod_1='" + data + "'")

		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_3", sqlca, "tab_livelli_prod_3", "cod_livello_prod_3", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  						"cod_livello_prod_1='" + data + "' and cod_livello_prod_2=''")
													  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca, "tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + data + "' and cod_livello_prod_2='' and cod_livello_prod_3=''")

		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
														"cod_livello_prod_1='" + data + "' and cod_livello_prod_2='' and cod_livello_prod_3 = '' and cod_livello_prod_4=''")
  
  	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_2"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		
		setitem(1, "cod_livello_prod_3", ls_null)
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_3", sqlca, "tab_livelli_prod_3", "cod_livello_prod_3", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
														"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "'")
														
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca, "tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "' and cod_livello_prod_3=''")
														  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + data + "' and cod_livello_prod_3='' and cod_livello_prod_4=''")
							  
							  
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_3"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(1, "cod_livello_prod_2")
		
		setitem(1, "cod_livello_prod_4", ls_null)
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_4", sqlca,"tab_livelli_prod_4", "cod_livello_prod_4", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3='" + data + "'")
														  
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2='" + ls_cod_livello_prod_2 + "' and "+&
														"cod_livello_prod_3='" + data + "' and cod_livello_prod_4=''")
							  
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	case "cod_livello_prod_4"
		ls_cod_livello_prod_1 = getitemstring(1, "cod_livello_prod_1")
		ls_cod_livello_prod_2 = getitemstring(1, "cod_livello_prod_2")
		ls_cod_livello_prod_3 = getitemstring(1, "cod_livello_prod_3")
		
		setitem(1, "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, "cod_livello_prod_5", sqlca, "tab_livelli_prod_5", "cod_livello_prod_5", "des_livello", &
                       "cod_azienda='" + s_cs_xx.cod_azienda + "' and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>" + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  							"cod_livello_prod_1='" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and "+&
														"cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + data + "'")
	
		
end choose
end event

