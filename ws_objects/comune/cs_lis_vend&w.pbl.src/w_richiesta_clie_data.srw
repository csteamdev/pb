﻿$PBExportHeader$w_richiesta_clie_data.srw
$PBExportComments$Finestra Richiesta Cod_cliente per listini
forward
global type w_richiesta_clie_data from w_cs_xx_principale
end type
type cb_duplica from commandbutton within w_richiesta_clie_data
end type
type cb_annulla from commandbutton within w_richiesta_clie_data
end type
type dw_selezione from uo_cs_xx_dw within w_richiesta_clie_data
end type
end forward

global type w_richiesta_clie_data from w_cs_xx_principale
integer width = 2633
integer height = 520
string title = "Duplicazione Listini"
cb_duplica cb_duplica
cb_annulla cb_annulla
dw_selezione dw_selezione
end type
global w_richiesta_clie_data w_richiesta_clie_data

type variables
string is_tipo_listino_prodotto, is_cod_valuta
long il_progressivo
datetime idt_data_inizio_val
end variables

on w_richiesta_clie_data.create
int iCurrent
call super::create
this.cb_duplica=create cb_duplica
this.cb_annulla=create cb_annulla
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_duplica
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_selezione
end on

on w_richiesta_clie_data.destroy
call super::destroy
destroy(this.cb_duplica)
destroy(this.cb_annulla)
destroy(this.dw_selezione)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
																	 
is_tipo_listino_prodotto = s_cs_xx.parametri.parametro_s_3
is_cod_valuta = s_cs_xx.parametri.parametro_s_2
idt_data_inizio_val = s_cs_xx.parametri.parametro_data_1
il_progressivo = s_cs_xx.parametri.parametro_d_1

end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_duplica from commandbutton within w_richiesta_clie_data
integer x = 2194
integer y = 320
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;boolean lb_listino_comune=true
string ls_cod_cliente, ls_cod_prodotto_listino, ls_cod_versione, ls_cod_prodotto, ls_db,ls_glc
integer li_risposta
long ll_progressivo, ll_prog_listino_produzione
long	ll_count_versione, ll_prog_range
date ld_data
datetime ldt_data_inizio_val
double ld_num_scaglione, ld_limite_dimensione_1, ld_limite_dimensione_2, ld_percento

/// ---------  aggiunta di variabili -----------------------
string ls_cod_versione_1,   ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_gruppo_variante, ls_cod_prodotto_1, &
ls_des_listino_produzione, ls_flag_sconto_a_parte,  ls_flag_tipo_scaglioni, ls_flag_sconto_mag_prezzo_1, &
ls_flag_sconto_mag_prezzo_2, ls_flag_sconto_mag_prezzo_3,ls_flag_sconto_mag_prezzo_4,ls_flag_sconto_mag_prezzo_5, &
ls_flag_origine_prezzo_1, ls_flag_origine_prezzo_2, ls_flag_origine_prezzo_3, ls_flag_origine_prezzo_4, &
ls_flag_origine_prezzo_5, ls_cod_cat_mer, ls_cod_categoria, ls_des_listino_vendite, ls_messaggio, ls_cod_versione_dest
long ll_num_sequenza, ll_prog_listino_prod_1, ll_num_seq_1,  ll_progr
double ld_minimo_fatt_altezza, ld_minimo_fatt_larghezza, ld_minimo_fatt_profondita, ld_minimo_fatt_superficie, &
ld_minimo_fatt_volume, ld_scaglione_1, ld_scaglione_2, ld_scaglione_3, ld_scaglione_4, ld_scaglione_5, &
ld_variazione_1, ld_variazione_2,ld_variazione_3,ld_variazione_4,ld_variazione_5, ld_provvigione_1,ld_provvigione_2
uo_condizioni_cliente luo_condizioni_cliente

// ----------------------------------------------------------
dw_selezione.accepttext( )

luo_condizioni_cliente = create uo_condizioni_cliente
ls_cod_cliente = dw_selezione.getitemstring(1, "cod_cliente")
ls_cod_prodotto = dw_selezione.getitemstring(1, "cod_prodotto")
ld_data = dw_selezione.getitemdate(1, "data_inizio")
ld_percento = dw_selezione.getitemnumber(1, "perc_aumento")

ldt_data_inizio_val = datetime(ld_data, 00:00:00)
if ls_cod_cliente = "" then setnull(ls_cod_cliente)
if ls_cod_prodotto = "" then setnull(ls_cod_prodotto)

if not isnull(ls_cod_cliente) then lb_listino_comune=false

select flag
into   :ls_glc
from   parametri
where  cod_parametro ='GLC';
if sqlca.sqlcode <> 0 then
	ls_glc = "N"
end if

if isnull(ls_glc) then ls_glc = "N"

if isnull(ls_cod_prodotto) and isnull(ls_cod_cliente) then
	g_mb.messagebox("Apice", "Nessuna impostazione di Cliente o Prodotto inserita!")
	return
end if

select max(progressivo) + 1
  into :ll_progressivo
  from listini_vendite
where cod_azienda = :s_cs_xx.cod_azienda
   and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
	and cod_valuta = :is_cod_valuta
	and data_inizio_val = :ldt_data_inizio_val;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Erroe in lettura dati dalla tabella Listini Vendite")
	return
end if

if isnull(ll_progressivo) or ll_progressivo = 0 then ll_progressivo = 1
 
if sqlca.sqlcode = 0 then

		  select cod_cat_mer,   
					cod_categoria,   
					des_listino_vendite,   
					minimo_fatt_altezza,   
					minimo_fatt_larghezza,   
					minimo_fatt_profondita,   
					minimo_fatt_superficie,   
					minimo_fatt_volume,   
					flag_sconto_a_parte,   
					flag_tipo_scaglioni,   
					scaglione_1,   
					scaglione_2,   
					scaglione_3,   
					scaglione_4,   
					scaglione_5,   
					flag_sconto_mag_prezzo_1,   
					flag_sconto_mag_prezzo_2,   
					flag_sconto_mag_prezzo_3,   
					flag_sconto_mag_prezzo_4,   
					flag_sconto_mag_prezzo_5,   
					variazione_1,   
					variazione_2,   
					variazione_3,   
					variazione_4,   
					variazione_5,   
					flag_origine_prezzo_1,   
					flag_origine_prezzo_2,   
					flag_origine_prezzo_3,   
					flag_origine_prezzo_4,   
					flag_origine_prezzo_5  	
			into  :ls_cod_cat_mer,   
					:ls_cod_categoria,   
					:ls_des_listino_vendite,   
					:ld_minimo_fatt_altezza,   
					:ld_minimo_fatt_larghezza,   
					:ld_minimo_fatt_profondita,   
					:ld_minimo_fatt_superficie,   
					:ld_minimo_fatt_volume,   
					:ls_flag_sconto_a_parte,   
					:ls_flag_tipo_scaglioni,   
					:ld_scaglione_1,   
					:ld_scaglione_2,   
					:ld_scaglione_3,   
					:ld_scaglione_4,   
					:ld_scaglione_5,   
					:ls_flag_sconto_mag_prezzo_1,   
					:ls_flag_sconto_mag_prezzo_2,   
					:ls_flag_sconto_mag_prezzo_3,   
					:ls_flag_sconto_mag_prezzo_4,   
					:ls_flag_sconto_mag_prezzo_5,   
					:ld_variazione_1,   
					:ld_variazione_2,   
					:ld_variazione_3,   
					:ld_variazione_4,   
					:ld_variazione_5,   
					:ls_flag_origine_prezzo_1,   
					:ls_flag_origine_prezzo_2,   
					:ls_flag_origine_prezzo_3,   
					:ls_flag_origine_prezzo_4,   
					:ls_flag_origine_prezzo_5  	
			 from listini_vendite
			where cod_azienda = :s_cs_xx.cod_azienda
			  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
			  and	cod_valuta = :is_cod_valuta
			  and data_inizio_val = :idt_data_inizio_val
			  and	progressivo = :il_progressivo;

		if ls_flag_sconto_mag_prezzo_1 = "P" and ld_percento <> 0 then
			ld_variazione_1 = ld_variazione_1 + round( ( (ld_variazione_1 * ld_percento) / 100 ),4)
			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_1,is_cod_valuta,ld_variazione_1,ls_messaggio)
		end if			
		if ls_flag_sconto_mag_prezzo_2 = "P" and ld_percento <> 0 then
			ld_variazione_2 = ld_variazione_2 + round( ( (ld_variazione_2 * ld_percento) / 100 ),4)
			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_2,is_cod_valuta,ld_variazione_2,ls_messaggio)
		end if			
		if ls_flag_sconto_mag_prezzo_3 = "P" and ld_percento <> 0 then
			ld_variazione_3 = ld_variazione_3 + round( ( (ld_variazione_3 * ld_percento) / 100 ),4)
			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_3,is_cod_valuta,ld_variazione_3,ls_messaggio)
		end if			
		if ls_flag_sconto_mag_prezzo_4 = "P" and ld_percento <> 0 then
			ld_variazione_4 = ld_variazione_4 + round( ( (ld_variazione_4 * ld_percento) / 100 ),4)
			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_4,is_cod_valuta,ld_variazione_4,ls_messaggio)
		end if			
		if ls_flag_sconto_mag_prezzo_5 = "P" and ld_percento <> 0 then
			ld_variazione_5 = ld_variazione_5 + round( ( (ld_variazione_5 * ld_percento) / 100 ),4)
			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_5,is_cod_valuta,ld_variazione_5,ls_messaggio)
		end if			
	
		// se gestione listini comune
		if ls_glc = "S" then
			
			if lb_listino_comune then
				
				// inserimento nel listino COMUNE
				insert into listini_ven_comune (cod_azienda,
												cod_tipo_listino_prodotto,
												cod_valuta,
												data_inizio_val,
												progressivo,
												cod_cat_mer, 
												cod_prodotto,   
												cod_categoria,   
												cod_cliente,   
												des_listino_vendite,   
												minimo_fatt_altezza,   
												minimo_fatt_larghezza,   
												minimo_fatt_profondita,   
												minimo_fatt_superficie,   
												minimo_fatt_volume,   
												flag_sconto_a_parte,   
												flag_tipo_scaglioni,   
												scaglione_1,   
												scaglione_2,   
												scaglione_3,   
												scaglione_4,   
												scaglione_5,   
												flag_sconto_mag_prezzo_1,   
												flag_sconto_mag_prezzo_2,   
												flag_sconto_mag_prezzo_3,   
												flag_sconto_mag_prezzo_4,   
												flag_sconto_mag_prezzo_5,   
												variazione_1,   
												variazione_2,   
												variazione_3,   
												variazione_4,   
												variazione_5,   
												flag_origine_prezzo_1,   
												flag_origine_prezzo_2,   
												flag_origine_prezzo_3,   
												flag_origine_prezzo_4,   
												flag_origine_prezzo_5 )
									values ( :s_cs_xx.cod_azienda,
												:is_tipo_listino_prodotto,
												:is_cod_valuta,
												:ldt_data_inizio_val,
												:ll_progressivo,
												:ls_cod_cat_mer,   
												:ls_cod_prodotto,   
												:ls_cod_categoria,   
												:ls_cod_cliente,   
												:ls_des_listino_vendite,   
												:ld_minimo_fatt_altezza,   
												:ld_minimo_fatt_larghezza,   
												:ld_minimo_fatt_profondita,   
												:ld_minimo_fatt_superficie,   
												:ld_minimo_fatt_volume,   
												:ls_flag_sconto_a_parte,   
												:ls_flag_tipo_scaglioni,   
												:ld_scaglione_1,   
												:ld_scaglione_2,   
												:ld_scaglione_3,   
												:ld_scaglione_4,   
												:ld_scaglione_5,   
												:ls_flag_sconto_mag_prezzo_1,   
												:ls_flag_sconto_mag_prezzo_2,   
												:ls_flag_sconto_mag_prezzo_3,   
												:ls_flag_sconto_mag_prezzo_4,   
												:ls_flag_sconto_mag_prezzo_5,   
												:ld_variazione_1,   
												:ld_variazione_2,   
												:ld_variazione_3,   
												:ld_variazione_4,   
												:ld_variazione_5,   
												:ls_flag_origine_prezzo_1,   
												:ls_flag_origine_prezzo_2,   
												:ls_flag_origine_prezzo_3,   
												:ls_flag_origine_prezzo_4,   
												:ls_flag_origine_prezzo_5  );
														
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite. Dettaglio: " + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		else
			
			insert into listini_ven_locale (cod_azienda,
											cod_tipo_listino_prodotto,
											cod_valuta,
											data_inizio_val,
											progressivo,
											cod_cat_mer, 
											cod_prodotto,   
											cod_categoria,   
											cod_cliente,   
											des_listino_vendite,   
											minimo_fatt_altezza,   
											minimo_fatt_larghezza,   
											minimo_fatt_profondita,   
											minimo_fatt_superficie,   
											minimo_fatt_volume,   
											flag_sconto_a_parte,   
											flag_tipo_scaglioni,   
											scaglione_1,   
											scaglione_2,   
											scaglione_3,   
											scaglione_4,   
											scaglione_5,   
											flag_sconto_mag_prezzo_1,   
											flag_sconto_mag_prezzo_2,   
											flag_sconto_mag_prezzo_3,   
											flag_sconto_mag_prezzo_4,   
											flag_sconto_mag_prezzo_5,   
											variazione_1,   
											variazione_2,   
											variazione_3,   
											variazione_4,   
											variazione_5,   
											flag_origine_prezzo_1,   
											flag_origine_prezzo_2,   
											flag_origine_prezzo_3,   
											flag_origine_prezzo_4,   
											flag_origine_prezzo_5 )
								values ( :s_cs_xx.cod_azienda,
											:is_tipo_listino_prodotto,
											:is_cod_valuta,
											:ldt_data_inizio_val,
											:ll_progressivo,
											:ls_cod_cat_mer,   
											:ls_cod_prodotto,   
											:ls_cod_categoria,   
											:ls_cod_cliente,   
											:ls_des_listino_vendite,   
											:ld_minimo_fatt_altezza,   
											:ld_minimo_fatt_larghezza,   
											:ld_minimo_fatt_profondita,   
											:ld_minimo_fatt_superficie,   
											:ld_minimo_fatt_volume,   
											:ls_flag_sconto_a_parte,   
											:ls_flag_tipo_scaglioni,   
											:ld_scaglione_1,   
											:ld_scaglione_2,   
											:ld_scaglione_3,   
											:ld_scaglione_4,   
											:ld_scaglione_5,   
											:ls_flag_sconto_mag_prezzo_1,   
											:ls_flag_sconto_mag_prezzo_2,   
											:ls_flag_sconto_mag_prezzo_3,   
											:ls_flag_sconto_mag_prezzo_4,   
											:ls_flag_sconto_mag_prezzo_5,   
											:ld_variazione_1,   
											:ld_variazione_2,   
											:ld_variazione_3,   
											:ld_variazione_4,   
											:ld_variazione_5,   
											:ls_flag_origine_prezzo_1,   
											:ls_flag_origine_prezzo_2,   
											:ls_flag_origine_prezzo_3,   
											:ls_flag_origine_prezzo_4,   
											:ls_flag_origine_prezzo_5  );
													
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite. Dettaglio: " + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	else
	
			insert into listini_vendite (cod_azienda,
											cod_tipo_listino_prodotto,
											cod_valuta,
											data_inizio_val,
											progressivo,
											cod_cat_mer, 
											cod_prodotto,   
											cod_categoria,   
											cod_cliente,   
											des_listino_vendite,   
											minimo_fatt_altezza,   
											minimo_fatt_larghezza,   
											minimo_fatt_profondita,   
											minimo_fatt_superficie,   
											minimo_fatt_volume,   
											flag_sconto_a_parte,   
											flag_tipo_scaglioni,   
											scaglione_1,   
											scaglione_2,   
											scaglione_3,   
											scaglione_4,   
											scaglione_5,   
											flag_sconto_mag_prezzo_1,   
											flag_sconto_mag_prezzo_2,   
											flag_sconto_mag_prezzo_3,   
											flag_sconto_mag_prezzo_4,   
											flag_sconto_mag_prezzo_5,   
											variazione_1,   
											variazione_2,   
											variazione_3,   
											variazione_4,   
											variazione_5,   
											flag_origine_prezzo_1,   
											flag_origine_prezzo_2,   
											flag_origine_prezzo_3,   
											flag_origine_prezzo_4,   
											flag_origine_prezzo_5 )
								values ( :s_cs_xx.cod_azienda,
											:is_tipo_listino_prodotto,
											:is_cod_valuta,
											:ldt_data_inizio_val,
											:ll_progressivo,
											:ls_cod_cat_mer,   
											:ls_cod_prodotto,   
											:ls_cod_categoria,   
											:ls_cod_cliente,   
											:ls_des_listino_vendite,   
											:ld_minimo_fatt_altezza,   
											:ld_minimo_fatt_larghezza,   
											:ld_minimo_fatt_profondita,   
											:ld_minimo_fatt_superficie,   
											:ld_minimo_fatt_volume,   
											:ls_flag_sconto_a_parte,   
											:ls_flag_tipo_scaglioni,   
											:ld_scaglione_1,   
											:ld_scaglione_2,   
											:ld_scaglione_3,   
											:ld_scaglione_4,   
											:ld_scaglione_5,   
											:ls_flag_sconto_mag_prezzo_1,   
											:ls_flag_sconto_mag_prezzo_2,   
											:ls_flag_sconto_mag_prezzo_3,   
											:ls_flag_sconto_mag_prezzo_4,   
											:ls_flag_sconto_mag_prezzo_5,   
											:ld_variazione_1,   
											:ld_variazione_2,   
											:ld_variazione_3,   
											:ld_variazione_4,   
											:ld_variazione_5,   
											:ls_flag_origine_prezzo_1,   
											:ls_flag_origine_prezzo_2,   
											:ls_flag_origine_prezzo_3,   
											:ls_flag_origine_prezzo_4,   
											:ls_flag_origine_prezzo_5  );
													
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite. Dettaglio: " + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	
	
	select count(distinct cod_versione)
		into :ll_count_versione
	   from listini_produzione
     where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
	    and cod_valuta = :is_cod_valuta
	    and data_inizio_val = :idt_data_inizio_val
	    and progressivo = :il_progressivo;
		 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Erroe in lettura dati dalla tabella Listini Produzione. Dettaglio: " + sqlca.sqlerrtext)
		rollback;
		return
	elseif sqlca.sqlcode = 100 or isnull(ll_count_versione) then
		ll_count_versione = 0
	end if
	
	if ll_count_versione > 1 then
		g_mb.messagebox("Apice","Nel listino di origine sono presenti più versioni.~r~nCorreggere i dati e riprovare")
		rollback;
		return
	end if
	
	declare cu_lis_produzione cursor for
	 select cod_prodotto_listino,
	 		  cod_versione,
			  prog_listino_produzione
	   from listini_produzione
     where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
	    and cod_valuta = :is_cod_valuta
	    and data_inizio_val = :idt_data_inizio_val
	    and progressivo = :il_progressivo;
	
	open cu_lis_produzione;
	do while true
		fetch cu_lis_produzione into :ls_cod_prodotto_listino, :ls_cod_versione, :ll_prog_listino_produzione;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Erroe in lettura dati dalla tabella Listini Produzione. Dettaglio: " + sqlca.sqlerrtext)
			rollback;
			return
		end if			
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = 0 then
				  select cod_versione,   
							prog_listino_produzione,   
							cod_prodotto_padre,   
							num_sequenza,   
							cod_prodotto_figlio,   
							cod_gruppo_variante,   
							progr,   
							cod_prodotto,   
							des_listino_produzione,   
							minimo_fatt_altezza,   
							minimo_fatt_larghezza,   
							minimo_fatt_profondita,   
							minimo_fatt_superficie,   
							minimo_fatt_volume,   
							flag_sconto_a_parte,   
							flag_tipo_scaglioni,   
							scaglione_1,   
							scaglione_2,   
							scaglione_3,   
							scaglione_4,   
							scaglione_5,   
							flag_sconto_mag_prezzo_1,   
							flag_sconto_mag_prezzo_2,   
							flag_sconto_mag_prezzo_3,   
							flag_sconto_mag_prezzo_4,   
							flag_sconto_mag_prezzo_5,   
							variazione_1,   
							variazione_2,   
							variazione_3,   
							variazione_4,   
							variazione_5,   
							flag_origine_prezzo_1,   
							flag_origine_prezzo_2,   
							flag_origine_prezzo_3,   
							flag_origine_prezzo_4,   
							flag_origine_prezzo_5,
							provvigione_1,
							provvigione_2
					INTO  :ls_cod_versione_1,   
							:ll_prog_listino_prod_1,   
							:ls_cod_prodotto_padre,   
							:ll_num_seq_1,   
							:ls_cod_prodotto_figlio,   
							:ls_cod_gruppo_variante,   
							:ll_progr,   
							:ls_cod_prodotto_1,   
							:ls_des_listino_produzione,   
							:ld_minimo_fatt_altezza,   
							:ld_minimo_fatt_larghezza,   
							:ld_minimo_fatt_profondita,   
							:ld_minimo_fatt_superficie,   
							:ld_minimo_fatt_volume,   
							:ls_flag_sconto_a_parte,   
							:ls_flag_tipo_scaglioni,   
							:ld_scaglione_1,   
							:ld_scaglione_2,   
							:ld_scaglione_3,   
							:ld_scaglione_4,   
							:ld_scaglione_5,   
							:ls_flag_sconto_mag_prezzo_1,   
							:ls_flag_sconto_mag_prezzo_2,   
							:ls_flag_sconto_mag_prezzo_3,   
							:ls_flag_sconto_mag_prezzo_4,   
							:ls_flag_sconto_mag_prezzo_5,   
							:ld_variazione_1,   
							:ld_variazione_2,   
							:ld_variazione_3,   
							:ld_variazione_4,   
							:ld_variazione_5,   
							:ls_flag_origine_prezzo_1,   
							:ls_flag_origine_prezzo_2,   
							:ls_flag_origine_prezzo_3,   
							:ls_flag_origine_prezzo_4,   
							:ls_flag_origine_prezzo_5,
							:ld_provvigione_1,
							:ld_provvigione_2
					 from listini_produzione
				   where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
					  and cod_valuta = :is_cod_valuta
					  and data_inizio_val = :idt_data_inizio_val
					  and progressivo = :il_progressivo
					  and cod_prodotto_listino = :ls_cod_prodotto_listino
					  and cod_versione = :ls_cod_versione
					  and prog_listino_produzione = :ll_prog_listino_produzione;

			if ls_flag_sconto_mag_prezzo_1 = "P" and ld_percento <> 0 then
				ld_variazione_1 = ld_variazione_1 + round( ( (ld_variazione_1 * ld_percento) / 100 ),4)
   			luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_1,is_cod_valuta,ld_variazione_1,ls_messaggio)				
			end if			
			if ls_flag_sconto_mag_prezzo_2 = "P" and ld_percento <> 0 then
				ld_variazione_2 = ld_variazione_2 + round( ( (ld_variazione_2 * ld_percento) / 100 ),4)
				luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_2,is_cod_valuta,ld_variazione_2,ls_messaggio)
		   end if			
			if ls_flag_sconto_mag_prezzo_3 = "P" and ld_percento <> 0 then
				ld_variazione_3 = ld_variazione_3 + round( ( (ld_variazione_3 * ld_percento) / 100 ),4)
				luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_3,is_cod_valuta,ld_variazione_3,ls_messaggio)
			end if			
			if ls_flag_sconto_mag_prezzo_4 = "P" and ld_percento <> 0 then
				ld_variazione_4 = ld_variazione_4 + round( ( (ld_variazione_4 * ld_percento) / 100 ),4)
				luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_4,is_cod_valuta,ld_variazione_4,ls_messaggio)
			end if			
			if ls_flag_sconto_mag_prezzo_5 = "P" and ld_percento <> 0 then
				ld_variazione_5 = ld_variazione_5 + round( ( (ld_variazione_5 * ld_percento) / 100 ),4)
				luo_condizioni_cliente.uof_arrotonda_prezzo(ld_variazione_5,is_cod_valuta,ld_variazione_5,ls_messaggio)
			end if			
			
			SELECT cod_versione  
			INTO   :ls_cod_versione_dest
			FROM   distinta_padri  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda  AND  
					 cod_prodotto = :ls_cod_prodotto AND  
					 flag_predefinita = 'S' ;
			if isnull(ls_cod_versione_dest) then
				ls_cod_versione_dest = ls_cod_versione_1
				if g_mb.messagebox("Apice", "Attenzione!! Per il prodotto "+ls_cod_prodotto+" manca la versione predefinita della distinta base. Proseguo con la versione del prodotto di origine?",Question!,yesno!, 2) = 2 then
					rollback;
					return
				end if
			end if
			
			// se gestione listini comune
			if ls_glc = "S" then
				
				if lb_listino_comune then
					
					// inserimento nel listino COMUNE
					insert into	listini_prod_comune (cod_azienda,   
									cod_tipo_listino_prodotto,   
									cod_valuta,   
									data_inizio_val,   
									progressivo,   
									cod_prodotto_listino,   
									cod_versione,   
									prog_listino_produzione,   
									cod_prodotto_padre,   
									num_sequenza,   
									cod_prodotto_figlio,   
									cod_gruppo_variante,   
									progr,   
									cod_prodotto,   
									des_listino_produzione,   
									minimo_fatt_altezza,   
									minimo_fatt_larghezza,   
									minimo_fatt_profondita,   
									minimo_fatt_superficie,   
									minimo_fatt_volume,   
									flag_sconto_a_parte,   
									flag_tipo_scaglioni,   
									scaglione_1,   
									scaglione_2,   
									scaglione_3,   
									scaglione_4,   
									scaglione_5,   
									flag_sconto_mag_prezzo_1,   
									flag_sconto_mag_prezzo_2,   
									flag_sconto_mag_prezzo_3,   
									flag_sconto_mag_prezzo_4,   
									flag_sconto_mag_prezzo_5,   
									variazione_1,   
									variazione_2,   
									variazione_3,   
									variazione_4,   
									variazione_5,   
									flag_origine_prezzo_1,   
									flag_origine_prezzo_2,   
									flag_origine_prezzo_3,   
									flag_origine_prezzo_4,   
									flag_origine_prezzo_5,
									provvigione_1,
									provvigione_2)
						  values(:s_cs_xx.cod_azienda,   
									:is_tipo_listino_prodotto,
									:is_cod_valuta,
									:ldt_data_inizio_val,
									:ll_progressivo,				  
									:ls_cod_prodotto,   
									:ls_cod_versione_dest,   
									:ll_prog_listino_prod_1,   
									:ls_cod_prodotto_padre,   
									:ll_num_seq_1,   
									:ls_cod_prodotto_figlio,   
									:ls_cod_gruppo_variante,   
									:ll_progr,   
									:ls_cod_prodotto_1,   
									:ls_des_listino_produzione,   
									:ld_minimo_fatt_altezza,   
									:ld_minimo_fatt_larghezza,   
									:ld_minimo_fatt_profondita,   
									:ld_minimo_fatt_superficie,   
									:ld_minimo_fatt_volume,   
									:ls_flag_sconto_a_parte,   
									:ls_flag_tipo_scaglioni,   
									:ld_scaglione_1,   
									:ld_scaglione_2,   
									:ld_scaglione_3,   
									:ld_scaglione_4,   
									:ld_scaglione_5,   
									:ls_flag_sconto_mag_prezzo_1,   
									:ls_flag_sconto_mag_prezzo_2,   
									:ls_flag_sconto_mag_prezzo_3,   
									:ls_flag_sconto_mag_prezzo_4,   
									:ls_flag_sconto_mag_prezzo_5,   
									:ld_variazione_1,   
									:ld_variazione_2,   
									:ld_variazione_3,   
									:ld_variazione_4,   
									:ld_variazione_5,   
									:ls_flag_origine_prezzo_1,   
									:ls_flag_origine_prezzo_2,   
									:ls_flag_origine_prezzo_3,   
									:ls_flag_origine_prezzo_4,   
									:ls_flag_origine_prezzo_5,
									:ld_provvigione_1,
									:ld_provvigione_2);
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Produzione. Dettaglio: " + sqlca.sqlerrtext)
						rollback;
						return
					end if	
					
				else
					
					insert into	listini_prod_locale (cod_azienda,   
									cod_tipo_listino_prodotto,   
									cod_valuta,   
									data_inizio_val,   
									progressivo,   
									cod_prodotto_listino,   
									cod_versione,   
									prog_listino_produzione,   
									cod_prodotto_padre,   
									num_sequenza,   
									cod_prodotto_figlio,   
									cod_gruppo_variante,   
									progr,   
									cod_prodotto,   
									des_listino_produzione,   
									minimo_fatt_altezza,   
									minimo_fatt_larghezza,   
									minimo_fatt_profondita,   
									minimo_fatt_superficie,   
									minimo_fatt_volume,   
									flag_sconto_a_parte,   
									flag_tipo_scaglioni,   
									scaglione_1,   
									scaglione_2,   
									scaglione_3,   
									scaglione_4,   
									scaglione_5,   
									flag_sconto_mag_prezzo_1,   
									flag_sconto_mag_prezzo_2,   
									flag_sconto_mag_prezzo_3,   
									flag_sconto_mag_prezzo_4,   
									flag_sconto_mag_prezzo_5,   
									variazione_1,   
									variazione_2,   
									variazione_3,   
									variazione_4,   
									variazione_5,   
									flag_origine_prezzo_1,   
									flag_origine_prezzo_2,   
									flag_origine_prezzo_3,   
									flag_origine_prezzo_4,   
									flag_origine_prezzo_5,
									provvigione_1,
									provvigione_2)
						  values(:s_cs_xx.cod_azienda,   
									:is_tipo_listino_prodotto,
									:is_cod_valuta,
									:ldt_data_inizio_val,
									:ll_progressivo,				  
									:ls_cod_prodotto,   
									:ls_cod_versione_dest,   
									:ll_prog_listino_prod_1,   
									:ls_cod_prodotto_padre,   
									:ll_num_seq_1,   
									:ls_cod_prodotto_figlio,   
									:ls_cod_gruppo_variante,   
									:ll_progr,   
									:ls_cod_prodotto_1,   
									:ls_des_listino_produzione,   
									:ld_minimo_fatt_altezza,   
									:ld_minimo_fatt_larghezza,   
									:ld_minimo_fatt_profondita,   
									:ld_minimo_fatt_superficie,   
									:ld_minimo_fatt_volume,   
									:ls_flag_sconto_a_parte,   
									:ls_flag_tipo_scaglioni,   
									:ld_scaglione_1,   
									:ld_scaglione_2,   
									:ld_scaglione_3,   
									:ld_scaglione_4,   
									:ld_scaglione_5,   
									:ls_flag_sconto_mag_prezzo_1,   
									:ls_flag_sconto_mag_prezzo_2,   
									:ls_flag_sconto_mag_prezzo_3,   
									:ls_flag_sconto_mag_prezzo_4,   
									:ls_flag_sconto_mag_prezzo_5,   
									:ld_variazione_1,   
									:ld_variazione_2,   
									:ld_variazione_3,   
									:ld_variazione_4,   
									:ld_variazione_5,   
									:ls_flag_origine_prezzo_1,   
									:ls_flag_origine_prezzo_2,   
									:ls_flag_origine_prezzo_3,   
									:ls_flag_origine_prezzo_4,   
									:ls_flag_origine_prezzo_5,
									:ld_provvigione_1,
									:ld_provvigione_2);
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Produzione. Dettaglio: " + sqlca.sqlerrtext)
						rollback;
						return
					end if	
					
				end if
				
			else		// niente listino comune
				
				insert into	listini_produzione (cod_azienda,   
								cod_tipo_listino_prodotto,   
								cod_valuta,   
								data_inizio_val,   
								progressivo,   
								cod_prodotto_listino,   
								cod_versione,   
								prog_listino_produzione,   
								cod_prodotto_padre,   
								num_sequenza,   
								cod_prodotto_figlio,   
								cod_gruppo_variante,   
								progr,   
								cod_prodotto,   
								des_listino_produzione,   
								minimo_fatt_altezza,   
								minimo_fatt_larghezza,   
								minimo_fatt_profondita,   
								minimo_fatt_superficie,   
								minimo_fatt_volume,   
								flag_sconto_a_parte,   
								flag_tipo_scaglioni,   
								scaglione_1,   
								scaglione_2,   
								scaglione_3,   
								scaglione_4,   
								scaglione_5,   
								flag_sconto_mag_prezzo_1,   
								flag_sconto_mag_prezzo_2,   
								flag_sconto_mag_prezzo_3,   
								flag_sconto_mag_prezzo_4,   
								flag_sconto_mag_prezzo_5,   
								variazione_1,   
								variazione_2,   
								variazione_3,   
								variazione_4,   
								variazione_5,   
								flag_origine_prezzo_1,   
								flag_origine_prezzo_2,   
								flag_origine_prezzo_3,   
								flag_origine_prezzo_4,   
								flag_origine_prezzo_5,
								provvigione_1,
								provvigione_2)
					  values(:s_cs_xx.cod_azienda,   
								:is_tipo_listino_prodotto,
								:is_cod_valuta,
								:ldt_data_inizio_val,
								:ll_progressivo,				  
								:ls_cod_prodotto,   
								:ls_cod_versione_dest,   
								:ll_prog_listino_prod_1,   
								:ls_cod_prodotto_padre,   
								:ll_num_seq_1,   
								:ls_cod_prodotto_figlio,   
								:ls_cod_gruppo_variante,   
								:ll_progr,   
								:ls_cod_prodotto_1,   
								:ls_des_listino_produzione,   
								:ld_minimo_fatt_altezza,   
								:ld_minimo_fatt_larghezza,   
								:ld_minimo_fatt_profondita,   
								:ld_minimo_fatt_superficie,   
								:ld_minimo_fatt_volume,   
								:ls_flag_sconto_a_parte,   
								:ls_flag_tipo_scaglioni,   
								:ld_scaglione_1,   
								:ld_scaglione_2,   
								:ld_scaglione_3,   
								:ld_scaglione_4,   
								:ld_scaglione_5,   
								:ls_flag_sconto_mag_prezzo_1,   
								:ls_flag_sconto_mag_prezzo_2,   
								:ls_flag_sconto_mag_prezzo_3,   
								:ls_flag_sconto_mag_prezzo_4,   
								:ls_flag_sconto_mag_prezzo_5,   
								:ld_variazione_1,   
								:ld_variazione_2,   
								:ld_variazione_3,   
								:ld_variazione_4,   
								:ld_variazione_5,   
								:ls_flag_origine_prezzo_1,   
								:ls_flag_origine_prezzo_2,   
								:ls_flag_origine_prezzo_3,   
								:ls_flag_origine_prezzo_4,   
								:ls_flag_origine_prezzo_5,
								:ld_provvigione_1,
								:ld_provvigione_2);
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Produzione. Dettaglio: " + sqlca.sqlerrtext)
					rollback;
					return
				end if		
			end if	
					
		end if
	
	loop
	close cu_lis_produzione;
	
	declare cu_lis_dimensioni cursor for
	 select num_scaglione,
	 		  limite_dimensione_1,
			  limite_dimensione_2,
			  prog_range
	   from listini_vendite_dimensioni
     where cod_azienda = :s_cs_xx.cod_azienda
	    and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
	    and cod_valuta = :is_cod_valuta
	    and data_inizio_val = :idt_data_inizio_val
	    and progressivo = :il_progressivo;
	
	open cu_lis_dimensioni;	
	
	do while 1 = 1
		fetch cu_lis_dimensioni into :ld_num_scaglione, :ld_limite_dimensione_1, :ld_limite_dimensione_2, :ll_prog_range;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Erroe in lettura dati dalla tabella Listini Vendite Dimensioni. Dettaglio: " + sqlca.sqlerrtext)
			rollback;
			return
		end if			
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = 0 then
			li_risposta = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)
			
			// se gestione listini comune
			if ls_glc = "S" then
				
				if lb_listino_comune then
					
					// inserimento nel listino COMUNE
			
					if ld_percento <> 0 and (ls_db = "SYBASE_ASA" or ls_db = "SYBASE_ASE") then
						insert into	listini_ven_dim_comune (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range
																			flag_sconto_mag_prezzo,   
																			variazione + round( ( (variazione * :ld_percento) / 100 ),2),
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_rabge = :ll_prog_range;
					else
						insert into	listini_ven_dim_comune (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione + round( ( (variazione * :ld_percento) / 100 ),2),
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_range = :ll_prog_range;
					end if
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite Dimensione. Dettaglio: " + sqlca.sqlerrtext)
						rollback;
						return
					end if		
					
				else
					if ld_percento <> 0 and (ls_db = "SYBASE_ASA" or ls_db = "SYBASE_ASE") then
						insert into	listini_ven_dim_locale (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,  
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione + round( ( (variazione * :ld_percento) / 100 ),2),
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_range = :ll_prog_range;
					else
						insert into	listini_ven_dim_locale (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,  
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione + round( ( (variazione * :ld_percento) / 100 ),2),
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_range = :ll_prog_range;
					end if
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite Dimensione. Dettaglio: " + sqlca.sqlerrtext)
						rollback;
						return
					end if		
				end if
			else
					if ld_percento <> 0 and (ls_db = "SYBASE_ASA" or ls_db = "SYBASE_ASE") then
						insert into	listini_vendite_dimensioni (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione + round( ( (variazione * :ld_percento) / 100 ),2),
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_range = :ll_prog_range;
					else
						insert into	listini_vendite_dimensioni (cod_azienda,   
																			cod_tipo_listino_prodotto,   
																			cod_valuta,   
																			data_inizio_val,   
																			progressivo,   
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																			)
																  select :s_cs_xx.cod_azienda,   
																			:is_tipo_listino_prodotto,
																			:is_cod_valuta,
																			:ldt_data_inizio_val,
																			:ll_progressivo,		
																			num_scaglione,   
																			limite_dimensione_1,   
																			limite_dimensione_2,   
																			prog_range,
																			flag_sconto_mag_prezzo,   
																			variazione,
																			cod_variabile_1,
																			valore_stringa_1,
																			valore_numero_1,
																			cod_variabile_2,
																			valore_stringa_2,
																			valore_numero_2,
																			cod_variabile_3,
																			valore_stringa_3,
																			valore_numero_3,
																			cod_variabile_4,
																			valore_stringa_4,
																			valore_numero_4
																	 from listini_vendite_dimensioni
																	where cod_azienda = :s_cs_xx.cod_azienda
																	  and cod_tipo_listino_prodotto = :is_tipo_listino_prodotto
																	  and cod_valuta = :is_cod_valuta
																	  and data_inizio_val = :idt_data_inizio_val
																	  and progressivo = :il_progressivo
																	  and num_scaglione = :ld_num_scaglione
																	  and limite_dimensione_1 = :ld_limite_dimensione_1
																	  and limite_dimensione_2 = :ld_limite_dimensione_2
																	  and prog_range = :ll_prog_range;
					end if
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Apice", "Erroe in inserimento dati nella tabella Listini Vendite Dimensione. Dettaglio: " + sqlca.sqlerrtext)
						rollback;
						return
					end if					
				
				
			end if					
			
		end if	
	loop
	close cu_lis_dimensioni;
	
	g_mb.messagebox("Apice", "Creazione Tabelle avvenuta con successo")
	commit;
	close(w_richiesta_clie_data)
end if				
	
destroy luo_condizioni_cliente
end event

type cb_annulla from commandbutton within w_richiesta_clie_data
integer x = 1806
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(w_richiesta_clie_data)
end event

type dw_selezione from uo_cs_xx_dw within w_richiesta_clie_data
integer y = 20
integer width = 2583
integer height = 280
integer taborder = 20
string dataobject = "d_richiesta_cli_data"
boolean border = false
end type

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "data_inizio", today())
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
end choose
end event

