﻿$PBExportHeader$w_storico_provvigioni.srw
forward
global type w_storico_provvigioni from w_cs_xx_principale
end type
type dw_provvigioni_dettaglio from uo_cs_xx_dw within w_storico_provvigioni
end type
type dw_folder_search from u_folder within w_storico_provvigioni
end type
type dw_provvigioni_lista from uo_cs_xx_dw within w_storico_provvigioni
end type
type dw_ricerca from u_dw_search within w_storico_provvigioni
end type
end forward

global type w_storico_provvigioni from w_cs_xx_principale
integer width = 3493
integer height = 2148
string title = "Storico Provvisioni"
dw_provvigioni_dettaglio dw_provvigioni_dettaglio
dw_folder_search dw_folder_search
dw_provvigioni_lista dw_provvigioni_lista
dw_ricerca dw_ricerca
end type
global w_storico_provvigioni w_storico_provvigioni

type variables

end variables

forward prototypes
public function boolean wf_get_anno_num (ref long al_anno_registrazione, ref long al_num_registrazione)
public subroutine wf_imposta_ricerca ()
public subroutine wf_annulla_ricerca ()
public function string wf_fix_data (date adt_date)
end prototypes

public function boolean wf_get_anno_num (ref long al_anno_registrazione, ref long al_num_registrazione);/**
 * recupera anno e numero dalla lista
 **/
 
long ll_row

setnull(al_anno_registrazione)
setnull(al_num_registrazione)

ll_row = dw_provvigioni_lista.getrow()

if  ll_row > 0 then

	al_anno_registrazione = dw_provvigioni_lista.getitemnumber(ll_row, "anno_registrazione")
	al_num_registrazione = dw_provvigioni_lista.getitemnumber(ll_row, "num_registrazione")
	return true

else
	
	return false
end if
end function

public subroutine wf_imposta_ricerca ();/**
 * Tabelle
 * tes_storico_provvigioni join on det_storico_provvigioni
 **/

string ls_data

dw_ricerca.accepttext()

//ldt_date = dw_ricerca.getitemdate(1, "data_creazione_d")
//dw_ricerca.setitem(1, "data_creazione", wf_fix_data(ldt_date))
//
//ldt_date = dw_ricerca.getitemdate(1, "data_inizio_d")
//dw_ricerca.setitem(1, "data_inizio", wf_fix_data(ldt_date))
//
//ldt_date = dw_ricerca.getitemdate(1, "data_fine_d")
//dw_ricerca.setitem(1, "data_fine", wf_fix_data(ldt_date))

setnull(ls_data)
if not isnull(dw_ricerca.getitemdate(1, "data_creazione_d")) then
	ls_data = mid(string(dw_ricerca.getitemdate(1, "data_creazione_d")), 4, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_creazione_d")), 1, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_creazione_d")), 9, 2)	
end if	
dw_ricerca.setitem(1, "data_creazione", ls_data)

setnull(ls_data)
if not isnull(dw_ricerca.getitemdate(1, "data_inizio_d")) then
	ls_data = mid(string(dw_ricerca.getitemdate(1, "data_inizio_d")), 4, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_inizio_d")), 1, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_inizio_d")), 9, 2)	
end if	
dw_ricerca.setitem(1, "data_inizio", ls_data)

setnull(ls_data)
if not isnull(dw_ricerca.getitemdate(1, "data_fine_d")) then
	ls_data = mid(string(dw_ricerca.getitemdate(1, "data_fine_d")), 4, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_fine_d")), 1, 2)
	ls_data = ls_data + "/" + mid(string(dw_ricerca.getitemdate(1, "data_fine_d")), 9, 2)	
end if	
dw_ricerca.setitem(1, "data_fine", ls_data)

dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_provvigioni_lista.change_dw_current()
triggerevent("pc_retrieve")
end subroutine

public subroutine wf_annulla_ricerca ();/**
 * pulisce i campi
 **/
 
dw_ricerca.fu_Reset()

end subroutine

public function string wf_fix_data (date adt_date);string ls_adt_date, ls_date


if not isnull(adt_date) then
	
	ls_date = string(adt_date)
	
	ls_adt_date = mid(ls_date, 4, 2)
	ls_adt_date += "/" + mid(ls_date, 1, 2)
	ls_adt_date += "/" + mid(ls_date, 9, 2)	
end if	

return ls_adt_date

end function

on w_storico_provvigioni.create
int iCurrent
call super::create
this.dw_provvigioni_dettaglio=create dw_provvigioni_dettaglio
this.dw_folder_search=create dw_folder_search
this.dw_provvigioni_lista=create dw_provvigioni_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_provvigioni_dettaglio
this.Control[iCurrent+2]=this.dw_folder_search
this.Control[iCurrent+3]=this.dw_provvigioni_lista
this.Control[iCurrent+4]=this.dw_ricerca
end on

on w_storico_provvigioni.destroy
call super::destroy
destroy(this.dw_provvigioni_dettaglio)
destroy(this.dw_folder_search)
destroy(this.dw_provvigioni_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject l_objects[]

//set_w_options(c_closenosave)

//dw_ricerca.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_noresizedw + c_nohighlightselected + c_cursorrowpointer)

dw_provvigioni_lista.set_dw_key("cod_azienda")
dw_provvigioni_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen + c_noenablepopup, c_default)

dw_provvigioni_dettaglio.set_dw_key("cod_azienda")
dw_provvigioni_dettaglio.set_dw_key("anno_registrazione")
dw_provvigioni_dettaglio.set_dw_key("num_registrazione")
dw_provvigioni_dettaglio.set_dw_options(sqlca, dw_provvigioni_lista, c_scrollparent + c_nodelete, c_default)
										 
iuo_dw_main = dw_provvigioni_dettaglio

// folder											 
l_objects[1] = dw_provvigioni_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_provvigioni_lista.change_dw_current()

// Filtri
l_criteriacolumn[1] = "anno_registrazione"
l_criteriacolumn[2] = "num_registrazione"
l_criteriacolumn[3] = "cod_cliente"
l_criteriacolumn[4] = "cod_agente"
//l_criteriacolumn[5] = "data_creazione"
//l_criteriacolumn[6] = "data_inizio"
//l_criteriacolumn[7] = "data_fine"


l_searchtable[1] = "tes_storico_provvigioni"
l_searchtable[2] = "tes_storico_provvigioni"
l_searchtable[3] = "det_storico_provvigioni"
l_searchtable[4] = "det_storico_provvigioni"
//l_searchtable[5] = "tes_storico_provvigioni"
//l_searchtable[6] = "det_storico_provvigioni"
//l_searchtable[7] = "det_storico_provvigioni"

l_searchcolumn[1] = "anno_registrazione"
l_searchcolumn[2] = "num_registrazione"
l_searchcolumn[3] = "cod_cliente"
l_searchcolumn[4] = "cod_agente"
//l_searchcolumn[5] = "data_creazione_report"
//l_searchcolumn[6] = "data_inizio"
//l_searchcolumn[7] = "data_fine"


dw_ricerca.fu_wiredw(l_criteriacolumn[], dw_provvigioni_lista, l_searchtable[], l_searchcolumn[], SQLCA)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_provvigioni_dettaglio, &
                 "cod_agente", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_provvigioni_dettaglio from uo_cs_xx_dw within w_storico_provvigioni
integer x = 23
integer y = 700
integer width = 3406
integer height = 1320
integer taborder = 20
string dataobject = "d_det_storico_provvigioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_cod_agente
long ll_anno_registrazione, ll_num_registrazione

ls_cod_cliente = dw_ricerca.getitemstring(1, "cod_cliente")
ls_cod_agente = dw_ricerca.getitemstring(1, "cod_agente")
ll_anno_registrazione = dw_provvigioni_lista.getitemnumber(dw_provvigioni_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_provvigioni_lista.getitemnumber(dw_provvigioni_lista.getrow(), "num_registrazione")

if retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ls_cod_agente, ls_cod_cliente) < 0 then
   pcca.error = c_fatal
end if

dw_provvigioni_lista.change_dw_current()
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_progressivo

// recupero anno e numero registrazione dalla lista
wf_get_anno_num(ll_anno_registrazione, ll_num_registrazione)

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
	end if
	
	if isnull(this.getitemnumber(ll_i, "num_registrazione")) then
		this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
	end if
	
	
	if isnull(this.getitemnumber(ll_i, "progressivo")) then
		
		if isnull(ll_progressivo) or ll_progressivo < 1 then
			
			select max(progressivo) +1
			into :ll_progressivo
			from det_storico_provvigioni
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione;
				
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il calcolo del progressivo", sqlca)
			elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo < 1 then
				ll_progressivo = 1
			end if
			
		else
			ll_progressivo++
		end if
		
		this.setitem(ll_i, "progressivo", ll_progressivo)
		
	end if
next
end event

event itemchanged;call super::itemchanged;choose case dwo.name
		
	case "cod_agente"
		if isnull(data) or data = "" then
			setitem(row, "des_agente", "")
		else
			
			setitem(row, "des_agente", f_des_tabella("anag_agenti", "cod_agente='" + data + "'", "rag_soc_1"))
			
		end if
		
		
	case "cod_cliente"
		if isnull(data) or data = "" then
			setitem(row, "des_cliente", "")
		else
			
			setitem(row, "des_cliente", data  + " - "  + f_des_tabella("anag_cliente", "cod_cliente='" + data + "'", "rag_soc_1"))
			
		end if
		
end choose
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
				guo_ricerca.uof_ricerca_cliente(dw_provvigioni_dettaglio,"cod_cliente")
		end if
end choose
end event

event pcd_delete;call super::pcd_delete;g_mb.show("E' possibile eliminare solo l'intero storico e non una singola riga.")
return -1
end event

type dw_folder_search from u_folder within w_storico_provvigioni
integer x = 23
integer y = 20
integer width = 3406
integer height = 640
integer taborder = 10
end type

type dw_provvigioni_lista from uo_cs_xx_dw within w_storico_provvigioni
integer x = 160
integer y = 60
integer width = 3177
integer height = 560
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tes_storico_provvigioni"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;
if retrieve(s_cs_xx.cod_azienda) < 0 then
   pcca.error = c_fatal
end if
end event

event updatestart;call super::updatestart;long ll_i, ll_anno_registrazione, ll_num_registrazione


if deletedcount() > 0 then
	
	for ll_i = 1 to deletedcount()
		
		ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione",  Delete!, true)
		ll_num_registrazione = getitemnumber(ll_i, "num_registrazione",  Delete!, true)
		
		// cancello le referenze nelle scadenze forzate
		update scad_fat_ven_impresa
		set
			anno_reg_det_storico_prov = null,
			num_reg_det_storico_prov = null,
			prog_reg_det_storico_prov = null
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante l'aggionamento delle scadenze forzate")
			return -1
		end if
		// ---
		
		
		delete from det_storico_provvigioni
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante la cancellazione delle righe di dettaglio", sqlca)
			return -1
		end if
		
	next
	
end if
end event

event doubleclicked;call super::doubleclicked;string ls_des_periodo, ls_des_periodo_old
long ll_anno, ll_num

if row > 0 then
	if dwo.name = "des_periodo" then
		
		ls_des_periodo = getitemstring(row, "des_periodo")
		ls_des_periodo_old = ls_des_periodo
		if g_mb.prompt(ls_des_periodo, "Descrizione Periodo", 254) then
			
			if ls_des_periodo <> ls_des_periodo_old and not isnull(ls_des_periodo) and ls_des_periodo <> "" then
				
				ll_anno = getitemnumber(row, "anno_registrazione")
				ll_num = getitemnumber(row, "num_registrazione")
				
				update tes_storico_provvigioni
				set des_periodo = :ls_des_periodo
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno and
					num_registrazione = :ll_num;
					
				if sqlca.sqlcode = 0 then
					setitem(row, "des_periodo", ls_des_periodo)
					commit;
				else
					g_mb.error("Errore durante l'aggiornamento della descrizione periodo", sqlca)
					rollback;
				end if
				
			end if
			
		end if
		
	end if
end if
end event

type dw_ricerca from u_dw_search within w_storico_provvigioni
integer x = 160
integer y = 60
integer width = 3177
integer height = 560
integer taborder = 40
string dataobject = "d_ricerca_storico_provvigioni"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name
		
	case "b_cliente"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")


	case "b_cerca"
		wf_imposta_ricerca()

	case "b_annulla_ric"
		wf_annulla_ricerca()
		
end choose
end event

