﻿$PBExportHeader$w_tes_list_ven.srw
forward
global type w_tes_list_ven from w_cs_xx_principale
end type
type tab_dettaglio from tab within w_tes_list_ven
end type
type tab_dati from userobject within tab_dettaglio
end type
type dw_det from uo_cs_xx_dw within tab_dati
end type
type tab_dati from userobject within tab_dettaglio
dw_det dw_det
end type
type tab_note from userobject within tab_dettaglio
end type
type dw_det_2 from uo_cs_xx_dw within tab_note
end type
type tab_note from userobject within tab_dettaglio
dw_det_2 dw_det_2
end type
type tab_dettaglio from tab within w_tes_list_ven
tab_dati tab_dati
tab_note tab_note
end type
type tab_lista from tab within w_tes_list_ven
end type
type det_lista from userobject within tab_lista
end type
type dw_lista from uo_cs_xx_dw within det_lista
end type
type det_lista from userobject within tab_lista
dw_lista dw_lista
end type
type tab_ricerca from userobject within tab_lista
end type
type dw_ricerca from uo_std_dw within tab_ricerca
end type
type cb_1 from commandbutton within tab_ricerca
end type
type tab_ricerca from userobject within tab_lista
dw_ricerca dw_ricerca
cb_1 cb_1
end type
type tab_lista from tab within w_tes_list_ven
det_lista det_lista
tab_ricerca tab_ricerca
end type
end forward

global type w_tes_list_ven from w_cs_xx_principale
integer width = 3278
integer height = 2104
string title = "Listini Vendita"
tab_dettaglio tab_dettaglio
tab_lista tab_lista
end type
global w_tes_list_ven w_tes_list_ven

type variables
private:
	string is_desktop
	string is_sql_select
end variables

forward prototypes
public function window get_window ()
public subroutine wf_stato_pulsanti (boolean ab_status)
public function integer wf_duplica ()
public function string wf_get_verniciature (string as_cod_prodotto, integer ai_cod_versione)
end prototypes

public function window get_window ();return this
end function

public subroutine wf_stato_pulsanti (boolean ab_status);tab_dettaglio.tab_dati.dw_det.object.b_ricerca_prodotto.enabled = not ab_status	
tab_dettaglio.tab_dati.dw_det.object.b_ricerca_immagine.enabled = not ab_status
tab_dettaglio.tab_dati.dw_det.object.b_ricerca_legenda.enabled = not ab_status

tab_dettaglio.tab_dati.dw_det.object.b_dettagli.enabled = ab_status
tab_dettaglio.tab_dati.dw_det.object.b_verniciature.enabled = ab_status
tab_dettaglio.tab_dati.dw_det.object.b_duplica.enabled = ab_status

tab_dettaglio.tab_dati.dw_det.object.b_lingue.enabled = ab_status
tab_dettaglio.tab_note.dw_det_2.object.b_lingue_testata.enabled = ab_status
tab_dettaglio.tab_note.dw_det_2.object.b_lingue_piede.enabled = ab_status


end subroutine

public function integer wf_duplica ();string ls_cod_prodotto_listino
int li_cod_versione, li_cod_versione_dupl

ls_cod_prodotto_listino = tab_dettaglio.tab_dati.dw_det.getitemstring(1, "cod_prodotto_listino")
li_cod_versione = tab_dettaglio.tab_dati.dw_det.getitemnumber(1, "cod_versione")

if not g_mb.confirm("Creare una nuova versione del prodotto " + ls_cod_prodotto_listino + "?") then
	return 0
end if

// calcolo progressivo
select max(cod_versione)
into :li_cod_versione_dupl
from tes_list_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :ls_cod_prodotto_listino;
		 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo della nuova versione.", sqlca)
	return 0
elseif isnull(li_cod_versione_dupl) or li_cod_versione_dupl < 1 then
	li_cod_versione_dupl = 0
end if

li_cod_versione_dupl ++
		 
// ## TESTATA
insert into tes_list_ven (
	cod_azienda,   
	cod_prodotto_listino,   
	cod_versione,   
	des_versione,   
	path_immagine,   
	flag_versione_attiva,   
	data_creazione,
	des_prodotto_listino,
	prog_ordine_stampa
) select 
	cod_azienda,   
	cod_prodotto_listino,   
	:li_cod_versione_dupl,   
	des_versione,   
	path_immagine,   
	'N',   
	data_creazione,
	des_prodotto_listino,
	prog_ordine_stampa
from tes_list_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :ls_cod_prodotto_listino and
		 cod_versione = :li_cod_versione;
		 
if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.error("Errure durante la duplicazione della testata.", sqlca)
	return -1
end if

// ## VERNICIATURE
insert into tes_list_ven_vern (
	cod_azienda,   
	cod_prodotto_listino,   
	cod_versione,   
	cod_verniciatura,   
	num_posizione_griglia
) select 
	cod_azienda,   
	cod_prodotto_listino,   
	:li_cod_versione_dupl,   
	cod_verniciatura,   
	num_posizione_griglia
from tes_list_ven_vern
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :ls_cod_prodotto_listino and
		 cod_versione = :li_cod_versione;
		 
if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.error("Errure durante la duplicazione delle verniciature.", sqlca)
	return -1
end if


// ## DETTAGLIO
insert into det_list_ven (
	cod_azienda,   
	cod_prodotto_listino,   
	cod_versione,   
	prog_list_ven,   
	cod_prodotto_vendita,   
	num_posizione,   
	num_prog_posizione
) select
	cod_azienda,   
	cod_prodotto_listino,   
	:li_cod_versione_dupl,   
	prog_list_ven,   
	cod_prodotto_vendita,   
	num_posizione,   
	num_prog_posizione
from det_list_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :ls_cod_prodotto_listino and
		 cod_versione = :li_cod_versione;

if sqlca.sqlcode <> 0 then
	rollback;
	g_mb.error("Errure durante la duplicazione del dettaglio.", sqlca)
	return -1
end if

commit;
g_mb.success("Duplicazione del prodotto avvenuta con successo!")
	 
return 1
end function

public function string wf_get_verniciature (string as_cod_prodotto, integer ai_cod_versione);/**
 * stefanop
 * 20/06/2012
 *
 * Ritorna la stringa delle verniciature ordinate che sono state selezionate
 **/
 
return ""
end function

on w_tes_list_ven.create
int iCurrent
call super::create
this.tab_dettaglio=create tab_dettaglio
this.tab_lista=create tab_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_dettaglio
this.Control[iCurrent+2]=this.tab_lista
end on

on w_tes_list_ven.destroy
call super::destroy
destroy(this.tab_dettaglio)
destroy(this.tab_lista)
end on

event resize;tab_lista.width = newwidth - 40
tab_lista.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;tab_lista.move(20,20)
tab_lista.selecttab(2)

tab_lista.tab_ricerca.dw_ricerca.move(0,0)
tab_lista.tab_ricerca.dw_ricerca.insertrow(0)

tab_dettaglio.tab_note.dw_det_2.il_limit_campo_note = 32000

tab_lista.det_lista.dw_lista.set_dw_key("cod_azienda")
tab_lista.det_lista.dw_lista.set_dw_key("cod_prodotto_listino")
tab_lista.det_lista.dw_lista.set_dw_key("cod_versione")
tab_lista.det_lista.dw_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)

tab_dettaglio.tab_dati.dw_det.set_dw_options(sqlca, tab_lista.det_lista.dw_lista, c_sharedata, c_default)
tab_dettaglio.tab_note.dw_det_2.set_dw_options(sqlca, tab_lista.det_lista.dw_lista, c_sharedata, c_default)


iuo_dw_main =tab_lista.det_lista.dw_lista
tab_lista.det_lista.dw_lista.change_dw_current()

is_desktop = guo_functions.uof_get_user_desktop_folder()
is_sql_select = tab_lista.det_lista.dw_lista.getsqlselect()
end event

type tab_dettaglio from tab within w_tes_list_ven
event create ( )
event destroy ( )
integer x = 27
integer y = 688
integer width = 3182
integer height = 1288
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean focusonbuttondown = true
integer selectedtab = 1
tab_dati tab_dati
tab_note tab_note
end type

on tab_dettaglio.create
this.tab_dati=create tab_dati
this.tab_note=create tab_note
this.Control[]={this.tab_dati,&
this.tab_note}
end on

on tab_dettaglio.destroy
destroy(this.tab_dati)
destroy(this.tab_note)
end on

type tab_dati from userobject within tab_dettaglio
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3145
integer height = 1160
long backcolor = 12632256
string text = "Dati"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det dw_det
end type

on tab_dati.create
this.dw_det=create dw_det
this.Control[]={this.dw_det}
end on

on tab_dati.destroy
destroy(this.dw_det)
end on

type dw_det from uo_cs_xx_dw within tab_dati
integer x = 14
integer y = 24
integer width = 2747
integer height = 1120
integer taborder = 30
string dataobject = "d_tes_list_ven_det"
boolean border = false
boolean i_allowmodify = true
boolean i_allownew = true
boolean ib_dw_detail = true
end type

event buttonclicked;call super::buttonclicked;string ls_docpath, ls_docname[]
int li_rtn

if row>0 then
else
	return
end if

choose case dwo.name
	
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det, "cod_prodotto_listino")
		
	case "b_ricerca_immagine"
		if GetFileOpenName("Select File", ls_docpath, ls_docname[], "", "Images,*.bmp;*.gif;*.jpg;*.jpeg", is_desktop) = 1 then
			setitem(row, "path_immagine", ls_docpath)
		end if
		
	case "b_ricerca_legenda"
		if GetFileOpenName("Select File", ls_docpath, ls_docname[], "", "Images,*.bmp;*.gif;*.jpg;*.jpeg", is_desktop) = 1 then
			setitem(row, "path_legenda", ls_docpath)
		end if
		
	case "b_dettagli"
		if not isnull(getitemstring(row, "cod_prodotto_listino")) then
			window_open_parm(w_det_list_ven, -1, tab_lista.det_lista.dw_lista)
		end if
		
	case "b_verniciature"
		if not isnull(getitemstring(row, "cod_prodotto_listino")) then
			window_open_parm(w_tes_list_ven_vern, -1, tab_lista.det_lista.dw_lista)
		end if
		
	case "b_duplica"
		if not isnull(getitemstring(row, "cod_prodotto_listino")) then
			wf_duplica()
		end if
		
	case "b_lingue"
		if not isnull(getitemstring(row, "cod_prodotto_listino")) then
			s_cs_xx.parametri.parametro_s_1 = "D"
			window_open_parm(w_tes_list_ven_lingue, -1, tab_lista.det_lista.dw_lista)
		end if
				
end choose
end event

event updatestart;call super::updatestart;string ls_cod_prodotto_listino
int li_i, li_cod_versione

// stefanop 28/06/2012: se sto cancellando la testa cancello anche i dettaglio
for li_i = 1 to deletedcount()
	
	ls_cod_prodotto_listino = this.getitemstring(li_i, "cod_prodotto_listino", delete!, true)
	li_cod_versione = this.getitemnumber(li_i, "cod_versione", delete!, true)
	
	// controllo per sicurezza
	if isnull(ls_cod_prodotto_listino) or ls_cod_prodotto_listino = "" then continue
	
	// # Dettaglio
	delete from det_list_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :li_cod_versione;
				
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione dei dettagli.", sqlca)
		return 1
	end if
	
	// # Verniciature
	delete from tes_list_ven_vern
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :li_cod_versione;
				
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione dei dettagli.", sqlca)
		return 1
	end if
	
next
// ------
end event

event pcd_delete;call super::pcd_delete;//wf_stato_pulsanti(false)
end event

event pcd_modify;call super::pcd_modify;wf_stato_pulsanti(false)
end event

event pcd_new;call super::pcd_new;wf_stato_pulsanti(false)

setitem(getrow(), "data_creazione", today())
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
long ll_cod_versione

ls_cod_prodotto = tab_lista.det_lista.dw_lista.getitemstring( tab_lista.det_lista.dw_lista.getrow(), "cod_prodotto_listino")
ll_cod_versione = tab_lista.det_lista.dw_lista.getitemnumber( tab_lista.det_lista.dw_lista.getrow(), "cod_versione")

if retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ll_cod_versione) < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
		
next
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_prodotto
long ll_i, ll_cod_versione

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemnumber(ll_i, "cod_versione")) then
		
		ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto_listino")
		
		select max(cod_versione)
		into :ll_cod_versione
		from tes_list_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_listino = :ls_cod_prodotto;
				 
		if isnull(ll_cod_versione) or ll_cod_versione < 0 then
			ll_cod_versione = 0
		end if
		
		ll_cod_versione++
		
		this.setitem(ll_i, "cod_versione", ll_cod_versione)
	end if
		
next
end event

event pcd_view;call super::pcd_view;wf_stato_pulsanti(true)
end event

type tab_note from userobject within tab_dettaglio
integer x = 18
integer y = 112
integer width = 3145
integer height = 1160
long backcolor = 12632256
string text = "Note Testata e Piede"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_2 dw_det_2
end type

on tab_note.create
this.dw_det_2=create dw_det_2
this.Control[]={this.dw_det_2}
end on

on tab_note.destroy
destroy(this.dw_det_2)
end on

type dw_det_2 from uo_cs_xx_dw within tab_note
integer x = 41
integer y = 16
integer width = 2734
integer height = 1136
integer taborder = 11
string dataobject = "d_tes_list_ven_det_2"
boolean border = false
boolean ib_dw_detail = true
end type

event buttonclicked;call super::buttonclicked;


if row>0 then
else
	return
end if

choose case dwo.name
		
	case "b_lingue_testata", "b_lingue_piede"
		if not isnull(getitemstring(row, "cod_prodotto_listino")) then
			
			if dwo.name= "b_lingue_testata" then
				s_cs_xx.parametri.parametro_s_1 = "T"
			else
				s_cs_xx.parametri.parametro_s_1 = "P"
			end if
			
			window_open_parm(w_tes_list_ven_lingue, -1, tab_lista.det_lista.dw_lista)
		end if
				
end choose
end event

event pcd_new;call super::pcd_new;wf_stato_pulsanti(false)
end event

event pcd_modify;call super::pcd_modify;wf_stato_pulsanti(false)
end event

event pcd_view;call super::pcd_view;wf_stato_pulsanti(true)
end event

type tab_lista from tab within w_tes_list_ven
event ue_resize ( )
integer x = 27
integer y = 20
integer width = 3182
integer height = 640
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean focusonbuttondown = true
tabposition tabposition = tabsonleft!
integer selectedtab = 2
det_lista det_lista
tab_ricerca tab_ricerca
end type

event ue_resize();/*
 * 
 */
 
det_lista.dw_lista.move(0,0)
det_lista.dw_lista.resize(det_lista.width, det_lista.height)
end event

on tab_lista.create
this.det_lista=create det_lista
this.tab_ricerca=create tab_ricerca
this.Control[]={this.det_lista,&
this.tab_ricerca}
end on

on tab_lista.destroy
destroy(this.det_lista)
destroy(this.tab_ricerca)
end on

type det_lista from userobject within tab_lista
integer x = 133
integer y = 16
integer width = 3031
integer height = 608
long backcolor = 12632256
string text = "Lista"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista dw_lista
end type

on det_lista.create
this.dw_lista=create dw_lista
this.Control[]={this.dw_lista}
end on

on det_lista.destroy
destroy(this.dw_lista)
end on

type dw_lista from uo_cs_xx_dw within det_lista
integer y = 4
integer width = 3013
integer height = 600
integer taborder = 20
string dataobject = "d_tes_list_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_sql

tab_lista.tab_ricerca.dw_ricerca.accepttext()

ls_sql = is_sql_select + " WHERE tes_list_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' "

if not isnull(tab_lista.tab_ricerca.dw_ricerca.getitemstring(1, "cod_prodotto")) and tab_lista.tab_ricerca.dw_ricerca.getitemstring(1, "cod_prodotto") <> "" then
	ls_sql += " AND tes_list_ven.cod_prodotto_listino='" + tab_lista.tab_ricerca.dw_ricerca.getitemstring(1, "cod_prodotto") + "' "
end if

dw_lista.setsqlselect(ls_sql)
// Se non si reimposta PB perde le proprietà di update
Modify( "DataWindow.Table.UpdateTable = ~"tes_list_ven~"")

if retrieve() < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
		
next
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_prodotto
long ll_i, ll_cod_versione

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemnumber(ll_i, "cod_versione")) then
		
		ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto_listino")
		
		select max(cod_versione)
		into :ll_cod_versione
		from tes_list_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_listino = :ls_cod_prodotto;
				 
		if isnull(ll_cod_versione) or ll_cod_versione < 0 then
			ll_cod_versione = 0
		end if
		
		ll_cod_versione++
		
		this.setitem(ll_i, "cod_versione", ll_cod_versione)
	end if
		
next
end event

event updatestart;call super::updatestart;string ls_cod_prodotto_listino
int li_i, li_cod_versione

// stefanop 28/06/2012: se sto cancellando la testa cancello anche i dettaglio
for li_i = 1 to deletedcount()
	
	ls_cod_prodotto_listino = this.getitemstring(li_i, "cod_prodotto_listino", delete!, true)
	li_cod_versione = this.getitemnumber(li_i, "cod_versione", delete!, true)
	
	// controllo per sicurezza
	if isnull(ls_cod_prodotto_listino) or ls_cod_prodotto_listino = "" then continue
	
	// # Dettaglio
	delete from det_list_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :li_cod_versione;
				
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione dei dettagli.", sqlca)
		return 1
	end if
	
	// # Verniciature
	delete from tes_list_ven_vern
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_listino = :ls_cod_prodotto_listino and
				cod_versione = :li_cod_versione;
				
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la cancellazione dei dettagli.", sqlca)
		return 1
	end if
	
next
// ------
end event

event pcd_new;call super::pcd_new;wf_stato_pulsanti(false)

setitem(getrow(), "data_creazione", today())
end event

event pcd_modify;call super::pcd_modify;wf_stato_pulsanti(false)
end event

event pcd_view;call super::pcd_view;wf_stato_pulsanti(true)
end event

type tab_ricerca from userobject within tab_lista
integer x = 133
integer y = 16
integer width = 3031
integer height = 608
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
cb_1 cb_1
end type

on tab_ricerca.create
this.dw_ricerca=create dw_ricerca
this.cb_1=create cb_1
this.Control[]={this.dw_ricerca,&
this.cb_1}
end on

on tab_ricerca.destroy
destroy(this.dw_ricerca)
destroy(this.cb_1)
end on

type dw_ricerca from uo_std_dw within tab_ricerca
integer x = 27
integer y = 24
integer width = 2560
integer height = 360
integer taborder = 20
string dataobject = "d_tes_list_ven_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(tab_lista.tab_ricerca.dw_ricerca, "cod_prodotto")
		
end choose
end event

type cb_1 from commandbutton within tab_ricerca
integer x = 2199
integer y = 444
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ricerca"
end type

event clicked;tab_lista.selecttab(1)
get_window().triggerevent("pc_retrieve")
end event

