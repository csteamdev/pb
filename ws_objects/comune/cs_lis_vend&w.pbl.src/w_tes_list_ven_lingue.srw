﻿$PBExportHeader$w_tes_list_ven_lingue.srw
forward
global type w_tes_list_ven_lingue from w_cs_xx_principale
end type
type dw_tes_list_ven_lingue_lista from uo_cs_xx_dw within w_tes_list_ven_lingue
end type
end forward

global type w_tes_list_ven_lingue from w_cs_xx_principale
integer width = 2944
integer height = 2128
string title = "Listini Vendita - Traduzioni"
dw_tes_list_ven_lingue_lista dw_tes_list_ven_lingue_lista
end type
global w_tes_list_ven_lingue w_tes_list_ven_lingue

type variables

private:
	string		is_cod_prodotto_listino, is_flag_tipo_campo, is_des_tipo_campo
	long		il_cod_versione
	
end variables

on w_tes_list_ven_lingue.create
int iCurrent
call super::create
this.dw_tes_list_ven_lingue_lista=create dw_tes_list_ven_lingue_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_list_ven_lingue_lista
end on

on w_tes_list_ven_lingue.destroy
call super::destroy
destroy(this.dw_tes_list_ven_lingue_lista)
end on

event pc_setwindow;call super::pc_setwindow;is_flag_tipo_campo = s_cs_xx.parametri.parametro_s_1
s_cs_xx.parametri.parametro_s_1 = ""

choose case is_flag_tipo_campo
	case "D"
		is_des_tipo_campo = "Des Prodotto (stampa)"
		dw_tes_list_ven_lingue_lista.il_limit_campo_note = 100
	case "T"
		is_des_tipo_campo = "Note Testata (stampa)"
		dw_tes_list_ven_lingue_lista.il_limit_campo_note = 255
	case "P"
		is_des_tipo_campo = "Note Piede (stampa)"
		dw_tes_list_ven_lingue_lista.il_limit_campo_note = 255
end choose
		

dw_tes_list_ven_lingue_lista.set_dw_key("cod_azienda")
dw_tes_list_ven_lingue_lista.set_dw_key("cod_prodotto_listino")
dw_tes_list_ven_lingue_lista.set_dw_key("cod_versione")
dw_tes_list_ven_lingue_lista.set_dw_key("flag_tipo_campo")
dw_tes_list_ven_lingue_lista.set_dw_key("cod_lingua")
dw_tes_list_ven_lingue_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)

iuo_dw_main = dw_tes_list_ven_lingue_lista

end event

event pc_setddlb;call super::pc_setddlb;

f_po_loaddddw_dw(	dw_tes_list_ven_lingue_lista, &
							  "cod_lingua", &
							  sqlca, &
							  "tab_lingue", &
							  "cod_lingua", &
							  "des_lingua", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_tes_list_ven_lingue_lista from uo_cs_xx_dw within w_tes_list_ven_lingue
integer x = 23
integer y = 20
integer width = 2871
integer height = 1988
integer taborder = 10
string dataobject = "d_tes_list_ven_lingue"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long		ll_errore

is_cod_prodotto_listino = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto_listino")
il_cod_versione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cod_versione")

parent.title = "Traduzioni per "+is_des_tipo_campo+": Prodotto "+is_cod_prodotto_listino+" - Versione "+string(il_cod_versione, "#####0")

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_listino, il_cod_versione, is_flag_tipo_campo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()

	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if

	if isnull(this.getitemstring(ll_i, "cod_prodotto_listino")) or this.getitemstring(ll_i, "cod_prodotto_listino")="" then
		this.setitem(ll_i, "cod_prodotto_listino", is_cod_prodotto_listino)
	end if

	if isnull(this.getitemstring(ll_i, "flag_tipo_campo")) or this.getitemstring(ll_i, "flag_tipo_campo")="" then
		this.setitem(ll_i, "flag_tipo_campo", is_flag_tipo_campo)
	end if

	if isnull(this.getitemnumber(ll_i, "cod_versione")) then
		this.setitem(ll_i, "cod_versione", il_cod_versione)
	end if

next
end event

