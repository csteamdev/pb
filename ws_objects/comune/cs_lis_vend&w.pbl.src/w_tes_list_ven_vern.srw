﻿$PBExportHeader$w_tes_list_ven_vern.srw
forward
global type w_tes_list_ven_vern from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tes_list_ven_vern
end type
type cb_salva from commandbutton within w_tes_list_ven_vern
end type
end forward

global type w_tes_list_ven_vern from w_cs_xx_principale
integer width = 3662
integer height = 1160
string title = "Verniciature"
dw_lista dw_lista
cb_salva cb_salva
end type
global w_tes_list_ven_vern w_tes_list_ven_vern

type variables
private:
	datastore ids_store
	
	string is_cod_prodotto_listino
	int ii_cod_versione
end variables

forward prototypes
public subroutine wf_crea_datastore ()
public subroutine wf_reset_dw ()
public function integer wf_save ()
public function boolean wf_esiste_verniciatura (string as_cod_verniciatura)
end prototypes

public subroutine wf_crea_datastore ();/**
 * stefanop
 * 20/06/2012
 *
 * Creo il datastore e lo slavo in istanza così posso mantenere aperta la finestra e scorrere i record del padre
 **/
 
string ls_sql, ls_errore

ls_sql = "SELECT cod_verniciatura, des_prodotto, cod_veloce " + &
			"FROM tab_verniciatura " + &
			" JOIN anag_prodotti ON anag_prodotti.cod_azienda = tab_verniciatura.cod_azienda " + &
			"	AND anag_prodotti.cod_prodotto = tab_verniciatura.cod_verniciatura " + &
			"WHERE tab_verniciatura.cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
			"	AND tab_verniciatura.flag_blocco <> 'S'"
			
guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_errore)

if not isnull(ls_errore) and ls_errore <> "" then
	g_mb.error(ls_errore)
	
end if
end subroutine

public subroutine wf_reset_dw ();long ll_i, ll_count, ll_j

dw_lista.reset()

ll_count = ids_store.rowcount()

for ll_i = 1 to ll_count
	
	ll_j = dw_lista.insertrow(0)
	dw_lista.setitem(ll_j, "cod_verniciatura", ids_store.getitemstring(ll_i, 1))
	dw_lista.setitem(ll_j, "des_verniciatura", ids_store.getitemstring(ll_i, 2))
	dw_lista.setitem(ll_j, "cod_veloce", ids_store.getitemstring(ll_i, 3))
	
next
end subroutine

public function integer wf_save ();string ls_cod_verniciatura
int li_num_posizione_griglia
long ll_row = 0, ll_count = 0


if not g_mb.confirm("Salvare le modifiche apportate?") then
	return 0
end if

ll_count = dw_lista.rowcount()

do while ll_row <= ll_count

	ll_row = dw_lista.GetNextModified(ll_row, Primary!)
	
	if ll_row < 1 then exit
	
	ls_cod_verniciatura = dw_lista.getitemstring(ll_row, "cod_verniciatura")
	
	// cerco posizione
	for li_num_posizione_griglia = 1 to 9
		if dw_lista.getitemstring(ll_row, "pos_" + string(li_num_posizione_griglia)) = "S" then exit
	next
	
	if li_num_posizione_griglia < 0 or li_num_posizione_griglia > 9 then
		// è fuori range, quindi può essere che abbia tirato via la spunta da tutto e quindi la voglio eliminare
		
		delete from tes_list_ven_vern
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_listino = :is_cod_prodotto_listino and
				 cod_versione = :ii_cod_versione and
				 cod_verniciatura = :ls_cod_verniciatura;
				 
		if sqlca.sqlcode < 0 then
			rollback;
			g_mb.error("Errore durante la cancellazione della verniciatura " + ls_cod_verniciatura)
			return -1
		end if
		
		continue
	end if
	// ----
	
	if wf_esiste_verniciatura(ls_cod_verniciatura) then
		
		update tes_list_ven_vern
		set num_posizione_griglia = :li_num_posizione_griglia
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_listino = :is_cod_prodotto_listino and
				 cod_versione = :ii_cod_versione and
				 cod_verniciatura = :ls_cod_verniciatura;
		
	else
		
		insert into tes_list_ven_vern (
			cod_azienda,
			cod_prodotto_listino,
			cod_versione,
			cod_verniciatura,
			num_posizione_griglia
		) values (
			:s_cs_xx.cod_azienda,
			:is_cod_prodotto_listino, 
			:ii_cod_versione,
			:ls_cod_verniciatura,
			:li_num_posizione_griglia
		);
		
	end if
	
	if sqlca.sqlcode <> 0 then
		rollback;
		g_mb.error("Errore durante il salvataggio della verniciatura " + ls_cod_verniciatura, sqlca)
		return -1
	end if
	
loop

commit;

return 1
end function

public function boolean wf_esiste_verniciatura (string as_cod_verniciatura);/**
 * stefanop
 * 20/06/2012
 *
 * Verifico se il codice verniciatura esiste all'interno della tabella
 **/
 
string ls_cod_verniciatura

select cod_verniciatura
into :ls_cod_verniciatura
from tes_list_ven_vern
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_listino = :is_cod_prodotto_listino and
		 cod_versione = :ii_cod_versione and
		 cod_verniciatura = :as_cod_verniciatura;
		 
return sqlca.sqlcode = 0
		 
end function

on w_tes_list_ven_vern.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
this.cb_salva=create cb_salva
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
this.Control[iCurrent+2]=this.cb_salva
end on

on w_tes_list_ven_vern.destroy
call super::destroy
destroy(this.dw_lista)
destroy(this.cb_salva)
end on

event pc_setsize;call super::pc_setsize;
set_w_options(c_closenosave)

dw_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)

iuo_dw_main = dw_lista

wf_crea_datastore()
end event

event resize;
cb_salva.move(newwidth - cb_salva.width - 20, newheight - cb_salva.height - 20)

dw_lista.move(20,20)
dw_lista.resize(newwidth - 20, cb_salva.y - 40)
end event

type dw_lista from uo_cs_xx_dw within w_tes_list_ven_vern
integer x = 23
integer y = 20
integer width = 3589
integer height = 900
integer taborder = 30
string dataobject = "d_tes_list_ven_vern"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_sql, ls_cod_verniciatura
int li_i, li_count, li_num_pos, li_find
datastore lds_store

dw_lista.setredraw(false)

// Cancello la lista dei valori vecchi
wf_reset_dw()

is_cod_prodotto_listino = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_prodotto_listino")
ii_cod_versione = i_parentdw.getitemnumber(i_parentdw.getrow(), "cod_versione")

parent.title = "Verniciature prodotto: " + is_cod_prodotto_listino + " - Versione: " + string(ii_cod_versione)

ls_sql = "SELECT cod_verniciatura, num_posizione_griglia FROM tes_list_ven_vern " + &
			"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
			"	AND cod_prodotto_listino='" + is_cod_prodotto_listino + "' " + &
			"	AND cod_versione=" + string(ii_cod_versione)
			
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)

if li_count < 0 then
	pcca.error = c_fatal
	return -1
end if

for li_i = 1 to li_count
	
	ls_cod_verniciatura = lds_store.getitemstring(li_i, 1)
	li_num_pos = lds_store.getitemnumber(li_i, 2)
	
	li_find = dw_lista.find("cod_verniciatura='" + ls_cod_verniciatura + "'", 0, dw_lista.rowcount())
	
	if li_find > 0 then
		
		dw_lista.setitem(li_find, "pos_" + string(li_num_pos), "S")
		
	end if
	
next

dw_lista.setredraw(true)
dw_lista.resetupdate()

postevent("pcd_modify")
end event

event pcd_save;call super::pcd_save;
wf_save()
end event

type cb_salva from commandbutton within w_tes_list_ven_vern
integer x = 3177
integer y = 940
integer width = 411
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;wf_save()
end event

