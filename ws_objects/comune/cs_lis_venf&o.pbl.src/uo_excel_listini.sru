﻿$PBExportHeader$uo_excel_listini.sru
forward
global type uo_excel_listini from nonvisualobject
end type
end forward

global type uo_excel_listini from nonvisualobject
end type
global uo_excel_listini uo_excel_listini

type variables
OLEObject iole_excel
boolean ib_OleObjectVisible = true
string is_path_file, is_nome_file
boolean ib_inizio = true
long il_FontSize = 8
end variables

forward prototypes
public function integer uof_inizializza (string fs_fogli[])
public function string uof_nome_cella (long fl_riga, long fl_colonna)
public function integer uof_test_colori (string fs_foglio)
public function long uof_colore (string fs_colore)
public function integer uof_scrivi (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor)
public subroutine uof_bordi (ref oleobject fole, long fl_riga, long fl_colonna, boolean fb_bordi)
public function integer uof_save ()
public function integer uof_crea_foglio (string fs_foglio)
public function integer uof_inizio ()
public function integer uof_scrivi_dec (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor, string fs_format)
public function integer uof_scrivi_nota (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor)
public function integer uof_scrivi_codice (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor)
public function integer uof_open ()
public subroutine uof_leggi (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo)
public subroutine uof_visible (boolean fb_visible)
public subroutine uof_get_sheets (ref string fs_sheets[])
public function integer uof_close ()
public function integer uof_leggi_ottimistico (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref datetime fdt_valore, string fs_tipo)
public function integer uof_scrivi_data (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor, string fs_format)
public function integer uof_save_as (string fs_path)
public function integer uof_delete_sheet (string fs_sheet)
public function integer uof_close (string fs_path)
public function integer uof_save_as_format (string fs_path, integer fi_version)
end prototypes

public function integer uof_inizializza (string fs_fogli[]);//DONATO 02-02-2010
/*
Questa funziona inizializza il file excel:
- crea una istanza di excel 
	(visibile o invisibile in base alla variabile istanza ib_OleObjectVisible [default è true])
- con tanti fogli di lavoro quanti sono nel parametro fs_fogli[]
-il nome dato ai fogli di lavoro sono quelli specificati dalle stringhe
	nello stesso array fs_fogli[] e nello stesso ordine
*/

long	ll_errore, ll_count, ll_index

ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return -1
end if

//myoleobject.Visible = ib_OleObjectVisible
iole_excel.application.visible = ib_OleObjectVisible
iole_excel.Workbooks.Add

//iole_excel.Activeworkbook.SaveAs("c:\file1.xls")

//quanti fogli mi ritrovo?
ll_count = iole_excel.worksheets.count

//li cancello tutti tranne uno (perchè se li cancello tutti darebbe errore!!)
for ll_index = ll_count to 2 step -1
	iole_excel.worksheets(ll_index).delete
next

//adesso creo quelli che mi servono specificati in upperbound(fs_fogli[])
ll_count = upperbound(fs_fogli)

for ll_index = 1 to ll_count
	//il primo già esiste e non devo aggiungerlo
	if ll_index > 1 then
		iole_excel.worksheets.Add
	end if
next

//adesso rinomino i fogli come specificat in fs_fogli[] (nello stesso ordine)
for ll_index = 1 to ll_count
	iole_excel.worksheets(ll_index).name = fs_fogli[ll_index]
next
//iole_excel.worksheets(1).name = "pippo"


return 0
end function

public function string uof_nome_cella (long fl_riga, long fl_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public function integer uof_test_colori (string fs_foglio);OLEObject lole_foglio
long ll_index, ll_count

ll_count = 56
lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//textcolor
for ll_index = 1 to ll_count
	lole_foglio.cells[2,ll_index] = ll_index
	
	lole_foglio.range(uof_nome_cella(2, ll_index) + ":" +&
						uof_nome_cella(2 , ll_index) ).font.colorindex = ll_index
next

//backcolor
for ll_index = 1 to ll_count
	lole_foglio.cells[4,ll_index] = ll_index
	
	lole_foglio.range(uof_nome_cella(2, ll_index) + ":" +&
						uof_nome_cella(2 , ll_index) ).interior.colorindex = ll_index
next







return 0
end function

public function long uof_colore (string fs_colore);choose case Upper(fs_colore)
	case "NERO"
		return 1
	case "BIANCO"
		return 2
	case "ROSSO"
		return 3
	case "VERDE"
		return 4
	case "BLU"
		return 5
	case "GIALLO"
		return 6
	case "FUCSIA"
		return 7
	case "AZZURRO"
		return 8
	case "MARRONE"
		return 9
	case "VERDE SCURO"
		return 10
	case else
		return -1	//non fare niente
end choose
end function

public function integer uof_scrivi (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor);OLEObject lole_foglio
long ll_ret


lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//BOLD
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).font.bold = fb_grassetto
						
//GRANDEZZA FONT
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
						uof_nome_cella(fl_riga , fl_colonna) ).font.size = il_FontSize

//COLORE SFONDO
if isnumber(fs_backcolor) then
	ll_ret = long(fs_backcolor)
else
	ll_ret = uof_colore(fs_backcolor)
end if

if ll_ret > 0 then
	lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).interior.colorindex = ll_ret
end if

//BORDI
uof_bordi(lole_foglio, fl_riga, fl_colonna,fb_bordi)

lole_foglio.cells[fl_riga,fl_colonna] = fany_valore

if fl_colonna > 1 then lole_foglio.columns(fl_colonna).EntireColumn.AutoFit

return 0

end function

public subroutine uof_bordi (ref oleobject fole, long fl_riga, long fl_colonna, boolean fb_bordi);if fb_bordi then
	//BORDI
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(1).LineStyle = 1
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(1).Weight = 3
							
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(2).LineStyle = 1
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(2).Weight = 3
							
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(3).LineStyle = 1
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(3).Weight = 3
							
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(4).LineStyle = 1
	fole.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
							uof_nome_cella(fl_riga , fl_colonna) ).Borders(4).Weight = 3
end if
end subroutine

public function integer uof_save ();iole_excel.Activeworkbook.SaveAs(is_path_file + is_nome_file)

return 0
end function

public function integer uof_crea_foglio (string fs_foglio);//DONATO 02-02-2010
/*
- crea il foglio di lavoro specificato in fs_foglio
*/
long	ll_count, ll_index
//long	ll_existing_index

//vedo quanti ce ne sono (uno almeno ci sarà)
//ll_existing_index = iole_excel.worksheets.count

//devo distinguere se sono qui la prima volta
if ib_inizio then
	//sono all'inizio: allora il foglio da creare deve essere solo rinominato
	//iole_excel.worksheets(ll_existing_index).name = fs_foglio
	
	//il foglio viene creato sempre in prima posizione
	iole_excel.worksheets(1).name = fs_foglio
else
	//sono già stato qui: il foglio deve essere creato e poi rinominato
	iole_excel.worksheets.Add
	
	//ll_existing_index += 1
	//iole_excel.worksheets(ll_existing_index).name = fs_foglio
	
	//il foglio viene creato sempre in prima posizione
	iole_excel.worksheets(1).name = fs_foglio
end if

ib_inizio = false

return 0
end function

public function integer uof_inizio ();long ll_errore, ll_count, ll_index

//inizio
ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return -1
end if

//myoleobject.Visible = ib_OleObjectVisible
iole_excel.application.visible = ib_OleObjectVisible
iole_excel.Workbooks.Add

//quanti fogli mi ritrovo all'inizio?
ll_count = iole_excel.worksheets.count

//li cancello tutti tranne uno (perchè se li cancello tutti darebbe errore!!)
for ll_index = ll_count to 2 step -1
	iole_excel.worksheets(ll_index).delete
next

//alla fine avrò il file excel con un solo foglio pronto
ib_inizio = true

return 0
end function

public function integer uof_scrivi_dec (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor, string fs_format);OLEObject lole_foglio
long ll_ret

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//BOLD
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).font.bold = fb_grassetto
						
//GRANDEZZA FONT
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
						uof_nome_cella(fl_riga , fl_colonna) ).font.size = il_FontSize

//COLORE SFONDO
if isnumber(fs_backcolor) then
	ll_ret = long(fs_backcolor)
else
	ll_ret = uof_colore(fs_backcolor)
end if

if ll_ret > 0 then
	lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).interior.colorindex = ll_ret
end if

//BORDI
uof_bordi(lole_foglio, fl_riga, fl_colonna,fb_bordi)

//FORMATO es "0,00"
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).NumberFormat = fs_format

lole_foglio.cells[fl_riga,fl_colonna] = fany_valore

lole_foglio.columns(fl_colonna).EntireColumn.AutoFit

return 0

end function

public function integer uof_scrivi_nota (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor);OLEObject lole_foglio
long ll_ret

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//BOLD
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).font.bold = fb_grassetto
						
//GRANDEZZA FONT
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
						uof_nome_cella(fl_riga , fl_colonna) ).font.size = il_FontSize

//COLORE SFONDO
if isnumber(fs_backcolor) then
	ll_ret = long(fs_backcolor)
else
	ll_ret = uof_colore(fs_backcolor)
end if

if ll_ret > 0 then
	lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).interior.colorindex = ll_ret
end if
					
//BORDI
uof_bordi(lole_foglio, fl_riga, fl_colonna,fb_bordi)

//VALORE
lole_foglio.cells[fl_riga,fl_colonna] = fany_valore

return 0

end function

public function integer uof_scrivi_codice (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor);OLEObject lole_foglio
long ll_ret


lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//BOLD
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).font.bold = fb_grassetto
						
//GRANDEZZA FONT
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
						uof_nome_cella(fl_riga , fl_colonna) ).font.size = il_FontSize

//COLORE SFONDO
if isnumber(fs_backcolor) then
	ll_ret = long(fs_backcolor)
else
	ll_ret = uof_colore(fs_backcolor)
end if

if ll_ret > 0 then
	lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).interior.colorindex = ll_ret
end if

//BORDI
uof_bordi(lole_foglio, fl_riga, fl_colonna,fb_bordi)

//imposto il formato testo
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).NumberFormat = "@"

lole_foglio.cells[fl_riga,fl_colonna] = fany_valore

if fl_colonna > 1 then lole_foglio.columns(fl_colonna).EntireColumn.AutoFit

return 0

end function

public function integer uof_open ();long ll_errore, ll_count, ll_index

//inizio
ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return -1
end if

iole_excel.Application.Workbooks.Open(is_path_file)

iole_excel.application.visible = ib_OleObjectVisible


return 0
end function

public subroutine uof_leggi (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo);OLEObject		lole_foglio
any				lany_ret

//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

fs_tipo = ""
setnull(fd_valore)
setnull(fs_valore)

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

lany_ret = lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).value
//lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value

try
	//prova con valore decimale
	fd_valore = lany_ret
	fs_tipo = "D"
catch (RuntimeError rte1)
	
	setnull(fd_valore)
	setnull(fs_tipo)
	try
		//prova con valore stringa
		fs_valore = lany_ret
		fs_tipo = "S"
	catch (RuntimeError rte2)
		setnull(fs_valore)
		setnull(fs_tipo)
	end try
	
end try
end subroutine

public subroutine uof_visible (boolean fb_visible);iole_excel.application.visible = fb_visible
end subroutine

public subroutine uof_get_sheets (ref string fs_sheets[]);long ll_i, ll_tot, ll_J

ll_tot = iole_excel.worksheets.count

ll_J = 0
for ll_i = ll_tot to 1 step -1
	ll_J += 1
	fs_sheets[ll_J] = iole_excel.worksheets(ll_i).name
next
end subroutine

public function integer uof_close ();iole_excel.Application.Quit()

return 0
end function

public function integer uof_leggi_ottimistico (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref datetime fdt_valore, string fs_tipo);OLEObject		lole_foglio
any				lany_valore
//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
in base al valore del parametro fs_tipo (S=stringa	D=decimale		C=datetime)
restituisce il valore letto solo in una delle tre variabili reference
fs_valore		se STRINGA
fd_valore 		se DECIMAL (o INTEGER, o LONG)
fdt_valore 		se DATETIME (o DATE)

se c'è un errore torna -1 altrimenti 1

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

setnull(fd_valore)
setnull(fs_valore)
setnull(fdt_valore)

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
lany_valore = lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
										uof_nome_cella(fl_riga , fl_colonna)). value
										
if isnull(lany_valore) then
	//sei arrivato alla fine dei dati-----
	return 0
end if

//prova le conversioni
try
	choose case fs_tipo
		case "S"
//			fs_valore = lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
//										uof_nome_cella(fl_riga , fl_colonna)). value
			fs_valore = string(lany_valore)
							
		case "D"
//			fd_valore = lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
//										uof_nome_cella(fl_riga , fl_colonna)). value
			if isnumber(string(lany_valore)) then
				fd_valore = dec (lany_valore)
			else
				return -1
			end if			
			
		case "T"
//			fdt_valore = lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
//										uof_nome_cella(fl_riga , fl_colonna)). value
			fdt_valore = datetime(lany_valore)
			
	end choose
catch (RuntimeError rte1)
	return -1
end try

return 1



end function

public function integer uof_scrivi_data (string fs_foglio, long fl_riga, long fl_colonna, any fany_valore, boolean fb_bordi, boolean fb_grassetto, string fs_backcolor, string fs_format);OLEObject lole_foglio
long ll_ret


lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)

//BOLD
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).font.bold = fb_grassetto
						
//GRANDEZZA FONT
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" + &
						uof_nome_cella(fl_riga , fl_colonna) ).font.size = il_FontSize

//COLORE SFONDO
if isnumber(fs_backcolor) then
	ll_ret = long(fs_backcolor)
else
	ll_ret = uof_colore(fs_backcolor)
end if

if ll_ret > 0 then
	lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).interior.colorindex = ll_ret
end if

//BORDI
uof_bordi(lole_foglio, fl_riga, fl_colonna,fb_bordi)

//FORMATO m/d/yyyy
lole_foglio.range(uof_nome_cella(fl_riga, fl_colonna) + ":" +&
						uof_nome_cella(fl_riga , fl_colonna) ).NumberFormat = fs_format

lole_foglio.cells[fl_riga,fl_colonna] = fany_valore

if fl_colonna > 1 then lole_foglio.columns(fl_colonna).EntireColumn.AutoFit

return 0

end function

public function integer uof_save_as (string fs_path);iole_excel.Activeworkbook.SaveAs(fs_path)

return 0
end function

public function integer uof_delete_sheet (string fs_sheet);
iole_excel.application.DisplayAlerts = false
iole_excel.worksheets(fs_sheet).delete
iole_excel.application.DisplayAlerts = true

return 0
end function

public function integer uof_close (string fs_path);iole_excel.Activeworkbook.Close(fs_path)

return 0
end function

public function integer uof_save_as_format (string fs_path, integer fi_version);integer li_excel2003, li_excel_2007_2010, li_version

li_excel2003 = 56
li_excel_2007_2010 = 50

choose case fi_version
	case 2007, 2010
		li_version = li_excel_2007_2010
	case else
		li_version = li_excel2003
		
end choose

iole_excel.Activeworkbook.SaveAs( fs_path, li_version )		//, "", "", false, false )

return 0
end function

on uo_excel_listini.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_excel_listini.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;date ldt_oggi
time ltm_adesso
string ls_nomefile
long	ll_errore, ll_count, ll_index

iole_excel = CREATE OLEObject
end event

event destructor;destroy iole_excel;
end event

