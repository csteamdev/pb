﻿$PBExportHeader$uo_report_listini.sru
$PBExportComments$Report Listini Condizioni Particolari
forward
global type uo_report_listini from nonvisualobject
end type
type fstr_livello from structure within uo_report_listini
end type
end forward

type fstr_livello from structure
	string		cod_livello
	decimal { 4 }		sconti[10]
	decimal { 4 }		provv_1
	decimal { 4 }		provv_2
end type

global type uo_report_listini from nonvisualobject
end type
global uo_report_listini uo_report_listini

type variables
long il_file_clienti, il_file_albero
statictext st_messaggio

end variables

forward prototypes
public function integer uof_dimensioni (ref long fl_indice, string fs_cod_prodotto, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, ref string fs_dimensioni[])
public function integer uof_livello (integer fl_num_livello, string fs_cod_cliente, string fs_cod_gruppo, string fs_cod_valuta, datetime fdt_data_inizio_val, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4, string fs_cod_livello_5, decimal fd_sconto[10], decimal fd_provv_1, decimal fd_provv_2, ref string fs_condizione[], ref long fl_ind_condizione)
public function string uof_componi_valore (string fs_flag_sconto_mag_prezzo, double ld_valore)
public function integer uof_carica_dimensioni (string fs_cod_prodotto, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, ref long fl_dim_1[], ref long fl_dim_2[])
public function integer uof_ricerca_clienti (ref string codice[], ref string descrizione[], ref string condizione[], ref string variante[], ref string dimensioni[], ref string messaggio)
end prototypes

public function integer uof_dimensioni (ref long fl_indice, string fs_cod_prodotto, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, ref string fs_dimensioni[]);string ls_sql, ls_flag_sconto_mag_prezzo, ls_str, ls_return, ls_dimensioni[]
integer ll_1, ll_2
long ll_dim_1[], ll_dim_2[], ll_x, ll_y, ll_i, ll_limite_dim_1, ll_limite_dim_2, ldec_riga, ldec_colonna, ll_dim
double ld_variazione

uof_carica_dimensioni(fs_cod_prodotto, fs_cod_tipo_listino_prodotto, fs_cod_valuta, fdt_data_inizio_val,fl_progressivo,fl_num_scaglione, ll_dim_1[], ll_dim_2[])

declare cu_dim dynamic cursor for sqlsa;
ls_sql = " SELECT limite_dimensione_1, limite_dimensione_2, flag_sconto_mag_prezzo, variazione " + &
         " FROM listini_vendite_dimensioni " + &
         " WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
               " cod_tipo_listino_prodotto = '" + fs_cod_tipo_listino_prodotto + "' AND " + &
               " cod_valuta = '" +fs_cod_valuta + "' AND " + &
               " data_inizio_val = '"+ string(fdt_data_inizio_val, s_cs_xx.db_funzioni.formato_data) + "' AND " + &
               " progressivo = " + string(fl_progressivo) + " AND " + &
               " num_scaglione = " + string(fl_num_scaglione) + " " + &
			" ORDER BY limite_dimensione_2 ASC, limite_dimensione_1 ASC "

prepare sqlsa from :ls_sql;

open dynamic cu_dim;

ll_x = 0
ll_i = 0
do while 1=1
   fetch cu_dim into :ll_limite_dim_1, :ll_limite_dim_2, :ls_flag_sconto_mag_prezzo, :ld_variazione;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if ll_i <> ll_limite_dim_2 then
		ll_y = ll_y + 1
		ll_i = ll_limite_dim_2
		ll_x = 0
		if ll_y = 1 then
			ls_dimensioni[ll_y] = string(fl_indice) + "#0#"
			for ll_dim = 1 to upperbound(ll_dim_1) 
				ls_dimensioni[ll_y] = ls_dimensioni[ll_y] + string(ll_dim_1[ll_dim]) + "#"
			next
			ll_y ++
		end if
		ls_dimensioni[ll_y] = string(fl_indice) + "#" + string(ll_dim_2[ll_y - 1]) + "#"
	end if
	ll_x = ll_x + 1
	ldec_riga = ll_x
	ldec_colonna = ll_y
	ls_str = uof_componi_valore(ls_flag_sconto_mag_prezzo, ld_variazione)
	ls_dimensioni[ll_y] = ls_dimensioni[ll_y] + ls_str + "#"
loop
close cu_dim;

ll_y = upperbound(fs_dimensioni[])

for ll_i = 1 to upperbound(ls_dimensioni)
	ll_y ++
	fs_dimensioni[ll_y] = ls_dimensioni[ll_i]
next

return 0

end function

public function integer uof_livello (integer fl_num_livello, string fs_cod_cliente, string fs_cod_gruppo, string fs_cod_valuta, datetime fdt_data_inizio_val, string fs_cod_livello_1, string fs_cod_livello_2, string fs_cod_livello_3, string fs_cod_livello_4, string fs_cod_livello_5, decimal fd_sconto[10], decimal fd_provv_1, decimal fd_provv_2, ref string fs_condizione[], ref long fl_ind_condizione);string ls_sql, ls_cod_livello_prod, ls_des_livello, ls_str, ls_tabella, ls_colonna
long ll_risposta, ll_cont, ll_i, ll_ind
dec{4} ld_sconto[10], ld_provv_1, ld_provv_2
fstr_livello lstr_livello[]

ll_ind = 0
ls_tabella = "tab_livelli_prod_" + string(fl_num_livello)
ls_colonna = "cod_livello_prod_" + string(fl_num_livello)

declare cu_livello dynamic cursor for sqlsa;
ls_sql = "select "+ls_colonna+", des_livello from "+ls_tabella+" where cod_azienda = '" + s_cs_xx.cod_azienda 
choose case fl_num_livello
	case 1
		ls_sql = ls_sql + "' ORDER BY " + ls_colonna
	case 2
		ls_sql = ls_sql + "' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' ORDER BY " + ls_colonna
	case 3
		ls_sql = ls_sql + "' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "' ORDER BY " + ls_colonna
	case 4
		ls_sql = ls_sql + "' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "' and cod_livello_prod_3 = '" +fs_cod_livello_3 + "' ORDER BY " + ls_colonna
	case 5
		ls_sql = ls_sql + "' and cod_livello_prod_1 = '" +fs_cod_livello_1 + "' and cod_livello_prod_2 = '" +fs_cod_livello_2 + "' and cod_livello_prod_3 = '" +fs_cod_livello_3 + "' and cod_livello_prod_4 = '" +fs_cod_livello_4 + "' ORDER BY " + ls_colonna
end choose
prepare sqlsa from :ls_sql;
open dynamic cu_livello;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore cu_livello." + sqlca.sqlerrtext)
	return -1
end if
do while true
   fetch cu_livello into :ls_cod_livello_prod, :ls_des_livello;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
	ll_cont = 0
	choose case fl_num_livello
		case 1
			SELECT count(*) INTO :ll_i FROM gruppi_sconto WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) and ( cod_gruppo_sconto = :fs_cod_gruppo ) and ( cod_valuta = :fs_cod_valuta )  AND  ( cod_cliente = :fs_cod_cliente ) AND  ( cod_livello_prod_1 = :ls_cod_livello_prod ) AND  ( cod_livello_prod_2 = :fs_cod_livello_2 ) AND  ( cod_livello_prod_3 = :fs_cod_livello_3 ) AND  ( cod_livello_prod_4 = :fs_cod_livello_4 ) AND  ( cod_livello_prod_5 = :fs_cod_livello_5 )   ;
			fs_cod_livello_1 = ls_cod_livello_prod
		case 2
			SELECT count(*) INTO :ll_i FROM gruppi_sconto WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) and ( cod_gruppo_sconto = :fs_cod_gruppo ) and ( cod_valuta = :fs_cod_valuta )  AND  ( cod_cliente = :fs_cod_cliente ) AND  ( cod_livello_prod_1 = :fs_cod_livello_1 ) AND  ( cod_livello_prod_2 = :ls_cod_livello_prod ) AND  ( cod_livello_prod_3 = :fs_cod_livello_3 ) AND  ( cod_livello_prod_4 = :fs_cod_livello_4 ) AND  ( cod_livello_prod_5 = :fs_cod_livello_5 )   ;
			fs_cod_livello_2 = ls_cod_livello_prod
		case 3
			SELECT count(*) INTO :ll_i FROM gruppi_sconto WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) and ( cod_gruppo_sconto = :fs_cod_gruppo ) and ( cod_valuta = :fs_cod_valuta )  AND  ( cod_cliente = :fs_cod_cliente ) AND  ( cod_livello_prod_1 = :fs_cod_livello_1 ) AND  ( cod_livello_prod_2 = :fs_cod_livello_2 ) AND  ( cod_livello_prod_3 = :ls_cod_livello_prod ) AND  ( cod_livello_prod_4 = :fs_cod_livello_4 ) AND  ( cod_livello_prod_5 = :fs_cod_livello_5 )   ;
			fs_cod_livello_3 = ls_cod_livello_prod
		case 4
			SELECT count(*) INTO :ll_i FROM gruppi_sconto WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) and ( cod_gruppo_sconto = :fs_cod_gruppo ) and ( cod_valuta = :fs_cod_valuta )  AND  ( cod_cliente = :fs_cod_cliente ) AND  ( cod_livello_prod_1 = :fs_cod_livello_1 ) AND  ( cod_livello_prod_2 = :fs_cod_livello_2 ) AND  ( cod_livello_prod_3 = :fs_cod_livello_3 ) AND  ( cod_livello_prod_4 = :ls_cod_livello_prod ) AND  ( cod_livello_prod_5 = :fs_cod_livello_5 )   ;
			fs_cod_livello_4 = ls_cod_livello_prod
		case 5
			SELECT count(*) INTO :ll_i FROM gruppi_sconto WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) and ( cod_gruppo_sconto = :fs_cod_gruppo ) and ( cod_valuta = :fs_cod_valuta )  AND  ( cod_cliente = :fs_cod_cliente ) AND  ( cod_livello_prod_1 = :fs_cod_livello_1 ) AND  ( cod_livello_prod_2 = :fs_cod_livello_2 ) AND  ( cod_livello_prod_3 = :fs_cod_livello_3 ) AND  ( cod_livello_prod_4 = :fs_cod_livello_4 ) AND  ( cod_livello_prod_5 = :ls_cod_livello_prod )   ;
			fs_cod_livello_5 = ls_cod_livello_prod
	end choose
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in select sul DB." + sqlca.sqlerrtext)
		return -1
	end if
	
	if ll_i = 0 then
		// trovato niente; mi tengo gli sconti del livello precedente (sempre supposto che ci siano)
		for ll_cont = 1 to 10 
			if fd_sconto[ll_cont] > 0 and not isnull(fd_sconto[ll_cont]) then
				fl_ind_condizione ++
				ls_str = "" + "#" + "" + "#" + "" + "#" + "" + "#" + "0" + "#"
				if not isnull(fs_cod_livello_1) then ls_str = ls_str + fs_cod_livello_1
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_2) then ls_str = ls_str + fs_cod_livello_2
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_3) then ls_str = ls_str + fs_cod_livello_3
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_4) then ls_str = ls_str + fs_cod_livello_4
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_5) then ls_str = ls_str + fs_cod_livello_5
				ls_str = ls_str + "S#" + string(fd_sconto[ll_cont]) + "#N#1#0#0"
				if not isnull(fd_provv_1) then
					ls_str = ls_str + "#" + string(fd_provv_1)
					if not isnull(fd_provv_2) then
						ls_str = ls_str + "#" + string(fd_provv_2)
					else
						ls_str = ls_str + "#0"
					end if
				else
					ls_str = ls_str + "#0#0"
				end if						
				fs_condizione[fl_ind_condizione] = fs_cod_cliente + "#" + ls_str
			end if
		next			
		ll_ind ++
		lstr_livello[ll_ind].cod_livello = ls_cod_livello_prod
		lstr_livello[ll_ind].sconti = fd_sconto
		lstr_livello[ll_ind].provv_1 = fd_provv_1
		lstr_livello[ll_ind].provv_2 = fd_provv_2
	else
		// trovato delle condizioni su questo livello; le scrivo !!!
		select sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
		into   :ld_sconto[1],:ld_sconto[2],:ld_sconto[3],:ld_sconto[4],:ld_sconto[5],:ld_sconto[6],:ld_sconto[7],:ld_sconto[8],:ld_sconto[9],:ld_sconto[10], :ld_provv_1, :ld_provv_2
		from gruppi_sconto
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_agente is null and
				 cod_valuta = :fs_cod_valuta and
				 cod_cliente = :fs_cod_cliente and
				 cod_gruppo_sconto  = :fs_cod_gruppo AND  
				 ( cod_livello_prod_1 = :fs_cod_livello_1 ) AND  ( cod_livello_prod_2 = :fs_cod_livello_2 ) AND  ( cod_livello_prod_3 = :fs_cod_livello_3 ) AND  ( cod_livello_prod_4 = :fs_cod_livello_4 ) AND  ( cod_livello_prod_5 = :fs_cod_livello_5 )   
		group by cod_gruppo_sconto, 
				 cod_valuta, 
				 data_inizio_val, 
				 progressivo
		having cod_cliente = :fs_cod_cliente and
				 cod_agente is null and
				 cod_gruppo_sconto  = :fs_cod_gruppo and
				 cod_valuta = :fs_cod_valuta and
				 data_inizio_val = max(data_inizio_val);

		for ll_cont = 1 to 10 
			if ld_sconto[ll_cont] > 0 and not isnull(ld_sconto[ll_cont]) then
				fl_ind_condizione ++
				ls_str = "" + "#" + "" + "#" + "" + "#" + "" + "#" + "0" + "#"
				if not isnull(fs_cod_livello_1) then ls_str = ls_str + fs_cod_livello_1
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_2) then ls_str = ls_str + fs_cod_livello_2
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_3) then ls_str = ls_str + fs_cod_livello_3
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_4) then ls_str = ls_str + fs_cod_livello_4
				ls_str = ls_str + "#"
				if not isnull(fs_cod_livello_5) then ls_str = ls_str + fs_cod_livello_5
				ls_str = ls_str + "S#" + string(ld_sconto[ll_cont]) + "#N#1#0#0"
				if not isnull(fd_provv_1) then
					ls_str = ls_str + "#" + string(ld_provv_1)
					if not isnull(fd_provv_2) then
						ls_str = ls_str + "#" + string(ld_provv_2)
					else
						ls_str = ls_str + "#0"
					end if
				else
					ls_str = ls_str + "#0#0"
				end if						
				fs_condizione[fl_ind_condizione] = fs_cod_cliente + "#" + ls_str
			end if
		next			
		ll_ind ++
		lstr_livello[ll_ind].cod_livello = ls_cod_livello_prod
		lstr_livello[ll_ind].sconti = ld_sconto
		lstr_livello[ll_ind].provv_1 = ld_provv_1
		lstr_livello[ll_ind].provv_2 = ld_provv_2
		for ll_cont = 1 to 10
			ld_sconto[ll_cont] = 0
		next
		ld_provv_1 = 0
		ld_provv_2 = 0
	end if
	
loop
close cu_livello;

for ll_ind = 1 to upperbound(lstr_livello)
	choose case fl_num_livello
		case 1
//			messagebox("entrata " + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
			fd_sconto = lstr_livello[ll_ind].sconti
			if uof_livello(fl_num_livello+1,fs_cod_cliente,fs_cod_gruppo,fs_cod_valuta,fdt_data_inizio_val,lstr_livello[ll_ind].cod_livello,fs_cod_livello_2,fs_cod_livello_3,fs_cod_livello_4,fs_cod_livello_5,fd_sconto,lstr_livello[ll_ind].provv_1,lstr_livello[ll_ind].provv_2,fs_condizione,fl_ind_condizione) < 0 then return -1
//			messagebox("uscita " + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
		case 2
//			messagebox("entrata " + fs_cod_livello_1 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
			fd_sconto = lstr_livello[ll_ind].sconti
			if uof_livello(fl_num_livello+1,fs_cod_cliente,fs_cod_gruppo,fs_cod_valuta,fdt_data_inizio_val,fs_cod_livello_1,lstr_livello[ll_ind].cod_livello,fs_cod_livello_3,fs_cod_livello_4,fs_cod_livello_5,fd_sconto,lstr_livello[ll_ind].provv_1,lstr_livello[ll_ind].provv_2,fs_condizione,fl_ind_condizione) < 0 then return -1
//			messagebox("uscita " + fs_cod_livello_1 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
		case 3
//			messagebox("entrata " + fs_cod_livello_1 +"/" + fs_cod_livello_2 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
			fd_sconto = lstr_livello[ll_ind].sconti
			if uof_livello(fl_num_livello+1,fs_cod_cliente,fs_cod_gruppo,fs_cod_valuta,fdt_data_inizio_val,fs_cod_livello_1,fs_cod_livello_2,lstr_livello[ll_ind].cod_livello,fs_cod_livello_4,fs_cod_livello_5,fd_sconto,lstr_livello[ll_ind].provv_1,lstr_livello[ll_ind].provv_2,fs_condizione,fl_ind_condizione) < 0 then return -1
//			messagebox("uscita " + fs_cod_livello_1 +"/" + fs_cod_livello_2 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
		case 4
//			messagebox("entrata " + fs_cod_livello_1 +"/" + fs_cod_livello_2 +"/" + fs_cod_livello_3 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
			fd_sconto = lstr_livello[ll_ind].sconti
			if uof_livello(fl_num_livello+1,fs_cod_cliente,fs_cod_gruppo,fs_cod_valuta,fdt_data_inizio_val,fs_cod_livello_1,fs_cod_livello_2,fs_cod_livello_3,lstr_livello[ll_ind].cod_livello,fs_cod_livello_5,fd_sconto,lstr_livello[ll_ind].provv_1,lstr_livello[ll_ind].provv_2,fs_condizione,fl_ind_condizione) < 0 then return -1
//			messagebox("uscita " + fs_cod_livello_1 +"/" + fs_cod_livello_2 +"/" + fs_cod_livello_3 +"/" + ls_livelli[ll_ind], "Livello:" + string(fl_num_livello) + " SCONTI " + string(fd_sconto[1]) +"+" + string(fd_sconto[2]))
	end choose
next
return 0
end function

public function string uof_componi_valore (string fs_flag_sconto_mag_prezzo, double ld_valore);string ls_return

choose case fs_flag_sconto_mag_prezzo
	case "S"  // sconto
		ls_return = string(ld_valore)
		ls_return = "-" + ls_return + "%"
	case "M"
		ls_return = string(ld_valore)
		ls_return = ls_return + "%"
	case "A"
		ls_return = string(ld_valore)
	case "D"
		ls_return = "-" + string(ld_valore)
	case "P"
		ls_return = "E." + string(ld_valore)
	case else
		ls_return = "errore"
end choose

return ls_return
		
end function

public function integer uof_carica_dimensioni (string fs_cod_prodotto, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, long fl_num_scaglione, ref long fl_dim_1[], ref long fl_dim_2[]);string ls_flag_tipo_dimensione_1, ls_flag_tipo_dimensione_2, ls_sql
long   ll_num_intervalli_dim_1, ll_num_intervalli_dim_2, &
       ll_i, ll_z, ll_x, ll_y, ll_valore

select flag_tipo_dimensione_1,
       flag_tipo_dimensione_2,
		 num_intervalli_dim_1,
		 num_intervalli_dim_2
into   :ls_flag_tipo_dimensione_1,
       :ls_flag_tipo_dimensione_2,
		 :ll_num_intervalli_dim_1,
		 :ll_num_intervalli_dim_2
from   tab_prodotti_dimensioni
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Non vi sono dimensioni caricate nella tabella Prodotti Dimensioni")
	return -1
end if

declare cu_dim dynamic cursor for sqlsa;
ls_sql = "SELECT limite_dimensione FROM tab_prodotti_dimensioni_det WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
         "cod_prodotto = '" + fs_cod_prodotto + "' AND " + &
			"flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_1 + "'" 

prepare sqlsa from :ls_sql;

open dynamic cu_dim;

ll_i = 0
do while 1=1
   fetch cu_dim into :ll_valore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	fl_dim_1[ll_i]	= ll_valore
loop
close cu_dim;
ll_x = ll_i

ls_sql = "SELECT limite_dimensione FROM tab_prodotti_dimensioni_det WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
         "cod_prodotto = '" + fs_cod_prodotto + "' AND " + &
			"flag_tipo_dimensione = '" + ls_flag_tipo_dimensione_2 + "'" 

prepare sqlsa from :ls_sql;

open dynamic cu_dim;

ll_i = 0
do while 1=1
   fetch cu_dim into :ll_valore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	fl_dim_2[ll_i] = ll_valore
loop
close cu_dim;

return 0		 
end function

public function integer uof_ricerca_clienti (ref string codice[], ref string descrizione[], ref string condizione[], ref string variante[], ref string dimensioni[], ref string messaggio);string ls_cod_cliente, ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_cod_gruppo_sconto, ls_cod_agente_1, &
       ls_cod_pagamento, ls_rag_soc_1_agente, ls_des_pagamento, ls_str, ls_null, ls_cod_prodotto, ls_dir_report, ls_path, &
		 ls_livello_1, ls_livello_2,ls_livello_3, ls_livello_4, ls_livello_5, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_des_prodotto, &
		 ls_cod_misura_mag, ls_cod_misura_ven, ls_fattore_conv, ls_prodotto_cur, ls_cod_valuta_cliente,ls_data_elaborazione, &
		 ls_flag_tipo_cliente
integer li_risposta
long ll_i, ll_file, ll_ind_condizione, ll_file1, ll_z, ll_y, ll_indice_varianti, ll_indice_dimensioni, ll_file2, ll_inden_varianti, &
     ll_file3, ll_righe_listini, ll_progressivo, ll_righe_varianti, ll_t, ll_conta, ll_cont, ll_ind_scaglione
double ld_provvigione_1, ld_provvigione_2, ld_sconti[10], ld_provv_1, ld_provv_2,ld_fat_conversione_ven, ld_scaglione, ld_cambio_val
datetime ldt_oggi,ldt_data_inizio_val
datastore lds_listini_clienti, lds_varianti


ldt_oggi = datetime(today(),00:00:00)

if isvalid(st_messaggio) then st_messaggio.text = "Inizializzazione"
DECLARE cu_clienti CURSOR FOR 
select listini_vendite.cod_cliente  
    from listini_vendite  
   where ( listini_vendite.cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( listini_vendite.cod_cliente  is not null) 
	group by cod_cliente
   union all 
  select gruppi_sconto.cod_cliente  
    from gruppi_sconto  
   where ( gruppi_sconto.cod_azienda = :s_cs_xx.cod_azienda ) and  
         ( gruppi_sconto.cod_cliente is not null)   
	group by cod_cliente;

DECLARE cu_listini CURSOR FOR 
select cod_prodotto, cod_cliente, cod_tipo_listino_prodotto, cod_valuta,data_inizio_val, progressivo
from listini_vendite 
where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente
group by cod_prodotto
having cod_cliente = :ls_cod_cliente and data_inizio_val = max(data_inizio_val)
order by cod_prodotto;


DECLARE cu_gruppi CURSOR FOR
select cod_livello_prod_1, cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4, cod_livello_prod_5, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2
from gruppi_sconto
where  cod_azienda = :s_cs_xx.cod_azienda and
		cod_agente is null and
		cod_valuta = :ls_cod_valuta_cliente and
		cod_cliente = :ls_cod_cliente and
		cod_gruppo_sconto  = :ls_cod_gruppo_sconto
group by cod_gruppo_sconto, 
         cod_valuta, 
			data_inizio_val, 
			progressivo
having cod_cliente = :ls_cod_cliente and
		cod_agente is null and
		cod_gruppo_sconto  = :ls_cod_gruppo_sconto and
		cod_valuta = :ls_cod_valuta_cliente and
		data_inizio_val = max(data_inizio_val);


open cu_clienti;
if sqlca.sqlcode <> 0 then
	messaggio = "Errore in open cursore cu_clienti" + sqlca.sqlerrtext
	return -1
end if

ll_i = 0
ll_indice_varianti = 0
ll_inden_varianti = 0
ll_ind_condizione = 0
ll_indice_dimensioni = 0

do while 1=1
	fetch cu_clienti into :ls_cod_cliente;
	if sqlca.sqlcode < 0 then
		messaggio = "Errore in fetch cursore cu_clienti" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 100 then 
		close cu_clienti;
		exit
	end if
	
	if isvalid(st_messaggio) then st_messaggio.text = ls_cod_cliente
	ll_i ++
	
	select rag_soc_1, indirizzo, cap, localita, provincia, cod_gruppo_sconto, cod_agente_1, cod_pagamento, cod_valuta, flag_tipo_cliente
	into   :ls_rag_soc_1, :ls_indirizzo, :ls_cap, :ls_localita, :ls_provincia, :ls_cod_gruppo_sconto, :ls_cod_agente_1, :ls_cod_pagamento, :ls_cod_valuta_cliente, :ls_flag_tipo_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode <> 0 then
		messaggio = "Errore in select su anag_clienti. " + sqlca.sqlerrtext
		return -1
	end if

	if not isnull(ls_cod_agente_1) then
		select rag_soc_1
		into   :ls_rag_soc_1_agente
		from   anag_agenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_agente = :ls_cod_agente_1;
	else
		ls_cod_agente_1 = ""
		ls_rag_soc_1_agente = ""
	end if

	if not isnull(ls_cod_pagamento) then
		select des_pagamento
		into   :ls_des_pagamento
		from   tab_pagamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_pagamento = :ls_cod_pagamento;
	else
		ls_cod_pagamento = ""
		ls_des_pagamento = ""
	end if
	
	if isnull(ls_provincia) then ls_provincia = ""
	if isnull(ls_cap) then ls_cap = ""
	if isnull(ls_localita) then ls_localita = ""

	codice[ll_i] = ls_cod_cliente
	descrizione[ll_i] = ls_rag_soc_1 + "#" + ls_indirizzo + "#" + ls_cap + "#" + ls_localita + "#" + ls_provincia + "#" + ls_cod_gruppo_sconto + "#" + ls_cod_agente_1 + "#" + ls_rag_soc_1_agente + "#" + ls_cod_pagamento + "#" + ls_des_pagamento + "#" + ls_flag_tipo_cliente

	select cod_gruppo_sconto
	into   :ls_cod_gruppo_sconto
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
	if not isnull(ls_cod_gruppo_sconto) then 
		// ----------------------------------  RICERCO LE LINEE DI SCONTO E PROVVIGIONE ----------------------------------------------
		setnull(ls_livello_1)
		setnull(ls_livello_2)
		setnull(ls_livello_3)
		setnull(ls_livello_4)
		setnull(ls_livello_5)
		uof_livello(1,ls_cod_cliente,ls_cod_gruppo_sconto,ls_cod_valuta_cliente, ldt_data_inizio_val, ls_livello_1,ls_livello_2,ls_livello_3,ls_livello_4,ls_livello_5,ld_sconti[],ld_provv_1,ld_provv_2,condizione[],ll_ind_condizione)
	end if

	// ----------------------------------  ricerco le condizioni particolari nei listini -----------------------------------------
	
	lds_listini_clienti = Create datastore
	lds_listini_clienti.dataobject = "d_ds_listini_vendite"
	lds_listini_clienti.settransobject(sqlca)
	open cu_listini;
	if sqlca.sqlcode <> 0 then
		messaggio = "Errore in open cursore cu_listini" + sqlca.sqlerrtext
		return -1
	end if
	do while 1=1
		fetch cu_listini into :ls_prodotto_cur, :ls_cod_cliente, :ls_cod_tipo_listino_prodotto, :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then 
			messaggio = "Errore in fetch cursore cu_clienti" + sqlca.sqlerrtext
			return -1
		end if
		ll_righe_listini = lds_listini_clienti.retrieve(s_cs_xx.cod_azienda, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ldt_data_inizio_val, ll_progressivo)
		if ll_righe_listini > 0 then
			if isvalid(st_messaggio) then st_messaggio.text = ls_cod_cliente + " " + string(ldt_data_inizio_val,"dd/mm/yyyy")
			for ll_y = 1 to ll_righe_listini
				ll_ind_scaglione = 1
				do
					ll_ind_condizione ++
					ld_scaglione = lds_listini_clienti.getitemnumber( ll_y, "scaglione_" + string(ll_ind_scaglione))
					ls_str = lds_listini_clienti.getitemstring( ll_y, "cod_prodotto")
					setnull(ls_livello_1)
					setnull(ls_livello_2)
					setnull(ls_livello_3)
					setnull(ls_livello_4)
					setnull(ls_livello_5)
					setnull(ls_des_prodotto)
					setnull(ls_cod_misura_mag)
					setnull(ls_cod_misura_ven)
					setnull(ld_fat_conversione_ven)
					
					ld_cambio_val = f_cambio_val_ven(ls_cod_valuta, ldt_oggi)

					
					// ESEGUO RICERCA DELLE PROVVIGIONI
					uo_condizioni_cliente luo_condizioni_cliente
					luo_condizioni_cliente = CREATE uo_condizioni_cliente
					luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					luo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_val
					luo_condizioni_cliente.str_parametri.data_riferimento = ldt_oggi
					luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					luo_condizioni_cliente.str_parametri.cod_prodotto = lds_listini_clienti.getitemstring( ll_y, "cod_prodotto")
					luo_condizioni_cliente.str_parametri.dim_1 = 0
					luo_condizioni_cliente.str_parametri.dim_2 = 0
					luo_condizioni_cliente.str_parametri.quantita = ld_scaglione
					luo_condizioni_cliente.str_parametri.valore = 0
					luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					luo_condizioni_cliente.str_parametri.fat_conversione_ven = 1
					luo_condizioni_cliente.ib_setitem = false
					luo_condizioni_cliente.ib_setitem_provvigioni = false
					luo_condizioni_cliente.wf_condizioni_cliente()
					ld_provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
					DESTROY luo_condizioni_cliente
					// FINE RICERCA PROVVIGIONE AGENTI				
					if not isnull(ls_str) then
						select des_prodotto, 
								 cod_misura_mag, 
								 cod_misura_ven, 
								 fat_conversione_ven
						into   :ls_des_prodotto, 
								 :ls_cod_misura_mag, 
								 :ls_cod_misura_ven, 
								 :ld_fat_conversione_ven
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_str;
						if isnull(ls_des_prodotto) then ls_des_prodotto = ""
						if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ""
						if isnull(ls_cod_misura_ven) then ls_cod_misura_ven = ""
						if isnull(ld_fat_conversione_ven) then 
							ls_fattore_conv = ""
						else
							ls_fattore_conv = string(ld_fat_conversione_ven, "####0.00000")
						end if
						
						select cod_livello_prod_1,   
								 cod_livello_prod_2,   
								 cod_livello_prod_3,   
								 cod_livello_prod_4,   
								 cod_livello_prod_5  
						into  :ls_livello_1,   
								:ls_livello_2,   
								:ls_livello_3,   
								:ls_livello_4,   
								:ls_livello_5  
						from  anag_prodotti  
						where cod_azienda = :s_cs_xx.cod_azienda  AND  
								cod_prodotto = :ls_str  ;
					end if
					ls_str = ls_str + "#" + ls_des_prodotto 
					if ld_scaglione < 99000000 then
						ls_str = ls_str + "<BR><STRONG> Fino a quantità " + string(ld_scaglione) + "</STRONG>"
					else
						if ll_ind_scaglione > 1 then		// caso in cui scrivere oltre
							ls_str = ls_str + "<BR><STRONG> OLTRE ....</STRONG>"
						end if
					end if
					ls_str = ls_str + "#" + ls_cod_misura_mag + "#" + ls_cod_misura_ven + "#" + ls_fattore_conv + "#"
					if not isnull(ls_livello_1) then ls_str = ls_str + ls_livello_1
					ls_str = ls_str + "#"
					if not isnull(ls_livello_2) then ls_str = ls_str + ls_livello_2
					ls_str = ls_str + "#"
					if not isnull(ls_livello_3) then ls_str = ls_str + ls_livello_3
					ls_str = ls_str + "#"
					if not isnull(ls_livello_4) then ls_str = ls_str + ls_livello_4
					ls_str = ls_str + "#"
					if not isnull(ls_livello_5) then ls_str = ls_str + ls_livello_5
					condizione[ll_ind_condizione] = ls_cod_cliente + "#" + ls_str
					choose case lds_listini_clienti.getitemstring(ll_y, "flag_sconto_mag_prezzo_" + string(ll_ind_scaglione))
						case "S" 
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "S#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + string(lds_listini_clienti.getitemnumber(ll_y, "variazione_" + string(ll_ind_scaglione))) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + lds_listini_clienti.getitemstring(ll_y, "flag_origine_prezzo_" + string(ll_ind_scaglione)) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "1#0#0"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ld_provvigione_1,"##0,0#")  + "#0"    /// AGGIUNTO PER PROVVIGIONE
						case "M"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "M#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + string(lds_listini_clienti.getitemnumber(ll_y, "variazione_" + string(ll_ind_scaglione))) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + lds_listini_clienti.getitemstring(ll_y, "flag_origine_prezzo_" + string(ll_ind_scaglione)) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "1#0#0"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ld_provvigione_1,"##0,0#")  + "#0"    /// AGGIUNTO PER PROVVIGIONE
//							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#0#0"    /// AGGIUNTO PER PROVVIGIONE
						case "P"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "P#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + string(lds_listini_clienti.getitemnumber(ll_y, "variazione_" + string(ll_ind_scaglione))) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + lds_listini_clienti.getitemstring(ll_y, "flag_origine_prezzo_" + string(ll_ind_scaglione)) + "#"
							condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "1"
							// verifica se esiste eed eventualmente carica tabella prezzi dimensioni
							
							setnull(ll_conta)
							select count(*)
							into   :ll_conta
							from   listini_vendite_dimensioni
							where cod_azienda = :s_cs_xx.cod_azienda and
									cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
									cod_valuta = :ls_cod_valuta and
									data_inizio_val = :ldt_data_inizio_val and
									progressivo = :ll_progressivo and
									num_scaglione = 1;
							if not isnull(ll_conta) and ll_conta > 0 then
								ll_indice_dimensioni ++
								ls_cod_prodotto = lds_listini_clienti.getitemstring(ll_y, "cod_prodotto")
								uof_dimensioni(ll_indice_dimensioni, ls_cod_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val,ll_progressivo, ll_ind_scaglione, dimensioni[])
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ll_indice_dimensioni)
							else
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#0"
							end if
							
								
							// verifica listino produzione e se esiste carica
							lds_varianti = Create datastore
							lds_varianti.dataobject = "d_ds_listini_produzione"
							lds_varianti.settransobject(sqlca)
							ll_righe_varianti = lds_varianti.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_listino_prodotto, ls_cod_valuta, ldt_data_inizio_val, ll_progressivo)
							if ll_righe_varianti > 0 then
								ll_inden_varianti ++
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ll_inden_varianti)
								// aggiunto per provvigioni 1,2   (enme18/7/2001)
//								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#0#0"
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ld_provvigione_1,"##0,0#")  + "#0"    /// AGGIUNTO PER PROVVIGIONE
								
								for ll_t = 1 to ll_righe_varianti
									ll_indice_varianti ++
									ld_provvigione_1 = lds_varianti.getitemnumber(ll_t, "provvigione_1")
									ld_provvigione_2 = lds_varianti.getitemnumber(ll_t, "provvigione_2")
									if isnull(ld_provvigione_1) then ld_provvigione_1  = 0
									if isnull(ld_provvigione_2) then ld_provvigione_2  = 0
									variante[ll_indice_varianti] = string(ll_inden_varianti) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "cod_prodotto_padre") + "#" + &
																			 lds_varianti.getitemstring(ll_t, "cod_prodotto_figlio") + "#" + &
																			 lds_varianti.getitemstring(ll_t, "anag_prodotti_des_prodotto") + "#" + &
																			 ls_cod_misura_ven + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "scaglione_1")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_sconto_mag_prezzo_1") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "variazione_1")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_origine_prezzo_1") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "scaglione_2")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_sconto_mag_prezzo_2") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "variazione_2")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_origine_prezzo_2") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "scaglione_3")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_sconto_mag_prezzo_3") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "variazione_3")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_origine_prezzo_3") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "scaglione_4")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_sconto_mag_prezzo_4") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "variazione_4")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_origine_prezzo_4") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "scaglione_5")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_sconto_mag_prezzo_5") + "#" + &
																			 string(lds_varianti.getitemnumber(ll_t, "variazione_5")) + "#" + &
																			 lds_varianti.getitemstring(ll_t, "flag_origine_prezzo_5") + "#"  + &
																			 string(ld_provvigione_1) + "#"  + &
																			 string(ld_provvigione_2) + "#" 
								next
							else  // non c'è alcuna variante collegata
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#0"
								// aggiunto per provvigioni 1,2   (enme18/7/2001)
//								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#0#0"
								condizione[ll_ind_condizione] = condizione[ll_ind_condizione] + "#" + string(ld_provvigione_1,"##0,0#")  + "#0"    /// AGGIUNTO PER PROVVIGIONE
							end if
							destroy lds_varianti
					end choose
					ll_ind_scaglione ++
				loop until ld_scaglione >= 99000000
			next
		end if		
	loop	
	// ---------------------------------------------------------------------------------------------------------------------------
	close cu_listini;
	destroy lds_listini_clienti
loop
close cu_clienti;

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VLS", ls_dir_report)
ls_path = ls_dir_report

// ---------------------------  creazione dei files dei livelli -------------------------------------
datastore lds_livello
// 1
lds_livello = Create datastore
lds_livello.dataobject = "d_ds_livello_prod_1"
lds_livello.settransobject(sqlca)
lds_livello.retrieve(s_cs_xx.cod_azienda)
lds_livello.saveas(ls_path + "livello_1.TXT", text!, false)
destroy lds_livello
// 2
lds_livello = Create datastore
lds_livello.dataobject = "d_ds_livello_prod_2"
lds_livello.settransobject(sqlca)
lds_livello.retrieve(s_cs_xx.cod_azienda)
lds_livello.saveas(ls_path + "livello_2.TXT", text!, false)
destroy lds_livello
// 3
lds_livello = Create datastore
lds_livello.dataobject = "d_ds_livello_prod_3"
lds_livello.settransobject(sqlca)
lds_livello.retrieve(s_cs_xx.cod_azienda)
lds_livello.saveas(ls_path + "livello_3.TXT", text!, false)
destroy lds_livello
// 4
lds_livello = Create datastore
lds_livello.dataobject = "d_ds_livello_prod_4"
lds_livello.settransobject(sqlca)
lds_livello.retrieve(s_cs_xx.cod_azienda)
lds_livello.saveas(ls_path + "livello_4.TXT", text!, false)
destroy lds_livello
// 5
lds_livello = Create datastore
lds_livello.dataobject = "d_ds_livello_prod_5"
lds_livello.settransobject(sqlca)
lds_livello.retrieve(s_cs_xx.cod_azienda)
lds_livello.saveas(ls_path + "livello_5.TXT", text!, false)
destroy lds_livello

ll_File2 = FileOpen(ls_path + "varianti.TXT", LINEMODE!, Write!, LockWrite!, Replace!)
for ll_z = 1 to upperbound(variante)
	filewrite(ll_file2, variante[ll_z])
next
fileclose(ll_file2)

ll_File1 = FileOpen(ls_path +  "condizioni.TXT", LINEMODE!, Write!, LockWrite!, Replace!)
for ll_z = 1 to upperbound(condizione)
	filewrite(ll_file1, condizione[ll_z])
next
fileclose(ll_file1)

ll_File = FileOpen(ls_path + "CLIENTI.TXT", LINEMODE!, Write!, LockWrite!, REPLACE!)
for ll_i = 1 to upperbound(codice)
	filewrite(ll_file, codice[ll_i] + "#" + descrizione[ll_i])
next
fileclose(ll_file)

ll_File3 = FileOpen(ls_path + "DIMENSIONI.TXT", LINEMODE!, Write!, LockWrite!, REPLACE!)
for ll_i = 1 to upperbound(dimensioni)
	filewrite(ll_file3, dimensioni[ll_i])
next
fileclose(ll_file3)

delete from parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'DUE';
		 
ls_data_elaborazione = "Data:" + string(today(),"dd/mm/yyyy") + " Ora:" + string(now(),"hh:mm:ss")

insert into parametri_azienda  
		( cod_azienda,   
		  flag_parametro,   
		  cod_parametro,   
		  des_parametro,   
		  stringa,   
		  flag,   
		  data,   
		  numero )  
values ( :s_cs_xx.cod_azienda,   
		  'S',   
		  'DUE',   
		  :ls_data_elaborazione,   
		  null,   
		  'N',   
		  null,   
		  0 )  ;
 
if isvalid(st_messaggio) then st_messaggio.text = "Pronto"

return 0

end function

on uo_report_listini.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_report_listini.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

