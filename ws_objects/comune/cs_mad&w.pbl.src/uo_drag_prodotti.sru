﻿$PBExportHeader$uo_drag_prodotti.sru
forward
global type uo_drag_prodotti from uo_dw_drag
end type
end forward

global type uo_drag_prodotti from uo_dw_drag
integer width = 3259
integer height = 728
string dataobject = "d_anag_prodotti_documenti"
boolean hscrollbar = true
boolean vscrollbar = true
event ue_retrieve ( string cod_prodotto )
end type
global uo_drag_prodotti uo_drag_prodotti

type variables
constant string ESC_COMMAND = "cs_team_esc_pressed"

private:
	datawindow idw_data
end variables

forward prototypes
public subroutine set_parent (datawindow adw_parent)
public function integer uof_abilita_disabilita (string as_cod_prodotto, string as_cod_blob, ref integer ai_disabilitato, ref string as_errore)
public function boolean uof_supervisore ()
end prototypes

event ue_retrieve(string cod_prodotto);retrieve(s_cs_xx.cod_azienda, cod_prodotto)
end event

public subroutine set_parent (datawindow adw_parent);idw_data = adw_parent
end subroutine

public function integer uof_abilita_disabilita (string as_cod_prodotto, string as_cod_blob, ref integer ai_disabilitato, ref string as_errore);

select		disabilitato
into		:ai_disabilitato
from anag_prodotti_blob
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto and
			cod_blob=:as_cod_blob;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in controllo documento disabilitato: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Dato non trovato in tabella note"
	return 1
else
	if isnull(ai_disabilitato) then ai_disabilitato = 0
end if

if ai_disabilitato=0 then
	ai_disabilitato = 1
else
	ai_disabilitato = 0
end if

return 0
end function

public function boolean uof_supervisore ();string				ls_flag_supervisore

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then return true
else
	//sei CS_SYSTEM, puoi fare tutto
	return true
end if


return false
end function

on uo_drag_prodotti.create
call super::create
end on

on uo_drag_prodotti.destroy
call super::destroy
end on

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

string ls_cod_prodotto, ls_sql, ls_cod_blob,  ls_dir, ls_file, ls_ext
long ll_row, ll_max, ll_len, ll_maxKB, ll_prog_mimetype, ll_count
blob lb_blob


ll_row = idw_data.getrow()

//di sicuro una riga selezionata c'è (controllato da ue_start_drop)
ls_cod_prodotto = idw_data.getitemstring(ll_row, "cod_prodotto")

select max(cod_blob)
into :ll_max
from anag_prodotti_blob 
where cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :ls_cod_prodotto and
	isnumeric(cod_blob)=1;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il conteggio dei documenti", sqlca)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_max) then
	ll_max = 0
end if
	
ll_max++

ls_cod_blob = string(ll_max, "000")
guo_functions.uof_file_to_blob(as_filename, lb_blob)

//lunghezza in BYTE del documento trascinato
ll_len = lenA(lb_blob)


guo_functions.uof_get_parametro("MLD", ll_maxKB)
if not isnull(ll_maxKB) and ll_maxKB>0 then
	if ll_len > ll_maxKB * 1024 then
		//documento oltre la grandezza massima pre-stabilita
		as_message = "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(ll_maxKB * 1024)+" KB) come da parametro multiazienda MLD"
		return false
	end if
end if

openwithparm(w_inserisci_altro_valore, ls_cod_blob)

if message.stringparm <> ESC_COMMAND then
	
	ls_cod_blob = left(message.stringparm, 15)
	
	if isnull(ls_cod_blob) or len(trim(ls_cod_blob)) < 1 then
		as_message = "Inserire almeno un carattere come codice documento"
		return false
	end if
	
	select count(*)
	into :ll_count
	from anag_prodotti_blob
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto and 
			cod_blob = :ls_cod_blob;
			
	if sqlca.sqlcode = 0 and ll_count > 0 then
		
		as_message = "Il codice documento " + ls_cod_blob + " è già stato utilizzato"
		return false
		
	end if
	
end if
	

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

insert into anag_prodotti_blob (
	cod_azienda,   
	cod_prodotto,   
	cod_blob,   
	des_blob,   
	note,   
	blob,   
	path_documento,   
	tipo_foto,   
	hit,   
	ultimo_hit,   
	prog_mimetype 
)  values (
	:s_cs_xx.cod_azienda,   
	:ls_cod_prodotto,   
	:ls_cod_blob,   
	:ls_file,   
	null,   
	null,   
	null,   
	null,   
	null,   
	null,   
	:ll_prog_mimetype) ;


if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante l'inserimento del nuovo documento", sqlca)
	return false
end if

updateblob anag_prodotti_blob
set blob = :lb_blob
where cod_azienda = :s_Cs_xx.cod_azienda and
		  cod_prodotto = :ls_cod_prodotto and
		  cod_blob = :ls_cod_blob;

return sqlca.sqlcode = 0
end event

event ue_end_drop;call super::ue_end_drop;if ab_status then
	commit;
else
	g_mb.error(as_message)
	rollback;
end if


if idw_data.getrow() > 0 then
	retrieve(s_cs_xx.cod_azienda, idw_data.getitemstring(idw_data.getrow(), "cod_prodotto"))
end if

return true
end event

event buttonclicked;call super::buttonclicked;string			ls_cod_blob, ls_des_blob, ls_cod_prodotto, ls_valore, ls_errore, ls_sql, ls_key
long			ll_prog_mimetype
blob			lb_blob

integer		li_disabilita, li_ret

uo_log_sistema		luo_log

transaction sqlc_blob



if row>0 then
else
	return
end if


ls_cod_prodotto = getitemstring(row, "cod_prodotto")
ls_cod_blob = getitemstring(row, "cod_blob")
ls_des_blob = getitemstring(row, "des_blob")

choose case dwo.name
	//----------------------------------------------------------------------------------------------------------------------------
	case "b_canc"
		//il documento è cancellabile solo se abilitato, e solo da un utente supervisore
		
		li_ret = uof_abilita_disabilita(ls_cod_prodotto, ls_cod_blob, li_disabilita, ls_errore)
		if li_ret<0 then
			g_mb.error(ls_errore)
			return
		elseif li_ret>0 then
			g_mb.warning(ls_errore)
			return
		else
			if li_disabilita=1 then
				//il documento è abilitato, prosegui
			else
				//il documento è DISABILITATO: impossibile proseguire
				g_mb.warning("Il documento "+ ls_cod_blob + " è disabilitato!")
				return
			end if
		end if
		
		if not uof_supervisore() then
			g_mb.warning("Non sei autorizzato ad effettuare questa operazione!")
			return
		end if
		
		if g_mb.confirm("Cancellare il documento " + ls_cod_blob + " - " + ls_des_blob + "?") then
			
			delete from anag_prodotti_blob
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_blob = :ls_cod_blob;
			commit;
			retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
		end if
	
	
	case "b_disable"
		//devi abilitare o disabilitare?		
		//se collegato non puoi disabilitare
		li_ret = uof_abilita_disabilita(ls_cod_prodotto, ls_cod_blob, li_disabilita, ls_errore)
		if li_ret<0 then
			g_mb.error(ls_errore)
			return
		elseif li_ret>0 then
			g_mb.warning(ls_errore)
			return
		else
			if li_disabilita=1 then
				ls_errore = "Procedo con la disabilitazione del documento " + ls_cod_blob + " ?"
				ls_key = ""
			else
				ls_errore = "Procedo con la riabilitazione del documento " + ls_cod_blob + " ?"
			end if
		end if
	
		//se hai richiesto di ri-abilitare e non sei supervisore non permetterlo
		if li_disabilita=1 then
		else
			if not uof_supervisore() then
				g_mb.warning("Non sei autorizzato ad effettuare questa operazione!")
				return
			end if
		end if
		
		if not g_mb.confirm(ls_errore) then return 
		
		ls_errore = ""
		// creo transazione separata
		if not guo_functions.uof_create_transaction_from( sqlca, sqlc_blob, ls_errore)  then
			g_mb.error("Errore in creazione transazione: " + ls_errore)
			return
		end if
		
		ls_sql = 	"update anag_prodotti_blob " + &
					"set disabilitato="+string(li_disabilita) + " " + &
					"where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
							  "cod_prodotto='" + ls_cod_prodotto + "' and " + &
							  "cod_blob='"+ ls_cod_blob + "' "
		
		execute immediate :ls_sql using sqlc_blob;
		
		ls_key =  "cod_prodotto='" + ls_cod_prodotto + " and cod_blob='"+ ls_cod_blob + " "
		
		if sqlc_blob.sqlcode <> 0 then
			ls_errore = "Errore in abilitazione/disabilitazione documento: " + sqlc_blob.sqlerrtext
			
			g_mb.error(ls_errore)
			rollback using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
		else
			commit using sqlc_blob;
			disconnect using sqlc_blob;
			destroy sqlc_blob
			
			//######################################################
			//scrivo in log sistema
			if li_disabilita = 1 then
				//hai disabilitato
				ls_errore = "DISABILITAZIONE documento"
			else
				//hai abilitato
				ls_errore = "ABILITAZIONE documento"
			end if
			
			ls_errore += " in tabella anag_prodotti_blob ("+ls_key+")"
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DOCS", ls_errore)
			destroy luo_log
			//######################################################
			
		end if		
		
		retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

		
		
end choose

end event

event doubleclicked;call super::doubleclicked;string					ls_valore, ls_cod_prodotto, ls_cod_blob, ls_estensione, ls_temp_dir, ls_rnd, ls_file_name, ls_errore
long					ll_prog_mimetype
blob					lb_blob
uo_shellexecute	luo_run
integer				li_ret, li_disabilitato


if row>0 then
else
	return
end if

ls_cod_prodotto = getitemstring(row, "cod_prodotto")
ls_cod_blob = getitemstring(row, "cod_blob")


choose case dwo.name
		
	case "cf_riga"
		
		//se disabilitato non permettere di visualizzare il documento
		li_ret = uof_abilita_disabilita(ls_cod_prodotto, ls_cod_blob, li_disabilitato, ls_errore)
		
		if li_ret<0 then
			g_mb.error(ls_errore)
			return
		elseif li_ret>0 then
			g_mb.warning(ls_errore)
			return
		else
			if li_disabilitato=1 then
				//il documento è abilitato, prosegui
			else
				//il documento è DISABILITATO: impossibile proseguire
				g_mb.warning("Il documento "+ ls_cod_blob + " è disabilitato!")
				return
			end if
		end if
		
		ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
		
		selectblob blob
		into :lb_blob
		from anag_prodotti_blob
		where cod_azienda = :S_Cs_xx.cod_azienda and
				  cod_prodotto = :ls_cod_prodotto and
				  cod_blob = :ls_cod_blob;
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
	
end choose
end event

event constructor;call super::constructor;string					ls_file, ls_file2,ls_expression



ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_grigio.png"
ls_expression = "'"+ls_file+"'"
ls_expression = "bitmap("+ls_expression+")"
object.cf_bitmap.expression = ls_expression

//legenda
object.cf_legenda_note.expression = ls_expression


ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_rosso.png"
ls_expression = "'"+ls_file+"'"
ls_expression = "bitmap("+ls_expression+")"
object.cf_bitmap2.expression = ls_expression

//legenda
object.cf_legenda_note2.expression = ls_expression


ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_editor_doc_child.png"
ls_expression = "'"+ls_file+"'"
ls_expression = "bitmap("+ls_expression+")"
object.cf_mod_des_blob.expression = ls_expression

//legenda
object.cf_legenda_mod_des_blob.expression = ls_expression


ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png"
ls_expression = "'"+ls_file+"'"
ls_expression = "bitmap("+ls_expression+")"
object.cf_mod_note.expression = ls_expression

//legenda
object.cf_legenda_mod_note.expression = ls_expression

end event

event clicked;call super::clicked;string			ls_cod_blob, ls_des_blob, ls_cod_prodotto, ls_valore
long			ll_prog_mimetype
blob			lb_blob

if row>0 then
else
	return
end if


ls_cod_prodotto = getitemstring(row, "cod_prodotto")
ls_cod_blob = getitemstring(row, "cod_blob")
ls_des_blob = getitemstring(row, "des_blob")

choose case dwo.name
	//----------------------------------------------------------------------------------------------------------------------------
	case "cf_mod_des_blob"
		openwithparm(w_inserisci_altro_valore, getitemstring(row, "des_blob"))
		ls_valore = message.stringparm
		
		if ls_valore <> ESC_COMMAND then
			update anag_prodotti_blob
			set des_blob = :ls_valore
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_blob = :ls_cod_blob;
			commit;

			event post ue_retrieve(ls_cod_prodotto)
			
		end if
		
	case "cf_mod_note"
		openwithparm(w_inserisci_altro_valore, getitemstring(row, "note"))
		ls_valore = message.stringparm
		
		if ls_valore <> ESC_COMMAND then
			
			update anag_prodotti_blob
			set note = :ls_valore
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_blob = :ls_cod_blob;
					 
			commit;
			
			event post ue_retrieve(ls_cod_prodotto)
			
		end if
		
end choose

end event

