﻿$PBExportHeader$uo_prodotti.sru
forward
global type uo_prodotti from nonvisualobject
end type
end forward

global type uo_prodotti from nonvisualobject
end type
global uo_prodotti uo_prodotti

type variables

end variables

forward prototypes
public function boolean uof_duplica_centri_costo (string as_cod_prodotto_origine, string as_cod_prodotti[], ref string as_error)
public function boolean uof_duplica_depositi (string as_cod_prodotto_origine, string as_cod_prodotti[], ref string as_error)
public function decimal uof_ultimo_prezzo_mov_mag (string as_cod_prodotto, datetime adt_data_registrazione, ref string as_error)
public function decimal uof_ultimo_prezzo_mov_mag (string as_cod_prodotto, datetime adt_data_registrazione, string as_flag_agg_mov, ref string as_error)
end prototypes

public function boolean uof_duplica_centri_costo (string as_cod_prodotto_origine, string as_cod_prodotti[], ref string as_error);/**
 * stefanop
 * 01/08/2011
 *
 * Funzione per duplicare i centri di costo da un prodotto di orgine a N prodotti di destinazione
 * In caso di errore ritorna FALSE ed il messaggio di errore nella variabile as_errore
 **/
 
string ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_centro_costo, ls_cod_prodotto_origine, &
       ls_cod_prodotto_corrente
dec{4} ld_percentuale
long ll_i

setnull(as_error)

if upperbound(as_cod_prodotti) < 1 then return true

for ll_i = 1 to upperbound(as_cod_prodotti)
		
	ls_cod_prodotto_corrente = as_cod_prodotti[ll_i]
	
	declare cu_centri_costo_acq cursor for  
		select cod_centro_costo, percentuale  
		from anag_prodotti_cc_acq  
		where 
			cod_azienda = :s_cs_xx.cod_azienda and  
			cod_prodotto = :as_cod_prodotto_origine
		order by cod_centro_costo asc  ;

	declare cu_centri_costo_ven cursor for  
		select cod_centro_costo, percentuale  
		from anag_prodotti_cc_ven  
		where 
			cod_azienda = :s_cs_xx.cod_azienda and  
			cod_prodotto = :as_cod_prodotto_origine
		order by cod_centro_costo asc  ;
	
	//  procedo con cc acquisti
	delete from anag_prodotti_cc_acq
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in delete anag_prodotti_cc_acq.~r~n" + sqlca.sqlerrtext
		return false
	end if
	
	open cu_centri_costo_acq;
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in OPEN cursore cu_centri_costo_acq.~r~n" + sqlca.sqlerrtext
		return false
	end if
	
	do while true
		fetch cu_centri_costo_acq into :ls_cod_centro_costo, :ld_percentuale;
		
		if sqlca.sqlcode = -1 then
			as_error = "Errore in FETCH cursore cu_centri_costo.~r~n" + sqlca.sqlerrtext
			return false
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		insert into anag_prodotti_cc_acq (
			cod_azienda,
			cod_prodotto,
			cod_centro_costo,
			percentuale)
		values (
			:s_cs_xx.cod_azienda,
			:ls_cod_prodotto_corrente,
			:ls_cod_centro_costo,
			:ld_percentuale);
		
		if sqlca.sqlcode <> 0 then
			as_error = "Errore in insert anag_prodotti_cc_acq.~r~n" + sqlca.sqlerrtext
			return false
		end if
		
	loop
	
	close cu_centri_costo_acq;
	
	
	// procedo con cc vendite
	delete from anag_prodotti_cc_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in delete anag_prodotti_cc_ven.~r~n" + sqlca.sqlerrtext
		return false
	end if
	
	open cu_centri_costo_ven;
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in OPEN cursore cu_centri_costo_ven.~r~n" + sqlca.sqlerrtext
		return false
	end if
	
	do while true
		fetch cu_centri_costo_ven into :ls_cod_centro_costo, :ld_percentuale;
		
		if sqlca.sqlcode = -1 then
			as_error = "Errore in FETCH cursore cu_centri_costo_ven.~r~n" + sqlca.sqlerrtext
			return false
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		insert into anag_prodotti_cc_ven (
			cod_azienda,
			cod_prodotto,
			cod_centro_costo,
			percentuale)
		values (
			:s_cs_xx.cod_azienda,
			:ls_cod_prodotto_corrente,
			:ls_cod_centro_costo,
			:ld_percentuale);
		
		if sqlca.sqlcode <> 0 then
			as_error = "Errore in insert anag_prodotti_cc_ven.~r~n" + sqlca.sqlerrtext
			return false
		end if
		
	loop
	
	close cu_centri_costo_ven;
	
next

return true
end function

public function boolean uof_duplica_depositi (string as_cod_prodotto_origine, string as_cod_prodotti[], ref string as_error);/**
 * stefanop
 * 01/08/2011
 *
 * Funzione per duplicare i depositi da un prodotto di orgine a N prodotti di destinazione
 * In caso di errore ritorna FALSE ed il messaggio di errore nella variabile as_errore
 **/
 
 long ll_i
 string ls_cod_prodotto_corrente
 
 if upperbound(as_cod_prodotti) < 1 then return true
 
 for ll_i = 1 to upperbound(as_cod_prodotti)
 	
	 ls_cod_prodotto_corrente = as_cod_prodotti[ll_i]
	 
 	//pulisco quello che c'è, se c'è, sul prodotto destinazione
	delete from anag_prodotti_reparti_depositi
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in delete anag_prodotti_reparti_depositi.~r~n" + sqlca.sqlerrtext
		return false
	end if
 	
	 //ricopio la configurazione reparti dal prodotto origine al prodotto destinazione
	 insert into anag_prodotti_reparti_depositi
		(cod_azienda,
		cod_prodotto,
		cod_deposito_origine,
		cod_deposito_produzione,
		cod_reparto_produzione)
	select  cod_azienda,
			:ls_cod_prodotto_corrente,
			cod_deposito_origine,
			cod_deposito_produzione,
			cod_reparto_produzione
	from anag_prodotti_reparti_depositi
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto_origine;
	
	if sqlca.sqlcode <> 0 then
		as_error = "Errore in insert anag_prodotti_reparti_depositi.~r~n" + sqlca.sqlerrtext
		return false
	end if
				
next

return true

 
// 
// 
//string ls_cod_prodotto_corrente, ls_cod_deposito_origine, ls_cod_deposito_produzione, ls_cod_reparto_produzione  
//dec{4} ld_percentuale
//long ll_i
//
//setnull(as_error)
//
//if upperbound(as_cod_prodotti) < 1 then return true
//
//for ll_i = 1 to upperbound(as_cod_prodotti)
//		
//	ls_cod_prodotto_corrente = as_cod_prodotti[ll_i]
//	
//	declare cu_prod_deposititi cursor for  
//		select cod_deposito_origine, cod_deposito_produzione, cod_reparto_produzione  
//		from anag_prodotti_reparti_depositi  
//		where 
//			cod_azienda = :s_cs_xx.cod_azienda and  
//			cod_prodotto = :as_cod_prodotto_origine
//		order by cod_deposito_origine asc  ;
//	
//	//  procedo con cc acquisti
//	delete from anag_prodotti_reparti_depositi
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		cod_prodotto = :ls_cod_prodotto_corrente;
//			 
//	if sqlca.sqlcode <> 0 then
//		as_error = "Errore in delete anag_prodotti_reparti_depositi.~r~n" + sqlca.sqlerrtext
//		return false
//	end if
//	
//	open cu_prod_deposititi;
//	if sqlca.sqlcode <> 0 then
//		as_error = "Errore in OPEN cursore cu_prod_deposititi.~r~n" + sqlca.sqlerrtext
//		return false
//	end if
//	
//	do while true
//		fetch cu_prod_deposititi into :ls_cod_deposito_origine, :ls_cod_deposito_produzione, :ls_cod_reparto_produzione;
//		
//		if sqlca.sqlcode = -1 then
//			as_error = "Errore in FETCH cursore cu_prod_deposititi.~r~n" + sqlca.sqlerrtext
//			return false
//		end if
//		
//		if sqlca.sqlcode = 100 then exit
//		
//		insert into anag_prodotti_reparti_depositi (
//			cod_azienda,
//			cod_prodotto,
//			cod_deposito_origine,
//			cod_deposito_produzione,
//			cod_reparto_produzione)
//		values (
//			:s_cs_xx.cod_azienda,
//			:ls_cod_prodotto_corrente,
//			:ls_cod_deposito_origine,
//			:ls_cod_deposito_produzione,
//			:ls_cod_reparto_produzione);
//		
//		if sqlca.sqlcode <> 0 then
//			as_error = "Errore in insert anag_prodotti_reparti_depositi.~r~n" + sqlca.sqlerrtext
//			return false
//		end if
//		
//	loop
//	
//	close cu_prod_deposititi;
//	
//next
//
//return true
end function

public function decimal uof_ultimo_prezzo_mov_mag (string as_cod_prodotto, datetime adt_data_registrazione, ref string as_error);/**
 * EnMe
 * 04/07/2012
 *
 * per caricamento prezzo da ultima fattura di acquisto in caso di bolle di trasferimento interno
 *
 * Ritorna:
 * -1 errore se errore altrimenti il prezzo trovato
 **/
 
 
string ls_sql_prezzo,ls_rag_soc_for
long ll_ret, ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_reg_tes_fat,ll_num_reg_tes_fat
dec{4} ld_prezzo_trasferimento
datetime ldt_data_protocollo
datastore lds_fatture

ls_sql_prezzo = 	"select tes_fat_acq.data_protocollo, mov_magazzino.val_movimento, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione, tes_fat_acq.anno_registrazione, tes_fat_acq.num_registrazione, anag_fornitori.rag_soc_1 from tes_fat_acq " + &
						" join det_fat_acq on tes_fat_acq.anno_registrazione =  det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione =  det_fat_acq.num_registrazione " + &
						" join anag_fornitori on tes_fat_acq.cod_azienda =  anag_fornitori.cod_azienda and tes_fat_acq.cod_fornitore =  anag_fornitori.cod_fornitore " + &
						" join mov_magazzino on  det_fat_acq.anno_registrazione_mov_mag = mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione " + &
						" where data_protocollo <= '"+string(adt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and flag_agg_mov='S' and det_fat_acq.cod_prodotto = '" + as_cod_prodotto + "' " + &
						" order by  tes_fat_acq.data_protocollo DESC , tes_fat_acq.num_registrazione DESC , det_fat_acq.prog_riga_fat_acq DESC "
			

ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo)

if ll_ret < 0 then
	destroy lds_fatture
	as_error = "Si è verificato un errore cercando il prezzo del prodotto " + as_cod_prodotto + " dalle fatture di acquisto "
	return -1
end if

if ll_ret > 0 then
	
	ldt_data_protocollo = lds_fatture.getitemdatetime(1, 1)
	ld_prezzo_trasferimento = lds_fatture.getitemnumber (1, 2)
	ll_anno_reg_mov_mag = lds_fatture.getitemnumber (1, 3)
	ll_num_reg_mov_mag = lds_fatture.getitemnumber (1, 4)
	ll_anno_reg_tes_fat = lds_fatture.getitemnumber (1, 5)
	ll_num_reg_tes_fat = lds_fatture.getitemnumber (1, 6)
	ls_rag_soc_for = lds_fatture.getitemstring (1, 7)
	
	as_error = "Applicato Prezzo di acquisto "+string(ld_prezzo_trasferimento,"###,##0.00")+" riferito alla data " + string(ldt_data_protocollo,"dd/mm/yyyy") + " (Mov Mag:" + string(ll_anno_reg_mov_mag) + "-" + string(ll_num_reg_mov_mag) + "  Nr.Int.FT:" + string(ll_anno_reg_tes_fat) + "-" + string(ll_num_reg_tes_fat) + " Fornitore: " + ls_rag_soc_for + ")"
else
	as_error = "Nessun prezzo di trasferimento reperibile dalle fatture di acquisto."
	ld_prezzo_trasferimento = 0
end if

destroy lds_fatture

return ld_prezzo_trasferimento

end function

public function decimal uof_ultimo_prezzo_mov_mag (string as_cod_prodotto, datetime adt_data_registrazione, string as_flag_agg_mov, ref string as_error);/**
 * EnMe
 * 04/07/2012
 *
 * per caricamento prezzo da ultima fattura di acquisto in caso di bolle di trasferimento interno
 *
 * Ritorna:
 * -1 errore se errore altrimenti il prezzo trovato
 **/
 
 
string ls_sql_prezzo,ls_rag_soc_for
long ll_ret, ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_reg_tes_fat,ll_num_reg_tes_fat, ll_i
dec{4} ld_prezzo_trasferimento
datetime ldt_data_protocollo
datastore lds_fatture

ls_sql_prezzo = 	"select tes_fat_acq.data_protocollo, mov_magazzino.val_movimento, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione, tes_fat_acq.anno_registrazione, tes_fat_acq.num_registrazione, anag_fornitori.rag_soc_1 from tes_fat_acq " + &
						" join det_fat_acq on tes_fat_acq.anno_registrazione =  det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione =  det_fat_acq.num_registrazione " + &
						" join anag_fornitori on tes_fat_acq.cod_azienda =  anag_fornitori.cod_azienda and tes_fat_acq.cod_fornitore =  anag_fornitori.cod_fornitore " + &
						" join mov_magazzino on  det_fat_acq.anno_registrazione_mov_mag = mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione " + &
						" where data_protocollo <= '"+string(adt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and flag_agg_mov='"+as_flag_agg_mov+"' and det_fat_acq.cod_prodotto = '" + as_cod_prodotto + "' "

if as_flag_agg_mov = "S" then
	ls_sql_prezzo += " order by  tes_fat_acq.data_protocollo DESC , tes_fat_acq.num_registrazione DESC , det_fat_acq.prog_riga_fat_acq DESC "
else
	// In caso di flag_agg_mov = 'N' allora prendo il primo valore dall'inzio anno
	ls_sql_prezzo += " order by  tes_fat_acq.data_protocollo ASC , tes_fat_acq.num_registrazione ASC , det_fat_acq.prog_riga_fat_acq ASC "
end if
			

ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo)

if ll_ret < 0 then
	destroy lds_fatture
	as_error = "Si è verificato un errore cercando il prezzo del prodotto " + as_cod_prodotto + " dalle fatture di acquisto "
	return -1
end if

if ll_ret > 0 then
	
	ll_i = 1
	
	if as_flag_agg_mov = "N" then
		// recupero la prima riga con prezzo valido
		for ll_i = 1 to ll_ret
			ld_prezzo_trasferimento = lds_fatture.getitemnumber (ll_i, 2)
			if not isnull(ld_prezzo_trasferimento) and ld_prezzo_trasferimento > 0 then
				// Ho trovato la riga con il prezzo valido; oppure sono arrivato alla fine e nessuno e valido!
				return ld_prezzo_trasferimento
			end if
		next
		
		return 0
	end if
	
	ldt_data_protocollo = lds_fatture.getitemdatetime(ll_i, 1)
	ld_prezzo_trasferimento = lds_fatture.getitemnumber (ll_i, 2)
	ll_anno_reg_mov_mag = lds_fatture.getitemnumber (ll_i, 3)
	ll_num_reg_mov_mag = lds_fatture.getitemnumber (ll_i, 4)
	ll_anno_reg_tes_fat = lds_fatture.getitemnumber (ll_i, 5)
	ll_num_reg_tes_fat = lds_fatture.getitemnumber (ll_i, 6)
	ls_rag_soc_for = lds_fatture.getitemstring (ll_i, 7)
	
	as_error = "Applicato Prezzo di acquisto "+string(ld_prezzo_trasferimento,"###,##0.00")+" riferito alla data " + string(ldt_data_protocollo,"dd/mm/yyyy") + " (Mov Mag:" + string(ll_anno_reg_mov_mag) + "-" + string(ll_num_reg_mov_mag) + "  Nr.Int.FT:" + string(ll_anno_reg_tes_fat) + "-" + string(ll_num_reg_tes_fat) + " Fornitore: " + ls_rag_soc_for + ")"
else
	as_error = "Nessun prezzo di trasferimento reperibile dalle fatture di acquisto."
	ld_prezzo_trasferimento = 0
end if

destroy lds_fatture

return ld_prezzo_trasferimento

end function

on uo_prodotti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_prodotti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

