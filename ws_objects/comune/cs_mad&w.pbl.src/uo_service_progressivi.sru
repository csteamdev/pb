﻿$PBExportHeader$uo_service_progressivi.sru
forward
global type uo_service_progressivi from uo_service_base
end type
end forward

global type uo_service_progressivi from uo_service_base
end type
global uo_service_progressivi uo_service_progressivi

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uf_ordinato (string as_cod_prodotto, ref string as_errore)
public function integer uf_assegnato (string as_cod_prodotto, ref string as_errore)
public function integer uf_in_produzione (string as_cod_prodotto, ref string as_errore)
public function integer uf_elabora (string as_modalita, string as_cod_prodotto, ref string as_errore)
public function integer uf_progressivi (string as_cod_prodotto, ref string as_errore)
public function integer uf_impegnato (string as_cod_prodotto, ref string as_errore)
public function integer uf_in_spedizione (string as_cod_prodotto, ref string as_errore)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);
string						ls_path_log, ls_modalita, ls_cod_prodotto_like, ls_errore, ls_cod_prodotto_cu, ls_array[], ls_perc

datetime					ldt_oggi

long						ll_index, ll_tot, ll_ok, ll_ko, ll_percentuale

integer					li_ret



//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=progressivi,A01,X,W,N
//																		dove
//																				X è una lettera maiuscola che identifica il valore da ricalcolare secondo la seguente tabella
//																						P		progressivi anagrafica prodotto
//																						I		impegnato
//																						S		in spedizione (ddt e fatture vendita non confermati)
//																						A		assegnato (in evasione)
//																						F		ordinato a fornitore
//																						Z		in produzione
//																						%		tutto
//																					NOTA --------------------------------------------------------------------------------
//																					il comando può essere impostato anche nel modo seguente:     PIA
//																					per eseguire ad esempio il ricolacolo progressivi, impegnato e assegnato ...
//																					----------------------------------------------------------------------------------------
//																				
//																				W è una condizione su prodotto da interpretarsi come like
//																					(passo % se devo elaborare tutto oppure metto una parte di codice o un codice prodotto intero per elaborazioni mirate
//
//																				N è un numero che indica la verbosità del log (min 1, max 5, default 2)
//
// esempi		cs_team.exe S=progressivi,A01,%,%,3				-> elabora tutti i valori su tutti i prodotti
//					cs_team.exe S=progressivi,A01,%,A2,3				-> elabora tutti i valori sui prodotti che iniziano per A2
//					cs_team.exe S=progressivi,A01,%,A2%,3			-> elabora tutti i valori sui prodotti che iniziano per A2
//					cs_team.exe S=progressivi,A01,P,%,3				-> elabora i soli progressivi su tutti i prodotti
//##########################################################################################


//PARAMETRI ----------------------------------------------------------------------------------------------------------------------------
//imposto l'azienda (se non passo niente metto A01)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

//cosa devo elaborare?
ls_modalita = auo_params.uof_get_string(2,"%")

//where su cod.prodotto
ls_cod_prodotto_like = auo_params.uof_get_string(3,"")
if g_str.isempty(ls_cod_prodotto_like) then ls_cod_prodotto_like = "%"
if right(ls_cod_prodotto_like, 1) <>"%" then ls_cod_prodotto_like = ls_cod_prodotto_like + "%"


//livello di logging (1..5 se non passo niente metto 2)
iuo_log.set_log_level(auo_params.uof_get_int(4, 2))

//FILE DI LOG -----------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log
ls_path_log =  s_cs_xx.volume + s_cs_xx.risorse + "logs\ricalcolo_progressivi.log"

//il file di LOG viene ad ogni elaborazione sovrascritto
iuo_log.set_file( ls_path_log, true)

iuo_log.log("### INIZIO ELABORAZIONE -  MODALITA: "+ls_modalita+"  #######################")
iuo_log.log("P		progressivi anagrafica prodotto~r~n"+&
"I		impegnato~r~n"+&
"S		in spedizione (ddt e fatture vendita non confermati)~r~n"+&
"A		assegnato (in evasione)~r~n"+&
"F		ordinato a fornitore~r~n"+&
"Z		in produzione~r~n"+&
"%		tutto~r~n")


//preparo in un array i prodotti da elaborare ------------------------------------------------------------------------------------------
ldt_oggi = datetime(today())

declare cu_prodotti cursor for 
select distinct cod_prodotto
from  mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto like :ls_cod_prodotto_like and data_registrazione <= :ldt_oggi
order by cod_prodotto;

ll_index = 0

open cu_prodotti;
if sqlca.sqlcode <> 0 then
	ls_errore = "Errore in apertura cursore cu_prodotti: " + sqlca.sqlerrtext
	iuo_log.error(ls_errore)
	return -1
end if

do while true
	fetch cu_prodotti into :ls_cod_prodotto_cu;
	
	if sqlca.sqlcode < 0 then
		ls_errore="Errore durante fetch prodotti: "+sqlca.sqlerrtext
		close cu_prodotti;
		iuo_log.error(ls_errore)
		return 0
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if not isnull(ls_cod_prodotto_cu) and ls_cod_prodotto_cu<>"" then
		ll_index += 1
		ls_array[ll_index] = ls_cod_prodotto_cu
	end if
loop
close cu_prodotti;

ll_tot = upperbound(ls_array[])

iuo_log.log("Totale prodotti da elaborare: " + string(ll_tot))

 ll_ok = 0
 ll_ko = 0


for ll_index=1 to ll_tot
	//percorso completo del file
	ls_cod_prodotto_cu =ls_array[ll_index]
	
	if ll_index < ll_tot then
		ls_perc = right(string("00") + string((ll_index * 100) / ll_tot), 2)
	else
		ls_perc = "100"
	end if
	
	iuo_log.log(ls_perc + " % : articolo " + ls_cod_prodotto_cu + "~r~n")
	
	//-----------------------------------------
	//0 tutto OK
	//-1 fallito, scrivi nel log e continua con il prodotto successivo
	li_ret = uf_elabora(ls_modalita, ls_cod_prodotto_cu, ls_errore)
	
	if li_ret = -1 then
		ll_ko += 1
		iuo_log.error(ls_errore + " errore generato durante l'elaborazione del prodotto " + ls_cod_prodotto_cu)
		rollback;
		continue
		
	elseif  li_ret = -2 then
		//stop
		iuo_log.error(ls_errore)
		rollback;
		
		iuo_log.log("Elaborazione interrotta, Totale prodotti elaborati: " + string(ll_tot) + "  -  Con Successo: " + string(ll_ok) + "  - con Errori : " + string(ll_ko))
		iuo_log.log("### FINE ELABORAZIONE ANTICIPATA ##################")
		
		//quindi interrompi l'elaborazione
		return 0
	end if
	//------------------------------------------
	
	ll_ok += 1
	
	//ad ogni prodotto (oltre ad ogni singola azione)
	commit;
	
next

iuo_log.log("Elaborazione terminata, Totale prodotti elaborati: " + string(ll_tot) + "  -  Con Successo: " + string(ll_ok) + "  - con Errori : " + string(ll_ko))
iuo_log.log("### FINE ELABORAZIONE #######################")

return 0
end function

public function integer uf_ordinato (string as_cod_prodotto, ref string as_errore);
uo_magazzino				luo_magazzino

integer						li_ret

string							ls_cod_misura_mag, ls_testo

dec{4}						ld_qta_ordinata_old, ld_qta_ordinata



select quan_ordinata
into :ld_qta_ordinata_old
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;
			
if isnull(ld_qta_ordinata_old) then ld_qta_ordinata_old = 0


luo_magazzino = create uo_magazzino
li_ret = luo_magazzino.uof_ricalcola_ordinato_fornitore(as_cod_prodotto, as_errore)
destroy luo_magazzino

if li_ret < 0 then
	return -1
else
	select quan_ordinata, cod_misura_mag
	into :ld_qta_ordinata, :ls_cod_misura_mag
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto;
	
	if isnull(ld_qta_ordinata) then ld_qta_ordinata = 0
	
	ls_testo = "Qordinata = " + string(ld_qta_ordinata, "###,###,##0.00") + " " + ls_cod_misura_mag			+ "  -> (era "+string(ld_qta_ordinata_old, "###,###,##0.00")+")" 
	iuo_log.log(ls_testo)
	
end if


return 0
end function

public function integer uf_assegnato (string as_cod_prodotto, ref string as_errore);string							ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_sql, ls_str, ls_cod_misura_mag, ls_testo
long							ll_prog_stock, ll_tot, ll_index
dec{4}						ld_quan_in_evasione, ld_quan_in_evasione_old
datetime						ldt_data_stock
datastore					lds_data


ls_sql = "SELECT    cod_deposito,"+&
						"cod_ubicazione,"+&
						"cod_lotto,"+&
						"data_stock,"+&
						"prog_stock,"+&
						"quan_in_evasione "+&
 			"FROM  evas_ord_ven "+&
			"WHERE  cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
		 				"cod_prodotto = '"+as_cod_prodotto+"' and "+&
		 				"flag_spedito <>'S' "


select quan_assegnata
into :ld_quan_in_evasione_old
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;

if isnull(ld_quan_in_evasione_old) then ld_quan_in_evasione_old = 0


update anag_prodotti
set    quan_assegnata = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in azzeramento assegnato in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

update stock
set    quan_assegnata = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in azzeramento assegnato negli stock: " + sqlca.sqlerrtext
	return -1
end if

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot<0 then
	as_errore = "Errore in creazione datastore righe assegnate: " + as_errore
	return -1
else
	for ll_index=1 to ll_tot
		ls_cod_deposito			= lds_data.getitemstring(ll_index, 1)
		ls_cod_ubicazione			= lds_data.getitemstring(ll_index, 2)
		ls_cod_lotto					= lds_data.getitemstring(ll_index, 3)
		ldt_data_stock				= lds_data.getitemdatetime(ll_index, 4)
		ll_prog_stock				= lds_data.getitemnumber(ll_index, 5)
		ld_quan_in_evasione		= lds_data.getitemdecimal(ll_index, 6)
		
		if isnull(ld_quan_in_evasione) or ld_quan_in_evasione<0 then ld_quan_in_evasione = 0
		
		update anag_prodotti
		set    quan_assegnata = quan_assegnata + :ld_quan_in_evasione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :as_cod_prodotto;
				 
		if sqlca.sqlcode< 0 then
			destroy lds_data
			as_errore = "Errore in aggiornamento prodotti: " + sqlca.sqlerrtext
			return -1
		end if
	
		update stock
		set    quan_assegnata = quan_assegnata + :ld_quan_in_evasione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :as_cod_prodotto and
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_stock and
				 prog_stock = :ll_prog_stock;
				 
		if sqlca.sqlcode = -1 then
			destroy lds_data
			as_errore = "Errore in aggiornamento stock: " + sqlca.sqlerrtext
			return -1
		end if
		
	next
end if


destroy lds_data

select quan_assegnata, cod_misura_mag
into :ld_quan_in_evasione, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;

if isnull(ld_quan_in_evasione) then ld_quan_in_evasione = 0


ls_testo = "Qassegnata = " + string(ld_quan_in_evasione, "###,###,##0.00") + " " + ls_cod_misura_mag			+ "  -> (era "+string(ld_quan_in_evasione_old, "###,###,##0.00")+")" 
iuo_log.log(ls_testo)


return 0
end function

public function integer uf_in_produzione (string as_cod_prodotto, ref string as_errore);string						ls_cod_prodotto_raggruppato, ls_sql, ls_cod_misura_mag, ls_testo
		 
dec{4}					ld_quan_in_produzione, ld_quan_raggruppo, ld_quan_in_produzione_old

datastore				lds_data

long						ll_tot, ll_index



//commesse del prodotto non bloccate e in avanzamento
ls_sql = 	"select quan_in_produzione "+&
			"from  anag_commesse "+&
			"where  cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
						"flag_blocco = 'N' and "+&
						"cod_prodotto = '"+as_cod_prodotto+"' and "+&
						"(flag_tipo_avanzamento='2' or flag_tipo_avanzamento='3') and "+&
						"quan_in_produzione>0 "


select quan_in_produzione
into :ld_quan_in_produzione_old
from anag_prodotti
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;

if isnull(ld_quan_in_produzione_old) then ld_quan_in_produzione_old = 0


update anag_prodotti
set    quan_in_produzione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in azzeramento quantita in produzione in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

//gestione prodotto raggruppato
update anag_prodotti  
set quan_in_produzione = 0
 where cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_prodotto in (	select cod_prodotto_raggruppato 
								from  anag_prodotti 
								where	cod_azienda = :s_cs_xx.cod_azienda and
											cod_prodotto like :as_cod_prodotto and 
											cod_prodotto_raggruppato is not null);
if sqlca.sqlcode = -1 then
	as_errore = "Errore in azzeramento quantita in produzione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	as_errore = "Errore in creazione datastore commesse: " + as_errore
	return -1
	
elseif ll_tot > 0 then
	
	for ll_index=1 to ll_tot
		yield()
		ld_quan_in_produzione = lds_data.getitemdecimal(ll_index, 1)
		//#######################################################################
		//													q_prodotta			q_in_prod		q_ordine
		//#######################################################################
		//Quando creo la commessa:						0						0					8
		//Quando attivo											0						8					8
		//Mentre produco										3						5					8
		//....														6						2					8
		//alla chiusura											8						0					8
		//in ogni caso la quota parte per ogni commessa di prodotto in produzione è nella colonna quan_in_produzione
		
		update anag_prodotti
		set    quan_in_produzione = quan_in_produzione + :ld_quan_in_produzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :as_cod_prodotto;
				 
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
	
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :as_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
		
			ld_quan_raggruppo = f_converti_qta_raggruppo(as_cod_prodotto, ld_quan_in_produzione, "M")
		
			update anag_prodotti  
			set quan_in_produzione = quan_in_produzione + :ld_quan_raggruppo 
			 where	cod_azienda = :s_cs_xx.cod_azienda and  
						cod_prodotto = :ls_cod_prodotto_raggruppato;
						
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in aggiornamento quantità in spedizione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
	next
end if

select quan_in_produzione, cod_misura_mag
into :ld_quan_in_produzione, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;

if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0

ls_testo = "Qin_produzione = " + string(ld_quan_in_produzione, "###,###,##0.00") + " " + ls_cod_misura_mag			+ "  -> (era "+string(ld_quan_in_produzione_old, "###,###,##0.00")+")" 
iuo_log.log(ls_testo)

return 0

end function

public function integer uf_elabora (string as_modalita, string as_cod_prodotto, ref string as_errore);boolean					lb_almeno_uno = false
integer					li_ret


as_errore = ""

//commit (o rollback) su ogni quantità ricalcolata (progressivi, assegnato, impegnato, ecc...)


//###########################################################################
//progressivi
if as_modalita="P" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("PROGRESSIVI ----------------------")
	li_ret = uf_progressivi(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo progressivi!")
		iuo_log.log("-----------------------------------")
	end if
end if


//###########################################################################
//impegnato
if as_modalita="I" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("IMPEGNATO ----------------------")
	li_ret = uf_impegnato(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo impegnato!")
	end if
end if


//###########################################################################
//in spedizione
if as_modalita="S" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("IN SPEDIZIONE ----------------------")
	li_ret = uf_in_spedizione(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo in spedizione!")
	end if
end if


//###########################################################################
//ordinato a fornitore
if as_modalita="F" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("ORDINATO A FORNITORE ---------------------")
	li_ret = uf_ordinato(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo ordinato a fornitore!")
	end if
end if


//###########################################################################
//assegnato o in evasione
if as_modalita="A" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("ASSEGNATO (IN EVASIONE)------------------------")
	li_ret = uf_assegnato(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo assegnato!")
	end if
end if


//###########################################################################
//in produzione
if as_modalita="Z" or as_modalita="%" then
	lb_almeno_uno = true
	
	iuo_log.log("IN PRODUZIONE -------------------")
	li_ret = uf_in_produzione(as_cod_prodotto, as_errore)
	if li_ret<0 then
		rollback;
		return -1
	else
		commit;
		iuo_log.log("Eseguito ricalcolo in produzione!")
	end if
end if


if not lb_almeno_uno then
	iuo_log.error("Opzione non valida o non tra quelle previste!")
	return -2
end if


return 0
end function

public function integer uf_progressivi (string as_cod_prodotto, ref string as_errore);//string					ls_error, ls_str
//dec{4}   				ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
//						ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_giacenza[],ld_saldo_quan_anno_prec_1
			

//-------------------------------------------------
dec{4}				ld_quant_val_attuale[], ld_vuoto[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza[], &
						ld_saldo_quan_inizio_anno_old, ld_saldo_quan_ultima_chiusura_old, ld_prog_quan_entrata_old, ld_val_quan_entrata_old, &
						ld_prog_quan_uscita_old, ld_val_quan_uscita_old, ld_prog_quan_acquistata_old, ld_val_quan_acquistata_old, &
						ld_prog_quan_venduta_old, ld_val_quan_venduta_old, ld_saldo_quan_inizio_anno, ld_val_inizio_anno, &
						ld_saldo_quan_ultima_chiusura, ld_val_acq, ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, &
						ld_val_quan_uscita, ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, ld_val_quan_venduta

string					ls_chiave[], ls_vuoto[], ls_where, ls_testo, ls_cod_misura_mag

datetime				ldt_max_data_mov

uo_magazzino		luo_magazzino

integer				li_ret



// ****************************** Ricalcolo Progressivi Prodotto *************************************
// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
	
ld_quant_val_attuale[] 			= ld_vuoto[]
ls_chiave[] 							= ls_vuoto[]
ld_giacenza[] 						= ld_vuoto[]
ld_costo_medio_stock[] 			= ld_vuoto[]
ld_quan_costo_medio_stock[] 	= ld_vuoto[]
	
ldt_max_data_mov = datetime(today())

luo_magazzino = create uo_magazzino
li_ret = luo_magazzino.uof_saldo_prod_date_decimal( 	as_cod_prodotto, 	ldt_max_data_mov, ls_where, &
																		ld_quant_val_attuale[], as_errore, "N", &
																		ls_chiave[], ld_giacenza[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
destroy luo_magazzino


if li_ret < 0 then
	as_errore = "Errore nel calcolo della situazione attuale del prodotto " + as_cod_prodotto + ", " + as_errore
	return -1
end if

//leggo prima il pre-esistente
select		saldo_quan_inizio_anno, saldo_quan_ultima_chiusura, prog_quan_entrata, val_quan_entrata,
			prog_quan_uscita, val_quan_uscita, prog_quan_acquistata, val_quan_acquistata, prog_quan_venduta, val_quan_venduta, cod_misura_mag
into			:ld_saldo_quan_inizio_anno_old, :ld_saldo_quan_ultima_chiusura_old, :ld_prog_quan_entrata_old, :ld_val_quan_entrata_old,
				:ld_prog_quan_uscita_old, :ld_val_quan_uscita_old, :ld_prog_quan_acquistata_old, :ld_val_quan_acquistata_old, :ld_prog_quan_venduta_old, :ld_val_quan_venduta_old, :ls_cod_misura_mag
from anag_prodotti
where	cod_azienda = :s_cs_xx.cod_azienda and 
			cod_prodotto = :as_cod_prodotto;
			

if isnull(ld_saldo_quan_inizio_anno_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_saldo_quan_ultima_chiusura_old) then ld_saldo_quan_ultima_chiusura_old=0
if isnull(ld_prog_quan_entrata_old) then ld_prog_quan_entrata_old=0
if isnull(ld_val_quan_entrata_old) then ld_val_quan_entrata_old=0
if isnull(ld_prog_quan_uscita_old) then ld_prog_quan_uscita_old=0
if isnull(ld_val_quan_uscita_old) then ld_val_quan_uscita_old=0
if isnull(ld_prog_quan_acquistata_old) then ld_prog_quan_acquistata_old=0
if isnull(ld_val_quan_acquistata_old) then ld_val_quan_acquistata_old=0
if isnull(ld_prog_quan_venduta_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_val_quan_venduta_old) then ld_val_quan_venduta_old=0


ld_saldo_quan_inizio_anno = ld_quant_val_attuale[1]
ld_prog_quan_entrata = ld_quant_val_attuale[4]
ld_val_quan_entrata = ld_quant_val_attuale[5]
ld_prog_quan_uscita = ld_quant_val_attuale[6]
ld_val_quan_uscita = ld_quant_val_attuale[7]
ld_prog_quan_acquistata = ld_quant_val_attuale[8]
ld_val_quan_acquistata = ld_quant_val_attuale[9]
ld_prog_quan_venduta = ld_quant_val_attuale[10]
ld_val_quan_venduta = ld_quant_val_attuale[11]

update anag_prodotti 
set	 saldo_quan_inizio_anno = :ld_saldo_quan_inizio_anno,
		 saldo_quan_ultima_chiusura = :ld_saldo_quan_inizio_anno,
		 prog_quan_entrata = :ld_prog_quan_entrata,
		 val_quan_entrata = :ld_val_quan_entrata,
		 prog_quan_uscita = :ld_prog_quan_uscita,
		 val_quan_uscita = :ld_val_quan_uscita,
		 prog_quan_acquistata = :ld_prog_quan_acquistata,
		 val_quan_acquistata = :ld_val_quan_acquistata,
		 prog_quan_venduta = :ld_prog_quan_venduta,
		 val_quan_venduta = :ld_val_quan_venduta
where	cod_azienda = :s_cs_xx.cod_azienda and 
			cod_prodotto = :as_cod_prodotto;	

if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante aggiornamento progressivi prodotto: " + sqlca.sqlerrtext
	return -1
end if

//#######################################################################################

ls_testo = 	"SALDI QUANTITA'~r~n"+&
				"inizio anno = " + string(ld_saldo_quan_inizio_anno, "###,###,##0.00") + " " + ls_cod_misura_mag + " ("+string(ld_saldo_quan_inizio_anno_old, "###,###,##0.00")+")" + "~r~n" + &
				"ultima.chiusura =  " + string(ld_saldo_quan_inizio_anno,"###,###,##0.00")	+ " " + ls_cod_misura_mag + "  ("+string(ld_saldo_quan_ultima_chiusura_old, "###,###,##0.00")+")" +  "~r~n" + &
				"PROGRESSIVI QUANTITA' E VALORI~r~n"+&
				"entrata = " + string(ld_prog_quan_entrata,"###,###,##0.00") + " " + ls_cod_misura_mag + " con valore "	+ string(ld_val_quan_entrata,"###,###,##0.00") + " € " + &
												"  ("+string(ld_prog_quan_entrata_old, "###,###,##0.00")+"   " + string(ld_val_quan_entrata_old, "###,###,##0.00") +" € )~r~n" + &
				"uscita = " + string(ld_prog_quan_uscita,"###,###,##0.00") + " " + ls_cod_misura_mag + " con valore "	+ string(ld_val_quan_uscita,"###,###,##0.00") + " € " + &
												"  ("+string(ld_prog_quan_uscita_old, "###,###,##0.00")+"   " + string(ld_val_quan_uscita_old, "###,###,##0.00") +" € )~r~n" + &
				"acquistata = " + string(ld_prog_quan_acquistata,"###,###,##0.00") + " " + ls_cod_misura_mag + " con valore "	+ string(ld_val_quan_acquistata,"###,###,##0.00") + " € " + &
												"  ("+string(ld_prog_quan_acquistata_old, "###,###,##0.00")+"   " + string(ld_val_quan_acquistata_old, "###,###,##0.00") +" € )~r~n" + &
				"venduta = " + string(ld_prog_quan_venduta,"###,###,##0.00") + " " + ls_cod_misura_mag + " con valore "	+ string(ld_val_quan_venduta,"###,###,##0.00") + " € " + &
												"  ("+string(ld_prog_quan_venduta_old, "###,###,##0.00")+"   " + string(ld_val_quan_venduta_old, "###,###,##0.00") +" € )"
/*
ls_testo = 	"Saldo Qinizio.anno = " + string(ld_saldo_quan_inizio_anno, "###,###,##0.00")		+ "  -> (era "+string(ld_saldo_quan_inizio_anno_old, "###,###,##0.00")+")" + "~r~n" + &
				"Saldo Qultima.chius =  " + string(ld_saldo_quan_inizio_anno,"###,###,##0.00")	+ "  -> (era "+string(ld_saldo_quan_ultima_chiusura_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Prog Qentrata = " + string(ld_prog_quan_entrata,"###,###,##0.00")					+ "  -> (era "+string(ld_prog_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Val Qentrata = " + string(ld_val_quan_entrata,"###,###,##0.00")						+ "  -> (era "+string(ld_val_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Prog Quscita = " + string(ld_prog_quan_uscita,"###,###,##0.00")						+ "  -> (era "+string(ld_prog_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Val Quscita = " + string(ld_val_quan_uscita,"###,###,##0.00")							+ "  -> (era "+string(ld_val_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Prog Qacquistata = " + string(ld_prog_quan_acquistata,"###,###,##0.00")			+ "  -> (era "+string(ld_prog_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Val Qacquistata = " + string(ld_val_quan_acquistata,"###,###,##0.00")				+ "  -> (era "+string(ld_val_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Prog Qvenduta = " + string(ld_prog_quan_venduta,"###,###,##0.00")				+ "  -> (era "+string(ld_prog_quan_venduta_old, "###,###,##0.00")+")" +  "~r~n" + &
				"Val Qvenduta = " + string(ld_val_quan_venduta,"###,###,##0.00")					+ "  -> (era "+string(ld_val_quan_venduta_old, "###,###,##0.00")+")"
*/

iuo_log.log(ls_testo)

return 0
end function

public function integer uf_impegnato (string as_cod_prodotto, ref string as_errore);
uo_magazzino					luo_magazzino

dec{4}							ld_quan_impegnata_old, ld_quan_impegnata_new

integer							li_ret

string								ls_cod_misura_mag, ls_testo



//leggo l'impegnato prima
select quan_impegnata
into :ld_quan_impegnata_old
from anag_prodotti
where	cod_azienda  = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;
			 
if sqlca.sqlcode<0 then
	as_errore = "Errore lettura q.tà impegnata (prima) per il prodotto: " +sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode=100 then
	as_errore = "Prodotto "+as_cod_prodotto+" inesistente in anagrafica!"
	return -1
end if

luo_magazzino = CREATE uo_magazzino
li_ret = luo_magazzino.uof_ricalcola_impegnato(as_cod_prodotto, as_errore)

destroy luo_magazzino

if li_ret < 0 then
	as_errore = "Errore nel calcolo dell'impegnato del prodotto " + as_cod_prodotto + ", " + as_errore
	return -1
end if
	
//commit del singolo prodotto
commit;
	
//leggo l'impegnato dopo
select quan_impegnata, cod_misura_mag
into :ld_quan_impegnata_new, :ls_cod_misura_mag
from anag_prodotti
where  cod_azienda  = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;

if isnull(ld_quan_impegnata_new) then ld_quan_impegnata_new = 0
	
ls_testo = "Qimpegnata = " + string(ld_quan_impegnata_new, "###,###,##0.00") + " " + ls_cod_misura_mag			+ "  -> (era "+string(ld_quan_impegnata_old, "###,###,##0.00")+")" 

iuo_log.log(ls_testo)

return 0
end function

public function integer uf_in_spedizione (string as_cod_prodotto, ref string as_errore);string					ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto,ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
						ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_sql, ls_sql2, ls_str, ls_cod_misura_mag, ls_testo
						
long					ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, &
						ll_index, ll_tot

dec{4}				ld_quan_consegnata, ld_quan_consegnata_old

datetime				ldt_data_stock

datastore			lds_data


ls_sql = "SELECT		a.cod_tipo_det_ven,"+&
				"a.cod_deposito,"+&
				"a.cod_ubicazione,"+&
				"a.cod_lotto,"+&
				"a.data_stock,"+&
				"a.progr_stock,"+&
				"a.quan_consegnata,"+&
				"a.anno_registrazione_mov_mag,"+&
				"a.num_registrazione_mov_mag "+&
 "FROM  det_bol_ven as a "+&
" JOIN tes_bol_ven as b ON 	b.cod_azienda=a.cod_azienda and "+&
 									"b.anno_registrazione=a.anno_registrazione and "+&
									 "b.num_registrazione=a.num_registrazione "+&
"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and "+&
			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
			"a.cod_prodotto = '"+as_cod_prodotto+"' "

ls_sql2 = "SELECT	a.cod_tipo_det_ven,"+&
			"a.cod_deposito,"+&
			"a.cod_ubicazione,"+&
			"a.cod_lotto,"+&
			"a.data_stock,"+&
			"a.progr_stock,"+&
			"a.quan_fatturata,"+&
			"a.anno_registrazione_mov_mag,"+&
			"a.num_registrazione_mov_mag "+&
 "FROM  det_fat_ven as a "+&
 "JOIN	tes_fat_ven as b ON		b.cod_azienda=a.cod_azienda and "+&
 								"b.anno_registrazione=a.anno_registrazione and "+&
								"b.num_registrazione=a.num_registrazione "+&
"JOIN tab_tipi_fat_ven as t ON 	t.cod_azienda=a.cod_azienda and "+&
										"t.cod_tipo_fat_ven=b.cod_tipo_fat_ven "+&
"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and t.flag_tipo_fat_ven='I' and "+&
			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
			"a.cod_prodotto = '"+as_cod_prodotto+"' "

//leggo la quantità in spedizione prima del ricalcolo
select quan_in_spedizione
into :ld_quan_consegnata_old
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;
			
if isnull(ld_quan_consegnata_old) then ld_quan_consegnata_old = 0


//azzero la quantità in spedizione in anagrafica del prodotto
update anag_prodotti
set    quan_in_spedizione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in azzeramento quantita in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
	return -1
end if

//azzero la quantità in spedizione negli stock del prodotto
update stock
set    quan_in_spedizione = 0 
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto like :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in azzeramento quantità in spedizione negli stock: " + sqlca.sqlerrtext
	return -1
end if

ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot<0 then
	as_errore = "Errore in creazione datastore ddt: " + as_errore
	return -1
elseif ll_tot > 0 then
	for ll_index=1 to ll_tot
		
		ls_cod_tipo_det_ven					= lds_data.getitemstring(ll_index, 1)
		ls_cod_deposito						= lds_data.getitemstring(ll_index, 2)
		ls_cod_ubicazione						= lds_data.getitemstring(ll_index, 3)
		ls_cod_lotto								= lds_data.getitemstring(ll_index, 4)
		ldt_data_stock							= lds_data.getitemdatetime(ll_index, 5)
		ll_prog_stock							= lds_data.getitemnumber(ll_index, 6)
		ld_quan_consegnata					= lds_data.getitemdecimal(ll_index, 7)
		
		
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
		
			update anag_prodotti
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto ;
					 
			if sqlca.sqlcode = -1 then
				as_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
			
			update stock
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto and
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
			if sqlca.sqlcode = -1 then
				as_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
		end if
	next
end if

destroy lds_data

ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql2, as_errore)

if ll_tot<0 then
	as_errore = "Errore in creazione datastore fatture: " + as_errore
	return -1
elseif ll_tot > 0 then
	
	for ll_index=1 to ll_tot
		ls_cod_tipo_det_ven							= lds_data.getitemstring(ll_index, 1)
		ls_cod_deposito								= lds_data.getitemstring(ll_index, 2)
		ls_cod_ubicazione								= lds_data.getitemstring(ll_index, 3)
		ls_cod_lotto										= lds_data.getitemstring(ll_index, 4)
		ldt_data_stock									= lds_data.getitemdatetime(ll_index, 5)
		ll_prog_stock									= lds_data.getitemnumber(ll_index,6)
		ld_quan_consegnata							= lds_data.getitemdecimal(ll_index, 7)
		
		//ll_anno_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 9)
		//ll_num_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 10)
		
		////riga ddt già movimentata?
		//if not isnull(ll_anno_registrazione_mov_mag) or not isnull(ll_anno_registrazione_mov_mag) then continue
	
		
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
			destroy lds_data
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
			
			update anag_prodotti
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto;
					 
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
			
			update stock
			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto and
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
				destroy lds_data
				return -1
			end if
		end if
	next
end if

destroy lds_data


select quan_in_spedizione, cod_misura_mag
into :ld_quan_consegnata, :ls_cod_misura_mag
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;
			
if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0

ls_testo = "Qin_sped = " + string(ld_quan_consegnata, "###,###,##0.00") + " " + ls_cod_misura_mag			+ "  -> (era "+string(ld_quan_consegnata_old, "###,###,##0.00")+")" 

iuo_log.log(ls_testo)

return 0
end function

on uo_service_progressivi.create
call super::create
end on

on uo_service_progressivi.destroy
call super::destroy
end on

