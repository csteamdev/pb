﻿$PBExportHeader$w_agg_acq_prod.srw
forward
global type w_agg_acq_prod from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_agg_acq_prod
end type
type st_1 from statictext within w_agg_acq_prod
end type
type dw_agg_acq_prod from datawindow within w_agg_acq_prod
end type
end forward

global type w_agg_acq_prod from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1774
integer height = 424
string title = "Aggiornamento Prezzo Acquisto"
boolean maxbox = false
boolean resizable = false
cb_1 cb_1
st_1 st_1
dw_agg_acq_prod dw_agg_acq_prod
end type
global w_agg_acq_prod w_agg_acq_prod

on w_agg_acq_prod.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.st_1=create st_1
this.dw_agg_acq_prod=create dw_agg_acq_prod
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_agg_acq_prod
end on

on w_agg_acq_prod.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.dw_agg_acq_prod)
end on

event pc_setwindow;call super::pc_setwindow;dw_agg_acq_prod.settransobject(sqlca)
end event

type cb_1 from commandbutton within w_agg_acq_prod
integer x = 23
integer y = 220
integer width = 1714
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "AVVIA AGGIORNAMENTO PREZZO DI ACQUISTO"
end type

event clicked;long   ll_i

string ls_cod_prodotto

dec{4} ld_costo_ultimo, ld_prezzo


if g_mb.messagebox("APICE","Aggiornare il prezzo di acquisto al costo ultimo per tutti i prodotti?",question!,yesno!,2) = 2 then
	return -1
end if

setpointer(hourglass!)

st_1.show()

if dw_agg_acq_prod.retrieve(s_cs_xx.cod_azienda) < 0 then
	g_mb.messagebox("APICE","Errore in caricamento anagrafica prodotti",stopsign!)
	setpointer(arrow!)
	st_1.hide()
	return -1
end if

st_1.hide()

dw_agg_acq_prod.height = 180

for ll_i = 1 to dw_agg_acq_prod.rowcount()
	
	dw_agg_acq_prod.setrow(ll_i)
	
	dw_agg_acq_prod.scrolltorow(ll_i)
	
	ld_prezzo = dw_agg_acq_prod.getitemnumber(ll_i,"prezzo_acquisto")
	
	ld_costo_ultimo = dw_agg_acq_prod.getitemnumber(ll_i,"costo_ultimo")
	
	ls_cod_prodotto = dw_agg_acq_prod.getitemstring(ll_i,"cod_prodotto")	
	
	if ld_costo_ultimo = 0 or isnull(ld_costo_ultimo) or ld_costo_ultimo = ld_prezzo then
		continue
	end if
	
	update anag_prodotti
	set    prezzo_acquisto = costo_ultimo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento prodotto " + ls_cod_prodotto + ": " + sqlca.sqlerrtext,stopsign!)
		rollback;
		dw_agg_acq_prod.height = 80
		setpointer(arrow!)
		return -1
	end if
	
next

commit;

dw_agg_acq_prod.height = 80

g_mb.messagebox("APICE","Aggiornamento completato",information!)

dw_agg_acq_prod.reset()

setpointer(arrow!)
end event

type st_1 from statictext within w_agg_acq_prod
boolean visible = false
integer x = 23
integer y = 120
integer width = 1714
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 15780518
string text = "Lettura elenco prodotti in corso..."
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_agg_acq_prod from datawindow within w_agg_acq_prod
integer x = 23
integer y = 20
integer width = 1714
integer height = 80
integer taborder = 20
string dataobject = "d_agg_acq_prod"
boolean border = false
boolean livescroll = true
end type

