﻿$PBExportHeader$w_anag_prodotti_breve.srw
$PBExportComments$Window anagrafica prodotti breve
forward
global type w_anag_prodotti_breve from w_cs_xx_principale
end type
type dw_anag_prodotti_breve_lista from uo_cs_xx_dw within w_anag_prodotti_breve
end type
type dw_anag_prodotti_breve_dettaglio from uo_cs_xx_dw within w_anag_prodotti_breve
end type
end forward

global type w_anag_prodotti_breve from w_cs_xx_principale
int Width=2263
int Height=1117
boolean TitleBar=true
string Title="Gestione Prodotti"
dw_anag_prodotti_breve_lista dw_anag_prodotti_breve_lista
dw_anag_prodotti_breve_dettaglio dw_anag_prodotti_breve_dettaglio
end type
global w_anag_prodotti_breve w_anag_prodotti_breve

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_anag_prodotti_breve_dettaglio, &
                 "cod_misura_mag", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_anag_prodotti_breve_lista.set_dw_key("cod_azienda")
dw_anag_prodotti_breve_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_anag_prodotti_breve_dettaglio.set_dw_options(sqlca,dw_anag_prodotti_breve_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_anag_prodotti_breve_lista
end on

on w_anag_prodotti_breve.create
int iCurrent
call w_cs_xx_principale::create
this.dw_anag_prodotti_breve_lista=create dw_anag_prodotti_breve_lista
this.dw_anag_prodotti_breve_dettaglio=create dw_anag_prodotti_breve_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_anag_prodotti_breve_lista
this.Control[iCurrent+2]=dw_anag_prodotti_breve_dettaglio
end on

on w_anag_prodotti_breve.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_anag_prodotti_breve_lista)
destroy(this.dw_anag_prodotti_breve_dettaglio)
end on

event pc_new;call super::pc_new;dw_anag_prodotti_breve_dettaglio.setitem(dw_anag_prodotti_breve_dettaglio.getrow(), "data_creazione", datetime(today()))

end event

type dw_anag_prodotti_breve_lista from uo_cs_xx_dw within w_anag_prodotti_breve
int X=19
int Y=17
int Width=2186
int Height=529
int TabOrder=10
string DataObject="d_anag_prodotti_breve_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;if i_extendmode then
	
   LONG  l_Idx

	FOR l_Idx = 1 TO RowCount()
   	IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      	SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	   END IF
	NEXT

end if

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_anag_prodotti_breve_dettaglio from uo_cs_xx_dw within w_anag_prodotti_breve
int X=19
int Y=561
int Width=2186
int Height=433
int TabOrder=20
string DataObject="d_anag_prodotti_breve_dettaglio"
BorderStyle BorderStyle=StyleRaised!
end type

