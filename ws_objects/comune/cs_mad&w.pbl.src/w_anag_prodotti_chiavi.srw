﻿$PBExportHeader$w_anag_prodotti_chiavi.srw
forward
global type w_anag_prodotti_chiavi from w_cs_xx_principale
end type
type dw_anag_prodotti from uo_cs_xx_dw within w_anag_prodotti_chiavi
end type
type st_1 from statictext within w_anag_prodotti_chiavi
end type
type sle_codice_prodotto from singlelineedit within w_anag_prodotti_chiavi
end type
type sle_descrizione from singlelineedit within w_anag_prodotti_chiavi
end type
type cb_cerca from commandbutton within w_anag_prodotti_chiavi
end type
end forward

global type w_anag_prodotti_chiavi from w_cs_xx_principale
int Width=1889
int Height=1353
boolean TitleBar=true
string Title="Gestione Prodotti Chiave"
dw_anag_prodotti dw_anag_prodotti
st_1 st_1
sle_codice_prodotto sle_codice_prodotto
sle_descrizione sle_descrizione
cb_cerca cb_cerca
end type
global w_anag_prodotti_chiavi w_anag_prodotti_chiavi

type variables
boolean ib_nuovo
end variables

on w_anag_prodotti_chiavi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_anag_prodotti=create dw_anag_prodotti
this.st_1=create st_1
this.sle_codice_prodotto=create sle_codice_prodotto
this.sle_descrizione=create sle_descrizione
this.cb_cerca=create cb_cerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_anag_prodotti
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=sle_codice_prodotto
this.Control[iCurrent+4]=sle_descrizione
this.Control[iCurrent+5]=cb_cerca
end on

on w_anag_prodotti_chiavi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_anag_prodotti)
destroy(this.st_1)
destroy(this.sle_codice_prodotto)
destroy(this.sle_descrizione)
destroy(this.cb_cerca)
end on

event pc_setwindow;call super::pc_setwindow;dw_anag_prodotti.set_dw_key("cod_azienda")
dw_anag_prodotti.set_dw_key("cod_prodotto")
dw_anag_prodotti.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)

end event

event open;call super::open;ib_nuovo=false
end event

event activate;call super::activate;dw_anag_prodotti.setfocus()
end event

type dw_anag_prodotti from uo_cs_xx_dw within w_anag_prodotti_chiavi
int X=1
int Y=241
int Width=1742
int Height=1001
int TabOrder=30
string DataObject="d_anag_prodotti_chiavi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_chiave
string ls_cod_prodotto,ls_descrizione,ls_des_chiave

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ls_descrizione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "des_prodotto")

sle_codice_prodotto.text=ls_cod_prodotto
sle_descrizione.text=ls_descrizione

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

//messagebox("Controllo",ls_cod_chiave+" "+ls_des_chiave)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_prodotto,ls_cod_chiave,ls_des_chiave

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

//select tab_chiavi.cod_chiave,des_chiave into :ls_cod_chiave,:ls_des_chiave
//from tab_chiavi,anag_prodotti_chiavi where(anag_prodotti_chiavi.cod_chiave=tab_chiavi.cod_chiave);
//

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end event

event updatestart;call super::updatestart;//if i_extendmode then
//   integer li_i
//   string ls_cod_prodotto, ls_cod_chiave
//
//   for li_i = 1 to this.deletedcount()
//		
//      ls_cod_prodotto = this.getitemstring(li_i, "cod_prodotto", delete!, true)
//      ls_cod_chiave = this.getitemstring(li_i, "cod_chiave", delete!, true)
//
//      delete from anag_prodotti_chiavi 
//       where anag_prodotti_chiavi.cod_azienda = :s_cs_xx.cod_azienda and 
//             anag_prodotti_chiavi.cod_prodotto = :ls_cod_prodotto and 
//             anag_prodotti_chiavi.cod_chiave = :ls_cod_chiave;
//
//      if sqlca.sqlcode <> 0 then
//         messagebox("Attenzione", "Si è verificato un errore in fase di cancellazione.", &
//                    exclamation!, ok!)
//         return
//      end if
//   next
//end if
end event

event pcd_new;call super::pcd_new;cb_cerca.enabled=true

ib_nuovo=true

triggerevent("ue_key")




end event

event pcd_save;call super::pcd_save;ib_nuovo=false

cb_cerca.enabled=false
end event

event losefocus;call super::losefocus;//string ls_cod_chiave
//integer li_presente
//
//if i_extendmode then
//	
// if ib_nuovo then
//
//  ls_cod_chiave=dw_anag_prodotti.getitemstring(dw_anag_prodotti.getrow(),"cod_chiave")
//
//  if isnull(ls_cod_chiave) then
//	//messagebox("Attenzione","Inserire il codice chiave",StopSign!)
//	cb_cerca.setfocus()
//  else
//	select count(*) into :li_presente from tab_chiavi where cod_chiave=:ls_cod_chiave;
//	if li_presente>0 then
//		triggerevent("pcd_save")
//		triggerevent("pcd_new")
//	else
//		//messagebox("Attenzione","La chiave: "+ls_cod_chiave+" non esiste")
//		cb_cerca.setfocus()
//	end if
//  end if
// end if
//end if
//




end event

event ue_key;call super::ue_key;string ls_cod_chiave,ls_cod_prodotto
integer li_presente,li_chiave

 dw_anag_prodotti.setfocus()
 ls_cod_chiave=dw_anag_prodotti.getitemstring(dw_anag_prodotti.rowcount(),"cod_chiave")
 ls_cod_prodotto=sle_codice_prodotto.text
 if key = keyenter! then
   select count(*) into :li_chiave from tab_chiavi where cod_azienda=:s_cs_xx.cod_azienda and cod_chiave=:ls_cod_chiave;
	  if li_chiave>0 then
		 select count(*) into :li_presente from anag_prodotti_chiavi where cod_chiave=:ls_cod_chiave and cod_prodotto=:ls_cod_prodotto;
	     if li_presente=0 then
		   triggerevent("pcd_save")
		   triggerevent("pcd_new")
		  end if
	 end if
  end if

end event

event pcd_active;call super::pcd_active;dw_anag_prodotti.setfocus()
end event

event editchanged;call super::editchanged;dw_anag_prodotti.setfocus()
end event

event itemchanged;call super::itemchanged;dw_anag_prodotti.setfocus()
end event

type st_1 from statictext within w_anag_prodotti_chiavi
int X=23
int Y=49
int Width=439
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Prodotto:"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_codice_prodotto from singlelineedit within w_anag_prodotti_chiavi
int X=261
int Y=45
int Width=668
int Height=77
int TabOrder=40
boolean BringToTop=true
boolean Border=false
boolean AutoHScroll=false
boolean DisplayOnly=true
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event getfocus; dw_anag_prodotti.setfocus()
end event

type sle_descrizione from singlelineedit within w_anag_prodotti_chiavi
int X=266
int Y=121
int Width=1482
int Height=77
int TabOrder=10
boolean BringToTop=true
boolean Border=false
boolean AutoHScroll=false
boolean DisplayOnly=true
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_cerca from commandbutton within w_anag_prodotti_chiavi
int X=1591
int Y=333
int Width=83
int Height=73
int TabOrder=20
boolean Enabled=false
boolean BringToTop=true
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;

window_open_parm(w_ricerca_chiavi, -1, dw_anag_prodotti)

end event

event getfocus;s_cs_xx.parametri.parametro_uo_dw_1 = dw_anag_prodotti
s_cs_xx.parametri.parametro_s_1 = "cod_chiave"
end event

