﻿$PBExportHeader$w_certificato_qualita.srw
$PBExportComments$Window Certificato di Qualità
forward
global type w_certificato_qualita from w_cs_xx_principale
end type
type dw_certificato_qualita from uo_cs_xx_dw within w_certificato_qualita
end type
end forward

global type w_certificato_qualita from w_cs_xx_principale
integer width = 3835
integer height = 2096
string title = "Report Certificato di Qualità"
boolean vscrollbar = true
dw_certificato_qualita dw_certificato_qualita
end type
global w_certificato_qualita w_certificato_qualita

forward prototypes
public function integer wf_stampa_accettazioni (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock)
public function integer wf_stampa_collaudi (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock)
public function integer wf_stampa_nc (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock)
public function integer wf_report ()
end prototypes

public function integer wf_stampa_accettazioni (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock);integer li_anno_acc_mat
long    ll_num_acc_mat,ll_j
double  ldd_quan_arrivata
datetime    ldt_data_prev_consegna,ldt_data_eff_consegna
string  ls_rif_ord_acq,ls_rif_bolla_acq,ls_rif_fat_acq
boolean lb_flag_descrizione


declare righe_accettazioni cursor for
select anno_acc_materiali,
		 num_acc_materiali,
		 quan_arrivata,
		 rif_ord_acq,
		 rif_bol_acq,
		 rif_fat_acq,
		 data_prev_consegna,
		 data_eff_consegna
from   acc_materiali
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:fs_cod_prodotto
and    cod_deposito=:fs_cod_deposito
and    cod_ubicazione=:fs_cod_ubicazione
and    cod_lotto=:fs_cod_lotto
and    data_stock=:fdt_data_stock
and    prog_stock=:fl_prog_stock;

open righe_accettazioni;

do while 1=1
	fetch righe_accettazioni
	into	:li_anno_acc_mat,
			:ll_num_acc_mat,
			:ldd_quan_arrivata,
			:ls_rif_ord_acq,
			:ls_rif_bolla_acq,
			:ls_rif_fat_acq,
			:ldt_data_prev_consegna,
			:ldt_data_eff_consegna;
			
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	lb_flag_descrizione = false
	
	if sqlca.sqlcode = 100 then exit
	
	if lb_flag_descrizione = false then
		ll_j = dw_certificato_qualita.insertrow(0)
		dw_certificato_qualita.setitem(ll_j,"riga_titolo","Accettazione Materiali")
		lb_flag_descrizione = true
		ll_j = dw_certificato_qualita.insertrow(0)
	end if
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Anno/Numero: " + string(li_anno_acc_mat) + " / " + string(ll_num_acc_mat))

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Quantità arrivata: " + string(ldd_quan_arrivata))

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Rif. Ordine Acquisto: " + ls_rif_ord_acq)

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Rif. Bolla Acquisto: " + ls_rif_bolla_acq)

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Data consegna prevista: " + string(date(ldt_data_prev_consegna)) + &
	" Data consegna effettiva: " + string(date(ldt_data_eff_consegna)))
		
loop

close righe_accettazioni;

return 0
end function

public function integer wf_stampa_collaudi (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock);long     ll_num_registrazione,ll_j
integer  li_anno_registrazione
string   ls_cod_test,ls_cod_operaio,ls_cod_misura,ls_cod_errore,ls_flag_esito,ls_des_test,ls_cognome,ls_nome,ls_des_difformita
boolean  lb_flag_descrizione
datetime ldt_data_registrazione
double   ldd_risultato

lb_flag_descrizione=false

declare righe_collaudi cursor for
select  anno_registrazione,
		  num_registrazione,
		  data_registrazione,
		  cod_test,
		  cod_operaio,
		  risultato,
		  cod_misura,
		  cod_errore,
		  flag_esito
from    collaudi
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_prodotto=:fs_cod_prodotto
and     cod_deposito=:fs_cod_deposito
and     cod_ubicazione=:fs_cod_ubicazione
and     cod_lotto=:fs_cod_lotto
and     data_stock=:fdt_data_stock
and     prog_stock=:fl_prog_stock;


open righe_collaudi;

do while 1=1
	fetch righe_collaudi
	into	:li_anno_registrazione,
			:ll_num_registrazione,
			:ldt_data_registrazione,
			:ls_cod_test,
			:ls_cod_operaio,
			:ldd_risultato,
			:ls_cod_misura,
			:ls_cod_errore,
			:ls_flag_esito;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	lb_flag_descrizione = false
	
	if sqlca.sqlcode = 100 then exit
	
	if lb_flag_descrizione = false then
		ll_j = dw_certificato_qualita.insertrow(0)
		dw_certificato_qualita.setitem(ll_j,"riga_titolo","Collaudi")
		lb_flag_descrizione = true
	end if

	select des_test
	into   :ls_des_test
	from   tab_test
	where  cod_test=:ls_cod_test;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	select cognome,
			 nome
	into   :ls_cognome,
			 :ls_nome
	from   anag_operai
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_operaio=:ls_cod_operaio;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	select des_difformita
	into   :ls_des_difformita
	from   tab_difformita
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_errore=:ls_cod_errore;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	ll_j = dw_certificato_qualita.insertrow(0)
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Anno Collaudo: " + string(li_anno_registrazione) + "  Numero: " + &
	string(ll_num_registrazione) + " Data: " + string(date(ldt_data_registrazione)))
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Test: " + ls_cod_test + "  " + ls_des_test)
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Operatore: " + ls_nome + " " + ls_cognome)

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Risultato: " + string(ldd_risultato) + "  U.M.: " + ls_cod_misura)
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Errore: " + ls_cod_errore + " " + ls_des_difformita + "  Esito: " + ls_flag_esito)
	
loop

close righe_collaudi;

return 0
end function

public function integer wf_stampa_nc (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_lotto, string fs_cod_ubicazione, datetime fdt_data_stock, long fl_prog_stock);long     ll_num_non_conf,ll_j
integer  li_anno_non_conf
string   ls_cod_trattamento,ls_des_trattamento
boolean  lb_flag_descrizione
datetime ldt_data_creazione,ldt_data_scadenza
double   ldd_risultato

lb_flag_descrizione=false

declare righe_nc cursor for
select  anno_non_conf,
		  num_non_conf,
		  data_creazione,
		  data_scadenza,
		  cod_trattamento
from    non_conformita
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_prodotto=:fs_cod_prodotto
and     cod_deposito=:fs_cod_deposito
and     cod_ubicazione=:fs_cod_ubicazione
and     cod_lotto=:fs_cod_lotto
and     data_stock=:fdt_data_stock
and     prog_stock=:fl_prog_stock;

open righe_nc;

do while 1=1
	fetch righe_nc
	into	:li_anno_non_conf,
			:ll_num_non_conf,
			:ldt_data_creazione,
			:ldt_data_scadenza,
			:ls_cod_trattamento;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	lb_flag_descrizione = false
	
	if sqlca.sqlcode = 100 then exit
	
	if lb_flag_descrizione = false then
		ll_j = dw_certificato_qualita.insertrow(0)
		dw_certificato_qualita.setitem(ll_j,"riga_titolo","Non Conformità")
		ll_j = dw_certificato_qualita.insertrow(0)
		lb_flag_descrizione = true
	end if

	select des_trattamento
	into   :ls_des_trattamento
	from   tab_trattamenti
	where  cod_trattamento=:ls_cod_trattamento;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Anno NC: " + string(li_anno_non_conf) + "  Numero: " + string(ll_num_non_conf))

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Data Creazione: " + string(date(ldt_data_creazione)) + " Data Scadenza: " + string(date(ldt_data_scadenza)))
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","-   Trattamento: " + ls_cod_trattamento + "  " + ls_des_trattamento)
	
	
loop

close righe_nc;

return 0
end function

public function integer wf_report ();long   ll_t,ll_prog_stock,ll_j,ll_num_acc_mat,ll_num_registrazione,ll_i,ll_num_righe
string ls_cod_prodotto,ls_cod_ubicazione,ls_cod_deposito,ls_cod_lotto,ls_rif_ord_acq,ls_rif_bolla_acq,ls_rif_fat_acq, & 
		 ls_cod_test,ls_cod_operaio,ls_cod_misura,ls_cod_errore,ls_flag_esito,ls_test,ls_des_mp,ls_errore,ls_des_prodotto,& 
		 ls_des_deposito,ls_rag_soc,ls_des_test,ls_cognome,ls_nome,ls_des_difformita
datetime ldt_data_stock,ldt_data_prev_consegna,ldt_data_eff_consegna,ldt_data_registrazione
integer li_anno_acc_mat,li_anno_registrazione,li_risposta
double ldd_quan_arrivata,ldd_risultato
boolean lb_flag_descrizione
s_stock sl_stock[]

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
ls_cod_deposito = s_cs_xx.parametri.parametro_s_2
ls_cod_ubicazione = s_cs_xx.parametri.parametro_s_3
ls_cod_lotto = s_cs_xx.parametri.parametro_s_4
ll_prog_stock = s_cs_xx.parametri.parametro_ul_2
ldt_data_stock = s_cs_xx.parametri.parametro_data_1

dw_certificato_qualita.reset()

select rag_soc_1
into   :ls_rag_soc
from   aziende
where  cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if


select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto =:ls_cod_prodotto;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if

select des_deposito
into   :ls_des_deposito
from   anag_depositi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_deposito=:ls_cod_deposito;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if

ls_des_prodotto = ls_cod_prodotto + "  " + ls_des_prodotto
ls_des_deposito = ls_cod_deposito + "  " + ls_des_deposito

dw_certificato_qualita.Modify("nome_azienda.text='" + ls_rag_soc + "'")
dw_certificato_qualita.Modify("cod_prodotto.text='" + ls_des_prodotto + "'")
dw_certificato_qualita.Modify("cod_deposito.text='"+ ls_des_deposito + "'")
dw_certificato_qualita.Modify("cod_ubicazione.text='"+ ls_cod_ubicazione + "'")
dw_certificato_qualita.Modify("cod_lotto.text='"+ ls_cod_lotto + "'")
dw_certificato_qualita.Modify("data_stock.text='"+ string(date(ldt_data_stock)) + "'")
dw_certificato_qualita.Modify("prog_stock.text='"+ string(ll_prog_stock) + "'")

ll_j = 1
	
// ***** trova accettazioni

li_risposta = wf_stampa_accettazioni(ls_cod_prodotto,ls_cod_deposito,ls_cod_lotto,ls_cod_ubicazione, & 
												 ldt_data_stock,ll_prog_stock)

// ***** fine trova accettazioni

if dw_certificato_qualita.rowcount() > 0 then dw_certificato_qualita.insertrow(0)

// **** trova Collaudi
li_risposta = wf_stampa_collaudi(ls_cod_prodotto,ls_cod_deposito,ls_cod_lotto,ls_cod_ubicazione, & 
												 ldt_data_stock,ll_prog_stock)


// **** Fine trova collaudi

if dw_certificato_qualita.rowcount() > 0 then dw_certificato_qualita.insertrow(0)


//***** Trova NC
li_risposta = wf_stampa_nc(ls_cod_prodotto,ls_cod_deposito,ls_cod_lotto,ls_cod_ubicazione, & 
												 ldt_data_stock,ll_prog_stock)

//***** Fine trova NC

if dw_certificato_qualita.rowcount() > 0 then dw_certificato_qualita.insertrow(0)


// ***** trova materie prime
//verifica se il prodotto è un PF

select cod_azienda
into   :ls_test
from   distinta_padri
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

// manca volutamente il controllo d'errore sqlca.sqlcode < 0


if isnull(ls_test) or ls_test ="" then return 0

li_risposta = f_trova_mp_pf (ls_cod_prodotto, &
									  ls_cod_deposito, &
									  ls_cod_ubicazione, & 
									  ls_cod_lotto, &
									  ldt_data_stock, & 
									  ll_prog_stock, &
									  sl_stock[], &
									  ls_errore )
									  
									  

if li_risposta < 0 then
	g_mb.messagebox("Omnia",ls_errore,information!)
end if

ll_j = dw_certificato_qualita.insertrow(0)
dw_certificato_qualita.setitem(ll_j,"riga_titolo","Materie Prime")
ll_j = dw_certificato_qualita.insertrow(0)

for ll_i = 1 to upperbound(sl_stock[]) -1
	
	select des_prodotto
	into   :ls_des_mp
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:sl_stock[ll_i].cod_prodotto;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia","Errore sul DB: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
		
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","   -Codice: " + sl_stock[ll_i].cod_prodotto + "   Descrizione: " +ls_des_mp)
			
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","       -Deposito: " + sl_stock[ll_i].cod_deposito + " Ubicazione: " + sl_stock[ll_i].cod_ubicazione)

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","       -Lotto: " + sl_stock[ll_i].cod_lotto + " Data Stock: " + string(date(sl_stock[ll_i].data_stock)))
	
	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","       -Prog. Stock: " + string(sl_stock[ll_i].prog_stock))

	ll_j = dw_certificato_qualita.insertrow(0)
	dw_certificato_qualita.setitem(ll_j,"riga","       -Quantità Utilizzata: " + string(sl_stock[ll_i].quan_movimento))

	ll_j = dw_certificato_qualita.insertrow(0)

	dw_certificato_qualita.setitem(ll_j,"riga_titolo","Accettazione")
	ll_j = dw_certificato_qualita.insertrow(0)
	li_risposta = wf_stampa_accettazioni(sl_stock[ll_i].cod_prodotto,sl_stock[ll_i].cod_deposito,sl_stock[ll_i].cod_lotto,sl_stock[ll_i].cod_ubicazione, & 
												    sl_stock[ll_i].data_stock,sl_stock[ll_i].prog_stock)
	
	ll_j = dw_certificato_qualita.insertrow(0)

	dw_certificato_qualita.setitem(ll_j,"riga_titolo","Collaudi")
	ll_j = dw_certificato_qualita.insertrow(0)
	li_risposta = wf_stampa_collaudi(sl_stock[ll_i].cod_prodotto,sl_stock[ll_i].cod_deposito,sl_stock[ll_i].cod_lotto,sl_stock[ll_i].cod_ubicazione, & 
												sl_stock[ll_i].data_stock,sl_stock[ll_i].prog_stock)
	
	ll_j = dw_certificato_qualita.insertrow(0)
	
	dw_certificato_qualita.setitem(ll_j,"riga_titolo","Collaudi")
	
	ll_j = dw_certificato_qualita.insertrow(0)
	li_risposta = wf_stampa_nc(sl_stock[ll_i].cod_prodotto,sl_stock[ll_i].cod_deposito,sl_stock[ll_i].cod_lotto,sl_stock[ll_i].cod_ubicazione, & 
												sl_stock[ll_i].data_stock,sl_stock[ll_i].prog_stock)
	
	ll_j = dw_certificato_qualita.insertrow(0)
next

// fine funzione di ricerca mp
return 0
end function

on w_certificato_qualita.create
int iCurrent
call super::create
this.dw_certificato_qualita=create dw_certificato_qualita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_certificato_qualita
end on

on w_certificato_qualita.destroy
call super::destroy
destroy(this.dw_certificato_qualita)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin)
dw_certificato_qualita.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )

save_on_close(c_socnosave)

wf_report()
end event

type dw_certificato_qualita from uo_cs_xx_dw within w_certificato_qualita
integer x = 23
integer y = 20
integer width = 3680
integer height = 4840
string dataobject = "d_certificato_qualita"
end type

