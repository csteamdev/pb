﻿$PBExportHeader$w_chiusure_magazzino.srw
$PBExportComments$Chiusura Periodica ed Annuale del Magazzino
forward
global type w_chiusure_magazzino from w_cs_xx_principale
end type
type st_file from statictext within w_chiusure_magazzino
end type
type dw_chiusure from datawindow within w_chiusure_magazzino
end type
type sle_1 from singlelineedit within w_chiusure_magazzino
end type
end forward

global type w_chiusure_magazzino from w_cs_xx_principale
integer x = 832
integer y = 360
integer width = 2789
integer height = 1876
string title = "Chiusura Periodica e Annuale"
st_file st_file
dw_chiusure dw_chiusure
sle_1 sle_1
end type
global w_chiusure_magazzino w_chiusure_magazzino

type variables
uo_log iuo_log
end variables

forward prototypes
public function integer wf_single_mov_mag_per (string fs_cod_tipo_movimento_det, decimal fd_quan_movimento, decimal fd_val_movimento, ref decimal fd_quant_val[], ref string fs_error, string fs_cod_tipo_movimento)
public function integer wf_chiusura_periodica_mag (datetime fdt_data_chiper, ref string fs_error)
public function integer wf_mov_mag (datetime fdt_data_movimento, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, string fs_cod_tipo_movimento, decimal fd_quan_movimento, decimal fd_val_unit_movimento, ref long fl_anno_registrazione[], ref long fl_num_registrazione[], string fs_messaggio)
public function integer wf_chiusura_annuale_mag (datetime fdt_data_chius, ref string fs_error)
end prototypes

public function integer wf_single_mov_mag_per (string fs_cod_tipo_movimento_det, decimal fd_quan_movimento, decimal fd_val_movimento, ref decimal fd_quant_val[], ref string fs_error, string fs_cod_tipo_movimento);// wf_single_mov_mag_per ( string fs_cod_tipo_movimento_det, double fd_quan_movimento, 
// 	double fd_val_movimento, by reference double fd_quant_val[], 
// by reference string fs_error, string fs_cod_tipo_movimento)
// usata nella chiusura periodica
// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// aggiorna le quantità e le rispettive valorizzazioni del vettore ld_quant_val in base 
// al tipo di movimento 



string ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita,   &
       ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
       ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_flag_cliente, &
		 ls_flag_saldo_quan_inizio_anno, ls_flag_val_inizio_anno, ls_flag_saldo_quan_ultima_chius, &
		 ls_flag_fornitore 
		 
select flag_fornitore, flag_cliente
  into :ls_flag_fornitore, :ls_flag_cliente  
  from det_tipi_movimenti
 where (det_tipi_movimenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (det_tipi_movimenti.cod_tipo_movimento = :fs_cod_tipo_movimento ) AND  
       (det_tipi_movimenti.cod_tipo_mov_det = :fs_cod_tipo_movimento_det )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca su DET_TIPI_MOVIMENTI: verificare!",StopSign!)
	return -1
end if
// ------ se è attiva l'indicazione flag_fornitore non aggiorno il mio magazzino --------
// -----------  perchè il movimento riguarda il magazzino del mio terzista   ------------
if ls_flag_fornitore = "S" then return 0
// if ls_flag_cliente = "S" then return 0

SELECT flag_saldo_quan_ultima_chius,   
		 flag_prog_quan_entrata, flag_prog_quan_uscita
 INTO  :ls_flag_saldo_quan_ultima_chius,   
		 :ls_flag_prog_quan_entrata,   
		 :ls_flag_prog_quan_uscita   
 FROM  tab_tipi_movimenti_det  
WHERE  ( tab_tipi_movimenti_det.cod_azienda = :s_cs_xx.cod_azienda      ) AND  
		 ( tab_tipi_movimenti_det.cod_tipo_mov_det = :fs_cod_tipo_movimento_det)   ;
if sqlca.sqlcode <> 0 then
	fs_error="Errore durante caricamento configurazione movimenti"
	return -1
end if


if ls_flag_saldo_quan_ultima_chius="+" then
	fd_quant_val[3] = fd_quant_val[3] + fd_quan_movimento
elseif ls_flag_saldo_quan_ultima_chius="-" then
	fd_quant_val[3] = fd_quant_val[3] - fd_quan_movimento
end if

if ls_flag_prog_quan_entrata="+" then
	fd_quant_val[4] = fd_quant_val[4] + fd_quan_movimento
elseif ls_flag_prog_quan_entrata="-" then
	fd_quant_val[4] = fd_quant_val[4] - fd_quan_movimento
end if

if ls_flag_prog_quan_uscita="+" then
	fd_quant_val[6] = fd_quant_val[6] + fd_quan_movimento
elseif ls_flag_prog_quan_uscita="-" then
	fd_quant_val[6] = fd_quant_val[6] - fd_quan_movimento
end if


return 0
end function

public function integer wf_chiusura_periodica_mag (datetime fdt_data_chiper, ref string fs_error);// wf_chiusura_periodica_mag( datetime fdt_data_chiper, by reference string fs_error)
// ritorna 0 se successo, -1 se errore
// non devono esserci bolle o fatture aperte
// calcola solo il saldo quantità ultima chiusura e pone a 'S' il flag storico in mov_magazzino
// Manca il calcolo dei consumi (FORECAST)
// Eseguire il LOCK sulla tabella anag_prodotti	DA FARE

string ls_cod_prodotto, ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_prod_in_chius[], ls_filtro_prodotto, ls_file
integer li_ret_funz, li_file
long ll_num_righe, ll_num_prod, ll_cont, ll_i
dec{4} ld_quant_val[], ld_saldo_quan_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_prog_quan_entrata, ld_prog_quan_uscita, &
       ld_quan_movimento, ld_val_movimento
// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
datetime ldt_data_registrazione, ldt_data_chiusura_periodica, ldt_oggi
//datastore lds_mov_magazzino

sle_1.text = "Preparazione...attendere prego." 

ls_file = "chiusura_periodica.log"
ls_filtro_prodotto = dw_chiusure.getitemstring(1,"cod_prodotto")
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

select data_chiusura_periodica
into :ldt_data_chiusura_periodica
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda
using sqlca;

if isnull(ldt_data_chiusura_periodica) then ldt_data_chiusura_periodica = datetime(date("01/01/1900"),00:00:00)
if ldt_data_chiusura_periodica > fdt_data_chiper then
	fs_error = "Operazione di chiusura già eseguita fino alla data " + string(ldt_data_chiusura_periodica)
	return -1
end if

// controllo se esistono bolle o fatture non confermate: se non confermate, esco con errore
sle_1.text = "Verifica Bolle Entrata." 
ll_num_righe = 0

select count(*)
into   :ll_num_righe
from   tes_bol_acq
where  flag_movimenti = 'N' and 
       cod_azienda = :s_cs_xx.cod_azienda	and 
		 data_registrazione < :fdt_data_chiper	and 
		 ( (data_bolla is null) or (data_bolla <:fdt_data_chiper)) ;
		 
if ll_num_righe > 0 then 	
	fs_error = "Esistono bolle di acquisto non confermate alla data chiusura"
	return -1	
end if

sle_1.text = "Verifica Bolle Uscita." 
select count(*)
into   :ll_num_righe
from   tes_bol_ven
where  flag_movimenti = 'N' and 
       cod_azienda = :s_cs_xx.cod_azienda	and 
		 data_registrazione <= :fdt_data_chiper and 
		 ( (data_bolla is null) or (data_bolla <= :fdt_data_chiper)) ;
		 
if ll_num_righe > 0 then 
	fs_error="Esistono bolle di vendita non confermate alla data chiusura"
	return -1
end if

sle_1.text = "Verifica Fatture Entrata." 
select count(*)
into   :ll_num_righe
from   tes_fat_acq 
where  cod_azienda = :s_cs_xx.cod_azienda	and 
       flag_fat_confermata = 'N' and 
		 data_registrazione < :fdt_data_chiper;
		 
if ll_num_righe > 0 then 
	fs_error = "Esistono fatture di acquisto non confermate alla data chiusura"
	return -1
end if

sle_1.text = "Verifica Fatture Vendita." 
select count(*)
into   :ll_num_righe
from   tes_fat_ven 
where  cod_azienda = :s_cs_xx.cod_azienda	and 
       flag_movimenti = 'N' and 
		 data_registrazione < :fdt_data_chiper;
		 
if ll_num_righe > 0 then 
	fs_error="Esistono fatture di vendita non confermate alla data chiusura"
	return -1
end if


DECLARE cur_prod CURSOR FOR 
select distinct cod_prodotto
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 cod_prodotto like :ls_filtro_prodotto and
		 data_registrazione <= :fdt_data_chiper and
		 data_registrazione > :ldt_data_chiusura_periodica and
       flag_storico = 'N'
order by cod_prodotto;

OPEN cur_prod;

if sqlca.sqlcode = -1 then
	fs_error="Errore in OPEN cursore cur_prod.~r~n"+sqlca.sqlerrtext
	return -1
end if

ll_i = 1

DO while 0 = 0
	
	FETCH cur_prod INTO :ls_cod_prodotto;
	
	if sqlca.sqlcode = 100 then exit 
	
	if sqlca.sqlcode < 0 then
		fs_error="Errore in lettura elenco prodotti di magazzino~r~n" + SQLCA.SQLErrText
		close cur_prod;
		rollback;
		return -1
	end if
	
	sle_1.text = string(ll_i) + "  " + ls_cod_prodotto
	yield()
	
	if not isnull(ls_cod_prodotto) then 
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		ll_i = ll_i + 1
	end if
	
LOOP

CLOSE cur_prod ;

ldt_oggi = datetime(today())

for ll_num_righe = 1 to  ll_i - 1
	
	ls_cod_prodotto = ls_prod_in_chius[ll_num_righe]
	
	sle_1.text = "Prodotto: " + ls_cod_prodotto
	yield()
	
	update mov_magazzino
	set    flag_storico = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_prodotto = :ls_cod_prodotto	and 
			 data_registrazione <= :fdt_data_chiper and 
			 flag_storico = 'N';
			 
	if sqlca.sqlcode < 0 then
		fs_error="Errore di aggiornamento del flag storico movimenti magazzino di " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	//commit ad ogni prodotto
	commit;
	
	li_file = fileopen(ls_file, linemode!, write!, LockReadWrite!, append!)
	filewrite(li_file, string(today(),"dd/mm/yyyy") + " " + string(now(),"hh:mm:ss") + " " + ls_cod_prodotto + "  Chiusura periodica eseguita con successo.")
	fileclose(li_file)
	
next


update con_magazzino
set    data_chiusura_periodica = :fdt_data_chiper
where  cod_azienda = :s_cs_xx.cod_azienda
using  sqlca;

if sqlca.sqlcode = -1 then
	fs_error="Si è verificato un errore in fase di aggiornamento con_magazzino"
	rollback;
	return -1
end if

commit;

return 0
end function

public function integer wf_mov_mag (datetime fdt_data_movimento, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, string fs_cod_tipo_movimento, decimal fd_quan_movimento, decimal fd_val_unit_movimento, ref long fl_anno_registrazione[], ref long fl_num_registrazione[], string fs_messaggio);string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[]
long ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_null
datetime ldt_data_stock[], ldt_null

uo_magazzino luo_mag


setnull(ll_null)
setnull(ldt_null)
ls_cod_deposito[1] = fs_cod_deposito
ls_cod_ubicazione[1] = fs_cod_ubicazione
ls_cod_lotto[1] = fs_cod_lotto
ldt_data_stock[1] = fdt_data_stock
ll_prog_stock[1] = fl_prog_stock
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

if isnull(ls_cod_deposito[1]) then 
	fs_messaggio = "Deposito mancante nello stock "
	return -1
end if
if isnull(ls_cod_ubicazione[1]) then 
	fs_messaggio = "Ubicazione mancante nello stock "
	return -1
end if
if isnull(ls_cod_lotto[1]) then 
	fs_messaggio = "Lotto mancante nello stock "
	return -1
end if
if isnull(ldt_data_stock[1]) then 
	fs_messaggio = "Data stock mancante nello stock "
	return -1
end if
if isnull(ll_prog_stock[1]) then 
	fs_messaggio = "Progressivo lotto mancante nello stock "
	return -1
end if

luo_mag = create uo_magazzino

luo_mag.ib_silentmode=true

if f_crea_dest_mov_magazzino (fs_cod_tipo_movimento, &
										fs_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock ) = -1 then
	iuo_log.error(g_str.format("Prodotto: $1~tERRORE CREAZIONE DESTINAZIONE MOVIMENTI:$2", fs_cod_prodotto, luo_mag.is_silentmode_error  ) )
	ROLLBACK;
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 fs_cod_tipo_movimento, &
								 fs_cod_prodotto) = -1 then
	iuo_log.error(g_str.format("Prodotto: $1~tERRORE VERIFICA MOVIMENTI:$2", fs_cod_prodotto, luo_mag.is_silentmode_error  ) )
	ROLLBACK;
	return 0
end if


luo_mag.ib_chiusura = true

if luo_mag.uof_movimenti_mag ( fdt_data_movimento, &
							fs_cod_tipo_movimento, &
							"S", &
							fs_cod_prodotto, &
							fd_quan_movimento, &
							fd_val_unit_movimento, &
							ll_null, &
							ldt_null, &
							"rett.inv.", &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							fl_anno_registrazione[], &
							fl_num_registrazione[] ) = 0 then
							
	destroy luo_mag
							
	COMMIT;

	if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock) = -1 then
		ROLLBACK;         // rollback della sol eliminazione dest_mov_magazzino
	end if
else
	iuo_log.error(g_str.format("Prodotto: $1~tERRORE IN CREAZIONE MOVIMENTO:$2", fs_cod_prodotto, luo_mag.is_silentmode_error  ) )
	destroy luo_mag
	ROLLBACK;
	return -1
end if	
return 0
end function

public function integer wf_chiusura_annuale_mag (datetime fdt_data_chius, ref string fs_error);// ------------------------------------------------------------------------------------------
// wf_chiusura_annuale( fdt_data_chius, fs_error)
//
//                       Funzione che esegue la chiusura annuale
//
// Calcola le quantità inizio anno, azzera i progressivi e le rispettive valorizzazioni.
// La procedura calcola inoltre il costo medio .
// Tali valori vengono calcolati come differenza fra quanto letto in anag_prodotti 
// e quanto calcolato per i movimenti dopo la chiusura annuale.
// Inoltre la procedura esegue un movimento di apertuta per ogni lotto verificando che la
// giacenza dei lotto corrisponda con la giacenza del prodotto a magazzino.
// Prima di tutto deve essere fatta la la chiusura periodica
// ritorna -1 in caso di errore
//
// ------------------------------------------------------------------------------------------

string ls_cod_prodotto, ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_prod_in_chius[], ls_where, &
       ls_filtro_prodotto, ls_chiave[], ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_data_stock, ls_prog_stock, &
		 ls_messaggio,ls_cod_tipo_mov_inv_iniz,ls_cod_tipo_mov_chius_annuale, ls_str, ls_tipo_valorizzazione, ls_vuoto[], &
		 ls_file
integer li_ret_funz, li_file
long ll_num_righe, ll_cont,ll_i, ll_y, ll_num_prod, ll_pos, ll_pos_old, ll_prog_stock,ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], &
     ll_anno_reg_mov_mag_chius[], ll_num_reg_mov_mag_chius[],ll_anno
dec{4}   ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
			ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
			ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
			ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
			ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[], ld_costo_medio_stock[], &
			ld_quan_costo_medio_stock[],ld_saldo_quan_anno_prec_1, ld_vuoto[]
datetime ldt_data_chiusura_annuale_prec, ldt_data_registrazione, ldt_oggi, ldt_data_chiusura_periodica, ldt_max_data_mov, &
         ldt_data_stock,ldt_data_apertura
datastore lds_mov_magazzino
uo_magazzino luo_magazzino

string ls_file_error
integer li_file_error
boolean lb_salta_prodotto = false

// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven


iuo_log.info( fill("*", 100) + "~r~n~t~tProcedura di chiusura / apertura eseguita il:" + string(today()) + "   ore:" + string(now()) + "~r~n" + fill("*",100) )

ls_filtro_prodotto = dw_chiusure.getitemstring(1,"cod_prodotto")
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

select data_chiusura_annuale, 
       data_chiusura_periodica,
		 cod_tipo_mov_inv_iniz,
		 cod_tipo_mov_chius_annuale
into   :ldt_data_chiusura_annuale_prec,
	    :ldt_data_chiusura_periodica,
		 :ls_cod_tipo_mov_inv_iniz,
		 :ls_cod_tipo_mov_chius_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if ldt_data_chiusura_annuale_prec > fdt_data_chius then
	fs_error = "Operazione di chiusura già eseguita fino alla data " + string(ldt_data_chiusura_annuale_prec)
	return -1
end if

ls_tipo_valorizzazione = ""
ls_tipo_valorizzazione = dw_chiusure.getitemstring(1,"flag_tipo_valoirizzazione")
if isnull(ls_tipo_valorizzazione) or ls_tipo_valorizzazione = "" then
	fs_error = "Assegnare un metodo di valorizzazione"
	return -1
end if

if isnull(ldt_data_chiusura_annuale_prec) then
	ldt_data_chiusura_annuale_prec=datetime(date("01/01/1900"))
end if

ll_num_righe=0

sle_1.text = "Elaborazione in corso. Attendere ......"

select max(data_registrazione)
into   :ldt_max_data_mov
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

select count(*)
into   :ll_num_righe
from   mov_magazzino
WHERE  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto like :ls_filtro_prodotto and
		 data_registrazione <= :fdt_data_chius	and 
       flag_storico = 'N' ;
		 
if ll_num_righe>0 then
	fs_error="Attenzione: prima di eseguire la chiusura annuale occorre eseguire quella periodica con la stessa data" 
	return -1
end if

ldt_oggi = datetime(today())

// seleziono i prodotti da chiudere
sle_1.text = "Ricerca dei prodotti da elaborare ....."

iuo_log.info("******************************************************")
iuo_log.info("INIZIO GENERAZIONE MOVIMENTI CHIUSURE")
iuo_log.info("******************************************************")

DECLARE cur_prod CURSOR FOR 
select distinct cod_prodotto
from  mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda AND  
		cod_prodotto like :ls_filtro_prodotto and
		data_registrazione <= :fdt_data_chius and
		data_registrazione > :ldt_data_chiusura_annuale_prec and
      flag_storico = 'S'
order by cod_prodotto;

ll_i=1

OPEN cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

DO while true
	
	FETCH cur_prod INTO :ls_cod_prodotto;
   
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_error="Errore durante lettura elenco prodotti~r~n "+SQLCA.SQLErrText
		rollback;
		return -1
	end if	
	
	sle_1.text = string(ll_i) + "  " + ls_cod_prodotto
	yield()
	
	if not isnull(ls_cod_prodotto) then 
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		ll_i = ll_i + 1
		sle_1.text = "Ricerca dei prodotti da elaborare ....."+ string(ll_i) + "(" + ls_cod_prodotto + ")"
	end if
	
LOOP

CLOSE cur_prod ;

if sqlca.sqlcode <> 0 then
	fs_error = "Errore in CLOSE cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

ls_where = ""

for ll_num_righe = 1 to  ll_i - 1
	lb_salta_prodotto = false
	
	// azzero variabili
	ld_quant_val = ld_vuoto
	ld_giacenza  = ld_vuoto
	ld_costo_medio_stock = ld_vuoto
	ld_quan_costo_medio_stock = ld_vuoto
	ls_chiave = ls_vuoto
	
	ls_cod_prodotto = ls_prod_in_chius[ll_num_righe]
	
	sle_1.text = "Elaborazione Prodotto: " + ls_cod_prodotto
	yield()
	// ricerco la situazione di ogni lotto
	
	luo_magazzino = CREATE uo_magazzino
	
	li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal ( ls_cod_prodotto, fdt_data_chius, ls_where, ref ld_quant_val[], ref ls_error, 'S', ref ls_chiave[], ref ld_giacenza[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
	
// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=val_costo_ultimo
// costo medio = [13]/[12]; [12] e [13]: mov_magazzino con flag_costo_ultimo=S
// costo ultimo: ultimo costo di acquisto alla data di riferimento
	
	if li_ret_funz < 0 then
		fs_error="Errore nel calcolo della situazione del prodotto ls_cod_prodotto, " + ls_error
		rollback;
		return -1
	end if
	
	if isnull(ld_quant_val[1]) then ld_quant_val[1] = 0
	if isnull(ld_quant_val[2]) then ld_quant_val[2] = 0
	if isnull(ld_quant_val[8]) then ld_quant_val[8] = 0
	if isnull(ld_quant_val[9]) then ld_quant_val[9] = 0
	
	choose case ls_tipo_valorizzazione
		case "1"		// costo medio (val_iniz + val_acq ) / (qta_iniz + qta_acq)
			/*
			ld_quan_acq = ld_quant_val[1] + ld_quant_val[8]
			ld_val_acq = ld_quant_val[2] + ld_quant_val[9]
			if ld_quan_acq <> 0 then
				ld_valore_prodotto = ld_val_acq / ld_quan_acq
			else
				ld_valore_prodotto = 0
			end if
			
			ld_valore_prodotto = round(ld_valore_prodotto, 4)
			*/
			if ld_quant_val[12] > 0 then
				ld_valore_prodotto = round(ld_quant_val[13] / ld_quant_val[12],4)
			else
				ld_valore_prodotto = 0
			end if
			
			iuo_log.info(g_str.format("Prodotto: $6 Valorizzato al costo medio acquisto da inventario:$5 ~tQuantità Inziale:~t$1~tValore Iniziale:~t$2~tQuantità Acquistata:~t$3~tValore Acquistato:$4 ",  ld_quant_val[1],  ld_quant_val[2], ld_quant_val[8], ld_quant_val[9], ld_valore_prodotto, ls_cod_prodotto))
			
		case "2"		// costo standard
			select costo_standard
			into   :ld_valore_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :ls_cod_prodotto;

			iuo_log.info(g_str.format("Prodotto: $2 Valorizzato al costo standard:$1", ld_valore_prodotto, ls_cod_prodotto))
					 
		case "3"		// costo ultimo
			ld_val_acq = ld_quant_val[14]
			if ld_val_acq <> 0 then
				ld_valore_prodotto = ld_val_acq
			else
				ld_valore_prodotto = 0
			end if
			ld_valore_prodotto = round(ld_valore_prodotto, 4)
			
			iuo_log.info(g_str.format("Prodotto: $2 Valorizzato al costo ultino:$1", ld_valore_prodotto, ls_cod_prodotto))

		case "4"		// costo medio di acquisto

			// nuovo sistema identico alla valorizzazione dell'inventario
			
			if ld_quant_val[12] > 0 then
				ld_valore_prodotto = ld_quant_val[13] / ld_quant_val[12]
				iuo_log.info(g_str.format("Prodotto: $3 Valorizzato al costo medio ponderato :$4:~tValore Acquisti:~t$1~tQuantità Acquisti:~t$2",  ld_quant_val[13], ld_quant_val[12], ld_valore_prodotto, ls_cod_prodotto))
			else
				
				select costo_medio_ponderato
				into   :ld_valore_prodotto
				from   lifo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 anno_lifo = (select max(anno_lifo)
										  from   lifo
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_prodotto = :ls_cod_prodotto);
														
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Chiusure di magazzino: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
					rollback;
					return -1
				elseif sqlca.sqlcode = 100 or isnull(ld_valore_prodotto) then
					ld_valore_prodotto = 0
				end if
				
				iuo_log.info(g_str.format("Prodotto: $2 Valorizzato al costo medio ultimo LIFO :$1 ", ld_valore_prodotto, ls_cod_prodotto))
			
			end if

			ld_valore_prodotto = round(ld_valore_prodotto, 4)
			
			
			
	end choose
	

	// mi salvo questo valore per non perderlo con i movimenti successivi.

	select saldo_quan_inizio_anno 
	into   :ld_saldo_quan_anno_prec_1
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		fs_error = "Errore durante aggiornamento progressivi prodotto " + ls_cod_prodotto + ", " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	// azzero i saldi altrimento si sommerebbero in modo errato (EnMe 11/10/2004)
	
	update anag_prodotti
	set    saldo_quan_inizio_anno = 0,   
			 val_inizio_anno = 0,   
			 saldo_quan_ultima_chiusura = 0
	WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
			 cod_prodotto = :ls_cod_prodotto ;
	if sqlca.sqlcode < 0 then
		fs_error = "Errore durante aggiornamento progressivi prodotto " + ls_cod_prodotto + ", " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	

	// eseguo movimento di apertura per ogni lotto.
	
	for ll_y = 1 to upperbound(ls_chiave[])
		if len(ls_chiave[ll_y]) > 0 and not isnull(ls_chiave[ll_y]) then
			
			sle_1.text = "Movimento lotto " + string(ll_y)
			yield()
			
			ll_pos = pos(ls_chiave[ll_y],"-",1)
			ls_cod_deposito = mid(ls_chiave[ll_y],1,ll_pos - 1)
			
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_y],"-",ll_pos_old)
			ls_cod_ubicazione = mid(ls_chiave[ll_y],ll_pos_old,ll_pos - ll_pos_old)
			
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_y],"-",ll_pos_old)
			ls_cod_lotto = mid(ls_chiave[ll_y],ll_pos_old,ll_pos - ll_pos_old)
		
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_y],"-",ll_pos_old)
			ls_data_stock = mid(ls_chiave[ll_y],ll_pos_old,ll_pos - ll_pos_old)
			
			ll_pos_old = ll_pos + 1
			ls_prog_stock = mid(ls_chiave[ll_y],ll_pos_old)
			
			ldt_data_stock = datetime(date(ls_data_stock),00:00:00)	
			ll_prog_stock = long(ls_prog_stock)
			
			if ld_giacenza[ll_y] < 0 then
				iuo_log.error("QUANTITA' GIACENZA NEGATIVA sul prodotto " + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tVALORE:" + string(ld_valore_prodotto) + "~tMOVIMENTI SALTATI" )
				continue
			end if
				
			if ld_giacenza[ll_y] < 0 then
				iuo_log.error("VALORE GIACENZA NEGATIVA sul prodotto " + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tVALORE:" + string(ld_valore_prodotto) + "~tMOVIMENTI SALTATI" )
				continue
			end if
				
			
			// ----------------------  ESEGUO MOVIMENTO DI APERTURA AL 1/1   ---------------------------------------
			sle_1.text = "Movimento  apertura " + ls_cod_prodotto
			yield()
			ldt_data_apertura = datetime( relativedate(date(fdt_data_chius),1) , 00:00:00)
			if wf_mov_mag(	ldt_data_apertura, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock,&
			           				ls_cod_tipo_mov_inv_iniz, ld_giacenza[ll_y], ld_valore_prodotto, &
				        				ref ll_anno_reg_mov_mag[], ref ll_num_reg_mov_mag[], ref ls_messaggio) <> 0 then
				fs_error = "Errore eseguendo il movimento di apertura sul prodotto " + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tERRORE:" + ls_messaggio  + "~tMESSAGGIO DB:" + sqlca.sqlerrtext
				
				rollback;
				//scrivo l'errore nel log e vado avanti
				iuo_log.error(fs_error)
				
				lb_salta_prodotto = true
				continue
				//------------------------------------------------
			end if
			iuo_log.info("PRODOTTO:" + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tVALORE:" + string(ld_valore_prodotto) + "~tMOVIMENTO APERTURA:" + string(ll_anno_reg_mov_mag[1]) + "/" + string(ll_num_reg_mov_mag[1]) )
			
			// ----------------------  ESEGUO MOVIMENTO DI CHIUSURA AL 31/12 ---------------------------------------
			sle_1.text = "Movimento  chiusura " + ls_cod_prodotto
			yield()
			
			if wf_mov_mag(fdt_data_chius, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock,&
			           ls_cod_tipo_mov_chius_annuale, ld_giacenza[ll_y], ld_valore_prodotto, &
				        ref ll_anno_reg_mov_mag_chius[], ref ll_num_reg_mov_mag_chius[], ref ls_messaggio) <> 0 then
				fs_error = "Errore eseguendo il movimento di chiusura sul prodotto " + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tERRORE:" + ls_messaggio  + "~tMESSAGGIO DB:" + sqlca.sqlerrtext
				
				rollback;
				
				//scrivo l'errore nel log e vado avanti
				iuo_log.error(fs_error)
				
				lb_salta_prodotto = true
				continue
				//------------------------------------------------
			end if
			iuo_log.info("PRODOTTO:" + ls_cod_prodotto + "~tLOTTO:" + ls_chiave[ll_y] + "~tGIACENZA:" + string(ld_giacenza[ll_y]) + "~tVALORE:" + string(ld_valore_prodotto) + "~tMOVIMENTO CHIUSURA:" + string(ll_anno_reg_mov_mag_chius[1]) + "/" + string(ll_num_reg_mov_mag_chius[1]) )
			
			update mov_magazzino
			   set flag_storico = 'S'
		    where cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_reg_mov_mag_chius[1] and
			       num_registrazione = :ll_num_reg_mov_mag_chius[1];
			if sqlca.sqlcode <> 0 then
				fs_error = "Errore in impostazione flag_storico sul movimento di magazzino " + string(ll_anno_reg_mov_mag_chius[1])+"/"+ string(ll_num_reg_mov_mag_chius[1])
				iuo_log.error(fs_error)
				rollback;
				return -1
			end if
			
		end if
	next
	
	iuo_log.info("PRODOTTO:" + ls_cod_prodotto + " CHIUSURE/APERTURE TERMINATE PER I LOTTI ")
	
	COMMIT;
next
iuo_log.info("******************************************************")
iuo_log.info("Movimenti di magazzino chiusure eseguiti con successo")
iuo_log.info("******************************************************")


ll_anno = year(date(fdt_data_chius))

ll_anno = ll_anno -1

ldt_data_chiusura_annuale_prec = datetime(date("31/12/" + string(ll_anno)  ),00:00:00)

update con_magazzino
set    data_chiusura_annuale = :fdt_data_chius,
 	    data_chiusura_periodica = :fdt_data_chius,
 	    data_chiusura_annuale_prec = :ldt_data_chiusura_annuale_prec
where  cod_azienda = :s_cs_xx.cod_azienda
using  sqlca;

if sqlca.sqlcode = -1 then
	fs_error="Si è verificato un errore in fase di aggiornamento con_magazzino " + sqlca.sqlerrtext
	rollback;
	return -1
end if

commit;

iuo_log.info("******************************************************")
iuo_log.info("Aggiornamento date in parametri magazzino eseguito con successo")
iuo_log.info("******************************************************")

// POSIZIONE RICACOLO PROGRESSIVI PRODOTTO
// *** 08/03/2005 michela: attenzione, il ricalcolo dei progressivi prodotto l'ho spostato in un altro for dopo l'update
//                         della con_magazzino. se lo facevo prima, la data chiusura annuale non era ancora aggiornata
//                         e quindi sballava tutti i progressivi
iuo_log.info("")
iuo_log.info("INIZIO RICALCOLO DEI PROGRESSIVI")
iuo_log.info("******************************************************")


for ll_num_righe = 1 to  ll_i - 1
	
	// ****************************** Ricalcolo Progressivi Prodotto *************************************
	// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
	// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
	// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
	
		
	ld_quant_val_attuale = ld_vuoto
	ls_chiave = ls_vuoto
	ld_giacenza = ld_vuoto
	ld_costo_medio_stock = ld_vuoto
	ld_quan_costo_medio_stock = ld_vuoto
	ls_cod_prodotto = ls_prod_in_chius[ll_num_righe]
	
	sle_1.text = "Aggiornamento progressivi prodotto " + ls_cod_prodotto
	
	luo_magazzino = CREATE uo_magazzino
	
	li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal( ls_cod_prodotto, &
	                                                         ldt_max_data_mov, &
																				ls_where, &
																				ref ld_quant_val_attuale[], &
																				ref ls_error, &
																				'N', &
																				ref ls_chiave[], &
																				ref ld_giacenza[], &
																				ref ld_costo_medio_stock[], &
																				ref ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
	
	if li_ret_funz < 0 then
		fs_error = "Errore nel calcolo della situazione attuale del prodotto " + ls_cod_prodotto + ", " + ls_error
		return -1
	end if
	
	iuo_log.info(g_str.format( "Prodotto $1 aggiornamento progressivi eseguito correttamente!" ,ls_cod_prodotto))

	ld_saldo_quan_inizio_anno = ld_quant_val_attuale[1]
	ld_prog_quan_entrata = ld_quant_val_attuale[4]
	ld_val_quan_entrata = ld_quant_val_attuale[5]
	ld_prog_quan_uscita = ld_quant_val_attuale[6]
	ld_val_quan_uscita = ld_quant_val_attuale[7]
	ld_prog_quan_acquistata = ld_quant_val_attuale[8]
	ld_val_quan_acquistata = ld_quant_val_attuale[9]
	ld_prog_quan_venduta = ld_quant_val_attuale[10]
	ld_val_quan_venduta = ld_quant_val_attuale[11]
	
	update anag_prodotti 
	set	 saldo_quan_inizio_anno = :ld_saldo_quan_inizio_anno,
			 saldo_quan_ultima_chiusura = :ld_saldo_quan_inizio_anno,
			 prog_quan_entrata = :ld_prog_quan_entrata,
			 val_quan_entrata = :ld_val_quan_entrata,
			 prog_quan_uscita = :ld_prog_quan_uscita,
			 val_quan_uscita = :ld_val_quan_uscita,
			 prog_quan_acquistata = :ld_prog_quan_acquistata,
			 val_quan_acquistata = :ld_val_quan_acquistata,
			 prog_quan_venduta = :ld_prog_quan_venduta,
			 val_quan_venduta = :ld_val_quan_venduta
	WHERE  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_prodotto = :ls_cod_prodotto;	
	
	if sqlca.sqlcode <> 0 then
		fs_error = "Errore durante aggiornamento progressivi prodotto " + ls_cod_prodotto + ", " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	commit;
	
next

commit;

iuo_log.info("PROGRESSIVI ANAGRAFICA PRODOTTI RICALCOLATI CON SUCCESSO")

destroy lds_mov_magazzino
sle_1.text = "Elaborazione terminata con successo !  "
return 0
end function

on w_chiusure_magazzino.create
int iCurrent
call super::create
this.st_file=create st_file
this.dw_chiusure=create dw_chiusure
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_file
this.Control[iCurrent+2]=this.dw_chiusure
this.Control[iCurrent+3]=this.sle_1
end on

on w_chiusure_magazzino.destroy
call super::destroy
destroy(this.st_file)
destroy(this.dw_chiusure)
destroy(this.sle_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_chiusure.insertrow(0)
end event

type st_file from statictext within w_chiusure_magazzino
integer x = 59
integer y = 1572
integer width = 2638
integer height = 148
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_chiusure from datawindow within w_chiusure_magazzino
integer x = 18
integer y = 16
integer width = 2697
integer height = 1416
integer taborder = 13
string title = "none"
string dataobject = "d_chiusure_magazzino_ext"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string ls_error, ls_file
integer li_ret_fun
datetime ldt_anno_chius,ldt_data_chius, ldt_data

choose case dwo.name
	case "b_annuale"
		
		ldt_data = dw_chiusure.getitemdatetime(1,"data_chiusura_annuale")
		
		if (isnull( ldt_data )) or (ldt_data <= datetime(date("01/01/1900"),time("00:00:000"))) then
			g_mb.messagebox("Chiusura Annuale","Indicare la data della chiusura Annuale")
			return 
		end if 
		
		SetPointer(HourGlass!)
		
		ldt_anno_chius = ldt_data
		SetPointer(Arrow!)
		
		iuo_log = create uo_log
		
		ls_file = guo_functions.uof_get_user_documents_folder( ) + "\" + guo_functions.uof_get_random_filename( )
		
		iuo_log.open(ls_file)
		
		st_file.text = ls_file
		
		li_ret_fun = wf_chiusura_annuale_mag(ldt_anno_chius, ls_error)
		
		if li_ret_fun < 0 then 
			iuo_log.error(g_str.format("PROCEDURA CHIUSURE ANNUALI INTERROTTA A CAUSA DI UN ERRORE.~t$1 ", ls_error))
			g_mb.messagebox("Errore in Chiusura Annuale", ls_error)
		else
			iuo_log.error("Chiusura Annuale eseguita con successo")
			g_mb.messagebox("Chiusura Annuale", "Chiusura Annuale eseguita con successo")
		end if
		
		
	case "b_periodica"
		
		ldt_data = dw_chiusure.getitemdatetime(1,"data_chiusura_periodica")

		if (isnull( ldt_data )) or (ldt_data <= datetime(date("01/01/1900"),time("00:00:000"))) then
			g_mb.messagebox("Chiusura Periodica","Indicare la data della chiusura Periodica")
			return 
		end if 
		
		SetPointer(HourGlass!)
		
		ldt_data_chius = ldt_data
		SetPointer(Arrow!)
		
		iuo_log = create uo_log
		iuo_log.open(guo_functions.uof_get_user_documents_folder( ) + "\" + guo_functions.uof_get_random_filename( ))
		
		li_ret_fun = wf_chiusura_periodica_mag(ldt_data_chius, ls_error)
		
		
		if li_ret_fun<0 then 
			iuo_log.error(g_str.format("PROCEDURA CHIUSURE PERIODICHE INTERROTTA A CAUSA DI UN ERRORE.~t$1 ", ls_error))
			destroy iuo_log
			g_mb.messagebox("Errore in Chiusura Periodica", ls_error)
		else
			iuo_log.error("Chiusura periodica eseguita con successo")
			destroy iuo_log
			g_mb.messagebox("Chiusura Periodica", "Chiusura Periodica eseguita con successo")
		end if

end choose
end event

type sle_1 from singlelineedit within w_chiusure_magazzino
integer x = 23
integer y = 1452
integer width = 2688
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
boolean border = false
boolean autohscroll = false
end type

