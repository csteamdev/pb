﻿$PBExportHeader$w_con_magazzino.srw
$PBExportComments$Finestra Gestione Parametri Magazzino
forward
global type w_con_magazzino from w_cs_xx_principale
end type
type dw_con_magazzino from uo_cs_xx_dw within w_con_magazzino
end type
end forward

global type w_con_magazzino from w_cs_xx_principale
integer width = 2761
integer height = 2120
string title = "Parametri Magazzino"
dw_con_magazzino dw_con_magazzino
end type
global w_con_magazzino w_con_magazzino

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_inv_ret_pos", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_inv_ret_neg", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_inv_iniz", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_chius_annuale", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_scarico_temp", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_ret_scarico_temp", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_inv_ret_val_pos", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_tipo_mov_inv_ret_val_neg", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_con_magazzino, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_magazzino.set_dw_key("cod_azienda")
dw_con_magazzino.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)

end on

on w_con_magazzino.create
int iCurrent
call super::create
this.dw_con_magazzino=create dw_con_magazzino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_magazzino
end on

on w_con_magazzino.destroy
call super::destroy
destroy(this.dw_con_magazzino)
end on

type dw_con_magazzino from uo_cs_xx_dw within w_con_magazzino
integer x = 23
integer y = 20
integer width = 2674
integer height = 1980
string dataobject = "d_con_magazzino"
boolean border = false
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

