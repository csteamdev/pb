﻿$PBExportHeader$w_correggi_schede_inventario.srw
forward
global type w_correggi_schede_inventario from w_cs_xx_principale
end type
type dw_correggi_schede_inventario_sel from datawindow within w_correggi_schede_inventario
end type
type dw_correggi_schede_inventario_2 from datawindow within w_correggi_schede_inventario
end type
type dw_folder from u_folder within w_correggi_schede_inventario
end type
type dw_correggi_schede_inventario_3 from datawindow within w_correggi_schede_inventario
end type
type dw_correggi_schede_inventario_1 from uo_cs_xx_dw within w_correggi_schede_inventario
end type
end forward

global type w_correggi_schede_inventario from w_cs_xx_principale
integer width = 4114
integer height = 3284
string title = "Esame Inventario"
dw_correggi_schede_inventario_sel dw_correggi_schede_inventario_sel
dw_correggi_schede_inventario_2 dw_correggi_schede_inventario_2
dw_folder dw_folder
dw_correggi_schede_inventario_3 dw_correggi_schede_inventario_3
dw_correggi_schede_inventario_1 dw_correggi_schede_inventario_1
end type
global w_correggi_schede_inventario w_correggi_schede_inventario

forward prototypes
public function integer wf_elabora (string as_cod_prodotto, string as_cod_deposito, long al_anno, long al_mese_inizio, long al_mese_fine, ref string as_errore)
public subroutine wf_distribuisci_scarichi (long al_riga, decimal ad_scarico)
public function integer wf_elabora_file (string as_filename, string as_cod_deposito, long al_anno, long al_mese_inizio, long al_mese_fine, ref string as_errore)
public function integer wf_imposta_quan_voluta (long al_mese, decimal al_quan_voluta)
public function integer wf_memo_schede_prodotto (ref string as_errore)
public function integer wf_view_scheda_prodotto (string as_cod_prodotto, string as_cod_deposito, ref string as_errore)
public function integer wf_genera_movimenti (ref string as_errore)
public function integer wf_crea_mov_mag (datetime adt_data_movimento, string as_cod_tipo_movimento, string as_cod_prodotto, decimal ad_quan_movimento, decimal ad_val_unit_movimento, string as_cod_deposito, string as_des_movimento, ref long al_anno_registrazione[], ref long al_num_registrazione[], ref string as_errore)
public function integer wf_distribuisci_mov_mag (string as_tipo_movimento, datetime adt_data_registrazione, string as_cod_movimento, string as_cod_prodotto, decimal ad_quan_movimento, string as_cod_deposito, ref string as_errore)
public function date wf_verifica_data (date adt_date, string as_tipo_movimento)
public subroutine wf_correggi_giacenze_negative (long al_num_riga, decimal ad_giacenza_voluta, ref decimal ad_quan_carico)
end prototypes

public function integer wf_elabora (string as_cod_prodotto, string as_cod_deposito, long al_anno, long al_mese_inizio, long al_mese_fine, ref string as_errore);string ls_where, ls_chiave[],ls_vettore_nullo[], ls_str, ls_des_prodotto
long ll_i, ll_mese, ll_lastday, ll_row,ll_num_stock, ll_j, ll_mese_corrente
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], fd_quan_costo_medio_stock[], ld_vettore_nullo[], ld_giacenza_precedente
datetime ldt_data_inventario
uo_magazzino luo_magazzino



dw_correggi_schede_inventario_1.reset()
ld_giacenza_precedente = 0

for ll_i = 1 to (al_mese_fine -  al_mese_inizio + 1)
	
	Yield()

	ll_mese_corrente = al_mese_inizio + ll_i -1
	ll_lastday = guo_functions.uof_get_ultimo_giorno_mese( ll_mese_corrente, al_anno)
	ls_str = string(ll_lastday, "00") + "/" +  string( ll_mese_corrente , "00") + "/" + string(al_anno,"0000")
	ldt_data_inventario = datetime( date( ls_str ), 00:00:00 )
	
	ls_chiave = ls_vettore_nullo
	ld_giacenza_stock = ld_vettore_nullo
	
	luo_magazzino = create uo_magazzino
	if luo_magazzino.uof_saldo_prod_date_decimal( as_cod_prodotto, ldt_data_inventario, ls_where, ref ld_quant_val[], ref as_errore, "D", ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_stock[], ref fd_quan_costo_medio_stock[]) < 0 then
		as_errore = "Errore in calcolo giacenza da inventario~r~n" + as_errore
		return -1
	end if
	
	destroy luo_magazzino

	ll_num_stock = upperbound(ls_chiave)
	ll_row = dw_correggi_schede_inventario_1.insertrow(0)
	dw_correggi_schede_inventario_1.setitem(ll_row, "cod_deposito", as_cod_deposito)
	dw_correggi_schede_inventario_1.setitem(ll_row, "cod_prodotto", as_cod_prodotto)
	dw_correggi_schede_inventario_1.setitem(ll_row, "anno", al_anno)
	dw_correggi_schede_inventario_1.setitem(ll_row, "mese", ll_mese_corrente)
	dw_correggi_schede_inventario_1.setitem(ll_row, "giacenza_inventario",  ld_giacenza_precedente)
	dw_correggi_schede_inventario_1.setitem(ll_row, "flag_mese_inventario", "N")
	
	select 	des_prodotto
	into		:ls_des_prodotto
	from		anag_prodotti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :as_cod_prodotto;
	
	dw_correggi_schede_inventario_1.setitem(ll_row, "des_prodotto", ls_des_prodotto)

	for ll_j = 1 to ll_num_stock
		if ls_chiave[ll_j] = as_cod_deposito then
			dw_correggi_schede_inventario_1.setitem(ll_row, "giacenza_inventario",  ld_giacenza_stock[ll_j])
			ld_giacenza_precedente = ld_giacenza_stock[ll_j]
			exit		// trovato ed Esco.
		end if
	next

next

for ll_i = 1 to dw_correggi_schede_inventario_1.rowcount()
	dw_correggi_schede_inventario_1.setitem(ll_i, "giacenza_finale", dw_correggi_schede_inventario_1.getitemnumber(ll_i,"giacenza_inventario"))
next

return 0
end function

public subroutine wf_distribuisci_scarichi (long al_riga, decimal ad_scarico);boolean lb_exit=false
long ll_i, ll_j, ll_mese
dec{4} ld_scarico, ld_perc, ld_scarico_perc, ld_giacenza_mese, ld_scarico_finale


ld_scarico = ad_scarico

// in ogni caso non posso scaricare più della giacenza attuale
ld_giacenza_mese = dw_correggi_schede_inventario_1.getitemnumber(al_riga, "giacenza_inventario")
if ld_giacenza_mese <= ld_scarico then
	ld_scarico = ld_giacenza_mese
end if

// primo giro verifico la possibilità di eseguire gli scarichi secondo le percentuali indicate
for ll_i = al_riga to 1 step -1
	
	ll_mese = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "mese")
	ld_perc = dw_correggi_schede_inventario_sel.getitemnumber(1, "mese_" + string(ll_mese) )
	
	ld_scarico_perc = round(ad_scarico / 100 * ld_perc,0)
	if ld_scarico_perc >= ld_scarico then ld_scarico_perc = ld_scarico
	
	// se non c'è abbastanza giacenza per scaricare
	if dw_correggi_schede_inventario_1.getitemnumber(ll_i, "giacenza_finale") <= ld_scarico_perc then
		ld_scarico_perc = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "giacenza_finale")
		lb_exit=true
	end if	
	
	ld_scarico = ld_scarico - ld_scarico_perc
	
	dw_correggi_schede_inventario_1.setitem(ll_i, "scarichi_aggiuntivi", ld_scarico_perc)
	
	for ll_j = ll_i to dw_correggi_schede_inventario_1.rowcount()
		dw_correggi_schede_inventario_1.setitem(ll_j, "giacenza_finale", dw_correggi_schede_inventario_1.getitemnumber(ll_j,"giacenza_finale") - ld_scarico_perc)
	next
	
	if lb_exit then exit
next

// verifico se mi è rimasta della quantità da scaricare e scarico di brutto
if ld_scarico > 0 then
	
	if ld_scarico <= dw_correggi_schede_inventario_1.getitemnumber(al_riga, "giacenza_finale") then
		ld_scarico_finale = ld_scarico
	else
		ld_scarico_finale = ld_scarico - dw_correggi_schede_inventario_1.getitemnumber(al_riga, "giacenza_finale")
	end if
	
	dw_correggi_schede_inventario_1.setitem(al_riga, "scarichi_aggiuntivi", dw_correggi_schede_inventario_1.getitemnumber(al_riga, "scarichi_aggiuntivi") + ld_scarico_finale)

	for ll_i = al_riga to dw_correggi_schede_inventario_1.rowcount()
		dw_correggi_schede_inventario_1.setitem(ll_i, "giacenza_finale", dw_correggi_schede_inventario_1.getitemnumber(ll_i,"giacenza_finale") - ld_scarico_finale)
	next

end if
	
	
end subroutine

public function integer wf_elabora_file (string as_filename, string as_cod_deposito, long al_anno, long al_mese_inizio, long al_mese_fine, ref string as_errore);long ll_file, ll_err, ll_mese, ll_pos, ll_ret, ll_riga
string ls_str, ls_cod_prodotto, ls_mese, ls_quan_voluta, ls_des_prodotto, ls_locale, ls_corsia, ls_scaffale, ls_colonna, ls_ripiano, ls_flag_memo_locazione
dec{4}	ld_quan_voluta

if FileExists ( as_filename ) then
	ll_file = FileOpen(as_filename, LineMode!, Read!)
	if ll_file < 0 then
		as_errore = "Errore durante l'apertura del file richiesto !"
		return -1
	end if
	
	ll_riga = 0
	ls_flag_memo_locazione = dw_correggi_schede_inventario_sel.getitemstring(1,"flag_memo_locazione")
	
	do while true
	
		ll_err = fileread(ll_file, ls_str)
		
		if ll_err = -100 then	exit //raggiunta file del file
		
		if ll_err < 0 then
			as_errore = "Errore di sistema durante la lettura del file !"
			fileclose(ll_file)
			return -1
		end if
		if ll_err = 0 then continue
		
		ll_riga ++
		
		ll_pos = pos(ls_str, "~t")
		if ll_pos < 1 then
			g_mb.messagebox("Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
			return -1
		end if
		ls_cod_prodotto = left(ls_str, ll_pos -1)
		ls_str = mid(ls_str, ll_pos + 1)

		// saldo colonna descrizione prodotto	
		ll_pos = pos(ls_str, "~t")
		ls_str = mid(ls_str, ll_pos + 1)
	
		ll_pos = pos(ls_str, "~t")
		if ll_pos < 1 then
			g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
			return -1
		end if
		ls_mese = left(ls_str, ll_pos -1)
		ll_mese = long(ls_mese)
		
		if ls_flag_memo_locazione = "N" then
			ls_str = mid(ls_str, ll_pos + 1)
			ld_quan_voluta = dec(ls_str)
		else
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 1 then
				g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			ld_quan_voluta = dec( left(ls_str, ll_pos -1) )
			
			// inizio con locazione
			// locale
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 1 then
				g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			ls_locale = trim(left(ls_str, ll_pos -1))
			//corsia
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 1 then
				g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			ls_corsia = trim(left(ls_str, ll_pos -1))
			// scaffale
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 1 then
				g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			ls_scaffale = trim(left(ls_str, ll_pos -1))
			// colonna
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 1 then
				g_mb.messagebox( "Apice" ,g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			ls_colonna = trim(left(ls_str, ll_pos -1))
			// ripiano
			ls_str = mid(ls_str, ll_pos + 1)
			ll_pos = pos(ls_str, "~t")
			if ll_pos < 0 then
				g_mb.messagebox("Apice", g_str.format("Errore nel formato del file alla riga $1", ll_riga), StopSign! )
				return -1
			end if
			
			if ll_pos = 0 then 
				ls_ripiano = ls_str
			else
				ls_ripiano = trim(left(ls_str, ll_pos -1))
			end if

		end if
		
		if isnull(ld_quan_voluta) then ld_quan_voluta = 0
		
		select 	des_prodotto
		into 		:ls_des_prodotto
		from		anag_prodotti
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
					
		if sqlca.sqlcode <> 0 then 
			if g_mb.messagebox("Verifica Prodotto", g_str.format("Prodotto $1 - $2 mancante in anagrafica prodotti: proseguo ?", ls_cod_prodotto, ls_des_prodotto), Question!, YesNo!, 1) = 1 then
				continue
			else
				exit
			end if
		end if
		
		if wf_elabora( ls_cod_prodotto, as_cod_deposito, al_anno, al_mese_inizio, al_mese_fine, ref as_errore) < 0 then  return -1

		wf_imposta_quan_voluta(ll_mese, ld_quan_voluta)
		
		// se richiesto memorizzo le locazioni di magazzino
		if ls_flag_memo_locazione = "S" then
			update	stock
			set			locale = :ls_locale,
						corsia = :ls_corsia,
						scaffale = :ls_scaffale,
						colonna = :ls_colonna,
						ripiano = :ls_ripiano
			where	cod_azienda		= :s_cs_xx.cod_azienda and
						cod_prodotto	= :ls_cod_prodotto and
						cod_deposito	= :as_cod_deposito;
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in memorizzazione locazioni di magazzino (wf_elabora_file)~r~n" + sqlca.sqlerrtext
				rollback;
				fileclose(ll_file)
				return -1
			end if
		end if
		
		delete 	tab_schede_magazzino
		where 	cod_azienda 		= :s_cs_xx.cod_azienda and
					cod_prodotto 		= :ls_cod_prodotto and
					cod_deposito 		= :as_cod_deposito and
					anno 				= :al_anno and
					mese 				between :al_mese_inizio and :al_mese_fine;
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in cancellazione schede magazzino precedentemente memeorizzate (wf_elabora_file)~r~n" + sqlca.sqlerrtext
			rollback;
			fileclose(ll_file)
			return -1
		end if
		
		if wf_memo_schede_prodotto(ref as_errore) < 0 then
			rollback;
			fileclose(ll_file)
			return -1
		end if
		
		commit;
		
	loop	
	
	fileclose(ll_file)
	
else
	if isnull(as_filename) then as_filename=" < non file vuoto > "
	as_errore = "il file " + as_filename + " non esiste!"
	return -1
end if

return 0
end function

public function integer wf_imposta_quan_voluta (long al_mese, decimal al_quan_voluta);long ll_i
DWObject dwo_column

for ll_i = 1 to dw_correggi_schede_inventario_1.rowcount()
	if dw_correggi_schede_inventario_1.getitemnumber(ll_i, "mese") = al_mese then
		dw_correggi_schede_inventario_1.setrow(ll_i)
		dw_correggi_schede_inventario_1.setitem(ll_i, "flag_mese_inventario", "N")
		dw_correggi_schede_inventario_1.setitem(ll_i, "giacenza_voluta", al_quan_voluta)
		dwo_column = dw_correggi_schede_inventario_1.object.giacenza_voluta
		dw_correggi_schede_inventario_1.event itemchanged(ll_i, dwo_column, string(al_quan_voluta))
		exit
	end if
next
return 0
end function

public function integer wf_memo_schede_prodotto (ref string as_errore);string 	ls_cod_prodotto,ls_cod_deposito, ls_flag_mese_inventario,ls_des_prodotto
long		ll_i, ll_anno, ll_mese
dec{4}	ld_giacenza_inventario, ld_carichi_aggiuntivi, ld_scarichi_aggiuntivi, ld_giacenza_voluta, ld_giacenza_finale


for ll_i = 1 to dw_correggi_schede_inventario_1.rowcount()

	ls_cod_deposito = dw_correggi_schede_inventario_1.getitemstring(ll_i, "cod_deposito")
	ls_cod_prodotto = dw_correggi_schede_inventario_1.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto = dw_correggi_schede_inventario_1.getitemstring(ll_i, "des_prodotto")
	ll_anno = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "anno")
	ll_mese = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "mese")
	ld_giacenza_inventario = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "giacenza_inventario")
	ld_carichi_aggiuntivi = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "carico_aggiuntivo")
	ld_scarichi_aggiuntivi = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "scarichi_aggiuntivi")
	ld_giacenza_voluta = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "giacenza_voluta")
	ld_giacenza_finale = dw_correggi_schede_inventario_1.getitemnumber(ll_i, "giacenza_finale")
	ls_flag_mese_inventario = dw_correggi_schede_inventario_1.getitemstring(ll_i,  "flag_mese_inventario")

	if isnull(ld_giacenza_inventario) then ld_giacenza_inventario = 0
	if isnull(ld_carichi_aggiuntivi) then ld_carichi_aggiuntivi = 0
	if isnull(ld_scarichi_aggiuntivi) then ld_scarichi_aggiuntivi = 0
	if isnull(ld_giacenza_voluta) then ld_giacenza_voluta = 0
	if isnull(ld_giacenza_finale) then ld_giacenza_finale = 0
	if isnull(ls_flag_mese_inventario) then ls_flag_mese_inventario = "N"
	
	INSERT INTO tab_schede_magazzino  
				( cod_azienda,   
				  cod_prodotto,   
				  cod_deposito,   
				  anno,   
				  mese,   
				  des_prodotto,   
				  giacenza_inventario,   
				  carichi_aggiuntivi,   
				  scarichi_aggiuntivi,   
				  giacenza_voluta,   
				  giacenza_finale,
				  flag_mese_inventario,
				  anno_reg_mov_carico,
				  num_reg_mov_carico,
				  anno_reg_mov_scarico,
				  num_reg_mov_scarico)  
	VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_prodotto,   
				  :ls_cod_deposito,   
				  :ll_anno,   
				  :ll_mese,   
				  :ls_des_prodotto,
				  :ld_giacenza_inventario,   
				  :ld_carichi_aggiuntivi,   
				  :ld_scarichi_aggiuntivi,   
				  :ld_giacenza_voluta,   
				  :ld_giacenza_finale,
				  :ls_flag_mese_inventario,
				  null,
				  null,
				  null,
				  null)  ;
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in insert tabella tab_schede_magazzino (wf_memo_schede_prodotto).~r~n" + sqlca.sqlerrtext
		return -1
	end if

next

return 0
end function

public function integer wf_view_scheda_prodotto (string as_cod_prodotto, string as_cod_deposito, ref string as_errore);string 	ls_sql
long		ll_i,  ll_row, ll_ret
datastore lds_data

ls_sql = " select	cod_deposito, cod_prodotto, anno, mese, giacenza_inventario, carichi_aggiuntivi, scarichi_aggiuntivi, giacenza_voluta, giacenza_finale, flag_mese_inventario, des_prodotto " + &
			" from tab_schede_magazzino " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + as_cod_prodotto + "' and cod_deposito = '" + as_cod_deposito + "' "

dw_correggi_schede_inventario_1.reset()

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
if ll_ret < 0 then
	as_errore = "Errore SQL nell'estrazione dati dalla tabella tab_schede_magazzino (wf_view_scheda_prodotto)"
	return -1
end if


for ll_i = 1 to lds_data.rowcount()
			
	ll_row = dw_correggi_schede_inventario_1.insertrow(0)
	
	dw_correggi_schede_inventario_1.setitem(ll_row, "cod_deposito", lds_data.getitemstring(ll_i, 1) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "cod_prodotto", lds_data.getitemstring(ll_i, 2) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "anno", lds_data.getitemnumber(ll_i, 3) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "mese", lds_data.getitemnumber(ll_i, 4) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "giacenza_inventario", lds_data.getitemnumber(ll_i, 5) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "carico_aggiuntivo", lds_data.getitemnumber(ll_i, 6) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "scarichi_aggiuntivi", lds_data.getitemnumber(ll_i, 7) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "giacenza_voluta", lds_data.getitemnumber(ll_i, 8) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "giacenza_finale", lds_data.getitemnumber(ll_i, 9) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "flag_mese_inventario", lds_data.getitemstring(ll_i, 10) )
	dw_correggi_schede_inventario_1.setitem(ll_row, "des_prodotto", lds_data.getitemstring(ll_i, 11) )

next

destroy lds_data

return 0
end function

public function integer wf_genera_movimenti (ref string as_errore);string ls_cod_prodotto, ls_str,ls_cod_deposito, ls_cod_mov_mag_carico,ls_cod_mov_mag_scarico, ls_errore,ls_flag_distribuzione_date
long ll_i, ll_row, ll_ret,ll_prog_stock[], ll_mese, ll_anno, ll_lastday,  ll_anno_registrazione[], ll_num_registrazione[]
dec{4} ld_quan_carico, ld_quan_scarico
datetime ldt_data_stock[], ldt_data_registrazione
datastore lds_data


ls_flag_distribuzione_date = dw_correggi_schede_inventario_3.getitemstring(1, "flag_distribuzione_date")


for ll_i = 1 to dw_correggi_schede_inventario_2.rowcount()
	
	if dw_correggi_schede_inventario_2.getitemstring(ll_i, "flag_selezione") = "N" then continue
	
	ls_cod_prodotto = dw_correggi_schede_inventario_2.getitemstring(ll_i, "tab_schede_magazzino_cod_prodotto")
	
	ls_str = " SELECT	cod_deposito,  carichi_aggiuntivi, scarichi_aggiuntivi, anno, mese FROM tab_schede_magazzino " + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' and anno_reg_mov_carico is null and anno_reg_mov_scarico is null order by anno, mese "
				
	ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_str)

	if ll_ret = 0 then continue
	if ll_ret < 0 then
		as_errore = "Errore in creazione datastore lds_data  (wf_genera_movimenti) "
		return -1
	end if

	ls_cod_mov_mag_carico = dw_correggi_schede_inventario_3.getitemstring(1, "cod_tipo_mov_mav_carico")
	ls_cod_mov_mag_scarico = dw_correggi_schede_inventario_3.getitemstring(1, "cod_tipo_mov_mav_scarico")
	
	if isnull(ls_cod_mov_mag_carico) then
		as_errore = "Attezione: è obbligatorio indicare il movimento di magazzino di CARICO."
		return -1
	end if
	
	if isnull(ls_cod_mov_mag_scarico) then
		as_errore = "Attezione: è obbligatorio indicare il movimento di magazzino di SCARICO."
		return -1
	end if
	

	for ll_row = 1 to ll_ret
		
		ls_cod_deposito = lds_data.getitemstring(ll_row, 1)
		
		ld_quan_carico = lds_data.getitemnumber(ll_row, 2)
		ld_quan_scarico = lds_data.getitemnumber(ll_row, 3)
		
		ll_anno = lds_data.getitemnumber(ll_row, 4)
		ll_mese = lds_data.getitemnumber(ll_row, 5)

		ll_lastday = guo_functions.uof_get_ultimo_giorno_mese( ll_mese, ll_anno)
		ls_str = string(ll_lastday, "00") + "/" +  string( ll_mese , "00") + "/" + string(ll_anno,"0000")
		ldt_data_registrazione = datetime( date( ls_str ), 00:00:00 )
		
		if ld_quan_carico > 0 then

			if ls_flag_distribuzione_date = "S" then
				if wf_distribuisci_mov_mag( "C", ldt_data_registrazione, ls_cod_mov_mag_carico, ls_cod_prodotto, ld_quan_carico,  ls_cod_deposito,  ls_errore) < 0 then
					g_mb.error(ls_errore)
					return -1
				end if
				// in questo caso i movimento sono molti; quindi con Alberto abbiamo deciso di non fare una tabella di dettaglio, ma solo scrivere qualcoswa per dire che il prodotto è stato elaborato
				update 	tab_schede_magazzino
				set 		anno_reg_mov_carico = 1,
							num_reg_mov_carico = 1
				where	cod_azienda = :s_cs_xx.cod_azienda and
							cod_deposito = :ls_cod_deposito and
							cod_prodotto = :ls_cod_prodotto and
							anno = :ll_anno and
							mese = :ll_mese ;
							
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in UPDATE tab_schede_magazzino; dettaglio errore: " + sqlca.sqlerrtext
					return -1
				end if
				
			else			
				if wf_crea_mov_mag( ldt_data_registrazione, ls_cod_mov_mag_carico, ls_cod_prodotto, ld_quan_carico, 0, ls_cod_deposito, "***SCH***", ll_anno_registrazione[], ll_num_registrazione[], ls_errore) < 0 then
					g_mb.error(ls_errore)
					return -1
				end if		
			
				update 	tab_schede_magazzino
				set 		anno_reg_mov_carico = :ll_anno_registrazione[1],
							num_reg_mov_carico = :ll_num_registrazione[1]
				where	cod_azienda = :s_cs_xx.cod_azienda and
							cod_deposito = :ls_cod_deposito and
							cod_prodotto = :ls_cod_prodotto and
							anno = :ll_anno and
							mese = :ll_mese ;
							
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in UPDATE tab_schede_magazzino; dettaglio errore: " + sqlca.sqlerrtext
					return -1
				end if
			end if
			
		end if
		
		if ld_quan_scarico > 0 then
			
			if ls_flag_distribuzione_date = "S" then
				if wf_distribuisci_mov_mag( "S", ldt_data_registrazione, ls_cod_mov_mag_scarico, ls_cod_prodotto, ld_quan_scarico,  ls_cod_deposito,  ls_errore) < 0 then
					g_mb.error(ls_errore)
					return -1
				end if		
				// in questo caso i movimento sono molti; quindi con Alberto abbiamo deciso di non fare una tabella di dettaglio, ma solo scrivere qualcoswa per dire che il prodotto è stato elaborato
				update 	tab_schede_magazzino
				set 		anno_reg_mov_carico = 1,
							num_reg_mov_carico = 1
				where	cod_azienda = :s_cs_xx.cod_azienda and
							cod_deposito = :ls_cod_deposito and
							cod_prodotto = :ls_cod_prodotto and
							anno = :ll_anno and
							mese = :ll_mese ;
							
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in UPDATE tab_schede_magazzino; dettaglio errore: " + sqlca.sqlerrtext
					return -1
				end if
				
			else			
				if wf_crea_mov_mag( ldt_data_registrazione, ls_cod_mov_mag_scarico, ls_cod_prodotto, ld_quan_scarico, 0, ls_cod_deposito, "***SCH***", ll_anno_registrazione[], ll_num_registrazione[], ls_errore) < 0 then
					g_mb.error(ls_errore)
					return -1
				end if	
				
				update 	tab_schede_magazzino
				set 		anno_reg_mov_scarico = :ll_anno_registrazione[1],
							num_reg_mov_scarico = :ll_num_registrazione[1]
				where	cod_azienda = :s_cs_xx.cod_azienda and
							cod_deposito = :ls_cod_deposito and
							cod_prodotto = :ls_cod_prodotto and
							anno = :ll_anno and
							mese = :ll_mese ;
							
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in UPDATE tab_schede_magazzino; dettaglio errore: " + sqlca.sqlerrtext
					return -1
				end if
			end if
			
		end if
		
		w_cs_xx_mdi.setmicrohelp("Scheda prodotto " + ls_cod_prodotto + " del deposito " + ls_cod_deposito + " elaborata.")
		Yield()
	next
	
	destroy lds_data
	
	commit;

next

return 0
end function

public function integer wf_crea_mov_mag (datetime adt_data_movimento, string as_cod_tipo_movimento, string as_cod_prodotto, decimal ad_quan_movimento, decimal ad_val_unit_movimento, string as_cod_deposito, string as_des_movimento, ref long al_anno_registrazione[], ref long al_num_registrazione[], ref string as_errore);string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[]
long ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_null
datetime ldt_data_stock[], ldt_null

uo_magazzino luo_mag

setnull(ll_null)
setnull(ldt_null)
ls_cod_deposito[1] = as_cod_deposito
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ll_prog_stock[1])
setnull(ldt_data_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

if isnull(ls_cod_deposito[1]) then 
	as_errore = "Deposito mancante nello stock del prodotto " + as_cod_prodotto
	return -1
end if

if f_crea_dest_mov_magazzino (as_cod_tipo_movimento, &
										as_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock ) = -1 then
	as_errore = "Errore durante la creazione destinazione stock del prodotto " + as_cod_prodotto
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 as_cod_tipo_movimento, &
								 as_cod_prodotto) = -1 then
	as_errore = "Errore durante la verifica destinazione stock del prodotto " + as_cod_prodotto
	return -1
end if

luo_mag = create uo_magazzino

luo_mag.ib_chiusura = true

ad_quan_movimento 		= round(ad_quan_movimento, 4)
ad_val_unit_movimento 	= round(ad_val_unit_movimento, 4)

if luo_mag.uof_movimenti_mag ( adt_data_movimento, &
							as_cod_tipo_movimento, &
							"S", &
							as_cod_prodotto, &
							ad_quan_movimento, &
							ad_val_unit_movimento, &
							ll_null, &
							ldt_null, &
							as_des_movimento, &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							al_anno_registrazione[], &
							al_num_registrazione[] ) = 0 then
							
	destroy luo_mag
							
	f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, 	ll_num_reg_dest_stock)
	
else
	as_errore = "Errore durante la creazione movimento di magazzino del prodotto " + as_cod_prodotto
	destroy luo_mag
	return -1
end if	

destroy luo_mag
return 0
end function

public function integer wf_distribuisci_mov_mag (string as_tipo_movimento, datetime adt_data_registrazione, string as_cod_movimento, string as_cod_prodotto, decimal ad_quan_movimento, string as_cod_deposito, ref string as_errore);// questa funzione serve per spalmare i movimenti di carico e scarico in modo proporzionale e progressivo nel mese

boolean lb_exit
string ls_flag_distribuzione_date, ls_errore
long  ll_anno_registrazione[], ll_num_registrazione[], ll_mese, ll_perc_distribuzione[10], ll_i
dec{4} ld_quan_distribuzione, ld_quan_cumulata
date ldd_data_registrazione, ldd_data_inizio

ll_perc_distribuzione[1] = 12
ll_perc_distribuzione[2] = 8
ll_perc_distribuzione[3] = 5
ll_perc_distribuzione[4] = 13
ll_perc_distribuzione[5] = 17
ll_perc_distribuzione[6] = 6
ll_perc_distribuzione[7] = 9
ll_perc_distribuzione[8] = 12
ll_perc_distribuzione[9] = 8
ll_perc_distribuzione[10] = 10



ldd_data_registrazione = date(adt_data_registrazione)
ll_mese = month( ldd_data_registrazione )

if as_tipo_movimento = "C" then
	ldd_data_inizio = date("05/" + string(month( ldd_data_registrazione )) + "/" + string(year( ldd_data_registrazione )) )
else
	ldd_data_inizio = date("25/" + string(month( ldd_data_registrazione )) + "/" + string(year( ldd_data_registrazione )) )
end if

ld_quan_cumulata = 0
lb_exit = false

for ll_i = 1 to 10

	// calcolo la quantità da distribuire
	if ad_quan_movimento <= 10 then 		// se meno di 10 pezzi faccio un solo movimento
		ld_quan_distribuzione = ad_quan_movimento
		lb_exit = true
	else
		ld_quan_distribuzione = round(ad_quan_movimento / 100 * ll_perc_distribuzione[ll_i] , 0)
		if ld_quan_distribuzione <= 0 then continue
		
		if ll_i = 10 then
			ld_quan_distribuzione = ad_quan_movimento - ld_quan_cumulata 		// alla fine faccio il movimento del rimanente
		else
			ld_quan_cumulata += ld_quan_distribuzione
			// raggiunta la quantità, esco
			if ld_quan_cumulata >= ad_quan_movimento then lb_exit = true
		end if
	end if		
	

	if as_tipo_movimento = "C" 	then	// movimento di carico, quindi inizio a distribuire dal 5 del mese in avanti saltando sabato, domenica
			ldd_data_inizio = relativedate(ldd_data_inizio,1)
			ldd_data_inizio = wf_verifica_data(ldd_data_inizio,"C")
	else		// movimento di scarico, quindi inizio a distribuire dal 25 del mese verso l'inizio mese saltanto sabato e domenica
			ldd_data_inizio = relativedate(ldd_data_inizio,-1)
			ldd_data_inizio = wf_verifica_data(ldd_data_inizio,"S")
	end if
	
	adt_data_registrazione = datetime(ldd_data_inizio, 00:00:00)
	
	if wf_crea_mov_mag( adt_data_registrazione, as_cod_movimento, as_cod_prodotto, ld_quan_distribuzione, 1, as_cod_deposito, "***SCH***", ll_anno_registrazione[], ll_num_registrazione[], ls_errore) < 0 then
		g_mb.error(ls_errore)
		return -1
	end if		

	if lb_exit then exit
	
next

return 0
end function

public function date wf_verifica_data (date adt_date, string as_tipo_movimento);date ld_feste[], ld_data_sost_carico[], ld_data_sost_scarico[]

// salto sabato e domenica
if as_tipo_movimento = "C" then
	
	if day(adt_date)=01 and month(adt_date)=01 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=06 and month(adt_date)=01 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=25 and month(adt_date)=04 then adt_date = relativedate(adt_date,2)
	if day(adt_date)=01 and month(adt_date)=05 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=15 and month(adt_date)=08 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=01 and month(adt_date)=11 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=08 and month(adt_date)=12 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=25 and month(adt_date)=12 then adt_date = relativedate(adt_date,1)
	if day(adt_date)=26 and month(adt_date)=12 then adt_date = relativedate(adt_date,1)

	if daynumber(adt_date) =1 then
		adt_date = relativedate(adt_date,1)
	elseif daynumber(adt_date) =7 then
		adt_date = relativedate(adt_date,2)
	end if
	
	
else	
	
	if day(adt_date)=01 and month(adt_date)=01 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=06 and month(adt_date)=01 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=25 and month(adt_date)=04 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=01 and month(adt_date)=05 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=15 and month(adt_date)=08 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=01 and month(adt_date)=11 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=08 and month(adt_date)=12 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=25 and month(adt_date)=12 then adt_date = relativedate(adt_date,-1)
	if day(adt_date)=26 and month(adt_date)=12 then adt_date = relativedate(adt_date,-2)
	
	if daynumber(adt_date) =1 then
		adt_date = relativedate(adt_date, -2)
	elseif daynumber(adt_date) =7 then
		adt_date = relativedate(adt_date, -1)
	end if
	
end if

return adt_date

end function

public subroutine wf_correggi_giacenze_negative (long al_num_riga, decimal ad_giacenza_voluta, ref decimal ad_quan_carico);long ll_i, ll_mese, ll_rand, ll_row
dec{4} ld_giacenza_inventario, ld_differenza, ld_giacenza_voluta, ld_min, ld_max

randomize( 0 )

for ll_row = 1 to al_num_riga
	
	ll_mese = dw_correggi_schede_inventario_1.getitemnumber(ll_row,"mese")

	ld_giacenza_inventario = dw_correggi_schede_inventario_1.getitemnumber(ll_row,"giacenza_finale")
	if ll_row < al_num_riga then
		ld_min = int(ad_giacenza_voluta / 20)
		ld_max = int(ad_giacenza_voluta / 10)
		if ld_max = 0 then
			ld_giacenza_voluta = 0
		else
			ll_rand = rand(ld_max - ld_min)
			ld_min = ld_min + ll_rand
			ld_giacenza_voluta = int (ld_min + 1)
		end if
	else
		ld_giacenza_voluta = int(ad_giacenza_voluta)
	end if

	if  ld_giacenza_inventario < 0  then
		ld_differenza = ld_giacenza_voluta - ld_giacenza_inventario
		dw_correggi_schede_inventario_1.setitem(ll_row, "carico_aggiuntivo", ld_differenza)
		ad_quan_carico = ld_differenza
//		for ll_i = ll_row to dw_correggi_schede_inventario_1.rowcount()
//			dw_correggi_schede_inventario_1.setitem(ll_i, "giacenza_finale", dw_correggi_schede_inventario_1.getitemnumber(ll_i,"giacenza_finale") + ld_differenza)
//		next
	else
	end if			

next

return
end subroutine

on w_correggi_schede_inventario.create
int iCurrent
call super::create
this.dw_correggi_schede_inventario_sel=create dw_correggi_schede_inventario_sel
this.dw_correggi_schede_inventario_2=create dw_correggi_schede_inventario_2
this.dw_folder=create dw_folder
this.dw_correggi_schede_inventario_3=create dw_correggi_schede_inventario_3
this.dw_correggi_schede_inventario_1=create dw_correggi_schede_inventario_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_correggi_schede_inventario_sel
this.Control[iCurrent+2]=this.dw_correggi_schede_inventario_2
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_correggi_schede_inventario_3
this.Control[iCurrent+5]=this.dw_correggi_schede_inventario_1
end on

on w_correggi_schede_inventario.destroy
call super::destroy
destroy(this.dw_correggi_schede_inventario_sel)
destroy(this.dw_correggi_schede_inventario_2)
destroy(this.dw_folder)
destroy(this.dw_correggi_schede_inventario_3)
destroy(this.dw_correggi_schede_inventario_1)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_correggi_schede_inventario_sel.insertrow(0)

dw_correggi_schede_inventario_1.set_dw_options(sqlca, &
							 pcca.null_object, &
							 c_nomodify + &
							 c_nodelete + &
							 c_newonopen + &
							 c_disableCC, &
							 c_noresizedw + &
							 c_nohighlightselected + &
							 c_nocursorrowpointer +&
							 c_nocursorrowfocusrect )


lw_oggetti[1] = dw_correggi_schede_inventario_sel
dw_folder.fu_assigntab(1, "Controllo", lw_oggetti[])

lw_oggetti[1] = dw_correggi_schede_inventario_2
lw_oggetti[2] = dw_correggi_schede_inventario_3
dw_folder.fu_assigntab(2, "Schede", lw_oggetti[])

dw_correggi_schede_inventario_3.reset()
dw_correggi_schede_inventario_3.insertrow(0)

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_correggi_schede_inventario_sel.setitem(1,"anno", f_anno_esercizio() )

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_correggi_schede_inventario_sel, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) AND flag_tipo_deposito<>'C' ")


f_PO_LoadDDDW_DW(dw_correggi_schede_inventario_3,"cod_tipo_mov_mav_carico",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_correggi_schede_inventario_3,"cod_tipo_mov_mav_scarico",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event resize;/* TOLTO ANCESTOR SCRITP */

dw_folder.width = newwidth - 40
dw_correggi_schede_inventario_1.width = dw_folder.width
dw_correggi_schede_inventario_1.height = newheight - 1852
end event

type dw_correggi_schede_inventario_sel from datawindow within w_correggi_schede_inventario
integer x = 64
integer y = 124
integer width = 3776
integer height = 1672
integer taborder = 10
string title = "none"
string dataobject = "d_correggi_schede_inventario_sel"
boolean livescroll = true
end type

event buttonclicked;string	docpath,ls_filepath, ls_filename, ls_errore, ls_cod_prodotto, ls_cod_deposito
long		ll_rtn, ll_anno, ll_mese_inizio, ll_mese_fine

accepttext()

choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_correggi_schede_inventario_sel,"cod_prodotto")

	case "b_salva"
		/*
		ls_cod_prodotto = dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"cod_prodotto")
		ls_cod_deposito = dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"cod_deposito")
		ll_anno = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"anno")
		ll_mese_inizio = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_inizio")
		ll_mese_fine = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_fine")
		
		delete 	tab_schede_magazzino
		where 	cod_azienda 		= :s_cs_xx.cod_azienda and
					cod_prodotto 		= :ls_cod_prodotto and
					cod_deposito 		= :ls_cod_deposito and
					anno 				= :ll_anno and
					mese 				between :ll_mese_inizio and :ll_mese_fine;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione schede magazzino precedentemente memeorizzate (wf_elabora_file)~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		if wf_memo_schede_prodotto(ls_errore) < 0 then
			g_mb.error(ls_errore)
			rollback;
			return
		end if
		
		commit;
		
		dw_correggi_schede_inventario_sel.object.b_salva.enabled="No"
		*/

	case "b_file"
			ll_rtn = GetFileOpenName(	"Selezione file", &
													ls_filepath, ls_filename, "TXT",  &
													"Text Files (*.TXT),*.TXT,"  + & 
													"All Files (*.*), *.*",  & 
													guo_functions.uof_get_user_documents_folder( ) , 18)
													
			if ll_rtn < 1 then return
			
			this.setitem(1, "filename", ls_filepath)

	case "b_elabora_file"
		
		if wf_elabora_file( dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"filename"), &
							dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"cod_deposito"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"anno"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_inizio"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_fine"), &
							ref ls_errore ) < 0 then
			rollback;
			g_mb.error(ls_errore)
		else
			// questo commit è solo per sicurezza; in realtà il commit viene fatto ad ogni scheda.
			commit;
		end if
		
		dw_correggi_schede_inventario_sel.setitem(dw_correggi_schede_inventario_sel.getrow(),"filename", "")
		
	case "b_elabora"
		
		//dw_correggi_schede_inventario_sel.object.b_salva.enabled="No"
		
		if wf_elabora( dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"cod_prodotto"), &
							dw_correggi_schede_inventario_sel.getitemstring(dw_correggi_schede_inventario_sel.getrow(),"cod_deposito"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"anno"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_inizio"), &
							dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_fine"), &
							ref ls_errore ) < 0 then
			rollback;
			g_mb.error(ls_errore)
		else
			rollback;
		end if

end choose
end event

event itemchanged;string ls_errore

choose case dwo.name
	case "cod_prodotto"
		if  not isnull( data ) and not isnull(getitemstring(row,"cod_deposito")) then
			if wf_view_scheda_prodotto(data, getitemstring(row,"cod_deposito"), ref ls_errore) < 0 then
				g_mb.error("ls_errore")
			end if
		end if
end choose
end event

type dw_correggi_schede_inventario_2 from datawindow within w_correggi_schede_inventario
event ue_retrieve ( )
integer x = 50
integer y = 124
integer width = 3945
integer height = 1132
integer taborder = 30
string title = "none"
string dataobject = "d_correggi_schede_inventario_2"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();string ls_sql, ls_cod_deposito,ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_where, ls_where_1, ls_where_2
long ll_anno, ll_mese_inizio, ll_mese_fine
settransobject(sqlca)

dw_correggi_schede_inventario_3.accepttext()
dw_correggi_schede_inventario_sel.accepttext()


ls_sql = " SELECT distinct tab_schede_magazzino.cod_prodotto, 	anag_prodotti.des_prodotto, tab_schede_magazzino.cod_deposito, anag_depositi.des_deposito, giacenza_voluta - giacenza_finale as differenza, 'N' as flag_selezione, tab_schede_magazzino.anno_reg_mov_carico, tab_schede_magazzino.num_reg_mov_carico, tab_schede_magazzino.anno_reg_mov_scarico, tab_schede_magazzino.num_reg_mov_scarico " + &
			" FROM tab_schede_magazzino   " + &
			" join anag_prodotti on anag_prodotti.cod_azienda = tab_schede_magazzino.cod_azienda and anag_prodotti.cod_prodotto = tab_schede_magazzino.cod_prodotto " + &
			" join anag_depositi on anag_depositi.cod_azienda = tab_schede_magazzino.cod_azienda and anag_depositi.cod_deposito = tab_schede_magazzino.cod_deposito "

ls_where = " WHERE tab_schede_magazzino.cod_azienda = '" +s_cs_xx.cod_azienda +"' "
ls_where_2 = " and flag_mese_inventario = 'S' "
			
ll_anno = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"anno")
ll_mese_inizio = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_inizio")
ll_mese_fine = dw_correggi_schede_inventario_sel.getitemnumber(dw_correggi_schede_inventario_sel.getrow(),"mese_fine")
ls_cod_deposito = dw_correggi_schede_inventario_sel.getitemstring(1,"cod_deposito") 
ls_cod_prodotto_inizio = dw_correggi_schede_inventario_3.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_correggi_schede_inventario_3.getitemstring(1,"cod_prodotto_fine")

ls_where += " and tab_schede_magazzino.anno = " + string(ll_anno)
ls_where += " and tab_schede_magazzino.mese >= " + string(ll_mese_inizio) + " and tab_schede_magazzino.mese <= " + string(ll_mese_fine)

if not isnull(ls_cod_prodotto_inizio) and len(ls_cod_prodotto_inizio) > 0 then
	ls_where += " and tab_schede_magazzino.cod_prodotto >= '" + ls_cod_prodotto_inizio + "' "
end if	
if not isnull(ls_cod_prodotto_fine) and len(ls_cod_prodotto_fine) > 0 then
	ls_where += " and tab_schede_magazzino.cod_prodotto <= '" + ls_cod_prodotto_fine + "' "
end if	

if not isnull(ls_cod_deposito) then ls_where += " and tab_schede_magazzino.cod_deposito = '" + ls_cod_deposito + "' "
			
ls_where_1 = ""
choose  case dw_correggi_schede_inventario_3.getitemstring(1,"flag_incompleti")
	case "N"  // mostra solo prodotti che hanno movimenti
		ls_where_1 += " and (giacenza_voluta - giacenza_finale) = 0 "
		
	case "S" // mostra solo prodotti senza movimenti
		ls_where_1 += " and (giacenza_voluta - giacenza_finale) <> 0 "
end choose

choose  case dw_correggi_schede_inventario_3.getitemstring(1,"flag_selezione")
	case "S"  // mostra solo prodotti che hanno movimenti

		ls_where += ls_where_1 + ls_where_2 + " and tab_schede_magazzino.cod_prodotto in ( " + &
						" select distinct cod_prodotto from tab_schede_magazzino " + ls_where + &
						" and (tab_schede_magazzino.anno_reg_mov_carico is not null or  tab_schede_magazzino.anno_reg_mov_scarico is not null ) )"
						
	case "N" // mostra solo prodotti senza movimenti
		ls_where += ls_where_1 + ls_where_2 + " and tab_schede_magazzino.cod_prodotto NOT in ( " + &
						" select distinct cod_prodotto from tab_schede_magazzino " + ls_where + &
						" and (tab_schede_magazzino.anno_reg_mov_carico is not null or  tab_schede_magazzino.anno_reg_mov_scarico is not null ) ) "
						
	case "T" // mostra solo prodotti senza movimenti
		ls_where += ls_where_1 + ls_where_2
						
end choose


dw_correggi_schede_inventario_2.setsqlselect( ls_sql + ls_where + " order by tab_schede_magazzino.cod_prodotto ")
		
retrieve()
end event

event clicked;string ls_errore

if row > 0 then
	if  not isnull(getitemstring(row,1)) and not isnull(getitemstring(row,3)) then
		
		if wf_view_scheda_prodotto(getitemstring(row,1), getitemstring(row,3), ref ls_errore) < 0 then
			g_mb.error("ls_errore")
		end if
	end if
end if
		
end event

event doubleclicked;choose case dwo.type
	case "text"
end choose


end event

event buttonclicked;choose case dwo.name
	case "b_sel"
		
		string ls_selezione
		long ll_i
		
		ls_selezione = getitemstring(1, "flag_selezione")
		
		for ll_i = 1 to rowcount()
			
			if ls_selezione = "S" then
				setitem(ll_i, "flag_selezione", "N")
			else
				setitem(ll_i, "flag_selezione", "S")
			end if
			
		next
end choose
end event

type dw_folder from u_folder within w_correggi_schede_inventario
integer x = 18
integer y = 20
integer width = 4032
integer height = 1812
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab
		
	case 1
		iuo_dw_main = dw_correggi_schede_inventario_1
	
	case 2
		
		dw_correggi_schede_inventario_2.triggerevent( "ue_retrieve" )
		
end choose


end event

type dw_correggi_schede_inventario_3 from datawindow within w_correggi_schede_inventario
integer x = 64
integer y = 1256
integer width = 3941
integer height = 432
integer taborder = 40
string title = "none"
string dataobject = "d_correggi_schede_inventario_3"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string ls_errore

choose case dwo.name
	case "b_gen_movimenti"
		if wf_genera_movimenti(ls_errore) < 0 then
			rollback;
			g_mb.error(ls_errore)
		else
			commit;
		end if
		dw_correggi_schede_inventario_2.postevent( "ue_retrieve" )
		
	case "b_ricerca_prod_inizio"
		
		guo_ricerca.uof_ricerca_prodotto(dw_correggi_schede_inventario_3,"cod_prodotto_inizio")

	case "b_ricerca_prod_fine"
		
		guo_ricerca.uof_ricerca_prodotto(dw_correggi_schede_inventario_3,"cod_prodotto_fine")
		
end choose


end event

event itemchanged;string ls_errore

choose case dwo.name
	case "cod_prodotto_inizio"
		dw_correggi_schede_inventario_2.postevent( "ue_retrieve" )
		if not isnull(data) then setitem(row, "cod_prodotto_fine", data)
	case "cod_prodotto_fine", "flag_selezione", "flag_incompleti"
		dw_correggi_schede_inventario_2.postevent( "ue_retrieve" )
end choose

end event

type dw_correggi_schede_inventario_1 from uo_cs_xx_dw within w_correggi_schede_inventario
integer x = 18
integer y = 1848
integer width = 4027
integer height = 1300
integer taborder = 10
string dataobject = "d_correggi_schede_inventario_1"
end type

event itemchanged;call super::itemchanged;long ll_i
dec{4} ld_giacenza_voluta, ld_giacenza_inventario, ld_differenza, ld_carico_aggiuntivo, ld_carico_aggiuntivo_old, ld_scarichi_aggiuntivi, ld_scarichi_aggiuntivi_old, ld_quan_carico
		
choose case dwo.name
		
	case "giacenza_voluta" 
		
		ld_giacenza_voluta = dec(data)
		
		ld_quan_carico = 0
		wf_correggi_giacenze_negative(row, ld_giacenza_voluta, ld_quan_carico)
		
		ld_giacenza_inventario = getitemnumber(row,"giacenza_finale")

		if ld_giacenza_voluta > ld_giacenza_inventario then
			ld_differenza = ld_giacenza_voluta - ld_giacenza_inventario
//			setitem(row, "carico_aggiuntivo", ld_differenza + ld_quan_carico)
			setitem(row, "carico_aggiuntivo", ld_differenza)
			for ll_i = row to rowcount()
				setitem(ll_i, "giacenza_finale", getitemnumber(ll_i,"giacenza_finale") + ld_differenza)
			next
		else
			ld_differenza = getitemnumber(row,"giacenza_finale") - ld_giacenza_voluta
			setitem(row, "scarichi_aggiuntivi", ld_differenza)
		end if	

		if getitemnumber(row,"scarichi_aggiuntivi") > 0 then
			wf_distribuisci_scarichi(row, getitemnumber(row,"scarichi_aggiuntivi"))
		end if	
		
		for ll_i = row to rowcount()
			setitem(ll_i, "flag_mese_inventario", "N")
		next
		
		setitem(row, "flag_mese_inventario", "S")
		
		
	case "carico_aggiuntivo" 
		ld_carico_aggiuntivo_old = getitemnumber(row, "carico_aggiuntivo")
		if isnull(ld_carico_aggiuntivo_old)  then ld_carico_aggiuntivo_old = 0
		ld_carico_aggiuntivo = dec(data)
		if isnull(ld_carico_aggiuntivo)  then ld_carico_aggiuntivo = 0
		
		ld_carico_aggiuntivo = ld_carico_aggiuntivo - ld_carico_aggiuntivo_old
		
		for ll_i = row to rowcount()
			setitem(ll_i, "giacenza_finale", getitemnumber(ll_i,"giacenza_finale") + ld_carico_aggiuntivo)
		next
		
	case "scarichi_aggiuntivi" 
		ld_scarichi_aggiuntivi_old = getitemnumber(row, "scarichi_aggiuntivi")
		if isnull(ld_scarichi_aggiuntivi_old)  then ld_scarichi_aggiuntivi_old = 0
		ld_scarichi_aggiuntivi = dec(data)
		if isnull(ld_scarichi_aggiuntivi)  then ld_scarichi_aggiuntivi = 0
		
		ld_scarichi_aggiuntivi = ld_scarichi_aggiuntivi - ld_scarichi_aggiuntivi_old

		for ll_i = row to rowcount()
			setitem(ll_i, "giacenza_finale", getitemnumber(ll_i,"giacenza_finale") - ld_scarichi_aggiuntivi)
		next
		
end choose		
end event

event buttonclicked;call super::buttonclicked;string	docpath,ls_filepath, ls_filename, ls_errore, ls_cod_prodotto, ls_cod_deposito
long		ll_rtn, ll_anno, ll_mese, ll_i

accepttext()

choose case dwo.name
	case "b_salva_scheda"
		
		for ll_i = row to rowcount()
			if getitemstring(ll_i, "flag_mese_inventario") = "S" then exit
			if ll_i = rowcount() then // non ho trovato 1 mese con inventario, ovvero in ui l'operatore abbia forzato una giacenza voluta
				g_mb.error("Attenzione: è obbligatorio imporre una giacenza voluta, anche reimpostando la medesima.")
				return
			end if
		next

		
		for ll_i = 1 to rowcount()
			
			ls_cod_prodotto = getitemstring(ll_i,"cod_prodotto")
			ls_cod_deposito = getitemstring(ll_i,"cod_deposito")
			ll_anno = getitemnumber(ll_i,"anno")
			ll_mese = getitemnumber(ll_i,"mese")
			
			delete 	tab_schede_magazzino
			where 	cod_azienda 		= :s_cs_xx.cod_azienda and
						cod_prodotto 		= :ls_cod_prodotto and
						cod_deposito 		= :ls_cod_deposito and
						anno 				= :ll_anno and
						mese 				= :ll_mese ;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in cancellazione schede magazzino precedentemente memorizzate ~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		next		
		
		if wf_memo_schede_prodotto(ls_errore) < 0 then
			g_mb.error(ls_errore)
			rollback;
			return
		end if
		
		commit;
		
		w_cs_xx_mdi.setmicrohelp("Scheda prodotto " + ls_cod_prodotto + " salvata.")
		
		dw_correggi_schede_inventario_2.postevent("ue_retrieve")
		
end choose
end event

