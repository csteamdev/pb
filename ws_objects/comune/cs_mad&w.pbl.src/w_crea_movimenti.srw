﻿$PBExportHeader$w_crea_movimenti.srw
$PBExportComments$Finestra Creazione Movimenti Magazzino Automatici
forward
global type w_crea_movimenti from w_cs_xx_principale
end type
type dw_mov_magazzino_det from uo_cs_xx_dw within w_crea_movimenti
end type
type cb_reset from commandbutton within w_crea_movimenti
end type
type cb_ricerca from commandbutton within w_crea_movimenti
end type
type dw_mov_magazzino_lista from uo_cs_xx_dw within w_crea_movimenti
end type
type dw_ricerca from u_dw_search within w_crea_movimenti
end type
type dw_folder_search from u_folder within w_crea_movimenti
end type
end forward

global type w_crea_movimenti from w_cs_xx_principale
int Width=2597
int Height=1885
boolean TitleBar=true
string Title="Movimenti di Magazzino"
dw_mov_magazzino_det dw_mov_magazzino_det
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_mov_magazzino_lista dw_mov_magazzino_lista
dw_ricerca dw_ricerca
dw_folder_search dw_folder_search
end type
global w_crea_movimenti w_crea_movimenti

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "(tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "')")

end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]


dw_mov_magazzino_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
dw_mov_magazzino_det.set_dw_options(sqlca, &
                               dw_mov_magazzino_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
iuo_dw_main=dw_mov_magazzino_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)


// -----------------------------  aggiunto per ricerca prodotti ----------------------------
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

l_criteriacolumn[1] = "rs_cod_tipo_movimento"
l_criteriacolumn[2] = "rs_cod_prodotto"
l_criteriacolumn[3] = "rs_cod_depositi"
l_criteriacolumn[4] = "rs_cod_ubicazione"
l_criteriacolumn[5] = "rs_cod_lotto"
l_criteriacolumn[6] = "rn_num_documento"
l_criteriacolumn[7] = "rdd_data_documento"
l_criteriacolumn[8] = "rs_referenza"
l_criteriacolumn[9] = "rs_flag_storico"

l_searchtable[1] = "mov_magazzino"
l_searchtable[2] = "mov_magazzino"
l_searchtable[3] = "mov_magazzino"
l_searchtable[4] = "mov_magazzino"
l_searchtable[5] = "mov_magazzino"
l_searchtable[6] = "mov_magazzino"
l_searchtable[7] = "mov_magazzino"
l_searchtable[8] = "mov_magazzino"
l_searchtable[9] = "mov_magazzino"

l_searchcolumn[1] = "cod_tipo_movimento"
l_searchcolumn[2] = "cod_prodotto"
l_searchcolumn[3] = "cod_depositi"
l_searchcolumn[4] = "cod_ubicazione"
l_searchcolumn[5] = "cod_lotto"
l_searchcolumn[6] = "num_documento"
l_searchcolumn[7] = "data_documento"
l_searchcolumn[8] = "referenza"
l_searchcolumn[9] = "flag_storico"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_mov_magazzino_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

l_objects[1] = dw_mov_magazzino_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_mov_magazzino_lista.change_dw_current()
end event

on w_crea_movimenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_mov_magazzino_det=create dw_mov_magazzino_det
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_mov_magazzino_lista=create dw_mov_magazzino_lista
this.dw_ricerca=create dw_ricerca
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_mov_magazzino_det
this.Control[iCurrent+2]=cb_reset
this.Control[iCurrent+3]=cb_ricerca
this.Control[iCurrent+4]=dw_mov_magazzino_lista
this.Control[iCurrent+5]=dw_ricerca
this.Control[iCurrent+6]=dw_folder_search
end on

on w_crea_movimenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_mov_magazzino_det)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_mov_magazzino_lista)
destroy(this.dw_ricerca)
destroy(this.dw_folder_search)
end on

type dw_mov_magazzino_det from uo_cs_xx_dw within w_crea_movimenti
int X=23
int Y=621
int Width=2515
int Height=1141
int TabOrder=2
string DataObject="d_mov_magazzino_det"
BorderStyle BorderStyle=StyleLowered!
end type

type cb_reset from commandbutton within w_crea_movimenti
event clicked pbm_bnclicked
int X=2103
int Y=481
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="Annulla Ric."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_crea_movimenti
event clicked pbm_bnclicked
int X=2103
int Y=381
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="Cerca"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_mov_magazzino_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_mov_magazzino_lista from uo_cs_xx_dw within w_crea_movimenti
event pcd_delete pbm_custom42
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
int X=161
int Y=61
int Width=2332
int Height=501
int TabOrder=20
string DataObject="d_mov_magazzino_lista"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_new;window_open_parm(w_crea_movimenti_det, 0, dw_mov_magazzino_lista)
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_ricerca from u_dw_search within w_crea_movimenti
int X=138
int Y=61
int Width=2355
int Height=521
int TabOrder=10
string DataObject="d_ext_filtro_mov_magazzino"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_folder_search from u_folder within w_crea_movimenti
int X=23
int Y=21
int Width=2515
int Height=581
int TabOrder=30
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "L."
      SetFocus(dw_mov_magazzino_lista)
   CASE "R."
      SetFocus(dw_folder_search)
END CHOOSE

end event

