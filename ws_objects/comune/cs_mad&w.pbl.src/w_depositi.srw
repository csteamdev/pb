﻿$PBExportHeader$w_depositi.srw
$PBExportComments$Finestra Gestione Depositi
forward
global type w_depositi from w_cs_xx_principale
end type
type dw_depositi_det from uo_cs_xx_dw within w_depositi
end type
type dw_folder from u_folder within w_depositi
end type
type dw_depositi_lista from uo_cs_xx_dw within w_depositi
end type
type dw_depositi_ricerca from u_dw_search within w_depositi
end type
end forward

global type w_depositi from w_cs_xx_principale
integer width = 2894
integer height = 2400
string title = "Gestione Depositi"
dw_depositi_det dw_depositi_det
dw_folder dw_folder
dw_depositi_lista dw_depositi_lista
dw_depositi_ricerca dw_depositi_ricerca
end type
global w_depositi w_depositi

type variables
private:
	string is_base_sql
end variables

forward prototypes
public function integer wf_mittente ()
public function integer wf_destinatari ()
end prototypes

public function integer wf_mittente ();string ls_cod_deposito
long ll_row

ll_row = dw_depositi_lista.getrow()

if ll_row>0 then
else
	return 0
end if

ls_cod_deposito = dw_depositi_lista.getitemstring(ll_row, "cod_deposito")

if g_str.isempty(ls_cod_deposito) then
	return 0
end if

window_open_parm(w_depositi_mittente, -1, dw_depositi_lista)

return 1
end function

public function integer wf_destinatari ();string ls_cod_deposito
long ll_row

ll_row = dw_depositi_lista.getrow()

if ll_row>0 then
else
	return 0
end if

ls_cod_deposito = dw_depositi_lista.getitemstring(ll_row, "cod_deposito")

if g_str.isempty(ls_cod_deposito) then
	return 0
end if

window_open_parm(w_depositi_destinatari, -1, dw_depositi_lista)

return 1
end function

event pc_setwindow;call super::pc_setwindow;dw_depositi_lista.set_dw_key("cod_azienda")
dw_depositi_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default + c_noretrieveonopen, &
                                 c_default)
dw_depositi_det.set_dw_options(sqlca, &
                               dw_depositi_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
										 								 
iuo_dw_main = dw_depositi_lista

// stefanop 07/07/2010: progetto 140 funzione 3
dw_depositi_det.object.b_ricerca_fornitore.enabled = false

windowobject lw_oggetti[]

lw_oggetti[1] = dw_depositi_lista
dw_folder.fu_assigntab(1, "L.", lw_oggetti[])

lw_oggetti[1] = dw_depositi_ricerca
dw_folder.fu_assigntab(2, "R.", lw_oggetti[])

dw_folder.fu_folderoptions(dw_folder.c_defaultheight, dw_folder.c_foldertableft)
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(2)

//dw_depositi_ricerca.insertrow(0)
is_base_sql = dw_depositi_lista.getsqlselect()
// ----

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_depositi_det, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_depositi.create
int iCurrent
call super::create
this.dw_depositi_det=create dw_depositi_det
this.dw_folder=create dw_folder
this.dw_depositi_lista=create dw_depositi_lista
this.dw_depositi_ricerca=create dw_depositi_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_depositi_det
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_depositi_lista
this.Control[iCurrent+4]=this.dw_depositi_ricerca
end on

on w_depositi.destroy
call super::destroy
destroy(this.dw_depositi_det)
destroy(this.dw_folder)
destroy(this.dw_depositi_lista)
destroy(this.dw_depositi_ricerca)
end on

event pc_view;call super::pc_view;// stefanop 07/07/2010: progetto 140 funzione 3
dw_depositi_det.object.b_ricerca_fornitore.enabled = false
end event

event pc_modify;call super::pc_modify;// stefanop 07/07/2010: progetto 140 funzione 3
dw_depositi_det.object.b_ricerca_fornitore.enabled = true
end event

event pc_new;call super::pc_new;// stefanop 07/07/2010: progetto 140 funzione 3
dw_depositi_det.object.b_ricerca_fornitore.enabled = true
end event

event pc_save;call super::pc_save;// stefanop 07/07/2010: progetto 140 funzione 3
dw_depositi_det.object.b_ricerca_fornitore.enabled = false
end event

type dw_depositi_det from uo_cs_xx_dw within w_depositi
integer x = 23
integer y = 1236
integer width = 2811
integer height = 1048
integer taborder = 20
string dataobject = "d_depositi_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_depositi_det,"cod_fornitore")
		
	case "b_mittente"
		wf_mittente()
		
	case "b_destinatari"
		wf_destinatari()
		
end choose
end event

event pcd_delete;call super::pcd_delete;object.b_mittente.enabled = false
object.b_destinatari.enabled = false
end event

event pcd_modify;call super::pcd_modify;object.b_mittente.enabled = false
object.b_destinatari.enabled = false
end event

event pcd_new;call super::pcd_new;object.b_mittente.enabled = false
object.b_destinatari.enabled = false
end event

event pcd_view;call super::pcd_view;object.b_mittente.enabled = true
object.b_destinatari.enabled = true
end event

type dw_folder from u_folder within w_depositi
integer x = 23
integer y = 20
integer width = 2811
integer height = 1200
integer taborder = 10
end type

type dw_depositi_lista from uo_cs_xx_dw within w_depositi
integer x = 146
integer y = 48
integer width = 2661
integer height = 1144
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_depositi_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string						ls_cod_fornitore, ls_sql, ls_error, ls_cod_deposito, ls_des_deposito, ls_flag_tipo_deposito
long						ll_errore

// stefanop 07/07/2010: filtro dati, progetto 140 funzione 3
ls_sql = " WHERE cod_azienda=:rs_cod_azienda "

dw_depositi_ricerca.accepttext()

ls_cod_fornitore = dw_depositi_ricerca.getitemstring(1, "cod_fornitore")
if g_str.isnotempty(ls_cod_fornitore) then
	ls_sql += " AND cod_fornitore=~~'" + ls_cod_fornitore + "~~' "
end if

ls_cod_deposito = dw_depositi_ricerca.getitemstring(1, "cod_deposito")
if g_str.isnotempty(ls_cod_deposito) then
	ls_sql += " AND cod_deposito=~~'" + ls_cod_deposito + "~~' "
end if

ls_des_deposito = dw_depositi_ricerca.getitemstring(1, "des_deposito")
if g_str.isnotempty(ls_des_deposito) then
	ls_sql += " AND des_deposito LIKE ~~'%" + ls_des_deposito + "%~~' "
end if

ls_flag_tipo_deposito = dw_depositi_ricerca.getitemstring(1, "flag_tipo_deposito")
if g_str.isnotempty(ls_flag_tipo_deposito) then
	if ls_flag_tipo_deposito <> "X" then
		ls_sql += " AND flag_tipo_deposito=~~'" + ls_flag_tipo_deposito + "~~' "
	end if
end if


// ----

ls_sql = left(is_base_sql, pos(is_base_sql, "WHERE") - 1) + ls_sql
ls_sql += mid(is_base_sql, pos(is_base_sql, "ORDER"))

ls_error = this.modify("DataWindow.Table.Select='" + ls_sql + "'")
if ls_error <> "" then
	messagebox("APICE", ls_error)
	reset()
	return -1
end if

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event updatestart;call super::updatestart;integer li_i
string ls_cod_documento, ls_numeratore_documento


for li_i = 1 to rowcount(this)
	ls_cod_documento = this.getitemstring(li_i, "cod_documento")
	ls_numeratore_documento = this.getitemstring(li_i, "numeratore_documento")

	if not f_numeratore(ls_numeratore_documento, ls_cod_documento) then
		g_mb.messagebox("Attenzione", "Numeratore non corretto.", exclamation!)
		return 1
	end if
next

end event

type dw_depositi_ricerca from u_dw_search within w_depositi
event ue_key pbm_dwnkey
integer x = 146
integer y = 48
integer width = 2327
integer height = 568
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_depositi_ricerca"
boolean border = false
end type

event ue_key;if key = KeyEnter! then
	dw_folder.fu_SelectTab(1)
	dw_depositi_lista.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if

end event

event buttonclicked;call super::buttonclicked;string					ls_null

setnull(ls_null)


choose case dwo.name
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_depositi_ricerca,"cod_fornitore")
		
		
	case "b_ricerca_deposito"
		guo_ricerca.uof_ricerca_deposito(dw_depositi_ricerca,"cod_deposito")
	
	
	case "b_cerca"
		dw_folder.fu_SelectTab(1)
		dw_depositi_lista.change_dw_current()
		parent.triggerevent("pc_retrieve")
		
		
	case "cb_reset"
		setitem(1, "cod_fornitore", ls_null)
		setitem(1, "cod_deposito", ls_null)
		setitem(1, "des_deposito", "")
		setitem(1, "flag_tipo_deposito", "X")
	
end choose
end event

