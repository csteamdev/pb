﻿$PBExportHeader$w_depositi_destinatari.srw
forward
global type w_depositi_destinatari from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_depositi_destinatari
end type
end forward

global type w_depositi_destinatari from w_cs_xx_principale
integer width = 2313
integer height = 1272
string title = "Mittente Deposito"
dw_lista dw_lista
end type
global w_depositi_destinatari w_depositi_destinatari

on w_depositi_destinatari.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_depositi_destinatari.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_deposito")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)

iuo_dw_main = dw_lista
end event

type dw_lista from uo_cs_xx_dw within w_depositi_destinatari
integer x = 27
integer y = 36
integer width = 2213
integer height = 1104
integer taborder = 10
string dataobject = "d_depositi_destinatari"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_deposito, ls_des_deposito
long ll_errore

ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")


select des_deposito
into   :ls_des_deposito
from   anag_depositi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :ls_cod_deposito;
		 
if sqlca.sqlcode = 0 then
	parent.title = "Destinatari per Deposito " + ls_cod_deposito + " - " + ls_des_deposito
end if
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_deposito)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_deposito

ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) or getitemstring(ll_i, "cod_azienda")="" then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	if isnull(getitemstring(ll_i, "cod_deposito")) or getitemstring(ll_i, "cod_deposito") = "" then
		setitem(ll_i, "cod_deposito", ls_cod_deposito)
	end if
next

end event

