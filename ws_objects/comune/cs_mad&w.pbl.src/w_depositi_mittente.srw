﻿$PBExportHeader$w_depositi_mittente.srw
forward
global type w_depositi_mittente from w_cs_xx_principale
end type
type st_nota from statictext within w_depositi_mittente
end type
type dw_lista from uo_cs_xx_dw within w_depositi_mittente
end type
end forward

global type w_depositi_mittente from w_cs_xx_principale
integer width = 3456
integer height = 564
string title = "Mittente Deposito"
st_nota st_nota
dw_lista dw_lista
end type
global w_depositi_mittente w_depositi_mittente

on w_depositi_mittente.create
int iCurrent
call super::create
this.st_nota=create st_nota
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_nota
this.Control[iCurrent+2]=this.dw_lista
end on

on w_depositi_mittente.destroy
call super::destroy
destroy(this.st_nota)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_deposito")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)

iuo_dw_main = dw_lista
end event

type st_nota from statictext within w_depositi_mittente
integer x = 50
integer y = 20
integer width = 3337
integer height = 64
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
string text = "INSERIRE UN SOLO MITTENTE PER OGNI DEPOSITO"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_lista from uo_cs_xx_dw within w_depositi_mittente
integer x = 27
integer y = 124
integer width = 3355
integer height = 304
integer taborder = 10
string dataobject = "d_depositi_mittente"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_deposito, ls_des_deposito
long ll_errore

ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")


select des_deposito
into   :ls_des_deposito
from   anag_depositi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :ls_cod_deposito;
		 
if sqlca.sqlcode = 0 then
	parent.title = "Mittente per Deposito " + ls_cod_deposito + " - " + ls_des_deposito
end if
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_deposito)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;//OCIO script ancestor sovrascritto .....

integer li_count
string ls_cod_deposito

ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")

select count(*)
into   :li_count
from   anag_depositi_mittenti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :ls_cod_deposito;
		 
if li_count>0 then
	g_mb.error("Non è possibile inserire più di un mittente per questo deposito!")
	
	return
end if

//------------------------------------
//da qui in poi l'ancestor
super::event pcd_new(wparam, lparam)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_deposito

ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) or getitemstring(ll_i, "cod_azienda")="" then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	if isnull(getitemstring(ll_i, "cod_deposito")) or getitemstring(ll_i, "cod_deposito") = "" then
		setitem(ll_i, "cod_deposito", ls_cod_deposito)
	end if
next

end event

