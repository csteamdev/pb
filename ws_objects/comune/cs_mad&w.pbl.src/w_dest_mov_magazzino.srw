﻿$PBExportHeader$w_dest_mov_magazzino.srw
$PBExportComments$Finestra RESPONSE Modifica Destinazione Movimenti Magazzino
forward
global type w_dest_mov_magazzino from w_cs_xx_risposta
end type
type cb_elimina from commandbutton within w_dest_mov_magazzino
end type
type cb_nuovo from commandbutton within w_dest_mov_magazzino
end type
type st_1 from statictext within w_dest_mov_magazzino
end type
type dw_dest_mov_magazzino from uo_cs_xx_dw within w_dest_mov_magazzino
end type
type cb_avanti from commandbutton within w_dest_mov_magazzino
end type
type cb_annulla from commandbutton within w_dest_mov_magazzino
end type
type cb_ric_stock from cb_stock_ricerca within w_dest_mov_magazzino
end type
end forward

global type w_dest_mov_magazzino from w_cs_xx_risposta
integer width = 2958
integer height = 1264
string title = "Destinazione Movimenti Magazzino"
cb_elimina cb_elimina
cb_nuovo cb_nuovo
st_1 st_1
dw_dest_mov_magazzino dw_dest_mov_magazzino
cb_avanti cb_avanti
cb_annulla cb_annulla
cb_ric_stock cb_ric_stock
end type
global w_dest_mov_magazzino w_dest_mov_magazzino

type variables
string	is_cod_prodotto
end variables

forward prototypes
public function integer wf_nuovo_stock ()
public function integer wf_elimina_stock (long fl_anno, long fl_numero, long fl_progressivo)
end prototypes

public function integer wf_nuovo_stock ();long ll_prog

select max(prog_registrazione)
into :ll_prog
from dest_mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
	num_registrazione = :s_cs_xx.parametri.parametro_d_2;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in calcolo max progressivo Stock! " + sqlca.sqlerrtext ,Exclamation!)
	return -1
end if

if isnull(ll_prog) then ll_prog = 0
ll_prog += 1

insert into dest_mov_magazzino
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_registrazione)
values (	:s_cs_xx.cod_azienda,
			:s_cs_xx.parametri.parametro_d_1,
			:s_cs_xx.parametri.parametro_d_2,
			:ll_prog);
			
if sqlca.sqlcode = 0 then
	commit;
	
	dw_dest_mov_magazzino.retrieve(	s_cs_xx.cod_azienda, &
						 								s_cs_xx.parametri.parametro_d_1, &
					 									s_cs_xx.parametri.parametro_d_2 )
											 
	if dw_dest_mov_magazzino.rowcount() = 1 then
		cb_ric_stock.postevent(clicked!)
	end if
else	
	g_mb.messagebox("APICE","Errore in inserimento Stock! " + sqlca.sqlerrtext ,Exclamation!)
	rollback;
	return -1
end if

return 1
end function

public function integer wf_elimina_stock (long fl_anno, long fl_numero, long fl_progressivo);
delete from dest_mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno and num_registrazione = :fl_numero and
		prog_registrazione = :fl_progressivo;
			
if sqlca.sqlcode = 0 then
	commit;
	
	dw_dest_mov_magazzino.retrieve(	s_cs_xx.cod_azienda, &
						 								s_cs_xx.parametri.parametro_d_1, &
					 									s_cs_xx.parametri.parametro_d_2 )
else	
	g_mb.messagebox("APICE","Errore in cancellazione Stock! " + sqlca.sqlerrtext, Exclamation!)
	rollback;
	return -1
end if

return 1
end function

event pc_setwindow;call super::pc_setwindow;is_cod_prodotto = s_cs_xx.parametri.parametro_s_10

dw_dest_mov_magazzino.set_dw_options(sqlca, &
													 pcca.null_object, &
													 c_nonew + &
													 c_nodelete + &
													 c_modifyonopen, &
													 c_default)
												 

this.title = g_str.format("Destinazione Movimenti Magazzino Prodotto $1", is_cod_prodotto)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_dest_mov_magazzino,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
// stefano 09/04/2010
f_PO_LoadDDDW_DW(dw_dest_mov_magazzino,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento", &
                 "")
// ----

end event

on w_dest_mov_magazzino.create
int iCurrent
call super::create
this.cb_elimina=create cb_elimina
this.cb_nuovo=create cb_nuovo
this.st_1=create st_1
this.dw_dest_mov_magazzino=create dw_dest_mov_magazzino
this.cb_avanti=create cb_avanti
this.cb_annulla=create cb_annulla
this.cb_ric_stock=create cb_ric_stock
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elimina
this.Control[iCurrent+2]=this.cb_nuovo
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_dest_mov_magazzino
this.Control[iCurrent+5]=this.cb_avanti
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.cb_ric_stock
end on

on w_dest_mov_magazzino.destroy
call super::destroy
destroy(this.cb_elimina)
destroy(this.cb_nuovo)
destroy(this.st_1)
destroy(this.dw_dest_mov_magazzino)
destroy(this.cb_avanti)
destroy(this.cb_annulla)
destroy(this.cb_ric_stock)
end on

type cb_elimina from commandbutton within w_dest_mov_magazzino
integer x = 1554
integer y = 1040
integer width = 183
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "-"
end type

event clicked;long ll_row, ll_anno, ll_numero, ll_progressivo

ll_row = dw_dest_mov_magazzino.getrow()

if ll_row > 0 then
	ll_anno = dw_dest_mov_magazzino.getitemnumber(ll_row, "anno_registrazione")
	ll_numero = dw_dest_mov_magazzino.getitemnumber(ll_row, "num_registrazione")
	ll_progressivo = dw_dest_mov_magazzino.getitemnumber(ll_row, "prog_registrazione")
	
	if g_mb.messagebox("APICE", "Sei sicuro di voler eliminare lo stock selezionato?", Question!, YesNo!, 2) = 1 then
		wf_elimina_stock(ll_anno, ll_numero, ll_progressivo)
	end if
else
	g_mb.messagebox("APICE", "E' necessario selezionare una riga da eliminare?", Exclamation!)
end if
end event

type cb_nuovo from commandbutton within w_dest_mov_magazzino
integer x = 1349
integer y = 1040
integer width = 183
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "+"
end type

event clicked;if g_mb.messagebox("APICE", "Sei sicuro di voler inserire un nuovo stock?", Question!, YesNo!, 2) = 1 then
	wf_nuovo_stock()
end if
end event

type st_1 from statictext within w_dest_mov_magazzino
integer x = 23
integer y = 1044
integer width = 1294
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "SHIFT - F1 per ricerca Clienti e Fornitori"
boolean focusrectangle = false
end type

type dw_dest_mov_magazzino from uo_cs_xx_dw within w_dest_mov_magazzino
integer y = 20
integer width = 2903
integer height = 1000
integer taborder = 20
string dataobject = "d_dest_mov_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long  l_error

l_Error = retrieve(s_cs_xx.cod_azienda, &
						 s_cs_xx.parametri.parametro_d_1, &
						 s_cs_xx.parametri.parametro_d_2 )

if l_error < 0 then
   pcca.error = c_fatal
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_cliente, ls_cod_fornitore, &
			 ls_flag_carico ,ls_cod_tipo_mov_det, ls_flag_cliente, ls_flag_fornitore, ls_flag_terzista
	datetime ldt_data_stock
	long   ll_i, ll_max, ll_prog_stock
	
	dw_dest_mov_magazzino.accepttext()
	for ll_i = 1 to rowcount()
		ls_cod_deposito = getitemstring(ll_i, "cod_deposito")
		ls_cod_ubicazione = getitemstring(ll_i, "cod_ubicazione")
		ls_cod_lotto = getitemstring(ll_i, "cod_lotto")
		ldt_data_stock = getitemdatetime(ll_i,"data_stock")
		ll_prog_stock = getitemnumber(ll_i,"prog_stock")
		ls_cod_cliente = getitemstring(ll_i, "cod_cliente")
		ls_cod_fornitore = getitemstring(ll_i, "cod_fornitore")
		ls_flag_carico = getitemstring(ll_i, "flag_carico")
		ls_cod_tipo_mov_det = getitemstring(ll_i, "cod_tipo_mov_det")
		ls_flag_cliente = getitemstring(ll_i, "flag_cliente")
		ls_flag_fornitore = getitemstring(ll_i, "flag_fornitore")
	
		if not isnull(ls_cod_fornitore) then
			select flag_terzista
			into   :ls_flag_terzista
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :ls_cod_fornitore;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Fornitore non trovato !!!" + sqlca.sqlerrtext)
				return 1
			end if
			if ls_flag_terzista = 'N' then
				g_mb.messagebox("APICE","Attenzione: questo fornitore non è un terzista !!! Potrebbero verificarsi potenziali errori nella gestione dell'inventario di magazzino!!",Information!)
			end if
		end if
//		if ls_flag_cliente = "S" and not isnull(ls_cod_cliente) and not isnull(ls_cod_ubicazione) and (ls_cod_ubicazione <> ls_cod_cliente) then
//			messagebox("APICE","Quando è indicato un cliente, il codice ubicazione deve corrispondere al codice del cliente", StopSign!)
//			return
//		end if
//		if ls_flag_fornitore = "S" and not isnull(ls_cod_fornitore) and not isnull(ls_cod_ubicazione) and (ls_cod_ubicazione <> ls_cod_fornitore) then
//			messagebox("APICE","Quando è indicato un fornitore, il codice ubicazione deve corrispondere al codice del fornitore", StopSign!)
//			return
//		end if
	
		if isnull(ls_cod_deposito) or &
			isnull(ls_cod_ubicazione) or len(ls_cod_ubicazione) < 1 or &
			isnull(ls_cod_lotto) or len(ls_cod_lotto) < 1 or &
			isnull(ldt_data_stock) or ldt_data_stock <= datetime(date("01/01/1900")) or &
			isnull(ll_prog_stock) or ll_prog_stock < 1 or &
			(ls_flag_cliente = "S" and isnull(ls_cod_cliente)) or &
			(ls_flag_fornitore = "S" and isnull(ls_cod_fornitore)) then
				g_mb.messagebox("APICE","Dati destinazione stock incompleti",Information!)
				return 1
		end if
	next
end if
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "cod_fornitore"
		if key = keyF1!  and keyflags = 1 then
			dw_dest_mov_magazzino.change_dw_current()
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_fornitore(dw_dest_mov_magazzino,"cod_fornitore")
		end if
	case "cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			dw_dest_mov_magazzino.change_dw_current()
			guo_ricerca.uof_ricerca_cliente(dw_dest_mov_magazzino,"cod_cliente")
		end if
end choose
end event

type cb_avanti from commandbutton within w_dest_mov_magazzino
integer x = 2537
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Avanti"
end type

event clicked;parent.triggerevent("pc_save")
parent.postevent("pc_close")

end event

type cb_annulla from commandbutton within w_dest_mov_magazzino
integer x = 2149
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nnulla"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "ANNULLA"
save_on_close(c_socnosave)
parent.postevent("pc_close")
end event

type cb_ric_stock from cb_stock_ricerca within w_dest_mov_magazzino
integer x = 1760
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 2
string text = "Ric.Stock"
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

s_cs_xx.parametri.parametro_s_11 = dw_dest_mov_magazzino.getitemstring(dw_dest_mov_magazzino.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_dest_mov_magazzino.getitemstring(dw_dest_mov_magazzino.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_dest_mov_magazzino.getitemstring(dw_dest_mov_magazzino.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0


dw_dest_mov_magazzino.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
setnull(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)
setnull(s_cs_xx.parametri.parametro_s_9)

window_open(w_ricerca_stock, 0)
end event

