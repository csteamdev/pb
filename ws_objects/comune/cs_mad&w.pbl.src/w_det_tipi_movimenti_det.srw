﻿$PBExportHeader$w_det_tipi_movimenti_det.srw
$PBExportComments$Finestra Dettagli Configurazione Movimenti Magazzino
forward
global type w_det_tipi_movimenti_det from w_cs_xx_principale
end type
type dw_det_tipi_movimenti_lista from uo_cs_xx_dw within w_det_tipi_movimenti_det
end type
type dw_det_tipi_movimenti_det from uo_cs_xx_dw within w_det_tipi_movimenti_det
end type
end forward

global type w_det_tipi_movimenti_det from w_cs_xx_principale
integer width = 2597
integer height = 1564
string title = "Configurazione Movimenti"
dw_det_tipi_movimenti_lista dw_det_tipi_movimenti_lista
dw_det_tipi_movimenti_det dw_det_tipi_movimenti_det
end type
global w_det_tipi_movimenti_det w_det_tipi_movimenti_det

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_det_tipi_movimenti_lista.set_dw_key("cod_azienda")
dw_det_tipi_movimenti_lista.set_dw_key("cod_tipo_movimento")

dw_det_tipi_movimenti_lista.set_dw_options(sqlca, &
                                  i_openparm, &
                                  c_default, &
                                  c_default)

dw_det_tipi_movimenti_det.set_dw_options(sqlca, &
                                  dw_det_tipi_movimenti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)

iuo_dw_main = dw_det_tipi_movimenti_lista

end event

on w_det_tipi_movimenti_det.create
int iCurrent
call super::create
this.dw_det_tipi_movimenti_lista=create dw_det_tipi_movimenti_lista
this.dw_det_tipi_movimenti_det=create dw_det_tipi_movimenti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_tipi_movimenti_lista
this.Control[iCurrent+2]=this.dw_det_tipi_movimenti_det
end on

on w_det_tipi_movimenti_det.destroy
call super::destroy
destroy(this.dw_det_tipi_movimenti_lista)
destroy(this.dw_det_tipi_movimenti_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_tipi_movimenti_det,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_tipi_movimenti_det,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_tipi_movimenti_det,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_tipi_movimenti_det,"cod_fornitore",sqlca,&
                 "anag_fornitori","cod_fornitore","rag_soc_1",&
                 "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "(anag_fornitori.flag_terzista = 'S') and " + &
					  "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_PO_LoadDDDW_DW(dw_det_tipi_movimenti_det,"cod_cliente",sqlca,&
                 "anag_clienti","cod_cliente","rag_soc_1",&
                 "(anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_clienti.flag_blocco <> 'S') or (anag_clienti.flag_blocco = 'S' and anag_clienti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  

end event

type dw_det_tipi_movimenti_lista from uo_cs_xx_dw within w_det_tipi_movimenti_det
integer x = 23
integer y = 20
integer width = 2514
integer height = 500
integer taborder = 20
string dataobject = "d_det_tipi_movimenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_movimento
long ll_i


ls_cod_tipo_movimento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_tipo_movimento")
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_tipo_movimento")) then
      this.setitem(ll_i, "cod_tipo_movimento", ls_cod_tipo_movimento)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_movimento
long ll_errore


ls_cod_tipo_movimento = i_parentdw.object.cod_tipo_movimento[i_parentdw.i_selectedrows[1]]
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_movimento)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;string ls_cod_tipo_movimento

ls_cod_tipo_movimento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_tipo_movimento")
dw_det_tipi_movimenti_lista.setitem(this.getrow(),"cod_tipo_movimento",ls_cod_tipo_movimento)

end event

type dw_det_tipi_movimenti_det from uo_cs_xx_dw within w_det_tipi_movimenti_det
integer x = 23
integer y = 540
integer width = 2514
integer height = 900
integer taborder = 10
string dataobject = "d_det_tipi_movimenti_det"
borderstyle borderstyle = styleraised!
end type

