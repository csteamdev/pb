﻿$PBExportHeader$w_duplica_prodotto.srw
$PBExportComments$Window che permette la duplicazione di un prodotto
forward
global type w_duplica_prodotto from w_cs_xx_risposta
end type
type cb_duplica_txt from commandbutton within w_duplica_prodotto
end type
type cbx_depositi from checkbox within w_duplica_prodotto
end type
type cbx_centri_costo from checkbox within w_duplica_prodotto
end type
type cb_duplica from commandbutton within w_duplica_prodotto
end type
type dw_detail from uo_std_dw within w_duplica_prodotto
end type
type dw_appoggio from datawindow within w_duplica_prodotto
end type
type dw_ricerca from u_dw_search within w_duplica_prodotto
end type
type dw_prodotti_lista from uo_cs_xx_dw within w_duplica_prodotto
end type
type dw_folder_search from u_folder within w_duplica_prodotto
end type
end forward

global type w_duplica_prodotto from w_cs_xx_risposta
integer width = 2720
integer height = 2232
string title = "Duplicazione prodotto"
event we_init_dw ( )
cb_duplica_txt cb_duplica_txt
cbx_depositi cbx_depositi
cbx_centri_costo cbx_centri_costo
cb_duplica cb_duplica
dw_detail dw_detail
dw_appoggio dw_appoggio
dw_ricerca dw_ricerca
dw_prodotti_lista dw_prodotti_lista
dw_folder_search dw_folder_search
end type
global w_duplica_prodotto w_duplica_prodotto

type variables
private:
	string is_cod_prodotto
	string is_column_disabled[]
	string is_sql_select
	
	string is_checked_image_path
	string is_unchecked_image_path
	
	uo_array iuo_label
end variables

forward prototypes
public subroutine wf_ricerca ()
public subroutine wf_build_dw ()
public subroutine wf_togle_checked ()
public subroutine wf_init_column_disabled ()
public function boolean wf_in_array (string as_find_value, ref string as_array[])
public function boolean wf_duplica ()
public subroutine wf_replace (ref string as_stringa, string as_trova, string as_cambia)
public subroutine wf_init_label ()
public function integer wf_duplica_txt ()
end prototypes

event we_init_dw();wf_init_column_disabled()
wf_init_label()
wf_build_dw()
end event

public subroutine wf_ricerca ();string ls_select, ls_orderby, ls_where
date ldt_data_da, ldt_data_a
long ll_pos

dw_ricerca.accepttext()

ll_pos = pos(is_sql_select, "WHERE")
ls_select = left(is_sql_select, ll_pos + 5)
ls_orderby = right(is_sql_select, len(is_sql_select) - pos(is_sql_select, "ORDER BY") + 1)

ls_where = " cod_azienda=:rs_cod_azienda "

// stefanop: 16/12/2011: Aggiunta modalità like, richiesta da Beatrice
if dw_ricerca.getitemstring(1, "flag_like") = "N" then
	
	if not isnull(dw_ricerca.getitemstring(1, "cod_prodotto_da")) then
		ls_where += " AND cod_prodotto >= '" + dw_ricerca.getitemstring(1, "cod_prodotto_da") + "' "
	end if
	
	if not isnull(dw_ricerca.getitemstring(1, "cod_prodotto_a")) then
		ls_where += " AND cod_prodotto <= '" + dw_ricerca.getitemstring(1, "cod_prodotto_a") + "' "
	end if
	
else
	
	if not isnull(dw_ricerca.getitemstring(1, "cod_prodotto_da")) then
		ls_where += " AND cod_prodotto LIKE '" + dw_ricerca.getitemstring(1, "cod_prodotto_da") + "' "
	end if
	
end if

if not isnull(dw_ricerca.getitemdate(1, "data_creazione_da")) then
	ls_where += " AND data_creazione <= '" + string(dw_ricerca.getitemdate(1, "data_creazione_da"), s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(dw_ricerca.getitemdate(1, "data_creazione_a")) then
	ls_where += " AND data_creazione >= '" +string(dw_ricerca.getitemdate(1, "data_creazione_a"), s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(dw_ricerca.getitemstring(1, "flag_blocco")) and dw_ricerca.getitemstring(1, "flag_blocco") <> "T" then
	ls_where += " AND flag_blocco = '" + dw_ricerca.getitemstring(1, "flag_blocco") + "' "
end if

wf_replace(ls_where, "'", "~~'")
ls_select += ls_where + ls_orderby

dw_prodotti_lista.Modify("DataWindow.Table.Select='" + ls_select + "'")

dw_folder_search.fu_selectTab(1)
dw_prodotti_lista.change_dw_current()
dw_prodotti_lista.triggerevent("pcd_retrieve")

end subroutine

public subroutine wf_build_dw ();/**
 * Creo in maniera dinamica la dw
 **/
string ls_sql, ls_dwsyntax, ls_errors, ls_presentation, ls_value, ls_column_text, ls_column_name, ls_column_type
int li_i, ll_row
datetime ldt_date


ls_sql = "SELECT * from anag_prodotti where cod_azienda='" + s_cs_xx.cod_azienda  +"' and cod_prodotto='" + is_cod_prodotto + "'"			 
ls_dwsyntax = SQLCA.SyntaxFromSQL(ls_sql, "Style(Type=grid)", ls_errors)

if len(ls_errors) > 0 then
	g_mb.error(ls_errors)
	return
end if

dw_appoggio.create(ls_dwsyntax, ls_errors)
dw_appoggio.SetTransObject( SQLCA )
dw_appoggio.Retrieve()

// Scorro le colonne per prelevare il valore
for li_i = 1 to long(dw_appoggio.Object.DataWindow.Column.Count)
	ls_column_name = dw_appoggio.describe("#"+string(li_i)+".name")
	ls_column_type = lower(dw_appoggio.describe("#"+string(li_i)+".coltype"))
	ls_column_text = dw_appoggio.describe(ls_column_name + "_t.text")
	setnull(ls_value)
		
	choose case left(ls_column_type, 4)
		case "date", "time"
			ldt_date = dw_appoggio.getitemdatetime(1, li_i)
			if not isnull(ldt_date) then ls_value = string(ldt_date, "dd/mm/yyyy")
			
		case "numb", "long", "deci"
			ls_value = string(dw_appoggio.getitemnumber(1, li_i))
			
		case else
			ls_value =dw_appoggio.getitemstring(1, li_i)
	end choose

	ll_row = dw_detail.insertrow(0)
	
	// custom label
	if iuo_label.haskey(ls_column_name) then ls_column_text = iuo_label.get(ls_column_name)
	
	dw_detail.setitem(ll_row, "column_name", ls_column_name)
	dw_detail.setitem(ll_row, "column_text", ls_column_text)
	dw_detail.setitem(ll_row, "column_type", ls_column_type)
	dw_detail.setitem(ll_row, "column_value", ls_value)
	
	if wf_in_array(ls_column_name, is_column_disabled) then
		dw_detail.setitem(ll_row, "flag_abilitato", "N")
	else
		dw_detail.setitem(ll_row, "flag_abilitato", "S")
	end if
next

// immagine selezionato nella dw
dw_detail.object.p_select.filename = is_checked_image_path
end subroutine

public subroutine wf_togle_checked ();/**
 * Toglie o mette la spunta su tutti i campi
 **/
 
string ls_flag_selezionato
int li_i

if dw_detail.object.p_select.filename = is_checked_image_path then
	 dw_detail.object.p_select.filename = is_unchecked_image_path
	 ls_flag_selezionato = "N"
else
	 dw_detail.object.p_select.filename = is_checked_image_path
	 ls_flag_selezionato = "S"
end if

for li_i = 1 to dw_detail.rowcount()
	
	if dw_detail.getitemstring(li_i, "flag_abilitato") = "S" then
		dw_detail.setitem(li_i, "flag_selezionato", ls_flag_selezionato)
	end if
	
next
end subroutine

public subroutine wf_init_column_disabled ();/**
 * Inizializza l'array delle colonne disabilitate
 * Al momento sono fisse ma nulla vieta di parametrizzare o 
 * caricare i valori da database
 **/
 
is_column_disabled[] = { &
	"cod_azienda", &
	"cod_prodotto", &
	"des_prodotto", &
	"data_creazione", &
	"cod_cat_mer", &
	"cod_misura_mag", &
	"cod_misura_ven", &
	"fat_conversione_ven", &
	"cod_misura_acq", &
	"fat_conversione_acq", &
	"cod_livello_prod_1" &
	}
end subroutine

public function boolean wf_in_array (string as_find_value, ref string as_array[]);/**
 * Cerca il valore all'interno dell'array
 *
 * Ritorna: TRUE se trovato e FALSE se non trovato
 **/
 
int li_i

for li_i = 1 to upperbound(as_array)
	
	if as_array[li_i] = as_find_value then return true
	
next

return false
end function

public function boolean wf_duplica ();/**
 * Duplica le informazioni del prodotto di partenza a N prodotti di destinazione
 **/
 
string ls_sql = "", ls_sql_update, ls_cod_prodotti[], ls_error, ls_cod_prodotto_dupl, ls_dec, ls_value
long ll_i
boolean lb_update = false

uo_prodotti luo_prodotti
dw_detail.accepttext()

// creo sql di update
ls_sql = "UPDATE anag_prodotti SET "
for ll_i = 1 to dw_detail.rowcount()
		
	if dw_detail.getitemstring(ll_i, "flag_abilitato") = "S" and dw_detail.getitemstring(ll_i, "flag_selezionato") = "S" then
		
		setnull(ls_value)
				
		choose case left(dw_detail.getitemstring(ll_i, "column_type"), 4)
			case "date", "time"
				ls_value = dw_detail.getitemstring(ll_i, "column_value")
				if not isnull(ls_value) then
					ls_value = "'" + string(date(ls_value), s_cs_xx.db_funzioni.formato_data) + "'"
				end if			
				
			case "numb", "deci", "long"
				ls_value = dw_detail.getitemstring(ll_i, "column_value")
				if not isnull(ls_value) and ls_value <> "" then f_sostituzione(ls_value, ",", ".")
				
			case else
				ls_value = "'" + dw_detail.getitemstring(ll_i, "column_value") + "'"
		end choose
		
		if not isnull(ls_value) and ls_value <> "" then
			if lb_update then ls_sql += ", "
			lb_update = true
			
			ls_sql += dw_detail.getitemstring(ll_i, "column_name") + "=" + ls_value
		end if
		
	end if
	
next
ls_sql += " WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' "

if not lb_update then 
	g_mb.show("Nessuna colonna selezionata per la duplicazione.~r~nSelezionare almeno una colonna e riprovare.")
	return false
end if
// ----

// aggiorno tutti i prodotti selezionati
if dw_prodotti_lista.rowcount() < 1 then
	g_mb.show("Nessun prodotto selezionato dove effettuare la duplicazione dei valori.")
	return false
end if

for ll_i = 1 to dw_prodotti_lista.rowcount()
	ls_cod_prodotto_dupl = dw_prodotti_lista.getitemstring(ll_i, "cod_prodotto")
	
	ls_cod_prodotti[upperbound(ls_cod_prodotti) + 1] = ls_cod_prodotto_dupl
	
	ls_sql_update = ls_sql + " AND cod_prodotto='" + ls_cod_prodotto_dupl + "'"
	
	execute immediate :ls_sql_update;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante la duplicazione delle informazioni nel prodotto " + ls_cod_prodotto_dupl + " -" + dw_prodotti_lista.getitemstring(ll_i, "des_prodotto"), sqlca)
		return false
	end if
next

luo_prodotti = create uo_prodotti

// copio centri di costo?
if cbx_centri_costo.checked then
	if not luo_prodotti.uof_duplica_centri_costo(is_cod_prodotto, ls_cod_prodotti, ls_error) then
		g_mb.error(ls_error)
	end if
end if
// ----

// copio depositi?
if cbx_depositi.checked then
	if not luo_prodotti.uof_duplica_depositi(is_cod_prodotto, ls_cod_prodotti, ls_error) then
		g_mb.error(ls_error)
	end if
end if
// ----

destroy luo_prodotti

return true
end function

public subroutine wf_replace (ref string as_stringa, string as_trova, string as_cambia);long ll_pos = 1

do while true
	
	ll_pos = pos(as_stringa, as_trova, ll_pos)
	
	if ll_pos < 1 then return
	
	as_stringa = replace(as_stringa, ll_pos, len(as_trova), as_cambia)
	ll_pos += len(as_cambia)
	
loop
end subroutine

public subroutine wf_init_label ();/**
 * Stefanop
 * 02/11/2011
 *
 * Imposto le label personalizzate per nome colonna
 **/
 
 
iuo_label.set("cod_misura_mag", "Codice misura magazzino")
iuo_label.set("cod_misura_vol", "Codice misura volume")
end subroutine

public function integer wf_duplica_txt ();/**
 * stefanop
 * 01/02/2012
 *
 * Aggiungo nuovi prodotti partendo da un file txt duplicando le informazioni del prodotto di partenza
 * La funzione era presente nella finestra w_duplica_prodotto della 11.5 che poi è stata persa.
 * La funzione è richiesta da beatrice.
 * 
 * Separatore = tabulatore
 * Formato del file
 * 		cod_prodotto_nuovo
 *		des_prodotto_nuovo
 **/
 
string ls_path, ls_filename, ls_desktop, ls_riga, ls_separator, ls_values[], ls_test, ls_error, ls_empty[]
long ll_handle, ll_result, ll_rows
boolean lb_complete
datetime ldt_data_blocco
uo_prodotti luo_prodotti

luo_prodotti = create uo_prodotti

ls_separator = "~t"
ls_desktop = guo_functions.uof_get_user_desktop_folder()
ldt_data_blocco = datetime(today(), 00:00:00)

if GetFileOpenName("Importazione Lista Prodotti da Duplicare", ls_path, ls_filename, "TXT", "File di testo (*.txt),*.txt", ls_desktop) < 1 then 
	return 1
end if

ll_handle = fileopen(ls_path, linemode!, read!)
if ll_handle < 1 then 
	g_mb.error("Non è possibile aprire il file. Provare in un secondo momento")
	return 1
end if

ll_rows = 0
lb_complete = false

do while true
	ll_result = fileread(ll_handle, ls_riga)
	
	if ll_result = -100 then
		lb_complete = true
	   exit
	elseif ll_result = -1 then
		g_mb.error("Errore durante la lettura del file di importazione")
	   exit
	end if
	ll_rows  ++
	
	ls_values = ls_empty
	pcca.mdi_frame.setmicrohelp("Lettura riga  " + string(ll_rows) + " in corso ...")	
	
	guo_functions.uof_explode(ls_riga,  "~t", ref ls_values[])
	
	if upperbound(ls_values) < 2 then
		g_mb.warning("Attenzione, nella riga " + string(ll_rows) + " non sembra essere specificato il codice del nuovo prodotto.~r~nRiga verrà SALTATA.")
		continue
	end if
	
	// controllo che il prodotto di origine sia valido
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_values[1];
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la verifica dell'esesitenza del codice prodotto origine della riga " + string(ll_rows), sqlca)
		exit
	elseif sqlca.sqlcode = 100 then
		g_mb.warning("Il codice prodotto di origine " + ls_values[1] + " NON è presente nel database.~r~nRiga verrà SALTATA.")
		continue
	end if
	// ----
		 
	// controllo che il prodotto non sia già stato inserito
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_values[2];
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la verifica dell'esesitenza del codice prodotto della riga " + string(ll_rows), sqlca)
		exit
	elseif sqlca.sqlcode = 0 then
		g_mb.warning("Il codice prodotto " + ls_values[2] + " è già presente nel database.~r~nRiga verrà SALTATA.")
		continue
	end if
	// ----
	
  	INSERT INTO anag_prodotti  
		( cod_azienda,   
		cod_prodotto,   
		cod_comodo,   
		cod_barre,   
		des_prodotto,   
		data_creazione,   
		cod_deposito,   
		cod_ubicazione,   
		flag_decimali,   
		cod_cat_mer,   
		cod_responsabile,   
		cod_misura_mag,   
		cod_misura_peso,   
		peso_lordo,   
		peso_netto,   
		cod_misura_vol,   
		volume,   
		pezzi_collo,   
		flag_classe_abc,   
		flag_sotto_scorta,   
		flag_lifo,   
		saldo_quan_anno_prec,   
		saldo_quan_inizio_anno,   
		val_inizio_anno,   
		saldo_quan_ultima_chiusura,   
		prog_quan_entrata,   
		val_quan_entrata,   
		prog_quan_uscita,   
		val_quan_uscita,   
		prog_quan_acquistata,   
		val_quan_acquistata,   
		prog_quan_venduta,   
		val_quan_venduta,   
		quan_ordinata,   
		quan_impegnata,   
		quan_assegnata,   
		quan_in_spedizione,   
		quan_anticipi,   
		costo_standard,   
		costo_ultimo,   
		lotto_economico,   
		livello_riordino,   
		scorta_minima,   
		scorta_massima,   
		indice_rotazione,   
		consumo_medio,   
		cod_prodotto_alt,   
		cod_misura_acq,   
		fat_conversione_acq,   
		cod_fornitore,   
		cod_gruppo_acq,   
		prezzo_acquisto,   
		sconto_acquisto,   
		quan_minima,   
		inc_ordine,   
		quan_massima,   
		tempo_app,   
		cod_misura_ven,   
		fat_conversione_ven,   
		cod_gruppo_ven,   
		cod_iva,   
		prezzo_vendita,   
		sconto,   
		provvigione,   
		flag_blocco,   
		data_blocco,   
		cod_livello_prod_1,   
		cod_livello_prod_2,   
		cod_livello_prod_3,   
		cod_livello_prod_4,   
		cod_livello_prod_5,   
		cod_nomenclatura,   
		cod_imballo,   
		flag_articolo_fiscale,   
		flag_cauzione,   
		cod_cauzione,   
		cod_tipo_politica_riordino,   
		cod_misura_lead_time,   
		lead_time,   
		cod_formula,   
		cod_reparto,   
		flag_materia_prima,   
		flag_escludibile,   
		cod_rifiuto,   
		flag_stato_rifiuto,   
		cod_comodo_2,   
		cod_tipo_costo_mp,   
		flag_modifica_disegno,   
		cod_prodotto_raggruppato,   
		cod_divisione,   
		cod_area_aziendale,   
		quan_in_produzione,   
		fat_conversione_rag_mag )  
	SELECT anag_prodotti.cod_azienda,   
		:ls_values[2],   
		anag_prodotti.cod_comodo,   
		anag_prodotti.cod_barre,   
		:ls_values[3],   
		:ldt_data_blocco,  // data creazione
		anag_prodotti.cod_deposito,   
		anag_prodotti.cod_ubicazione,   
		anag_prodotti.flag_decimali,   
		anag_prodotti.cod_cat_mer,   
		anag_prodotti.cod_responsabile,   
		anag_prodotti.cod_misura_mag,   
		anag_prodotti.cod_misura_peso,   
		anag_prodotti.peso_lordo,   
		anag_prodotti.peso_netto,   
		anag_prodotti.cod_misura_vol,   
		anag_prodotti.volume,   
		anag_prodotti.pezzi_collo,   
		anag_prodotti.flag_classe_abc,   
		anag_prodotti.flag_sotto_scorta,   
		anag_prodotti.flag_lifo,   
		anag_prodotti.saldo_quan_anno_prec,   
		anag_prodotti.saldo_quan_inizio_anno,   
		anag_prodotti.val_inizio_anno,   
		anag_prodotti.saldo_quan_ultima_chiusura,   
		anag_prodotti.prog_quan_entrata,   
		anag_prodotti.val_quan_entrata,   
		anag_prodotti.prog_quan_uscita,   
		anag_prodotti.val_quan_uscita,   
		anag_prodotti.prog_quan_acquistata,   
		anag_prodotti.val_quan_acquistata,   
		anag_prodotti.prog_quan_venduta,   
		anag_prodotti.val_quan_venduta,   
		anag_prodotti.quan_ordinata,   
		anag_prodotti.quan_impegnata,   
		anag_prodotti.quan_assegnata,   
		anag_prodotti.quan_in_spedizione,   
		anag_prodotti.quan_anticipi,   
		anag_prodotti.costo_standard,   
		anag_prodotti.costo_ultimo,   
		anag_prodotti.lotto_economico,   
		anag_prodotti.livello_riordino,   
		anag_prodotti.scorta_minima,   
		anag_prodotti.scorta_massima,   
		anag_prodotti.indice_rotazione,   
		anag_prodotti.consumo_medio,   
		anag_prodotti.cod_prodotto_alt,   
		anag_prodotti.cod_misura_acq,   
		anag_prodotti.fat_conversione_acq,   
		anag_prodotti.cod_fornitore,   
		anag_prodotti.cod_gruppo_acq,   
		anag_prodotti.prezzo_acquisto,   
		anag_prodotti.sconto_acquisto,   
		anag_prodotti.quan_minima,   
		anag_prodotti.inc_ordine,   
		anag_prodotti.quan_massima,   
		anag_prodotti.tempo_app,   
		anag_prodotti.cod_misura_ven,   
		anag_prodotti.fat_conversione_ven,   
		anag_prodotti.cod_gruppo_ven,   
		anag_prodotti.cod_iva,   
		anag_prodotti.prezzo_vendita,   
		anag_prodotti.sconto,   
		anag_prodotti.provvigione,   
		'S', // flag blocco
		:ldt_data_blocco, // data blocco
		anag_prodotti.cod_livello_prod_1,   
		anag_prodotti.cod_livello_prod_2,   
		anag_prodotti.cod_livello_prod_3,   
		anag_prodotti.cod_livello_prod_4,   
		anag_prodotti.cod_livello_prod_5,   
		anag_prodotti.cod_nomenclatura,   
		anag_prodotti.cod_imballo,   
		anag_prodotti.flag_articolo_fiscale,   
		anag_prodotti.flag_cauzione,   
		anag_prodotti.cod_cauzione,   
		anag_prodotti.cod_tipo_politica_riordino,   
		anag_prodotti.cod_misura_lead_time,   
		anag_prodotti.lead_time,   
		anag_prodotti.cod_formula,   
		anag_prodotti.cod_reparto,   
		anag_prodotti.flag_materia_prima,   
		anag_prodotti.flag_escludibile,   
		anag_prodotti.cod_rifiuto,   
		anag_prodotti.flag_stato_rifiuto,   
		anag_prodotti.cod_comodo_2,   
		anag_prodotti.cod_tipo_costo_mp,   
		anag_prodotti.flag_modifica_disegno,   
		anag_prodotti.cod_prodotto_raggruppato,   
		anag_prodotti.cod_divisione,   
		anag_prodotti.cod_area_aziendale,   
		anag_prodotti.quan_in_produzione,   
		anag_prodotti.fat_conversione_rag_mag  
		FROM anag_prodotti
		WHERE 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_values[1];

	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'inserimento del nuovo prodotto. Riga " + string(ll_rows), sqlca)
		exit
	end if
	
	// copio centri di costo?
	if cbx_centri_costo.checked then
		if not luo_prodotti.uof_duplica_centri_costo(ls_values[1], {ls_values[2]}, ls_error) then
			g_mb.error("Riga " + string(ll_rows), ls_error)
			exit
		end if
	end if
	// ----
	
	// copio depositi?
	if cbx_depositi.checked then
		if not luo_prodotti.uof_duplica_depositi(ls_values[1],  {ls_values[2]}, ls_error) then
			g_mb.error("Riga " + string(ll_rows), ls_error)
			exit
		end if
	end if
	// ----
	
loop

destroy luo_prodotti
fileclose(ll_handle)

if lb_complete then
	commit;
	g_mb.success("Dupliaczioni prodotti avvenuta con successo.~r~nLette " + string(ll_rows) + " righe.")
else
	rollback;
	return -1
end if
end function

on w_duplica_prodotto.create
int iCurrent
call super::create
this.cb_duplica_txt=create cb_duplica_txt
this.cbx_depositi=create cbx_depositi
this.cbx_centri_costo=create cbx_centri_costo
this.cb_duplica=create cb_duplica
this.dw_detail=create dw_detail
this.dw_appoggio=create dw_appoggio
this.dw_ricerca=create dw_ricerca
this.dw_prodotti_lista=create dw_prodotti_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_duplica_txt
this.Control[iCurrent+2]=this.cbx_depositi
this.Control[iCurrent+3]=this.cbx_centri_costo
this.Control[iCurrent+4]=this.cb_duplica
this.Control[iCurrent+5]=this.dw_detail
this.Control[iCurrent+6]=this.dw_appoggio
this.Control[iCurrent+7]=this.dw_ricerca
this.Control[iCurrent+8]=this.dw_prodotti_lista
this.Control[iCurrent+9]=this.dw_folder_search
end on

on w_duplica_prodotto.destroy
call super::destroy
destroy(this.cb_duplica_txt)
destroy(this.cbx_depositi)
destroy(this.cbx_centri_costo)
destroy(this.cb_duplica)
destroy(this.dw_detail)
destroy(this.dw_appoggio)
destroy(this.dw_ricerca)
destroy(this.dw_prodotti_lista)
destroy(this.dw_folder_search)
end on

event pc_setwindow;call super::pc_setwindow;string ls_criteriacolumn[], ls_searchtable[], ls_searchcolumn[]
windowobject l_objects[]

is_cod_prodotto = s_cs_xx.parametri.parametro_s_1
title="Duplicazione del prodotto: " + is_cod_prodotto + " " + s_cs_xx.parametri.parametro_s_2

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
l_objects[1] = dw_prodotti_lista
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)

dw_prodotti_lista.set_dw_key("cod_azienda")
dw_prodotti_lista.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)
is_sql_select = dw_prodotti_lista.GetSQLSelect()

is_checked_image_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\check-box.png"
is_unchecked_image_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\check-box-uncheck.png"

yield()

iuo_label = create uo_array

postevent("we_init_dw")

end event

type cb_duplica_txt from commandbutton within w_duplica_prodotto
integer x = 1298
integer y = 2020
integer width = 663
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica da txt"
end type

event clicked;wf_duplica_txt()
end event

type cbx_depositi from checkbox within w_duplica_prodotto
integer x = 667
integer y = 2020
integer width = 480
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Copia depositi"
boolean checked = true
end type

type cbx_centri_costo from checkbox within w_duplica_prodotto
integer x = 23
integer y = 2020
integer width = 613
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Copia centri di costo"
boolean checked = true
end type

type cb_duplica from commandbutton within w_duplica_prodotto
integer x = 1989
integer y = 2020
integer width = 663
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;
if g_mb.confirm("Procedo con la duplicazione delle informazioni selezionate?") then
	if wf_duplica() then
		commit;
		g_mb.show("Duplicazione completata con successo")
		close(parent)
	else
		rollback;
	end if
end if
end event

type dw_detail from uo_std_dw within w_duplica_prodotto
integer x = 23
integer y = 620
integer width = 2629
integer height = 1380
integer taborder = 40
string dataobject = "d_duplica_prodotto_dettaglio"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;choose case dwo.name
		
	case "p_select"
		wf_togle_checked()
		
end choose
end event

type dw_appoggio from datawindow within w_duplica_prodotto
boolean visible = false
integer x = 1874
integer y = 1620
integer width = 754
integer height = 360
integer taborder = 50
string title = "none"
borderstyle borderstyle = stylelowered!
end type

type dw_ricerca from u_dw_search within w_duplica_prodotto
integer x = 137
integer y = 40
integer width = 2469
integer height = 540
integer taborder = 30
string dataobject = "d_duplica_prodotto_ricerca"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca"
		wf_ricerca()
		
	case "b_annulla_ric"
		dw_ricerca.fu_reset()
		
	case "b_ricerca_prodotto_da"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_da")
	
		
	case "b_ricerca_prodotto_a"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_a")
		
end choose
end event

event itemchanged;/** BASTA ANCESTOR, E' TROPPO OBSOLETO **/

if isvalid(dwo) then
	choose case dwo.name
			
		case "cod_prodotto_da"
			if isnull(getitemstring(1, "cod_prodotto_a")) then
				setitem(1, "cod_prodotto_a", data)
			end if
			
	end choose
end if
end event

type dw_prodotti_lista from uo_cs_xx_dw within w_duplica_prodotto
event ue_aggiorna_descrizione ( )
event ue_riposiziona_riga ( )
integer x = 137
integer y = 40
integer width = 2011
integer height = 540
integer taborder = 20
string dataobject = "d_prodotti_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder_search from u_folder within w_duplica_prodotto
integer x = 23
integer y = 20
integer width = 2629
integer height = 580
integer taborder = 10
end type

