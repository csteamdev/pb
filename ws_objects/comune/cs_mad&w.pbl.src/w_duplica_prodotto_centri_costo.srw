﻿$PBExportHeader$w_duplica_prodotto_centri_costo.srw
forward
global type w_duplica_prodotto_centri_costo from w_cs_xx_risposta
end type
type cb_duplica from commandbutton within w_duplica_prodotto_centri_costo
end type
type dw_duplica from uo_cs_xx_dw within w_duplica_prodotto_centri_costo
end type
end forward

global type w_duplica_prodotto_centri_costo from w_cs_xx_risposta
integer width = 2501
integer height = 488
string title = "Duplicazione Dati Centri di Costo"
cb_duplica cb_duplica
dw_duplica dw_duplica
end type
global w_duplica_prodotto_centri_costo w_duplica_prodotto_centri_costo

on w_duplica_prodotto_centri_costo.create
int iCurrent
call super::create
this.cb_duplica=create cb_duplica
this.dw_duplica=create dw_duplica
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_duplica
this.Control[iCurrent+2]=this.dw_duplica
end on

on w_duplica_prodotto_centri_costo.destroy
call super::destroy
destroy(this.cb_duplica)
destroy(this.dw_duplica)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_duplica.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_nomodify + &
								 c_nodelete + &
								 c_newonopen + &
								 c_disableCC, &
								 c_noresizedw + &
								 c_nohighlightselected + &
								 c_cursorrowpointer)

end event

type cb_duplica from commandbutton within w_duplica_prodotto_centri_costo
integer x = 974
integer y = 288
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;string ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_centro_costo, ls_cod_prodotto_origine, &
       ls_cod_prodotto_corrente
dec{4} ld_percentuale

dw_duplica.accepttext()

ls_cod_prodotto_origine = s_cs_xx.parametri.parametro_s_10

if g_mb.messagebox("APICE","Proseguo con la duplicazione?",Question!,yesno!,2) = 2 then return

ls_cod_prodotto_inizio = dw_duplica.getitemstring(1,"cod_prodotto_inizio")
if isnull(ls_cod_prodotto_inizio) then ls_cod_prodotto_inizio = "!"

ls_cod_prodotto_fine = dw_duplica.getitemstring(1,"cod_prodotto_fine")
if isnull(ls_cod_prodotto_fine) then ls_cod_prodotto_fine = "{"

declare cu_prodotti cursor for  
select cod_prodotto
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_prodotto >= :ls_cod_prodotto_inizio and
		cod_prodotto <= :ls_cod_prodotto_fine
order by cod_prodotto;


declare cu_centri_costo_acq cursor for  
  select cod_centro_costo,   
         percentuale  
    from anag_prodotti_cc_acq  
   where cod_azienda = :s_cs_xx.cod_azienda and  
         cod_prodotto = :ls_cod_prodotto_origine
order by cod_centro_costo asc  ;

declare cu_centri_costo_ven cursor for  
  select cod_centro_costo,   
         percentuale  
    from anag_prodotti_cc_ven  
   where cod_azienda = :s_cs_xx.cod_azienda and  
         cod_prodotto = :ls_cod_prodotto_origine
order by cod_centro_costo asc  ;


open cu_prodotti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN cursore cu_prodotti.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_prodotti into :ls_cod_prodotto_corrente;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in FETCH cursore cu_prodotti.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	///    procedo con cc acquisti
	
	delete from anag_prodotti_cc_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in delete anag_prodotti_cc_acq.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	open cu_centri_costo_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in OPEN cursore cu_centri_costo_acq.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	do while true
		fetch cu_centri_costo_acq into :ls_cod_centro_costo, :ld_percentuale;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore in FETCH cursore cu_centri_costo.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		insert into anag_prodotti_cc_acq
			(cod_azienda,
			cod_prodotto,
			cod_centro_costo,
			percentuale)
		values
			(:s_cs_xx.cod_azienda,
			:ls_cod_prodotto_corrente,
			:ls_cod_centro_costo,
			:ld_percentuale);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in insert anag_prodotti_cc_acq.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	loop
	
	close cu_centri_costo_acq;
	
	
	// procedo con cc vendite
	
	delete from anag_prodotti_cc_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in delete anag_prodotti_cc_ven.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	open cu_centri_costo_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in OPEN cursore cu_centri_costo_ven.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	do while true
		fetch cu_centri_costo_ven into :ls_cod_centro_costo, :ld_percentuale;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore in FETCH cursore cu_centri_costo_ven.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		insert into anag_prodotti_cc_ven
			(cod_azienda,
			cod_prodotto,
			cod_centro_costo,
			percentuale)
		values
			(:s_cs_xx.cod_azienda,
			:ls_cod_prodotto_corrente,
			:ls_cod_centro_costo,
			:ld_percentuale);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in insert anag_prodotti_cc_ven.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	loop
	
	close cu_centri_costo_ven;
	
loop

close cu_prodotti;

commit;

g_mb.messagebox("APICE","Duplicazione eseguita correttamente")

close(parent)
end event

type dw_duplica from uo_cs_xx_dw within w_duplica_prodotto_centri_costo
integer x = 9
integer y = 16
integer width = 2446
integer height = 244
integer taborder = 10
string dataobject = "d_duplica_prodotto_centri_costo"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_duplica,"cod_prodotto_inizio")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_duplica,"cod_prodotto_fine")
end choose
end event

