﻿$PBExportHeader$w_elenco_pf.srw
$PBExportComments$Window elenco pf
forward
global type w_elenco_pf from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_elenco_pf
end type
type dw_ricerca from u_dw_search within w_elenco_pf
end type
type dw_elenco_pf from uo_cs_xx_dw within w_elenco_pf
end type
type cb_2 from commandbutton within w_elenco_pf
end type
type cb_1 from commandbutton within w_elenco_pf
end type
type gb_1 from groupbox within w_elenco_pf
end type
end forward

global type w_elenco_pf from w_cs_xx_principale
integer width = 3442
integer height = 2312
string title = "Ricerca Distinte contenenti il prodotto indicato."
hpb_1 hpb_1
dw_ricerca dw_ricerca
dw_elenco_pf dw_elenco_pf
cb_2 cb_2
cb_1 cb_1
gb_1 gb_1
end type
global w_elenco_pf w_elenco_pf

forward prototypes
public function integer wf_report ()
public function integer wf_null (ref string ls_stringa[], ref double ldd_double[])
public function integer wf_ricerca_padri (string fs_prodotto_figlio, string fs_cod_versione, ref string fs_prodotti_padri[], ref string fs_versioni_padri[])
public function integer wf_report_padri ()
public function integer wf_ricerca_padri (string fs_prodotto_figlio, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[])
public function integer wf_ricerca_padri_2 (string fs_prodotto_figlio, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[])
public function integer wf_ricerca_padri_2 (string fs_prodotto_figlio, string fs_cod_versione, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[])
end prototypes

public function integer wf_report ();string 	ls_materia_prima[],ls_cod_prodotto_finito,ls_cod_versione,ls_errore,ls_null,ls_cod_materia_prima, &
			ls_des_prodotto, ls_versione_prima[]
double   ldd_quantita_utilizzo[],ldd_quan_utilizzo_prec
integer  li_risposta
long     ll_null,ll_i,ll_t
uo_funzioni_1 luo_funzioni

dw_elenco_pf.reset()

setnull(ll_null)
setnull(ls_null)

ls_cod_materia_prima = s_cs_xx.parametri.parametro_s_1

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_materia_prima;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

dw_elenco_pf.Modify("materia_prima.text='" + ls_cod_materia_prima + " "+ ls_des_prodotto + "'")

declare righe_padri cursor for 
select  cod_prodotto,
		  cod_versione
from    distinta_padri
where   cod_azienda=:s_cs_xx.cod_azienda;

open righe_padri;

do while 1=1
	fetch righe_padri
	into  :ls_cod_prodotto_finito,
			:ls_cod_versione;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		exit
	end if

	if sqlca.sqlcode = 100 then exit
	
	wf_null(	ls_materia_prima[],ldd_quantita_utilizzo[])
	
	luo_funzioni = create uo_funzioni_1
	
	li_risposta = luo_funzioni.uof_trova_mat_prime_varianti (ls_cod_prodotto_finito, &
														   ls_cod_versione, &
														   ls_null,&
														   ls_materia_prima[], &
															ls_versione_prima[], &
														   ldd_quantita_utilizzo[], &
														   ldd_quan_utilizzo_prec, &
														   ll_null, &
														   ll_null, &
														   ll_null, &
														   ls_null, &
														   ls_null, &
														   ls_errore) 	
	
	destroy luo_funzioni

	if li_risposta < 0 then
		g_mb.messagebox("Apice",ls_errore,stopsign!)
		exit
	end if
	
	for ll_i = 1 to upperbound(ls_materia_prima[])
		if ls_materia_prima[ll_i] = ls_cod_materia_prima then
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_finito;
	
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
				close righe_padri;
				return -1
			end if

			ll_t = dw_elenco_pf.insertrow(0)
			dw_elenco_pf.setitem(ll_t,"cod_prodotto_finito", ls_cod_prodotto_finito)
			dw_elenco_pf.setitem(ll_t,"des_prodotto", ls_des_prodotto)
			dw_elenco_pf.setitem(ll_t,"cod_versione", ls_cod_versione)
			exit
		end if
	next
		
loop

close righe_padri;

return 0
end function

public function integer wf_null (ref string ls_stringa[], ref double ldd_double[]);string ls_1[]
double ldd_1[]

ls_stringa[] = ls_1[]
ldd_double[] = ldd_1[]

return 0
end function

public function integer wf_ricerca_padri (string fs_prodotto_figlio, string fs_cod_versione, ref string fs_prodotti_padri[], ref string fs_versioni_padri[]);string 	ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto,&
         ls_cod_versione,ls_errore,ls_null
dec{4}   ldd_quantita_utilizzo[], ldd_quantita
integer  li_risposta
long     ll_t,ll_i,ll_y, ll_cont

string ls_flag_mp

declare cu_righe_padri cursor for 
select  cod_prodotto_padre,
		  cod_versione,
		  quan_utilizzo,flag_materia_prima
from    distinta
where   cod_azienda=:s_cs_xx.cod_azienda and
        cod_prodotto_figlio = :fs_prodotto_figlio and
		  cod_versione = :fs_cod_versione //and
		  //flag_materia_prima = 'N'
;

open cu_righe_padri;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore in open cursore cu_righe_padri.~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_i = 0
	
do while true
	fetch cu_righe_padri
	into  :ls_cod_prodotto,
			:ls_cod_versione,
			:ldd_quantita,:ls_flag_mp
	;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		rollback;
		exit
	end if

	if sqlca.sqlcode = 100 then exit
	
	if ls_flag_mp = "S" then
		continue
	end if
	
	ll_i ++
	
	ls_cod_prodotto_padre[ll_i] = ls_cod_prodotto
	ls_cod_versione_padre[ll_i] = ls_cod_versione
	
loop

close cu_righe_padri;

for ll_y = 1 to ll_i
	select count(*)
	into   :ll_cont
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_figlio = :ls_cod_prodotto_padre[ll_y] and
			 cod_versione = :ls_cod_versione_padre[ll_y] and
			 flag_materia_prima = 'N';
			 
	if ll_cont > 0 then
		wf_ricerca_padri(ls_cod_prodotto_padre[ll_y], ls_cod_versione_padre[ll_y], ref fs_prodotti_padri, ref fs_versioni_padri)
	else
		ll_t = upperbound(fs_prodotti_padri)
		fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto_padre[ll_y]
		fs_versioni_padri[ll_t + 1] = ls_cod_versione_padre[ll_y]
	end if
next

return 0
end function

public function integer wf_report_padri ();string 	ls_materia_prima[],ls_cod_prodotto_finito,ls_cod_versione,ls_errore,ls_null,ls_cod_materia_prima, &
			ls_des_prodotto, ls_prodotti_padri[], ls_versioni_padri[], ls_azienda
dec{4}   ld_quan_utilizzo[]
integer  li_risposta
long     ll_null,ll_i,ll_row, ll_cont

//Donato 27-10-2008
//aggiunto flag per visualizzare anche le sottodistinte e non solo l'ultimo livello
string ls_flag_visualizza_tutto

dw_elenco_pf.reset()

setnull(ll_null)
setnull(ls_null)

ls_cod_materia_prima = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")
ls_flag_visualizza_tutto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_visualizza_tutto")

if isnull(ls_cod_materia_prima) or len(ls_cod_materia_prima) < 1 then
	g_mb.messagebox("SEP", "Indicare un codice prodotto da cercare.")
	return 0
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_materia_prima;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore in ricerca prodotto:" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select rag_soc_1
into   :ls_azienda
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore in ricerca azienda:" + sqlca.sqlerrtext,stopsign!)
	return -1
end if


dw_elenco_pf.Modify("materia_prima.text='" + ls_cod_materia_prima + " "+ ls_des_prodotto + "'")

//Donato 27-10-2008
//aggiunto flag per visualizzare anche le sottodistinte e non solo l'ultimo livello
if ls_flag_visualizza_tutto = "S" then
	wf_ricerca_padri_2(ls_cod_materia_prima, ref ls_prodotti_padri, ref ls_versioni_padri, ref ld_quan_utilizzo)
else
	wf_ricerca_padri(ls_cod_materia_prima, ref ls_prodotti_padri, ref ls_versioni_padri, ref ld_quan_utilizzo)
end if
//wf_ricerca_padri(ls_cod_materia_prima, ref ls_prodotti_padri, ref ls_versioni_padri, ref ld_quan_utilizzo)

//fine modifica ------------------------------

ll_cont = upperbound(ls_prodotti_padri)

long ll_j
string ls_prod, ls_vers
boolean lb_trovata = false

for ll_i = 1 to ll_cont
	
	setnull(ls_des_prodotto)
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_prodotti_padri[ll_i];

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		ls_des_prodotto = "-- PRODOTTO CANCELLATO --"
	end if
	
	if ll_i > 1 then
		ls_prod = ls_prodotti_padri[ll_i]
		ls_vers = ls_versioni_padri[ll_i]
		for ll_j = 1 to ll_i - 1
			if ls_prodotti_padri[ll_j] = ls_prod and ls_versioni_padri[ll_j] = ls_vers then
				lb_trovata = true
				exit
			else
				lb_trovata = false
			end if
		next
	end if
	
	if lb_trovata then
		lb_trovata = false
	else
		ll_row = dw_elenco_pf.insertrow(0)
		dw_elenco_pf.setitem(ll_row,"cod_prodotto_finito", ls_prodotti_padri[ll_i])
		dw_elenco_pf.setitem(ll_row,"des_prodotto", ls_des_prodotto)
		dw_elenco_pf.setitem(ll_row,"cod_versione", ls_versioni_padri[ll_i])
		dw_elenco_pf.setitem(ll_row,"quan_utilizzo", ld_quan_utilizzo[ll_i])
		dw_elenco_pf.setitem(ll_row,"azienda", ls_azienda)
	end if
next

rollback;

dw_elenco_pf.setsort("cod_prodotto_finito")
dw_elenco_pf.sort()

return 0
end function

public function integer wf_ricerca_padri (string fs_prodotto_figlio, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[]);string 	ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto,&
         ls_cod_versione,ls_errore,ls_null
dec{4}   ldd_quantita_utilizzo[], ldd_quantita
integer  li_risposta
long     ll_t, ll_i, ll_y, ll_cont, lt_old, ll_z

declare cu_righe_padri cursor for 
select  cod_prodotto_padre,
		  cod_versione,
		  quan_utilizzo
from    distinta
where   cod_azienda=:s_cs_xx.cod_azienda and
        cod_prodotto_figlio = :fs_prodotto_figlio  //and
		  //flag_materia_prima = 'N'
		  ;

open cu_righe_padri;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore in open cursore cu_righe_padri.~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_i = 0
	
do while true
	fetch cu_righe_padri
	into  :ls_cod_prodotto,
			:ls_cod_versione,
			:ldd_quantita;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		rollback;
		exit
	end if

	if sqlca.sqlcode = 100 then exit
	
	ll_i ++
	
	ls_cod_prodotto_padre[ll_i] = ls_cod_prodotto
	ls_cod_versione_padre[ll_i] = ls_cod_versione
	ldd_quantita_utilizzo[ll_i] = ldd_quantita
	
loop

close cu_righe_padri;

for ll_y = 1 to ll_i
	select count(*)
	into   :ll_cont
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto_figlio = :ls_cod_prodotto_padre[ll_y] and
			 cod_versione = :ls_cod_versione_padre[ll_y] and
			 flag_materia_prima = 'N';
			 
	hpb_1.position = round( ( (ll_y * 100) / ll_i), 0 )

	if ll_cont > 0 then
		lt_old = upperbound(fs_prodotti_padri)
		wf_ricerca_padri(ls_cod_prodotto_padre[ll_y], ls_cod_versione_padre[ll_y], ref fs_prodotti_padri, ref fs_versioni_padri)
		if upperbound(fs_prodotti_padri) > lt_old then
			for ll_z = lt_old + 1 to upperbound(fs_prodotti_padri)
				fdd_quan_utilizzo[ll_z] = ldd_quantita_utilizzo[ll_y]
			next
		end if
	else
		ll_t = upperbound(fs_prodotti_padri)
		fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto_padre[ll_y]
		fs_versioni_padri[ll_t + 1] = ls_cod_versione_padre[ll_y]
		fdd_quan_utilizzo[ll_t + 1] = ldd_quantita_utilizzo[ll_y]
	end if
next

return 0
end function

public function integer wf_ricerca_padri_2 (string fs_prodotto_figlio, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[]);string 	ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto,&
         ls_cod_versione,ls_errore,ls_null, ls_flag_mp, ls_cod_materia_prima
dec{4}   ldd_quantita_utilizzo[], ldd_quantita
integer  li_risposta
long     ll_t, ll_i, ll_y, ll_cont, lt_old, ll_z
string ls_flag_mp_filter

dw_ricerca.accepttext()
ls_cod_materia_prima = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")

declare cu_righe_padri_mp1 cursor for 
select  cod_prodotto_padre,
		  cod_versione,
		  quan_utilizzo,flag_materia_prima
from    distinta
where   cod_azienda=:s_cs_xx.cod_azienda and
		  cod_prodotto_figlio = :fs_prodotto_figlio  
		  ;

open cu_righe_padri_mp1;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore in open cursore cu_righe_padri.~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_i = 0
	
do while true
	fetch cu_righe_padri_mp1
	into  :ls_cod_prodotto,
			:ls_cod_versione,
			:ldd_quantita, :ls_flag_mp
			;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		rollback;
		exit
	end if

	if sqlca.sqlcode = 100 then exit	
//	if ls_flag_mp = "S" then //and ls_cod_materia_prima<>fs_prodotto_figlio then
//		continue
//	end if
	if ls_cod_materia_prima<>fs_prodotto_figlio then
		continue
	end if
	
	ll_i ++	
	ls_cod_prodotto_padre[ll_i] = ls_cod_prodotto
	ls_cod_versione_padre[ll_i] = ls_cod_versione
	ldd_quantita_utilizzo[ll_i] = ldd_quantita
	
//	//se il prodotto è presente in distinta_padri allora lo aggiungiamo
//	select count(*)
//	into   :ll_cont
//	from   distinta_padri
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_prodotto = :ls_cod_prodotto and
//			 cod_versione = :ls_cod_versione;
//			 
//	if ll_cont >0 then
//		//si tratta di una distinta
//		//Donato 06/10/2008
//		//inserire la distinta
//		ll_t = upperbound(fs_prodotti_padri)
//		fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto
//		fs_versioni_padri[ll_t + 1] = ls_cod_versione
//		fdd_quan_utilizzo[ll_t + 1] = ldd_quantita
//		//--------------------------------------------
//	end if	

	//inserire la distinta
	ll_t = upperbound(fs_prodotti_padri)
	fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto
	fs_versioni_padri[ll_t + 1] = ls_cod_versione
	fdd_quan_utilizzo[ll_t + 1] = ldd_quantita
loop
close cu_righe_padri_mp1;

//verificare la presenza delle distinte appena trovate in ulteriori distinte
for ll_y = 1 to ll_i
	ls_cod_materia_prima = ls_cod_prodotto_padre[ll_y]
	ls_cod_versione = ls_cod_versione_padre[ll_y]
	
	wf_ricerca_padri_2(ls_cod_materia_prima, ls_cod_versione, ref fs_prodotti_padri, ref fs_versioni_padri, ref fdd_quan_utilizzo)
next

return 0
end function

public function integer wf_ricerca_padri_2 (string fs_prodotto_figlio, string fs_cod_versione, ref string fs_prodotti_padri[], ref string fs_versioni_padri[], ref decimal fdd_quan_utilizzo[]);string 	ls_cod_prodotto_padre[], ls_cod_versione_padre[], ls_cod_prodotto,&
         ls_cod_versione,ls_errore,ls_null, ls_flag_mp
dec{4}   ldd_quantita_utilizzo[], ldd_quantita
integer  li_risposta
long     ll_t,ll_i,ll_y, ll_cont

declare cu_righe_padri cursor for 
select  cod_prodotto_padre,
		  cod_versione,
		  quan_utilizzo,flag_materia_prima
from    distinta
where   cod_azienda=:s_cs_xx.cod_azienda and
        cod_prodotto_figlio = :fs_prodotto_figlio and
		  cod_versione = :fs_cod_versione //and
		  //flag_materia_prima = 'N'
		  ;

open cu_righe_padri;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore in open cursore cu_righe_padri.~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_i = 0
	
do while true
	fetch cu_righe_padri
	into  :ls_cod_prodotto,
			:ls_cod_versione,
			:ldd_quantita,
			:ls_flag_mp
	;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		rollback;
		exit
	end if

	if sqlca.sqlcode = 100 then exit
	/*
	ll_i ++
	
	ls_cod_prodotto_padre[ll_i] = ls_cod_prodotto
	ls_cod_versione_padre[ll_i] = ls_cod_versione
	//Donato -------------------
	ldd_quantita_utilizzo[ll_i] = ldd_quantita
	//---------------------------
	*/
	if ls_flag_mp = "S" and ls_cod_prodotto<>fs_prodotto_figlio then
		continue
	end if
	
	ll_i ++	
	ls_cod_prodotto_padre[ll_i] = ls_cod_prodotto
	ls_cod_versione_padre[ll_i] = ls_cod_versione
	ldd_quantita_utilizzo[ll_i] = ldd_quantita
	
//	//se il prodotto è presente in distinta_padri allora lo aggiungiamo
//	select count(*)
//	into   :ll_cont
//	from   distinta_padri
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_prodotto = :ls_cod_prodotto and
//			 cod_versione = :ls_cod_versione;
//			 
//	if ll_cont >0 then
//		//si tratta di una distinta
//		//Donato 06/10/2008
//		//inserire la distinta
//		ll_t = upperbound(fs_prodotti_padri)
//		fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto
//		fs_versioni_padri[ll_t + 1] = ls_cod_versione
//		fdd_quan_utilizzo[ll_t + 1] = ldd_quantita
//		//--------------------------------------------
//	end if	

	//inserire la distinta
	ll_t = upperbound(fs_prodotti_padri)
	fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto
	fs_versioni_padri[ll_t + 1] = ls_cod_versione
	fdd_quan_utilizzo[ll_t + 1] = ldd_quantita
loop

close cu_righe_padri;

for ll_y = 1 to ll_i
//	select count(*)
//	into   :ll_cont
//	from   distinta
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_prodotto_figlio = :ls_cod_prodotto_padre[ll_y] and
//			 cod_versione = :ls_cod_versione_padre[ll_y] //and
//			 //flag_materia_prima = 'N'
//	;
//			 
//	if ll_cont > 0 then
//		wf_ricerca_padri_2(ls_cod_prodotto_padre[ll_y], ls_cod_versione_padre[ll_y], ref fs_prodotti_padri, ref fs_versioni_padri, ref fdd_quan_utilizzo)
//	else
////		ll_t = upperbound(fs_prodotti_padri)
////		fs_prodotti_padri[ll_t + 1] = ls_cod_prodotto_padre[ll_y]
////		fs_versioni_padri[ll_t + 1] = ls_cod_versione_padre[ll_y]
////		//Donato ----------------
////		fdd_quan_utilizzo[ll_t + 1] = ldd_quantita_utilizzo[ll_y]
////		//-----------------------
//	end if
	wf_ricerca_padri_2(ls_cod_prodotto_padre[ll_y], ls_cod_versione_padre[ll_y], ref fs_prodotti_padri, ref fs_versioni_padri, ref fdd_quan_utilizzo)
next

return 0
end function

on w_elenco_pf.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dw_ricerca=create dw_ricerca
this.dw_elenco_pf=create dw_elenco_pf
this.cb_2=create cb_2
this.cb_1=create cb_1
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dw_ricerca
this.Control[iCurrent+3]=this.dw_elenco_pf
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.gb_1
end on

on w_elenco_pf.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dw_ricerca)
destroy(this.dw_elenco_pf)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.gb_1)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin+ c_NoEnablePopup)

save_on_close(c_socnosave)

dw_elenco_pf.change_dw_current()

if NOT isnull(i_openparm) then
	
	dw_elenco_pf.set_dw_options(sqlca, &
										i_openparm, &
										c_nonew + &
										c_nomodify + &
										c_nodelete + &
										c_noenablenewonopen + &
										c_noenablemodifyonopen + &
										c_scrollparent + &
										c_disablecc, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowfocusrect + &
										c_nocursorrowpointer + &
										c_InactiveDWColorUnchanged + c_ViewModeColorUnchanged +c_ViewModeBorderUnchanged)
	
	dw_elenco_pf.postevent("ue_imposta_ricerca")
	
else

	dw_elenco_pf.set_dw_options(sqlca, &
										pcca.null_object, &
										c_nonew + &
										c_nomodify + &
										c_nodelete + &
										c_noenablenewonopen + &
										c_noenablemodifyonopen + &
										c_scrollparent + &
										c_disablecc, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowfocusrect + &
										c_nocursorrowpointer + &
										c_InactiveDWColorUnchanged + c_ViewModeColorUnchanged +c_ViewModeBorderUnchanged)

end if

iuo_dw_main = dw_elenco_pf

//dw_elenco_pf.set_dw_options(sqlca, &
//									i_openparm, &
//									c_default , c_InactiveDWColorUnchanged + c_ViewModeColorUnchanged +c_ViewModeBorderUnchanged) 


end event

type hpb_1 from hprogressbar within w_elenco_pf
integer x = 2446
integer y = 160
integer width = 891
integer height = 80
unsignedinteger maxposition = 100
integer setstep = 10
end type

type dw_ricerca from u_dw_search within w_elenco_pf
event ue_key pbm_dwnkey
integer x = 46
integer y = 40
integer width = 2400
integer height = 220
integer taborder = 32
boolean bringtotop = true
string dataobject = "d_distinta_padri_cerca_mp"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_elenco_pf from uo_cs_xx_dw within w_elenco_pf
event ue_imposta_ricerca ( )
integer x = 23
integer y = 260
integer width = 3360
integer height = 1940
integer taborder = 32
string dataobject = "d_elenco_pf"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_imposta_ricerca();if i_parentdw.dataobject = 'd_prodotti_lista' or i_parentdw.dataobject = 'd_prodotti_det_1_tv' then
	dw_ricerca.setitem(dw_ricerca.getrow(),"rs_cod_prodotto", i_parentdw.getitemstring(i_parentdw.getrow() , "cod_prodotto"))
end if
end event

type cb_2 from commandbutton within w_elenco_pf
integer x = 2994
integer y = 72
integer width = 366
integer height = 80
integer taborder = 22
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;setpointer(hourglass!)
dw_elenco_pf.setredraw(false)
dw_elenco_pf.change_dw_current()

wf_report_padri()

dw_elenco_pf.setredraw(true)
setpointer(arrow!)

dw_elenco_pf.change_dw_current()
// non cambiare questa variabile altrimenti non stampa più.
dw_elenco_pf.i_IsEmpty=FALSE

hpb_1.position = 0
end event

type cb_1 from commandbutton within w_elenco_pf
boolean visible = false
integer x = 2994
integer y = 32
integer width = 366
integer height = 80
integer taborder = 22
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
boolean default = true
end type

event clicked;setpointer(hourglass!)
wf_report()
setpointer(arrow!)
end event

type gb_1 from groupbox within w_elenco_pf
integer width = 3383
integer height = 340
integer taborder = 32
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
end type

