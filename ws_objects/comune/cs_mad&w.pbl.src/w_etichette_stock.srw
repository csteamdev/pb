﻿$PBExportHeader$w_etichette_stock.srw
$PBExportComments$Etichette Stock
forward
global type w_etichette_stock from w_cs_xx_principale
end type
type dw_etichette_stock from uo_cs_xx_dw within w_etichette_stock
end type
end forward

global type w_etichette_stock from w_cs_xx_principale
int Width=2437
int Height=1705
boolean TitleBar=true
string Title="Etichette Stock"
boolean HScrollBar=true
boolean VScrollBar=true
dw_etichette_stock dw_etichette_stock
end type
global w_etichette_stock w_etichette_stock

on w_etichette_stock.create
int iCurrent
call w_cs_xx_principale::create
this.dw_etichette_stock=create dw_etichette_stock
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_etichette_stock
end on

on w_etichette_stock.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_etichette_stock)
end on

event pc_setwindow;call super::pc_setwindow;
dw_etichette_stock.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_scrollparent + &
											c_disablecc, &
                                 c_default)

iuo_dw_main = dw_etichette_stock


end event

type dw_etichette_stock from uo_cs_xx_dw within w_etichette_stock
int X=23
int Y=21
int Width=2286
int Height=1501
string DataObject="d_etichette_stock"
end type

event pcd_retrieve;call super::pcd_retrieve;string   ls_cod_prodotto,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto
datetime ldt_data_stock
long     ll_errore,ll_prog_stock

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_deposito = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_deposito")
ls_cod_ubicazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_ubicazione")
ls_cod_lotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lotto")
ldt_data_stock = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_stock")
ll_prog_stock = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_stock")

ll_errore = retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto,ls_cod_deposito, & 
							ls_cod_ubicazione,ls_cod_lotto,ldt_data_stock,ll_prog_stock)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

