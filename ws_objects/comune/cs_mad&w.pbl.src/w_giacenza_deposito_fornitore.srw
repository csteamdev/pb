﻿$PBExportHeader$w_giacenza_deposito_fornitore.srw
forward
global type w_giacenza_deposito_fornitore from w_cs_xx_risposta
end type
type dw_lista_giacenze from uo_std_dw within w_giacenza_deposito_fornitore
end type
type cb_annulla from commandbutton within w_giacenza_deposito_fornitore
end type
type cb_ok from commandbutton within w_giacenza_deposito_fornitore
end type
type dw_list_vecchia from uo_cs_xx_dw within w_giacenza_deposito_fornitore
end type
type dw_search from u_dw_search within w_giacenza_deposito_fornitore
end type
end forward

global type w_giacenza_deposito_fornitore from w_cs_xx_risposta
integer width = 2555
integer height = 1872
string title = "Giacenza Fornitore"
event ue_titolo ( )
dw_lista_giacenze dw_lista_giacenze
cb_annulla cb_annulla
cb_ok cb_ok
dw_list_vecchia dw_list_vecchia
dw_search dw_search
end type
global w_giacenza_deposito_fornitore w_giacenza_deposito_fornitore

type variables
private:
	uo_std_dw iuo_dw_parent
	string is_cod_fornitore
end variables

event ue_titolo();string ls_cod_fornitore, ls_rag_soc_1, ls_cod_deposito
long ll_row

ll_row = iuo_dw_parent.getrow()
ls_cod_fornitore = iuo_dw_parent.getitemstring(ll_row, "cod_fornitore")

// Fornitore
select rag_soc_1
into :ls_rag_soc_1
from anag_fornitori
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_fornitore = :ls_cod_fornitore;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("APICE", "Errore durante il controllo del fornitore.")
	cb_annulla.triggerevent("clicked")
	return
end if
if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ""

is_cod_fornitore = ls_cod_fornitore
this.title = "Giacenza deposito: " + ls_cod_fornitore + " - " + ls_rag_soc_1
// ---

//// stefnaop 09/02/2012: abilito multideposito
//ls_sql = "SELECT cod_deposito FROM anag_depositi WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "
//ls_sql += " AND cod_fornitore='" + ls_cod_fornitore + "'"
//
//ll_row = guo_functions.uof_crea_datastore(lds_store, ls_sql)
//
//if ll_row < 0 then
//	g_mb.error("APICE", "Errore durante il controllo del deposito.~r~n" + sqlca.sqlerrtext)
//	cb_annulla.triggerevent("clicked")
//elseif ll_row = 0 then
//	g_mb.show("APICE", "Nessun deposito trovato per il fornitore " + ls_cod_fornitore + ".")
//	cb_annulla.triggerevent("clicked")
//else
//	for ll_i = 1 to ll_row
//		ls_cod_depositi[ll_i] = lds_store.getitemstring(ll_i, 1)
//	next
//	
//	is_cod_deposito = "'" + guo_functions.uof_implode(ls_cod_depositi, "','") + "'"
//end if
	
// carico deposito
//select cod_deposito
//into :is_cod_deposito
//from anag_depositi
//where
//	cod_azienda = :s_cs_xx.cod_azienda and
//	cod_fornitore = :ls_cod_fornitore;
//	
//if sqlca.sqlcode < 0 then
//	g_mb.error("APICE", "Errore durante il recupero del deposito.~r~n" +sqlca.sqlerrtext)
//	cb_annulla.triggerevent("clicked")
//	return
//elseif sqlca.sqlcode = 100 then
//	g_mb.show("APICE", "Il fornitore " + ls_cod_fornitore + " non ha nessun deposito associato.")
//	cb_annulla.triggerevent("clicked")
//	return
//elseif sqlca.sqlcode = 0 and (isnull(is_cod_deposito) or is_cod_deposito = "") then
//	g_mb.show("APICE", "Il fornitore " + ls_cod_fornitore + " non sembra avere un deposito valido.")
//	cb_annulla.triggerevent("clicked")
//	return
//end if
// ----
end event

on w_giacenza_deposito_fornitore.create
int iCurrent
call super::create
this.dw_lista_giacenze=create dw_lista_giacenze
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_list_vecchia=create dw_list_vecchia
this.dw_search=create dw_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista_giacenze
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.dw_list_vecchia
this.Control[iCurrent+5]=this.dw_search
end on

on w_giacenza_deposito_fornitore.destroy
call super::destroy
destroy(this.dw_lista_giacenze)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_list_vecchia)
destroy(this.dw_search)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_deposito

iuo_dw_parent = s_cs_xx.parametri.parametro_dw_1

//dw_list.set_dw_key("cod_azienda")												
//dw_list.set_dw_options(sqlca, pcca.null_object, c_default + c_noretrieveonopen, c_default)

ls_cod_deposito = s_cs_xx.parametri.parametro_s_1
setnull( s_cs_xx.parametri.parametro_s_1)
dw_search.setitem(dw_search.getrow(), "cod_deposito", ls_cod_deposito)
dw_lista_giacenze.ib_dw_report=true
												
event post ue_titolo()



	
end event

type dw_lista_giacenze from uo_std_dw within w_giacenza_deposito_fornitore
event ue_retrieve ( )
integer y = 440
integer width = 2491
integer height = 1200
integer taborder = 40
string dataobject = "d_giacenze_deposito_fornitore_1"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_retrieve();string ls_sql, ls_errore, ls_cod_prodotto, ls_error, ls_chiave[], ls_vuoto[], ls_where
long	ll_ret, ll_i, ll_return, ll_row, ll_t
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_vuoto[]
datetime ldt_data_rif
uo_magazzino	luo_mag
datastore 		lds_data	

reset()

dw_search.object.t_wait.visible='1'

ls_sql = g_str.format( "SELECT anag_prodotti.cod_prodotto,  anag_prodotti.des_prodotto,   anag_prodotti.cod_misura_mag ,stock.cod_deposito " + &
							" FROM stock " + &
							" LEFT JOIN anag_prodotti  ON anag_prodotti.cod_azienda = stock.cod_azienda AND anag_prodotti.cod_prodotto = stock.cod_prodotto " + &
							" LEFT JOIN anag_depositi ON anag_depositi.cod_azienda = stock.cod_azienda AND anag_depositi.cod_deposito = stock.cod_deposito " + &
							" WHERE anag_prodotti.cod_azienda = '$1'  AND   anag_depositi.cod_fornitore = '$2' ", s_cs_xx.cod_azienda, is_cod_fornitore )

if not isnull(dw_search.getitemstring(1,"cod_prodotto")) then ls_sql += g_str.format(" AND anag_prodotti.cod_prodotto = '$1' ", dw_search.getitemstring(1,"cod_prodotto"))
if not isnull(dw_search.getitemstring(1,"cod_deposito")) then ls_sql += g_str.format(" AND stock.cod_deposito = '$1' ", dw_search.getitemstring(1,"cod_deposito"))

ls_sql += " order by 1 "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
if ll_ret < 0 then
	g_mb.error(ls_errore)
	return
end if

luo_mag = Create uo_magazzino
ldt_data_rif = datetime(today(),00:00:00)

for ll_i = 1 to ll_ret
	ld_quant_val = ld_vuoto
	ld_giacenza_stock = ld_vuoto
	ld_costo_medio_stock = ld_vuoto
	ld_quan_costo_medio_stock = ld_vuoto
	
	ls_chiave = ls_vuoto
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i,1)
	
	if not isnull(dw_search.getitemstring(1,"cod_deposito")) then 
		luo_mag.is_considera_depositi_fornitori = "I"
		luo_mag.is_cod_depositi_in = "('" +  dw_search.getitemstring(1,"cod_deposito") + "')"
	end if
	
	ll_return = luo_mag.uof_saldo_prod_date_decimal ( ls_cod_prodotto, ldt_data_rif, "", ref ld_quant_val[], ref ls_error, "D", ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[] )
	
	if upperbound(ls_chiave) > 0 then
		for ll_t = 1 to upperbound(ls_chiave)
			if ld_giacenza_stock[ll_t] <= 0 and dw_search.getitemstring(1,"flag_giacenze_zero") = "N" then continue
			ll_row = insertrow(0)
			setitem(ll_row, 1, ls_chiave[ll_t])
			setitem(ll_row, 2, ls_cod_prodotto)
			setitem(ll_row, 3, lds_data.getitemstring(ll_i,2))
			setitem(ll_row, 4, lds_data.getitemstring(ll_i,3))
			setitem(ll_row, 5, ld_giacenza_stock[ll_t])
		next
	end if		
next

destroy luo_mag
destroy lds_data

dw_search.object.t_wait.visible='0'
end event

event doubleclicked;// non eseguire questo evento
end event

type cb_annulla from commandbutton within w_giacenza_deposito_fornitore
integer x = 1737
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nulla"
end type

event clicked;s_cs_xx.parametri.parametro_b_1 = false
close(parent)
end event

type cb_ok from commandbutton within w_giacenza_deposito_fornitore
integer x = 2126
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Assegna"
end type

event clicked;if dw_lista_giacenze.rowcount() > 0 and dw_lista_giacenze.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_lista_giacenze.getitemstring(dw_lista_giacenze.getrow(), "cod_prodotto")
	s_cs_xx.parametri.parametro_b_1 = true
	close(parent)
end if
		
end event

type dw_list_vecchia from uo_cs_xx_dw within w_giacenza_deposito_fornitore
integer x = 114
integer y = 2200
integer width = 1463
integer height = 360
integer taborder = 30
string dataobject = "d_giacenze_deposito_fornitore_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_fornitore, ls_rag_soc_1, ls_cod_deposito,ls_cod_prodotto, ls_des_prodotto
long ll_rows, ll_row, ll_i
dec{4} ld_giacenza_usata, ld_giacenza_stock

dw_search.accepttext()

// FILTRI
ls_cod_prodotto = dw_search.getitemstring(1, "cod_prodotto")
ls_cod_deposito = dw_search.getitemstring(1, "cod_deposito")
// ----

ll_rows = retrieve(s_cs_xx.cod_azienda, is_cod_fornitore, ls_cod_prodotto, ls_cod_deposito)

IF ll_rows < 0 THEN
   PCCA.Error = c_Fatal
	return
END IF

// Calcolo quantità usata nelle righe della dw padre
setnull(ls_cod_prodotto)
for ll_row = 1 to ll_rows
	ls_cod_prodotto = getitemstring(ll_row, "cod_prodotto")
	ld_giacenza_stock = getitemnumber(ll_row, "giacenza")
	ld_giacenza_usata = 0
	
	for ll_i = 1 to iuo_dw_parent.rowcount()
		if iuo_dw_parent.getitemstring(ll_i, "cod_prodotto_alt") = ls_cod_prodotto then
			ld_giacenza_usata += iuo_dw_parent.getitemnumber(ll_i, "quan_acquisto_alt")
		end if
	next
	
	setitem(ll_row, "giacenza", ld_giacenza_stock - ld_giacenza_usata)
next
// ----

resetupdate()
end event

event doubleclicked;call super::doubleclicked;cb_ok.event post clicked()
end event

type dw_search from u_dw_search within w_giacenza_deposito_fornitore
integer width = 2491
integer height = 420
integer taborder = 20
string dataobject = "d_giacenza_deposito_fornitore_ricerca"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name		
	case "cb_reset"
		string ls_null
		setnull(ls_null)
		
		setitem(1, "cod_prodotto", ls_null)
		setitem(1, "des_prodotto", ls_null)
		
	case "cb_cerca"
//		dw_list.change_dw_current()
//		dw_list.triggerevent("pcd_retrieve")

		accepttext()
		dw_lista_giacenze.triggerevent("ue_retrieve")	
	
	case "b_ricerca_deposito"
		guo_ricerca.uof_ricerca_deposito(dw_search,"cod_deposito")
		
	case "b_prodotto_ricerca"
		guo_ricerca.uof_ricerca_prodotto(dw_search,"cod_prodotto")

end choose
end event

