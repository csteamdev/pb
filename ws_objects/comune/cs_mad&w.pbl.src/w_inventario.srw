﻿$PBExportHeader$w_inventario.srw
forward
global type w_inventario from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_inventario
end type
type st_log from statictext within w_inventario
end type
type cb_genera_inventario from commandbutton within w_inventario
end type
type cb_cancella_2 from commandbutton within w_inventario
end type
type cb_cancella_1 from commandbutton within w_inventario
end type
type cb_stock_non_mov from commandbutton within w_inventario
end type
type cb_aggiorna from commandbutton within w_inventario
end type
type cbx_gen_mov from checkbox within w_inventario
end type
type dw_stringa_ricerca_prodotti from datawindow within w_inventario
end type
type st_5 from statictext within w_inventario
end type
type st_1 from statictext within w_inventario
end type
type st_ret_val_neg from statictext within w_inventario
end type
type st_ret_val_pos from statictext within w_inventario
end type
type st_2 from statictext within w_inventario
end type
type st_4 from statictext within w_inventario
end type
type st_cod_mov_ret_val_neg from statictext within w_inventario
end type
type st_cod_mov_ret_val_pos from statictext within w_inventario
end type
type cb_gen_mov_quan from commandbutton within w_inventario
end type
type cb_gen_mov_val from commandbutton within w_inventario
end type
type em_log from editmask within w_inventario
end type
type pb_path from picturebutton within w_inventario
end type
type st_10 from statictext within w_inventario
end type
type dw_ext_inventario_prodotto from uo_cs_xx_dw within w_inventario
end type
type gb_1 from groupbox within w_inventario
end type
type dw_inventario_stock_lista from uo_cs_xx_dw within w_inventario
end type
type st_3 from statictext within w_inventario
end type
type dw_conferma_inventario_ext from uo_cs_xx_dw within w_inventario
end type
type dw_folder from u_folder within w_inventario
end type
type dw_inventario_lista from uo_cs_xx_dw within w_inventario
end type
type dw_ext_genera_inventario from uo_cs_xx_dw within w_inventario
end type
end forward

global type w_inventario from w_cs_xx_principale
integer width = 3808
integer height = 1852
string title = "Inventario"
hpb_1 hpb_1
st_log st_log
cb_genera_inventario cb_genera_inventario
cb_cancella_2 cb_cancella_2
cb_cancella_1 cb_cancella_1
cb_stock_non_mov cb_stock_non_mov
cb_aggiorna cb_aggiorna
cbx_gen_mov cbx_gen_mov
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
st_5 st_5
st_1 st_1
st_ret_val_neg st_ret_val_neg
st_ret_val_pos st_ret_val_pos
st_2 st_2
st_4 st_4
st_cod_mov_ret_val_neg st_cod_mov_ret_val_neg
st_cod_mov_ret_val_pos st_cod_mov_ret_val_pos
cb_gen_mov_quan cb_gen_mov_quan
cb_gen_mov_val cb_gen_mov_val
em_log em_log
pb_path pb_path
st_10 st_10
dw_ext_inventario_prodotto dw_ext_inventario_prodotto
gb_1 gb_1
dw_inventario_stock_lista dw_inventario_stock_lista
st_3 st_3
dw_conferma_inventario_ext dw_conferma_inventario_ext
dw_folder dw_folder
dw_inventario_lista dw_inventario_lista
dw_ext_genera_inventario dw_ext_genera_inventario
end type
global w_inventario w_inventario

type variables
boolean ib_salta_controllo = false, ib_avvisa = false
end variables

forward prototypes
public function integer wf_mov_mag (datetime fdt_data_movimento, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, string fs_cod_tipo_movimento, decimal fd_quan_movimento, decimal fd_val_unit_movimento, ref long fl_anno_registrazione[], ref long fl_num_registrazione[], ref string fs_messaggio)
public function integer wf_rett_val ()
end prototypes

public function integer wf_mov_mag (datetime fdt_data_movimento, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock, string fs_cod_tipo_movimento, decimal fd_quan_movimento, decimal fd_val_unit_movimento, ref long fl_anno_registrazione[], ref long fl_num_registrazione[], ref string fs_messaggio);string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[]
long ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_null
datetime ldt_data_stock[], ldt_null

uo_magazzino luo_mag


setnull(ll_null)
setnull(ldt_null)
ls_cod_deposito[1] = fs_cod_deposito
ls_cod_ubicazione[1] = fs_cod_ubicazione
ls_cod_lotto[1] = fs_cod_lotto
ldt_data_stock[1] = fdt_data_stock
ll_prog_stock[1] = fl_prog_stock
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

if isnull(ls_cod_deposito[1]) then 
	fs_messaggio = "Deposito mancante nello stock "
	return -1
end if
if isnull(ls_cod_ubicazione[1]) then 
	fs_messaggio = "Ubicazione mancante nello stock "
	return -1
end if
if isnull(ls_cod_lotto[1]) then 
	fs_messaggio = "Lotto mancante nello stock "
	return -1
end if
if isnull(ldt_data_stock[1]) then 
	fs_messaggio = "Data stock mancante nello stock "
	return -1
end if
if isnull(ll_prog_stock[1]) then 
	fs_messaggio = "Progressivo lotto mancante nello stock "
	return -1
end if

if f_crea_dest_mov_magazzino (fs_cod_tipo_movimento, &
										fs_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock ) = -1 then
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 fs_cod_tipo_movimento, &
								 fs_cod_prodotto) = -1 then
	return -1
end if

luo_mag = create uo_magazzino

// *** imposto i flag solo se occorre saltare il controllo della giacenza del prodotto (senza assegnata e in spedizione)

if ib_salta_controllo then
	luo_mag.ib_salta_controllo = true
	if ib_avvisa then
		luo_mag.ib_avvisa = true
	end if
end if


if luo_mag.uof_movimenti_mag ( fdt_data_movimento, &
							fs_cod_tipo_movimento, &
							"S", &
							fs_cod_prodotto, &
							fd_quan_movimento, &
							fd_val_unit_movimento, &
							ll_null, &
							ldt_null, &
							"rett.inv.", &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							fl_anno_registrazione[], &
							fl_num_registrazione[] ) = 0 then
																		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
																											ll_num_reg_dest_stock) = -1 then
																			ROLLBACK;         // rollback della sol eliminazione dest_mov_magazzino
		
																end if
																
	destroy luo_mag
	
else
	destroy luo_mag
	return -1
end if

return 0
end function

public function integer wf_rett_val ();datetime	ldt_data_inventario, ldt_data_stock

long			ll_file, ll_count, ll_i, ll_prog_stock, ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], ll_vuoto[], ll_j

dec{4}		ld_valore_unitario, ld_valore_reale, ld_valore_rettifica, ld_val_med_stock, ld_quan_costo_medio_stock

string		ls_cod_mov_pos, ls_cod_mov_neg, ls_cod_prodotto, ls_cod_tipo_mov_mag, &
				ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_stringa_stock, ls_messaggio

datastore	lds_prodotti, lds_stock


select
	cod_tipo_mov_inv_ret_val_pos,
	cod_tipo_mov_inv_ret_val_neg
 into
 	:ls_cod_mov_pos,
	:ls_cod_mov_neg
 from
 	con_magazzino
where
	cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_cod_mov_pos) or isnull(ls_cod_mov_neg) then
	g_mb.messagebox("APICE","Impostare i tipi movimenti di rettifica in parametri magazzino!")
	return -1
end if

ll_file = fileopen(em_log.text,linemode!,write!,lockwrite!,replace!)

if ll_file = -1 then
	g_mb.messagebox("APICE","Impossibile aprire il file di log; verificare il percorso!")
	return -1
end if

filewrite(ll_file,"Generazione movimenti di inventario al " + string(today(),"dd/mm/yyyy") + "~r~n")

lds_prodotti = create datastore
lds_prodotti.dataobject = 'd_inventario_lista'
lds_prodotti.settransobject(sqlca)

lds_stock = create datastore
lds_stock.dataobject = 'd_inventario_stock_lista'
lds_stock.settransobject(sqlca)

//Lettura dall'inventario di tutti i prodotti non ancora generati ("N") ma impostati come da generare ("S")
ll_count = lds_prodotti.retrieve(s_cs_xx.cod_azienda, "N", "S")

for ll_i = 1 to ll_count
	
	ls_cod_prodotto = lds_prodotti.getitemstring(ll_i,"cod_prodotto")
	ldt_data_inventario = lds_prodotti.getitemdatetime(ll_i,"data_inventario")
	ld_valore_unitario = lds_prodotti.getitemnumber(ll_i,"valore_unitario")
	ld_valore_reale = lds_prodotti.getitemnumber(ll_i,"inventario_valore_reale")
	
	if ld_valore_reale = ld_valore_unitario then
		continue
	end if
	
	if lds_prodotti.getitemstring(ll_i,"flag_crea_movimento") <> "S" then
		continue
	end if
	
	if lds_prodotti.getitemstring(ll_i,"inventario_flag_tipo_valorizzazione") <> "M" then
		filewrite(ll_file,"~r~nIl prodotto " + ls_cod_prodotto + " non è valorizzato a costo medio ponderato")
		continue
	end if
	
	if lds_stock.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto) < 1 then
		filewrite(ll_file,"~r~nErrore in lettura elenco stock")
		continue
	end if
	
	for ll_j = 1 to lds_stock.rowcount()
	
		ls_cod_deposito = lds_stock.getitemstring(ll_j,"cod_deposito")
		ls_cod_ubicazione = lds_stock.getitemstring(ll_j,"cod_ubicazione")
		ls_cod_lotto = lds_stock.getitemstring(ll_j,"cod_lotto")
		ldt_data_stock = lds_stock.getitemdatetime(ll_j,"data_stock")
		ll_prog_stock = lds_stock.getitemnumber(ll_j,"prog_stock")
		
		ld_val_med_stock = lds_stock.getitemnumber(ll_j,"val_med_stock")
		ld_quan_costo_medio_stock = lds_stock.getitemnumber(ll_j,"quan_costo_medio_stock")
		
		if ld_val_med_stock = ld_valore_reale then
			continue
		end if
		
		ld_valore_rettifica = ld_valore_reale * ld_quan_costo_medio_stock - ld_val_med_stock * ld_quan_costo_medio_stock
		
		if ld_valore_rettifica > 0 then
			ls_cod_tipo_mov_mag = ls_cod_mov_pos
		else
			ls_cod_tipo_mov_mag = ls_cod_mov_neg
		end if
		
		ld_valore_rettifica = abs(ld_valore_rettifica)
		
		ls_stringa_stock = ls_cod_prodotto + "~t" +  ls_cod_deposito + "~t" +  ls_cod_ubicazione + "~t" +  ls_cod_lotto + &
							  "~t" + string(ldt_data_stock,"dd/mm/yyyy") + "~t" + string(ll_prog_stock)
		
		ll_anno_reg_mov_mag[] = ll_vuoto[]
		ll_num_reg_mov_mag[] = ll_vuoto[]
		
		if wf_mov_mag(ldt_data_inventario, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock,&
						  ls_cod_tipo_mov_mag, 1, ld_valore_rettifica, &
						  ref ll_anno_reg_mov_mag[], ref ll_num_reg_mov_mag[], ref ls_messaggio) <> 0 then
			filewrite(ll_file, "~r~nErrore in generazione rettifica valore lotto:" + ls_stringa_stock)
			rollback;
			continue
		else
			
			ls_messaggio = "~r~nMov. Rettifica Valore " + ls_cod_tipo_mov_mag + "~r~nValore = " + string(ld_valore_rettifica) + "~r~n" + &
								ls_stringa_stock + "~r~nREGISTRAZIONE MOVIMENTO " + string(ll_anno_reg_mov_mag[1]) + "/" + string(ll_num_reg_mov_mag[1]) +"~r~n" 
			
			filewrite(ll_file,ls_messaggio)
			
			update
				inventario
			set
				flag_gen_movimento = 'S'
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
					 
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in aggiornamento tabella INVENTARIO con il prodotto ~r~n" + ls_cod_prodotto + " L'elaborazione di questo lotto sarà saltata! "
				filewrite(ll_file,ls_messaggio)
				rollback;
				continue
			end if
			
			update
				inventario_stock
			set
				anno_reg_mov_mag = :ll_anno_reg_mov_mag[1], 
				num_reg_mov_mag  = :ll_num_reg_mov_mag[1]
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto  and
				cod_deposito = :ls_cod_deposito  and
				cod_ubicazione = :ls_cod_ubicazione and
				cod_lotto = :ls_cod_lotto  and
				data_stock = :ldt_data_stock and
				prog_stock = :ll_prog_stock;
			
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in aggiornamento tabella INVENTARIO STOCK con lotto  ~r~n" + ls_stringa_stock + " L'elaborazione di questo lotto sarà saltata! "
				filewrite(ll_file,ls_messaggio)
				rollback;
				continue
			end if
			
		end if
		
	next
	
	commit;
	
next

destroy lds_prodotti
destroy lds_stock
fileclose(ll_file)

return 0
end function

on w_inventario.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.st_log=create st_log
this.cb_genera_inventario=create cb_genera_inventario
this.cb_cancella_2=create cb_cancella_2
this.cb_cancella_1=create cb_cancella_1
this.cb_stock_non_mov=create cb_stock_non_mov
this.cb_aggiorna=create cb_aggiorna
this.cbx_gen_mov=create cbx_gen_mov
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.st_5=create st_5
this.st_1=create st_1
this.st_ret_val_neg=create st_ret_val_neg
this.st_ret_val_pos=create st_ret_val_pos
this.st_2=create st_2
this.st_4=create st_4
this.st_cod_mov_ret_val_neg=create st_cod_mov_ret_val_neg
this.st_cod_mov_ret_val_pos=create st_cod_mov_ret_val_pos
this.cb_gen_mov_quan=create cb_gen_mov_quan
this.cb_gen_mov_val=create cb_gen_mov_val
this.em_log=create em_log
this.pb_path=create pb_path
this.st_10=create st_10
this.dw_ext_inventario_prodotto=create dw_ext_inventario_prodotto
this.gb_1=create gb_1
this.dw_inventario_stock_lista=create dw_inventario_stock_lista
this.st_3=create st_3
this.dw_conferma_inventario_ext=create dw_conferma_inventario_ext
this.dw_folder=create dw_folder
this.dw_inventario_lista=create dw_inventario_lista
this.dw_ext_genera_inventario=create dw_ext_genera_inventario
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.st_log
this.Control[iCurrent+3]=this.cb_genera_inventario
this.Control[iCurrent+4]=this.cb_cancella_2
this.Control[iCurrent+5]=this.cb_cancella_1
this.Control[iCurrent+6]=this.cb_stock_non_mov
this.Control[iCurrent+7]=this.cb_aggiorna
this.Control[iCurrent+8]=this.cbx_gen_mov
this.Control[iCurrent+9]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+10]=this.st_5
this.Control[iCurrent+11]=this.st_1
this.Control[iCurrent+12]=this.st_ret_val_neg
this.Control[iCurrent+13]=this.st_ret_val_pos
this.Control[iCurrent+14]=this.st_2
this.Control[iCurrent+15]=this.st_4
this.Control[iCurrent+16]=this.st_cod_mov_ret_val_neg
this.Control[iCurrent+17]=this.st_cod_mov_ret_val_pos
this.Control[iCurrent+18]=this.cb_gen_mov_quan
this.Control[iCurrent+19]=this.cb_gen_mov_val
this.Control[iCurrent+20]=this.em_log
this.Control[iCurrent+21]=this.pb_path
this.Control[iCurrent+22]=this.st_10
this.Control[iCurrent+23]=this.dw_ext_inventario_prodotto
this.Control[iCurrent+24]=this.gb_1
this.Control[iCurrent+25]=this.dw_inventario_stock_lista
this.Control[iCurrent+26]=this.st_3
this.Control[iCurrent+27]=this.dw_conferma_inventario_ext
this.Control[iCurrent+28]=this.dw_folder
this.Control[iCurrent+29]=this.dw_inventario_lista
this.Control[iCurrent+30]=this.dw_ext_genera_inventario
end on

on w_inventario.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.st_log)
destroy(this.cb_genera_inventario)
destroy(this.cb_cancella_2)
destroy(this.cb_cancella_1)
destroy(this.cb_stock_non_mov)
destroy(this.cb_aggiorna)
destroy(this.cbx_gen_mov)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.st_5)
destroy(this.st_1)
destroy(this.st_ret_val_neg)
destroy(this.st_ret_val_pos)
destroy(this.st_2)
destroy(this.st_4)
destroy(this.st_cod_mov_ret_val_neg)
destroy(this.st_cod_mov_ret_val_pos)
destroy(this.cb_gen_mov_quan)
destroy(this.cb_gen_mov_val)
destroy(this.em_log)
destroy(this.pb_path)
destroy(this.st_10)
destroy(this.dw_ext_inventario_prodotto)
destroy(this.gb_1)
destroy(this.dw_inventario_stock_lista)
destroy(this.st_3)
destroy(this.dw_conferma_inventario_ext)
destroy(this.dw_folder)
destroy(this.dw_inventario_lista)
destroy(this.dw_ext_genera_inventario)
end on

event pc_setwindow;call super::pc_setwindow;string					ls_cod_mov_pos, ls_cod_mov_neg, ls_des_mov, ls_cod_mov_ret_val_pos, ls_cod_mov_ret_val_neg

windowobject 		lw_oggetti[], lw_vuoto[]

set_w_options(c_closenosave)

dw_inventario_lista.set_dw_key("cod_azienda")
dw_inventario_stock_lista.set_dw_key("cod_azienda")
dw_inventario_stock_lista.set_dw_key("cod_prodotto")

dw_inventario_lista.set_dw_options(sqlca, &
												pcca.null_object, &
												c_noretrieveonopen, &
												c_default)
												
dw_inventario_stock_lista.set_dw_options(sqlca, &
												dw_inventario_lista, &
                                    c_scrollparent + c_noretrieveonopen, &
                                    c_ViewModeBorderUnchanged + &
												c_NoHighlightSelected + &
												c_InactiveDWColorUnchanged)

dw_ext_genera_inventario.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
dw_ext_inventario_prodotto.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_conferma_inventario_ext.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

lw_oggetti[1] = dw_inventario_stock_lista
dw_folder.fu_assigntab(3, "Stock.", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_ext_inventario_prodotto
dw_folder.fu_assigntab(1, "Inventario", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_inventario_lista
lw_oggetti[2] = cb_aggiorna
lw_oggetti[3] = cbx_gen_mov
lw_oggetti[4] = dw_stringa_ricerca_prodotti
lw_oggetti[5] = st_5
dw_folder.fu_assigntab(2, "Movimenti", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_ext_genera_inventario
lw_oggetti[2] = cb_genera_inventario
lw_oggetti[3] = cb_cancella_1
lw_oggetti[4] = cb_cancella_2
lw_oggetti[5] = cb_stock_non_mov
lw_oggetti[6] = hpb_1
dw_folder.fu_assigntab(4, "Genera Inventario", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = gb_1
lw_oggetti[2] = st_1
lw_oggetti[3] = st_2
lw_oggetti[4] = st_3
lw_oggetti[5] = st_4
lw_oggetti[6] = st_10
lw_oggetti[7] = em_log
lw_oggetti[8] = pb_path
lw_oggetti[9] = cb_gen_mov_quan
lw_oggetti[10] = cb_gen_mov_val
lw_oggetti[11] = st_ret_val_pos
lw_oggetti[12] = st_cod_mov_ret_val_pos
lw_oggetti[13] = st_ret_val_neg
lw_oggetti[14] = st_cod_mov_ret_val_neg
lw_oggetti[15] = dw_conferma_inventario_ext
dw_folder.fu_assigntab(5, "Conferma Inventario", lw_oggetti[])


dw_folder.fu_foldercreate(5, 5)
dw_folder.fu_selecttab(1)

dw_stringa_ricerca_prodotti.insertrow(0)
dw_ext_genera_inventario.resetupdate()
dw_ext_inventario_prodotto.resetupdate()
em_log.text = "C:\INVENTARIO.LOG"

// --------------------------------------------

select cod_tipo_mov_inv_ret_pos,   
		 cod_tipo_mov_inv_ret_neg,
		 cod_tipo_mov_inv_ret_val_pos,   
		 cod_tipo_mov_inv_ret_val_neg
 into :ls_cod_mov_pos,   
		:ls_cod_mov_neg,
		:ls_cod_mov_ret_val_pos,
		:ls_cod_mov_ret_val_neg
 from  con_magazzino  
where  cod_azienda = :s_cs_xx.cod_azienda   ;


if not isnull(ls_cod_mov_pos) then
	select des_tipo_movimento  
	into   :ls_des_mov  
	from   tab_tipi_movimenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
		    cod_tipo_movimento = :ls_cod_mov_pos ;
	st_2.text = ls_cod_mov_pos + " - " + ls_des_mov
else
	st_2.text = "non impostato"
end if

if not isnull(ls_cod_mov_neg) then
	select tab_tipi_movimenti.des_tipo_movimento  
	into   :ls_des_mov  
	from   tab_tipi_movimenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_movimento = :ls_cod_mov_neg ;
	st_4.text = ls_cod_mov_neg + " - " + ls_des_mov
else
	st_4.text = "non impostato"
end if

// --------- Michele 12/05/2004 Lettura tipi mov X rettifiche valore ----------

if not isnull(ls_cod_mov_ret_val_pos) then
	select des_tipo_movimento  
	into   :ls_des_mov
	from   tab_tipi_movimenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
		    cod_tipo_movimento = :ls_cod_mov_ret_val_pos ;
	st_cod_mov_ret_val_pos.text = ls_cod_mov_ret_val_pos + " - " + ls_des_mov
else
	st_cod_mov_ret_val_pos.text = "non impostato"
end if

if not isnull(ls_cod_mov_ret_val_neg) then
	select des_tipo_movimento  
	into   :ls_des_mov
	from   tab_tipi_movimenti  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
		    cod_tipo_movimento = :ls_cod_mov_ret_val_neg ;
	st_cod_mov_ret_val_neg.text = ls_cod_mov_ret_val_neg + " - " + ls_des_mov
else
	st_cod_mov_ret_val_neg.text = "non impostato"
end if
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_genera_inventario,"cod_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco <> 'S'")

f_po_loaddddw_dw(dw_ext_genera_inventario,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type hpb_1 from hprogressbar within w_inventario
integer x = 69
integer y = 1168
integer width = 3634
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_log from statictext within w_inventario
integer x = 91
integer y = 1584
integer width = 2683
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_genera_inventario from commandbutton within w_inventario
integer x = 3269
integer y = 1020
integer width = 411
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera Inv."
end type

event clicked;string			ls_sql, ls_cod_prodotto, ls_where, ls_chiave[], ls_errore, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &	
					ls_data_stock, ls_prog_stock, ls_vuoto[], ls_flag_prodotti_movimentati, ls_flag_tipo_valorizzazione, ls_deposito_filtro, ls_err
					
long				ll_i, ll_pos, ll_pos_old, ll_row,ll_prog_stock, ll_ret, ll_secondi, ll_tot, ll_index, ll_count

datetime		ldt_data_inventario, ldt_data_stock

dec{4}			ld_quan[], ld_giacenza_stock[], ld_giacenza_prodotto,ld_quan_inventario, ld_vuoto[], ld_costo, ld_quan_costo_medio, &
					ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
					
time				lt_inizio, lt_fine

uo_magazzino luo_mag

datastore		lds_data


dw_ext_genera_inventario.accepttext()
hpb_1.position = 0

ldt_data_inventario = dw_ext_genera_inventario.getitemdatetime(1,"data_inventario")

if isnull(ldt_data_inventario) or ldt_data_inventario <= datetime(date(01/01/1900),00:00:00) then
	g_mb.error("APICE","La data inventario è obbligatoria")
	return
end if


ls_sql = "select cod_prodotto from anag_prodotti "
//ls_sql_count = "select count(*) from anag_prodotti "

ls_where = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(dw_ext_genera_inventario.getitemstring(1,"cod_prodotto_inizio")) then
	ls_where += " and cod_prodotto >= '" + dw_ext_genera_inventario.getitemstring(1,"cod_prodotto_inizio") + "' "
end if

if not isnull(dw_ext_genera_inventario.getitemstring(1,"cod_prodotto_fine")) then
	ls_where += " and cod_prodotto <= '" + dw_ext_genera_inventario.getitemstring(1,"cod_prodotto_fine") + "' "
end if

if not isnull(dw_ext_genera_inventario.getitemstring(1,"cod_comodo")) then
	ls_where += " and cod_comodo like '" + dw_ext_genera_inventario.getitemstring(1,"cod_comodo") + "' "
end if

if not isnull(dw_ext_genera_inventario.getitemstring(1,"cod_comodo_2")) then
	ls_where += " and cod_comodo_2 like '" + dw_ext_genera_inventario.getitemstring(1,"cod_comodo_2") + "' "
end if

if not isnull(dw_ext_genera_inventario.getitemstring(1,"cod_cat_mer")) then
	ls_where += " and cod_cat_mer = '" + dw_ext_genera_inventario.getitemstring(1,"cod_cat_mer") + "' "
end if

if dw_ext_genera_inventario.getitemstring(1,"flag_classe") <> "T" then
	ls_where += " and flag_classe_abc = '" + dw_ext_genera_inventario.getitemstring(1,"flag_classe") + "' "
end if

if dw_ext_genera_inventario.getitemstring(1,"flag_lifo") <> "N" then
	ls_where += " and flag_lifo = '" + dw_ext_genera_inventario.getitemstring(1,"flag_lifo") + "' "
end if

if dw_ext_genera_inventario.getitemstring(1,"flag_fiscali") = "S" then
	ls_where += " and flag_articolo_fiscale = 'S' "
end if

ls_deposito_filtro = dw_ext_genera_inventario.getitemstring(1,"cod_deposito")

if not isnull(ls_deposito_filtro) and ls_deposito_filtro<>"" then
	ls_where += " and exists (select cod_prodotto from mov_magazzino where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = anag_prodotti.cod_prodotto and cod_deposito = '" + ls_deposito_filtro + "')"
end if


ls_sql += ls_where
//ls_sql_count += ls_where

ls_where = ""

st_log.text= "Preparazione query in corso ..."

//conteggio
ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_err)

if ll_tot<= 0 then
	g_mb.error("APICE","Errore creazione datastore: "+ls_err)
	return
	
elseif ll_tot=0 then
	g_mb.warning("APICE","Nessun prodotto da elaborare!")
	destroy lds_data
	return
end if

hpb_1.maxposition = ll_tot
hpb_1.setstep = 1
ll_index = 0


ls_flag_prodotti_movimentati = dw_ext_genera_inventario.getitemstring(1,"flag_prodotti_movimentati")
ls_flag_tipo_valorizzazione = dw_ext_genera_inventario.getitemstring(1,"flag_tipo_valorizzazione")


//declare cu_inventario DYNAMIC CURSOR FOR SQLSA ;
//PREPARE SQLSA FROM :ls_sql;
//OPEN DYNAMIC cu_inventario ;
//if sqlca.sqlcode <> 0 then
//	g_mb.error("APICE","Errore in OPEN cursore cu_inventario." + sqlca.sqlerrtext)
//	rollback;
//	return
//end if

lt_inizio = now()

luo_mag = create uo_magazzino



if not isnull(ls_deposito_filtro) and ls_deposito_filtro<>"" then
	luo_mag.is_cod_depositi_in = "'" + ls_deposito_filtro + "'"
	luo_mag.is_considera_depositi_fornitori="I"
end if



//do while true
for ll_index=1 to ll_tot
	
//	fetch cu_inventario into :ls_cod_prodotto;
//	if sqlca.sqlcode = -1 then
//		g_mb.error("APICE","Errore in fetch cursore cu_inventario." + sqlca.sqlerrtext)
//		close cu_inventario;
//		destroy luo_mag
//		rollback;
//	end if
//	if sqlca.sqlcode = 100 then exit
	
	Yield()
	hpb_1.stepit()
	ls_cod_prodotto = lds_data.getitemstring(ll_index, "cod_prodotto")
	
	ls_err = string((ll_index / ll_tot) * 100, "##0") + " % : Elaborazione Prodotto " + ls_cod_prodotto
	st_log.text= ls_err + " in corso ..."
	
	
	ld_quan[] = ld_vuoto[]
	ld_giacenza_stock[] = ld_vuoto[]
	ls_chiave[] = ls_vuoto[]
	
	ll_ret = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_inventario, ls_where, ld_quan[], ls_errore, "S", ls_chiave[],ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	
	

	if ls_flag_prodotti_movimentati = "N" then
		if upperbound(ld_quan) > 0 then
			if (ld_quan[1] = 0 or isnull(ld_quan[1])) and (ld_quan[4] = 0 or isnull(ld_quan[4])) and (ld_quan[6] = 0 or isnull(ld_quan[6])) then continue
		end if
	end if
	
	if isnull(ls_deposito_filtro) then
		ld_quan_inventario = ld_quan[1] + ld_quan[4] - ld_quan[6]
	else
		ld_quan_inventario = 0
		for ll_i = 1 to upperbound(ld_giacenza_stock)
			if not isnull(ld_giacenza_stock[ll_i]) and mid(ls_chiave[ll_i],1,pos(ls_chiave[ll_i],"-",1) - 1) = ls_deposito_filtro then
				ld_quan_inventario += ld_giacenza_stock[ll_i]
			end if
		next
	end if
	
	// valorizzo il prodotto	
	choose case ls_flag_tipo_valorizzazione
			
		case "S" 										// costo standard
			select costo_standard
			into   :ld_costo
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <>0 then
				g_mb.error("APICE","Errore in ricerca del costo standard del prodotto " + ls_cod_prodotto+ "~r~n" + sqlca.sqlerrtext )
				continue
			end if
			if isnull(ld_costo) then ld_costo = 0
			
			setnull(ld_quan_costo_medio)
			
		case  "A" 												//ultimo costo acquisto
			// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
			if ld_quan[14] > 0 then
				ld_costo = ld_quan[14]
			else
				ld_costo = 0
			end if
			
			setnull(ld_quan_costo_medio)
			
		case  "M" 												//costo medio
			if ld_quan[12] > 0 then
				ld_costo = ld_quan[13] / ld_quan[12]
			else
				
				select costo_medio_ponderato
				into   :ld_costo
				from   lifo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 anno_lifo = (select max(anno_lifo)
						 				  from   lifo
										  where  cod_azienda = :s_cs_xx.cod_azienda and
										  		 	cod_prodotto = :ls_cod_prodotto);
														
				if sqlca.sqlcode < 0 then
					g_mb.error("APICE","Errore in ricerca del costo medio del prodotto " + ls_cod_prodotto+ " in tabella LIFO~r~n" + sqlca.sqlerrtext)
					continue
					
				elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
					ld_costo = 0
					
				end if
				
			end if
			
			ld_quan_costo_medio = ld_quan[12]
			
		case "N"
			ld_costo = 0
			
			setnull(ld_quan_costo_medio)
			
	end choose
	
	// procedo con insert in tabella inventario
	st_log.text= ls_err + " inserimento in tabella inventario in corso ..."
	
	setnull(ll_count)
	
	select count(*)
	into :ll_count
	from inventario
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode=0 and ll_count=1 then
		
		UPDATE inventario
			SET	data_inventario					= :ldt_data_inventario,
					giacenza_inventario			= :ld_quan_inventario,
					giacenza_reale					= :ld_quan_inventario,
					flag_crea_movimento			= 'S',
					flag_gen_movimento			= 'N',
					flag_stock							= 'N',
					nota_inventario					= null,
					valore_unitario					= :ld_costo,
					valore_reale						= :ld_costo,
					flag_tipo_valorizzazione		= :ls_flag_tipo_valorizzazione,
					cod_deposito						= :ls_deposito_filtro
		WHERE 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_prodotto;
		
	else
	
		INSERT INTO inventario  
				( cod_azienda,   
				  cod_prodotto,   
				  data_inventario,   
				  giacenza_inventario,   
				  giacenza_reale,   
				  flag_crea_movimento,   
				  flag_gen_movimento,   
				  flag_stock,   
				  nota_inventario,   
				  valore_unitario,
				  valore_reale,
				  flag_tipo_valorizzazione,
				  cod_deposito)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_prodotto,   
				  :ldt_data_inventario,   
				  :ld_quan_inventario,   
				  :ld_quan_inventario,   
				  'S',   
				  'N',   
				  'N',   
				  null,   
				  :ld_costo,
				  :ld_costo,
				  :ls_flag_tipo_valorizzazione,
				  :ls_deposito_filtro)  ;
				  
	end if
				  
	if sqlca.sqlcode <> 0 then
		g_mb.error("APICE","Errore in inserimento dati in inventario nel prodotto "+ ls_cod_prodotto +".~r~nForse il prodotto è già presente nell'inventario.~r~nQuesto prodotto sarà saltato.~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if

	for ll_i = 1 to upperbound(ls_chiave[])
		if len(ls_chiave[ll_i]) > 0 and not isnull(ls_chiave[ll_i]) then
			ll_pos = pos(ls_chiave[ll_i],"-",1)
			ls_cod_deposito = mid(ls_chiave[ll_i],1,ll_pos - 1)
			
			if not isnull(ls_deposito_filtro) and isnull(ls_cod_deposito) or ls_cod_deposito <> ls_deposito_filtro then
				continue
			end if
			
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_i],"-",ll_pos_old)
			ls_cod_ubicazione = mid(ls_chiave[ll_i],ll_pos_old,ll_pos - ll_pos_old)
			
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_i],"-",ll_pos_old)
			ls_cod_lotto = mid(ls_chiave[ll_i],ll_pos_old,ll_pos - ll_pos_old)
		
			ll_pos_old = ll_pos + 1
			ll_pos = pos(ls_chiave[ll_i],"-",ll_pos_old)
			ls_data_stock = mid(ls_chiave[ll_i],ll_pos_old,ll_pos - ll_pos_old)
			
			ll_pos_old = ll_pos + 1
			ls_prog_stock = mid(ls_chiave[ll_i],ll_pos_old)
			
			ldt_data_stock = datetime(date(ls_data_stock),00:00:00)	
			ll_prog_stock = long(ls_prog_stock)
			
			st_log.text= ls_err + " inserimento in tabella inventario_stock in corso ..."
			
			setnull(ll_count)
	
			select count(*)
			into :ll_count
			from inventario_stock
			where 	cod_azienda			= :s_cs_xx.cod_azienda and
						cod_prodotto			= :ls_cod_prodotto and
						cod_deposito			= :ls_cod_deposito and
						cod_ubicazione		= :ls_cod_ubicazione and
						cod_lotto				= :ls_cod_lotto and
						data_stock				= :ldt_data_stock and
						prog_stock				= :ll_prog_stock;
			
			if sqlca.sqlcode=0 and ll_count=1 then
				
				UPDATE inventario_stock
					SET	anno_reg_mov_mag			= null,
							num_reg_mov_mag				= null,
							giacenza_inventario			= :ld_giacenza_stock[ll_i],
							giacenza_reale					= :ld_giacenza_stock[ll_i], 
							val_med_stock					= :ld_costo_medio_stock[ll_i],
							quan_costo_medio_stock		= :ld_quan_costo_medio_stock[ll_i]
					WHERE	cod_azienda			= :s_cs_xx.cod_azienda and
								cod_prodotto			= :ls_cod_prodotto and
								cod_deposito			= :ls_cod_deposito and
								cod_ubicazione		= :ls_cod_ubicazione and
								cod_lotto				= :ls_cod_lotto and
								data_stock				= :ldt_data_stock and
								prog_stock				= :ll_prog_stock;
				
			else
			
				INSERT INTO inventario_stock  
						( cod_azienda,   
						  cod_prodotto,   
						  cod_deposito,   
						  cod_ubicazione,   
						  cod_lotto,   
						  data_stock,   
						  prog_stock,   
						  anno_reg_mov_mag,   
						  num_reg_mov_mag,   
						  giacenza_inventario,   
						  giacenza_reale,
						  val_med_stock,
						  quan_costo_medio_stock)  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ls_cod_prodotto,   
						  :ls_cod_deposito,   
						  :ls_cod_ubicazione,   
						  :ls_cod_lotto,   
						  :ldt_data_stock,   
						  :ll_prog_stock,   
						  null,   
						  null,   
						  :ld_giacenza_stock[ll_i],   
						  :ld_giacenza_stock[ll_i],
						  :ld_costo_medio_stock[ll_i],
						  :ld_quan_costo_medio_stock[ll_i]);
			
			end if
			
			if sqlca.sqlcode <> 0 then
				g_mb.error("APICE","Errore in inserimento dati in inventario stock del prodotto "+ ls_cod_prodotto +".~r~nGli stock del prodotto saranno saltati.~r~n" + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
		end if
	next
	
	commit;
	
//loop
next

destroy luo_mag

lt_fine = now()

ll_secondi = secondsafter(lt_inizio, lt_fine)

st_log.text= "Elaborazione Terminata in " + string( ll_secondi ) + " sec."

commit;

//close cu_inventario;
destroy lds_data

end event

type cb_cancella_2 from commandbutton within w_inventario
integer x = 3246
integer y = 1580
integer width = 421
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera TUTTO"
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di volere eliminare tutti i dati degli inventari ?",question!,yesno!,2) = 2 then return

delete from inventario_stock
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 then
	delete from inventario
	where  cod_azienda = :s_cs_xx.cod_azienda;
	if sqlca.sqlcode = 0 then
		commit;
	else
		g_mb.messagebox("APICE","Errore in cancellazione inventario ~r~n" + sqlca.sqlerrtext)
		rollback;
	end if
else
	g_mb.messagebox("APICE","Errore in cancellazione inventario stock~r~n" + sqlca.sqlerrtext)
	rollback;
end if

								
end event

type cb_cancella_1 from commandbutton within w_inventario
integer x = 2789
integer y = 1580
integer width = 434
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina Mov."
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di volere eliminare l'inventario dei prodotti NON CONFERMATI ?",question!,yesno!,2) = 2 then return

delete from inventario_stock
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_prodotto in (
								select cod_prodotto
								from   inventario
								where  cod_azienda = :s_cs_xx.cod_azienda and
								       flag_gen_movimento = 'N'
										 );

if sqlca.sqlcode = 0 then
	delete from inventario
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 flag_gen_movimento = 'N';
	if sqlca.sqlcode = 0 then
		commit;
	else
		g_mb.messagebox("APICE","Errore in cancellazione inventario ~r~n" + sqlca.sqlerrtext)
		rollback;
	end if
else
	g_mb.messagebox("APICE","Errore in cancellazione inventario stock~r~n" + sqlca.sqlerrtext)
	rollback;
end if

								
end event

type cb_stock_non_mov from commandbutton within w_inventario
integer x = 2834
integer y = 1020
integer width = 411
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Agg. Stock"
end type

event clicked;datetime ldt_data_stock

long 		ll_i, ll_count, ll_j, ll_prog_stock

string 	ls_cod_prodotto, ls_sql, ls_errore, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto

datastore lds_prodotti, lds_stock


if g_mb.messagebox("APICE","Aggiungere gli stock non movimentati ai prodotti senza stock?",question!,yesno!,2) = 2 then
	return -1
end if

setpointer(hourglass!)

ls_sql = "select cod_prodotto from inventario where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_gen_movimento = 'N' and cod_prodotto not in (select cod_prodotto from inventario_stock where cod_azienda = '" + s_cs_xx.cod_azienda + "')"
	
setnull(ls_errore)

ls_sql = sqlca.syntaxfromsql(ls_sql,"style(type=grid)",ls_errore)

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	return -1
end if

lds_prodotti = create datastore

if lds_prodotti.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	destroy lds_stock
	return -1
end if

if lds_prodotti.settransobject(sqlca) = -1 then
	g_mb.messagebox("APICE","Errore in impostazione transazione",stopsign!)
	destroy lds_prodotti
	return -1
end if

if lds_prodotti.retrieve() = -1 then
	g_mb.messagebox("APICE","Errore in lettura dati",stopsign!)
	destroy lds_prodotti
	return -1
end if

for ll_i = 1 to lds_prodotti.rowcount()
	
	ls_cod_prodotto = lds_prodotti.getitemstring(ll_i,"cod_prodotto")
	
	ls_sql = "select cod_deposito, cod_ubicazione, cod_lotto, data_stock, prog_stock from stock where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' "
	
	setnull(ls_errore)
	
	ls_sql = sqlca.syntaxfromsql(ls_sql,"style(type=grid)",ls_errore)
	
	if ls_sql = "" and not isnull(ls_errore) then
		g_mb.messagebox("APICE",ls_errore,stopsign!)
		destroy lds_prodotti
		return -1
	end if
	
	lds_stock = create datastore
	
	if lds_stock.create(ls_sql,ls_errore) = -1 then
		g_mb.messagebox("APICE",ls_errore,stopsign!)
		destroy lds_stock
		destroy lds_prodotti
		return -1
	end if
	
	if lds_stock.settransobject(sqlca) = -1 then
		g_mb.messagebox("APICE","Errore in impostazione transazione",stopsign!)
		destroy lds_stock
		destroy lds_prodotti
		return -1
	end if
	
	if lds_stock.retrieve() = -1 then
		g_mb.messagebox("APICE","Errore in lettura dati",stopsign!)
		destroy lds_stock
		destroy lds_prodotti
		return -1
	end if
	
	for ll_j = 1 to lds_stock.rowcount()
		
		ls_cod_deposito = lds_stock.getitemstring(ll_j,"cod_deposito")
		
		ls_cod_ubicazione = lds_stock.getitemstring(ll_j,"cod_ubicazione")
		
		ls_cod_lotto = lds_stock.getitemstring(ll_j,"cod_lotto")
		
		ldt_data_stock = lds_stock.getitemdatetime(ll_j,"data_stock")
		
		ll_prog_stock = lds_stock.getitemnumber(ll_j,"prog_stock")
		
		insert into
			inventario_stock
			(cod_azienda,
			cod_prodotto,
			cod_deposito,
			cod_ubicazione,
			cod_lotto,
			data_stock,
			prog_stock,
			anno_reg_mov_mag,
			num_reg_mov_mag,
			giacenza_inventario,
			giacenza_reale)
		values
			(:s_cs_xx.cod_azienda,
			:ls_cod_prodotto,
			:ls_cod_deposito,
			:ls_cod_ubicazione,
			:ls_cod_lotto,
			:ldt_data_stock,
			:ll_prog_stock,
			null,
			null,
			null,
			null);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento stock: " + sqlca.sqlerrtext,stopsign!)
			rollback;
			destroy lds_stock
			destroy lds_prodotti
			return -1
		end if
		
	next
	
	commit;
	
	destroy lds_stock
	
next

destroy lds_prodotti
end event

type cb_aggiorna from commandbutton within w_inventario
integer x = 937
integer y = 140
integer width = 389
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_inventario_lista.change_dw_current()
iuo_dw_main = dw_inventario_lista

parent.triggerevent("pc_retrieve")
end event

type cbx_gen_mov from checkbox within w_inventario
integer x = 69
integer y = 140
integer width = 800
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Vis.Movimenti Generati"
boolean lefttext = true
boolean threestate = true
end type

type dw_stringa_ricerca_prodotti from datawindow within w_inventario
event ue_key pbm_dwnkey
integer x = 1966
integer y = 120
integer width = 800
integer height = 100
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
boolean border = false
end type

event ue_key;long ll_riga
string ls_stringa
if key = keyenter! then
	ls_stringa = dw_stringa_ricerca_prodotti.gettext()
	ll_riga = dw_inventario_lista.find("cod_prodotto like '" + ls_stringa + "'", 1, dw_inventario_lista.rowcount())
	if ll_riga > 0 then
		dw_inventario_lista.scrolltorow(ll_riga)
		dw_inventario_lista.setfocus()
	end if
end if
end event

event getfocus;string ls_str

if getrow() > 0 then
	ls_str = getitemstring(1,"stringa_ricerca")
	this.setcolumn("stringa_ricerca")
	this.selecttext(1,len(ls_str))
end if
end event

type st_5 from statictext within w_inventario
integer x = 1417
integer y = 140
integer width = 521
integer height = 64
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ricerca Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_inventario
integer x = 411
integer y = 380
integer width = 1326
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOVIMENTO RETTIFICA IN AUMENTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_val_neg from statictext within w_inventario
integer x = 329
integer y = 760
integer width = 1408
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOVIMENTO RETTIFICA VALORE IN DIMINUZIONE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_ret_val_pos from statictext within w_inventario
integer x = 407
integer y = 640
integer width = 1330
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOVIMENTO RETTIFICA VALORE IN AUMENTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_inventario
integer x = 1783
integer y = 380
integer width = 1783
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_4 from statictext within w_inventario
integer x = 1783
integer y = 500
integer width = 1783
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_cod_mov_ret_val_neg from statictext within w_inventario
integer x = 1783
integer y = 760
integer width = 1783
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_cod_mov_ret_val_pos from statictext within w_inventario
integer x = 1783
integer y = 640
integer width = 1783
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_gen_mov_quan from commandbutton within w_inventario
integer x = 1440
integer y = 1060
integer width = 480
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. Mov. Q.tà"
end type

event clicked;boolean   lb_exit, lb_errore, lb_controlla = false

string    ls_cod_prodotto, ls_flag_crea_movimento, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_mov_pos, ls_cod_mov_neg, &
          ls_cod_tipo_mov_mag, ls_sort, ls_messaggio, ls_cod_tipo_mov_stock, ls_stringa_stock, ls_flag_negativo, ls_flag_avvisa_singolo, ls_flag_salta, ls_flag_avvisa

long      ll_FileNum, ll_righe, ll_i, ll_y, ll_prog_stock, ll_righe_stock,ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], &
          ll_vuoto[]

dec{4}    ld_giacenza_reale, ld_giacenza_inventario,ld_valore_unitario, ld_quantita_rettifica, ld_quan_stock_inventario, &
          ld_quan_residuo, ld_quan_movimento, ld_quan_assegnata, ld_quan_in_spedizione, ld_costo_medio_stock, ld_quantita_da_controllare

datetime  ldt_data_inventario, ldt_data_stock

datastore lds_prodotti, lds_stock

ll_FileNum = FileOpen(em_log.text,LineMode!, Write!, LockWrite!, Replace!)
if ll_FileNum = -1 then
	g_mb.messagebox("APICE","Impossibile aprire il file di log; verificare il percorso!")
	return
end if
filewrite(ll_filenum,"")
fileclose(ll_filenum)

select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 or ls_flag_negativo <> 'S' or isnull(ls_flag_negativo) then
	ls_flag_negativo = 'N'
end if

select cod_tipo_mov_inv_ret_pos,   
		 cod_tipo_mov_inv_ret_neg  
into   :ls_cod_mov_pos,   
		 :ls_cod_mov_neg  
from   con_magazzino  
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 or isnull(ls_cod_mov_pos) or isnull(ls_cod_mov_neg) then
	g_mb.messagebox("APICE","Importare i tipi movimenti di rettifica in parametri magazzino !")
	return
end if

ll_FileNum = FileOpen(em_log.text,LineMode!, Write!, LockWrite!, Append!)

filewrite(ll_filenum,"Generazione movimenti di inventario al " + string(today(),"dd/mm/yyyy hh:mm:ss") + "~r~n")

// *** michela 08/07/2005: su richiesta di marco lazzari per unifast (ma in generale) si aggiunge la possibilità di considerare
//                         oppure no le quantita (impegnata e in spedizione) durante il ricalcolo della quantità a magazzino


ls_flag_salta = dw_conferma_inventario_ext.getitemstring( 1, "flag_salta")
ls_flag_avvisa = dw_conferma_inventario_ext.getitemstring( 1, "flag_avvisa")
ls_flag_avvisa_singolo = dw_conferma_inventario_ext.getitemstring( 1, "flag_avvisa_singolo")

if isnull(ls_flag_salta) then ls_flag_salta = "N"
if isnull(ls_flag_avvisa) then ls_flag_avvisa = "N"
if isnull(ls_flag_avvisa_singolo) then ls_flag_avvisa_singolo = "N"

if ls_flag_salta = "S" then
	if ls_flag_avvisa = "S" then
		if isnull(ls_flag_avvisa_singolo) then
			ls_flag_avvisa_singolo = "N" 
		end if
	elseif ls_flag_avvisa = "N" then
		ls_flag_avvisa_singolo = "N" 
	end if
elseif ls_flag_salta = "N" then
	ls_flag_avvisa = "N" 
	ls_flag_avvisa_singolo = "N"
else
	ls_flag_salta = "N"
	ls_flag_avvisa = "N" 
	ls_flag_avvisa_singolo = "N"	
end if

// ***

lds_prodotti = CREATE datastore
lds_prodotti.dataobject = 'd_inventario_lista'
lds_prodotti.settransobject(sqlca)

lds_stock = CREATE datastore
lds_stock.dataobject = 'd_inventario_stock_lista'
lds_stock.settransobject(sqlca)

// Retrieve dall'inventario di tutti i prodotti non ancora generati ("N") ma impostati come da generare ("S")
ll_righe = lds_prodotti.retrieve(s_cs_xx.cod_azienda, "N", "S")

for ll_i = 1 to ll_righe

	if lds_prodotti.getitemstring(ll_i,"flag_stock") = "N" then
		
		ls_cod_prodotto = lds_prodotti.getitemstring(ll_i,"cod_prodotto")
		ldt_data_inventario = lds_prodotti.getitemdatetime(ll_i,"data_inventario")
		ld_giacenza_reale = lds_prodotti.getitemnumber(ll_i,"giacenza_reale")
		ld_giacenza_inventario = lds_prodotti.getitemnumber(ll_i,"giacenza_inventario")
		ls_flag_crea_movimento = lds_prodotti.getitemstring(ll_i,"flag_crea_movimento")
		ld_valore_unitario = lds_prodotti.getitemnumber(ll_i,"valore_unitario")
		ld_quantita_rettifica = ld_giacenza_inventario - ld_giacenza_reale
		
		
		// modifiche michela: se la giacenza reale è 0 e la rettifica è maggiore di 0 devo fare una rettifica negativa.
		if ld_giacenza_reale >= 0 then
			if ld_quantita_rettifica > 0 then
				// se devo decrementare la quantità vado a consumare i lotto più vecchi
				ls_cod_tipo_mov_mag = ls_cod_mov_neg
				ls_sort = "data_stock A, cod_deposito A"
			else
				// se devo incrementare la quantità vado ad aumentare i lotto più recenti
				ls_cod_tipo_mov_mag = ls_cod_mov_pos
				ls_sort = "data_stock D, cod_deposito A"
			end if
		else
		if ld_quantita_rettifica > 0 then
			// se devo incrementare la quantità vado ad aumentare i lotto più recenti
			ls_cod_tipo_mov_mag = ls_cod_mov_pos
			ls_sort = "data_stock D, cod_deposito A"
		else
			// se devo decrementare la quantità vado a consumare i lotto più vecchi
			ls_cod_tipo_mov_mag = ls_cod_mov_neg
			ls_sort = "data_stock A, cod_deposito A"
		end if
		end if
		ll_righe_stock = lds_stock.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
		lds_stock.setsort(ls_sort)
		lds_stock.sort()
		ld_quan_residuo = abs(ld_quantita_rettifica)
		lb_exit = false
		lb_errore = false
		for ll_y = 1 to ll_righe_stock
			ls_cod_deposito = lds_stock.getitemstring(ll_y,"cod_deposito")
			ls_cod_ubicazione = lds_stock.getitemstring(ll_y,"cod_ubicazione")
			ls_cod_lotto = lds_stock.getitemstring(ll_y,"cod_lotto")
			ldt_data_stock = lds_stock.getitemdatetime(ll_y,"data_stock")
			ll_prog_stock = lds_stock.getitemnumber(ll_y,"prog_stock")
			ld_quan_stock_inventario = lds_stock.getitemnumber(ll_y,"giacenza_inventario")
			
//			if ld_quan_stock_inventario <= 0 then continue
			
			ls_stringa_stock = ls_cod_prodotto + "~t" +  ls_cod_deposito + "~t" +  ls_cod_ubicazione + "~t" +  ls_cod_lotto + &
								  "~t" + string(ldt_data_stock,"dd/mm/yyyy") + "~t" + string(ll_prog_stock)


// *** michela 08/07/2005: su richiesta di marco lazzari per unifast (ma in generale) si aggiunge la possibilità di considerare
//                         oppure no le quantita (impegnata e in spedizione) durante il ricalcolo della quantità a magazzino

			lb_controlla = false
			
			if ls_flag_salta = "S" then
				
				select quan_assegnata,
						 quan_in_spedizione,
						 costo_medio
				into   :ld_quan_assegnata,
						 :ld_quan_in_spedizione,
						 :ld_costo_medio_stock
				from   stock
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto  and
						 cod_deposito = :ls_cod_deposito  and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto  and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_prog_stock  ;
				if sqlca.sqlcode <> 0 then  
					ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in ricerca lotto ~r~n" + ls_stringa_stock + " L'elaborazione di questo lotto sarà saltata! "
					filewrite(ll_filenum, ls_messaggio)
					continue
				end if		
				
				lb_controlla = true
				
				
			else

	//			solo se giacenza stock positiva cerco la q.ta assegnata e in spedizione del lotto 
	//			altrimenti il mov di magazzino mi darebbe errore.
				select quan_assegnata,
						 quan_in_spedizione,
						 costo_medio
				into   :ld_quan_assegnata,
						 :ld_quan_in_spedizione,
						 :ld_costo_medio_stock
				from   stock
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto  and
						 cod_deposito = :ls_cod_deposito  and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto  and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_prog_stock  ;
				if sqlca.sqlcode = 0 then
					if ld_quan_stock_inventario > 0 and ls_flag_negativo = 'N' then
						ld_quan_stock_inventario = ld_quan_stock_inventario - ( ld_quan_assegnata + ld_quan_in_spedizione )
					end if
				else
					ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in ricerca lotto ~r~n" + ls_stringa_stock + " L'elaborazione di questo lotto sarà saltata! "
					filewrite(ll_filenum, ls_messaggio)
					continue
				end if
				
			end if
			
		
			if ls_cod_tipo_mov_mag = ls_cod_mov_pos then
				// se è un movimento di incremento, lo faccio sul primo lotto a disposizione
					ld_quan_movimento = ld_quan_residuo
					ld_quan_residuo = 0
					lb_exit = true					
			else
				
// *** sempre michela 08/07/2005: a questo punto:
// --> lb_controlla = false             allora dopo non devo fare niente
// --> lb_controlla = true   vuol dire che non ho fatto la sottrazione e quindi devo avvertire l'utente
				
				if lb_controlla then
					
					ib_salta_controllo = true					
					if ls_flag_avvisa = "S" then ib_avvisa = true
					

				else
					
					ib_salta_controllo = false
					ib_avvisa = false
					
				end if
				
				// nel caso si tratti di un decremento controllo di non andare sotto la disponibilità del lotto tenendo conto
				// delle spedizioni già pronte e degli ordini già assegnati
				if ld_quan_residuo > ld_quan_stock_inventario then					
					ld_quan_movimento = ABS(ld_quan_stock_inventario)
					ld_quan_residuo = ld_quan_residuo  - ABS(ld_quan_stock_inventario)					
				else
					ld_quan_movimento = ld_quan_residuo
					ld_quan_residuo = 0
					lb_exit = true
				end if
				
// *** fine michela
// ********************
				
			end if
			if ld_quan_movimento = 0 then continue			// se quantità movimento = 0 allora passo ad un altro lotto
			
			ls_cod_tipo_mov_stock = ls_cod_tipo_mov_mag
			if ld_quan_movimento < 0 then
				ls_cod_tipo_mov_stock = ls_cod_mov_pos		// imposto movimento di incremento
				ld_quan_movimento = abs(ld_quan_movimento)
			end if
			
			if ls_cod_tipo_mov_stock = ls_cod_mov_neg and ld_valore_unitario > ld_costo_medio_stock then
				// se sto facendo un movimento di decremento e il costo medio del lotto è minore del costo medio del prodotto,
				// verrebbe fuori un errore; quindi in questo caso eseguo il movimento con il costo medio del lotto.
				ld_valore_unitario = ld_costo_medio_stock
				// segnalo la cosa sul file di log
			end if
			ll_anno_reg_mov_mag[] = ll_vuoto[]
			ll_num_reg_mov_mag[] = ll_vuoto[]
			if wf_mov_mag(ldt_data_inventario, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, ll_prog_stock,&
			              ls_cod_tipo_mov_stock, ld_quan_movimento, ld_valore_unitario, &
				           ref ll_anno_reg_mov_mag[], ref ll_num_reg_mov_mag[], ref ls_messaggio) <> 0 then
				filewrite(ll_filenum, "~r~nErrore in generazione rettifica lotto:" + ls_stringa_stock + "~r~nScarto il lotto e passo al successivo")
				if ld_quan_residuo = 0 then lb_exit = false
				ld_quan_residuo = ld_quan_residuo + ld_quan_movimento
			else
				ls_messaggio = "~r~nMov.Rettifica " + ls_cod_tipo_mov_stock + "~tQuantità=~t" + string(ld_quan_movimento) + "~tResiduo=~t" + string(ld_quan_residuo) + "~tValore unit=~t" + string(ld_valore_unitario) + "~t" + &
				               ls_stringa_stock + "~t REGISTRAZIONE MOVIMENTO " + string(ll_anno_reg_mov_mag[1]) + "/" + string(ll_num_reg_mov_mag[1]) +"~r~n" 
				filewrite(ll_filenum, ls_messaggio)
				
				update inventario
				set    flag_gen_movimento = 'S'
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in aggiornamento tabella INVENTARIO con il prodotto ~r~n" + ls_cod_prodotto + " L'elaborazione di questo lotto sarà saltata! "
					filewrite(ll_filenum, ls_messaggio)
					lb_errore = true
				end if
				update inventario_stock
				set 	 anno_reg_mov_mag = :ll_anno_reg_mov_mag[1], 
				       num_reg_mov_mag  = :ll_num_reg_mov_mag[1]
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto  and
						 cod_deposito = :ls_cod_deposito  and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto  and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_prog_stock  ;
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "~r~nErrore " + string(sqlca.sqlcode) + " in aggiornamento tabella INVENTARIO STOCK con lotto  ~r~n" + ls_stringa_stock + " L'elaborazione di questo lotto sarà saltata! "
					filewrite(ll_filenum, ls_messaggio)
					lb_errore = true
				end if
			end if
			if lb_exit then exit
			if lb_errore then exit
		next
		if lb_errore then
			rollback;
		end if
		if ld_quan_residuo <> 0 then
			ls_messaggio = "Non ci sono sufficienti lotti per decrementare il residuo di "+string(ld_quan_residuo)+" dalla giacenza dal prodotto " + ls_cod_prodotto + "~r~nIl prodotto non sarà elaborato"
			filewrite(ll_filenum, ls_messaggio)
			rollback;
		end if
	else
		
	end if
	commit;
next

commit;
destroy lds_prodotti
destroy lds_stock
fileclose(ll_filenum)

end event

type cb_gen_mov_val from commandbutton within w_inventario
integer x = 1943
integer y = 1060
integer width = 480
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. Mov. Val."
end type

event clicked;setpointer(hourglass!)

if wf_rett_val() = 0 then
	g_mb.messagebox("APICE","Operazione completata.~nConsultare il file LOG per verificare la presenza di eventuali errori",information!)
end if

setpointer(arrow!)
end event

type em_log from editmask within w_inventario
integer x = 1783
integer y = 880
integer width = 1486
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type pb_path from picturebutton within w_inventario
integer x = 3291
integer y = 880
integer width = 101
integer height = 88
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\cs_70\framework\RISORSE\Listwiev.bmp"
alignment htextalign = left!
end type

event clicked;LONG LL_VAL
STRING docname, named

LL_VAL = GetFileOpenName("SELEZIONA UN FILE", &
		+ docname, named, "LOG", &
		+ "Text Files (*.TXT),*.TXT," &
		+ "Doc Files (*.LOG),*.LOG")
IF LL_VAL = 1 then em_log.text = docname
end event

type st_10 from statictext within w_inventario
integer x = 411
integer y = 880
integer width = 1326
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "PERCORSO DEL FILE DI LOG:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_ext_inventario_prodotto from uo_cs_xx_dw within w_inventario
event ue_torna ( )
event ue_key1 pbm_dwnkey
event ue_conferma ( )
integer x = 142
integer y = 160
integer width = 2560
integer height = 340
integer taborder = 30
string dataobject = "d_ext_inventario_prodotto"
boolean border = false
end type

event ue_torna();setcolumn("cod_prodotto")
end event

event ue_key1;long ll_ret
datetime ldt_data

resetupdate()
choose case this.getcolumnname()
	case "data_registrazione"
		if key = keyenter! then
			if accepttext() = 1 then
				setcolumn("cod_prodotto")
			end if
		end if
	case "cod_prodotto"
		if key = keyenter! then
			if accepttext() = 1 then
				setcolumn("quan_rilevata")
			end if
		end if
	case "quan_rilevata"
		if key = keyenter! then
			if accepttext() = 1 then
				if g_mb.messagebox("INVENTARIO","Memorizzo i dati rilevati?",Question!,YesNo!,1) = 1 then
					dw_ext_inventario_prodotto.accepttext()
					triggerevent("ue_conferma")
				else
					ldt_data = dw_ext_inventario_prodotto.getitemdatetime(1,"data_registrazione")
					dw_ext_inventario_prodotto.reset()
					dw_ext_inventario_prodotto.insertrow(0)
					dw_ext_inventario_prodotto.setitem(1,"data_registrazione",ldt_data)
					setcolumn("cod_prodotto")
				end if
			end if				
		end if
end choose
end event

event ue_conferma();string ls_cod_prodotto
datetime ldt_data
dec{4} ld_quan_rilevata, ld_giacenza_reale

dw_ext_inventario_prodotto.accepttext()
ls_cod_prodotto = dw_ext_inventario_prodotto.getitemstring(1,"cod_prodotto")
ldt_data = dw_ext_inventario_prodotto.getitemdatetime(1,"data_registrazione")
ld_quan_rilevata = dw_ext_inventario_prodotto.getitemnumber(1,"quan_rilevata")

select giacenza_reale
into   :ld_giacenza_reale
from   inventario
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto and
		 flag_gen_movimento = 'N';
choose case sqlca.sqlcode
	case 100
		g_mb.messagebox("APICE","Il prodotto non esiste in questo inventario.")
		rollback;
		return
	case -1
		g_mb.messagebox("APICE","Errore in ricerca giacenza reale del prodotto selezionato nell'inventario.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	case else
		if not isnull(ld_giacenza_reale) and ld_giacenza_reale > 0 then
			if g_mb.messagebox("APICE","Per questo prodotto è già stata rilevata la quantità reale;~r~naccetto la nuova quantità rilevata?",Question!,YesNo!,2) = 2 then
				rollback;
				return
			end if
		end if
		update inventario
		set    giacenza_reale = :ld_quan_rilevata
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in aggiornamento giacenza reale del prodotto selezionato nell'inventario.~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		else
			commit;
		end if
end choose
dw_ext_inventario_prodotto.reset()
dw_ext_inventario_prodotto.insertrow(0)
dw_ext_inventario_prodotto.setitem(1,"data_registrazione",ldt_data)
setcolumn("cod_prodotto")
end event

event itemfocuschanged;call super::itemfocuschanged;choose case dwo.name
	case "quan_rilevata", "data_registrazione"
		selecttext(1, len(gettext()) )
end choose
end event

event itemchanged;call super::itemchanged;string ls_flag_gen_movimento, ls_cod_prodotto
dec{4} ld_giacenza_inventario, ld_giacenza_reale

resetupdate()
choose case dwo.name
	case "cod_prodotto"
		ls_cod_prodotto = data
		if not isnull(ls_cod_prodotto) then
			select giacenza_inventario,
					 giacenza_reale,
					 flag_gen_movimento
			into   :ld_giacenza_inventario, 
					 :ld_giacenza_reale,
					 :ls_flag_gen_movimento
			from   inventario
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Questo prodotto non è inserito in questo inventario.")
				postevent("ue_torna")
				return
			end if
			
			if ls_flag_gen_movimento = "S" then
				g_mb.messagebox("APICE","Per questo prodotto è già stato generato il movimento di rettifica.")
				postevent("ue_torna")
				return
			end if
			
			if not isnull(ld_giacenza_reale) and ld_giacenza_reale > 0 then
				g_mb.messagebox("APICE","Attenzione! Giacenza reale già rilevata per questo prodotto~r~nPremere INVIO !")
			end if
			
			setitem(1,"quan_inventario", ld_giacenza_inventario)
		end if
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ext_inventario_prodotto,"cod_prodotto")
end choose
end event

type gb_1 from groupbox within w_inventario
integer x = 137
integer y = 180
integer width = 3520
integer height = 1140
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "GENERAZIONE MOVIMENTI DI RETTIFICA"
end type

type dw_inventario_stock_lista from uo_cs_xx_dw within w_inventario
integer x = 46
integer y = 140
integer width = 3680
integer height = 1580
integer taborder = 20
string dataobject = "d_inventario_stock_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
long ll_errore

if dw_inventario_lista.rowcount() > 0 then
	ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")
	ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
	
	if ll_errore < 0 then
		pcca.error = c_fatal
	end if
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto
long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	if upperbound(i_parentdw.i_selectedrows[]) > 0 then
		ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_prodotto")
		if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
			this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
		end if
	end if	
next      
end event

type st_3 from statictext within w_inventario
integer x = 411
integer y = 500
integer width = 1326
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOVIMENTO RETTIFICA IN DIMINUZIONE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_conferma_inventario_ext from uo_cs_xx_dw within w_inventario
integer x = 160
integer y = 1040
integer width = 1257
integer height = 260
integer taborder = 60
string dataobject = "d_conferma_inventario_ext"
boolean border = false
end type

event itemchanged;call super::itemchanged;this.resetupdate()
end event

type dw_folder from u_folder within w_inventario
integer x = 23
integer y = 20
integer width = 3726
integer height = 1720
integer taborder = 10
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab	
	case 1

	case 2
		
	case 3
		dw_inventario_stock_lista.change_dw_current()
		dw_inventario_stock_lista.triggerevent("pcd_retrieve")
	case 4
		
end choose
end event

type dw_inventario_lista from uo_cs_xx_dw within w_inventario
integer x = 46
integer y = 240
integer width = 3680
integer height = 1480
integer taborder = 10
string dataobject = "d_inventario_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_gen_mov
long ll_errore

if cbx_gen_mov.thirdstate then
	ls_gen_mov = "%"
else
	if cbx_gen_mov.checked then
		ls_gen_mov = "S"
	else
		ls_gen_mov = "N"
	end if
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_gen_mov, "%")

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next      
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_prodotto
	long ll_cont, ll_i
	
	ll_cont = deletedcount()
	for ll_i = 1 to ll_cont
		ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto", delete!, true)
		
		delete from inventario_stock
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in cancellazione stock~r~n." + sqlca.sqlerrtext)
			return 1
		end if
	next
end if 
end event

type dw_ext_genera_inventario from uo_cs_xx_dw within w_inventario
integer x = 69
integer y = 140
integer width = 3634
integer height = 880
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_ext_genera_inventario"
boolean border = false
end type

event itemchanged;call super::itemchanged;this.resetupdate()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_ext_genera_inventario,"cod_prodotto_inizio")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_ext_genera_inventario,"cod_prodotto_fine")
end choose
end event

