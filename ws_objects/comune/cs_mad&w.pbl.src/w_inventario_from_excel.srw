﻿$PBExportHeader$w_inventario_from_excel.srw
forward
global type w_inventario_from_excel from w_cs_xx_principale
end type
type st_26 from statictext within w_inventario_from_excel
end type
type em_tipo_movimento from editmask within w_inventario_from_excel
end type
type st_25 from statictext within w_inventario_from_excel
end type
type st_log from statictext within w_inventario_from_excel
end type
type cb_4 from commandbutton within w_inventario_from_excel
end type
type st_24 from statictext within w_inventario_from_excel
end type
type cb_3 from commandbutton within w_inventario_from_excel
end type
type st_23 from statictext within w_inventario_from_excel
end type
type cb_2 from commandbutton within w_inventario_from_excel
end type
type em_data from editmask within w_inventario_from_excel
end type
type st_22 from statictext within w_inventario_from_excel
end type
type st_21 from statictext within w_inventario_from_excel
end type
type cb_1 from commandbutton within w_inventario_from_excel
end type
type em_file from editmask within w_inventario_from_excel
end type
type st_20 from statictext within w_inventario_from_excel
end type
type st_13 from statictext within w_inventario_from_excel
end type
type st_19 from statictext within w_inventario_from_excel
end type
type st_18 from statictext within w_inventario_from_excel
end type
type st_17 from statictext within w_inventario_from_excel
end type
type st_16 from statictext within w_inventario_from_excel
end type
type st_15 from statictext within w_inventario_from_excel
end type
type st_14 from statictext within w_inventario_from_excel
end type
type st_12 from statictext within w_inventario_from_excel
end type
type st_11 from statictext within w_inventario_from_excel
end type
type st_10 from statictext within w_inventario_from_excel
end type
type st_9 from statictext within w_inventario_from_excel
end type
type st_8 from statictext within w_inventario_from_excel
end type
type st_7 from statictext within w_inventario_from_excel
end type
type st_6 from statictext within w_inventario_from_excel
end type
type st_5 from statictext within w_inventario_from_excel
end type
type st_4 from statictext within w_inventario_from_excel
end type
type st_3 from statictext within w_inventario_from_excel
end type
type st_2 from statictext within w_inventario_from_excel
end type
type st_1 from statictext within w_inventario_from_excel
end type
end forward

global type w_inventario_from_excel from w_cs_xx_principale
integer width = 3922
integer height = 2040
string title = "Importazione Inventario Excel"
long backcolor = 16777215
st_26 st_26
em_tipo_movimento em_tipo_movimento
st_25 st_25
st_log st_log
cb_4 cb_4
st_24 st_24
cb_3 cb_3
st_23 st_23
cb_2 cb_2
em_data em_data
st_22 st_22
st_21 st_21
cb_1 cb_1
em_file em_file
st_20 st_20
st_13 st_13
st_19 st_19
st_18 st_18
st_17 st_17
st_16 st_16
st_15 st_15
st_14 st_14
st_12 st_12
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
end type
global w_inventario_from_excel w_inventario_from_excel

on w_inventario_from_excel.create
int iCurrent
call super::create
this.st_26=create st_26
this.em_tipo_movimento=create em_tipo_movimento
this.st_25=create st_25
this.st_log=create st_log
this.cb_4=create cb_4
this.st_24=create st_24
this.cb_3=create cb_3
this.st_23=create st_23
this.cb_2=create cb_2
this.em_data=create em_data
this.st_22=create st_22
this.st_21=create st_21
this.cb_1=create cb_1
this.em_file=create em_file
this.st_20=create st_20
this.st_13=create st_13
this.st_19=create st_19
this.st_18=create st_18
this.st_17=create st_17
this.st_16=create st_16
this.st_15=create st_15
this.st_14=create st_14
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_26
this.Control[iCurrent+2]=this.em_tipo_movimento
this.Control[iCurrent+3]=this.st_25
this.Control[iCurrent+4]=this.st_log
this.Control[iCurrent+5]=this.cb_4
this.Control[iCurrent+6]=this.st_24
this.Control[iCurrent+7]=this.cb_3
this.Control[iCurrent+8]=this.st_23
this.Control[iCurrent+9]=this.cb_2
this.Control[iCurrent+10]=this.em_data
this.Control[iCurrent+11]=this.st_22
this.Control[iCurrent+12]=this.st_21
this.Control[iCurrent+13]=this.cb_1
this.Control[iCurrent+14]=this.em_file
this.Control[iCurrent+15]=this.st_20
this.Control[iCurrent+16]=this.st_13
this.Control[iCurrent+17]=this.st_19
this.Control[iCurrent+18]=this.st_18
this.Control[iCurrent+19]=this.st_17
this.Control[iCurrent+20]=this.st_16
this.Control[iCurrent+21]=this.st_15
this.Control[iCurrent+22]=this.st_14
this.Control[iCurrent+23]=this.st_12
this.Control[iCurrent+24]=this.st_11
this.Control[iCurrent+25]=this.st_10
this.Control[iCurrent+26]=this.st_9
this.Control[iCurrent+27]=this.st_8
this.Control[iCurrent+28]=this.st_7
this.Control[iCurrent+29]=this.st_6
this.Control[iCurrent+30]=this.st_5
this.Control[iCurrent+31]=this.st_4
this.Control[iCurrent+32]=this.st_3
this.Control[iCurrent+33]=this.st_2
this.Control[iCurrent+34]=this.st_1
end on

on w_inventario_from_excel.destroy
call super::destroy
destroy(this.st_26)
destroy(this.em_tipo_movimento)
destroy(this.st_25)
destroy(this.st_log)
destroy(this.cb_4)
destroy(this.st_24)
destroy(this.cb_3)
destroy(this.st_23)
destroy(this.cb_2)
destroy(this.em_data)
destroy(this.st_22)
destroy(this.st_21)
destroy(this.cb_1)
destroy(this.em_file)
destroy(this.st_20)
destroy(this.st_13)
destroy(this.st_19)
destroy(this.st_18)
destroy(this.st_17)
destroy(this.st_16)
destroy(this.st_15)
destroy(this.st_14)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
end on

type st_26 from statictext within w_inventario_from_excel
integer x = 251
integer y = 1600
integer width = 1829
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "rispetto alla data inventario."
boolean focusrectangle = false
end type

type em_tipo_movimento from editmask within w_inventario_from_excel
integer x = 1760
integer y = 1400
integer width = 229
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "INI"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_25 from statictext within w_inventario_from_excel
integer x = 229
integer y = 1400
integer width = 1509
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "ed eseguire i movimenti usando il tipo movimento: "
boolean focusrectangle = false
end type

type st_log from statictext within w_inventario_from_excel
integer x = 23
integer y = 1840
integer width = 3840
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_inventario_from_excel
integer x = 2309
integer y = 1700
integer width = 937
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "RICALCOLO SCHEDE"
end type

event clicked;window_open(w_aggiorna_impegnato, 0)
end event

type st_24 from statictext within w_inventario_from_excel
integer x = 160
integer y = 1720
integer width = 1989
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "3. Eseguire il ricalcolo dei progressivi delle schede di magazzino:"
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_inventario_from_excel
integer x = 2309
integer y = 1520
integer width = 937
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "APRI PARAMETRI MAGAZZINO"
end type

event clicked;window_open(w_con_magazzino, 0)
end event

type st_23 from statictext within w_inventario_from_excel
integer x = 160
integer y = 1540
integer width = 2117
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "2. Impostare la data di chiusura magazzino al giorno precedente"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_inventario_from_excel
integer x = 2309
integer y = 1300
integer width = 937
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "IMPORTA"
end type

event clicked;string		ls_str, ls_cod_prodotto, ls_cod_cliente[], ls_array_null[], ls_cod_fornitore[], &
			ls_cod_deposito[],ls_cod_ubicazione[], ls_cod_lotto[],ls_referenza
long		ll_i, ll_prog_stock[], ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_num_documento,  ll_array_null[], &
			ll_anno_registrazione[], ll_num_registrazione[]
dec{4}	ld_giacenza, ld_valore
date		ldd_data
datetime ldt_data, ldt_data_stock[], ldt_data_documento, ldt_array_null[]
any		la_dataexcel
uo_excel luo_excel
uo_magazzino luo_mag


	
	
ls_str = em_data.text
if len(ls_str) < 1 then
	g_mb.error("Indicare una data_valida")
	return
end if

ldt_data = datetime(date(ls_str), 00:00:00)

if not g_mb.confirm( g_str.format("L'inventario sarà caricato in data $1, CONFERMI?", string(ldt_data, "dd/mm/yyyy") ) ) then return

luo_excel = create uo_excel

if not fileexists(em_file.text) then
	g_mb.error("File inesistente!")
	return
end if

luo_excel.uof_open( em_file.text, false)

ll_i = 1
luo_excel.uof_set_sheet( "INVENTARIO")

do while true
	ll_i ++
	
	la_dataexcel  = luo_excel.uof_read(ll_i, 1)
	
	if isnull(la_dataexcel) then exit
	
	ls_cod_prodotto = string( la_dataexcel )
	
	if len(ls_cod_prodotto) < 1 then exit
	
	st_log.text = g_str.format("$1 - $2", ll_i, ls_cod_prodotto)
	
	// esco alla prima cella col prodotto vuoto
	ls_cod_deposito[1] = luo_excel.uof_read(ll_i, 3)
	
	la_dataexcel = luo_excel.uof_read(ll_i, 4)
	ls_cod_ubicazione[1] = string( la_dataexcel )
	
	la_dataexcel = luo_excel.uof_read(ll_i, 5)
	ls_cod_lotto[1] = string( la_dataexcel )
	ldt_data_stock[1] =  luo_excel.uof_read(ll_i, 6)
	ll_prog_stock[1] =  luo_excel.uof_read(ll_i, 7)
	ld_giacenza =  luo_excel.uof_read(ll_i, 8)
	ld_valore =  luo_excel.uof_read(ll_i, 9)
	
	ls_cod_cliente[] = ls_array_null[]
	ls_cod_fornitore[] = ls_array_null[]
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	if f_crea_dest_mov_magazzino (em_tipo_movimento.text, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		ROLLBACK;
		return -1
	end if	
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 em_tipo_movimento.text, &
									 ls_cod_prodotto) = -1 then
		ROLLBACK;
		return 0
	end if	
	
//	ldt_data_stock[1] = s_cs_xx.parametri.parametro_data_1
	setnull(ll_num_documento)
	setnull(ldt_data_documento)
	setnull(ls_referenza)
	ls_referenza = "IMP-INV"
//	ls_cod_deposito[] = ls_array_null[]
//	ls_cod_ubicazione[] = ls_array_null[]
//	ls_cod_lotto[] = ls_array_null[]
//	ldt_data_stock[] = ldt_array_null[]
//	ll_prog_stock[] = ll_array_null[]
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( ldt_data, &
							em_tipo_movimento.text, &
							"N", &
							ls_cod_prodotto, &
							ld_giacenza, &
							ld_valore, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_registrazione[], &
							ll_num_registrazione[] ) = 0 then
		COMMIT;
	
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock) = -1 then
			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
		end if
		COMMIT;

	else
		ROLLBACK;

	end if
	
	destroy luo_mag
	
	
loop

destroy luo_excel

return
end event

type em_data from editmask within w_inventario_from_excel
integer x = 1463
integer y = 1300
integer width = 549
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
boolean dropdowncalendar = true
end type

type st_22 from statictext within w_inventario_from_excel
integer x = 160
integer y = 1320
integer width = 1303
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "1. Importare inventario alla seguente data:"
boolean focusrectangle = false
end type

type st_21 from statictext within w_inventario_from_excel
integer x = 46
integer y = 1220
integer width = 1303
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "SEQUENZA ATTIVITA~' DA SVOLGERE"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_inventario_from_excel
integer x = 3200
integer y = 1020
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica File"
end type

event clicked;string ls_filepath, ls_filename
long ll_rtn
ll_rtn = GetFileOpenName(	"Selezione File Excel Inventario", &
										ls_filepath, ls_filename, "XLSX",  &
										"Excel (*.XLSX),*.XLSX,"  + & 
										"All Files (*.*), *.*",  & 
										guo_functions.uof_get_user_documents_folder( ) , 18)
										
if ll_rtn < 1 then return

em_file.text =  ls_filepath

end event

type em_file from editmask within w_inventario_from_excel
integer x = 23
integer y = 1020
integer width = 3154
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_20 from statictext within w_inventario_from_excel
integer x = 549
integer y = 860
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Valore Inventario"
boolean focusrectangle = false
end type

type st_13 from statictext within w_inventario_from_excel
integer x = 549
integer y = 780
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Giacenza Inventario"
boolean focusrectangle = false
end type

type st_19 from statictext within w_inventario_from_excel
integer x = 114
integer y = 860
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA I: "
boolean focusrectangle = false
end type

type st_18 from statictext within w_inventario_from_excel
integer x = 114
integer y = 780
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA H: "
boolean focusrectangle = false
end type

type st_17 from statictext within w_inventario_from_excel
integer x = 549
integer y = 700
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Progessivo"
boolean focusrectangle = false
end type

type st_16 from statictext within w_inventario_from_excel
integer x = 549
integer y = 620
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Data Stock"
boolean focusrectangle = false
end type

type st_15 from statictext within w_inventario_from_excel
integer x = 549
integer y = 540
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Lotto"
boolean focusrectangle = false
end type

type st_14 from statictext within w_inventario_from_excel
integer x = 549
integer y = 460
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Ubicazione"
boolean focusrectangle = false
end type

type st_12 from statictext within w_inventario_from_excel
integer x = 549
integer y = 380
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Codice Deposito"
boolean focusrectangle = false
end type

type st_11 from statictext within w_inventario_from_excel
integer x = 549
integer y = 300
integer width = 1120
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "Descrizione prodotto (può essere omessa)"
boolean focusrectangle = false
end type

type st_10 from statictext within w_inventario_from_excel
integer x = 549
integer y = 220
integer width = 535
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 553648127
string text = "CODICE PRODOTTO"
boolean focusrectangle = false
end type

type st_9 from statictext within w_inventario_from_excel
integer x = 114
integer y = 700
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA G: "
boolean focusrectangle = false
end type

type st_8 from statictext within w_inventario_from_excel
integer x = 114
integer y = 620
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA F: "
boolean focusrectangle = false
end type

type st_7 from statictext within w_inventario_from_excel
integer x = 114
integer y = 540
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA E: "
boolean focusrectangle = false
end type

type st_6 from statictext within w_inventario_from_excel
integer x = 114
integer y = 460
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA D: "
boolean focusrectangle = false
end type

type st_5 from statictext within w_inventario_from_excel
integer x = 114
integer y = 380
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA C: "
boolean focusrectangle = false
end type

type st_4 from statictext within w_inventario_from_excel
integer x = 114
integer y = 300
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA B: "
boolean focusrectangle = false
end type

type st_3 from statictext within w_inventario_from_excel
integer x = 114
integer y = 220
integer width = 434
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "COLONNA A: "
boolean focusrectangle = false
end type

type st_2 from statictext within w_inventario_from_excel
integer x = 23
integer y = 140
integer width = 2688
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "Il formato del file Excel è il seguente: (ATTENZIONE, verificare che non ci siano celle vuote)."
boolean focusrectangle = false
end type

type st_1 from statictext within w_inventario_from_excel
integer x = 23
integer y = 20
integer width = 3817
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 553648127
string text = "Questa finestra esegue l~'importazione di un file Excel con l~'inventario e genera i conseguenti movimenti di magazzino"
boolean focusrectangle = false
end type

