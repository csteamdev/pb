﻿$PBExportHeader$w_lifo.srw
$PBExportComments$Finestra Gestione Lifo
forward
global type w_lifo from w_cs_xx_principale
end type
type dw_lifo_prodotto from datawindow within w_lifo
end type
type cbx_1 from checkbox within w_lifo
end type
type st_avanzamento from statictext within w_lifo
end type
type cb_elimina_lifo from commandbutton within w_lifo
end type
type st_5 from statictext within w_lifo
end type
type st_4 from statictext within w_lifo
end type
type st_3 from statictext within w_lifo
end type
type pb_2 from picturebutton within w_lifo
end type
type em_path_log from editmask within w_lifo
end type
type st_2 from statictext within w_lifo
end type
type pb_1 from picturebutton within w_lifo
end type
type st_1 from statictext within w_lifo
end type
type cb_importazione from commandbutton within w_lifo
end type
type cb_reset from commandbutton within w_lifo
end type
type em_path from editmask within w_lifo
end type
type dw_ricerca from u_dw_search within w_lifo
end type
type dw_folder from u_folder within w_lifo
end type
type dw_lifo_det from uo_cs_xx_dw within w_lifo
end type
end forward

global type w_lifo from w_cs_xx_principale
integer width = 3214
integer height = 1344
string title = "Gestione Dati Lifo"
dw_lifo_prodotto dw_lifo_prodotto
cbx_1 cbx_1
st_avanzamento st_avanzamento
cb_elimina_lifo cb_elimina_lifo
st_5 st_5
st_4 st_4
st_3 st_3
pb_2 pb_2
em_path_log em_path_log
st_2 st_2
pb_1 pb_1
st_1 st_1
cb_importazione cb_importazione
cb_reset cb_reset
em_path em_path
dw_ricerca dw_ricerca
dw_folder dw_folder
dw_lifo_det dw_lifo_det
end type
global w_lifo w_lifo

forward prototypes
public function integer wf_log (string messaggio)
public function integer wf_pulisci_log ()
public function integer wf_valorizza_giacenza (string fs_cod_prodotto, ref decimal fd_somma_valore_giacenza, ref string fs_messaggio)
public function integer wf_togli_strati (string fs_cod_prodotto, decimal fd_quantita, ref string fs_messaggio)
end prototypes

public function integer wf_log (string messaggio);long ll_file, ll_ret

ll_file = FileOpen(em_path_log.text, LineMode!, Write!, LockWrite!, Append!)
if ll_file = -1 then
	g_mb.messagebox("APICE","Errore in apertura file di LOG",Stopsign!)
	return -1
end if
ll_ret = filewrite(ll_file,messaggio)
if ll_ret = -1 then
	g_mb.messagebox("APICE","Errore in scrittura file di LOG",Stopsign!)
	return -1
end if
fileclose(ll_file)
return 0
end function

public function integer wf_pulisci_log ();long ll_file, ll_ret

ll_file = FileOpen(em_path_log.text, LineMode!, Write!, LockWrite!, replace!)
if ll_file = -1 then
	g_mb.messagebox("APICE","Errore in apertura file di LOG",Stopsign!)
	return -1
end if
ll_ret = filewrite(ll_file,"")
if ll_file = -1 then
	g_mb.messagebox("APICE","Errore in pulizia file di LOG",Stopsign!)
	return -1
end if
fileclose(ll_file)
return 0
end function

public function integer wf_valorizza_giacenza (string fs_cod_prodotto, ref decimal fd_somma_valore_giacenza, ref string fs_messaggio);long ll_righe, ll_i
dec{4} ld_rimanenza_strato, ld_costo_medio_ponderato, ld_valore

ll_righe = dw_lifo_prodotto.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto)
if ll_righe = -1 then
	fs_messaggio = "Errore nella retrieve dei dati (wf_valorizza_giacenza)"
	return -1
end if
fd_somma_valore_giacenza = 0

for ll_i = 1 to ll_righe
	ld_rimanenza_strato = dw_lifo_prodotto.getitemnumber(ll_i,"rimanenza_strato")
	ld_costo_medio_ponderato = dw_lifo_prodotto.getitemnumber(ll_i,"costo_medio_ponderato")
	fd_somma_valore_giacenza = fd_somma_valore_giacenza + round((ld_rimanenza_strato * ld_costo_medio_ponderato),4)
next
	
return 0

end function

public function integer wf_togli_strati (string fs_cod_prodotto, decimal fd_quantita, ref string fs_messaggio);boolean lb_exit=false
long ll_righe, ll_i,ll_anno_lifo, ll_ret
dec{4} ld_rimanenza_strato, ld_residuo,ld_valore_update

ll_righe = dw_lifo_prodotto.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto)
if ll_righe = -1 then
	fs_messaggio = "Errore in retrieve datastore lds_lifo (wf_togli_strati)."
	return -1
end if

ld_residuo = fd_quantita
for ll_i = 1 to ll_righe
	ll_anno_lifo = dw_lifo_prodotto.getitemnumber(ll_i,"anno_lifo")
	ld_rimanenza_strato = dw_lifo_prodotto.getitemnumber(ll_i,"rimanenza_strato")
	if ld_rimanenza_strato >= ld_residuo then
		ld_residuo = ld_rimanenza_strato - ld_residuo
//		ld_rimanenza_strato = 0
		ld_valore_update = ld_residuo
		lb_exit = true
	else
		// MANGIO TUTTO LO STRATO
		ld_residuo = ld_residuo - ld_rimanenza_strato
//		ld_rimanenza_strato = 0
//		ld_residuo = 0
		ld_valore_update = 0
	end if
	
	update lifo
	set rimanenza_strato = :ld_valore_update
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_lifo = :ll_anno_lifo and
			cod_prodotto = :fs_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in aggiornamento rimamenza lifo anno " + string(ll_anno_lifo) + " del prodotto " + fs_cod_prodotto + "~r~n" + sqlca.sqlerrtext
	end if
	
	if lb_exit then exit
	if (ll_i = ll_righe) and (ld_residuo > 0) then
		fs_messaggio = "Errore; non riesco a togliere tutto il residuo."
		return -1
	end if
next
return 0

end function

event pc_setddlb;call super::pc_setddlb;//f_po_loadddlb(ddlb_cod_prodotto, &
//                 sqlca, &
//                 "anag_prodotti", &
//                 "cod_prodotto", &
//                 "des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
//                 "")
end event

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[]

dw_lifo_det.set_dw_key("cod_azienda")
dw_lifo_det.set_dw_key("cod_prodotto")
dw_lifo_det.set_dw_options(sqlca, &
                           pcca.null_object, &
                           c_noretrieveonopen, &
                           c_default)
iuo_dw_main = dw_lifo_det

lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = dw_lifo_det
lw_oggetti[3] = cb_reset
//lw_oggetti[4] = cb_ricerca_prodotto
dw_folder.fu_assigntab(1, "Lifo", lw_oggetti[])
lw_oggetti[1] = cb_importazione
lw_oggetti[2] = st_1
lw_oggetti[3] = st_2
lw_oggetti[4] = st_3
lw_oggetti[5] = em_path
lw_oggetti[6] = em_path_log
lw_oggetti[7] = pb_1
lw_oggetti[8] = pb_2
lw_oggetti[9] = st_5
lw_oggetti[9] = st_4
lw_oggetti[10] = cb_elimina_lifo
lw_oggetti[11] = st_avanzamento
lw_oggetti[12] = cbx_1
dw_folder.fu_assigntab(2, "Importazione", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

l_criteriacolumn[1] = "cod_prodotto"
l_criteriacolumn[2] = "anno_lifo"

l_searchtable[1] = "lifo"
l_searchtable[2] = "lifo"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "anno_lifo"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_lifo_det, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)
									
end event

on w_lifo.create
int iCurrent
call super::create
this.dw_lifo_prodotto=create dw_lifo_prodotto
this.cbx_1=create cbx_1
this.st_avanzamento=create st_avanzamento
this.cb_elimina_lifo=create cb_elimina_lifo
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.pb_2=create pb_2
this.em_path_log=create em_path_log
this.st_2=create st_2
this.pb_1=create pb_1
this.st_1=create st_1
this.cb_importazione=create cb_importazione
this.cb_reset=create cb_reset
this.em_path=create em_path
this.dw_ricerca=create dw_ricerca
this.dw_folder=create dw_folder
this.dw_lifo_det=create dw_lifo_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lifo_prodotto
this.Control[iCurrent+2]=this.cbx_1
this.Control[iCurrent+3]=this.st_avanzamento
this.Control[iCurrent+4]=this.cb_elimina_lifo
this.Control[iCurrent+5]=this.st_5
this.Control[iCurrent+6]=this.st_4
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.pb_2
this.Control[iCurrent+9]=this.em_path_log
this.Control[iCurrent+10]=this.st_2
this.Control[iCurrent+11]=this.pb_1
this.Control[iCurrent+12]=this.st_1
this.Control[iCurrent+13]=this.cb_importazione
this.Control[iCurrent+14]=this.cb_reset
this.Control[iCurrent+15]=this.em_path
this.Control[iCurrent+16]=this.dw_ricerca
this.Control[iCurrent+17]=this.dw_folder
this.Control[iCurrent+18]=this.dw_lifo_det
end on

on w_lifo.destroy
call super::destroy
destroy(this.dw_lifo_prodotto)
destroy(this.cbx_1)
destroy(this.st_avanzamento)
destroy(this.cb_elimina_lifo)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.pb_2)
destroy(this.em_path_log)
destroy(this.st_2)
destroy(this.pb_1)
destroy(this.st_1)
destroy(this.cb_importazione)
destroy(this.cb_reset)
destroy(this.em_path)
destroy(this.dw_ricerca)
destroy(this.dw_folder)
destroy(this.dw_lifo_det)
end on

type dw_lifo_prodotto from datawindow within w_lifo
boolean visible = false
integer x = 23
integer y = 1452
integer width = 3109
integer height = 240
integer taborder = 50
string title = "none"
string dataobject = "d_datastore_lifo"
boolean border = false
boolean livescroll = true
end type

type cbx_1 from checkbox within w_lifo
integer x = 96
integer y = 996
integer width = 1152
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Vis.Messaggi per descrizione diversa"
boolean checked = true
end type

type st_avanzamento from statictext within w_lifo
integer x = 471
integer y = 608
integer width = 1947
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_elimina_lifo from commandbutton within w_lifo
integer x = 91
integer y = 600
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina lifo"
end type

event clicked;delete from lifo
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE",sqlca.sqlerrtext)
	rollback;
else
	commit;
end if
end event

type st_5 from statictext within w_lifo
integer x = 46
integer y = 800
integer width = 3049
integer height = 180
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nel caso si desideri importare il L.I.F.O. degli anni precedenti da file Excel, è necessario aprire il file XLS ed eseguire l~'operazione ~"Salva con nome~" su file di tipo TXT-Tab Separated."
boolean focusrectangle = false
end type

type st_4 from statictext within w_lifo
integer x = 46
integer y = 160
integer width = 3086
integer height = 120
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "IMPORTAZIONE L.I.F.O. DA FILE DI TESTO TAB-SEPARATED"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_3 from statictext within w_lifo
integer x = 46
integer y = 1040
integer width = 3077
integer height = 160
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

type pb_2 from picturebutton within w_lifo
integer x = 2834
integer y = 480
integer width = 73
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "\cs_70\framework\risorse\listwiev_3.bmp"
string disabledname = "\cs_70\framework\risorse\listwiev_3.bmp"
alignment htextalign = left!
end type

event clicked;string ls_file, ls_path
integer ll_ret

ll_ret = GetFileOpenName("Cerca File Importazione", ls_path, ls_file, "TXT", "Text Files (*.TXT),*.TXT")

em_path_log.text = ls_path
end event

type em_path_log from editmask within w_lifo
integer x = 800
integer y = 480
integer width = 2011
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type st_2 from statictext within w_lifo
integer x = 114
integer y = 480
integer width = 677
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "FILE LOG:"
alignment alignment = right!
boolean focusrectangle = false
end type

type pb_1 from picturebutton within w_lifo
integer x = 2834
integer y = 380
integer width = 73
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "\cs_70\framework\risorse\listwiev_3.bmp"
string disabledname = "\cs_70\framework\risorse\listwiev_3.bmp"
alignment htextalign = left!
end type

event clicked;string ls_file, ls_path
integer ll_ret

ll_ret = GetFileOpenName("Cerca File Importazione", ls_path, ls_file, "TXT", "Text Files (*.TXT),*.TXT")

em_path.text = ls_path
end event

type st_1 from statictext within w_lifo
integer x = 114
integer y = 380
integer width = 677
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "FILE DA ELABORARE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_importazione from commandbutton within w_lifo
integer x = 2446
integer y = 600
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;boolean lb_exit = false
string ls_str, ls_tab, ls_string, ls_cod_prodotto, ls_des_prodotto, ls_comodo_2,ls_test_codice,ls_test_descrizione, ls_messaggio
integer ll_file, ll_ret, ll_pos, ll_start, ll_campo,ll_anno_lifo, ll_conta_anni
dec{4} ld_costo_medio_ponderato, ld_quan_fine_anno, ld_giacenza_finale_anno_prec, ld_costo_medio_anno_prec, &
       ld_rimanenza_anno_prec, ld_valore_anno_prec,ld_rimanenza_strato_anno_corrente,ld_giacenza_sommata,ld_valore_giacenza

ll_ret = dw_lifo_prodotto.settransobject(sqlca)
if ll_ret = -1 then
	g_mb.messagebox("APICE","Errore -1 settransobject() datastore lds_lifo (wf_togli_strati).",stopsign!)
	return -1
end if

st_avanzamento.text = ""
if isnull(em_path.text) or len(em_path.text) < 1 then
	g_mb.messagebox("APICE","Immettere un nome di file valido",stopsign!)
	return
end if
if isnull(em_path_log.text) or len(em_path_log.text) < 1 then
	g_mb.messagebox("APICE","Immettere un nome di file LOG valido",stopsign!)
	return
end if

if wf_pulisci_log() = -1 then
	return
end if


ls_tab = "~t"
ll_file = FileOpen(em_path.text, LineMode!)
if ll_file = -1 then return

ll_pos = 0
do while 1=1
	ll_ret = fileread(ll_file,ls_str)
	if ll_ret = -100 then exit /// fine
	if ll_ret = -1   then exit /// errore
	ll_start = 1
	ll_campo = 0
	lb_exit = false
	do while 1=1
		ll_pos = pos(ls_str,ls_tab,ll_pos+1)
		if ll_pos = 0 then   // non trovato tab
			ll_campo ++
			ls_string = mid(ls_str,ll_start,len(ls_str) - ll_start + 1)
			lb_exit = true		
		else			
			ll_campo ++
			ls_string = mid(ls_str,ll_start,ll_pos - ll_start)
			ll_start = ll_pos + 1
		end if
		choose case ll_campo
			case 1 // anno lifo
				ll_anno_lifo = long(ls_string)
			case 2 // cod prodotto
				ls_cod_prodotto = trim(ls_string)
			case 3 // descrizione prodotto
				ls_des_prodotto = ls_string
			case 4 // rimamenza fine anno
				ld_quan_fine_anno = dec(ls_string)
			case 5 // costo medio ponderato
				ld_costo_medio_ponderato = dec(ls_string)
			case 6 // codice comodo 2
				ls_comodo_2 = ls_string
			case else
		end choose
		if lb_exit then exit
	loop
	// eseguo dei controlli sull'anagrafica prodotti
	
	st_avanzamento.text = ls_cod_prodotto + " - " + ls_des_prodotto
	select cod_prodotto, des_prodotto
	into   :ls_test_codice, :ls_test_descrizione
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then
		wf_log("Prodotto " + ls_cod_prodotto + " inesistente in anagrafica prodotti")
		continue
	end if
	if ls_test_descrizione <> ls_des_prodotto then
		wf_log("Attenzione! Il prodotto codice " + ls_cod_prodotto + " ha descrizione in anagrafica prodotti " + ls_test_descrizione + "~r~nmentre nel file Excel " + ls_des_prodotto)
		if cbx_1.checked then
			if g_mb.messagebox("APICE","Attenzione! Il prodotto codice " + ls_cod_prodotto + " ha descrizione in anagrafica prodotti " + ls_test_descrizione + "~r~nmentre nel file Excel " + ls_des_prodotto + "~r~nInserire il record ?",Question!,YesNo!,2)  = 2 then continue
		end if
	end if
	
	// ---------------------------  inizio procedura di elaborazione dei dati -------------------------------//
	
	// ------->> per prima cosa memorizzo il codice comodo 2 in anag_prodotti -------------------------------//
	
	if not isnull(ls_comodo_2) and len(ls_comodo_2) > 0 then
		update anag_prodotti
		set    cod_comodo_2 = :ls_comodo_2
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore scrivendo il codice comodo 2 del prodotto "+ls_cod_prodotto+"in anag_prodotti.~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
			wf_log("ERRORE-->~r~nErrore scrivendo il codice comodo 2 del prodotto "+ls_cod_prodotto+"in anag_prodotti.~r~n" + sqlca.sqlerrtext)
		end if
	end if	
	
	// ------->> vado avanti con l'elaborazione del LIFO ----------------------------------------------------//
	ll_conta_anni = 0
	select count(*)
	into   :ll_conta_anni
	from   lifo
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    cod_prodotto = :ls_cod_prodotto;
	if ll_conta_anni < 1 then    /// sono al primo inserimento
	
		ld_valore_giacenza = round(ld_costo_medio_ponderato * ld_quan_fine_anno,4)
	
			INSERT INTO lifo  
					( cod_azienda,   
					  cod_prodotto,   
					  anno_lifo,   
					  giacenza_finale,   
					  costo_ultimo,   
					  flag_provvisorio,   
					  strato_provvisorio,   
					  des_prodotto,   
					  costo_medio_ponderato,   
					  rimanenza_strato,   
					  valore_fine_anno )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_prodotto,   
					  :ll_anno_lifo,   
					  :ld_quan_fine_anno,   
					  0,   
					  'N',   
					  0,   
					  :ls_des_prodotto,   
					  :ld_costo_medio_ponderato,   
					  :ld_quan_fine_anno,   
					  :ld_valore_giacenza)  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
			wf_log("ERRORE-->~r~nErrore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext)
		end if					  
	else
		// cerco l'ultimo anno
		select max(anno_lifo)
		into   :ll_conta_anni
		from   lifo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		// carico i dati dell'ultimo anno lifo del prodotto
		SELECT giacenza_finale,   
				costo_medio_ponderato,   
				rimanenza_strato,   
				valore_fine_anno  
		 INTO :ld_giacenza_finale_anno_prec,   
				:ld_costo_medio_anno_prec,   
				:ld_rimanenza_anno_prec,   
				:ld_valore_anno_prec  
		 FROM lifo  
		WHERE cod_azienda  = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :ls_cod_prodotto AND  
				anno_lifo    = :ll_conta_anni;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in select lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_conta_anni)+".~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
			wf_log("ERRORE-->~r~nErrore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_conta_anni)+".~r~n" + sqlca.sqlerrtext)
		end if					  
		
		// confronto la giacenza dell'anno che sto caricando con quella dell'anno trovato qui sopra
		choose case ld_quan_fine_anno
			// se uguale
			case ld_giacenza_finale_anno_prec
				
				if wf_valorizza_giacenza(ls_cod_prodotto, ref ld_giacenza_sommata, ref ls_messaggio) = -1 then
					g_mb.messagebox("APICE",ls_messaggio + "~r~nElaborazione Interrotta !!!")
					wf_log(ls_messaggio + "~r~nElaborazione Interrotta !!!")
					rollback;
					return
				end if					  
				
				
				INSERT INTO lifo  
							( cod_azienda,   
							  cod_prodotto,   
							  anno_lifo,   
							  giacenza_finale,   
							  costo_ultimo,   
							  flag_provvisorio,   
							  strato_provvisorio,   
							  des_prodotto,   
							  costo_medio_ponderato,   
							  rimanenza_strato,   
							  valore_fine_anno )  
				  VALUES ( :s_cs_xx.cod_azienda,   
							  :ls_cod_prodotto,   
							  :ll_anno_lifo,   
							  :ld_quan_fine_anno,   
							  0,   
							  'N',   
							  0,   
							  :ls_des_prodotto,   
							  :ld_costo_medio_ponderato,   
							  0,   
							  :ld_giacenza_sommata)  ;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
					wf_log("ERRORE-->~r~nErrore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext)
				end if					  
//			// se maggiore
			case is > ld_giacenza_finale_anno_prec
		
				if wf_valorizza_giacenza(ls_cod_prodotto, ref ld_giacenza_sommata, ref ls_messaggio) = -1 then
					g_mb.messagebox("APICE",ls_messaggio + "~r~nElaborazione Interrotta !!!")
					wf_log(ls_messaggio + "~r~nElaborazione Interrotta !!!")
					rollback;
					return
				end if					  
				ld_rimanenza_strato_anno_corrente = ld_quan_fine_anno - ld_giacenza_finale_anno_prec
				ld_giacenza_sommata = ld_giacenza_sommata + (ld_rimanenza_strato_anno_corrente * ld_costo_medio_ponderato)
		
				INSERT INTO lifo  
							( cod_azienda,   
							  cod_prodotto,   
							  anno_lifo,   
							  giacenza_finale,   
							  costo_ultimo,   
							  flag_provvisorio,   
							  strato_provvisorio,   
							  des_prodotto,   
							  costo_medio_ponderato,   
							  rimanenza_strato,   
							  valore_fine_anno )  
				  VALUES ( :s_cs_xx.cod_azienda,   
							  :ls_cod_prodotto,   
							  :ll_anno_lifo,   
							  :ld_quan_fine_anno,   
							  0,   
							  'N',   
							  0,   
							  :ls_des_prodotto,   
							  :ld_costo_medio_ponderato,   
							  :ld_rimanenza_strato_anno_corrente,   
							  :ld_giacenza_sommata);
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
					wf_log("ERRORE-->~r~nErrore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext)
				end if					  
				
		// se minore
			case is < ld_giacenza_finale_anno_prec
		
				// cerco la giacenza anno precedente
					// --> ce l'ho già in   ld_giacenza_finale_anno_prec
				// faccio giacenza anno precedente - giacenza anno corrente
				ld_rimanenza_strato_anno_corrente = ld_giacenza_finale_anno_prec - ld_quan_fine_anno
				// tolgo la differenza scalandola dagli rimanenza_strato degli anni precedenti in decremento dall'anno maggiore
				ll_ret = wf_togli_strati(ls_cod_prodotto, ld_rimanenza_strato_anno_corrente, ls_messaggio)
				if ll_ret < 0 then
					g_mb.messagebox("APICE",ls_messaggio + "~r~nElaborazione Interrotta !!!")
					wf_log(ls_messaggio + "~r~nElaborazione Interrotta !!!")
					rollback;
					return
				end if					  
					
				// valorizzo giacenza
				if wf_valorizza_giacenza(ls_cod_prodotto, ref ld_giacenza_sommata, ref ls_messaggio) = -1 then
					g_mb.messagebox("APICE",ls_messaggio + "~r~nElaborazione Interrotta !!!")
					wf_log(ls_messaggio + "~r~nElaborazione Interrotta !!!")
					rollback;
					return
				end if					  

				INSERT INTO lifo  
						( cod_azienda,   
						  cod_prodotto,   
						  anno_lifo,   
						  giacenza_finale,   
						  costo_ultimo,   
						  flag_provvisorio,   
						  strato_provvisorio,   
						  des_prodotto,   
						  costo_medio_ponderato,   
						  rimanenza_strato,   
						  valore_fine_anno )  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ls_cod_prodotto,   
						  :ll_anno_lifo,   
						  :ld_quan_fine_anno,   
						  0,   
						  'N',   
						  0,   
						  :ls_des_prodotto,   
						  :ld_costo_medio_ponderato,   
						  0,   
						  :ld_giacenza_sommata );
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext+"~r~nL'elaborazione prosegue.")
					wf_log("ERRORE-->~r~nErrore in inserimento lifo del prodotto "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+".~r~n" + sqlca.sqlerrtext)
				end if					  
			end choose					  
		
	end if
	
	
	commit;
loop
commit;
fileclose(ll_file)	

st_avanzamento.text = "Elaborazione terminata con successo !!!"

	
end event

type cb_reset from commandbutton within w_lifo
integer x = 2766
integer y = 140
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
parent.triggerevent("pc_retrieve")


end event

type em_path from editmask within w_lifo
integer x = 800
integer y = 380
integer width = 2011
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = stringmask!
end type

type dw_ricerca from u_dw_search within w_lifo
event ue_key pbm_dwnkey
integer x = 46
integer y = 120
integer width = 2295
integer height = 200
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_lifo_search"
boolean border = false
end type

event ue_key;choose case this.getcolumnname()
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
			guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
		end if
end choose

if key = keyenter! then cb_reset.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_lifo
integer x = 23
integer y = 20
integer width = 3131
integer height = 1200
integer taborder = 30
end type

type dw_lifo_det from uo_cs_xx_dw within w_lifo
integer x = 46
integer y = 340
integer width = 3086
integer height = 860
integer taborder = 20
string dataobject = "d_lifo_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_prodotto

ls_cod_prodotto = dw_ricerca.getitemstring(1,"cod_prodotto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end event

event pcd_new;call super::pcd_new;string ls_cod_prodotto

ls_cod_prodotto = dw_ricerca.getitemstring(1,"cod_prodotto")
if isnull(ls_cod_prodotto) then
	pcca.error = c_fatal
	return
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	dec{4} ld_giacenza, ld_valore
	choose case i_colname
		case "giacenza_finale"
			ld_giacenza = dec(i_coltext)
			ld_valore = getitemnumber(getrow(),"costo_medio_ponderato")
			setitem(getrow(),"valore_fine_anno",ld_giacenza * ld_valore)
		case "costo_medio_ponderato"
			ld_giacenza = getitemnumber(getrow(),"giacenza_finale")
			ld_valore = dec(i_coltext)
			setitem(getrow(),"valore_fine_anno",ld_giacenza * ld_valore)
	end choose
end if

end event

