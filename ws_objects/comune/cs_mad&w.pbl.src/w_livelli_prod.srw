﻿$PBExportHeader$w_livelli_prod.srw
$PBExportComments$Finestra Livelli Prodotti
forward
global type w_livelli_prod from w_cs_xx_principale
end type
type cb_spese_trasp_1 from commandbutton within w_livelli_prod
end type
type cb_spese_trasp_2 from commandbutton within w_livelli_prod
end type
type cb_spese_trasp_3 from commandbutton within w_livelli_prod
end type
type cb_spese_trasp_4 from commandbutton within w_livelli_prod
end type
type cb_spese_trasp_5 from commandbutton within w_livelli_prod
end type
type dw_tab_livelli_selezione from datawindow within w_livelli_prod
end type
type cb_blob_livello_1 from commandbutton within w_livelli_prod
end type
type cb_imm_liv_1 from commandbutton within w_livelli_prod
end type
type cb_imm_liv_2 from commandbutton within w_livelli_prod
end type
type cb_imm_liv_3 from commandbutton within w_livelli_prod
end type
type cb_imm_liv_4 from commandbutton within w_livelli_prod
end type
type cb_imm_liv_5 from commandbutton within w_livelli_prod
end type
type dw_tab_livelli_prod_2_det from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_3_det from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_1_det from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_4_det from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_5_det from uo_cs_xx_dw within w_livelli_prod
end type
type cb_blob_livello_5 from commandbutton within w_livelli_prod
end type
type cb_blob_livello_4 from commandbutton within w_livelli_prod
end type
type cb_blob_livello_3 from commandbutton within w_livelli_prod
end type
type cb_blob_livello_2 from commandbutton within w_livelli_prod
end type
type cb_lingue_liv_1 from commandbutton within w_livelli_prod
end type
type cb_lingue_liv_5 from commandbutton within w_livelli_prod
end type
type cb_lingue_liv_4 from commandbutton within w_livelli_prod
end type
type cb_lingue_liv_3 from commandbutton within w_livelli_prod
end type
type cb_lingue_liv_2 from commandbutton within w_livelli_prod
end type
type dw_tab_livelli_prod_1_lista from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_3_lista from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_4_lista from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_5_lista from uo_cs_xx_dw within w_livelli_prod
end type
type dw_tab_livelli_prod_2_lista from uo_cs_xx_dw within w_livelli_prod
end type
type dw_folder from u_folder within w_livelli_prod
end type
end forward

global type w_livelli_prod from w_cs_xx_principale
integer width = 2711
integer height = 2012
string title = "Livelli Prodotti"
cb_spese_trasp_1 cb_spese_trasp_1
cb_spese_trasp_2 cb_spese_trasp_2
cb_spese_trasp_3 cb_spese_trasp_3
cb_spese_trasp_4 cb_spese_trasp_4
cb_spese_trasp_5 cb_spese_trasp_5
dw_tab_livelli_selezione dw_tab_livelli_selezione
cb_blob_livello_1 cb_blob_livello_1
cb_imm_liv_1 cb_imm_liv_1
cb_imm_liv_2 cb_imm_liv_2
cb_imm_liv_3 cb_imm_liv_3
cb_imm_liv_4 cb_imm_liv_4
cb_imm_liv_5 cb_imm_liv_5
dw_tab_livelli_prod_2_det dw_tab_livelli_prod_2_det
dw_tab_livelli_prod_3_det dw_tab_livelli_prod_3_det
dw_tab_livelli_prod_1_det dw_tab_livelli_prod_1_det
dw_tab_livelli_prod_4_det dw_tab_livelli_prod_4_det
dw_tab_livelli_prod_5_det dw_tab_livelli_prod_5_det
cb_blob_livello_5 cb_blob_livello_5
cb_blob_livello_4 cb_blob_livello_4
cb_blob_livello_3 cb_blob_livello_3
cb_blob_livello_2 cb_blob_livello_2
cb_lingue_liv_1 cb_lingue_liv_1
cb_lingue_liv_5 cb_lingue_liv_5
cb_lingue_liv_4 cb_lingue_liv_4
cb_lingue_liv_3 cb_lingue_liv_3
cb_lingue_liv_2 cb_lingue_liv_2
dw_tab_livelli_prod_1_lista dw_tab_livelli_prod_1_lista
dw_tab_livelli_prod_3_lista dw_tab_livelli_prod_3_lista
dw_tab_livelli_prod_4_lista dw_tab_livelli_prod_4_lista
dw_tab_livelli_prod_5_lista dw_tab_livelli_prod_5_lista
dw_tab_livelli_prod_2_lista dw_tab_livelli_prod_2_lista
dw_folder dw_folder
end type
global w_livelli_prod w_livelli_prod

type variables
string is_livello_1, is_livello_2, is_livello_3, is_livello_4
string is_livello_5
end variables

forward prototypes
public function integer wf_carica_label (ref string ws_label_1, ref string ws_label_2, ref string ws_label_3, ref string ws_label_4, ref string ws_label_5)
end prototypes

public function integer wf_carica_label (ref string ws_label_1, ref string ws_label_2, ref string ws_label_3, ref string ws_label_4, ref string ws_label_5);SELECT con_magazzino.label_livello_prod_1,
		 con_magazzino.label_livello_prod_2,
		 con_magazzino.label_livello_prod_3,
		 con_magazzino.label_livello_prod_4,
		 con_magazzino.label_livello_prod_5
 INTO  :ws_label_1,
		 :ws_label_2,
		 :ws_label_3,
		 :ws_label_4,
		 :ws_label_5
  FROM con_magazzino
 WHERE con_magazzino.cod_azienda = :s_cs_xx.cod_azienda;

dw_tab_livelli_selezione.modify("st_cod_livello_prod_1.text='" + ws_label_1 + ":'")
dw_tab_livelli_selezione.modify("st_cod_livello_prod_2.text='" + ws_label_2 + ":'")
dw_tab_livelli_selezione.modify("st_cod_livello_prod_3.text='" + ws_label_3 + ":'")
dw_tab_livelli_selezione.modify("st_cod_livello_prod_4.text='" + ws_label_4 + ":'")
dw_tab_livelli_selezione.modify("st_cod_livello_prod_5.text='" + ws_label_5 + ":'")
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
string ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3
string ls_cod_livello_prod_4, ls_cod_livello_prod_5


wf_carica_label(ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5)

l_objects[1] = dw_tab_livelli_prod_1_lista
l_objects[2] = dw_tab_livelli_prod_1_det
l_objects[3] = cb_blob_livello_1
l_objects[4] = cb_imm_liv_1
l_objects[5] = cb_lingue_liv_1
l_objects[6] = cb_spese_trasp_1
dw_folder.fu_AssignTab(1, ls_cod_livello_prod_1, l_Objects[])

l_objects[1] = dw_tab_livelli_prod_2_lista
l_objects[2] = dw_tab_livelli_prod_2_det
l_objects[3] = cb_blob_livello_2
l_objects[4] = cb_imm_liv_2
l_objects[5] = cb_lingue_liv_2
l_objects[6] = cb_spese_trasp_2
dw_folder.fu_AssignTab(2, ls_cod_livello_prod_2, l_Objects[])

l_objects[1] = dw_tab_livelli_prod_3_lista
l_objects[2] = dw_tab_livelli_prod_3_det
l_objects[3] = cb_blob_livello_3
l_objects[4] = cb_imm_liv_3
l_objects[5] = cb_lingue_liv_3
l_objects[6] = cb_spese_trasp_3
dw_folder.fu_AssignTab(3, ls_cod_livello_prod_3, l_Objects[])

l_objects[1] = dw_tab_livelli_prod_4_lista
l_objects[2] = dw_tab_livelli_prod_4_det
l_objects[3] = cb_blob_livello_4
l_objects[4] = cb_imm_liv_4
l_objects[5] = cb_lingue_liv_4
l_objects[6] = cb_spese_trasp_4
dw_folder.fu_AssignTab(4, ls_cod_livello_prod_4, l_Objects[])

l_objects[1] = dw_tab_livelli_prod_5_lista
l_objects[2] = dw_tab_livelli_prod_5_det
l_objects[3] = cb_blob_livello_5
l_objects[4] = cb_imm_liv_5
l_objects[5] = cb_lingue_liv_5
l_objects[6] = cb_spese_trasp_5
dw_folder.fu_AssignTab(5, ls_cod_livello_prod_5, l_Objects[])

dw_folder.fu_FolderCreate(5,5)
dw_folder.fu_SelectTab(1)

dw_tab_livelli_prod_1_lista.set_dw_key("cod_azienda")
dw_tab_livelli_prod_1_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_livelli_prod_1_det.set_dw_options(sqlca,dw_tab_livelli_prod_1_lista,c_sharedata+c_scrollparent,c_default)

dw_tab_livelli_prod_2_lista.set_dw_key("cod_azienda")
dw_tab_livelli_prod_2_lista.set_dw_key("cod_livello_prod_1")
dw_tab_livelli_prod_2_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_livelli_prod_2_det.set_dw_options(sqlca,dw_tab_livelli_prod_2_lista,c_sharedata+c_scrollparent,c_default)

dw_tab_livelli_prod_3_lista.set_dw_key("cod_azienda")
dw_tab_livelli_prod_3_lista.set_dw_key("cod_livello_prod_1")
dw_tab_livelli_prod_3_lista.set_dw_key("cod_livello_prod_2")
dw_tab_livelli_prod_3_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_livelli_prod_3_det.set_dw_options(sqlca,dw_tab_livelli_prod_3_lista,c_sharedata+c_scrollparent,c_default)

dw_tab_livelli_prod_4_lista.set_dw_key("cod_azienda")
dw_tab_livelli_prod_4_lista.set_dw_key("cod_livello_prod_1")
dw_tab_livelli_prod_4_lista.set_dw_key("cod_livello_prod_2")
dw_tab_livelli_prod_4_lista.set_dw_key("cod_livello_prod_3")
dw_tab_livelli_prod_4_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_livelli_prod_4_det.set_dw_options(sqlca,dw_tab_livelli_prod_4_lista,c_sharedata+c_scrollparent,c_default)

dw_tab_livelli_prod_5_lista.set_dw_key("cod_azienda")
dw_tab_livelli_prod_5_lista.set_dw_key("cod_livello_prod_1")
dw_tab_livelli_prod_5_lista.set_dw_key("cod_livello_prod_2")
dw_tab_livelli_prod_5_lista.set_dw_key("cod_livello_prod_3")
dw_tab_livelli_prod_5_lista.set_dw_key("cod_livello_prod_4")
dw_tab_livelli_prod_5_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_livelli_prod_5_det.set_dw_options(sqlca,dw_tab_livelli_prod_5_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_livelli_prod_1_lista

dw_folder.fu_hidetab(2)
dw_folder.fu_hidetab(3)
dw_folder.fu_hidetab(4)
dw_folder.fu_hidetab(5)

is_livello_1 ="."
is_livello_2 ="."
is_livello_3 ="."
is_livello_4 ="."
is_livello_5 ="."

dw_tab_livelli_selezione.insertrow(1)

end event

on w_livelli_prod.create
int iCurrent
call super::create
this.cb_spese_trasp_1=create cb_spese_trasp_1
this.cb_spese_trasp_2=create cb_spese_trasp_2
this.cb_spese_trasp_3=create cb_spese_trasp_3
this.cb_spese_trasp_4=create cb_spese_trasp_4
this.cb_spese_trasp_5=create cb_spese_trasp_5
this.dw_tab_livelli_selezione=create dw_tab_livelli_selezione
this.cb_blob_livello_1=create cb_blob_livello_1
this.cb_imm_liv_1=create cb_imm_liv_1
this.cb_imm_liv_2=create cb_imm_liv_2
this.cb_imm_liv_3=create cb_imm_liv_3
this.cb_imm_liv_4=create cb_imm_liv_4
this.cb_imm_liv_5=create cb_imm_liv_5
this.dw_tab_livelli_prod_2_det=create dw_tab_livelli_prod_2_det
this.dw_tab_livelli_prod_3_det=create dw_tab_livelli_prod_3_det
this.dw_tab_livelli_prod_1_det=create dw_tab_livelli_prod_1_det
this.dw_tab_livelli_prod_4_det=create dw_tab_livelli_prod_4_det
this.dw_tab_livelli_prod_5_det=create dw_tab_livelli_prod_5_det
this.cb_blob_livello_5=create cb_blob_livello_5
this.cb_blob_livello_4=create cb_blob_livello_4
this.cb_blob_livello_3=create cb_blob_livello_3
this.cb_blob_livello_2=create cb_blob_livello_2
this.cb_lingue_liv_1=create cb_lingue_liv_1
this.cb_lingue_liv_5=create cb_lingue_liv_5
this.cb_lingue_liv_4=create cb_lingue_liv_4
this.cb_lingue_liv_3=create cb_lingue_liv_3
this.cb_lingue_liv_2=create cb_lingue_liv_2
this.dw_tab_livelli_prod_1_lista=create dw_tab_livelli_prod_1_lista
this.dw_tab_livelli_prod_3_lista=create dw_tab_livelli_prod_3_lista
this.dw_tab_livelli_prod_4_lista=create dw_tab_livelli_prod_4_lista
this.dw_tab_livelli_prod_5_lista=create dw_tab_livelli_prod_5_lista
this.dw_tab_livelli_prod_2_lista=create dw_tab_livelli_prod_2_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_spese_trasp_1
this.Control[iCurrent+2]=this.cb_spese_trasp_2
this.Control[iCurrent+3]=this.cb_spese_trasp_3
this.Control[iCurrent+4]=this.cb_spese_trasp_4
this.Control[iCurrent+5]=this.cb_spese_trasp_5
this.Control[iCurrent+6]=this.dw_tab_livelli_selezione
this.Control[iCurrent+7]=this.cb_blob_livello_1
this.Control[iCurrent+8]=this.cb_imm_liv_1
this.Control[iCurrent+9]=this.cb_imm_liv_2
this.Control[iCurrent+10]=this.cb_imm_liv_3
this.Control[iCurrent+11]=this.cb_imm_liv_4
this.Control[iCurrent+12]=this.cb_imm_liv_5
this.Control[iCurrent+13]=this.dw_tab_livelli_prod_2_det
this.Control[iCurrent+14]=this.dw_tab_livelli_prod_3_det
this.Control[iCurrent+15]=this.dw_tab_livelli_prod_1_det
this.Control[iCurrent+16]=this.dw_tab_livelli_prod_4_det
this.Control[iCurrent+17]=this.dw_tab_livelli_prod_5_det
this.Control[iCurrent+18]=this.cb_blob_livello_5
this.Control[iCurrent+19]=this.cb_blob_livello_4
this.Control[iCurrent+20]=this.cb_blob_livello_3
this.Control[iCurrent+21]=this.cb_blob_livello_2
this.Control[iCurrent+22]=this.cb_lingue_liv_1
this.Control[iCurrent+23]=this.cb_lingue_liv_5
this.Control[iCurrent+24]=this.cb_lingue_liv_4
this.Control[iCurrent+25]=this.cb_lingue_liv_3
this.Control[iCurrent+26]=this.cb_lingue_liv_2
this.Control[iCurrent+27]=this.dw_tab_livelli_prod_1_lista
this.Control[iCurrent+28]=this.dw_tab_livelli_prod_3_lista
this.Control[iCurrent+29]=this.dw_tab_livelli_prod_4_lista
this.Control[iCurrent+30]=this.dw_tab_livelli_prod_5_lista
this.Control[iCurrent+31]=this.dw_tab_livelli_prod_2_lista
this.Control[iCurrent+32]=this.dw_folder
end on

on w_livelli_prod.destroy
call super::destroy
destroy(this.cb_spese_trasp_1)
destroy(this.cb_spese_trasp_2)
destroy(this.cb_spese_trasp_3)
destroy(this.cb_spese_trasp_4)
destroy(this.cb_spese_trasp_5)
destroy(this.dw_tab_livelli_selezione)
destroy(this.cb_blob_livello_1)
destroy(this.cb_imm_liv_1)
destroy(this.cb_imm_liv_2)
destroy(this.cb_imm_liv_3)
destroy(this.cb_imm_liv_4)
destroy(this.cb_imm_liv_5)
destroy(this.dw_tab_livelli_prod_2_det)
destroy(this.dw_tab_livelli_prod_3_det)
destroy(this.dw_tab_livelli_prod_1_det)
destroy(this.dw_tab_livelli_prod_4_det)
destroy(this.dw_tab_livelli_prod_5_det)
destroy(this.cb_blob_livello_5)
destroy(this.cb_blob_livello_4)
destroy(this.cb_blob_livello_3)
destroy(this.cb_blob_livello_2)
destroy(this.cb_lingue_liv_1)
destroy(this.cb_lingue_liv_5)
destroy(this.cb_lingue_liv_4)
destroy(this.cb_lingue_liv_3)
destroy(this.cb_lingue_liv_2)
destroy(this.dw_tab_livelli_prod_1_lista)
destroy(this.dw_tab_livelli_prod_3_lista)
destroy(this.dw_tab_livelli_prod_4_lista)
destroy(this.dw_tab_livelli_prod_5_lista)
destroy(this.dw_tab_livelli_prod_2_lista)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_1",sqlca,&
                 "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
                 "tab_livelli_prod_1.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_spese_trasp_1 from commandbutton within w_livelli_prod
integer x = 827
integer y = 1740
integer width = 489
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spese Trasporto"
end type

event clicked;string			ls_livello_prod_1


ls_livello_prod_1 = dw_tab_livelli_prod_1_lista.getitemstring(dw_tab_livelli_prod_1_lista.getrow(),"cod_livello_prod_1")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 then return


window_open_parm(w_livelli_prod_spese_trasp_1, -1, dw_tab_livelli_prod_1_lista )
end event

type cb_spese_trasp_2 from commandbutton within w_livelli_prod
integer x = 827
integer y = 1740
integer width = 489
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spese Trasporto"
end type

event clicked;string			ls_livello_prod_1, ls_livello_prod_2

ls_livello_prod_1 = dw_tab_livelli_prod_2_lista.getitemstring(dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_2_lista.getitemstring(dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_2")

if 	isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
	isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 then return


window_open_parm(w_livelli_prod_spese_trasp_2, -1, dw_tab_livelli_prod_2_lista )
end event

type cb_spese_trasp_3 from commandbutton within w_livelli_prod
integer x = 827
integer y = 1740
integer width = 489
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spese Trasporto"
end type

event clicked;string ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3

ls_livello_prod_1 = dw_tab_livelli_prod_3_lista.getitemstring(dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_3_lista.getitemstring(dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_3_lista.getitemstring(dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_3")

if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 then return
	

window_open_parm(w_livelli_prod_spese_trasp_3, -1, dw_tab_livelli_prod_3_lista)
end event

type cb_spese_trasp_4 from commandbutton within w_livelli_prod
integer x = 827
integer y = 1740
integer width = 489
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spese Trasporto"
end type

event clicked;string ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3, ls_livello_prod_4

ls_livello_prod_1 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_3")
ls_livello_prod_4 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_4")

if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 or &
   isnull(ls_livello_prod_4) or len(ls_livello_prod_4) = 0 then return
	

window_open_parm(w_livelli_prod_spese_trasp_4, -1, dw_tab_livelli_prod_4_lista )
end event

type cb_spese_trasp_5 from commandbutton within w_livelli_prod
integer x = 827
integer y = 1740
integer width = 489
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spese Trasporto"
end type

event clicked;string ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3, ls_livello_prod_4, ls_livello_prod_5


ls_livello_prod_1 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_3")
ls_livello_prod_4 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_4")
ls_livello_prod_5 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_5")

if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 or &
   isnull(ls_livello_prod_4) or len(ls_livello_prod_4) = 0 or &
   isnull(ls_livello_prod_5) or len(ls_livello_prod_5) = 0  then return
	

window_open_parm(w_livelli_prod_spese_trasp_5, -1, dw_tab_livelli_prod_5_lista )
end event

type dw_tab_livelli_selezione from datawindow within w_livelli_prod
integer x = 23
integer y = 20
integer width = 2629
integer height = 380
integer taborder = 10
string dataobject = "d_tab_livelli_selezione"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;string ls_where, ls_null
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]


setnull(ls_null)
choose case getcolumnname()
	case "rs_cod_livello_1"
		if is_livello_1 <> gettext() then
			is_livello_1 = gettext()
			is_livello_2 = "."
			is_livello_3 = "."
			is_livello_4 = "."

			setitem(1, "rs_cod_livello_2", ls_null)
			setitem(1, "rs_cod_livello_3", ls_null)
			setitem(1, "rs_cod_livello_4", ls_null)
			setitem(1, "rs_cod_livello_5", ls_null)
			
         if isnull(is_livello_1) or len(is_livello_1) = 0 then
				dw_folder.fu_showtab(1)
				dw_folder.fu_selecttab(1)
				dw_folder.fu_hidetab(2)
				iuo_dw_main = dw_tab_livelli_prod_1_lista
			else
				dw_folder.fu_showtab(2)
				dw_folder.fu_selecttab(2)
				dw_folder.fu_hidetab(1)
				iuo_dw_main = dw_tab_livelli_prod_2_lista
			end if
			dw_folder.fu_hidetab(3) 
			dw_folder.fu_hidetab(4) 
			dw_folder.fu_hidetab(5)
			iuo_dw_main.change_dw_current()
			parent.triggerevent("pc_retrieve")
		  end if

	case "rs_cod_livello_2"
		if is_livello_2 <> gettext() then
			is_livello_2 = gettext()
			is_livello_3 = "."
			is_livello_4 = "."
			
			setitem(1, "rs_cod_livello_3", ls_null)
			setitem(1, "rs_cod_livello_4", ls_null)
			setitem(1, "rs_cod_livello_5", ls_null)
			
         if isnull(is_livello_2) or len(is_livello_2) = 0 then
				dw_folder.fu_showtab(2)
				dw_folder.fu_selecttab(2)
				dw_folder.fu_hidetab(3)
				iuo_dw_main = dw_tab_livelli_prod_2_lista
			else
				dw_folder.fu_showtab(3)
				dw_folder.fu_selecttab(3)
				dw_folder.fu_hidetab(2)
				iuo_dw_main = dw_tab_livelli_prod_3_lista
			end if
			dw_folder.fu_hidetab(4) 
			dw_folder.fu_hidetab(5)
			dw_folder.fu_hidetab(1)
			iuo_dw_main.change_dw_current()
			parent.triggerevent("pc_retrieve")
		  end if

	case "rs_cod_livello_3"
		if is_livello_3 <> gettext() then
			is_livello_3 = gettext()
			is_livello_4 = "."
			
			setitem(1, "rs_cod_livello_4", ls_null)
			setitem(1, "rs_cod_livello_5", ls_null)
			
         if isnull(is_livello_3) or len(is_livello_3) = 0 then
				dw_folder.fu_showtab(3)
				dw_folder.fu_selecttab(3)
				dw_folder.fu_hidetab(4)
				iuo_dw_main = dw_tab_livelli_prod_3_lista
			else
				dw_folder.fu_showtab(4)
				dw_folder.fu_selecttab(4)
				dw_folder.fu_hidetab(3)
				iuo_dw_main = dw_tab_livelli_prod_4_lista
			end if
			dw_folder.fu_hidetab(5) 
			dw_folder.fu_hidetab(1) 
			dw_folder.fu_hidetab(2) 
			iuo_dw_main.change_dw_current()
			parent.triggerevent("pc_retrieve")
		  end if

	case "rs_cod_livello_4"
		if is_livello_4 <> gettext()  then
			is_livello_4 = gettext()
			
         if isnull(is_livello_4) or len(is_livello_4) = 0 then
				dw_folder.fu_showtab(4)
				dw_folder.fu_selecttab(4)
				dw_folder.fu_hidetab(5)
				iuo_dw_main = dw_tab_livelli_prod_4_lista
			else
				dw_folder.fu_showtab(5)
				dw_folder.fu_selecttab(5)
				dw_folder.fu_hidetab(4)
				iuo_dw_main = dw_tab_livelli_prod_5_lista
			end if
			dw_folder.fu_hidetab(1) 
			dw_folder.fu_hidetab(2) 
			dw_folder.fu_hidetab(3) 
			iuo_dw_main.change_dw_current()
			parent.triggerevent("pc_retrieve")
		  end if
end choose

f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_2",sqlca,&
	  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
	  "(tab_livelli_prod_2.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_2.cod_livello_prod_1 = '" + is_livello_1 + "')" )
	  
f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_3",sqlca,&
	  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
	  "(tab_livelli_prod_3.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_3.cod_livello_prod_1 = '" + is_livello_1 + "') and  " + &
	  "(tab_livelli_prod_3.cod_livello_prod_2 = '" + is_livello_2 + "')"  )
	  
f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_4",sqlca,&
	  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
	  "(tab_livelli_prod_4.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_1 = '" + is_livello_1 + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_2 = '" + is_livello_2 + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_3 = '" + is_livello_3 + "')"  )

end event

type cb_blob_livello_1 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 2126
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_db, ls_doc
integer li_i, li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ls_livello_prod_1 = dw_tab_livelli_prod_1_lista.getitemstring(dw_tab_livelli_prod_1_lista.getrow(),"cod_livello_prod_1")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 then return


select prog_mimetype
into :ll_prog_mimetype
from tab_livelli_prod_1
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1;
	
selectblob blob
into :lbl_blob
from tab_livelli_prod_1
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_livelli_prod_1
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1;
	else
		updateblob tab_livelli_prod_1
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1;
			
		update tab_livelli_prod_1
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if


// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tab_livelli_prod_1.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_1
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 
//	using      sqlcb;
//
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tab_livelli_prod_1.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_1
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 ;
//
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_livelli_prod_1
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 
//		using      sqlcb;
//		
//		destroy    sqlcb;
//		
//	else
//		
//	   updateblob tab_livelli_prod_1
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 ;
//					  
//	end if
//
//   update     tab_livelli_prod_1
//   set        path_documento = :s_cs_xx.parametri.parametro_s_1
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//              cod_livello_prod_1 = :ls_livello_prod_1 ;
//
//   commit;
//end if
end event

type cb_imm_liv_1 from commandbutton within w_livelli_prod
integer x = 1737
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_livello_prod_1
integer li_return


ls_livello_prod_1 = dw_tab_livelli_prod_1_lista.getitemstring(dw_tab_livelli_prod_1_lista.getrow(),"cod_livello_prod_1")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) < 1 then return

li_return = GetFileOpenName("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")

update     tab_livelli_prod_1
set        tab_livelli_prod_1.path_documento = :ls_nomefile
where      tab_livelli_prod_1.cod_azienda = :s_cs_xx.cod_azienda and
			  tab_livelli_prod_1.cod_livello_prod_1 = :ls_livello_prod_1 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
end event

type cb_imm_liv_2 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 1737
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_livello_prod_2
integer li_return


ls_livello_prod_2 = dw_tab_livelli_prod_2_lista.getitemstring(dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_2")
if isnull(ls_livello_prod_2) or len(ls_livello_prod_2) < 1 then return

li_return = GetFileOpenName("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")

update     tab_livelli_prod_2
set        tab_livelli_prod_2.path_documento = :ls_nomefile
where      tab_livelli_prod_2.cod_azienda = :s_cs_xx.cod_azienda and
			  tab_livelli_prod_2.cod_livello_prod_2 = :ls_livello_prod_2 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
end event

type cb_imm_liv_3 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 1737
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_livello_prod_3
integer li_return


ls_livello_prod_3 = dw_tab_livelli_prod_3_lista.getitemstring(dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_3")
if isnull(ls_livello_prod_3) or len(ls_livello_prod_3) < 1 then return

li_return = GetFileOpenName("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")

update     tab_livelli_prod_3
set        tab_livelli_prod_3.path_documento = :ls_nomefile
where      tab_livelli_prod_3.cod_azienda = :s_cs_xx.cod_azienda and
			  tab_livelli_prod_3.cod_livello_prod_3 = :ls_livello_prod_3 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
end event

type cb_imm_liv_4 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 1737
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_livello_prod_4
integer li_return


ls_livello_prod_4 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_4")
if isnull(ls_livello_prod_4) or len(ls_livello_prod_4) < 1 then return

li_return = GetFileOpenName("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")

update     tab_livelli_prod_4
set        tab_livelli_prod_4.path_documento = :ls_nomefile
where      tab_livelli_prod_4.cod_azienda = :s_cs_xx.cod_azienda and
			  tab_livelli_prod_4.cod_livello_prod_4 = :ls_livello_prod_4 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
end event

type cb_imm_liv_5 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 1737
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_livello_prod_5
integer li_return


ls_livello_prod_5 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_5")
if isnull(ls_livello_prod_5) or len(ls_livello_prod_5) < 1 then return

li_return = GetFileOpenName("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")

update     tab_livelli_prod_5
set        tab_livelli_prod_5.path_documento = :ls_nomefile
where      tab_livelli_prod_5.cod_azienda = :s_cs_xx.cod_azienda and
			  tab_livelli_prod_5.cod_livello_prod_5 = :ls_livello_prod_5 ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
end event

type dw_tab_livelli_prod_2_det from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 1040
integer width = 2537
integer height = 696
integer taborder = 210
string dataobject = "d_tab_livelli_prod_2_det"
boolean border = false
end type

type dw_tab_livelli_prod_3_det from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 1040
integer width = 2537
integer height = 696
integer taborder = 200
string dataobject = "d_tab_livelli_prod_3_det"
boolean border = false
end type

type dw_tab_livelli_prod_1_det from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 1040
integer width = 2537
integer height = 700
integer taborder = 170
string dataobject = "d_tab_livelli_prod_1_det"
boolean border = false
end type

type dw_tab_livelli_prod_4_det from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 1040
integer width = 2537
integer height = 696
integer taborder = 190
string dataobject = "d_tab_livelli_prod_4_det"
boolean border = false
end type

type dw_tab_livelli_prod_5_det from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 1040
integer width = 2537
integer height = 700
integer taborder = 180
string dataobject = "d_tab_livelli_prod_5_det"
boolean border = false
end type

type cb_blob_livello_5 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 2126
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3, ls_doc
string ls_livello_prod_4, ls_livello_prod_5, ls_db
integer li_i, li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ls_livello_prod_1 = dw_tab_livelli_prod_5_lista.getitemstring(&
                    dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_5_lista.getitemstring(&
                    dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_5_lista.getitemstring(&
                    dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_3")
ls_livello_prod_4 = dw_tab_livelli_prod_5_lista.getitemstring(&
                    dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_4")
ls_livello_prod_5 = dw_tab_livelli_prod_5_lista.getitemstring(&
                    dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_5")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 or &
   isnull(ls_livello_prod_4) or len(ls_livello_prod_4) = 0 or &
   isnull(ls_livello_prod_5) or len(ls_livello_prod_5) = 0  then return
	
	
select prog_mimetype
into :ll_prog_mimetype
from tab_livelli_prod_5
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3 and
	cod_livello_prod_3 = :ls_livello_prod_4 and
	cod_livello_prod_3 = :ls_livello_prod_5;
	
selectblob blob
into :lbl_blob
from tab_livelli_prod_5
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3 and
	cod_livello_prod_3 = :ls_livello_prod_4 and
	cod_livello_prod_3 = :ls_livello_prod_5;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_livelli_prod_5
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4 and
			cod_livello_prod_3 = :ls_livello_prod_5;
		
	else
		updateblob tab_livelli_prod_5
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4 and
			cod_livello_prod_3 = :ls_livello_prod_5;
			
		update tab_livelli_prod_5
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4 and
			cod_livello_prod_3 = :ls_livello_prod_5;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tab_livelli_prod_5.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_5
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//	           cod_livello_prod_3 = :ls_livello_prod_3 and
//	           cod_livello_prod_4 = :ls_livello_prod_4 and
//	           cod_livello_prod_5 = :ls_livello_prod_5 
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tab_livelli_prod_5.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_5
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//	           cod_livello_prod_3 = :ls_livello_prod_3 and
//	           cod_livello_prod_4 = :ls_livello_prod_4 and
//	           cod_livello_prod_5 = :ls_livello_prod_5 ;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_livelli_prod_5
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 and
//	              cod_livello_prod_4 = :ls_livello_prod_4 and
//	              cod_livello_prod_5 = :ls_livello_prod_5 
//		using      sqlcb;
//		
//		destroy    sqlcb;
//		
//	else
//		
//	   updateblob tab_livelli_prod_5
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 and
//	              cod_livello_prod_4 = :ls_livello_prod_4 and
//	              cod_livello_prod_5 = :ls_livello_prod_5 ;
//					  
//	end if
//	
//   update     tab_livelli_prod_5
//   set        path_documento = :s_cs_xx.parametri.parametro_s_1
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//              cod_livello_prod_1 = :ls_livello_prod_1 and
//              cod_livello_prod_2 = :ls_livello_prod_2 and
//              cod_livello_prod_3 = :ls_livello_prod_3 and
//              cod_livello_prod_4 = :ls_livello_prod_4 and
//              cod_livello_prod_5 = :ls_livello_prod_5 ;
//   commit;
//end if
//
end event

type cb_blob_livello_4 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 2126
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3, ls_livello_prod_4, ls_db, ls_doc
integer li_i, li_risposta 
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ls_livello_prod_1 = dw_tab_livelli_prod_4_lista.getitemstring(&
                    dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_4_lista.getitemstring(&
                    dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_4_lista.getitemstring(&
                    dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_3")
ls_livello_prod_4 = dw_tab_livelli_prod_4_lista.getitemstring(&
                    dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_4")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 or &
   isnull(ls_livello_prod_4) or len(ls_livello_prod_4) = 0 then return
	
	
select prog_mimetype
into :ll_prog_mimetype
from tab_livelli_prod_4
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3 and
	cod_livello_prod_3 = :ls_livello_prod_4;
	
selectblob blob
into :lbl_blob
from tab_livelli_prod_4
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3 and
	cod_livello_prod_3 = :ls_livello_prod_4;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_livelli_prod_4
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4;
			
	else
		updateblob tab_livelli_prod_4
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4;
			
		update tab_livelli_prod_4
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3 and
			cod_livello_prod_3 = :ls_livello_prod_4;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tab_livelli_prod_4.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_4
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//	           cod_livello_prod_3 = :ls_livello_prod_3 and
//	           cod_livello_prod_4 = :ls_livello_prod_4 
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tab_livelli_prod_4.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_4
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//	           cod_livello_prod_3 = :ls_livello_prod_3 and
//	           cod_livello_prod_4 = :ls_livello_prod_4 ;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_livelli_prod_4
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 and
//	              cod_livello_prod_4 = :ls_livello_prod_4 
//		using      sqlcb;
//		
//		destroy    sqlcb;
//		
//	else
//		
//	   updateblob tab_livelli_prod_4
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 and
//	              cod_livello_prod_4 = :ls_livello_prod_4 ;
//					  
//	end if					  
//	
//   update     tab_livelli_prod_4
//   set        path_documento = :s_cs_xx.parametri.parametro_s_1
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//              cod_livello_prod_1 = :ls_livello_prod_1 and
//              cod_livello_prod_2 = :ls_livello_prod_2 and
//              cod_livello_prod_3 = :ls_livello_prod_3 and
//              cod_livello_prod_4 = :ls_livello_prod_4 ;
//   commit;
//end if
//
end event

type cb_blob_livello_3 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 2126
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_livello_prod_2, ls_livello_prod_3, ls_db, ls_doc
integer li_i, li_risposta 
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ls_livello_prod_1 = dw_tab_livelli_prod_3_lista.getitemstring(&
                    dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_3_lista.getitemstring(&
                    dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_2")
ls_livello_prod_3 = dw_tab_livelli_prod_3_lista.getitemstring(&
                    dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_3")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 or &
   isnull(ls_livello_prod_3) or len(ls_livello_prod_3) = 0 then return
	
	
select prog_mimetype
into :ll_prog_mimetype
from tab_livelli_prod_3
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3;
	
selectblob blob
into :lbl_blob
from tab_livelli_prod_3
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2 and
	cod_livello_prod_3 = :ls_livello_prod_3;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_livelli_prod_3
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3;
			
	else
		updateblob tab_livelli_prod_3
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3;
			
		update tab_livelli_prod_3
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2 and
			cod_livello_prod_3 = :ls_livello_prod_3;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tab_livelli_prod_3.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_3
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//   	        cod_livello_prod_3 = :ls_livello_prod_3 
//	using sqlcb;
//
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tab_livelli_prod_3.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_3
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 and
//   	        cod_livello_prod_3 = :ls_livello_prod_3 ;
//
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_livelli_prod_3
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tab_livelli_prod_3
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 and
//	              cod_livello_prod_3 = :ls_livello_prod_3 ;
//	
//	end if
//	
//   update     tab_livelli_prod_3
//   set        path_documento = :s_cs_xx.parametri.parametro_s_1
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//              cod_livello_prod_1 = :ls_livello_prod_1 and
//              cod_livello_prod_2 = :ls_livello_prod_2 and
//              cod_livello_prod_3 = :ls_livello_prod_3 ;
//   commit;
//end if
//
end event

type cb_blob_livello_2 from commandbutton within w_livelli_prod
event clicked pbm_bnclicked
integer x = 2126
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_blob, ls_livello_prod_1, ls_livello_prod_2, ls_db, ls_doc
integer li_i, li_risposta
long ll_prog_mimetype
transaction sqlcb
blob lbl_null, lbl_blob

setnull(lbl_null)
ls_livello_prod_1 = dw_tab_livelli_prod_2_lista.getitemstring(&
                    dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_1")
ls_livello_prod_2 = dw_tab_livelli_prod_2_lista.getitemstring(&
                    dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_2")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) = 0 or &
   isnull(ls_livello_prod_2) or len(ls_livello_prod_2) = 0 then return
	
select prog_mimetype
into :ll_prog_mimetype
from tab_livelli_prod_2
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2;
	
selectblob blob
into :lbl_blob
from tab_livelli_prod_2
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_livello_prod_1 = :ls_livello_prod_1 and
	cod_livello_prod_2 = :ls_livello_prod_2;

if sqlca.sqlcode <> 0 then
	lbl_blob = lbl_null
end if

ls_doc = "Documento"
if f_documento(ref lbl_blob, ls_doc, ll_prog_mimetype) then
	// aggiorno documento
	
	if isnull(lbl_blob) or len(lbl_blob) < 1 then
		update tab_livelli_prod_2
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2;
	else
		updateblob tab_livelli_prod_2
		set blob = :lbl_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2;
			
		update tab_livelli_prod_2
		set prog_mimetype = :ll_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_livello_prod_1 = :ls_livello_prod_1 and
			cod_livello_prod_2 = :ls_livello_prod_2;
		
	end if
		
	if sqlca.sqlcode <> 0 then
		g_mb.error("", "Errore durante il salvataggio del documento.~r~n" + sqlca.sqlerrtext)
		return
	end if
	
end if

// 15-07-2002 modifiche Michela: controllo l'enginetype
//ls_db = f_db()
//if ls_db = "MSSQL" then
//	
//	li_risposta = f_crea_sqlcb(sqlcb)
//	
//	selectblob tab_livelli_prod_2.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_2
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 
//	using      sqlcb;
//	
//	if sqlcb.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//	destroy sqlcb;
//	
//else
//	
//	selectblob tab_livelli_prod_2.blob
//	into       :s_cs_xx.parametri.parametro_bl_1
//	from       tab_livelli_prod_2
//	where      cod_azienda = :s_cs_xx.cod_azienda and
//	           cod_livello_prod_1 = :ls_livello_prod_1 and
//	           cod_livello_prod_2 = :ls_livello_prod_2 ;
//	
//	if sqlca.sqlcode <> 0 then
//	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
//	end if
//	
//end if
//
//window_open(w_ole, 0)
//
//if not isnull(s_cs_xx.parametri.parametro_bl_1) then
//	
//	if ls_db = "MSSQL" then
//		
//		li_risposta = f_crea_sqlcb(sqlcb)
//		
//	   updateblob tab_livelli_prod_2
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 
//		using      sqlcb;
//		
//		destroy sqlcb;
//		
//	else
//		
//	   updateblob tab_livelli_prod_2
//	   set        blob = :s_cs_xx.parametri.parametro_bl_1
//	   where      cod_azienda = :s_cs_xx.cod_azienda and
//	              cod_livello_prod_1 = :ls_livello_prod_1 and
//	              cod_livello_prod_2 = :ls_livello_prod_2 ;
//	
//	end if
//	
//   update     tab_livelli_prod_2
//   set        path_documento = :s_cs_xx.parametri.parametro_s_1
//   where      cod_azienda = :s_cs_xx.cod_azienda and
//              cod_livello_prod_1 = :ls_livello_prod_1 and
//              cod_livello_prod_2 = :ls_livello_prod_2 ;
//   commit;
//end if
//
end event

type cb_lingue_liv_1 from commandbutton within w_livelli_prod
integer x = 1349
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;string ls_livello_prod_1

ls_livello_prod_1 = dw_tab_livelli_prod_1_lista.getitemstring(dw_tab_livelli_prod_1_lista.getrow(),"cod_livello_prod_1")
if isnull(ls_livello_prod_1) or len(ls_livello_prod_1) < 1 then return



window_open_parm(w_livello_prod_1_lingua, -1,dw_tab_livelli_prod_1_lista )
end event

type cb_lingue_liv_5 from commandbutton within w_livelli_prod
integer x = 1349
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 141
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;string ls_livello_prod_5

ls_livello_prod_5 = dw_tab_livelli_prod_5_lista.getitemstring(dw_tab_livelli_prod_5_lista.getrow(),"cod_livello_prod_5")
if isnull(ls_livello_prod_5) or len(ls_livello_prod_5) < 1 then return

window_open_parm(w_livello_prod_5_lingua, -1,dw_tab_livelli_prod_5_lista )
end event

type cb_lingue_liv_4 from commandbutton within w_livelli_prod
integer x = 1349
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;string ls_livello_prod_4

ls_livello_prod_4 = dw_tab_livelli_prod_4_lista.getitemstring(dw_tab_livelli_prod_4_lista.getrow(),"cod_livello_prod_4")
if isnull(ls_livello_prod_4) or len(ls_livello_prod_4) < 1 then return


window_open_parm(w_livello_prod_4_lingua, -1,dw_tab_livelli_prod_4_lista )
end event

type cb_lingue_liv_3 from commandbutton within w_livelli_prod
integer x = 1349
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;string ls_livello_prod_3

ls_livello_prod_3 = dw_tab_livelli_prod_3_lista.getitemstring(dw_tab_livelli_prod_3_lista.getrow(),"cod_livello_prod_3")
if isnull(ls_livello_prod_3) or len(ls_livello_prod_3) < 1 then return



window_open_parm(w_livello_prod_3_lingua, -1,dw_tab_livelli_prod_3_lista )
end event

type cb_lingue_liv_2 from commandbutton within w_livelli_prod
integer x = 1349
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;string ls_livello_prod_2

ls_livello_prod_2 = dw_tab_livelli_prod_2_lista.getitemstring(dw_tab_livelli_prod_2_lista.getrow(),"cod_livello_prod_2")
if isnull(ls_livello_prod_2) or len(ls_livello_prod_2) < 1 then return


window_open_parm(w_livello_prod_2_lingua, -1,dw_tab_livelli_prod_2_lista )
end event

type dw_tab_livelli_prod_1_lista from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 540
integer width = 2537
integer height = 500
integer taborder = 260
string dataobject = "d_tab_livelli_prod_1_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updateend;call super::updateend;f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_1",sqlca,&
                 "tab_livelli_prod_1","cod_livello_prod_1","des_livello", &
                 "tab_livelli_prod_1.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pcd_new;call super::pcd_new;dw_tab_livelli_prod_1_det.setcolumn(2)
end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_liv_prod_1
   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_livelli_prod_1_lingue"
      ls_codice  = "cod_livello_prod_1"
      ls_cod_liv_prod_1 = this.getitemstring(li_i, "cod_livello_prod_1", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_liv_prod_1)
   next
end if
end event

type dw_tab_livelli_prod_3_lista from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 540
integer width = 2537
integer height = 500
integer taborder = 220
string dataobject = "d_tab_livelli_prod_3_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_livello_1, is_livello_2)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_1")) THEN
      SetItem(l_Idx, "cod_livello_prod_1", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_1") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_2")) THEN
      SetItem(l_Idx, "cod_livello_prod_2", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_2") )
   END IF
NEXT

end event

event updateend;call super::updateend;f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_3",sqlca,&
	  "tab_livelli_prod_3","cod_livello_prod_3","des_livello", &
	  "(tab_livelli_prod_3.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_3.cod_livello_prod_1 = '" + is_livello_1 + "') and  " + &
	  "(tab_livelli_prod_3.cod_livello_prod_2 = '" + is_livello_2 + "')"  )

end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_liv_prod_3
   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_livelli_prod_3_lingue"
      ls_codice  = "cod_livello_prod_3"
      ls_cod_liv_prod_3 = this.getitemstring(li_i, "cod_livello_prod_3", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_liv_prod_3)
   next
end if
end event

type dw_tab_livelli_prod_4_lista from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 540
integer width = 2537
integer height = 500
integer taborder = 240
string dataobject = "d_tab_livelli_prod_4_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_livello_1, is_livello_2, is_livello_3)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_1")) THEN
      SetItem(l_Idx, "cod_livello_prod_1", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_1") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_2")) THEN
      SetItem(l_Idx, "cod_livello_prod_2", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_2") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_3")) THEN
      SetItem(l_Idx, "cod_livello_prod_3", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_3") )
   END IF
NEXT

end event

event updateend;call super::updateend;f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_4",sqlca,&
	  "tab_livelli_prod_4","cod_livello_prod_4","des_livello", &
	  "(tab_livelli_prod_4.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_1 = '" + is_livello_1 + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_2 = '" + is_livello_2 + "') and  " + &
	  "(tab_livelli_prod_4.cod_livello_prod_3 = '" + is_livello_3 + "')"  )

end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_liv_prod_4
   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_livelli_prod_4_lingue"
      ls_codice  = "cod_livello_prod_4"
      ls_cod_liv_prod_4 = this.getitemstring(li_i, "cod_livello_prod_4", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_liv_prod_4)
   next
end if
end event

type dw_tab_livelli_prod_5_lista from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 540
integer width = 2537
integer height = 500
integer taborder = 230
string dataobject = "d_tab_livelli_prod_5_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_1")) THEN
      SetItem(l_Idx, "cod_livello_prod_1", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_1") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_2")) THEN
      SetItem(l_Idx, "cod_livello_prod_2", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_2") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_3")) THEN
      SetItem(l_Idx, "cod_livello_prod_3", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_3") )
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_4")) THEN
      SetItem(l_Idx, "cod_livello_prod_4", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_4") )
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_livello_1, is_livello_2, is_livello_3, is_livello_4)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_liv_prod_5
   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_livelli_prod_5_lingue"
      ls_codice  = "cod_livello_prod_5"
      ls_cod_liv_prod_5 = this.getitemstring(li_i, "cod_livello_prod_5", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_liv_prod_5)
   next
end if
end event

type dw_tab_livelli_prod_2_lista from uo_cs_xx_dw within w_livelli_prod
integer x = 69
integer y = 540
integer width = 2537
integer height = 500
integer taborder = 250
string dataobject = "d_tab_livelli_prod_2_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_livello_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF

   IF IsNull(GetItemstring(l_Idx, "cod_livello_prod_1")) THEN
      SetItem(l_Idx, "cod_livello_prod_1", dw_tab_livelli_selezione.getitemstring(1,"rs_cod_livello_1") )
   END IF
NEXT

end event

event updateend;call super::updateend;f_PO_LoadDDDW_DW(dw_tab_livelli_selezione,"rs_cod_livello_2",sqlca,&
	  "tab_livelli_prod_2","cod_livello_prod_2","des_livello", &
	  "(tab_livelli_prod_2.cod_azienda = '" + s_cs_xx.cod_azienda + "') and  " + &
	  "(tab_livelli_prod_2.cod_livello_prod_1 = '" + is_livello_1 + "')" )

end event

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_liv_prod_2
   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_livelli_prod_2_lingue"
      ls_codice  = "cod_livello_prod_2"
      ls_cod_liv_prod_2 = this.getitemstring(li_i, "cod_livello_prod_2", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_liv_prod_2)
   next
end if
end event

type dw_folder from u_folder within w_livelli_prod
integer x = 23
integer y = 420
integer width = 2629
integer height = 1460
integer taborder = 160
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "Livello 1"
		dw_tab_livelli_prod_1_lista.change_dw_current()
   CASE "Livello 2"
		dw_tab_livelli_prod_2_lista.change_dw_current()
   CASE "Livello 3"
      dw_tab_livelli_prod_3_lista.change_dw_current()
   CASE "Livello 4"
		dw_tab_livelli_prod_4_lista.change_dw_current()
   CASE "Livello 5"
		dw_tab_livelli_prod_5_lista.change_dw_current()
END CHOOSE

end event

