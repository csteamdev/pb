﻿$PBExportHeader$w_livelli_prod_spese_trasp.srw
$PBExportComments$Finestra Livelli Prodotti
forward
global type w_livelli_prod_spese_trasp from w_cs_xx_principale
end type
type st_log from statictext within w_livelli_prod_spese_trasp
end type
type cb_verifica from commandbutton within w_livelli_prod_spese_trasp
end type
type st_2 from statictext within w_livelli_prod_spese_trasp
end type
type st_1 from statictext within w_livelli_prod_spese_trasp
end type
type dw_lista from uo_cs_xx_dw within w_livelli_prod_spese_trasp
end type
type r_log from rectangle within w_livelli_prod_spese_trasp
end type
end forward

global type w_livelli_prod_spese_trasp from w_cs_xx_principale
integer width = 4635
integer height = 2072
string title = "Spese Trasporto per Livelli"
st_log st_log
cb_verifica cb_verifica
st_2 st_2
st_1 st_1
dw_lista dw_lista
r_log r_log
end type
global w_livelli_prod_spese_trasp w_livelli_prod_spese_trasp

type variables


string				is_sql_base
end variables

forward prototypes
public function integer wf_verifica_dati (string as_cod_livello_prod_1, string as_cod_livello_prod_2, string as_cod_livello_prod_3, string as_cod_livello_prod_4, string as_cod_livello_prod_5, ref string as_errore)
public function string wf_descrizione_livelli (string as_cod_livello_prod_1, string as_cod_livello_prod_2, string as_cod_livello_prod_3, string as_cod_livello_prod_4, string as_cod_livello_prod_5)
end prototypes

public function integer wf_verifica_dati (string as_cod_livello_prod_1, string as_cod_livello_prod_2, string as_cod_livello_prod_3, string as_cod_livello_prod_4, string as_cod_livello_prod_5, ref string as_errore);
string			ls_cod_zona, ls_cod_cliente, ls_sql, ls_sql_base, ls_messaggio, ls_des_livello, ls_msg_livelli, ls_where_livelli

long			ll_i, ll_tot, ll_count, ll_index

datastore	lds_data


ll_tot = dw_lista.rowcount()
if ll_tot <=0 then
//	//warning
//	as_errore = "Nessuna riga da controllare per questa combinazione di livelli!"
//	return 1
	return 0
end if

ls_msg_livelli = ""
ls_where_livelli = ""
if as_cod_livello_prod_1<>"" and not isnull(as_cod_livello_prod_1) then
	ls_where_livelli += " and cod_livello_prod_1='"+as_cod_livello_prod_1+"'"
else
	ls_where_livelli += " and cod_livello_prod_1 is null"
end if

if as_cod_livello_prod_2<>"" and not isnull(as_cod_livello_prod_2) then
	ls_where_livelli += " and cod_livello_prod_2='"+as_cod_livello_prod_2+"'"
else
	ls_where_livelli += " and cod_livello_prod_2 is null"
end if

if as_cod_livello_prod_3<>"" and not isnull(as_cod_livello_prod_3) then
	ls_where_livelli += " and cod_livello_prod_3='"+as_cod_livello_prod_3+"'"
else
	ls_where_livelli += " and cod_livello_prod_3 is null"
end if

if as_cod_livello_prod_4<>"" and not isnull(as_cod_livello_prod_4) then
	ls_where_livelli += " and cod_livello_prod_4='"+as_cod_livello_prod_4+"'"
else
	ls_where_livelli += " and cod_livello_prod_4 is null"
end if

if as_cod_livello_prod_5<>"" and not isnull(as_cod_livello_prod_5) then
	ls_where_livelli += " and cod_livello_prod_5='"+as_cod_livello_prod_5+"'"
else
	ls_where_livelli += " and cod_livello_prod_5 is null"
end if

//recupero descrizioni dei livelli per costruire un eventuale messaggio di warning duplicazione dati
//es.   Livelli ( 01.CONFEZIONATO - REP20.CONFEZIONI - CAT02.LINEAROSSA )
ls_msg_livelli = "Livelli ( " + wf_descrizione_livelli(	as_cod_livello_prod_1, as_cod_livello_prod_2, as_cod_livello_prod_3, &
																as_cod_livello_prod_4, as_cod_livello_prod_5) + " )"

//query di base
ls_sql_base = "select count(*) from tab_livelli_prod_spese where cod_azienda='"+s_cs_xx.cod_azienda+"' " + ls_where_livelli

//controllo ripetizioni
//una sola combinazione livelli/zona/cliente
//una sola combinazione livelli/zona/senza cliente
for ll_i = 1 to ll_tot

	ls_sql = ls_sql_base
	ls_messaggio = "Combinazione già esistente per: " + ls_msg_livelli
	
	
	ls_cod_zona					= dw_lista.getitemstring(ll_i, "cod_zona")
	if ls_cod_zona<>"" and not isnull(ls_cod_zona) then
		ls_sql += " and cod_zona='"+ls_cod_zona+"'"
		ls_messaggio += "  Zona "+ls_cod_zona + "."+f_des_tabella("tab_zone", "cod_zona='" +  ls_cod_zona + "'", "des_zona")
	else
		ls_sql += " and cod_zona is null"
		ls_messaggio += "  Zona <vuota>"
	end if
	
	ls_cod_cliente				= dw_lista.getitemstring(ll_i, "cod_cliente")
	if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
		ls_sql += " and cod_cliente='"+ls_cod_cliente+"'"
		ls_messaggio += "  Cliente "+ls_cod_cliente + "." + f_des_tabella("anag_clienti", "cod_cliente='" +  ls_cod_cliente + "'", "rag_soc_1")
	else
		ls_sql += " and cod_cliente is null"
		ls_messaggio += "  Cliente <vuoto>"
	end if
	
	ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	
	if ll_count>=0 then
		ll_count = lds_data.getitemnumber(1, 1)
		if ll_count>1 then
			as_errore = ls_messaggio + "   [TOT: "+string(ll_count)+"]"
			
			//al primo warning, scrivi nella casella di testo e poi esci dal ciclo
			return -1
		end if
	else
		//no data, anche se almeno una riga dovrebbe esserci, con valore zero, cmq considera zero, quindi nessun warning
	end if
	
next

return 0
end function

public function string wf_descrizione_livelli (string as_cod_livello_prod_1, string as_cod_livello_prod_2, string as_cod_livello_prod_3, string as_cod_livello_prod_4, string as_cod_livello_prod_5);string			ls_des_livello


ls_des_livello = ""

//es.
//		01.CONFEZIONATO - REP10.CAPPOTTE - CAT01.CAPPOTTINE


//recupero descrizioni dei livelli
if not isnull(as_cod_livello_prod_1) and as_cod_livello_prod_1<>"" then
	//fino al livello 1
	ls_des_livello += as_cod_livello_prod_1 + "." + f_des_tabella(	"tab_livelli_prod_1", &
																					"cod_livello_prod_1='"+as_cod_livello_prod_1+"' ", &
																					"des_livello")
end if

if not isnull(as_cod_livello_prod_2) and as_cod_livello_prod_2<>"" then
	//fino al livello 2
	ls_des_livello += " - " + as_cod_livello_prod_2 + "." + f_des_tabella(	"tab_livelli_prod_2", &
																							"cod_livello_prod_1='"+as_cod_livello_prod_1+"' and "+&
																							"cod_livello_prod_2='"+as_cod_livello_prod_2+"'", &
																							"des_livello")
else
	return ls_des_livello
end if

if not isnull(as_cod_livello_prod_3) and as_cod_livello_prod_3<>"" then
	//fino al livello 3
	ls_des_livello += " - " + as_cod_livello_prod_3 + "." + f_des_tabella(	"tab_livelli_prod_3", &
																							"cod_livello_prod_1='"+as_cod_livello_prod_1+"' and "+&
																							"cod_livello_prod_2='"+as_cod_livello_prod_2+"' and "+&
																							"cod_livello_prod_3='"+as_cod_livello_prod_3+"'", &
																							"des_livello")
else
	return ls_des_livello
end if	

if not isnull(as_cod_livello_prod_4) and as_cod_livello_prod_4<>"" then
	//fino al livello 4
	ls_des_livello += " - " + as_cod_livello_prod_4 + "." + f_des_tabella(	"tab_livelli_prod_4", &
																							"cod_livello_prod_1='"+as_cod_livello_prod_1+"' and "+&
																							"cod_livello_prod_2='"+as_cod_livello_prod_2+"' and "+&
																							"cod_livello_prod_3='"+as_cod_livello_prod_3+"' and "+&
																							"cod_livello_prod_4='"+as_cod_livello_prod_4+"'", &
																							"des_livello")
else
	return ls_des_livello
end if

if 	not isnull(as_cod_livello_prod_5) and as_cod_livello_prod_5<>"" then
	//tutti e 5 i livelli
	ls_des_livello += " - " + as_cod_livello_prod_4 + "." + f_des_tabella(	"tab_livelli_prod_4", &
																							"cod_livello_prod_1='"+as_cod_livello_prod_1+"' and "+&
																							"cod_livello_prod_2='"+as_cod_livello_prod_2+"' and "+&
																							"cod_livello_prod_3='"+as_cod_livello_prod_3+"' and "+&
																							"cod_livello_prod_4='"+as_cod_livello_prod_4+"' and "+&
																							"cod_livello_prod_5='"+as_cod_livello_prod_5+"'", &
																							"des_livello")
else
	return ls_des_livello
end if


return ls_des_livello
end function

on w_livelli_prod_spese_trasp.create
int iCurrent
call super::create
this.st_log=create st_log
this.cb_verifica=create cb_verifica
this.st_2=create st_2
this.st_1=create st_1
this.dw_lista=create dw_lista
this.r_log=create r_log
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.cb_verifica
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_lista
this.Control[iCurrent+6]=this.r_log
end on

on w_livelli_prod_spese_trasp.destroy
call super::destroy
destroy(this.st_log)
destroy(this.cb_verifica)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_lista)
destroy(this.r_log)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("progressivo")
dw_lista.set_dw_options(	sqlca, &
									 i_openparm, &
                                    	c_scrollparent, &
                                    	c_default + &
                                    	c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)

is_sql_base = dw_lista.getsqlselect()
end event

event pc_setddlb;call super::pc_setddlb;
f_po_loaddddw_dw(dw_lista, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//visualizzo solo i tipi dettaglio
f_po_loaddddw_dw(dw_lista, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_ven='T'")
end event

type st_log from statictext within w_livelli_prod_spese_trasp
integer x = 462
integer y = 1724
integer width = 4091
integer height = 200
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_verifica from commandbutton within w_livelli_prod_spese_trasp
integer x = 32
integer y = 1716
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Verifica"
end type

type st_2 from statictext within w_livelli_prod_spese_trasp
integer x = 32
integer y = 160
integer width = 4526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "tasti SHIFT + F1 sui campi ~"Prodotto Spesa~" e ~"Cod.Cliente~" per selezionare il dato dall~'anagrafica"
boolean focusrectangle = false
end type

type st_1 from statictext within w_livelli_prod_spese_trasp
integer x = 32
integer y = 20
integer width = 4526
integer height = 88
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "Dati per livelli"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_lista from uo_cs_xx_dw within w_livelli_prod_spese_trasp
integer x = 32
integer y = 244
integer width = 4526
integer height = 1444
integer taborder = 10
string dataobject = "d_tab_livelli_prod_spese_trasp"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;call super::ue_key;long			ll_row


ll_row = this.getrow()



choose case getcolumnname()
	case "cod_prodotto_spesa"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_lista,"cod_prodotto_spesa")
		end if
		
	case "cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_cliente(dw_lista,"cod_cliente")
		end if

end choose
end event

event pcd_validaterow;call super::pcd_validaterow;
long				ll_i


//controllo obbligatorietà
for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_zona")) or getitemstring(ll_i, "cod_zona")="" then
		g_mb.error("Attenzione: manca la zona alla riga "+string(ll_i))
		pcca.error = c_fatal
	end if
	
	if isnull(getitemstring(ll_i, "cod_tipo_det_ven")) or getitemstring(ll_i, "cod_tipo_det_ven")="" then
		g_mb.error("Attenzione: manca il codice del tipo dettaglio spesa alla riga "+string(ll_i))
		pcca.error = c_fatal
	end if
	
	if isnull(getitemstring(ll_i, "cod_prodotto_spesa")) or getitemstring(ll_i, "cod_prodotto_spesa")="" then
		g_mb.error("Attenzione: manca il codice prodotto spesa alla riga "+string(ll_i))
		pcca.error = c_fatal
	end if
next


end event

event pcd_delete;call super::pcd_delete;
//r_log.fillcolor = 12632256
//st_log.text = ""

cb_verifica.enabled = false
end event

event pcd_modify;call super::pcd_modify;
//r_log.fillcolor = 12632256
//st_log.text = ""

cb_verifica.enabled = false
end event

event pcd_view;call super::pcd_view;
//r_log.fillcolor = 12632256
//st_log.text = ""

cb_verifica.enabled = true

end event

event updateend;call super::updateend;

cb_verifica.triggerevent(clicked!)
end event

type r_log from rectangle within w_livelli_prod_spese_trasp
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 73
integer y = 1816
integer width = 320
integer height = 100
end type

