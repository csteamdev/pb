﻿$PBExportHeader$w_livelli_prod_spese_trasp_3.srw
forward
global type w_livelli_prod_spese_trasp_3 from w_livelli_prod_spese_trasp
end type
end forward

global type w_livelli_prod_spese_trasp_3 from w_livelli_prod_spese_trasp
string title = "Spese Trasporto fino a Max 3 Livelli impostati"
end type
global w_livelli_prod_spese_trasp_3 w_livelli_prod_spese_trasp_3

on w_livelli_prod_spese_trasp_3.create
call super::create
end on

on w_livelli_prod_spese_trasp_3.destroy
call super::destroy
end on

type st_log from w_livelli_prod_spese_trasp`st_log within w_livelli_prod_spese_trasp_3
end type

type cb_verifica from w_livelli_prod_spese_trasp`cb_verifica within w_livelli_prod_spese_trasp_3
end type

event cb_verifica::clicked;call super::clicked;string				ls_errore, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5

integer			li_ret

long				ll_row


r_log.fillcolor = 12632256

ll_row = dw_lista.getrow()

ls_cod_livello_prod_1 = dw_lista.getitemstring(ll_row, "cod_livello_prod_1")
if isnull(ls_cod_livello_prod_1) or ls_cod_livello_prod_1="" then return

ls_cod_livello_prod_2 = dw_lista.getitemstring(ll_row, "cod_livello_prod_2")
if isnull(ls_cod_livello_prod_2) or ls_cod_livello_prod_2="" then return

ls_cod_livello_prod_3 = dw_lista.getitemstring(ll_row, "cod_livello_prod_3")
if isnull(ls_cod_livello_prod_3) or ls_cod_livello_prod_3="" then return

setnull(ls_cod_livello_prod_4)
setnull(ls_cod_livello_prod_5)

li_ret = wf_verifica_dati(ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_errore)

if li_ret > 0 then
	//warning
	st_log.text = ls_errore
	r_log.fillcolor = 65535
elseif li_ret < 0 then
	//errore duplicazione
	st_log.text = ls_errore
	r_log.fillcolor = 255
else
	//OK
	st_log.text = "Verifica dati completata con successo!"
	r_log.fillcolor = 65280
end if

return
end event

type st_2 from w_livelli_prod_spese_trasp`st_2 within w_livelli_prod_spese_trasp_3
end type

type st_1 from w_livelli_prod_spese_trasp`st_1 within w_livelli_prod_spese_trasp_3
end type

type dw_lista from w_livelli_prod_spese_trasp`dw_lista within w_livelli_prod_spese_trasp_3
end type

event dw_lista::pcd_new;call super::pcd_new;string			ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5

long			ll_row

r_log.fillcolor = 12632256

ll_row = getrow()

ls_cod_livello_prod_1 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_1")
ls_cod_livello_prod_2 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_2")
ls_cod_livello_prod_3 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_3")

setnull(ls_cod_livello_prod_4)
setnull(ls_cod_livello_prod_5)


setitem(ll_row, "cod_livello_prod_1", ls_cod_livello_prod_1)
setitem(ll_row, "cod_livello_prod_2", ls_cod_livello_prod_2)
setitem(ll_row, "cod_livello_prod_3", ls_cod_livello_prod_3)
setitem(ll_row, "cod_livello_prod_4", ls_cod_livello_prod_4)
setitem(ll_row, "cod_livello_prod_5", ls_cod_livello_prod_5)

cb_verifica.enabled = false
end event

event dw_lista::pcd_retrieve;call super::pcd_retrieve;long			ll_errore
string			ls_titolo, ls_cod_livello_prod_1,ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_sql


ls_cod_livello_prod_1 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_1")
ls_cod_livello_prod_2 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_2")
ls_cod_livello_prod_3 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_3")

setnull(ls_cod_livello_prod_4)
setnull(ls_cod_livello_prod_5)

ls_sql = is_sql_base + " where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
										"cod_livello_prod_1='"+ls_cod_livello_prod_1+"' and "+&
										"cod_livello_prod_2='"+ls_cod_livello_prod_2+"' and "+&
										"cod_livello_prod_3='"+ls_cod_livello_prod_3+"' and "+&
										"cod_livello_prod_4 is null and "+&
										"cod_livello_prod_5 is null "

ls_sql += "order by cod_zona, cod_cliente "

ls_titolo = "Spese Trasporto Livelli: ( " + &
					wf_descrizione_livelli(ls_cod_livello_prod_1,ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5) + &
			 " )"

st_1.text = ls_titolo

setsqlselect(ls_sql)
ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event dw_lista::pcd_setkey;call super::pcd_setkey;long			ll_i, ll_progressivo

string			ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3


ls_cod_livello_prod_1 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_1")
ls_cod_livello_prod_2 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_2")
ls_cod_livello_prod_3 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_3")

setnull(ll_progressivo)

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) or getitemstring(ll_i, "cod_azienda")="" then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	
	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo") <= 0 then
		//la riga non ha il campo progressivo valorizzato
		
		if isnull(ll_progressivo) or ll_progressivo <= 0 then
			//leggo il più alto progressivo memorizzato in tabella
		
			select max(progressivo)
			into :ll_progressivo
			from tab_livelli_prod_spese
			where cod_azienda=:s_cs_xx.cod_azienda;
			
			if isnull(ll_progressivo) then ll_progressivo =0
			ll_progressivo += 1
			
		else
			//è stato già letto il progressivo più alto presente nella tabella, quindi incremento
			ll_progressivo += 1
		end if
		
		setitem(ll_i, "progressivo", ll_progressivo)
		
	else
		//il campo progressivo è a posto per questa riga
	end if

	if isnull(getitemstring(ll_i, "cod_livello_prod_1")) or getitemstring(ll_i, "cod_livello_prod_1")="" then
		setitem(ll_i, "cod_livello_prod_1", ls_cod_livello_prod_1)
	end if
	if isnull(getitemstring(ll_i, "cod_livello_prod_2")) or getitemstring(ll_i, "cod_livello_prod_2")="" then
		setitem(ll_i, "cod_livello_prod_2", ls_cod_livello_prod_2)
	end if
	if isnull(getitemstring(ll_i, "cod_livello_prod_3")) or getitemstring(ll_i, "cod_livello_prod_3")="" then
		setitem(ll_i, "cod_livello_prod_3", ls_cod_livello_prod_3)
	end if
	
	//GLI ALTRI CODICI LIVELLO SARANNO SEMPRE COMUNQUE NULL

next
end event

type r_log from w_livelli_prod_spese_trasp`r_log within w_livelli_prod_spese_trasp_3
end type

