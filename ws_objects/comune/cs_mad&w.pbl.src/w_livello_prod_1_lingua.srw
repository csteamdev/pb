﻿$PBExportHeader$w_livello_prod_1_lingua.srw
forward
global type w_livello_prod_1_lingua from w_cs_xx_principale
end type
type dw_livello_prod_1_lingua from uo_cs_xx_dw within w_livello_prod_1_lingua
end type
end forward

global type w_livello_prod_1_lingua from w_cs_xx_principale
int Width=2455
int Height=1145
boolean TitleBar=true
string Title="Livello 1"
dw_livello_prod_1_lingua dw_livello_prod_1_lingua
end type
global w_livello_prod_1_lingua w_livello_prod_1_lingua

on w_livello_prod_1_lingua.create
int iCurrent
call w_cs_xx_principale::create
this.dw_livello_prod_1_lingua=create dw_livello_prod_1_lingua
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_livello_prod_1_lingua
end on

on w_livello_prod_1_lingua.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_livello_prod_1_lingua)
end on

event pc_setwindow;call super::pc_setwindow;dw_livello_prod_1_lingua.set_dw_key("cod_azienda")
dw_livello_prod_1_lingua.set_dw_key("cod_livello_prod_1")
dw_livello_prod_1_lingua.set_dw_options(sqlca, &
                              i_openparm, &
                              c_scrollparent, &
                              c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_livello_prod_1_lingua, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event open;call super::open;//string ls_label
//select label_livello_prod_1 into :ls_label from con_magazzino;
//this.title=ls_label
end event

type dw_livello_prod_1_lingua from uo_cs_xx_dw within w_livello_prod_1_lingua
int X=23
int Y=21
int Width=2378
int Height=1001
string DataObject="d_livello_prod_1_lingua"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_livello

ls_cod_livello = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_1")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_livello_prod_1")) then
      this.setitem(ll_i, "cod_livello_prod_1", ls_cod_livello)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_livello


ls_cod_livello = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_livello_prod_1")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_livello)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

