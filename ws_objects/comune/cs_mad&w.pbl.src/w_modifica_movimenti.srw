﻿$PBExportHeader$w_modifica_movimenti.srw
$PBExportComments$Finestra RESPONSE Modifica Movimenti
forward
global type w_modifica_movimenti from w_cs_xx_risposta
end type
type b_ricerca_cliente from commandbutton within w_modifica_movimenti
end type
type cb_esegui from commandbutton within w_modifica_movimenti
end type
type cb_annulla from commandbutton within w_modifica_movimenti
end type
type cb_ricerca_stock from cb_stock_ricerca within w_modifica_movimenti
end type
type dw_modifica_movimenti from uo_cs_xx_dw within w_modifica_movimenti
end type
end forward

global type w_modifica_movimenti from w_cs_xx_risposta
integer width = 2437
integer height = 1424
string title = "Modifica Movimenti"
boolean controlmenu = false
b_ricerca_cliente b_ricerca_cliente
cb_esegui cb_esegui
cb_annulla cb_annulla
cb_ricerca_stock cb_ricerca_stock
dw_modifica_movimenti dw_modifica_movimenti
end type
global w_modifica_movimenti w_modifica_movimenti

type variables
integer ii_riga, ii_max_righe

end variables

event pc_setwindow;call super::pc_setwindow;dw_modifica_movimenti.set_dw_options(sqlca, &
												 i_openparm, &
												 c_nonew + &
												 c_nodelete + &
												 c_modifyonopen, &
												 c_default)
save_on_close(c_socnosave)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_modifica_movimenti,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_modifica_movimenti,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
f_PO_LoadDDDW_DW(dw_modifica_movimenti,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_modifica_movimenti,"cod_fornitore",sqlca,&
//                 "anag_fornitori","cod_fornitore","rag_soc_1",&
//                 "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//                 "(anag_fornitori.flag_terzista = 'S') and " + &
//					  "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//					  
//f_PO_LoadDDDW_DW(dw_modifica_movimenti,"cod_cliente",sqlca,&
//                 "anag_clienti","cod_cliente","rag_soc_1",&
//                 "(anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//					  "((anag_clienti.flag_blocco <> 'S') or (anag_clienti.flag_blocco = 'S' and anag_clienti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  

end event

on w_modifica_movimenti.create
int iCurrent
call super::create
this.b_ricerca_cliente=create b_ricerca_cliente
this.cb_esegui=create cb_esegui
this.cb_annulla=create cb_annulla
this.cb_ricerca_stock=create cb_ricerca_stock
this.dw_modifica_movimenti=create dw_modifica_movimenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.b_ricerca_cliente
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ricerca_stock
this.Control[iCurrent+5]=this.dw_modifica_movimenti
end on

on w_modifica_movimenti.destroy
call super::destroy
destroy(this.b_ricerca_cliente)
destroy(this.cb_esegui)
destroy(this.cb_annulla)
destroy(this.cb_ricerca_stock)
destroy(this.dw_modifica_movimenti)
end on

type b_ricerca_cliente from commandbutton within w_modifica_movimenti
integer x = 2080
integer y = 840
integer width = 96
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

type cb_esegui from commandbutton within w_modifica_movimenti
integer x = 2011
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string						ls_str1, ls_str2, ls_cod_tipo_movimento, ls_cod_prodotto, ls_referenza, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[]
						
datetime					ldt_data_registrazione, ldt_data_stock[], ldt_data_documento

long						ll_num_documento, ll_prog_mov, ll_prog_stock[], ll_i, ll_y, ll_anno_registrazione[], ll_num_registrazione[], ll_anno_reg

double					ld_val_movimento, ld_quan_movimento

datastore				lds_mov_magazzino

s_cs_xx_parametri		lstr_return


// ------------  faccio in modo che sulla DW venga scatenato itemchanged -----------------
//           altrimento il valore impostato nell'ultimo campo non viene accettato
dw_modifica_movimenti.setcolumn(04)
dw_modifica_movimenti.setcolumn(14)
// ----------------------------------------------------------------------------------------

ll_prog_mov = dw_modifica_movimenti.getitemnumber(1,"prog_mov")

ll_anno_reg = dw_modifica_movimenti.getitemnumber(1,"anno_registrazione")

lds_mov_magazzino = create datastore
lds_mov_magazzino.dataobject = "d_mov_magazzino"

lds_mov_magazzino.settransobject(sqlca)
ll_y = lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, ll_prog_mov, ll_anno_reg)


ldt_data_registrazione = dw_modifica_movimenti.getitemdatetime(1,"data_registrazione")
ls_cod_tipo_movimento = dw_modifica_movimenti.getitemstring(1,"cod_tipo_movimento")
ls_cod_prodotto = dw_modifica_movimenti.getitemstring(1,"cod_prodotto")
ld_quan_movimento = dw_modifica_movimenti.getitemnumber(1,"quan_movimento")
ld_val_movimento = dw_modifica_movimenti.getitemnumber(1,"val_movimento")
ll_num_documento = dw_modifica_movimenti.getitemnumber(1,"num_documento")
ldt_data_documento = dw_modifica_movimenti.getitemdatetime(1,"data_documento")
ls_referenza = dw_modifica_movimenti.getitemstring(1,"referenza")
ll_prog_mov = dw_modifica_movimenti.getitemnumber(1,"prog_mov")
ls_cod_deposito[1] = dw_modifica_movimenti.getitemstring(1,"cod_deposito")
ls_cod_ubicazione[1] = dw_modifica_movimenti.getitemstring(1,"cod_ubicazione")
ls_cod_lotto[1] = dw_modifica_movimenti.getitemstring(1,"cod_lotto")
ldt_data_stock[1] = dw_modifica_movimenti.getitemdatetime(1,"data_stock")
ll_prog_stock[1] = dw_modifica_movimenti.getitemnumber(1,"prog_stock")
ls_cod_fornitore[1] = dw_modifica_movimenti.getitemstring(1,"cod_fornitore")
ls_cod_cliente[1] = dw_modifica_movimenti.getitemstring(1,"cod_cliente")
ll_anno_registrazione[1] = dw_modifica_movimenti.getitemnumber(1,"anno_registrazione")
ll_num_registrazione[1] = dw_modifica_movimenti.getitemnumber(1,"num_registrazione")

if f_modifica_movimenti(ldt_data_registrazione, &
								ls_cod_tipo_movimento, &
								ls_cod_prodotto, &
								ld_quan_movimento, &
								ld_val_movimento, &
								ll_num_documento, &
								ldt_data_documento, &
								ls_referenza, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[]) = 0 then
	g_mb.messagebox("APICE","Modifica del movimento eseguita con successo", Information!)
	commit;
	
	lstr_return.parametro_b_1 = true
	
else
	rollback;
	
	lstr_return.parametro_b_1 = false
end if

closewithreturn(parent, lstr_return)

//parent.postevent("pc_close")




end event

type cb_annulla from commandbutton within w_modifica_movimenti
integer x = 1623
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx_parametri		lstr_return


lstr_return.parametro_b_1 = false
closewithreturn(parent, lstr_return)

//parent.triggerevent("pc_close")
end event

type cb_ricerca_stock from cb_stock_ricerca within w_modifica_movimenti
integer x = 2094
integer y = 448
integer width = 73
integer height = 80
integer taborder = 40
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
long   ll_prog_stock
double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
datetime ldt_data_stock

s_cs_xx.parametri.parametro_s_10 = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_modifica_movimenti.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)

ls_cod_prodotto = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_prodotto")
ls_cod_deposito = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_deposito")
ls_cod_ubicazione = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_ubicazione")
ls_cod_lotto = dw_modifica_movimenti.getitemstring(dw_modifica_movimenti.getrow(),"cod_lotto")
ldt_data_stock = dw_modifica_movimenti.getitemdatetime(dw_modifica_movimenti.getrow(),"data_stock")
ll_prog_stock  = dw_modifica_movimenti.getitemnumber(dw_modifica_movimenti.getrow(),"prog_stock")
if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
	not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
	not(ll_prog_stock < 1) then
	  select stock.giacenza_stock,
				stock.quan_assegnata,
				stock.quan_in_spedizione
	  into   :ld_giacenza,
				:ld_quan_assegnata,
				:ld_quan_in_spedizione
	  from   stock  
	  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
				(stock.cod_prodotto   = :ls_cod_prodotto ) and
				(stock.cod_deposito   = :ls_cod_deposito ) and
				(stock.cod_ubicazione = :ls_cod_ubicazione ) and
				(stock.cod_lotto      = :ls_cod_lotto ) and
				(stock.data_stock     = :ldt_data_stock ) and
				(stock.prog_stock     = :ll_prog_stock )   ;
	  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione    ),"###,###,##0.0###"  )
	  dw_modifica_movimenti.object.st_giacenza.text = ls_str
end if



end event

type dw_modifica_movimenti from uo_cs_xx_dw within w_modifica_movimenti
event ue_post_retrieve ( )
integer x = 23
integer y = 20
integer width = 2354
integer height = 1180
integer taborder = 70
string dataobject = "d_modifica_movimenti"
borderstyle borderstyle = stylelowered!
end type

event ue_post_retrieve();string ls_flag_cliente, ls_cod_cliente, ls_flag_fornitore, ls_cod_fornitore, ls_sql, ls_null

setnull(ls_null)

declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.flag_cliente, det_tipi_movimenti.cod_cliente, det_tipi_movimenti.flag_fornitore, det_tipi_movimenti.cod_fornitore FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + this.getitemstring(this.getrow(),"cod_tipo_movimento") + "')"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;

fetch cu_det_movimenti into :ls_flag_cliente, :ls_cod_cliente, :ls_flag_fornitore, :ls_cod_fornitore;
if sqlca.sqlcode = 0 then
	if ls_flag_cliente = "S" then
		dw_modifica_movimenti.object.cod_cliente.border = 5
		dw_modifica_movimenti.object.cod_cliente.background.mode = "0"
		dw_modifica_movimenti.object.cod_cliente.background.color = rgb(255,255,255)
		dw_modifica_movimenti.object.cod_cliente.tabsequence = 60
		dw_modifica_movimenti.setitem(dw_modifica_movimenti.getrow(),"cod_cliente",ls_cod_cliente)
//		cb_clienti_ric.enabled = true
	else
		dw_modifica_movimenti.setitem(dw_modifica_movimenti.getrow(),"cod_cliente",ls_null)
		dw_modifica_movimenti.Object.cod_cliente.border = 6
		dw_modifica_movimenti.Object.cod_cliente.background.mode = "1"
		dw_modifica_movimenti.Object.cod_cliente.background.color = rgb(255,255,255)
		dw_modifica_movimenti.Object.cod_cliente.tabsequence = 0
//		cb_clienti_ric.enabled = false
	end if
	if ls_flag_fornitore = "S" then
		dw_modifica_movimenti.Object.cod_fornitore.border = 5
		dw_modifica_movimenti.Object.cod_fornitore.background.mode = "0"
		dw_modifica_movimenti.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_modifica_movimenti.Object.cod_fornitore.tabsequence = 70
		dw_modifica_movimenti.setitem(dw_modifica_movimenti.getrow(),"cod_fornitore",ls_cod_fornitore)
//		cb_fornitore_ric.enabled = true
	else
		dw_modifica_movimenti.setitem(dw_modifica_movimenti.getrow(),"cod_fornitore",ls_null)
		dw_modifica_movimenti.Object.cod_fornitore.border = 6
		dw_modifica_movimenti.Object.cod_fornitore.background.mode = "1"
		dw_modifica_movimenti.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_modifica_movimenti.Object.cod_fornitore.tabsequence = 0
//		cb_fornitore_ric.enabled = false
	end if
end if
close cu_det_movimenti;



end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
long   ll_prog_stock, l_error, ll_prog_mov, ll_anno_registrazione, ll_num_registrazione
double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
datetime ldt_data_stock


ll_prog_mov = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"prog_mov")
ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1],"anno_registrazione")

select min(num_registrazione)
into   :ll_num_registrazione
from   mov_magazzino
where  mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda and
  		 mov_magazzino.anno_registrazione = :ll_anno_registrazione and
		 mov_magazzino.prog_mov = :ll_prog_mov;

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ll_anno_registrazione, &
						 ll_num_registrazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF


ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
	not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
	not(ll_prog_stock < 1) then
	  select stock.giacenza_stock,
				stock.quan_assegnata,
				stock.quan_in_spedizione
	  into   :ld_giacenza,
				:ld_quan_assegnata,
				:ld_quan_in_spedizione
	  from   stock  
	  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
				(stock.cod_prodotto   = :ls_cod_prodotto ) and
				(stock.cod_deposito   = :ls_cod_deposito ) and
				(stock.cod_ubicazione = :ls_cod_ubicazione ) and
				(stock.cod_lotto      = :ls_cod_lotto ) and
				(stock.data_stock     = :ldt_data_stock ) and
				(stock.prog_stock     = :ll_prog_stock )   ;
	  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione    )  )
	  dw_modifica_movimenti.object.st_giacenza.text = ls_str
end if

postevent("ue_post_retrieve")

end event

event pcd_modify;call super::pcd_modify;cb_ricerca_stock.enabled = true
//cb_ricerca_prodotti.enabled = true
end event

event pcd_view;call super::pcd_view;cb_ricerca_stock.enabled = false
//cb_ricerca_prodotti.enabled = true
end event

event pcd_new;call super::pcd_new;cb_ricerca_stock.enabled = true
//cb_ricerca_prodotti.enabled = true
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
	long   ll_prog_stock
	double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
	datetime ldt_data_stock
	
	choose case i_colname
		case "cod_prodotto"
			ls_cod_prodotto = i_coltext
			ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
				not(ll_prog_stock < 1) then
				  select stock.giacenza_stock,
							stock.quan_assegnata,
							stock.quan_in_spedizione
				  into   :ld_giacenza,
							:ld_quan_assegnata,
							:ld_quan_in_spedizione
				  from   stock  
				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
							(stock.cod_prodotto   = :ls_cod_prodotto ) and
							(stock.cod_deposito   = :ls_cod_deposito ) and
							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
							(stock.cod_lotto      = :ls_cod_lotto ) and
							(stock.data_stock     = :ldt_data_stock ) and
							(stock.prog_stock     = :ll_prog_stock )   ;
				  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione ),"###,###,##0.0###")
				  dw_modifica_movimenti.object.st_giacenza.text = ls_str
			end if

		case "cod_deposito"
			ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
			ls_cod_deposito = i_coltext
			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
				not(ll_prog_stock < 1) then
				  select stock.giacenza_stock,
							stock.quan_assegnata,
							stock.quan_in_spedizione
				  into   :ld_giacenza,
							:ld_quan_assegnata,
							:ld_quan_in_spedizione
				  from   stock  
				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
							(stock.cod_prodotto   = :ls_cod_prodotto ) and
							(stock.cod_deposito   = :ls_cod_deposito ) and
							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
							(stock.cod_lotto      = :ls_cod_lotto ) and
							(stock.data_stock     = :ldt_data_stock ) and
							(stock.prog_stock     = :ll_prog_stock )   ;
		  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione    ),"###,###,##0.0###"  )
				  dw_modifica_movimenti.object.st_giacenza.text = ls_str
			end if
	end choose
end if

end event

event editchanged;call super::editchanged;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
long   ll_prog_stock
double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
datetime ldt_data_stock

if ((dw_modifica_movimenti.getcolumnname() = "cod_prodotto") or &
   (dw_modifica_movimenti.getcolumnname() = "cod_deposito") or &
   (dw_modifica_movimenti.getcolumnname() = "cod_ubicazione") or &
   (dw_modifica_movimenti.getcolumnname() = "cod_lotto") or &
   (dw_modifica_movimenti.getcolumnname() = "data_stock") or &
   (dw_modifica_movimenti.getcolumnname() = "prog_stock")) and &
	(i_extendmode) then
	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
	ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
	ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
	ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
	ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
	ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")

	choose case dw_modifica_movimenti.getcolumnname()
		case "cod_prodotto"
			ls_cod_prodotto = data
		case "cod_deposito"
			ls_cod_deposito = data
		case "cod_ubicazione"
			ls_cod_ubicazione = data
		case "cod_lotto"
			ls_cod_lotto = data
		case "data_stock"
			ldt_data_stock = datetime(data)
		case "prog_stock"
			ll_prog_stock  = long(data)
	end choose

	if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
		not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
		not(ll_prog_stock < 1) then
		  select stock.giacenza_stock,
		         stock.quan_assegnata,
					stock.quan_in_spedizione
		  into   :ld_giacenza,
		  		   :ld_quan_assegnata,
					:ld_quan_in_spedizione
		  from   stock  
		  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
					(stock.cod_prodotto   = :ls_cod_prodotto ) and
					(stock.cod_deposito   = :ls_cod_deposito ) and
					(stock.cod_ubicazione = :ls_cod_ubicazione ) and
					(stock.cod_lotto      = :ls_cod_lotto ) and
					(stock.data_stock     = :ldt_data_stock ) and
					(stock.prog_stock     = :ll_prog_stock )   ;
		  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione    ),"###,###,##0.0###"  )
		  dw_modifica_movimenti.object.st_giacenza.text = ls_str
	end if
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_modifica_movimenti,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_modifica_movimenti,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_modifica_movimenti,"cod_fornitore")
end choose
end event

