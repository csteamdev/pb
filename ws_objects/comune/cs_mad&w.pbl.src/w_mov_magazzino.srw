﻿$PBExportHeader$w_mov_magazzino.srw
$PBExportComments$Finestra Movimenti di Magazzino
forward
global type w_mov_magazzino from w_cs_xx_principale
end type
type dw_mov_magazzino_det1 from datawindow within w_mov_magazzino
end type
type cbx_doc_rif from checkbox within w_mov_magazzino
end type
type dw_ricerca from uo_cs_xx_dw within w_mov_magazzino
end type
type dw_mov_magazzino_lista from uo_cs_xx_dw within w_mov_magazzino
end type
type gb_1 from groupbox within w_mov_magazzino
end type
type dw_folder_search from u_folder within w_mov_magazzino
end type
type cbx_prod_rif from checkbox within w_mov_magazzino
end type
type dw_mov_magazzino_det from uo_cs_xx_dw within w_mov_magazzino
end type
type st_doc_rif from statictext within w_mov_magazzino
end type
end forward

global type w_mov_magazzino from w_cs_xx_principale
integer width = 4361
integer height = 2644
string title = "Movimenti di Magazzino"
dw_mov_magazzino_det1 dw_mov_magazzino_det1
cbx_doc_rif cbx_doc_rif
dw_ricerca dw_ricerca
dw_mov_magazzino_lista dw_mov_magazzino_lista
gb_1 gb_1
dw_folder_search dw_folder_search
cbx_prod_rif cbx_prod_rif
dw_mov_magazzino_det dw_mov_magazzino_det
st_doc_rif st_doc_rif
end type
global w_mov_magazzino w_mov_magazzino

type variables
string is_sqlselect=""

// stefano 18/06/2010: progetto 140 funzione 4
private:
	uo_ref_mov_mag iuo_ref_mov_mag
	
	string is_enginetype
end variables

forward prototypes
public function integer wf_doc_rif (long al_row)
public function integer wf_carica_prodotto (string as_cod_prodotto)
public function integer wf_prod_rif (long al_row)
public function integer wf_codice_alternativo (string as_cod_prodotto, ref string as_cod_prodotto_alternativo)
public function integer wf_movimenti_carico (ref string as_cod_mov_carico)
public function integer wf_carica_val_un (string as_cod_prodotto, ref double ad_valore_unitario, ref datetime adt_data_registrazione)
public subroutine wf_ricerca ()
end prototypes

public function integer wf_doc_rif (long al_row);string	 ls_full_doc_rif, ls_small_doc_rif, ls_errore
long	 ll_anno, ll_num

// stefanop 18/06/2010: commento perchè creo una variabile di istanza, molto più performante
ll_anno = dw_mov_magazzino_lista.getitemnumber(al_row,"anno_registrazione")
ll_num = dw_mov_magazzino_lista.getitemnumber(al_row,"num_registrazione")

if isnull(ll_anno) or isnull(ll_num) or ll_anno = 0 or ll_num = 0 then
	return -1
end if

if iuo_ref_mov_mag.uof_ref_mov_mag(ll_anno, ll_num, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore) <> 0 then
	st_doc_rif.text = "" // stefanop 18/06/2010
	g_mb.messagebox("APICE","Errore in lettura documenti collegati!~r~n" + ls_errore ,stopsign!)
else
	// stefanop 18/06/2010 
	if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
		if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
			
			//inizio modifica --------------------------
			iuo_ref_mov_mag.uof_ref_mov_mag_prog_mov(ll_anno, ll_num, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore)
			
			if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
				if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
					st_doc_rif.text = "MANCA"
				else
					st_doc_rif.text = ls_full_doc_rif
				end if
			else
				st_doc_rif.text = ls_small_doc_rif
			end if
			//fine modifica --------------------------
			
			//commentato per questa modifica ---
			//st_doc_rif.text = "MANCA"
			
		else
			st_doc_rif.text = ls_full_doc_rif
		end if
	else
		st_doc_rif.text = ls_small_doc_rif
	end if
	// ---
end if

return 0
end function

public function integer wf_carica_prodotto (string as_cod_prodotto);string ls_des_prodotto, ls_cod_misura_mag

select des_prodotto, cod_misura_mag
into :ls_des_prodotto, :ls_cod_misura_mag
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto;
	
if sqlca.sqlcode <> 0 then
	return -1
else
	dw_mov_magazzino_det1.setitem(1, "cod_grezzo", as_cod_prodotto)
	dw_mov_magazzino_det1.setitem(1, "des_grezzo", ls_des_prodotto)
	dw_mov_magazzino_det1.setitem(1, "cod_um_mag", ls_cod_misura_mag)
end if
end function

public function integer wf_prod_rif (long al_row);/**
 * stefanop
 * 21/06/2010
 * progetto 140 funzione 4
 *
 * Carico dettagli grezzo
 **/
 
 string 	ls_cod_prodotto_bf, ls_cod_prodotto_mov, ls_errore, ls_flag_prog_quan_entrata, ls_flag_prog_quan_acquistata, &
 			ls_cod_tipo_movimento, ls_cod_prodotto_alt, ls_cod_misura_mag_mov, ls_cod_misura_mag, ls_des_prodotto, &
			ls_cod_prodotto, ls_cod_mov_carico, ls_cod_misura_mag1, ls_cod_prodotto_raggruppato
 long		ll_anno, ll_num
 double	ld_val_movimento, ld_fat_conversione_rag_mag, ld_val_movimento_raggr
 datetime ldt_data_registrazione, ldt_data_registrazione_raggr

dw_mov_magazzino_det1.reset()
dw_mov_magazzino_det1.insertrow(0)
ll_anno = dw_mov_magazzino_lista.getitemnumber(al_row,"anno_registrazione")
ll_num = dw_mov_magazzino_lista.getitemnumber(al_row,"num_registrazione")
ls_cod_prodotto_mov = dw_mov_magazzino_lista.getitemstring(al_row,"cod_prodotto")
ls_cod_tipo_movimento = dw_mov_magazzino_lista.getitemstring(al_row,"cod_tipo_movimento")

if isnull(ll_anno) or isnull(ll_num) or ll_anno < 1900 or ll_num < 1 then return -1

if iuo_ref_mov_mag.uof_prodotto_ref_mov_mag(ll_anno, ll_num,ref ls_cod_prodotto_bf,ref ls_errore) < 0 then
	g_mb.error("APICE", ls_errore)
	return -1
end if

if isnull(ls_cod_prodotto_bf) or ls_cod_prodotto_bf = "" then 
	// non è stata associata nessuna bolla o fattua al movimento, probabilmente è manuale
	// controllo che sia un movimento di carico
	select flag_prog_quan_entrata, flag_prog_quan_acquistata
	into :ls_flag_prog_quan_entrata, :ls_flag_prog_quan_acquistata
	from tab_tipi_movimenti_det
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_mov_det = :ls_cod_tipo_movimento;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("APICE", "Errore durante il controllo del dettaglio movimento con codice: " + ls_cod_tipo_movimento)
		return -1
	// stefanop 19/07/2010: ticket 2010/184
	elseif sqlca.sqlcode = 100 then
		return 0
	end if
	
	if (isnull(ls_flag_prog_quan_entrata) or ls_flag_prog_quan_entrata <> "+") or (isnull(ls_flag_prog_quan_acquistata) or ls_flag_prog_quan_acquistata <> "+") then
		// non è un movimento di carico, quindi esco
		return -1
	end if
	
	// carico codice alternativo
	ls_cod_prodotto = ls_cod_prodotto_mov
	
elseif ls_cod_prodotto_bf = ls_cod_prodotto_mov then
	ls_cod_prodotto = ls_cod_prodotto_mov
	
elseif  ls_cod_prodotto_bf <> ls_cod_prodotto_mov then // assegno il codice della bolla o fattura
	ls_cod_prodotto = ls_cod_prodotto_bf
else
	return -1
end if

// controllo codice alternativo
if wf_codice_alternativo(ls_cod_prodotto, ref ls_cod_prodotto_alt) < 0 then return -1
if isnull(ls_cod_prodotto_alt) or ls_cod_prodotto_alt = "" then 
	return -1
end if

// DESCRIZIONE (campo b della specifica)
select des_prodotto, cod_misura_mag
into :ls_des_prodotto, :ls_cod_misura_mag
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :ls_cod_prodotto_alt;
	
if sqlca.sqlcode <> 0 then
	return -1
else
	dw_mov_magazzino_det1.setitem(1, "cod_grezzo", ls_cod_prodotto_alt)
	dw_mov_magazzino_det1.setitem(1, "des_grezzo", ls_des_prodotto)
	dw_mov_magazzino_det1.setitem(1, "cod_um_mag", ls_cod_misura_mag)
end if

// carico i movimenti di magazzino che fanno carico
//if wf_movimenti_carico(ref ls_cod_mov_carico) < 0 then return -1
// carico la data di registrazione più grande
if wf_carica_val_un(ls_cod_prodotto_alt, ref ld_val_movimento, ref ldt_data_registrazione) < 0 then
	return -1
else
	dw_mov_magazzino_det1.setitem(1, "val_movimento", ld_val_movimento)
	dw_mov_magazzino_det1.setitem(1, "data_registrazione", ldt_data_registrazione)
end if
// ----

// dettaglio c della specifica	
select cod_misura_mag
into :ls_cod_misura_mag1
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :ls_cod_prodotto_mov;
	
if sqlca.sqlcode <> 0 or isnull(ls_cod_misura_mag1) then  return -1
if ls_cod_misura_mag1 <> ls_cod_misura_mag then
	dw_mov_magazzino_det1.setitem(1, "cod_um_mag3", ls_cod_misura_mag1)
	
	// controllo codice raggruppo
	select cod_prodotto_raggruppato, fat_conversione_rag_mag
	into :ls_cod_prodotto_raggruppato, :ld_fat_conversione_rag_mag
	from anag_prodotti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto;
		
	if sqlca.sqlcode <> 0 or isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato = "" then return -1
	
//	// carico fattore di conversione del prodotto raggruppato
//	select fat_conversione_acq
//	into :ld_fat_conversione_acq
//	from anag_prodotti
//	where
//		cod_azienda = :s_cs_xx.cod_azienda and
//		cod_prodotto = :ls_cod_prodotto_raggruppato;
//		
//	if sqlca.sqlcode <> 0 or isnull(ld_fat_conversione_acq) or ld_fat_conversione_acq = 0 then return -1

	/*if wf_carica_val_un(ls_cod_prodotto, ref ld_val_movimento_raggr, ref ldt_data_registrazione_raggr) < 0 then
		return -1
	else
		ld_val_movimento =  ld_val_movimento / ld_fat_conversione_acq
		dw_mov_magazzino_det1.setitem(1, "val_movimento2", ld_val_movimento)
	end if*/
	ld_val_movimento =  ld_val_movimento / ld_fat_conversione_rag_mag
	dw_mov_magazzino_det1.setitem(1, "val_movimento2", ld_val_movimento)
		
	dw_mov_magazzino_det1.setitem(1, "cod_grezzo2", ls_cod_prodotto)
	dw_mov_magazzino_det1.setitem(1, "cod_um_mag2", ls_cod_misura_mag1)
end if

return 1
end function

public function integer wf_codice_alternativo (string as_cod_prodotto, ref string as_cod_prodotto_alternativo);/**
 * Stefanop 
 * 18/06/2010
 *
 * Controllo se il prodotto ha un codice alternativo impostato e lo ritorno
 **/
 

select cod_prodotto_alt
into :as_cod_prodotto_alternativo
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto;
	
if sqlca.sqlcode <> 0 then return -1
return 0
end function

public function integer wf_movimenti_carico (ref string as_cod_mov_carico);/**
 * stefano
 * 21/06/2010
 *
 * Carico i codici dei movimenti che fanno carico
 **/
 
string ls_sql, ls_in
long 	ll_i
datastore lds_store

ls_in = ""
ls_sql = "select cod_tipo_mov_det from tab_tipi_movimenti_det where flag_prog_quan_entrata = '+' or	flag_prog_quan_acquistata = '+'"
if f_crea_datastore(lds_store, ls_sql) then
	for ll_i = 1 to lds_store.retrieve()
		ls_in += ", " + lds_store.getitemstring(ll_i, 1)
	next
	
	if len(ls_in) > 0 then ls_in = mid(ls_in, 2)
	
	as_cod_mov_carico = ls_in
	return 0
else
	return -1
end if
end function

public function integer wf_carica_val_un (string as_cod_prodotto, ref double ad_valore_unitario, ref datetime adt_data_registrazione);setnull(ad_valore_unitario)
setnull(adt_data_registrazione)

select max(data_registrazione)
into :adt_data_registrazione
from mov_magazzino
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto and
	cod_tipo_movimento in (
		select cod_tipo_mov_det
		from tab_tipi_movimenti_det
		where 
			//flag_prog_quan_entrata = '+' or
			flag_prog_quan_acquistata = '+'
	);

if sqlca.sqlcode <> 0 then
	g_mb.error("APICE", "Errore durante il recupero della data dell'ultimo movimento di carico per il prodotto: " + as_cod_prodotto)
	return -1
elseif isnull(adt_data_registrazione) then
	return -1
end if

select val_movimento
into :ad_valore_unitario
from mov_magazzino
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto and
	data_registrazione = :adt_data_registrazione and
	cod_tipo_movimento in (
		select cod_tipo_mov_det
		from tab_tipi_movimenti_det
		where 
			//flag_prog_quan_entrata = '+' or
			flag_prog_quan_acquistata = '+'
	);
end function

public subroutine wf_ricerca ();dw_folder_search.fu_SelectTab(1)
dw_mov_magazzino_lista.change_dw_current()

triggerevent("pc_retrieve")
end subroutine

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ricerca,"rs_cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_mov_magazzino_det,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "(tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "')")	  
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[],l_objects1[]

set_w_options(c_closenosave )

// stefanop 18/06/2010: progetto 140 funzione 4
iuo_ref_mov_mag = create uo_ref_mov_mag
// ----

dw_ricerca.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_noretrieveonopen + &
								 c_newonopen + &
								 c_nodelete + &
								 c_nomodify + &
								 c_disableCC, &
								 c_default)
								 
dw_mov_magazzino_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
										 
dw_mov_magazzino_det.set_dw_options(sqlca, &
                               dw_mov_magazzino_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)

iuo_dw_main=dw_mov_magazzino_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)

l_objects[1] = dw_mov_magazzino_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_mov_magazzino_lista.change_dw_current()
dw_ricerca.resetupdate()

RegistryGet(	"HKEY_LOCAL_MACHINE\Software\Consulting&Software\database_" + s_cs_xx.profilocorrente ,"enginetype", is_enginetype)
end event

on w_mov_magazzino.create
int iCurrent
call super::create
this.dw_mov_magazzino_det1=create dw_mov_magazzino_det1
this.cbx_doc_rif=create cbx_doc_rif
this.dw_ricerca=create dw_ricerca
this.dw_mov_magazzino_lista=create dw_mov_magazzino_lista
this.gb_1=create gb_1
this.dw_folder_search=create dw_folder_search
this.cbx_prod_rif=create cbx_prod_rif
this.dw_mov_magazzino_det=create dw_mov_magazzino_det
this.st_doc_rif=create st_doc_rif
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mov_magazzino_det1
this.Control[iCurrent+2]=this.cbx_doc_rif
this.Control[iCurrent+3]=this.dw_ricerca
this.Control[iCurrent+4]=this.dw_mov_magazzino_lista
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.dw_folder_search
this.Control[iCurrent+7]=this.cbx_prod_rif
this.Control[iCurrent+8]=this.dw_mov_magazzino_det
this.Control[iCurrent+9]=this.st_doc_rif
end on

on w_mov_magazzino.destroy
call super::destroy
destroy(this.dw_mov_magazzino_det1)
destroy(this.cbx_doc_rif)
destroy(this.dw_ricerca)
destroy(this.dw_mov_magazzino_lista)
destroy(this.gb_1)
destroy(this.dw_folder_search)
destroy(this.cbx_prod_rif)
destroy(this.dw_mov_magazzino_det)
destroy(this.st_doc_rif)
end on

event resize;
// IN BASSO
dw_mov_magazzino_det1.move(20, newheight - dw_mov_magazzino_det1.height - 20)

// SINISTRA, BASSO
dw_mov_magazzino_det.move(20, dw_mov_magazzino_det1.y - dw_mov_magazzino_det.height - 20)
cbx_prod_rif.move(20, dw_mov_magazzino_det.y + dw_mov_magazzino_det.height - cbx_prod_rif.height)
cbx_doc_rif.move(dw_mov_magazzino_det.x + dw_mov_magazzino_det.width + 40, dw_mov_magazzino_det.y)
gb_1.move(cbx_doc_rif.x , cbx_doc_rif.y + cbx_doc_rif.height + 20)
st_doc_rif.move(gb_1.x + 40, gb_1.y + 80)
st_doc_rif.BringToTop = TRUE

// SINISTRA, ALTO
dw_folder_search.move(20,20)
dw_folder_search.resize(newwidth - 40, dw_mov_magazzino_det.y - 40)

dw_mov_magazzino_lista.move(160, 40)
dw_mov_magazzino_lista.resize(dw_folder_search.width - 160, dw_mov_magazzino_det.y - 80)
end event

type dw_mov_magazzino_det1 from datawindow within w_mov_magazzino
integer x = 46
integer y = 2192
integer width = 4247
integer height = 312
integer taborder = 70
string title = "none"
string dataobject = "d_mov_magazzino_det_1"
boolean border = false
boolean livescroll = true
end type

type cbx_doc_rif from checkbox within w_mov_magazzino
integer x = 3081
integer y = 1140
integer width = 1221
integer height = 88
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizzazione dei documenti referenziati"
boolean checked = true
end type

event clicked;if checked  then
	setpointer(hourglass!)
	wf_doc_rif(dw_mov_magazzino_lista.getrow())
	setpointer(arrow!)
else
	st_doc_rif.text = ""
end if
end event

type dw_ricerca from uo_cs_xx_dw within w_mov_magazzino
integer x = 137
integer y = 40
integer width = 3040
integer height = 1020
integer taborder = 30
string dataobject = "d_ext_filtro_mov_magazzino"
boolean border = false
end type

event pcd_new;call super::pcd_new;dw_ricerca.setitem(1,"rs_flag_manuale","T")
dw_ricerca.setitem(1,"rs_flag_storico","N")
dw_ricerca.setitem(1,"rn_anno_registrazione", f_anno_esercizio())
end event

event clicked;call super::clicked;if row < 0 then return 0


choose case dwo.name
	case "b_ricerca_prodotto"
		dw_ricerca.change_dw_current()
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		
	case "b_ricerca_cliente"
		dw_ricerca.change_dw_current()
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")


	case "b_ricerca_fornitore"
		dw_ricerca.change_dw_current()
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"rs_cod_fornitore")
		
	case "cb_ricerca"
//		dw_folder_search.fu_SelectTab(1)
//		dw_mov_magazzino_lista.change_dw_current()
//		
//		parent.triggerevent("pc_retrieve")
		wf_ricerca()

	case "cb_reset"
		long ll_anno, ll_null
		string ls_anno
		string ls_null
		datetime ldd_null
		
		setnull(ls_null)
		setnull(ldd_null)
		setnull(ll_null)
		
		dw_ricerca.setItem(1, "rs_cod_tipo_movimento", ls_null)
		dw_ricerca.setItem(1, "rs_cod_prodotto", ls_null)
		dw_ricerca.setItem(1, "rs_cod_deposito", ls_null)
		dw_ricerca.setItem(1, "rs_cod_ubicazione", ls_null)
		dw_ricerca.setItem(1, "rs_cod_lotto", ls_null)
		dw_ricerca.setItem(1, "rs_cod_cliente", ls_null)
		dw_ricerca.setItem(1, "rs_cod_fornitore", ls_null)
		dw_ricerca.setItem(1, "rs_flag_storico", "N")
		dw_ricerca.setItem(1, "rs_flag_manuale", "T")
		dw_ricerca.setItem(1, "rdd_data_documento_da", ldd_null)
		dw_ricerca.setItem(1, "rdd_data_documento_a", ldd_null)
		dw_ricerca.setItem(1, "rn_num_registrazione", ll_null)
		dw_ricerca.setItem(1, "rn_anno_registrazione", string(year(today())))
		dw_ricerca.setItem(1, "rn_num_registrazione", ls_null)
		dw_ricerca.setItem(1, "rs_referenza", ls_null)
		dw_ricerca.setItem(1, "rn_num_documento", ll_null)
		dw_ricerca.setItem(1, "val_movimento_da", ll_null)
		dw_ricerca.setItem(1, "val_movimento_a", 9999999)

end choose
end event

event ue_key;call super::ue_key;if key = keyenter! then
	wf_ricerca()
end if


return 0
end event

type dw_mov_magazzino_lista from uo_cs_xx_dw within w_mov_magazzino
event pcd_delete pbm_custom42
event pcd_modify pbm_custom51
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
integer x = 160
integer y = 40
integer width = 4110
integer height = 1024
integer taborder = 60
string dataobject = "d_mov_magazzino_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_delete;uo_magazzino luo_mag

if this.getitemstring(this.getrow(),"flag_manuale") = "N" then
		g_mb.messagebox("APICE","Impossibile cancellare movimenti automatici", StopSign!)
		return
end if
if this.getitemstring(this.getrow(),"flag_storico") = "S" then
		g_mb.messagebox("APICE","Impossibile cancellare movimenti storicizzati", StopSign!)
		return
end if

if g_mb.messagebox("APICE","Elimino movimento selezionato ?",Question!,YesNo!, 2) = 1 then
	luo_mag = create uo_magazzino
	if luo_mag.uof_elimina_movimenti(	this.getitemnumber(this.getrow(),"anno_registrazione"),&
													this.getitemnumber(this.getrow(),"num_registrazione"),&
													true) = 0 then
		g_mb.messagebox("APICE","Cancellazione movimento eseguita con successo", Information!)
		COMMIT;
	end if
	
	destroy luo_mag
	
end if

end event

event pcd_modify;if this.getitemstring(this.getrow(),"flag_manuale") = "N" then
		g_mb.messagebox("APICE","Impossibile modificare movimenti automatici", StopSign!)
		return
end if
if this.getitemstring(this.getrow(),"flag_storico") = "S" then
		g_mb.messagebox("APICE","Impossibile modificare movimenti storicizzati", StopSign!)
		return
end if

window_open_parm(w_modifica_movimenti, 0, dw_mov_magazzino_lista)
end event

event pcd_new;string ls_parametro_laped
window lw_window

select stringa
into   :ls_parametro_laped
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LAP';
if sqlca.sqlcode = 0 then
	if not isnull(ls_parametro_laped) and ls_parametro_laped <> "" then
	   window_type_open(lw_window, ls_parametro_laped, 0)
	end if
else
	window_open_parm(w_mov_magazzino_det, 0, dw_mov_magazzino_lista)
end if
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_cliente, ls_cod_fornitore, ls_flag_storico, &
		ls_flag_manuale, ls_referenza, ls_message_overflow

long ll_anno_registrazione, ll_num_registrazione, ll_num_documento

string  new_select, where_clause, ls_count
string ls_anno, ls_data_da, ls_data_a, ls_cod_azienda, ls_engine_registro
long ll_i, ll_tot_righe_trovate
long ll_errore
decimal{2} ld_val_movimento_da, ld_val_movimento_a
datetime ldd_data_documento_da, ldd_data_documento_a
datastore lds_store

ll_tot_righe_trovate = 0

dw_ricerca.accepttext()

ls_cod_tipo_movimento = dw_ricerca.GetItemString(1, "rs_cod_tipo_movimento")
ls_cod_prodotto = dw_ricerca.GetItemString(1, "rs_cod_prodotto")
ls_cod_deposito = dw_ricerca.GetItemString(1, "rs_cod_deposito")
ls_cod_ubicazione = dw_ricerca.GetItemString(1, "rs_cod_ubicazione")
ls_cod_lotto = dw_ricerca.GetItemString(1, "rs_cod_lotto")
ls_cod_cliente = dw_ricerca.GetItemString(1, "rs_cod_cliente")
ls_cod_fornitore = dw_ricerca.GetItemString(1, "rs_cod_fornitore")
ls_flag_storico = dw_ricerca.GetItemString(1, "rs_flag_storico")
ls_flag_manuale = dw_ricerca.GetItemString(1, "rs_flag_manuale")
ldd_data_documento_da = dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da")
ldd_data_documento_a = dw_ricerca.GetItemdatetime(1, "rdd_data_documento_a")
ll_anno_registrazione = dw_ricerca.GetItemnumber(1, "rn_anno_registrazione")
ll_num_registrazione = dw_ricerca.GetItemnumber(1, "rn_num_registrazione")
ll_num_documento = dw_ricerca.GetItemnumber(1, "rn_num_documento")
ls_referenza = dw_ricerca.GetItemString(1, "rs_referenza")
ld_val_movimento_da = dw_ricerca.getitemdecimal(1, "val_movimento_da")
ld_val_movimento_a = dw_ricerca.getitemdecimal(1, "val_movimento_a")

//ls_count = "select cod_azienda from mov_magazzino "
ls_count = "select count(*) from mov_magazzino "

where_clause = " where mov_magazzino.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_tipo_movimento) then
	where_clause += " and mov_magazzino.cod_tipo_movimento = '" + ls_cod_tipo_movimento + "'"
end if

if not isnull(ls_cod_prodotto) then
	where_clause += " and mov_magazzino.cod_prodotto = '" + ls_cod_prodotto + "'"
end if	

if not isnull(ls_cod_deposito) then
	where_clause = where_clause + " and mov_magazzino.cod_deposito = '" + ls_cod_deposito + "'"
end if	

if not isnull(ls_cod_ubicazione) then
	where_clause += " and mov_magazzino.cod_ubicazione = '" + ls_cod_ubicazione + "'"
end if	

if not isnull(ls_cod_lotto) then
	where_clause += " and mov_magazzino.cod_lotto = '" + ls_cod_lotto + "'"
end if	

if not isnull(ls_cod_cliente) then
	where_clause += " and mov_magazzino.cod_cliente = '" + ls_cod_cliente + "'"
end if	

if not isnull(ls_cod_fornitore) then
	where_clause += " and mov_magazzino.cod_fornitore = '" + ls_cod_fornitore + "'"
end if	

if not isnull(ls_flag_storico) then
	where_clause += " and mov_magazzino.flag_storico = '" + ls_flag_storico + "'"
else
	ls_flag_storico = "N"
	dw_ricerca.setItem(1, "rs_flag_storico", "N")	
	where_clause += " and mov_magazzino.flag_storico = '" + ls_flag_storico + "'"	
end if	

if not isnull(ls_flag_manuale) then
	if ls_flag_manuale <> "T" then
		where_clause += " and mov_magazzino.flag_manuale = '" + ls_flag_manuale + "'"
	end if
else
	dw_ricerca.setItem(1, "rs_flag_manuale", "T")
end if	

if not isnull(ldd_data_documento_da) then
	where_clause +=  " and mov_magazzino.data_registrazione >= '" + string(ldd_data_documento_da, s_cs_xx.db_funzioni.formato_data) + "'"
end if	
if not isnull(ldd_data_documento_a) then
	where_clause +=  " and mov_magazzino.data_registrazione <= '" + string(ldd_data_documento_a, s_cs_xx.db_funzioni.formato_data) + "'"
end if	

if not isnull(ll_anno_registrazione) then
	where_clause = where_clause + " and mov_magazzino.anno_registrazione = " + string(ll_anno_registrazione)
else
	// stefanop 26/07/2012: ma chi ha programmato????
//	ls_anno = mid(string(today(), "yyyy/mm/dd"), 1, 4)
//	ll_anno_registrazione = integer(ls_anno)
	where_clause += " and mov_magazzino.anno_registrazione = " + string(year(today()))	
end if	

if not isnull(ll_num_registrazione) then
	where_clause = where_clause + " and mov_magazzino.num_registrazione = " + string(ll_num_registrazione) 
end if	

if not isnull(ll_num_documento) and ll_num_documento > 0 then
	where_clause = where_clause + " and mov_magazzino.num_documento = " + string(ll_num_documento) 
end if	

if not isnull(ls_referenza) and len( ls_referenza ) > 0 then
	where_clause = where_clause + " and mov_magazzino.referenza = '" + string(ls_referenza)  + "' "
end if	

if not isnull(ld_val_movimento_da) then
	where_clause += " and mov_magazzino.val_movimento >= " + g_str.replace(string(ld_val_movimento_da), ",", ".")
end if
if not isnull(ld_val_movimento_a) then
	where_clause += " and mov_magazzino.val_movimento <= " + g_str.replace(string(ld_val_movimento_a), ",", ".")
end if

new_select = i_sqlselect + where_clause

// Datastore per recuperare il numero di righe
// ----------------------------
ls_count = ls_count + where_clause

ll_tot_righe_trovate = guo_functions.uof_crea_datastore(lds_store, ls_count)
if ll_tot_righe_trovate > 0 then
	ll_tot_righe_trovate = lds_store.getitemnumber(1,1)
end if

destroy lds_store
// ----------------------------


// Controllo superamento dei limiti delle righe
// ----------------------------
ls_message_overflow = g_str.format("Attenzione: $1 movimenti di magazzino da visualizzare potrebbero rallentare il sistema.~r~nContinuo a visualizzare i dati?", ll_tot_righe_trovate)

if is_enginetype = "SYBASE_ASA" then
	if ll_tot_righe_trovate > 300 then
		if not g_mb.confirm(ls_message_overflow) then return 0
	end if
elseif is_enginetype = "SYBASE_ASE" then
	if ll_tot_righe_trovate > 1000 then
		if not g_mb.confirm(ls_message_overflow) then return 0
	end if	
elseif is_enginetype = "ORACLE" then
	if ll_tot_righe_trovate > 1000 then
		if not g_mb.confirm(ls_message_overflow) then return 0
	end if	
end if
// ----------------------------


// esegio la retrieve
// ----------------------------
dw_mov_magazzino_lista.SetSQLSelect(new_select)
ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
	pcca.error = c_fatal
	return -1
end if
// ----------------------------

// stefanop 18/06/2010
event post rowfocuschanged(1)

end event

event rowfocuschanged;call super::rowfocuschanged;if currentrow > 0 and not isnull(currentrow) then
	if cbx_doc_rif.checked then wf_doc_rif(currentrow)
	if cbx_prod_rif.checked then wf_prod_rif(currentrow)
end if
end event

type gb_1 from groupbox within w_mov_magazzino
integer x = 3081
integer y = 1228
integer width = 1221
integer height = 832
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = " Documenti Referenziati "
borderstyle borderstyle = stylebox!
end type

type dw_folder_search from u_folder within w_mov_magazzino
integer x = 23
integer y = 20
integer width = 4274
integer height = 1072
integer taborder = 70
end type

event po_tabclicked;call super::po_tabclicked;CHOOSE CASE i_SelectedTabName
   CASE "L."
      SetFocus(dw_mov_magazzino_lista)
   CASE "R."
      SetFocus(dw_folder_search)
END CHOOSE

end event

type cbx_prod_rif from checkbox within w_mov_magazzino
integer x = 55
integer y = 2088
integer width = 818
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizza prodotto riferimento"
boolean checked = true
end type

event clicked;dw_mov_magazzino_det1.visible = checked

if checked  then
	setpointer(hourglass!)
	wf_prod_rif(dw_mov_magazzino_lista.getrow())
	setpointer(arrow!)
end if
end event

type dw_mov_magazzino_det from uo_cs_xx_dw within w_mov_magazzino
integer x = 27
integer y = 1132
integer width = 3017
integer height = 1040
integer taborder = 40
string dataobject = "d_mov_magazzino_det"
boolean border = false
end type

event pcd_delete;dw_mov_magazzino_lista.triggerevent("pcd_delete")
end event

event pcd_new;dw_mov_magazzino_lista.triggerevent("pcd_new")
end event

event pcd_modify;call super::pcd_modify;dw_mov_magazzino_lista.triggerevent("pcd_modify")
end event

event clicked;call super::clicked;choose case dwo.name
	case "cb_stampa"
		datetime ldt_data_reg
		dec{4}	ld_quan_mov, ld_val_mov
		long 	 	ll_row, ll_job, ll_anno_mov, ll_num_mov, ll_prog_mov
		string 	ls_str, ls_cod_prodotto, ls_des_prodotto, ls_tipo_mov, ls_tipo_det
		
		ll_row = dw_mov_magazzino_lista.getrow()
		ll_anno_mov = dw_mov_magazzino_lista.getitemnumber(ll_row,"anno_registrazione")
		ll_num_mov = dw_mov_magazzino_lista.getitemnumber(ll_row,"num_registrazione")
		ll_prog_mov = dw_mov_magazzino_lista.getitemnumber(ll_row,"prog_mov")
		ldt_data_reg = dw_mov_magazzino_lista.getitemdatetime(ll_row,"data_registrazione")
		ls_cod_prodotto = dw_mov_magazzino_lista.getitemstring(ll_row,"cod_prodotto")
		ls_des_prodotto = f_des_tabella("anag_prodotti","cod_prodotto = '" + ls_cod_prodotto + "'","des_prodotto")
		ls_tipo_mov = dw_mov_magazzino_lista.getitemstring(ll_row,"cod_tipo_movimento")
		ls_tipo_det = dw_mov_magazzino_lista.getitemstring(ll_row,"cod_tipo_mov_det")
		ld_quan_mov = dw_mov_magazzino_lista.getitemnumber(ll_row,"quan_movimento")
		ld_val_mov = dw_mov_magazzino_lista.getitemnumber(ll_row,"val_movimento")
		
		ls_str = "MOVIMENTO: " + string(ll_anno_mov) + "/" + string(ll_num_mov) + " progressivo " + string(ll_prog_mov) + &
					"~r~nDATA REGISTRAZIONE: " + string(date(ldt_data_reg),"dd/mm/yyyy") + &
					"~r~nPRODOTTO: " + ls_cod_prodotto + " " + ls_des_prodotto + &
					"~r~nTIPO MOVIMENTO: " + ls_tipo_mov + " dettaglio " + ls_tipo_det + &
					"~r~nQUANTITA': " + string(ld_quan_mov) + &
					"~r~nVALORE: " + string(ld_val_mov) + &
					"~r~n~r~n" + st_doc_rif.text
		
		if isnull(ls_str) then
			return -1
		end if
		
		ll_job = printopen()
		print(ll_job,ls_str)
		printclose(ll_job)
		
end choose
end event

type st_doc_rif from statictext within w_mov_magazzino
integer x = 3109
integer y = 1296
integer width = 1166
integer height = 736
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

