﻿$PBExportHeader$w_mov_magazzino_det.srw
$PBExportComments$Finestra Movimenti di Magazzino
forward
global type w_mov_magazzino_det from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_mov_magazzino_det
end type
type cb_annulla from commandbutton within w_mov_magazzino_det
end type
type cb_2 from cb_stock_ricerca within w_mov_magazzino_det
end type
type cb_costo_medio from commandbutton within w_mov_magazzino_det
end type
type cb_costo_ultimo from commandbutton within w_mov_magazzino_det
end type
type cb_costo_standard from commandbutton within w_mov_magazzino_det
end type
type dw_dati_mov_magazzino from uo_cs_xx_dw within w_mov_magazzino_det
end type
end forward

global type w_mov_magazzino_det from w_cs_xx_risposta
integer width = 2528
integer height = 1352
string title = "Dati Movimento Magazzino"
boolean controlmenu = false
event ue_postopen ( )
cb_ok cb_ok
cb_annulla cb_annulla
cb_2 cb_2
cb_costo_medio cb_costo_medio
cb_costo_ultimo cb_costo_ultimo
cb_costo_standard cb_costo_standard
dw_dati_mov_magazzino dw_dati_mov_magazzino
end type
global w_mov_magazzino_det w_mov_magazzino_det

type variables
boolean ib_new = false


s_cs_xx_parametri			istr_return
end variables

forward prototypes
public function integer wf_cli_for (string ws_cod_tipo_movimento)
public subroutine wf_add_to_return (integer ai_anno_mov, long al_num_mov)
end prototypes

event ue_postopen;call super::ue_postopen;dw_dati_mov_magazzino.triggerevent("pcd_new")
ib_new = true
end event

public function integer wf_cli_for (string ws_cod_tipo_movimento);string ls_flag_cliente, ls_cod_cliente, ls_flag_fornitore, ls_cod_fornitore, ls_sql, ls_null

setnull(ls_null)

declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.flag_cliente, det_tipi_movimenti.cod_cliente, det_tipi_movimenti.flag_fornitore, det_tipi_movimenti.cod_fornitore FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + ws_cod_tipo_movimento + "')"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;

fetch cu_det_movimenti into :ls_flag_cliente, :ls_cod_cliente, :ls_flag_fornitore, :ls_cod_fornitore;
if sqlca.sqlcode = 0 then
	if ls_flag_cliente = "S" then
//		cb_cliente_ric.enabled = true
		dw_dati_mov_magazzino.Object.cod_cliente.border = 5
		dw_dati_mov_magazzino.Object.cod_cliente.background.mode = "0"
		dw_dati_mov_magazzino.Object.cod_cliente.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_cliente.tabsequence = 90
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_cliente",ls_cod_cliente)
//		f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_cliente",sqlca,&
//							  "anag_clienti","cod_cliente","rag_soc_1",&
//							  "(anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//							  "((anag_clienti.flag_blocco <> 'S') or (anag_clienti.flag_blocco = 'S' and anag_clienti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	else
//		cb_cliente_ric.enabled = false
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_cliente",ls_null)
		dw_dati_mov_magazzino.Object.cod_cliente.border = 6
		dw_dati_mov_magazzino.Object.cod_cliente.background.mode = "1"
		dw_dati_mov_magazzino.Object.cod_cliente.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_cliente.tabsequence = 0
	end if
	if ls_flag_fornitore = "S" then
//		cb_fornitore_ric.enabled = true
		dw_dati_mov_magazzino.Object.cod_fornitore.border = 5
		dw_dati_mov_magazzino.Object.cod_fornitore.background.mode = "0"
		dw_dati_mov_magazzino.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_fornitore.tabsequence = 90
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_fornitore",ls_cod_fornitore)
//		f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_fornitore",sqlca,&
//							  "anag_fornitori","cod_fornitore","rag_soc_1",&
//							  "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//							  "(anag_fornitori.flag_terzista = 'S') and " + &
//							  "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	else
//		cb_fornitore_ric.enabled = false
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_fornitore",ls_null)
		dw_dati_mov_magazzino.Object.cod_fornitore.border = 6
		dw_dati_mov_magazzino.Object.cod_fornitore.background.mode = "1"
		dw_dati_mov_magazzino.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_fornitore.tabsequence = 0
	end if
end if
close cu_det_movimenti;
					  
					  

return 0
end function

public subroutine wf_add_to_return (integer ai_anno_mov, long al_num_mov);integer				li_index


li_index = upperbound(istr_return.parametro_d_1_a[])
if isnull(li_index) then li_index = 0

li_index += 1

istr_return.parametro_d_1_a[li_index] = ai_anno_mov
istr_return.parametro_d_2_a[li_index] = al_num_mov


return
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_dati_mov_magazzino.set_dw_options(sqlca, &
												 i_openparm, &
												 c_noretrieveonopen, &
												 c_default)
postevent("ue_postopen")
save_on_close(c_socnosave)

end event

on w_mov_magazzino_det.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.cb_2=create cb_2
this.cb_costo_medio=create cb_costo_medio
this.cb_costo_ultimo=create cb_costo_ultimo
this.cb_costo_standard=create cb_costo_standard
this.dw_dati_mov_magazzino=create dw_dati_mov_magazzino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_costo_medio
this.Control[iCurrent+5]=this.cb_costo_ultimo
this.Control[iCurrent+6]=this.cb_costo_standard
this.Control[iCurrent+7]=this.dw_dati_mov_magazzino
end on

on w_mov_magazzino_det.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.cb_2)
destroy(this.cb_costo_medio)
destroy(this.cb_costo_ultimo)
destroy(this.cb_costo_standard)
destroy(this.dw_dati_mov_magazzino)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type cb_ok from commandbutton within w_mov_magazzino_det
integer x = 2107
integer y = 1152
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], &
       ls_cod_cliente[], ls_referenza, ls_cod_tipo_movimento, ls_cod_prodotto
long   ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
		 ll_anno_reg_dest_stock, ll_num_reg_dest_stock
double ll_quantita, ll_valore
datetime ldt_data_stock[], ldt_data_documento

uo_magazzino luo_mag

// ------------  faccio in modo che sulla DW venga scatenato itemchanged -----------------
//           altrimento il valore impostato nell'ultimo campo non viene accettato
dw_dati_mov_magazzino.setcolumn(4)
dw_dati_mov_magazzino.setcolumn(14)
// ----------------------------------------------------------------------------------------

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

ls_cod_tipo_movimento = dw_dati_mov_magazzino.object.cod_tipo_movimento[1]
ls_cod_prodotto = dw_dati_mov_magazzino.object.cod_prodotto[1]
ll_quantita = dw_dati_mov_magazzino.object.quan_movimento[1]
ll_valore   = dw_dati_mov_magazzino.object.val_movimento[1]
ll_num_documento = dw_dati_mov_magazzino.object.num_documento[1]
ldt_data_documento = dw_dati_mov_magazzino.object.data_documento[1]
ls_referenza = dw_dati_mov_magazzino.object.referenza[1]


if isnull(ls_cod_prodotto) then
	g_mb.messagebox("APICE","Impostare un codice prodotto",StopSign!)
	return
end if

if ll_quantita = 0 then
	if g_mb.messagebox("APICE","Attenzione movimento con quantità ZERO: Proseguo?",Question!,YesNo!,2) = 2 then return
end if


if not isnull(dw_dati_mov_magazzino.object.cod_deposito[1]) then 
	ls_cod_deposito[1] = dw_dati_mov_magazzino.object.cod_deposito[1]
end if
if not isnull(dw_dati_mov_magazzino.object.cod_ubicazione[1]) then 
	ls_cod_ubicazione[1] = dw_dati_mov_magazzino.object.cod_ubicazione[1]
end if
if not isnull(dw_dati_mov_magazzino.object.cod_lotto[1]) then 
	ls_cod_lotto[1] = dw_dati_mov_magazzino.object.cod_lotto[1]
end if
if not isnull(dw_dati_mov_magazzino.object.data_stock[1]) then 
	ldt_data_stock[1] = dw_dati_mov_magazzino.object.data_stock[1]
end if
if not isnull(dw_dati_mov_magazzino.object.prog_stock[1]) then 
	ll_prog_stock[1] = dw_dati_mov_magazzino.object.prog_stock[1]
end if

if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
										ls_cod_prodotto, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_cliente[], &
										ls_cod_fornitore[], &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock ) = -1 then
	ROLLBACK;
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 ls_cod_tipo_movimento, &
								 ls_cod_prodotto) = -1 then
	ROLLBACK;
	return 0
end if

luo_mag = create uo_magazzino

if luo_mag.uof_movimenti_mag ( dw_dati_mov_magazzino.object.data_registrazione[1], &
							dw_dati_mov_magazzino.object.cod_tipo_movimento[1], &
							"S", &
							dw_dati_mov_magazzino.object.cod_prodotto[1], &
							ll_quantita, &
							ll_valore, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_registrazione[], &
							ll_num_registrazione[] ) = 0 then
	COMMIT;

	if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
								      ll_num_reg_dest_stock) = -1 then
		ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
	end if
	COMMIT;
	
	//metto nella struttura il movimento creato
	wf_add_to_return(ll_anno_registrazione[1], ll_num_registrazione[1])
	
	
	g_mb.messagebox("APICE","Movimento eseguito con successo")
else
	ROLLBACK;
	g_mb.messagebox("APICE","Movimento non eseguito a causa di un errore")
end if

if g_mb.messagebox("APICE","Chiudere la maschera di generazione manuale movimenti?",question!,yesno!,2) = 1 then
	
	closewithreturn(parent, istr_return)
	//parent.postevent("pc_close")
	
else
	dw_dati_mov_magazzino.setfocus()
	dw_dati_mov_magazzino.setcolumn("cod_prodotto")
end if

destroy luo_mag
end event

type cb_annulla from commandbutton within w_mov_magazzino_det
integer x = 1719
integer y = 1152
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;

closewithreturn(parent, istr_return)

//parent.triggerevent("pc_close")
end event

type cb_2 from cb_stock_ricerca within w_mov_magazzino_det
integer x = 2034
integer y = 320
integer width = 320
integer height = 80
integer taborder = 20
string text = "STOCK"
end type

event clicked;call super::clicked;// ------------------------------------------------------------------------------------------
//                              RICERCA STOCK PRODOTTO
// Significato dei parametri
//
//
//   s_cs_xx.parametri.parametro_s_1 .... s_9   --->> nomi colonne su cui incidere i valori cercati
//   s_cs_xx.parametri.parametro_s_10.... s_15  --->> valori di filtro
//
//   s_cs_xx.parametri.parametro_s_10 = codice prodotto
//   s_cs_xx.parametri.parametro_s_11 = codice deposito
//   s_cs_xx.parametri.parametro_s_12 = codice ubicazione
//   s_cs_xx.parametri.parametro_s_13 = codice lotto
//   s_cs_xx.parametri.parametro_data_1 = data stock
//   s_cs_xx.parametri.parametro_d_1 = progressivo stock
//
// ------------------------------------------------------------------------------------------

string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
long   ll_prog_stock
double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione	
datetime ldt_data_stock


dw_dati_mov_magazzino.setcolumn(4)
dw_dati_mov_magazzino.setcolumn(14)

s_cs_xx.parametri.parametro_s_10 = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0
if isnull(s_cs_xx.parametri.parametro_s_10) then
   g_mb.messagebox("Ricerca Stock","Indicare un prodotto per la ricerca degli stock",StopSign!)
   return
end if

dw_dati_mov_magazzino.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)

ls_cod_prodotto = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_prodotto")
ls_cod_deposito = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_deposito")
ls_cod_ubicazione = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_ubicazione")
ls_cod_lotto = dw_dati_mov_magazzino.getitemstring(dw_dati_mov_magazzino.getrow(),"cod_lotto")
ldt_data_stock = dw_dati_mov_magazzino.getitemdatetime(dw_dati_mov_magazzino.getrow(),"data_stock")
ll_prog_stock  = dw_dati_mov_magazzino.getitemnumber(dw_dati_mov_magazzino.getrow(),"prog_stock")


if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
	not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
	not(ll_prog_stock < 1) then
	
	  select stock.giacenza_stock,
				stock.quan_assegnata,
				stock.quan_in_spedizione
	  into   :ld_giacenza,
				:ld_quan_assegnata,
				:ld_quan_in_spedizione
	  from   stock  
	  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
				(stock.cod_prodotto   = :ls_cod_prodotto ) and
				(stock.cod_deposito   = :ls_cod_deposito ) and
				(stock.cod_ubicazione = :ls_cod_ubicazione ) and
				(stock.cod_lotto      = :ls_cod_lotto ) and
				(stock.data_stock     = :ldt_data_stock ) and
				(stock.prog_stock     = :ll_prog_stock )   ;
	  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione),"###,###,##0.0###")
	  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
	  
end if



end event

type cb_costo_medio from commandbutton within w_mov_magazzino_det
integer x = 1120
integer y = 880
integer width = 64
integer height = 76
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "M"
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
datetime ldt_data_stock
long   ll_prog_stock
double ld_costo_medio


if not g_mb.confirm("Impostare come valore unitario del movimento il costo medio del prodotto indicato in tabella stock?") then return


ls_cod_prodotto = dw_dati_mov_magazzino.getitemstring(1, "cod_prodotto")
ls_cod_deposito = dw_dati_mov_magazzino.getitemstring(1, "cod_deposito")
ls_cod_ubicazione = dw_dati_mov_magazzino.getitemstring(1, "cod_ubicazione")
ls_cod_lotto = dw_dati_mov_magazzino.getitemstring(1, "cod_lotto")
ldt_data_stock = dw_dati_mov_magazzino.getitemdatetime(1, "data_stock")
ll_prog_stock = dw_dati_mov_magazzino.getitemnumber(1, "prog_stock")

ld_costo_medio = 0
select stock.costo_medio
into   :ld_costo_medio
from   stock
where  stock.cod_azienda    = :s_cs_xx.cod_azienda and
		 stock.cod_prodotto   = :ls_cod_prodotto and
		 stock.cod_deposito   = :ls_cod_deposito and
		 stock.cod_ubicazione = :ls_cod_ubicazione and
		 stock.cod_lotto      = :ls_cod_lotto and
		 stock.data_stock     = :ldt_data_stock and
		 stock.prog_stock     = :ll_prog_stock ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Stock prodotto non indicato o mancante", Information!)
end if

if isnull(ld_costo_medio) then ld_costo_medio = 0

dw_dati_mov_magazzino.setitem(1, "val_movimento", ld_costo_medio)
end event

type cb_costo_ultimo from commandbutton within w_mov_magazzino_det
event clicked pbm_bnclicked
integer x = 1189
integer y = 880
integer width = 69
integer height = 76
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "U"
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
long   ll_prog_stock
double ld_costo_ultimo


if not g_mb.confirm("Impostare come valore unitario del movimento il costo ultimo del prodotto indicato in anagrafica prodotti?") then return


ls_cod_prodotto = dw_dati_mov_magazzino.getitemstring(1, "cod_prodotto")

ld_costo_ultimo = 0
select anag_prodotti.costo_ultimo
into   :ld_costo_ultimo
from   anag_prodotti
where  anag_prodotti.cod_azienda  = :s_cs_xx.cod_azienda and
		 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Prodotto non indicato o mancante", Information!)
end if

if isnull(ld_costo_ultimo) then ld_costo_ultimo = 0

dw_dati_mov_magazzino.setitem(1, "val_movimento", ld_costo_ultimo)

end event

type cb_costo_standard from commandbutton within w_mov_magazzino_det
event clicked pbm_bnclicked
integer x = 1262
integer y = 880
integer width = 69
integer height = 76
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S"
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
long   ll_prog_stock
double ld_costo_standard


if not g_mb.confirm("Impostare come valore unitario del movimento il costo standard del prodotto indicato in anagrafica prodotti?") then return


ls_cod_prodotto = dw_dati_mov_magazzino.getitemstring(1, "cod_prodotto")

ld_costo_standard = 0
select anag_prodotti.costo_standard
into   :ld_costo_standard
from   anag_prodotti
where  anag_prodotti.cod_azienda  = :s_cs_xx.cod_azienda and
		 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Prodotto non indicato o mancante", Information!)
end if

if isnull(ld_costo_standard) then ld_costo_standard = 0

dw_dati_mov_magazzino.setitem(1, "val_movimento", ld_costo_standard)

end event

type dw_dati_mov_magazzino from uo_cs_xx_dw within w_mov_magazzino_det
integer y = 20
integer width = 2491
integer height = 1132
integer taborder = 50
string dataobject = "d_mov_magazzino_dati"
boolean border = false
end type

event pcd_modify;call super::pcd_modify;string ls_cod_tipo_movimento

ls_cod_tipo_movimento = getitemstring(getrow(),"cod_tipo_movimento")

if not isnull(ls_cod_tipo_movimento) and (i_extendmode) then
	f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "(tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "') and" + &
					  "(tab_tipi_movimenti_det.cod_tipo_mov_det in " + & 
					       "(select det_tipi_movimenti.cod_tipo_mov_det " + &
							 "from det_tipi_movimenti " + &
							 "where det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_tipi_movimenti.cod_tipo_movimento = '" + ls_cod_tipo_movimento + "'))" )
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str, &
	       ls_cod_misura, ls_des_misura
	long   ll_prog_stock
	double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
	datetime ldt_data_stock
	
	choose case i_colname
		case "cod_tipo_movimento"
			if not isnull(i_coltext) then
				wf_cli_for(i_coltext)
			end if
		case "cod_prodotto"
			ls_cod_prodotto = i_coltext
			ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
				not(ll_prog_stock < 1) then
				  select stock.giacenza_stock,
							stock.quan_assegnata,
							stock.quan_in_spedizione
				  into   :ld_giacenza,
							:ld_quan_assegnata,
							:ld_quan_in_spedizione
				  from   stock  
				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
							(stock.cod_prodotto   = :ls_cod_prodotto ) and
							(stock.cod_deposito   = :ls_cod_deposito ) and
							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
							(stock.cod_lotto      = :ls_cod_lotto ) and
							(stock.data_stock     = :ldt_data_stock ) and
							(stock.prog_stock     = :ll_prog_stock )   ;
				  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione),"###,###,##0.0###")
				  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
			end if
			select anag_prodotti.cod_misura_mag,   
                tab_misure.des_misura
         into   :ls_cod_misura,   
                :ls_des_misura  
         from   anag_prodotti,   
                tab_misure  
         where (tab_misure.cod_azienda = anag_prodotti.cod_azienda ) and  
               (tab_misure.cod_misura = anag_prodotti.cod_misura_mag ) and  
               ( (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) and
                 (anag_prodotti.cod_prodotto = :ls_cod_prodotto ) );
			if sqlca.sqlcode = 0 then
				dw_dati_mov_magazzino.object.st_quantita.text = "Quantità in " + ls_cod_misura + " - " + ls_des_misura
			end if

	case "cod_deposito"
			ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
			ls_cod_deposito = i_coltext
			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
				not(ll_prog_stock < 1) then
				  select stock.giacenza_stock  
				  into   :ld_giacenza
				  from   stock  
				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
							(stock.cod_prodotto   = :ls_cod_prodotto ) and
							(stock.cod_deposito   = :ls_cod_deposito ) and
							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
							(stock.cod_lotto      = :ls_cod_lotto ) and
							(stock.data_stock     = :ldt_data_stock ) and
							(stock.prog_stock     = :ll_prog_stock )   ;
				  ls_str = string(ld_giacenza)
				  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
			end if
	end choose
end if

end event

event editchanged;call super::editchanged;string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
long   ll_prog_stock
double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
datetime ldt_data_stock

if ((dw_dati_mov_magazzino.getcolumnname() = "cod_prodotto") or &
   (dw_dati_mov_magazzino.getcolumnname() = "cod_deposito") or &
   (dw_dati_mov_magazzino.getcolumnname() = "cod_ubicazione") or &
   (dw_dati_mov_magazzino.getcolumnname() = "cod_lotto") or &
   (dw_dati_mov_magazzino.getcolumnname() = "data_stock") or &
   (dw_dati_mov_magazzino.getcolumnname() = "prog_stock")) and &
	(i_extendmode) then
	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
	ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
	ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
	ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
	ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
	ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")

	choose case dw_dati_mov_magazzino.getcolumnname()
		case "cod_prodotto"
			ls_cod_prodotto = data
		case "cod_deposito"
			ls_cod_deposito = data
		case "cod_ubicazione"
			ls_cod_ubicazione = data
		case "cod_lotto"
			ls_cod_lotto = data
		case "data_stock"
			ldt_data_stock = datetime(data)
		case "prog_stock"
			ll_prog_stock  = long(data)
	end choose

	if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
		not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime("01/01/1900")) and &
		not(ll_prog_stock < 1) then
		  select stock.giacenza_stock,
					stock.quan_assegnata,
					stock.quan_in_spedizione
		  into   :ld_giacenza,
					:ld_quan_assegnata,
					:ld_quan_in_spedizione
		  from   stock  
		  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
					(stock.cod_prodotto   = :ls_cod_prodotto ) and
					(stock.cod_deposito   = :ls_cod_deposito ) and
					(stock.cod_ubicazione = :ls_cod_ubicazione ) and
					(stock.cod_lotto      = :ls_cod_lotto ) and
					(stock.data_stock     = :ldt_data_stock ) and
					(stock.prog_stock     = :ll_prog_stock )   ;
		  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione) ,"###,###,##0.0###")
		  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
	end if
end if

end event

event losefocus;call super::losefocus;if ib_new then
	this.triggerevent("itemchanged")
end if
end event

event pcd_new;call super::pcd_new;this.setitem(this.getrow(), "data_registrazione", datetime(today()))
end event

event buttonclicked;call super::buttonclicked;guo_ricerca.uof_set_response(parent)

choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_dati_mov_magazzino,"cod_cliente")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_dati_mov_magazzino,"cod_prodotto")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_dati_mov_magazzino,"cod_fornitore")
		
end choose
end event

event getfocus;dw_dati_mov_magazzino.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

