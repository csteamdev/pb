﻿$PBExportHeader$w_mov_magazzino_tv.srw
$PBExportComments$Finestra Movimenti di Magazzino
forward
global type w_mov_magazzino_tv from w_cs_xx_treeview
end type
type dw_2 from datawindow within det_1
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
end forward

global type w_mov_magazzino_tv from w_cs_xx_treeview
integer width = 4517
integer height = 2704
string title = "Movimenti di Magazzino"
end type
global w_mov_magazzino_tv w_mov_magazzino_tv

type variables
private:
	datastore ids_store
	long il_livello
	
	// icone
	int ICONA_MOV, ICONA_MOV_MANUALE, ICONA_MOV_STORICO
	int	ICONA_ANNO, ICONA_TIPO_MOVIMENTO, ICONA_PRODOTTO, ICONA_DEPOSITO, ICONA_DATA_REGISTRAZIONE
	
	boolean ib_chiedi_anno_esc = true
	
	long il_max_row = 255
	
	uo_ref_mov_mag iuo_ref_mov_mag
	
	/*
	SYBASE_ASA
	SYBASE_ASE
	MSSQL
	ORACLE
	*/
	
	string is_enginetype, is_top_clause
	
	integer	ii_ESC = 0
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_tipo_movimento (long al_handle)
public function long wf_inserisci_anno (long al_handle)
public function long wf_inserisci_prodotti (long al_handle)
public function long wf_inserisci_depositi (long al_handle)
public function long wf_inserisci_data_registrazione (long al_handle)
public function long wf_inserisci_movimenti (long al_handle)
public function long wf_inserisci_mov (long al_handle, integer ai_anno_registrazione, long al_num_registrazione)
public function integer wf_doc_rif (integer ai_anno_reg, long al_num_reg)
public function integer wf_prod_rif (long al_row)
public function integer wf_carica_val_un (string as_cod_prodotto, ref double ad_valore_unitario, ref datetime adt_data_registrazione)
public function integer wf_codice_alternativo (string as_cod_prodotto, ref string as_cod_prodotto_alternativo)
public function boolean wf_singolo_nodo (string as_tipologia, ref string as_valore)
public function long wf_modifica_item_mov (long al_handle)
end prototypes

public subroutine wf_imposta_ricerca ();long				ll_anno_registrazione, ll_num_registrazione
datetime			ldt_data_fine
date				ldd_date


ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_num_registrazione")

is_sql_filtro = ""

//se hai impostato sia anno che numero, caricalo nella query e disinteressati degli altri valori
if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 and not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " AND mov_magazzino.anno_registrazione=" + string(ll_anno_registrazione)
	is_sql_filtro += " AND mov_magazzino.num_registrazione=" + string(ll_num_registrazione)
	// mi fermo non serve altro, filtro solo il singolo movimento
	return
end if

//se non hai impostato l'anno nel filtro, chiedi se si vuole impostarlo con ESC (ma lo chiedi solo una volta)
if (isnull(ll_anno_registrazione) or ll_anno_registrazione<=0) and ib_chiedi_anno_esc and ii_ESC>0 then
	if g_mb.confirm("Non hai impostato l'anno registrazione nella ricerca. Vuoi impostarlo con il valore del parametro ESC?") then
		ll_anno_registrazione = ii_ESC
		tab_ricerca.ricerca.dw_ricerca.setItem(1, "rn_anno_registrazione", ii_ESC)
	end if
	
	//da adesso in poi, qualunque sia stata la tua risposta non chiederlo più
	ib_chiedi_anno_esc = false
end if

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	is_sql_filtro += " AND mov_magazzino.anno_registrazione=" + string(ll_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND mov_magazzino.num_registrazione=" + string(ll_num_registrazione)
end if



if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_prog_mov")) and tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_prog_mov")>0 then
	is_sql_filtro += " AND mov_magazzino.prog_mov=" + string(tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_prog_mov"))
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da")) and year(date(tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da")))>1950 then
	is_sql_filtro += " AND mov_magazzino.data_registrazione>='" + string(tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if

ldt_data_fine = tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_a")

if not isnull(ldt_data_fine) and year(date(ldt_data_fine))>1950 then
	//vado al giorno successivo e imposto la relazione MINORE STRETTO (<)
	ldd_date = date(ldt_data_fine)
	ldd_date = relativedate(ldd_date, 1)
	ldt_data_fine = datetime(ldd_date, 00:00:00)
	
	//is_sql_filtro += " AND mov_magazzino.data_registrazione<='" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) +"'"
	is_sql_filtro += " AND mov_magazzino.data_registrazione<'" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) +"'"
end if




if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_tipo_movimento")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_tipo_movimento")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_tipo_movimento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_tipo_movimento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_prodotto")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_prodotto")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_prodotto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_deposito")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_deposito")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_ubicazione")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_ubicazione")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_ubicazione='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_ubicazione") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_lotto")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_lotto")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_lotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_lotto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_cliente")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_cliente")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_cliente") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_fornitore")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_fornitore")<>"" then
	is_sql_filtro += " AND mov_magazzino.cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_cod_fornitore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_flag_storico")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_flag_storico")<>"" then
	is_sql_filtro += " AND mov_magazzino.flag_storico='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_flag_storico") +"'"
end if
if tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_flag_manuale")<>"T" then
	is_sql_filtro += " AND mov_magazzino.flag_manuale='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_flag_manuale") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_referenza")) and tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_referenza")<>"" then
	is_sql_filtro += " AND mov_magazzino.referenza LIKE '%" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"rs_referenza") +"%'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1, "rn_num_documento")) then
	is_sql_filtro += " AND mov_magazzino.num_documento=" + string(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1,"rn_num_documento"))
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1, "val_movimento_da")) then
	is_sql_filtro += " AND mov_magazzino.val_movimento>=" + string(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1,"val_movimento_da"))
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1, "val_movimento_a")) then
	is_sql_filtro += " AND mov_magazzino.val_movimento<=" + string(tab_ricerca.ricerca.dw_ricerca.GetItemNumber(1,"val_movimento_a"))
end if


end subroutine

public function long wf_leggi_livello (long al_handle, integer ai_livello);long				ll_ret

il_livello = ai_livello

setpointer(Hourglass!)
pcca.mdi_frame.setmicrohelp("Preparazione query in corso ...")

choose case wf_get_valore_livello(ai_livello)
	case "A"
		ll_ret = wf_inserisci_anno(al_handle)
	
	case "T"
		ll_ret = wf_inserisci_tipo_movimento(al_handle)
		
	case "P"
		ll_ret = wf_inserisci_prodotti(al_handle)
	
	case "D"
		ll_ret = wf_inserisci_depositi(al_handle)
		
	case "R"
		ll_ret = wf_inserisci_data_registrazione(al_handle)
		
	case else
		ll_ret = wf_inserisci_movimenti(al_handle)
		
end choose

setpointer(Arrow!)
pcca.mdi_frame.setmicrohelp("Pronto!")

return ll_ret
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long						ll_item
treeviewitem			ltv_item
str_treeview				lstr_data
date						ldt_date
datetime					ldt_datetime

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello	
			
		case "A"
			as_sql += " AND mov_magazzino.anno_registrazione = " + lstr_data.codice
		
		
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mov_magazzino.cod_tipo_movimento is null "
			else
				as_sql += " AND mov_magazzino.cod_tipo_movimento = '" + lstr_data.codice + "' "
			end if
		
		
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mov_magazzino.cod_prodotto is null "
			else
				as_sql += " AND mov_magazzino.cod_prodotto = '" + lstr_data.codice + "' "
			end if
		
		
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mov_magazzino.cod_deposito is null "
			else
				as_sql += " AND mov_magazzino.cod_deposito = '" + lstr_data.codice + "' "
			end if
		
		case "R"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND mov_magazzino.data_registrazione is null "
			else
				//as_sql += " AND mov_magazzino.data_registrazione = '" + lstr_data.codice + "' "
				
				//per evitare problemi dovuti a eventuali registrazuini di movimento con data avente anche l'orario (es. 20140531 15:25:25)
				//allora compongo la clausola con un range (data >= [VALORE] and data < [VALORE + 1 GG])
				ldt_datetime = lstr_data.data[1]
				as_sql += " AND mov_magazzino.data_registrazione >= '" + string(ldt_datetime, s_cs_xx.db_funzioni.formato_data) + "' "
				
				//giorno successivo
				ldt_datetime = datetime(relativedate(date(ldt_datetime), 1), 00:00:00)
				as_sql += " AND mov_magazzino.data_registrazione < '" + string(ldt_datetime, s_cs_xx.db_funzioni.formato_data) + "' "
				
			end if
		
	
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_treeview_icons ();ICONA_MOV = wf_treeview_add_icon("treeview\documento_grigio.png")
ICONA_MOV_MANUALE = wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_MOV_STORICO = wf_treeview_add_icon("treeview\documento_rosso.png")

ICONA_TIPO_MOVIMENTO = wf_treeview_add_icon("treeview\area_aziendale.png")
ICONA_ANNO = wf_treeview_add_icon("treeview\anno.png")
ICONA_DATA_REGISTRAZIONE = wf_treeview_add_icon("treeview\data_1.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")

end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Anno Registrazione", "A")
wf_add_valore_livello("Tipo Movimento", "T")
wf_add_valore_livello("Deposito", "D")
wf_add_valore_livello("Data Registrazione", "R")
wf_add_valore_livello("Prodotto", "P")
end subroutine

public function long wf_inserisci_tipo_movimento (long al_handle);string ls_sql, ls_label, ls_cod_tipo_mov, ls_des_tipo_mov, ls_error, ls_flag_ordinamento,ls_valore
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data
boolean lb_singolo = false


//TRICK
//se hai impostato il livello tipo movimento e il filtro per tipo movimento è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("T", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" mov_magazzino.cod_tipo_movimento "+&
				"FROM mov_magazzino "+&
				"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by mov_magazzino.cod_tipo_movimento"
		
		if ls_flag_ordinamento="S" then
			ls_sql += " DESC "
		else
			ls_sql += " ASC "
		end if
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "T"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_TIPO_MOVIMENTO)
		ltvi_item.data = lstr_data
		ltvi_item.label = ls_valore + " - " + f_des_tabella("tab_tipi_movimenti", "cod_tipo_movimento = '" +  ls_valore + "'", "des_tipo_movimento")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")

ls_sql = "SELECT DISTINCT mov_magazzino.cod_tipo_movimento, tab_tipi_movimenti.des_tipo_movimento "+&
			"FROM mov_magazzino "+&
			"JOIN tab_tipi_movimenti ON tab_tipi_movimenti.cod_azienda = mov_magazzino.cod_azienda AND "+&
												"tab_tipi_movimenti.cod_tipo_movimento = mov_magazzino.cod_tipo_movimento "+ &
			"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc")
else
	ids_store.setsort("#1 asc")
end if


ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	ls_cod_tipo_mov = ids_store.getitemstring(ll_i, 1)
	ls_des_tipo_mov = ids_store.getitemstring(ll_i, 2)
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo tipo movimento "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_mov
	
	if isnull(ls_cod_tipo_mov) then
		ls_label = "<Tipo Movimento mancante>"
	elseif isnull(ls_des_tipo_mov) then
		ls_label = ls_cod_tipo_mov
	else
		ls_label = ls_cod_tipo_mov + " - " + ls_des_tipo_mov
	end if
	
	ltvi_item = wf_new_item(true, ICONA_TIPO_MOVIMENTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(arrow!)

return ll_rows
end function

public function long wf_inserisci_anno (long al_handle);string ls_sql, ls_error, ls_flag_ordinamento, ls_valore
long ll_rows, ll_i
treeviewitem ltvi_item
boolean lb_singolo = false
str_treeview lstr_data

//TRICK
//se hai impostato il livello anno registrazione e il filtro per anno è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("A", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" mov_magazzino.anno_registrazione "+&
				"FROM mov_magazzino "+&
				"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by mov_magazzino.anno_registrazione"
		
		if ls_flag_ordinamento="S" then
			ls_sql += " DESC "
		else
			ls_sql += " ASC "
		end if
	end if

	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "A"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_ANNO)
		ltvi_item.data = lstr_data
		ltvi_item.label = ls_valore
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if

ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")
ls_sql = "SELECT DISTINCT mov_magazzino.anno_registrazione FROM mov_magazzino WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
wf_leggi_parent(al_handle, ls_sql)



ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc")
else
	ids_store.setsort("#1 asc")
end if

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo anno registrazione "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "A"
	lstr_data.codice = string(ids_store.getitemnumber(ll_i, 1))
	
	ltvi_item = wf_new_item(true, ICONA_ANNO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = string(ids_store.getitemnumber(ll_i, 1))
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function long wf_inserisci_prodotti (long al_handle);string ls_sql, ls_label, ls_cod_prodotto, ls_des_prodotto, ls_error, ls_flag_ordinamento, ls_valore
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data
boolean lb_singolo = false


//TRICK
//se hai impostato il livello prodotto e il filtro per prodotto è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("P", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" mov_magazzino.cod_prodotto "+&
				"FROM mov_magazzino "+&
				"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by mov_magazzino.cod_prodotto"
		
		if ls_flag_ordinamento="S" then
			ls_sql += " DESC "
		else
			ls_sql += " ASC "
		end if
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "P"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
		ltvi_item.data = lstr_data
		ltvi_item.label = ls_valore + " - " + f_des_tabella("anag_prodotti", "cod_prodotto = '" +  ls_valore + "'", "des_prodotto")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")

ls_sql = "SELECT DISTINCT mov_magazzino.cod_prodotto, anag_prodotti.des_prodotto "+&
			"FROM mov_magazzino "+&
			"JOIN anag_prodotti ON anag_prodotti.cod_azienda = mov_magazzino.cod_azienda AND "+&
										"anag_prodotti.cod_prodotto = mov_magazzino.cod_prodotto " + &
			"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc")
else
	ids_store.setsort("#1 asc")
end if

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo prodotto "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)

return ll_rows
end function

public function long wf_inserisci_depositi (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error, ls_flag_ordinamento,ls_valore
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data
boolean lb_singolo = false


//TRICK
//se hai impostato il livello deposito e il filtro per deposito è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("D", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" mov_magazzino.cod_deposito "+&
				"FROM mov_magazzino "+&
				"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by mov_magazzino.cod_deposito"
		
		if ls_flag_ordinamento="S" then
			ls_sql += " DESC "
		else
			ls_sql += " ASC "
		end if
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "D"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
		ltvi_item.data = lstr_data
		ltvi_item.label = ls_valore + " - " + f_des_tabella("anag_depositi", "cod_deposito = '" +  ls_valore + "'", "des_deposito")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")


ls_sql = "SELECT DISTINCT mov_magazzino.cod_deposito, anag_depositi.des_deposito FROM mov_magazzino "+&
			"JOIN anag_depositi ON anag_depositi.cod_azienda = mov_magazzino.cod_azienda AND "+&
										"anag_depositi.cod_deposito = mov_magazzino.cod_deposito " + &
			"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc")
else
	ids_store.setsort("#1 asc")
end if

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo deposito "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function long wf_inserisci_data_registrazione (long al_handle);string ls_sql, ls_error, ls_flag_ordinamento, ls_valore
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data
boolean lb_singolo = false
datetime ldt_datetime
date		ldt_date


//TRICK
//se hai impostato il livello data registrazione e il filtro per data registrazione (da-a) è impostato con un valore (e sono tra loro uguali)
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("R", ls_valore)

if lb_singolo and is_top_clause<>"" then
	
	//rileggo la data impostata
	ldt_datetime = tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da")
	
	ls_sql = "SELECT "+is_top_clause+" mov_magazzino.data_registrazione "+&
				"FROM mov_magazzino "+&
				"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by mov_magazzino.data_registrazione"
		
		if ls_flag_ordinamento="S" then
			ls_sql += " DESC "
		else
			ls_sql += " ASC "
		end if
	end if
	
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "R"
		lstr_data.codice = string(ldt_datetime, s_cs_xx.db_funzioni.formato_data)
		
		lstr_data.data[1] = datetime(date(ldt_datetime), 00:00:00)
		
		ltvi_item = wf_new_item(true, ICONA_DATA_REGISTRAZIONE)
		ltvi_item.data = lstr_data
		ltvi_item.label = ls_valore
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")

if is_enginetype = "ORACLE" then
	ls_sql = "SELECT DISTINCT mov_magazzino.data_registrazione "
else
	ls_sql = "SELECT DISTINCT convert(date, mov_magazzino.data_registrazione, 105) "
end if

ls_sql += "FROM mov_magazzino "+&
			"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc")
else
	ids_store.setsort("#1 asc")
end if

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo data registrazione "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "R"
	
	if is_enginetype = "ORACLE" then
		ldt_datetime = ids_store.getitemdatetime(ll_i, 1)
		ldt_date = date(ldt_datetime)
	else
		ldt_date = ids_store.getitemdate(ll_i, 1)
	end if
	
	lstr_data.codice = string(ldt_date, s_cs_xx.db_funzioni.formato_data)
	lstr_data.data[1] = datetime(ldt_date, 00:00:00)
	
	ltvi_item = wf_new_item(true, ICONA_DATA_REGISTRAZIONE)
	
	ltvi_item.data = lstr_data
	
	if isnull(ldt_date) or year(ldt_date) <= 1950 then
		ltvi_item.label = "<Data Registrazione mancante>"
	else
		//ltvi_item.label = string(ids_store.getitemdatetime(ll_i, 1), "dd/mm/yyyy")
		ltvi_item.label = string(ldt_date, "dd/mm/yyyy")
	end if
	
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)

return ll_rows
end function

public function long wf_inserisci_movimenti (long al_handle);string					ls_sql, ls_error, ls_message_overflow, ls_flag_ordinamento
long					ll_rows, ll_i, ll_handle, li_num_reg
integer				li_anno_reg
treeviewitem		ltvi_item


ls_flag_ordinamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_ordinamento")

ls_sql = "SELECT mov_magazzino.anno_registrazione, mov_magazzino.num_registrazione "+&
			"FROM mov_magazzino "+&
			"WHERE mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

//imposto l'ordinamento per anno e numero
ls_sql += " ORDER BY mov_magazzino.anno_registrazione, mov_magazzino.num_registrazione"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

if ls_flag_ordinamento="S" then
	ids_store.setsort("#1 desc, #2 desc")
else
	ids_store.setsort("#1 asc, #2 asc")
end if


ids_store.sort()

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_row  then
	
	ls_message_overflow = g_str.format("Attenzione: $1 movimenti di magazzino da visualizzare potrebbero rallentare il sistema.~r~nContinuo a visualizzare i dati?", ll_rows)
	
	if not g_mb.confirm(ls_message_overflow) then
		// Visualizzo solo i movimenti nel limite
		ll_rows = il_max_row
	end if
end if

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	li_anno_reg =  ids_store.getitemnumber(ll_i, 1)
	li_num_reg = ids_store.getitemnumber(ll_i, 2)
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo movimento "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	ll_handle = wf_inserisci_mov(al_handle, li_anno_reg, li_num_reg)

next

setpointer(Hourglass!)


return ll_rows
end function

public function long wf_inserisci_mov (long al_handle, integer ai_anno_registrazione, long al_num_registrazione);string				ls_sql, ls_error, ls_cod_prodotto, ls_um_mag, ls_flag_manuale, ls_flag_storico
dec{4}			ld_quan_mov, ld_val_mov
treeviewitem	ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = ai_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

//mettiamo sul nodo le info principali -----------------------------
select 	m.quan_movimento,m.val_movimento,m.cod_prodotto,m.flag_manuale,m.flag_storico,p.cod_misura_mag
into   		:ld_quan_mov,:ld_val_mov,:ls_cod_prodotto,:ls_flag_manuale,:ls_flag_storico,:ls_um_mag
from   mov_magazzino as m
join anag_prodotti as p on p.cod_azienda=m.cod_azienda and p.cod_prodotto=m.cod_prodotto
where	m.cod_azienda = :s_cs_xx.cod_azienda and
			m.anno_registrazione = :ai_anno_registrazione and
			m.num_registrazione = :al_num_registrazione;


if ls_flag_storico = "S" then
	ltvi_item = wf_new_item(false, ICONA_MOV_STORICO)
elseif ls_flag_manuale="S" then
	ltvi_item = wf_new_item(false, ICONA_MOV_MANUALE)
else
	ltvi_item = wf_new_item(false, ICONA_MOV)
end if

if isnull(ld_quan_mov) then ld_quan_mov=0.00
if isnull(ld_val_mov) then ld_val_mov=0.00
ld_val_mov = ld_val_mov * ld_quan_mov

if isnull(ls_um_mag) then ls_um_mag=""

ltvi_item.label = string(ai_anno_registrazione) + "/"+ string(al_num_registrazione)
ltvi_item.label += " - " + ls_cod_prodotto
ltvi_item.label += " - "+ ls_um_mag + " " + string(ld_quan_mov, "#,###,###,##0.00##") + " - Val. "+string(ld_val_mov, "#,###,###,##0.00##") 

ltvi_item.data = lstr_data


return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)


end function

public function integer wf_doc_rif (integer ai_anno_reg, long al_num_reg);string	 ls_full_doc_rif, ls_small_doc_rif, ls_errore, ls_visualizza


if isnull(ai_anno_reg) or isnull(al_num_reg) or ai_anno_reg = 0 or al_num_reg = 0 then
	return -1
end if


//ls_visualizza = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_visualizza_doc")
//if ls_visualizza="S" then
//else
//	tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = ""
//	return 0
//end if


if iuo_ref_mov_mag.uof_ref_mov_mag(ai_anno_reg, al_num_reg, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore) <> 0 then
	tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = "Errore in lettura documenti collegati: " + ls_errore
	//g_mb.messagebox("APICE","Errore in lettura documenti collegati!~r~n" + ls_errore ,stopsign!)
	return 0
else
	// stefanop 18/06/2010 
	if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
		if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
			
			//inizio modifica --------------------------
			iuo_ref_mov_mag.uof_ref_mov_mag_prog_mov(ai_anno_reg, al_num_reg, ref ls_full_doc_rif, ref ls_small_doc_rif, ref ls_errore)
			
			if isnull(ls_small_doc_rif) or ls_small_doc_rif = "" then
				if isnull(ls_full_doc_rif) or ls_full_doc_rif = "" then
					tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = "MANCA"
				else
					tab_dettaglio.det_1.dw_1.object.st_doc_rif.dw_1.object.text = ls_full_doc_rif
				end if
			else
				tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = ls_small_doc_rif
			end if
			//fine modifica --------------------------
			
			//commentato per questa modifica ---
			//st_doc_rif.text = "MANCA"
			
		else
			tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = ls_full_doc_rif
		end if
	else
		tab_dettaglio.det_1.dw_1.object.st_doc_rif.text = ls_small_doc_rif
	end if
	// ---
end if

return 0
end function

public function integer wf_prod_rif (long al_row);/**
 * stefanop
 * 21/06/2010
 * progetto 140 funzione 4
 *
 * Carico dettagli grezzo
 **/
 
 string 	ls_cod_prodotto_bf, ls_cod_prodotto_mov, ls_errore, ls_flag_prog_quan_entrata, ls_flag_prog_quan_acquistata, &
 			ls_cod_tipo_movimento, ls_cod_prodotto_alt, ls_cod_misura_mag_mov, ls_cod_misura_mag, ls_des_prodotto, &
			ls_cod_prodotto, ls_cod_mov_carico, ls_cod_misura_mag1, ls_cod_prodotto_raggruppato, ls_visualizza
 long		ll_anno, ll_num
 double	ld_val_movimento, ld_fat_conversione_rag_mag, ld_val_movimento_raggr
 datetime ldt_data_registrazione, ldt_data_registrazione_raggr



tab_dettaglio.det_1.dw_2.reset()

if al_row > 0 then
else
	return 1
end if


ls_visualizza = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_visualizza_prod_rif")
if ls_visualizza="S" then
	tab_dettaglio.det_1.dw_2.visible = true
else
	tab_dettaglio.det_1.dw_2.visible = false
	return 1
end if

tab_dettaglio.det_1.dw_2.insertrow(0)
ll_anno = tab_dettaglio.det_1.dw_1.getitemnumber(al_row,"anno_registrazione")
ll_num = tab_dettaglio.det_1.dw_1.getitemnumber(al_row,"num_registrazione")
ls_cod_prodotto_mov = tab_dettaglio.det_1.dw_1.getitemstring(al_row,"cod_prodotto")
ls_cod_tipo_movimento = tab_dettaglio.det_1.dw_1.getitemstring(al_row,"cod_tipo_movimento")

if isnull(ll_anno) or isnull(ll_num) or ll_anno < 1900 or ll_num < 1 then return -1

if iuo_ref_mov_mag.uof_prodotto_ref_mov_mag(ll_anno, ll_num,ref ls_cod_prodotto_bf,ref ls_errore) < 0 then
	g_mb.error("APICE", ls_errore)
	return -1
end if

if isnull(ls_cod_prodotto_bf) or ls_cod_prodotto_bf = "" then 
	// non è stata associata nessuna bolla o fattua al movimento, probabilmente è manuale
	// controllo che sia un movimento di carico
	select flag_prog_quan_entrata, flag_prog_quan_acquistata
	into :ls_flag_prog_quan_entrata, :ls_flag_prog_quan_acquistata
	from tab_tipi_movimenti_det
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_mov_det = :ls_cod_tipo_movimento;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("APICE", "Errore durante il controllo del dettaglio movimento con codice: " + ls_cod_tipo_movimento)
		return -1
	// stefanop 19/07/2010: ticket 2010/184
	elseif sqlca.sqlcode = 100 then
		return 0
	end if
	
	if (isnull(ls_flag_prog_quan_entrata) or ls_flag_prog_quan_entrata <> "+") or (isnull(ls_flag_prog_quan_acquistata) or ls_flag_prog_quan_acquistata <> "+") then
		// non è un movimento di carico, quindi esco
		return -1
	end if
	
	// carico codice alternativo
	ls_cod_prodotto = ls_cod_prodotto_mov
	
elseif ls_cod_prodotto_bf = ls_cod_prodotto_mov then
	ls_cod_prodotto = ls_cod_prodotto_mov
	
elseif  ls_cod_prodotto_bf <> ls_cod_prodotto_mov then // assegno il codice della bolla o fattura
	ls_cod_prodotto = ls_cod_prodotto_bf
else
	return -1
end if

// controllo codice alternativo
if wf_codice_alternativo(ls_cod_prodotto, ref ls_cod_prodotto_alt) < 0 then return -1
if isnull(ls_cod_prodotto_alt) or ls_cod_prodotto_alt = "" then 
	return -1
end if

// DESCRIZIONE (campo b della specifica)
select des_prodotto, cod_misura_mag
into :ls_des_prodotto, :ls_cod_misura_mag
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :ls_cod_prodotto_alt;
	
if sqlca.sqlcode <> 0 then
	return -1
else
	tab_dettaglio.det_1.dw_2.setitem(1, "cod_grezzo", ls_cod_prodotto_alt)
	tab_dettaglio.det_1.dw_2.setitem(1, "des_grezzo", ls_des_prodotto)
	tab_dettaglio.det_1.dw_2.setitem(1, "cod_um_mag", ls_cod_misura_mag)
end if

// carico i movimenti di magazzino che fanno carico

// carico la data di registrazione più grande
if wf_carica_val_un(ls_cod_prodotto_alt, ref ld_val_movimento, ref ldt_data_registrazione) < 0 then
	return -1
else
	tab_dettaglio.det_1.dw_2.setitem(1, "val_movimento", ld_val_movimento)
	tab_dettaglio.det_1.dw_2.setitem(1, "data_registrazione", ldt_data_registrazione)
end if
// ----

// dettaglio c della specifica	
select cod_misura_mag
into :ls_cod_misura_mag1
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :ls_cod_prodotto_mov;
	
if sqlca.sqlcode <> 0 or isnull(ls_cod_misura_mag1) then  return -1
if ls_cod_misura_mag1 <> ls_cod_misura_mag then
	tab_dettaglio.det_1.dw_2.setitem(1, "cod_um_mag3", ls_cod_misura_mag1)
	
	// controllo codice raggruppo
	select cod_prodotto_raggruppato, fat_conversione_rag_mag
	into :ls_cod_prodotto_raggruppato, :ld_fat_conversione_rag_mag
	from anag_prodotti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto;
		
	if sqlca.sqlcode <> 0 or isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato = "" then return -1

	ld_val_movimento =  ld_val_movimento / ld_fat_conversione_rag_mag
	tab_dettaglio.det_1.dw_2.setitem(1, "val_movimento2", ld_val_movimento)
		
	tab_dettaglio.det_1.dw_2.setitem(1, "cod_grezzo2", ls_cod_prodotto)
	tab_dettaglio.det_1.dw_2.setitem(1, "cod_um_mag2", ls_cod_misura_mag1)
end if

return 1
end function

public function integer wf_carica_val_un (string as_cod_prodotto, ref double ad_valore_unitario, ref datetime adt_data_registrazione);setnull(ad_valore_unitario)
setnull(adt_data_registrazione)

select max(data_registrazione)
into :adt_data_registrazione
from mov_magazzino
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto and
	cod_tipo_movimento in (
		select cod_tipo_mov_det
		from tab_tipi_movimenti_det
		where 
			//flag_prog_quan_entrata = '+' or
			flag_prog_quan_acquistata = '+'
	);

if sqlca.sqlcode <> 0 then
	g_mb.error("APICE", "Errore durante il recupero della data dell'ultimo movimento di carico per il prodotto: " + as_cod_prodotto)
	return -1
elseif isnull(adt_data_registrazione) then
	return -1
end if

select val_movimento
into :ad_valore_unitario
from mov_magazzino
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto and
	data_registrazione = :adt_data_registrazione and
	cod_tipo_movimento in (
		select cod_tipo_mov_det
		from tab_tipi_movimenti_det
		where 
			//flag_prog_quan_entrata = '+' or
			flag_prog_quan_acquistata = '+'
	);

return 0
end function

public function integer wf_codice_alternativo (string as_cod_prodotto, ref string as_cod_prodotto_alternativo);/**
 * Stefanop 
 * 18/06/2010
 *
 * Controllo se il prodotto ha un codice alternativo impostato e lo ritorno
 **/
 

select cod_prodotto_alt
into :as_cod_prodotto_alternativo
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :as_cod_prodotto;
	
if sqlca.sqlcode <> 0 then return -1
return 0
end function

public function boolean wf_singolo_nodo (string as_tipologia, ref string as_valore);integer			li_valore
string				ls_valore
datetime			ldt_valore, ldt_valore_2


choose case as_tipologia
	case "A"
		li_valore = tab_ricerca.ricerca.dw_ricerca.GetItemnumber(1, "rn_anno_registrazione")
		if li_valore > 0 and not isnull(li_valore) then
			as_valore = string(li_valore)
			return true
		end if
		
		
	case "T"
		ls_valore = tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_tipo_movimento")
		if ls_valore <> "" and not isnull(ls_valore) then
			as_valore = ls_valore
			return true
		end if
		
		
	case "D"
		ls_valore = tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_deposito")
		if ls_valore <> "" and not isnull(ls_valore) then
			as_valore = ls_valore
			return true
		end if
		
		
	case "R"
		ldt_valore = tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_da")
		ldt_valore_2 = tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "rdd_data_documento_a")
		
		if	year(date(ldt_valore)) > 1950 and not isnull(ldt_valore) and &
			year(date(ldt_valore_2)) > 1950 and not isnull(ldt_valore_2) and &
			ldt_valore = ldt_valore_2 			then
			
			as_valore = string(ldt_valore, "dd/mm/yyyy")
			return true
		end if
		
		
	case "P"
		ls_valore = tab_ricerca.ricerca.dw_ricerca.GetItemString(1, "rs_cod_prodotto")
		if ls_valore <> "" and not isnull(ls_valore) then
			as_valore = ls_valore
			return true
		end if
		
		
	case else
		return false
		
end choose


return false
end function

public function long wf_modifica_item_mov (long al_handle);string				ls_cod_prodotto, ls_um_mag, ls_flag_manuale, ls_flag_storico
dec{4}			ld_quan_mov, ld_val_mov
treeviewitem	ltvi_item
integer			li_anno_mov
long				ll_num_mov

str_treeview		lstr_data


tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltvi_item)
lstr_data = ltvi_item.data

li_anno_mov = integer(lstr_data.decimale[1])
ll_num_mov = long(lstr_data.decimale[2])


//mettiamo sul nodo le info principali -----------------------------
select 	m.quan_movimento, m.val_movimento, m.cod_prodotto, m.flag_manuale, m.flag_storico, p.cod_misura_mag
into   		:ld_quan_mov, 	:ld_val_mov, :ls_cod_prodotto, :ls_flag_manuale, 	:ls_flag_storico, :ls_um_mag
from   mov_magazzino as m
join anag_prodotti as p on p.cod_azienda=m.cod_azienda and p.cod_prodotto=m.cod_prodotto
where	m.cod_azienda = :s_cs_xx.cod_azienda and
			m.anno_registrazione = :li_anno_mov and
			m.num_registrazione = :ll_num_mov;

if ls_flag_storico = "S" then
	ltvi_item = wf_new_item(false, ICONA_MOV_STORICO)
elseif ls_flag_manuale="S" then
	ltvi_item = wf_new_item(false, ICONA_MOV_MANUALE)
else
	ltvi_item = wf_new_item(false, ICONA_MOV)
end if

if isnull(ld_quan_mov) then ld_quan_mov=0.00
if isnull(ld_val_mov) then ld_val_mov=0.00
ld_val_mov = ld_val_mov * ld_quan_mov


if isnull(ls_um_mag) then ls_um_mag=""

ltvi_item.label = string(li_anno_mov) + "/"+ string(ll_num_mov)
ltvi_item.label += " - " + ls_cod_prodotto
ltvi_item.label += " - " + ls_um_mag + " " + string(ld_quan_mov, "#,###,###,##0.00##") + " - Val. "+string(ld_val_mov, "#,###,###,##0.00##") 

ltvi_item.data = lstr_data

tab_ricerca.selezione.tv_selezione.setitem(al_handle, ltvi_item)

return 0


end function

on w_mov_magazzino_tv.create
int iCurrent
call super::create
end on

on w_mov_magazzino_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;string							ls_wizard, ls_prova
dec{0}						ld_valore
s_cs_xx_parametri			lstr_param



iuo_ref_mov_mag = create uo_ref_mov_mag

is_codice_filtro = "MOV"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//max count e clausola selezione primo record (che è diverso da dbms a dbms)
RegistryGet(	"HKEY_LOCAL_MACHINE\Software\Consulting&Software\database_" + s_cs_xx.profilocorrente ,"enginetype", is_enginetype)

if is_enginetype = "SYBASE_ASA" then
	il_max_row=300
	is_top_clause = " TOP 1 START AT 1 "
	
elseif is_enginetype = "SYBASE_ASE" or is_enginetype = "MSSQL" then
	il_max_row=1000
	is_top_clause = " TOP 1 "

elseif is_enginetype = "ORACLE" then
	il_max_row=1000
	is_top_clause = ""
end if
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

guo_functions.uof_get_parametro_azienda("ESC", ld_valore)
if ld_valore > 0 then ii_ESC = integer(ld_valore)

try
	lstr_param = message.powerobjectparm
	
	if lstr_param.parametro_ul_1>0 and lstr_param.parametro_ul_2 > 0 then
		tab_ricerca.ricerca.dw_ricerca.setitem(1, "rn_anno_registrazione", lstr_param.parametro_ul_1)
		tab_ricerca.ricerca.dw_ricerca.setitem(1, "rn_num_registrazione", lstr_param.parametro_ul_2)
		
		wf_treeview_search()
	end if
	
catch (runtimeerror err)
end try



end event

event close;call super::close;

destroy iuo_ref_mov_mag
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca,"rs_cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_ricerca.ricerca.dw_ricerca,"rs_cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_dettaglio.det_1.dw_1,"cod_tipo_mov_det",sqlca,&
                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
                 "(tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "')")	  
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_mov_magazzino_tv
integer x = 1303
integer width = 3154
integer height = 2564
end type

on tab_dettaglio.create
call super::create
this.Control[]={this.det_1}
end on

on tab_dettaglio.destroy
call super::destroy
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3118
integer height = 2440
string text = "Dettaglio Movimento"
dw_2 dw_2
dw_1 dw_1
end type

on det_1.create
this.dw_2=create dw_2
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_2
this.Control[iCurrent+2]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_2)
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_mov_magazzino_tv
integer width = 1271
integer height = 2556
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1234
integer height = 2432
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1275
integer height = 2428
string dataobject = "d_mov_magazzino_sel_tv"
boolean vscrollbar = false
end type

event dw_ricerca::itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_visualizza_doc"
		if data="S" then
			tab_dettaglio.det_1.dw_1.object.st_doc_rif.visible = true
			tab_dettaglio.det_1.dw_1.object.r_doc_rif.visible = true
		else
			tab_dettaglio.det_1.dw_1.object.st_doc_rif.visible = false
			tab_dettaglio.det_1.dw_1.object.r_doc_rif.visible = false
		end if
		
	case "flag_visualizza_prod_rif"
		if data="S" then
			tab_dettaglio.det_1.dw_2.visible = true
		else
			tab_dettaglio.det_1.dw_2.visible = false
		end if
		
end choose
end event

event dw_ricerca::buttonclicked;call super::buttonclicked;if row < 0 then return 0


choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")


	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"rs_cod_fornitore")

end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1234
integer height = 2432
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1221
integer height = 2408
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::rightclicked;call super::rightclicked;//long ll_row
//treeviewitem ltvi_item
//str_treeview lstr_data
//m_ordini_acquisto lm_menu
//
//if AncestorReturnValue < 0 then return
//
//pcca.window_current = getwindow()
//ll_row = tab_dettaglio.det_1.dw_1.getrow()
//
////tab_ricerca.selezione.tv_selezione.selectitem(handle)
//tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)
//
//lstr_data = ltvi_item.data
//
//if lstr_data.tipo_livello = "N" then
//
//	lm_menu = create m_ordini_acquisto
//	
//	lm_menu.m_salda.enabled =  (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_evasione") <> "E")
//	lm_menu.m_blocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "N")
//	lm_menu.m_sblocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "S")
//	
//	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
//	
//	destroy lm_menu
//	
//end if
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

type dw_2 from datawindow within det_1
integer x = 18
integer y = 1860
integer width = 3081
integer height = 568
integer taborder = 30
string title = "none"
string dataobject = "d_mov_magazzino_det_1_tv"
boolean livescroll = true
end type

type dw_1 from uo_cs_xx_dw within det_1
integer x = 18
integer y = 12
integer width = 3081
integer height = 1840
integer taborder = 30
string dataobject = "d_mov_magazzino_det_tv"
end type

event pcd_delete;uo_magazzino		luo_mag
long					ll_handle



if dw_1.getitemstring(dw_1.getrow(),"flag_manuale") = "N" and s_cs_xx.cod_utente<>"CS_SYSTEM" then
		g_mb.warning("Impossibile cancellare movimenti automatici!")
		PCCA.Error = c_Fatal
		postevent("pcd_view")
		return
end if
if this.getitemstring(dw_1.getrow(),"flag_storico") = "S" and s_cs_xx.cod_utente<>"CS_SYSTEM" then
		g_mb.warning("Impossibile cancellare movimenti storicizzati!")
		PCCA.Error = c_Fatal
		postevent("pcd_view")
		return
end if

if g_mb.confirm("Elimino movimento selezionato ?") then
	luo_mag = create uo_magazzino
	if luo_mag.uof_elimina_movimenti(	dw_1.getitemnumber(dw_1.getrow(),"anno_registrazione"),&
													dw_1.getitemnumber(dw_1.getrow(),"num_registrazione"),&
													true) = 0 then
		g_mb.success("Cancellazione movimento eseguita con successo")
		COMMIT;
		
		//rimozione del nodo
		tab_ricerca.selezione.tv_selezione.findItem(CurrentTreeItem!, ll_handle)
		if ll_handle > 0 then tab_ricerca.selezione.tv_selezione.deleteitem(ll_handle)
		wf_set_deleted_item_status()
		
	end if
	
	destroy luo_mag
end if
end event

event pcd_modify;s_cs_xx_parametri				lstr_return
long								ll_handle
treeviewitem					ltvi_item


if this.getitemstring(this.getrow(),"flag_manuale") = "N" and s_cs_xx.cod_utente<>"CS_SYSTEM" then
		g_mb.warning("Impossibile modificare movimenti automatici!")
		triggerevent("pcd_view")
		
elseif this.getitemstring(this.getrow(),"flag_storico") = "S" and s_cs_xx.cod_utente<>"CS_SYSTEM" then
		g_mb.warning("Impossibile modificare movimenti storicizzati")
		triggerevent("pcd_view")

else
	window_open_parm(w_modifica_movimenti, 0, dw_1)
	try
		lstr_return = message.powerobjectparm
		
		if lstr_return.parametro_b_1 then
			//la modifica ha avuto successo
			
			ll_handle = wf_get_current_handle()
			
			if ll_handle > 0 then
				//aggiorno la label del nodo
				wf_modifica_item_mov(ll_handle)
				
				//retrieve della dw
				dw_1.change_dw_current()
				dw_1.postevent("pcd_retrieve")
			end if

		end if
	catch (runtimeerror e)
	end try
	
end if



end event

event pcd_new;//ancestor script disattivato


string						ls_parametro_laped
window					lw_window
s_cs_xx_parametri		lstr_return
boolean					lb_struttura = false
integer					li_index


select stringa
into   :ls_parametro_laped
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LAP';
		 
if sqlca.sqlcode = 0 then
	if not isnull(ls_parametro_laped) and ls_parametro_laped <> "" then
		window_type_open(lw_window, ls_parametro_laped, 0)
		
		if ls_parametro_laped="w_mov_magazzino_det" then lb_struttura = true
		
	end if
else
	window_open_parm(w_mov_magazzino_det, 0, dw_1)
	lb_struttura = true
end if

if lb_struttura then
	
	try
		lstr_return = message.powerobjectparm
		
		if upperbound(lstr_return.parametro_d_1_a[]) > 0 then
			for li_index=1 to upperbound(lstr_return.parametro_d_1_a[])
				wf_inserisci_mov(0, lstr_return.parametro_d_1_a[li_index], lstr_return.parametro_d_2_a[li_index])
			next
		end if
	catch (runtimeerror e)
	end try
	
end if

end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "cb_stampa"
//		datetime ldt_data_reg
//		dec{4}	ld_quan_mov, ld_val_mov
//		long 	 	ll_row, ll_job, ll_anno_mov, ll_num_mov, ll_prog_mov
//		string 	ls_str, ls_cod_prodotto, ls_des_prodotto, ls_tipo_mov, ls_tipo_det
//		
//		ll_row = dw_1.getrow()
//		ll_anno_mov = dw_1.getitemnumber(ll_row,"anno_registrazione")
//		ll_num_mov = dw_1.getitemnumber(ll_row,"num_registrazione")
//		ll_prog_mov = dw_1.getitemnumber(ll_row,"prog_mov")
//		ldt_data_reg = dw_1.getitemdatetime(ll_row,"data_registrazione")
//		ls_cod_prodotto = dw_1.getitemstring(ll_row,"cod_prodotto")
//		ls_des_prodotto = f_des_tabella("anag_prodotti","cod_prodotto = '" + ls_cod_prodotto + "'","des_prodotto")
//		ls_tipo_mov = dw_1.getitemstring(ll_row,"cod_tipo_movimento")
//		ls_tipo_det = dw_1.getitemstring(ll_row,"cod_tipo_mov_det")
//		ld_quan_mov = dw_1.getitemnumber(ll_row,"quan_movimento")
//		ld_val_mov = dw_1.getitemnumber(ll_row,"val_movimento")
//		
//		ls_str = "MOVIMENTO: " + string(ll_anno_mov) + "/" + string(ll_num_mov) + " progressivo " + string(ll_prog_mov) + &
//					"~r~nDATA REGISTRAZIONE: " + string(date(ldt_data_reg),"dd/mm/yyyy") + &
//					"~r~nPRODOTTO: " + ls_cod_prodotto + " " + ls_des_prodotto + &
//					"~r~nTIPO MOVIMENTO: " + ls_tipo_mov + " dettaglio " + ls_tipo_det + &
//					"~r~nQUANTITA': " + string(ld_quan_mov) + &
//					"~r~nVALORE: " + string(ld_val_mov) + &
//					"~r~n~r~n" + tab_dettaglio.det_1.dw_1.object.st_doc_rif.text
//		
//		if isnull(ls_str) then
//			return -1
//		end if
//		
//		ll_job = printopen()
//		print(ll_job,ls_str)
//		printclose(ll_job)
		
		
		//tab_dettaglio.det_1.dw_1.change_dw_current()
		tab_dettaglio.det_1.dw_1.postevent("pcd_print")
		
		
end choose
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

setpointer(hourglass!)
wf_doc_rif(istr_data.decimale[1],istr_data.decimale[2])

wf_prod_rif(tab_dettaglio.det_1.dw_1.getrow())

setpointer(arrow!)

change_dw_current()
end event

event updatestart;call super::updatestart;//long					ll_i, ll_handle
//
//if i_extendmode then
//
//	//-------------------------------------------------------------------------------------------
//	if modifiedcount() > 0 then
//		for ll_i = 1 to dw_1.modifiedcount()
//			//aggiorno la dw del movimento modioficato
//			tab_ricerca.selezione.tv_selezione.findItem(CurrentTreeItem!, ll_handle)
//			if ll_handle > 0 then
//				dw_1.change_dw_current()
//				dw_1.postevent("pcd_retrieve")
//			end if
//		next
//	end if
//	
//
//end if
end event

