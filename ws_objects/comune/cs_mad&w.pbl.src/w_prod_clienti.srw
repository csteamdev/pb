﻿$PBExportHeader$w_prod_clienti.srw
$PBExportComments$Finestra Gestione Prodotti Clienti
forward
global type w_prod_clienti from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_prod_clienti
end type
type dw_prod_clienti_lista from uo_cs_xx_dw within w_prod_clienti
end type
type dw_prod_clienti_det from uo_cs_xx_dw within w_prod_clienti
end type
type dw_folder from u_folder within w_prod_clienti
end type
end forward

global type w_prod_clienti from w_cs_xx_principale
integer width = 3031
integer height = 2056
string title = "Gestione Prodotti Clienti"
dw_ricerca dw_ricerca
dw_prod_clienti_lista dw_prod_clienti_lista
dw_prod_clienti_det dw_prod_clienti_det
dw_folder dw_folder
end type
global w_prod_clienti w_prod_clienti

type variables
string				is_sql_base
end variables

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[1] = dw_prod_clienti_lista
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)



dw_prod_clienti_lista.set_dw_key("cod_azienda")

//dw_prod_clienti_lista.set_dw_options(sqlca, &
//                                     pcca.null_object, &
//                                     c_default, &
//                                     c_default)
dw_prod_clienti_lista.set_dw_options(	sqlca, &
													pcca.null_object, &
													c_noretrieveonopen, &
													c_default)

dw_prod_clienti_det.set_dw_options(sqlca, &
                                   dw_prod_clienti_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)

iuo_dw_main = dw_prod_clienti_lista

is_sql_base = dw_prod_clienti_lista.getsqlselect()
end event

on w_prod_clienti.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.dw_prod_clienti_lista=create dw_prod_clienti_lista
this.dw_prod_clienti_det=create dw_prod_clienti_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.dw_prod_clienti_lista
this.Control[iCurrent+3]=this.dw_prod_clienti_det
this.Control[iCurrent+4]=this.dw_folder
end on

on w_prod_clienti.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.dw_prod_clienti_lista)
destroy(this.dw_prod_clienti_det)
destroy(this.dw_folder)
end on

type dw_ricerca from u_dw_search within w_prod_clienti
integer x = 41
integer y = 140
integer width = 2894
integer height = 704
integer taborder = 20
string dataobject = "d_prod_clienti_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string		ls_null

choose case dwo.name
	case "b_cerca"
		dw_prod_clienti_lista.change_dw_current()
		parent.triggerevent("pc_retrieve")


	case "b_annulla"
		setnull(ls_null)
		dw_ricerca.setitem(1, "cod_prodotto", ls_null)
		dw_ricerca.setitem(1, "cod_cliente", ls_null)
		dw_ricerca.setitem(1, "cod_prod_cliente", ls_null)


	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")


	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")


end choose
end event

type dw_prod_clienti_lista from uo_cs_xx_dw within w_prod_clienti
integer x = 23
integer y = 128
integer width = 2949
integer height = 1376
integer taborder = 10
string dataobject = "d_prod_clienti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_retrieve;call super::pcd_retrieve;string			ls_sql, ls_valore
long			ll_errore


dw_ricerca.accepttext()

ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_valore = dw_ricerca.getitemstring(1, "cod_prodotto")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and cod_prodotto='"+ls_valore+"' "
end if

ls_valore = dw_ricerca.getitemstring(1, "cod_cliente")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and cod_cliente='"+ls_valore+"' "
end if

ls_valore = dw_ricerca.getitemstring(1, "cod_prod_cliente")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and cod_prod_cliente like '%"+ls_valore+"%' "
end if

//metto order by
ls_sql += " order by cod_prodotto, cod_cliente,cod_prod_cliente "

dw_prod_clienti_lista.setsqlselect(ls_sql)
ll_errore = dw_prod_clienti_lista.retrieve()
//ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

dw_folder.fu_selecttab(2)
end event

type dw_prod_clienti_det from uo_cs_xx_dw within w_prod_clienti
integer x = 23
integer y = 1532
integer width = 2949
integer height = 400
integer taborder = 20
string dataobject = "d_prod_clienti_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_prod_clienti_det,"cod_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_prod_clienti_det,"cod_prodotto")
end choose
end event

event pcd_delete;call super::pcd_delete;dw_prod_clienti_det.object.b_ricerca_prodotto.enabled = false
dw_prod_clienti_det.object.b_ricerca_cliente.enabled = false
end event

event pcd_modify;call super::pcd_modify;dw_prod_clienti_det.object.b_ricerca_prodotto.enabled = false
dw_prod_clienti_det.object.b_ricerca_cliente.enabled = false
end event

event pcd_new;call super::pcd_new;dw_prod_clienti_det.object.b_ricerca_prodotto.enabled = true
dw_prod_clienti_det.object.b_ricerca_cliente.enabled = true
end event

event pcd_view;call super::pcd_view;dw_prod_clienti_det.object.b_ricerca_prodotto.enabled = false
dw_prod_clienti_det.object.b_ricerca_cliente.enabled = false
end event

type dw_folder from u_folder within w_prod_clienti
integer x = 5
integer y = 16
integer width = 2976
integer height = 1492
integer taborder = 10
boolean border = false
end type

