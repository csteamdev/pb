﻿$PBExportHeader$w_prod_collegati.srw
forward
global type w_prod_collegati from w_cs_xx_principale
end type
type dw_prod_collegati_lista from uo_cs_xx_dw within w_prod_collegati
end type
type dw_prod_collegati_det from uo_cs_xx_dw within w_prod_collegati
end type
type cb_ricerca_prod from cb_prod_ricerca within w_prod_collegati
end type
end forward

global type w_prod_collegati from w_cs_xx_principale
int Width=2282
int Height=1177
boolean TitleBar=true
string Title="Gestione Prodotti Collegati"
dw_prod_collegati_lista dw_prod_collegati_lista
dw_prod_collegati_det dw_prod_collegati_det
cb_ricerca_prod cb_ricerca_prod
end type
global w_prod_collegati w_prod_collegati

on w_prod_collegati.create
int iCurrent
call w_cs_xx_principale::create
this.dw_prod_collegati_lista=create dw_prod_collegati_lista
this.dw_prod_collegati_det=create dw_prod_collegati_det
this.cb_ricerca_prod=create cb_ricerca_prod
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_prod_collegati_lista
this.Control[iCurrent+2]=dw_prod_collegati_det
this.Control[iCurrent+3]=cb_ricerca_prod
end on

on w_prod_collegati.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_prod_collegati_lista)
destroy(this.dw_prod_collegati_det)
destroy(this.cb_ricerca_prod)
end on

event pc_setwindow;call super::pc_setwindow;dw_prod_collegati_lista.set_dw_key("cod_azienda")
dw_prod_collegati_lista.set_dw_key("cod_prodotto")
dw_prod_collegati_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_prod_collegati_det.set_dw_options(sqlca, &
                                    dw_prod_collegati_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_prod_collegati_lista


end event

type dw_prod_collegati_lista from uo_cs_xx_dw within w_prod_collegati
int X=23
int Y=21
int Width=2195
int Height=621
int TabOrder=10
string DataObject="d_prod_collegati_lista"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_n_righe
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ll_n_righe = this.rowcount()

for ll_i = 1 to ll_n_righe
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end event

type dw_prod_collegati_det from uo_cs_xx_dw within w_prod_collegati
int X=23
int Y=661
int Width=2195
int Height=381
int TabOrder=20
string DataObject="d_prod_collegati_det"
end type

type cb_ricerca_prod from cb_prod_ricerca within w_prod_collegati
int X=2126
int Y=761
int TabOrder=2
boolean BringToTop=true
end type

event getfocus;call super::getfocus;dw_prod_collegati_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_collegato"
end event

