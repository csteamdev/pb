﻿$PBExportHeader$w_prod_dw.srw
$PBExportComments$Finestra Ricerca Prodotti Per Livelli ( Senza Immagini )
forward
global type w_prod_dw from Window
end type
type pb_restituisci from picturebutton within w_prod_dw
end type
type st_livello_attuale from statictext within w_prod_dw
end type
type pb_return from picturebutton within w_prod_dw
end type
type pb_close from picturebutton within w_prod_dw
end type
type tab_liv_prod from tab within w_prod_dw
end type
type tabpage_dettagli from userobject within tab_liv_prod
end type
type mle_note from multilineedit within tabpage_dettagli
end type
type st_des_aggiuntiva from statictext within tabpage_dettagli
end type
type st_descrizione from statictext within tabpage_dettagli
end type
type st_codice from statictext within tabpage_dettagli
end type
type st_2 from statictext within tabpage_dettagli
end type
type st_1 from statictext within tabpage_dettagli
end type
type gb_3 from groupbox within tabpage_dettagli
end type
type gb_1 from groupbox within tabpage_dettagli
end type
type tabpage_immagine from userobject within tab_liv_prod
end type
type dw_lista_blob from datawindow within tabpage_immagine
end type
type p_immagine from picture within tabpage_immagine
end type
type gb_2 from groupbox within w_prod_dw
end type
type dw_prod_liv_prod from datawindow within w_prod_dw
end type
type dw_prod_liv_5 from datawindow within w_prod_dw
end type
type dw_prod_liv_4 from datawindow within w_prod_dw
end type
type dw_prod_liv_3 from datawindow within w_prod_dw
end type
type dw_prod_liv_2 from datawindow within w_prod_dw
end type
type dw_prod_liv_1 from datawindow within w_prod_dw
end type
type tabpage_dettagli from userobject within tab_liv_prod
mle_note mle_note
st_des_aggiuntiva st_des_aggiuntiva
st_descrizione st_descrizione
st_codice st_codice
st_2 st_2
st_1 st_1
gb_3 gb_3
gb_1 gb_1
end type
type tabpage_immagine from userobject within tab_liv_prod
dw_lista_blob dw_lista_blob
p_immagine p_immagine
end type
type tab_liv_prod from tab within w_prod_dw
tabpage_dettagli tabpage_dettagli
tabpage_immagine tabpage_immagine
end type
end forward

global type w_prod_dw from Window
int X=595
int Y=989
int Width=3004
int Height=1577
boolean TitleBar=true
string Title="Catalogo Prodotti"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
event ue_refresh_items ( )
event ue_postopen ( )
event ue_arrangeicons ( )
event ue_chglistview ( string as_newview )
event ue_edititem ( )
pb_restituisci pb_restituisci
st_livello_attuale st_livello_attuale
pb_return pb_return
pb_close pb_close
tab_liv_prod tab_liv_prod
gb_2 gb_2
dw_prod_liv_prod dw_prod_liv_prod
dw_prod_liv_5 dw_prod_liv_5
dw_prod_liv_4 dw_prod_liv_4
dw_prod_liv_3 dw_prod_liv_3
dw_prod_liv_2 dw_prod_liv_2
dw_prod_liv_1 dw_prod_liv_1
end type
global w_prod_dw w_prod_dw

type variables
DataStore	ids_Products
Integer	ii_SortCol = 2, ii_WindowBorder = 15
grsorttype	igrs_Sort = ascending!
integer ii_livello_attuale
string is_prod_liv_1, is_prod_liv_2, is_prod_liv_3
string is_prod_liv_4, is_prod_liv_5
string is_label_liv_1, is_label_liv_2, is_label_liv_3
string is_label_liv_4, is_label_liv_5
boolean ib_livello_prodotti = FALSE
long il_index
end variables

forward prototypes
public function string wf_select_prodotti ()
public function integer wf_agg_livello ()
end prototypes

event ue_refresh_items;//Integer			li_Rows, li_Cnt
//String			ls_Desc, ls_path_immagine, ls_Id, ls_des_agg, ls_descrizione, ls_Qty, ls_Price, ls_sql
//string			ls_des[],ls_cod[], ls_path[],ls_note[], ls_dag[]
//long   			ll_newrow
//Double			ldb_Price
//ListViewItem	llvi_Item
//
//
//SetPointer(Hourglass!)
//dw_prod_liv_1.reset()
//dw_prod_liv_1.setredraw(false)
//declare cu_livelli dynamic cursor for sqlsa;
//
//choose case ii_livello_attuale
//	case 1
//     ls_sql = "SELECT tab_livelli_prod_1.cod_livello_prod_1, tab_livelli_prod_1.des_livello, " + &
//	           "tab_livelli_prod_1.path_documento, tab_livelli_prod_1.note, tab_livelli_prod_1.des_agg_livello " + &
//			     "FROM tab_livelli_prod_1 " +&
//	           "WHERE tab_livelli_prod_1.cod_azienda = '" + s_cs_xx.cod_azienda + "'"
//	case 2 
//     ls_sql = "SELECT tab_livelli_prod_2.cod_livello_prod_2, tab_livelli_prod_2.des_livello, " + &
//	           "tab_livelli_prod_2.path_documento, tab_livelli_prod_2.note, tab_livelli_prod_2.des_agg_livello " + &
//			     "FROM tab_livelli_prod_2 " +&
//	           "WHERE (tab_livelli_prod_2.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//				  		  "(tab_livelli_prod_2.cod_livello_prod_1 = '" + is_prod_liv_1 + "') "
//	case 3 
//     ls_sql = "SELECT tab_livelli_prod_3.cod_livello_prod_3, tab_livelli_prod_3.des_livello, " + &
//	           "tab_livelli_prod_3.path_documento, tab_livelli_prod_3.note, tab_livelli_prod_3.des_agg_livello " + &
//			     "FROM tab_livelli_prod_3 " +&
//	           "WHERE (tab_livelli_prod_3.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//				  		  "(tab_livelli_prod_3.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
//				  		  "(tab_livelli_prod_3.cod_livello_prod_2  = '" + is_prod_liv_2 + "') "
//	case 4 
//     ls_sql = "SELECT tab_livelli_prod_4.cod_livello_prod_4, tab_livelli_prod_4.des_livello, " + &
//	           "tab_livelli_prod_4.path_documento, tab_livelli_prod_4.note, tab_livelli_prod_4.des_agg_livello " + &
//			     "FROM tab_livelli_prod_4 " +&
//	           "WHERE (tab_livelli_prod_4.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//				  		  "(tab_livelli_prod_4.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
//				  		  "(tab_livelli_prod_4.cod_livello_prod_2  = '" + is_prod_liv_2 + "') and " + &
//				  		  "(tab_livelli_prod_4.cod_livello_prod_3  = '" + is_prod_liv_3 + "') "
//	case 5
//     ls_sql = "SELECT tab_livelli_prod_5.cod_livello_prod_5, tab_livelli_prod_5.des_livello, " + &
//	           "tab_livelli_prod_5.path_documento, tab_livelli_prod_5.note, tab_livelli_prod_5.des_agg_livello " + &
//			     "FROM tab_livelli_prod_5 " +&
//	           "WHERE (tab_livelli_prod_5.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
//				  		  "(tab_livelli_prod_5.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
//				  		  "(tab_livelli_prod_5.cod_livello_prod_2  = '" + is_prod_liv_2 + "') and " + &
//				  		  "(tab_livelli_prod_5.cod_livello_prod_3  = '" + is_prod_liv_3 + "') and " + &
//				  		  "(tab_livelli_prod_5.cod_livello_prod_4  = '" + is_prod_liv_4 + "') "
//end choose
//
//prepare sqlsa from :ls_sql;
//open dynamic cu_livelli;
//
//li_cnt = 1
//do while 1=1
//   fetch cu_livelli into :ls_cod[li_cnt], :ls_des[li_cnt], :ls_path[li_cnt], :ls_note[li_cnt], :ls_dag[li_cnt];
//   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
//	li_cnt = li_cnt + 1
//loop
//close cu_livelli;
//
//
//// --------------------  uscito dal cursore: non esistono altri livelli -------------------
//// ----------------------------  allora ricerco i prodotti  -------------------------------
//if li_cnt = 1 then
//declare cu_prodotti dynamic cursor for sqlsa;
//
//		ls_sql = wf_select_prodotti()
//		
//		prepare sqlsa from :ls_sql;
//		open dynamic cu_prodotti;
//		li_cnt = 1
//		do while 1=1
//   		fetch cu_prodotti into :ls_cod[li_cnt], :ls_des[li_cnt];
//			setnull(ls_path[li_cnt])
//			setnull(ls_dag[li_cnt])
//			setnull(ls_note[li_cnt])
//		   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
//			li_cnt = li_cnt + 1
//		loop
//		close cu_prodotti;
//		ib_livello_prodotti = true
//end if
//// --------------------  carico dati livello sottostante -----------------------------------
//for li_cnt = 1 to upperbound(ls_cod) - 1
//		if isnull(ls_des[li_cnt]) then ls_des[li_cnt] = ""
//		if isnull(ls_note[li_cnt]) then ls_note[li_cnt] = ""
//		if isnull(ls_dag[li_cnt]) then ls_dag[li_cnt] = ""
//		if isnull(ls_path[li_cnt]) then ls_path[li_cnt] = "cartel1.bmp"
//		ll_newrow = dw_prod_liv_1.insertrow(0)
//		dw_prod_liv_1.object.cod_prodotto[ll_newrow] = ls_cod[li_cnt]
//		dw_prod_liv_1.object.des_prodotto_1[ll_newrow] = ls_des[li_cnt]
//		dw_prod_liv_1.object.des_prodotto_2[ll_newrow] = ls_dag[li_cnt]
//		dw_prod_liv_1.object.path_immagine[ll_newrow] = f_path_immagini() + "\" + ls_path[li_cnt]
//      pcca.mdi_frame.setmicrohelp("Indice:" + string(li_cnt) )
//next
//
//// ---------------------  aggiorno indicazione livello -------------------------------------
//
//if ib_livello_prodotti then
//	st_livello_attuale.text = "Dettaglio Prodotti"
//else	
//	choose case ii_livello_attuale
//		case 1
//			st_livello_attuale.text = is_label_liv_1
//		case 2
//			st_livello_attuale.text = is_label_liv_2
//		case 3
//			st_livello_attuale.text = is_label_liv_3
//		case 4
//			st_livello_attuale.text = is_label_liv_4
//		case 5
//			st_livello_attuale.text = is_label_liv_5
//	end choose		
//end if
//
//dw_prod_liv_1.setredraw(true)
end event

event ue_postopen;SetPointer(Hourglass!)

// Using a datastore to retrieve data from the database
ids_Products = Create DataStore
ids_Products.DataObject = "d_products"
// Set the large and small picture sizes
//Post Event ue_refresh_items()
dw_prod_liv_1.retrieve(s_cs_xx.cod_azienda)
dw_prod_liv_1.show()
dw_prod_liv_2.hide()
dw_prod_liv_3.hide()
dw_prod_liv_4.hide()
dw_prod_liv_5.hide()
dw_prod_liv_prod.hide()

end event

public function string wf_select_prodotti ();string ls_sql
choose case ii_livello_attuale
	case 2
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"( (anag_prodotti.cod_livello_prod_2 like '%' ) or ( anag_prodotti.cod_livello_prod_2 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_3 like '%' ) or ( anag_prodotti.cod_livello_prod_3 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 3
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_3 like '%' ) or ( anag_prodotti.cod_livello_prod_3 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 4
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 5
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_4 like '" + is_prod_liv_4 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%')  or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 6
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_4 like '" + is_prod_liv_4 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_5 like '" + is_prod_liv_5 + "' )"
end choose

return ls_sql
end function

public function integer wf_agg_livello ();string ls_descrizione, ls_desc_agg, ls_codice
long   ll_return


choose case ii_livello_attuale
	case 1
		ls_descrizione = dw_prod_liv_1.object.des_livello[dw_prod_liv_1.getrow()]
		ls_desc_agg = dw_prod_liv_1.object.des_agg_livello[dw_prod_liv_1.getrow()]
		ls_codice = dw_prod_liv_1.object.cod_livello_prod_1[dw_prod_liv_1.getrow()]
		ii_livello_attuale = 2
		is_prod_liv_1 = ls_codice
		dw_prod_liv_1.hide()
		ll_return = dw_prod_liv_2.retrieve(s_cs_xx.cod_azienda, &
						            			  is_prod_liv_1 )
		if ll_return = 0 or isnull(ll_return) then
			ll_return = dw_prod_liv_prod.retrieve(s_cs_xx.cod_azienda, &
														  	  is_prod_liv_1, &
															  "%", &
															  "%", &
															  "%", &
															  "%"  )
			ib_livello_prodotti = true
			dw_prod_liv_prod.show()
		else
			dw_prod_liv_2.show()
		end if
	case 2
		ls_descrizione = dw_prod_liv_2.object.des_livello[dw_prod_liv_2.getrow()]
		ls_desc_agg = dw_prod_liv_2.object.des_agg_livello[dw_prod_liv_2.getrow()]
		ls_codice = dw_prod_liv_2.object.cod_livello_prod_2[dw_prod_liv_2.getrow()]
		ii_livello_attuale = 3
		is_prod_liv_2 = ls_codice
		dw_prod_liv_2.hide()
		ll_return = dw_prod_liv_3.retrieve(s_cs_xx.cod_azienda, &
									 				  is_prod_liv_1, &
									  				  is_prod_liv_2 )
		if ll_return = 0 or isnull(ll_return) then
			ll_return = dw_prod_liv_prod.retrieve(s_cs_xx.cod_azienda, &
														  	  is_prod_liv_1, &
															  is_prod_liv_2, &
															  "%", &
															  "%", &
															  "%"  )
			ib_livello_prodotti = true
			dw_prod_liv_prod.show()
		else
			dw_prod_liv_3.show()
		end if
	case 3
		ls_descrizione = dw_prod_liv_3.object.des_livello[dw_prod_liv_3.getrow()]
		ls_desc_agg = dw_prod_liv_3.object.des_agg_livello[dw_prod_liv_3.getrow()]
		ls_codice = dw_prod_liv_3.object.cod_livello_prod_3[dw_prod_liv_3.getrow()]
		ii_livello_attuale = 4
		is_prod_liv_3 = ls_codice
		dw_prod_liv_3.hide()
		ll_return = dw_prod_liv_4.retrieve(s_cs_xx.cod_azienda, &
													  is_prod_liv_1, &
													  is_prod_liv_2, &
													  is_prod_liv_3 )
		if ll_return = 0 or isnull(ll_return) then
			ll_return = dw_prod_liv_prod.retrieve(s_cs_xx.cod_azienda, &
														  	  is_prod_liv_1, &
															  is_prod_liv_2, &
															  is_prod_liv_3, &
															  "%", &
															  "%"  )
			ib_livello_prodotti = true
			dw_prod_liv_prod.show()
		else
			dw_prod_liv_4.show()
		end if
	case 4
		ls_descrizione = dw_prod_liv_4.object.des_livello[dw_prod_liv_4.getrow()]
		ls_desc_agg = dw_prod_liv_4.object.des_agg_livello[dw_prod_liv_4.getrow()]
		ls_codice = dw_prod_liv_4.object.cod_livello_prod_4[dw_prod_liv_4.getrow()]
		ii_livello_attuale = 5
		is_prod_liv_4 = ls_codice
		dw_prod_liv_4.hide()
		ll_return = dw_prod_liv_5.retrieve(s_cs_xx.cod_azienda, &
													  is_prod_liv_1, &
													  is_prod_liv_2, &
													  is_prod_liv_3, &
													  is_prod_liv_4 )
		if ll_return = 0 or isnull(ll_return) then
			ll_return = dw_prod_liv_prod.retrieve(s_cs_xx.cod_azienda, &
														  	  is_prod_liv_1, &
															  is_prod_liv_2, &
															  is_prod_liv_3, &
															  is_prod_liv_4, &
															  "%"  )
			ib_livello_prodotti = true
			dw_prod_liv_prod.show()
		else
			dw_prod_liv_5.show()
		end if
	case 5
		ls_descrizione = dw_prod_liv_5.object.des_livello[dw_prod_liv_5.getrow()]
		ls_desc_agg = dw_prod_liv_5.object.des_agg_livello[dw_prod_liv_5.getrow()]
		ls_codice = dw_prod_liv_5.object.cod_livello_prod_5[dw_prod_liv_5.getrow()]
		ii_livello_attuale = 6
		is_prod_liv_5 = ls_codice
		dw_prod_liv_5.hide()
		dw_prod_liv_prod.retrieve(s_cs_xx.cod_azienda, &
									  is_prod_liv_1, &
									  is_prod_liv_2, &
									  is_prod_liv_3, &
									  is_prod_liv_4, &
									  is_prod_liv_5 )
		ib_livello_prodotti = true
		dw_prod_liv_prod.show()
		
end choose
// ---------------------  aggiorno indicazione livello -------------------------------------

if ib_livello_prodotti then
	st_livello_attuale.text = "Dettaglio Prodotti"
else	
	choose case ii_livello_attuale
		case 1
			st_livello_attuale.text = is_label_liv_1
		case 2
			st_livello_attuale.text = is_label_liv_2
		case 3
			st_livello_attuale.text = is_label_liv_3
		case 4
			st_livello_attuale.text = is_label_liv_4
		case 5
			st_livello_attuale.text = is_label_liv_5
	end choose		
end if

return 0
end function

on w_prod_dw.destroy
destroy(this.pb_restituisci)
destroy(this.st_livello_attuale)
destroy(this.pb_return)
destroy(this.pb_close)
destroy(this.tab_liv_prod)
destroy(this.gb_2)
destroy(this.dw_prod_liv_prod)
destroy(this.dw_prod_liv_5)
destroy(this.dw_prod_liv_4)
destroy(this.dw_prod_liv_3)
destroy(this.dw_prod_liv_2)
destroy(this.dw_prod_liv_1)
end on

event open;ii_livello_attuale = 1
SELECT con_magazzino.label_livello_prod_1,   
		 con_magazzino.label_livello_prod_2,   
		 con_magazzino.label_livello_prod_3,   
		 con_magazzino.label_livello_prod_4,   
		 con_magazzino.label_livello_prod_5  
 INTO :is_label_liv_1,   
		:is_label_liv_2,   
		:is_label_liv_3,   
		:is_label_liv_4,   
		:is_label_liv_5  
 FROM con_magazzino  
WHERE con_magazzino.cod_azienda = :s_cs_xx.cod_azienda   ;

tab_liv_prod.tabpage_immagine.dw_lista_blob.SetTransObject(SQLCA)
dw_prod_liv_1.SetTransObject(SQLCA)
dw_prod_liv_2.SetTransObject(SQLCA)
dw_prod_liv_3.SetTransObject(SQLCA)
dw_prod_liv_4.SetTransObject(SQLCA)
dw_prod_liv_5.SetTransObject(SQLCA)
dw_prod_liv_prod.SetTransObject(SQLCA)

Post Event ue_postopen()

end event

event resize;//lv_prod.Resize(newwidth - (2 * ii_WindowBorder), newheight - (lv_prod.Y + ii_WindowBorder))
//
end event

event close;//Destroy ids_Products

//If IsValid(w_update_prod) Then Close(w_update_prod)
close(this)
end event

on w_prod_dw.create
this.pb_restituisci=create pb_restituisci
this.st_livello_attuale=create st_livello_attuale
this.pb_return=create pb_return
this.pb_close=create pb_close
this.tab_liv_prod=create tab_liv_prod
this.gb_2=create gb_2
this.dw_prod_liv_prod=create dw_prod_liv_prod
this.dw_prod_liv_5=create dw_prod_liv_5
this.dw_prod_liv_4=create dw_prod_liv_4
this.dw_prod_liv_3=create dw_prod_liv_3
this.dw_prod_liv_2=create dw_prod_liv_2
this.dw_prod_liv_1=create dw_prod_liv_1
this.Control[]={ this.pb_restituisci,&
this.st_livello_attuale,&
this.pb_return,&
this.pb_close,&
this.tab_liv_prod,&
this.gb_2,&
this.dw_prod_liv_prod,&
this.dw_prod_liv_5,&
this.dw_prod_liv_4,&
this.dw_prod_liv_3,&
this.dw_prod_liv_2,&
this.dw_prod_liv_1}
end on

event deactivate;this.hide()
end event

type pb_restituisci from picturebutton within w_prod_dw
event clicked pbm_bnclicked
int X=161
int Y=41
int Width=101
int Height=85
int TabOrder=40
string PictureName="\cs_xx\risorse\ritorna.bmp"
string DisabledName="\cs_xx\risorse\dritorna.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_prodotto

if dw_prod_liv_prod.getrow() > 0 then
	ls_prodotto = dw_prod_liv_prod.object.cod_prodotto[dw_prod_liv_prod.getrow()]
	if (ib_livello_prodotti) and (len(ls_prodotto) > 0)  then
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_prodotto)
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
		parent.hide()
	end if	
end if
end event

type st_livello_attuale from statictext within w_prod_dw
int X=846
int Y=41
int Width=686
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type pb_return from picturebutton within w_prod_dw
event clicked pbm_bnclicked
int X=343
int Y=41
int Width=101
int Height=85
int TabOrder=10
string PictureName="\cs_xx\risorse\ritorno1.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if ii_livello_attuale > 1 then
	ii_livello_attuale = ii_livello_attuale -1
//	wf_agg_livello()
end if	
if ib_livello_prodotti then
	ib_livello_prodotti = false
	tab_liv_prod.tabpage_immagine.dw_lista_blob.reset()
end if	
choose case ii_livello_attuale
	case 1
		st_livello_attuale.text = is_label_liv_1
		dw_prod_liv_1.show()
	case 2
		st_livello_attuale.text = is_label_liv_2
		dw_prod_liv_2.show()
	case 3
		st_livello_attuale.text = is_label_liv_3
		dw_prod_liv_3.show()
	case 4
		st_livello_attuale.text = is_label_liv_4
		dw_prod_liv_4.show()
	case 5
		st_livello_attuale.text = is_label_liv_5
		dw_prod_liv_5.show()
end choose
end event

type pb_close from picturebutton within w_prod_dw
int X=46
int Y=41
int Width=101
int Height=85
int TabOrder=20
string PictureName="\cs_xx\risorse\esc.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;parent.triggerevent("close")
end event

type tab_liv_prod from tab within w_prod_dw
event create ( )
event destroy ( )
int X=1601
int Y=21
int Width=1372
int Height=1461
int TabOrder=100
boolean FixedWidth=true
boolean RaggedRight=true
Alignment Alignment=Center!
int SelectedTab=1
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_dettagli tabpage_dettagli
tabpage_immagine tabpage_immagine
end type

on tab_liv_prod.create
this.tabpage_dettagli=create tabpage_dettagli
this.tabpage_immagine=create tabpage_immagine
this.Control[]={ this.tabpage_dettagli,&
this.tabpage_immagine}
end on

on tab_liv_prod.destroy
destroy(this.tabpage_dettagli)
destroy(this.tabpage_immagine)
end on

type tabpage_dettagli from userobject within tab_liv_prod
int X=19
int Y=105
int Width=1335
int Height=1341
long BackColor=79741120
string Text="Dettagli"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=553648127
mle_note mle_note
st_des_aggiuntiva st_des_aggiuntiva
st_descrizione st_descrizione
st_codice st_codice
st_2 st_2
st_1 st_1
gb_3 gb_3
gb_1 gb_1
end type

on tabpage_dettagli.create
this.mle_note=create mle_note
this.st_des_aggiuntiva=create st_des_aggiuntiva
this.st_descrizione=create st_descrizione
this.st_codice=create st_codice
this.st_2=create st_2
this.st_1=create st_1
this.gb_3=create gb_3
this.gb_1=create gb_1
this.Control[]={ this.mle_note,&
this.st_des_aggiuntiva,&
this.st_descrizione,&
this.st_codice,&
this.st_2,&
this.st_1,&
this.gb_3,&
this.gb_1}
end on

on tabpage_dettagli.destroy
destroy(this.mle_note)
destroy(this.st_des_aggiuntiva)
destroy(this.st_descrizione)
destroy(this.st_codice)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.gb_3)
destroy(this.gb_1)
end on

type mle_note from multilineedit within tabpage_dettagli
int X=74
int Y=877
int Width=1166
int Height=421
int TabOrder=13
boolean Border=false
boolean AutoHScroll=true
boolean AutoVScroll=true
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_des_aggiuntiva from statictext within tabpage_dettagli
int X=51
int Y=257
int Width=1212
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_descrizione from statictext within tabpage_dettagli
int X=394
int Y=177
int Width=869
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_codice from statictext within tabpage_dettagli
int X=394
int Y=97
int Width=412
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within tabpage_dettagli
int X=51
int Y=177
int Width=339
int Height=77
boolean Enabled=false
string Text="Descrizione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within tabpage_dettagli
int X=142
int Y=97
int Width=247
int Height=77
boolean Enabled=false
string Text="Codice:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_3 from groupbox within tabpage_dettagli
int X=28
int Y=17
int Width=1249
int Height=761
int TabOrder=61
string Text="Dati Livello / Prodotto"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within tabpage_dettagli
int X=28
int Y=797
int Width=1258
int Height=521
int TabOrder=12
string Text="Annotazioni"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type tabpage_immagine from userobject within tab_liv_prod
event create ( )
event destroy ( )
int X=19
int Y=105
int Width=1335
int Height=1341
long BackColor=79741120
string Text="Immagine"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_lista_blob dw_lista_blob
p_immagine p_immagine
end type

on tabpage_immagine.create
this.dw_lista_blob=create dw_lista_blob
this.p_immagine=create p_immagine
this.Control[]={ this.dw_lista_blob,&
this.p_immagine}
end on

on tabpage_immagine.destroy
destroy(this.dw_lista_blob)
destroy(this.p_immagine)
end on

type dw_lista_blob from datawindow within tabpage_immagine
event ue_retrieve pbm_custom01
int X=5
int Y=937
int Width=1326
int Height=401
int TabOrder=14
string DataObject="d_lista_blob"
boolean TitleBar=true
string Title="Lista Immagini Associate al Singolo Prodotto"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event retrieveend;if this.rowcount() > 0 then
	this.setrow(1)
	postevent("rowfocuschanged")
end if	
end event

event rowfocuschanged;string ls_str, ls_path

if this.rowcount() > 0 then
	ls_str = this.getitemstring(this.getrow(),"path_documento")
	SetRowFocusIndicator(FocusRect!) 
	if not(isnull(ls_str)) and len(ls_str) > 0 then
		ls_path = f_path_immagini() + "\" + ls_str
		if not isnull(ls_path) then 
			tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
		else
			tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
		end if
//		tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_str
	end if
end if

end event

type p_immagine from picture within tabpage_immagine
int X=5
int Y=37
int Width=1303
int Height=881
boolean Border=true
boolean FocusRectangle=false
end type

type gb_2 from groupbox within w_prod_dw
int X=23
int Width=1555
int Height=141
int TabOrder=90
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_prod_liv_prod from datawindow within w_prod_dw
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=41
string DataObject="d_prod_liv_prod"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path
long ll_row

this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_prodotto[row]
ls_codice = this.object.cod_prodotto[row]
ls_desc_agg = ""
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
if ib_livello_prodotti then
	ll_row = tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
	if ll_row > 0 then
		ls_path = f_path_immagini() + "\" + tab_liv_prod.tabpage_immagine.dw_lista_blob.getitemstring(1,"path_documento")
		if not isnull(ls_path) then 
			tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
		else
			tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
		end if
	else
		tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
	end if	
end if

this.setrowfocusindicator(hand!,0,50)

end event

type dw_prod_liv_5 from datawindow within w_prod_dw
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=50
string DataObject="d_prod_liv_5"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path


this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_livello[row]
ls_path = f_path_immagini() + "\" +this.object.path_documento[row]
ls_codice = this.object.cod_livello_prod_5[row]
ls_desc_agg = this.object.des_agg_livello[row]
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
if not isnull(ls_path) then 
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
else
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
end if
if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

event doubleclicked;String ls_codice, ls_descrizione, ls_desc_agg

if not(ib_livello_prodotti) then
	if this.getrow() > 0 then
		wf_agg_livello()
	end if
end if


end event

type dw_prod_liv_4 from datawindow within w_prod_dw
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=60
string DataObject="d_prod_liv_4"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path


this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_livello[row]
ls_path = f_path_immagini() + "\" +this.object.path_documento[row]
ls_codice = this.object.cod_livello_prod_4[row]
ls_desc_agg = this.object.des_agg_livello[row]
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
if not isnull(ls_path) then 
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
else
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
end if
if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

event doubleclicked;String ls_codice, ls_descrizione, ls_desc_agg

if not(ib_livello_prodotti) then
	if this.getrow() > 0 then
		wf_agg_livello()
	end if
end if


end event

type dw_prod_liv_3 from datawindow within w_prod_dw
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=70
string DataObject="d_prod_liv_3"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path


this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_livello[row]
ls_path = f_path_immagini() + "\" +this.object.path_documento[row]
ls_codice = this.object.cod_livello_prod_3[row]
ls_desc_agg = this.object.des_agg_livello[row]
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
if not isnull(ls_path) then 
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
else
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
end if
if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

event doubleclicked;String ls_codice, ls_descrizione, ls_desc_agg

if not(ib_livello_prodotti) then
	if this.getrow() > 0 then
		wf_agg_livello()
	end if
end if


end event

type dw_prod_liv_2 from datawindow within w_prod_dw
event clicked pbm_dwnlbuttonclk
event doubleclicked pbm_dwnlbuttondblclk
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=80
string DataObject="d_prod_liv_2"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path


this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_livello[row]
ls_path = f_path_immagini() + "\" +this.object.path_documento[row]
ls_codice = this.object.cod_livello_prod_2[row]
ls_desc_agg = this.object.des_agg_livello[row]
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
if not isnull(ls_path) then 
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
else
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
end if
if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

event doubleclicked;String ls_codice, ls_descrizione, ls_desc_agg

if not(ib_livello_prodotti) then
	if this.getrow() > 0 then
		wf_agg_livello()
	end if
end if


end event

type dw_prod_liv_1 from datawindow within w_prod_dw
int X=23
int Y=161
int Width=1555
int Height=1321
int TabOrder=30
string DataObject="d_prod_liv_1"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;string ls_codice, ls_descrizione, ls_desc_agg, ls_path

this.setrowfocusindicator(hand!,0,50)
ls_descrizione = this.object.des_livello[row]
ls_path = f_path_immagini() + "\" +this.object.path_documento[row]
ls_codice = this.object.cod_livello_prod_1[row]
ls_desc_agg = this.object.des_agg_livello[row]
tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice

if not isnull(ls_path) then 
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
else
	tab_liv_prod.tabpage_immagine.p_immagine.PictureName = " "
end if

if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

event doubleclicked;String ls_codice, ls_descrizione, ls_desc_agg

if not(ib_livello_prodotti) then
	if this.getrow() > 0 then
		wf_agg_livello()
	end if
end if


end event

