﻿$PBExportHeader$w_prod_listview.srw
$PBExportComments$Finestra Ricerca Prodotti Livelli
forward
global type w_prod_listview from Window
end type
type pb_restituisci from picturebutton within w_prod_listview
end type
type st_livello_attuale from statictext within w_prod_listview
end type
type pb_return from picturebutton within w_prod_listview
end type
type pb_list from picturebutton within w_prod_listview
end type
type pb_small from picturebutton within w_prod_listview
end type
type pb_large from picturebutton within w_prod_listview
end type
type pb_close from picturebutton within w_prod_listview
end type
type tab_liv_prod from tab within w_prod_listview
end type
type tabpage_dettagli from userobject within tab_liv_prod
end type
type mle_note from multilineedit within tabpage_dettagli
end type
type st_des_aggiuntiva from statictext within tabpage_dettagli
end type
type st_descrizione from statictext within tabpage_dettagli
end type
type st_codice from statictext within tabpage_dettagli
end type
type st_2 from statictext within tabpage_dettagli
end type
type st_1 from statictext within tabpage_dettagli
end type
type gb_3 from groupbox within tabpage_dettagli
end type
type gb_1 from groupbox within tabpage_dettagli
end type
type tabpage_immagine from userobject within tab_liv_prod
end type
type dw_lista_blob from datawindow within tabpage_immagine
end type
type p_immagine from picture within tabpage_immagine
end type
type lv_prod from listview within w_prod_listview
end type
type gb_2 from groupbox within w_prod_listview
end type
type tabpage_dettagli from userobject within tab_liv_prod
mle_note mle_note
st_des_aggiuntiva st_des_aggiuntiva
st_descrizione st_descrizione
st_codice st_codice
st_2 st_2
st_1 st_1
gb_3 gb_3
gb_1 gb_1
end type
type tabpage_immagine from userobject within tab_liv_prod
dw_lista_blob dw_lista_blob
p_immagine p_immagine
end type
type tab_liv_prod from tab within w_prod_listview
tabpage_dettagli tabpage_dettagli
tabpage_immagine tabpage_immagine
end type
end forward

global type w_prod_listview from Window
int X=595
int Y=989
int Width=3283
int Height=1589
boolean TitleBar=true
string Title="Catalogo Prodotti"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
event ue_refresh_items ( )
event ue_postopen ( )
event ue_arrangeicons ( )
event ue_chglistview ( string as_newview )
event ue_edititem ( )
pb_restituisci pb_restituisci
st_livello_attuale st_livello_attuale
pb_return pb_return
pb_list pb_list
pb_small pb_small
pb_large pb_large
pb_close pb_close
tab_liv_prod tab_liv_prod
lv_prod lv_prod
gb_2 gb_2
end type
global w_prod_listview w_prod_listview

type variables
DataStore	ids_Products
Integer	ii_SortCol = 2, ii_WindowBorder = 15
grsorttype	igrs_Sort = ascending!
integer ii_livello_attuale
string is_prod_liv_1, is_prod_liv_2, is_prod_liv_3
string is_prod_liv_4, is_prod_liv_5
string is_label_liv_1, is_label_liv_2, is_label_liv_3
string is_label_liv_4, is_label_liv_5
boolean ib_livello_prodotti = FALSE
long il_index
end variables

forward prototypes
public function string of_getcurrentstyle ()
public function string wf_select_prodotti ()
end prototypes

event ue_refresh_items;Integer			li_Rows, li_Cnt
String			ls_Desc, ls_path_immagine, ls_Id, ls_des_agg, ls_descrizione, ls_Qty, ls_Price, ls_sql
string			ls_des[],ls_cod[], ls_path[],ls_note[], ls_dag[]
Double			ldb_Price
ListViewItem	llvi_Item


SetPointer(Hourglass!)
lv_prod.DeleteItems()

declare cu_livelli dynamic cursor for sqlsa;

choose case ii_livello_attuale
	case 1
     ls_sql = "SELECT tab_livelli_prod_1.cod_livello_prod_1, tab_livelli_prod_1.des_livello, " + &
	           "tab_livelli_prod_1.path_documento, tab_livelli_prod_1.note, tab_livelli_prod_1.des_agg_livello " + &
			     "FROM tab_livelli_prod_1 " +&
	           "WHERE tab_livelli_prod_1.cod_azienda = '" + s_cs_xx.cod_azienda + "'"
	case 2 
     ls_sql = "SELECT tab_livelli_prod_2.cod_livello_prod_2, tab_livelli_prod_2.des_livello, " + &
	           "tab_livelli_prod_2.path_documento, tab_livelli_prod_2.note, tab_livelli_prod_2.des_agg_livello " + &
			     "FROM tab_livelli_prod_2 " +&
	           "WHERE (tab_livelli_prod_2.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
				  		  "(tab_livelli_prod_2.cod_livello_prod_1 = '" + is_prod_liv_1 + "') "
	case 3 
     ls_sql = "SELECT tab_livelli_prod_3.cod_livello_prod_3, tab_livelli_prod_3.des_livello, " + &
	           "tab_livelli_prod_3.path_documento, tab_livelli_prod_3.note, tab_livelli_prod_3.des_agg_livello " + &
			     "FROM tab_livelli_prod_3 " +&
	           "WHERE (tab_livelli_prod_3.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
				  		  "(tab_livelli_prod_3.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
				  		  "(tab_livelli_prod_3.cod_livello_prod_2  = '" + is_prod_liv_2 + "') "
	case 4 
     ls_sql = "SELECT tab_livelli_prod_4.cod_livello_prod_4, tab_livelli_prod_4.des_livello, " + &
	           "tab_livelli_prod_4.path_documento, tab_livelli_prod_4.note, tab_livelli_prod_4.des_agg_livello " + &
			     "FROM tab_livelli_prod_4 " +&
	           "WHERE (tab_livelli_prod_4.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
				  		  "(tab_livelli_prod_4.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
				  		  "(tab_livelli_prod_4.cod_livello_prod_2  = '" + is_prod_liv_2 + "') and " + &
				  		  "(tab_livelli_prod_4.cod_livello_prod_3  = '" + is_prod_liv_3 + "') "
	case 5
     ls_sql = "SELECT tab_livelli_prod_5.cod_livello_prod_5, tab_livelli_prod_5.des_livello, " + &
	           "tab_livelli_prod_5.path_documento, tab_livelli_prod_5.note, tab_livelli_prod_5.des_agg_livello " + &
			     "FROM tab_livelli_prod_5 " +&
	           "WHERE (tab_livelli_prod_5.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
				  		  "(tab_livelli_prod_5.cod_livello_prod_1  = '" + is_prod_liv_1 + "') and " + &
				  		  "(tab_livelli_prod_5.cod_livello_prod_2  = '" + is_prod_liv_2 + "') and " + &
				  		  "(tab_livelli_prod_5.cod_livello_prod_3  = '" + is_prod_liv_3 + "') and " + &
				  		  "(tab_livelli_prod_5.cod_livello_prod_4  = '" + is_prod_liv_4 + "') "
end choose

prepare sqlsa from :ls_sql;
open dynamic cu_livelli;

li_cnt = 1
do while 1=1
   fetch cu_livelli into :ls_cod[li_cnt], :ls_des[li_cnt], :ls_path[li_cnt], :ls_note[li_cnt], :ls_dag[li_cnt];
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	li_cnt = li_cnt + 1
loop
close cu_livelli;


// --------------------  uscito dal cursore: non esistono altri livelli -------------------
// ----------------------------  allora ricerco i prodotti  -------------------------------
if li_cnt = 1 then
declare cu_prodotti dynamic cursor for sqlsa;

		ls_sql = wf_select_prodotti()
		
		prepare sqlsa from :ls_sql;
		open dynamic cu_prodotti;
		li_cnt = 1
		do while 1=1
   		fetch cu_prodotti into :ls_cod[li_cnt], :ls_des[li_cnt];
			setnull(ls_path[li_cnt])
			setnull(ls_dag[li_cnt])
			setnull(ls_note[li_cnt])
		   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
			li_cnt = li_cnt + 1
		loop
		close cu_prodotti;
		ib_livello_prodotti = true
end if
// --------------------  carico dati livello sottostante -----------------------------------
for li_cnt = 1 to upperbound(ls_cod) - 1
		if isnull(ls_des[li_cnt]) then ls_des[li_cnt] = ""
		if isnull(ls_note[li_cnt]) then ls_note[li_cnt] = ""
		if isnull(ls_dag[li_cnt]) then ls_dag[li_cnt] = ""
		if isnull(ls_path[li_cnt]) then ls_path[li_cnt] = "cartel1.bmp"
		llvi_Item.PictureIndex = lv_prod.AddLargePicture(f_path_immagini() + "\" + ls_path[li_cnt])
		lv_prod.AddSmallPicture(f_path_immagini() + "\" + ls_path[li_cnt])
		llvi_Item.Data = f_path_immagini() + "\" + ls_path[li_cnt]
		ls_Desc = ls_cod[li_cnt] + " - " + ls_des[li_cnt]
		ls_path_immagine = f_path_immagini() + "\" + ls_path[li_cnt]
		ls_Id = ls_cod[li_cnt]
		ls_des_agg = ls_dag[li_cnt]
		ls_descrizione = ls_des[li_cnt]
		llvi_Item.Label = ls_Desc + "~t" + ls_path_immagine + "~t" + ls_Id + "~t" + ls_des_agg + "~t" + &
								ls_descrizione
		lv_prod.AddItem(llvi_Item)
      pcca.mdi_frame.setmicrohelp("Indice:" + string(li_cnt) )
		lv_prod.insertItem(li_cnt, llvi_Item)
next

// ---------------------  aggiorno indicazione livello -------------------------------------

if ib_livello_prodotti then
	st_livello_attuale.text = "Dettaglio Prodotti"
else	
	choose case ii_livello_attuale
		case 1
			st_livello_attuale.text = is_label_liv_1
		case 2
			st_livello_attuale.text = is_label_liv_2
		case 3
			st_livello_attuale.text = is_label_liv_3
		case 4
			st_livello_attuale.text = is_label_liv_4
		case 5
			st_livello_attuale.text = is_label_liv_5
	end choose		
end if


end event

event ue_postopen;SetPointer(Hourglass!)

// Using a datastore to retrieve data from the database
ids_Products = Create DataStore
ids_Products.DataObject = "d_products"

// Set the large and small picture sizes
lv_prod.LargePictureWidth = 120
lv_prod.LargePictureHeight = 120
lv_prod.SmallPictureWidth = 24
lv_prod.SmallPictureHeight = 24

// Add Columns used in the listview
lv_prod.AddColumn("Product", Left!, 525)
lv_prod.AddColumn("Name", Left!, 350)
lv_prod.AddColumn("ID", Left!, 150)
lv_prod.AddColumn("Size", Left!, 400)
lv_prod.AddColumn("Color", Left!, 225)
lv_prod.AddColumn("Quantity", Right!, 225)
lv_prod.AddColumn("Unit Price", Right!, 250)

Post Event ue_refresh_items()

end event

event ue_arrangeicons;lv_prod.Arrange()

end event

event ue_chglistview;Choose Case as_NewView
	Case "LargeIcon"
		lv_prod.View = ListViewLargeIcon!
	Case "SmallIcon"
		lv_prod.View = ListViewSmallIcon!
	Case "List"
		lv_prod.View = ListViewList!
	Case "Report"
		lv_prod.View = ListViewReport!
End Choose

end event

event ue_edititem;Integer			li_Index
ListViewItem	llvi_Current

li_Index = lv_prod.SelectedIndex()
If li_Index <= 0 Then Return

lv_prod.Post Event DoubleClicked(li_Index)

end event

public function string of_getcurrentstyle ();// Return the style of the ListView
Choose Case lv_prod.View
	Case ListViewLargeIcon!
		Return "LargeIcon"
	Case ListViewSmallIcon!
		Return "SmallIcon"
	Case ListViewList!
		Return "List"
	Case ListViewReport!
		Return "Report"
End Choose

end function

public function string wf_select_prodotti ();string ls_sql
choose case ii_livello_attuale
	case 2
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"( (anag_prodotti.cod_livello_prod_2 like '%' ) or ( anag_prodotti.cod_livello_prod_2 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_3 like '%' ) or ( anag_prodotti.cod_livello_prod_3 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 3
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_3 like '%' ) or ( anag_prodotti.cod_livello_prod_3 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 4
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_4 like '%' ) or ( anag_prodotti.cod_livello_prod_4 is null )) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%' ) or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 5
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_4 like '" + is_prod_liv_4 + "' ) AND  " + &
							"( (anag_prodotti.cod_livello_prod_5 like '%')  or ( anag_prodotti.cod_livello_prod_5 is null ) )"
	case 6
		ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto " + &  
					"FROM anag_prodotti " + &  
					"WHERE (anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_1 like '" + is_prod_liv_1 + "') AND  " + &
							"(anag_prodotti.cod_livello_prod_2 like '" + is_prod_liv_2 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_3 like '" + is_prod_liv_3 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_4 like '" + is_prod_liv_4 + "' ) AND  " + &
							"(anag_prodotti.cod_livello_prod_5 like '" + is_prod_liv_5 + "' )"
end choose

return ls_sql
end function

on w_prod_listview.destroy
destroy(this.pb_restituisci)
destroy(this.st_livello_attuale)
destroy(this.pb_return)
destroy(this.pb_list)
destroy(this.pb_small)
destroy(this.pb_large)
destroy(this.pb_close)
destroy(this.tab_liv_prod)
destroy(this.lv_prod)
destroy(this.gb_2)
end on

event open;ii_livello_attuale = 1
SELECT con_magazzino.label_livello_prod_1,   
		 con_magazzino.label_livello_prod_2,   
		 con_magazzino.label_livello_prod_3,   
		 con_magazzino.label_livello_prod_4,   
		 con_magazzino.label_livello_prod_5  
 INTO :is_label_liv_1,   
		:is_label_liv_2,   
		:is_label_liv_3,   
		:is_label_liv_4,   
		:is_label_liv_5  
 FROM con_magazzino  
WHERE con_magazzino.cod_azienda = :s_cs_xx.cod_azienda   ;

tab_liv_prod.tabpage_immagine.dw_lista_blob.SetTransObject(SQLCA)


Post Event ue_postopen()

end event

event resize;//lv_prod.Resize(newwidth - (2 * ii_WindowBorder), newheight - (lv_prod.Y + ii_WindowBorder))
//
end event

event close;//Destroy ids_Products

//If IsValid(w_update_prod) Then Close(w_update_prod)
close(this)
end event

on w_prod_listview.create
this.pb_restituisci=create pb_restituisci
this.st_livello_attuale=create st_livello_attuale
this.pb_return=create pb_return
this.pb_list=create pb_list
this.pb_small=create pb_small
this.pb_large=create pb_large
this.pb_close=create pb_close
this.tab_liv_prod=create tab_liv_prod
this.lv_prod=create lv_prod
this.gb_2=create gb_2
this.Control[]={ this.pb_restituisci,&
this.st_livello_attuale,&
this.pb_return,&
this.pb_list,&
this.pb_small,&
this.pb_large,&
this.pb_close,&
this.tab_liv_prod,&
this.lv_prod,&
this.gb_2}
end on

event deactivate;this.hide()
end event

type pb_restituisci from picturebutton within w_prod_listview
event clicked pbm_bnclicked
int X=161
int Y=41
int Width=101
int Height=85
int TabOrder=51
string PictureName="\cs_xx\risorse\ritorna.bmp"
string DisabledName="\cs_xx\risorse\dritorna.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_prodotto

lv_prod.GetItem(il_index, 3, ls_prodotto)
if (ib_livello_prodotti) and (len(ls_prodotto) > 0)  then
//	lv_prod.GetItem(il_index, 3, ls_prodotto)
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_prodotto)
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
//	parent.triggerevent("close")
	parent.hide()
end if	


end event

type st_livello_attuale from statictext within w_prod_listview
int X=846
int Y=41
int Width=686
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type pb_return from picturebutton within w_prod_listview
event clicked pbm_bnclicked
int X=343
int Y=41
int Width=101
int Height=85
int TabOrder=30
string PictureName="\cs_xx\risorse\ritorno1.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if ii_livello_attuale > 1 then
	ii_livello_attuale = ii_livello_attuale -1
	parent.triggerevent("ue_refresh_items")
end if	
if ib_livello_prodotti then
	ib_livello_prodotti = false
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, "")
end if	
choose case ii_livello_attuale
	case 1
		st_livello_attuale.text = is_label_liv_1
	case 2
		st_livello_attuale.text = is_label_liv_2
	case 3
		st_livello_attuale.text = is_label_liv_3
	case 4
		st_livello_attuale.text = is_label_liv_4
	case 5
		st_livello_attuale.text = is_label_liv_5
end choose
end event

type pb_list from picturebutton within w_prod_listview
int X=732
int Y=41
int Width=97
int Height=81
int TabOrder=10
string PictureName="\cs_xx\risorse\list1.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;lv_prod.view =listviewlist!
end event

type pb_small from picturebutton within w_prod_listview
int X=618
int Y=41
int Width=97
int Height=81
int TabOrder=20
string PictureName="\cs_xx\risorse\small1.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;lv_prod.view =listviewsmallicon!
end event

type pb_large from picturebutton within w_prod_listview
int X=508
int Y=41
int Width=92
int Height=81
int TabOrder=40
string PictureName="\cs_xx\risorse\large1.bmp"
Alignment HTextAlign=Left!
VTextAlign VTextAlign=Top!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;lv_prod.view =listviewlargeicon!
end event

type pb_close from picturebutton within w_prod_listview
int X=46
int Y=41
int Width=101
int Height=85
int TabOrder=50
string PictureName="\cs_xx\risorse\esc.bmp"
Alignment HTextAlign=Right!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;parent.triggerevent("close")
end event

type tab_liv_prod from tab within w_prod_listview
event create ( )
event destroy ( )
int X=1875
int Y=21
int Width=1372
int Height=1461
int TabOrder=80
boolean FixedWidth=true
boolean RaggedRight=true
Alignment Alignment=Center!
int SelectedTab=1
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_dettagli tabpage_dettagli
tabpage_immagine tabpage_immagine
end type

on tab_liv_prod.create
this.tabpage_dettagli=create tabpage_dettagli
this.tabpage_immagine=create tabpage_immagine
this.Control[]={ this.tabpage_dettagli,&
this.tabpage_immagine}
end on

on tab_liv_prod.destroy
destroy(this.tabpage_dettagli)
destroy(this.tabpage_immagine)
end on

type tabpage_dettagli from userobject within tab_liv_prod
int X=19
int Y=105
int Width=1335
int Height=1341
long BackColor=79741120
string Text="Dettagli"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=553648127
mle_note mle_note
st_des_aggiuntiva st_des_aggiuntiva
st_descrizione st_descrizione
st_codice st_codice
st_2 st_2
st_1 st_1
gb_3 gb_3
gb_1 gb_1
end type

on tabpage_dettagli.create
this.mle_note=create mle_note
this.st_des_aggiuntiva=create st_des_aggiuntiva
this.st_descrizione=create st_descrizione
this.st_codice=create st_codice
this.st_2=create st_2
this.st_1=create st_1
this.gb_3=create gb_3
this.gb_1=create gb_1
this.Control[]={ this.mle_note,&
this.st_des_aggiuntiva,&
this.st_descrizione,&
this.st_codice,&
this.st_2,&
this.st_1,&
this.gb_3,&
this.gb_1}
end on

on tabpage_dettagli.destroy
destroy(this.mle_note)
destroy(this.st_des_aggiuntiva)
destroy(this.st_descrizione)
destroy(this.st_codice)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.gb_3)
destroy(this.gb_1)
end on

type mle_note from multilineedit within tabpage_dettagli
int X=74
int Y=877
int Width=1166
int Height=421
int TabOrder=13
boolean Border=false
boolean AutoHScroll=true
boolean AutoVScroll=true
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_des_aggiuntiva from statictext within tabpage_dettagli
int X=51
int Y=257
int Width=1212
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_descrizione from statictext within tabpage_dettagli
int X=394
int Y=177
int Width=869
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_codice from statictext within tabpage_dettagli
int X=394
int Y=97
int Width=412
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within tabpage_dettagli
int X=51
int Y=177
int Width=339
int Height=77
boolean Enabled=false
string Text="Descrizione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within tabpage_dettagli
int X=142
int Y=97
int Width=247
int Height=77
boolean Enabled=false
string Text="Codice:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_3 from groupbox within tabpage_dettagli
int X=28
int Y=17
int Width=1249
int Height=761
int TabOrder=61
string Text="Dati Livello / Prodotto"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within tabpage_dettagli
int X=28
int Y=797
int Width=1258
int Height=521
int TabOrder=12
string Text="Annotazioni"
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type tabpage_immagine from userobject within tab_liv_prod
event create ( )
event destroy ( )
int X=19
int Y=105
int Width=1335
int Height=1341
long BackColor=79741120
string Text="Immagine"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_lista_blob dw_lista_blob
p_immagine p_immagine
end type

on tabpage_immagine.create
this.dw_lista_blob=create dw_lista_blob
this.p_immagine=create p_immagine
this.Control[]={ this.dw_lista_blob,&
this.p_immagine}
end on

on tabpage_immagine.destroy
destroy(this.dw_lista_blob)
destroy(this.p_immagine)
end on

type dw_lista_blob from datawindow within tabpage_immagine
event ue_retrieve pbm_custom01
int X=5
int Y=937
int Width=1326
int Height=401
int TabOrder=14
string DataObject="d_lista_blob"
boolean TitleBar=true
string Title="Lista Immagini Associate al Singolo Prodotto"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event retrieveend;if this.rowcount() > 0 then
	this.setrow(1)
	postevent("rowfocuschanged")
end if	
end event

event rowfocuschanged;string ls_str

if this.rowcount() > 0 then
	ls_str = this.getitemstring(this.getrow(),"path_documento")
	SetRowFocusIndicator(FocusRect!) 
	if not(isnull(ls_str)) and len(ls_str) > 0 then tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_str
end if
end event

type p_immagine from picture within tabpage_immagine
int X=5
int Y=37
int Width=1303
int Height=881
boolean Border=true
boolean FocusRectangle=false
end type

type lv_prod from listview within w_prod_listview
int X=23
int Y=161
int Width=1829
int Height=1321
int TabOrder=70
boolean DragAuto=true
BorderStyle BorderStyle=StyleLowered!
boolean EditLabels=true
boolean HideSelection=false
boolean ExtendedSelect=true
int LargePictureWidth=32
int LargePictureHeight=32
long LargePictureMaskColor=12632256
int SmallPictureWidth=16
int SmallPictureHeight=16
long SmallPictureMaskColor=12632256
long StatePictureMaskColor=553648127
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event doubleclicked;String			ls_codice, ls_descrizione, ls_desc_agg
ListViewItem	llvi_Item

if ib_livello_prodotti then return

GetItem(index, 1, ls_descrizione)
GetItem(index, 2, ls_desc_agg)
GetItem(index, 3, ls_codice)

If Trim(ls_codice) <> "" Then
	choose case ii_livello_attuale
		case 1
			ii_livello_attuale = 2
			is_prod_liv_1 = ls_codice
		case 2
			ii_livello_attuale = 3
			is_prod_liv_2 = ls_codice
		case 3
			ii_livello_attuale = 4
			is_prod_liv_3 = ls_codice
		case 4
			ii_livello_attuale = 5
			is_prod_liv_4 = ls_codice
		case 5
			ii_livello_attuale = 6
			is_prod_liv_5 = ls_codice
	end choose
	Parent.triggerevent("ue_refresh_items")
End if

end event

event rightclicked;//m_lv_rmb_prod	lm_PopMenu
//
//lm_PopMenu = CREATE m_lv_rmb_prod
//
//If index < 1 Then
//	lm_PopMenu.m_action.m_edititem.Enabled = False
//End If
//
//If This.View = ListViewList! Or This.View = ListViewReport! Then
//	lm_PopMenu.m_action.m_lineupicons.Enabled = False
//End If
//
//lm_PopMenu.m_action.PopMenu(Parent.PointerX(), Parent.PointerY())
//
end event

event columnclick;// Sort the list based on the column clicked.
// If the same column is clicked twice, sort descending.

If column <> ii_SortCol Then
	igrs_Sort = ascending!
	ii_SortCol = column
Else
	If igrs_Sort = ascending! Then
		igrs_Sort = descending!
	Else
		igrs_Sort = ascending!
	End if
End if

// Since all columns are strings, numeric values will not sort
// properly.  So call userdefined sort for Quantity and Unit Price.
// This will execute the "sort" event for each item comparison.

If column < 6 Then
	This.Sort(igrs_Sort, column)
Else
	This.Sort(userdefinedsort!, column)
End If

end event

event sort;// Sort by Quantity or Unit Price.  Convert the data
// to numbers before comparing.

String	ls_Item1, ls_Item2
Double	ldb_Item1, ldb_Item2

GetItem(index1, column, ls_Item1)
GetItem(index2, column, ls_Item2)

If column = 7 Then
	// Strip off the "$"
	ls_Item1 = Right(ls_Item1, (Len(ls_Item1) - 1))
	ls_Item2 = Right(ls_Item2, (Len(ls_Item2) - 1))
End If	

ldb_Item1 = Double(ls_Item1)
ldb_Item2 = Double(ls_Item2)

// This event returns:
// 0 - the two items are equal
// -1 - Item1 should be placed before Item2
// 1 - Item2 should be placed before Item1

If ldb_Item1 = ldb_Item2 Then Return 0

If igrs_Sort = ascending! Then
	If ldb_Item1 < ldb_Item2 Then
		Return -1
	Else
		Return 1
	End If
Else
	If ldb_Item1 < ldb_Item2 Then
		Return 1
	Else
		Return -1
	End If
End if

end event

event clicked;String			ls_codice, ls_descrizione, ls_desc_agg, ls_path
ListViewItem	llvi_Item

il_index = index

GetItem(index, 1, ls_descrizione)
GetItem(index, 2, ls_path)
GetItem(index, 3, ls_codice)
GetItem(index, 4, ls_desc_agg)

tab_liv_prod.tabpage_dettagli.st_descrizione.text = ls_descrizione
tab_liv_prod.tabpage_dettagli.st_des_aggiuntiva.text = ls_desc_agg
tab_liv_prod.tabpage_dettagli.st_codice.text = ls_codice
tab_liv_prod.tabpage_immagine.p_immagine.PictureName = ls_path
if ib_livello_prodotti then
	tab_liv_prod.tabpage_immagine.dw_lista_blob.retrieve(s_cs_xx.cod_azienda, ls_codice)
end if

end event

type gb_2 from groupbox within w_prod_listview
int X=23
int Width=1555
int Height=141
int TabOrder=60
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

