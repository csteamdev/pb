﻿$PBExportHeader$w_prod_note_ricerca.srw
$PBExportComments$Finestra Ricerca Prodotti Note Ricerca
forward
global type w_prod_note_ricerca from w_cs_xx_risposta
end type
type cb_ok from uo_cb_ok within w_prod_note_ricerca
end type
type cb_annulla from uo_cb_close within w_prod_note_ricerca
end type
type dw_prodotti_note_lista from uo_cs_xx_dw within w_prod_note_ricerca
end type
type cb_lingue from uo_cb_ok within w_prod_note_ricerca
end type
end forward

global type w_prod_note_ricerca from w_cs_xx_risposta
int Width=2620
int Height=1249
boolean TitleBar=true
string Title="Ricerca Note Prodotti"
cb_ok cb_ok
cb_annulla cb_annulla
dw_prodotti_note_lista dw_prodotti_note_lista
cb_lingue cb_lingue
end type
global w_prod_note_ricerca w_prod_note_ricerca

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_prodotti_note_lista.set_dw_key("cod_azienda")
dw_prodotti_note_lista.set_dw_key("cod_prodotto")
dw_prodotti_note_lista.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                      c_default, &
                                      c_default)

end on

on w_prod_note_ricerca.create
int iCurrent
call w_cs_xx_risposta::create
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_prodotti_note_lista=create dw_prodotti_note_lista
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_ok
this.Control[iCurrent+2]=cb_annulla
this.Control[iCurrent+3]=dw_prodotti_note_lista
this.Control[iCurrent+4]=cb_lingue
end on

on w_prod_note_ricerca.destroy
call w_cs_xx_risposta::destroy
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_prodotti_note_lista)
destroy(this.cb_lingue)
end on

type cb_ok from uo_cb_ok within w_prod_note_ricerca
int X=2195
int Y=1041
int Width=366
int Height=81
int TabOrder=10
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;call uo_cb_ok::clicked;if dw_prodotti_note_lista.getrow() > 0 then
   s_cs_xx.parametri.parametro_s_2 = dw_prodotti_note_lista.getitemstring(dw_prodotti_note_lista.getrow(),"nota_prodotto")
else
   setnull(s_cs_xx.parametri.parametro_s_2)
end if
close(parent)
end on

type cb_annulla from uo_cb_close within w_prod_note_ricerca
int X=1806
int Y=1041
int Width=366
int Height=81
int TabOrder=20
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_prodotti_note_lista from uo_cs_xx_dw within w_prod_note_ricerca
int X=23
int Y=21
int Width=2538
int Height=1001
int TabOrder=40
string DataObject="d_prodotti_note_lista"
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_prodotto

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

type cb_lingue from uo_cb_ok within w_prod_note_ricerca
int X=1418
int Y=1041
int Width=366
int Height=81
int TabOrder=30
string Text="&Lingue"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;call uo_cb_ok::clicked;if dw_prodotti_note_lista.getrow() > 0 then
   s_cs_xx.parametri.parametro_s_2 = dw_prodotti_note_lista.getitemstring(dw_prodotti_note_lista.getrow(),"cod_nota_prodotto")
   window_open(w_prod_note_lingue_ricerca, 0)
else
   setnull(s_cs_xx.parametri.parametro_s_2)
end if
close(parent)

end on

