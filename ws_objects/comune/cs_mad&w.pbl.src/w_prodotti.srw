﻿$PBExportHeader$w_prodotti.srw
$PBExportComments$Finestra Gestione Prodotti
forward
global type w_prodotti from w_cs_xx_principale
end type
type dw_documenti from uo_drag_prodotti within w_prodotti
end type
type r_1 from rectangle within w_prodotti
end type
type dw_folder from u_folder within w_prodotti
end type
type dw_ricerca from u_dw_search within w_prodotti
end type
type dw_prodotti_lista from uo_cs_xx_dw within w_prodotti
end type
type dw_folder_search from u_folder within w_prodotti
end type
type dw_prodotti_det_1 from uo_cs_xx_dw within w_prodotti
end type
type dw_prodotti_det_2 from uo_cs_xx_dw within w_prodotti
end type
type dw_prodotti_det_3 from uo_cs_xx_dw within w_prodotti
end type
type dw_prodotti_det_4 from uo_cs_xx_dw within w_prodotti
end type
type dw_prodotti_det_5 from uo_cs_xx_dw within w_prodotti
end type
type dw_prodotti_det_6 from uo_cs_xx_dw within w_prodotti
end type
type dw_colonne_dinamiche from uo_colonne_dinamiche_dw within w_prodotti
end type
type cb_barcode from commandbutton within w_prodotti
end type
type cb_centro_costo_acq from commandbutton within w_prodotti
end type
type cb_centro_costo_ven from commandbutton within w_prodotti
end type
type cb_chiavi from commandbutton within w_prodotti
end type
type cb_collegati from commandbutton within w_prodotti
end type
type cb_documenti from commandbutton within w_prodotti
end type
type cb_duplica_cc from commandbutton within w_prodotti
end type
type cb_duplica_prodotto from commandbutton within w_prodotti
end type
type cb_lingue from commandbutton within w_prodotti
end type
type cb_note from commandbutton within w_prodotti
end type
type cb_prezzo_acq from commandbutton within w_prodotti
end type
type cb_prezzo_ven from commandbutton within w_prodotti
end type
type cb_reset from commandbutton within w_prodotti
end type
type cb_ric_prod_1 from cb_prod_ricerca within w_prodotti
end type
type cb_ricerca from commandbutton within w_prodotti
end type
type cb_trova_distinta from commandbutton within w_prodotti
end type
end forward

global type w_prodotti from w_cs_xx_principale
integer width = 3415
integer height = 2244
string title = "Gestione Prodotti"
dw_documenti dw_documenti
r_1 r_1
dw_folder dw_folder
dw_ricerca dw_ricerca
dw_prodotti_lista dw_prodotti_lista
dw_folder_search dw_folder_search
dw_prodotti_det_1 dw_prodotti_det_1
dw_prodotti_det_2 dw_prodotti_det_2
dw_prodotti_det_3 dw_prodotti_det_3
dw_prodotti_det_4 dw_prodotti_det_4
dw_prodotti_det_5 dw_prodotti_det_5
dw_prodotti_det_6 dw_prodotti_det_6
dw_colonne_dinamiche dw_colonne_dinamiche
cb_barcode cb_barcode
cb_centro_costo_acq cb_centro_costo_acq
cb_centro_costo_ven cb_centro_costo_ven
cb_chiavi cb_chiavi
cb_collegati cb_collegati
cb_documenti cb_documenti
cb_duplica_cc cb_duplica_cc
cb_duplica_prodotto cb_duplica_prodotto
cb_lingue cb_lingue
cb_note cb_note
cb_prezzo_acq cb_prezzo_acq
cb_prezzo_ven cb_prezzo_ven
cb_reset cb_reset
cb_ric_prod_1 cb_ric_prod_1
cb_ricerca cb_ricerca
cb_trova_distinta cb_trova_distinta
end type
global w_prodotti w_prodotti

type variables
boolean ib_vrp = false, ib_nuovo = false
long    il_row
end variables

forward prototypes
public subroutine wf_updatestart ()
public function integer wf_updateend ()
end prototypes

public subroutine wf_updatestart ();long    ll_cod_barre, li_i, li_i1
string  ls_tabella, ls_codice, ls_cod_prodotto, ls_cod_prodotto_old, ls_des_prodotto,ls_flag

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CMN';
if ls_flag = "S" then
	for li_i = 1 to dw_prodotti_lista.rowcount()
		if dw_prodotti_lista.getitemstatus(li_i, 0, primary!) = NewModified! then
			g_mb.messagebox("APICE","Attenzione!! Hai creato un nuovo prodotto di magazzino e devi creare il lotto tramite un movimento di magazzino a mano, altrimenti il magazzino non si movimenta.",information!)
			exit
		end if
	next
end if

for li_i = 1 to pcca.window_currentdw.deletedcount()
   ls_tabella = "anag_prodotti_lingue"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)
	// cancello da tab_prodotti_collegati
   ls_tabella = "tab_prodotti_collegati"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)

	// cancello da anag_prodotti_chiavi
   ls_tabella = "anag_prodotti_chiavi"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)
	

next

li_i = 0
do while li_i <= pcca.window_currentdw.rowcount()
   li_i = pcca.window_currentdw.getnextmodified(li_i, Primary!)

   if li_i = 0 then
      return
   end if

	select cod_barre
   into   :ll_cod_barre
   from   con_magazzino
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_barre_azienda <> 0;

   if sqlca.sqlcode = 0 then
      if pcca.window_currentdw.getitemnumber(li_i, "cod_barre") = 0 then
         pcca.window_currentdw.setitem(li_i, "cod_barre", ll_cod_barre + 1)
   
         update con_magazzino
         set    cod_barre = :ll_cod_barre + 1
         where  cod_azienda = :s_cs_xx.cod_azienda;
      end if
 
      ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto")
      ll_cod_barre = pcca.window_currentdw.getitemnumber(li_i, "cod_barre")
 
      declare cu_cod_barre cursor for select cod_prodotto, des_prodotto from anag_prodotti where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto <> :ls_cod_prodotto and cod_barre = :ll_cod_barre and cod_barre <> 0;

      open cu_cod_barre;

      li_i1 = 0
      do while 0 = 0
         li_i1 ++
         fetch cu_cod_barre into :ls_cod_prodotto_old, :ls_des_prodotto;

         if sqlca.sqlcode <> 0 or li_i1 = 1 then exit
      loop

      if sqlca.sqlcode = 0 then
      	g_mb.messagebox("Attenzione", "Codice a barre già utilizzato per " + ls_cod_prodotto_old + " " + ls_des_prodotto + "!", &
                    exclamation!, ok!)
         pcca.error = pcca.window_currentdw.c_fatal
         close cu_cod_barre;
         return
      end if
      close cu_cod_barre;
   end if

   if pcca.window_currentdw.getitemnumber(li_i, "livello_riordino") > 0 and not isnull(pcca.window_currentdw.getitemnumber(li_i, "livello_riordino")) then
		if pcca.window_currentdw.getitemnumber(li_i, "livello_riordino") < pcca.window_currentdw.getitemnumber(li_i, "scorta_minima") then
			g_mb.messagebox("Attenzione", "Il livello di riordino deve essere maggiore o uguale aella scorta minima!", &
						  exclamation!, ok!)
			pcca.error = pcca.window_currentdw.c_fatal
			return
		end if
	end if
   if pcca.window_currentdw.getitemnumber(li_i, "quan_minima") > pcca.window_currentdw.getitemnumber(li_i, "quan_massima") then
   	g_mb.messagebox("Attenzione", "La quantità minima deve essere minore della quantità massima!", &
                 exclamation!, ok!)
      pcca.error = pcca.window_currentdw.c_fatal
      return
   end if
loop


end subroutine

public function integer wf_updateend ();long li_i

for li_i = 1 to dw_prodotti_lista.rowcount()
	if dw_prodotti_lista.getitemstatus(li_i, 0, primary!) = NewModified! then	
		dw_prodotti_lista.setrow(li_i)
		ib_nuovo = true
		window_open_parm(w_prodotti_chiavi_valori, -1, dw_prodotti_lista)
	end if
next

if li_i > 0 then
	return 100
else
	return 0
end if


end function

event pc_setwindow;call super::pc_setwindow;datetime ldt_data_chiusura_annuale_prec, ldt_data_chiusura_annuale, &
       ldt_data_chiusura_periodica
string ls_vrp, ls_data_chiusura_annuale_prec, ls_data_chiusura_annuale, &
       ls_data_chiusura_periodica, ls_cod_livello_prod_1, ls_cod_livello_prod_2, &
   	 ls_cod_livello_prod_3, ls_cod_livello_prod_4, ls_cod_livello_prod_5, &
       l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[], l_objects[ ],lw_oggetti_1[]

dw_documenti.settransobject(sqlca)
dw_documenti.set_parent(dw_prodotti_lista)

lw_oggetti[1] = dw_documenti
dw_folder.fu_assigntab(8, "Documenti", lw_oggetti[])

lw_oggetti[1] = dw_colonne_dinamiche
dw_folder.fu_assigntab(7, "Col. Dinamiche", lw_oggetti[])

lw_oggetti[1] = dw_prodotti_det_2
dw_folder.fu_assigntab(3, "Situazione", lw_oggetti[])

lw_oggetti[1] = dw_prodotti_det_6
dw_folder.fu_assigntab(6, "Raggruppo", lw_oggetti[])

lw_oggetti_1[1] = dw_prodotti_det_1
lw_oggetti_1[2] = cb_duplica_prodotto
dw_folder.fu_assigntab(1, "Principale", lw_oggetti_1[])

lw_oggetti_1[1] = dw_prodotti_det_5
lw_oggetti_1[2] = cb_ric_prod_1
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti_1[])

lw_oggetti[1] = dw_prodotti_det_4
lw_oggetti[2] = cb_prezzo_ven
lw_oggetti[3] = cb_centro_costo_ven
dw_folder.fu_assigntab(5, "Vendite/Rif.", lw_oggetti[])

lw_oggetti[1] = dw_prodotti_det_3
lw_oggetti[2] = cb_prezzo_acq
lw_oggetti[3] = cb_centro_costo_acq
dw_folder.fu_assigntab(4, "Acquisti", lw_oggetti[])

dw_folder.fu_foldercreate(8,8)
dw_folder.fu_selecttab(1)

dw_prodotti_lista.set_dw_key("cod_azienda")
dw_prodotti_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_noretrieveonopen, &
                                 c_default)
dw_prodotti_det_1.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_prodotti_det_2.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_prodotti_det_3.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_prodotti_det_4.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_prodotti_det_5.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

dw_prodotti_det_6.set_dw_options(sqlca, &
                                dw_prodotti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main=dw_prodotti_lista

// stefanop: 22/11/2011: colonne dinamiche
if not dw_colonne_dinamiche.uof_set_dw(dw_prodotti_lista, {"cod_prodotto"}) then
	g_mb.error(dw_colonne_dinamiche.uof_get_error())
end if
// ----


SELECT con_magazzino.data_chiusura_annuale_prec,
       con_magazzino.data_chiusura_annuale,
       con_magazzino.data_chiusura_periodica,
 	    con_magazzino.label_livello_prod_1,
		 con_magazzino.label_livello_prod_2,
		 con_magazzino.label_livello_prod_3,
		 con_magazzino.label_livello_prod_4,
		 con_magazzino.label_livello_prod_5
 INTO  :ldt_data_chiusura_annuale_prec,    
       :ldt_data_chiusura_annuale,
       :ldt_data_chiusura_periodica,
   	 :ls_cod_livello_prod_1,
		 :ls_cod_livello_prod_2,
		 :ls_cod_livello_prod_3,
		 :ls_cod_livello_prod_4,
		 :ls_cod_livello_prod_5
  FROM con_magazzino
 WHERE con_magazzino.cod_azienda = :s_cs_xx.cod_azienda;

dw_prodotti_det_1.modify("st_cod_livello_prod_1.text='" + ls_cod_livello_prod_1 + ":'")
dw_prodotti_det_1.modify("st_cod_livello_prod_2.text='" + ls_cod_livello_prod_2 + ":'")
dw_prodotti_det_1.modify("st_cod_livello_prod_3.text='" + ls_cod_livello_prod_3 + ":'")
dw_prodotti_det_1.modify("st_cod_livello_prod_4.text='" + ls_cod_livello_prod_4 + ":'")
dw_prodotti_det_1.modify("st_cod_livello_prod_5.text='" + ls_cod_livello_prod_5 + ":'")

ls_data_chiusura_annuale_prec=string(ldt_data_chiusura_annuale_prec,"dd/mm/yyyy")
dw_prodotti_det_2.modify("st_data_chiusura_annuale_prec.text='" + ls_data_chiusura_annuale_prec + "'")

ls_data_chiusura_annuale=string(relativedate(date(ldt_data_chiusura_annuale),1),"dd/mm/yyyy")
dw_prodotti_det_2.modify("st_data_chiusura_annuale.text='" + ls_data_chiusura_annuale + "'")

ls_data_chiusura_periodica=string(ldt_data_chiusura_periodica,"dd/mm/yyyy")
dw_prodotti_det_2.modify("st_data_chiusura_periodica.text='" + ls_data_chiusura_periodica + "'")

cb_documenti.enabled = false
cb_lingue.enabled = false
cb_barcode.enabled = false
cb_note.enabled = false
cb_prezzo_acq.enabled = false
cb_prezzo_ven.enabled = false
cb_chiavi.enabled = false
cb_collegati.enabled = false
cb_trova_distinta.enabled = false
cb_duplica_prodotto.enabled = false
cb_ric_prod_1.enabled = false

dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = true
dw_prodotti_det_3.object.b_ricerca_fornitore.enabled = true
	
cb_centro_costo_acq.enabled = false
cb_centro_costo_ven.enabled = false

l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "rs_cod_comodo"
l_criteriacolumn[3] = "rs_flag_classe_abc"
l_criteriacolumn[4] = "rs_cod_cat_mer"
l_criteriacolumn[5] = "rs_flag_blocco"

//Donato 23-02-2009 la ricerca per data blocco non funzionava
//allora ho aggiunto un campo data di tipo stringa in cui scrivere il valore data formattato come si deve

//vedi anche evento clicked del cb_reset e cb_ricerca

//l_criteriacolumn[6] = "rd_data_blocco"
l_criteriacolumn[6] = "rs_data_blocco"
//fine modifica -----------------------------------

l_criteriacolumn[7] = "rs_cod_comodo_2"

l_searchtable[1] = "anag_prodotti"
l_searchtable[2] = "anag_prodotti"
l_searchtable[3] = "anag_prodotti"
l_searchtable[4] = "anag_prodotti"
l_searchtable[5] = "anag_prodotti"
l_searchtable[6] = "anag_prodotti"
l_searchtable[7] = "anag_prodotti"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_comodo"
l_searchcolumn[3] = "flag_classe_abc"
l_searchcolumn[4] = "cod_cat_mer"
l_searchcolumn[5] = "flag_blocco"
l_searchcolumn[6] = "data_blocco"
l_searchcolumn[7] = "cod_comodo_2"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_prodotti_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)
l_objects[1] = dw_prodotti_lista
l_objects[2] = cb_barcode
l_objects[3] = cb_duplica_cc
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_prodotti_lista.change_dw_current()

// --------------------- Michele 18/12/2003 -----------------------
string ls_fpb

select flag
into	 :ls_fpb
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'FPB';
		 
if sqlca.sqlcode = 0 and not isnull(ls_fpb) then
	if ls_fpb = "S" then
		dw_ricerca.setitem(dw_ricerca.getrow(),"rs_flag_blocco","S")
	elseif ls_fpb = "N" then
		dw_ricerca.setitem(dw_ricerca.getrow(),"rs_flag_blocco","N")
	end if
end if
// ----------------------------------------------------------------


// *** Michela 12/07/2006: se esiste il parametro aziendale VRP ed è a Si allora la descrizione del prodotto
//                         si salva con le chiavi inserite e il prodotto diventa progressivo.
select flag
into   :ls_vrp
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'VRP';
		 
if sqlca.sqlcode = 0 and not isnull(ls_vrp) and ls_vrp = 'S' then
	ib_vrp = true
else
	ib_vrp = false
end if


try
	dw_prodotti_det_2.object.p_impegnato.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_prodotti_det_2.object.p_assegnato.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_prodotti_det_2.object.p_in_spedizione.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_prodotti_det_2.object.p_progressivi.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png"
catch (runtimeerror err)
end try



end event

event pc_delete;call super::pc_delete;cb_documenti.enabled = false
cb_lingue.enabled = false
cb_barcode.enabled = false
cb_note.enabled = false
cb_prezzo_acq.enabled = false
cb_prezzo_ven.enabled = false
cb_chiavi.enabled = false
cb_collegati.enabled = false
cb_trova_distinta.enabled = false
cb_duplica_prodotto.enabled = false
dw_prodotti_det_3.object.b_ricerca_fornitore.enabled = false
cb_ric_prod_1.enabled = false
dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = false
cb_centro_costo_acq.enabled = false
cb_centro_costo_ven.enabled = false

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_prodotti_det_1, &
                 "cod_nomenclatura", &
                 sqlca, &
                 "tab_nomenclature", &
                 "cod_nomenclatura", &
                 "des_nomenclatura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_1, &
                 "cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_1, &
                 "cod_responsabile", &
                 sqlca, &
                 "tab_responsabili", &
                 "cod_responsabile", &
                 "des_responsabile", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_misura_mag", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_misura_peso", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_misura_vol", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_3, &
                 "cod_misura_acq", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_misura_lead_time", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_tipo_politica_riordino", &
                 sqlca, &
                 "tab_tipi_politiche_riordino", &
                 "cod_tipo_politica_riordino", &
                 "des_tipo_politica_riordino", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_3, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_3, &
                 "cod_gruppo_acq", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_4, &
                 "cod_misura_ven", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_4, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_4, &
                 "cod_gruppo_ven", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_5, &
                 "cod_formula", &
                 sqlca, &
                 "tab_formule_db", &
                 "cod_formula", &
                 "des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//dw_ricerca.fu_loadcode("rs_cod_prodotto", &
//                 "anag_prodotti", &
//					  "cod_prodotto", &
//					  "des_prodotto", &
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_cat_mer", &
                 "tab_cat_mer", &
					  "cod_cat_mer", &
					  "des_cat_mer", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_cat_mer asc", "(Tutti)" )
f_po_loaddddw_dw(dw_prodotti_det_1, &
                 "cod_livello_prod_1", &
                 sqlca, &
                 "tab_livelli_prod_1", &
                 "cod_livello_prod_1", &
                 "des_livello", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_prodotti_det_4, &
                 "cod_rifiuto", &
                 sqlca, &
                 "tab_rifiuti", &
                 "cod_rifiuto", &
                 "des_rifiuto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_prodotti_det_1, &
                 "cod_tipo_costo_mp", &
                 sqlca, &
                 "tab_tipi_costi_mp", &
                 "cod_tipo_costo_mp", &
                 "des_tipo_costo_mp", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_prodotti.create
int iCurrent
call super::create
this.dw_documenti=create dw_documenti
this.r_1=create r_1
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
this.dw_prodotti_lista=create dw_prodotti_lista
this.dw_folder_search=create dw_folder_search
this.dw_prodotti_det_1=create dw_prodotti_det_1
this.dw_prodotti_det_2=create dw_prodotti_det_2
this.dw_prodotti_det_3=create dw_prodotti_det_3
this.dw_prodotti_det_4=create dw_prodotti_det_4
this.dw_prodotti_det_5=create dw_prodotti_det_5
this.dw_prodotti_det_6=create dw_prodotti_det_6
this.dw_colonne_dinamiche=create dw_colonne_dinamiche
this.cb_barcode=create cb_barcode
this.cb_centro_costo_acq=create cb_centro_costo_acq
this.cb_centro_costo_ven=create cb_centro_costo_ven
this.cb_chiavi=create cb_chiavi
this.cb_collegati=create cb_collegati
this.cb_documenti=create cb_documenti
this.cb_duplica_cc=create cb_duplica_cc
this.cb_duplica_prodotto=create cb_duplica_prodotto
this.cb_lingue=create cb_lingue
this.cb_note=create cb_note
this.cb_prezzo_acq=create cb_prezzo_acq
this.cb_prezzo_ven=create cb_prezzo_ven
this.cb_reset=create cb_reset
this.cb_ric_prod_1=create cb_ric_prod_1
this.cb_ricerca=create cb_ricerca
this.cb_trova_distinta=create cb_trova_distinta
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_documenti
this.Control[iCurrent+2]=this.r_1
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.dw_prodotti_lista
this.Control[iCurrent+6]=this.dw_folder_search
this.Control[iCurrent+7]=this.dw_prodotti_det_1
this.Control[iCurrent+8]=this.dw_prodotti_det_2
this.Control[iCurrent+9]=this.dw_prodotti_det_3
this.Control[iCurrent+10]=this.dw_prodotti_det_4
this.Control[iCurrent+11]=this.dw_prodotti_det_5
this.Control[iCurrent+12]=this.dw_prodotti_det_6
this.Control[iCurrent+13]=this.dw_colonne_dinamiche
this.Control[iCurrent+14]=this.cb_barcode
this.Control[iCurrent+15]=this.cb_centro_costo_acq
this.Control[iCurrent+16]=this.cb_centro_costo_ven
this.Control[iCurrent+17]=this.cb_chiavi
this.Control[iCurrent+18]=this.cb_collegati
this.Control[iCurrent+19]=this.cb_documenti
this.Control[iCurrent+20]=this.cb_duplica_cc
this.Control[iCurrent+21]=this.cb_duplica_prodotto
this.Control[iCurrent+22]=this.cb_lingue
this.Control[iCurrent+23]=this.cb_note
this.Control[iCurrent+24]=this.cb_prezzo_acq
this.Control[iCurrent+25]=this.cb_prezzo_ven
this.Control[iCurrent+26]=this.cb_reset
this.Control[iCurrent+27]=this.cb_ric_prod_1
this.Control[iCurrent+28]=this.cb_ricerca
this.Control[iCurrent+29]=this.cb_trova_distinta
end on

on w_prodotti.destroy
call super::destroy
destroy(this.dw_documenti)
destroy(this.r_1)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
destroy(this.dw_prodotti_lista)
destroy(this.dw_folder_search)
destroy(this.dw_prodotti_det_1)
destroy(this.dw_prodotti_det_2)
destroy(this.dw_prodotti_det_3)
destroy(this.dw_prodotti_det_4)
destroy(this.dw_prodotti_det_5)
destroy(this.dw_prodotti_det_6)
destroy(this.dw_colonne_dinamiche)
destroy(this.cb_barcode)
destroy(this.cb_centro_costo_acq)
destroy(this.cb_centro_costo_ven)
destroy(this.cb_chiavi)
destroy(this.cb_collegati)
destroy(this.cb_documenti)
destroy(this.cb_duplica_cc)
destroy(this.cb_duplica_prodotto)
destroy(this.cb_lingue)
destroy(this.cb_note)
destroy(this.cb_prezzo_acq)
destroy(this.cb_prezzo_ven)
destroy(this.cb_reset)
destroy(this.cb_ric_prod_1)
destroy(this.cb_ricerca)
destroy(this.cb_trova_distinta)
end on

event pc_new;call super::pc_new;dw_prodotti_det_1.setitem(dw_prodotti_det_1.getrow(), "data_creazione", datetime(today()))

end event

type dw_documenti from uo_drag_prodotti within w_prodotti
integer x = 46
integer y = 720
integer width = 3269
integer height = 1372
integer taborder = 210
end type

type r_1 from rectangle within w_prodotti
long linecolor = 255
linestyle linestyle = transparent!
long fillcolor = 255
integer x = 2898
integer y = 28
integer width = 55
integer height = 60
end type

type dw_folder from u_folder within w_prodotti
integer x = 23
integer y = 620
integer width = 3314
integer height = 1496
integer taborder = 90
end type

type dw_ricerca from u_dw_search within w_prodotti
event ue_key pbm_dwnkey
integer x = 137
integer y = 40
integer width = 2674
integer height = 440
integer taborder = 60
string dataobject = "d_prodotti_search"
end type

event ue_key;choose case this.getcolumnname()
	case "rs_cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
		end if
end choose

if key = keyenter! then cb_reset.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

type dw_prodotti_lista from uo_cs_xx_dw within w_prodotti
event ue_aggiorna_descrizione ( )
event ue_riposiziona_riga ( )
integer x = 137
integer y = 40
integer width = 2011
integer height = 540
integer taborder = 110
string dataobject = "d_prodotti_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_aggiorna_descrizione();//if ib_nuovo then
	cb_chiavi.triggerevent("ue_aggiorna_descrizione")
	ib_nuovo = false
//end if
end event

event ue_riposiziona_riga();//if il_row > 0 then
	change_dw_current()
//	scrolltorow(il_row)
//	il_row = 0
//end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	dw_colonne_dinamiche.uof_Delete()
	this.change_dw_current()
	wf_updatestart()
end if

end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_prodotto")) then
      cb_documenti.enabled = true
      cb_lingue.enabled = true
		cb_chiavi.enabled = true
		cb_collegati.enabled = true
      cb_note.enabled = true
		cb_trova_distinta.enabled = true
		cb_duplica_prodotto.enabled = true
		cb_centro_costo_acq.enabled = true
		cb_centro_costo_ven.enabled = true
		cb_duplica_cc.enabled = true
   else
      cb_documenti.enabled = false
		cb_trova_distinta.enabled = false
      cb_lingue.enabled = false
      cb_note.enabled = false
		cb_chiavi.enabled = false
		cb_collegati.enabled = false
		cb_centro_costo_acq.enabled = false
		cb_centro_costo_ven.enabled = false
		cb_duplica_cc.enabled = false
   end if

	cb_prezzo_acq.enabled = false
	cb_prezzo_ven.enabled = false
	cb_ric_prod_1.enabled = false
	dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = false
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_prodotto, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
			 ls_cod_livello_prod_4, ls_cod_livello_prod_5 
	long ll_cont
	
	if this.getrow() > 0 then
		
		ls_cod_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
		
		dw_colonne_dinamiche.retrieve()
		
		select count(*)
		into   :ll_cont
		from   anag_prodotti_note
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if ll_cont > 0 then
			r_1.fillcolor = rgb(255,0,0)
		else
			r_1.fillcolor = parent.backcolor
		end if
		
		cb_prezzo_acq.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura_acq")
		cb_prezzo_ven.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura_ven")
		
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> this.getitemstring(this.getrow(),"cod_misura_acq") then
			dw_prodotti_det_3.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_acq"))) + " " + this.getitemstring(this.getrow(),"cod_misura_acq") + ")'")
		else
			dw_prodotti_det_3.modify("st_fattore_conv.text=''")
		end if
		
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> this.getitemstring(this.getrow(),"cod_misura_ven") then
			dw_prodotti_det_4.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_ven"))) + " " + this.getitemstring(this.getrow(),"cod_misura_ven") + ")'")
		else
			dw_prodotti_det_4.modify("st_fattore_conv.text=''")
		end if
		
		if isnull(ls_cod_prodotto) then
			ls_cod_prodotto = ""
		end if

		ls_cod_livello_prod_1 = this.getitemstring(this.getrow(), "cod_livello_prod_1")
		ls_cod_livello_prod_2 = this.getitemstring(this.getrow(), "cod_livello_prod_2")
		ls_cod_livello_prod_3 = this.getitemstring(this.getrow(), "cod_livello_prod_3")
		ls_cod_livello_prod_4 = this.getitemstring(this.getrow(), "cod_livello_prod_4")
		ls_cod_livello_prod_5 = this.getitemstring(this.getrow(), "cod_livello_prod_5")
		
		f_po_loaddddw_dw(dw_prodotti_det_1, &
							  "cod_livello_prod_2", &
							  sqlca, &
							  "tab_livelli_prod_2", &
							  "cod_livello_prod_2", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
							  "cod_livello_prod_3", &
							  sqlca, &
							  "tab_livelli_prod_3", &
							  "cod_livello_prod_3", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
							  "cod_livello_prod_4", &
							  sqlca, &
							  "tab_livelli_prod_4", &
							  "cod_livello_prod_4", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
							  "cod_livello_prod_5", &
							  sqlca, &
							  "tab_livelli_prod_5", &
							  "cod_livello_prod_5", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "'")
	else

		f_po_loaddddw_dw(dw_prodotti_det_3, &
						  "cod_prodotto_alt", &
						  sqlca, &
						  "anag_prodotti", &
						  "cod_prodotto", &
						  "des_prodotto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	end if
	
	if dw_colonne_dinamiche.rowcount() > 0 then dw_colonne_dinamiche.event trigger rowfocuschanged(1)
	
	dw_documenti.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
end if
end event

event pcd_setkey;call super::pcd_setkey;long   ll_i, ll_cont
string ls_cod_prodotto, ls_test


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next


end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_sql

ll_errore = retrieve(s_cs_xx.cod_azienda)

ls_sql = getsqlselect()

if ll_errore < 0 then
   pcca.error = c_fatal
elseif ll_errore = 0 then
	dw_colonne_dinamiche.reset()
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_documenti.enabled = false
	cb_trova_distinta.enabled = false
	cb_lingue.enabled = false
	cb_barcode.enabled = false
	cb_note.enabled = false
	cb_chiavi.enabled = false
	cb_collegati.enabled = false
	cb_duplica_prodotto.enabled = false
	cb_prezzo_acq.enabled = true
	cb_prezzo_ven.enabled = true
	
	dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = true
	dw_prodotti_det_3.object.b_ricerca_fornitore.enabled = true
	
	cb_ric_prod_1.enabled = true
	cb_centro_costo_acq.enabled = false
	cb_centro_costo_ven.enabled = false
	cb_duplica_cc.enabled = false
	
	dw_prodotti_det_5.object.b_dettaglio.enabled = false
end if

// *** Michela 12/07/2006: se esiste il parametro aziendale VRP devo creare in modo automatico il codice
//                         mettendo un progressivo

long   ll_i, ll_cont
string ls_cod_prodotto, ls_test

if ib_vrp then
	
	for ll_i = 1 to this.rowcount()
		
		if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
			
			ll_cont = 0
			
			do
				ll_cont ++
				
				ls_cod_prodotto = string(ll_cont,"000000000000000")
				
				// verifico se il codice esiste già
				
				select cod_prodotto
				into   :ls_test
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
				
			loop until sqlca.sqlcode = 100						
			
			this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
			
		end if
	next
	dw_prodotti_det_1.setcolumn("des_prodotto")
	
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_documenti.enabled = false
	cb_trova_distinta.enabled = false
	cb_lingue.enabled = false
	cb_barcode.enabled = false
	cb_note.enabled = false
	cb_chiavi.enabled = false
	cb_collegati.enabled = false
	cb_duplica_prodotto.enabled = false
	cb_prezzo_acq.enabled = true
	cb_prezzo_ven.enabled = true
	cb_ric_prod_1.enabled = true
	
	dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = true
	dw_prodotti_det_3.object.b_ricerca_fornitore.enabled = true
	
	cb_centro_costo_acq.enabled = false
	cb_centro_costo_ven.enabled = false
	cb_duplica_cc.enabled = false
	
	dw_prodotti_det_5.object.b_dettaglio.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_prodotto")) then
		cb_documenti.enabled = true
		cb_lingue.enabled = true
		cb_barcode.enabled = true
		cb_note.enabled = true
		cb_chiavi.enabled = true
		cb_collegati.enabled = true
		cb_trova_distinta.enabled = true
		cb_duplica_prodotto.enabled = true
		cb_centro_costo_acq.enabled = true
		cb_centro_costo_ven.enabled = true
		cb_duplica_cc.enabled = true
	else
		cb_documenti.enabled = false
		cb_lingue.enabled = false
		cb_barcode.enabled = false
		cb_note.enabled = false
		cb_chiavi.enabled = false
		cb_collegati.enabled = false
		cb_trova_distinta.enabled = false
		cb_duplica_prodotto.enabled = false
		cb_centro_costo_acq.enabled = false
		cb_centro_costo_ven.enabled = false
		cb_duplica_cc.enabled = false
	end if

	cb_prezzo_acq.enabled = false
	cb_prezzo_ven.enabled = false
	cb_ric_prod_1.enabled = false
	
	dw_prodotti_det_3.object.b_ricerca_prodotto_alt.enabled = false
	dw_prodotti_det_3.object.b_ricerca_fornitore.enabled = false
	
	dw_prodotti_det_5.object.b_dettaglio.enabled = true
	
end if
end event

event updateend;call super::updateend;long li_i
string ls_cod_misura_mag, ls_cod_prodotto

// *** Michela 12/07/2006: se esiste il parametro aziendale VRP devo creare in modo automatico il codice
//                         mettendo un progressivo

if ib_vrp then		
	wf_updateend()
end if	

//Donato 27-10-2008
//se è cambiata la unita di misura allora occorre aggiornare nelle eventuali distinte
//in cui il prodotto è presente
for li_i = 1 to dw_prodotti_lista.rowcount()
	if dw_prodotti_lista.getitemstatus(li_i, "cod_misura_mag", primary!) = DataModified! then	
		ls_cod_misura_mag = dw_prodotti_lista.getitemstring(li_i, "cod_misura_mag")
		ls_cod_prodotto = dw_prodotti_lista.getitemstring(li_i, "cod_prodotto")
		
		update distinta
		set cod_misura = :ls_cod_misura_mag
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto_figlio = :ls_cod_prodotto
		;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante l'aggiornamento della Unità di Misura nelle distinte del prodotto:"+ls_cod_prodotto, StopSign!)
		end if
	end if
next
//fine modifica -----------------------------

// stefanop: salvo colonne dinamiche
dw_colonne_dinamiche.update()
end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "anag_prodotti"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_prodotto")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_prodotto")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

event rowfocuschanging;call super::rowfocuschanging;if currentrow <> newrow then
	
	dw_colonne_dinamiche.uof_verify_changes()
	
end if
end event

type dw_folder_search from u_folder within w_prodotti
integer x = 23
integer y = 20
integer width = 2857
integer height = 580
integer taborder = 50
end type

type dw_prodotti_det_1 from uo_cs_xx_dw within w_prodotti
integer x = 46
integer y = 720
integer width = 3269
integer height = 1388
integer taborder = 190
boolean bringtotop = true
string dataobject = "d_prodotti_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
       ls_cod_livello_prod_4

setnull(ls_null)

choose case i_colname
	case "cod_prodotto"
		long ll_cont
		select count(cod_prodotto)
		into   :ll_cont
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :i_coltext;
		if ll_cont > 0 then
			g_mb.messagebox("APICE","Esiste già un prodotto con questo codice!", StopSign!)
			return 1
		end if

	case "cod_livello_prod_1"
		this.setitem(this.getrow(), "cod_livello_prod_2", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_3", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_4", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_5", ls_null)
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_2", &
                       sqlca, &
                       "tab_livelli_prod_2", &
                       "cod_livello_prod_2", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_3", &
                       sqlca, &
                       "tab_livelli_prod_3", &
                       "cod_livello_prod_3", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + i_coltext + "' and cod_livello_prod_2 = ''")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + i_coltext + "' and cod_livello_prod_2 = '' and cod_livello_prod_3 = ''")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + i_coltext + "' and cod_livello_prod_2 = '' and cod_livello_prod_3 = '' and cod_livello_prod_4 = ''")
   case "cod_livello_prod_2"
		ls_cod_livello_prod_1 = this.getitemstring(this.getrow(), "cod_livello_prod_1")
		this.setitem(this.getrow(), "cod_livello_prod_3", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_4", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_5", ls_null)
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_3", &
                       sqlca, &
                       "tab_livelli_prod_3", &
                       "cod_livello_prod_3", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "' and cod_livello_prod_3 = ''")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "' and cod_livello_prod_3 = '' and cod_livello_prod_4 = ''")
   case "cod_livello_prod_3"
		ls_cod_livello_prod_1 = this.getitemstring(this.getrow(), "cod_livello_prod_1")
		ls_cod_livello_prod_2 = this.getitemstring(this.getrow(), "cod_livello_prod_2")
		this.setitem(this.getrow(), "cod_livello_prod_4", ls_null)
		this.setitem(this.getrow(), "cod_livello_prod_5", ls_null)
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + i_coltext + "' and cod_livello_prod_4 = ''")
	case "cod_livello_prod_4"
		ls_cod_livello_prod_1 = this.getitemstring(this.getrow(), "cod_livello_prod_1")
		ls_cod_livello_prod_2 = this.getitemstring(this.getrow(), "cod_livello_prod_2")
		ls_cod_livello_prod_3 = this.getitemstring(this.getrow(), "cod_livello_prod_3")
		this.setitem(this.getrow(), "cod_livello_prod_5", ls_null)
		f_po_loaddddw_dw(dw_prodotti_det_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + i_coltext + "'")
end choose

end event

type dw_prodotti_det_2 from uo_cs_xx_dw within w_prodotti
integer x = 50
integer y = 720
integer width = 3182
integer height = 1232
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_prodotti_det_2"
boolean border = false
end type

event ue_key;call super::ue_key;if key = keyf12! and keyflags = 1 then
	s_cs_xx.parametri.parametro_s_1 = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto")
	open(w_prodotti_ricalcolo_saldi)
end if
end event

event clicked;call super::clicked;long											ll_row
string											ls_cod_prodotto
w_report_impegnato_prodotto			lw_imp
w_report_assegnato_prodotto			lw_ass
w_report_inspediz_prodotto				lw_insped
w_aggiorna_impegnato					lw_progressivi


if isvalid(dwo) then
	
	ll_row = getrow()
	if ll_row> 0 then
		ls_cod_prodotto = getitemstring(ll_row, "cod_prodotto")
	end if
	
	choose case dwo.name
		case "p_impegnato"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_imp, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)
			//window_open_parm(w_report_impegnato_prodotto, -1, dw_prodotti_lista)

		case "p_assegnato"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_ass, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_report_assegnato_prodotto, ls_cod_prodotto)

		case "p_in_spedizione"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_insped, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_report_inspediz_prodotto, ls_cod_prodotto)

		case "p_progressivi"
			opensheetwithparm(lw_progressivi, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)

	end choose
end if


	
end event

type dw_prodotti_det_3 from uo_cs_xx_dw within w_prodotti
integer x = 46
integer y = 720
integer width = 3246
integer height = 1180
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_prodotti_det_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_misura_acq"
      cb_prezzo_acq.text = "Prezzo al " + i_coltext
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> i_coltext then
	 		dw_prodotti_det_3.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_acq"))) + " " + i_coltext + ")'")
		else
			dw_prodotti_det_3.modify("st_fattore_conv.text=''")
		end if
	case "fat_conversione_acq"
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> this.getitemstring(this.getrow(),"cod_misura_acq") then
	 		dw_prodotti_det_3.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + i_coltext + " " + trim(this.getitemstring(this.getrow(),"cod_misura_acq")) + ")'")
		else
			dw_prodotti_det_3.modify("st_fattore_conv.text=''")
		end if
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_prodotti_det_3,"cod_fornitore")
	case "b_ricerca_prodotto_alt"
		guo_ricerca.uof_ricerca_prodotto(dw_prodotti_det_3,"cod_prodotto_alt")
end choose
end event

type dw_prodotti_det_4 from uo_cs_xx_dw within w_prodotti
integer x = 46
integer y = 720
integer width = 3246
integer height = 1228
integer taborder = 200
boolean bringtotop = true
string dataobject = "d_prodotti_det_4"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_misura_ven"
      cb_prezzo_ven.text = "Prezzo al " + i_coltext
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> i_coltext then
	 		dw_prodotti_det_4.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + trim(f_double_string(this.getitemnumber(dw_prodotti_det_4.getrow(),"fat_conversione_ven"))) + " " + i_coltext + ")'")
		else
	 		dw_prodotti_det_4.modify("st_fattore_conv.text=''")
		end if
	case "fat_conversione_ven"
		if this.getitemstring(this.getrow(),"cod_misura_mag") <> this.getitemstring(this.getrow(),"cod_misura_ven") then
	 		dw_prodotti_det_4.modify("st_fattore_conv.text='(1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + "=" + i_coltext + " " + this.getitemstring(this.getrow(),"cod_misura_ven") + ")'")
		else
	 		dw_prodotti_det_4.modify("st_fattore_conv.text=''")
		end if
end choose

end event

type dw_prodotti_det_5 from uo_cs_xx_dw within w_prodotti
event ue_dettaglio_stabilimento ( )
integer x = 46
integer y = 720
integer width = 3269
integer height = 1204
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_prodotti_det_5"
boolean border = false
end type

event ue_dettaglio_stabilimento();long		ll_row
string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione

ll_row = dw_prodotti_lista.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_prodotto = dw_prodotti_lista.getitemstring(ll_row, "cod_prodotto")
ls_cod_reparto = dw_prodotti_lista.getitemstring(ll_row, "cod_reparto")
	
if 		ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto)			then
else
	return
end if

window_open_parm(w_prodotti_reparti_depositi, -1, dw_prodotti_lista)
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_misura_mag"
		if isnull(dw_prodotti_det_3.getitemstring(dw_prodotti_det_3.getrow(), "cod_misura_acq")) then
	      dw_prodotti_det_3.setitem(dw_prodotti_det_3.getrow(), "cod_misura_acq", i_coltext)
   	end if
		if isnull(dw_prodotti_det_4.getitemstring(dw_prodotti_det_4.getrow(), "cod_misura_ven")) then
	      dw_prodotti_det_4.setitem(dw_prodotti_det_4.getrow(), "cod_misura_ven", i_coltext)
   	end if

		if i_coltext <> this.getitemstring(this.getrow(),"cod_misura_acq") then
	 		dw_prodotti_det_3.modify("st_fattore_conv.text='(1 " + trim(i_coltext) + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_acq"))) + " " + this.getitemstring(this.getrow(),"cod_misura_acq") + ")'")
		else
			dw_prodotti_det_3.modify("st_fattore_conv.text=''")
		end if

		if i_coltext <> this.getitemstring(this.getrow(),"cod_misura_ven") then
	 		dw_prodotti_det_4.modify("st_fattore_conv.text='(1 " + trim(i_coltext) + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_ven"))) + " " + this.getitemstring(this.getrow(),"cod_misura_ven") + ")'")
		else
			dw_prodotti_det_4.modify("st_fattore_conv.text=''")
		end if
end choose

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_dettaglio"
		triggerevent("ue_dettaglio_stabilimento")
		
end choose
end event

type dw_prodotti_det_6 from uo_cs_xx_dw within w_prodotti
integer x = 46
integer y = 740
integer width = 3246
integer height = 1140
integer taborder = 150
boolean bringtotop = true
string dataobject = "d_prodotti_det_6"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_modify, ls_cod_prodotto, ls_cod_prodotto_raggruppato, ls_cod_misura_mag
dec{5} ld_fat_conv_rag

choose case i_colname
	case "fat_conversione_rag_mag"
		
		ls_cod_prodotto_raggruppato = this.getitemstring(this.getrow(),"cod_prodotto_raggruppato")
		ls_cod_prodotto				 = this.getitemstring(this.getrow(),"cod_prodotto")

		if not isnull(ls_cod_prodotto_raggruppato) and len(ls_cod_prodotto_raggruppato) > 0 then
			
			select cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
			
			ld_fat_conv_rag = DEC(i_coltext)
			ls_modify = "( 1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + " " + ls_cod_prodotto + "=" + string(ld_fat_conv_rag,"###,##0.0####") + " " + trim( ls_cod_misura_mag ) + " " + ls_cod_prodotto_raggruppato + " )"
			dw_prodotti_det_6.object.st_fattore_conv_acq.text = ls_modify
			
		end if
		
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_modify, ls_cod_prodotto, ls_cod_prodotto_raggruppato, ls_cod_misura_mag
dec{5} ld_fat_conv_rag_mag

if i_CursorRow > 0 then
	
	ls_cod_prodotto_raggruppato = this.getitemstring(this.getrow(),"cod_prodotto_raggruppato")
	ls_cod_prodotto				 = this.getitemstring(this.getrow(),"cod_prodotto")

	if not isnull(ls_cod_prodotto_raggruppato) and len(ls_cod_prodotto_raggruppato) > 0 then
		
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
				 
		ld_fat_conv_rag_mag = this.getitemnumber(this.getrow(),"fat_conversione_rag_mag")
		
		ls_modify = "( 1 " + trim(this.getitemstring(this.getrow(),"cod_misura_mag")) + " " + ls_cod_prodotto + "=" + string(ld_fat_conv_rag_mag,"###,##0.0####") + " " + trim( ls_cod_misura_mag ) + " " + ls_cod_prodotto_raggruppato + " )"
		dw_prodotti_det_6.object.st_fattore_conv_acq.text = ls_modify
	end if
end if

end event

type dw_colonne_dinamiche from uo_colonne_dinamiche_dw within w_prodotti
integer x = 46
integer y = 720
integer width = 3246
integer height = 1140
integer taborder = 210
boolean bringtotop = true
end type

type cb_barcode from commandbutton within w_prodotti
integer x = 2446
integer y = 60
integer width = 375
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Barcode"
end type

event clicked;window_open_parm(w_prodotti_barcode, -1, dw_prodotti_lista)

end event

type cb_centro_costo_acq from commandbutton within w_prodotti
integer x = 2706
integer y = 1596
integer width = 366
integer height = 80
integer taborder = 220
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CC Acquisti"
end type

event clicked;if dw_prodotti_lista.rowcount() > 0 then
	window_open_parm(w_prodotti_cc_acq, -1, dw_prodotti_lista)

end if
end event

type cb_centro_costo_ven from commandbutton within w_prodotti
integer x = 2752
integer y = 1056
integer width = 366
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CC Vendite"
end type

event clicked;if dw_prodotti_lista.rowcount() > 0 then
	window_open_parm(w_prodotti_cc_ven, -1, dw_prodotti_lista)

end if
end event

type cb_chiavi from commandbutton within w_prodotti
event ue_aggiorna_descrizione ( )
integer x = 2971
integer y = 320
integer width = 375
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiavi"
end type

event ue_aggiorna_descrizione();string ls_cod_prodotto, ls_valori, ls_cod_chiave, ls_valore, ls_des_prodotto_new, ls_null
long   ll_progressivo, ll_riga

ll_riga = dw_prodotti_lista.getrow()

if isnull(ll_riga) or ll_riga < 1 then return

if not ib_vrp then return

ls_cod_prodotto = dw_prodotti_lista.getitemstring( dw_prodotti_lista.getrow(), "cod_prodotto")

if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then return 

declare cu_chiavi cursor for
select cod_chiave,
       progressivo
from   anag_prodotti_chiavi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
		 
		 
open cu_chiavi;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante l'apertura del cursore cu_chiavi:" + sqlca.sqlerrtext)
	return 
end if

ls_valori = ""

do while 1 = 1
	
	fetch cu_chiavi into :ls_cod_chiave,
	                     :ll_progressivo;
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox( "APICE", "Errore durante la fetch del cursore cu_chiavi:" + sqlca.sqlerrtext)
		close cu_chiavi;
		return
	end if
	if sqlca.sqlcode = 100 then exit
	
	setnull(ls_valore)
	
	select valore
	into   :ls_valore
	from   tab_chiavi_valori
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_chiave = :ls_cod_chiave and
			 progressivo = :ll_progressivo;
			 
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox( "APICE", "Errore durante la ricerca del valore: " + sqlca.sqlerrtext)
		close cu_chiavi;
		return
	end if
	
	if isnull(ls_valore) then ls_valore = ""
	if ls_valori = "" then
		ls_valori += ls_valore
	else
		ls_valori += ls_valore
	end if
	
loop

close cu_chiavi;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante l'apertura del cursore cu_chiavi:" + sqlca.sqlerrtext)
	return 
end if

if ls_valori <> "" then
	
//	select des_prodotto
//	into   :ls_des_prodotto_new
//	from   anag_prodotti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_prodotto = :ls_cod_prodotto;
//			 
//	if isnull(ls_des_prodotto_new) then 
//		ls_des_prodotto_new = ls_valori
//	else
		ls_des_prodotto_new = ls_valori
//	end if

	update anag_prodotti
	set    des_prodotto = :ls_des_prodotto_new
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	commit;
	
	dw_prodotti_lista.change_dw_current()	
	il_row = dw_prodotti_lista.getrow()
	parent.triggerevent("pc_retrieve")	
	dw_prodotti_lista.postevent("ue_riposiziona_riga")
	
end if

return
end event

event clicked;//window_open_parm(w_anag_prodotti_chiavi, -1, dw_prodotti_lista)
ib_nuovo = false
window_open_parm(w_prodotti_chiavi_valori, -1, dw_prodotti_lista)
end event

type cb_collegati from commandbutton within w_prodotti
integer x = 2971
integer y = 420
integer width = 375
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Collegati"
end type

event clicked;window_open_parm(w_prod_collegati, -1, dw_prodotti_lista)

end event

type cb_documenti from commandbutton within w_prodotti
boolean visible = false
integer x = 2971
integer y = 220
integer width = 375
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documenti"
end type

on clicked;window_open_parm(w_prodotti_blob, -1, dw_prodotti_lista)

end on

type cb_duplica_cc from commandbutton within w_prodotti
integer x = 2446
integer y = 156
integer width = 375
integer height = 80
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica C. C."
end type

event clicked;if dw_prodotti_lista.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_10 = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto")
	if isnull(s_cs_xx.parametri.parametro_s_10) or len(s_cs_xx.parametri.parametro_s_10) < 1 then return
	window_open(w_duplica_prodotto_centri_costo, 0)
end if
end event

type cb_duplica_prodotto from commandbutton within w_prodotti
integer x = 91
integer y = 760
integer width = 366
integer height = 80
integer taborder = 200
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Du&plica"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_2 = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"des_prodotto")
window_open(w_duplica_prodotto,0)
end event

type cb_lingue from commandbutton within w_prodotti
integer x = 2971
integer y = 120
integer width = 375
integer height = 80
integer taborder = 170
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Lingue"
end type

on clicked;window_open_parm(w_prodotti_lingue, -1, dw_prodotti_lista)

end on

type cb_note from commandbutton within w_prodotti
integer x = 2971
integer y = 20
integer width = 375
integer height = 80
integer taborder = 180
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Note"
end type

on clicked;window_open_parm(w_prodotti_note, -1, dw_prodotti_lista)

end on

type cb_prezzo_acq from commandbutton within w_prodotti
event clicked pbm_bnclicked
integer x = 2706
integer y = 1480
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_prodotti_det_3.setcolumn(48)
s_cs_xx.parametri.parametro_s_1 = dw_prodotti_det_3.gettext()
dw_prodotti_det_3.setcolumn(52)
s_cs_xx.parametri.parametro_d_1 = double(dw_prodotti_det_3.gettext())
dw_prodotti_det_3.setcolumn(49)
s_cs_xx.parametri.parametro_d_2 = double(dw_prodotti_det_3.gettext())

window_open(w_prezzo_um_prodotti, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_prodotti_det_3.setitem(dw_prodotti_det_3.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
end if
end event

type cb_prezzo_ven from commandbutton within w_prodotti
integer x = 2377
integer y = 1240
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_prodotti_det_4.setcolumn(58)
s_cs_xx.parametri.parametro_s_1 = dw_prodotti_det_4.gettext()
dw_prodotti_det_4.setcolumn(62)
s_cs_xx.parametri.parametro_d_1 = double(dw_prodotti_det_4.gettext())
dw_prodotti_det_4.setcolumn(59)
s_cs_xx.parametri.parametro_d_2 = double(dw_prodotti_det_4.gettext())

window_open(w_prezzo_um_prodotti, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_prodotti_det_4.setitem(dw_prodotti_det_4.getrow(), "prezzo_vendita", s_cs_xx.parametri.parametro_d_1)
end if
end event

type cb_reset from commandbutton within w_prodotti
event clicked pbm_bnclicked
integer x = 2057
integer y = 500
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_data_blocco
date ldt_data_blocco

//Donato 23-02-2009 la ricerca per data dava errore
dw_ricerca.accepttext()

if not isnull(dw_ricerca.getitemdate(1, "rd_data_blocco")) then
	ldt_data_blocco = dw_ricerca.getitemdate(1, "rd_data_blocco")
	ls_data_blocco = "'"+string(ldt_data_blocco, s_cs_xx.db_funzioni.formato_data)+"'"
end if
dw_ricerca.setitem(1, "rs_data_blocco", ls_data_blocco)

//fine modifica --------------

dw_ricerca.fu_buildsearch(TRUE)
dw_folder_search.fu_selecttab(1)
dw_prodotti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

cb_chiavi.enabled=true

end event

type cb_ric_prod_1 from cb_prod_ricerca within w_prodotti
integer x = 3008
integer y = 1660
integer width = 69
integer height = 80
integer taborder = 210
boolean bringtotop = true
fontcharset fontcharset = ansi!
end type

event getfocus;call super::getfocus;dw_prodotti_det_5.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cauzione"
end event

type cb_ricerca from commandbutton within w_prodotti
event clicked pbm_bnclicked
integer x = 2446
integer y = 500
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_trova_distinta from commandbutton within w_prodotti
integer x = 2971
integer y = 520
integer width = 366
integer height = 80
integer taborder = 131
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Trova Dist."
end type

event clicked;window_open_parm(w_elenco_pf,-1, dw_prodotti_lista)


end event

