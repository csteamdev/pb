﻿$PBExportHeader$w_prodotti_barcode.srw
$PBExportComments$Finestra Gestione Prodotti Lingue
forward
global type w_prodotti_barcode from w_cs_xx_principale
end type
type dw_prodotti_barcode from uo_cs_xx_dw within w_prodotti_barcode
end type
end forward

global type w_prodotti_barcode from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1902
integer height = 1148
string title = "Barcode"
dw_prodotti_barcode dw_prodotti_barcode
end type
global w_prodotti_barcode w_prodotti_barcode

event pc_setwindow;call super::pc_setwindow;dw_prodotti_barcode.set_dw_key("cod_azienda")
dw_prodotti_barcode.set_dw_key("cod_prodotto")
dw_prodotti_barcode.set_dw_options(sqlca, &
                              i_openparm, &
                              c_scrollparent, &
                              c_default)

end event

on w_prodotti_barcode.create
int iCurrent
call super::create
this.dw_prodotti_barcode=create dw_prodotti_barcode
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_barcode
end on

on w_prodotti_barcode.destroy
call super::destroy
destroy(this.dw_prodotti_barcode)
end on

type dw_prodotti_barcode from uo_cs_xx_dw within w_prodotti_barcode
integer x = 23
integer y = 20
integer width = 1829
integer height = 1000
string dataobject = "d_prodotti_barcode"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_prodotto


ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end on

