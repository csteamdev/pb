﻿$PBExportHeader$w_prodotti_blob.srw
$PBExportComments$Finestra Gestione Prodotti Blob
forward
global type w_prodotti_blob from w_cs_xx_principale
end type
type dw_prodotti_blob_lista from uo_cs_xx_dw within w_prodotti_blob
end type
type dw_prodotti_blob_det from uo_cs_xx_dw within w_prodotti_blob
end type
type cb_note_esterne from commandbutton within w_prodotti_blob
end type
type cb_imm_liv_5 from commandbutton within w_prodotti_blob
end type
end forward

global type w_prodotti_blob from w_cs_xx_principale
integer width = 2592
integer height = 1552
string title = "Gestione Documenti Prodotti"
dw_prodotti_blob_lista dw_prodotti_blob_lista
dw_prodotti_blob_det dw_prodotti_blob_det
cb_note_esterne cb_note_esterne
cb_imm_liv_5 cb_imm_liv_5
end type
global w_prodotti_blob w_prodotti_blob

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_prodotti_blob_lista.set_dw_key("cod_azienda")
dw_prodotti_blob_lista.set_dw_key("cod_prodotto")
dw_prodotti_blob_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_prodotti_blob_det.set_dw_options(sqlca, &
                                    dw_prodotti_blob_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main=dw_prodotti_blob_lista

cb_note_esterne.enabled = false

end on

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false

end on

on w_prodotti_blob.create
int iCurrent
call super::create
this.dw_prodotti_blob_lista=create dw_prodotti_blob_lista
this.dw_prodotti_blob_det=create dw_prodotti_blob_det
this.cb_note_esterne=create cb_note_esterne
this.cb_imm_liv_5=create cb_imm_liv_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_blob_lista
this.Control[iCurrent+2]=this.dw_prodotti_blob_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_imm_liv_5
end on

on w_prodotti_blob.destroy
call super::destroy
destroy(this.dw_prodotti_blob_lista)
destroy(this.dw_prodotti_blob_det)
destroy(this.cb_note_esterne)
destroy(this.cb_imm_liv_5)
end on

type dw_prodotti_blob_lista from uo_cs_xx_dw within w_prodotti_blob
integer x = 23
integer y = 20
integer width = 2126
integer height = 500
integer taborder = 10
string dataobject = "d_prodotti_blob_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_prodotto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_prodotto


ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_prodotto")) then
      cb_note_esterne.enabled = true
   else
      cb_note_esterne.enabled = false
   end if
end if
end on

type dw_prodotti_blob_det from uo_cs_xx_dw within w_prodotti_blob
integer x = 23
integer y = 540
integer width = 2514
integer height = 880
integer taborder = 20
string dataobject = "d_prodotti_blob_det"
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_prodotti_blob
integer x = 2171
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_prodotto, ls_cod_blob, ls_db
integer li_i, li_risposta

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_prodotti_blob_lista.getrow()
ls_cod_prodotto = dw_prodotti_blob_lista.getitemstring(li_i, "cod_prodotto")
ls_cod_blob = dw_prodotti_blob_lista.getitemstring(li_i, "cod_blob")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob anag_prodotti_blob.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_prodotti_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_prodotto = :ls_cod_prodotto and 
	           cod_blob = :ls_cod_blob
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob anag_prodotti_blob.blob
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_prodotti_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_prodotto = :ls_cod_prodotto and 
	           cod_blob = :ls_cod_blob;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob anag_prodotti_blob
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_prodotto = :ls_cod_prodotto and 
	              cod_blob = :ls_cod_blob
		using sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob anag_prodotti_blob
	   set        blob = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_prodotto = :ls_cod_prodotto and 
	              cod_blob = :ls_cod_blob;
	
	end if

   update     anag_prodotti_blob
   set        path_documento = :s_cs_xx.parametri.parametro_s_1
   where      cod_azienda = :s_cs_xx.cod_azienda and
              cod_prodotto = :ls_cod_prodotto and 
              cod_blob = :ls_cod_blob;

   commit;
end if
end event

type cb_imm_liv_5 from commandbutton within w_prodotti_blob
event clicked pbm_bnclicked
integer x = 2171
integer y = 116
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Immagine"
end type

event clicked;string ls_path, ls_nomefile, ls_cod_prodotto, ls_cod_blob
integer li_return


ls_cod_prodotto = dw_prodotti_blob_lista.getitemstring(dw_prodotti_blob_lista.getrow(),"cod_prodotto")
ls_cod_blob = dw_prodotti_blob_lista.getitemstring(dw_prodotti_blob_lista.getrow(),"cod_blob")
if isnull(ls_cod_prodotto) then return

li_return = getfileopenname("Selezionare un file immagine",  &
	                         + ls_path, ls_nomefile, "BMP",  &
                            + "BMP Files (*.BMP),*.BMP,")


update anag_prodotti_blob  
set    path_documento = :ls_nomefile
where  anag_prodotti_blob.cod_azienda = :s_cs_xx.cod_azienda and  
       anag_prodotti_blob.cod_prodotto = :ls_cod_prodotto and 
       anag_prodotti_blob.cod_blob = :ls_cod_blob ;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante l'aggiornamento del nome file", Information!)
end if
commit;
end event

