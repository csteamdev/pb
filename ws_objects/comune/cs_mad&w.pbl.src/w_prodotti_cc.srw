﻿$PBExportHeader$w_prodotti_cc.srw
$PBExportComments$window che contiene la dw_anag_prodotti_cc
forward
global type w_prodotti_cc from w_cs_xx_principale
end type
type dw_prodotti_cc from uo_cs_xx_dw within w_prodotti_cc
end type
end forward

global type w_prodotti_cc from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2350
integer height = 680
string title = "Centro di Costo Prodotti"
dw_prodotti_cc dw_prodotti_cc
end type
global w_prodotti_cc w_prodotti_cc

on w_prodotti_cc.create
int iCurrent
call super::create
this.dw_prodotti_cc=create dw_prodotti_cc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_cc
end on

on w_prodotti_cc.destroy
call super::destroy
destroy(this.dw_prodotti_cc)
end on

event pc_setwindow;call super::pc_setwindow;dw_prodotti_cc.set_dw_key("cod_azienda")
dw_prodotti_cc.set_dw_key("cod_prodotto")
dw_prodotti_cc.set_dw_key("cod_centro_costo")

// opzioni per collegamento db, gestione parametri, e window etc…
dw_prodotti_cc.set_dw_options(sqlca, &
                            		i_openparm, &
                            		c_scrollparent, &
                           		c_default)

end event

event pc_setddlb;call super::pc_setddlb;	f_po_loaddddw_dw(dw_prodotti_cc, &
                 	  "cod_centro_costo", &
                 		sqlca, &
                    "tab_centri_costo", &
                    "cod_centro_costo", &
                    "des_centro_costo", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event closequery;long   l_errore, ll_i, ll_count, ll_percentuale

ll_count = 0

// controlla se c'è almeno una riga.
//this.setFocus()
dw_prodotti_cc.acceptText()
if dw_prodotti_cc.rowcount() > 0 then
	//se c'è una riga controlla che il totale sia cento
	for ll_i = 1 to dw_prodotti_cc.rowcount() 
   	ll_count +=dw_prodotti_cc.getitemnumber(ll_i, "percentuale")  		
	next
//se il totale non è cento lancia un messaggio di avvertimento e non permette la chiusura
	if ll_count <> 100 or isnull(ll_count) then
		g_mb.messagebox("Errore", "Attenzione: la somma delle percentuali non è cento")
		return 1
		
	else
		call super::closequery
		return 0
	end if
	
else
	call super::closequery
	
end if


end event

type dw_prodotti_cc from uo_cs_xx_dw within w_prodotti_cc
integer width = 2304
integer height = 544
integer taborder = 10
string dataobject = "d_prodotti_cc"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
long   l_errore

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

l_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_prodotto
long   l_errore, ll_i

ls_cod_prodotto= i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
	
this.SetFocus ( )
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
  	if isnull(this.getitemstring(ll_i, "cod_prodotto")) or &
      this.getitemstring(ll_i, "cod_prodotto") = '' then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
	
	next

end event

event itemchanged;call super::itemchanged;
decimal  ld_percentuale

ld_percentuale = dec(data)
if Dwo.name = "percentuale" then
	if ld_percentuale > 100 or ld_percentuale <= 0 then
		//errato
		g_mb.messagebox("Errore", "Attenzione: il valore inserito non è valido")
     	return -1
	end if
else
	return 0
end if


end event

