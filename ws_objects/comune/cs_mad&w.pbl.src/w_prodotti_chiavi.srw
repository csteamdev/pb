﻿$PBExportHeader$w_prodotti_chiavi.srw
$PBExportComments$Finestra Chiavi per Prodotto
forward
global type w_prodotti_chiavi from w_cs_xx_principale
end type
type dw_prodotti_chiavi from uo_cs_xx_dw within w_prodotti_chiavi
end type
end forward

global type w_prodotti_chiavi from w_cs_xx_principale
int Width=1911
int Height=1145
boolean TitleBar=true
string Title="Chiavi per Prodotto"
dw_prodotti_chiavi dw_prodotti_chiavi
end type
global w_prodotti_chiavi w_prodotti_chiavi

on w_prodotti_chiavi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_prodotti_chiavi=create dw_prodotti_chiavi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_prodotti_chiavi
end on

on w_prodotti_chiavi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_prodotti_chiavi)
end on

event pc_setwindow;call super::pc_setwindow;dw_prodotti_chiavi.set_dw_key("cod_azienda")
dw_prodotti_chiavi.set_dw_key("cod_prodotto")
dw_prodotti_chiavi.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
iuo_dw_main = dw_prodotti_chiavi


end event

type dw_prodotti_chiavi from uo_cs_xx_dw within w_prodotti_chiavi
int X=23
int Y=21
int Width=1829
int Height=1001
int TabOrder=20
string DataObject="d_prodotti_chiavi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_n_righe
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ll_n_righe = this.rowcount()

for ll_i = 1 to ll_n_righe
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end event

