﻿$PBExportHeader$w_prodotti_chiavi_valori.srw
$PBExportComments$Finestra Chiavi per Prodotto
forward
global type w_prodotti_chiavi_valori from w_cs_xx_principale
end type
type dw_prodotti_chiavi_valori from uo_cs_xx_dw within w_prodotti_chiavi_valori
end type
type dw_prodotti_chiavi_valori_lista from uo_cs_xx_dw within w_prodotti_chiavi_valori
end type
end forward

global type w_prodotti_chiavi_valori from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2203
integer height = 984
string title = "Chiavi per Prodotto"
dw_prodotti_chiavi_valori dw_prodotti_chiavi_valori
dw_prodotti_chiavi_valori_lista dw_prodotti_chiavi_valori_lista
end type
global w_prodotti_chiavi_valori w_prodotti_chiavi_valori

type variables
boolean ib_modifica = false
end variables

on w_prodotti_chiavi_valori.create
int iCurrent
call super::create
this.dw_prodotti_chiavi_valori=create dw_prodotti_chiavi_valori
this.dw_prodotti_chiavi_valori_lista=create dw_prodotti_chiavi_valori_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_chiavi_valori
this.Control[iCurrent+2]=this.dw_prodotti_chiavi_valori_lista
end on

on w_prodotti_chiavi_valori.destroy
call super::destroy
destroy(this.dw_prodotti_chiavi_valori)
destroy(this.dw_prodotti_chiavi_valori_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_prodotti_chiavi_valori_lista.set_dw_key("cod_azienda")
dw_prodotti_chiavi_valori_lista.set_dw_key("cod_prodotto")

dw_prodotti_chiavi_valori.ib_proteggi_chiavi=false

dw_prodotti_chiavi_valori_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent + c_norefreshparent, &
                                      c_default)
dw_prodotti_chiavi_valori.set_dw_options(sqlca, &
                                      dw_prodotti_chiavi_valori_lista, &
												  c_sharedata + c_scrollparent, &
												  c_default)
												  
iuo_dw_main = dw_prodotti_chiavi_valori_lista

ib_modifica = false


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
                 "cod_chiave", &
                 sqlca, &
                 "tab_chiavi", &
                 "cod_chiave", &
                 "des_chiave", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
//                 "progressivo", &
//                 sqlca, &
//                 "tab_chiavi_valori", &
//                 "progressivo", &
//                 "valore", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

event close;call super::close;if ib_modifica then
	dw_prodotti_chiavi_valori_lista.i_parentdw.postevent("ue_aggiorna_descrizione")
end if
end event

type dw_prodotti_chiavi_valori from uo_cs_xx_dw within w_prodotti_chiavi_valori
integer x = 32
integer y = 536
integer width = 2098
integer height = 324
integer taborder = 30
string dataobject = "d_prodotti_chiavi_valori"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;long ll_null, ll_i

if i_extendmode then
	setnull(ll_null)
	
	choose case i_colname
		case "cod_chiave"
			if not isnull(i_coltext) and i_coltext <> "" then
				
				f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
									  "progressivo", &
									  sqlca, &
									  "tab_chiavi_valori", &
									  "progressivo", &
									  "valore", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '" + i_coltext + "' ")		
									  
				setitem(ll_i,"progressivo", ll_null)
				
			else
				
				f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
									  "progressivo", &
									  sqlca, &
									  "tab_chiavi_valori", &
									  "progressivo", &
									  "valore", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
									  
				setitem(ll_i,"progressivo", ll_null)
				
			end if
	end choose
end if
end event

type dw_prodotti_chiavi_valori_lista from uo_cs_xx_dw within w_prodotti_chiavi_valori
integer x = 23
integer y = 20
integer width = 2103
integer height = 500
integer taborder = 20
string dataobject = "d_anag_prodotti_chiavi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_n_righe
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ll_n_righe = this.rowcount()

for ll_i = 1 to ll_n_righe
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end event

event updateend;call super::updateend;ib_modifica = true
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if getrow() > 0 then
		if not isnull(getitemstring(getrow(),"cod_chiave")) then
			f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
							  "progressivo", &
							  sqlca, &
							  "tab_chiavi_valori", &
							  "progressivo", &
							  "valore", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '" + getitemstring(getrow(),"cod_chiave") + "' ")		
		else
			f_po_loaddddw_dw(dw_prodotti_chiavi_valori, &
							  "progressivo", &
							  sqlca, &
							  "tab_chiavi_valori", &
							  "progressivo", &
							  "valore", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '--' ")		
		end if
	end if
end if
end event

