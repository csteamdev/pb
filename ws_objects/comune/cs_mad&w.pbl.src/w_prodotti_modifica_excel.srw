﻿$PBExportHeader$w_prodotti_modifica_excel.srw
forward
global type w_prodotti_modifica_excel from w_std_principale
end type
type st_13 from statictext within w_prodotti_modifica_excel
end type
type st_12 from statictext within w_prodotti_modifica_excel
end type
type cb_3 from commandbutton within w_prodotti_modifica_excel
end type
type cb_1 from commandbutton within w_prodotti_modifica_excel
end type
type st_11 from statictext within w_prodotti_modifica_excel
end type
type st_10 from statictext within w_prodotti_modifica_excel
end type
type st_9 from statictext within w_prodotti_modifica_excel
end type
type st_8 from statictext within w_prodotti_modifica_excel
end type
type st_7 from statictext within w_prodotti_modifica_excel
end type
type st_6 from statictext within w_prodotti_modifica_excel
end type
type st_5 from statictext within w_prodotti_modifica_excel
end type
type st_4 from statictext within w_prodotti_modifica_excel
end type
type st_3 from statictext within w_prodotti_modifica_excel
end type
type st_2 from statictext within w_prodotti_modifica_excel
end type
type st_1 from statictext within w_prodotti_modifica_excel
end type
type st_file from statictext within w_prodotti_modifica_excel
end type
type cb_2 from commandbutton within w_prodotti_modifica_excel
end type
end forward

global type w_prodotti_modifica_excel from w_std_principale
integer width = 3118
integer height = 1920
string title = "Modifica Anagrafica Prodotti"
st_13 st_13
st_12 st_12
cb_3 cb_3
cb_1 cb_1
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
st_file st_file
cb_2 cb_2
end type
global w_prodotti_modifica_excel w_prodotti_modifica_excel

type variables
OLEObject iole_excel
string is_path_file

end variables

on w_prodotti_modifica_excel.create
int iCurrent
call super::create
this.st_13=create st_13
this.st_12=create st_12
this.cb_3=create cb_3
this.cb_1=create cb_1
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.st_file=create st_file
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_13
this.Control[iCurrent+2]=this.st_12
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.st_11
this.Control[iCurrent+6]=this.st_10
this.Control[iCurrent+7]=this.st_9
this.Control[iCurrent+8]=this.st_8
this.Control[iCurrent+9]=this.st_7
this.Control[iCurrent+10]=this.st_6
this.Control[iCurrent+11]=this.st_5
this.Control[iCurrent+12]=this.st_4
this.Control[iCurrent+13]=this.st_3
this.Control[iCurrent+14]=this.st_2
this.Control[iCurrent+15]=this.st_1
this.Control[iCurrent+16]=this.st_file
this.Control[iCurrent+17]=this.cb_2
end on

on w_prodotti_modifica_excel.destroy
call super::destroy
destroy(this.st_13)
destroy(this.st_12)
destroy(this.cb_3)
destroy(this.cb_1)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.st_file)
destroy(this.cb_2)
end on

type st_13 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 1680
integer width = 3017
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12639424
boolean focusrectangle = false
end type

type st_12 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 1260
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "La prima riga del foglio Excel non viene elaborata perchè viene considerata come intestazione."
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_prodotti_modifica_excel
integer x = 1280
integer y = 1480
integer width = 777
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "2 - Esegui Elaborazione"
end type

event clicked;boolean lb_ret
string ls_cod_prodotto,ls_cod_comodo_2,ls_cod_prodotto_alternativo,ls_cod_prodotto_raggruppato,ls_flag_lifo,ls_flag_articolo_fiscale,ls_flag_classe_abc,ls_cod_tipo_politica_riordino
long ll_riga
uo_excel luo_excel

luo_excel = create uo_excel
lb_ret = luo_excel.uof_open( is_path_file, false, true)
luo_excel.uof_set_sheet( "Foglio1")

ll_riga = 1

do while true
	ll_riga ++
	
	ls_cod_prodotto = luo_excel.uof_read_string( ll_riga, "A")
	if ls_cod_prodotto = "FINE" then exit
	ls_cod_comodo_2 = luo_excel.uof_read_string( ll_riga, "D")
	ls_cod_prodotto_alternativo = luo_excel.uof_read_string( ll_riga, "E")
	ls_cod_prodotto_raggruppato = luo_excel.uof_read_string( ll_riga, "F")
	ls_flag_lifo = luo_excel.uof_read_string( ll_riga, "G")
	ls_flag_articolo_fiscale = luo_excel.uof_read_string( ll_riga, "H")
	ls_flag_classe_abc = luo_excel.uof_read_string( ll_riga, "I")
	ls_cod_tipo_politica_riordino = luo_excel.uof_read_string( ll_riga, "J")
	
	st_13.text = string(ll_riga) + " " + ls_cod_prodotto  
	Yield()
	
	if len(ls_cod_prodotto) > 0 and not isnull(ls_cod_prodotto) then
		
		if len(ls_cod_comodo_2) > 0 and not isnull(ls_cod_comodo_2) then
			update anag_prodotti
			set cod_comodo_2 = :ls_cod_comodo_2
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " COMODO 2~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_cod_prodotto_alternativo) > 0 and not isnull(ls_cod_prodotto_alternativo) then
			update anag_prodotti
			set cod_prodotto_alt = :ls_cod_prodotto_alternativo
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " PRODOTTO ALTERNATIVO~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_cod_prodotto_raggruppato) > 0 and not isnull(ls_cod_prodotto_raggruppato) then
			update anag_prodotti
			set cod_prodotto_raggruppato = :ls_cod_prodotto_raggruppato
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " PRODOTTO ALTERNATIVO~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_flag_lifo) > 0 and not isnull(ls_flag_lifo) then
			update anag_prodotti
			set flag_lifo = :ls_flag_lifo
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + "LIFO~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_flag_articolo_fiscale) > 0 and not isnull(ls_flag_articolo_fiscale) then
			update anag_prodotti
			set flag_articolo_fiscale = :ls_flag_articolo_fiscale
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " FISCALE~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_flag_classe_abc) > 0 and not isnull(ls_flag_classe_abc) then
			update anag_prodotti
			set flag_classe_abc = :ls_flag_classe_abc
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " CLASSE~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
			
		if len(ls_cod_tipo_politica_riordino) > 0 and not isnull(ls_cod_tipo_politica_riordino) then
			update anag_prodotti
			set cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				g_mb.error("RIGA " + string(ll_riga) + " POLITICA~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		end if
	end if
	commit;
loop

commit;

destroy luo_excel

return
end event

type cb_1 from commandbutton within w_prodotti_modifica_excel
integer x = 1280
integer y = 1360
integer width = 786
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 - Test Lettura Foglio Execel"
end type

event clicked;boolean lb_ret
string ls_cod_prodotto,ls_cod_comodo_2,ls_cod_prodotto_alternativo,ls_cod_prodotto_raggruppato,ls_flag_lifo,ls_flag_articolo_fiscale,ls_flag_classe_abc,ls_cod_tipo_politica_riordino
long ll_riga
uo_excel luo_excel

luo_excel = create uo_excel
lb_ret = luo_excel.uof_open( is_path_file, false, true)
luo_excel.uof_set_sheet( "Foglio1")

ll_riga = 1

do while true
	ll_riga ++
	
	ls_cod_prodotto = luo_excel.uof_read_string( ll_riga, "A")
	if ls_cod_prodotto = "FINE" then exit
	ls_cod_comodo_2 = luo_excel.uof_read_string( ll_riga, "D")
	ls_cod_prodotto_alternativo = luo_excel.uof_read_string( ll_riga, "E")
	ls_cod_prodotto_raggruppato = luo_excel.uof_read_string( ll_riga, "F")
	ls_flag_lifo = luo_excel.uof_read_string( ll_riga, "G")
	ls_flag_articolo_fiscale = luo_excel.uof_read_string( ll_riga, "H")
	ls_flag_classe_abc = luo_excel.uof_read_string( ll_riga, "I")
	ls_cod_tipo_politica_riordino = luo_excel.uof_read_string( ll_riga, "J")
	
	st_13.text = string(ll_riga) + " " + ls_cod_prodotto  
	Yield()
loop

//luo_excel.uof_close( )
destroy luo_excel

return
end event

type st_11 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 1180
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA J: codice tipo politica di riordino"
boolean focusrectangle = false
end type

type st_10 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 1100
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA I: flag_classe  - Valori ABC"
boolean focusrectangle = false
end type

type st_9 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 1020
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA H: flag_articolo fiscale - Valori SN"
boolean focusrectangle = false
end type

type st_8 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 940
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA G: flag LIFO - Valori SN"
boolean focusrectangle = false
end type

type st_7 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 860
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA F: codice prodotto raggruppato "
boolean focusrectangle = false
end type

type st_6 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 780
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA E: codice prodotto alternativo"
boolean focusrectangle = false
end type

type st_5 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 700
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA D: codice comodo 2"
boolean focusrectangle = false
end type

type st_4 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 620
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "COLONNA A: CODICE PRODOTTO (non viene modificato ma serve per sapere quale prodotto fare le modifiche)"
boolean focusrectangle = false
end type

type st_3 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 540
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Formato del foglio:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_prodotti_modifica_excel
integer x = 23
integer y = 460
integer width = 3017
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Questa finestra provvede a modificare alcune colonne della anagrafica prodotti partendo da un foglio EXCEL."
boolean focusrectangle = false
end type

type st_1 from statictext within w_prodotti_modifica_excel
integer x = 27
integer y = 20
integer width = 3003
integer height = 124
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16777215
long backcolor = 0
string text = "Utility di gestione dati globali per fusione tabelle e anagrafiche gruppo Gibus"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_file from statictext within w_prodotti_modifica_excel
integer x = 18
integer y = 276
integer width = 2770
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Selezionare il file da elaborare"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_prodotti_modifica_excel
integer x = 2816
integer y = 268
integer width = 233
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "File"
end type

event clicked;string		ls_path, docpath, docname[]
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", 	+ "Files Excel (*.XLS),*.XLS,", 	ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	st_file.text = docpath
	is_path_file = docpath
end if
end event

