﻿$PBExportHeader$w_prodotti_note.srw
$PBExportComments$Finestra Gestione Prodotti Note
forward
global type w_prodotti_note from w_cs_xx_principale
end type
type dw_prodotti_note_lista from uo_cs_xx_dw within w_prodotti_note
end type
type dw_prodotti_note_det from uo_cs_xx_dw within w_prodotti_note
end type
type cb_lingue from commandbutton within w_prodotti_note
end type
end forward

global type w_prodotti_note from w_cs_xx_principale
integer width = 2615
integer height = 1400
string title = "Gestione Note Prodotti"
dw_prodotti_note_lista dw_prodotti_note_lista
dw_prodotti_note_det dw_prodotti_note_det
cb_lingue cb_lingue
end type
global w_prodotti_note w_prodotti_note

type variables

end variables

on pc_delete;call w_cs_xx_principale::pc_delete;cb_lingue.enabled=false
end on

event pc_setwindow;call super::pc_setwindow;dw_prodotti_note_lista.uof_imposta_limite_colonne_note(5000)
dw_prodotti_note_det.uof_imposta_limite_colonne_note(5000)

dw_prodotti_note_lista.set_dw_key("cod_azienda")
dw_prodotti_note_lista.set_dw_key("cod_prodotto")
dw_prodotti_note_lista.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)
dw_prodotti_note_det.set_dw_options(sqlca, &
                                    dw_prodotti_note_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
iuo_dw_main = dw_prodotti_note_lista

cb_lingue.enabled=false
end event

on w_prodotti_note.create
int iCurrent
call super::create
this.dw_prodotti_note_lista=create dw_prodotti_note_lista
this.dw_prodotti_note_det=create dw_prodotti_note_det
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_note_lista
this.Control[iCurrent+2]=this.dw_prodotti_note_det
this.Control[iCurrent+3]=this.cb_lingue
end on

on w_prodotti_note.destroy
call super::destroy
destroy(this.dw_prodotti_note_lista)
destroy(this.dw_prodotti_note_det)
destroy(this.cb_lingue)
end on

type dw_prodotti_note_lista from uo_cs_xx_dw within w_prodotti_note
integer x = 23
integer y = 20
integer width = 2537
integer height = 500
integer taborder = 10
string dataobject = "d_prodotti_note_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_nota_prodotto")) then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_nota_prodotto")) then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_cod_prodotto, ls_cod_nota_prodotto

   for li_i = 1 to this.deletedcount()
      ls_cod_prodotto = this.getitemstring(li_i, "cod_prodotto", delete!, true)
      ls_cod_nota_prodotto = this.getitemstring(li_i, "cod_nota_prodotto", delete!, true)

      delete from anag_prodotti_note_lingue 
       where anag_prodotti_note_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_prodotti_note_lingue.cod_prodotto = :ls_cod_prodotto and 
             anag_prodotti_note_lingue.cod_nota_prodotto = :ls_cod_nota_prodotto;

      if sqlca.sqlcode <> 0 then
         g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di cancellazione.", &
                    exclamation!, ok!)
         return
      end if
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

type dw_prodotti_note_det from uo_cs_xx_dw within w_prodotti_note
integer x = 23
integer y = 540
integer width = 2537
integer height = 640
integer taborder = 30
string dataobject = "d_prodotti_note_det"
borderstyle borderstyle = styleraised!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   dw_prodotti_note_lista.postevent(updatestart!)
end if
end on

type cb_lingue from commandbutton within w_prodotti_note
integer x = 2194
integer y = 1200
integer width = 375
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_prodotti_note_lingue, -1, dw_prodotti_note_lista)

end on

