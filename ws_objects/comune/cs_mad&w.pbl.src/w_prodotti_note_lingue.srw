﻿$PBExportHeader$w_prodotti_note_lingue.srw
$PBExportComments$Finestra Gestione Prodotti Note Lingue
forward
global type w_prodotti_note_lingue from w_cs_xx_principale
end type
type dw_prodotti_note_lingue_lista from uo_cs_xx_dw within w_prodotti_note_lingue
end type
type dw_prodotti_note_lingue_det from uo_cs_xx_dw within w_prodotti_note_lingue
end type
end forward

global type w_prodotti_note_lingue from w_cs_xx_principale
integer width = 2619
integer height = 1308
string title = "Gestione Note Prodotti Lingue"
dw_prodotti_note_lingue_lista dw_prodotti_note_lingue_lista
dw_prodotti_note_lingue_det dw_prodotti_note_lingue_det
end type
global w_prodotti_note_lingue w_prodotti_note_lingue

event pc_setwindow;call super::pc_setwindow;dw_prodotti_note_lingue_lista.uof_imposta_limite_colonne_note(5000)
dw_prodotti_note_lingue_det.uof_imposta_limite_colonne_note(5000)

dw_prodotti_note_lingue_lista.set_dw_key("cod_azienda")
dw_prodotti_note_lingue_lista.set_dw_key("cod_prodotto")
dw_prodotti_note_lingue_lista.set_dw_key("cod_nota_prodotto")

dw_prodotti_note_lingue_lista.set_dw_options(sqlca, &
                                             i_openparm, &
                                             c_scrollparent, &
                                             c_default)
dw_prodotti_note_lingue_det.set_dw_options(sqlca, &
                                           dw_prodotti_note_lingue_lista, &
                                           c_sharedata + c_scrollparent, &
                                           c_default)
iuo_dw_main = dw_prodotti_note_lingue_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_prodotti_note_lingue_det, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_prodotti_note_lingue.create
int iCurrent
call super::create
this.dw_prodotti_note_lingue_lista=create dw_prodotti_note_lingue_lista
this.dw_prodotti_note_lingue_det=create dw_prodotti_note_lingue_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_note_lingue_lista
this.Control[iCurrent+2]=this.dw_prodotti_note_lingue_det
end on

on w_prodotti_note_lingue.destroy
call super::destroy
destroy(this.dw_prodotti_note_lingue_lista)
destroy(this.dw_prodotti_note_lingue_det)
end on

type dw_prodotti_note_lingue_lista from uo_cs_xx_dw within w_prodotti_note_lingue
integer x = 23
integer y = 20
integer width = 2537
integer height = 500
integer taborder = 10
string dataobject = "d_prodotti_note_lingue_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_prodotto, ls_cod_nota_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_nota_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_nota_prodotto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_prodotto")) then
      this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
   end if
   if isnull(this.getitemstring(ll_i, "cod_nota_prodotto")) then
      this.setitem(ll_i, "cod_nota_prodotto", ls_cod_nota_prodotto)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_prodotto, ls_cod_nota_prodotto

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_cod_nota_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_nota_prodotto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto, ls_cod_nota_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_prodotti_note_lingue_det from uo_cs_xx_dw within w_prodotti_note_lingue
integer x = 23
integer y = 540
integer width = 2537
integer height = 640
integer taborder = 20
string dataobject = "d_prodotti_note_lingue_det"
borderstyle borderstyle = styleraised!
end type

