﻿$PBExportHeader$w_prodotti_reparti_depositi.srw
forward
global type w_prodotti_reparti_depositi from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_prodotti_reparti_depositi
end type
end forward

global type w_prodotti_reparti_depositi from w_cs_xx_principale
integer width = 3954
integer height = 1356
string title = "Stabilimenti Origine e Produzione"
dw_lista dw_lista
end type
global w_prodotti_reparti_depositi w_prodotti_reparti_depositi

forward prototypes
public function integer wf_filtra_reparti (string fs_cod_deposito_produzione)
end prototypes

public function integer wf_filtra_reparti (string fs_cod_deposito_produzione);datawindowchild  		ldwc_child
integer					li_child
string						ls_filter

li_child = dw_lista.GetChild("cod_reparto_produzione", ldwc_child)


if fs_cod_deposito_produzione="" or isnull(fs_cod_deposito_produzione) then
	//fai vedere tutti i reparti
	ls_filter = ""
else
	//filtra i reparti per deposito produzione
	ls_filter = "cod_deposito='"+fs_cod_deposito_produzione+"'"
end if

ldwc_child.setfilter(ls_filter)
ldwc_child.filter()

return 1
end function

event pc_setddlb;call super::pc_setddlb;
//f_po_loaddddw_dw(dw_lista, &
//                 "cod_deposito_origine", &
//                 sqlca, &
//                 "anag_depositi", &
//                 "cod_deposito", &
//                 "des_deposito", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_origine", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_produzione", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_lista,"cod_reparto_produzione",sqlca,&
//                 "anag_reparti","cod_reparto","des_reparto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
datawindowchild		ldwc_child

dw_lista.getchild("cod_reparto_produzione", ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Retrieve()
//------------------------






end event

on w_prodotti_reparti_depositi.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_prodotti_reparti_depositi.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_prodotto")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)
end event

type dw_lista from uo_cs_xx_dw within w_prodotti_reparti_depositi
integer x = 23
integer width = 3872
integer height = 1232
integer taborder = 10
string dataobject = "d_prodotti_reparti_depositi"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_prodotto
long ll_errore

ls_cod_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")

parent.title = "Stabilimenti Origine e Produzione (" + ls_cod_prodotto + ")"

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string		ls_cod_prodotto
long		ll_i, ll_selected_row

ll_selected_row = i_parentdw.i_selectedrows[1]

ls_cod_prodotto =  i_parentdw.getitemstring(ll_selected_row, "cod_prodotto")

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) or this.getitemstring(ll_i, "cod_azienda")="" then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_prodotto")) or this.getitemstring(ll_i, "cod_prodotto") = "" then
		this.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	end if
next

end event

event pcd_validaterow;call super::pcd_validaterow;long		ll_i

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_deposito_origine")) or this.getitemstring(ll_i, "cod_deposito_origine")="" then
		g_mb.error("APICE","Manca il Deposito Origine in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_deposito_produzione")) or this.getitemstring(ll_i, "cod_deposito_produzione") = "" then
		g_mb.error("APICE","Manca il Deposito Produzione in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
	if isnull(this.getitemstring(ll_i, "cod_reparto_produzione")) or this.getitemstring(ll_i, "cod_reparto_produzione") = "" then
		g_mb.error("APICE","Manca il Reparto Produzione in qualche riga!")
		pcca.error = c_fatal
		return
	end if
	
next

end event

event itemchanged;call super::itemchanged;

if row > 0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito_produzione"
		wf_filtra_reparti(data)
		
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_deposito_produzione

if currentrow > 0 then
else
	return
end if

ls_cod_deposito_produzione = getitemstring(currentrow, "cod_deposito_produzione")
wf_filtra_reparti(ls_cod_deposito_produzione)
end event

