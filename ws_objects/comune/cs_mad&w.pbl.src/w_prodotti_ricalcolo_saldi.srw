﻿$PBExportHeader$w_prodotti_ricalcolo_saldi.srw
$PBExportComments$Finestra RESPONSE di riclacolo saldi anag_prodotti
forward
global type w_prodotti_ricalcolo_saldi from window
end type
type cb_1 from commandbutton within w_prodotti_ricalcolo_saldi
end type
type em_data_ultimo_movimento from editmask within w_prodotti_ricalcolo_saldi
end type
type em_data_chisura from editmask within w_prodotti_ricalcolo_saldi
end type
type st_5 from statictext within w_prodotti_ricalcolo_saldi
end type
type st_4 from statictext within w_prodotti_ricalcolo_saldi
end type
type st_3 from statictext within w_prodotti_ricalcolo_saldi
end type
type st_2 from statictext within w_prodotti_ricalcolo_saldi
end type
type st_1 from statictext within w_prodotti_ricalcolo_saldi
end type
type ln_1 from line within w_prodotti_ricalcolo_saldi
end type
end forward

global type w_prodotti_ricalcolo_saldi from window
integer width = 2190
integer height = 1116
boolean titlebar = true
string title = "RICALCOLO PROGRESSIVI ANAGRAFICA PRODOTTI"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
cb_1 cb_1
em_data_ultimo_movimento em_data_ultimo_movimento
em_data_chisura em_data_chisura
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
ln_1 ln_1
end type
global w_prodotti_ricalcolo_saldi w_prodotti_ricalcolo_saldi

event open;datetime ldt_data

st_1.text = "LETTURA PARAMETRI DI SISTEMA IN CORSO ..."

select data_chiusura_annuale
into :ldt_data
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;
if isnull(ldt_data) then ldt_data = datetime(date("01/01/1900"), 00:00:00)

em_data_chisura.text = string(ldt_data,"dd/mm/yyyy")


select max(data_registrazione)
into :ldt_data
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;
if isnull(ldt_data) then ldt_data = datetime(date("31/12/2099"), 00:00:00)

em_data_ultimo_movimento.text = string(ldt_data, "dd/mm/yyyy")

st_1.text = "RICALCOLO PROGRESSIVI ANAGRAFICA PRODOTTI"

end event

on w_prodotti_ricalcolo_saldi.create
this.cb_1=create cb_1
this.em_data_ultimo_movimento=create em_data_ultimo_movimento
this.em_data_chisura=create em_data_chisura
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.ln_1=create ln_1
this.Control[]={this.cb_1,&
this.em_data_ultimo_movimento,&
this.em_data_chisura,&
this.st_5,&
this.st_4,&
this.st_3,&
this.st_2,&
this.st_1,&
this.ln_1}
end on

on w_prodotti_ricalcolo_saldi.destroy
destroy(this.cb_1)
destroy(this.em_data_ultimo_movimento)
destroy(this.em_data_chisura)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.ln_1)
end on

type cb_1 from commandbutton within w_prodotti_ricalcolo_saldi
integer x = 891
integer y = 840
integer width = 402
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricalcola"
end type

event clicked;string   ls_cod_prodotto, ls_error, ls_chiave[], ls_where,ls_des_prodotto
integer  li_ret_funz
dec{4}   ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
			ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
			ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
			ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
			ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[], &
			ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_rif
uo_magazzino luo_magazzino

ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then 
	g_mb.messagebox("APICE","Specificare un codice prodotto")
	return
end if

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Il prodotto digitato è inesistente in anagrafica")
	return
end if

ldt_data_rif = datetime(date(em_data_ultimo_movimento.text), 00:00:00)

if isnull(ldt_data_rif) or date(ldt_data_rif) <= date("01/01/1900") then 
	g_mb.messagebox("APICE","Specificare una data chiusura annuale valida")
	return
end if
	
if g_mb.messagebox("APICE","Sei sicuro di voler ricalcolare i saldi della anagrafica del prodotto " + ls_cod_prodotto + "?",Question!,yesno!,2) = 2 then return

if g_mb.messagebox("APICE","I saldi del prodotto " + ls_cod_prodotto + " verranno ricalcolati esaminando i movimenti fino al " + string(ldt_data_rif) + "~r~nProseguo ?",Question!,yesno!,2) = 2 then return

luo_magazzino = CREATE uo_magazzino

li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_rif, ls_where, ref ld_quant_val[], ref ls_error, 'S', ref ls_chiave[], ref ld_giacenza[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

destroy luo_magazzino

// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=val_costo_ultimo
// costo medio = [13]/[12]; [12] e [13]: mov_magazzino con flag_costo_ultimo=S
// costo ultimo: ultimo costo di acquisto alla data di riferimento
if li_ret_funz < 0 then
	g_mb.messagebox("APICE", "Errore nel calcolo della situazione del prodotto ls_cod_prodotto~r~n " + ls_error)
	return
end if
	
update anag_prodotti 
SET saldo_quan_inizio_anno = :ld_quant_val[1],
	val_inizio_anno = :ld_quant_val[2],
	prog_quan_entrata = :ld_quant_val[4],
	val_quan_entrata = :ld_quant_val[5],
	prog_quan_uscita = :ld_quant_val[6],
	val_quan_uscita = :ld_quant_val[7],
	prog_quan_acquistata = :ld_quant_val[8],
	val_quan_acquistata = :ld_quant_val[9],
	prog_quan_venduta = :ld_quant_val[10],
	val_quan_venduta = :ld_quant_val[11]
WHERE  cod_azienda=:s_cs_xx.cod_azienda and 
	cod_prodotto =:ls_cod_prodotto;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore di aggiornamento anagrafica prodotto "+ ls_cod_prodotto + ", " + sqlca.SQLErrText)
	rollback;
	return
end if

commit;

g_mb.messagebox("APICE","Ricalcolo eseguito con successo !!!")
end event

type em_data_ultimo_movimento from editmask within w_prodotti_ricalcolo_saldi
integer x = 1486
integer y = 680
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type em_data_chisura from editmask within w_prodotti_ricalcolo_saldi
integer x = 1760
integer y = 560
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_5 from statictext within w_prodotti_ricalcolo_saldi
integer x = 46
integer y = 680
integer width = 1417
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "ALLA DATA DELL~'ULTIMO MOVIMENTO DI MAGAZZINO:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_prodotti_ricalcolo_saldi
integer x = 46
integer y = 560
integer width = 1691
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "VERRANNO RICALCOLATI I SALDI DALLA ULTIMA CHIUSURA DEL:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_prodotti_ricalcolo_saldi
integer x = 526
integer y = 200
integer width = 1623
integer height = 220
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "QUESTA PROCEDURA DEVE ESSERE ESEGUITA SOLO DALL~'AMMINISTRATORE DEL SISTEMA; UN USO ERRATO PUO~' PROVOCARE DANNI IRREPARABILI."
boolean focusrectangle = false
end type

type st_2 from statictext within w_prodotti_ricalcolo_saldi
integer x = 23
integer y = 200
integer width = 434
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Attenzione !!!"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_prodotti_ricalcolo_saldi
integer x = 23
integer y = 20
integer width = 2126
integer height = 100
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "RICALCOLO PROGRESSIVI ANAGRAFICA PRODOTTI"
alignment alignment = center!
boolean focusrectangle = false
end type

type ln_1 from line within w_prodotti_ricalcolo_saldi
integer linethickness = 3
integer beginx = 46
integer beginy = 120
integer endx = 2149
integer endy = 120
end type

