﻿$PBExportHeader$w_prodotti_ricerca.srw
$PBExportComments$Finestra Prodotti Ricerca
forward
global type w_prodotti_ricerca from w_cs_xx_principale
end type
type cb_annulla_chiavi from uo_cb_close within w_prodotti_ricerca
end type
type cb_reset from commandbutton within w_prodotti_ricerca
end type
type cb_ok_chiavi from commandbutton within w_prodotti_ricerca
end type
type dw_chiavi_ricerca from uo_cs_xx_dw within w_prodotti_ricerca
end type
type st_3 from statictext within w_prodotti_ricerca
end type
type cb_nuovo from commandbutton within w_prodotti_ricerca
end type
type st_1 from statictext within w_prodotti_ricerca
end type
type cb_ok from commandbutton within w_prodotti_ricerca
end type
type cb_annulla from uo_cb_close within w_prodotti_ricerca
end type
type dw_stringa_ricerca_prodotti from datawindow within w_prodotti_ricerca
end type
type st_2 from statictext within w_prodotti_ricerca
end type
type dw_nuovo from uo_cs_xx_dw within w_prodotti_ricerca
end type
type dw_prodotti_lista from datawindow within w_prodotti_ricerca
end type
type dw_folder from u_folder within w_prodotti_ricerca
end type
end forward

global type w_prodotti_ricerca from w_cs_xx_principale
integer width = 3360
integer height = 1744
string title = "Ricerca Prodotti"
cb_annulla_chiavi cb_annulla_chiavi
cb_reset cb_reset
cb_ok_chiavi cb_ok_chiavi
dw_chiavi_ricerca dw_chiavi_ricerca
st_3 st_3
cb_nuovo cb_nuovo
st_1 st_1
cb_ok cb_ok
cb_annulla cb_annulla
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
st_2 st_2
dw_nuovo dw_nuovo
dw_prodotti_lista dw_prodotti_lista
dw_folder dw_folder
end type
global w_prodotti_ricerca w_prodotti_ricerca

type variables
long il_row
string is_tipo_ricerca='C'
end variables

forward prototypes
public subroutine wf_pos_ricerca ()
public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca)
public subroutine wf_dw_select_chiavi ()
end prototypes

public subroutine wf_pos_ricerca ();// s_cs_xx.parametri.parametro_tipo_ricerca  = 1   ----> ricerca per codice prodotto
//															  2   ----> ricerca per descrizione prodotto

dw_prodotti_lista.reset()
if isnull(s_cs_xx.parametri.parametro_tipo_ricerca) or s_cs_xx.parametri.parametro_tipo_ricerca <> 2 then
	is_tipo_ricerca = "C"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca per Codice:"
	//----------------- Modifica Michele perchè non si azzeri la condizione di ricerca -----------------
	if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
		dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
	end if
	//--------------------------------------------------------------------------------------------------
else
	is_tipo_ricerca = "D"
	wf_dw_select(is_tipo_ricerca, s_cs_xx.parametri.parametro_pos_ricerca)
	st_1.text = "Ricerca per Descrizione:"
	//----------------- Modifica Michele perchè non si azzeri la condizione di ricerca -----------------
	if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) then
		dw_stringa_ricerca_prodotti.setitem(1,"stringa_ricerca", s_cs_xx.parametri.parametro_pos_ricerca)
	end if
	//--------------------------------------------------------------------------------------------------
end if	
setnull(s_cs_xx.parametri.parametro_pos_ricerca)
dw_prodotti_lista.setfocus()


end subroutine

public subroutine wf_dw_select (string fs_tipo_ricerca, string fs_stringa_ricerca);string ls_sql, ls_stringa,ls_db, ls_str1, ls_str2
long ll_i, ll_y, ll_ret, ll_pos
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

choose case ls_db
	case "SYBASE_ASE"
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'"
	case else
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( flag_blocco = 'N' or ( flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "' ))"
end choose

// ****************** aggiungo in automatico un asterisco alla fine della stringa *******
if right(trim(fs_stringa_ricerca), 1) <> "*" and pos(fs_stringa_ricerca, "&") < 1 then
	fs_stringa_ricerca += "*"
end if
// ***************************************************************************************

ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next


//****************** correzione ricerca con apostrofo - Michele 23/04/2002 ******************

ll_pos = 1

do

   ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
	
   if ll_pos <> 0 then
      fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
      ll_pos = ll_pos + 2
   end if	

loop while ll_pos <> 0

//*******************************************************************************************


choose case fs_tipo_ricerca
	case "C"  				// tipo ricerca
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and cod_prodotto like '" + fs_stringa_ricerca + "'"
			else
				ls_sql = ls_sql + " and cod_prodotto = '" + fs_stringa_ricerca + "'"
			end if			
		end if
	   ls_sql = ls_sql + " order by cod_prodotto"
	case "D"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and des_prodotto like '" + fs_stringa_ricerca + "'"
			else
				ls_sql = ls_sql + " and des_prodotto = '" + fs_stringa_ricerca + "'"
			end if			
		end if
	   ls_sql = ls_sql + " order by des_prodotto"
	case "M"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "&") > 0 then
				ls_str1 = left(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") - 1)
				ls_str2 = mid(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") + 1)
				ls_sql = ls_sql + " and cod_comodo like '" + ls_str1 + "%' and cod_comodo_2 like '" + ls_str2 + "%'"
			else	
				if pos(fs_stringa_ricerca, "%") > 0 then 
					ls_sql = ls_sql + " and cod_comodo like '" + fs_stringa_ricerca + "'"
				else
					ls_sql = ls_sql + " and cod_comodo = '" + fs_stringa_ricerca + "'"
				end if		
			end if
		end if
	   ls_sql = ls_sql + " order by cod_comodo, cod_comodo_2"
	case "M2"
		if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
			if pos(fs_stringa_ricerca, "&") > 0 then
				ls_str1 = left(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") - 1)
				ls_str2 = mid(fs_stringa_ricerca, pos(fs_stringa_ricerca, "&") + 1)
				ls_sql = ls_sql + " and cod_comodo_2 like '" + ls_str1 + "%' and cod_comodo like '" + ls_str2 + "%'"
			else	
				if pos(fs_stringa_ricerca, "%") > 0 then 
					ls_sql = ls_sql + " and cod_comodo_2 like '" + fs_stringa_ricerca + "'"
				else
					ls_sql = ls_sql + " and cod_comodo_2 = '" + fs_stringa_ricerca + "'"
				end if	
			end if
		end if
	   ls_sql = ls_sql + " order by cod_comodo_2, cod_comodo"
end choose
//ls_stringa = dw_prodotti_lista.Modify("DataWindow.Table.Select='" + ls_sql + "'")
ll_ret = dw_prodotti_lista.setsqlselect(ls_sql)
if ll_ret < 1 then
	g_mb.messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	return
end if
ll_y = dw_prodotti_lista.retrieve()
if ll_y > 0 then
	st_2.text = "TROVATI " + string(ll_y) + " PRODOTTI"
else
	st_2.text = "NESSUN PRODOTTO TROVATO"
	dw_stringa_ricerca_prodotti.setfocus()
end if

end subroutine

public subroutine wf_dw_select_chiavi ();string ls_sql, ls_stringa,ls_db, ls_str1, ls_str2, ls_cod_chiave, ls_sql_chiavi
long ll_i, ll_y, ll_ret, ll_pos, ll_progressivo
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

choose case ls_db
	case "SYBASE_ASE"
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'"
	case else
		ls_sql = "select cod_prodotto, des_prodotto, cod_comodo, cod_comodo_2 from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( flag_blocco = 'N' or ( flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "' ))"
end choose

// *** carico tutti i prodotti con le chiavi

dw_chiavi_ricerca.accepttext()

ls_sql_chiavi = ""

for ll_i = 1 to dw_chiavi_ricerca.rowcount()
	
	ls_cod_chiave = dw_chiavi_ricerca.getitemstring( ll_i, "cod_chiave")
	ll_progressivo = dw_chiavi_ricerca.getitemnumber( ll_i, "progressivo")
	
	if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 then
		
		ls_sql_chiavi += " and cod_prodotto IN ( select cod_prodotto from anag_prodotti_chiavi where cod_azienda = '" + s_cs_xx.cod_azienda + "' and ( cod_chiave = '" + ls_cod_chiave + "' and progressivo = " + string(ll_progressivo) + " )) "
		
	end if
	
next

if ls_sql_chiavi <> "" then 
	ls_sql += " " + ls_sql_chiavi
end if

ls_sql = ls_sql + " order by cod_prodotto"

ll_ret = dw_prodotti_lista.setsqlselect(ls_sql)
if ll_ret < 1 then
	g_mb.messagebox("APICE", "Errore nella assegnazione sintassi SQL di ricerca con setsqlselect: comunicare l'errore all'amministratore del sistema")
	return
end if
ll_y = dw_prodotti_lista.retrieve()
if ll_y > 0 then
	// se tutto va bene resetto la datawindow di ricerca
	dw_chiavi_ricerca.reset()
	dw_chiavi_ricerca.set_dw_options(sqlca, &
														pcca.null_object, &
														c_nomodify + &
														c_noretrieveonopen + &
														c_nodelete + &
														c_newonopen + &
														c_disableCC, &
														c_noresizedw + &
														c_nohighlightselected + &
														c_nocursorrowpointer +&
														c_nocursorrowfocusrect )
	dw_chiavi_ricerca.insertrow(0)
	//
	st_2.text = "TROVATI " + string(ll_y) + " PRODOTTI"
else
	st_2.text = "NESSUN PRODOTTO TROVATO"
	dw_stringa_ricerca_prodotti.setfocus()
end if

dw_folder.fu_selectTab(1)
end subroutine

on deactivate;call w_cs_xx_principale::deactivate;this.hide()
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_prodotti_lista.settransobject(sqlca)
dw_stringa_ricerca_prodotti.insertrow(0)
dw_prodotti_lista.setrowfocusindicator(hand!)
if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
	dw_prodotti_lista.setfocus()
else
	dw_stringa_ricerca_prodotti.setfocus()	
end if

dw_folder.fu_folderoptions(dw_folder.c_defaultheight , dw_folder.c_foldertabright)

l_objects[1] = dw_nuovo
l_objects[2] = cb_nuovo
l_objects[3] = st_3
dw_folder.fu_assigntab(2, "R.", l_Objects[])

// *** terzo folder per la ricerca dei prodotti tramite le chiavi

l_objects[1] = dw_chiavi_ricerca
l_objects[2] = cb_ok_chiavi
l_objects[3] = cb_annulla_chiavi
l_objects[4] = cb_reset
dw_folder.fu_assigntab(3, "C.", l_Objects[])

dw_chiavi_ricerca.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_nomodify + &
													c_noretrieveonopen + &
                     					   c_nodelete + &
                                       c_newonopen + &
                                       c_disableCC, &
                                       c_noresizedw + &
                                       c_nohighlightselected + &
                                       c_nocursorrowpointer +&
                                       c_nocursorrowfocusrect )
							 

// ***

l_objects[1] = dw_prodotti_lista
l_objects[2] = cb_annulla
l_objects[3] = cb_ok
l_objects[4] = dw_stringa_ricerca_prodotti
l_objects[5] = st_1
l_objects[6] = st_2
dw_folder.fu_assigntab(1, "L.", l_Objects[])
dw_folder.fu_foldercreate(3,3)
dw_folder.fu_selectTab(1)

dw_nuovo.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_nomodify + &
								 c_nodelete + &
								 c_newonopen + &
								 c_disableCC, &
								 c_noresizedw + &
								 c_nohighlightselected + &
								 c_cursorrowpointer)

end event

on w_prodotti_ricerca.create
int iCurrent
call super::create
this.cb_annulla_chiavi=create cb_annulla_chiavi
this.cb_reset=create cb_reset
this.cb_ok_chiavi=create cb_ok_chiavi
this.dw_chiavi_ricerca=create dw_chiavi_ricerca
this.st_3=create st_3
this.cb_nuovo=create cb_nuovo
this.st_1=create st_1
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.st_2=create st_2
this.dw_nuovo=create dw_nuovo
this.dw_prodotti_lista=create dw_prodotti_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla_chiavi
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.cb_ok_chiavi
this.Control[iCurrent+4]=this.dw_chiavi_ricerca
this.Control[iCurrent+5]=this.st_3
this.Control[iCurrent+6]=this.cb_nuovo
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.cb_ok
this.Control[iCurrent+9]=this.cb_annulla
this.Control[iCurrent+10]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.dw_nuovo
this.Control[iCurrent+13]=this.dw_prodotti_lista
this.Control[iCurrent+14]=this.dw_folder
end on

on w_prodotti_ricerca.destroy
call super::destroy
destroy(this.cb_annulla_chiavi)
destroy(this.cb_reset)
destroy(this.cb_ok_chiavi)
destroy(this.dw_chiavi_ricerca)
destroy(this.st_3)
destroy(this.cb_nuovo)
destroy(this.st_1)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.st_2)
destroy(this.dw_nuovo)
destroy(this.dw_prodotti_lista)
destroy(this.dw_folder)
end on

event activate;call super::activate;if not isnull(s_cs_xx.parametri.parametro_pos_ricerca) and len(s_cs_xx.parametri.parametro_pos_ricerca) > 0 then
	wf_pos_ricerca()
else
//	dw_prodotti_lista.reset()
	dw_stringa_ricerca_prodotti.setfocus()
end if


end event

event pc_setddlb;call super::pc_setddlb;	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_deposito", &
						  sqlca, &
						  "anag_depositi", &
						  "cod_deposito", &
						  "des_deposito", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_cat_mer", &
						  sqlca, &
						  "tab_cat_mer", &
						  "cod_cat_mer", &
						  "des_cat_mer", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_misura_mag", &
						  sqlca, &
						  "tab_misure", &
						  "cod_misura", &
						  "des_misura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_misura_acq", &
						  sqlca, &
						  "tab_misure", &
						  "cod_misura", &
						  "des_misura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_misura_ven", &
						  sqlca, &
						  "tab_misure", &
						  "cod_misura", &
						  "des_misura", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	f_po_loaddddw_dw(dw_nuovo, &
						  "cod_iva", &
						  sqlca, &
						  "tab_ive", &
						  "cod_iva", &
						  "des_iva", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


	f_po_loaddddw_dw( dw_chiavi_ricerca, &
						  "cod_chiave", &
						  sqlca, &
						  "tab_chiavi", &
						  "cod_chiave", &
						  "des_chiave", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

end event

event closequery;save_on_close(c_socnosave)
call super ::closequery
end event

type cb_annulla_chiavi from uo_cb_close within w_prodotti_ricerca
integer x = 1989
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type cb_reset from commandbutton within w_prodotti_ricerca
integer x = 2377
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reset"
end type

event clicked;// se tutto va bene resetto la datawindow di ricerca
dw_chiavi_ricerca.reset()
dw_chiavi_ricerca.set_dw_options(sqlca, &
												pcca.null_object, &
												c_nomodify + &
												c_noretrieveonopen + &
												c_nodelete + &
												c_newonopen + &
												c_disableCC, &
												c_noresizedw + &
												c_nohighlightselected + &
												c_nocursorrowpointer +&
												c_nocursorrowfocusrect )
dw_chiavi_ricerca.insertrow(0)
//
end event

type cb_ok_chiavi from commandbutton within w_prodotti_ricerca
integer x = 2766
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;wf_dw_select_chiavi()
dw_prodotti_lista.setfocus()
end event

type dw_chiavi_ricerca from uo_cs_xx_dw within w_prodotti_ricerca
integer x = 69
integer y = 60
integer width = 3063
integer height = 1440
integer taborder = 40
string dataobject = "d_anag_prodotti_chiavi_ricerca"
end type

event itemchanged;call super::itemchanged;long ll_null, ll_i, ll_progressivo
string ls_cod_chiave

setnull(ll_null)
ll_i = dw_chiavi_ricerca.getrow()
if ll_i <= 0 then return -1

choose case i_colname
	case "cod_chiave"
		if not isnull(i_coltext) and i_coltext <> "" then
			
			f_po_loaddddw_dw(dw_chiavi_ricerca, &
								  "progressivo", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_chiave = '" + i_coltext + "' ")		
								  
			setitem(ll_i,"progressivo", ll_null)
			
		else
			
			f_po_loaddddw_dw(dw_chiavi_ricerca, &
								  "progressivo", &
								  sqlca, &
								  "tab_chiavi_valori", &
								  "progressivo", &
								  "valore", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
								  
			setitem(ll_i,"progressivo", ll_null)
			
		end if
		
		
	case "progressivo"
		
		if rowcount() = row then
			
			ls_cod_chiave = getitemstring( row, "cod_chiave")
			ll_progressivo = long(i_coltext)
			
			if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 then
				insertrow(0)
			end if			
			
		end if
end choose
end event

event ue_key;call super::ue_key;//string ls_cod_chiave
//long   ll_progressivo, ll_riga
//
//IF KeyDown(KeyTab!) then
//	
//	ll_riga = getrow()
//	ls_cod_chiave = getitemstring( ll_riga, "cod_chiave")
//	ll_progressivo = getitemnumber( ll_riga, "progressivo")
//	
//	if not isnull(ls_cod_chiave) and ls_cod_chiave <> "" and not isnull(ll_progressivo) and ll_progressivo > 0 then
//		
//		insertrow(0)
//		
//	end if
//	
//end if
end event

type st_3 from statictext within w_prodotti_ricerca
integer x = 46
integer y = 1280
integer width = 2583
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_nuovo from commandbutton within w_prodotti_ricerca
integer x = 2720
integer y = 1280
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;boolean lb_obbligo = false
string  ls_msg, ls_cod_prodotto, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_flag_articolo_fiscale, &
        ls_cod_cat_mer, ls_cod_deposito, ls_cod_iva, ls_cod_misura_mag, ls_cod_misura_acq, ls_cod_misura_ven, ls_null
long ll_cont
decimal ld_pezzi_collo,ld_fat_conv_acq, ld_prezzo_acq, ld_sconto_acq, ld_fat_conv_ven, ld_prezzo_ven, ld_sconto_ven

dw_nuovo.accepttext()
ls_cod_prodotto = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_prodotto")
select count(*)
into   :ll_cont
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
if not isnull(ll_cont) and ll_cont > 0 then
	st_3.text = "Codice prodotto già presente in anagrafica !!!"
	beep(3)
	return
end if

if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_prodotto")) then
	st_3.text = "Codice Prodotto Obbligatorio"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"des_prodotto")) then
	st_3.text = "Descrizione Prodotto Obbligatoria"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_deposito")) then
	st_3.text = "Deposito Obbligatorio"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_iva")) then
	st_3.text = "Codice I.V.A. Obbligatorio"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_mag")) then
	st_3.text = "Unità di Misura di Magazzino Obbligatorio"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_acq")) then
	st_3.text = "Unità di Misura di Acquisto Obbligatorio"
	beep(3)
	return
end if
if isnull(dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_ven")) then
	st_3.text = "Unità di Misura di Vendita Obbligatorio"
	return
end if

// ----------- verificato i dati procedo effettivamente con l'insert del prodotto -------------- //

ls_des_prodotto = dw_nuovo.getitemstring(dw_nuovo.getrow(),"des_prodotto")
ls_cod_comodo = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_comodo")
ls_cod_comodo_2 = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_comodo_2")
ls_flag_articolo_fiscale = dw_nuovo.getitemstring(dw_nuovo.getrow(),"flag_articolo_fiscale")
ls_cod_cat_mer = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_cat_mer")
ls_cod_deposito = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_deposito")
ls_cod_iva = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_iva")
ls_cod_misura_mag = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_mag")
ls_cod_misura_acq = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_acq")
ls_cod_misura_ven = dw_nuovo.getitemstring(dw_nuovo.getrow(),"cod_misura_ven")
ld_pezzi_collo = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"pezzi_collo")
ld_fat_conv_acq = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"fat_conversione_acq")
ld_prezzo_acq = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"prezzo_acq")
ld_sconto_acq = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"sconto_acquisto")
ld_fat_conv_ven = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"fat_conversione_ven")
ld_prezzo_ven = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"prezzo_vendita")
ld_sconto_ven = dw_nuovo.getitemnumber(dw_nuovo.getrow(),"sconto")

insert into anag_prodotti
	(cod_azienda,
	 cod_prodotto,
	 des_prodotto,
	 cod_comodo,
	 cod_comodo_2,
	 flag_articolo_fiscale,
	 cod_cat_mer,
	 cod_deposito,
	 pezzi_collo,
	 cod_iva,
	 cod_misura_mag,
	 cod_misura_acq,
	 fat_conversione_acq,
	 prezzo_acquisto,
	 sconto_acquisto,
	 cod_misura_ven,
	 fat_conversione_ven,
	 prezzo_vendita,
	 sconto)
values
	(:s_cs_xx.cod_azienda,
	 :ls_cod_prodotto,
	 :ls_des_prodotto,
	 :ls_cod_comodo,
	 :ls_cod_comodo_2,
	 :ls_flag_articolo_fiscale,
	 :ls_cod_cat_mer,
	 :ls_cod_deposito,
	 :ld_pezzi_collo,
	 :ls_cod_iva,
	 :ls_cod_misura_mag,
	 :ls_cod_misura_acq,
	 :ld_fat_conv_acq,
	 :ld_prezzo_acq,
	 :ld_sconto_acq,
	 :ls_cod_misura_ven,
	 :ld_fat_conv_ven,
	 :ld_prezzo_ven,
	 :ld_sconto_ven);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in fase di inserimento del nuovo prodotto~r~n"+sqlca.sqlerrtext)
	rollback;
	return
else
	commit;
end if

// ------------  porto il prodotto appena creato nella maschera da cui è --------------------------------
//						partita la richiesta
dw_folder.fu_selectTab(1)
dw_stringa_ricerca_prodotti.setfocus()

if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.SetText(ls_cod_prodotto)
	s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()		
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
else
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.settext(ls_cod_prodotto)
	s_cs_xx.parametri.parametro_uo_dw_search.AcceptText()		
	s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
end if

// ------------ Azzero la DW Nuovo ------------------------------------------------------------------------
setnull(ls_null)

dw_nuovo.setitem(dw_nuovo.getrow(),"des_prodotto",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_comodo",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_comodo_2",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"flag_articolo_fiscale",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_cat_mer",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_deposito",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_iva",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_misura_mag",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_misura_acq",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"cod_misura_ven",ls_null)
dw_nuovo.setitem(dw_nuovo.getrow(),"pezzi_collo",0)
dw_nuovo.setitem(dw_nuovo.getrow(),"fat_conversione_acq",1)
dw_nuovo.setitem(dw_nuovo.getrow(),"prezzo_acq",0)
dw_nuovo.setitem(dw_nuovo.getrow(),"sconto_acquisto",0)
dw_nuovo.setitem(dw_nuovo.getrow(),"fat_conversione_ven",1)
dw_nuovo.setitem(dw_nuovo.getrow(),"prezzo_vendita",0)
dw_nuovo.setitem(dw_nuovo.getrow(),"sconto",0)

end event

type st_1 from statictext within w_prodotti_ricerca
integer x = 46
integer y = 60
integer width = 686
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Ricerca per Codice:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_prodotti_ricerca
integer x = 2766
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_prodotti_lista.getrow() > 0 then

	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.SetText (dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
		s_cs_xx.parametri.parametro_uo_dw_1.AcceptText()		
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setfocus()
	elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
		s_cs_xx.parametri.parametro_uo_dw_search.AcceptText()		
		s_cs_xx.parametri.parametro_uo_dw_search.setfocus()
	else
		s_cs_xx.parametri.parametro_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_dw_1.settext(dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
		s_cs_xx.parametri.parametro_dw_1.AcceptText()		
		s_cs_xx.parametri.parametro_dw_1.setfocus()
	end if
	
	if il_row > 0 and not isnull(il_row) then
		dw_prodotti_lista.setrow(il_row - 1)
		il_row = 0
	end if
	
	parent.hide()
end if
end event

type cb_annulla from uo_cb_close within w_prodotti_ricerca
integer x = 2377
integer y = 1520
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

type dw_stringa_ricerca_prodotti from datawindow within w_prodotti_ricerca
event ue_key pbm_dwnkey
integer x = 754
integer y = 40
integer width = 2377
integer height = 100
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;string ls_stringa
if key = keyenter! then
	ls_stringa = dw_stringa_ricerca_prodotti.gettext()
	wf_dw_select(is_tipo_ricerca, ls_stringa)
	dw_prodotti_lista.setfocus()
end if
end event

event getfocus;string ls_str

if getrow() > 0 then
	ls_str = getitemstring(1,"stringa_ricerca")
	this.setcolumn("stringa_ricerca")
	this.selecttext(1,len(ls_str))
end if
end event

type st_2 from statictext within w_prodotti_ricerca
integer x = 46
integer y = 1520
integer width = 1760
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type dw_nuovo from uo_cs_xx_dw within w_prodotti_ricerca
integer x = 46
integer y = 160
integer width = 3040
integer height = 1100
integer taborder = 60
string dataobject = "d_prodotti_ricerca_nuovo_prod"
end type

event itemchanged;call super::itemchanged;string ls_cod_prodotto, ls_des_prodotto
long ll_cont

choose case i_colname
	case "cod_prodotto"
		select count(*)
		into   :ll_cont
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :i_coltext;
		if not isnull(ll_cont) and ll_cont > 0 then
			st_3.text = "Codice prodotto già presente in anagrafica !!!"
			beep(3)
			return 1
		end if
	case "des_prodotto"
		select count(*)
		into   :ll_cont
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 des_prodotto = :i_coltext;
		if not isnull(ll_cont) and ll_cont > 0 then
			st_3.text = "La descrizione digitata è già presente in un altro prodotto; si consiglia di variarla !!!"
			beep(3)
			return 1
		end if
	case "cod_misura_mag"
		setitem(getrow(),"cod_misura_acq", i_coltext)
		setitem(getrow(),"cod_misura_ven", i_coltext)
end choose
st_3.text = ""
end event

type dw_prodotti_lista from datawindow within w_prodotti_ricerca
event ue_key pbm_dwnkey
integer x = 46
integer y = 160
integer width = 3086
integer height = 1340
integer taborder = 70
string dataobject = "d_prodotti_ricerca"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
		end if
END CHOOSE

end event

event doubleclicked;cb_ok.postevent(clicked!)


end event

event buttonclicked;choose case dwo.name
	case "cb_cod_prodotto" 
		dw_prodotti_lista.reset()
		is_tipo_ricerca = "C"
		st_1.text = "Ricerca per Codice:"
		dw_stringa_ricerca_prodotti.setfocus()
	case "cb_des_prodotto" 
		dw_prodotti_lista.reset()
		is_tipo_ricerca = "D"
		st_1.text = "Ricerca per Des.Prodotto:"
		dw_stringa_ricerca_prodotti.setfocus()		
	case "cb_cod_comodo" 
		dw_prodotti_lista.reset()
		is_tipo_ricerca = "M"
		st_1.text = "Ricerca per Comodo:"
		dw_stringa_ricerca_prodotti.setfocus()
	case "cb_cod_comodo_2" 
		dw_prodotti_lista.reset()
		is_tipo_ricerca = "M2"
		st_1.text = "Ricerca per Comodo 2:"
		dw_stringa_ricerca_prodotti.setfocus()
end choose
	
	
	
end event

type dw_folder from u_folder within w_prodotti_ricerca
integer width = 3314
integer height = 1620
integer taborder = 20
end type

event po_tabclicked;call super::po_tabclicked;if i_SelectedTab	= 2 then

	

end if
end event

