﻿$PBExportHeader$w_prodotti_ricerca_response.srw
$PBExportComments$Finestra Ricerca Prodotti per Window Response (Ereditata da w_prodotti_ricerca)
forward
global type w_prodotti_ricerca_response from w_prodotti_ricerca
end type
end forward

global type w_prodotti_ricerca_response from w_prodotti_ricerca
integer x = 673
integer y = 265
integer width = 3337
integer height = 1712
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
end type
global w_prodotti_ricerca_response w_prodotti_ricerca_response

on w_prodotti_ricerca_response.create
call super::create
end on

on w_prodotti_ricerca_response.destroy
call super::destroy
end on

type cb_annulla_chiavi from w_prodotti_ricerca`cb_annulla_chiavi within w_prodotti_ricerca_response
end type

type cb_reset from w_prodotti_ricerca`cb_reset within w_prodotti_ricerca_response
end type

type cb_ok_chiavi from w_prodotti_ricerca`cb_ok_chiavi within w_prodotti_ricerca_response
end type

type dw_chiavi_ricerca from w_prodotti_ricerca`dw_chiavi_ricerca within w_prodotti_ricerca_response
end type

type st_3 from w_prodotti_ricerca`st_3 within w_prodotti_ricerca_response
end type

type cb_nuovo from w_prodotti_ricerca`cb_nuovo within w_prodotti_ricerca_response
end type

event cb_nuovo::clicked;call super::clicked;close(parent)
end event

type st_1 from w_prodotti_ricerca`st_1 within w_prodotti_ricerca_response
end type

type cb_ok from w_prodotti_ricerca`cb_ok within w_prodotti_ricerca_response
end type

event cb_ok::clicked;if dw_prodotti_lista.getrow() > 0 then

	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.settext(dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
		s_cs_xx.parametri.parametro_uo_dw_1.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	elseif not isnull(s_cs_xx.parametri.parametro_uo_dw_search) then
		s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_search.settext(dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto"))
		s_cs_xx.parametri.parametro_uo_dw_search.accepttext()
	else
		s_cs_xx.parametri.parametro_s_10 = dw_prodotti_lista.getitemstring(dw_prodotti_lista.getrow(),"cod_prodotto")
	end if
	
	if il_row > 0 and not isnull(il_row) then
		dw_prodotti_lista.setrow(il_row - 1)
		il_row = 0
	end if
	
	close(parent)
end if
end event

type cb_annulla from w_prodotti_ricerca`cb_annulla within w_prodotti_ricerca_response
end type

type dw_stringa_ricerca_prodotti from w_prodotti_ricerca`dw_stringa_ricerca_prodotti within w_prodotti_ricerca_response
end type

type st_2 from w_prodotti_ricerca`st_2 within w_prodotti_ricerca_response
end type

type dw_nuovo from w_prodotti_ricerca`dw_nuovo within w_prodotti_ricerca_response
end type

type dw_prodotti_lista from w_prodotti_ricerca`dw_prodotti_lista within w_prodotti_ricerca_response
end type

type dw_folder from w_prodotti_ricerca`dw_folder within w_prodotti_ricerca_response
end type

