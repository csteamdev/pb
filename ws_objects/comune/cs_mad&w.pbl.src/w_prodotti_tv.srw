﻿$PBExportHeader$w_prodotti_tv.srw
$PBExportComments$Finestra Gestione Prodotti
forward
global type w_prodotti_tv from w_cs_xx_treeview
end type
type dw_export from datawindow within det_1
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
type det_5 from userobject within tab_dettaglio
end type
type dw_5 from uo_cs_xx_dw within det_5
end type
type det_5 from userobject within tab_dettaglio
dw_5 dw_5
end type
type det_6 from userobject within tab_dettaglio
end type
type dw_6 from uo_cs_xx_dw within det_6
end type
type det_6 from userobject within tab_dettaglio
dw_6 dw_6
end type
type det_7 from userobject within tab_dettaglio
end type
type dw_colonne_dinamiche from uo_colonne_dinamiche_dw within det_7
end type
type det_7 from userobject within tab_dettaglio
dw_colonne_dinamiche dw_colonne_dinamiche
end type
type det_8 from userobject within tab_dettaglio
end type
type dw_documenti from uo_drag_prodotti within det_8
end type
type det_8 from userobject within tab_dettaglio
dw_documenti dw_documenti
end type
end forward

global type w_prodotti_tv from w_cs_xx_treeview
integer width = 4795
integer height = 2284
string title = "Anagrafica Prodotti"
event pc_menu_pulsanti ( string as_azione )
event pc_menu_note ( )
event pc_menu_lingue ( )
event pc_menu_chiavi ( )
event pc_menu_collegati ( )
event pc_menu_trova_dist ( )
event pc_menu_barcode ( )
event pc_menu_duplica_cc ( )
event pc_imposta_campi_ricerca ( string as_fpb )
event pc_aggiorna_descrizione ( )
event pc_menu_utilizzo_prodotto ( )
end type
global w_prodotti_tv w_prodotti_tv

type variables
private:
	datastore ids_store
	long il_livello
	
	// icone
	int ICONA_PRODOTTO, ICONA_PROD_RAGG
	int	ICONA_CATEGORIA, ICONA_CLASSEABC, ICONA_NOMENCLATURA, &
	     ICONA_FORNITORE, ICONA_LIV1, ICONA_LIV2, ICONA_LIV3, ICONA_LIV4, ICONA_LIV5
	
	long il_max_row = 255
	
	string is_nome_liv_1 = "Livello 1"
	string is_nome_liv_2 = "Livello 2"
	string is_nome_liv_3 = "Livello 3"
	string is_nome_liv_4 = "Livello 4"
	string is_nome_liv_5 = "Livello 5"
	string is_where_to_export = ""
	
	boolean ib_new
	
	/*
	SYBASE_ASA
	SYBASE_ASE
	MSSQL
	ORACLE
	*/
	string is_enginetype, is_top_clause
		

end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_categoria (long al_handle)
public function boolean wf_singolo_nodo (string as_tipologia, ref string as_valore)
public function integer wf_inserisci_classe_abc (long al_handle)
public function integer wf_inserisci_nomenclatura (long al_handle)
public function integer wf_inserisci_fornitore (long al_handle)
public function integer wf_inserisci_prodotto_ragg (long al_handle)
public function integer wf_inserisci_liv_prod_1 (long al_handle)
public function integer wf_inserisci_liv_prod_2 (long al_handle)
public function integer wf_inserisci_liv_prod_3 (long al_handle)
public function integer wf_inserisci_liv_prod_4 (long al_handle)
public function integer wf_inserisci_liv_prod_5 (long al_handle)
public function integer wf_inserisci_prodotti (long al_handle)
public function integer wf_inserisci_prod (long al_handle, string as_cod_prodotto, string as_des_prodotto)
public subroutine wf_abilita_pulsanti (boolean ab_enabled)
public subroutine wf_updatestart ()
end prototypes

event pc_menu_pulsanti(string as_azione);long				ll_row

ll_row = tab_dettaglio.det_1.dw_1.getrow()
if ll_row>0 then
	if g_str.isempty(tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_prodotto")) then return
else
	return
end if


choose case upper(as_azione)
	case "NOTE"
		window_open_parm(w_prodotti_note, -1, tab_dettaglio.det_1.dw_1)
	
	case "LINGUE"
		window_open_parm(w_prodotti_lingue, -1, tab_dettaglio.det_1.dw_1)
		
	case "DOCUMENTI"
		window_open_parm(w_prodotti_blob, -1, tab_dettaglio.det_1.dw_1)
		
	case "CHIAVI"
//		ib_nuovo = false
		window_open_parm(w_prodotti_chiavi_valori, -1, tab_dettaglio.det_1.dw_1)
		
	case "COLLEGATI"
		window_open_parm(w_prod_collegati, -1, tab_dettaglio.det_1.dw_1)
		
	case "TROVADIST"
		window_open_parm(w_elenco_pf, -1, tab_dettaglio.det_1.dw_1)
		
	case "BARCODE"
		window_open_parm(w_prodotti_barcode, -1, tab_dettaglio.det_1.dw_1)
		
	case "DUPLICACC"
		s_cs_xx.parametri.parametro_s_10 =  tab_dettaglio.det_1.dw_1.getitemstring(ll_row,"cod_prodotto")
		window_open(w_duplica_prodotto_centri_costo, 0)
		
end choose


return
end event

event pc_menu_note();

event trigger pc_menu_pulsanti("NOTE")
end event

event pc_menu_lingue();

event trigger pc_menu_pulsanti("LINGUE")
end event

event pc_menu_chiavi();

event trigger pc_menu_pulsanti("CHIAVI")
end event

event pc_menu_collegati();

event trigger pc_menu_pulsanti("COLLEGATI")
end event

event pc_menu_trova_dist();

event trigger pc_menu_pulsanti("TROVADIST")
end event

event pc_menu_barcode();

event trigger pc_menu_pulsanti("BARCODE")
end event

event pc_menu_duplica_cc();

event trigger pc_menu_pulsanti("DUPLICACC")
end event

event pc_imposta_campi_ricerca(string as_fpb);


tab_ricerca.ricerca.dw_ricerca.setitem(tab_ricerca.ricerca.dw_ricerca.getrow(),"rs_flag_blocco", as_fpb)

end event

event pc_menu_utilizzo_prodotto();/**
 * stefanop
 * 21/03/2016
 *
 * Visualizzo le referenze del prodotto selezionato.
 * Viene aperta un finestra che mostra dove il prodotto è utilizzato
 **/
 
string ls_cod_prodotto

ls_cod_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_prodotto")

uo_visualizza_referenze luo_referenze
luo_referenze = create uo_visualizza_referenze

luo_referenze.uof_referenze_prodotto(ls_cod_prodotto)
end event

public subroutine wf_imposta_ricerca ();
datetime			ldt_data
date				ldd_date


is_sql_filtro = ""

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto")) then
	is_sql_filtro += " AND anag_prodotti.cod_prodotto LIKE '%" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto") + "%'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "des_prodotto")) then
	is_sql_filtro += " AND anag_prodotti.des_prodotto LIKE '%" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "des_prodotto") + "%'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_comodo")) then
	is_sql_filtro += " AND anag_prodotti.cod_comodo LIKE '%" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_comodo") + "%'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_comodo_2")) then
	is_sql_filtro += " AND anag_prodotti.cod_comodo_2 LIKE '%" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_comodo_2") + "%'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_cat_mer")) then
	is_sql_filtro += " AND anag_prodotti.cod_cat_mer ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_cat_mer") + "'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_blocco") <> 'T' then
	is_sql_filtro += " AND anag_prodotti.flag_blocco ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_blocco") + "'"
end if

ldt_data = tab_ricerca.ricerca.dw_ricerca.GetItemdatetime(1, "data_blocco")
if not isnull(ldt_data) and year(date(ldt_data))>1950 then
	is_sql_filtro += " AND anag_prodotti.data_blocco='" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) +"'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_classe_abc") <> 'T' then
	is_sql_filtro += " AND anag_prodotti.flag_classe_abc ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_classe_abc") + "'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_fiscale") <> 'T' then
	is_sql_filtro += " AND anag_prodotti.flag_articolo_fiscale ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_fiscale") + "'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_lifo") <> 'T' then
	is_sql_filtro += " AND anag_prodotti.flag_lifo ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_lifo") + "'"
end if

if tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_lifo") <> 'T' then
	is_sql_filtro += " AND anag_prodotti.flag_lifo ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_lifo") + "'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_nomenclatura")) then
	is_sql_filtro += " AND anag_prodotti.cod_nomenclatura ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_nomenclatura") + "'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto_alt")) then
	is_sql_filtro += " AND anag_prodotti.cod_prodotto_alt ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto_alt") + "'"
end if

if g_str.isnotempty(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto_raggruppato")) then
	is_sql_filtro += " AND anag_prodotti.cod_prodotto_raggruppato ='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_prodotto_raggruppato") + "'"
end if

is_where_to_export = "where cod_azienda='"+s_cs_xx.cod_azienda+"' " + is_sql_filtro




end subroutine

public subroutine wf_treeview_icons ();

ICONA_PRODOTTO			= wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_PROD_RAGG			= wf_treeview_add_icon("treeview\documento_grigio.png")

ICONA_CATEGORIA			= wf_treeview_add_icon("treeview\Categoria_cliente.png")
ICONA_CLASSEABC			= wf_treeview_add_icon("treeview\tag_green.png")
ICONA_NOMENCLATURA		= wf_treeview_add_icon("treeview\calculator.png")
ICONA_FORNITORE			= wf_treeview_add_icon("treeview\operatore.png")

ICONA_LIV1						= wf_treeview_add_icon("treeview\cartella.png")
ICONA_LIV2						= wf_treeview_add_icon("treeview\cartella_rossa.png")
ICONA_LIV3						= wf_treeview_add_icon("treeview\cartella_verde.png")
ICONA_LIV4						= wf_treeview_add_icon("treeview\cartella_grigia.png")
ICONA_LIV5						= wf_treeview_add_icon("treeview\cartella_grigia.png")

end subroutine

public subroutine wf_valori_livelli ();string		ls_tmp


wf_add_valore_livello("Non Specificato", "N")

wf_add_valore_livello("Categoria", "C")
wf_add_valore_livello("Classe ABC", "S")
wf_add_valore_livello("Nomenclatura", "T")
wf_add_valore_livello("Fornitore abituale", "F")
wf_add_valore_livello("Prodotto raggruppato", "P")


ls_tmp = g_str.replace(is_nome_liv_1, "/", "-")
wf_add_valore_livello("1." + trim(ls_tmp), "1")

ls_tmp = g_str.replace(is_nome_liv_2, "/", "-")
wf_add_valore_livello("2." + trim(ls_tmp), "2")

ls_tmp = g_str.replace(is_nome_liv_3, "/", "-")
wf_add_valore_livello("3." + trim(ls_tmp), "3")

ls_tmp = g_str.replace(is_nome_liv_4, "/", "-")
wf_add_valore_livello("4." + trim(ls_tmp), "4")

ls_tmp = g_str.replace(is_nome_liv_5, "/", "-")
wf_add_valore_livello("5." + trim(ls_tmp), "5")
end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long						ll_item
treeviewitem			ltv_item
str_treeview				lstr_data
date						ldt_date
datetime					ldt_datetime

if al_handle = 0 then return 0

do
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello	
			
		case "C"
			if g_str.isempty(lstr_data.codice) then
				as_sql += " AND anag_prodotti.cod_cat_mer is null "
			else
				as_sql += " AND anag_prodotti.cod_cat_mer = '" + lstr_data.codice+"' "
			end if
		
		case "S"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.flag_classe_abc is null or anag_prodotti.flag_classe_abc='') "
			else
				as_sql += " AND anag_prodotti.flag_classe_abc = '" + lstr_data.codice + "' "
			end if
		
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_prodotti.cod_nomenclatura is null "
			else
				as_sql += " AND anag_prodotti.cod_nomenclatura = '" + lstr_data.codice + "' "
			end if
		
		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_prodotti.cod_fornitore is null "
			else
				as_sql += " AND anag_prodotti.cod_fornitore = '" + lstr_data.codice + "' "
			end if
		
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND anag_prodotti.cod_prodotto_raggruppato is null "
			else
				as_sql += " AND anag_prodotti.cod_prodotto_raggruppato = '" + lstr_data.codice + "' "
			end if
			
		case "1"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.cod_livello_prod_1 is null or anag_prodotti.cod_livello_prod_1='') "
			else
				as_sql += " AND anag_prodotti.cod_livello_prod_1 = '" + lstr_data.codice + "' "
			end if
		
		case "2"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.cod_livello_prod_2 is null or anag_prodotti.cod_livello_prod_2='') "
			else
				as_sql += " AND anag_prodotti.cod_livello_prod_2 = '" + lstr_data.codice + "' "
			end if
		
		case "3"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.cod_livello_prod_3 is null or anag_prodotti.cod_livello_prod_3='') "
			else
				as_sql += " AND anag_prodotti.cod_livello_prod_3 = '" + lstr_data.codice + "' "
			end if
		
		case "4"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.cod_livello_prod_4 is null or anag_prodotti.cod_livello_prod_4='') "
			else
				as_sql += " AND anag_prodotti.cod_livello_prod_4 = '" + lstr_data.codice + "' "
			end if
		
		case "5"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND (anag_prodotti.cod_livello_prod_5 is null or anag_prodotti.cod_livello_prod_5='') "
			else
				as_sql += " AND anag_prodotti.cod_livello_prod_5 = '" + lstr_data.codice + "' "
			end if
		
	
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);long				ll_ret

il_livello = ai_livello

setpointer(Hourglass!)
pcca.mdi_frame.setmicrohelp("Preparazione query in corso ...")

choose case wf_get_valore_livello(ai_livello)
	case "C"
		ll_ret = wf_inserisci_categoria(al_handle)
	
	case "S"
		ll_ret = wf_inserisci_classe_abc(al_handle)
	
	case "T"
		ll_ret = wf_inserisci_nomenclatura(al_handle)
	
	case "F"
		ll_ret = wf_inserisci_fornitore(al_handle)
	
	case "P"
		ll_ret = wf_inserisci_prodotto_ragg(al_handle)
	
	case "1"
		ll_ret = wf_inserisci_liv_prod_1(al_handle)
	
	case "2"
		ll_ret = wf_inserisci_liv_prod_2(al_handle)
	
	case "3"
		ll_ret = wf_inserisci_liv_prod_3(al_handle)
	
	case "4"
		ll_ret = wf_inserisci_liv_prod_4(al_handle)
	
	case "5"
		ll_ret = wf_inserisci_liv_prod_5(al_handle)
		
	case else
		ll_ret = wf_inserisci_prodotti(al_handle)
		
end choose

setpointer(Arrow!)
pcca.mdi_frame.setmicrohelp("Pronto!")

return ll_ret
end function

public function integer wf_inserisci_categoria (long al_handle);string					ls_sql, ls_label, ls_error, ls_valore
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data
boolean				lb_singolo = false


//TRICK
//se hai impostato il livello categoria e il filtro per categoria è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("C", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" anag_prodotti.cod_cat_mer "+&
				"FROM anag_prodotti "+&
				"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by anag_prodotti.cod_cat_mer"
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "C"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_CATEGORIA)
		ltvi_item.data = lstr_data
		ltvi_item.label = ids_store.getitemstring(1, 1) + " - " + &
							f_des_tabella("tab_cat_mer", "cod_cat_mer = '" +  ls_valore + "'", "des_cat_mer")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_sql = "SELECT DISTINCT anag_prodotti.cod_cat_mer, tab_cat_mer.des_cat_mer FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_cat_mer ON tab_cat_mer.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_cat_mer.cod_cat_mer = anag_prodotti.cod_cat_mer " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo categoria merceologica "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<Categ. Merceol. mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_CATEGORIA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function boolean wf_singolo_nodo (string as_tipologia, ref string as_valore);integer			li_valore
string				ls_valore, ls_colonna
datetime			ldt_valore, ldt_valore_2


choose case as_tipologia
	case "C", "T", "F", "P", "1", "2", "3", "4", "5"
		
		choose case as_tipologia
			case "C"
				ls_colonna = "cod_cat_mer"
			case "T"
				ls_colonna = "cod_nomenclatura"
			case "F"
				ls_colonna = "cod_fornitore"
			case "P"
				ls_colonna = "cod_prodotto_raggruppato"
			case else			//livelli prod
				ls_colonna = "cod_livello_prod_" + string(as_tipologia)
		end choose
		
		ls_valore = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, ls_colonna)
		if g_str.isnotempty(ls_valore) then
			as_valore = ls_valore
			return true
		end if
		
	case "S"
		ls_valore = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_classe_abc")
		if g_str.isnotempty(ls_valore) and ls_valore<>"T" then
			as_valore = ls_valore
			return true
		end if
	

	case else
		return false
		
end choose


return false
end function

public function integer wf_inserisci_classe_abc (long al_handle);string					ls_sql, ls_label, ls_error, ls_valore
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data
boolean				lb_singolo = false


//TRICK
//se hai impostato il livello flag_classe_abc e il filtro per flag_classe_abc è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("S", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" anag_prodotti.flag_classe_abc "+&
				"FROM anag_prodotti "+&
				"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by anag_prodotti.flag_classe_abc"
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "S"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_CLASSEABC)
		ltvi_item.data = lstr_data
		
		ltvi_item.label = ids_store.getitemstring(1, 1) + " - Classe " + ls_valore
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_sql = "SELECT DISTINCT anag_prodotti.flag_classe_abc FROM anag_prodotti "+&
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo classe ABC "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "S"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<Classe ABC mancante>"
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - Classe " + ids_store.getitemstring(ll_i, 1)
	end if

	ltvi_item = wf_new_item(true, ICONA_CLASSEABC)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_nomenclatura (long al_handle);string					ls_sql, ls_label, ls_error, ls_valore
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data
boolean				lb_singolo = false


//TRICK
//se hai impostato il livello nomenclatura e il filtro per nomenclatura è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("T", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" anag_prodotti.cod_nomenclatura "+&
				"FROM anag_prodotti "+&
				"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by anag_prodotti.cod_nomenclatura"
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "T"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_NOMENCLATURA)
		ltvi_item.data = lstr_data
		ltvi_item.label = ids_store.getitemstring(1, 1) + " - " + &
							f_des_tabella("tab_nomenclature", "cod_nomenclatura = '" +  ls_valore + "'", "des_nomenclatura")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_sql = "SELECT DISTINCT anag_prodotti.cod_nomenclatura, tab_nomenclature.des_nomenclatura FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_nomenclature ON tab_nomenclature.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_nomenclature.cod_nomenclatura = anag_prodotti.cod_nomenclatura " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo nomenclatura "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<Nomenclatura mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_NOMENCLATURA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_fornitore (long al_handle);string					ls_sql, ls_label, ls_error, ls_valore
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data
boolean				lb_singolo = false


//TRICK
//se hai impostato il livello fornitore abituale e il filtro per fornitore abitiale è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("F", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" anag_prodotti.cod_fornitore "+&
				"FROM anag_prodotti "+&
				"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by anag_prodotti.cod_fornitore"
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "F"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_FORNITORE)
		ltvi_item.data = lstr_data
		ltvi_item.label = ids_store.getitemstring(1, 1) + " - " + &
							f_des_tabella("anag_fornitori", "cod_fornitore = '" +  ls_valore + "'", "rag_soc_1")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_sql = "SELECT DISTINCT anag_prodotti.cod_fornitore, anag_fornitori.rag_soc_1 FROM anag_prodotti "+&
			"LEFT OUTER JOIN anag_fornitori ON anag_fornitori.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"anag_fornitori.cod_fornitore = anag_prodotti.cod_fornitore " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo fornitore abituale "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<Fornitore abituale mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_prodotto_ragg (long al_handle);string					ls_sql, ls_label, ls_error, ls_valore
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data
boolean				lb_singolo = false


//TRICK
//se hai impostato il livello prodotto raggruppato e il filtro per prodotto raggruppato è impostato con un valore 
//allora non fare la query ma inserisci direttamente il nodo, se la where della query è verificata
lb_singolo = wf_singolo_nodo("P", ls_valore)

if lb_singolo and is_top_clause<>"" then
	ls_sql = "SELECT "+is_top_clause+" anag_prodotti.cod_prodotto_raggruppato "+&
				"FROM anag_prodotti "+&
				"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
				
	wf_leggi_parent(al_handle, ls_sql)
	
	//ANNOTAZIONE (caso ORACLE non ho trovato nulla che mi faccia fare la select TOP 1, intal caso procederà come sempre, essendo is_top_clause posto a "" )
	//purtroppo in ASA in tal caso occorre mettere order by obbligatoriamente, altrimenti da errore di "risultato non deterministico"
	if is_enginetype = "SYBASE_ASA" then
		ls_sql += " order by anag_prodotti.cod_prodotto_raggruppato"
	end if
	
	ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
	
	if ll_rows < 0 then g_mb.error(ls_error, sqlca)
	if ll_rows > 0 then
		//inserisci l'unico nodo
		//------------------------------------------------------------------
		lstr_data.livello = il_livello
		lstr_data.tipo_livello = "P"
		lstr_data.codice = ls_valore
		ltvi_item = wf_new_item(true, ICONA_PROD_RAGG)
		ltvi_item.data = lstr_data
		ltvi_item.label = ids_store.getitemstring(1, 1) + " - " + &
							f_des_tabella("anag_prodotti", "cod_prodotto = '" +  ls_valore + "'", "des_prodotto")
		tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
		return 1
		//------------------------------------------------------------------
	end if
	
	return 0
end if


ls_sql = "SELECT DISTINCT anag_prodotti.cod_prodotto_raggruppato, a.des_prodotto FROM anag_prodotti "+&
			"LEFT OUTER JOIN anag_prodotti as a ON a.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"a.cod_prodotto = anag_prodotti.cod_prodotto_raggruppato " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo prodotto raggruppato "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<Prodotto Raggruppato mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_PROD_RAGG)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_liv_prod_1 (long al_handle);string					ls_sql, ls_label, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data


ls_sql = "SELECT DISTINCT anag_prodotti.cod_livello_prod_1, tab_livelli_prod_1.des_livello FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_livelli_prod_1 ON tab_livelli_prod_1.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_1.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo " + is_nome_liv_1 + " " +string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "1"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<"+is_nome_liv_1+" mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_LIV1)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_liv_prod_2 (long al_handle);string					ls_sql, ls_label, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data


ls_sql = "SELECT DISTINCT anag_prodotti.cod_livello_prod_2, tab_livelli_prod_2.des_livello FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_livelli_prod_1 ON tab_livelli_prod_1.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_1.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 " + &
			"LEFT OUTER JOIN tab_livelli_prod_2 ON tab_livelli_prod_2.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_2.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_2.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo " + is_nome_liv_2 + " " +string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "2"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<"+is_nome_liv_2+" mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_LIV2)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_liv_prod_3 (long al_handle);string					ls_sql, ls_label, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data


ls_sql = "SELECT DISTINCT anag_prodotti.cod_livello_prod_3, tab_livelli_prod_3.des_livello FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_livelli_prod_1 ON tab_livelli_prod_1.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_1.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 " + &
			"LEFT OUTER JOIN tab_livelli_prod_2 ON tab_livelli_prod_2.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_2.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_2.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 " + &
			"LEFT OUTER JOIN tab_livelli_prod_3 ON tab_livelli_prod_3.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_3.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo " + is_nome_liv_3 + " " +string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "3"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<"+is_nome_liv_3+" mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_LIV3)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_liv_prod_4 (long al_handle);string					ls_sql, ls_label, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data


ls_sql = "SELECT DISTINCT anag_prodotti.cod_livello_prod_4, tab_livelli_prod_4.des_livello FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_livelli_prod_1 ON tab_livelli_prod_1.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_1.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 " + &
			"LEFT OUTER JOIN tab_livelli_prod_2 ON tab_livelli_prod_2.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_2.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_2.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 " + &
			"LEFT OUTER JOIN tab_livelli_prod_3 ON tab_livelli_prod_3.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_3.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 " + &
			"LEFT OUTER JOIN tab_livelli_prod_4 ON tab_livelli_prod_4.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_4.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_4 = anag_prodotti.cod_livello_prod_4 " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo " + is_nome_liv_4 + " " +string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "4"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<"+is_nome_liv_4+" mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_LIV4)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_liv_prod_5 (long al_handle);string					ls_sql, ls_label, ls_error
long					ll_rows, ll_i
treeviewitem		ltvi_item
str_treeview			lstr_data


ls_sql = "SELECT DISTINCT anag_prodotti.cod_livello_prod_5, tab_livelli_prod_5.des_livello FROM anag_prodotti "+&
			"LEFT OUTER JOIN tab_livelli_prod_1 ON tab_livelli_prod_1.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_1.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 " + &
			"LEFT OUTER JOIN tab_livelli_prod_2 ON tab_livelli_prod_2.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_2.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_2.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 " + &
			"LEFT OUTER JOIN tab_livelli_prod_3 ON tab_livelli_prod_3.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_3.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_3.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 " + &
			"LEFT OUTER JOIN tab_livelli_prod_4 ON tab_livelli_prod_4.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_4.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 AND " + &
										"tab_livelli_prod_4.cod_livello_prod_4 = anag_prodotti.cod_livello_prod_4 " + &
			"LEFT OUTER JOIN tab_livelli_prod_5 ON tab_livelli_prod_5.cod_azienda = anag_prodotti.cod_azienda AND "+&
										"tab_livelli_prod_5.cod_livello_prod_1 = anag_prodotti.cod_livello_prod_1 AND " + &
										"tab_livelli_prod_5.cod_livello_prod_2 = anag_prodotti.cod_livello_prod_2 AND " + &
										"tab_livelli_prod_5.cod_livello_prod_3 = anag_prodotti.cod_livello_prod_3 AND " + &
										"tab_livelli_prod_5.cod_livello_prod_4 = anag_prodotti.cod_livello_prod_4 AND " + &
										"tab_livelli_prod_5.cod_livello_prod_5 = anag_prodotti.cod_livello_prod_5 " + &
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")

ids_store.sort()

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	pcca.mdi_frame.setmicrohelp("Inserimento nodo " + is_nome_liv_5 + " " +string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "5"
	lstr_data.codice = ids_store.getitemstring(ll_i, 1)
	
	
	if isnull(ids_store.getitemstring(ll_i, 1)) then
		ls_label = "<"+is_nome_liv_5+" mancante>"
	elseif isnull(ids_store.getitemstring(ll_i, 2)) then
		ls_label = ids_store.getitemstring(ll_i, 1)
	else
		ls_label = ids_store.getitemstring(ll_i, 1) + " - " + ids_store.getitemstring(ll_i, 2)
	end if

	ltvi_item = wf_new_item(true, ICONA_LIV5)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setpointer(Arrow!)


return ll_rows
end function

public function integer wf_inserisci_prodotti (long al_handle);string					ls_sql, ls_error, ls_message_overflow, ls_cod_prodotto, ls_des_prodotto
long					ll_rows, ll_i, ll_handle

treeviewitem		ltvi_item

ls_sql = "SELECT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto "+&
			"FROM anag_prodotti "+&
			"WHERE anag_prodotti.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

ids_store.setsort("#1 asc")
ids_store.sort()

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_row  then
	
	ls_message_overflow = g_str.format("Attenzione: $1 prodotti da visualizzare potrebbero rallentare il sistema.~r~nContinuo a visualizzare i dati?", ll_rows)
	
	if not g_mb.confirm(ls_message_overflow) then
		// Visualizzo solo i movimenti nel limite
		ll_rows = il_max_row
	end if
end if

setpointer(Hourglass!)

for ll_i = 1 to ll_rows
	Yield()
	ls_cod_prodotto =  ids_store.getitemstring(ll_i, 1)
	ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	pcca.mdi_frame.setmicrohelp("Inserimento nodo prodotto "+string(ll_i)+" di "+string(ll_rows)+" in corso ...")
	
	ll_handle = wf_inserisci_prod(al_handle, ls_cod_prodotto, ls_des_prodotto)

next

setpointer(Hourglass!)


return ll_rows
end function

public function integer wf_inserisci_prod (long al_handle, string as_cod_prodotto, string as_des_prodotto);string				ls_sql, ls_error
treeviewitem	ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.stringa[1] = as_cod_prodotto

ltvi_item = wf_new_item(false, ICONA_PRODOTTO)

ltvi_item.label = as_cod_prodotto
if g_str.isnotempty(as_des_prodotto) then ltvi_item.label += " - " + as_des_prodotto

ltvi_item.data = lstr_data

return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)


end function

public subroutine wf_abilita_pulsanti (boolean ab_enabled);/*
L'argomento "ab_enabled" è attualmente passato come
-------------------------------------------------------------------------------------------------------
TRUE:
				pcd_new, pcd_modify
-------------------------------------------------------------------------------------------------------
FALSE:
				pcd_view
-------------------------------------------------------------------------------------------------------
*/

//abilitare solo in visualizza
tab_dettaglio.det_1.dw_1.object.b_duplica_prodotto.enabled = not ab_enabled

tab_dettaglio.det_2.dw_2.object.b_dettaglio.enabled = not ab_enabled

tab_dettaglio.det_4.dw_4.object.b_cc_acquisti.enabled = not ab_enabled

tab_dettaglio.det_5.dw_5.object.b_cc_vendite.enabled = not ab_enabled



//abilitare in nuovo e modifica
tab_dettaglio.det_4.dw_4.object.b_ricerca_prodotto_alt.enabled = ab_enabled
tab_dettaglio.det_4.dw_4.object.b_ricerca_fornitore.enabled = ab_enabled
tab_dettaglio.det_4.dw_4.object.b_prezzo_acq.enabled = ab_enabled

tab_dettaglio.det_5.dw_5.object.b_prezzo_ven.enabled = ab_enabled

tab_dettaglio.det_6.dw_6.object.b_sel_prodotto_ragg.enabled = ab_enabled


end subroutine

public subroutine wf_updatestart ();long			ll_cod_barre, li_i, li_i1
string			ls_tabella, ls_codice, ls_cod_prodotto, ls_cod_prodotto_old, ls_des_prodotto,ls_flag

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CMN';
		 
if ls_flag = "S" then
	for li_i = 1 to tab_dettaglio.det_1.dw_1.rowcount()
		if tab_dettaglio.det_1.dw_1.getitemstatus(li_i, 0, primary!) = NewModified! then
			g_mb.warning("Attenzione! Creando un nuovo prodotto di magazzino devi creare anche lo stock/lotto "+&
								"tramite un movimento di magazzino a mano o fare un carico acquisti, "+&
								"altrimenti il magazzino non si movimenta!")
			exit
		end if
	next
end if

for li_i = 1 to pcca.window_currentdw.deletedcount()
   ls_tabella = "anag_prodotti_lingue"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)
	// cancello da tab_prodotti_collegati
   ls_tabella = "tab_prodotti_collegati"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)

	// cancello da anag_prodotti_chiavi
   ls_tabella = "anag_prodotti_chiavi"
   ls_codice  = "cod_prodotto"
   ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto", delete!, true)
   f_del_lingue(ls_tabella, ls_codice, ls_cod_prodotto)
	

next

li_i = 0
do while li_i <= pcca.window_currentdw.rowcount()
   li_i = pcca.window_currentdw.getnextmodified(li_i, Primary!)

   if li_i = 0 then
      return
   end if

	select cod_barre
   into   :ll_cod_barre
   from   con_magazzino
   where  cod_azienda = :s_cs_xx.cod_azienda and 
	       cod_barre_azienda <> 0;

   if sqlca.sqlcode = 0 then
      if pcca.window_currentdw.getitemnumber(li_i, "cod_barre") = 0 then
         pcca.window_currentdw.setitem(li_i, "cod_barre", ll_cod_barre + 1)
   
         update con_magazzino
         set    cod_barre = :ll_cod_barre + 1
         where  cod_azienda = :s_cs_xx.cod_azienda;
      end if
 
      ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto")
      ll_cod_barre = pcca.window_currentdw.getitemnumber(li_i, "cod_barre")
 
      declare cu_cod_barre cursor for select cod_prodotto, des_prodotto from anag_prodotti where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto <> :ls_cod_prodotto and cod_barre = :ll_cod_barre and cod_barre <> 0;

      open cu_cod_barre;

      li_i1 = 0
      do while 0 = 0
         li_i1 ++
         fetch cu_cod_barre into :ls_cod_prodotto_old, :ls_des_prodotto;

         if sqlca.sqlcode <> 0 or li_i1 = 1 then exit
      loop

      if sqlca.sqlcode = 0 then
      	g_mb.messagebox("Attenzione", "Codice a barre già utilizzato per " + ls_cod_prodotto_old + " " + ls_des_prodotto + "!", &
                    exclamation!, ok!)
         pcca.error = pcca.window_currentdw.c_fatal
         close cu_cod_barre;
         return
      end if
      close cu_cod_barre;
   end if

   if pcca.window_currentdw.getitemnumber(li_i, "livello_riordino") > 0 and not isnull(pcca.window_currentdw.getitemnumber(li_i, "livello_riordino")) then
		if pcca.window_currentdw.getitemnumber(li_i, "livello_riordino") < pcca.window_currentdw.getitemnumber(li_i, "scorta_minima") then
			g_mb.messagebox("Attenzione", "Il livello di riordino deve essere maggiore o uguale aella scorta minima!", &
						  exclamation!, ok!)
			pcca.error = pcca.window_currentdw.c_fatal
			return
		end if
	end if
   if pcca.window_currentdw.getitemnumber(li_i, "quan_minima") > pcca.window_currentdw.getitemnumber(li_i, "quan_massima") then
   	g_mb.messagebox("Attenzione", "La quantità minima deve essere minore della quantità massima!", &
                 exclamation!, ok!)
      pcca.error = pcca.window_currentdw.c_fatal
      return
   end if
loop


end subroutine

on w_prodotti_tv.create
int iCurrent
call super::create
end on

on w_prodotti_tv.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;datetime				ldt_data_chiusura_annuale_prec, ldt_data_chiusura_annuale, ldt_data_chiusura_periodica

string					ls_fpb


tab_dettaglio.det_8.dw_documenti.settransobject(sqlca)
tab_dettaglio.det_8.dw_documenti.set_parent(tab_dettaglio.det_1.dw_1)

is_codice_filtro = "PROD"
tab_dettaglio.det_1.dw_export.settransobject(sqlca)

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_3.dw_3.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_4.dw_4.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_5.dw_5.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_6.dw_6.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true
tab_dettaglio.det_5.dw_5.ib_dw_detail = true
tab_dettaglio.det_6.dw_6.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0

if not tab_dettaglio.det_7.dw_colonne_dinamiche.uof_set_dw(tab_dettaglio.det_1.dw_1, {"cod_prodotto"}) then
	g_mb.error(tab_dettaglio.det_7.dw_colonne_dinamiche.uof_get_error())
end if


select		data_chiusura_annuale_prec,
			data_chiusura_annuale,
			data_chiusura_periodica,
			label_livello_prod_1,
			label_livello_prod_2,
			label_livello_prod_3,
			label_livello_prod_4,
			label_livello_prod_5
 INTO		:ldt_data_chiusura_annuale_prec,    
			:ldt_data_chiusura_annuale,
			:ldt_data_chiusura_periodica,
			:is_nome_liv_1,
			:is_nome_liv_2,
			:is_nome_liv_3,
			:is_nome_liv_4,
			:is_nome_liv_5
  FROM con_magazzino
 WHERE cod_azienda = :s_cs_xx.cod_azienda;

if g_str.isempty(is_nome_liv_1) then is_nome_liv_1 = "Livello 1"
if g_str.isempty(is_nome_liv_2) then is_nome_liv_2 = "Livello 2"
if g_str.isempty(is_nome_liv_3) then is_nome_liv_3 = "Livello 3"
if g_str.isempty(is_nome_liv_4) then is_nome_liv_4 = "Livello 4"
if g_str.isempty(is_nome_liv_5) then is_nome_liv_5 = "Livello 5"

tab_dettaglio.det_1.dw_1.modify("st_cod_livello_prod_1.text='" + is_nome_liv_1 + ":'")
tab_dettaglio.det_1.dw_1.modify("st_cod_livello_prod_2.text='" + is_nome_liv_2 + ":'")
tab_dettaglio.det_1.dw_1.modify("st_cod_livello_prod_3.text='" + is_nome_liv_3 + ":'")
tab_dettaglio.det_1.dw_1.modify("st_cod_livello_prod_4.text='" + is_nome_liv_4 + ":'")
tab_dettaglio.det_1.dw_1.modify("st_cod_livello_prod_5.text='" + is_nome_liv_5 + ":'")

tab_dettaglio.det_3.dw_3.modify("st_data_chiusura_annuale_prec.text='" + string(ldt_data_chiusura_annuale_prec,"dd/mm/yyyy") + "'")
tab_dettaglio.det_3.dw_3.modify("st_data_chiusura_annuale.text='" + string(relativedate(date(ldt_data_chiusura_annuale),1),"dd/mm/yyyy") + "'")
tab_dettaglio.det_3.dw_3.modify("st_data_chiusura_periodica.text='" + string(ldt_data_chiusura_periodica,"dd/mm/yyyy") + "'")


select flag
into	 :ls_fpb
from	 parametri_azienda
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_parametro = 'FPB';
		 
if sqlca.sqlcode = 0 and not isnull(ls_fpb) and ls_fpb<>"" then
	this.event post pc_imposta_campi_ricerca(ls_fpb)
end if

try
	tab_dettaglio.det_3.dw_3.object.p_impegnato.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	tab_dettaglio.det_3.dw_3.object.p_assegnato.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	tab_dettaglio.det_3.dw_3.object.p_in_spedizione.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	tab_dettaglio.det_3.dw_3.object.p_progressivi.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_modificata.png"
catch (runtimeerror err)
end try



//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//max count e clausola selezione primo record (che è diverso da dbms a dbms)
RegistryGet(	"HKEY_LOCAL_MACHINE\Software\Consulting&Software\database_" + s_cs_xx.profilocorrente ,"enginetype", is_enginetype)

if is_enginetype = "SYBASE_ASA" then
	il_max_row=300
	is_top_clause = " TOP 1 START AT 1 "
	
elseif is_enginetype = "SYBASE_ASE" or is_enginetype = "MSSQL" then
	il_max_row=1000
	is_top_clause = " TOP 1 "

elseif is_enginetype = "ORACLE" then
	il_max_row=1000
	is_top_clause = ""
end if
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_nomenclatura", &
                 sqlca, &
                 "tab_nomenclature", &
                 "cod_nomenclatura", &
                 "des_nomenclatura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_responsabile", &
                 sqlca, &
                 "tab_responsabili", &
                 "cod_responsabile", &
                 "des_responsabile", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_misura_mag", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_misura_peso", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_misura_vol", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_4.dw_4, &
                 "cod_misura_acq", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_misura_lead_time", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_tipo_politica_riordino", &
                 sqlca, &
                 "tab_tipi_politiche_riordino", &
                 "cod_tipo_politica_riordino", &
                 "des_tipo_politica_riordino", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_4.dw_4, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_4.dw_4, &
                 "cod_gruppo_acq", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_5.dw_5, &
                 "cod_misura_ven", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_5.dw_5, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_5.dw_5, &
                 "cod_gruppo_ven", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_2.dw_2, &
                 "cod_formula", &
                 sqlca, &
                 "tab_formule_db", &
                 "cod_formula", &
                 "des_formula", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_livello_prod_1", &
                 sqlca, &
                 "tab_livelli_prod_1", &
                 "cod_livello_prod_1", &
                 "des_livello", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_5.dw_5, &
                 "cod_rifiuto", &
                 sqlca, &
                 "tab_rifiuti", &
                 "cod_rifiuto", &
                 "des_rifiuto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_tipo_costo_mp", &
                 sqlca, &
                 "tab_tipi_costi_mp", &
                 "cod_tipo_costo_mp", &
                 "des_tipo_costo_mp", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
                 "cod_cat_mer", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
					  
					  
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_prodotti_tv
integer x = 1445
integer y = 24
integer width = 3287
integer height = 2128
boolean fixedwidth = false
boolean multiline = true
boolean raggedright = false
boolean boldselectedtext = true
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
det_6 det_6
det_7 det_7
det_8 det_8
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
this.det_6=create det_6
this.det_7=create det_7
this.det_8=create det_8
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5,&
this.det_6,&
this.det_7,&
this.det_8}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
destroy(this.det_6)
destroy(this.det_7)
destroy(this.det_8)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3250
integer height = 2004
string text = "Principale"
dw_export dw_export
dw_1 dw_1
end type

on det_1.create
this.dw_export=create dw_export
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_export
this.Control[iCurrent+2]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_export)
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_prodotti_tv
integer width = 1399
integer height = 2136
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1362
integer height = 2012
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1303
integer height = 1968
string dataobject = "d_prodotti_search_tv"
end type

event dw_ricerca::itemchanged;call super::itemchanged;//integer			li_level, li_valore_livello
//string				ls_column
//
//if row>0 then
//else
//	return
//end if
//
//
//if left(dwo.name, 8) = "livello_" then
//	//se hai selezionato di raggruppare per cod_livello_prod_N (dove N>1)
//	//controlla che al livello immediatamente precedente sia stato impostato il raggruppamento per cod_livello_prod_N-1
//
//	//che numero di livello di raggruppamento stai tentando di impostare sul filtro per raggruppamento (1, 2, 3, 4, 5 ???)
//	li_level = integer(right(dwo.name, 1))
//
//	//quale cod_livello_prod stai cercando di di impostare sul filtro per raggruppamento sulla colonna <dwo.name>   ("1","2","3","4","5",)
//	ls_column = dwo.name
//	li_valore_livello = integer(dw_ricerca.getitemstring(row, ls_column))
//
//	//se hai impostato cod_livello_prod_1, non c'è problema su raggruppamenti precedenti
//	if li_valore_livello > 1 then
//		//se sei al livello di raggruppamento 1, non puoi
//		if li_level = 1 then
//			//NON PUOI
//			return 1
//		else
//			//vai a vedere cosa hai impostato al livello immediatamente precedente
//			li_level -= 1
//			ls_column = "livello_"+ string(li_level)
//
//			if integer(dw_ricerca.getitemstring(row, ls_column)) = li_valore_livello - 1 then
//				//OK
//			else
//				//NON PUOI
//				return 1
//			end if
//		end if
//	end if
//end if
end event

event dw_ricerca::clicked;call super::clicked;string				ls_sql
long				ll_ret

choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
	
	case "b_ricerca_nomenclatura"
		guo_ricerca.uof_set_where("cod_nomenclatura IN ("+&
															"select DISTINCT cod_nomenclatura "+&
															"from anag_prodotti "+&
															"WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																		"cod_nomenclatura is not null and cod_nomenclatura<>'' )")
		guo_ricerca.uof_ricerca_nomenclature(dw_ricerca, "cod_nomenclatura")
	
	case "b_ricerca_prod_alt"
		guo_ricerca.uof_set_where("cod_prodotto IN ("+&
															"select DISTINCT cod_prodotto_alt "+&
															"from anag_prodotti "+&
															"WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																		"cod_prodotto_alt is not null and cod_prodotto_alt<>'' )")
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_alt")
	
	case "b_ricerca_prod_ragg"
		guo_ricerca.uof_set_where("cod_prodotto IN ("+&
															"select DISTINCT cod_prodotto_raggruppato "+&
															"from anag_prodotti "+&
															"WHERE cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																		"cod_prodotto_raggruppato is not null and cod_prodotto_raggruppato<>'' )")
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_raggruppato")
	
	case "b_esporta"
		if g_str.isempty(is_where_to_export) then is_where_to_export = "where anag_prodotti.cod_azienda='"+s_cs_xx.cod_azienda+"'"
		
		ls_sql = tab_dettaglio.det_1.dw_export.getsqlselect()
		ll_ret = pos(upper(ls_sql), "FROM")
		ls_sql = left(ls_sql, ll_ret -1)
		ls_sql += " FROM anag_prodotti "
		ls_sql += is_where_to_export
		tab_dettaglio.det_1.dw_export.setsqlselect(ls_sql)
		
		pcca.mdi_frame.setmicrohelp("Preparazione query di esportazione in corso ...")
					
		ll_ret = tab_dettaglio.det_1.dw_export.retrieve()
		if ll_ret<0 then
			pcca.mdi_frame.setmicrohelp("Pronto!")
			g_mb.error("Errore in retrieve datawindow per export!")
			return
		elseif ll_ret = 0 then
			pcca.mdi_frame.setmicrohelp("Pronto!")
			g_mb.warning("Nessun dato da esportare con questo filtro impostato!")
			return
		end if
		
		pcca.mdi_frame.setmicrohelp("Attendere creazione file!")
		tab_dettaglio.det_1.dw_export.saveas( )
		pcca.mdi_frame.setmicrohelp("Pronto!")
		
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1362
integer height = 2012
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer x = 14
integer y = 16
integer width = 1344
integer height = 1992
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::rightclicked;call super::rightclicked;

long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_anag_prodotti lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row =tab_dettaglio.det_1.dw_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then
	lm_menu = create m_anag_prodotti
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	destroy lm_menu
end if
end event

type dw_export from datawindow within det_1
boolean visible = false
integer x = 2437
integer y = 1272
integer width = 631
integer height = 308
integer taborder = 21
string title = "none"
string dataobject = "d_prodotti_lista_to_export"
boolean livescroll = true
end type

type dw_1 from uo_cs_xx_dw within det_1
integer x = 5
integer width = 3255
integer height = 1940
integer taborder = 30
string dataobject = "d_prodotti_det_1_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) then return
if upperbound(istr_data.stringa[]) <= 0 then return
if g_str.isempty(istr_data.stringa[1]) then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.stringa[1])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()

tab_dettaglio.det_7.dw_colonne_dinamiche.reset()
end event

event itemchanged;call super::itemchanged;string				ls_null, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, ls_cod_livello_prod_4
long				ll_cont

setnull(ls_null)

choose case i_colname
	case "cod_prodotto"
		select count(cod_prodotto)
		into   :ll_cont
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :i_coltext;
		if ll_cont > 0 then
			g_mb.warning("Esiste già un prodotto con questo codice!")
			return 1
		end if


	case "cod_livello_prod_1"
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_2", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_3", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_4", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_2", &
                       sqlca, &
                       "tab_livelli_prod_2", &
                       "cod_livello_prod_2", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
										"cod_livello_prod_1 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_3", &
                       sqlca, &
                       "tab_livelli_prod_3", &
                       "cod_livello_prod_3", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + i_coltext + "' and (cod_livello_prod_2 = '' or cod_livello_prod_2 is null)")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + i_coltext + "' and (cod_livello_prod_2 = '' or cod_livello_prod_2 is null) and "+&
							  "(cod_livello_prod_3 = '' or cod_livello_prod_3 is null)")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + i_coltext + "' and (cod_livello_prod_2 = '' or cod_livello_prod_2 is null) and "+&
							  "(cod_livello_prod_3 = '' or cod_livello_prod_3 is null) and (cod_livello_prod_4 = '' or cod_livello_prod_4 is null)")
  

case "cod_livello_prod_2"
		ls_cod_livello_prod_1 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_1")
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_3", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_4", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_3", &
                       sqlca, &
                       "tab_livelli_prod_3", &
                       "cod_livello_prod_3", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "' and "+&
							  "(cod_livello_prod_3 = '' or cod_livello_prod_3 is null)")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + i_coltext + "' and "+&
							  "(cod_livello_prod_3 = '' or cod_livello_prod_3 is null) and "+&
							  "(cod_livello_prod_4 = '' or cod_livello_prod_4 is null)")
   
	
	case "cod_livello_prod_3"
		ls_cod_livello_prod_1 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_1")
		ls_cod_livello_prod_2 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_2")
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_4", ls_null)
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_4", &
                       sqlca, &
                       "tab_livelli_prod_4", &
                       "cod_livello_prod_4", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + i_coltext + "'")
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + i_coltext + "' and "+&
							  "(cod_livello_prod_4 = '' or cod_livello_prod_4 is null)")


	case "cod_livello_prod_4"
		ls_cod_livello_prod_1 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_1")
		ls_cod_livello_prod_2 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_2")
		ls_cod_livello_prod_3 = dw_1.getitemstring(dw_1.getrow(), "cod_livello_prod_3")
		dw_1.setitem(dw_1.getrow(), "cod_livello_prod_5", ls_null)
		
		f_po_loaddddw_dw(dw_1, &
                       "cod_livello_prod_5", &
                       sqlca, &
                       "tab_livelli_prod_5", &
                       "cod_livello_prod_5", &
                       "des_livello", &
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and "+&
							  "cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + i_coltext + "'")

	case "cod_misura_mag"
		if g_str.isempty(tab_dettaglio.det_2.dw_2.getitemstring(row, "cod_misura_acq")) and data<>"" then
			tab_dettaglio.det_4.dw_4.setitem(row, "cod_misura_acq", data)
		end if
		if g_str.isempty(tab_dettaglio.det_2.dw_2.getitemstring(row, "cod_misura_ven")) and data<>"" then
			tab_dettaglio.det_5.dw_5.setitem(row, "cod_misura_ven", data)
		end if

		if data <> tab_dettaglio.det_2.dw_2.getitemstring(row, "cod_misura_acq") then
	 		tab_dettaglio.det_4.dw_4.modify("st_fattore_conv.text='(1 "+trim(data)+"="+trim(f_double_string(tab_dettaglio.det_2.dw_2.getitemnumber(row,"fat_conversione_acq")))+" "+tab_dettaglio.det_2.dw_2.getitemstring(row,"cod_misura_acq")+")'")
		else
			tab_dettaglio.det_4.dw_4.modify("st_fattore_conv.text=''")
		end if

		if data <> tab_dettaglio.det_2.dw_2.getitemstring(row,"cod_misura_ven") then
	 		tab_dettaglio.det_5.dw_5.modify("st_fattore_conv.text='(1 "+trim(data)+"="+trim(f_double_string(tab_dettaglio.det_2.dw_2.getitemnumber(row,"fat_conversione_ven")))+" "+tab_dettaglio.det_2.dw_2.getitemstring(row,"cod_misura_ven")+")'")
		else
			tab_dettaglio.det_5.dw_5.modify("st_fattore_conv.text=''")
		end if

end choose

end event

event rowfocuschanged;call super::rowfocuschanged;string				ls_cod_prodotto, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
					ls_cod_livello_prod_4, ls_cod_livello_prod_5 
long				ll_cont, ll_row


if i_extendmode then
	
	ll_row = dw_1.getrow()
	
	if ll_row > 0 then
		
		ls_cod_prodotto = dw_1.getitemstring(ll_row, "cod_prodotto")
		
		tab_dettaglio.det_7.dw_colonne_dinamiche.retrieve()
		
		dw_1.object.note_t.visible = false
		
		select count(*)
		into   :ll_cont
		from   anag_prodotti_note
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
		
		if ll_cont > 0 then
			dw_1.object.note_t.visible = true
		end if

		tab_dettaglio.det_4.dw_4.object.b_prezzo_acq.text = "Prezzo al " + dw_1.getitemstring(ll_row, "cod_misura_acq")
		tab_dettaglio.det_5.dw_5.object.b_prezzo_ven.text = "Prezzo al " + dw_1.getitemstring(ll_row, "cod_misura_ven")

		if dw_1.getitemstring(ll_row, "cod_misura_mag") <> dw_1.getitemstring(ll_row, "cod_misura_acq") then
			tab_dettaglio.det_4.dw_4.modify("st_fattore_conv.text='(1 " + trim(dw_1.getitemstring(ll_row, "cod_misura_mag")) + "=" + trim(f_double_string(dw_1.getitemnumber(ll_row, "fat_conversione_acq"))) + " " + dw_1.getitemstring(ll_row, "cod_misura_acq") + ")'")
		else
			tab_dettaglio.det_4.dw_4.modify("st_fattore_conv.text=''")
		end if

		if dw_1.getitemstring(ll_row, "cod_misura_mag") <> dw_1.getitemstring(dw_1.getrow(),"cod_misura_ven") then
			tab_dettaglio.det_5.dw_5.modify("st_fattore_conv.text='(1 " + trim(dw_1.getitemstring(ll_row, "cod_misura_mag")) + "=" + trim(f_double_string(dw_1.getitemnumber(ll_row, "fat_conversione_ven"))) + " " + dw_1.getitemstring(ll_row, "cod_misura_ven") + ")'")
		else
			tab_dettaglio.det_5.dw_5.modify("st_fattore_conv.text=''")
		end if
		
		if isnull(ls_cod_prodotto) then
			ls_cod_prodotto = ""
		end if

		ls_cod_livello_prod_1 = dw_1.getitemstring(ll_row, "cod_livello_prod_1")
		ls_cod_livello_prod_2 = dw_1.getitemstring(ll_row, "cod_livello_prod_2")
		ls_cod_livello_prod_3 = dw_1.getitemstring(ll_row, "cod_livello_prod_3")
		ls_cod_livello_prod_4 = dw_1.getitemstring(ll_row, "cod_livello_prod_4")
		ls_cod_livello_prod_5 = dw_1.getitemstring(ll_row, "cod_livello_prod_5")
		
		f_po_loaddddw_dw(dw_1, &
							  "cod_livello_prod_2", &
							  sqlca, &
							  "tab_livelli_prod_2", &
							  "cod_livello_prod_2", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "'")
		f_po_loaddddw_dw(dw_1, &
							  "cod_livello_prod_3", &
							  sqlca, &
							  "tab_livelli_prod_3", &
							  "cod_livello_prod_3", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "'")
		f_po_loaddddw_dw(dw_1, &
							  "cod_livello_prod_4", &
							  sqlca, &
							  "tab_livelli_prod_4", &
							  "cod_livello_prod_4", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_1 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "'")
		f_po_loaddddw_dw(dw_1, &
							  "cod_livello_prod_5", &
							  sqlca, &
							  "tab_livelli_prod_5", &
							  "cod_livello_prod_5", &
							  "des_livello", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and cod_livello_prod_1 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_2 = '" + ls_cod_livello_prod_2 + "' and cod_livello_prod_3 = '" + ls_cod_livello_prod_3 + "' and cod_livello_prod_4 = '" + ls_cod_livello_prod_4 + "'")
	else

		f_po_loaddddw_dw(tab_dettaglio.det_4.dw_4, &
						  "cod_prodotto_alt", &
						  sqlca, &
						  "anag_prodotti", &
						  "cod_prodotto", &
						  "des_prodotto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	end if
	
	if tab_dettaglio.det_7.dw_colonne_dinamiche.rowcount() > 0 then tab_dettaglio.det_7.dw_colonne_dinamiche.event trigger rowfocuschanged(1)
	tab_dettaglio.det_8.dw_documenti.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
	
end if
end event

event buttonclicked;call super::buttonclicked;string				ls_cod_prodotto


choose case dwo.name
	case "b_duplica_prodotto"
		if row>0 then
		else
			return
		end if
		
		ls_cod_prodotto = dw_1.getitemstring(row,"cod_prodotto")
		
		if g_str.isempty(ls_cod_prodotto) then return
		
		s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
		s_cs_xx.parametri.parametro_s_2 = dw_1.getitemstring(row,"des_prodotto")
		window_open(w_duplica_prodotto,0)
		
end choose
end event

event pcd_new;call super::pcd_new;long				ll_cont, ll_row
string				ls_cod_prodotto, ls_test, ls_cod_deposito_default


wf_abilita_pulsanti(true)

ll_row = dw_1.getrow()

dw_1.setitem(ll_row, "data_creazione", datetime(today()))

ib_new = true

select cod_deposito
into	:ls_cod_deposito_default
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

dw_1.setitem(ll_row, "cod_deposito", ls_cod_deposito_default)
end event

event pcd_modify;call super::pcd_modify;

wf_abilita_pulsanti(true)

ib_new = false
end event

event pcd_view;call super::pcd_view;

wf_abilita_pulsanti(false)

ib_new = false
end event

event updatestart;call super::updatestart;if i_extendmode then
	tab_dettaglio.det_7.dw_colonne_dinamiche.uof_Delete()
	dw_1.change_dw_current()
	wf_updatestart()
	
	//elimino nodo selezionato
	long ll_handle
	tab_ricerca.selezione.tv_selezione.findItem(CurrentTreeItem!, ll_handle)
	if ll_handle > 0 then tab_ricerca.selezione.tv_selezione.deleteitem(ll_handle)
     if DeletedCount() > 0 then
		wf_set_deleted_item_status()
	end if

	if ib_new then
		wf_inserisci_prod(0, dw_1.getitemstring(getrow(), "cod_prodotto"), dw_1.getitemstring(getrow(), "des_prodotto"))
	end if
	
end if
end event

event updateend;call super::updateend;long				li_i, ll_handle
string				ls_cod_misura_mag, ls_cod_prodotto
treeviewitem	ltvi_item
str_treeview		lstr_data



//Donato 27-10-2008
//se è cambiata la unita di misura allora occorre aggiornare nelle eventuali distinte
//in cui il prodotto è presente
for li_i = 1 to tab_dettaglio.det_1.dw_1.rowcount()
	if tab_dettaglio.det_1.dw_1.getitemstatus(li_i, "cod_misura_mag", primary!) = DataModified! then	
		ls_cod_misura_mag = tab_dettaglio.det_1.dw_1.getitemstring(li_i, "cod_misura_mag")
		ls_cod_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(li_i, "cod_prodotto")
		
		update distinta
		set cod_misura = :ls_cod_misura_mag
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto_figlio = :ls_cod_prodotto;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in aggiornamento cod_misura in tabella distinta per prodotto figlio prodotto:"+ls_cod_prodotto + " : "+sqlca.sqlerrtext)
		end if
	end if
next
//fine modifica -----------------------------

// stefanop: salvo colonne dinamiche
tab_dettaglio.det_7.dw_colonne_dinamiche.update()



end event

event pcd_setkey;call super::pcd_setkey;long   ll_i, ll_cont
string ls_cod_prodotto, ls_test


for ll_i = 1 to dw_1.rowcount()
	if isnull(dw_1.getitemstring(ll_i, "cod_azienda")) then
		dw_1.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
next

end event

event rowfocuschanging;call super::rowfocuschanging;if currentrow <> newrow then
	tab_dettaglio.det_7.dw_colonne_dinamiche.uof_verify_changes()
end if
end event

event pcd_save;call super::pcd_save;


ib_new = false
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
event ue_dettaglio_stabilimento ( )
integer y = 12
integer width = 3269
integer height = 1892
integer taborder = 11
string dataobject = "d_prodotti_det_5"
boolean border = false
end type

event ue_dettaglio_stabilimento();string		ls_cod_prodotto, ls_cod_reparto, ls_cod_lavorazione, ls_cod_versione
long		ll_row

ll_row = dw_2.getrow()
if ll_row>0 then
else
	return
end if

ls_cod_prodotto = dw_2.getitemstring(ll_row, "cod_prodotto")
ls_cod_reparto = dw_2.getitemstring(ll_row, "cod_reparto")
	
if 		ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto)			then
else
	return
end if

window_open_parm(w_prodotti_reparti_depositi, -1, tab_dettaglio.det_1.dw_1)
end event

event buttonclicked;call super::buttonclicked;

choose case dwo.name
	case "b_dettaglio"
		triggerevent("ue_dettaglio_stabilimento")
		
end choose
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Situazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
integer x = 5
integer y = 12
integer width = 3232
integer height = 1864
integer taborder = 11
string dataobject = "d_prodotti_det_2"
boolean border = false
end type

event clicked;call super::clicked;long											ll_row
string											ls_cod_prodotto
w_report_impegnato_prodotto			lw_imp
w_report_assegnato_prodotto			lw_ass
w_report_inspediz_prodotto				lw_insped
w_aggiorna_impegnato					lw_progressivi


if isvalid(dwo) then
	
	ll_row = getrow()
	if ll_row> 0 then
		ls_cod_prodotto = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_prodotto")
	end if
	
	choose case dwo.name
		case "p_impegnato"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_imp, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)

		case "p_assegnato"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_ass, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)

		case "p_in_spedizione"
			if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then opensheetwithparm(lw_insped, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)

		case "p_progressivi"
			opensheetwithparm(lw_progressivi, ls_cod_prodotto, PCCA.MDI_Frame, 6,  Original!)

	end choose
end if

end event

event ue_key;call super::ue_key;

if key = keyf12! and keyflags = 1 then
	s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_prodotto")
	open(w_prodotti_ricalcolo_saldi)
end if
end event

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Acquisti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
integer x = 23
integer y = 8
integer width = 3195
integer height = 1756
integer taborder = 11
string dataobject = "d_prodotti_det_3_tv"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if


choose case dwo.name
	case "b_ricerca_prodotto_alt"
		guo_ricerca.uof_ricerca_prodotto(dw_4, "cod_prodotto_alt")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_4, "cod_fornitore")
	
	//pulsante spostato come butto object della dw
	case "b_cc_acquisti"
		if tab_dettaglio.det_1.dw_1.rowcount() > 0 then
			window_open_parm(w_prodotti_cc_acq, -1, tab_dettaglio.det_1.dw_1)
		end if
	
	//pulsante spostato come butto object della dw
	case "b_prezzo_acq"
		s_cs_xx.parametri.parametro_s_1 = dw_4.getitemstring(row, "cod_misura_acq")
		s_cs_xx.parametri.parametro_d_1 = dw_4.getitemdecimal(row, "prezzo_acquisto")
		s_cs_xx.parametri.parametro_d_2 = dw_4.getitemdecimal(row, "fat_conversione_acq")
		
		window_open(w_prezzo_um_prodotti, 0)
		
		if s_cs_xx.parametri.parametro_d_1 <> 0 then
			dw_4.setitem(row, "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
		end if
	
end choose

end event

event itemchanged;call super::itemchanged;
if row>0 then
else
	return
end if

choose case i_colname
	case "cod_misura_acq"
		dw_4.object.b_prezzo_acq.text = "Prezzo al " + i_coltext
		if dw_4.getitemstring(row, "cod_misura_mag") <> i_coltext then
	 		dw_4.modify("st_fattore_conv.text='(1 " + trim(dw_4.getitemstring(row, "cod_misura_mag")) + "=" + trim(f_double_string(dw_4.getitemnumber(row, "fat_conversione_acq"))) + " " + i_coltext + ")'")
		else
			dw_4.modify("st_fattore_conv.text=''")
		end if
		
	case "fat_conversione_acq"
		if dw_4.getitemstring(row, "cod_misura_mag") <> dw_4.getitemstring(row, "cod_misura_acq") then
	 		dw_4.modify("st_fattore_conv.text='(1 " + trim(dw_4.getitemstring(row, "cod_misura_mag")) + "=" + i_coltext + " " + trim(dw_4.getitemstring(row, "cod_misura_acq")) + ")'")
		else
			dw_4.modify("st_fattore_conv.text=''")
		end if
end choose

end event

type det_5 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Vendite"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_5 dw_5
end type

on det_5.create
this.dw_5=create dw_5
this.Control[]={this.dw_5}
end on

on det_5.destroy
destroy(this.dw_5)
end on

type dw_5 from uo_cs_xx_dw within det_5
integer x = 18
integer y = 16
integer width = 3209
integer height = 1740
integer taborder = 11
string dataobject = "d_prodotti_det_4_tv"
boolean border = false
end type

event itemchanged;call super::itemchanged;if row>0 then
else
	return 
end if


choose case i_colname
	case "cod_misura_ven"
		dw_5.object.b_prezzo_ven.text = "Prezzo al " + i_coltext
		if dw_5.getitemstring(row,"cod_misura_mag") <> i_coltext then
	 		dw_5.modify("st_fattore_conv.text='(1 " + trim(dw_5.getitemstring(row, "cod_misura_mag")) + "=" + trim(f_double_string(dw_5.getitemnumber(row, "fat_conversione_ven"))) + " " + i_coltext + ")'")
		else
	 		dw_5.modify("st_fattore_conv.text=''")
		end if

	case "fat_conversione_ven"
		if this.getitemstring(row, "cod_misura_mag") <> dw_5.getitemstring(row, "cod_misura_ven") then
	 		dw_5.modify("st_fattore_conv.text='(1 " + trim(dw_5.getitemstring(row, "cod_misura_mag")) + "=" + i_coltext + " " + dw_5.getitemstring(row, "cod_misura_ven") + ")'")
		else
	 		dw_5.modify("st_fattore_conv.text=''")
		end if

end choose

end event

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if


choose case dwo.name
	
	//pulsante spostato come butto object della dw
	case "b_cc_vendite"
		if tab_dettaglio.det_1.dw_1.rowcount() > 0 then
			window_open_parm(w_prodotti_cc_ven, -1, tab_dettaglio.det_1.dw_1)
		end if
	
	//pulsante spostato come butto object della dw
	case "b_prezzo_ven"
		s_cs_xx.parametri.parametro_s_1 = dw_5.getitemstring(row, "cod_misura_ven")
		s_cs_xx.parametri.parametro_d_1 = dw_5.getitemdecimal(row, "prezzo_vendita")
		s_cs_xx.parametri.parametro_d_2 = dw_5.getitemdecimal(row, "fat_conversione_ven")
		
		window_open(w_prezzo_um_prodotti, 0)
		
		if s_cs_xx.parametri.parametro_d_1 <> 0 then
			dw_5.setitem(row, "prezzo_vendita", s_cs_xx.parametri.parametro_d_1)
		end if
	
end choose

end event

type det_6 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Raggruppo"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_6 dw_6
end type

on det_6.create
this.dw_6=create dw_6
this.Control[]={this.dw_6}
end on

on det_6.destroy
destroy(this.dw_6)
end on

type dw_6 from uo_cs_xx_dw within det_6
integer y = 32
integer width = 3264
integer height = 1028
integer taborder = 11
string dataobject = "d_prodotti_det_6"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_sel_prodotto_ragg"
		guo_ricerca.uof_ricerca_prodotto(dw_6, "cod_prodotto_raggruppato")
		
end choose


end event

event itemchanged;call super::itemchanged;string ls_modify, ls_cod_prodotto, ls_cod_prodotto_raggruppato, ls_cod_misura_mag
dec{5} ld_fat_conv_rag

if row>0 then
else
	return
end if

choose case dwo.name
	case "fat_conversione_rag_mag"
		
		ls_cod_prodotto_raggruppato 	= dw_6.getitemstring(row, "cod_prodotto_raggruppato")
		ls_cod_prodotto				 	= dw_6.getitemstring(row, "cod_prodotto")

		if not isnull(ls_cod_prodotto_raggruppato) and len(ls_cod_prodotto_raggruppato) > 0 then
			
			select cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto_raggruppato;
			
			ld_fat_conv_rag = dec(data)
			ls_modify = "( 1 " + trim(dw_6.getitemstring(row,"cod_misura_mag"))+" "+ls_cod_prodotto+"="+string(ld_fat_conv_rag,"###,##0.0####")+" "+trim( ls_cod_misura_mag )+" "+ls_cod_prodotto_raggruppato+" )"
			dw_6.object.st_fattore_conv_acq.text = ls_modify
			
		end if
		
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_modify, ls_cod_prodotto, ls_cod_prodotto_raggruppato, ls_cod_misura_mag
dec{5} ld_fat_conv_rag_mag

if i_CursorRow > 0 then
	
	ls_cod_prodotto_raggruppato	= dw_6.getitemstring(this.getrow(),"cod_prodotto_raggruppato")
	ls_cod_prodotto				 	= dw_6.getitemstring(this.getrow(),"cod_prodotto")

	if not isnull(ls_cod_prodotto_raggruppato) and len(ls_cod_prodotto_raggruppato) > 0 then
		
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
				 
		ld_fat_conv_rag_mag = dw_6.getitemnumber(this.getrow(),"fat_conversione_rag_mag")
		
		ls_modify = "( 1 " + trim(dw_6.getitemstring(dw_6.getrow(),"cod_misura_mag")) + " " + ls_cod_prodotto + "=" + string(ld_fat_conv_rag_mag,"###,##0.0####") + " " + trim( ls_cod_misura_mag ) + " " + ls_cod_prodotto_raggruppato + " )"
		dw_6.object.st_fattore_conv_acq.text = ls_modify
	end if
end if
end event

type det_7 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Col.Dinamiche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_colonne_dinamiche dw_colonne_dinamiche
end type

on det_7.create
this.dw_colonne_dinamiche=create dw_colonne_dinamiche
this.Control[]={this.dw_colonne_dinamiche}
end on

on det_7.destroy
destroy(this.dw_colonne_dinamiche)
end on

type dw_colonne_dinamiche from uo_colonne_dinamiche_dw within det_7
integer x = 41
integer y = 52
integer width = 3186
integer height = 1928
integer taborder = 21
end type

type det_8 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3250
integer height = 2004
long backcolor = 12632256
string text = "Documenti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_documenti dw_documenti
end type

on det_8.create
this.dw_documenti=create dw_documenti
this.Control[]={this.dw_documenti}
end on

on det_8.destroy
destroy(this.dw_documenti)
end on

type dw_documenti from uo_drag_prodotti within det_8
integer x = 41
integer y = 36
integer width = 3186
integer height = 1944
integer taborder = 31
end type

