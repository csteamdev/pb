﻿$PBExportHeader$w_report_lifo_annuale.srw
$PBExportComments$Report Lifo a Scatti Annuale
forward
global type w_report_lifo_annuale from w_cs_xx_principale
end type
type cb_selezione from commandbutton within w_report_lifo_annuale
end type
type dw_selezione from uo_cs_xx_dw within w_report_lifo_annuale
end type
type dw_report from uo_cs_xx_dw within w_report_lifo_annuale
end type
end forward

global type w_report_lifo_annuale from w_cs_xx_principale
integer width = 3557
integer height = 1656
string title = "Report Lifo Annuale"
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_lifo_annuale w_report_lifo_annuale

forward prototypes
public function integer wf_report ()
public subroutine wf_set_size (boolean ab_ricerca)
end prototypes

public function integer wf_report ();string ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_flag_descrizione, ls_ricerca_avanzata, ls_flag_tipo_ricerca, ls_filtro, ls_flag_lifo, ls_cod_deposito, ls_flag_fiscale
long   ll_anno_lifo, ll_i, ll_y

dw_selezione.accepttext()
dw_report.reset()

ll_anno_lifo = dw_selezione.getitemnumber(1,"anno_lifo")
ls_cod_prodotto_inizio = dw_selezione.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_selezione.getitemstring(1,"cod_prodotto_fine")
ls_flag_descrizione = dw_selezione.getitemstring(1,"flag_tipo_descrizione")
ls_ricerca_avanzata = dw_selezione.getitemstring(1,"cod_prodotto")
ls_flag_tipo_ricerca = dw_selezione.getitemstring(1,"flag_tipo_ricerca")
ls_flag_lifo = dw_selezione.getitemstring(1,"flag_lifo")
ls_flag_fiscale = dw_selezione.getitemstring(1,"flag_fiscale")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")

if isnull(ls_cod_prodotto_inizio) then ls_cod_prodotto_inizio = "!"
if isnull(ls_cod_prodotto_fine) then ls_cod_prodotto_fine = "{"

//if ls_flag_lifo = "N" then setnull(ls_flag_lifo)
if ls_flag_lifo <> "N" and ls_flag_lifo<>"S" then setnull(ls_flag_lifo)

if ls_flag_fiscale <> "N" and ls_flag_fiscale<>"S" then setnull(ls_flag_fiscale)

dw_report.retrieve(s_cs_xx.cod_azienda, ll_anno_lifo, ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_flag_descrizione, ls_flag_lifo, ls_cod_deposito,ls_flag_fiscale)

// --------------------- APPLICO FILTRO SPECIALE ---------------------------------

if isnull(ls_ricerca_avanzata) then
	ls_filtro = ""
else
	ll_i = len(ls_ricerca_avanzata)
	for ll_y = 1 to ll_i
		if mid(ls_ricerca_avanzata, ll_y, 1) = "*" then
			ls_ricerca_avanzata = replace(ls_ricerca_avanzata, ll_y, 1, "%")
		end if
	next
	
	if ls_flag_tipo_ricerca = "C" then
		ls_filtro = "cod_prodotto like '" + ls_ricerca_avanzata + "'"
	else
		ls_filtro = "anag_prodotti.des_prodotto like '" + ls_ricerca_avanzata + "'"
	end if
end if

dw_report.setfilter(ls_filtro)
dw_report.filter()
 // --------------------------------  FINE FILTRO -----------------------------------
 
//dw_report.show()
//cb_selezione.show()

dw_report.change_dw_current()
dw_report.groupcalc()

wf_set_size(false)

//x = 100
//y = 50
width = 3553
height = 1665

return 1

end function

public subroutine wf_set_size (boolean ab_ricerca);if ab_ricerca then
	dw_selezione.move(20,20)
	width = dw_selezione.width + 80
	height = dw_selezione.height + 140
end if

dw_selezione.visible = ab_ricerca

dw_report.visible = not ab_ricerca
cb_selezione.visible = not ab_ricerca
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report


// dimensioni
wf_set_size(true)
end event

on w_report_lifo_annuale.create
int iCurrent
call super::create
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_selezione
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_report
end on

on w_report_lifo_annuale.destroy
call super::destroy
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event resize;/* TOLTO ANCESTOR */


cb_selezione.move(newwidth - cb_selezione.width - 40, newheight - cb_selezione.height - 20)

dw_report.move(20,20)
dw_report.resize(newwidth - 20, cb_selezione.y - 40)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_selezione from commandbutton within w_report_lifo_annuale
boolean visible = false
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;//string ls_prodotto, ls_cat_mer
//
//cb_selezione.hide()
//dw_report.hide()
//
//dw_selezione.change_dw_current()
//dw_selezione.show()
//cb_ricerca_prod_inizio.show()
//cb_ricerca_prod_fine.show()
//
//parent.x = 741
//parent.y = 885
//parent.width = 2651
//parent.height = 708
//

wf_set_size(true)
end event

type dw_selezione from uo_cs_xx_dw within w_report_lifo_annuale
integer x = 23
integer y = 20
integer width = 2560
integer height = 656
integer taborder = 20
string dataobject = "d_selezione_report_lifo_annuale"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_report"
		wf_report()
		
	case "b_annulla"
		
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione, "cod_prodotto_inizio")
		
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione, "cod_prodotto_fine")
		
		
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_lifo_annuale
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_lifo_annuale"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

