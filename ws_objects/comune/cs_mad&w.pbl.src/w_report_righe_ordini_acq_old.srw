﻿$PBExportHeader$w_report_righe_ordini_acq_old.srw
forward
global type w_report_righe_ordini_acq_old from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_report_righe_ordini_acq_old
end type
type dw_report from uo_cs_xx_dw within w_report_righe_ordini_acq_old
end type
type cb_annulla from commandbutton within w_report_righe_ordini_acq_old
end type
type cb_report from commandbutton within w_report_righe_ordini_acq_old
end type
end forward

global type w_report_righe_ordini_acq_old from w_cs_xx_principale
integer width = 3598
integer height = 2000
string title = "Report Righe Ordini Acquisto"
dw_selezione dw_selezione
dw_report dw_report
cb_annulla cb_annulla
cb_report cb_report
end type
global w_report_righe_ordini_acq_old w_report_righe_ordini_acq_old

event pc_setwindow;call super::pc_setwindow;string ls_des_azienda

dw_report.ib_dw_report = true

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
save_on_close(c_socnosave)
iuo_dw_main = dw_report
//dw_selezione.resetupdate()

end event

event activate;call super::activate;this.x = 20
this.y = 20
this.width = 3600
this.height = 2000

end event

on w_report_righe_ordini_acq_old.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_report
end on

on w_report_righe_ordini_acq_old.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_annulla)
destroy(this.cb_report)
end on

type dw_selezione from uo_cs_xx_dw within w_report_righe_ordini_acq_old
integer y = 20
integer width = 3109
integer height = 380
integer taborder = 40
string dataobject = "d_sel_righe_report_ordini_acq"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)

dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "anno", 2000)
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)
dw_selezione.setitem(1, "flag_blocco", 'T')
dw_selezione.setitem(1, "flag_evasione", 'T')
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

type dw_report from uo_cs_xx_dw within w_report_righe_ordini_acq_old
integer x = 23
integer y = 400
integer width = 3520
integer height = 1472
integer taborder = 50
string dataobject = "d_righe_report_ordini_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_annulla from commandbutton within w_report_righe_ordini_acq_old
integer x = 3182
integer y = 296
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
date ld_null

setnull(ls_null)
setnull(ld_null)

//dw_selezione.insertrow(0)
dw_selezione.setitem(1, "cod_fornitore", ls_null)
dw_selezione.setitem(1, "cod_prodotto", ls_null)
dw_selezione.setitem(1, "data_da", ld_null)
dw_selezione.setitem(1, "data_a", ld_null)
dw_selezione.setitem(1, "anno", 2000)
dw_selezione.setitem(1, "num_ordine_da", 1)
dw_selezione.setitem(1, "num_ordine_a", 999999)
dw_selezione.setitem(1, "flag_blocco", 'T')
dw_selezione.setitem(1, "flag_evasione", 'T')
end event

type cb_report from commandbutton within w_report_righe_ordini_acq_old
integer x = 3182
integer y = 196
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_fornitore_sel, ls_cod_prodotto_sel, ls_flag_blocco_sel, ls_flag_evasione_sel
date ld_data_da_sel, ld_data_a_sel
long ll_anno_sel, ll_num_ordine_da_sel, ll_num_ordine_a_sel
string ls_cod_prodotto, ls_des_prodotto, ls_unita_misura, ls_iva, ls_stato, ls_tipo_listino, &
		 ls_valuta, ls_pagamento, ls_fornitore, ls_stato_ordine, ls_flag_saldo
long ll_prog_riga, ll_confezioni, ll_pezzi_x_confezione, ll_num_ordine, ll_anno_ordine, ll_i
double ld_qta_ordine, ld_residuo_ordine, ld_prezzo_acq, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
		 ld_sconto_4,  ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_valore_riga, ld_sconto, ld_quan_arrivata
datetime ldt_data_consegna, ldt_data_consegna_for, ldt_data_registrazione
string ls_dettaglio, ls_testata, ls_rag_soc_1, ls_da, ls_a

dw_report.Reset( ) 

dw_selezione.accepttext()
ls_cod_fornitore_sel = dw_selezione.getitemstring(1, "cod_fornitore")
ls_cod_prodotto_sel = dw_selezione.getitemstring(1, "cod_prodotto")
ls_flag_blocco_sel = dw_selezione.getitemstring(1, "flag_blocco")
ls_flag_evasione_sel = dw_selezione.getitemstring(1, "flag_evasione")
ld_data_da_sel = dw_selezione.getitemdate(1, "data_da")
ld_data_a_sel = dw_selezione.getitemdate(1, "data_a")
ll_anno_sel = dw_selezione.getitemnumber(1, "anno")
ll_num_ordine_da_sel = dw_selezione.getitemnumber(1, "num_ordine_da")
ll_num_ordine_a_sel = dw_selezione.getitemnumber(1, "num_ordine_a")

ll_i = 1

declare cu_testata dynamic cursor for sqlsa;

ls_testata = "select anno_registrazione, num_registrazione, data_registrazione, cod_tipo_listino_prodotto, cod_valuta, cod_pagamento, cod_fornitore, flag_evasione, sconto from tes_ord_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_fornitore_sel) then
	ls_testata = ls_testata + " and cod_fornitore = '" + ls_cod_fornitore_sel + "'"
end if

if not isnull(ls_flag_blocco_sel) and ls_flag_blocco_sel <> "T" then
	ls_testata = ls_testata + " and flag_blocco = '" + ls_flag_blocco_sel + "'"
end if

if not isnull(ls_flag_evasione_sel) and ls_flag_evasione_sel <> "T" then
	ls_testata = ls_testata + " and flag_evasione = '" + ls_flag_evasione_sel + "'"
end if

ls_da = string(ld_data_da_sel, "yyyy/mm/dd")

if not isnull(ld_data_da_sel) then
	ls_testata = ls_testata + " and data_registrazione >= '" + ls_da + "'"
end if

ls_a = string(ld_data_a_sel, "yyyy/mm/dd")

if not isnull(ld_data_a_sel) then
	ls_testata = ls_testata + " and data_registrazione <= '" + string(ls_a) + "'"
end if

if not isnull(ll_anno_sel) then
	ls_testata = ls_testata + " and anno_registrazione = " + string(ll_anno_sel) + " "
end if

if not isnull(ll_num_ordine_da_sel) then
	ls_testata = ls_testata + " and num_registrazione >= " + string(ll_num_ordine_da_sel) + " "
end if

if not isnull(ll_num_ordine_a_sel) then
	ls_testata = ls_testata + " and num_registrazione <= " + string(ll_num_ordine_a_sel) + " "
end if

prepare sqlsa from :ls_testata;

open dynamic cu_testata;

do while 1=1
   fetch cu_testata into :ll_anno_ordine, :ll_num_ordine, :ldt_data_registrazione, :ls_tipo_listino, :ls_valuta, :ls_pagamento, :ls_fornitore, :ls_stato_ordine, :ld_sconto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		declare cu_dettaglio dynamic cursor for sqlsa;
		ls_dettaglio = "select prog_riga_ordine_acq, cod_prodotto, des_prodotto, cod_misura, quan_ordinata, (quan_ordinata - quan_arrivata), prezzo_acquisto, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9,	 sconto_10, val_riga, cod_iva, data_consegna, data_consegna_fornitore, flag_saldo, quan_arrivata from det_ord_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(ll_anno_ordine) + " and num_registrazione = " + string(ll_num_ordine) + " "
		if not isnull(ls_cod_prodotto_sel) then
			ls_dettaglio = ls_dettaglio + " and cod_prodotto = '" + ls_cod_prodotto_sel + "'"
		end if
		prepare sqlsa from :ls_dettaglio;
		open dynamic cu_dettaglio;

		do while 1=1
			fetch cu_dettaglio into :ll_prog_riga, :ls_cod_prodotto, :ls_des_prodotto, :ls_unita_misura, :ld_qta_ordine, :ld_residuo_ordine, :ld_prezzo_acq, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_valore_riga, :ls_iva, :ldt_data_consegna, :ldt_data_consegna_for, :ls_flag_saldo, :ld_quan_arrivata;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then		
				if not isnull(ll_num_ordine) and not isnull(ll_anno_ordine) then
					dw_report.insertrow(0)
					dw_report.setitem(ll_i, "num_ordine", ll_num_ordine)
					dw_report.setitem(ll_i, "anno_ordine", ll_anno_ordine)
					dw_report.setitem(ll_i, "data_registrazione", ldt_data_registrazione)
					dw_report.setitem(ll_i, "tipo_listino", ls_tipo_listino)
					dw_report.setitem(ll_i, "valuta", ls_valuta)
					dw_report.setitem(ll_i, "pagamento", ls_pagamento)
					
					select rag_soc_1
					  into :ls_rag_soc_1
					  from anag_fornitori
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_fornitore = :ls_fornitore;
					
					dw_report.setitem(ll_i, "fornitore", ls_rag_soc_1)
					
					if ls_stato_ordine = "E" then ls_stato_ordine = "Evaso"
					if ls_stato_ordine = "P" then ls_stato_ordine = "Parziale"
					if ls_stato_ordine = "A" then ls_stato_ordine = "Aperto"				
					
					dw_report.setitem(ll_i, "stato_ordine", ls_stato_ordine)
					dw_report.setitem(ll_i, "sconto", ld_sconto)
					
					dw_report.setitem(ll_i, "prog_riga", ll_prog_riga)
					dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
					
					select des_prodotto
					  into :ls_des_prodotto
					  from anag_prodotti
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_prodotto = :ls_cod_prodotto;
					
					dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
					dw_report.setitem(ll_i, "unita_misura", ls_unita_misura)
					dw_report.setitem(ll_i, "qta_ordine", ld_qta_ordine)
					dw_report.setitem(ll_i, "residuo_ordine", ld_residuo_ordine)
					dw_report.setitem(ll_i, "prezzo_acq", ld_prezzo_acq)
					dw_report.setitem(ll_i, "sconto_1", ld_sconto_1)
					dw_report.setitem(ll_i, "sconto_2", ld_sconto_2)
					dw_report.setitem(ll_i, "sconto_3", ld_sconto_3)
					dw_report.setitem(ll_i, "sconto_4", ld_sconto_4)				
					dw_report.setitem(ll_i, "sconto_5", ld_sconto_5)
					dw_report.setitem(ll_i, "sconto_6", ld_sconto_6)
					dw_report.setitem(ll_i, "sconto_7", ld_sconto_7)
					dw_report.setitem(ll_i, "sconto_8", ld_sconto_8)								
					dw_report.setitem(ll_i, "sconto_9", ld_sconto_9)
					dw_report.setitem(ll_i, "sconto_10", ld_sconto_10)	
					dw_report.setitem(ll_i, "valore_riga", ld_valore_riga)	
					dw_report.setitem(ll_i, "iva", ls_iva)
					dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
					dw_report.setitem(ll_i, "data_consegna_for", ldt_data_consegna_for)
	
					if ls_flag_saldo = "N" and (ld_quan_arrivata = 0 or isnull(ld_quan_arrivata)) then ls_stato = "A"
					if ls_flag_saldo = "N" and ld_quan_arrivata > 0 then ls_stato = "P"
					if ls_flag_saldo = "S" then ls_stato = "E"
					
					dw_report.setitem(ll_i, "stato", ls_stato)				
					
					ll_i ++
				end if	
			end if
		loop
		close cu_dettaglio;
	end if	
loop
close cu_testata;	



dw_report.setsort("anno_ordine A, num_ordine A, data_registrazione A")
dw_report.sort()
dw_report.groupcalc()

dw_report.change_dw_current()
end event

