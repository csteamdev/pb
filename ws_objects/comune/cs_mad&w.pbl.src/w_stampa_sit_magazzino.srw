﻿$PBExportHeader$w_stampa_sit_magazzino.srw
$PBExportComments$Finestra gestione situazione di magazzino
forward
global type w_stampa_sit_magazzino from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_stampa_sit_magazzino
end type
type dw_report from uo_cs_xx_dw within w_stampa_sit_magazzino
end type
type cb_report from commandbutton within w_stampa_sit_magazzino
end type
type cb_1 from commandbutton within w_stampa_sit_magazzino
end type
type st_stato_avanzamento from statictext within w_stampa_sit_magazzino
end type
end forward

global type w_stampa_sit_magazzino from w_cs_xx_principale
integer width = 3634
integer height = 2200
string title = "Report Situazione Magazzino"
dw_selezione dw_selezione
dw_report dw_report
cb_report cb_report
cb_1 cb_1
st_stato_avanzamento st_stato_avanzamento
end type
global w_stampa_sit_magazzino w_stampa_sit_magazzino

type variables
datastore ids_situazione_mag
end variables

forward prototypes
public function integer wf_quan_venduta (string fs_cod_prodotto, ref decimal fd_quantita)
end prototypes

public function integer wf_quan_venduta (string fs_cod_prodotto, ref decimal fd_quantita);datetime ldt_data_da
dec{4}   ld_quan_fatturata

select data_chiusura_annuale
into   :ldt_data_da
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;


select SUM(det_fat_ven.quan_fatturata)
into   :fd_quantita
from 	 det_fat_ven,  
		 tes_fat_ven,  
		 tab_tipi_fat_ven  
where ( tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda ) and  
		( tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione ) and 
		( tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione ) and  
		( tab_tipi_fat_ven.cod_azienda = tes_fat_ven.cod_azienda ) and  
		( tab_tipi_fat_ven.cod_tipo_fat_ven = tes_fat_ven.cod_tipo_fat_ven ) and  
		( det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda ) AND 
		( det_fat_ven.cod_prodotto is not null ) AND  
		( det_fat_ven.num_registrazione_bol_ven is null ) AND  
		( tes_fat_ven.flag_blocco = 'N' ) AND  
		( tes_fat_ven.flag_movimenti = 'S' ) AND  
		( tab_tipi_fat_ven.flag_tipo_fat_ven = 'I' ) AND 
		( tes_fat_ven.data_registrazione >= :ldt_data_da ) and
		( det_fat_ven.cod_prodotto = :fs_cod_prodotto )
GROUP BY det_fat_ven.cod_prodotto;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante la lettura della quantità venduta:" + sqlca.sqlerrtext)
	return -1
end if

return 0
end function

on w_stampa_sit_magazzino.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_report=create cb_report
this.cb_1=create cb_1
this.st_stato_avanzamento=create st_stato_avanzamento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.st_stato_avanzamento
end on

on w_stampa_sit_magazzino.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_report)
destroy(this.cb_1)
destroy(this.st_stato_avanzamento)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_cat_merc", &
                 sqlca, &
                 "tab_cat_mer", &
                 "cod_cat_mer", &
                 "des_cat_mer", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event pc_setwindow;call super::pc_setwindow;string ls_des_azienda

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
iuo_dw_main = dw_report

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	
end event

event close;call super::close;//destroy ids_situazione_mag
end event

type dw_selezione from uo_cs_xx_dw within w_stampa_sit_magazzino
integer y = 20
integer width = 2674
integer height = 440
integer taborder = 50
string dataobject = "d_sel_stampa_sit_magazzino"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null

setnull(ls_null)

dw_selezione.setitem(1, "cod_prodotto_da", ls_null)
dw_selezione.setitem(1, "cod_prodotto_a", ls_null)
dw_selezione.setitem(1, "data_riferimento", today() )
dw_selezione.setitem(1, "cod_cat_merc", ls_null)
dw_selezione.setitem(1, "flag_consegna", 'S')
dw_selezione.setitem(1, "flag_mancanti", 'N')

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	if i_colname = "cod_prodotto_da" then
		if len(i_coltext) > 0 then dw_selezione.setitem(1, "cod_prodotto_a", i_coltext)
	end if
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

type dw_report from uo_cs_xx_dw within w_stampa_sit_magazzino
integer x = 23
integer y = 460
integer width = 3543
integer height = 1620
integer taborder = 60
string dataobject = "d_stampa_sit_magazzino"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_report from commandbutton within w_stampa_sit_magazzino
integer x = 3205
integer y = 24
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_flag_semplificato,  &
		 ls_flag_mancanti_sel, ls_data_riferimento_sel, ls_data_inizio, ls_flag_prodotti_sel, ls_des_prodotto_sel, ls_where, ls_error, &
		 ls_chiave[], ls_cod_prodotto
date ldt_data_riferimento_sel
datetime ldt_data_riferimento_sel_2
long ll_i, ll_i_mancanti, ll_ret
uo_situazione_magazzino iuo_situazione_magazzino
dec{4}	ld_quan_venduta, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[]

uo_magazzino luo_magazzino

SetPointer(HourGlass!)

ids_situazione_mag = create datastore
ids_situazione_mag.DataObject = 'd_sit_magazzino_comodo'
ids_situazione_mag.SetTransObject (sqlca)

dw_report.reset() 
 
dw_selezione.accepttext()
ls_cod_prodotto_da_sel = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a_sel = dw_selezione.getitemstring(1, "cod_prodotto_a")
ls_cod_cat_merce_sel = dw_selezione.getitemstring(1, "cod_cat_merc")
ls_flag_consegna_sel = dw_selezione.getitemstring(1, "flag_consegna")
ls_flag_mancanti_sel = dw_selezione.getitemstring(1, "flag_mancanti")
ldt_data_riferimento_sel = dw_selezione.getitemdate(1, "data_riferimento")
ldt_data_riferimento_sel_2 = datetime(date(ldt_data_riferimento_sel), 00:00:00)
ls_flag_prodotti_sel = dw_selezione.getitemstring( 1, "flag_prodotti")
ls_des_prodotto_sel = dw_selezione.getitemstring( 1, "des_prodotto")
ls_flag_semplificato = dw_selezione.getitemstring( 1, "flag_semplificato")

if isnull(ls_flag_semplificato) or ls_flag_semplificato = "" then ls_flag_semplificato = "N"
if isnull(ls_flag_prodotti_sel) or ls_flag_prodotti_sel = "" then ls_flag_prodotti_sel = "N"

if isnull(ls_cod_cat_merce_sel) then
	ls_cod_cat_merce_sel = "%"
end if	

ls_data_inizio = string(today(), s_cs_xx.db_funzioni.formato_data)
ls_data_riferimento_sel = string(ldt_data_riferimento_sel, s_cs_xx.db_funzioni.formato_data)

iuo_situazione_magazzino = create uo_situazione_magazzino

st_stato_avanzamento.text = "Ciclo Ordini Vendita"
if iuo_situazione_magazzino.wf_ordini_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
	return
end if

st_stato_avanzamento.text = "Ciclo Bolle Vendita"
if iuo_situazione_magazzino.wf_bolle_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo bolle di vendita")	
	return
end if

st_stato_avanzamento.text = "Ciclo Fatture Vendita"
if iuo_situazione_magazzino.wf_fatture_vendita(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo fatture di vendita")	
	return
end if

st_stato_avanzamento.text = "Ciclo Ordini Acquisto"
if iuo_situazione_magazzino.wf_ordini_acquisto(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo ordini di acquisto")	
	return
end if

st_stato_avanzamento.text = "Ciclo Commesse"
if iuo_situazione_magazzino.wf_commesse(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ls_flag_consegna_sel, ls_data_inizio, ls_data_riferimento_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
	g_mb.messagebox("Apice","Errore in funzione ciclo commesse")	
	return
end if

if ls_flag_prodotti_sel = "S" then
	st_stato_avanzamento.text = "Ciclo Generale Prodotti"
	if iuo_situazione_magazzino.wf_prodotti_generale(ls_cod_prodotto_da_sel, ls_cod_prodotto_a_sel, ls_cod_cat_merce_sel, ids_situazione_mag, ls_des_prodotto_sel) = -1 then
		g_mb.messagebox("Apice","Errore in funzione ciclo ordini di vendita")	
		return
	end if	
end if

destroy iuo_situazione_magazzino

st_stato_avanzamento.text = "Preparazione Dati"
ll_i_mancanti = 1 

if ls_flag_semplificato = "S" then
	dw_report.dataobject = "d_stampa_sit_magazzino_semp"
end if

for ll_i = 1 to ids_situazione_mag.RowCount()
	if ls_flag_mancanti_sel = "S" then
		if (ids_situazione_mag.getitemdecimal(ll_i, "da_produrre_com")) > 0 or &
			(ids_situazione_mag.getitemdecimal(ll_i, "da_ordinare_com")) > 0 or &
			(ids_situazione_mag.getitemdecimal(ll_i, "c_lavorazione_com")) > 0 then 

			dw_report.insertrow(0)
			dw_report.setitem(ll_i_mancanti, "cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
			dw_report.setitem(ll_i_mancanti, "des_prodotto", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
			dw_report.setitem(ll_i_mancanti, "giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
			dw_report.setitem(ll_i_mancanti, "impegnato", ids_situazione_mag.getitemdecimal(ll_i, "impegnato_com"))
			dw_report.setitem(ll_i_mancanti, "assegnato", ids_situazione_mag.getitemdecimal(ll_i, "assegnato_com"))
			dw_report.setitem(ll_i_mancanti, "spedizione", ids_situazione_mag.getitemdecimal(ll_i, "spedizione_com"))
			dw_report.setitem(ll_i_mancanti, "anticipi", ids_situazione_mag.getitemdecimal(ll_i, "anticipi_com"))
			dw_report.setitem(ll_i_mancanti, "disp_reale", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
			dw_report.setitem(ll_i_mancanti, "ord_a_fornitore", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))
			dw_report.setitem(ll_i_mancanti, "prod_lanciata", ids_situazione_mag.getitemdecimal(ll_i, "prod_lanciata_com"))
			dw_report.setitem(ll_i_mancanti, "disp_teorica", ids_situazione_mag.getitemdecimal(ll_i, "disp_teorica_com"))
			dw_report.setitem(ll_i_mancanti, "scorta_minima", ids_situazione_mag.getitemdecimal(ll_i, "scorta_minima_com"))	
			dw_report.setitem(ll_i_mancanti, "scorta_max", ids_situazione_mag.getitemdecimal(ll_i, "scorta_max_com"))	
			dw_report.setitem(ll_i_mancanti, "lotto_economico_acq", ids_situazione_mag.getitemdecimal(ll_i, "lotto_economico_acq_com"))				
			dw_report.setitem(ll_i_mancanti, "da_produrre", ids_situazione_mag.getitemdecimal(ll_i, "da_produrre_com"))
			dw_report.setitem(ll_i_mancanti, "da_ordinare", ids_situazione_mag.getitemdecimal(ll_i, "da_ordinare_com"))
			dw_report.setitem(ll_i_mancanti, "c_lavorazione", ids_situazione_mag.getitemdecimal(ll_i, "c_lavorazione_com"))
			dw_report.setitem(ll_i_mancanti, "flag_eseguito", "N")
			
			if ls_flag_semplificato = "S" then
				
				for ll_ret = 1 to 14
					ld_quant_val[ll_ret] = 0
				next	
				
				ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
				luo_magazzino = CREATE uo_magazzino
				ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento_sel_2, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
				
				destroy luo_magazzino
				
				if ll_ret <0 then
					g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"), ls_error)
					return -1
				end if
				
				dw_report.setitem(ll_i_mancanti, "quan_venduta", ld_quant_val[10])
				dw_report.setitem(ll_i_mancanti, "quan_acquistata", ld_quant_val[8])
				
			end if			
			
			ll_i_mancanti ++
		end if
	else 			
		dw_report.insertrow(0)		
		dw_report.setitem(ll_i, "cod_prodotto", ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"))
		dw_report.setitem(ll_i, "des_prodotto", ids_situazione_mag.getitemstring(ll_i, "des_prodotto_com"))
		dw_report.setitem(ll_i, "giacenza", ids_situazione_mag.getitemdecimal(ll_i, "giacenza_com"))
		dw_report.setitem(ll_i, "impegnato", ids_situazione_mag.getitemdecimal(ll_i, "impegnato_com"))
		dw_report.setitem(ll_i, "assegnato", ids_situazione_mag.getitemdecimal(ll_i, "assegnato_com"))
		dw_report.setitem(ll_i, "spedizione", ids_situazione_mag.getitemdecimal(ll_i, "spedizione_com"))
		dw_report.setitem(ll_i, "anticipi", ids_situazione_mag.getitemdecimal(ll_i, "anticipi_com"))
		dw_report.setitem(ll_i, "disp_reale", ids_situazione_mag.getitemdecimal(ll_i, "disp_reale_com"))
		dw_report.setitem(ll_i, "ord_a_fornitore", ids_situazione_mag.getitemdecimal(ll_i, "ord_a_fornitore_com"))
		dw_report.setitem(ll_i, "prod_lanciata", ids_situazione_mag.getitemdecimal(ll_i, "prod_lanciata_com"))
		dw_report.setitem(ll_i, "disp_teorica", ids_situazione_mag.getitemdecimal(ll_i, "disp_teorica_com"))
		dw_report.setitem(ll_i, "scorta_minima", ids_situazione_mag.getitemdecimal(ll_i, "scorta_minima_com"))	
		dw_report.setitem(ll_i, "scorta_max", ids_situazione_mag.getitemdecimal(ll_i, "scorta_max_com"))	
		dw_report.setitem(ll_i, "lotto_economico_acq", ids_situazione_mag.getitemdecimal(ll_i, "lotto_economico_acq_com"))				
		dw_report.setitem(ll_i, "da_produrre", ids_situazione_mag.getitemdecimal(ll_i, "da_produrre_com"))
		dw_report.setitem(ll_i, "da_ordinare", ids_situazione_mag.getitemdecimal(ll_i, "da_ordinare_com"))
		dw_report.setitem(ll_i, "c_lavorazione", ids_situazione_mag.getitemdecimal(ll_i, "c_lavorazione_com"))
		dw_report.setitem(ll_i, "flag_eseguito", "N")
		
		if ls_flag_semplificato = "S" then

			for ll_ret = 1 to 14
				ld_quant_val[ll_ret] = 0
			next	
			
			ls_cod_prodotto = ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com")
			
			luo_magazzino = CREATE uo_magazzino
			
			ll_ret = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_riferimento_sel_2, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
			
			if ll_ret <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ids_situazione_mag.getitemstring(ll_i, "cod_prodotto_com"), ls_error)
				return -1
			end if
			
			//ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			//dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
			
			// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
			// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
			// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
			dw_report.setitem(ll_i, "quan_venduta", ld_quant_val[10])
			dw_report.setitem(ll_i, "quan_acquistata", ld_quant_val[8])
		end if			
		
	end if
next	

dw_report.SetSort("cod_prodotto A")
dw_report.Sort( )	

st_stato_avanzamento.text = "Elaborazione Eseguita"

destroy ids_situazione_mag

SetPointer(Arrow!)

end event

type cb_1 from commandbutton within w_stampa_sit_magazzino
integer x = 2811
integer y = 24
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string ls_null

setnull(ls_null)

dw_selezione.setitem(1, "cod_prodotto_da", ls_null)
dw_selezione.setitem(1, "cod_prodotto_a", ls_null)
dw_selezione.setitem(1, "data_riferimento", today() )
dw_selezione.setitem(1, "cod_cat_merc", ls_null)
dw_selezione.setitem(1, "flag_consegna", 'S')
dw_selezione.setitem(1, "flag_mancanti", 'N')

end event

type st_stato_avanzamento from statictext within w_stampa_sit_magazzino
integer x = 1943
integer y = 196
integer width = 690
integer height = 72
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

