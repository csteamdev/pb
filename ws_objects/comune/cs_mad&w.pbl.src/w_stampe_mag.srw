﻿$PBExportHeader$w_stampe_mag.srw
$PBExportComments$Window Stampe movimenti magazzino
forward
global type w_stampe_mag from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_stampe_mag
end type
type dw_stampe_mag_lista from uo_cs_xx_dw within w_stampe_mag
end type
type dw_stampe_mag_det from uo_cs_xx_dw within w_stampe_mag
end type
end forward

global type w_stampe_mag from w_cs_xx_principale
integer width = 2633
integer height = 1800
string title = "Stampe Magazzino"
cb_dettaglio cb_dettaglio
dw_stampe_mag_lista dw_stampe_mag_lista
dw_stampe_mag_det dw_stampe_mag_det
end type
global w_stampe_mag w_stampe_mag

on w_stampe_mag.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.dw_stampe_mag_lista=create dw_stampe_mag_lista
this.dw_stampe_mag_det=create dw_stampe_mag_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.dw_stampe_mag_lista
this.Control[iCurrent+3]=this.dw_stampe_mag_det
end on

on w_stampe_mag.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.dw_stampe_mag_lista)
destroy(this.dw_stampe_mag_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_stampe_mag_det,"cod_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW(dw_stampe_mag_det,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_stampe_mag_det,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_stampe_mag_lista.set_dw_key("cod_azienda")
dw_stampe_mag_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_stampe_mag_det.set_dw_options(sqlca,dw_stampe_mag_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_stampe_mag_lista
end event

type cb_dettaglio from commandbutton within w_stampe_mag
integer x = 2217
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettaglio"
end type

event clicked;window_open_parm(w_stampe_mag_det,-1,dw_stampe_mag_lista)
end event

type dw_stampe_mag_lista from uo_cs_xx_dw within w_stampe_mag
integer x = 23
integer y = 20
integer width = 2560
integer height = 440
integer taborder = 30
string dataobject = "d_stampe_mag_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_dettaglio.enabled = false
	dw_stampe_mag_det.object.b_ricerca_prodotto.enabled = true
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_dettaglio.enabled = false
	dw_stampe_mag_det.object.b_ricerca_prodotto.enabled = true
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_dettaglio.enabled = true
	dw_stampe_mag_det.object.b_ricerca_prodotto.enabled = false
end if
end event

type dw_stampe_mag_det from uo_cs_xx_dw within w_stampe_mag
integer x = 23
integer y = 480
integer width = 2560
integer height = 1100
integer taborder = 10
string dataobject = "d_stampe_mag_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_stampe_mag_det,"cod_prodotto")
end choose
end event

