﻿$PBExportHeader$w_stampe_mag_det.srw
$PBExportComments$Window stampe mag_det
forward
global type w_stampe_mag_det from w_cs_xx_principale
end type
type dw_stampe_mag_det_lista from uo_cs_xx_dw within w_stampe_mag_det
end type
end forward

global type w_stampe_mag_det from w_cs_xx_principale
int Width=2108
int Height=1441
boolean TitleBar=true
string Title="Dettaglio Stampe Magazzino"
dw_stampe_mag_det_lista dw_stampe_mag_det_lista
end type
global w_stampe_mag_det w_stampe_mag_det

on w_stampe_mag_det.create
int iCurrent
call w_cs_xx_principale::create
this.dw_stampe_mag_det_lista=create dw_stampe_mag_det_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_stampe_mag_det_lista
end on

on w_stampe_mag_det.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_stampe_mag_det_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_stampe_mag_det_lista.set_dw_key("cod_azienda")
dw_stampe_mag_det_lista.set_dw_key("cod_tipo_stampa")
dw_stampe_mag_det_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)

iuo_dw_main = dw_stampe_mag_det_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_stampe_mag_det_lista,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

type dw_stampe_mag_det_lista from uo_cs_xx_dw within w_stampe_mag_det
int X=23
int Y=21
int Width=2035
int Height=1301
string DataObject="d_stampe_mag_det_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_stampa_mag

ls_cod_stampa_mag = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_stampa_mag")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_stampa_mag)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
string ls_cod_stampa_mag

ls_cod_stampa_mag = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_stampa_mag")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_stampa_mag", ls_cod_stampa_mag)
   END IF
NEXT
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_prog
	string ls_cod_stampa_mag
	
	ls_cod_stampa_mag = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_stampa_mag")

	select max(prog)
   into   :ll_prog
   from   tab_stampe_mag_det
   where  cod_azienda = :s_cs_xx.cod_azienda and
          cod_stampa_mag = :ls_cod_stampa_mag;

   if isnull(ll_prog) then
      setitem(getrow(), "prog", 1)
   else
      setitem(getrow(), "prog", ll_prog + 1)
   end if

end if

end event

