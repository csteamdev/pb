﻿$PBExportHeader$w_stock.srw
$PBExportComments$Finestra Gestione Stock
forward
global type w_stock from w_cs_xx_principale
end type
type cb_etichetta from commandbutton within w_stock
end type
type dw_stock_det from uo_cs_xx_dw within w_stock
end type
type cb_reset from commandbutton within w_stock
end type
type cb_ricerca from commandbutton within w_stock
end type
type cb_certificato_qualita from commandbutton within w_stock
end type
type dw_stock_lista from uo_cs_xx_dw within w_stock
end type
type dw_folder_search from u_folder within w_stock
end type
type dw_ricerca from u_dw_search within w_stock
end type
end forward

global type w_stock from w_cs_xx_principale
integer width = 3506
integer height = 1952
string title = "Stock"
cb_etichetta cb_etichetta
dw_stock_det dw_stock_det
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_certificato_qualita cb_certificato_qualita
dw_stock_lista dw_stock_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_stock w_stock

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[],ls_test
windowobject l_objects[]

l_criteriacolumn[1] = "rs_cod_prodotto"
l_criteriacolumn[2] = "rs_ubicazione"
l_criteriacolumn[3] = "rs_lotto"
l_criteriacolumn[4] = "rs_cod_cliente"
l_criteriacolumn[5] = "rs_cod_fornitore"
l_criteriacolumn[6] = "rs_cod_deposito"
l_criteriacolumn[7] = "rs_stato_lotto"

l_searchtable[1] = "stock"
l_searchtable[2] = "stock"
l_searchtable[3] = "stock"
l_searchtable[4] = "stock"
l_searchtable[5] = "stock"
l_searchtable[6] = "stock"
l_searchtable[7] = "stock"

l_searchcolumn[1] = "cod_prodotto"
l_searchcolumn[2] = "cod_ubicazione"
l_searchcolumn[3] = "cod_lotto"
l_searchcolumn[4] = "cod_cliente"
l_searchcolumn[5] = "cod_fornitore"
l_searchcolumn[6] = "cod_deposito"
l_searchcolumn[7] = "flag_stato_lotto"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_stock_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							sqlca)


dw_stock_lista.set_dw_key("cod_azienda")

dw_stock_lista.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_noenablenewonopen + &
                              c_nonew + &
                              c_noretrieveonopen + &
                              c_noenablemodifyonopen + &
                              c_nodelete + &                            
                              c_sharedata + &
                              c_scrollparent + &
										c_noretrieveonopen, &
                              c_default)
										
//dw_stock_lista.set_dw_options(sqlca, &
//                              pcca.null_object, &
//                              c_noenablenewonopen + &
//                              c_nonew + &
//                              c_noretrieveonopen + &
//                              c_noenablemodifyonopen + &
//                              c_nomodify + &
//                              c_nodelete + &                            
//                              c_sharedata + &
//                              c_scrollparent + &
//										c_noretrieveonopen, &
//                              c_default)										

dw_stock_det.set_dw_options(sqlca, &
                            dw_stock_lista, &
                            c_noenablenewonopen + &
                            c_nonew + &
                            c_noenablemodifyonopen + &
                            c_nodelete + &                            
                            c_sharedata + &
                            c_scrollparent, &
                            c_default)

//dw_stock_det.set_dw_options(sqlca, &
//                            dw_stock_lista, &
//                            c_noenablenewonopen + &
//                            c_nonew + &
//                            c_noenablemodifyonopen + &
//                            c_nomodify + &
//                            c_nodelete + &                            
//                            c_sharedata + &
//                            c_scrollparent, &
//                            c_default)

iuo_dw_main = dw_stock_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
l_objects[1] = dw_stock_lista
l_objects[2] = cb_etichetta
l_objects[3] = cb_certificato_qualita
dw_folder_search.fu_assigntab(1, "L.", l_Objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto
dw_folder_search.fu_assigntab(2, "R.", l_Objects[])
dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selectTab(2)
dw_stock_lista.change_dw_current()

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_stock_det, &
                 "cod_prodotto", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_stock_det, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_stock_det, &
                 "cod_cliente", &
                 sqlca, &
                 "anag_clienti", &
                 "cod_cliente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_stock_det, &
                 "cod_fornitore", &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


// stefanop: 16/01/2013: Campo in DW ma non carica un cazz se non si mette il codice
f_po_loaddddw_dw(dw_ricerca, &
                 "rs_cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

on w_stock.create
int iCurrent
call super::create
this.cb_etichetta=create cb_etichetta
this.dw_stock_det=create dw_stock_det
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_certificato_qualita=create cb_certificato_qualita
this.dw_stock_lista=create dw_stock_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_etichetta
this.Control[iCurrent+2]=this.dw_stock_det
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_certificato_qualita
this.Control[iCurrent+6]=this.dw_stock_lista
this.Control[iCurrent+7]=this.dw_folder_search
this.Control[iCurrent+8]=this.dw_ricerca
end on

on w_stock.destroy
call super::destroy
destroy(this.cb_etichetta)
destroy(this.dw_stock_det)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_certificato_qualita)
destroy(this.dw_stock_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

type cb_etichetta from commandbutton within w_stock
integer x = 2990
integer y = 120
integer width = 434
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichetta"
end type

event clicked;window_open_parm(w_etichette_stock,-1,dw_stock_lista)
end event

type dw_stock_det from uo_cs_xx_dw within w_stock
integer x = 18
integer y = 816
integer width = 3429
integer height = 1008
integer taborder = 60
string dataobject = "d_stock_det"
borderstyle borderstyle = styleraised!
end type

type cb_reset from commandbutton within w_stock
integer x = 2967
integer y = 700
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;dw_ricerca.fu_buildsearch(TRUE)
dw_stock_lista.change_dw_current()
parent.triggerevent("pc_retrieve")

dw_folder_search.fu_selecttab(1)
end event

type cb_ricerca from commandbutton within w_stock
integer x = 2578
integer y = 700
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_reset()



end event

type cb_certificato_qualita from commandbutton within w_stock
integer x = 2990
integer y = 28
integer width = 434
integer height = 80
integer taborder = 61
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Certif.Qualità"
end type

event clicked;
s_cs_xx.parametri.parametro_s_1 = dw_stock_lista.getitemstring(dw_stock_lista.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_2 = dw_stock_lista.getitemstring(dw_stock_lista.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_3 = dw_stock_lista.getitemstring(dw_stock_lista.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_4 = dw_stock_lista.getitemstring(dw_stock_lista.getrow(),"cod_lotto")
s_cs_xx.parametri.parametro_ul_2 = dw_stock_lista.getitemnumber(dw_stock_lista.getrow(),"prog_stock")
s_cs_xx.parametri.parametro_data_1 =  dw_stock_lista.getitemdatetime(dw_stock_lista.getrow(),"data_stock")


window_open(w_certificato_qualita,-1)

end event

type dw_stock_lista from uo_cs_xx_dw within w_stock
integer x = 219
integer y = 20
integer width = 2752
integer height = 760
integer taborder = 50
string dataobject = "d_stock_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_folder_search from u_folder within w_stock
integer x = 23
integer width = 3424
integer height = 800
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_stock
event ue_key pbm_dwnkey
integer x = 750
integer y = 84
integer width = 2505
integer height = 608
integer taborder = 10
string dataobject = "d_stock_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"rs_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
end choose
end event

