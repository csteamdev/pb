﻿$PBExportHeader$w_stock_non_mov.srw
forward
global type w_stock_non_mov from w_cs_xx_principale
end type
type st_2 from statictext within w_stock_non_mov
end type
type cb_esegui from commandbutton within w_stock_non_mov
end type
type rb_azzera from radiobutton within w_stock_non_mov
end type
type rb_esporta from radiobutton within w_stock_non_mov
end type
type rb_carica from radiobutton within w_stock_non_mov
end type
type st_stato from statictext within w_stock_non_mov
end type
type hpb_1 from hprogressbar within w_stock_non_mov
end type
type st_1 from statictext within w_stock_non_mov
end type
type dw_stock_non_mov from datawindow within w_stock_non_mov
end type
type gb_1 from groupbox within w_stock_non_mov
end type
end forward

global type w_stock_non_mov from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3058
integer height = 552
string title = "Gestione Stock Senza Movimentazione"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
st_2 st_2
cb_esegui cb_esegui
rb_azzera rb_azzera
rb_esporta rb_esporta
rb_carica rb_carica
st_stato st_stato
hpb_1 hpb_1
st_1 st_1
dw_stock_non_mov dw_stock_non_mov
gb_1 gb_1
end type
global w_stock_non_mov w_stock_non_mov

type variables
integer ii_comando = 1
end variables

forward prototypes
public function integer wf_azzera_stock ()
public function integer wf_carica_stock ()
end prototypes

public function integer wf_azzera_stock ();datetime ldt_data_stock

long 		ll_i, ll_prog_stock, ll_count

string	ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto


st_stato.text = "azzeramento giacenza stock..."

for ll_i = 1 to dw_stock_non_mov.rowcount()
	
	dw_stock_non_mov.setrow(ll_i)
	
	dw_stock_non_mov.scrolltorow(ll_i)
	
	ls_cod_prodotto = dw_stock_non_mov.getitemstring(ll_i,"cod_prodotto")
	
	ls_cod_deposito = dw_stock_non_mov.getitemstring(ll_i,"cod_deposito")
	
	ls_cod_ubicazione = dw_stock_non_mov.getitemstring(ll_i,"cod_ubicazione")
	
	ls_cod_lotto = dw_stock_non_mov.getitemstring(ll_i,"cod_lotto")
	
	ldt_data_stock = dw_stock_non_mov.getitemdatetime(ll_i,"data_stock")
	
	ll_prog_stock = dw_stock_non_mov.getitemnumber(ll_i,"prog_stock")
	
	update
		stock
	set
		giacenza_stock = 0
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_deposito = :ls_cod_deposito and
		cod_ubicazione = :ls_cod_ubicazione and
		cod_lotto = :ls_cod_lotto and
		data_stock = :ldt_data_stock and
		prog_stock = :ll_prog_stock;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in azzeramento giacenza stock:~n" + sqlca.sqlerrtext,stopsign!)
		hpb_1.position = 0
		st_stato.text = "pronto"
		rollback;
		return -1
	end if
	
	hpb_1.position = round((ll_i * 1000 / dw_stock_non_mov.rowcount()),0)
	
next

commit;

g_mb.messagebox("APICE","Azzeramento stock completato con successo",information!)

hpb_1.position = 0

dw_stock_non_mov.reset()

rb_esporta.enabled = false

rb_esporta.checked = false

rb_azzera.enabled = false

rb_azzera.checked = false

rb_carica.checked = true

st_stato.text = "pronto"

return 0
end function

public function integer wf_carica_stock ();datetime ldt_data_stock

long 		ll_i, ll_prog_stock, ll_return

string	ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto

datastore lds_mov_stock


st_stato.text = "caricamento elenco stock..."

dw_stock_non_mov.reset()

rb_esporta.enabled = false

rb_esporta.checked = false

rb_azzera.enabled = false

rb_azzera.checked = false

dw_stock_non_mov.setredraw(false)

ll_return = dw_stock_non_mov.retrieve(s_cs_xx.cod_azienda)

dw_stock_non_mov.setredraw(true)

if ll_return < 0 then
	g_mb.messagebox("APICE","Errore in lettura elenco stock!",stopsign!)
	st_stato.text = "pronto"
	return -1
end if

st_stato.text = "verifica movimentazione..."

lds_mov_stock = create datastore

lds_mov_stock.dataobject = "d_ds_mov_stock"

if lds_mov_stock.settransobject(sqlca) < 0 then
	g_mb.messagebox("APICE","Errore in impostazione transazione!",stopsign!)
	st_stato.text = "pronto"
	return -1
end if

for ll_i = 1 to dw_stock_non_mov.rowcount()
	
	dw_stock_non_mov.setrow(ll_i)
	
	dw_stock_non_mov.scrolltorow(ll_i)
	
	ls_cod_prodotto = dw_stock_non_mov.getitemstring(ll_i,"cod_prodotto")
	
	ls_cod_deposito = dw_stock_non_mov.getitemstring(ll_i,"cod_deposito")
	
	ls_cod_ubicazione = dw_stock_non_mov.getitemstring(ll_i,"cod_ubicazione")
	
	ls_cod_lotto = dw_stock_non_mov.getitemstring(ll_i,"cod_lotto")
	
	ldt_data_stock = dw_stock_non_mov.getitemdatetime(ll_i,"data_stock")
	
	ll_prog_stock = dw_stock_non_mov.getitemnumber(ll_i,"prog_stock")
	
	ll_return = lds_mov_stock.retrieve(s_cs_xx.cod_azienda,ls_cod_prodotto,ls_cod_deposito,ls_cod_ubicazione, &
												  ls_cod_lotto,ldt_data_stock,ll_prog_stock)
	
	choose case ll_return
		case is < 0
			g_mb.messagebox("APICE","Errore in controllo movimentazione stock!",stopsign!)
			hpb_1.position = 0
			st_stato.text = "pronto"
			destroy lds_mov_stock
			dw_stock_non_mov.reset()
			return -1
		case is > 0
			dw_stock_non_mov.deleterow(ll_i)
			ll_i --
	end choose
	
	hpb_1.position = round((ll_i * 1000 / dw_stock_non_mov.rowcount()),0)
	
next

if dw_stock_non_mov.rowcount() > 0 then
	rb_esporta.enabled = true
	rb_azzera.enabled = true
end if

destroy lds_mov_stock

hpb_1.position = 0

st_stato.text = "rilevati " + string(dw_stock_non_mov.rowcount()) + " stock senza movimenti"

return 0
end function

on w_stock_non_mov.create
int iCurrent
call super::create
this.st_2=create st_2
this.cb_esegui=create cb_esegui
this.rb_azzera=create rb_azzera
this.rb_esporta=create rb_esporta
this.rb_carica=create rb_carica
this.st_stato=create st_stato
this.hpb_1=create hpb_1
this.st_1=create st_1
this.dw_stock_non_mov=create dw_stock_non_mov
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.rb_azzera
this.Control[iCurrent+4]=this.rb_esporta
this.Control[iCurrent+5]=this.rb_carica
this.Control[iCurrent+6]=this.st_stato
this.Control[iCurrent+7]=this.hpb_1
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.dw_stock_non_mov
this.Control[iCurrent+10]=this.gb_1
end on

on w_stock_non_mov.destroy
call super::destroy
destroy(this.st_2)
destroy(this.cb_esegui)
destroy(this.rb_azzera)
destroy(this.rb_esporta)
destroy(this.rb_carica)
destroy(this.st_stato)
destroy(this.hpb_1)
destroy(this.st_1)
destroy(this.dw_stock_non_mov)
destroy(this.gb_1)
end on

event open;call super::open;dw_stock_non_mov.settransobject(sqlca)
end event

type st_2 from statictext within w_stock_non_mov
integer x = 46
integer y = 352
integer width = 274
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
string text = "Comando:"
boolean focusrectangle = false
end type

type cb_esegui from commandbutton within w_stock_non_mov
integer x = 2629
integer y = 340
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
string text = "Esegui"
end type

event clicked;enabled = false

setpointer(hourglass!)

choose case ii_comando
	case 1
		wf_carica_stock()
	case 2
		dw_stock_non_mov.saveas()
	case 3
		wf_azzera_stock()
end choose

setpointer(arrow!)

enabled = true
end event

type rb_azzera from radiobutton within w_stock_non_mov
integer x = 1897
integer y = 352
integer width = 686
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Azzera Giacenza Stock"
borderstyle borderstyle = stylelowered!
end type

event clicked;ii_comando = 3
end event

type rb_esporta from radiobutton within w_stock_non_mov
integer x = 1234
integer y = 352
integer width = 640
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Esporta Elenco Stock"
borderstyle borderstyle = stylelowered!
end type

event clicked;ii_comando = 2
end event

type rb_carica from radiobutton within w_stock_non_mov
integer x = 343
integer y = 352
integer width = 869
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
string text = "Carica Stock non Movimentati"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;ii_comando = 1
end event

type st_stato from statictext within w_stock_non_mov
integer x = 206
integer y = 20
integer width = 2789
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
string text = "pronto"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_stock_non_mov
integer x = 23
integer y = 80
integer width = 2971
integer height = 60
unsignedinteger maxposition = 1000
integer setstep = 1
end type

type st_1 from statictext within w_stock_non_mov
integer x = 23
integer y = 20
integer width = 183
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
string text = "Stato:"
boolean focusrectangle = false
end type

type dw_stock_non_mov from datawindow within w_stock_non_mov
integer x = 23
integer y = 160
integer width = 2971
integer height = 160
integer taborder = 10
string dataobject = "d_stock_non_mov"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_stock_non_mov
integer x = 23
integer y = 320
integer width = 2583
integer height = 100
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

