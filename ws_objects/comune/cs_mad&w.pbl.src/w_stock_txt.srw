﻿$PBExportHeader$w_stock_txt.srw
forward
global type w_stock_txt from w_cs_xx_principale
end type
type cb_azzera from commandbutton within w_stock_txt
end type
type dw_stock_txt from datawindow within w_stock_txt
end type
end forward

global type w_stock_txt from w_cs_xx_principale
integer width = 2857
integer height = 1552
string title = "Caricamento Stock da file TXT"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
cb_azzera cb_azzera
dw_stock_txt dw_stock_txt
end type
global w_stock_txt w_stock_txt

on w_stock_txt.create
int iCurrent
call super::create
this.cb_azzera=create cb_azzera
this.dw_stock_txt=create dw_stock_txt
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_azzera
this.Control[iCurrent+2]=this.dw_stock_txt
end on

on w_stock_txt.destroy
call super::destroy
destroy(this.cb_azzera)
destroy(this.dw_stock_txt)
end on

event open;call super::open;string ls_file, ls_wc


if getfileopenname("Selezione elenco stock",ls_file,ls_wc,"TXT","File di testo (*.TXT),*.TXT") <> 1 then
	return -1
end if

setpointer(hourglass!)

if dw_stock_txt.importfile(ls_file) <= 0 then
	setpointer(arrow!)
	g_mb.messagebox("STOCK","Errore in caricamento stock da file",stopsign!)
	return -1
	postevent("close")
end if

setpointer(arrow!)
end event

type cb_azzera from commandbutton within w_stock_txt
integer x = 2423
integer y = 1340
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;datetime ldt_data_stock

long 	 	ll_i, ll_prog_stock

string 	ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto


for ll_i = 1 to dw_stock_txt.rowcount()
	
	dw_stock_txt.setrow(ll_i)
	
	dw_stock_txt.scrolltorow(ll_i)
	
	ls_cod_prodotto = dw_stock_txt.getitemstring(ll_i,"cod_prodotto")
	
	ls_cod_deposito = dw_stock_txt.getitemstring(ll_i,"cod_deposito")
	
	ls_cod_ubicazione = dw_stock_txt.getitemstring(ll_i,"cod_ubicazione")
	
	ls_cod_lotto = dw_stock_txt.getitemstring(ll_i,"cod_lotto")
	
	ldt_data_stock = dw_stock_txt.getitemdatetime(ll_i,"data_stock")
	
	ll_prog_stock = dw_stock_txt.getitemnumber(ll_i,"prog_stock")
	
	update
		stock
	set
		giacenza_stock = 0
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto and
		cod_deposito = :ls_cod_deposito and
		cod_ubicazione = :ls_cod_ubicazione and
		cod_lotto = :ls_cod_lotto and
		data_stock = :ldt_data_stock and
		prog_stock = :ll_prog_stock;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("STOCK","Errore in azzeramento giacenza stock: " + sqlca.sqlerrtext,stopsign!)
		rollback;
		return -1
	end if
	
next

g_mb.messagebox("STOCK","Operazione completata!",information!)

commit;
end event

type dw_stock_txt from datawindow within w_stock_txt
integer x = 23
integer y = 20
integer width = 2766
integer height = 1300
integer taborder = 10
string dataobject = "d_stock_txt"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

