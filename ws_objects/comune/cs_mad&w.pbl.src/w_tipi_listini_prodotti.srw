﻿$PBExportHeader$w_tipi_listini_prodotti.srw
$PBExportComments$Finestra Gestione Tipi Listini Prodotti
forward
global type w_tipi_listini_prodotti from w_cs_xx_principale
end type
type dw_tipi_listini_prodotti from uo_cs_xx_dw within w_tipi_listini_prodotti
end type
type cb_lingue from commandbutton within w_tipi_listini_prodotti
end type
end forward

global type w_tipi_listini_prodotti from w_cs_xx_principale
int Width=2483
int Height=1237
boolean TitleBar=true
string Title="Gestione Tipi Listini Prodotti"
dw_tipi_listini_prodotti dw_tipi_listini_prodotti
cb_lingue cb_lingue
end type
global w_tipi_listini_prodotti w_tipi_listini_prodotti

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_listini_prodotti.set_dw_key("cod_azienda")
dw_tipi_listini_prodotti.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_default, &
                                        c_default)
cb_lingue.enabled=false 
end on

on w_tipi_listini_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_listini_prodotti=create dw_tipi_listini_prodotti
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_listini_prodotti
this.Control[iCurrent+2]=cb_lingue
end on

on w_tipi_listini_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_listini_prodotti)
destroy(this.cb_lingue)
end on

type dw_tipi_listini_prodotti from uo_cs_xx_dw within w_tipi_listini_prodotti
int X=23
int Y=21
int Width=2401
int Height=1001
int TabOrder=10
string DataObject="d_tipi_listini_prodotti"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_tipo_listino_prodotto

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_tipi_listini_prod_lingue"
      ls_codice  = "cod_tipo_listino_prodotto"
      ls_cod_tipo_listino_prodotto = this.getitemstring(li_i, "cod_tipo_listino_prodotto", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_tipo_listino_prodotto)
   next
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

type cb_lingue from commandbutton within w_tipi_listini_prodotti
int X=2058
int Y=1041
int Width=375
int Height=81
int TabOrder=20
string Text="&Lingue"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_tipi_listini_prodotti_lingue, -1, dw_tipi_listini_prodotti)

end on

