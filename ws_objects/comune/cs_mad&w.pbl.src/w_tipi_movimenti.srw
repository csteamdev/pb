﻿$PBExportHeader$w_tipi_movimenti.srw
$PBExportComments$Finestra Gestione Tipi Movimenti
forward
global type w_tipi_movimenti from w_cs_xx_principale
end type
type dw_tipi_movimenti_lista from uo_cs_xx_dw within w_tipi_movimenti
end type
type cb_dettagli from commandbutton within w_tipi_movimenti
end type
end forward

global type w_tipi_movimenti from w_cs_xx_principale
int Width=1756
int Height=1245
boolean TitleBar=true
string Title="Tipi Movimenti"
dw_tipi_movimenti_lista dw_tipi_movimenti_lista
cb_dettagli cb_dettagli
end type
global w_tipi_movimenti w_tipi_movimenti

event pc_setwindow;call super::pc_setwindow;dw_tipi_movimenti_lista.set_dw_key("cod_azienda")
dw_tipi_movimenti_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
iuo_dw_main=dw_tipi_movimenti_lista
dw_tipi_movimenti_lista.ib_proteggi_chiavi = false
end event

on w_tipi_movimenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_movimenti_lista=create dw_tipi_movimenti_lista
this.cb_dettagli=create cb_dettagli
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_movimenti_lista
this.Control[iCurrent+2]=cb_dettagli
end on

on w_tipi_movimenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_movimenti_lista)
destroy(this.cb_dettagli)
end on

type dw_tipi_movimenti_lista from uo_cs_xx_dw within w_tipi_movimenti
int X=23
int Y=21
int Width=1669
int Height=1001
int TabOrder=10
string DataObject="d_tipi_movimenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type cb_dettagli from commandbutton within w_tipi_movimenti
int X=1326
int Y=1041
int Width=366
int Height=81
int TabOrder=11
boolean BringToTop=true
string Text="&Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_det_tipi_movimenti_det, -1, dw_tipi_movimenti_lista)
end event

