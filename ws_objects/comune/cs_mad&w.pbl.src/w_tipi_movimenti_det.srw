﻿$PBExportHeader$w_tipi_movimenti_det.srw
$PBExportComments$Finestra Configurazione Tipi Movimenti
forward
global type w_tipi_movimenti_det from w_cs_xx_principale
end type
type dw_tipi_movimenti_det_lista from uo_cs_xx_dw within w_tipi_movimenti_det
end type
type dw_tipi_movimenti_det_det_3 from uo_cs_xx_dw within w_tipi_movimenti_det
end type
type dw_tipi_movimenti_det_det_1 from uo_cs_xx_dw within w_tipi_movimenti_det
end type
type dw_tipi_movimenti_det_det_2 from uo_cs_xx_dw within w_tipi_movimenti_det
end type
type dw_folder from u_folder within w_tipi_movimenti_det
end type
end forward

global type w_tipi_movimenti_det from w_cs_xx_principale
int Width=2487
int Height=1505
boolean TitleBar=true
string Title="Tipi Movimenti Dettaglio"
dw_tipi_movimenti_det_lista dw_tipi_movimenti_det_lista
dw_tipi_movimenti_det_det_3 dw_tipi_movimenti_det_det_3
dw_tipi_movimenti_det_det_1 dw_tipi_movimenti_det_det_1
dw_tipi_movimenti_det_det_2 dw_tipi_movimenti_det_det_2
dw_folder dw_folder
end type
global w_tipi_movimenti_det w_tipi_movimenti_det

type variables

end variables

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


lw_oggetti[1] = dw_tipi_movimenti_det_det_1
dw_folder.fu_assigntab(1, "Riferimenti", lw_oggetti[])
lw_oggetti[1] = dw_tipi_movimenti_det_det_2
dw_folder.fu_assigntab(2, "Entrate", lw_oggetti[])
lw_oggetti[1] = dw_tipi_movimenti_det_det_3
dw_folder.fu_assigntab(3, "Medie", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_tipi_movimenti_det_lista.set_dw_key("cod_azienda")
dw_tipi_movimenti_det_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_tipi_movimenti_det_det_1.set_dw_options(sqlca, &
                                  dw_tipi_movimenti_det_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
dw_tipi_movimenti_det_det_2.set_dw_options(sqlca, &
                                  dw_tipi_movimenti_det_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
dw_tipi_movimenti_det_det_3.set_dw_options(sqlca, &
                                  dw_tipi_movimenti_det_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main=dw_tipi_movimenti_det_lista

end event

on w_tipi_movimenti_det.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_movimenti_det_lista=create dw_tipi_movimenti_det_lista
this.dw_tipi_movimenti_det_det_3=create dw_tipi_movimenti_det_det_3
this.dw_tipi_movimenti_det_det_1=create dw_tipi_movimenti_det_det_1
this.dw_tipi_movimenti_det_det_2=create dw_tipi_movimenti_det_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_movimenti_det_lista
this.Control[iCurrent+2]=dw_tipi_movimenti_det_det_3
this.Control[iCurrent+3]=dw_tipi_movimenti_det_det_1
this.Control[iCurrent+4]=dw_tipi_movimenti_det_det_2
this.Control[iCurrent+5]=dw_folder
end on

on w_tipi_movimenti_det.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_movimenti_det_lista)
destroy(this.dw_tipi_movimenti_det_det_3)
destroy(this.dw_tipi_movimenti_det_det_1)
destroy(this.dw_tipi_movimenti_det_det_2)
destroy(this.dw_folder)
end on

type dw_tipi_movimenti_det_lista from uo_cs_xx_dw within w_tipi_movimenti_det
int X=23
int Y=21
int Width=2401
int Height=501
int TabOrder=40
string DataObject="d_tipi_movimenti_det_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_tipi_movimenti_det_det_3 from uo_cs_xx_dw within w_tipi_movimenti_det
int X=46
int Y=641
int Width=2058
int Height=701
int TabOrder=10
string DataObject="d_tipi_movimenti_det_det_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_tipi_movimenti_det_det_1 from uo_cs_xx_dw within w_tipi_movimenti_det
int X=69
int Y=641
int Width=2263
int Height=681
int TabOrder=50
string DataObject="d_tipi_movimenti_det_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_tipi_movimenti_det_det_2 from uo_cs_xx_dw within w_tipi_movimenti_det
int X=46
int Y=641
int Width=2332
int Height=501
int TabOrder=30
string DataObject="d_tipi_movimenti_det_det_2"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_folder from u_folder within w_tipi_movimenti_det
int X=23
int Y=541
int Width=2401
int Height=841
int TabOrder=20
end type

