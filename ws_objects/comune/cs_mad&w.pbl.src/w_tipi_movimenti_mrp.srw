﻿$PBExportHeader$w_tipi_movimenti_mrp.srw
$PBExportComments$Finestra Gestione Tipi Movimenti
forward
global type w_tipi_movimenti_mrp from w_cs_xx_principale
end type
type dw_tipi_movimenti_mrp from uo_cs_xx_dw within w_tipi_movimenti_mrp
end type
end forward

global type w_tipi_movimenti_mrp from w_cs_xx_principale
integer width = 2921
integer height = 1144
string title = "Tipi Movimenti MRP"
dw_tipi_movimenti_mrp dw_tipi_movimenti_mrp
end type
global w_tipi_movimenti_mrp w_tipi_movimenti_mrp

event pc_setwindow;call super::pc_setwindow;
dw_tipi_movimenti_mrp.set_dw_key("cod_azienda")

dw_tipi_movimenti_mrp.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
iuo_dw_main=dw_tipi_movimenti_mrp

end event

on w_tipi_movimenti_mrp.create
int iCurrent
call super::create
this.dw_tipi_movimenti_mrp=create dw_tipi_movimenti_mrp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_movimenti_mrp
end on

on w_tipi_movimenti_mrp.destroy
call super::destroy
destroy(this.dw_tipi_movimenti_mrp)
end on

type dw_tipi_movimenti_mrp from uo_cs_xx_dw within w_tipi_movimenti_mrp
integer x = 23
integer y = 20
integer width = 2830
integer height = 1000
integer taborder = 10
string dataobject = "d_tipi_movimenti_mrp"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

