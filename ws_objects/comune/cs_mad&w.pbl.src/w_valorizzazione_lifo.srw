﻿$PBExportHeader$w_valorizzazione_lifo.srw
$PBExportComments$Finestra gestione LIFO
forward
global type w_valorizzazione_lifo from w_cs_xx_principale
end type
type st_log from statictext within w_valorizzazione_lifo
end type
type hpb_1 from hprogressbar within w_valorizzazione_lifo
end type
type dw_depositi from uo_dddw_checkbox within w_valorizzazione_lifo
end type
type dw_selezione from uo_cs_xx_dw within w_valorizzazione_lifo
end type
type dw_report from uo_cs_xx_dw within w_valorizzazione_lifo
end type
type cb_calcolo from commandbutton within w_valorizzazione_lifo
end type
type cb_conferma from commandbutton within w_valorizzazione_lifo
end type
type cb_reset from commandbutton within w_valorizzazione_lifo
end type
end forward

global type w_valorizzazione_lifo from w_cs_xx_principale
integer width = 3794
integer height = 2624
string title = "Stampa Valorizzazione Lifo"
st_log st_log
hpb_1 hpb_1
dw_depositi dw_depositi
dw_selezione dw_selezione
dw_report dw_report
cb_calcolo cb_calcolo
cb_conferma cb_conferma
cb_reset cb_reset
end type
global w_valorizzazione_lifo w_valorizzazione_lifo

type variables
datastore		ids_lifo_appoggio

end variables

forward prototypes
public function integer wf_calcolo_lifo_raggruppato (string as_codice_raggruppo, date adt_data_calcolo, ref double add_cmp_raggruppo, ref double add_qta_raggruppo, ref double add_valore_raggruppo, ref double add_strato_raggruppo[], ref long al_anno_raggruppo[], ref string as_errore)
public function integer wf_aggiorna_rimanenza_strato (string as_flag_incremento_decremento, string as_cod_prodotto, long al_anno_lifo_corrente, decimal ad_variazione_strato_anno)
end prototypes

public function integer wf_calcolo_lifo_raggruppato (string as_codice_raggruppo, date adt_data_calcolo, ref double add_cmp_raggruppo, ref double add_qta_raggruppo, ref double add_valore_raggruppo, ref double add_strato_raggruppo[], ref long al_anno_raggruppo[], ref string as_errore);uo_calcolo_lifo			luo_calcola_lifo
string						ls_cod_prodotto, ls_sql, ls_err
double					ldd_cmp, ld_qta, ld_valore_lifo, ldd_strato[], ldd_vuoto[], ld_temp, ld_fat_conv, ld_Numeratore, ld_Denominatore
long						ll_anno[], ll_vuoto[], ll_index, ll_pos
datastore				lds_data

//CMP = SOMMA ( gz x Cm ) / SOMMA ( gz x Fconv )

//solo struttura, quindi metto anno_registrazione<0
ls_sql = 	"select anno_registrazione, quan_movimento "+&
			"from mov_magazzino "+&
			"where anno_registrazione<0"

guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_err)

luo_calcola_lifo = create uo_calcolo_lifo

declare cu_prod cursor for  
	select cod_prodotto, fat_conversione_rag_mag
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and 
				cod_prodotto_raggruppato=:as_codice_raggruppo;

open cu_prod;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore codici raggruppati: " + sqlca.sqlerrtext
	return -1
end if

add_qta_raggruppo = 0
add_valore_raggruppo = 0
ld_Numeratore = 0
ld_Denominatore = 0

do while true
	fetch cu_prod into :ls_cod_prodotto, :ld_fat_conv;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore codici raggruppati: " + sqlca.sqlerrtext
		close cu_prod;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	ll_anno[] = ll_vuoto[]
	ldd_strato[] = ldd_vuoto[]
	ld_valore_lifo = 0
	ld_qta = 0
	ldd_cmp = 0
	luo_calcola_lifo.wf_calcolo_lifo(ls_cod_prodotto, &
											adt_data_calcolo, &
											ldd_cmp, ld_qta, ld_valore_lifo, ldd_strato[], ll_anno[])
	
	add_qta_raggruppo += ld_qta * ld_fat_conv
	ld_Numeratore += ld_qta * ldd_cmp
	ld_Denominatore += ld_qta * ld_fat_conv
	
	if upperbound(ll_anno[]) > 0 then
	
		//metto in un datastore che poi passero in array
		for ll_index=1 to upperbound(ll_anno[])
			ll_pos = lds_data.find("anno_registrazione="+string(ll_anno[ll_index]), 1, lds_data.rowcount())
			ld_temp = ldd_strato[ll_index] * ld_fat_conv
			
			if ll_pos>0 then
				//aggiorno
				lds_data.setitem(ll_pos, "quan_movimento", lds_data.getitemnumber(ll_pos, "quan_movimento") + ld_temp)
			else
				//inserisco l'anno
				ll_pos = lds_data.insertrow(0)
				lds_data.setitem(ll_pos, "anno_registrazione", ll_anno[ll_index])
				lds_data.setitem(ll_pos, "quan_movimento", ld_temp)
			end if
		next
		
	else
		
		if upperbound(ldd_strato[]) > 0 then
			ll_pos = lds_data.insertrow(0)
			lds_data.setitem(ll_pos, "anno_registrazione", 0)
			
			ld_temp = ldd_strato[1] * ld_fat_conv
			lds_data.setitem(ll_pos, "quan_movimento", ld_temp)
		end if
		
	end if
loop

close cu_prod;
destroy luo_calcola_lifo

if ld_Denominatore > 0 then
	add_cmp_raggruppo = ld_Numeratore / ld_Denominatore
else
	add_cmp_raggruppo = 0
end if

add_valore_raggruppo = add_cmp_raggruppo * add_qta_raggruppo


//svuoto in array
for ll_index=1 to lds_data.rowcount()
	if lds_data.getitemnumber(ll_index, "anno_registrazione")>0 then
		ll_pos = upperbound(al_anno_raggruppo[])
		ll_pos += 1
		al_anno_raggruppo[ll_pos] = lds_data.getitemnumber(ll_index, "anno_registrazione")
		add_strato_raggruppo[ll_pos] = lds_data.getitemnumber(ll_index, "quan_movimento")
		
	else
		ll_pos = upperbound(add_strato_raggruppo[])
		ll_pos += 1
		add_strato_raggruppo[ll_pos] = lds_data.getitemnumber(ll_index, "quan_movimento")
	end if
	
next

return 0

end function

public function integer wf_aggiorna_rimanenza_strato (string as_flag_incremento_decremento, string as_cod_prodotto, long al_anno_lifo_corrente, decimal ad_variazione_strato_anno);string ls_sql, ls_error
long	ll_tot, ll_i, ll_anno_lifo
dec{4} ld_rimanenza_strato
datastore lds_data


choose case as_flag_incremento_decremento 
		
	case "I"
		
		update lifo
		set rimanenza_strato = :ad_variazione_strato_anno
		where cod_azienda 	= :s_cs_xx.cod_azienda and
				cod_prodotto 	= :as_cod_prodotto and
				anno_lifo 		= :al_anno_lifo_corrente;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore SQL in aggiornamento rimanenza strato in tabella LIFO.~r~n" + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
		
	case "D"

		ls_sql = "select anno_lifo, rimanenza_strato from lifo where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + as_cod_prodotto + "' and anno_lifo < "+string(al_anno_lifo_corrente)+" order by anno_lifo DESC"
		
		ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)
		if ll_tot<0 then
			st_log.text = "Fine con errori!"
			g_mb.error("(CALCOLO) Errore preparaz. codici da elaborare: " + ls_error)
			return -1
		end if
	
		for ll_i = 1 to ll_tot
			
			ll_anno_lifo = lds_data.getitemnumber(ll_i,1)
			ld_rimanenza_strato = lds_data.getitemdecimal(ll_i,2)
			
			if ld_rimanenza_strato >= ad_variazione_strato_anno then
				ld_rimanenza_strato = ld_rimanenza_strato - ad_variazione_strato_anno
				ad_variazione_strato_anno = 0
			else
				ad_variazione_strato_anno = ad_variazione_strato_anno - ld_rimanenza_strato
				ld_rimanenza_strato = 0
			end if
				
			update lifo
			set rimanenza_strato = :ld_rimanenza_strato
			where cod_azienda 	= :s_cs_xx.cod_azienda and
					cod_prodotto 	= :as_cod_prodotto and
					anno_lifo 		= :ll_anno_lifo;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore SQL in aggiornamento rimanenza strato in tabella LIFO.~r~n" + sqlca.sqlerrtext)
				rollback;
				return -1
			end if
			
			if ad_variazione_strato_anno = 0 then exit		// se ho finito di decrementare dagli anni passati posso uscire.
			
		next
end choose

return 0

end function

on w_valorizzazione_lifo.create
int iCurrent
call super::create
this.st_log=create st_log
this.hpb_1=create hpb_1
this.dw_depositi=create dw_depositi
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.cb_calcolo=create cb_calcolo
this.cb_conferma=create cb_conferma
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.dw_depositi
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_calcolo
this.Control[iCurrent+7]=this.cb_conferma
this.Control[iCurrent+8]=this.cb_reset
end on

on w_valorizzazione_lifo.destroy
call super::destroy
destroy(this.st_log)
destroy(this.hpb_1)
destroy(this.dw_depositi)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.cb_calcolo)
destroy(this.cb_conferma)
destroy(this.cb_reset)
end on

event pc_setwindow;call super::pc_setwindow;string ls_des_azienda

select rag_soc_1
  into :ls_des_azienda
  from aziende
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Descrizione Azienda non trovata")
end if

if sqlca.sqlcode = 0 then
	dw_report.object.st_azienda.text = ls_des_azienda
end if	

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.ib_dw_report = true

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
									 c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
                            
// save_on_close(c_socnosave)
iuo_dw_main = dw_report

ids_lifo_appoggio = create datastore
ids_lifo_appoggio.DataObject = "d_lifo_appoggio"

// stefanop 26/06/2012: aggiunti depositi e flag_lifo
dw_depositi.uof_set_column("cod_deposito, des_deposito", "anag_depositi", "cod_azienda='" + s_cs_xx.cod_azienda + "'", "cod_deposito")
dw_depositi.uof_set_parent_dw(dw_selezione, "cod_depositi")

dw_report.object.datawindow.print.preview = "Yes"

end event

event close;call super::close;destroy ids_lifo_appoggio
end event

event resize;//dw_selezione.move(0,0)
//
//cb_calcolo.move(newwidth - cb_calcolo.width - 20, 20)
//cb_reset.move(cb_calcolo.x, cb_calcolo.y + cb_calcolo.height + 40)
//cb_conferma.move(cb_calcolo.x, cb_reset.y + cb_reset.height + 40)
//
//dw_report.move(20, dw_selezione.y + dw_selezione.height)
//dw_report.resize(newwidth -40, newheight - dw_report.y - 20)
end event

type st_log from statictext within w_valorizzazione_lifo
integer x = 14
integer y = 92
integer width = 3685
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_valorizzazione_lifo
integer x = 14
integer y = 16
integer width = 3685
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_depositi from uo_dddw_checkbox within w_valorizzazione_lifo
boolean visible = false
integer x = 389
integer y = 520
integer width = 2117
integer taborder = 70
end type

type dw_selezione from uo_cs_xx_dw within w_valorizzazione_lifo
integer y = 184
integer width = 2583
integer height = 480
integer taborder = 50
string dataobject = "d_sel_valorizzazioni_lifo"
boolean border = false
end type

event pcd_new;call super::pcd_new;string ls_null

setnull(ls_null)

dw_selezione.setitem(1, "cod_prodotto_da", ls_null)
dw_selezione.setitem(1, "cod_prodotto_a", ls_null)
dw_selezione.setitem(1, "data_calcolo", today() )

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"cod_prodotto_a")
end choose
end event

event clicked;call super::clicked;choose case dwo.name
	case "cod_depositi"
		dw_depositi.show()
		
end choose
end event

type dw_report from uo_cs_xx_dw within w_valorizzazione_lifo
integer x = 23
integer y = 688
integer width = 3685
integer height = 1780
integer taborder = 60
string dataobject = "d_valorizzazioni_lifo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type cb_calcolo from commandbutton within w_valorizzazione_lifo
integer x = 2656
integer y = 228
integer width = 416
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcolo"
end type

event clicked;string						ls_cod_prodotto_let, ls_des_prodotto_let, ls_comodo_prodotto, ls_cod_azienda, ls_array_prodotto[], &
							ls_array_prod_confermanti[], ls_flag_lifo, ls_cod_deposito_in, ls_sql, ls_error, ls_chiave[], ls_cod_prodotto_lifo, &
							ls_cod_prodotto_da, ls_cod_prodotto_a, ls_lifo, ls_data, ls_des_prodotto, ls_errore, ls_vuoto[], ls_flag_fiscale
							
long						ll_anno_lifo_let, ll_found, ll_max_array, ll_year, ll_i_esistenti, ll_max_esistenti, &
							ll_i_confermati, ll_max_confermati, ll_i, ll_riga, ll_anno[], ll_ins, ll_index, ll_tot, ll_new, ll_count_raggruppo
							
double					ld_cmd_let, ld_rimanenza_strato_let, ld_giacenza_finale_let, ld_valore_fine_anno_let, &
							ld_quant_val[], ld_giacenza[], ld_quantita, ld_cmp, ld_valore_lifo, ld_qta_strato_lifo, &
							ld_qta_fine_lifo, ld_anno_lifo, ld_qta_strato_calcolo, ld_cmp_calcolo, ld_strato[], &
							ld_qta_rimasta, ld_somma_strato, ld_vuoto[]

date						ldt_data_calcolo

uo_calcolo_lifo			luo_calcola_lifo

datastore				lds_data


dw_report.Reset( ) 
dw_selezione.accepttext()
hpb_1.position = 0
dw_report.setredraw(false)

ldt_data_calcolo = dw_selezione.getitemdate(1, "data_calcolo")

if isnull(ldt_data_calcolo) or year(date(ldt_data_calcolo))<1980 then
	g_mb.warning("Impostare la data del calcolo!")
	return
end if

dw_report.object.data_titolo.text = string(ldt_data_calcolo)
ls_cod_prodotto_da = dw_selezione.getitemstring(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring(1, "cod_prodotto_a")
ls_flag_lifo = dw_selezione.getitemstring(1, "flag_lifo")
ls_flag_fiscale = dw_selezione.getitemstring(1, "flag_fiscale")
ls_cod_deposito_in = dw_depositi.uof_get_selected_column("cod_deposito")

ll_year = Year (ldt_data_calcolo) 

// stefanop 26/06/2012: aggiunto flag_lifo e cod_deposito
// -------------------------------
ls_sql = "select cod_prodotto from anag_prodotti where cod_azienda='" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_cod_prodotto_da) and ls_cod_prodotto_da <> "" then ls_sql += " and cod_prodotto >= '" + ls_cod_prodotto_da + "' "
if not isnull(ls_cod_prodotto_a) and ls_cod_prodotto_a <> "" then ls_sql += " and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
if not isnull(ls_flag_lifo) and ls_flag_lifo <> "X" then ls_sql += " and flag_lifo = '" + ls_flag_lifo + "' "
if not isnull(ls_flag_fiscale) and ls_flag_fiscale <> "X" then ls_sql += " and flag_articolo_fiscale = '" + ls_flag_fiscale + "' "
if not isnull(ls_cod_deposito_in) and ls_cod_deposito_in <> "" then ls_sql += " and " + ls_cod_deposito_in

//ls_sql += " and flag_blocco='N' "
//ls_sql += " and ((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>'" + string(ldt_data_calcolo, s_cs_xx.db_funzioni.formato_data) + "'))"
ls_sql += " order by cod_prodotto"
// -------------------------------

//#########################################################################
st_log.text = "Preparazione codici da elaborare in corso ..."

ll_i_esistenti = 0
ll_i_confermati = 0
setnull(ls_array_prodotto[1])
setnull(ls_array_prod_confermanti[1])

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_tot<0 then
	st_log.text = "Fine con errori!"
	dw_report.setredraw(true)
	g_mb.error("(CALCOLO) Errore preparaz. codici da elaborare: " + ls_errore)
	return
end if

hpb_1.maxposition = ll_tot

for ll_index=1 to ll_tot
	Yield()
	ls_comodo_prodotto = lds_data.getitemstring(ll_index, 1)
	st_log.text = "Controllo presenza codice "+ls_comodo_prodotto+" codici in LIFO anno "+string(ll_year)+" in corso ..."

	hpb_1.stepit()

	select cod_azienda 
	into :ls_cod_azienda
	from lifo
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_comodo_prodotto and
				anno_lifo = :ll_year;
	
	if sqlca.sqlcode = 100 then
		//il codice non è attualmente presente in tabella LIFO
		 ll_i_esistenti = ll_i_esistenti + 1
		 ls_array_prodotto[ll_i_esistenti] = ls_comodo_prodotto
		 
	elseif sqlca.sqlcode = 0 then
		//codice già presente in tabella LIFO
		ll_i_confermati = ll_i_confermati + 1
		ls_array_prod_confermanti[ll_i_confermati] = ls_comodo_prodotto
		
	end if
next

destroy lds_data

//#########################################################################
//se l'array ls_array_prodotto[] è vuoto vuol dire che tutti i codici selezionati sono presenti già in tabella LIFO (array ls_array_prod_confermanti[])
//e quindi non ci sono nuovi codici da elaborare ed inserire in LIFO (che altrimenti sarebbero nell'array ls_array_prodotto[])
//#########################################################################

if isnull(ls_array_prodotto[1]) then
//if isnull(ls_array_prod_confermanti[1]) then
	st_log.text = "Pronto!"
	dw_report.setredraw(true)
	g_mb.warning("Prodotti già esistenti in tabella LIFO oppure nessun prodotto da confermare in base al filtro impostato!")
	return
else
	//elaborazione codici
	hpb_1.position = 0
	st_log.text = "Preparazione dati già presenti in tabella LIFO in corso ..."
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//resetto il datastore appoggio di istanza
	ids_lifo_appoggio.Reset()
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//ciclo tutti i codici GIA' presenti in tabella LIFO e li carico nel datastore appoggio di istanza
	ls_sql =  "select cod_prodotto, anno_lifo, des_prodotto, costo_medio_ponderato, rimanenza_strato, giacenza_finale, valore_fine_anno "+&
				"from lifo "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda +"' "+&
						" and anno_lifo="+string(ll_year) + " "+&
				"order by cod_prodotto, anno_lifo "
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	if ll_tot<0 then
		st_log.text = "Fine con errori!"
		dw_report.setredraw(true)
		g_mb.error("(CALCOLO) Errore preparaz. dati: " + ls_errore)
		return
	end if
	
	hpb_1.maxposition = ll_tot
	
	for ll_index=1 to ll_tot
		Yield()
		ls_cod_prodotto_let			= lds_data.getitemstring(ll_index, 1)
		
		st_log.text = "Inserimento da tabella LIFO in ds di appoggio codice "+ls_cod_prodotto_let+" ..."
		hpb_1.stepit()
		
		ll_anno_lifo_let					= lds_data.getitemnumber(ll_index, 2)
		ls_des_prodotto_let			= lds_data.getitemstring(ll_index, 3)
		ld_cmd_let						= lds_data.getitemdecimal(ll_index, 4)
		ld_rimanenza_strato_let		= lds_data.getitemdecimal(ll_index, 5)
		ld_giacenza_finale_let		= lds_data.getitemdecimal(ll_index, 6)
		ld_valore_fine_anno_let		= lds_data.getitemdecimal(ll_index, 7)

		ll_new = ids_lifo_appoggio.insertrow(0)
		ids_lifo_appoggio.setitem(ll_new, "anno_registrazione", ll_anno_lifo_let)
		ids_lifo_appoggio.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_let)
		ids_lifo_appoggio.setitem(ll_new, "descrizione", ls_des_prodotto_let)
		ids_lifo_appoggio.setitem(ll_new, "costo_medio_ponderato", ld_cmd_let)
		ids_lifo_appoggio.setitem(ll_new, "rimanenza_strato", ld_rimanenza_strato_let)
		ids_lifo_appoggio.setitem(ll_new, "qta_finale", ld_giacenza_finale_let)
		ids_lifo_appoggio.setitem(ll_new, "valore_finale", ld_valore_fine_anno_let)	
	next
	
	destroy lds_data
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	//ciclo tutti i codici NON ANCORA presenti in LIFO e li carico nella datawindow del report e nel datastore appoggio di istanza
	hpb_1.position = 0
	st_log.text = "Preparazione dati NON ANCORA presenti in tabella LIFO in corso ..."
	
	ll_max_esistenti = upperbound(ls_array_prodotto)
	hpb_1.maxposition = ll_max_esistenti
	
	luo_calcola_lifo = create uo_calcolo_lifo
	
	for ll_index = 1 to ll_max_esistenti
		Yield()
		st_log.text = "Elaborazione ed inserimento da tabella temporanea su report codice "+ls_array_prodotto[ll_index]+" ..."
		hpb_1.stepit()
		
		select des_prodotto
		into :ls_des_prodotto
		from anag_prodotti 
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_array_prodotto[ll_index]; 
		
		if sqlca.sqlcode < 0 then
			destroy lds_data
			destroy luo_calcola_lifo
			st_log.text = "Fine con errori!"
			dw_report.setredraw(true)
			g_mb.error("(CALCOLO) Errore elaborazione codici non presenti in LIFO: " + sqlca.sqlerrtext)
			return
		end if
		
		//se è un prodotto che raggruppa elabora dai codici che raggruppa
		setnull(ll_count_raggruppo)
		
		select count(*)
		into :ll_count_raggruppo
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto_raggruppato=:ls_array_prodotto[ll_index];
					
		ld_strato[] = ld_vuoto[] 
		
		if ll_count_raggruppo>0 then
			//è un codice di ragguppo
			if wf_calcolo_lifo_raggruppato(ls_array_prodotto[ll_index], ldt_data_calcolo, ld_cmp, ld_quantita, ld_valore_lifo, ld_strato[], ll_anno[], ls_error) < 0 then
				g_mb.error("(CALCOLO 2) Errore calcolo lifo codice "+ls_array_prodotto[ll_index])
			end if
		else
			//non è un codice di raggruppo (quindi caso normale, ovvero grezzo o colorato)
			if luo_calcola_lifo.wf_calcolo_lifo(ls_array_prodotto[ll_index], ldt_data_calcolo, ld_cmp, ld_quantita, ld_valore_lifo, ld_strato[], ll_anno[]) = -1 then
				g_mb.error("(CALCOLO 1) Errore calcolo lifo codice "+ls_array_prodotto[ll_index])
			end if
		end if
//		if luo_calcola_lifo.wf_calcolo_lifo(ls_array_prodotto[ll_index], ldt_data_calcolo, ld_cmp, ld_quantita, ld_valore_lifo, ld_strato[], ll_anno[]) = -1 then
//			g_mb.error("(CALCOLO) Errore calcolo lifo codice "+ls_array_prodotto[ll_index])
//		end if
		
		
		
		//inserisco nel report --------------------------
		ll_new = dw_report.insertrow(0)
		dw_report.setitem(ll_new, "cod_prodotto", ls_array_prodotto[ll_index])
		dw_report.setitem(ll_new, "des_prodotto", ls_des_prodotto)	
		dw_report.setitem(ll_new, "costo_medio_ponderato", ld_cmp)		
		dw_report.setitem(ll_new, "quantita", ld_quantita)
		dw_report.setitem(ll_new, "valore_tot_lifo", ld_valore_lifo)	
		dw_report.setitem(ll_new, "strato", ld_strato[1])			
		if ld_quantita <> 0 then
			dw_report.setitem(ll_new, "valore_medio", (ld_valore_lifo / ld_quantita))	
		else
			dw_report.setitem(ll_new, "valore_medio", ld_valore_lifo)				
		end if
		dw_report.setitem(ll_new, "flag_confermato", 'N')
		
		st_log.text = "Inserimento da tabella temporanea su datastore appoggio codice "+ls_array_prodotto[ll_index]+" ..."
		//inserisco nel datastore --------------------------
		ll_new = ids_lifo_appoggio.insertrow(0)
		ids_lifo_appoggio.setitem(ll_new, "anno_registrazione", ll_year)
		ids_lifo_appoggio.setitem(ll_new, "cod_prodotto", ls_array_prodotto[ll_index])
		ids_lifo_appoggio.setitem(ll_new, "descrizione", ls_des_prodotto)
		ids_lifo_appoggio.setitem(ll_new, "costo_medio_ponderato", ld_cmp)
		ids_lifo_appoggio.setitem(ll_new, "rimanenza_strato", ld_strato[1])
		ids_lifo_appoggio.setitem(ll_new, "qta_finale", ld_quantita)
		ids_lifo_appoggio.setitem(ll_new, "valore_finale", ld_valore_lifo)
		
		//scrivo la rimanenza_strato del codice nel datastore di appoggio, prendendolo dall'utlimo strato
		ll_max_array = upperbound(ll_anno)
		if ll_max_array > 0 then
			for ll_i = 1 to ll_max_array
				ll_found = ids_lifo_appoggio.Find("cod_prodotto = '" + ls_array_prodotto[ll_index] + "' and anno_registrazione = " + string(ll_anno[ll_i]) , 1, ids_lifo_appoggio.RowCount( ))	

				if ll_found <> 0 then
					ids_lifo_appoggio.setitem(ll_found, "rimanenza_strato", ld_strato[ll_i])
				end if	
			next
		end if	
		
	next
	
	destroy luo_calcola_lifo
end if

//ciclo tutti i codici GIA' presenti in LIFO e li carico nella datawindow del report per l'anno relativo a quello di calcolo (nel datastore appoggio di istanza sono stati già inseriti, pure per altri strati)
hpb_1.position = 0
st_log.text = "Pronto!"

if not isnull(ls_array_prod_confermanti[1]) then
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
	ll_max_confermati = upperbound(ls_array_prod_confermanti[])
	hpb_1.maxposition = ll_max_confermati
	
	for ll_index=1 to ll_max_confermati
		st_log.text = "Elaborazione ed inserimento da tabella temporanea su report codice "+ls_array_prod_confermanti[ll_index]+" ..."
		hpb_1.stepit()
		
		select 	des_prodotto,
					costo_medio_ponderato,
					rimanenza_strato,
					giacenza_finale,
					valore_fine_anno
	 	into	:ls_des_prodotto_let,
				:ld_cmd_let,
				:ld_rimanenza_strato_let,
				:ld_giacenza_finale_let,
				:ld_valore_fine_anno_let
		from lifo
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_array_prod_confermanti[ll_index] and
					anno_lifo = :ll_year;
	
		if sqlca.sqlcode < 0 then
			st_log.text = "Fine con errori!"
			dw_report.setredraw(true)
			g_mb.error("(CALCOLO) Errore elaborazione codici presenti in LIFO: " + sqlca.sqlerrtext)
			return
		end if
		
		//inserisco nel report --------------------------
		ll_new = dw_report.insertrow(0)
		dw_report.setitem(ll_new, "cod_prodotto", ls_array_prod_confermanti[ll_index])
		dw_report.setitem(ll_new, "des_prodotto", ls_des_prodotto_let)	
		dw_report.setitem(ll_new, "costo_medio_ponderato", ld_cmd_let)		
		dw_report.setitem(ll_new, "quantita", ld_giacenza_finale_let)
		dw_report.setitem(ll_new, "valore_tot_lifo", ld_valore_fine_anno_let)	
		if ld_giacenza_finale_let <> 0 then
			dw_report.setitem(ll_new, "valore_medio", (ld_valore_fine_anno_let / ld_giacenza_finale_let))	
		else
			dw_report.setitem(ll_new, "valore_medio", ld_valore_fine_anno_let)
		end if
		dw_report.setitem(ll_new, "flag_confermato", 'S')
		
	next
end if

dw_report.setsort("cod_prodotto asc")
dw_report.sort()
dw_report.setredraw(true)

hpb_1.position = 0
st_log.text = "Procedura di Calcolo terminata!"




end event

type cb_conferma from commandbutton within w_valorizzazione_lifo
integer x = 2656
integer y = 472
integer width = 416
integer height = 84
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Con&ferma"
end type

event clicked;long					ll_i, ll_anno_lifo, ll_tot

string					ls_cod_prodotto, ls_comodo, ls_des_prodotto, ls_errore

dec{4}				ld_cmd, ld_rimanenza_strato, ld_qta_finale, ld_valore_finale, ld_giacenza_finale_anno_prec

hpb_1.position = 0

if not g_mb.confirm("Sei sicuro di voler aggionare la tabella LIFO?") then return

ll_tot = ids_lifo_appoggio.rowcount()
hpb_1.maxposition = ll_tot

for ll_i = 1 to ll_tot
	Yield()
	ls_cod_prodotto = ids_lifo_appoggio.getitemstring(ll_i, "cod_prodotto")
	st_log.text ="Elaborazione codice " + ls_cod_prodotto + " in corso ..."
	hpb_1.stepit()
	
	ll_anno_lifo = ids_lifo_appoggio.getitemnumber(ll_i, "anno_registrazione")
	ls_des_prodotto = ids_lifo_appoggio.getitemstring(ll_i, "descrizione")
	ld_cmd =	ids_lifo_appoggio.getitemdecimal(ll_i, "costo_medio_ponderato")
	ld_rimanenza_strato = ids_lifo_appoggio.getitemdecimal(ll_i, "rimanenza_strato")
	ld_qta_finale = ids_lifo_appoggio.getitemdecimal(ll_i, "qta_finale")
	ld_valore_finale = ids_lifo_appoggio.getitemdecimal(ll_i, "valore_finale")		
	
	select cod_prodotto
	into :ls_comodo
	from lifo
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				anno_lifo = :ll_anno_lifo;

	if sqlca.sqlcode < 0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		st_log.text ="Fine con errori!"
		g_mb.error("(CONFERMA) Errore in lettura tabella LIFO: "+ls_errore)
		return
		
	elseif sqlca.sqlcode = 0 then
		//dato già presente: faccio update
		
		update lifo
		set		anno_lifo = :ll_anno_lifo,
				cod_prodotto = :ls_cod_prodotto,
				des_prodotto = :ls_des_prodotto,
				costo_medio_ponderato = :ld_cmd,
				rimanenza_strato = 0,
				giacenza_finale = :ld_qta_finale,
				valore_fine_anno = :ld_valore_finale
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto and
					anno_lifo = :ll_anno_lifo;
		
		if sqlca.sqlcode < 0 then
			ls_errore = sqlca.sqlerrtext
			rollback;
			st_log.text ="Fine con errori!"
			g_mb.error("(CONFERMA) Errore in aggiornamento tabella LIFO (cod.prodotto/anno lifo "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+") : " + ls_errore)
			return
		end if
		
	elseif sqlca.sqlcode = 100 then
		//dato non presente in LIFO: faccio insert
	
		insert into lifo (cod_azienda, anno_lifo, cod_prodotto, des_prodotto, costo_medio_ponderato, rimanenza_strato, giacenza_finale, valore_fine_anno)
	     values(:s_cs_xx.cod_azienda, :ll_anno_lifo, :ls_cod_prodotto, :ls_des_prodotto, :ld_cmd, 0, :ld_qta_finale, :ld_valore_finale) ;

		if sqlca.sqlcode < 0 then
			ls_errore = sqlca.sqlerrtext
			rollback;
			st_log.text ="Fine con errori!"
			g_mb.error("(CONFERMA) Errore in inserimento tabella LIFO (cod.prodotto/anno lifo "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+") : " + ls_errore)
			return
		end if
	end if	
	
	// sistemo la q.ta strato (EnMe 09-04-2015)
	select 	giacenza_finale
	into		:ld_giacenza_finale_anno_prec
	from 		lifo
	where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				anno_lifo = (:ll_anno_lifo -1) ;
	
	if sqlca.sqlcode = 0 then
		if ld_qta_finale < ld_giacenza_finale_anno_prec then // caso decremento
			wf_aggiorna_rimanenza_strato( "D", ls_cod_prodotto, ll_anno_lifo, abs(ld_qta_finale - ld_giacenza_finale_anno_prec))
		else
			wf_aggiorna_rimanenza_strato( "I", ls_cod_prodotto, ll_anno_lifo, abs(ld_qta_finale - ld_giacenza_finale_anno_prec))
		end if
	end if
	
	// aggiorno il valore fine anno (EnMe 09-04-2015)
	
	select		sum( isnull(costo_medio_ponderato,0) * isnull(rimanenza_strato,0) )
	into		:ld_valore_finale
	from		lifo
	where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		st_log.text ="Fine con errori!"
		g_mb.error("(CONFERMA) Errore in lettura da tabella LIFO (cod.prodotto/anno lifo "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+") : " + ls_errore)
		return
	end if
	
	ld_valore_finale = round(ld_valore_finale,4)
	
	update 	lifo
	set			valore_fine_anno = :ld_valore_finale
	where		cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				anno_lifo = :ll_anno_lifo;
	if sqlca.sqlcode < 0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		st_log.text ="Fine con errori!"
		g_mb.error("(CONFERMA) Errore in aggiornamento valore fine anno su tabella LIFO (cod.prodotto/anno lifo "+ls_cod_prodotto+"/"+string(ll_anno_lifo)+") : " + ls_errore)
		return
	end if
	
	
	//se arrivi fin qui fai commit su singolo prodotto LIFO
	commit;

next	

st_log.text ="Conferma avvenuta con successo (Aggiornate/Inserite "+string(ll_tot)+" righe in tabella LIFO) !"
g_mb.success("(CONFERMA) Operazione avvenuta con successo: (Aggiornate/Inserite "+string(ll_tot)+" righe in tabella LIFO) !")




end event

type cb_reset from commandbutton within w_valorizzazione_lifo
integer x = 2656
integer y = 320
integer width = 416
integer height = 84
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reset Filtro"
end type

event clicked;string ls_null

setnull(ls_null)

dw_selezione.setitem(1, "cod_prodotto_da", ls_null)
dw_selezione.setitem(1, "cod_prodotto_a", ls_null)
dw_selezione.setitem(1, "data_calcolo", today() )

dw_selezione.setitem(1, "flag_lifo", "X")
dw_selezione.setitem(1, "flag_fiscale", "X")

end event

