﻿$PBExportHeader$cb_gps_maps.sru
forward
global type cb_gps_maps from commandbutton
end type
end forward

global type cb_gps_maps from commandbutton
integer width = 366
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = thaicharset!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Localizza"
end type
global cb_gps_maps cb_gps_maps

type variables

end variables

forward prototypes
public subroutine uf_rimpiazza (ref string as_mystring)
end prototypes

public subroutine uf_rimpiazza (ref string as_mystring);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(as_mystring) and as_mystring<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(as_mystring, ",", start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 as_mystring = Replace(as_mystring, start_pos, Len(","), ".")
		 // Find the next occurrence of old_str.
		 start_pos = Pos(as_mystring, ",", start_pos+Len("."))
	LOOP	
else
	as_mystring="0"
end if
end subroutine

on cb_gps_maps.create
end on

on cb_gps_maps.destroy
end on

event clicked;string ls_url, ls_lat, ls_long, ls_run
inet iinet_base
decimal ld_lat, ld_long

ls_url = "http://maps.google.it/maps?f=q&source=s_q&hl=it&q="

ld_lat = s_cs_xx.parametri.parametro_d_1
ld_long = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

if isnull(ld_lat) then ld_lat = 0
if isnull(ld_long) then ld_long = 0

ls_lat = string(ld_lat)
uf_rimpiazza(ls_lat)
ls_long = string(ld_long)
uf_rimpiazza(ls_long)

ls_url += ls_lat + "," + ls_long

/*
//1° metodo run internet explorer
ls_run = "C:\Programmi\Internet Explorer\iexplore.exe "+ ls_url
run(ls_run)
//---------------------------------------------------
*/


//2° metodo Get Context Service "Internet"
GetContextService("Internet", iinet_base)
iinet_base.HyperlinkToUrl(ls_url)
//---------------------------------------------------

/*
//3° metodo run firefox
ls_run = "C:\Programmi\Mozilla Firefox\firefox.exe "+ ls_url
run(ls_run)
//---------------------------------------------------
*/

/*
//4° metodo apri window con oggetto webbrowser

////N.B. va creata la w_gps_web_browser con un oggetto 
////fare insert->control-> OLE
////poi clic sul tab Insert Control e selezionare Microsoft Web Browser
//
////codice: 
////ls_url = "http://maps.google.it/maps?f=q&source=s_q&hl=it&q="+ls_lat+","+ls_long
////ole_webcontrol.object.Navigate( sle_url.text )
////ole_webcontrol.object.Navigate( ls_url )

//s_cs_xx.parametri.parametro_s_1 = ls_url
//open(w_gps_webbrowser)
//---------------------------------------------------------------
*/
end event

