﻿$PBExportHeader$uo_cb_chiavi_doc_ricerca.sru
$PBExportComments$bottone ricerca chiavi
forward
global type uo_cb_chiavi_doc_ricerca from commandbutton
end type
end forward

global type uo_cb_chiavi_doc_ricerca from commandbutton
int Width=74
int Height=81
int TabOrder=1
string Text="..."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global uo_cb_chiavi_doc_ricerca uo_cb_chiavi_doc_ricerca

event clicked;if not isvalid(w_ricerca_chiavi_doc) then
   window_open(w_ricerca_chiavi_doc, 0)
end if

w_ricerca_chiavi_doc.show()
end event

