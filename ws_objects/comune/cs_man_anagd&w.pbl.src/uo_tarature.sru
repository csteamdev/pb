﻿$PBExportHeader$uo_tarature.sru
forward
global type uo_tarature from nonvisualobject
end type
end forward

global type uo_tarature from nonvisualobject
end type
global uo_tarature uo_tarature

type variables

end variables

forward prototypes
public subroutine wf_calcola_taratura (ref datawindow adw_data, integer ai_row)
end prototypes

public subroutine wf_calcola_taratura (ref datawindow adw_data, integer ai_row);/**
 * Esegue tutti i calcoli relativi alla taratura
 **/
 
string ls_col_abilitata, ls_col_valore, ls_cod_primario, ls_esito
int li_columns, li_i, li_num_tarature
dec{4} ld_totale_tarature, ld_valore_taratura,  ld_v_min, ld_v_max, ld_incertezza, ld_fattore, ld_grado_precisione, ld_tolleranza, ld_valore_medio

if ai_row < 1 then return

adw_data.accepttext()

// conto le colonne sempre da una DW perchè il dataobject è SEMPRE uguale
li_columns = integer(adw_data.Describe("DataWindow.Column.Count"))

li_num_tarature = 0
ld_v_min = 9999999999999
ld_v_max = 0

for li_i = 1 to li_columns
	
	ls_col_abilitata = adw_data.Describe("#" + string(li_i) + ".Name")
	
	// Controllo che la colonna sia 'taratura_X_abilitata' e che abbia valore 'S'
	if right(ls_col_abilitata, 9) <> "abilitata" or adw_data.getitemstring(ai_row, ls_col_abilitata) = 'N' then continue

	ls_col_valore = left(ls_col_abilitata, len(ls_col_abilitata) - 9) + "valore" // colonna "taratura_X_valore"
	ld_valore_taratura = adw_data.getitemnumber(ai_row, ls_col_valore)
	
	li_num_tarature++
	ld_totale_tarature += ld_valore_taratura
	
	// Controllo minimo / massimo
	if ld_valore_taratura < ld_v_min then ld_v_min = ld_valore_taratura
	if ld_valore_taratura > ld_v_max then ld_v_max = ld_valore_taratura
	
next

if li_num_tarature > 0 then
	// Valore medio
	ld_valore_medio = ld_totale_tarature / li_num_tarature
	ld_fattore = 1
	
	// Incertezza
	select fattore
	into :ld_fattore
	from tab_fattore_tarature
	where
		 cod_azienda = :s_cs_xx.cod_azienda and
		 num_tarature = :li_num_tarature;
		 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il recupero del fattore di divisione.~r~n" + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 100 and li_num_tarature < 2 then
		return
	elseif sqlca.sqlcode = 100 then
		g_mb.show("Attenzione: non è stato trovato nessun fattore di divisione per il numero di tarature uguale a " + string(li_num_tarature))
		return
	end if
	
	ld_incertezza =  ((ld_v_max - ld_v_min) / ld_fattore) * 4
	
	// Esito
	ls_esito = 'N'
	ls_cod_primario = adw_data.getitemstring(ai_row, "cod_attrezzatura_primario")
	ld_tolleranza = adw_data.getitemnumber(1, "tolleranza_stabilita")
	
	select grado_precisione
	into :ld_grado_precisione
	from anag_attrezzature
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :ls_cod_primario;
		
	// tm < Vm < tM
	if (ld_grado_precisione - ld_tolleranza) <= ld_valore_medio and ld_valore_medio <= (ld_grado_precisione + ld_tolleranza) then
		
		// I <= 1/3 x t x 2
		if ld_incertezza <= (1 / 3) * ld_tolleranza * 2 then
			
			ls_esito = 'P'
			
		end if
		
	end if
else
	// nessuna misurazione fatta.. metto a zero tutti i valori
	ld_valore_medio = 0
	ld_incertezza = 0
	ls_esito = "N"
end if

adw_data.setitem(ai_row, "valore_medio", ld_valore_medio)
adw_data.setitem(ai_row, "incertezza", ld_incertezza)
adw_data.setitem(ai_row, "esito", ls_esito)

end subroutine

on uo_tarature.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_tarature.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

