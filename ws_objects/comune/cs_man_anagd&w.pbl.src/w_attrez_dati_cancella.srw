﻿$PBExportHeader$w_attrez_dati_cancella.srw
forward
global type w_attrez_dati_cancella from window
end type
type st_log from statictext within w_attrez_dati_cancella
end type
type hpb_1 from hprogressbar within w_attrez_dati_cancella
end type
type dw_selezione from datawindow within w_attrez_dati_cancella
end type
type cb_annulla from commandbutton within w_attrez_dati_cancella
end type
type cb_conferma from commandbutton within w_attrez_dati_cancella
end type
type st_attrezzatura from statictext within w_attrez_dati_cancella
end type
end forward

global type w_attrez_dati_cancella from window
integer width = 2574
integer height = 888
boolean titlebar = true
string title = "Selezione per eliminazione dati dati da attrezzatura"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_log st_log
hpb_1 hpb_1
dw_selezione dw_selezione
cb_annulla cb_annulla
cb_conferma cb_conferma
st_attrezzatura st_attrezzatura
end type
global w_attrez_dati_cancella w_attrez_dati_cancella

type variables
string is_cod_attrezzatura, is_des_attrezzatura

boolean ib_global_service
end variables

forward prototypes
public function integer wf_elimina_tipi_man (ref long fl_tipi_man_eliminate, ref string fs_errore)
public function boolean wf_controlla_referenze_bloccanti (string fs_tipo, string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, long fl_anno, long fl_numero, ref string fs_errore)
public function integer wf_elimina_man_ordinarie (string fs_elimina_piani, ref long fl_piani_eliminati, ref long fl_man_eliminate, ref string fs_errore)
public function integer wf_elimina_manutenzione (integer ai_anno, long al_numero, ref string as_errore)
public function integer wf_elimina_richieste (ref long fl_richieste_eliminate, ref string fs_errore)
public function integer wf_conferma (string fs_elimina_tipi_man, string fs_elimina_piani, string fs_elimina_richieste, ref long fl_tipi_man_eliminati, ref long fl_piani_eliminati, ref long fl_man_eliminate, ref long fl_richieste_eliminate, ref string fs_errore)
end prototypes

public function integer wf_elimina_tipi_man (ref long fl_tipi_man_eliminate, ref string fs_errore);

string ls_delete, ls_tabelle[]
long	ll_index, ll_tot, ll_count


hpb_1.position=0
st_log.text = "Preparazione dati delle tipologie di manutenzione in corso ..."


select count(*)
into :fl_tipi_man_eliminate
from tab_tipi_manutenzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_attrezzatura=:is_cod_attrezzatura;
			
if sqlca.sqlcode<0 then
	fs_errore = "Errore in conteggio tipi manutenzione attrezzatura: "+sqlca.sqlerrtext
	return -1
end if

if isnull(fl_tipi_man_eliminate) then fl_tipi_man_eliminate=0

if fl_tipi_man_eliminate=0 then return 1



//caricamento array tabelle da ripulire ###################################
ll_index = 0


ll_index += 1
ls_tabelle[ll_index] = "det_tipi_manutenzioni"

ll_index += 1
ls_tabelle[ll_index] = "elenco_risorse"

ll_index += 1
ls_tabelle[ll_index] = "det_budget_manut_periodi"

ll_index += 1
ls_tabelle[ll_index] = "det_budget_manut_simulate"

ll_index += 1
ls_tabelle[ll_index] = "simulazione_manutenzioni"

ll_index += 1
ls_tabelle[ll_index] = "tab_piani_manutenzione"

ll_index += 1
ls_tabelle[ll_index] = "tab_guasti"

ll_index += 1
ls_tabelle[ll_index] = "manutenzioni_simulate"

ll_index += 1
ls_tabelle[ll_index] = "tipi_manutenzioni_ricambi"

//questa qui è la tabella tipi manutenzioni, ultimo livello
ll_index += 1
ls_tabelle[ll_index] = "tab_tipi_manutenzione"
//#########################################################



ll_tot = upperbound(ls_tabelle)

hpb_1.maxposition = fl_tipi_man_eliminate

for ll_index=1 to ll_tot

	hpb_1.stepit()
	st_log.text = string(ll_index) + " di " + string(ll_tot) + " : pulizia tabella "+ls_tabelle[ll_index]+" in corso ..."

	ls_delete = "delete from "+ls_tabelle[ll_index]+" "+&
					"where 	cod_azienda='"+s_cs_xx.cod_azienda+"'  and "+&
								"cod_attrezzatura='"+is_cod_attrezzatura+"' "

	execute immediate :ls_delete;
	
//	if sqlca.sqlcode < 0 then
//		fs_errore = "Errore in cancellazione dati tabella " +ls_tabelle[ll_index]+ ": " + sqlca.sqlerrtext
//
//		return -1
//	end if	

next

hpb_1.position=0
st_log.text = ""


return 1
end function

public function boolean wf_controlla_referenze_bloccanti (string fs_tipo, string fs_cod_tipo_manutenzione, string fs_cod_attrezzatura, long fl_anno, long fl_numero, ref string fs_errore);//long ll_count
//
//if fs_tipo="M" then
//	//verifica richieste
//	select count(*)
//	into :ll_count
//	from manutenzioni
//	where 	cod_azienda=:s_cs_xx.cod_azienda and
//				
//				anno_reg_richiesta is not null and anno_reg_richiesta>0 and
//				num_reg_richiesta is not null and num_reg_richiesta>0;
//	
//elseif fs_tipo="T" then
//	//verifica richieste, test prodotti, distinta attrezzature
//else
//	return false
//end if
//
//
//

return true
end function

public function integer wf_elimina_man_ordinarie (string fs_elimina_piani, ref long fl_piani_eliminati, ref long fl_man_eliminate, ref string fs_errore);//elimina i piani e le manutenzioni dell'attrezzatura

string					ls_sql
datastore			lds_data
long					ll_tot, ll_index, ll_anno_cu, ll_num_cu, ll_count


hpb_1.position=0
st_log.text = "Preparazione dati delle manutenzioni ordinarie in corso ..."

//leggo le manutenzioni (non su richiesta) dell'apparecchiatura
ls_sql = 				"select "+&
							"anno_registrazione,"+&
							"num_registrazione "+&
						"from manutenzioni "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_attrezzatura='"+is_cod_attrezzatura+"' and "+&
								"(anno_reg_richiesta is null or anno_reg_richiesta=0) "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_errore = "Errore creazione datastore manutenzioni attrezzatura! Operazione annullata!"
	return -1
end if

ll_tot = lds_data.retrieve()

hpb_1.maxposition = ll_tot

for ll_index=1 to ll_tot
	ll_anno_cu = lds_data.getitemnumber(ll_index, 1)
	ll_num_cu = lds_data.getitemnumber(ll_index, 2)
	
	hpb_1.stepit()
	st_log.text = string(ll_index) + " di " + string(ll_tot) + " : eliminazione manutenzione n. "+string(ll_anno_cu)+" / "+string(ll_num_cu)+" in corso ..."
	
	if wf_elimina_manutenzione(ll_anno_cu, ll_num_cu, fs_errore) < 0 then
		destroy lds_data;
		return -1
	end if
	
	fl_man_eliminate += 1
next

destroy lds_data;


if fs_elimina_piani="S" then
	//continua
else
	//esci
	return 1
end if


hpb_1.position=0
st_log.text = "Preparazione dati dei piani di manutenzioni ordinarie in corso ..."

//leggo i piani manutenzione ---------------
ls_sql = 				"select "+&
							"anno_registrazione,"+&
							"num_registrazione "+&
						"from programmi_manutenzione "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_attrezzatura='"+is_cod_attrezzatura+"' "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_errore = "Errore creazione datastore piani programmazione! Operazione annullata!"
	
	return -1
end if

ll_tot = lds_data.retrieve()

hpb_1.maxposition = ll_tot

for ll_index=1 to ll_tot
	ll_anno_cu = lds_data.getitemnumber(ll_index, 1)
	ll_num_cu = lds_data.getitemnumber(ll_index, 2)
	
	hpb_1.stepit()
	st_log.text = string(ll_index) + " di " + string(ll_tot) + " : eliminazione piano manutenzione n. "+string(ll_anno_cu)+" / "+string(ll_num_cu)+" in corso ..."
	
	//elimina prog_manutenzioni_ricambi
	delete from prog_manutenzioni_ricambi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione prog manut. ricambi: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	//elimina det_prog_manutenzioni
	delete from det_prog_manutenzioni
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione det. prog manutenzioni: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	//caso non global service  ##################
	
	//elimina prog_manutenzioni_operai eventuali
	delete from prog_manutenzioni_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
			
	//elimina prog_manutenzioni_risorse_est eventuali
	delete from prog_manutenzioni_risorse_est
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
	
	delete from prog_man_ordinati
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
	
	
	//elimina piano manutenzione
	delete from programmi_manutenzione
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_cu and
			num_registrazione=:ll_num_cu;
	//#################################
			
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione piano manutenzione: "+sqlca.sqlerrtext
		destroy lds_data;
		
		return -1
	end if
	
	fl_piani_eliminati += 1
	
next

destroy lds_data;

hpb_1.position=0
st_log.text = ""

return 1
end function

public function integer wf_elimina_manutenzione (integer ai_anno, long al_numero, ref string as_errore);string				ls_sql
datastore		lds_data
long				ll_index, ll_tot, ll_num_taratura
integer			li_anno_taratura


if ib_global_service then
	
	//elimina le fasi operaio
	delete from manutenzioni_fasi_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno and
			num_registrazione=:al_numero;
			
	if sqlca.sqlcode<0 then
		as_errore = "Errore in cancellazione fasi operai: "+sqlca.sqlerrtext
		
		return -1
	end if

	//elimina le fasi risorse esterne
	delete from manutenzioni_fasi_risorse
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno and
			num_registrazione=:al_numero;
			
	if sqlca.sqlcode<0 then
		as_errore = "Errore in cancellazione fasi risorse esterne: "+sqlca.sqlerrtext
		
		return -1
	end if

	//elimina le fasi
	delete from manutenzioni_fasi
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno and
			num_registrazione=:al_numero;
			
	if sqlca.sqlcode<0 then
		as_errore = "Errore in cancellazione fasi: "+sqlca.sqlerrtext
		
		return -1
	end if
	
end if

//------------------------------------------------------------
//eliminazione tarature
ls_sql = "select anno_reg_taratura, num_reg_taratura "+&
			"from tes_registro_tarature "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(ai_anno) + " and "+&
						"num_registrazione="+string(al_numero)

ll_tot = guo_functions.uof_crea_datastore(lds_data,ls_sql, as_errore)

if ll_tot>0 then
	for ll_index=1 to ll_tot
		li_anno_taratura = lds_data.getitemnumber(ll_index, 1)
		ll_num_taratura = lds_data.getitemnumber(ll_index, 2)
		
		delete from det_registro_tarature
		where	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					anno_reg_taratura=:li_anno_taratura and
					num_reg_taratura=:ll_num_taratura;
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore in cancellazione det_registro_tarature: "+sqlca.sqlerrtext
			return -1
		end if
		
	next
	
	delete from tes_registro_tarature
	where cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione=:ai_anno and
			num_registrazione=:al_numero;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in cancellazione tes_registro_tarature: "+sqlca.sqlerrtext
		return -1
	end if
	
end if


//elimina ricambi
delete from manutenzioni_ricambi
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;
		
if sqlca.sqlcode<0 then
	as_errore = "Errore in cancellazione ricambi: "+sqlca.sqlerrtext
	
	return -1
end if

//caso non global service  ##########

//elimina manutenzioni_operai eventuali
delete from manutenzioni_operai
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;

//elimina manutenzioni_risorse_esterne eventuali
delete from manutenzioni_risorse_esterne
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;

//#########################


delete from manutenzioni_avvisi
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;
		
delete from manutenzioni_destinatari
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;

delete from fermi_macchina
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;

delete from det_manutenzioni
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;
		
delete from tab_operatori_manutenzioni
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;
		
delete from det_cal_progr_manut
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_reg_manu=:ai_anno and
		num_reg_manu=:al_numero;


//elimina manutenzione
delete from manutenzioni
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ai_anno and
		num_registrazione=:al_numero;
		
if sqlca.sqlcode<0 then
	as_errore = "Errore in cancellazione manutenzione: "+sqlca.sqlerrtext
	
	return -1
end if


return 0
end function

public function integer wf_elimina_richieste (ref long fl_richieste_eliminate, ref string fs_errore);//elimina i piani e le manutenzioni dell'attrezzatura

string					ls_sql
datastore			lds_data, lds_data2
long					ll_tot, ll_index, ll_num_man, ll_num_richiesta_cu, ll_count, ll_tot2, ll_index2
integer				li_anno_richiesta_cu, li_anno_man


hpb_1.position=0
st_log.text = "Preparazione dati delle richieste in corso ..."

//seleziono le richieste legate all'attrezzatura
ls_sql = "select anno_registrazione, num_registrazione "+&
			"from tab_richieste "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_attrezzatura='"+is_cod_attrezzatura+"' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)

if ll_tot<0 then
	return -1
end if

hpb_1.maxposition=ll_tot

for ll_index=1 to ll_tot
	//anno e numero della richiesta
	li_anno_richiesta_cu = lds_data.getitemnumber(ll_index, 1)
	ll_num_richiesta_cu = lds_data.getitemnumber(ll_index, 2)
	
	hpb_1.stepit()
	st_log.text = string(ll_index) + " di " + string(ll_tot) + " : eliminazione richiesta n. "+string(li_anno_richiesta_cu)+" / "+string(ll_num_richiesta_cu)+&
						" (e dati manutentivi collegati) in corso ..."
	
	//se la richiesta ha delle offerte di acquisto blocco tutto!
	select count(*)
	into :ll_count
	from tes_off_acq
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_richiesta=:li_anno_richiesta_cu and
				num_richiesta=:ll_num_richiesta_cu;
	
	if ll_count>0 then
		fs_errore = "Esiste una richiesta (n. " +string(li_anno_richiesta_cu)+ "/" + string(ll_num_richiesta_cu) + ") legata all'attrezzatura"+&
						" che ha una o più offerte di acquisto generate e collegate! Impossibile proseguire!"
		destroy lds_data;
		return -1
	end if
	
	//recupero le manutenzioni legate a questa richiesta
	ls_sql = "select anno_registrazione, num_registrazione "+&
				"from manutenzioni "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"anno_reg_richiesta="+string(li_anno_richiesta_cu)+" and "+&
							"num_reg_richiesta="+string(ll_num_richiesta_cu)
	
	ll_tot2 = guo_functions.uof_crea_datastore(lds_data2, ls_sql, fs_errore)
	if ll_tot2<0 then
		destroy lds_data;
		return -1
	end if
	
	for ll_index2=1 to ll_tot2
		//anno e numero della manutenzione
		li_anno_man = lds_data2.getitemnumber(ll_index2, 1)
		ll_num_man = lds_data2.getitemnumber(ll_index2, 2)
		
		//elimina manutenzione/i su richiesta e annessi
		if wf_elimina_manutenzione(li_anno_man, ll_num_man, fs_errore) < 0 then
			destroy lds_data2;
			destroy lds_data;
			return -1
		end if
	next
	
	
	delete from tab_richieste_blob
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_richiesta_cu and
				num_registrazione=:ll_num_richiesta_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione documenti richiesta: "+sqlca.sqlerrtext
		destroy lds_data;
		return -1
	end if
	
	
	delete from tab_richieste_supplementari
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_richiesta_cu and
				num_registrazione=:ll_num_richiesta_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione richieste supplementari: "+sqlca.sqlerrtext
		destroy lds_data;
		return -1
	end if
	
	
	delete from tab_richieste_sospensioni
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_richiesta_cu and
				num_registrazione=:ll_num_richiesta_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione sospensioni richiesta: "+sqlca.sqlerrtext
		destroy lds_data;
		return -1
	end if
	
	
	delete from tab_richieste
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_richiesta_cu and
				num_registrazione=:ll_num_richiesta_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in cancellazione richiesta: "+sqlca.sqlerrtext
		destroy lds_data;
		return -1
	end if

	
	fl_richieste_eliminate += 1
	
next

destroy lds_data;


//adesso faccio un controllo se per caso esistono ancora manutenzioni su richiesta sulla apparecchiatura selezionata
//ma la cui richiesta collegata non è sull'attrezzatura (stranamente) e quindi la manutenzione non è stata intercettata e cancellata
//questo può dare problemi se poi faccio la cancellazione della tabella guasti su apparecchiature (errore di fk con la manutenzioni...)
//cancello solo la manutenzione ma lascio stare la richiesta
//recupero le manutenzioni legate a questa apparecchiatura (eventualmente ancora presenti)
ls_sql = "select anno_registrazione, num_registrazione "+&
			"from manutenzioni "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_attrezzatura='"+is_cod_attrezzatura+"' and "+&
					"anno_reg_richiesta is not null and anno_reg_richiesta>0 "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)
if ll_tot<0 then
	destroy lds_data;
	return -1
end if

hpb_1.position=0
hpb_1.maxposition=ll_tot

for ll_index=1 to ll_tot
	//anno e numero della manutenzione
	li_anno_man = lds_data.getitemnumber(ll_index, 1)
	ll_num_man = lds_data.getitemnumber(ll_index, 2)
	
	hpb_1.stepit()
	st_log.text = "Pulizia eventuali manutenzioni su richiesta collegate all'attrezzatura (e non eliminate al passo precedente) in corso ..."
	
	//elimina manutenzione e annessi
	if wf_elimina_manutenzione(li_anno_man, ll_num_man, fs_errore) < 0 then
		destroy lds_data2;
		destroy lds_data;
		return -1
	end if
next

hpb_1.position=0
st_log.text = ""

return 1
end function

public function integer wf_conferma (string fs_elimina_tipi_man, string fs_elimina_piani, string fs_elimina_richieste, ref long fl_tipi_man_eliminati, ref long fl_piani_eliminati, ref long fl_man_eliminate, ref long fl_richieste_eliminate, ref string fs_errore);string ls_elimina_piani


if fs_elimina_richieste = "S" then
	if wf_elimina_richieste(fl_richieste_eliminate, fs_errore) < 0 then
		//in fs_errore il messaggio
	
		return -1
	end if
end if

//elimna manutenzioni ordinarie e piani
if wf_elimina_man_ordinarie(fs_elimina_piani, fl_piani_eliminati, fl_man_eliminate, fs_errore)<0 then
	//in fs_errore il messaggio
	
	return -1
end if

if fs_elimina_tipi_man="S" then
	if wf_elimina_tipi_man(fl_tipi_man_eliminati, fs_errore)<0 then
		//in fs_errore il messaggio
	
		return -1
	end if
else
	//hai finito
	return 1
end if

//se arrivi fin qui tutto a posto ...
return 1
end function

on w_attrez_dati_cancella.create
this.st_log=create st_log
this.hpb_1=create hpb_1
this.dw_selezione=create dw_selezione
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.st_attrezzatura=create st_attrezzatura
this.Control[]={this.st_log,&
this.hpb_1,&
this.dw_selezione,&
this.cb_annulla,&
this.cb_conferma,&
this.st_attrezzatura}
end on

on w_attrez_dati_cancella.destroy
destroy(this.st_log)
destroy(this.hpb_1)
destroy(this.dw_selezione)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.st_attrezzatura)
end on

event open;
 is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1
 is_des_attrezzatura = s_cs_xx.parametri.parametro_s_2
ib_global_service = s_cs_xx.parametri.parametro_b_2

dw_selezione.insertrow(0)

setnull(s_cs_xx.parametri.parametro_s_1 )
setnull(s_cs_xx.parametri.parametro_s_2 )
setnull(s_cs_xx.parametri.parametro_d_1 )

s_cs_xx.parametri.parametro_b_2 = false

st_attrezzatura.text = "Eliminazione dati da attrezzatura "+is_cod_attrezzatura + " " + is_des_attrezzatura
end event

type st_log from statictext within w_attrez_dati_cancella
integer x = 32
integer y = 524
integer width = 2501
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_attrez_dati_cancella
integer x = 32
integer y = 608
integer width = 2501
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_selezione from datawindow within w_attrez_dati_cancella
integer x = 32
integer y = 136
integer width = 2501
integer height = 280
integer taborder = 10
string title = "none"
string dataobject = "d_attrez_dati_cancella_sel"
boolean border = false
boolean livescroll = true
end type

event itemchanged;choose case dwo.name
	case "flag_elimina_piani"
		if data="S" then
		else
			//metti a N la cancellazione dei tipi manutenzione, anche perchè poi diventa invisibile
			setitem(1, "flag_elimina_tipi_man", "N")
		end if
		
		
	case "flag_elimina_tipi_man"
		if data="S" then
			setitem(1, "flag_elimina_richieste", "S")
		end if
		
end choose
end event

type cb_annulla from commandbutton within w_attrez_dati_cancella
integer x = 1307
integer y = 712
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;setnull(s_cs_xx.parametri.parametro_d_1 )

close(parent)
end event

type cb_conferma from commandbutton within w_attrez_dati_cancella
integer x = 809
integer y = 712
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string			ls_errore, ls_flag_elimina_piani, ls_flag_elimina_tipi_man, ls_messaggio, ls_flag_elimina_richieste
long			ll_tipi_man_eliminate, ll_piani_eliminati, ll_man_eliminate, ll_richieste_eliminate

dw_selezione.accepttext()

hpb_1.position=0
st_log.text = ""

ls_flag_elimina_piani = dw_selezione.getitemstring(1, "flag_elimina_piani")
ls_flag_elimina_tipi_man = dw_selezione.getitemstring(1, "flag_elimina_tipi_man")
ls_flag_elimina_richieste = dw_selezione.getitemstring(1, "flag_elimina_richieste")

//ulteriore controllo per scrupolo: non posso eliminate le tipologie se non voglio eliminare i piani
if ls_flag_elimina_piani="N" then ls_flag_elimina_tipi_man="N"

//se devo eliminare le tipologie allora devo per forza eliminare anche le richieste
if ls_flag_elimina_tipi_man = "S" then ls_flag_elimina_richieste = "S"


ls_messaggio = "Procedere all'eliminazione dei seguenti dati?~r~n"
ls_messaggio += "Manutenzioni attrezzatura "+is_cod_attrezzatura+"~r~n"

if ls_flag_elimina_richieste="S" then
	ls_messaggio += "Richieste Manutenzione attrezzatura "+is_cod_attrezzatura+" (e relativi interventi)~r~n"
end if

if ls_flag_elimina_piani="S" then
	
	ls_messaggio += "Piani Manutenzione attrezzatura "+is_cod_attrezzatura+"~r~n"
	
	if ls_flag_elimina_tipi_man="S" then
		ls_messaggio += "Tipi Manutenzione attrezzatura "+is_cod_attrezzatura
	end if
	
end if

if g_mb.confirm("OMNIA", ls_messaggio, 2 ) then
	//continua
else
	return
end if

ll_tipi_man_eliminate = 0
ll_piani_eliminati = 0
ll_man_eliminate = 0
ll_richieste_eliminate = 0

if wf_conferma(	ls_flag_elimina_tipi_man, ls_flag_elimina_piani, ls_flag_elimina_richieste, &
						ll_tipi_man_eliminate, ll_piani_eliminati, ll_man_eliminate, ll_richieste_eliminate, ls_errore)<0 then
	rollback;
	
	st_log.text = "Errore!"
	
	setnull(s_cs_xx.parametri.parametro_d_1)
	g_mb.error(ls_errore)
	
	return
end if

//se sei qui fai commit

hpb_1.position=0
st_log.text = "Fine!"

commit;

s_cs_xx.parametri.parametro_d_1 = 0

ls_messaggio = "Operazione terminata con successo! Sono stati eliminati i seguenti dati:~r~n"
ls_messaggio +="N° Manutenzioni ordinarie: "+string(ll_man_eliminate)+"~r~n"

if ls_flag_elimina_richieste="S" then ls_messaggio +="N° Richieste: "+string(ll_richieste_eliminate)+"~r~n"
if ls_flag_elimina_piani="S" then ls_messaggio +="N° Piani Manutenzione: "+string(ll_piani_eliminati)+"~r~n"
if ls_flag_elimina_tipi_man="S" then ls_messaggio +="N° Tipi Manutenzione: "+string(ll_tipi_man_eliminate)

g_mb.show("OMNIA",ls_messaggio)

close(parent)
end event

type st_attrezzatura from statictext within w_attrez_dati_cancella
integer x = 18
integer y = 16
integer width = 2537
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Attrezzatura"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

