﻿$PBExportHeader$w_attrezzature.srw
$PBExportComments$Anagrafica Attrezzature
forward
global type w_attrezzature from w_cs_xx_principale
end type
type cb_report_tarature from commandbutton within w_attrezzature
end type
type cb_cerca from commandbutton within w_attrezzature
end type
type cb_richiama_predefinito from commandbutton within w_attrezzature
end type
type cb_predefinito from commandbutton within w_attrezzature
end type
type sle_1 from singlelineedit within w_attrezzature
end type
type cb_etichette_bc from commandbutton within w_attrezzature
end type
type cb_documento from commandbutton within w_attrezzature
end type
type cb_etichette from commandbutton within w_attrezzature
end type
type dw_filtro from u_dw_search within w_attrezzature
end type
type dw_folder_tree from u_folder within w_attrezzature
end type
type dw_folder from u_folder within w_attrezzature
end type
type dw_attrezzature_2 from uo_cs_xx_dw within w_attrezzature
end type
type dw_attrezzature_1 from uo_cs_xx_dw within w_attrezzature
end type
type dw_tarature_periodiche from uo_cs_xx_dw within w_attrezzature
end type
type dw_tarature_incrociate from uo_cs_xx_dw within w_attrezzature
end type
type tv_1 from treeview within w_attrezzature
end type
type dw_col_din from uo_colonne_dinamiche_dw within w_attrezzature
end type
end forward

global type w_attrezzature from w_cs_xx_principale
integer width = 4448
integer height = 2260
string title = "Attrezzature"
event pc_menu_note ( )
event pc_menu_componenti ( )
event type integer pc_menu_contratto ( )
event pc_menu_codice ( )
event pc_menu_duplica ( )
event pc_menu_refresh ( )
event we_navigatore_manutenzioni ( )
event pc_menu_cancella_dati ( )
cb_report_tarature cb_report_tarature
cb_cerca cb_cerca
cb_richiama_predefinito cb_richiama_predefinito
cb_predefinito cb_predefinito
sle_1 sle_1
cb_etichette_bc cb_etichette_bc
cb_documento cb_documento
cb_etichette cb_etichette
dw_filtro dw_filtro
dw_folder_tree dw_folder_tree
dw_folder dw_folder
dw_attrezzature_2 dw_attrezzature_2
dw_attrezzature_1 dw_attrezzature_1
dw_tarature_periodiche dw_tarature_periodiche
dw_tarature_incrociate dw_tarature_incrociate
tv_1 tv_1
dw_col_din dw_col_din
end type
global w_attrezzature w_attrezzature

type variables
//boolean ib_new=FALSE
long il_entrata = 0
boolean ib_new_modify = false
boolean ib_close = false
boolean ib_tree_delete = false


// Filtro
string is_sql_filtro, is_cod_attrezzatura
long 	il_livello_corrente, il_ultimo_handle
treeviewitem tvi_campo

// campi automatici
string is_divisione, is_reparto, is_categoria, is_area

// stefanop 21/12/2009: Modifiche Navigatori
private:
	string is_navigatore_tipo
	string is_navigatore_valore
	string is_navigatore_doc

// stefanop 15/03/2011: vignoni (parametro aziendale ATE)
private:
	boolean ib_tarature_enable = false, ib_global_service=false
	uo_tarature iuo_tarature

constant string TARATURA_PERIODICA = "P"
constant string TARATURA_INCROCIATA = "I"
end variables

forward prototypes
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_treeview ()
public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_registrazione)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
private subroutine wf_stato_pulsanti (boolean fb_enabled)
public subroutine wf_menu_note ()
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_cancella_livello (long fl_handle)
public subroutine wf_cancella_attrezzatura ()
public subroutine wf_inserisci_singola_attrezzatura (long row)
public subroutine wf_abilita_tab_tarature (integer al_row)
public function string wf_prog_alphanumerico ()
public subroutine wf_cancella_taratura (integer ai_row, string as_tipo_taratura)
public subroutine wf_visualizza_taratura (integer ai_row, string as_tipo_taratura, boolean ab_nuova_taratura)
public subroutine wf_calcola_taratura (integer ai_row, string as_tipo_taratura)
public function integer wf_inserisci_aree (long fl_handle)
public function integer wf_inserisci_categorie (long fl_handle)
public function integer wf_inserisci_divisioni (long fl_handle)
public function integer wf_inserisci_reparti (long fl_handle)
public function integer wf_inserisci_attrezzature (long fl_handle)
end prototypes

event pc_menu_note;dw_attrezzature_1.accepttext()

window_open_parm(w_attrezzature_note, -1, dw_attrezzature_1)
end event

event pc_menu_componenti();if dw_attrezzature_1.getrow() > 0 then
   s_cs_xx.parametri.parametro_s_1 = dw_attrezzature_1.getitemstring( &
                                     dw_attrezzature_1.getrow(), "cod_prodotto")
   if isnull(s_cs_xx.parametri.parametro_s_1) then
      g_mb.messagebox("Anagrafica Attrezzature","Manca il codice prodotto distinta corrispondente all'attrezzatura", StopSign!)
      return
   end if
end if

if not isvalid(w_vista_distinta_attrezzat) then
   window_open_parm(w_vista_distinta_attrezzat, 0, dw_attrezzature_1)
end if
end event

event type integer pc_menu_contratto();long   ll_return, ll_anno[], ll_progressivo[]

string ls_cod_attrezzatura, ls_attivi


ls_cod_attrezzatura = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(),"cod_attrezzatura")

ll_return = f_contratti_manut_attivi(ls_cod_attrezzatura,ll_anno,ll_progressivo,ls_attivi)

if ll_return = -1 then
	g_mb.messagebox("OMNIA",ls_attivi,stopsign!)
	return -1
elseif ll_return = 0 then
	g_mb.messagebox("OMNIA","Non esistono contratti di manutenzione attivi per questa attrezzatura!",stopsign!)
	return -1
elseif ll_return > 1 then
	g_mb.messagebox("OMNIA","Esistono più contratti di manutenzione attivi per questa attrezzatura!~n" + ls_attivi,stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_b_1 = true

s_cs_xx.parametri.parametro_d_1 = ll_anno[1]

s_cs_xx.parametri.parametro_d_2 = ll_progressivo[1]

window_open(w_contratti_manut, -1)

end event

event pc_menu_codice();if dw_attrezzature_1.getrow() < 1 or isnull(dw_attrezzature_1.getrow()) then
	g_mb.messagebox("OMNIA","Selezionare un'attrezzatura")
	return
end if

s_cs_xx.parametri.parametro_s_1 = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(), "cod_attrezzatura")

window_open(w_mod_codice_attr,0)

if s_cs_xx.parametri.parametro_d_1 = 0 then
	triggerevent("pc_retrieve")
end if
end event

event pc_menu_duplica();window_open(w_sel_copia_attrez, 0)
end event

event pc_menu_refresh();long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_data

// ritrovo handle corrente
ll_handle = tv_1.finditem(currenttreeitem!, 0)
// chiudo il nodo se aperto
tv_1.CollapseItem(ll_handle)
// prelevo le informazioni
tv_1.getitem(ll_handle, tvi_item)
// pulisco i figli
wf_cancella_livello(ll_handle)
// dichiaro che ha figli
tvi_item.children = true
// dico che non è mai stato aperto, così rilancia l'evento itempopulate
tvi_item.ExpandedOnce = false
// imposto il figlio
tv_1.setitem(ll_handle, tvi_item)

end event

event we_navigatore_manutenzioni();dw_filtro.reset()
dw_filtro.insertrow(0)

choose case is_navigatore_tipo
	case "area"
		dw_filtro.setitem(dw_filtro.getrow(), "cod_area_aziendale", is_navigatore_valore)		
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "E")	
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "reparto"
		dw_filtro.setitem(dw_filtro.getrow(), "cod_reparto", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "R")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)
		
	case "divisione"
		dw_filtro.setitem(dw_filtro.getrow(), "cod_divisione", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "D")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "A")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)	
	
	case "scheda"
		dw_filtro.setitem(dw_filtro.getrow(), "cod_attrezzatura_da", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "cod_attrezzatura_a", is_navigatore_valore)
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_1", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_2", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_3", "N")
		dw_filtro.setitem(dw_filtro.getrow(), "flag_tipo_livello_4", "N")	
		cb_cerca.TriggerEvent(Clicked!)
		if is_navigatore_doc = "doc" then
			cb_documento.postevent(clicked!)
		end if
	
end choose
end event

event pc_menu_cancella_dati();string ls_cod_attrezzatura

if dw_attrezzature_1.getrow() < 1 or isnull(dw_attrezzature_1.getrow()) then
	g_mb.error("OMNIA","Selezionare un'attrezzatura!")
	return
end if

ls_cod_attrezzatura  = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(), "cod_attrezzatura")
if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura="" then
	g_mb.error("OMNIA","Selezionare un'attrezzatura!")
	return
end if

s_cs_xx.parametri.parametro_s_1 = ls_cod_attrezzatura
s_cs_xx.parametri.parametro_s_2 = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(), "descrizione")
s_cs_xx.parametri.parametro_b_2 = ib_global_service

window_open(w_attrez_dati_cancella, 0 )

//if s_cs_xx.parametri.parametro_d_1 = 0 then
//	triggerevent("pc_retrieve")
//end if

return
end event

public subroutine wf_cancella_treeview ();ib_tree_delete = true
tv_1.deleteitem(0)
ib_tree_delete = false
end subroutine

public subroutine wf_imposta_treeview ();string ls_cod_da, ls_cod_a
setpointer(HourGlass!)
tv_1.setredraw(false)


// se non è stato impostato alcun livello ed è stata specificata una attrezzatura
// procedo con il caricamento delle registrazioni della sola attrezzatura richiesta 
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_1") = "N" then
	// -- stefanop 09/10/2009: Elimino il trim
	ls_cod_da = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da")
	ls_cod_a  = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a")
	// ----
	
	wf_inserisci_attrezzature(0)
	tv_1.setredraw(true)
	setpointer(arrow!)
	return

end if
// --------------------------------

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "D"
		 wf_inserisci_divisioni(0)
		// caricamento divisioni
	case "R"
		wf_inserisci_reparti(0)
		// caricamento reparti
	case "C"
		wf_inserisci_categorie(0)
		// caricamento categorie
	case "E" 
		wf_inserisci_aree(0)
		// caricamento aree
	case "A", "N"
		wf_inserisci_attrezzature(0)
		// caricamento attrezzature
end choose
// --------------------------------

tv_1.setredraw(true)
setpointer(arrow!)
end subroutine

public function integer wf_inserisci_registrazioni (long fl_handle, string fs_cod_registrazione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento registrazioni di manutenzione / taratura (STD)
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false

string  ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_str, ls_sql, ls_sql_livello, &
        ls_ordine_registrazione, ls_flag_ordinarie, ls_flag_eseguito, ls_data_registrazione, ls_cod_guasto_filtro, &
		  ls_cod_tipo_manut_filtro, ls_flag_scadenze
		  
long    ll_risposta, ll_i, ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_livello, ll_anno_reg_filtro, &
        ll_anno_reg_programma, ll_num_reg_programma
		  
datetime ldt_data_registrazione, ldt_data_inizio, ldt_data_fine

str_attrezzature_tree lstr_record

ll_livello = il_livello_corrente

declare cu_registrazioni dynamic cursor for sqlsa;
ls_sql = "SELECT anno_registrazione, " + &
         "       num_registrazione, " + &
			"       data_registrazione, " + &
			"       cod_tipo_manutenzione, " + &
			"       cod_attrezzatura, " + &
			"       flag_eseguito, " + &
			"       anno_reg_programma, " + &
			"       num_reg_programma " + &			
			"FROM   manutenzioni " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

//----------- Michele 26/04/2004 correzione caricamento registrazioni -------------
if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro + ")"
end if
//----------- Fine Modifica -------------

choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_eseguito")
	case "S"
		ls_sql += " and flag_eseguito = 'S' "
	case "N"
		ls_sql += " and flag_eseguito = 'N' "
end choose

ll_anno_reg_filtro = dw_filtro.getitemnumber(dw_filtro.getrow(),"anno_registrazione")
ls_flag_ordinarie = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinarie")
ls_ordine_registrazione = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_ordinamento_registrazioni")
ldt_data_inizio = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_inizio")
ldt_data_fine = dw_filtro.getitemdatetime(dw_filtro.getrow(),"data_fine")
ls_cod_tipo_manut_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")
ls_cod_guasto_filtro = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_guasto")

if not isnull(ll_anno_reg_filtro) and ll_anno_reg_filtro > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg_filtro) + " "
end if	

if not isnull(ldt_data_inizio) and ldt_data_inizio >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fine) and ldt_data_fine >= datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if

choose case ls_flag_ordinarie
	case "S"
		ls_sql += " and flag_ordinario = 'S' "
	case "N"
		ls_sql += " and flag_ordinario = 'N' "
end choose

if len(ls_cod_tipo_manut_filtro) < 1 then setnull(ls_cod_tipo_manut_filtro)

if not isnull(ls_cod_tipo_manut_filtro) then
	ls_sql += " and cod_tipo_manutenzione = '" + ls_cod_tipo_manut_filtro + "' "
end if

if len(ls_cod_guasto_filtro) < 1 then setnull(ls_cod_guasto_filtro)

if not isnull(ls_cod_guasto_filtro) then
	ls_sql += " and cod_guasto = '" + ls_cod_guasto_filtro + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_attrezzatura in (select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + " ) "
end if

if isnull(ls_ordine_registrazione) then ls_ordine_registrazione = "D"

// -- stefanop 04/06/2009: commento perchè il flag non è più usato.
//choose case ls_ordine_registrazione
//	case "C"
//		ls_sql += " order by data_registrazione ASC "
//	case "D"
//		ls_sql += " order by data_registrazione DESC "
//	case "A"
//		ls_sql += " order by cod_attrezzatura ASC " 
//	case "T"
//		ls_sql += " order by cod_tipo_manutenzione ASC "
//end choose

choose case dw_filtro.getitemstring(1, "flag_ordine")
	case "C"
		ls_sql += " order by cod_attrezzatura"
	case "D"
		ls_sql += " order by cod_tipo_manutenzione"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch cu_registrazioni into :ll_anno_registrazione, 
	                            :ll_num_registrazione, 
										 :ldt_data_registrazione, 
										 :ls_cod_tipo_manutenzione, 
										 :ls_cod_attrezzatura, 
										 :ls_flag_eseguito,
										 :ll_anno_reg_programma,
										 :ll_num_reg_programma;

   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_manutenzioni:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		return -1
	end if

	if isnull(ls_cod_tipo_manutenzione) then ls_cod_tipo_manutenzione = ""
	
	if isnull(ls_cod_attrezzatura) then ls_cod_attrezzatura = ""
	
	lb_dati = true
	
	setnull(ls_des_tipo_manutenzione)
	
	select des_tipo_manutenzione
	into   :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<DESCRIZIONE MANCANTE>"
	
	if isnull(ldt_data_registrazione) then
		ls_data_registrazione = "DATA MANCANTE"
	else
		ls_data_registrazione = string(ldt_data_registrazione,"dd/mm/yyyy")
	end if
	
	
	//lstr_record.des_manutenzione = ls_des_tipo_manutenzione
	
	lstr_record.codice = ""		
	lstr_record.descrizione = ""
	lstr_record.livello = 100
	lstr_record.tipo_livello = "M"
	

	choose case dw_filtro.getitemstring(1, "flag_ordine")
		case "C"
			ls_str = ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " + ls_cod_tipo_manutenzione
		case else
			ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	end choose
	//ls_str = ls_cod_tipo_manutenzione + " " + ls_data_registrazione + " " + ls_des_tipo_manutenzione + " " 
	
	tvi_campo.data = lstr_record
	tvi_campo.label = ls_str
	
	/*
	ls_flag_scadenze = wf_controlla_scadenza_periodo( ll_anno_reg_programma, ll_num_reg_programma)
	if ls_flag_eseguito = "S" then	
		if ls_flag_scadenze = 'S' then
			tvi_campo.pictureindex = 6
			tvi_campo.selectedpictureindex = 6
			tvi_campo.overlaypictureindex = 6	
		else	
			tvi_campo.pictureindex = 10
			tvi_campo.selectedpictureindex = 10
			tvi_campo.overlaypictureindex = 10				
		end if
	else
		if ls_flag_scadenze = 'S' then	
			tvi_campo.pictureindex = 7	
			tvi_campo.selectedpictureindex = 7
			tvi_campo.overlaypictureindex = 7
		else
			tvi_campo.pictureindex = 11
			tvi_campo.selectedpictureindex = 11
			tvi_campo.overlaypictureindex = 11
		end if
	end if	
	*/
	
	tvi_campo.children = false
	tvi_campo.selected = false
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop

close cu_registrazioni;
return 0
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);str_attrezzature_tree lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
		case "D"
			if isnull(lstr_item.codice) or lstr_item.codice = "" then
				as_sql += "and anag_attrezzature.cod_area_aziendale is null "
			else
				as_sql += " and anag_attrezzature.cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali "+&
																							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + lstr_item.codice + "' ) "
			end if
			
		case "R"
			if isnull(lstr_item.codice) or lstr_item.codice = "" then
				as_sql += " and anag_attrezzature.cod_reparto is null "
			else
				as_sql += " and anag_attrezzature.cod_reparto = '" + lstr_item.codice + "' "
			end if
			
			
		case "C"
			if isnull(lstr_item.codice) or lstr_item.codice = "" then
				as_sql += " and anag_attrezzature.cod_cat_attrezzature is null "
			else
				as_sql += " and anag_attrezzature.cod_cat_attrezzature = '" + lstr_item.codice + "' "
			end if
			
			
		case "E"
			if isnull(lstr_item.codice) or lstr_item.codice = "" then
				as_sql += " and anag_attrezzature.cod_area_aziendale is null "
			else
				as_sql += " and anag_attrezzature.cod_area_aziendale = '" + lstr_item.codice + "' "
			end if
			
		case "A"
			if isnull(lstr_item.codice) or lstr_item.codice = "" then
				as_sql += " AND anag_attrezzature.cod_attrezzatura is null "
			else
				as_sql += " AND anag_attrezzature.cod_attrezzatura = '" + lstr_item.codice + "' "
			end if
			
		case "N"
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

private subroutine wf_stato_pulsanti (boolean fb_enabled);cb_etichette_bc.enabled = fb_enabled
cb_etichette.enabled = fb_enabled
cb_documento.enabled = fb_enabled
end subroutine

public subroutine wf_menu_note ();
end subroutine

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_filtro.getitemdatetime(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_filtro.getitemstring(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_filtro.getitemnumber(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ATTR';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'ATTR';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'ATTR');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ATTR';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_cancella_livello (long fl_handle);long tvi_hdl = 0

tvi_hdl = tv_1.FindItem (ChildTreeItem!, fl_handle)
DO UNTIL tvi_hdl <= 0
	tv_1.deleteitem(tvi_hdl)
	tvi_hdl = tv_1.FindItem (ChildTreeItem!, fl_handle)
LOOP

return
end subroutine

public subroutine wf_cancella_attrezzatura ();long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_data

ll_handle = tv_1.finditem(currenttreeitem!, 0)
if ll_handle = -1 then return

tv_1.getitem(ll_handle, tvi_item)
lstr_data = tvi_item.data

if lstr_data.tipo_livello = "A" then
	tv_1.deleteitem(ll_handle)
end if
end subroutine

public subroutine wf_inserisci_singola_attrezzatura (long row);// return
long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_record

// -- Controllo il nodo selezionato
ll_handle = tv_1.finditem(currenttreeitem!, 0)
if ll_handle = -1 then return

if tv_1.getitem(ll_handle, tvi_item) = -1 then return

lstr_record = tvi_item.data
if lstr_record.tipo_livello = "A" then
	ll_handle = tv_1.finditem(parenttreeitem!, ll_handle)
end if
// ---------------------

lstr_record.codice = dw_attrezzature_1.getitemstring(row, "cod_attrezzatura")

lstr_record.descrizione = dw_attrezzature_1.getitemstring(row, "descrizione")
if isnull(lstr_record.descrizione) or lstr_record.descrizione = "" then
	lstr_record.descrizione = "<descrizione mancante>"
elseif len(lstr_record.descrizione) > 30 then
	lstr_record.descrizione = left(lstr_record.descrizione, 30) + "..."
end if

lstr_record.handle_padre = ll_handle
lstr_record.tipo_livello = "A"
lstr_record.livello = il_livello_corrente + 1
tvi_campo.data = lstr_record
tvi_campo.label = lstr_record.codice + ", " + lstr_record.descrizione

tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1

tvi_campo.children = false
tvi_campo.selected = false

tv_1.insertitemlast(ll_handle, tvi_campo)
end subroutine

public subroutine wf_abilita_tab_tarature (integer al_row);/**
 * Stefanop
 * 15/03/2011
 *
 * Questa funzione abilita o meno i tab extra delle tarature in base
 * al flag_taratura (=S abilitate, =N disabilitate)
 **/
 
string ls_cod_attrezzatura

 // controllo errore
 if al_row < 1 or not ib_tarature_enable then return

// stefanop: controllo il flag_taratura e abilito i tab
if dw_attrezzature_1.getitemstring(al_row, "flag_taratura") = 'S' then
	dw_folder.fu_enabletab(4)
	dw_folder.fu_enabletab(5)
	
	ls_cod_attrezzatura = dw_attrezzature_1.getitemstring(al_row, "cod_attrezzatura")
	
	// eseguo la query
	dw_tarature_periodiche.triggerevent("pcd_retrieve")
	dw_tarature_incrociate.triggerevent("pcd_retrieve")
else
	dw_folder.fu_selecttab(1)
	
	dw_folder.fu_disabletab(4)
	dw_folder.fu_disabletab(5)
	
	dw_tarature_periodiche.reset()
	dw_tarature_incrociate.reset()
end if
end subroutine

public function string wf_prog_alphanumerico ();/**
 * stefanop
 * 15/03/2011
 *
 * Creo un codice univoco per l'attrezzatura
 */
 
string ls_cod_attrezzatura
integer ll_prog


select max(cod_attrezzatura)
into :ls_cod_attrezzatura
from anag_attrezzature
where
	cod_azienda=:s_cs_xx.cod_azienda and
	IsNumeric(cod_attrezzatura) = 1;
	
if len(ls_cod_attrezzatura) < 1 or isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura= "" then ls_cod_attrezzatura = "0"

ll_prog = long(ls_cod_attrezzatura)
ll_prog++

return right("000000" + string(ll_prog), 6)
end function

public subroutine wf_cancella_taratura (integer ai_row, string as_tipo_taratura);/***/

if ai_row < 1 then return

if g_mb.confirm("Omnia", "Confermare la cancellazione della riga?") then
	
	if as_tipo_taratura = TARATURA_PERIODICA then
		dw_tarature_periodiche.deleterow(ai_row)
		dw_tarature_periodiche.update()
	else
		dw_tarature_incrociate.deleterow(ai_row)
		dw_tarature_incrociate.update()
	end if
	
	commit;
end if	

end subroutine

public subroutine wf_visualizza_taratura (integer ai_row, string as_tipo_taratura, boolean ab_nuova_taratura);/**
 * Apro la finestra dei dettagli della taratura
 **/

s_cs_xx_parametri lstr_parametri

lstr_parametri.parametro_s_1 = is_cod_attrezzatura
lstr_parametri.parametro_s_2 = as_tipo_taratura

if not ab_nuova_taratura then
	if as_tipo_taratura = TARATURA_PERIODICA then
		lstr_parametri.parametro_ul_1 = dw_tarature_periodiche.getitemnumber(ai_row, "prog_taratura")
	else
		lstr_parametri.parametro_ul_1 = dw_tarature_incrociate.getitemnumber(ai_row, "prog_taratura")
	end if
else
	lstr_parametri.parametro_ul_1 = -1
end if

window_open_parm(w_attrezzature_taratura, 0, lstr_parametri)

lstr_parametri = message.powerobjectparm
setnull(message.powerobjectparm)

if lstr_parametri.parametro_s_1 = 'T' then
	if as_tipo_taratura = TARATURA_PERIODICA then
		dw_tarature_periodiche.triggerevent("pcd_retrieve")
	else
		dw_tarature_incrociate.triggerevent("pcd_retrieve")
	end if
end if
end subroutine

public subroutine wf_calcola_taratura (integer ai_row, string as_tipo_taratura);/**
 * Esegue tutti i calcoli relativi alla taratura
 **/
 
string ls_col_abilitata, ls_col_valore, ls_cod_primario, ls_esito
int li_columns, li_i, li_num_tarature
dec{4} ld_totale_tarature, ld_valore_taratura,  ld_v_min, ld_v_max, ld_incertezza, ld_fattore, ld_grado_precisione, ld_tolleranza, ld_valore_medio
uo_cs_xx_dw ldw_datawindow

if ai_row < 1 then return

// conto le colonne sempre da una DW perchè il dataobject è SEMPRE uguale
li_columns = integer(dw_tarature_periodiche.Describe("DataWindow.Column.Count"))

if as_tipo_taratura = TARATURA_PERIODICA then 
	ldw_datawindow = dw_tarature_periodiche
else
	ldw_datawindow = dw_tarature_incrociate
end if

li_num_tarature = 0
ld_v_min = 9999999999999
ld_v_max = 0

for li_i = 1 to li_columns
	
	ls_col_abilitata = ldw_datawindow.Describe("#" + string(li_i) + ".Name")
	
	// Controllo che la colonna sia 'taratura_X_abilitata' e che abbia valore 'S'
	if right(ls_col_abilitata, 9) <> "abilitata" or ldw_datawindow.getitemstring(ai_row, ls_col_abilitata) = 'N' then continue

	ls_col_valore = left(ls_col_abilitata, len(ls_col_abilitata) - 9) + "valore" // colonna "taratura_X_valore"
	ld_valore_taratura = ldw_datawindow.getitemnumber(ai_row, ls_col_valore)
	
	li_num_tarature++
	ld_totale_tarature += ld_valore_taratura
	
	// Controllo minimo / massimo
	if ld_valore_taratura < ld_v_min then ld_v_min = ld_valore_taratura
	if ld_valore_taratura > ld_v_max then ld_v_max = ld_valore_taratura
	
next


// Valore medio
ld_valore_medio = ld_totale_tarature / li_num_tarature
ldw_datawindow.setitem(ai_row, "valore_medio", ld_valore_medio)

// Incertezza
select fattore
into :ld_fattore
from tab_fattore_tarature
where
	 cod_azienda = :s_cs_xx.cod_azienda and
	 num_tarature = :li_num_tarature;
	 
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero del fattore di divisione.~r~n" + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.show("Attenzione: non è stato trovato nessun fattore di divisione per il numero di tarature uguale a " + string(li_num_tarature))
	return
end if

ld_incertezza =  ((ld_v_max - ld_v_min) / ld_fattore) * 4
ldw_datawindow.setitem(ai_row, "incertezza", ld_incertezza)

// Esito
ls_esito = 'N'
ls_cod_primario = ldw_datawindow.getitemstring(ai_row, "cod_attrezzatura_primario")
ld_tolleranza = dw_attrezzature_1.getitemnumber(1, "tolleranza_stabilita")

select grado_precisione
into :ld_grado_precisione
from anag_attrezzature
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :ls_cod_primario;
	
// tm < Vm < tM
if (ld_grado_precisione - ld_tolleranza) <= ld_valore_medio and ld_valore_medio <= (ld_grado_precisione + ld_tolleranza) then
	
	// I <= 1/3 x t x 2
	if ld_incertezza <= (1 / 3) * ld_tolleranza * 2 then
		
		ls_esito = 'P'
		
	end if
	
end if

ldw_datawindow.setitem(ai_row, "esito", ls_esito)

end subroutine

public function integer wf_inserisci_aree (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean							lb_dati = false
string								ls_sql, ls_cod_area_aziendale, ls_des_area, ls_sql_livello, ls_errore, ls_sort
long								ll_risposta, ll_i, ll_livello, ll_tot
str_attrezzature_tree			lstr_record
treeviewitem					ltvi_campo
datastore						lds_data


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct anag_attrezzature.cod_area_aziendale, tab_aree_aziendali.des_area "+&
			"from anag_attrezzature "+&
			"left outer join tab_aree_aziendali on tab_aree_aziendali.cod_azienda=anag_attrezzature.cod_azienda and "+&
												"tab_aree_aziendali.cod_area_aziendale=anag_attrezzature.cod_area_aziendale "
ls_sql_livello = ""

wf_leggi_parent(fl_handle, ls_sql_livello)

ls_sql += " where anag_attrezzature.cod_azienda='"+s_cs_xx.cod_azienda+"' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

choose case dw_filtro.getitemstring(1, "flag_ordine")
case "C"
		ls_sort = " #1 asc"
	case "D"
		ls_sort = " #2 asc"
	case else
		ls_sort = ""
end choose

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_tot<0 then
	g_mb.error("Errore creazione datastore (wf_inserisci_aree): " + ls_errore)
	return -1
end if

lds_data.setsort(ls_sort)
lds_data.sort()

for ll_i=1 to ll_tot
 ls_cod_area_aziendale		= lds_data.getitemstring(ll_i, 1)
	ls_des_area					= lds_data.getitemstring(ll_i, 2)
	
	if isnull(ls_cod_area_aziendale) or ls_cod_area_aziendale="" then
		ltvi_campo.label = "<Area Aziendale Mancante>"
		
	elseif isnull(ls_des_area) or ls_des_area="" then
		ltvi_campo.label = ls_cod_area_aziendale
		ls_des_area = ""
	else 
		ltvi_campo.label = ls_cod_area_aziendale + " - " + ls_des_area
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_area_aziendale
	lstr_record.descrizione = ls_des_area
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "E"
	ltvi_campo.data = lstr_record
	
	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

next

destroy lds_data;
return 0
end function

public function integer wf_inserisci_categorie (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean					lb_dati = false
string						ls_sql, ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_sql_livello, ls_sort, ls_errore
long						ll_risposta, ll_i, ll_livello, ll_tot
str_attrezzature_tree	lstr_record
treeviewitem			ltvi_campo
datastore				lds_data


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct anag_attrezzature.cod_cat_attrezzature,"+&
							"tab_cat_attrezzature.des_cat_attrezzature "+&
			"from anag_attrezzature "+&
			"left outer join tab_cat_attrezzature on tab_cat_attrezzature.cod_azienda=anag_attrezzature.cod_azienda and "+&
													"tab_cat_attrezzature.cod_cat_attrezzature=anag_attrezzature.cod_cat_attrezzature "

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

ls_sql += "where anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "C"
		ls_sort = "#1 ASC"
	case "D"
		ls_sort = "#2 ASC"
	case else
		ls_sort = ""
end choose


ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_tot<0 then
	g_mb.error("Errore in creazione datastore (wf_inserisci_categorie): " + ls_errore)
	return -1
end if

lds_data.setsort(ls_sort)
lds_data.sort()

for ll_i=1 to ll_tot
  ls_cod_cat_attrezzatura		= lds_data.getitemstring(ll_i, 1)
	ls_des_cat_attrezzatura		= lds_data.getitemstring(ll_i, 2)
	
	if isnull(ls_cod_cat_attrezzatura) or ls_cod_cat_attrezzatura="" then
		ltvi_campo.label = "<Categoria Mancante>"
		
	elseif isnull(ls_des_cat_attrezzatura) then
		ltvi_campo.label = ls_cod_cat_attrezzatura
		
	else 
		ltvi_campo.label = ls_cod_cat_attrezzatura + " - " + ls_des_cat_attrezzatura
	end if
	
	lb_dati = true
	
	ltvi_campo.itemhandle = fl_handle
	lstr_record.codice = ls_cod_cat_attrezzatura
	lstr_record.descrizione = ls_des_cat_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "C"
	lstr_record.livello = ll_livello
	ltvi_campo.data = lstr_record

	ltvi_campo.pictureindex = 4
	ltvi_campo.selectedpictureindex = 4
	ltvi_campo.overlaypictureindex = 4
	ltvi_campo.children = true
	ltvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
next

destroy lds_data;
return 0
end function

public function integer wf_inserisci_divisioni (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean					lb_dati = false
string						ls_sql, ls_cod_divisione, ls_des_divisione, ls_sql_livello, ls_errore, ls_sort
long						ll_risposta, ll_i, ll_livello, ll_tot
str_attrezzature_tree lstr_record
datastore				lds_data

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tab_aree_aziendali.cod_divisione, anag_divisioni.des_divisione "+&
			"from anag_attrezzature "+&
			"left outer join tab_aree_aziendali on tab_aree_aziendali.cod_azienda=anag_attrezzature.cod_azienda and "+&
														"tab_aree_aziendali.cod_area_aziendale=anag_attrezzature.cod_area_aziendale "+&
			"left outer join anag_divisioni on anag_divisioni.cod_azienda=tab_aree_aziendali.cod_azienda and "+&
														"anag_divisioni.cod_divisione=tab_aree_aziendali.cod_divisione "

ls_sql_livello = ""
wf_leggi_parent(fl_handle, ls_sql_livello)

ls_sql += "where anag_attrezzature.cod_azienda='"+s_cs_xx.cod_azienda+"' "

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "C"
		ls_sort = "#1 ASC"
	case "D"
		ls_sort = "#2 ASC"
	case else
		ls_sort = ""
end choose

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot < 0 then
	g_mb.error("Errore in creazione datastore (wf_inserisci_divisioni): " + ls_errore)
	return -1
end if

lds_data.setsort(ls_sort)
lds_data.sort()

for ll_i=1 to ll_tot
	ls_cod_divisione = lds_data.getitemstring(ll_i, 1)
	ls_des_divisione = lds_data.getitemstring(ll_i, 2)
	
	if isnull(ls_cod_divisione) or ls_cod_divisione="" then
		tvi_campo.label = "<Divisione Mancante>"
	elseif isnull(ls_des_divisione) then
		tvi_campo.label = ls_cod_divisione
	else 
		tvi_campo.label = ls_cod_divisione + " - " + ls_des_divisione
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_divisione
	lstr_record.descrizione = ls_des_divisione
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "D"
	tvi_campo.data = lstr_record

	tvi_campo.pictureindex = 5
	tvi_campo.selectedpictureindex = 5
	tvi_campo.overlaypictureindex = 5
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	
next
destroy lds_data;

return 0
end function

public function integer wf_inserisci_reparti (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello reparti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean							lb_dati = false
string								ls_sql, ls_cod_reparto, ls_des_reparto, ls_sql_livello, ls_errore, ls_sort
long								ll_risposta, ll_i, ll_livello, ll_tot
str_attrezzature_tree			lstr_record
treeviewitem					ltvi_campo
datastore						lds_data


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct anag_attrezzature.cod_reparto, anag_reparti.des_reparto "+&
			"from anag_attrezzature "+&
			"left outer join anag_reparti on anag_reparti.cod_azienda=anag_attrezzature.cod_azienda and "+&
													"anag_reparti.cod_reparto=anag_attrezzature.cod_reparto "
	
ls_sql_livello = ""
wf_leggi_parent(fl_handle, ls_sql_livello)

ls_sql += "where anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "C"
		ls_sort = " #1 ASC"
	case "D"
		ls_sort = " #2 ASC"
	case else
		ls_sort = ""
end choose

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot < 0 then
	g_mb.error("Errore creazione datastore (wf_inserisci_reparti): " + ls_errore)
	return -1
end if

lds_data.setsort(ls_sort)
lds_data.sort()

for ll_i=1 to ll_tot
	ls_cod_reparto	= lds_data.getitemstring(ll_i, 1)
	ls_des_reparto	= lds_data.getitemstring(ll_i, 2)
  
	if isnull(ls_cod_reparto) or ls_cod_reparto="" then
		ltvi_campo.label = "<Reparto Mancante>"
	elseif isnull(ls_des_reparto) then
		ltvi_campo.label = ls_cod_reparto
	else 
		ltvi_campo.label = ls_cod_reparto + " - " + ls_des_reparto
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_reparto
	lstr_record.descrizione = ls_des_reparto
	lstr_record.tipo_livello = "R"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle

	ltvi_campo.data = lstr_record
	
	ltvi_campo.pictureindex = 6
	ltvi_campo.selectedpictureindex = 6
	ltvi_campo.overlaypictureindex = 6
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

next
destroy lds_data;

return 0
end function

public function integer wf_inserisci_attrezzature (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean 			lb_dati = false
string 			ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura, ls_errore, ls_sort, ls_sql_livello
long 				ll_risposta, ll_i, ll_num_righe, ll_livello, ll_tot
str_attrezzature_tree lstr_record
datastore			lds_data


ll_livello = il_livello_corrente + 1

ls_sql = "select cod_attrezzatura, descrizione from anag_attrezzature "

ls_sql_livello = ""
wf_leggi_parent(fl_handle, ls_sql_livello)

ls_sql += "where anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql +=ls_sql_livello
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "C"
		ls_sort = "#1 ASC"
	case "D"
		ls_sort += "#2 ASC"
	case else
		ls_sort=""
end choose


ll_tot=guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_tot<0 then
	g_mb.error("Errore in creazione datastore (wf_inserisci_attrezzature): " + ls_errore)
	return -1
end if

lds_data.setsort(ls_sort)
lds_data.sort()

for ll_i=1 to ll_tot
	ls_cod_attrezzatura = lds_data.getitemstring(ll_i, 1)
	ls_des_attrezzatura = lds_data.getitemstring(ll_i, 2)
	
	lb_dati = true
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""
	if len(ls_des_attrezzatura) > 30 then ls_des_attrezzatura = left(ls_des_attrezzatura, 30) + "..."

	lstr_record.codice = ls_cod_attrezzatura
	lstr_record.descrizione = ls_des_attrezzatura
	
	tvi_campo.label = ls_cod_attrezzatura + " " + ls_des_attrezzatura
	
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "A"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 2
	tvi_campo.overlaypictureindex = 2
	
	tvi_campo.children = false
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
next

destroy lds_data;

return 0
end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_attrezzature_1,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
					  "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((anag_reparti.flag_blocco <> 'S') or (anag_reparti.flag_blocco = 'S' and anag_reparti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_attrezzature_1,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "tab_aree_aziendali.cod_azienda = '" + s_cs_xx.cod_azienda + "' ")

f_PO_LoadDDDW_DW(dw_attrezzature_1,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
					  "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")

f_PO_LoadDDLB_DW(dw_attrezzature_2,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente", &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_attrezzature_1,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo", &
                 " (tab_centri_costo.cod_azienda = '" + s_cs_xx.cod_azienda + "')") 
					  			  
					 
// stefanop 19/03/2009: Liste per il filtro
f_PO_LoadDDDW_DW(dw_filtro,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
// -------------------------

// stefanop 15/03/2011
if ib_tarature_enable then
	f_PO_LoadDDDW_DW(dw_attrezzature_1,"intervallo_misura_um",sqlca,&
		"tab_misure","cod_misura","des_misura", &
		"tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"((tab_misure.flag_blocco <> 'S') or (tab_misure.flag_blocco = 'S' and tab_misure.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	
	f_PO_LoadDDDW_DW(dw_attrezzature_1,"tolleranza_stabilita_um",sqlca,&
		"tab_misure","cod_misura","des_misura", &
		"tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"((tab_misure.flag_blocco <> 'S') or (tab_misure.flag_blocco = 'S' and tab_misure.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		
	f_PO_LoadDDDW_DW(dw_attrezzature_1,"qualita_attrezzatura",sqlca,&
		"tab_qualita_attrezzature","cod_qualita_attrezzatura","des_qualita_attrezzatura", &
		"tab_qualita_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		
	f_PO_LoadDDDW_DW(dw_attrezzature_1,"grado_precisione_um",sqlca,&
		"tab_misure","cod_misura","des_misura", &
		"tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"((tab_misure.flag_blocco <> 'S') or (tab_misure.flag_blocco = 'S' and tab_misure.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
else
	f_PO_LoadDDDW_DW(dw_attrezzature_2,"grado_precisione_um",sqlca,&
		"tab_misure","cod_misura","des_misura", &
		"tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
		"((tab_misure.flag_blocco <> 'S') or (tab_misure.flag_blocco = 'S' and tab_misure.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end if
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ], l_vuoto[]

// stefanop 11/10/2010: ticket 2010/284
if f_db() = "SYBASE_ASE" then 
	dw_attrezzature_2.il_limit_campo_note = 1999
end if
// ----

// stefanop 15/03/2011
string ls_flag

select flag
into :ls_flag
from parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_parametro = 'ATE' and
	flag_parametro = 'F';
	
if sqlca.sqlcode = 0 and ls_flag = 'S' then
	ib_tarature_enable = true
end if


//controllo se sono in global service --------------------------
select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
if sqlca.sqlcode = 0 and ls_flag ="S" then ib_global_service = true

if ib_global_service then
	//fasi manutenzione
else
	//no fasi manutenzione
end if
//----------------------------------------------------------------


//Donato 06/09/2010 aggiunti tab per le colonne dinamiche
l_objects = l_vuoto
l_objects[1] = dw_col_din
dw_folder.fu_assigntab(3, "&Colonne Dinamiche", l_Objects[])
dw_col_din.uof_set_dw(dw_attrezzature_1, {"cod_attrezzatura"})

//-------------------------------------------------------------------------

l_objects = l_vuoto
l_objects[1] = dw_attrezzature_2
dw_folder.fu_AssignTab(2, "&Manutenzione", l_Objects[])

l_objects = l_vuoto
l_objects[1] = dw_attrezzature_1
l_objects[2] = cb_documento
l_objects[3] = cb_etichette
l_objects[4] = cb_etichette_bc
dw_folder.fu_AssignTab(1, "Dati &Anagrafici", l_Objects[])

dw_attrezzature_1.set_dw_key("cod_azienda")
dw_attrezzature_1.set_dw_key("cod_attrezzatura")

// stefanop: specifica VIGNONI
cb_report_tarature.visible = false
if ib_tarature_enable then
	iuo_tarature = create uo_tarature
	
	dw_attrezzature_1.dataobject = "d_attrezzature_3" // cambio DW con i campi per le tarature
	dw_attrezzature_2.dataobject = "d_attrezzature_4" // cambio DW con i campi per le tarature
	
	dw_tarature_periodiche.settransobject(sqlca)
	l_objects = l_vuoto
	l_objects[1] = dw_tarature_periodiche
	l_objects[2] = cb_report_tarature
	dw_folder.fu_AssignTab(4, "Tarature Per. Interne", l_Objects[])
	
	dw_tarature_incrociate.settransobject(sqlca)
	l_objects = l_vuoto
	l_objects[1] = dw_tarature_incrociate
	l_objects[2] = cb_report_tarature
	dw_folder.fu_AssignTab(5, "Tarature Incrociate", l_Objects[])
	// ----
	
	dw_tarature_periodiche.object.b_add.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
	dw_tarature_periodiche.object.b_delete.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"
	dw_tarature_periodiche.object.p_4.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
	dw_tarature_periodiche.object.p_2.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"
	dw_tarature_incrociate.object.b_add.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
	dw_tarature_incrociate.object.b_delete.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"
	dw_tarature_incrociate.object.p_4.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
	dw_tarature_incrociate.object.p_2.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"

	dw_folder.fu_FolderCreate(5,5)
else
	
	dw_tarature_periodiche.visible = false
	dw_tarature_incrociate.visible = false
	dw_folder.fu_FolderCreate(3, 4)
end if

dw_folder.fu_SelectTab(1)

dw_attrezzature_1.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_attrezzature_2.set_dw_options(sqlca,dw_attrezzature_1,c_sharedata+c_scrollparent,c_default)
iuo_dw_main = dw_attrezzature_1

// Filtro
dw_filtro.insertrow(0)

l_objects = l_vuoto
l_objects[1] = tv_1
dw_folder_tree.fu_assigntab(2, "Attrezzature", l_objects[])
l_objects[1] = dw_filtro
l_objects[2] = cb_cerca
l_objects[3] = cb_richiama_predefinito
l_objects[4] = cb_predefinito
dw_folder_tree.fu_assigntab(1, "Filtro", l_objects[])
dw_folder_tree.fu_foldercreate(2, 2)
dw_folder_tree.fu_selecttab(1)

// Treeview
tv_1.deletepictures()
//tv_1.PictureHeight = 16
//tv_1.PictureWidth = 16

// 1, 2 -- attrezzatura
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")

// 3 -- area
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

// 4 -- categoria
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")

// 5 -- divisioni
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_divisioni.png")

// 6 -- reparti
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")

dw_filtro.reset()
dw_filtro.insertrow(0)
end event

on w_attrezzature.create
int iCurrent
call super::create
this.cb_report_tarature=create cb_report_tarature
this.cb_cerca=create cb_cerca
this.cb_richiama_predefinito=create cb_richiama_predefinito
this.cb_predefinito=create cb_predefinito
this.sle_1=create sle_1
this.cb_etichette_bc=create cb_etichette_bc
this.cb_documento=create cb_documento
this.cb_etichette=create cb_etichette
this.dw_filtro=create dw_filtro
this.dw_folder_tree=create dw_folder_tree
this.dw_folder=create dw_folder
this.dw_attrezzature_2=create dw_attrezzature_2
this.dw_attrezzature_1=create dw_attrezzature_1
this.dw_tarature_periodiche=create dw_tarature_periodiche
this.dw_tarature_incrociate=create dw_tarature_incrociate
this.tv_1=create tv_1
this.dw_col_din=create dw_col_din
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_report_tarature
this.Control[iCurrent+2]=this.cb_cerca
this.Control[iCurrent+3]=this.cb_richiama_predefinito
this.Control[iCurrent+4]=this.cb_predefinito
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.cb_etichette_bc
this.Control[iCurrent+7]=this.cb_documento
this.Control[iCurrent+8]=this.cb_etichette
this.Control[iCurrent+9]=this.dw_filtro
this.Control[iCurrent+10]=this.dw_folder_tree
this.Control[iCurrent+11]=this.dw_folder
this.Control[iCurrent+12]=this.dw_attrezzature_2
this.Control[iCurrent+13]=this.dw_attrezzature_1
this.Control[iCurrent+14]=this.dw_tarature_periodiche
this.Control[iCurrent+15]=this.dw_tarature_incrociate
this.Control[iCurrent+16]=this.tv_1
this.Control[iCurrent+17]=this.dw_col_din
end on

on w_attrezzature.destroy
call super::destroy
destroy(this.cb_report_tarature)
destroy(this.cb_cerca)
destroy(this.cb_richiama_predefinito)
destroy(this.cb_predefinito)
destroy(this.sle_1)
destroy(this.cb_etichette_bc)
destroy(this.cb_documento)
destroy(this.cb_etichette)
destroy(this.dw_filtro)
destroy(this.dw_folder_tree)
destroy(this.dw_folder)
destroy(this.dw_attrezzature_2)
destroy(this.dw_attrezzature_1)
destroy(this.dw_tarature_periodiche)
destroy(this.dw_tarature_incrociate)
destroy(this.tv_1)
destroy(this.dw_col_din)
end on

event open;call super::open;s_cs_xx_parametri lstr_parametri

dw_filtro.postevent("ue_leggi_filtro")
lstr_parametri = Message.PowerObjectParm

if not isnull( lstr_parametri) then
	
	if isvalid( lstr_parametri) then
		
		if not isnull(lstr_parametri.parametro_s_1) and not isnull(lstr_parametri.parametro_s_2) then
			is_navigatore_tipo = lstr_parametri.parametro_s_1
			is_navigatore_valore = lstr_parametri.parametro_s_2
			is_navigatore_doc = lstr_parametri.parametro_s_3
			event post we_navigatore_manutenzioni()
		end if
		
	end if
	
end if
end event

type cb_report_tarature from commandbutton within w_attrezzature
integer x = 3794
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;//w_attrezzature_tarature_report
s_cs_xx_parametri lstr_parametri

lstr_parametri.parametro_s_1 = dw_attrezzature_1.getitemstring(1, "cod_attrezzatura")

if dw_folder.i_selectedtab = 4 then
	lstr_parametri.parametro_s_2 = TARATURA_PERIODICA
elseif dw_folder.i_selectedtab = 5 then
	lstr_parametri.parametro_s_2 = TARATURA_INCROCIATA
else
	return
end  if

window_open_parm(w_attrezzature_tarature_report, 0, lstr_parametri)
end event

type cb_cerca from commandbutton within w_attrezzature
integer x = 800
integer y = 2040
integer width = 343
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_flag_primarie, ls_flag_secondarie, ls_flag_ausiliarie, ls_flag_blocco

dw_filtro.accepttext()

ls_flag_primarie = dw_filtro.getitemstring(1, "flag_primarie")
ls_flag_secondarie = dw_filtro.getitemstring(1, "flag_secondarie")
ls_flag_ausiliarie = dw_filtro.getitemstring(1, "flag_ausiliarie")

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""

// cod attrezzatura da
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da")) then
	is_sql_filtro += " and cod_attrezzatura >= '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da") + "' "
end if

// cod attrezzatura a
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a")) then
	is_sql_filtro += " and cod_attrezzatura <= '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a") + "' "
end if

// cod area aziendale
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) then
	is_sql_filtro += " and cod_area_aziendale = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale") + "' "
end if

// cod divisione
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) and len(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) > 0 then
	is_sql_filtro += " and cod_area_aziendale IN (select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_divisione = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione") + "' ) "
end if

// cod reparto
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) then
	is_sql_filtro += " and cod_reparto = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto") + "' "
end if

// cod cat attrezzatura
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) then
	is_sql_filtro += " and cod_cat_attrezzature = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura") + "' "
end if

if ls_flag_primarie = "S" and ls_flag_secondarie = "S" and ls_flag_ausiliarie = "S" then
	
elseif ls_flag_primarie = "S" and ls_flag_secondarie = "T" and ls_flag_ausiliarie = "T" then
	is_sql_filtro += " AND ( flag_primario = 'S' ) "
elseif ls_flag_primarie = "S" and ls_flag_secondarie = "S" and ls_flag_ausiliarie = "N" then
	is_sql_filtro += " AND ( qualita_attrezzatura = 'P' or qualita_attrezzatura = 'S' ) "
elseif ls_flag_primarie = "S" and ls_flag_secondarie = "N" and ls_flag_ausiliarie = "S" then
	is_sql_filtro += " AND ( qualita_attrezzatura = 'P' or qualita_attrezzatura = 'A' ) "
elseif ls_flag_primarie = "N" and ls_flag_secondarie = "S" and ls_flag_ausiliarie = "S" then
	is_sql_filtro += " AND ( qualita_attrezzatura = 'S' or qualita_attrezzatura = 'A' ) "
elseif ls_flag_primarie = "N" and ls_flag_secondarie = "N" and ls_flag_ausiliarie = "N" then
	
elseif ls_flag_primarie = "N" and ls_flag_secondarie = "N" and ls_flag_ausiliarie = "S" then
	is_sql_filtro += " AND ( qualita_attrezzatura = 'A' ) "
elseif ls_flag_primarie = "N" and ls_flag_secondarie = "S" and ls_flag_ausiliarie = "N" then
	is_sql_filtro += " AND ( qualita_attrezzatura = 'S'  ) "
elseif ls_flag_primarie = "S" and ls_flag_secondarie = "N" and ls_flag_ausiliarie = "N" then	
	is_sql_filtro += " AND ( qualita_attrezzatura = 'P' ) "
end if

// blocco
ls_flag_blocco = dw_filtro.getitemstring(dw_filtro.getrow(), "flag_blocco")
if not isnull(ls_flag_blocco) and ls_flag_blocco <> "" then
	if ls_flag_blocco = "S" then
		is_sql_filtro += " AND (flag_blocco = 'S' and (data_blocco is null or data_blocco <= " + string(datetime(today(), now()), s_cs_xx.db_funzioni.formato_data) + ") ) "
	elseif ls_flag_blocco = "N" then
		is_sql_filtro += "AND (flag_blocco is null OR flag_blocco ='N') "
	end if
end if
// ------------------------------------------------------------

wf_cancella_treeview()

wf_imposta_treeview()

dw_folder_tree.fu_selecttab(2)

tv_1.setfocus()
end event

type cb_richiama_predefinito from commandbutton within w_attrezzature
integer x = 434
integer y = 2040
integer width = 361
integer height = 80
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()

//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
//if ib_fam then
//	dw_filtro.setitem(1,"cod_area_aziendale",is_area)
//end if
//----------------------------- 22/04/2004 Fine -----------------------------------

dw_filtro.postevent("ue_reset")
end event

type cb_predefinito from commandbutton within w_attrezzature
integer x = 46
integer y = 2040
integer width = 379
integer height = 80
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Predef."
end type

event clicked;dw_filtro.accepttext()
wf_memorizza_filtro()
end event

type sle_1 from singlelineedit within w_attrezzature
boolean visible = false
integer x = 2560
integer y = 340
integer width = 366
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 255
string text = "Manuten. ?"
boolean autohscroll = false
end type

type cb_etichette_bc from commandbutton within w_attrezzature
event clicked pbm_bnclicked
integer x = 2354
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etich. B.C."
end type

event clicked;window_open(w_etichette_attrezzature, -1)

end event

type cb_documento from commandbutton within w_attrezzature
event clicked pbm_bnclicked
integer x = 3131
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_attrezzature_1.accepttext()


window_open_parm(w_det_attrezzature, -1, dw_attrezzature_1)


end event

type cb_etichette from commandbutton within w_attrezzature
integer x = 2743
integer y = 2040
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichette"
end type

event clicked;string ls_cod_attrezzatura
long   ll_num_registrazione

ls_cod_attrezzatura = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(),"cod_attrezzatura")

select max(num_registrazione)
into   :ll_num_registrazione
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_attrezzatura;
if (sqlca.sqlcode <> 0) or (isnull(ll_num_registrazione)) or (ll_num_registrazione = 0) then
	g_mb.messagebox("OMNIA","Non vi sono manutenzioni programmate per questa attrezzatura")
end if

if dw_attrezzature_1.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_1 = ls_cod_attrezzatura
	s_cs_xx.parametri.parametro_d_1 = ll_num_registrazione
	window_open_parm(w_report_eti_attrezzatura, -1, dw_attrezzature_1)
end if
end event

type dw_filtro from u_dw_search within w_attrezzature
event ue_key pbm_dwnkey
event ue_cerca_attrezzatura ( string as_column )
event ue_reset ( )
event ue_clear ( )
event ue_leggi_filtro ( )
integer x = 46
integer y = 120
integer width = 1097
integer height = 1640
integer taborder = 110
string dataobject = "d_attrezzature_filtro"
boolean border = false
end type

event ue_key;if key = keyenter! then
	accepttext()
	cb_cerca.triggerevent("clicked")
	postevent("ue_reset")
end if

end event

event ue_cerca_attrezzatura(string as_column);guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,as_column)
end event

event ue_reset();dw_filtro.resetupdate()
end event

event ue_clear();dw_filtro.reset()
end event

event ue_leggi_filtro();wf_leggi_filtro()
end event

event itemchanged;call super::itemchanged;string ls_null
setnull(ls_null)

if isnull(dwo) or not isvalid(dwo) then
	return
end if

choose case dwo.name
	case "cod_attrezzatura_da"
		if data <> "" or not isnull(data) then								  
			setitem(row, "cod_attrezzatura_a", data)
		end if
		
	case "flag_tipo_livello_1"
		setitem(row, "flag_tipo_livello_2", "N")
		setitem(row, "flag_tipo_livello_3", "N")
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_2", 150)
		end if
		
	case "flag_tipo_livello_2"
		setitem(row, "flag_tipo_livello_3", "N")
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_3", 160)
		end if
		
	case "flag_tipo_livello_3"
		setitem(row, "flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_4", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_4", 170)
		end if
		
end choose

postevent("ue_reset")

end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_attrezzatura_da"
		event ue_cerca_attrezzatura("cod_attrezzatura_da")

	case "b_attrezzatura_a"
		event ue_cerca_attrezzatura("cod_attrezzatura_a")
		
end choose
end event

type dw_folder_tree from u_folder within w_attrezzature
integer x = 23
integer y = 20
integer width = 1143
integer height = 2120
integer taborder = 10
end type

type dw_folder from u_folder within w_attrezzature
integer x = 1189
integer y = 20
integer width = 3200
integer height = 2120
integer taborder = 50
end type

type dw_attrezzature_2 from uo_cs_xx_dw within w_attrezzature
integer x = 1234
integer y = 180
integer width = 2834
integer height = 1680
integer taborder = 120
string dataobject = "d_attrezzature_2"
boolean border = false
end type

type dw_attrezzature_1 from uo_cs_xx_dw within w_attrezzature
integer x = 1211
integer y = 180
integer width = 2857
integer height = 1860
integer taborder = 130
string dataobject = "d_attrezzature_1"
boolean border = false
end type

event itemchanged;/*
 * TOLTO ANCHESTOR SCRIPT,
 * altrimenti da sempre il messaggio di cod_attrezzatura obbligatoria
 */
 
 
choose case i_colname
case "cod_prodotto"
	f_PO_LoadDDDW_DW(dw_attrezzature_1,"cod_versione",sqlca,&
						  "distinta_padri","cod_versione","des_versione", &
						  " (distinta_padri.cod_azienda = '" + s_cs_xx.cod_azienda + "') and cod_prodotto = '" + i_coltext + "'") 	
end choose	
	
end event

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

//--------------------------------- Modifica Nicola --------------------------------------------
long ll_count, ld_num_protocollo
string ls_cod_attrezzatura

ls_cod_attrezzatura = dw_attrezzature_1.getitemstring(dw_attrezzature_1.getrow(), "cod_attrezzatura")

select count(num_protocollo)
  into :ll_count
  from det_attrezzature
 where cod_azienda = :s_cs_xx.cod_azienda
   and cod_attrezzatura = :ls_cod_attrezzatura;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Attrezzature" + sqlca.sqlerrtext)
	return
end if	

if ll_count > 0 then
	if g_mb.messagebox("Omnia", "Attenzione esistono dei documenti allegati all'attrezzatura:~r~n si vuole proseguire con la cancellazione?", Exclamation!, YesNo!, 2) = 2 then
		return				
	else		
		declare cu_det_attrezzature cursor for
			select num_protocollo
			  from det_attrezzature
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_attrezzatura = :ls_cod_attrezzatura;
		
		open cu_det_attrezzature;
		
		do while 1 = 1
			fetch cu_det_attrezzature into :ld_num_protocollo;
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Dettaglio Attrezzature" + sqlca.sqlerrtext)
				return
			end if	
			
			if sqlca.sqlcode = 0 then
				
				delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if		  
		
				delete from det_attrezzature  //cancellazione dettaglio attrezzature
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_attrezzatura = :ls_cod_attrezzatura;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione dalla tabella DET_ATTREZZATURE:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if	  		  
				  
				delete from tab_protocolli  //cancellazione protocollo
				where cod_azienda = :s_cs_xx.cod_azienda
				  and num_protocollo = :ld_num_protocollo;
				  
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
					return
				end if
			end if
		loop	
		close cu_det_attrezzature;
	end if	
end if
//---------------------------------- Fine Modifica ---------------------------------------------	

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse

triggerevent("pcd_save")
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_ricerca_prodotto"
		dw_attrezzature_1.change_dw_current()
		guo_ricerca.uof_ricerca_prodotto(dw_attrezzature_1,"cod_prodotto")
		
end choose
end event

event pcd_new;call super::pcd_new;long row

row = getrow()

if i_extendmode then
   this.setitem(this.getrow(), "unita_tempo", "E")
   this.setitem(this.getrow(), "periodicita_revisione", 1)
   this.setitem(this.getrow(), "utilizzo_scadenza", "S")
end if

// carico dati automatici in base al nodo
if not isnull(is_categoria) and is_categoria <> "" then	setitem(row, "cod_cat_attrezzature", is_categoria)
if not isnull(is_reparto) and is_reparto <> "" then	setitem(row, "cod_reparto", is_reparto)
if not isnull(is_area) and is_area <> "" then	setitem(row, "cod_area_aziendale", is_area)



// blocco pulsanti
wf_stato_pulsanti(false)

end event

event updateend;call super::updateend;wf_stato_pulsanti(true)


if dw_col_din.update() = 1 then
	commit;
else
	rollback;
end if

if rowsinserted > 0 then
	wf_inserisci_singola_attrezzatura(getrow())
end if

if rowsdeleted > 0 then
	wf_cancella_attrezzatura()
end if

wf_abilita_tab_tarature(getrow())
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura)

if ll_errore < 0 then
	dw_col_din.reset()
   pcca.error = c_fatal
elseif ll_errore = 0 then
	dw_col_din.reset()
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_attrezzatura
long  l_Idx

for l_Idx = 1 to RowCount()
	if IsNull(GetItemstring(l_Idx, "cod_azienda")) then
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	// nel caso il campo sia vuoto creo un codice automatico
	ls_cod_attrezzatura = getitemstring(getrow(), "cod_attrezzatura")
	
	if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura = "" then
		setitem(getrow(), "cod_attrezzatura", wf_prog_alphanumerico())
	end if
	// ----
	
NEXT

end event

event pcd_modify;call super::pcd_modify;//  blocco pulsanti
wf_stato_pulsanti(false)
end event

event pcd_view;call super::pcd_view;wf_stato_pulsanti(true)
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_attrezzatura
long ll_row

ll_row = this.getrow()

if ll_row >0 then

	ls_cod_attrezzatura = this.getitemstring(ll_row, "cod_attrezzatura")
	
	if ls_cod_attrezzatura <> "" then
		dw_col_din.retrieve()
	end if
	
	// abilito o disabilito i tab delle tarature
	wf_abilita_tab_tarature(ll_row)
else
//	dw_attrezzature_col_dinamiche.retrieve("-codice inesistente-", "-codice inesistente-")
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	
	dw_col_din.uof_Delete()
	
end if
end event

type dw_tarature_periodiche from uo_cs_xx_dw within w_attrezzature
integer x = 1234
integer y = 180
integer width = 3109
integer height = 1840
integer taborder = 130
boolean bringtotop = true
string dataobject = "d_attrezzature_tarature_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_row

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, TARATURA_PERIODICA)

if ll_errore < 0 then
   pcca.error = c_fatal
else
	
	for ll_row = 1 to ll_errore
		//wf_calcola_taratura(ll_row, TARATURA_PERIODICA)
		iuo_tarature.wf_calcola_taratura(dw_tarature_periodiche, ll_row)
	next
end if
end event

event pcd_setkey;call super::pcd_setkey;long  l_Idx

for l_Idx = 1 to RowCount()
	if IsNull(GetItemstring(l_Idx, "cod_azienda")) then
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
NEXT

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_add"
		wf_visualizza_taratura(row, TARATURA_PERIODICA, true)
		
	case "b_delete"
		wf_cancella_taratura(row, TARATURA_PERIODICA)
		
end choose
end event

event doubleclicked;call super::doubleclicked;if row < 1 then return

wf_visualizza_taratura(row, TARATURA_PERIODICA, false)
end event

type dw_tarature_incrociate from uo_cs_xx_dw within w_attrezzature
integer x = 1234
integer y = 180
integer width = 3109
integer height = 1840
integer taborder = 130
boolean bringtotop = true
string dataobject = "d_attrezzature_tarature_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_row

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, TARATURA_INCROCIATA)

if ll_errore < 0 then
   pcca.error = c_fatal
else
	
	for ll_row = 1 to ll_errore
		iuo_tarature.wf_calcola_taratura(dw_tarature_incrociate, ll_row)
		//wf_calcola_taratura(ll_row, TARATURA_INCROCIATA)
	next
end if
end event

event pcd_setkey;call super::pcd_setkey;long  l_Idx

for l_Idx = 1 to RowCount()
	if IsNull(GetItemstring(l_Idx, "cod_azienda")) then
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
NEXT

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_add"
		wf_visualizza_taratura(row, TARATURA_INCROCIATA, true)
		
	case "b_delete"
		wf_cancella_taratura(row, TARATURA_INCROCIATA)
		
end choose
end event

event doubleclicked;call super::doubleclicked;if row < 1 then return

wf_visualizza_taratura(row, TARATURA_INCROCIATA, false)
end event

type tv_1 from treeview within w_attrezzature
integer x = 46
integer y = 120
integer width = 1097
integer height = 2000
integer taborder = 110
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"","","","","",""}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
str_attrezzature_tree lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 4 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "D"
			// caricamento divisioni
			if wf_inserisci_divisioni(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento reparti
			if wf_inserisci_reparti(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento categorie
			if wf_inserisci_categorie(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "E" 
			// caricamento aree
			if wf_inserisci_aree(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A", "N"
			// caricamento attrezzature
			if wf_inserisci_attrezzature(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_attrezzature(handle) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

commit;

// Dati automatici
choose case lws_record.tipo_livello 
	case "D"
		// divisione
		is_divisione = lws_record.codice
		
	case "R"
		// reparti
		is_reparto = lws_record.codice
		
	case "C"
		// categorie
		is_categoria = lws_record.codice
		
	case "E" 
		// area
		is_area = lws_record.codice
end choose
end event

event selectionchanged;treeviewitem ltv_item
str_attrezzature_tree lws_record

if ib_tree_delete then return 0

if isnull(newhandle) or newhandle <= 0 then
	return 0
end if

il_ultimo_handle = newhandle

getitem(newhandle,ltv_item)
lws_record = ltv_item.data

choose case lws_record.tipo_livello 
	case "D"
		// divisione
		is_divisione = lws_record.codice
		
	case "R"
		// reparti
		is_reparto = lws_record.codice
		
	case "C"
		// categorie
		is_categoria = lws_record.codice
		
	case "E" 
		// area
		is_area = lws_record.codice
	
	case "A"
		// attrezzatura
		is_cod_attrezzatura = lws_record.codice
		dw_attrezzature_1.change_dw_current()
		parent.postevent("pc_retrieve")
		
end choose

end event

event rightclicked;treeviewitem ltv_item
str_attrezzature_tree lws_record

if handle < 1 then return 

getitem(handle,ltv_item)
lws_record = ltv_item.data
is_cod_attrezzatura = lws_record.codice

// non sono im modifica o nuovo
if not dw_attrezzature_1.ib_stato_nuovo and not dw_attrezzature_1.ib_stato_modifica then 
	
	m_attrezzature_menu lm_menu
	lm_menu = create m_attrezzature_menu
		
	// attrezzatura
	if lws_record.tipo_livello = "A" then
		lm_menu.m_refresh.visible = false
	else
		lm_menu.m_note.visible = false
		lm_menu.m_0.visible = false
		lm_menu.m_contr.visible = false
		lm_menu.m_modcodice.visible = false
		lm_menu.m_duplica.visible = false
		lm_menu.m_refresh.visible = true
	end if
	
	pcca.window_current  = parent
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	destroy lm_menu

end if
end event

event selectionchanging;treeviewitem ltv_item
str_attrezzature_tree lws_record

if ib_tree_delete then return

if isnull(oldhandle) or oldhandle <= 0 then
	return 0
end if


getitem(oldhandle,ltv_item)
lws_record = ltv_item.data

choose case lws_record.tipo_livello 	
	case "A"
		// attrezzatura
		dw_col_din.accepttext()
		dw_col_din.uof_verify_changes( )
		
end choose

end event

type dw_col_din from uo_colonne_dinamiche_dw within w_attrezzature
integer x = 1234
integer y = 180
integer width = 3109
integer height = 1840
integer taborder = 130
boolean bringtotop = true
end type

