﻿$PBExportHeader$w_attrezzature_note.srw
$PBExportComments$Finestra Istruzioni Operative Attrezzature
forward
global type w_attrezzature_note from w_cs_xx_principale
end type
type dw_attrezzature_note_lista from uo_cs_xx_dw within w_attrezzature_note
end type
type dw_attrezzature_note_det from uo_cs_xx_dw within w_attrezzature_note
end type
end forward

global type w_attrezzature_note from w_cs_xx_principale
integer width = 2373
integer height = 1288
string title = "Note Attrezzatura"
dw_attrezzature_note_lista dw_attrezzature_note_lista
dw_attrezzature_note_det dw_attrezzature_note_det
end type
global w_attrezzature_note w_attrezzature_note

type variables
boolean ib_in_new
end variables

on w_attrezzature_note.create
int iCurrent
call super::create
this.dw_attrezzature_note_lista=create dw_attrezzature_note_lista
this.dw_attrezzature_note_det=create dw_attrezzature_note_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_attrezzature_note_lista
this.Control[iCurrent+2]=this.dw_attrezzature_note_det
end on

on w_attrezzature_note.destroy
call super::destroy
destroy(this.dw_attrezzature_note_lista)
destroy(this.dw_attrezzature_note_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_attrezzature_note_lista.set_dw_key("cod_azienda")
dw_attrezzature_note_lista.set_dw_key("cod_attrezzatura")
dw_attrezzature_note_lista.set_dw_key("cod_nota")
dw_attrezzature_note_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_attrezzature_note_det.set_dw_options(sqlca,dw_attrezzature_note_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_attrezzature_note_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_attrezzature_note_det,"cod_nota",sqlca,"tab_note_modelli","cod_nota","des_nota", &
					  "")
end event

type dw_attrezzature_note_lista from uo_cs_xx_dw within w_attrezzature_note
integer x = 23
integer y = 20
integer width = 2286
integer height = 500
integer taborder = 10
string dataobject = "d_attrezzature_note_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;string	ls_cod_attrezzatura

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_attrezzatura")

setitem(getrow(),"cod_attrezzatura", ls_cod_attrezzatura)


ib_in_new = true
end event

event pcd_retrieve;call super::pcd_retrieve;string	ls_cod_attrezzatura
long	l_error

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_attrezzatura")

l_Error = Retrieve(s_cs_xx.cod_azienda, ls_cod_attrezzatura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;long  l_Idx
string	ls_cod_attrezzatura

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_attrezzatura")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_attrezzatura")) THEN
      SetItem(l_Idx, "cod_attrezzatura", ls_cod_attrezzatura)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;ib_in_new = false
end event

event pcd_save;call super::pcd_save;ib_in_new = false
end event

event pcd_view;call super::pcd_view;ib_in_new = false
end event

type dw_attrezzature_note_det from uo_cs_xx_dw within w_attrezzature_note
integer x = 23
integer y = 540
integer width = 2286
integer height = 620
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_attrezzature_note_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_note, ls_des_nota
long	ll_ordinamento

choose case i_colname
		
	case "cod_nota"
		
		select note,
				ordinamento	,
				des_nota
		into	:ls_note,
				:ll_ordinamento,
				:ls_des_nota
		from	tab_note_modelli
		where cod_nota = :i_coltext;
		
		setitem( row, "note", ls_note)
		setitem( row, "ordinamento", ll_ordinamento)
		setitem( row, "des_nota", ls_des_nota)
		
end choose
end event

