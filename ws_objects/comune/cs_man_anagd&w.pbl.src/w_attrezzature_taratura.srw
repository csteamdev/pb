﻿$PBExportHeader$w_attrezzature_taratura.srw
forward
global type w_attrezzature_taratura from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_attrezzature_taratura
end type
type cb_1 from commandbutton within w_attrezzature_taratura
end type
type dw_1 from uo_cs_xx_dw within w_attrezzature_taratura
end type
end forward

global type w_attrezzature_taratura from w_cs_xx_risposta
integer width = 2711
integer height = 1820
string title = "Nuova Taratura"
boolean resizable = false
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
end type
global w_attrezzature_taratura w_attrezzature_taratura

type variables
private:
	s_cs_xx_parametri istr_parametri
	uo_tarature iuo_tarature
	string is_tipo_taratura, is_cod_attrezzatura
	long il_prog_taratura
	
	long il_enable_bg = 16777215
	long il_disable_bg = 12632256
	string is_need_reload = 'F'
end variables

forward prototypes
public subroutine wf_dettagli (integer ai_row, string as_cod_attrezzatura)
public subroutine wf_abilita_tarature ()
public subroutine wf_colora_colonna (string as_nome_colonna)
public subroutine wf_colora_colonna (string as_nome_colonna, string as_data)
end prototypes

public subroutine wf_dettagli (integer ai_row, string as_cod_attrezzatura);/*
 * Carico dettagli attrezzatura primaria
 */
 
string ls_num_matricola
double ld_grado_precisione, ld_tolleranza

setnull(ls_num_matricola)
setnull(ld_grado_precisione)
setnull(ld_tolleranza)

if not isnull(as_cod_attrezzatura) and as_cod_attrezzatura <> "" then

	select num_matricola, grado_precisione, tolleranza_stabilita
	into :ls_num_matricola, :ld_grado_precisione, :ld_tolleranza
	from anag_attrezzature
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :as_cod_attrezzatura;
		
	if sqlca.sqlcode <> 0 then
		setnull(ls_num_matricola)
		setnull(ld_grado_precisione)
		setnull(ld_tolleranza)
	end if
	
end if

dw_1.setitem(ai_row, "num_matricola", ls_num_matricola)
dw_1.setitem(ai_row, "gradi_precisione", ld_grado_precisione)
dw_1.setitem(ai_row, "tolleranza_stabilita", ld_tolleranza)
end subroutine

public subroutine wf_abilita_tarature ();/**
 * Controlla se è possibile abilitare la taratura
 **/
 
string ls_col_name, ls_abilitata
int li_columns, li_i
 
li_columns = integer(dw_1.Describe("DataWindow.Column.Count"))

for li_i = 1 to li_columns
	
	ls_col_name = dw_1.Describe("#" + string(li_i) + ".Name")
	
	if right(ls_col_name, 9) <> "abilitata" then continue
	
	wf_colora_colonna(ls_col_name)
	
next
end subroutine

public subroutine wf_colora_colonna (string as_nome_colonna);wf_colora_colonna(as_nome_colonna,  dw_1.getitemstring(1, as_nome_colonna) )
end subroutine

public subroutine wf_colora_colonna (string as_nome_colonna, string as_data);string ls_mod_color, ls_mod_sequence, ls_colonna
double ld_null

setnull(ld_null)
ls_colonna = left(string(as_nome_colonna), len(string(as_nome_colonna)) - 9) + "valore" 

if as_data = 'S' then
	ls_mod_color = ls_colonna + '.Background.Color="' + string(il_enable_bg) + '"'
	ls_mod_sequence = ls_colonna + '.TabSequence=' + dw_1.describe(as_nome_colonna + ".TabSequence")
else
	ls_mod_color = ls_colonna + '.Background.Color="' + string(il_disable_bg) + '"'
	ls_mod_sequence = ls_colonna + '.TabSequence=0'
	
	dw_1.setitem(1, ls_colonna, ld_null)
end if

dw_1.modify(ls_mod_color)
dw_1.modify(ls_mod_sequence)
end subroutine

on w_attrezzature_taratura.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_1
end on

on w_attrezzature_taratura.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;istr_parametri = message.PowerObjectParm

is_cod_attrezzatura = istr_parametri.parametro_s_1
is_tipo_taratura = istr_parametri.parametro_s_2
il_prog_taratura = istr_parametri.parametro_ul_1

iuo_tarature = create uo_tarature

set_w_options(c_closenosave + c_noresizewin)

if isnull(il_prog_taratura) or il_prog_taratura < 1 then
	dw_1.set_dw_options(sqlca, pcca.null_object, c_noretrieveonopen, c_default)
	dw_1.postevent("pcd_new")
	
	title = "Nuova Taratura"

else
	dw_1.set_dw_options(sqlca, pcca.null_object, c_retrieveonopen, c_default)
	
	title = "Modifica Taratura"
end if

return 1
end event

event closequery;call super::closequery;istr_parametri.parametro_s_1 = is_need_reload
message.powerobjectparm = istr_parametri
destroy iuo_tarature
end event

type cb_2 from commandbutton within w_attrezzature_taratura
integer x = 1829
integer y = 1620
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_1 from commandbutton within w_attrezzature_taratura
integer x = 2263
integer y = 1620
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;dw_1.triggerevent("pcd_save")
end event

type dw_1 from uo_cs_xx_dw within w_attrezzature_taratura
event ue_close ( )
integer x = 23
integer y = 20
integer width = 2651
integer height = 1580
integer taborder = 10
string dataobject = "d_attrezzature_tarature_det"
end type

event pcd_new;call super::pcd_new;string ls_cod_mansionario = ""
long ll

if s_cs_xx.cod_utente <> "CS_SYSTEM" then 
	
	// TODO: protebbe dare errore in quanto l'utente può essere assegnato a più mansionari
	select cod_resp_divisione
	into :ls_cod_mansionario
	from mansionari
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente;
		
end if
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero del manisonario collegato.~r~n" + sqlca.sqlerrtext)

elseif sqlca.sqlcode = 100 then
	g_mb.error("Attenzione: l'utente non ha nessun mansionario collegato")

elseif sqlca.sqlcode = 0 and not isnull(ls_cod_mansionario) and ls_cod_mansionario <> "" then
	setitem(1, "cod_resp_divisione", ls_cod_mansionario)

end if


setitem(1, "data_taratura", today())

// forzo colorazione colonne tarature
wf_abilita_tarature()

end event

event clicked;call super::clicked;choose case dwo.name
	case "b_attrezzature"
		guo_ricerca.uof_ricerca_attrezzatura(dw_1,"cod_attrezzatura_primario")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_colonna

if string(dwo.name) = "cod_attrezzatura_primario" then
	wf_dettagli(row, data)
	
elseif len(string(dwo.name)) > 10 then
	
	// controllo che sia un campo taratura_X_abilita
	if right(string(dwo.name), 9) = "abilitata" then
		
		wf_colora_colonna(string(dwo.name), data)
		
	end if
	
end if

iuo_tarature.wf_calcola_taratura(dw_1, row)
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_azienda, ls_cod_attrezzatura, ls_tipo_taratura
long ll_prog_taratura

ls_cod_azienda = getitemstring(1, "cod_azienda")
if isnull(ls_cod_azienda) or ls_cod_azienda = "" then
	setitem(1, "cod_azienda", s_cs_xx.cod_azienda)
end if

ls_cod_attrezzatura = getitemstring(1, "cod_attrezzatura")
if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura = "" then
	setitem(1, "cod_attrezzatura", is_cod_attrezzatura)
end if

ls_tipo_taratura = getitemstring(1, "flag_tipo_taratura")
if isnull(ls_tipo_taratura) or ls_tipo_taratura = "" then
	setitem(1, "flag_tipo_taratura", is_tipo_taratura)
end if

ll_prog_taratura = getitemnumber(1, "prog_taratura")
if isnull(ll_prog_taratura) or ll_prog_taratura < 1 then
	
	select max(prog_taratura) + 1
	into :ll_prog_taratura
	from tab_attrezzature_tarature
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :is_cod_attrezzatura;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il calcolo del progressivo.~r~n" + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 or isnull(ll_prog_taratura) or ll_prog_taratura < 1 then
		ll_prog_taratura = 1
	end if
	
	setitem(1, "prog_taratura", ll_prog_taratura)
end if

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, il_prog_taratura)

if ll_errore < 0 then
   pcca.error = c_fatal
else
	postevent("pcd_modify")
end if
end event

event updateend;call super::updateend;if rowsinserted > 0 or rowsupdated > 0 then
	
	is_need_reload = 'T'
	parent.postevent("pc_close")
	
end if
end event

event pcd_modify;call super::pcd_modify;// forzo colorazione colonne tarature
wf_abilita_tarature()

wf_dettagli(1, getitemstring(1, "cod_attrezzatura_primario"))
iuo_tarature.wf_calcola_taratura(dw_1, 1)
end event

event pcd_validaterow;call super::pcd_validaterow;datetime ldt_data_taratura

ldt_data_taratura = getitemdatetime(1, "data_taratura")

if isnull(ldt_data_taratura) then
	g_mb.messagebox("Attenzione!","Impostare una data taratura valida")
	pcca.error = c_fatal
	return
end if
end event

