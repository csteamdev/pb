﻿$PBExportHeader$w_attrezzature_tarature_report.srw
$PBExportComments$Finestra Gestione Parametri
forward
global type w_attrezzature_tarature_report from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_attrezzature_tarature_report
end type
type dw_lista from uo_cs_xx_dw within w_attrezzature_tarature_report
end type
end forward

global type w_attrezzature_tarature_report from w_cs_xx_principale
integer width = 4123
integer height = 2528
string title = "Report Attrezzature Tarature"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
boolean center = true
cb_1 cb_1
dw_lista dw_lista
end type
global w_attrezzature_tarature_report w_attrezzature_tarature_report

type variables
private:
	string is_cod_attrezzatura, is_tipo_tarature
	uo_tarature iuo_tarature
end variables

forward prototypes
public subroutine wf_calcola_tarature ()
end prototypes

public subroutine wf_calcola_tarature ();/**

**/

string ls_col_abilitata, ls_col_valore, ls_esito, ls_cod_primario, ls_matricola_primario
int li_columns, li_num_tarature, li_i
long ll_rowcount, ll_row
dec{4} ld_v_min, ld_v_max, ld_valore_taratura, ld_totale_tarature, ld_valore_medio, ld_fattore, ld_incertezza, ld_tolleranza, &
		ld_grado_precisione
uo_tarature luo_tarature

luo_tarature = create uo_tarature
ll_rowcount = dw_lista.rowcount()

for ll_row = 1 to ll_rowcount
	
	luo_tarature.wf_calcola_taratura(dw_lista, ll_row)
	
next

dw_lista.resetupdate()
destroy luo_tarature
end subroutine

on w_attrezzature_tarature_report.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_lista
end on

on w_attrezzature_tarature_report.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path_logo
s_cs_xx_parametri lstr_parametri

lstr_parametri = message.powerobjectparm
setnull(message.powerobjectparm)

iuo_tarature = create uo_tarature

is_cod_attrezzatura = lstr_parametri.parametro_s_1
is_tipo_tarature = lstr_parametri.parametro_s_2

set_w_options(c_noresizewin)

dw_lista.ib_dw_report = true
dw_lista.set_dw_options(sqlca, pcca.null_object, c_nonew + c_nomodify + c_nodelete + c_noenablenewonopen + &
                                 c_noenablemodifyonopen + c_scrollparent + c_disablecc, c_noresizedw + c_nohighlightselected + &
                                 c_nocursorrowfocusrect + c_nocursorrowpointer)

dw_lista.set_dw_key("cod_azienda")

dw_lista.object.datawindow.print.preview = 'Yes'
dw_lista.object.datawindow.print.preview.rulers = 'Yes'
dw_lista.object.datawindow.Print.DocumentName = "Report Tarature Attrezzatura " + is_cod_attrezzatura

// LOGO
select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'S' and &
		 parametri_azienda.cod_parametro = 'LO1';
		 
dw_lista.modify("intestazione.filename='" + s_cs_xx.volume + ls_path_logo + "'")

end event

type cb_1 from commandbutton within w_attrezzature_tarature_report
integer x = 46
integer y = 20
integer width = 389
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_lista.triggerevent("pcd_print")
end event

type dw_lista from uo_cs_xx_dw within w_attrezzature_tarature_report
integer x = 23
integer y = 140
integer width = 4069
integer height = 2280
integer taborder = 10
string dataobject = "d_attrezzature_tarature_report"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;if retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, is_tipo_tarature) < 0 then
   pcca.error = c_fatal
else
	wf_calcola_tarature()
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

