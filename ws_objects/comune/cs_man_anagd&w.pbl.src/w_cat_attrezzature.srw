﻿$PBExportHeader$w_cat_attrezzature.srw
$PBExportComments$Anagrafica Categorie Attrezzature
forward
global type w_cat_attrezzature from w_cs_xx_principale
end type
type dw_cat_attrezzature_lista from uo_cs_xx_dw within w_cat_attrezzature
end type
type dw_cat_attrezzature_1 from uo_cs_xx_dw within w_cat_attrezzature
end type
end forward

global type w_cat_attrezzature from w_cs_xx_principale
integer width = 1751
integer height = 1644
string title = "Tabella Categorie Attrezzature"
dw_cat_attrezzature_lista dw_cat_attrezzature_lista
dw_cat_attrezzature_1 dw_cat_attrezzature_1
end type
global w_cat_attrezzature w_cat_attrezzature

event pc_setwindow;call super::pc_setwindow;dw_cat_attrezzature_lista.set_dw_key("cod_azienda")
dw_cat_attrezzature_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_cat_attrezzature_1.set_dw_options(sqlca, dw_cat_attrezzature_lista, c_sharedata + c_scrollparent, c_default)

iuo_dw_main = dw_cat_attrezzature_lista
end event

on w_cat_attrezzature.create
int iCurrent
call super::create
this.dw_cat_attrezzature_lista=create dw_cat_attrezzature_lista
this.dw_cat_attrezzature_1=create dw_cat_attrezzature_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cat_attrezzature_lista
this.Control[iCurrent+2]=this.dw_cat_attrezzature_1
end on

on w_cat_attrezzature.destroy
call super::destroy
destroy(this.dw_cat_attrezzature_lista)
destroy(this.dw_cat_attrezzature_1)
end on

type dw_cat_attrezzature_lista from uo_cs_xx_dw within w_cat_attrezzature
integer x = 23
integer y = 20
integer width = 1669
integer height = 500
integer taborder = 10
string dataobject = "d_cat_attrezzature_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_cat_attrezzature"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_cat_attrezzature")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_cat_attrezzature")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

type dw_cat_attrezzature_1 from uo_cs_xx_dw within w_cat_attrezzature
integer x = 23
integer y = 540
integer width = 1669
integer height = 980
integer taborder = 20
string dataobject = "d_cat_attrezzature_1"
borderstyle borderstyle = styleraised!
end type

