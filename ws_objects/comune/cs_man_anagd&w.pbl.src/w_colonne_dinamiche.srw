﻿$PBExportHeader$w_colonne_dinamiche.srw
forward
global type w_colonne_dinamiche from w_cs_xx_principale
end type
type cb_valori from commandbutton within w_colonne_dinamiche
end type
type dw_chiavi from uo_cs_xx_dw within w_colonne_dinamiche
end type
end forward

global type w_colonne_dinamiche from w_cs_xx_principale
integer width = 3026
integer height = 1256
string title = "Gestioni Chiavi"
cb_valori cb_valori
dw_chiavi dw_chiavi
end type
global w_colonne_dinamiche w_colonne_dinamiche

on w_colonne_dinamiche.create
int iCurrent
call super::create
this.cb_valori=create cb_valori
this.dw_chiavi=create dw_chiavi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_valori
this.Control[iCurrent+2]=this.dw_chiavi
end on

on w_colonne_dinamiche.destroy
call super::destroy
destroy(this.cb_valori)
destroy(this.dw_chiavi)
end on

event pc_setwindow;call super::pc_setwindow;dw_chiavi.set_dw_key("cod_azienda")
dw_chiavi.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

dw_chiavi.object.formato.protect = 0

end event

event resize;cb_valori.move(newwidth - 40 - cb_valori.width, newheight - 40 - cb_valori.height)
dw_chiavi.move(20,20)
dw_chiavi.resize(newwidth - 40, cb_valori.y - 60 )
end event

type cb_valori from commandbutton within w_colonne_dinamiche
integer x = 2487
integer y = 1036
integer width = 375
integer height = 80
integer taborder = 12
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Valori"
end type

event clicked;// solo se è di tipo lista
if dw_chiavi.getitemstring(dw_chiavi.getrow(), "flag_tipo_colonna") = "L" then
	window_open_parm(w_colonne_dinamiche_valori, -1, dw_chiavi)
end if
end event

type dw_chiavi from uo_cs_xx_dw within w_colonne_dinamiche
integer x = 23
integer width = 2939
integer height = 1000
string dataobject = "d_colonne_dinamiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;if  retrieve(s_cs_xx.cod_azienda) < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;cb_valori.enabled = false
this.object.b_formato.enabled = true
end event

event pcd_new;call super::pcd_new;cb_valori.enabled = false
this.object.b_formato.enabled = true
end event

event pcd_view;call super::pcd_view;cb_valori.enabled = true
this.object.b_formato.enabled = false
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	cb_valori.triggerevent(Clicked!)
end if
end event

event itemchanged;call super::itemchanged;string ls_null

if row >0 then
else	
	return
end if

setnull(ls_null)

choose case dwo.name
	case "flag_tipo_colonna"
		
		choose case data
			case "N"	//formato numerico
				this.modify("formato.values='###,##0.00/###,##0.00##/###,##0'")
				this.setitem(row, "formato", "")
				this.object.formato.protect = 0
				
			case "D"	//formato data
				this.modify("formato.values='dd-mm-yyyy/dd-mm-yyyy hh:mm/d-mmm-yy'")
				this.setitem(row, "formato", "")
				this.object.formato.protect = 0
				
			case else
				this.modify("formato.values=''")
				this.setitem(row, "formato", "")
				this.object.formato.protect = 1
				
		end choose
		
end choose
end event

event clicked;call super::clicked;string ls_flag_tipo_colonna

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_formato"
		
		if this.object.b_formato.enabled="1" then
			ls_flag_tipo_colonna = getitemstring(row, "flag_tipo_colonna")
			
			if ls_flag_tipo_colonna="D" or ls_flag_tipo_colonna="N" then				
				s_cs_xx.parametri.parametro_s_1 = ls_flag_tipo_colonna
				s_cs_xx.parametri.parametro_s_2 = "COLONNA_DINAMICA"
				window_open(w_formato_col_dinamiche_sel, 0)
				
				if s_cs_xx.parametri.parametro_s_1 = "OK" then
					setitem(row, "formato", s_cs_xx.parametri.parametro_s_2)
				end if				
			end if
		end if
		
end choose
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_flag_tipo_colonna

if currentrow > 0 then
	ls_flag_tipo_colonna = this.getitemstring(currentrow, "flag_tipo_colonna")
	
	if ls_flag_tipo_colonna = "D" or ls_flag_tipo_colonna="N" then
		this.object.formato.protect= 0
	else
		this.object.formato.protect= 1
	end if
end if
end event

