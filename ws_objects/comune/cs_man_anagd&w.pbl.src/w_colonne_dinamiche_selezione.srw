﻿$PBExportHeader$w_colonne_dinamiche_selezione.srw
forward
global type w_colonne_dinamiche_selezione from w_cs_xx_principale
end type
type st_1 from statictext within w_colonne_dinamiche_selezione
end type
type dw_1 from uo_cs_xx_dw within w_colonne_dinamiche_selezione
end type
end forward

global type w_colonne_dinamiche_selezione from w_cs_xx_principale
integer width = 1193
integer height = 976
string title = "Selezione"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
st_1 st_1
dw_1 dw_1
end type
global w_colonne_dinamiche_selezione w_colonne_dinamiche_selezione

type variables
private:
	string is_cod_colonna_dinamica
end variables

on w_colonne_dinamiche_selezione.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_colonne_dinamiche_selezione.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;
if isnull(s_cs_xx.parametri.parametro_s_1) then
	g_mb.error("Omnia", "Parametri non corretti per aprire la finestra")
	close(this)
end if

dw_1.settransobject(sqlca)
is_cod_colonna_dinamica = s_cs_xx.parametri.parametro_s_1
setnull(s_cs_xx.parametri.parametro_s_1)

dw_1.postevent("pcd_retrieve")
end event

type st_1 from statictext within w_colonne_dinamiche_selezione
integer x = 23
integer y = 40
integer width = 1143
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8421504
long backcolor = 12632256
string text = "Doppio click per selezionare il valore"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_1 from uo_cs_xx_dw within w_colonne_dinamiche_selezione
integer x = 23
integer y = 140
integer width = 1143
integer height = 740
integer taborder = 10
string dataobject = "d_colonne_dinamiche_selezione"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda, is_cod_colonna_dinamica)
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = "OK"
	s_cs_xx.parametri.parametro_ul_1 = getitemnumber(row, "progressivo")
	
	close(parent)
end if
end event

