﻿$PBExportHeader$w_contratti_manut.srw
forward
global type w_contratti_manut from w_cs_xx_principale
end type
type cb_scadenze from commandbutton within w_contratti_manut
end type
type cb_eseguite from commandbutton within w_contratti_manut
end type
type cb_report from commandbutton within w_contratti_manut
end type
type cb_fatture from commandbutton within w_contratti_manut
end type
type cb_documenti from commandbutton within w_contratti_manut
end type
type cb_attrezzature from commandbutton within w_contratti_manut
end type
type dw_contratti_manut_det from uo_cs_xx_dw within w_contratti_manut
end type
type cb_cerca from commandbutton within w_contratti_manut
end type
type cb_annulla from commandbutton within w_contratti_manut
end type
type dw_contratti_manut_ricerca from u_dw_search within w_contratti_manut
end type
type dw_contratti_manut_lista from uo_cs_xx_dw within w_contratti_manut
end type
type dw_folder from u_folder within w_contratti_manut
end type
end forward

global type w_contratti_manut from w_cs_xx_principale
integer width = 2505
integer height = 1604
string title = "Gestione Contratti di Manutenzione"
boolean maxbox = false
boolean resizable = false
cb_scadenze cb_scadenze
cb_eseguite cb_eseguite
cb_report cb_report
cb_fatture cb_fatture
cb_documenti cb_documenti
cb_attrezzature cb_attrezzature
dw_contratti_manut_det dw_contratti_manut_det
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_contratti_manut_ricerca dw_contratti_manut_ricerca
dw_contratti_manut_lista dw_contratti_manut_lista
dw_folder dw_folder
end type
global w_contratti_manut w_contratti_manut

type variables
long    il_anno, il_progressivo

boolean ib_cerca
end variables

on w_contratti_manut.create
int iCurrent
call super::create
this.cb_scadenze=create cb_scadenze
this.cb_eseguite=create cb_eseguite
this.cb_report=create cb_report
this.cb_fatture=create cb_fatture
this.cb_documenti=create cb_documenti
this.cb_attrezzature=create cb_attrezzature
this.dw_contratti_manut_det=create dw_contratti_manut_det
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_contratti_manut_ricerca=create dw_contratti_manut_ricerca
this.dw_contratti_manut_lista=create dw_contratti_manut_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_scadenze
this.Control[iCurrent+2]=this.cb_eseguite
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_fatture
this.Control[iCurrent+5]=this.cb_documenti
this.Control[iCurrent+6]=this.cb_attrezzature
this.Control[iCurrent+7]=this.dw_contratti_manut_det
this.Control[iCurrent+8]=this.cb_cerca
this.Control[iCurrent+9]=this.cb_annulla
this.Control[iCurrent+10]=this.dw_contratti_manut_ricerca
this.Control[iCurrent+11]=this.dw_contratti_manut_lista
this.Control[iCurrent+12]=this.dw_folder
end on

on w_contratti_manut.destroy
call super::destroy
destroy(this.cb_scadenze)
destroy(this.cb_eseguite)
destroy(this.cb_report)
destroy(this.cb_fatture)
destroy(this.cb_documenti)
destroy(this.cb_attrezzature)
destroy(this.dw_contratti_manut_det)
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_contratti_manut_ricerca)
destroy(this.dw_contratti_manut_lista)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_contratti_manut_det,"cod_pagamento",sqlca,"tab_pagamenti","cod_pagamento",&
					  "des_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]


ib_cerca = s_cs_xx.parametri.parametro_b_1

setnull(s_cs_xx.parametri.parametro_b_1)

il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_progressivo = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

dw_contratti_manut_lista.set_dw_options(sqlca, &
		                                  pcca.null_object, &
      		                            c_noretrieveonopen, &
            		                      c_default)

dw_contratti_manut_det.set_dw_options(sqlca, &
                  		              dw_contratti_manut_lista, &
                        		        c_sharedata + c_scrollparent, &
                              		  c_default)

dw_folder.fu_folderoptions(dw_folder.c_defaultheight, &
                           dw_folder.c_foldertableft)
									
iuo_dw_main = dw_contratti_manut_lista

dw_contratti_manut_lista.change_dw_current()

l_objects[1] = dw_contratti_manut_lista
dw_folder.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_contratti_manut_ricerca
l_objects[2] = cb_annulla
l_objects[3] = cb_cerca
dw_folder.fu_assigntab(2, "R.", l_Objects[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selectTab(2)

cb_annulla.triggerevent("clicked")

if ib_cerca then
	dw_contratti_manut_ricerca.setitem(1,"anno",il_anno)
	dw_contratti_manut_ricerca.setitem(1,"progressivo",il_progressivo)
	cb_cerca.postevent("clicked")
end if
end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_contratti_manut_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_scadenze from commandbutton within w_contratti_manut
integer x = 2103
integer y = 520
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Scadenze"
end type

event clicked;window_open(w_contratti_manut_scad,-1)
end event

type cb_eseguite from commandbutton within w_contratti_manut
integer x = 2103
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Eseguite"
end type

event clicked;long ll_row, ll_anno, ll_progressivo


ll_row = dw_contratti_manut_lista.getrow()

if ll_row = 0 or isnull(ll_row) then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_anno = dw_contratti_manut_lista.getitemnumber(ll_row,"anno")

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_progressivo = dw_contratti_manut_lista.getitemnumber(ll_row,"progressivo")

if isnull(ll_progressivo) or ll_progressivo = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_progressivo

window_open(w_contratti_manut_eseguite,-1)
end event

type cb_report from commandbutton within w_contratti_manut
integer x = 2103
integer y = 320
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;long ll_row, ll_anno, ll_progressivo


ll_row = dw_contratti_manut_lista.getrow()

if ll_row = 0 or isnull(ll_row) then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_anno = dw_contratti_manut_lista.getitemnumber(ll_row,"anno")

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_progressivo = dw_contratti_manut_lista.getitemnumber(ll_row,"progressivo")

if isnull(ll_progressivo) or ll_progressivo = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_progressivo

window_open(w_contratti_manut_report,-1)
end event

type cb_fatture from commandbutton within w_contratti_manut
integer x = 2103
integer y = 220
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Fatture"
end type

event clicked;long ll_row, ll_anno, ll_progressivo


ll_row = dw_contratti_manut_lista.getrow()

if ll_row = 0 or isnull(ll_row) then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_anno = dw_contratti_manut_lista.getitemnumber(ll_row,"anno")

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_progressivo = dw_contratti_manut_lista.getitemnumber(ll_row,"progressivo")

if isnull(ll_progressivo) or ll_progressivo = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_progressivo

window_open(w_contratti_manut_fatt,-1)
end event

type cb_documenti from commandbutton within w_contratti_manut
integer x = 2103
integer y = 120
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;long ll_row, ll_anno, ll_progressivo


ll_row = dw_contratti_manut_lista.getrow()

if ll_row = 0 or isnull(ll_row) then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_anno = dw_contratti_manut_lista.getitemnumber(ll_row,"anno")

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_progressivo = dw_contratti_manut_lista.getitemnumber(ll_row,"progressivo")

if isnull(ll_progressivo) or ll_progressivo = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_progressivo

window_open(w_contratti_manut_doc,-1)
end event

type cb_attrezzature from commandbutton within w_contratti_manut
integer x = 2103
integer y = 20
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Attrezzature"
end type

event clicked;long ll_row, ll_anno, ll_progressivo


ll_row = dw_contratti_manut_lista.getrow()

if ll_row = 0 or isnull(ll_row) then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_anno = dw_contratti_manut_lista.getitemnumber(ll_row,"anno")

if isnull(ll_anno) or ll_anno = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

ll_progressivo = dw_contratti_manut_lista.getitemnumber(ll_row,"progressivo")

if isnull(ll_progressivo) or ll_progressivo = 0 then
	g_mb.messagebox("OMNIA","Selezionare un contratto prima di proseguire!",stopsign!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = ll_anno

s_cs_xx.parametri.parametro_d_2 = ll_progressivo

window_open(w_contratti_manut_attr,-1)
end event

type dw_contratti_manut_det from uo_cs_xx_dw within w_contratti_manut
integer x = 23
integer y = 620
integer width = 2446
integer height = 880
integer taborder = 50
string dataobject = "d_contratti_manut_det"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_contratti_manut_det,"cod_fornitore")
end choose

end event

type cb_cerca from commandbutton within w_contratti_manut
integer x = 1669
integer y = 480
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;long     ll_anno, ll_progressivo

string   ls_fornitore, ls_select, ls_numero, ls_attivo

datetime ldt_da, ldt_a


dw_contratti_manut_ricerca.accepttext()

ll_anno = dw_contratti_manut_ricerca.getitemnumber(1,"anno")

ll_progressivo = dw_contratti_manut_ricerca.getitemnumber(1,"progressivo")

ls_fornitore = dw_contratti_manut_ricerca.getitemstring(1,"cod_fornitore")

ldt_da = dw_contratti_manut_ricerca.getitemdatetime(1,"da_data")

ldt_a = dw_contratti_manut_ricerca.getitemdatetime(1,"a_data")

ls_numero = dw_contratti_manut_ricerca.getitemstring(1,"numero")

ls_attivo = dw_contratti_manut_ricerca.getitemstring(1,"attivo")

ls_select = + &
"select cod_azienda, " + &
"       anno, " + &
"       progressivo, " + &
"       data, " + &
"       cod_fornitore, " + &
"       inizio_validita, " + &
"       scadenza, " + &
"       importo, " + &
"       tipo_fatturazione, " + &
"       cod_pagamento, " + &
"       note, " + &
"       attivo, " + &
"       numero " + &
"from   contratti_manut " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ll_anno) then
	ls_select = ls_select + " and anno = " + string(ll_anno)
end if

if not isnull(ll_progressivo) then
	ls_select = ls_select + " and progressivo = " + string(ll_progressivo)
end if

if not isnull(ls_fornitore) then
	ls_select = ls_select + " and cod_fornitore = '" + ls_fornitore + "'"
end if

if not isnull(ldt_da) then
	ls_select = ls_select + " and scadenza >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ldt_a) then
	ls_select = ls_select + " and scadenza <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ls_numero) then
	ls_select = ls_select + " and numero = '" + ls_numero + "'"
end if

if not isnull(ls_attivo) and ls_attivo <> "I" then
	ls_select = ls_select + " and attivo = '" + ls_attivo + "'"
end if

ls_select = ls_select + " order by anno ASC, progressivo ASC"

if dw_contratti_manut_lista.setsqlselect(ls_select) = -1 then
	g_mb.messagebox("OMNIA","Errore in impostazione select!",stopsign!)
	return -1
end if

dw_folder.fu_selecttab(1)

dw_contratti_manut_lista.change_dw_current()

parent.postevent("pc_retrieve")
end event

type cb_annulla from commandbutton within w_contratti_manut
integer x = 1280
integer y = 480
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&nnulla"
end type

event clicked;long     ll_null

string   ls_null

datetime ldt_null


setnull(ll_null)

setnull(ls_null)

setnull(ldt_null)

dw_contratti_manut_ricerca.setitem(1,"anno",f_anno_esercizio())

dw_contratti_manut_ricerca.setitem(1,"progressivo",ll_null)

dw_contratti_manut_ricerca.setitem(1,"cod_fornitore",ls_null)

dw_contratti_manut_ricerca.setitem(1,"da_data",ldt_null)

dw_contratti_manut_ricerca.setitem(1,"a_data",ldt_null)

dw_contratti_manut_ricerca.setitem(1,"numero",ls_null)

dw_contratti_manut_ricerca.setitem(1,"attivo","I")
end event

type dw_contratti_manut_ricerca from u_dw_search within w_contratti_manut
integer x = 137
integer y = 40
integer width = 1920
integer height = 360
integer taborder = 50
string dataobject = "d_contratti_manut_ricerca"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_contratti_manut_ricerca,"cod_fornitore")
end choose

end event

type dw_contratti_manut_lista from uo_cs_xx_dw within w_contratti_manut
integer x = 137
integer y = 40
integer width = 1920
integer height = 540
integer taborder = 30
string dataobject = "d_contratti_manut_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long ll_progressivo


dw_contratti_manut_det.object.b_ricerca_fornitore.enabled = true
cb_attrezzature.enabled = false
cb_documenti.enabled = false
cb_fatture.enabled = false
cb_report.enabled = false
cb_eseguite.enabled = false
cb_scadenze.enabled = false

select max(progressivo)
into   :ll_progressivo
from   contratti_manut
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo da contratti_manut: " + sqlca.sqlerrtext,stopsign!)
	return 1
end if

if isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

setitem(getrow(),"anno",f_anno_esercizio())

setitem(getrow(),"progressivo",ll_progressivo)

setitem(getrow(),"data",datetime(today(),00:00:00))
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_Cs_xx.cod_azienda)
	end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;if retrieve() = -1 then
	g_mb.messagebox("OMNIA","Errore in lettura dati!",stopsign!)
	return -1
end if
end event

event pcd_view;call super::pcd_view;dw_contratti_manut_det.object.b_ricerca_fornitore.enabled=false
end event

event pcd_modify;call super::pcd_modify;dw_contratti_manut_det.object.b_ricerca_fornitore.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_contratti_manut_det.object.b_ricerca_fornitore.enabled=false
end event

type dw_folder from u_folder within w_contratti_manut
integer x = 23
integer y = 20
integer width = 2057
integer height = 580
integer taborder = 10
end type

