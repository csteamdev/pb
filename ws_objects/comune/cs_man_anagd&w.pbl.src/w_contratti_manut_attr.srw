﻿$PBExportHeader$w_contratti_manut_attr.srw
forward
global type w_contratti_manut_attr from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_contratti_manut_attr
end type
type cb_elimina from commandbutton within w_contratti_manut_attr
end type
type cb_inserisci from commandbutton within w_contratti_manut_attr
end type
type dw_contratti_manut_attr from datawindow within w_contratti_manut_attr
end type
type dw_contratti_manut_attr_sel from uo_cs_xx_dw within w_contratti_manut_attr
end type
end forward

global type w_contratti_manut_attr from w_cs_xx_principale
integer width = 2985
integer height = 1424
string title = "Elenco Attrezzature - Contratto di Manutenzione"
boolean maxbox = false
boolean resizable = false
cb_2 cb_2
cb_elimina cb_elimina
cb_inserisci cb_inserisci
dw_contratti_manut_attr dw_contratti_manut_attr
dw_contratti_manut_attr_sel dw_contratti_manut_attr_sel
end type
global w_contratti_manut_attr w_contratti_manut_attr

type variables
long il_anno, il_progressivo
end variables

on w_contratti_manut_attr.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_elimina=create cb_elimina
this.cb_inserisci=create cb_inserisci
this.dw_contratti_manut_attr=create dw_contratti_manut_attr
this.dw_contratti_manut_attr_sel=create dw_contratti_manut_attr_sel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_elimina
this.Control[iCurrent+3]=this.cb_inserisci
this.Control[iCurrent+4]=this.dw_contratti_manut_attr
this.Control[iCurrent+5]=this.dw_contratti_manut_attr_sel
end on

on w_contratti_manut_attr.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_elimina)
destroy(this.cb_inserisci)
destroy(this.dw_contratti_manut_attr)
destroy(this.dw_contratti_manut_attr_sel)
end on

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_progressivo = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

title = "Elenco Attrezzature - Contratto di Manutenzione " + string(il_anno) + "/" + string(il_progressivo)

set_w_options(c_closenosave + c_noenablepopup)

dw_contratti_manut_attr_sel.set_dw_options(sqlca, &
														 pcca.null_object, &
														 c_noretrieveonopen + &
														 c_newonopen + &
														 c_nomodify + &
														 c_nodelete, &
														 c_default)
														 
iuo_dw_main = dw_contratti_manut_attr_sel

dw_contratti_manut_attr_sel.change_dw_current()

dw_contratti_manut_attr.settransobject(sqlca)

dw_contratti_manut_attr.retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)
end event

type cb_2 from commandbutton within w_contratti_manut_attr
integer x = 2057
integer y = 48
integer width = 73
integer height = 72
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;call super::clicked;dw_contratti_manut_attr_sel.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_attrezzatura"
end event

type cb_elimina from commandbutton within w_contratti_manut_attr
integer x = 2149
integer y = 40
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;long   ll_test

string ls_cod_attrezzatura


dw_contratti_manut_attr_sel.accepttext()

ls_cod_attrezzatura = dw_contratti_manut_attr_sel.getitemstring(1,"cod_attrezzatura")

if isnull(ls_cod_attrezzatura) then
	g_mb.messagebox("OMNIA","Selezionare un'attrezzatura prima di proseguire!",stopsign!)
	return -1
end if

select count(*)
into   :ll_test
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in controllo attrezzatura su anag_attrezzature: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 0 and ll_test < 1 then	
	g_mb.messagebox("OMNIA","Selezionare un'attrezzatura valida prima di proseguire!",stopsign!)
	return -1
end if

select count(*)
into   :ll_test
from   contratti_manut_attr
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in controllo attrezzatura su contratti_manut_attr: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_test) or ll_test < 1 then
	g_mb.messagebox("OMNIA","L'attrezzatura selezionata non è presente nell'elenco!",stopsign!)
	return -1
end if

if g_mb.messagebox("OMNIA","Eliminare dall'elenco l'attrezzatura selezionata?",question!,yesno!,2) = 2 then
	return -1
end if

delete
from   contratti_manut_attr
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in cancellazione attrezzatura da contratti_manut_attr: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","L'attrezzatura selezionata non è presente nell'elenco!",stopsign!)
	return -1
end if

dw_contratti_manut_attr.retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)
end event

type cb_inserisci from commandbutton within w_contratti_manut_attr
integer x = 2537
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Inserisci"
end type

event clicked;long   ll_test, ll_anno[], ll_progressivo[], ll_return

string ls_cod_attrezzatura, ls_attivi


dw_contratti_manut_attr_sel.accepttext()

ls_cod_attrezzatura = dw_contratti_manut_attr_sel.getitemstring(1,"cod_attrezzatura")

if isnull(ls_cod_attrezzatura) then
	g_mb.messagebox("OMNIA","Selezionare un'attrezzatura prima di proseguire!",stopsign!)
	return -1
end if

select count(*)
into   :ll_test
from   anag_attrezzature
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in controllo attrezzatura su anag_attrezzature: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 0 and ll_test < 1 then	
	g_mb.messagebox("OMNIA","Selezionare un'attrezzatura valida prima di continuare!",stopsign!)
	return -1
end if

select count(*)
into   :ll_test
from   contratti_manut_attr
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo and
		 cod_attrezzatura = :ls_cod_attrezzatura;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in controllo attrezzatura su contratti_manut_attr: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if not isnull(ll_test) and ll_test > 0 then
	g_mb.messagebox("OMNIA","L'attrezzatura selezionata è già presente nell'elenco!",stopsign!)
	return -1
end if

ll_return = f_contratti_manut_attivi(ls_cod_attrezzatura,ll_anno,ll_progressivo,ls_attivi)

if ll_return = -1 then
	g_mb.messagebox("OMNIA",ls_attivi,stopsign!)
elseif ll_return > 0 then
	if g_mb.messagebox("OMNIA","Sono già presenti contratti attivi per questa attrezzatura!~n" + ls_attivi + "~nContinuare?",question!,yesno!,2) = 2 then
		return -1
	end if
end if

insert
into   contratti_manut_attr
		 (cod_azienda,
		 anno,
		 progressivo,
		 cod_attrezzatura)
values (:s_cs_xx.cod_azienda,
		 :il_anno,
		 :il_progressivo,
		 :ls_cod_attrezzatura);
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in inserimento attrezzatura in contratti_manut_attr: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

dw_contratti_manut_attr.retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)
end event

type dw_contratti_manut_attr from datawindow within w_contratti_manut_attr
integer x = 23
integer y = 160
integer width = 2926
integer height = 1160
integer taborder = 20
string dataobject = "d_contratti_manut_attr"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;if not isnull(row) and row > 0 then
	dw_contratti_manut_attr_sel.setitem(1,"cod_attrezzatura",dw_contratti_manut_attr.getitemstring(row,"contratti_manut_attr_cod_attrezzatura"))
end if
end event

type dw_contratti_manut_attr_sel from uo_cs_xx_dw within w_contratti_manut_attr
integer x = 23
integer y = 20
integer width = 2926
integer height = 120
integer taborder = 10
string dataobject = "d_contratti_manut_attr_sel"
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_contratti_manut_attr_sel,"cod_attrezzatura")
end choose
end event

