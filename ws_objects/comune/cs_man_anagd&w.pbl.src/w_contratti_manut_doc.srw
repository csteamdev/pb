﻿$PBExportHeader$w_contratti_manut_doc.srw
forward
global type w_contratti_manut_doc from w_cs_xx_principale
end type
type dw_drag from uo_dw_drag within w_contratti_manut_doc
end type
type dw_contratti_manut_doc from uo_cs_xx_dw within w_contratti_manut_doc
end type
end forward

global type w_contratti_manut_doc from w_cs_xx_principale
integer width = 3077
integer height = 1276
string title = "Documenti - Contratto di Manutenzione"
boolean maxbox = false
dw_drag dw_drag
dw_contratti_manut_doc dw_contratti_manut_doc
end type
global w_contratti_manut_doc w_contratti_manut_doc

type variables
long il_anno, il_progressivo
end variables

forward prototypes
public subroutine wf_download_file (long al_row)
end prototypes

public subroutine wf_download_file (long al_row);string ls_temp_dir, ls_rnd, ls_estensione, ls_file_name, ls_cod_divisione
long ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
blob lb_blob
uo_shellexecute luo_run

if al_row > 0 then
	
	setpointer(Hourglass!)
	
	ll_progressivo = dw_contratti_manut_doc.getitemnumber(al_row, "prog_documento")
	ll_prog_mimetype = dw_contratti_manut_doc.getitemnumber(al_row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from contratti_manut_doc
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno = :il_anno and
			progressivo = :il_progressivo and
			prog_documento = :ll_progressivo;
			
	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(this), ls_file_name )
		destroy(luo_run)
		
	end if
	
	setpointer(Arrow!)
	
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_progressivo = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

title = "Documenti - Contratto di Manutenzione " + string(il_anno) + "/" + string(il_progressivo)

dw_contratti_manut_doc.set_dw_key("cod_azienda")
dw_contratti_manut_doc.set_dw_key("anno")
dw_contratti_manut_doc.set_dw_key("progressivo")

dw_contratti_manut_doc.set_dw_options(sqlca, &
											pcca.null_object, &
											c_scrollself, &
											c_default)

iuo_dw_main = dw_contratti_manut_doc

dw_drag.insertrow(0)
dw_drag.uof_center_dw_text()
end event

on w_contratti_manut_doc.create
int iCurrent
call super::create
this.dw_drag=create dw_drag
this.dw_contratti_manut_doc=create dw_contratti_manut_doc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_drag
this.Control[iCurrent+2]=this.dw_contratti_manut_doc
end on

on w_contratti_manut_doc.destroy
call super::destroy
destroy(this.dw_drag)
destroy(this.dw_contratti_manut_doc)
end on

type dw_drag from uo_dw_drag within w_contratti_manut_doc
integer x = 23
integer y = 900
integer width = 2994
integer height = 240
integer taborder = 20
string dataobject = "d_drag"
end type

event ue_drop_file;call super::ue_drop_file;string ls_dir, ls_file, ls_ext
long   ll_progressivo, ll_prog_mimetype
dec{6} ld_num_protocollo
blob lblob_file
	
	
select max(prog_documento) + 1
into   :ll_progressivo
from   contratti_manut_doc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo;
		
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in lettura dati da tabella contratti_manut_doc: " + sqlca.sqlerrtext)
	pcca.error = c_Fatal
	return false
end if
	
if ll_progressivo = 0 or isnull(ll_progressivo) then ll_progressivo = 1

// Assegnare un nuovo numero di protocollo
setnull(ld_num_protocollo)

select max(num_protocollo)
into   :ld_num_protocollo
from   tab_protocolli
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext)
	dw_contratti_manut_doc.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return false
end if
	
if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then ld_num_protocollo = 0
	
ld_num_protocollo ++	
	
insert into tab_protocolli(cod_azienda, num_protocollo)
values (:s_cs_xx.cod_azienda, :ld_num_protocollo);
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
	dw_contratti_manut_doc.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return false
end if

guo_functions.uof_file_to_blob(as_filename, lblob_file)
guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)

ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
		
if sqlca.sqlcode <> 0 then
	g_mb.error("Attenzione: estensione file " + ls_ext + " non prevista da sistema.")
	return false
end if

// Insert
INSERT INTO contratti_manut_doc(
	cod_azienda,
	anno,
	progressivo,
	prog_documento,
	descrizione,
	num_protocollo,
	flag_blocco,
	data_blocco,
	des_blob,
	path_documento,
	prog_mimetype
) VALUES (
	:s_cs_xx.cod_azienda,
	:il_anno,
	:il_progressivo,
	:ll_progressivo,
	:ls_file,
	:ld_num_protocollo,
	"N",
	null,
	:ls_file,
	:as_filename,
	:ll_prog_mimetype
);

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante il salvataggio del file", sqlca)
	rollback;
	return false
end if

updateblob contratti_manut_doc
set blob = :lblob_file
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo and
		 prog_documento = :ll_progressivo;
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante il salvataggio del blob", sqlca)
	rollback;
	return false
end if

commit;

dw_contratti_manut_doc.postevent("pcd_retrieve")
end event

type dw_contratti_manut_doc from uo_cs_xx_dw within w_contratti_manut_doc
integer x = 23
integer y = 20
integer width = 2994
integer height = 860
integer taborder = 10
string dataobject = "d_contratti_manut_doc"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	setitem(ll_i, "anno", il_anno)
	setitem(ll_i, "progressivo", il_progressivo)
next
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_new;call super::pcd_new;if i_extendmode then

	long   ll_progressivo
	
	dec{6} ld_num_protocollo
	
	
	select max(prog_documento) + 1
	into   :ll_progressivo
	from   contratti_manut_doc
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno = :il_anno and
			 progressivo = :il_progressivo;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella contratti_manut_doc: " + sqlca.sqlerrtext)
		pcca.error = c_Fatal
		return
	end if
	
	if ll_progressivo = 0 or isnull(ll_progressivo) then
		ll_progressivo = 1
	end if

	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into   :ld_num_protocollo
	from   tab_protocolli
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext)
		dw_contratti_manut_doc.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
	(cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_contratti_manut_doc.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	setitem(getrow(), "num_protocollo", ld_num_protocollo)
	setitem(getrow(), "prog_documento", ll_progressivo)
	setitem(getrow(), "flag_blocco", "N")
	
end if	
end event

event updateend;call super::updateend;long   ll_i

dec{6} ld_num_protocollo

string ls_cod_famiglia_chiavi, ls_cod_chiave


for ll_i = 1 to this.deletedcount()
	
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	//cancellazione tabella chiavi protocollo
	delete
	from   tab_chiavi_protocollo
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return
	end if		  

	//cancellazione tabella protocollo
	delete
	from   tab_protocolli
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
next
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_download"
		wf_download_file(row)
		
end choose
end event

event doubleclicked;call super::doubleclicked;wf_download_file(row)
end event

