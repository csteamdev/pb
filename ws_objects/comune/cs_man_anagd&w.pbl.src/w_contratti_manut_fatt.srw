﻿$PBExportHeader$w_contratti_manut_fatt.srw
forward
global type w_contratti_manut_fatt from w_cs_xx_principale
end type
type dw_contratti_manut_fatt from uo_cs_xx_dw within w_contratti_manut_fatt
end type
end forward

global type w_contratti_manut_fatt from w_cs_xx_principale
integer width = 3401
integer height = 1068
string title = "Fatture - Contratto di Manutenzione"
boolean maxbox = false
boolean resizable = false
dw_contratti_manut_fatt dw_contratti_manut_fatt
end type
global w_contratti_manut_fatt w_contratti_manut_fatt

type variables
long il_anno, il_progressivo
end variables

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_progressivo = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

title = "Fatture - Contratto di Manutenzione " + string(il_anno) + "/" + string(il_progressivo)

iuo_dw_main = dw_contratti_manut_fatt

dw_contratti_manut_fatt.change_dw_current()

dw_contratti_manut_fatt.set_dw_options(sqlca, &
													pcca.null_object, &
													c_default, &
													c_default)
end event

on w_contratti_manut_fatt.create
int iCurrent
call super::create
this.dw_contratti_manut_fatt=create dw_contratti_manut_fatt
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_contratti_manut_fatt
end on

on w_contratti_manut_fatt.destroy
call super::destroy
destroy(this.dw_contratti_manut_fatt)
end on

type dw_contratti_manut_fatt from uo_cs_xx_dw within w_contratti_manut_fatt
integer x = 23
integer y = 20
integer width = 3314
integer height = 920
integer taborder = 10
string dataobject = "d_contratti_manut_fatt"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)
end event

event pcd_new;call super::pcd_new;long ll_prog


select max(prog_fattura)
into   :ll_prog
from   contratti_manut_fatt
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno = :il_anno and
		 progressivo = :il_progressivo;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("OMNIA","Errore in lettura massimo progressivo fattura da contratti_manut_fatt: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ll_prog) then
	ll_prog = 0
end if

ll_prog ++

setitem(getrow(),"prog_fattura",ll_prog)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	setitem(ll_i, "anno", il_anno)
	setitem(ll_i, "progressivo", il_progressivo)
next
end event

