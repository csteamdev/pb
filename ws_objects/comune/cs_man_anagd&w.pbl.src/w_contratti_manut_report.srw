﻿$PBExportHeader$w_contratti_manut_report.srw
forward
global type w_contratti_manut_report from w_cs_xx_principale
end type
type dw_contratti_manut_report from uo_cs_xx_dw within w_contratti_manut_report
end type
end forward

global type w_contratti_manut_report from w_cs_xx_principale
integer width = 3214
integer height = 2244
string title = "Stampa - Contratto di Manutenzione"
boolean maxbox = false
boolean resizable = false
dw_contratti_manut_report dw_contratti_manut_report
end type
global w_contratti_manut_report w_contratti_manut_report

type variables
long il_anno, il_progressivo
end variables

on w_contratti_manut_report.create
int iCurrent
call super::create
this.dw_contratti_manut_report=create dw_contratti_manut_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_contratti_manut_report
end on

on w_contratti_manut_report.destroy
call super::destroy
destroy(this.dw_contratti_manut_report)
end on

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_progressivo = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)

title = "Stampa - Contratto di Manutenzione " + string(il_anno) + "/" + string(il_progressivo)

iuo_dw_main = dw_contratti_manut_report

dw_contratti_manut_report.change_dw_current()

dw_contratti_manut_report.set_dw_options(sqlca, &
	 												  pcca.null_object, &
													  c_nonew + &
													  c_nomodify + &
													  c_nodelete, &
													  c_nohighlightselected + &
													  c_nocursorrowpointer + &
													  c_nocursorrowfocusrect)
end event

type dw_contratti_manut_report from uo_cs_xx_dw within w_contratti_manut_report
integer x = 23
integer y = 20
integer width = 3154
integer height = 2120
integer taborder = 10
string dataobject = "d_contratti_manut_report"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,il_anno,il_progressivo)
end event

