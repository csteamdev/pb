﻿$PBExportHeader$w_contratti_manut_scad.srw
forward
global type w_contratti_manut_scad from w_cs_xx_principale
end type
type cb_cerca from commandbutton within w_contratti_manut_scad
end type
type cb_annulla from commandbutton within w_contratti_manut_scad
end type
type dw_contratti_manut_scad_sel from datawindow within w_contratti_manut_scad
end type
type dw_contratti_manut_scad from uo_cs_xx_dw within w_contratti_manut_scad
end type
end forward

global type w_contratti_manut_scad from w_cs_xx_principale
integer width = 3214
integer height = 2244
string title = "Scadenze Contratti di Manutenzione"
boolean maxbox = false
boolean resizable = false
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_contratti_manut_scad_sel dw_contratti_manut_scad_sel
dw_contratti_manut_scad dw_contratti_manut_scad
end type
global w_contratti_manut_scad w_contratti_manut_scad

type variables

end variables

on w_contratti_manut_scad.create
int iCurrent
call super::create
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_contratti_manut_scad_sel=create dw_contratti_manut_scad_sel
this.dw_contratti_manut_scad=create dw_contratti_manut_scad
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cerca
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_contratti_manut_scad_sel
this.Control[iCurrent+4]=this.dw_contratti_manut_scad
end on

on w_contratti_manut_scad.destroy
call super::destroy
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_contratti_manut_scad_sel)
destroy(this.dw_contratti_manut_scad)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_contratti_manut_scad

dw_contratti_manut_scad.change_dw_current()

dw_contratti_manut_scad.set_dw_options(sqlca, &
	 												pcca.null_object, &
													c_noretrieveonopen + &
													c_nonew + &
													c_nomodify + &
													c_nodelete, &
													c_nohighlightselected + &
													c_nocursorrowpointer + &
													c_nocursorrowfocusrect)
													
dw_contratti_manut_scad_sel.insertrow(0)
end event

type cb_cerca from commandbutton within w_contratti_manut_scad
integer x = 2766
integer y = 40
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_contratti_manut_scad_sel.accepttext()

dw_contratti_manut_scad.change_dw_current()

parent.postevent("pc_retrieve")
end event

type cb_annulla from commandbutton within w_contratti_manut_scad
integer x = 2377
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;datetime ldt_null


setnull(ldt_null)

dw_contratti_manut_scad_sel.setitem(1,"data_da",ldt_null)

dw_contratti_manut_scad_sel.setitem(1,"data_a",ldt_null)
end event

type dw_contratti_manut_scad_sel from datawindow within w_contratti_manut_scad
integer x = 23
integer y = 20
integer width = 3154
integer height = 120
integer taborder = 10
string dataobject = "d_contratti_manut_scad_sel"
boolean livescroll = true
end type

type dw_contratti_manut_scad from uo_cs_xx_dw within w_contratti_manut_scad
integer x = 23
integer y = 160
integer width = 3154
integer height = 1980
integer taborder = 10
string dataobject = "d_contratti_manut_scad"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;datetime ldt_da, ldt_a


ldt_da = dw_contratti_manut_scad_sel.getitemdatetime(1,"data_da")

ldt_a = dw_contratti_manut_scad_sel.getitemdatetime(1,"data_a")

if isnull(ldt_da) or isnull(ldt_a) or ldt_da > ldt_a then
	g_mb.messagebox("OMNIA","Impostare correttamente le date di ricerca!")
end if

object.titolo.text = "Dal " +  string(date(ldt_da)) + " al " + string(date(ldt_a))

retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a)
end event

