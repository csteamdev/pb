﻿$PBExportHeader$w_det_attrezzature.srw
$PBExportComments$Finestra di dettaglio documenti per attrezzature
forward
global type w_det_attrezzature from w_cs_xx_principale
end type
type dw_1 from uo_dw_drag within w_det_attrezzature
end type
type dw_det_attrezzature from uo_cs_xx_dw within w_det_attrezzature
end type
type cb_chiavi from commandbutton within w_det_attrezzature
end type
end forward

global type w_det_attrezzature from w_cs_xx_principale
integer width = 2875
integer height = 1396
string title = "Documenti"
dw_1 dw_1
dw_det_attrezzature dw_det_attrezzature
cb_chiavi cb_chiavi
end type
global w_det_attrezzature w_det_attrezzature

type variables
blob ib_blob


private:
	string is_cod_attrezzatura
	transaction it_transaction
end variables

on w_det_attrezzature.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_det_attrezzature=create dw_det_attrezzature
this.cb_chiavi=create cb_chiavi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_det_attrezzature
this.Control[iCurrent+3]=this.cb_chiavi
end on

on w_det_attrezzature.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_det_attrezzature)
destroy(this.cb_chiavi)
end on

event pc_setwindow;call super::pc_setwindow;string ls_descrizione

dw_det_attrezzature.set_dw_key("cod_azienda")
dw_det_attrezzature.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_default)

iuo_dw_main = dw_det_attrezzature


dw_1.settransobject(sqlca)
dw_1.insertrow(0)
dw_1.object.message_t.width = dw_1.width
end event

type dw_1 from uo_dw_drag within w_det_attrezzature
integer x = 23
integer y = 840
integer width = 2789
integer height = 320
integer taborder = 40
string dataobject = "d_drag"
end type

event ue_end_drop;call super::ue_end_drop;long					ll_row

if ab_status then
	//commit
	commit using it_transaction;
	disconnect using it_transaction;
	destroy it_transaction;
else
	//rollback
	if isvalid(it_transaction) then
		rollback using it_transaction;
		disconnect using it_transaction;
		destroy it_transaction;
	end if
	
	if as_message<>"" and not isnull(as_message) then g_mb.error(as_message)
	
end if

dw_det_attrezzature.postevent("pcd_retrieve")

return true
end event

event ue_drop_file;//****************************************************
//ANCESTOR SCRIPT DISATTIVATO
//****************************************************

integer			li_anno_registrazione
long				ll_num_registrazione, ll_row, ll_progressivo, ll_len, ll_prog_mimetype, ll_protocollo
string				ls_cod_nota, ls_des_blob, ls_dir, ls_file, ls_ext,ls_errore
blob				lb_note_esterne

select 	max(prog_attrezzatura) 
into 		:ll_progressivo
from 		det_attrezzature
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :is_cod_attrezzatura;
			
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

ls_des_blob = "Documento"

guo_functions.uof_file_to_blob(as_filename, lb_note_esterne)

ll_len = lenA(lb_note_esterne)

guo_functions.uof_get_file_info( as_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	as_message = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return false
end if

// Calcolo protocollo
setnull(ll_protocollo)

select max(num_protocollo)
into   :ll_protocollo
from   tab_protocolli
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	as_message = "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext
	return false
end if

if (sqlca.sqlcode <> 0 ) or isnull(ll_protocollo) then
	ll_protocollo = 0
end if

ll_protocollo = ll_protocollo + 1	

insert into tab_protocolli
		 (cod_azienda, num_protocollo)
values (:s_cs_xx.cod_azienda, :ll_protocollo)
using it_transaction;

if it_transaction.sqlcode <> 0 then
	as_message = "Impossibile Aggiornare la tabella protocolli " + it_transaction.sqlerrtext
	return false
end if
// fine protocollo

insert into det_attrezzature  
		( cod_azienda,   
		cod_attrezzatura,
		  prog_attrezzatura,   
		  des_blob, 
		  blob,
		  prog_mimetype,
		  descrizione,
		  num_protocollo)  
values ( :s_cs_xx.cod_azienda,   
		  :is_cod_attrezzatura,   
		  :ll_progressivo,
		  :ls_file,   
		  null,
		 :ll_prog_mimetype,
		 :ls_file,
		 :ll_protocollo) 
using it_transaction;

if it_transaction.sqlcode = 0 then
	
	updateblob det_attrezzature
	set blob = :lb_note_esterne
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :is_cod_attrezzatura and
			prog_attrezzatura = :ll_progressivo
	using it_transaction;
			
	if it_transaction.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + it_transaction.sqlerrtext
		return false
	end if
	
else
	
	if it_transaction.sqlcode <> 0 then
		as_message = "Errore nell'inserimento del documento digitale.~r~n" + it_transaction.sqlerrtext
		return false
	end if
end if

return true


end event

event ue_start_drop;call super::ue_start_drop;string ls_errore

//creo la transazione (di istanza) che sarà usata per il salvataggio dei documenti
if not guo_functions.uof_create_transaction_from( sqlca, it_transaction, ls_errore)  then
	as_message = "Errore in creazione transazione~r~n" + ls_errore
	ab_return = false
	return
end if

ab_return = true

return
end event

type dw_det_attrezzature from uo_cs_xx_dw within w_det_attrezzature
integer x = 23
integer y = 40
integer width = 2789
integer height = 780
integer taborder = 20
string dataobject = "d_det_attrezzature"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_descrizione

is_cod_attrezzatura = dw_det_attrezzature.i_parentdw.getitemstring(dw_det_attrezzature.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
ls_descrizione = dw_det_attrezzature.i_parentdw.getitemstring(dw_det_attrezzature.i_parentdw.i_selectedrows[1], "descrizione")


parent.title = "Documenti Attrezzatura " + is_cod_attrezzatura + " " + ls_descrizione

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_attrezzatura

ls_cod_attrezzatura = dw_det_attrezzature.i_parentdw.getitemstring(dw_det_attrezzature.i_parentdw.i_selectedrows[1], "cod_attrezzatura")

for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "cod_attrezzatura", ls_cod_attrezzatura)
next


end event

event pcd_new;call super::pcd_new;if i_extendmode then

	long ll_progressivo
	double ld_num_protocollo
	string ls_cod_attrezzatura
	
	ls_cod_attrezzatura = dw_det_attrezzature.i_parentdw.getitemstring(dw_det_attrezzature.i_parentdw.i_selectedrows[1], "cod_attrezzatura")
	
	select max(prog_attrezzatura) + 1
	into   :ll_progressivo
	from   det_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda	and 
	       cod_attrezzatura = :ls_cod_attrezzatura;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Omnia", "Errore in lettura dati da tabella DET_ATTREZZATURE:" + sqlca.sqlerrtext)
		pcca.error = c_Fatal
		return
	end if
	
	if ll_progressivo = 0 or isnull(ll_progressivo) then
		ll_progressivo = 1
	end if
	
	// Assegnare un nuovo numero di protocollo
	
	setnull(ld_num_protocollo)
	
	select max(num_protocollo)
	into   :ld_num_protocollo
	from   tab_protocolli
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
		dw_det_attrezzature.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then
		ld_num_protocollo = 0
	end if
	
	ld_num_protocollo = ld_num_protocollo + 1	
	
	insert into tab_protocolli
			 (cod_azienda, num_protocollo)
	values (:s_cs_xx.cod_azienda, :ld_num_protocollo);

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
		dw_det_attrezzature.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	this.setitem(this.getrow(), "num_protocollo", ld_num_protocollo)
	this.setitem(this.getrow(), "prog_attrezzatura", ll_progressivo)
	this.setitem(this.getrow(), "flag_blocco", "N")
	
	cb_chiavi.enabled = false
	
end if	
end event

event updateend;call super::updateend;long ll_i
double ld_num_protocollo
string ls_cod_famiglia_chiavi, ls_cod_chiave

for ll_i = 1 to this.deletedcount()
	ld_num_protocollo = getitemnumber(ll_i, "num_protocollo", delete!, true)

	delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return
	end if	

	delete from tab_protocolli  //cancellazione tabella protocollo
	where cod_azienda = :s_cs_xx.cod_azienda
	  and num_protocollo = :ld_num_protocollo;
	  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
		return 1
	end if
	
	cb_chiavi.enabled = true
	
next



end event

event pcd_save;call super::pcd_save;cb_chiavi.enabled = true
end event

event pcd_view;call super::pcd_view;cb_chiavi.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_chiavi.enabled = false
end event

event doubleclicked;call super::doubleclicked;string ls_temp_dir, ls_rnd, ls_estensione, ls_file_name, ls_cod_attrezzatura
long ll_cont,ll_prog_mimetype, ll_len, ll_progressivo
blob lb_blob
uo_shellexecute luo_run

if row > 0 then
	
	setpointer(Hourglass!)
	
	ls_cod_attrezzatura = getitemstring(row, "cod_attrezzatura")
	ll_progressivo = getitemnumber(row, "prog_attrezzatura")
	ll_prog_mimetype = getitemnumber(row, "prog_mimetype")
	
	selectblob blob
	into :lb_blob
	from det_attrezzature
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura and
			prog_attrezzatura = :ll_progressivo;
			
	if sqlca.sqlcode = 0  then 
		
		ll_len = lenA(lb_blob)
		
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		
		ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
		ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
		
		
		luo_run = create uo_shellexecute
		luo_run.uof_run( handle(parent), ls_file_name )
		destroy(luo_run)
		
	end if
	
	setpointer(Arrow!)
	
end if
end event

type cb_chiavi from commandbutton within w_det_attrezzature
integer x = 2423
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiavi"
end type

event clicked;if dw_det_attrezzature.getrow() < 1 or &
	isnull(dw_det_attrezzature.getrow()) then
	g_mb.messagebox("Omnia", "Inserire almeno un dettaglio")
	return
end if	

window_open_parm(w_tab_chiavi_protocollo, -1, dw_det_attrezzature)
end event

