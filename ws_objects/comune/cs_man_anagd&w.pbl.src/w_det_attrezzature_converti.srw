﻿$PBExportHeader$w_det_attrezzature_converti.srw
$PBExportComments$Finestra di dettaglio documenti per attrezzature
forward
global type w_det_attrezzature_converti from window
end type
type sle_9 from singlelineedit within w_det_attrezzature_converti
end type
type sle_8 from singlelineedit within w_det_attrezzature_converti
end type
type sle_7 from singlelineedit within w_det_attrezzature_converti
end type
type sle_6 from singlelineedit within w_det_attrezzature_converti
end type
type sle_5 from singlelineedit within w_det_attrezzature_converti
end type
type sle_4 from singlelineedit within w_det_attrezzature_converti
end type
type sle_3 from singlelineedit within w_det_attrezzature_converti
end type
type sle_2 from singlelineedit within w_det_attrezzature_converti
end type
type sle_1 from singlelineedit within w_det_attrezzature_converti
end type
type st_14 from statictext within w_det_attrezzature_converti
end type
type st_13 from statictext within w_det_attrezzature_converti
end type
type st_12 from statictext within w_det_attrezzature_converti
end type
type st_11 from statictext within w_det_attrezzature_converti
end type
type st_10 from statictext within w_det_attrezzature_converti
end type
type st_9 from statictext within w_det_attrezzature_converti
end type
type st_8 from statictext within w_det_attrezzature_converti
end type
type st_7 from statictext within w_det_attrezzature_converti
end type
type st_6 from statictext within w_det_attrezzature_converti
end type
type cb_2 from commandbutton within w_det_attrezzature_converti
end type
type st_cc from statictext within w_det_attrezzature_converti
end type
type st_3 from statictext within w_det_attrezzature_converti
end type
type st_bb from statictext within w_det_attrezzature_converti
end type
type st_2 from statictext within w_det_attrezzature_converti
end type
type mle_2 from multilineedit within w_det_attrezzature_converti
end type
type st_a3 from statictext within w_det_attrezzature_converti
end type
type st_a2 from statictext within w_det_attrezzature_converti
end type
type st_a1 from statictext within w_det_attrezzature_converti
end type
type st_5 from statictext within w_det_attrezzature_converti
end type
type st_4 from statictext within w_det_attrezzature_converti
end type
type st_1 from statictext within w_det_attrezzature_converti
end type
type mle_1 from multilineedit within w_det_attrezzature_converti
end type
type cb_1 from commandbutton within w_det_attrezzature_converti
end type
end forward

global type w_det_attrezzature_converti from window
integer width = 3209
integer height = 2188
boolean titlebar = true
string title = "Converti documenti Attrezzature"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
sle_9 sle_9
sle_8 sle_8
sle_7 sle_7
sle_6 sle_6
sle_5 sle_5
sle_4 sle_4
sle_3 sle_3
sle_2 sle_2
sle_1 sle_1
st_14 st_14
st_13 st_13
st_12 st_12
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
cb_2 cb_2
st_cc st_cc
st_3 st_3
st_bb st_bb
st_2 st_2
mle_2 mle_2
st_a3 st_a3
st_a2 st_a2
st_a1 st_a1
st_5 st_5
st_4 st_4
st_1 st_1
mle_1 mle_1
cb_1 cb_1
end type
global w_det_attrezzature_converti w_det_attrezzature_converti

type variables
string is_path
end variables

forward prototypes
public function integer wf_documenti ()
public function integer wf_converti ()
public function integer wf_imposta_dir ()
public function integer wf_converti_documenti ()
public function integer wf_carica_doc (string fs_nome_immagine, string fs_path, string fs_cod_attrezzatura, long fl_prog_attrezzatura, transaction sqlcb)
end prototypes

public function integer wf_documenti ();string      ls_db, ls_cod_attrezzatura, ls_descrizione, ls_des_blob, ls_path_documento, ls_id_attrezzatura
long        li_risposta, ll_prog_attrezzatura, ll_num_protocollo, ll_prog_mimetype, ll_doc_link, ll_doc_no_link
transaction sqlcb
blob        lb_blob

ls_db = f_db()
if ls_db = "MSSQL" then
	li_risposta = f_crea_sqlcb(sqlcb)
end if	

declare cu_doc cursor for
select  cod_attrezzatura,   
		  prog_attrezzatura,   
		  descrizione,   
		  num_protocollo,   
		  des_blob,   
		  path_documento,   
		  prog_mimetype
from    det_attrezzature  
where   cod_azienda = :s_cs_xx.cod_azienda and
        flag_blocco = 'N' and
		  (prog_mimetype < 1 or prog_mimetype is null);

open cu_doc;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_doc:" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

mle_1.text = "DOCUMENTI NULLI:~r~n"
mle_2.text = "DOCUMENTI ESISTENTI SENZA LINK:~r~n"
ll_doc_no_link = 0
ll_doc_link = 0

do while 1 = 1
	fetch cu_doc into :ls_cod_attrezzatura,
	                  :ll_prog_attrezzatura,
							:ls_descrizione,
							:ll_num_protocollo,
							:ls_des_blob,
							:ls_path_documento,
							:ll_prog_mimetype;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante fetch cursore cu_doc:" + sqlca.sqlerrtext)
		close cu_doc;
		rollback;
		return -1
	end if
	if sqlca.sqlcode = 100 then exit
	
	select id_attrezzatura 
	into   :ls_id_attrezzatura
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura;
	
	setnull(lb_blob)
	
	if ls_db = "MSSQL" then
		
		selectblob blob 
		into       :lb_blob
		from       det_attrezzature
		where      cod_azienda = :s_cs_xx.cod_azienda and
		           cod_attrezzatura = :ls_cod_attrezzatura and
		           prog_attrezzatura = :ll_prog_attrezzatura
		order by   cod_attrezzatura, prog_attrezzatura
		using      sqlcb;
		
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
			continue;
		end if
	
	else
		
		selectblob blob 
		into       :lb_blob
		from       det_attrezzature
		where      cod_azienda = :s_cs_xx.cod_azienda and
		           cod_attrezzatura = :ls_cod_attrezzatura and
		           prog_attrezzatura = :ll_prog_attrezzatura
		order by   cod_attrezzatura, prog_attrezzatura;
		
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
			continue;
		end if
		
	end if		

	if isnull(ls_path_documento) then ls_path_documento = ""
	
	if len(lb_blob) = 0 or isnull(len(lb_blob)) or isnull(lb_blob) then
		mle_1.text = mle_1.text + ls_id_attrezzatura + " - " + ls_cod_attrezzatura + "/" + string(ll_prog_attrezzatura) + ": DOCUMENTO NULLO!  Path:" + ls_path_documento + "~r~n"
	else
		if isnull(ls_path_documento) or ls_path_documento = "" then
			ll_doc_no_link++
			mle_2.text = mle_2.text + ls_id_attrezzatura + " - " + ls_cod_attrezzatura + "/" + string(ll_prog_attrezzatura) + ": PATH NULL!~r~n"
		else
			ll_doc_link++
		end if		
	end if	
	
loop

st_bb.text = string(ll_doc_no_link)
st_cc.text = string(ll_doc_link)

if ls_db = "MSSQL" then
	destroy sqlcb
end if	

close cu_doc;
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore cu_doc:" + sqlca.sqlerrtext)
	rollback;
	return -1	
end if
return 0
end function

public function integer wf_converti ();string      ls_db, ls_cod_attrezzatura, ls_descrizione, ls_des_blob, ls_path_documento, ls_formato, ls_sql, ls_syntax, ls_errore, &
            ls_immagine
long        li_risposta, ll_prog_attrezzatura, ll_num_protocollo, ll_prog_mimetype, ll_doc_link, ll_doc_no_link, ll_pos, ll_tipo, ll_ret, &
            ll_i
transaction sqlcb
blob        lb_blob, lb_blob_nuovo

datastore   lds_documenti

ls_db = f_db()
if ls_db = "MSSQL" then
	li_risposta = f_crea_sqlcb(sqlcb)
end if	

wf_imposta_dir()

//ls_sql = " select distinct(path_documento)   " + &
//	 	   " from   det_attrezzature " + &
//		   " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " 
//
//ls_syntax = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)
//
//if len(ls_errore) > 0 then
//   MessageBox( "OMNIA", ls_errore)
//   return -1
//end if
//lds_documenti = create datastore
//lds_documenti.Create( ls_syntax, ls_errore)
//
//if len(ls_errore) > 0 then
//   MessageBox( "OMNIA", ls_errore)
//   return -1
//end if
//
//lds_documenti.settransobject(sqlca)
//ll_ret = lds_documenti.retrieve()
//
//if ll_ret < 0 then
//   MessageBox( "OMNIA", "Errore durante ricerca risulatati. SQL:~r~n" + ls_sql)
//	destroy lds_documenti;
//	rollback;
//   return -1	
//end if

for ll_i = 1 to ll_ret
	
	ls_cod_attrezzatura = lds_documenti.getitemstring( ll_i, "cod_attrezzatura")
	ll_prog_attrezzatura = lds_documenti.getitemnumber( ll_i, "prog_attrezzatura")
	ls_path_documento = lds_documenti.getitemstring( ll_i, "path_documento")
	
	setnull(lb_blob)
	
	if ls_db = "MSSQL" then
		
		selectblob blob 
		into       :lb_blob
		from       det_attrezzature
		where      cod_azienda = :s_cs_xx.cod_azienda and
		           cod_attrezzatura = :ls_cod_attrezzatura and
		           prog_attrezzatura = :ll_prog_attrezzatura
		order by   cod_attrezzatura, prog_attrezzatura
		using      sqlcb;
		
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
			continue;
		end if
	
	else
		
		selectblob blob 
		into       :lb_blob
		from       det_attrezzature
		where      cod_azienda = :s_cs_xx.cod_azienda and
		           cod_attrezzatura = :ls_cod_attrezzatura and
		           prog_attrezzatura = :ll_prog_attrezzatura
		order by   cod_attrezzatura, prog_attrezzatura;
		
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
			continue;
		end if
		
	end if		

	if isnull(ls_path_documento) then ls_path_documento = ""
	
	if len(lb_blob) = 0 or isnull(len(lb_blob)) or isnull(lb_blob) then
		continue
	else
		if isnull(ls_path_documento) or ls_path_documento = "" then
			continue
		else
			
			choose case ls_path_documento
				case "100KgPolvere.gif"
					ls_immagine = sle_1.text
				case "12KgPolvere.gif"
					ls_immagine = sle_2.text
				case "2KgCO2.gif"
					ls_immagine = sle_3.text
				case "50KgPolvere.gif"
					ls_immagine = sle_4.text
				case "5KgCO2.gif" 
					ls_immagine = sle_5.text
				case "6KgPolvere.gif"
					ls_immagine = sle_6.text
				case "9KgPolvere.gif"
					ls_immagine = sle_7.text
				case "DAE.JPG"
					ls_immagine = sle_8.text
				case "PD30 Polvere.gif"
					ls_immagine = sle_9.text
			end choose
			
			
			// *** trovo il formato del documento
			ls_formato = reverse(ls_path_documento)
			ll_pos = pos( ls_formato, ".")
			if isnull(ll_pos) or ll_pos < 1 then continue
			ls_formato = left( ls_formato, ll_pos - 1)
			ls_formato = UPPER(reverse( ls_formato))
			
			select prog_mimetype
			into   :ll_tipo
			from   tab_mimetype
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       estensione = :ls_formato;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox( "OMNIA", "Errore durante la ricerca del tipo mimetype:" + sqlca.sqlerrtext)
				continue
			end if
			
			if not isnull(ll_tipo) and ll_tipo > 0 then
				
				lb_blob_nuovo = f_carica_blob( ls_immagine)
				
				if ll_ret = 0 then
					
					if ls_db = "MSSQL" then
						
						updateblob det_attrezzature
						set        blob = :lb_blob_nuovo
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  cod_attrezzatura = :ls_cod_attrezzatura and
									  prog_attrezzatura = :ll_prog_attrezzatura
						using      sqlcb;
						
						if sqlcb.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
							rollback using sqlcb;
							continue;
						end if
						
						commit using sqlcb;
					
					else
					
						updateblob det_attrezzature
						set        blob = :lb_blob_nuovo
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  cod_attrezzatura = :ls_cod_attrezzatura and
									  prog_attrezzatura = :ll_prog_attrezzatura;
						
						if sqlcb.sqlcode < 0 then
							g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlca.sqlerrtext)
							continue;
						end if
						
					end if	
					
					update det_attrezzature
					set    prog_mimetype = :ll_tipo
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_attrezzatura = :ls_cod_attrezzatura and
							 prog_attrezzatura = :ll_prog_attrezzatura;
							 
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox( "OMNIA", "Errore durante la scrittura del tipo documento: " + sqlca.sqlerrtext)
						rollback;
						continue
					end if
					
					commit;
					
					exit
					
				end if
		
			end if
			
		end if		
	end if	
	
next

destroy lds_documenti;

if ls_db = "MSSQL" then
	destroy sqlcb
end if	

return 0
end function

public function integer wf_imposta_dir ();string  ls_dir, ls_path, ls_valore, ls_str, ls_vol, ls_ris
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VQL", ls_dir)	  // parametro registro VQL

setnull(is_path)

if isnull(ls_dir) then ls_dir = ""

select stringa
into   :ls_valore
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'TDD';

if sqlca.sqlcode < 0 then	
	g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext)	
	return	-1
end if

if isnull(ls_valore) or ls_valore = "" or len(trim(ls_valore)) = 0 then // *** metto tutto nelle risorse

	// *** Michela 13/03/2006: leggo il parametro VOL se non c'è TDD, cosi metto il documento dove
	//                         sono situate anche le risorse
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VOL", ls_vol)	  // parametro registro VOL
	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "ris", ls_ris)	  // parametro registro VOL
	ls_valore = ls_vol + ls_ris

else
	if right( ls_dir, 1) <> "\" and left(ls_valore,1) <> "\" then
		ls_valore = ls_dir + "\" + ls_valore
	else
		ls_valore = ls_dir + ls_valore
	end if
	
end if

setnull(ls_str)

ls_str = right(ls_valore, 1)

if ls_str <> "\" then ls_valore = ls_valore + "\"

is_path = ls_valore

return 0
end function

public function integer wf_converti_documenti ();string ls_f1, ls_f2, ls_f3, ls_f4, ls_f5, ls_f6, ls_f7, ls_f8, ls_f9, ls_sql, ls_syntax, ls_errore, ls_cod_attrezzatura, &
       ls_path_documento
long   ll_ret, ll_i, ll_prog_attrezzatura
transaction sqlcb
datastore lds_documenti

ls_f1 = sle_1.text
ls_f2 = sle_2.text
ls_f3 = sle_3.text
ls_f4 = sle_4.text
ls_f5 = sle_5.text
ls_f6 = sle_6.text
ls_f7 = sle_7.text
ls_f8 = sle_8.text
ls_f9 = sle_9.text

if isnull(ls_f1) or ls_f1 = "" or len(ls_f1) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 100KgPolvere.gif!")
	return -1
end if

if isnull(ls_f2) or ls_f2 = "" or len(ls_f2) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 12KgPolvere.gif!")
	return -1
end if

if isnull(ls_f3) or ls_f3 = "" or len(ls_f3) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 2KgCO2.gif!")
	return -1
end if

if isnull(ls_f4) or ls_f4 = "" or len(ls_f4) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 50KgPolvere.gif!")
	return -1
end if

if isnull(ls_f5) or ls_f5 = "" or len(ls_f5) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 5KgCO2.gif!")
	return -1
end if

if isnull(ls_f6) or ls_f6 = "" or len(ls_f6) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 6KgPolvere.gif!")
	return -1
end if

if isnull(ls_f7) or ls_f7 = "" or len(ls_f7) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine 9KgPolvere.gif!")
	return -1
end if

if isnull(ls_f8) or ls_f8 = "" or len(ls_f8) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine DAE.JPG!")
	return -1
end if

if isnull(ls_f9) or ls_f9 = "" or len(ls_f9) < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: specificare il percorso per l'immagine PD30 Polvere.gif!")
	return -1
end if


// devo scorrere una a una le attrezzature

ls_sql = " select cod_attrezzatura, prog_attrezzatura, path_documento   " + &
	 	   " from   det_attrezzature " + &
		   " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " 

ls_syntax = SQLCA.SyntaxFromSQL( ls_sql, "style(type=grid)", ls_errore)

if len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", ls_errore)
   return -1
end if
lds_documenti = create datastore
lds_documenti.Create( ls_syntax, ls_errore)

if len(ls_errore) > 0 then
   g_mb.messagebox( "OMNIA", ls_errore)
   return -1
end if

lds_documenti.settransobject(sqlca)
ll_ret = lds_documenti.retrieve()

if ll_ret < 0 then
   g_mb.messagebox( "OMNIA", "Errore durante ricerca risulatati. SQL:~r~n" + ls_sql)
	destroy lds_documenti;
	rollback;
   return -1	
end if

f_crea_sqlcb(sqlcb)

for ll_i = 1 to lds_documenti.rowcount()
	
	ls_cod_attrezzatura = lds_documenti.getitemstring( ll_i, "cod_attrezzatura")
	ll_prog_attrezzatura = lds_documenti.getitemnumber( ll_i, "prog_attrezzatura")
	ls_path_documento = lds_documenti.getitemstring( ll_i, "path_documento")
	
	if isnull(ls_path_documento) or ls_path_documento = "" then continue
	
	choose case ls_path_documento
		case "100KgPolvere.gif"
			ll_ret = wf_carica_doc( "100KgPolvere.gif", ls_f1, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "12KgPolvere.gif"
			ll_ret = wf_carica_doc( "12KgPolvere.gif", ls_f2, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "2KgCO2.gif"
			ll_ret = wf_carica_doc( "2KgCO2.gif", ls_f3, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "50KgPolvere.gif"
			ll_ret = wf_carica_doc( "50KgPolvere.gif", ls_f4, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "5KgCO2.gif" 
			ll_ret = wf_carica_doc( "5KgCO2.gif", ls_f5, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "6KgPolvere.gif"
			ll_ret = wf_carica_doc( "6KgPolvere.gif", ls_f6, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "9KgPolvere.gif"
			ll_ret = wf_carica_doc( "9KgPolvere.gif", ls_f7, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "DAE.JPG"
			ll_ret = wf_carica_doc( "DAE.JPG", ls_f8, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
		case "PD30 Polvere.gif"
			ll_ret = wf_carica_doc( "PD30 Polvere.gif", ls_f9, ls_cod_attrezzatura, ll_prog_attrezzatura, sqlcb)
	end choose
			
next

destroy lds_documenti;
destroy sqlcb;
g_mb.messagebox( "OMNIA", "Operazione terminata!")
return 0
end function

public function integer wf_carica_doc (string fs_nome_immagine, string fs_path, string fs_cod_attrezzatura, long fl_prog_attrezzatura, transaction sqlcb);string ls_formato
long   ll_pos, ll_tipo, li_risposta
blob   lb_blob_nuovo

// *** trovo il formato del documento
ls_formato = reverse(fs_path)
ll_pos = pos( ls_formato, ".")
if isnull(ll_pos) or ll_pos < 1 then
	g_mb.messagebox( "OMNIA", "Attenzione: impossibile identificare il formato del documento!")
	return -1
end if

ls_formato = left( ls_formato, ll_pos - 1)
ls_formato = UPPER(reverse( ls_formato))

select prog_mimetype
into   :ll_tipo
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = :ls_formato;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca del tipo mimetype:" + sqlca.sqlerrtext)
	return -1
end if
			
if not isnull(ll_tipo) and ll_tipo > 0 then
	
	setnull(lb_blob_nuovo)
	
		
	lb_blob_nuovo = f_carica_blob( fs_path)
				
	if isnull(lb_blob_nuovo) then
		g_mb.messagebox( "OMNIA", "Documento nullo: impossibile continuare!")		
		destroy sqlcb;
		return -1
	end if
	
	updateblob det_attrezzature
	set        blob = :lb_blob_nuovo
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  cod_attrezzatura = :fs_cod_attrezzatura and
				  prog_attrezzatura = :fl_prog_attrezzatura
	using      sqlcb;
	
	if sqlcb.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la lettura del documento: " + sqlcb.sqlerrtext)
		rollback using sqlcb;
		return -1
	end if

	commit using sqlcb;
			
	update det_attrezzature
	set    prog_mimetype = :ll_tipo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :fs_cod_attrezzatura and
			 prog_attrezzatura = :fl_prog_attrezzatura;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la scrittura del tipo documento: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	commit;
					
end if	
return 0
end function

on w_det_attrezzature_converti.create
this.sle_9=create sle_9
this.sle_8=create sle_8
this.sle_7=create sle_7
this.sle_6=create sle_6
this.sle_5=create sle_5
this.sle_4=create sle_4
this.sle_3=create sle_3
this.sle_2=create sle_2
this.sle_1=create sle_1
this.st_14=create st_14
this.st_13=create st_13
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.cb_2=create cb_2
this.st_cc=create st_cc
this.st_3=create st_3
this.st_bb=create st_bb
this.st_2=create st_2
this.mle_2=create mle_2
this.st_a3=create st_a3
this.st_a2=create st_a2
this.st_a1=create st_a1
this.st_5=create st_5
this.st_4=create st_4
this.st_1=create st_1
this.mle_1=create mle_1
this.cb_1=create cb_1
this.Control[]={this.sle_9,&
this.sle_8,&
this.sle_7,&
this.sle_6,&
this.sle_5,&
this.sle_4,&
this.sle_3,&
this.sle_2,&
this.sle_1,&
this.st_14,&
this.st_13,&
this.st_12,&
this.st_11,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_6,&
this.cb_2,&
this.st_cc,&
this.st_3,&
this.st_bb,&
this.st_2,&
this.mle_2,&
this.st_a3,&
this.st_a2,&
this.st_a1,&
this.st_5,&
this.st_4,&
this.st_1,&
this.mle_1,&
this.cb_1}
end on

on w_det_attrezzature_converti.destroy
destroy(this.sle_9)
destroy(this.sle_8)
destroy(this.sle_7)
destroy(this.sle_6)
destroy(this.sle_5)
destroy(this.sle_4)
destroy(this.sle_3)
destroy(this.sle_2)
destroy(this.sle_1)
destroy(this.st_14)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.cb_2)
destroy(this.st_cc)
destroy(this.st_3)
destroy(this.st_bb)
destroy(this.st_2)
destroy(this.mle_2)
destroy(this.st_a3)
destroy(this.st_a2)
destroy(this.st_a1)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_1)
destroy(this.mle_1)
destroy(this.cb_1)
end on

event open;long    ll_count

select  count(cod_azienda)  
into    :ll_count
from    det_attrezzature  
where   cod_azienda = :s_cs_xx.cod_azienda and
        flag_blocco = 'N' and
		  (prog_mimetype < 1 or prog_mimetype is null) ;
		  
st_a1.text = string(ll_count)		  

select  count(cod_azienda)  
into    :ll_count
from    det_attrezzature  
where   cod_azienda = :s_cs_xx.cod_azienda and
        flag_blocco = 'N' and
		  (prog_mimetype < 1 or prog_mimetype is null) and
		  path_documento <> '' and 
		  path_documento is not null;
		  
st_a2.text = string(ll_count)


select  count(cod_azienda)  
into    :ll_count
from    det_attrezzature  
where   cod_azienda = :s_cs_xx.cod_azienda and
        flag_blocco = 'N' and
		  (prog_mimetype < 1 or prog_mimetype is null) and
		  (path_documento = '' or path_documento is null);
		  
st_a3.text = string(ll_count)		
end event

type sle_9 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1860
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\PD30 Polvere.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_8 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1760
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\DAE.JPG"
borderstyle borderstyle = stylelowered!
end type

type sle_7 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1660
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\9KgPolvere.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_6 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1560
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\6KgPolvere.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_5 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1460
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\5KgCO2.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_4 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1360
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\50KgPolvere.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_3 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1260
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\2KgCO2.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_2 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1160
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\12KgPolvere.gif"
borderstyle borderstyle = stylelowered!
end type

type sle_1 from singlelineedit within w_det_attrezzature_converti
integer x = 937
integer y = 1060
integer width = 1531
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\100KgPolvere.gif"
borderstyle borderstyle = stylelowered!
end type

type st_14 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1880
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"PD30 Polvere.gif~":"
boolean focusrectangle = false
end type

type st_13 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1780
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"DAE.JPG~":"
boolean focusrectangle = false
end type

type st_12 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1680
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"9KgPolvere.gif~":"
boolean focusrectangle = false
end type

type st_11 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1580
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"6KgPolvere.gif~":"
boolean focusrectangle = false
end type

type st_10 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1480
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"5KgCO2.gif~":"
boolean focusrectangle = false
end type

type st_9 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1380
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"50KgPolvere.gif~":"
boolean focusrectangle = false
end type

type st_8 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1280
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"2KgCO2.gif~":"
boolean focusrectangle = false
end type

type st_7 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1180
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"12KgPolvere.gif~":"
boolean focusrectangle = false
end type

type st_6 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 1060
integer width = 891
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Percorso       ~"100KgPolvere.gif~":"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_det_attrezzature_converti
integer x = 2514
integer y = 1840
integer width = 571
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Converti Documenti"
end type

event clicked;wf_converti_documenti()
end event

type st_cc from statictext within w_det_attrezzature_converti
integer x = 2400
integer y = 100
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_3 from statictext within w_det_attrezzature_converti
integer x = 1531
integer y = 100
integer width = 818
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documenti Esistenti con link:"
boolean focusrectangle = false
end type

type st_bb from statictext within w_det_attrezzature_converti
integer x = 2400
integer y = 20
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_2 from statictext within w_det_attrezzature_converti
integer x = 1531
integer y = 20
integer width = 818
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documenti Esistenti senza link:"
boolean focusrectangle = false
end type

type mle_2 from multilineedit within w_det_attrezzature_converti
integer x = 23
integer y = 400
integer width = 2446
integer height = 300
integer taborder = 20
integer textsize = -7
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type st_a3 from statictext within w_det_attrezzature_converti
integer x = 754
integer y = 180
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_a2 from statictext within w_det_attrezzature_converti
integer x = 754
integer y = 100
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_a1 from statictext within w_det_attrezzature_converti
integer x = 754
integer y = 20
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_5 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 180
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documenti senza link:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 100
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documenti con link:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_det_attrezzature_converti
integer x = 23
integer y = 20
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Documenti totali:"
boolean focusrectangle = false
end type

type mle_1 from multilineedit within w_det_attrezzature_converti
integer x = 23
integer y = 720
integer width = 2446
integer height = 300
integer taborder = 20
integer textsize = -7
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_det_attrezzature_converti
integer x = 23
integer y = 280
integer width = 2446
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola Documenti Inseriti con Documento Nullo"
end type

event clicked;setpointer(hourglass!)
wf_documenti()
g_mb.messagebox( "OMNIA", "Calcolo terminato!")
setpointer(arrow!)
end event

