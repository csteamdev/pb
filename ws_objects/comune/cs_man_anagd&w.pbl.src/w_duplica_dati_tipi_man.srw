﻿$PBExportHeader$w_duplica_dati_tipi_man.srw
forward
global type w_duplica_dati_tipi_man from w_cs_xx_risposta
end type
type cbx_manutenzioni from checkbox within w_duplica_dati_tipi_man
end type
type dw_commento from uo_cs_xx_dw within w_duplica_dati_tipi_man
end type
type cb_chiudi from commandbutton within w_duplica_dati_tipi_man
end type
type dw_elenco_campi from datawindow within w_duplica_dati_tipi_man
end type
type dw_elenco_attr from datawindow within w_duplica_dati_tipi_man
end type
type cbx_programmi from checkbox within w_duplica_dati_tipi_man
end type
type cb_tutto from commandbutton within w_duplica_dati_tipi_man
end type
type cb_azzera from commandbutton within w_duplica_dati_tipi_man
end type
type cb_esegui from commandbutton within w_duplica_dati_tipi_man
end type
end forward

global type w_duplica_dati_tipi_man from w_cs_xx_risposta
integer width = 2981
integer height = 2212
string title = "Trasferimento Modifiche - "
cbx_manutenzioni cbx_manutenzioni
dw_commento dw_commento
cb_chiudi cb_chiudi
dw_elenco_campi dw_elenco_campi
dw_elenco_attr dw_elenco_attr
cbx_programmi cbx_programmi
cb_tutto cb_tutto
cb_azzera cb_azzera
cb_esegui cb_esegui
end type
global w_duplica_dati_tipi_man w_duplica_dati_tipi_man

type variables
string is_attr, is_tipo_man

long 	 il_campi = 0, il_attrezzature = 0

boolean ib_fasi

dec{0}  id_data_inizio_val_1, id_data_fine_val_1
dec{0}  id_data_inizio_val_2, id_data_fine_val_2
end variables

forward prototypes
public function integer wf_togli_apostrofo (ref string as_stringa)
public function integer wf_crea_revisione_programmi (ref datastore ads_ricerca, ref datastore ads_inserisci, ref string as_errore)
end prototypes

public function integer wf_togli_apostrofo (ref string as_stringa);long ll_pos = 1


if isnull(as_stringa) then
	return 0
end if

do
	
	ll_pos = pos(as_stringa,"'",ll_pos)
	
	if ll_pos > 0 then
		as_stringa = replace(as_stringa,ll_pos,1,"''")
		ll_pos += 2
	end if
	
loop while ll_pos > 0

return 0
end function

public function integer wf_crea_revisione_programmi (ref datastore ads_ricerca, ref datastore ads_inserisci, ref string as_errore);boolean				lb_trasferisci_ricambi

integer				li_ret

long					ll_net, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione, ll_num_revisione, &
						ll_anno_reg_progr, ll_num_reg_progr, ll_index

string					ls_sql_tipi, ls_sql_where, ls_campo[], ls_valore[], ls_codice, ls_where_tipi, ls_messaggio,ls_sql, ls_null,&
						ls_cod_attrezzatura, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_des_tipo_man_storico, ls_str, ls_msg

datetime				ldt_today


ldt_today = datetime(today(),00:00:00)
setnull(ls_null)
ls_sql_where = ""
ll_index = 0
lb_trasferisci_ricambi=false

for ll_i = 1 to dw_elenco_campi.rowcount()
	
	if dw_elenco_campi.getitemstring(ll_i,"selezione") = "S"  and dw_elenco_campi.getitemstring(ll_i,"programmi") = "S" then

		if dw_elenco_campi.getitemstring(ll_i,"nome_prog") = "cod_ricambio" then
			
			lb_trasferisci_ricambi = true
			
		else

			ll_index ++
			
			ls_campo[ll_index] = dw_elenco_campi.getitemstring(ll_i,"nome_prog")
		
			ls_valore[ll_index] = dw_elenco_campi.getitemstring(ll_i,"valore")
		end if		
	end if
	
next

ls_sql_where = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' and flag_storico = 'N' " + &
					 " and cod_tipo_manutenzione = '" + is_tipo_man + "'"

// L'elaborazione deve essere eseguita anche per l'attrezzatura selezionata

ls_where_tipi = "'" + is_attr + "'"

for ll_i = 1 to dw_elenco_attr.rowcount()
	
	if dw_elenco_attr.getitemstring(ll_i,"selezione") = "S" then
		
		ls_codice = dw_elenco_attr.getitemstring(ll_i,"nome")
		
		if ls_where_tipi <> "" then
			ls_where_tipi += ", "
		end if
	
		ls_where_tipi = ls_where_tipi + "'" + ls_codice + "'"
		
	end if
	
next


ls_msg = "Non ci sono piani di manutenzione che soddisfano ai requisiti di ricerca "+&
			 "(non bloccati, non storicizzati, con tipo man. = '"+is_tipo_man+"')"

if len(ls_where_tipi) > 0 and not isnull(ls_where_tipi) then
	ls_sql_where += " and cod_attrezzatura in (" + ls_where_tipi + ") "
	
	ls_msg += " e cod.attrez. tra ("+ls_where_tipi+")"
end if




// procedo con la duplicazione

//ads_ricerca = CREATE uo_datastore_ds
ads_ricerca = CREATE datastore
ads_ricerca.dataobject = 'd_ds_programmi_manutenzione_duplica'
ads_ricerca.settransobject(sqlca)

ls_sql = ads_ricerca.getsqlselect()

ls_sql += ls_sql_where

li_ret = ads_ricerca.setsqlselect(ls_sql)
if li_ret = -1 then
	as_errore = "Errore nella creazione della sintassi SQL dinamica passata al DATASTORE."
	destroy ads_ricerca
	return -1
end if

//ads_inserisci = CREATE uo_datastore_ds
ads_inserisci = CREATE datastore
ads_inserisci.dataobject = 'd_ds_programmi_manutenzione_duplica'
ads_inserisci.settransobject(sqlca)

STRING ls_sql2
ls_sql2 = ads_ricerca.getsqlselect()


li_ret = ads_ricerca.retrieve()
if li_ret < 1 then
	as_errore = ls_msg
	destroy ads_ricerca
	destroy ads_inserisci
	
	//quindi salta il trasferimento ai programmi
	return 1
end if


li_ret = ads_ricerca.RowsCopy(ads_ricerca.GetRow(), ads_ricerca.RowCount(), Primary!, ads_inserisci, 1, Primary!)
if li_ret = -1 then
	as_errore = "Errore nella duplicazione dei programmi di manutenzione (rowscopy)."
	destroy ads_ricerca
	destroy ads_inserisci
	return -1
end if

// cerco l'ultimo numero di registrazione dei programmi
ll_anno_registrazione = f_anno_esercizio()
if ll_anno_registrazione <= 0 then
	as_errore = "Impostare l'anno di esercizio ESC in parametri aziendali"
	destroy ads_ricerca
	destroy ads_inserisci
	return -1
end if

ll_max_registrazione = 0
select max(num_registrazione)
into   :ll_max_registrazione
from   programmi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione;
if isnull(ll_max_registrazione) then
	ll_max_registrazione = 0
end if



for ll_i = 1 to ads_inserisci.rowcount()
	// creo il collegamento al vecchio programma di manutenzione
	ads_inserisci.setitem(ll_i, "anno_reg_progr_padre", ads_inserisci.getitemnumber(ll_i,"anno_registrazione") )
	ads_inserisci.setitem(ll_i,  "num_reg_progr_padre", ads_inserisci.getitemnumber(ll_i,"num_registrazione") )
	
	// imposto il nuovo numero di registrazione del programma di manutenzione
	ll_max_registrazione ++
	ads_inserisci.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
	ads_inserisci.setitem(ll_i, "num_registrazione", ll_max_registrazione)
	
	// imposto il commento
	ads_inserisci.setitem(ll_i, "commento_modifiche", dw_commento.getitemstring(dw_commento.getrow(),"commento")  )
	
	// imposto la data di creazione e data ultima modifica
	ads_inserisci.setitem(ll_i, "data_creazione", ldt_today )
	ads_inserisci.setitem(ll_i, "data_ultima_modifica", ldt_today )
	
	// imposto le firme 
	ads_inserisci.setitem(ll_i, "redatto_da", dw_commento.getitemstring(dw_commento.getrow(),"firma_redazione")  )
	ads_inserisci.setitem(ll_i, "autorizzato_da", dw_commento.getitemstring(dw_commento.getrow(),"firma_autorizzazione")  )
	ads_inserisci.setitem(ll_i, "approvato_da", dw_commento.getitemstring(dw_commento.getrow(),"firma_approvazione")  )
	
	// incremento numero revisione
	ll_num_revisione = ads_inserisci.getitemnumber(ll_i, "num_revisione")
	if isnull(ll_num_revisione) then 
		ll_num_revisione = 1
	else
		ll_num_revisione ++
	end if
	
	ads_inserisci.setitem(ll_i, "num_revisione", ll_num_revisione )
	
	ads_inserisci.setitem(ll_i, "des_tipo_manutenzione_storico", ls_null )
	
	// aggiorno i campi modificati nei programmi
	
	for ll_index = 1 to upperbound(ls_campo)
		setnull(ls_str)
		choose case ls_campo[ll_index]
				
			case "periodicita"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "periodicita", ls_str )
				
			case "frequenza"
				ads_inserisci.setitem(ll_i, "frequenza", long( ls_valore[ll_index] ) )
				
			case "flag_tipo_intervento"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "flag_tipo_intervento", ls_str )
				
			case "note_idl"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "note_idl", ls_str )
				
			case "cod_operaio"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "cod_operaio", ls_str )
				
			case "cod_risorsa_esterna"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "cod_risorsa_esterna", ls_str )
				
			case "cod_cat_risorse_esterne"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				end if
				ads_inserisci.setitem(ll_i, "cod_cat_risorse_esterne", ls_str )
				
			case "cod_tipo_lista_dist"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				else
					setnull(ls_str)
				end if
				ads_inserisci.setitem(ll_i, "cod_tipo_lista_dist", ls_str )
			
			case "cod_lista_dist"
				if len(ls_valore[ll_index]) > 0 then
					ls_str = mid(ls_valore[ll_index],2, len(ls_valore[ll_index]) - 2 )
				else
					setnull(ls_str)
				end if
				ads_inserisci.setitem(ll_i, "cod_lista_dist", ls_str )
				
		end choose
		
	next
	
next

for ll_i = 1 to ads_ricerca.rowcount()
	// metto il flag blocco nei vecchi programmi
	ads_ricerca.setitem(ll_i, "data_blocco", ldt_today )
	ads_ricerca.setitem(ll_i, "flag_blocco", "S")
	ads_ricerca.setitem(ll_i, "flag_storico", "S")
	
	// impostare la descrizione della tipologia manutenzione.
	ls_cod_attrezzatura = ads_ricerca.getitemstring(ll_i, "cod_attrezzatura")
	ls_cod_tipo_manutenzione = ads_ricerca.getitemstring(ll_i, "cod_tipo_manutenzione")
	
	select des_tipo_manutenzione_storico,
	       des_tipo_manutenzione
	into   :ls_des_tipo_man_storico,
	       :ls_des_tipo_manutenzione
	from   tab_tipi_manutenzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_attrezzatura = :ls_cod_attrezzatura and
			 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore nella ricerca tipo manutenzione: "+sqlca.sqlerrtext
		destroy ads_ricerca
		destroy ads_inserisci
		return -1
	end if	
	
	if not isnull(ls_des_tipo_man_storico) then
		
		ads_ricerca.setitem(ll_i, "des_tipo_manutenzione_storico", ls_des_tipo_man_storico)
	else
		
		ads_ricerca.setitem(ll_i, "des_tipo_manutenzione_storico", ls_des_tipo_manutenzione)
	end if	

next

li_ret = ads_ricerca.update()
if li_ret = -1 then
	as_errore = "Errore nella creazione della revisione dei programmi (update ds ricerca)"
	destroy ads_ricerca
	destroy ads_inserisci
	return -1
end if
	
li_ret = ads_inserisci.update()
if li_ret = -1 then
	as_errore = "Errore nella creazione della revisione dei programmi (update ds inserisci)"
	destroy ads_ricerca
	destroy ads_inserisci
	return -1
end if

for ll_i = 1 to ads_inserisci.rowcount()
	// sostituisco i riferimenti al programma nelle manutenzioni da confermare
	
	ll_anno_registrazione = ads_inserisci.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = ads_inserisci.getitemnumber(ll_i, "num_registrazione")
	ll_anno_reg_progr = ads_inserisci.getitemnumber(ll_i, "anno_reg_progr_padre")
	ll_num_reg_progr = ads_inserisci.getitemnumber(ll_i, "num_reg_progr_padre")
	
	update manutenzioni
	set   anno_reg_programma = :ll_anno_registrazione, 
	      num_reg_programma = :ll_num_registrazione
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_reg_programma = :ll_anno_reg_progr and
	      num_reg_programma = :ll_num_reg_progr and
		   flag_eseguito = 'N';
			
	if sqlca.sqlcode < 0 then	
		as_errore = "Errore in storicizzazione programmi manutenzione: "+sqlca.sqlerrtext
		destroy ads_ricerca
		destroy ads_inserisci
		return -1
	end if
	
	// nel caso sia stato richiesto sostituisco anche la lista dei ricambi prendendoli
	// dalla tabella delle tipoligie manutenzioni
	
	if lb_trasferisci_ricambi then
		
		delete from prog_manutenzioni_ricambi
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione =  :ll_num_registrazione;
				 
		if sqlca.sqlcode <> 0 then	
			as_errore = "Errore in cancellazione elenco ricambi da programmi manutenzione:"+sqlca.sqlerrtext
			destroy ads_ricerca
			destroy ads_inserisci
			return -1
		end if
		
		insert into prog_manutenzioni_ricambi
			(cod_azienda, 
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_ricambio,
			 cod_prodotto,
			 des_prodotto,
			 quan_utilizzo,
			 prezzo_ricambio)
			(select cod_azienda,
					  :ll_anno_registrazione,
					  :ll_num_registrazione,
					  prog_riga_ricambio,
					  cod_prodotto,
					  des_prodotto,
					  quan_utilizzo,
					  prezzo_ricambio
				from tipi_manutenzioni_ricambi
			  where cod_azienda = :s_cs_xx.cod_azienda and
					  cod_attrezzatura = :is_attr  and
					  cod_tipo_manutenzione = :is_tipo_man);
					  
		if sqlca.sqlcode <> 0 then	
			as_errore = "Errore in aggiornamento elenco ricambi in programmi manutenzione: "+sqlca.sqlerrtext
			destroy ads_ricerca
			destroy ads_inserisci
			return -1
		end if
		
	end if
next

return 0
end function

on w_duplica_dati_tipi_man.create
int iCurrent
call super::create
this.cbx_manutenzioni=create cbx_manutenzioni
this.dw_commento=create dw_commento
this.cb_chiudi=create cb_chiudi
this.dw_elenco_campi=create dw_elenco_campi
this.dw_elenco_attr=create dw_elenco_attr
this.cbx_programmi=create cbx_programmi
this.cb_tutto=create cb_tutto
this.cb_azzera=create cb_azzera
this.cb_esegui=create cb_esegui
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_manutenzioni
this.Control[iCurrent+2]=this.dw_commento
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.dw_elenco_campi
this.Control[iCurrent+5]=this.dw_elenco_attr
this.Control[iCurrent+6]=this.cbx_programmi
this.Control[iCurrent+7]=this.cb_tutto
this.Control[iCurrent+8]=this.cb_azzera
this.Control[iCurrent+9]=this.cb_esegui
end on

on w_duplica_dati_tipi_man.destroy
call super::destroy
destroy(this.cbx_manutenzioni)
destroy(this.dw_commento)
destroy(this.cb_chiudi)
destroy(this.dw_elenco_campi)
destroy(this.dw_elenco_attr)
destroy(this.cbx_programmi)
destroy(this.cb_tutto)
destroy(this.cb_azzera)
destroy(this.cb_esegui)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_noresizewin)

dw_commento.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


is_attr = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

is_tipo_man = s_cs_xx.parametri.parametro_s_2

setnull(s_cs_xx.parametri.parametro_s_2)

title = "Trasferimento Modifiche - Attrezzatura " + is_attr + " Tipologia Manutenzione " + is_tipo_man

dw_elenco_campi.postevent("ue_carica")

dw_elenco_attr.postevent("ue_carica")

// *** controllo se devo considerare le fasi di lavoro = global service. michela 25/03/2005
string ls_flag

ib_fasi = false

select flag
into   :ls_flag
from   parametri_omnia
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CFL';
		 
if sqlca.sqlcode = 0 then
	if ls_flag = "S" and not isnull(ls_flag) then
		ib_fasi = true
	end if
end if

//
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_commento, &
				  "firma_redazione", &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
				  
f_PO_LoadDDDW_DW(dw_commento, &
				  "firma_approvazione", &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
				  
f_PO_LoadDDDW_DW(dw_commento, &
				  "firma_autorizzazione", &
              sqlca, &
				  "mansionari", &
				  "cod_resp_divisione", &
				  "cognome + ' ' + nome", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
				  

end event

type cbx_manutenzioni from checkbox within w_duplica_dati_tipi_man
integer x = 23
integer y = 1500
integer width = 1527
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiorna le Manutenzioni non ancora confermate"
end type

type dw_commento from uo_cs_xx_dw within w_duplica_dati_tipi_man
integer x = 23
integer y = 1576
integer width = 2903
integer height = 540
integer taborder = 30
string dataobject = "d_trasferisci_modifiche_commento"
boolean border = false
end type

type cb_chiudi from commandbutton within w_duplica_dati_tipi_man
integer x = 2560
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = -1
long   ll_net
ll_net = g_mb.messagebox("OMNIA", "Chiudere la finestra?", Exclamation!, OKCancel!, 2)

IF ll_net = 2 THEN
	return 0
END IF
close(parent)
end event

type dw_elenco_campi from datawindow within w_duplica_dati_tipi_man
event type long ue_carica ( )
integer x = 23
integer y = 20
integer width = 1097
integer height = 1380
integer taborder = 10
string dataobject = "d_elenco_campi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event type long ue_carica();long   	ll_riga, ll_i, ll_frequenza

string 	ls_des[], ls_nome[], ls_valore[], ls_programmi[], ls_nome_prog[], ls_flag_manut, ls_modalita, &
			ls_periodicita, ls_descrizione, ls_flag_blocco, ls_cod_operaio, ls_cod_cat_risorse_esterne, &
			ls_cod_risorsa_esterna, ls_manutenzioni[], ls_cod_tipo_lista_dist, ls_cod_lista_dist
			
dec{0}	ld_data_inizio_val_1, ld_data_fine_val_1, ld_data_inizio_val_2, ld_data_fine_val_2


reset()

setpointer(hourglass!)

select des_tipo_manutenzione,
		 periodicita,
		 frequenza,
		 flag_manutenzione,
		 modalita_esecuzione,
		 flag_blocco,
		 cod_operaio,
		 cod_cat_risorse_esterne,
		 cod_risorsa_esterna,
		 data_inizio_val_1,
		 data_fine_val_1,
		 data_inizio_val_2,
		 data_fine_val_2,
		 cod_tipo_lista_dist,
		 cod_lista_dist
into   :ls_descrizione,
		 :ls_periodicita,
		 :ll_frequenza,
		 :ls_flag_manut,
		 :ls_modalita,
		 :ls_flag_blocco,
		 :ls_cod_operaio,
		 :ls_cod_cat_risorse_esterne,
		 :ls_cod_risorsa_esterna,
		 :id_data_inizio_val_1,
		 :id_data_fine_val_1,
		 :id_data_inizio_val_2,
		 :id_data_fine_val_2,
		 :ls_cod_tipo_lista_dist,
		 :ls_cod_lista_dist
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :is_attr and
		 cod_tipo_manutenzione = :is_tipo_man;
		 
if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox("OMNIA","Errore in lettura dati tipologia manutenzione: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

wf_togli_apostrofo(ls_modalita)

wf_togli_apostrofo(ls_descrizione)

ls_des[1] = "Descrizione tipologia"
ls_nome[1] = "des_tipo_manutenzione"
ls_nome_prog[1] = ""
ls_valore[1] =  "'" + ls_descrizione + "'"
ls_programmi[1] = "N"
ls_manutenzioni[1] = "N"

ls_des[2] = "Periodicità manutenzione"
ls_nome[2] = "periodicita"
ls_nome_prog[2] = "periodicita"
ls_valore[2] =  "'" + ls_periodicita + "'"
ls_programmi[2] = "S"
ls_manutenzioni[2] = "N"

ls_des[3] = "Frequenza manutenzione"
ls_nome[3] = "frequenza"
ls_nome_prog[3] = "frequenza"
ls_valore[3] = string(ll_frequenza)
ls_programmi[3] = "S"
ls_manutenzioni[3] = "N"

ls_des[4] = "Tipo intervento"
ls_nome[4] = "flag_manutenzione"
ls_nome_prog[4] = "flag_tipo_intervento"
ls_valore[4] = "'" + ls_flag_manut + "'"
ls_programmi[4] = "S"
ls_manutenzioni[4] = "S"

ls_des[5] = "Modalità di esecuzione"
ls_nome[5] = "modalita_esecuzione"
ls_nome_prog[5] = "note_idl"
ls_valore[5] = "'" + ls_modalita + "'"
ls_programmi[5] = "S"
ls_manutenzioni[5] = "S"

ls_des[6] = "Flag Blocco"
ls_nome[6] = "flag_blocco"
ls_nome_prog[6] = ""
ls_valore[6] = "'" + ls_flag_blocco + "'"
ls_programmi[6] = "N"

ls_des[7] = "Operatore"
ls_nome[7] = "cod_operaio"
ls_nome_prog[7] = "cod_operaio"
ls_valore[7] = "'" + ls_cod_operaio + "'"
ls_programmi[7] = "S"
ls_manutenzioni[7] = "S"

ls_des[8] = "Cat.Risorse Esterne"
ls_nome[8] = "cod_cat_risorse_esterne"
ls_nome_prog[8] = "cod_cat_risorse_esterne"
ls_valore[8] = "'" + ls_cod_cat_risorse_esterne + "'"
ls_programmi[8] = "S"
ls_manutenzioni[8] = "S"

ls_des[9] = "Risorsa Esterna"
ls_nome[9] = "cod_risorsa_esterna"
ls_nome_prog[9] = "cod_risorsa_esterna"
ls_valore[9] = "'" + ls_cod_risorsa_esterna + "'"
ls_programmi[9] = "S"
ls_manutenzioni[9] = "S"

ls_des[10] = "Ricambi"
ls_nome[10] = "cod_ricambio"
ls_nome_prog[10] = "cod_ricambio"
ls_valore[10] = ""
ls_programmi[10] = "S"
ls_manutenzioni[10] = "N"

ls_des[11] = "Intervallo 1"
ls_nome[11] = "data_inizio_val_1"
ls_nome_prog[11] = "data_inizio_val_1"
ls_valore[11] = ""
ls_programmi[11] = "S"
ls_manutenzioni[11] = "N"

//Donato 01-12-2008 modifica per specifica integrazione_orv
ls_des[12] = "Intervallo 2"
ls_nome[12] = "data_inizio_val_2"
ls_nome_prog[12] = "data_inizio_val_2"
ls_valore[12] = ""
ls_programmi[12] = "S"
ls_manutenzioni[12] = "N"
//-----------------

ls_des[12] = "Tipo Lista Distrib."
ls_nome[12] = "cod_tipo_lista_dist"
ls_nome_prog[12] = "cod_tipo_lista_dist"
ls_valore[12] = "'" + ls_cod_tipo_lista_dist + "'"
ls_programmi[12] = "S"
ls_manutenzioni[12] = "N"

ls_des[13] = "Lista Distrib."
ls_nome[13] = "cod_lista_dist"
ls_nome_prog[13] = "cod_lista_dist"
ls_valore[13] = "'" + ls_cod_lista_dist + "'"
ls_programmi[13] = "S"
ls_manutenzioni[13] = "N"



for ll_i = 1 to upperbound(ls_nome)

	ll_riga = insertrow(0)
	
	setitem(ll_riga,"descrizione",ls_des[ll_i])
	
	setitem(ll_riga,"nome",ls_nome[ll_i])
	
	setitem(ll_riga,"nome_prog",ls_nome_prog[ll_i])
	
	setitem(ll_riga,"valore",ls_valore[ll_i])
	
	setitem(ll_riga,"programmi",ls_programmi[ll_i])
	
	setitem(ll_riga,"selezione","N")
	
	setitem(ll_riga,"flag_tipi_manut","S")
	
	setitem(ll_riga,"flag_manut", ls_manutenzioni[ll_i])
	
next

setpointer(arrow!)

sort()

return 0
end event

event itemchanged;long			ll_pos


if isnull(row) or row < 1 then
	return
end if

if dwo.name = "selezione" then
	if data = "S" then
		il_campi ++
	else
		il_campi --
	end if
	
	//---------------------------------------------------------------------------------------------------------------------------------
	//se il campo selezionato è uno tra cod_tipo_lista_dist e cod_lista dist, prendili entrambi
	//altrimenti se il campo DE-selezionato è uno tra cod_tipo_lista_dist e cod_lista dist, toglili entrambi
	choose case getitemstring(row, "nome")
			
		//*****************************
		case "cod_tipo_lista_dist"
			ll_pos = Find ("nome='cod_lista_dist'", 1, rowcount())
			
			if ll_pos>0 then
				//seleziono/deseleziono anche l'altro
				setitem(ll_pos, "selezione", data)
				
				//incremento/decremento il numero campi
				if data="S" then
					il_campi ++
				else
					il_campi --
				end if
			end if
		
		//*****************************
		case "cod_lista_dist"
			ll_pos = Find ("nome='cod_tipo_lista_dist'", 1, rowcount())
			
			if ll_pos>0 then
				//seleziono/deseleziono anche l'altro
				setitem(ll_pos, "selezione", data)
				
				//incremento/decremento il numero campi
				if data="S" then
					il_campi ++
				else
					il_campi --
				end if
			end if
	end choose
	//---------------------------------------------------------------------------------------------------------------------------------
	
end if
end event

type dw_elenco_attr from datawindow within w_duplica_dati_tipi_man
event type long ue_carica ( )
integer x = 1143
integer y = 20
integer width = 1783
integer height = 1380
integer taborder = 10
string dataobject = "d_elenco_attr"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event type long ue_carica();long   ll_riga

string ls_des_attr, ls_elenco


reset()

setpointer(hourglass!)

declare attr cursor for
select distinct(tab_tipi_manutenzione.cod_attrezzatura), anag_attrezzature.descrizione
from   tab_tipi_manutenzione, anag_attrezzature
where  tab_tipi_manutenzione.cod_azienda = anag_attrezzature.cod_azienda and
		 tab_tipi_manutenzione.cod_attrezzatura = anag_attrezzature.cod_attrezzatura and
		 tab_tipi_manutenzione. cod_azienda = :s_cs_xx.cod_azienda and
		 tab_tipi_manutenzione.cod_attrezzatura <> :is_attr and
		 tab_tipi_manutenzione.cod_tipo_manutenzione = :is_tipo_man
order by tab_tipi_manutenzione.cod_attrezzatura ASC;
//declare attr cursor for
//select distinct(cod_attrezzatura)
//from   tab_tipi_manutenzione
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_attrezzatura <> :is_attr and
//		 cod_tipo_manutenzione = :is_tipo_man
//order by cod_attrezzatura ASC;

open attr;

if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	g_mb.messagebox("OMNIA","Errore in open cursore per lettura elenco attrezzature: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	
	fetch attr
	into  :ls_elenco, :ls_des_attr;
	
	if sqlca.sqlcode < 0 then
		setpointer(arrow!)
		g_mb.messagebox("OMNIA","Errore in fetch cursore per lettura elenco attrezzature: " + sqlca.sqlerrtext,stopsign!)
		close attr;
		return -1
	elseif sqlca.sqlcode = 100 then
		close attr;
		exit
	end if
	
//	select descrizione
//	into   :ls_des_attr
//	from   anag_attrezzature
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_attrezzatura = :ls_elenco;
//	
//	if sqlca.sqlcode <> 0 then
//		setpointer(arrow!)
//		g_mb.messagebox("OMNIA","Errore in lettura descrizione attrezzatura da anag_attrezzature: " + sqlca.sqlerrtext,stopsign!)
//		close attr;
//		return -1
//	end if
	
	ll_riga = insertrow(0)
	
	setitem(ll_riga,"descrizione",ls_elenco + " - " + ls_des_attr)
	
	setitem(ll_riga,"nome",ls_elenco)
	
	setitem(ll_riga,"selezione","N")
	
loop

close attr;

setpointer(arrow!)

sort()

return 0
end event

event itemchanged;if isnull(row) or row < 1 then
	return
end if

if dwo.name = "selezione" then
	if data = "S" then
		il_attrezzature ++
	else
		il_attrezzature --
	end if
end if
end event

type cbx_programmi from checkbox within w_duplica_dati_tipi_man
integer x = 23
integer y = 1420
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiorna i programmi di manutenzione"
boolean checked = true
end type

type cb_tutto from commandbutton within w_duplica_dati_tipi_man
integer x = 1394
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sel. Tutto"
end type

event clicked;long ll_i


setpointer(hourglass!)

for ll_i = 1 to dw_elenco_attr.rowcount()
	dw_elenco_attr.setitem(ll_i,"selezione","S")
	il_attrezzature ++
next

setpointer(arrow!)
end event

type cb_azzera from commandbutton within w_duplica_dati_tipi_man
integer x = 1783
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;long ll_i


setpointer(hourglass!)

for ll_i = 1 to dw_elenco_attr.rowcount()
	dw_elenco_attr.setitem(ll_i,"selezione","N")
	il_attrezzature --
next

setpointer(arrow!)
end event

type cb_esegui from commandbutton within w_duplica_dati_tipi_man
integer x = 2171
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Trasferisci"
end type

event clicked;boolean			lb_trasferisci_ricambi

long				ll_i, ll_y, ll_anno_registrazione, ll_num_registrazione, ll_prog_fase, start_pos

datetime			ldt_today

string				ls_sql_colonne_tipologie, ls_sql_prog, ls_campo, ls_valore, ls_codice, ls_where_tipi, ls_messaggio, ls_sql, &
					ls_elenco_attrezzature[],ls_sql_ricambi, ls_cod_attrezzatura, ls_sql_colonne_manutenzioni, &
					ls_sql_update, ls_cod_cat_risorsa_esterna, ls_cod_risorsa_esterna, ls_sql_appo, ls_cod_operaio, ls_des_fase, ls_s1, ls_s2

integer			li_ret


DECLARE cu_manut DYNAMIC CURSOR FOR SQLSA ;
declare cu_manut_ricambi dynamic cursor for sqlsa;

//uo_datastore_ds lds_ricerca, lds_inserisci

datastore lds_ricerca, lds_inserisci

dw_commento.accepttext()


if not g_mb.confirm("Continuare con il trasferimento dei dati?") THEN
	g_mb.warning("OMNIA","Operazione annullata!")
	return 0
end if


if il_campi < 1 then
	g_mb.warning("Selezionare almeno un campo del quale trasferire le modifiche!")
	return -1
end if

if il_attrezzature < 1 and cbx_programmi.checked = false then
	g_mb.warning("Selezionare almeno un'attrezzatura o i programmi di manutenzione!")
	return -1
end if

setpointer(hourglass!)

if cbx_programmi.checked then
	
	li_ret = wf_crea_revisione_programmi(lds_ricerca, lds_inserisci, ls_messaggio)
	if li_ret < 0 then
		rollback;
		g_mb.error(ls_messaggio)
		return
		
	elseif li_ret = 1 then
		//revisione programmi saltata
		g_mb.warning(ls_messaggio)
		cbx_programmi.checked = false
	end if
	
end if

//if il_attrezzature > 0 then

	ls_sql_colonne_tipologie = ""
	ls_sql_colonne_manutenzioni = ""
	
	lb_trasferisci_ricambi = false
	
	setnull(ls_cod_cat_risorsa_esterna)
	setnull(ls_cod_risorsa_esterna)
	
	for ll_i = 1 to dw_elenco_campi.rowcount()
		
		if dw_elenco_campi.getitemstring(ll_i,"selezione") = "S" then
			
			ls_campo = dw_elenco_campi.getitemstring(ll_i,"nome")
			
			if ls_campo = "cod_ricambio" then
				lb_trasferisci_ricambi = true
			else
				
				if ls_campo = "cod_cat_risorse_esterne" then ls_cod_cat_risorsa_esterna = dw_elenco_campi.getitemstring(ll_i,"valore")
				if ls_campo = "cod_risorsa_esterna" then ls_cod_risorsa_esterna = dw_elenco_campi.getitemstring(ll_i,"valore")
				if ls_campo = "cod_operaio" then ls_cod_operaio = dw_elenco_campi.getitemstring(ll_i,"valore")
				
				ls_valore = dw_elenco_campi.getitemstring(ll_i,"valore")
											
				if isnull(ls_valore) then
					ls_valore = "null"
				end if
								
				
				if dw_elenco_campi.getitemstring(ll_i,"flag_manut") = "S" then
					if ls_sql_colonne_manutenzioni <> "" then
						ls_sql_colonne_manutenzioni += ", "
					end if
						
					ls_sql_colonne_manutenzioni = ls_sql_colonne_manutenzioni + dw_elenco_campi.getitemstring(ll_i,"nome_prog") + " = " + ls_valore
				end if
				
				if dw_elenco_campi.getitemstring(ll_i,"flag_tipi_manut") = "S" then
					if ls_sql_colonne_tipologie <> "" then
						ls_sql_colonne_tipologie += ", "
					end if
					
					if ls_campo = "data_inizio_val_1" then
						
						if isnull(id_data_inizio_val_1) or id_data_inizio_val_1 = 0 then
							ls_s1 = "null"
						else
							ls_s1 = string(id_data_inizio_val_1)
						end if
						
						if isnull(id_data_fine_val_1) or id_data_fine_val_1 = 0 then
							ls_s2 = "null"
						else
							ls_s2 = string(id_data_fine_val_1)
						end if						
						
						ls_sql_colonne_tipologie = ls_sql_colonne_tipologie + " data_inizio_val_1 = " + ls_s1 + ", "
						ls_sql_colonne_tipologie = ls_sql_colonne_tipologie + " data_fine_val_1 = " + ls_s2
						
					//Donato 01-12-2008 modifica per integrazione_orv
					elseif ls_campo = "data_inizio_val_2" then
						if isnull(id_data_inizio_val_2) or id_data_inizio_val_2 = 0 then
							ls_s1 = "null"
						else
							ls_s1 = string(id_data_inizio_val_2)
						end if
						
						if isnull(id_data_fine_val_2) or id_data_fine_val_2 = 0 then
							ls_s2 = "null"
						else
							ls_s2 = string(id_data_fine_val_2)
						end if						
						
						ls_sql_colonne_tipologie = ls_sql_colonne_tipologie + " data_inizio_val_2 = " + ls_s1 + ", "
						ls_sql_colonne_tipologie = ls_sql_colonne_tipologie + " data_fine_val_2 = " + ls_s2
					//fine modifica ----------------------------------------------------
					else
						ls_sql_colonne_tipologie = ls_sql_colonne_tipologie + ls_campo + " = " + ls_valore					
					end if					
				end if
			end if			
		end if
	next
	
	ls_where_tipi = ""
	ll_y = 0
	
	for ll_i = 1 to dw_elenco_attr.rowcount()
		
		if dw_elenco_attr.getitemstring(ll_i,"selezione") = "S" then
			
			ls_codice = dw_elenco_attr.getitemstring(ll_i,"nome")
			
			if ls_where_tipi <> "" then
				ls_where_tipi += ", "
			end if
		
			ls_where_tipi = ls_where_tipi + "'" + ls_codice + "'"

			ll_y ++
			
			ls_elenco_attrezzature[ll_y] = ls_codice
			
		end if
		
	next
	
	// procedo con update tipologie manutenzioni (sempre che ci sia qualcosa da aggiornare)
	if len(ls_sql_colonne_tipologie) > 0 and len(ls_where_tipi) > 0 then
	
		ls_sql_update = "update tab_tipi_manutenzione set " + ls_sql_colonne_tipologie + ", des_tipo_manutenzione_storico = null where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_tipo_manutenzione = '" + &
						  is_tipo_man + "' and cod_attrezzatura in (" + ls_where_tipi + ")"
		
		execute immediate :ls_sql_update;
		
		if sqlca.sqlcode <> 0 then
			setpointer(arrow!)
			ls_messaggio = "Errore in aggiornamento tab_tipi_manutenzione.: " + sqlca.sqlerrtext
			rollback;
			g_mb.error(ls_messaggio)
			
			return -1
		end if
		
	end if	
	
	// aggiornamento delle registrazioni delle manutenzioni non ancora confermate
	if cbx_manutenzioni.checked then
		
		if len(ls_sql_colonne_manutenzioni) > 0 then
		
			// aggiungo il codice dell'attrezzatura corrente
			if ls_where_tipi <> "" then
				ls_where_tipi += ", "
			end if
		
			ls_where_tipi = ls_where_tipi + "'" + is_attr + "'"
			
			ls_sql_update = "update manutenzioni set " + ls_sql_colonne_manutenzioni + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_eseguito = 'N' and flag_ordinario = 'S' and  cod_tipo_manutenzione = '" + &
							  is_tipo_man + "' and cod_attrezzatura in (" + ls_where_tipi + ")"
			
			execute immediate :ls_sql_update;
			
			if sqlca.sqlcode <> 0 then
				setpointer(arrow!)
				ls_messaggio = "Errore in aggiornamento registrazioni di manutenzione.: " + sqlca.sqlerrtext
				rollback;
				g_mb.error(ls_messaggio)
				
				return -1
			end if
			
		end if
		
		// *** se non sono nulli i campi relativi alle risorse e ho la gestione delle fasi allora imposto le manutenzioni
		if not isnull(ls_cod_cat_risorsa_esterna) and not isnull(ls_cod_risorsa_esterna) and ls_cod_cat_risorsa_esterna <> "" and ls_cod_risorsa_esterna <> ""  and ib_fasi then
			
			start_pos = Pos(ls_cod_cat_risorsa_esterna, "'", 1)
			DO WHILE start_pos > 0
			   ls_cod_cat_risorsa_esterna = Replace(ls_cod_cat_risorsa_esterna, start_pos, 1, "")
			   start_pos = Pos(ls_cod_cat_risorsa_esterna, "'", start_pos + Len(""))
			LOOP

			start_pos = Pos(ls_cod_risorsa_esterna, "'", 1)
			DO WHILE start_pos > 0
			   ls_cod_risorsa_esterna = Replace(ls_cod_risorsa_esterna, start_pos, 1, "")
			   start_pos = Pos(ls_cod_risorsa_esterna, "'", start_pos + Len(""))
			LOOP

			
			ls_sql_appo = "select manutenzioni.anno_registrazione, " + &
			              "       manutenzioni.num_registrazione " + &
						     "FROM   manutenzioni " + &
						     "WHERE  manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						     "       manutenzioni.flag_eseguito = 'N' and " + &
						     "       manutenzioni.flag_ordinario = 'S' and  " + &
						     "       manutenzioni.cod_tipo_manutenzione = '" + is_tipo_man + "' and " + &
						     "       manutenzioni.cod_attrezzatura in (" + ls_where_tipi + ")  "
			
			
			PREPARE SQLSA FROM :ls_sql_appo;
			
			OPEN DYNAMIC cu_manut ;
			
			if sqlca.sqlcode <> 0 then
				setpointer(arrow!)
				ls_messaggio = "Errore in apertura cursore cu_manut.: " + sqlca.sqlerrtext
				rollback;
				g_mb.error(ls_messaggio)
				
				return -1
			end if
			
			do while 1 = 1
			
				FETCH cu_manut INTO :ll_anno_registrazione,
										  :ll_num_registrazione;
				
				if sqlca.sqlcode = 100 then exit
				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in lettura dati dal cursore cu_manut.: " + sqlca.sqlerrtext
					close cu_manut;
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if
				
				select max(prog_fase)
				into   :ll_prog_fase
				from   manutenzioni_fasi
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione;
						 
				if isnull(ll_prog_fase) then ll_prog_fase = 0
				
				ll_prog_fase = ll_prog_fase + 1
				
				ls_des_fase = "Fase " + string(ll_prog_fase)
				
				insert into manutenzioni_fasi( cod_azienda,
				                               anno_registrazione,
														 num_registrazione,
														 prog_fase,
														 des_fase)
											values  ( :s_cs_xx.cod_azienda,
											          :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ll_prog_fase,
														 :ls_des_fase);
														 
				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in inserimento fase nella manutenzione.: " + sqlca.sqlerrtext
					close cu_manut;
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if	
				
				insert into manutenzioni_fasi_risorse(  cod_azienda,
							                               anno_registrazione,
																	 num_registrazione,
																	 prog_fase,
																	 cod_cat_risorse_esterne,
																	 cod_risorsa_esterna)
														values  ( :s_cs_xx.cod_azienda,
														          :ll_anno_registrazione,
																	 :ll_num_registrazione,
																	 :ll_prog_fase,
																	 :ls_cod_cat_risorsa_esterna,
																	 :ls_cod_risorsa_esterna);
																	 
				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in inserimento risorsa nella fase nella manutenzione.: " + sqlca.sqlerrtext
					close cu_manut;
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if																		 
				
			loop
			
			CLOSE cu_manut ;						       
			
		end if
		
		if not isnull(ls_cod_operaio) and ls_cod_operaio <> "" and ib_fasi then

			start_pos = Pos(ls_cod_operaio, "'", 1)
			DO WHILE start_pos > 0
			   ls_cod_operaio = Replace(ls_cod_operaio, start_pos, 1, "")
			   start_pos = Pos(ls_cod_operaio, "'", start_pos + Len(""))
			LOOP

			ls_sql_appo = "select manutenzioni.anno_registrazione, " + &
			              "       manutenzioni.num_registrazione " + &
						     "FROM   manutenzioni " + &
						     "WHERE  manutenzioni.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						     "       manutenzioni.flag_eseguito = 'N' and " + &
						     "       manutenzioni.flag_ordinario = 'S' and  " + &
						     "       manutenzioni.cod_tipo_manutenzione = '" + is_tipo_man + "' and " + &
						     "       manutenzioni.cod_attrezzatura in (" + ls_where_tipi + ")  "

			DECLARE cu_manut_op DYNAMIC CURSOR FOR SQLSA ;

			PREPARE SQLSA FROM :ls_sql_appo;

			OPEN DYNAMIC cu_manut_op ;

			if sqlca.sqlcode <> 0 then
				setpointer(arrow!)
				ls_messaggio = "Errore in apertura cursore cu_manut.:" + sqlca.sqlerrtext
				rollback;
				g_mb.error(ls_messaggio)

				return -1
			end if

			do while 1 = 1

				FETCH cu_manut_op INTO :ll_anno_registrazione,
										     :ll_num_registrazione;

				if sqlca.sqlcode = 100 then exit
				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in lettura dati dal cursore cu_manut.: " + sqlca.sqlerrtext
					close cu_manut_op;
					rollback;
					g_mb.error(ls_messaggio)

					return -1
				end if

				select max(prog_fase)
				into   :ll_prog_fase
				from   manutenzioni_fasi
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione;

				if isnull(ll_prog_fase) then ll_prog_fase = 0

				ll_prog_fase = ll_prog_fase + 1
				
				ls_des_fase = "Fase " + string(ll_prog_fase)

				insert into manutenzioni_fasi( cod_azienda,
				                               anno_registrazione,
														 num_registrazione,
														 prog_fase,
														 des_fase)
											values  ( :s_cs_xx.cod_azienda,
											          :ll_anno_registrazione,
														 :ll_num_registrazione,
														 :ll_prog_fase,
														 :ls_des_fase);

				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in inserimento fase nella manutenzione.: " + sqlca.sqlerrtext
					close cu_manut_op;
					rollback;
					g_mb.error(ls_messaggio)

					return -1
				end if	

				insert into manutenzioni_fasi_operai(  	cod_azienda,
							                               				anno_registrazione,
																	num_registrazione,
																	prog_fase,
																	cod_operaio)
														values  (  :s_cs_xx.cod_azienda,
														          	 :ll_anno_registrazione,
																	 :ll_num_registrazione,
																	 :ll_prog_fase,
																	 :ls_cod_operaio);

				if sqlca.sqlcode < 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in inserimento operaio nella fase nella manutenzione: " + sqlca.sqlerrtext
					close cu_manut_op;
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if																		 
				
			loop
			
			CLOSE cu_manut_op ;						       
			
		end if		
		
	end if	
	
	
	// trasferimento ricambi nelle tipologie manutenzioni selezionate
	

	ll_y ++
	ls_elenco_attrezzature[ll_y] = is_attr  // aggiunto codice attrezzatura corrente
	
	if lb_trasferisci_ricambi then
	
		for ll_i = 1 to ll_y
			
			if ls_elenco_attrezzature[ll_i] <> is_attr then
			
				// cancello vecchio elenco elenco ricambi
				delete from tipi_manutenzioni_ricambi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_manutenzione = :is_tipo_man and
						 cod_attrezzatura = :ls_elenco_attrezzature[ll_i];
						 
				if sqlca.sqlcode <> 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in cancellazione precedente elenco ricambi.: " + sqlca.sqlerrtext
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if
				
				// inserisco nuovo elenco ricambi
				insert into tipi_manutenzioni_ricambi
				(cod_azienda, 
				 cod_attrezzatura,
				 cod_tipo_manutenzione,
				 prog_riga_ricambio,
				 cod_prodotto,
				 des_prodotto,
				 quan_utilizzo,
				 prezzo_ricambio)
				(select cod_azienda,
						  :ls_elenco_attrezzature[ll_i],
						  cod_tipo_manutenzione,
						  prog_riga_ricambio,
						  cod_prodotto,
						  des_prodotto,
						  quan_utilizzo,
						  prezzo_ricambio
					from tipi_manutenzioni_ricambi
				  where cod_azienda = :s_cs_xx.cod_azienda and
						  cod_attrezzatura = :is_attr  and
						  cod_tipo_manutenzione = :is_tipo_man);
							  
				if sqlca.sqlcode <> 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in inserimento nuovo elenco ricambi.: " + sqlca.sqlerrtext
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if
				
			end if
			
			if cbx_manutenzioni.checked then
				
				ls_sql_ricambi = " select anno_registrazione, num_registrazione " + &
				                 " from  manutenzioni " + & 
									  " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_eseguito = 'N' and flag_ordinario = 'S' and  " + &
									  " cod_tipo_manutenzione = '" + is_tipo_man + "' and " + &
									  " cod_attrezzatura = '" + ls_elenco_attrezzature[ll_i] + "' "
				prepare sqlsa from :ls_sql_ricambi;
				
				open cu_manut_ricambi;
				if sqlca.sqlcode <> 0 then
					setpointer(arrow!)
					ls_messaggio = "Errore in open cursore manut_ricambi.: " + sqlca.sqlerrtext
					rollback;
					g_mb.error(ls_messaggio)
					
					return -1
				end if
				
				do while true
					fetch cu_manut_ricambi into :ll_anno_registrazione, :ll_num_registrazione;
					if sqlca.sqlcode = -1 then
						setpointer(arrow!)
						ls_messaggio = "Errore in fetch cursore cu_manut_ricambi.: " + sqlca.sqlerrtext
						close cu_manut_ricambi;
						rollback;
						g_mb.error(ls_messaggio)
						
						return -1
					end if
					
					if sqlca.sqlcode = 100 then exit
					
					delete manutenzioni_ricambi
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       anno_registrazione = :ll_anno_registrazione and
							 num_registrazione = :ll_num_registrazione;
							 
					if sqlca.sqlcode <> 0 then
						setpointer(arrow!)
						ls_messaggio = "Errore in cancellazione ricambi da manutenzioni non eseguite.:" + sqlca.sqlerrtext
						close cu_manut_ricambi;
						rollback;
						g_mb.error(ls_messaggio)
						
						return -1
					end if
							 
					insert into manutenzioni_ricambi
					(cod_azienda, 
					 anno_registrazione,
					 num_registrazione,
					 prog_riga_ricambio,
					 cod_prodotto,
					 des_prodotto,
					 quan_utilizzo,
					 prezzo_ricambio)
					(select cod_azienda,
							  :ll_anno_registrazione,
							  :ll_num_registrazione,
							  prog_riga_ricambio,
							  cod_prodotto,
							  des_prodotto,
							  quan_utilizzo,
							  prezzo_ricambio
						from tipi_manutenzioni_ricambi
					  where cod_azienda = :s_cs_xx.cod_azienda and
							  cod_attrezzatura = :is_attr  and
							  cod_tipo_manutenzione = :is_tipo_man);
								  
					if sqlca.sqlcode <> 0 then
						setpointer(arrow!)
						ls_messaggio = "Errore in inserimento nuovo elenco ricambi in manutenzioni.: " + sqlca.sqlerrtext
						close cu_manut_ricambi;
						rollback;
						g_mb.error(ls_messaggio)
						
						return -1
					end if
					
				loop
				
				close cu_manut_ricambi;
				
			end if
		next
		
	end if	
	// fine trasferimento ricambi
//end if

ls_sql = "update tab_tipi_manutenzione set des_tipo_manutenzione_storico = null where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_tipo_manutenzione = '" + &
				  is_tipo_man + "' and cod_attrezzatura = '" + is_attr + "'"

execute immediate :ls_sql;

if sqlca.sqlcode <> 0 then
	setpointer(arrow!)
	ls_messaggio = "Errore in aggiornamento tab_tipi_manutenzione: " + sqlca.sqlerrtext
	rollback;
	g_mb.error(ls_messaggio)
	
	return -1
end if

setpointer(arrow!)

commit;

destroy lds_ricerca
destroy lds_inserisci

g_mb.success("Aggiornamento effettuato!")

s_cs_xx.parametri.parametro_d_1 = 0

close(parent)
end event

