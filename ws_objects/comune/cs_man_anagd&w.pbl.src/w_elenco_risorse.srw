﻿$PBExportHeader$w_elenco_risorse.srw
$PBExportComments$Finestra Elenco Risorse
forward
global type w_elenco_risorse from w_cs_xx_principale
end type
type dw_elenco_risorse from uo_cs_xx_dw within w_elenco_risorse
end type
end forward

global type w_elenco_risorse from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3360
integer height = 960
string title = "Risorse"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_elenco_risorse dw_elenco_risorse
end type
global w_elenco_risorse w_elenco_risorse

event pc_setwindow;call super::pc_setwindow;dw_elenco_risorse.set_dw_key("cod_azienda")
//dw_elenco_risorse.set_dw_options(sqlca, &
//											i_openparm, &
//											c_scrollparent, &
//											c_default)

dw_elenco_risorse.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)

iuo_dw_main = dw_elenco_risorse

end event

on w_elenco_risorse.create
int iCurrent
call super::create
this.dw_elenco_risorse=create dw_elenco_risorse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_elenco_risorse
end on

on w_elenco_risorse.destroy
call super::destroy
destroy(this.dw_elenco_risorse)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_elenco_risorse,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event close;call super::close;long ll_righe, ll_totale, ll_i, ll_tempo

ll_righe = dw_elenco_risorse.rowcount()

if ll_righe > 0 then
	for ll_i = 1 to ll_righe
		ll_tempo = dw_elenco_risorse.getitemnumber(ll_i, "tempo_esecuzione")
		ll_totale = ll_totale + ll_tempo
	next
end if

s_cs_xx.parametri.parametro_d_8 = ll_totale


end event

type dw_elenco_risorse from uo_cs_xx_dw within w_elenco_risorse
integer x = 37
integer y = 32
integer width = 3255
integer height = 800
integer taborder = 10
string dataobject = "d_elenco_risorse"
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_10, s_cs_xx.parametri.parametro_s_11)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   this.setitem(ll_i, "cod_attrezzatura", s_cs_xx.parametri.parametro_s_10)
   this.setitem(ll_i, "cod_tipo_manutenzione", s_cs_xx.parametri.parametro_s_11)	
next


end event

