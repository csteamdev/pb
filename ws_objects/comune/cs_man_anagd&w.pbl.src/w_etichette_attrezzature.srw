﻿$PBExportHeader$w_etichette_attrezzature.srw
$PBExportComments$Window etichette attrezzature con Codici a Barre
forward
global type w_etichette_attrezzature from w_cs_xx_principale
end type
type dw_etichette_attrezzature from uo_cs_xx_dw within w_etichette_attrezzature
end type
end forward

global type w_etichette_attrezzature from w_cs_xx_principale
int Width=2899
int Height=1361
boolean TitleBar=true
string Title="Etichette Difformita/Difetti"
boolean HScrollBar=true
boolean VScrollBar=true
dw_etichette_attrezzature dw_etichette_attrezzature
end type
global w_etichette_attrezzature w_etichette_attrezzature

on w_etichette_attrezzature.create
int iCurrent
call w_cs_xx_principale::create
this.dw_etichette_attrezzature=create dw_etichette_attrezzature
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_etichette_attrezzature
end on

on w_etichette_attrezzature.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_etichette_attrezzature)
end on

event pc_setwindow;call super::pc_setwindow;
dw_etichette_attrezzature.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_scrollparent + &
											c_disablecc, &
                                 c_default)

iuo_dw_main =dw_etichette_attrezzature


end event

type dw_etichette_attrezzature from uo_cs_xx_dw within w_etichette_attrezzature
int X=23
int Y=21
int Width=2743
int Height=1161
string DataObject="d_etichette_attrezzature"
end type

event pcd_retrieve;call super::pcd_retrieve;long     ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

