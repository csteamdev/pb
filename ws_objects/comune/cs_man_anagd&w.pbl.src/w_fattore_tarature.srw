﻿$PBExportHeader$w_fattore_tarature.srw
$PBExportComments$Finestra Gestione Parametri
forward
global type w_fattore_tarature from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_fattore_tarature
end type
end forward

global type w_fattore_tarature from w_cs_xx_principale
integer width = 1413
integer height = 944
string title = "Fattore Tarature"
boolean center = true
dw_lista dw_lista
end type
global w_fattore_tarature w_fattore_tarature

event open;call super::open;dw_lista.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("num_tarature")

end event

on w_fattore_tarature.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_fattore_tarature.destroy
call super::destroy
destroy(this.dw_lista)
end on

type dw_lista from uo_cs_xx_dw within w_fattore_tarature
integer x = 23
integer y = 20
integer width = 1326
integer height = 800
integer taborder = 10
string dataobject = "d_fattore_tarature"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;if retrieve(s_cs_xx.cod_azienda) < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

