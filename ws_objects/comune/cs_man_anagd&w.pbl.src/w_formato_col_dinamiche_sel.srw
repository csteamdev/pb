﻿$PBExportHeader$w_formato_col_dinamiche_sel.srw
forward
global type w_formato_col_dinamiche_sel from w_cs_xx_principale
end type
type dw_1 from datawindow within w_formato_col_dinamiche_sel
end type
type st_1 from statictext within w_formato_col_dinamiche_sel
end type
end forward

global type w_formato_col_dinamiche_sel from w_cs_xx_principale
integer width = 1746
integer height = 980
string title = "Selezione"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_1 dw_1
st_1 st_1
end type
global w_formato_col_dinamiche_sel w_formato_col_dinamiche_sel

type variables
private:
	string is_parametro, is_flag_tipo_window
end variables

on w_formato_col_dinamiche_sel.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.st_1
end on

on w_formato_col_dinamiche_sel.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql, ls_syntax, ls_error
long ll_row

if isnull(s_cs_xx.parametri.parametro_s_1) then
	g_mb.error("Omnia", "Parametri non corretti per aprire la finestra")
	close(this)
end if

dw_1.settransobject(sqlca)

is_flag_tipo_window = s_cs_xx.parametri.parametro_s_2
is_parametro = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)

if is_flag_tipo_window = "ATTREZZATURA" then
	//in is_parametro c'è il cod_attrezzatura
	
	//rendi disponibili le colonne non ancora associate all'attrezzatura e non bloccate
	ls_sql = "SELECT tab_colonne_dinamiche.cod_colonna_dinamica, tab_colonne_dinamiche.des_colonna_dinamica "+ &
				"FROM tab_colonne_dinamiche " + &
				"LEFT OUTER JOIN anag_attr_col_dinamiche on " + &
				"tab_colonne_dinamiche.cod_azienda=anag_attr_col_dinamiche.cod_azienda "+ &
				"and tab_colonne_dinamiche.cod_colonna_dinamica=anag_attr_col_dinamiche.cod_colonna_dinamica "+ &
				"and anag_attr_col_dinamiche.cod_attrezzatura ='" + is_parametro + "' " + &
				"where tab_colonne_dinamiche.cod_azienda='"+s_cs_xx.cod_azienda+"' " + &
				"and ((tab_colonne_dinamiche.flag_blocco <> 'S') or (tab_colonne_dinamiche.flag_blocco = 'S' and tab_colonne_dinamiche.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "+&
				"and anag_attr_col_dinamiche.cod_colonna_dinamica is null"
 
 // stefanop 25/09/2012: cambio la query perchè su Oracle non funziona!!!
//	ls_sql = "select "+&
//					"a.cod_colonna_dinamica,"+&
//					"a.des_colonna_dinamica "+&
//				"from tab_colonne_dinamiche as a "+&
//				"left outer join (	select cod_azienda, cod_attrezzatura, cod_colonna_dinamica "+&
//										"from anag_attr_col_dinamiche "+&					
//										"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and cod_attrezzatura='"+is_parametro+"' "+&
//									") as b on a.cod_azienda=b.cod_azienda "+&
//											"and a.cod_colonna_dinamica=b.cod_colonna_dinamica "+&
//				"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and ((a.flag_blocco <> 'S') or (a.flag_blocco = 'S' and a.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "+&
//    									"and b.cod_colonna_dinamica is null "
else
	//in is_parametro c'è il flag_tipo_colonna
	
	//serve una dw con struttura un solo campo varchar(20)
	ls_sql = "select '                    ' as formato from aziende where cod_azienda='CODICEINESISTENTE' "
end if
	
ls_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)
	
if len(ls_error) > 0 then
	g_mb.messagebox("OMNIA", "Errore creazione sintassi SQL! " + ls_error , Exclamation!)
else
	dw_1.create(ls_syntax, ls_error)
	if Len(ls_error) > 0 then
		g_mb.messagebox("OMNIA", "Errore creazione datawindow! " + ls_error , Exclamation!)
	end if
end if

dw_1.SetTransObject(SQLCA)
dw_1.Retrieve()

if is_flag_tipo_window = "ATTREZZATURA" then
	//in is_parametro c'è il cod_attrezzatura
	
else
	choose case is_parametro
		case "N"	//numerico
			//precarica alcuni formati numerici		
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "###,##0.00")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "###,##0.00#")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "###,##0.00##")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "###,##0")
			
			dw_1.selectrow(1, true)
			
		case "D"	//datetime
			//precarica alcuni formati datetime
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "dd-mm-yyyy")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "dd-mm-yyyy hh:mm:ss")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "dd-mm-yy")
			
			ll_row = dw_1.insertrow(0)
			dw_1.setitem(ll_row, "formato", "dd-mm-yy hh:mm")
			
			dw_1.selectrow(1, true)
			
	end choose
	
end if
end event

type dw_1 from datawindow within w_formato_col_dinamiche_sel
integer x = 27
integer y = 140
integer width = 1687
integer height = 736
integer taborder = 10
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = "OK"
	s_cs_xx.parametri.parametro_s_2 = getitemstring(row, 1)
	
	close(parent)
end if
end event

event rowfocuschanged;dw_1.selectrow(0, false)
dw_1.selectrow(currentrow, true)
end event

type st_1 from statictext within w_formato_col_dinamiche_sel
integer x = 23
integer y = 40
integer width = 1143
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 8421504
long backcolor = 12632256
string text = "Doppio click per selezionare"
alignment alignment = center!
boolean focusrectangle = false
end type

