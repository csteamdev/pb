﻿$PBExportHeader$w_guasti.srw
$PBExportComments$Finestra Gestione Tabella Guasti
forward
global type w_guasti from w_cs_xx_principale
end type
type dw_guasti_lista from uo_cs_xx_dw within w_guasti
end type
type dw_guasti_det from uo_cs_xx_dw within w_guasti
end type
end forward

global type w_guasti from w_cs_xx_principale
integer width = 2775
integer height = 1520
string title = "Tipologie Guasti su Attrezzature"
dw_guasti_lista dw_guasti_lista
dw_guasti_det dw_guasti_det
end type
global w_guasti w_guasti

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_guasti_det,"cod_attrezzatura",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione",&
                 "anag_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_guasti.create
int iCurrent
call super::create
this.dw_guasti_lista=create dw_guasti_lista
this.dw_guasti_det=create dw_guasti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_guasti_lista
this.Control[iCurrent+2]=this.dw_guasti_det
end on

on w_guasti.destroy
call super::destroy
destroy(this.dw_guasti_lista)
destroy(this.dw_guasti_det)
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_guasti_lista.set_dw_key("cod_azienda")
dw_guasti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_guasti_det.set_dw_options(sqlca,dw_guasti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_guasti_lista
end on

type dw_guasti_lista from uo_cs_xx_dw within w_guasti
integer x = 23
integer y = 20
integer width = 2697
integer height = 500
integer taborder = 10
string dataobject = "d_guasti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_attrezzatura
	
   ls_cod_attrezzatura = this.getitemstring(this.getrow(),"cod_attrezzatura")
	f_PO_LoadDDDW_DW(dw_guasti_det,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "(tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " +&
                 "(tab_tipi_manutenzione.cod_attrezzatura = '" +  ls_cod_attrezzatura + "')" )
end if
end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_guasti"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_guasto")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_attrezzatura")
//	lstr_des_multilingua.chiave_str_2 = getitemstring(row,"cod_guasto")
//
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

type dw_guasti_det from uo_cs_xx_dw within w_guasti
integer x = 23
integer y = 540
integer width = 2697
integer height = 860
integer taborder = 20
string dataobject = "d_guasti_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "cod_attrezzatura"
		   f_PO_LoadDDDW_DW(dw_guasti_det,"cod_tipo_manutenzione",sqlca,&
              "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
              "(tab_tipi_manutenzione.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " +&
              "(tab_tipi_manutenzione.cod_attrezzatura = '" + i_coltext+ "')" )
	end choose
end if	

end event

