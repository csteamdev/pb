﻿$PBExportHeader$w_importa_schede.srw
forward
global type w_importa_schede from w_cs_xx_principale
end type
type cb_cerca from commandbutton within w_importa_schede
end type
type dw_filtro from uo_std_dw within w_importa_schede
end type
type cb_importa from commandbutton within w_importa_schede
end type
type sle_categoria from singlelineedit within w_importa_schede
end type
type sle_impianto from singlelineedit within w_importa_schede
end type
type sle_ubicazione from singlelineedit within w_importa_schede
end type
type st_3 from statictext within w_importa_schede
end type
type st_2 from statictext within w_importa_schede
end type
type st_1 from statictext within w_importa_schede
end type
type dw_1 from uo_cs_xx_dw within w_importa_schede
end type
type cb_disconnetti_profilo from commandbutton within w_importa_schede
end type
type st_log_conn from statictext within w_importa_schede
end type
type cmb_profili from dropdownlistbox within w_importa_schede
end type
type cb_connetti_profilo from commandbutton within w_importa_schede
end type
type gb_1 from groupbox within w_importa_schede
end type
type gb_2 from groupbox within w_importa_schede
end type
type gb_3 from groupbox within w_importa_schede
end type
end forward

global type w_importa_schede from w_cs_xx_principale
integer width = 3342
integer height = 2044
string title = "Importa Schede"
event ue_load_dddw ( )
cb_cerca cb_cerca
dw_filtro dw_filtro
cb_importa cb_importa
sle_categoria sle_categoria
sle_impianto sle_impianto
sle_ubicazione sle_ubicazione
st_3 st_3
st_2 st_2
st_1 st_1
dw_1 dw_1
cb_disconnetti_profilo cb_disconnetti_profilo
st_log_conn st_log_conn
cmb_profili cmb_profili
cb_connetti_profilo cb_connetti_profilo
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
global w_importa_schede w_importa_schede

type variables
private:
	string app_name = "Importa Schede"
	string is_cod_cat_attrezzatura
	string is_cod_reparto
	string is_cod_area


boolean ib_connected = false

long il_rows, il_current_row
transaction sqlcb


int NORMAL = 0
int PROCESS = 1
int WARN = 2
int SUCCESS = 3
int INFO = 4


long NORMAL_COLOR = 16777215
long PROCESS_COLOR = 11599871
long WARN_COLOR = 10329599
long SUCCESS_COLOR = 12910532
long INFO_COLOR = 16777162
end variables

forward prototypes
public function boolean wf_leggi_profili_registro ()
public function boolean wf_connetti_profilo ()
public function integer wf_imposta_sqlcb (string as_key)
public subroutine wf_disconnetti_profilo ()
public function integer wf_cerca ()
public function integer wf_importa ()
public subroutine wf_log (string as_message)
public subroutine wf_stato_layout (boolean ab_connesso)
public subroutine wf_dw_log (string as_message, integer ai_level)
public function boolean wf_importa_attrezzatura (string as_cod_attrezzatura)
public function boolean wf_importa_tipi_manut (string as_cod_azienda, string as_cod_attrezzatura)
end prototypes

event ue_load_dddw();// Categoria Atterzzatura
f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_attrezzatura",sqlcb,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature","")
					  
// Tipologia Impianto
f_PO_LoadDDDW_DW(dw_filtro,"cod_reparto",sqlcb,&
                 "anag_reparti","cod_reparto","des_reparto", "")
end event

public function boolean wf_leggi_profili_registro ();/**
 * Stefano Pulze
 * 12/01/10
 *
 * Lista le chiavi di registro per la connessione al database
 **/
 
string ls_keys[]
int		li_ret

li_ret = RegistryKeys("HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\", ls_keys)
if li_ret = -1 then
	g_mb.messagebox(app_name, "Impossibile controllare chiavi di registro")
	return false
end if

for li_ret = 1 to upperbound(ls_keys[])
	if left(ls_keys[li_ret], 3) = "app" then
		cmb_profili.insertitem(ls_keys[li_ret], 1)
	end if
next
return true
end function

public function boolean wf_connetti_profilo ();string ls_profilo
ls_profilo = cmb_profili.Text

if isnull(ls_profilo) or ls_profilo = "" then return false
ls_profilo = "database_" + mid(ls_profilo, pos(ls_profilo, "_") + 1)

cb_connetti_profilo.enabled = false

st_log_conn.text = "Leggo valori profilo" + ls_profilo
if wf_imposta_sqlcb(ls_profilo) = 0 then
	
	try
		disconnect using sqlcb;
		connect using sqlcb;
		ib_connected = true
		st_log_conn.text = "Connesso al database"
		
		event ue_load_dddw()
	catch(RuntimeError ex)
		ib_connected = false
		st_log_conn.text = "Errore durante la connessione"
		g_mb.messagebox(app_name, "Errore durante la connessione al database.~r~n" + ex.text)
	end try
	
else
	st_log_conn.text = "Errore durante la lettura del registro"
	ib_connected = false
end if

// Abilito il pulsante solo se la connessione non è avvenuta
cb_connetti_profilo.enabled = true
wf_stato_layout(ib_connected)
return ib_connected
end function

public function integer wf_imposta_sqlcb (string as_key);/**
 * Stefano Pulze
 * 23-11-2009
 *
 * Leggo i paramentri dal registro e imposto la transazione,
 * il codice è copiato da quello usato dal client/server
 * CODICE COPIATO DALLA INTRANET! (uo_ptenda)
 */

string ls_logpass, ls_option, ls_reg_chiave_root
int li_risposta, ll_ret

sqlcb = create transaction

// -- Leggo i dati dal registro
ls_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"
li_risposta = registryget(ls_reg_chiave_root + as_key, "servername", sqlcb.ServerName)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: servername.", StopSign!)
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + as_key, "dbms", sqlcb.DBMS)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: dbms.", StopSign!)
	return -1
end if

//li_risposta = registryget(ls_reg_chiave_root + as_key, "database_intranet", sqlcb.Database)
//if li_risposta = -1 then
//	g_mb.messagebox(app_name, "Mancano le impostazione del database sul registro: database.", StopSign!)
//	return -1
//end if
//
li_risposta = registryget(ls_reg_chiave_root + as_key, "logid", sqlcb.LogId)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: logid.", StopSign!)
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + as_key, "logpass", ls_logpass)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: logpass.", StopSign!)
	return -1
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if sqlcb.DBMS <> "ODBC" then
	
	if isnull(ls_logpass) or ls_logpass="" then		
		
		g_mb.messagebox(app_name, "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", StopSign!)
		return -1	
		
	end if	
		
	n_cst_crypto luo_crypto
	luo_crypto = create n_cst_crypto
	ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
	destroy luo_crypto;
	if ll_ret < 0 then
		g_mb.messagebox(app_name, "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
					"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
					"- alcuni simboli.Modificare la password e riprovare", StopSign!)
		return -1
	end if
	
	sqlcb.LogPass = ls_logpass
	
end if

li_risposta = registryget(ls_reg_chiave_root + as_key, "userid", sqlcb.UserId)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: userid.", StopSign!)
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + as_key, "dbpass", sqlcb.DBPass)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: dbpass.", StopSign!)
	return -1
end if

li_risposta = registryget(ls_reg_chiave_root + as_key, "dbparm", sqlcb.DBParm)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazione del database sul registro: dbparm.", StopSign!)
	return -1
end if

//li_risposta = registryget(ls_reg_chiave_root + as_key, "option", ls_option)
//if li_risposta = -1 then
//	g_mb.messagebox(app_name, "Mancano le impostazione del database sul registro: option.", StopSign!)
//	return -1
//end if

sqlcb.dbparm = sqlcb.dbparm + ls_option

li_risposta = registryget(ls_reg_chiave_root + as_key, "lock", sqlcb.Lock)
if li_risposta = -1 then
	g_mb.messagebox(app_name, "Mancano le impostazioni del database sul registro: lock.", StopSign!)
	return -1
end if

li_risposta = Registryset(ls_reg_chiave_root + "profilocorrente", "appo", "1.2")

return 0
end function

public subroutine wf_disconnetti_profilo ();if ib_connected then
	try
		disconnect using sqlcb;
		ib_connected = false
		st_log_conn.text= "Non connesso"
	catch(RuntimeError ex)
		st_log_conn.text= "Errore durante la disconnessione"
		g_mb.messagebox(app_name, ex.text)
		ib_connected = true
	end try
	
	wf_stato_layout(ib_connected)
end if
end subroutine

public function integer wf_cerca ();string ls_cod_da, ls_cod_a, ls_cod_cat, ls_cod_rep, ls_sql
long	ll_rows, ll_i, ll_row
datastore lds_store

dw_filtro.accepttext()
ls_cod_da	= dw_filtro.getitemstring(1, "cod_attrezzatura_da")
ls_cod_a		= dw_filtro.getitemstring(1, "cod_attrezzatura_a")
ls_cod_cat	= dw_filtro.getitemstring(1, "cod_cat_attrezzatura")
ls_cod_rep	= dw_filtro.getitemstring(1, "cod_reparto")

ls_sql = "SELECT cod_azienda, cod_attrezzatura, descrizione FROM anag_attrezzature WHERE 1=1 "

// -- Cod attrezzatura
if not isnull(ls_cod_da) and ls_cod_da <> "" then
	if not isnull(ls_cod_a) and ls_cod_a <> "" then
		ls_sql += " and (cod_attrezzatura >= '" + ls_cod_da + "' and cod_attrezzatura <= '" + ls_cod_a + "') "
	else
		ls_sql += " and cod_attrezzatura >= '" + ls_cod_da + "' "
	end if
elseif isnull(ls_cod_a) and not isnull(ls_cod_a) then
	ls_sql += " and cod_attrezzatura <= '" + ls_cod_a + "' "
end if
// ----

// -- Cod reparto
if not isnull(ls_cod_rep) and ls_cod_rep <> "" then
	ls_sql += " and cod_reparto = '" + ls_cod_rep + "' "
end if

// -- Cod cat attrezzatura
if not isnull(ls_cod_cat) and ls_cod_cat <> "" then
	ls_sql += " and cod_cat_attrezzature = '" + ls_cod_cat + "' "
end if

if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(app_name, "Impossibile creare datastore")
	return -1
end if

lds_store.settransobject(sqlcb)

wf_log("Ricerca in corso...")
dw_1.setredraw(false)
dw_1.reset()
ll_rows = lds_store.retrieve()
if ll_rows > 0 then
	for ll_i = 1 to ll_rows
		
		ll_row = dw_1.insertrow(0)
		dw_1.setitem(ll_row, "cod_azienda", lds_store.getitemstring(ll_i, "cod_azienda"))
		dw_1.setitem(ll_row, "cod_attrezzatura", lds_store.getitemstring(ll_i, "cod_attrezzatura"))
		dw_1.setitem(ll_row, "des_attrezzatura", lds_store.getitemstring(ll_i, "descrizione"))
		
	next
end if
dw_1.setredraw(true)
wf_log("Ricerca terminata")
return 0
end function

public function integer wf_importa ();string ls_cod_ubicazione, ls_cod_impianto, ls_cod_categoria, ls_test

is_cod_area = sle_ubicazione.text
is_cod_reparto	= sle_impianto.text
is_cod_cat_attrezzatura	= sle_categoria.text

if (isnull(is_cod_area) or is_cod_area = "" ) or &
   (isnull(is_cod_reparto) or is_cod_reparto = "") or &
   (isnull(is_cod_cat_attrezzatura) or is_cod_cat_attrezzatura = "") then
	
	g_mb.messagebox(app_name, "Impostare tutti i paramentri (Ubicazione, Impianto, Categoria) prima di procede con l'importazione.")
	return -1
end if

// -- Controllo che i paramentri esistano
select cod_area_aziendale
into :ls_test
from tab_aree_aziendali
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_area_aziendale = :is_cod_area;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox(app_name, "L'ubicazione inserita non è valida")
	return -1
end if

select cod_reparto
into :ls_test
from anag_reparti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_reparto = :is_cod_reparto;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox(app_name, "Il Reparto inserito non è valido")
	return -1
end if

select cod_cat_attrezzature
into :ls_test
from tab_cat_attrezzature
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cat_attrezzature = :is_cod_cat_attrezzatura;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox(app_name, "Tipologia inserita non è valida")
	return -1
end if
// ----

// -- Filtro solo quelle marcate
dw_1.setredraw(false)
dw_1.setFilter("checked='S'")
dw_1.filter()
dw_1.setredraw(true)
// ----

// -- Importo
il_rows = dw_1.rowCount()
if il_rows < 1 then
	wf_log("Nessuna attrezzatura selezionata per l'importazione")
	dw_1.setredraw(false)
	dw_1.setFilter("")
	dw_1.filter()
	dw_1.setredraw(true)
	return 0
end if

wf_log(string(il_rows) + " attrezzature da importare")

for il_current_row = 1 to il_rows
	wf_dw_log("Analizzo", PROCESS)
	
	if(wf_importa_attrezzatura(dw_1.getitemstring(il_current_row, "cod_attrezzatura"))) then
		commit;
	else
		rollback;
	end if
next
// ----

return 1
end function

public subroutine wf_log (string as_message);st_log_conn.text  = as_message
yield()
end subroutine

public subroutine wf_stato_layout (boolean ab_connesso);cb_disconnetti_profilo.visible = ab_connesso

cmb_profili.visible = not ab_connesso
cb_connetti_profilo.visible = not ab_connesso

dw_filtro.enabled = ab_connesso
cb_cerca.enabled = ab_connesso
sle_ubicazione.enabled = ab_connesso
sle_impianto.enabled = ab_connesso
sle_categoria.enabled = ab_connesso
cb_importa.enabled = ab_connesso
end subroutine

public subroutine wf_dw_log (string as_message, integer ai_level);long ll_color

choose case ai_level
	case NORMAL
		ll_color = NORMAL_COLOR
		
	case PROCESS
		ll_color = PROCESS_COLOR
		
	case WARN
		ll_color = WARN_COLOR
		
	case SUCCESS
		ll_color = SUCCESS_COLOR
		
	case INFO
		ll_color = INFO_COLOR
end choose

dw_1.setredraw(false)
dw_1.setitem(il_current_row, "stato", as_message)
dw_1.setitem(il_current_row, "color", ll_color)
dw_1.setredraw(true)
yield()
end subroutine

public function boolean wf_importa_attrezzatura (string as_cod_attrezzatura);string ls_des_attrezzatura, ls_test, ls_cod_azienda

// -- Controllo che il codice non esista già nel DB
select descrizione
into :ls_test
from anag_attrezzature
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :as_cod_attrezzatura
using sqlca;
	
if sqlca.sqlcode < 0 then
	wf_dw_log("Errore nel controllo codice attrezzatura", WARN)
	g_mb.messagebox(app_name, sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 0 then
	wf_dw_log("Codice esistente", INFO)
	return false
end if
// ----

ls_cod_azienda = dw_1.getitemstring(il_current_row, "cod_azienda")

// -- Prelevo descrizione ed eventuali campi futuri devo essere aggiunti qui!
select descrizione
into :ls_des_attrezzatura
from anag_attrezzature
where 
	cod_azienda = :ls_cod_azienda and 
	cod_attrezzatura = :as_cod_attrezzatura
using sqlcb;
	
if sqlcb.sqlcode < 0 then
	wf_dw_log("Errore lettura attrezzatura", WARN)
	g_mb.messagebox(app_name, sqlcb.sqlerrtext)
	return false
elseif sqlcb.sqlcode = 100 then
	wf_dw_log("Attrezzatura non trovata", WARN)
	return false
elseif sqlcb.sqlcode = 0 then
	
	insert into anag_attrezzature (
		cod_azienda,
		cod_attrezzatura,
		cod_cat_attrezzature,
		descrizione,
		cod_reparto,
		cod_area_aziendale,
		unita_tempo,
		periodicita_revisione,
		utilizzo_scadenza
		)
	values (
		:s_cs_xx.cod_azienda,
		:as_cod_attrezzatura,
		:is_cod_cat_attrezzatura,
		:ls_des_attrezzatura,
		:is_cod_reparto,
		:is_cod_area,
		'E',
		1,
		'S')
	using sqlca;
	
	if sqlca.sqlcode < 0 then
		wf_dw_log("Errore inserimento attrezzatura", WARN)
		g_mb.messagebox(app_name, sqlca.sqlerrtext)
		return false
	end if
	
	wf_dw_log("Attrezzattura importata, proseguo...", PROCESS)
	
	if wf_importa_tipi_manut(ls_cod_azienda, as_cod_attrezzatura) then
		wf_dw_log("Completato", SUCCESS)
		return true
	else
		return false
	end if
	
end if
// ----
end function

public function boolean wf_importa_tipi_manut (string as_cod_azienda, string as_cod_attrezzatura);string 	ls_sql, ls_test, ls_cod_tipo_manutenzione, ls_flag_blocco, ls_des_tipo_manutenzione, &
			ls_flag_manutenzione, ls_periodicita, ls_modalita_esecuzione
decimal 	ld_frequenza, ld_tolleranza, ld_data_inizio_val_1, ld_data_inizio_val_2, &
			ld_data_fine_val_1, ld_data_fine_val_2
long		ll_rows, ll_i
datetime ldt_data_blocco, ldt_data_ultima_modifica,ldt_data_inizio_val_1, ldt_tempo_previsto
datastore lds_store

ls_sql = "SELECT cod_tipo_manutenzione, flag_blocco,  data_blocco, des_tipo_manutenzione, data_ultima_modifica, "
ls_sql += "flag_manutenzione, tempo_previsto, periodicita, frequenza, tolleranza, data_inizio_val_1, "
ls_sql += "data_fine_val_1, data_inizio_val_2, data_fine_val_2, modalita_esecuzione "
ls_sql += "FROM tab_tipi_manutenzione "
ls_sql += "WHERE cod_azienda='"+as_cod_azienda+"' AND cod_attrezzatura='"+as_cod_attrezzatura+"'"

if not f_crea_datastore(lds_store, ls_sql) then
	g_mb.messagebox(app_name, "Impossibile creare datastore per tab_tipi_manutenzioni")
	destroy lds_store
	return false
end if

lds_store.settransobject(sqlcb)
ll_rows = lds_store.retrieve()

if ll_rows < 1 then return true
for ll_i = 1 to ll_rows
	
	ls_cod_tipo_manutenzione = lds_store.getitemstring(ll_i, "cod_tipo_manutenzione")
	
	// controllo che non esista
	select cod_tipo_manutenzione
	into	:ls_test
	from	tab_tipi_manutenzione
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_attrezzatura = :as_cod_attrezzatura and
		cod_tipo_manutenzione = :ls_cod_tipo_manutenzione
	using sqlca;
	
	if sqlca.sqlcode < 0 then
		wf_dw_log("Errore in controllo codice tipologia", WARN)
		return false
	elseif sqlca.sqlcode = 0 then
		//wf_log("Tipologia già esistente", INFO)
		continue
	end if
	
	// -- Leggo campi dal datastore
	ls_flag_blocco = lds_store.getitemstring(ll_i, "flag_blocco")
	ldt_data_blocco = lds_store.getitemdatetime(ll_i, "data_blocco")
	ls_des_tipo_manutenzione = lds_store.getitemstring(ll_i, "des_tipo_manutenzione")
	ls_flag_manutenzione = lds_store.getitemstring(ll_i, "flag_manutenzione")
	ldt_data_ultima_modifica = lds_store.getitemdatetime(ll_i, "data_ultima_modifica")
	ldt_tempo_previsto = lds_store.getitemdatetime(ll_i, "tempo_previsto")
	ls_periodicita = lds_store.getitemstring(ll_i, "periodicita")
	ld_frequenza = lds_store.getitemdecimal(ll_i, "frequenza")
	ld_tolleranza = lds_store.getitemdecimal(ll_i, "tolleranza")
	ld_data_inizio_val_1 = lds_store.getitemdecimal(ll_i, "data_inizio_val_1")
	ld_data_inizio_val_2 = lds_store.getitemdecimal(ll_i, "data_inizio_val_2")
	ld_data_fine_val_1 = lds_store.getitemdecimal(ll_i, "data_fine_val_1")
	ld_data_fine_val_2 = lds_store.getitemdecimal(ll_i, "data_fine_val_2")
	ls_modalita_esecuzione = lds_store.getitemstring(ll_i, "modalita_esecuzione")
	// ----
	
	// -- Inserisco
	insert into tab_tipi_manutenzione (
		cod_azienda,
		cod_attrezzatura,
		cod_tipo_manutenzione,
		flag_blocco,
		data_blocco,
		des_tipo_manutenzione,
		flag_manutenzione,
		data_ultima_modifica,
		flag_ricambio_codificato,
		tempo_previsto,
		periodicita,
		frequenza,
		tolleranza,
		data_inizio_val_1,
		data_fine_val_1,
		data_inizio_val_2,
		data_fine_val_2,
		modalita_esecuzione)
	values (
		:s_cs_xx.cod_azienda,
		:as_cod_attrezzatura,
		:ls_cod_tipo_manutenzione,
		:ls_flag_blocco,
		:ldt_data_blocco,
		:ls_des_tipo_manutenzione,
		:ls_flag_manutenzione,
		:ldt_data_ultima_modifica,
		'N',
		:ldt_tempo_previsto,
		:ls_periodicita,
		:ld_frequenza,
		:ld_tolleranza,
		:ld_data_inizio_val_1,
		:ld_data_fine_val_1,
		:ld_data_inizio_val_2,
		:ld_data_fine_val_2,
		:ls_modalita_esecuzione
	)
	using sqlca;
	
	if sqlca.sqlcode = 0 then
		continue
	elseif sqlca.sqlcode < 0 then
		wf_dw_log("Errore inserimento tipologia", WARN)
		g_mb.messagebox(app_name, sqlca.sqlerrtext)
		destroy lds_store
		return false
	end if
	// ----
next

destroy lds_store
return true
end function

on w_importa_schede.create
int iCurrent
call super::create
this.cb_cerca=create cb_cerca
this.dw_filtro=create dw_filtro
this.cb_importa=create cb_importa
this.sle_categoria=create sle_categoria
this.sle_impianto=create sle_impianto
this.sle_ubicazione=create sle_ubicazione
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.dw_1=create dw_1
this.cb_disconnetti_profilo=create cb_disconnetti_profilo
this.st_log_conn=create st_log_conn
this.cmb_profili=create cmb_profili
this.cb_connetti_profilo=create cb_connetti_profilo
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cerca
this.Control[iCurrent+2]=this.dw_filtro
this.Control[iCurrent+3]=this.cb_importa
this.Control[iCurrent+4]=this.sle_categoria
this.Control[iCurrent+5]=this.sle_impianto
this.Control[iCurrent+6]=this.sle_ubicazione
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.dw_1
this.Control[iCurrent+11]=this.cb_disconnetti_profilo
this.Control[iCurrent+12]=this.st_log_conn
this.Control[iCurrent+13]=this.cmb_profili
this.Control[iCurrent+14]=this.cb_connetti_profilo
this.Control[iCurrent+15]=this.gb_1
this.Control[iCurrent+16]=this.gb_2
this.Control[iCurrent+17]=this.gb_3
end on

on w_importa_schede.destroy
call super::destroy
destroy(this.cb_cerca)
destroy(this.dw_filtro)
destroy(this.cb_importa)
destroy(this.sle_categoria)
destroy(this.sle_impianto)
destroy(this.sle_ubicazione)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_1)
destroy(this.cb_disconnetti_profilo)
destroy(this.st_log_conn)
destroy(this.cmb_profili)
destroy(this.cb_connetti_profilo)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

event close;call super::close;wf_disconnetti_profilo()
end event

event pc_setwindow;call super::pc_setwindow;wf_stato_layout(false)
wf_leggi_profili_registro()

dw_filtro.reset()
dw_filtro.insertrow(0)

//dw_filtro.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
end event

type cb_cerca from commandbutton within w_importa_schede
integer x = 411
integer y = 1140
integer width = 503
integer height = 100
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cerca"
end type

event clicked;wf_cerca()
end event

type dw_filtro from uo_std_dw within w_importa_schede
integer x = 46
integer y = 380
integer width = 891
integer height = 720
integer taborder = 40
string dataobject = "d_importa_schede_filtro"
boolean border = false
end type

type cb_importa from commandbutton within w_importa_schede
integer x = 411
integer y = 1780
integer width = 503
integer height = 100
integer taborder = 110
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Importa"
end type

event clicked;wf_importa()
end event

type sle_categoria from singlelineedit within w_importa_schede
integer x = 411
integer y = 1660
integer width = 503
integer height = 80
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_impianto from singlelineedit within w_importa_schede
integer x = 411
integer y = 1560
integer width = 503
integer height = 80
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ubicazione from singlelineedit within w_importa_schede
integer x = 411
integer y = 1460
integer width = 503
integer height = 80
integer taborder = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_importa_schede
integer x = 69
integer y = 1680
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Categoria:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_importa_schede
integer x = 69
integer y = 1580
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impianto:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_importa_schede
integer x = 69
integer y = 1480
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Ubicazione:"
boolean focusrectangle = false
end type

type dw_1 from uo_cs_xx_dw within w_importa_schede
integer x = 983
integer y = 320
integer width = 2309
integer height = 1600
integer taborder = 60
string dataobject = "d_importa_schede_attr"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_disconnetti_profilo from commandbutton within w_importa_schede
integer x = 69
integer y = 120
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Disconnetti"
end type

event clicked;wf_disconnetti_profilo()
end event

type st_log_conn from statictext within w_importa_schede
integer x = 1600
integer y = 120
integer width = 1646
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Non connesso"
alignment alignment = right!
boolean focusrectangle = false
end type

type cmb_profili from dropdownlistbox within w_importa_schede
integer x = 69
integer y = 120
integer width = 846
integer height = 1000
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_connetti_profilo from commandbutton within w_importa_schede
integer x = 937
integer y = 120
integer width = 320
integer height = 80
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Connetti"
end type

event clicked;wf_connetti_profilo()
end event

type gb_1 from groupbox within w_importa_schede
integer x = 23
integer y = 20
integer width = 3269
integer height = 240
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = " Profilo "
end type

type gb_2 from groupbox within w_importa_schede
integer x = 23
integer y = 300
integer width = 937
integer height = 1020
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Filtri"
end type

type gb_3 from groupbox within w_importa_schede
integer x = 23
integer y = 1340
integer width = 937
integer height = 580
integer taborder = 130
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Importazione"
end type

