﻿$PBExportHeader$w_mod_codice_attr.srw
forward
global type w_mod_codice_attr from w_cs_xx_risposta
end type
type cb_chiudi from commandbutton within w_mod_codice_attr
end type
type cb_esegui from commandbutton within w_mod_codice_attr
end type
type dw_mod_codice_attr from datawindow within w_mod_codice_attr
end type
end forward

global type w_mod_codice_attr from w_cs_xx_risposta
integer width = 3131
integer height = 268
string title = "Cambio Codice -"
cb_chiudi cb_chiudi
cb_esegui cb_esegui
dw_mod_codice_attr dw_mod_codice_attr
end type
global w_mod_codice_attr w_mod_codice_attr

type variables
string is_attrezzatura
end variables

on w_mod_codice_attr.create
int iCurrent
call super::create
this.cb_chiudi=create cb_chiudi
this.cb_esegui=create cb_esegui
this.dw_mod_codice_attr=create dw_mod_codice_attr
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.dw_mod_codice_attr
end on

on w_mod_codice_attr.destroy
call super::destroy
destroy(this.cb_chiudi)
destroy(this.cb_esegui)
destroy(this.dw_mod_codice_attr)
end on

event pc_setwindow;call super::pc_setwindow;is_attrezzatura = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

title = "Cambio Codice - Attrezzatura " + is_attrezzatura

dw_mod_codice_attr.insertrow(0)
end event

type cb_chiudi from commandbutton within w_mod_codice_attr
integer x = 2286
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = -1

close(parent)
end event

type cb_esegui from commandbutton within w_mod_codice_attr
integer x = 2674
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;string ls_nuovo, ls_messaggio

uo_manutenzioni luo_mod_codice


dw_mod_codice_attr.accepttext()

ls_nuovo = dw_mod_codice_attr.getitemstring(1,"cod_nuovo")

if g_mb.messagebox("OMNIA","Proseguire con la modifica del codice attrezzatura?",question!,yesno!,2) = 2 then
	return -1
end if

setpointer(hourglass!)

luo_mod_codice = create uo_manutenzioni

if luo_mod_codice.uof_mod_codice_attr(is_attrezzatura,ls_nuovo,ls_messaggio) < 0 then
	rollback;
	destroy luo_mod_codice
	setpointer(arrow!)
	g_mb.messagebox("OMNIA",ls_messaggio,stopsign!)
else
	commit;
	destroy luo_mod_codice
	setpointer(arrow!)
	g_mb.messagebox("OMNIA","Attrezzatura " + is_attrezzatura + " modificata in " + ls_nuovo,information!)
	s_cs_xx.parametri.parametro_d_1 = 0
	close(parent)
end if
end event

type dw_mod_codice_attr from datawindow within w_mod_codice_attr
integer x = 23
integer y = 20
integer width = 3040
integer height = 120
integer taborder = 10
string dataobject = "d_mod_codice_attr"
boolean livescroll = true
end type

