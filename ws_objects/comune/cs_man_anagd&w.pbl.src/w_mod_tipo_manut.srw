﻿$PBExportHeader$w_mod_tipo_manut.srw
forward
global type w_mod_tipo_manut from w_cs_xx_risposta
end type
type cbx_1 from checkbox within w_mod_tipo_manut
end type
type cb_chiudi from commandbutton within w_mod_tipo_manut
end type
type cb_zero from commandbutton within w_mod_tipo_manut
end type
type cb_tutti from commandbutton within w_mod_tipo_manut
end type
type dw_elenco_attr from datawindow within w_mod_tipo_manut
end type
type cb_esegui from commandbutton within w_mod_tipo_manut
end type
type dw_mod_tipo_manut from datawindow within w_mod_tipo_manut
end type
end forward

global type w_mod_tipo_manut from w_cs_xx_risposta
integer width = 2217
integer height = 1612
string title = "Modifica Tipologia -"
cbx_1 cbx_1
cb_chiudi cb_chiudi
cb_zero cb_zero
cb_tutti cb_tutti
dw_elenco_attr dw_elenco_attr
cb_esegui cb_esegui
dw_mod_tipo_manut dw_mod_tipo_manut
end type
global w_mod_tipo_manut w_mod_tipo_manut

type variables
string is_attrezzatura, is_tipo_manut
end variables

forward prototypes
public function integer wf_elenco ()
end prototypes

public function integer wf_elenco ();long	 ll_riga

string ls_codice, ls_descrizione


declare attr cursor for
select cod_attrezzatura
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_manutenzione = :is_tipo_manut and
		 cod_attrezzatura <> :is_attrezzatura
order by cod_attrezzatura;
		 
open attr;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in open cursore attr: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	
	fetch attr
	into  :ls_codice;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("OMNIA","Errore in fetch cursore attr: " + sqlca.sqlerrtext,stopsign!)
		close attr;
		return -1
	elseif sqlca.sqlcode = 100 then
		close attr;
		exit
	end if
	
	select descrizione
	into   :ls_descrizione
	from   anag_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_attrezzatura = :ls_codice;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA","Errore in lettura descrizione attrezzatura " + ls_codice + ": " + sqlca.sqlerrtext,stopsign!)
		close attr;
		return -1
	end if
	
	ll_riga = dw_elenco_attr.insertrow(0)
	
	dw_elenco_attr.setitem(ll_riga,"nome",ls_codice)
	
	dw_elenco_attr.setitem(ll_riga,"descrizione",ls_descrizione)
	
	dw_elenco_attr.setitem(ll_riga,"selezione","N")
	
loop

return 0
end function

on w_mod_tipo_manut.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.cb_chiudi=create cb_chiudi
this.cb_zero=create cb_zero
this.cb_tutti=create cb_tutti
this.dw_elenco_attr=create dw_elenco_attr
this.cb_esegui=create cb_esegui
this.dw_mod_tipo_manut=create dw_mod_tipo_manut
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.cb_chiudi
this.Control[iCurrent+3]=this.cb_zero
this.Control[iCurrent+4]=this.cb_tutti
this.Control[iCurrent+5]=this.dw_elenco_attr
this.Control[iCurrent+6]=this.cb_esegui
this.Control[iCurrent+7]=this.dw_mod_tipo_manut
end on

on w_mod_tipo_manut.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.cb_chiudi)
destroy(this.cb_zero)
destroy(this.cb_tutti)
destroy(this.dw_elenco_attr)
destroy(this.cb_esegui)
destroy(this.dw_mod_tipo_manut)
end on

event pc_setwindow;call super::pc_setwindow;is_attrezzatura = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

is_tipo_manut = s_cs_xx.parametri.parametro_s_2

setnull(s_cs_xx.parametri.parametro_s_2)

title = "Modifica Tipologia - Attrezzatura " + is_attrezzatura + " Tipologia Manutenzione " + is_tipo_manut

dw_mod_tipo_manut.insertrow(0)

string ls_descrizione

select des_tipo_manutenzione
into   :ls_descrizione
from   tab_tipi_manutenzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_attrezzatura = :is_attrezzatura and
		 cod_tipo_manutenzione = :is_tipo_manut;
		 
if sqlca.sqlcode = 0 and not isnull(ls_descrizione) then
	dw_mod_tipo_manut.setitem(1,"des_nuovo",ls_descrizione)
end if

wf_elenco()
end event

type cbx_1 from checkbox within w_mod_tipo_manut
integer x = 869
integer y = 1412
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Blocca Attuale"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

type cb_chiudi from commandbutton within w_mod_tipo_manut
integer x = 1349
integer y = 1400
integer width = 398
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = -1

close(parent)
end event

type cb_zero from commandbutton within w_mod_tipo_manut
integer x = 434
integer y = 1400
integer width = 398
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;long ll_i

for ll_i = 1 to dw_elenco_attr.rowcount()
	dw_elenco_attr.setitem(ll_i,"selezione","N")
next
end event

type cb_tutti from commandbutton within w_mod_tipo_manut
integer x = 23
integer y = 1400
integer width = 398
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selez. Tutti"
end type

event clicked;long ll_i

for ll_i = 1 to dw_elenco_attr.rowcount()
	dw_elenco_attr.setitem(ll_i,"selezione","S")
next
end event

type dw_elenco_attr from datawindow within w_mod_tipo_manut
integer x = 23
integer y = 240
integer width = 2126
integer height = 1140
integer taborder = 30
string title = "none"
string dataobject = "d_elenco_attr"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_esegui from commandbutton within w_mod_tipo_manut
integer x = 1760
integer y = 1400
integer width = 398
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;boolean lb_blocco

long    ll_num, ll_i

string  ls_nuovo, ls_messaggio, ls_des_nuovo, ls_attrezzature[]

uo_manutenzioni luo_mod_codice


dw_mod_tipo_manut.accepttext()

lb_blocco = cbx_1.checked

ls_nuovo = dw_mod_tipo_manut.getitemstring(1,"cod_nuovo")

ls_des_nuovo = dw_mod_tipo_manut.getitemstring(1,"des_nuovo")

if g_mb.messagebox("OMNIA","Proseguire con la modifica del tipo manutenzione?",question!,yesno!,2) = 2 then
	return -1
end if

setpointer(hourglass!)

ll_num = 1

ls_attrezzature[ll_num] = is_attrezzatura

for ll_i = 1 to dw_elenco_attr.rowcount()
	
	if dw_elenco_attr.getitemstring(ll_i,"selezione") = "S" then
		ll_num ++
		ls_attrezzature[ll_num] = dw_elenco_attr.getitemstring(ll_i,"nome")
	end if
	
next

for ll_i = 1 to ll_num
	
	luo_mod_codice = create uo_manutenzioni

	if luo_mod_codice.uof_mod_tipo_manut(ls_attrezzature[ll_i],is_tipo_manut,ls_nuovo,ls_des_nuovo,lb_blocco,ls_messaggio) < 0 then
		rollback;
		destroy luo_mod_codice
		setpointer(arrow!)
		g_mb.messagebox("OMNIA",ls_messaggio,stopsign!)
		return -1
	end if
	
next

commit;

destroy luo_mod_codice

setpointer(arrow!)

g_mb.messagebox("OMNIA","Tipo manutenzione " + is_tipo_manut + " modificato in " + ls_nuovo,information!)

s_cs_xx.parametri.parametro_d_1 = 0

close(parent)
end event

type dw_mod_tipo_manut from datawindow within w_mod_tipo_manut
integer x = 23
integer y = 20
integer width = 2126
integer height = 200
integer taborder = 20
string title = "none"
string dataobject = "d_mod_tipo_manut"
boolean livescroll = true
end type

