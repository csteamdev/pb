﻿$PBExportHeader$w_note_modelli.srw
forward
global type w_note_modelli from w_cs_xx_principale
end type
type dw_note_modelli_lista from uo_cs_xx_dw within w_note_modelli
end type
type dw_note_modelli_det from uo_cs_xx_dw within w_note_modelli
end type
end forward

global type w_note_modelli from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2373
integer height = 1344
string title = "MODELLI NOTE"
dw_note_modelli_lista dw_note_modelli_lista
dw_note_modelli_det dw_note_modelli_det
end type
global w_note_modelli w_note_modelli

type variables
boolean ib_in_new
end variables

on w_note_modelli.create
int iCurrent
call super::create
this.dw_note_modelli_lista=create dw_note_modelli_lista
this.dw_note_modelli_det=create dw_note_modelli_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_note_modelli_lista
this.Control[iCurrent+2]=this.dw_note_modelli_det
end on

on w_note_modelli.destroy
call super::destroy
destroy(this.dw_note_modelli_lista)
destroy(this.dw_note_modelli_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_note_modelli_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_note_modelli_det.set_dw_options(sqlca,dw_note_modelli_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_note_modelli_lista
end event

type dw_note_modelli_lista from uo_cs_xx_dw within w_note_modelli
integer x = 23
integer y = 20
integer width = 2286
integer height = 500
integer taborder = 10
string dataobject = "d_note_modelli_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;ib_in_new = true
end event

event pcd_retrieve;call super::pcd_retrieve;long	l_error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_modify;call super::pcd_modify;ib_in_new = false
end event

event pcd_save;call super::pcd_save;ib_in_new = false
end event

event pcd_view;call super::pcd_view;ib_in_new = false
end event

type dw_note_modelli_det from uo_cs_xx_dw within w_note_modelli
integer x = 23
integer y = 540
integer width = 2286
integer height = 680
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_note_modelli_det"
borderstyle borderstyle = styleraised!
end type

