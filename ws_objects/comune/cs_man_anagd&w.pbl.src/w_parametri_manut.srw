﻿$PBExportHeader$w_parametri_manut.srw
$PBExportComments$Finestra Parametri Manutenzione
forward
global type w_parametri_manut from w_cs_xx_principale
end type
type dw_parametri_manutenzioni from uo_cs_xx_dw within w_parametri_manut
end type
end forward

global type w_parametri_manut from w_cs_xx_principale
integer width = 2574
integer height = 1756
string title = "Parametri Manutenzioni"
boolean minbox = false
boolean maxbox = false
dw_parametri_manutenzioni dw_parametri_manutenzioni
end type
global w_parametri_manut w_parametri_manut

on w_parametri_manut.create
int iCurrent
call super::create
this.dw_parametri_manutenzioni=create dw_parametri_manutenzioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parametri_manutenzioni
end on

on w_parametri_manut.destroy
call super::destroy
destroy(this.dw_parametri_manutenzioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_parametri_manutenzioni.set_dw_key("cod_azienda")

dw_parametri_manutenzioni.set_dw_options(sqlca, &
										pcca.null_object, &
										c_retrieveonopen, &
										c_default)				
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_det_ven_rif", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")		
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_det_ven_ricambi", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_det_ven_regis", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_lista_dist", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")				
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_lista_dist", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 " cod_azienda = '" + s_cs_xx.cod_azienda + "' ")			
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_lista_rda", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")				
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_lista_rda", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 " cod_azienda = '" + s_cs_xx.cod_azienda + "' ")		
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_tipo_lista_rda_2", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")				
					  
f_po_loaddddw_dw(dw_parametri_manutenzioni, &
                 "cod_lista_rda_2", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 " cod_azienda = '" + s_cs_xx.cod_azienda + "' ")											  
end event

type dw_parametri_manutenzioni from uo_cs_xx_dw within w_parametri_manut
integer width = 2501
integer height = 1604
integer taborder = 10
string dataobject = "d_parametri_manutenzioni"
boolean border = false
end type

event pcd_new;call super::pcd_new;this.setitem(1, "flag_gest_manutenzione","N")
setitem(getrow(),"flag_includi_festivi","N")

dw_parametri_manutenzioni.object.b_ric_divisioni.enabled = true
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)
	
if ll_errore < 0 then
	pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
next	
end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_tipo_lista_dist"
		
	f_po_loaddddw_dw(dw_parametri_manutenzioni, &
						  "cod_lista_dist", &
						  sqlca, &
						  "tes_liste_dist", &
						  "cod_lista_dist", &
						  "des_lista_dist", &
						  " cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")							
		
end choose
end event

event buttonclicked;call super::buttonclicked;if row>0 then

	choose case dwo.name
		case "b_ric_divisioni"
			if dwo.enabled = "0" then return
			
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = dw_parametri_manutenzioni
//			s_cs_xx.parametri.parametro_s_1 = "cod_divisione"
//			if not isvalid(w_divisioni_ricerca) then
//				window_open(w_divisioni_ricerca, 0)
//			end if
//			w_divisioni_ricerca.show()
			guo_ricerca.uof_ricerca_divisione(dw_parametri_manutenzioni, "cod_divisione")
			
	end choose
	
end if
end event

event pcd_delete;call super::pcd_delete;dw_parametri_manutenzioni.object.b_ric_divisioni.enabled = false
end event

event pcd_modify;call super::pcd_modify;dw_parametri_manutenzioni.object.b_ric_divisioni.enabled = true
end event

event pcd_view;call super::pcd_view;dw_parametri_manutenzioni.object.b_ric_divisioni.enabled = false
end event

