﻿$PBExportHeader$w_report_attrezzature_barcode.srw
forward
global type w_report_attrezzature_barcode from w_cs_xx_principale
end type
type tab_1 from tab within w_report_attrezzature_barcode
end type
type tabpage_1 from userobject within tab_1
end type
type dw_ricerca from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_ricerca dw_ricerca
end type
type tabpage_2 from userobject within tab_1
end type
type cb_stampa from commandbutton within tabpage_2
end type
type dw_report from uo_cs_xx_dw within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_stampa cb_stampa
dw_report dw_report
end type
type tab_1 from tab within w_report_attrezzature_barcode
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
end forward

global type w_report_attrezzature_barcode from w_cs_xx_principale
integer width = 3163
integer height = 1740
string title = "Report Attrezzature"
tab_1 tab_1
end type
global w_report_attrezzature_barcode w_report_attrezzature_barcode

type variables
private:
	string is_datawindow[]
	
	string is_current_printer
	string is_zebra_printer
	boolean ib_printer_changed = false
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

public subroutine wf_report ();/**
 * stefanop
 * 26/01/2016
 *
 **/
 
string ls_sql, ls_cod_attrezzatura_da, ls_cod_attrezzatura_a, ls_cod_reparto, ls_cod_cat_attrezzature, ls_dataobject
datawindow ldw_ricerca, ldw_report
 
// Recupero referenza per scrivere meno
ldw_ricerca = tab_1.tabpage_1.dw_ricerca
ldw_report = tab_1.tabpage_2.dw_report
ldw_ricerca.accepttext()

ls_dataobject = is_datawindow[ldw_ricerca.getitemnumber(1, "flag_tipo_report")]
ls_cod_attrezzatura_da = ldw_ricerca.getitemstring(1, "cod_attrezzatura_da")
ls_cod_attrezzatura_a = ldw_ricerca.getitemstring(1, "cod_attrezzatura_a")
ls_cod_reparto = ldw_ricerca.getitemstring(1, "cod_reparto")
ls_cod_cat_attrezzature = ldw_ricerca.getitemstring(1, "cod_cat_attrezzature")

// Inizio creazione SQL
ldw_report.dataobject = ls_dataobject
ldw_report.settransobject(sqlca)
ldw_report.setredraw(false)
ls_sql = ldw_report.GetSQLSelect()

ls_sql += " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "'"

if g_str.isnotempty(ls_cod_attrezzatura_da) then
	ls_sql += " AND cod_attrezzatura >= '" + ls_cod_attrezzatura_da + "'"
end if

if g_str.isnotempty(ls_cod_attrezzatura_a) then
	ls_sql += " AND cod_attrezzatura <= '" + ls_cod_attrezzatura_a  + "'"
end if

if g_str.isnotempty(ls_cod_reparto) then
	ls_sql += " AND cod_reparto >= '" + ls_cod_reparto + "'"
end if

if g_str.isnotempty(ls_cod_cat_attrezzature) then
	ls_sql += " AND cod_cat_attrezzature >= '" + ls_cod_cat_attrezzature + "'"
end if

if ldw_report.setSQLSelect(ls_sql) < 0 then
	g_mb.error("Errore durante l'applicazione della query di selezione. Qualche parametro non valido.")
	return
end if


if ldw_report.retrieve() >= 0 then
	tab_1.selecttab(2)	
end if

ldw_report.setredraw(true)
end subroutine

on w_report_attrezzature_barcode.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_report_attrezzature_barcode.destroy
call super::destroy
destroy(this.tab_1)
end on

event resize;call super::resize;tab_1.width = newwidth - 60
tab_1.height = newheight - 40

tab_1.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;
// Imposto array datawindow disponibili
is_datawindow[1] = "d_report_attrezzature_barcode_1"

set_w_options(c_closenosave + c_autoposition)

// Imposto ricerca
tab_1.tabpage_1.dw_ricerca.insertrow(0)

tab_1.tabpage_2.dw_report.ib_dw_report = true
tab_1.tabpage_2.dw_report.ib_dw_detail = true

tab_1.tabpage_2.dw_report.set_dw_options(sqlca, pcca.null_object,  c_nomodify + &
                         c_nodelete + c_newonopen + c_disableCC, c_nohighlightselected + &
                         c_nocursorrowpointer + c_nocursorrowfocusrect )
								 
iuo_dw_main = tab_1.tabpage_2.dw_report


// Stampante Zebra
if guo_functions.uof_get_zebra_printer(is_current_printer, is_zebra_printer) then
	
	if guo_functions.uof_imposta_stampante(is_zebra_printer) then
		tab_1.tabpage_1.dw_ricerca.object.t_printer.text = "Stampante: " + is_zebra_printer
		ib_printer_changed = true
	else
		ib_printer_changed = false
		tab_1.tabpage_1.dw_ricerca.object.t_printer.text = "Stampante: " + is_current_printer
		g_mb.error("Errore durante l'impostazione della stampante Zebra '" + g_str.safe(is_zebra_printer) + "'.~r~nVerificare che il nome sia corretto e riprovare (parametro utente ZPN).") 
	end if
	
end if


end event

event close;call super::close;if ib_printer_changed then
	guo_functions.uof_imposta_stampante(is_current_printer)
end if
end event

type tab_1 from tab within w_report_attrezzature_barcode
event ue_resize ( )
integer x = 18
integer y = 16
integer width = 3017
integer height = 1584
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

event ue_resize();tabpage_2.dw_report.width = tabpage_2.width - 40
tabpage_2.dw_report.height = tabpage_2.height - 210
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2981
integer height = 1456
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
end type

on tabpage_1.create
this.dw_ricerca=create dw_ricerca
this.Control[]={this.dw_ricerca}
end on

on tabpage_1.destroy
destroy(this.dw_ricerca)
end on

type dw_ricerca from datawindow within tabpage_1
integer x = 18
integer y = 16
integer width = 2798
integer height = 788
integer taborder = 20
string title = "none"
string dataobject = "d_report_attrezzature_barcode_lista"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
		
	case "b_ricerca_attrezzatura_da"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca, "cod_attrezzatura_da")
		
		
	case "b_ricerca_attrezzatura_a"
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca, "cod_attrezzatura_a")
		
	case "b_ricerca_reparto"
		guo_ricerca.uof_ricerca_reparto(dw_ricerca, "cod_reparto")
		
	case "b_ricerca_categoria"
		guo_ricerca.uof_ricerca_cat_attrezzature( dw_ricerca, "cod_cat_attrezzature")
		
	case "b_ricerca"
		wf_report()
		
		
end choose
end event

event itemchanged;choose case dwo.name
	case "cod_attrezzatura_da"
		if g_str.isempty(getitemstring(row, "cod_attrezzatura_a")) then
			setitem(row, "cod_attrezzatura_a", data)
		end if
end choose
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2981
integer height = 1456
long backcolor = 12632256
string text = "Report"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_stampa cb_stampa
dw_report dw_report
end type

on tabpage_2.create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
this.Control[]={this.cb_stampa,&
this.dw_report}
end on

on tabpage_2.destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

type cb_stampa from commandbutton within tabpage_2
integer x = 37
integer y = 32
integer width = 640
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Stampa"
end type

event clicked;dw_report.triggerevent("pcd_print")

//w_report_attrezzature_barcode.triggerevent("pc_print")
end event

type dw_report from uo_cs_xx_dw within tabpage_2
integer x = 18
integer y = 176
integer width = 2597
integer height = 1168
integer taborder = 11
string dataobject = "d_report_attrezzature_barcode_1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

