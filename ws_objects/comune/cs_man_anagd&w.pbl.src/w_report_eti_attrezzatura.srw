﻿$PBExportHeader$w_report_eti_attrezzatura.srw
$PBExportComments$Finestra Composizione Etichette Attrezzature
forward
global type w_report_eti_attrezzatura from w_cs_xx_principale
end type
type st_3 from statictext within w_report_eti_attrezzatura
end type
type st_1 from statictext within w_report_eti_attrezzatura
end type
type st_2 from statictext within w_report_eti_attrezzatura
end type
type cb_4 from commandbutton within w_report_eti_attrezzatura
end type
type em_date from editmask within w_report_eti_attrezzatura
end type
type dw_report_eti_attrezzatura from uo_cs_xx_dw within w_report_eti_attrezzatura
end type
type em_2 from editmask within w_report_eti_attrezzatura
end type
type em_1 from editmask within w_report_eti_attrezzatura
end type
type em_3 from editmask within w_report_eti_attrezzatura
end type
type st_4 from statictext within w_report_eti_attrezzatura
end type
type dw_report_eti_attrezzatura_nc from uo_cs_xx_dw within w_report_eti_attrezzatura
end type
type dw_report_eti_attrezzatura_sec from uo_cs_xx_dw within w_report_eti_attrezzatura
end type
type dw_folder from u_folder within w_report_eti_attrezzatura
end type
end forward

global type w_report_eti_attrezzatura from w_cs_xx_principale
integer width = 1929
integer height = 1624
string title = "Etichetta Attrezzatura"
st_3 st_3
st_1 st_1
st_2 st_2
cb_4 cb_4
em_date em_date
dw_report_eti_attrezzatura dw_report_eti_attrezzatura
em_2 em_2
em_1 em_1
em_3 em_3
st_4 st_4
dw_report_eti_attrezzatura_nc dw_report_eti_attrezzatura_nc
dw_report_eti_attrezzatura_sec dw_report_eti_attrezzatura_sec
dw_folder dw_folder
end type
global w_report_eti_attrezzatura w_report_eti_attrezzatura

type variables
string is_cod_attrezzatura
end variables

forward prototypes
public function integer wf_retrieve_3 ()
public function integer wf_retrieve_2 ()
public function integer wf_retrieve_1 ()
end prototypes

public function integer wf_retrieve_3 ();long ll_i, ll_y

ll_y = long(em_1.text)
save_on_close(c_socnosave)
dw_report_eti_attrezzatura_nc.change_dw_current()
dw_report_eti_attrezzatura_nc.triggerevent("pcd_search")
if ll_y < 1 then return 0
for ll_i = 1 to ll_y
	dw_report_eti_attrezzatura_nc.insertrow(1)
next
return 0
end function

public function integer wf_retrieve_2 ();long ll_i, ll_y

ll_y = long(em_1.text)
save_on_close(c_socnosave)
dw_report_eti_attrezzatura_sec.change_dw_current()
dw_report_eti_attrezzatura_sec.triggerevent("pcd_search")
if ll_y < 1 then return 0
for ll_i = 1 to ll_y
	dw_report_eti_attrezzatura_sec.insertrow(1)
next
return 0
end function

public function integer wf_retrieve_1 ();long ll_i, ll_y

ll_y = long(em_1.text)
save_on_close(c_socnosave)
dw_report_eti_attrezzatura.setfocus()
dw_report_eti_attrezzatura.change_dw_current()
dw_report_eti_attrezzatura.triggerevent("pcd_search")
if ll_y < 1 then return 0
for ll_i = 1 to ll_y
	dw_report_eti_attrezzatura.insertrow(1)
next
return 0

end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[ ]

dw_report_eti_attrezzatura.ib_dw_report = true
dw_report_eti_attrezzatura_sec.ib_dw_report = true
dw_report_eti_attrezzatura_nc.ib_dw_report = true

set_w_options(c_noenablepopup)




dw_report_eti_attrezzatura.set_dw_options(sqlca, &
														i_openparm, &
														c_nonew + &
														c_nodelete + &
														c_nomodify + &
														c_noretrieveonopen + &
														c_disableCC, &
														c_default)
dw_report_eti_attrezzatura_sec.set_dw_options(sqlca, &
														i_openparm, &
														c_nonew + &
														c_nodelete + &
														c_nomodify + &
														c_noretrieveonopen + &
														c_disableCC, &
														c_default)
dw_report_eti_attrezzatura_nc.set_dw_options(sqlca, &
														i_openparm, &
														c_nonew + &
														c_nodelete + &
														c_nomodify + &
														c_noretrieveonopen + &
														c_disableCC, &
														c_default)

l_objects[1] = dw_report_eti_attrezzatura
l_objects[2] = em_1
l_objects[3] = st_1
dw_folder.fu_AssignTab(1, "Primarie", l_Objects[])
l_objects[1] = dw_report_eti_attrezzatura_sec
l_objects[2] = em_2
l_objects[3] = st_2
dw_folder.fu_AssignTab(2, "Secondarie", l_Objects[])
l_objects[1] = dw_report_eti_attrezzatura_nc
l_objects[2] = em_3
l_objects[3] = st_3
dw_folder.fu_AssignTab(3, "Non Conformi", l_Objects[])

dw_folder.fu_FolderCreate(3,4)
dw_folder.fu_SelectTab(1)

em_date.text = string(today())
em_1.text = "0"
save_on_close(c_socnosave)
is_cod_attrezzatura = s_cs_xx.parametri.parametro_s_1

end event

on w_report_eti_attrezzatura.create
int iCurrent
call super::create
this.st_3=create st_3
this.st_1=create st_1
this.st_2=create st_2
this.cb_4=create cb_4
this.em_date=create em_date
this.dw_report_eti_attrezzatura=create dw_report_eti_attrezzatura
this.em_2=create em_2
this.em_1=create em_1
this.em_3=create em_3
this.st_4=create st_4
this.dw_report_eti_attrezzatura_nc=create dw_report_eti_attrezzatura_nc
this.dw_report_eti_attrezzatura_sec=create dw_report_eti_attrezzatura_sec
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.cb_4
this.Control[iCurrent+5]=this.em_date
this.Control[iCurrent+6]=this.dw_report_eti_attrezzatura
this.Control[iCurrent+7]=this.em_2
this.Control[iCurrent+8]=this.em_1
this.Control[iCurrent+9]=this.em_3
this.Control[iCurrent+10]=this.st_4
this.Control[iCurrent+11]=this.dw_report_eti_attrezzatura_nc
this.Control[iCurrent+12]=this.dw_report_eti_attrezzatura_sec
this.Control[iCurrent+13]=this.dw_folder
end on

on w_report_eti_attrezzatura.destroy
call super::destroy
destroy(this.st_3)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_4)
destroy(this.em_date)
destroy(this.dw_report_eti_attrezzatura)
destroy(this.em_2)
destroy(this.em_1)
destroy(this.em_3)
destroy(this.st_4)
destroy(this.dw_report_eti_attrezzatura_nc)
destroy(this.dw_report_eti_attrezzatura_sec)
destroy(this.dw_folder)
end on

type st_3 from statictext within w_report_eti_attrezzatura
integer x = 937
integer y = 1280
integer width = 599
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Etichette da saltare:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_eti_attrezzatura
integer x = 937
integer y = 1280
integer width = 599
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Etichette da saltare:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_report_eti_attrezzatura
integer x = 937
integer y = 1280
integer width = 599
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Etichette da saltare:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_report_eti_attrezzatura
integer x = 1509
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiorna"
end type

event clicked;string ls_cod_attrezzatura
long   ll_num_registrazione, ll_i, ll_num_righe
datetime   ldt_oggi, ldt_data
datastore lds_schede_manutenzioni

lds_schede_manutenzioni = Create DataStore
lds_schede_manutenzioni.DataObject = "d_datastore_schede_manutenzioni"
lds_schede_manutenzioni.SetTransObject(sqlca)

ll_num_righe = lds_schede_manutenzioni.Retrieve(s_cs_xx.cod_azienda,is_cod_attrezzatura)

ldt_oggi = datetime(em_date.text)
for ll_i = 1 to ll_num_righe
	if lds_schede_manutenzioni.object.data_registrazione[ll_i] > ldt_oggi then
		s_cs_xx.parametri.parametro_d_1 = lds_schede_manutenzioni.object.num_registrazione[ll_i]
		wf_retrieve_1()
		wf_retrieve_2()
		wf_retrieve_3()
		if ll_i > 1 then
			ll_i = ll_i -1
			ldt_data = lds_schede_manutenzioni.object.data_registrazione[ll_i]
			dw_report_eti_attrezzatura.Modify("st_data_ultima_revisione.Text='" + string(ldt_data,"dd/mm/yyyy")   + "'")
			dw_report_eti_attrezzatura_sec.Modify("st_data_ultima_revisione.Text='" + string(ldt_data,"dd/mm/yyyy")   + "'")
		end if
		return
	end if
next

end event

type em_date from editmask within w_report_eti_attrezzatura
integer x = 1074
integer y = 1420
integer width = 389
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

type dw_report_eti_attrezzatura from uo_cs_xx_dw within w_report_eti_attrezzatura
event pcd_retrieve pbm_custom60
integer x = 69
integer y = 140
integer width = 1737
integer height = 1120
integer taborder = 20
string dataobject = "d_eti_indent_attrezzatura"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_attrezzatura
long   l_Error

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")


l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_attrezzatura, &
						 s_cs_xx.parametri.parametro_d_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type em_2 from editmask within w_report_eti_attrezzatura
integer x = 1486
integer y = 1280
integer width = 274
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
string mask = "###"
boolean spin = true
string displaydata = "("
string minmax = "0~~999"
end type

type em_1 from editmask within w_report_eti_attrezzatura
integer x = 1486
integer y = 1280
integer width = 274
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
string mask = "###"
boolean spin = true
string displaydata = "("
string minmax = "0~~999"
end type

type em_3 from editmask within w_report_eti_attrezzatura
integer x = 1486
integer y = 1280
integer width = 274
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
alignment alignment = center!
string mask = "###"
boolean spin = true
string displaydata = "("
string minmax = "0~~999"
end type

type st_4 from statictext within w_report_eti_attrezzatura
integer x = 526
integer y = 1420
integer width = 521
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Data di Riferimento:"
boolean focusrectangle = false
end type

type dw_report_eti_attrezzatura_nc from uo_cs_xx_dw within w_report_eti_attrezzatura
event pcd_retrieve pbm_custom60
integer x = 69
integer y = 144
integer width = 1737
integer height = 1116
integer taborder = 10
string dataobject = "d_eti_attrezzatura_nc"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_attrezzatura
long   l_Error

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")


l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_attrezzatura)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_report_eti_attrezzatura_sec from uo_cs_xx_dw within w_report_eti_attrezzatura
integer x = 69
integer y = 140
integer width = 1714
integer height = 1120
integer taborder = 40
string dataobject = "d_eti_attrezzatura_sec"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_attrezzatura
long   l_Error

ls_cod_attrezzatura = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_attrezzatura")


l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_attrezzatura, &
						 s_cs_xx.parametri.parametro_d_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type dw_folder from u_folder within w_report_eti_attrezzatura
integer x = 23
integer y = 20
integer width = 1851
integer height = 1380
integer taborder = 30
end type

