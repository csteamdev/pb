﻿$PBExportHeader$w_ricerca_chiavi_doc.srw
$PBExportComments$Finestra ricerca chiavi
forward
global type w_ricerca_chiavi_doc from w_cs_xx_risposta
end type
type dw_request_chiavi_doc from uo_cs_xx_dw within w_ricerca_chiavi_doc
end type
type cb_ok from commandbutton within w_ricerca_chiavi_doc
end type
type dw_lista_chiavi from datawindow within w_ricerca_chiavi_doc
end type
type cb_chiudi from commandbutton within w_ricerca_chiavi_doc
end type
type cb_famiglia from commandbutton within w_ricerca_chiavi_doc
end type
type cb_chiave from commandbutton within w_ricerca_chiavi_doc
end type
type cb_des_chiave from commandbutton within w_ricerca_chiavi_doc
end type
end forward

global type w_ricerca_chiavi_doc from w_cs_xx_risposta
int Width=2789
int Height=1317
boolean TitleBar=true
string Title="Ricerca Chiavi Documenti"
dw_request_chiavi_doc dw_request_chiavi_doc
cb_ok cb_ok
dw_lista_chiavi dw_lista_chiavi
cb_chiudi cb_chiudi
cb_famiglia cb_famiglia
cb_chiave cb_chiave
cb_des_chiave cb_des_chiave
end type
global w_ricerca_chiavi_doc w_ricerca_chiavi_doc

type variables
long il_row, il_apertura
string is_cod_fam_chiavi, is_cod_chiave

end variables

on w_ricerca_chiavi_doc.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_request_chiavi_doc=create dw_request_chiavi_doc
this.cb_ok=create cb_ok
this.dw_lista_chiavi=create dw_lista_chiavi
this.cb_chiudi=create cb_chiudi
this.cb_famiglia=create cb_famiglia
this.cb_chiave=create cb_chiave
this.cb_des_chiave=create cb_des_chiave
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_request_chiavi_doc
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=dw_lista_chiavi
this.Control[iCurrent+4]=cb_chiudi
this.Control[iCurrent+5]=cb_famiglia
this.Control[iCurrent+6]=cb_chiave
this.Control[iCurrent+7]=cb_des_chiave
end on

on w_ricerca_chiavi_doc.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_request_chiavi_doc)
destroy(this.cb_ok)
destroy(this.dw_lista_chiavi)
destroy(this.cb_chiudi)
destroy(this.cb_famiglia)
destroy(this.cb_chiave)
destroy(this.cb_des_chiave)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_lista_chiavi, &
                 "cod_famiglia_chiavi", &
                 sqlca, &
                 "tab_fam_chiavi_doc", &
                 "cod_famiglia_chiavi", &
                 "des_famiglia_chiavi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_request_chiavi_doc, &
                 "cod_famiglia_chiavi", &
                 sqlca, &
                 "tab_fam_chiavi_doc", &
                 "cod_famiglia_chiavi", &
                 "des_famiglia_chiavi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_request_chiavi_doc.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
dw_lista_chiavi.settransobject(sqlca)									 


il_apertura = 1
end event

event open;call super::open;dw_request_chiavi_doc.setfocus()


end event

type dw_request_chiavi_doc from uo_cs_xx_dw within w_ricerca_chiavi_doc
int X=23
int Y=21
int Width=2698
int Height=101
int TabOrder=50
string DataObject="d_request_chiavi_doc"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_new;call super::pcd_new;setitem(1, "cod_chiave", "*")
end event

event ue_key;call super::ue_key;CHOOSE CASE key
	CASE KeyEnter!
		accepttext()
		il_row = this.getrow()
		 il_apertura = 0
	LONG  l_Error, ll_i, ll_y
	string ls_cod_fam_chiavi, ls_cod_chiave
	datetime ldt_oggi
	
	ldt_oggi = datetime(today())
	ll_i = this.getrow()
	if ll_i > 0 then
		ls_cod_fam_chiavi = dw_request_chiavi_doc.getitemstring(ll_i, "cod_famiglia_chiavi")
		ls_cod_chiave = dw_request_chiavi_doc.getitemstring(ll_i, "cod_chiave")
	else
		ls_cod_fam_chiavi = "%"
		ls_cod_chiave = "%"
	end if
	
	if isnull(ls_cod_fam_chiavi) or ls_cod_fam_chiavi = "" then ls_cod_fam_chiavi = "%"
	if isnull(ls_cod_chiave) or ls_cod_chiave = "" then ls_cod_chiave = "%"
	
	ll_i = len(ls_cod_chiave)
	for ll_y = 1 to ll_i
		if mid(ls_cod_chiave, ll_y, 1) = "*" then
			ls_cod_chiave = replace(ls_cod_chiave, ll_y, 1, "%")
		end if
	next
	if il_apertura = 1 then	//no retrieve on open manuale
			return
	END IF
	
	l_Error = dw_lista_chiavi.Retrieve(s_cs_xx.cod_azienda, ls_cod_fam_chiavi, ls_cod_chiave, ldt_oggi)
	dw_lista_chiavi.setfocus()
END CHOOSE


end event

type cb_ok from commandbutton within w_ricerca_chiavi_doc
int X=2355
int Y=1101
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_fam_chiavi, ls_cod_chiave
long ll_i

ll_i = dw_lista_chiavi.getrow()
if ll_i = 0 then
	g_mb.messagebox("Attenzione", "Non è stata scelta nessuna chiave")
	return
end if

ls_cod_fam_chiavi = dw_lista_chiavi.getitemstring(ll_i, "cod_famiglia_chiavi")
ls_cod_chiave = dw_lista_chiavi.getitemstring(dw_lista_chiavi.getrow(), "cod_chiave")

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_cod_fam_chiavi )
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ls_cod_chiave)


close(parent)





end event

type dw_lista_chiavi from datawindow within w_ricerca_chiavi_doc
event ue_key pbm_dwnkey
int X=23
int Y=121
int Width=2698
int Height=941
int TabOrder=60
boolean BringToTop=true
string DataObject="d_tab_chiavi_doc_search"
boolean Border=false
boolean VScrollBar=true
boolean LiveScroll=true
end type

event ue_key;
CHOOSE CASE key
	CASE KeyEnter!
		il_row = this.getrow()
		this.SelectRow(0, false)
		this.SelectRow(il_row, true)
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < this.rowcount() then
			il_row = this.getrow() + 1
			this.SelectRow(0, false)
			this.SelectRow(il_row, true)
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = this.getrow() - 1
			this.SelectRow(0, false)
			this.SelectRow(il_row, true)
		end if
END CHOOSE



end event

type cb_chiudi from commandbutton within w_ricerca_chiavi_doc
int X=1943
int Y=1101
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

type cb_famiglia from commandbutton within w_ricerca_chiavi_doc
int X=23
int Y=121
int Width=686
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="Famiglia"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_lista_chiavi.rowcount() > 0 then
	dw_lista_chiavi.setsort("cod_famiglia_chiavi A, cod_chiave A, des_chiave A")
	dw_lista_chiavi.sort()
end if
end event

type cb_chiave from commandbutton within w_ricerca_chiavi_doc
int X=718
int Y=121
int Width=682
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Cod. Chiave"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_lista_chiavi.rowcount() > 0 then
	dw_lista_chiavi.setsort(" cod_chiave A, cod_famiglia_chiavi A, des_chiave A")
	dw_lista_chiavi.sort()
end if


end event

type cb_des_chiave from commandbutton within w_ricerca_chiavi_doc
int X=1404
int Y=121
int Width=1203
int Height=81
int TabOrder=51
boolean BringToTop=true
string Text="Des. Chiave"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if dw_lista_chiavi.rowcount() > 0 then
	dw_lista_chiavi.setsort(" des_chiave A, cod_famiglia_chiavi A, cod_chiave A")
	dw_lista_chiavi.sort()
end if


end event

