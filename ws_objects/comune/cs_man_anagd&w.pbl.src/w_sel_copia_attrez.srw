﻿$PBExportHeader$w_sel_copia_attrez.srw
$PBExportComments$Finestra  per duplicare una attrezzatura
forward
global type w_sel_copia_attrez from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_sel_copia_attrez
end type
type cb_esegui from commandbutton within w_sel_copia_attrez
end type
type dw_sel_duplica_attrez from uo_cs_xx_dw within w_sel_copia_attrez
end type
end forward

global type w_sel_copia_attrez from w_cs_xx_risposta
integer width = 2094
integer height = 488
string title = "Richiesta Dati"
boolean minbox = true
boolean resizable = false
windowtype windowtype = main!
cb_annulla cb_annulla
cb_esegui cb_esegui
dw_sel_duplica_attrez dw_sel_duplica_attrez
end type
global w_sel_copia_attrez w_sel_copia_attrez

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_sel_duplica_attrez.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 							 
end event

on w_sel_copia_attrez.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_esegui=create cb_esegui
this.dw_sel_duplica_attrez=create dw_sel_duplica_attrez
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.dw_sel_duplica_attrez
end on

on w_sel_copia_attrez.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_esegui)
destroy(this.dw_sel_duplica_attrez)
end on

event clicked;call super::clicked;dw_sel_duplica_attrez.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "rs_cod_attrezzatura_da"
end event

type cb_annulla from commandbutton within w_sel_copia_attrez
integer x = 1303
integer y = 320
integer width = 357
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(w_sel_copia_attrez)
end event

type cb_esegui from commandbutton within w_sel_copia_attrez
integer x = 1691
integer y = 320
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_cod_attrezzatura_da, ls_cod_attrezzatura_a, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, &
		 ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_kit_ricambio, ls_cod_ricambio_alternativo, &
		 ls_des_ricambio_non_codificato, ls_modalita_esecuzione, ls_cod_tipo_ord_acq, ls_cod_tipo_off_acq, ls_cod_tipo_det_acq, &
		 ls_cod_tipo_movimento, ls_periodicita, ls_cod_primario, ls_cod_misura, &
		 ls_risorsa, ls_des_risorsa, ls_cod_versione, ls_des_ricambio, ls_cod_ricambio, ls_messaggio, ls_cod_lista_dist, &
		 ls_cod_tipo_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_cod_new, &
		 ls_des_new, ls_cod_azienda, ls_cod_attrezzatura, ls_cod_cat_attrezzature, ls_descrizione, ls_fabbricante, &
		 ls_modello, ls_utilizzo_scadenza, ls_cod_reparto, ls_certificata_fabbricante, ls_certificata_ce, &
			         ls_cod_inventario, ls_indice_capacita, ls_indice_affidabilita, ls_indice_manutenibilita,  &
			         ls_costo_orario_medio, ls_indice_costo, ls_valore_residuo, &  
			         ls_cod_prodotto, ls_stato_attuale, ls_vita_utile,  &
			         ls_cod_area_aziendale, ls_flag_manuale, ls_flag_duplicazione, ls_flag_conta_ore, ls_accessibilita,  &
			         ls_cod_utente, ls_flag_reg_man,    &
			        ls_reperibilita, ls_flag_primario, ls_flag_blocco, ls_note, &
			         ls_grado_precisione, ls_disp_ricamnbi, &   
			         ls_grado_precisione_um, ls_denominazione_ente, ls_qualita_attrezzatura, ls_cod_centro_costo, &
		 ls_descrizione_cu, ls_flag_blocco_cu, ls_des_blob_cu, ls_path_documento_cu, ls_num_matricola, ls_unita_tempo, &
		 ls_num_certificato_ente, ls_freq_guasti
												
datetime ldt_data_blocco, ldt_data_prima_attivazione, ldt_data_installazione, ldt_data_acquisto, ldt_data_blocco_cu, &
         ldt_data_certificato_ente

dec{4} ld_num_reg_lista, ld_num_versione, ld_num_edizione, ld_frequenza, ld_quan_ricambio, ld_costo_unitario_ricambio, &
		 ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso, ld_tempo_risorsa, ld_costo_risorsa, ld_prezzo_ricambio, &
		 ld_grado_precisione, ld_costo_orario_medio, ld_costo_acquisto, ld_vita_utile, ld_indice_capacita, ld_indice_affidabilita,&
		 ld_indice_manutenibilita, ld_indice_costo, ld_valore_residuo
		 
long ll_contatore, ll_prog_ricambio, ll_count, ll_freq_guasti, ll_costo_acquisto, ll_num_certificato_ente, ll_unita_tempo,&
ll_num_matricola, ll_periodicita_revisione, ll_prog_attrezzatura_cu, ll_num_protocollo_cu

blob lb_blob



dw_sel_duplica_attrez.accepttext()

ls_cod_attrezzatura_da = dw_sel_duplica_attrez.getitemstring(1, "rs_cod_attrezzatura_da")

ls_cod_new = dw_sel_duplica_attrez.getitemstring(1, "cod_attrezzatura_new")

ls_des_new = dw_sel_duplica_attrez.getitemstring(1, "des_attrezzatura_new")


if isnull(ls_cod_attrezzatura_da) then
	g_mb.messagebox("Omnia", "Il codice attrezzatura DA non può essere nullo")
	return
end if	

if isnull(ls_cod_new) then
	g_mb.messagebox("Omnia", "Il codice attrezzatura non può essere nullo")
	return	
end if	

if isnull(ls_des_new) then
	g_mb.messagebox("Omnia", "La descrizione attrezzatura non può essere nulla")
	return	
end if		

select count(*)
into   :ll_count
from   anag_attrezzature 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_attrezzatura = :ls_cod_new;
		 
if isnull(ll_count) then ll_count = 0

if ll_count > 0 then
	g_mb.messagebox("Omnia", "Esiste già un'attrezzatura con questo codice.")
	return		
end if

SELECT anag_attrezzature.cod_azienda,   
       anag_attrezzature.cod_attrezzatura,   
		 anag_attrezzature.cod_cat_attrezzature,   
		 anag_attrezzature.descrizione,   
		 anag_attrezzature.fabbricante,   
		 anag_attrezzature.modello,   
		 anag_attrezzature.num_matricola,   
		 anag_attrezzature.data_acquisto,   
 		 anag_attrezzature.periodicita_revisione,   
		 anag_attrezzature.unita_tempo,   
		 anag_attrezzature.utilizzo_scadenza,   
		 anag_attrezzature.cod_reparto,   
		 anag_attrezzature.certificata_fabbricante,   
		 anag_attrezzature.certificata_ce,   
		 anag_attrezzature.cod_inventario,   
		 anag_attrezzature.costo_orario_medio,   
		 anag_attrezzature.cod_prodotto,   
		 anag_attrezzature.cod_area_aziendale,   
		 anag_attrezzature.cod_utente,   
		 anag_attrezzature.reperibilita,   
		 anag_attrezzature.grado_precisione,   
		 anag_attrezzature.grado_precisione_um,   
		 anag_attrezzature.num_certificato_ente,   
		 anag_attrezzature.data_certificato_ente,   
	    anag_attrezzature.denominazione_ente ,   
		 anag_attrezzature.qualita_attrezzatura,   
		 anag_attrezzature.cod_centro_costo,   
		 anag_attrezzature.flag_primario,   
		 anag_attrezzature.flag_blocco,   
		 anag_attrezzature.data_blocco,   
		 anag_attrezzature.cod_versione,   
		 anag_attrezzature.note ,   
		 anag_attrezzature.data_prima_attivazione,   
		 anag_attrezzature.data_installazione,   
		 anag_attrezzature.flag_reg_man,   
       anag_attrezzature.flag_manuale,   
       anag_attrezzature.flag_duplicazione,   
       anag_attrezzature.flag_conta_ore,   
       anag_attrezzature.costo_acquisto,   
       anag_attrezzature.freq_guasti,   
       anag_attrezzature.accessibilita,   
       anag_attrezzature.disp_ricamnbi,   
       anag_attrezzature.stato_attuale
 into :ls_cod_azienda,   
		:ls_cod_attrezzatura,   
      :ls_cod_cat_attrezzature,   
      :ls_descrizione,   
      :ls_fabbricante,   
	   :ls_modello,   
	   :ls_num_matricola,   
		:ldt_data_acquisto,   
      :ll_periodicita_revisione,   
      :ls_unita_tempo,   
      :ls_utilizzo_scadenza,   
      :ls_cod_reparto,   
      :ls_certificata_fabbricante,   
      :ls_certificata_ce,   
      :ls_cod_inventario,   
      :ld_costo_orario_medio,   
      :ls_cod_prodotto,   
      :ls_cod_area_aziendale,   
      :ls_cod_utente,   
      :ls_reperibilita,   
      :ld_grado_precisione,   
      :ls_grado_precisione_um,   
      :ls_num_certificato_ente,   
      :ldt_data_certificato_ente,   
      :ls_denominazione_ente ,   
      :ls_qualita_attrezzatura,   
		:ls_cod_centro_costo,   
		:ls_flag_primario,   
		:ls_flag_blocco,   
		:ldt_data_blocco,   
		:ls_cod_versione,   
		:ls_note,   
		:ldt_data_prima_attivazione,   
		:ldt_data_installazione,   
		:ls_flag_reg_man,   
		:ls_flag_manuale,   
		:ls_flag_duplicazione,   
		:ls_flag_conta_ore,   
		:ld_costo_acquisto,   
		:ls_freq_guasti,   
		:ls_accessibilita,   
	   :ls_disp_ricamnbi,   
		:ls_stato_attuale
FROM  anag_attrezzature
where anag_attrezzature.cod_azienda = :s_cs_xx.cod_azienda and
      anag_attrezzature.cod_attrezzatura = :ls_cod_attrezzatura_da;
						 
string ls_appo
ls_appo = ""
						 
INSERT INTO anag_attrezzature  
         (  cod_azienda,   
            cod_attrezzatura,   
            cod_cat_attrezzature,   
            descrizione,   
            fabbricante,   
            modello,   
            num_matricola,   
            data_acquisto,   
            periodicita_revisione,   
            unita_tempo,   
            utilizzo_scadenza,   
            cod_reparto,   
            certificata_fabbricante,   
            certificata_ce,   
            cod_inventario,   
            costo_orario_medio,   
            cod_prodotto,   
            cod_area_aziendale,   
            cod_utente,   
            reperibilita,   
            grado_precisione,   
            grado_precisione_um,   
            num_certificato_ente,   
            data_certificato_ente,   
            denominazione_ente ,   
            qualita_attrezzatura,   
            cod_centro_costo,   
            flag_primario,   
            flag_blocco,   
            data_blocco,   
            cod_versione,   
            note ,   
            data_prima_attivazione,   
            data_installazione,   
            flag_reg_man,   
            flag_manuale,   
            flag_duplicazione,   
            flag_conta_ore,   
            costo_acquisto,   
            freq_guasti,   
            accessibilita,   
            disp_ricamnbi,   
            stato_attuale
)  
  VALUES ( :ls_cod_azienda,   
           :ls_cod_new,   
			         :ls_cod_cat_attrezzature,   
			         :ls_des_new,   
		            :ls_fabbricante,   
						:ls_modello,   
						:ls_num_matricola,   
						:ldt_data_acquisto,   
						:ll_periodicita_revisione,   
						:ls_unita_tempo,   
						:ls_utilizzo_scadenza,   
						:ls_cod_reparto,   
						:ls_certificata_fabbricante,   
						:ls_certificata_ce,   
						:ls_cod_inventario,   
						:ld_costo_orario_medio,   
						:ls_cod_prodotto,   
						:ls_cod_area_aziendale,   
						:ls_cod_utente,   
						:ls_reperibilita,   
						:ld_grado_precisione,   
						:ls_grado_precisione_um,   
						:ls_num_certificato_ente,   
						:ldt_data_certificato_ente,   
						:ls_denominazione_ente ,   
						:ls_qualita_attrezzatura,   
						:ls_cod_centro_costo,   
						:ls_flag_primario,   
						:ls_flag_blocco,   
						:ldt_data_blocco,   
						:ls_cod_versione,   
						:ls_note ,   
						:ldt_data_prima_attivazione,   
						:ldt_data_installazione,   
						:ls_flag_reg_man,   
						:ls_flag_manuale,   
						:ls_flag_duplicazione,   
						:ls_flag_conta_ore,   
						:ld_costo_acquisto,   
						:ls_freq_guasti,   
						:ls_accessibilita,   
						:ls_disp_ricamnbi,   
						:ls_stato_attuale
);

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'inserimento della nuova attrezzatura: " + sqlca.sqlerrtext)
	g_mb.messagebox( "", string(ll_periodicita_revisione))
	rollback;
	return -1	
end if

DECLARE cu_det_attrezzature CURSOR FOR  
 SELECT prog_attrezzatura,   
        descrizione,   
        num_protocollo,   
        flag_blocco,   
        data_blocco,   
        des_blob,   
        path_documento  
   FROM det_attrezzature  
  where cod_azienda = :s_cs_xx.cod_azienda and
	     cod_attrezzatura = :ls_cod_attrezzatura_da;
			 
open cu_det_attrezzature;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore: " + sqlca.sqlerrtext)
	rollback;
	return -1	
end if

do while 1 = 1 
	fetch cu_det_attrezzature into :ll_prog_attrezzatura_cu,   
	                               :ls_descrizione_cu,
											 :ll_num_protocollo_cu,
											 :ls_flag_blocco_cu,
											 :ldt_data_blocco_cu,
											 :ls_des_blob_cu,
											 :ls_path_documento_cu;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante la fetch del cursore: " + sqlca.sqlerrtext)
		rollback;
		return -1	
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	insert into det_attrezzature(cod_azienda,
	                             cod_attrezzatura,
										  prog_attrezzatura,   
							           descrizione,   
							           num_protocollo,   
							           flag_blocco,   
										  data_blocco,   
										  des_blob,   
										  path_documento)
								values (:s_cs_xx.cod_azienda,
								        :ls_cod_new,
										  :ll_prog_attrezzatura_cu,   
	                             :ls_descrizione_cu,
										  :ll_num_protocollo_cu,
  										  :ls_flag_blocco_cu,
										  :ldt_data_blocco_cu,
									     :ls_des_blob_cu,
										  :ls_path_documento_cu);
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "OMNIA", "Errore durante l'inserimento del dettaglio: " + sqlca.sqlerrtext)
		rollback;
		return -1	
	end if
	
	selectblob blob
	into       :lb_blob
	from       det_attrezzature
	where      cod_azienda = :s_cs_xx.cod_azienda and
              cod_attrezzatura = :ls_cod_attrezzatura_da and
			     prog_attrezzatura = :ll_prog_attrezzatura_cu;
				  
	updateblob det_attrezzature 
	set blob = :lb_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
              cod_attrezzatura = :ls_cod_new and
			     prog_attrezzatura = :ll_prog_attrezzatura_cu;
	
	
loop

close cu_det_attrezzature;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext)
	rollback;
	return -1	
end if

commit;

g_mb.messagebox("Omnia", "Duplicazione Eseguita con Successo!")
close(w_sel_copia_attrez)

end event

type dw_sel_duplica_attrez from uo_cs_xx_dw within w_sel_copia_attrez
integer x = 23
integer y = 20
integer width = 2057
integer height = 300
integer taborder = 10
string dataobject = "d_sel_copia_attrez"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;guo_ricerca.uof_set_response(parent)

choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_duplica_attrez,"rs_cod_attrezzatura_da")
end choose
end event

