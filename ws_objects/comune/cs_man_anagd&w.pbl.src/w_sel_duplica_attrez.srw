﻿$PBExportHeader$w_sel_duplica_attrez.srw
$PBExportComments$Finestra Manutenzioni
forward
global type w_sel_duplica_attrez from w_cs_xx_risposta
end type
type mle_idl from multilineedit within w_sel_duplica_attrez
end type
type dw_sel_duplica_tipiman_origine from datawindow within w_sel_duplica_attrez
end type
type dw_lista from datawindow within w_sel_duplica_attrez
end type
type dw_ricerca from uo_dw_search within w_sel_duplica_attrez
end type
type dw_sel_duplica_attrez from uo_cs_xx_dw within w_sel_duplica_attrez
end type
end forward

global type w_sel_duplica_attrez from w_cs_xx_risposta
integer width = 4279
integer height = 3080
string title = "Richiesta Dati"
boolean resizable = false
windowtype windowtype = main!
mle_idl mle_idl
dw_sel_duplica_tipiman_origine dw_sel_duplica_tipiman_origine
dw_lista dw_lista
dw_ricerca dw_ricerca
dw_sel_duplica_attrez dw_sel_duplica_attrez
end type
global w_sel_duplica_attrez w_sel_duplica_attrez

forward prototypes
public function integer wf_ricerca_attrezzature_dest (ref string as_errore)
public function integer wf_ricerca_tipologie_origine (string as_cod_attrezzatura, ref string as_errore)
public subroutine wf_set_idl (long al_row)
public function integer wf_duplica (ref string as_attrezz_saltate, ref string as_errore)
end prototypes

public function integer wf_ricerca_attrezzature_dest (ref string as_errore);string				ls_attrezzatura_da, ls_attrezzatura_a, ls_cod_categoria, ls_cod_area, ls_cod_reparto, ls_sql, ls_syntax, ls_errore, &
					ls_codice, ls_descrizione, ls_cod_attrezzatura_origine

long				ll_i, ll_riga

datastore		lds_lista



dw_lista.reset()
dw_ricerca.accepttext()

dw_sel_duplica_attrez.accepttext()

ls_cod_attrezzatura_origine = dw_sel_duplica_attrez.getitemstring(1, "rs_cod_attrezzatura_da")

if isnull(ls_cod_attrezzatura_origine) then
	as_errore = "Il codice attrezzatura Origine non può essere nullo!"
	return 1
end if	


ls_attrezzatura_da = dw_ricerca.getitemstring( 1, "attrezzatura_da")
ls_attrezzatura_a = dw_ricerca.getitemstring( 1, "attrezzatura_a")
ls_cod_categoria = dw_ricerca.getitemstring( 1, "cod_categoria")
ls_cod_area = dw_ricerca.getitemstring( 1, "cod_area")
ls_cod_reparto = dw_ricerca.getitemstring( 1, "cod_reparto")

ls_sql = " SELECT cod_attrezzatura, descrizione " + &
         " FROM   anag_attrezzature " + &
			" WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' " 

if not isnull(ls_cod_attrezzatura_origine) and ls_cod_attrezzatura_origine <> "" then
	ls_sql = ls_sql + " and cod_attrezzatura NOT IN ('" + ls_cod_attrezzatura_origine + "') "
end if

if not isnull(ls_attrezzatura_da) and ls_attrezzatura_da <> "" then
	ls_sql = ls_sql + " and cod_attrezzatura >= '" + ls_attrezzatura_da + "' "
end if

if not isnull(ls_attrezzatura_a) and ls_attrezzatura_a <> "" then
	ls_sql = ls_sql + " and cod_attrezzatura <= '" + ls_attrezzatura_a + "' "	
end if

if not isnull(ls_cod_categoria) and ls_cod_categoria <> "" then
	ls_sql = ls_sql + " and cod_cat_attrezzature = '" + ls_cod_categoria + "' "	
end if

if not isnull(ls_cod_area) and ls_cod_area <> "" then
	ls_sql = ls_sql + " and cod_area_aziendale = '" + ls_cod_area + "' "		
end if

if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
	ls_sql = ls_sql + " and cod_reparto = '" + ls_cod_reparto + "' "			
end if

ls_sql = ls_sql + " ORDER BY cod_attrezzatura ASC "
ls_syntax = sqlca.syntaxfromsql( ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	as_errore = "Errore durante l'impostazione sql:" + ls_errore
	return -1
end if

lds_lista = create datastore

lds_lista.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_lista
	as_errore = "Errore in impostazione select attrezzature: " + ls_errore
	return -1
end if

lds_lista.settransobject(sqlca)

if lds_lista.retrieve() = -1 then
	destroy lds_lista
	as_errore = "Errore in lettura dati attrezzature!"
	return -1
end if	

for ll_i = 1 to lds_lista.rowcount()
	ll_riga = dw_lista.insertrow( 0)
	ls_codice = lds_lista.getitemstring( ll_i, "cod_attrezzatura")
	ls_descrizione = lds_lista.getitemstring( ll_i, "descrizione")
	dw_lista.setitem( ll_i, "cod_attrezzatura", ls_codice)
	dw_lista.setitem( ll_i, "descrizione", ls_descrizione)
next

return 0

end function

public function integer wf_ricerca_tipologie_origine (string as_cod_attrezzatura, ref string as_errore);long			ll_tot



if isnull(as_cod_attrezzatura) or as_cod_attrezzatura="" then
	as_errore = "Selezionare l'attrezzatura di origine per la duplicazione delle tipologie!"
	return 1
end if


ll_tot = dw_sel_duplica_tipiman_origine.retrieve(s_cs_xx.cod_azienda, as_cod_attrezzatura)

if ll_tot < 0 then
	as_errore = "Errore in recupero Tipologie Manutenzioni dell'attrezzatura " + as_cod_attrezzatura
	return -1
	
elseif ll_tot=0 then
	as_errore = "L'attrezzatura " + as_cod_attrezzatura + " non presenta Tipologie di manutenzione attive!"
	return 1
	
else

end if

return 0
end function

public subroutine wf_set_idl (long al_row);
string				ls_tipo_man, ls_testo_idl, ls_sel, ls_flag_idl
long				ll_color


if al_row>0 then
	ls_tipo_man = dw_sel_duplica_tipiman_origine.getitemstring(al_row, "cod_tipo_manutenzione")
	ls_testo_idl = dw_sel_duplica_tipiman_origine.getitemstring(al_row, "modalita_esecuzione")
	ls_sel = dw_sel_duplica_tipiman_origine.getitemstring(al_row, "selezionato")
	ls_flag_idl = dw_sel_duplica_tipiman_origine.getitemstring(al_row, "duplica_idl")
	
	if ls_sel="N" then
		//Tipo Manutenzione NON selezionato
		ll_color = 7039851		//grigio scuro
	else
		//Tipo Manutenzione SELEZIONATA
		if ls_flag_idl="S" then
			// e IDL da riportare
			ll_color = 128		//rosso scuro
		else
			//Tipo Manutenzione non selezionato e IDL da NON riportare
			ll_color = 16711680		//blu (RGB(0, 0, 255)
		end if
	end if
	
	if isnull(ls_testo_idl) or ls_testo_idl="" then ls_testo_idl = "Non ci sono Istruzioni di Lavoro codificate per questa tipologia di manutenzione!"
	
	ls_testo_idl = "ISTRUZIONI DI LAVORO DELLA TIPOLOGIA SELEZIONATA "+ls_tipo_man+" :~r~n~r~n" + ls_testo_idl
	
else
	ll_color = 0
	ls_testo_idl = ""

end if

mle_idl.textcolor = ll_color
mle_idl.text = ls_testo_idl


return
end subroutine

public function integer wf_duplica (ref string as_attrezz_saltate, ref string as_errore);string						ls_cod_attrezzatura_da, ls_cod_attrezzatura_a, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, &
							ls_flag_manutenzione, ls_flag_ricambio_codificato, ls_kit_ricambio, ls_cod_ricambio_alternativo, &
							ls_des_ricambio_non_codificato, ls_modalita_esecuzione, ls_cod_tipo_ord_acq, ls_cod_tipo_off_acq, ls_cod_tipo_det_acq, &
							ls_cod_tipo_movimento, ls_periodicita, ls_cod_primario, ls_cod_misura, &
							ls_risorsa, ls_des_risorsa, ls_cod_versione, ls_des_ricambio, ls_cod_ricambio, ls_messaggio, ls_cod_lista_dist, &
							ls_cod_tipo_lista_dist, ls_cod_operaio, ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_selezione, ls_sql, &
							ls_syntax, ls_errore, ls_idl_attr_destinazione, ls_sel_tipoman, ls_sel_idl_tipoman

datetime					ldt_tempo_previsto

dec{4}					ld_num_reg_lista, ld_num_versione, ld_num_edizione, ld_frequenza, ld_quan_ricambio, ld_costo_unitario_ricambio, &
							ld_val_min_taratura, ld_val_max_taratura, ld_valore_atteso, ld_tempo_risorsa, ld_costo_risorsa, ld_prezzo_ricambio

long						ll_contatore, ll_prog_ricambio, ll_count, ll_i, ll_j

uo_ricambi				luo_ricambi




dw_sel_duplica_attrez.accepttext()

ls_cod_attrezzatura_da = dw_sel_duplica_attrez.getitemstring(1, "rs_cod_attrezzatura_da")

if isnull(ls_cod_attrezzatura_da) then
	as_errore = "Il codice attrezzatura Origine non può essere nullo!"
	return 1
end if	

ll_count = 0
for ll_i = 1 to dw_sel_duplica_tipiman_origine.rowcount()
	if dw_sel_duplica_tipiman_origine.getitemstring( ll_i, "selezionato") = "S" then ll_count = ll_count + 1
next

if ll_count < 1 then
	as_errore = "Selezionare almeno una tipologia dell'attrezzatura di origine da duplicare!"
	return	 1
end if


ll_count = 0

for ll_i = 1 to dw_lista.rowcount()
	if dw_lista.getitemstring( ll_i, "flag_selezione") = "S" then ll_count = ll_count + 1
next

if ll_count < 1 then
	as_errore = "Selezionare almeno un'apparecchiatura di destinazione!"
	return	 1
end if

as_attrezz_saltate = ""

//ciclo per tutte le attrezzature destinazione (quelle non selezionate le salta ...)
for ll_i = 1 to dw_lista.rowcount()
	ls_selezione = dw_lista.getitemstring( ll_i, "flag_selezione")
	
	pcca.mdi_frame.setmicrohelp("Attendere ...")
	
	if ls_selezione = "S" then
		ls_cod_attrezzatura_a = dw_lista.getitemstring( ll_i, "cod_attrezzatura")
		ls_idl_attr_destinazione = dw_lista.getitemstring( ll_i, "duplica_idl")
		
		pcca.mdi_frame.setmicrohelp("Duplicazione Tipologie su Attrezzature '"+ls_cod_attrezzatura_a+"' in corso ...")
	
		//ciclo le tipologie di manutenzione dell'attrezzatura origine ()
		for ll_j = 1 to dw_sel_duplica_tipiman_origine.rowcount()
			ls_sel_tipoman = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "selezionato")
			
			if ls_sel_tipoman="S" then
			else
				//tipologia esclusa dalla duplicazione
				continue
			end if
			
			ls_sel_idl_tipoman = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "duplica_idl")	
			ls_cod_tipo_manutenzione = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_manutenzione")	
			ls_des_tipo_manutenzione = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "des_tipo_manutenzione")			
			ldt_tempo_previsto = dw_sel_duplica_tipiman_origine.getitemdatetime( ll_j, "tempo_previsto")		
			ls_flag_manutenzione = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "flag_manutenzione")			
			ls_flag_ricambio_codificato = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "flag_ricambio_codificato")			
			ls_kit_ricambio = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_ricambio")			
			ls_cod_versione = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_versione")			
			ls_cod_ricambio_alternativo = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_ricambio_alternativo")			
			ls_des_ricambio_non_codificato = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "des_ricambio_non_codificato")			
			ld_num_reg_lista = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "num_reg_lista")			
			ld_num_versione = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "num_versione")			
			ld_num_edizione = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "num_edizione")
			
			ls_modalita_esecuzione = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "modalita_esecuzione")
			if ls_sel_idl_tipoman = "S" then
				if ls_idl_attr_destinazione = "S" then
					//riporta le istruzioni, se presenti
				else
					//non ricopiare le istruzioni di questa tipologia nell'attrezzatura destinazione corrente ...
					ls_modalita_esecuzione = ""
				end if
			else
				//non ricopiare le istruzioni di questa tipologia in alcuna attrezzatura destinazione ...
				ls_modalita_esecuzione = ""
			end if
			
			ls_cod_tipo_ord_acq = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_ord_acq")			
			ls_cod_tipo_off_acq = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_off_acq")			
			ls_cod_tipo_det_acq = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_det_acq")		
			ls_cod_tipo_movimento = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_movimento")
			ls_periodicita = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "periodicita")
			ld_frequenza = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "frequenza")
			ld_quan_ricambio = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "quan_ricambio")
			ld_costo_unitario_ricambio = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "costo_unitario_ricambio")
			ls_cod_primario = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_primario")			
			ls_cod_misura = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_misura")	
			ld_val_min_taratura = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "val_min_taratura")		
			ld_val_max_taratura = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "val_max_taratura")	
			ld_valore_atteso = dw_sel_duplica_tipiman_origine.getitemnumber( ll_j, "valore_atteso")		
			ls_cod_tipo_lista_dist = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_tipo_lista_dist")	
			ls_cod_lista_dist = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_lista_dist")
			ls_cod_operaio = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_operaio")
			ls_cod_cat_risorse_esterne = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_cat_risorse_esterne") 
			ls_cod_risorsa_esterna = dw_sel_duplica_tipiman_origine.getitemstring( ll_j, "cod_risorsa_esterna")
	
		
			select count(cod_attrezzatura)
			into   :ll_contatore
			from   tab_tipi_manutenzione
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_attrezzatura = :ls_cod_attrezzatura_a and 
					 cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in lettura dati da tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext
				
				return -1
			end if			
			
			if not isnull(ll_contatore) and ll_contatore > 0 then
//				g_mb.warning("L'attrezzatura con codice " + ls_cod_attrezzatura_a + &
//									" ha già una Tipologia Manutenzione con il codice " + ls_cod_tipo_manutenzione + "! "+&
//									"Pertanto questa tipologia no sarò ricopiata!")
				as_attrezz_saltate += "~r~n" + ls_cod_attrezzatura_a + " - " + ls_cod_tipo_manutenzione
			else
				
				if ls_cod_tipo_lista_dist = "" then setnull(ls_cod_tipo_lista_dist)
				if ls_cod_lista_dist = "" then setnull(ls_cod_lista_dist)
				
				insert into tab_tipi_manutenzione (cod_azienda,
															  cod_attrezzatura,
															  cod_tipo_manutenzione,
															  des_tipo_manutenzione,
															  tempo_previsto,
															  flag_manutenzione,
															  flag_ricambio_codificato,
															  cod_ricambio,
															  cod_versione,
															  cod_ricambio_alternativo,
															  des_ricambio_non_codificato,
															  num_reg_lista,
															  num_versione,
															  num_edizione,
															  modalita_esecuzione,
															  cod_tipo_ord_acq,
															  cod_tipo_off_acq,
															  cod_tipo_det_acq,
															  cod_tipo_movimento,
															  periodicita,
															  frequenza,
															  quan_ricambio,
															  costo_unitario_ricambio,
															  cod_primario,
															  cod_misura,
															  val_min_taratura,
															  val_max_taratura,
															  valore_atteso,
															  cod_tipo_lista_dist,
															  cod_lista_dist,
															  cod_operaio, 
															  cod_cat_risorse_esterne, 
															  cod_risorsa_esterna)
													 values (:s_cs_xx.cod_azienda,
															  :ls_cod_attrezzatura_a,
															  :ls_cod_tipo_manutenzione,
															  :ls_des_tipo_manutenzione,
															  :ldt_tempo_previsto,
															  :ls_flag_manutenzione,
															  :ls_flag_ricambio_codificato,
															  :ls_kit_ricambio,
															  :ls_cod_versione,
															  :ls_cod_ricambio_alternativo,
															  :ls_des_ricambio_non_codificato,
															  :ld_num_reg_lista,
															  :ld_num_versione,
															  :ld_num_edizione,
															  :ls_modalita_esecuzione,
															  :ls_cod_tipo_ord_acq,
															  :ls_cod_tipo_off_acq,
															  :ls_cod_tipo_det_acq,
															  :ls_cod_tipo_movimento,
															  :ls_periodicita,
															  :ld_frequenza,
															  :ld_quan_ricambio,
															  :ld_costo_unitario_ricambio,
															  :ls_cod_primario,
															  :ls_cod_misura,
															  :ld_val_min_taratura,
															  :ld_val_max_taratura,
															  :ld_valore_atteso,
															  :ls_cod_tipo_lista_dist,
															  :ls_cod_lista_dist,
															  :ls_cod_operaio, 
															  :ls_cod_cat_risorse_esterne, 
															  :ls_cod_risorsa_esterna) ;
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in inserimento dati in tabella tab_tipi_manutenzioni " + sqlca.sqlerrtext
					
					return -1
				end if																  
				
				
				//duplicazione risorse da elenco_risorse -------------------------------------------------------------
				declare cu_risorse cursor for
				select  cod_cat_attrezzature,
						  des_risorsa,
						  tempo_esecuzione,
						  costo_risorsa
				from    elenco_risorse
				where   cod_azienda = :s_cs_xx.cod_azienda and 
						  cod_attrezzatura = :ls_cod_attrezzatura_da and 
						  cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
					 
				open cu_risorse;
				
				if sqlca.sqlcode <> 0 then
					as_errore = "Errore in apertura cursore cu_risorse " + sqlca.sqlerrtext
					
					return -1
				end if		
				
				do while 1 = 1 
					fetch cu_risorse into :ls_risorsa, 
												 :ls_des_risorsa, 
												 :ld_tempo_risorsa, 
												 :ld_costo_risorsa;
					if sqlca.sqlcode < 0 then
						as_errore = "Errore in lettura dati dalla tabella elenco_risorse " + sqlca.sqlerrtext
						close cu_risorse;
						
						return -1
					end if
					
					if sqlca.sqlcode = 100 then exit
					insert into elenco_risorse (cod_azienda,
														 cod_attrezzatura,
														 cod_tipo_manutenzione,
														 cod_cat_attrezzature,
														 des_risorsa,
														 tempo_esecuzione,
														 costo_risorsa)
												values (:s_cs_xx.cod_azienda,
														  :ls_cod_attrezzatura_a,
														  :ls_cod_tipo_manutenzione,
														  :ls_risorsa,
														  :ls_des_risorsa,
														  :ld_tempo_risorsa,
														  :ld_costo_risorsa);
														  
					if sqlca.sqlcode <> 0 then
						as_errore = "Errore in inserimento dati nella tabella elenco_risorse " + sqlca.sqlerrtext
						close cu_risorse;
						
						return -1
					end if		
				loop
				
				close cu_risorse;
				//--------------------------------------------------------------------------------------------------
				
				
				
				//duplicazione elenco ricambi  ----------------------------------------------------------------
				luo_ricambi = create uo_ricambi
				if luo_ricambi.uof_duplica_tipologia(ls_cod_attrezzatura_da,ls_cod_tipo_manutenzione, &
																 ls_cod_attrezzatura_a,ls_cod_tipo_manutenzione, as_errore) <> 0 then
					destroy luo_ricambi;
					close cu_risorse;
					
					return -1
				end if
				
				destroy luo_ricambi;
				//-----------------------------------------------------------------------------------------------------
				
			end if	
		
		next
	end if
	
next
//fine ciclo attrezzature destinazione

return 0

end function

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_sel_duplica_attrez.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 


dw_sel_duplica_tipiman_origine.settransobject(sqlca)
									 



end event

on w_sel_duplica_attrez.create
int iCurrent
call super::create
this.mle_idl=create mle_idl
this.dw_sel_duplica_tipiman_origine=create dw_sel_duplica_tipiman_origine
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
this.dw_sel_duplica_attrez=create dw_sel_duplica_attrez
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_idl
this.Control[iCurrent+2]=this.dw_sel_duplica_tipiman_origine
this.Control[iCurrent+3]=this.dw_lista
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.dw_sel_duplica_attrez
end on

on w_sel_duplica_attrez.destroy
call super::destroy
destroy(this.mle_idl)
destroy(this.dw_sel_duplica_tipiman_origine)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
destroy(this.dw_sel_duplica_attrez)
end on

event open;call super::open;dw_ricerca.postevent("getfocus")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca,&
                 "cod_reparto", &
                 sqlca, &
					  "anag_reparti",&
					  "cod_reparto",&
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ricerca,&
                 "cod_area", &
                 sqlca, &
					  "tab_aree_aziendali",&
					  "cod_area_aziendale",&
					  "des_area", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ricerca,&
                 "cod_categoria", &
                 sqlca, &
					  "tab_cat_attrezzature",&
					  "cod_cat_attrezzature",&
					  "des_cat_attrezzature", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana = 'N'")



end event

type mle_idl from multilineedit within w_sel_duplica_attrez
integer x = 1957
integer y = 20
integer width = 2286
integer height = 1476
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
string text = "ISTRUZIONI DI LAVORO DELLA TIPOLOGIA SELEZIONATA : "
boolean hscrollbar = true
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
end type

type dw_sel_duplica_tipiman_origine from datawindow within w_sel_duplica_attrez
event ue_set_idl ( long al_row )
integer x = 32
integer y = 520
integer width = 1902
integer height = 980
integer taborder = 20
string title = "Tipologie Manutenzioni Disponibili"
string dataobject = "d_sel_duplica_attrez_tipiman_origine"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_set_idl(long al_row);

wf_set_idl(al_row)

return
end event

event rowfocuschanged;


wf_set_idl(currentrow)
end event

event clicked;

if row>0 then
	
	choose case dwo.name
		case "cod_tipo_manutenzione", "periodicita", "frequenza", "selezionato", "duplica_idl"
			setrow(row)
			
			
	end choose
	
end if
end event

event itemchanged;

if row>0 then
else
	return
end if

choose case dwo.name
	case "selezionato", "duplica_idl"
		
		event post ue_set_idl(row)
		
end choose
end event

event buttonclicked;
long					ll_row, ll_index
string					ls_flag, ls_column_flag, ls_errore
integer				li_ret


ll_row = rowcount()

if ll_row>0 then
else
	return
end if


choose case dwo.name
	case "b_sel", "b_sel2"
		if dwo.text = "Tutti" then
			//hai scelto di selezionare tutti
			ls_flag = "S"
			dwo.text = "Nessuno"
		else
			ls_flag = "N"
			dwo.text = "Tutti"
		end if
		
		if dwo.name = "b_sel" then
			ls_column_flag = "selezionato"
		else
			ls_column_flag = "duplica_idl"
		end if
		
		setpointer(Hourglass!)
		for ll_index = 1 to ll_row
			setitem(ll_index, ls_column_flag, ls_flag)
		next
		setpointer(Arrow!)
		

end choose
end event

type dw_lista from datawindow within w_sel_duplica_attrez
integer x = 1957
integer y = 1540
integer width = 2286
integer height = 1432
integer taborder = 30
string title = "none"
string dataobject = "d_sel_duplica_attrez_attrezzature"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;
long					ll_row, ll_index
string					ls_flag, ls_column_flag, ls_errore, ls_attrezzature_saltate
integer				li_ret


ll_row = rowcount()

if ll_row>0 then
else
	
end if


choose case dwo.name
	case "b_sel", "b_sel2"
		if dwo.text = "Tutti" then
			//hai scelto di selezionare tutti
			ls_flag = "S"
			dwo.text = "Nessuno"
		else
			ls_flag = "N"
			dwo.text = "Tutti"
		end if
		
		if dwo.name = "b_sel" then
			ls_column_flag = "flag_selezione"
		else
			ls_column_flag = "duplica_idl"
		end if
		
		setpointer(Hourglass!)
		for ll_index = 1 to ll_row
			setitem(ll_index, ls_column_flag, ls_flag)
		next
		setpointer(Arrow!)
		
	
	case "b_annulla"
		
		if g_mb.confirm("Chiudere la finestra di duplicazione tipologie?") then
			setnull(s_cs_xx.parametri.parametro_s_10)
			setnull(s_cs_xx.parametri.parametro_s_11)
			
			close(w_sel_duplica_attrez)
		end if
		
		
	case "b_duplica"
		
		setpointer(Hourglass!)
		ls_attrezzature_saltate = ""
		li_ret = wf_duplica(ls_attrezzature_saltate, ls_errore)
		setpointer(Arrow!)
		
		if li_ret<0 then
			rollback;
			g_mb.error(ls_errore)
			return
			
		elseif li_ret>0 then
			rollback;
			g_mb.warning(ls_errore)
			return
			
		else
			commit;
			
			if ls_attrezzature_saltate<>"" and not isnull(ls_attrezzature_saltate) then
				ls_errore = "Duplicazione Eseguita ma le seguenti coppie attrezzatura-tipologia sono state saltate in quanto già presenti: "+&
								ls_attrezzature_saltate
				g_mb.warning(ls_errore)
			else
				g_mb.success("Duplicazione Eseguita con Successo!")
			end if
			
			
			close(w_sel_duplica_attrez)
		end if
		
		

end choose
end event

type dw_ricerca from uo_dw_search within w_sel_duplica_attrez
event ue_cod_attr ( )
event ue_carica_tipologie ( )
integer x = 32
integer y = 1540
integer width = 1902
integer height = 1432
integer taborder = 10
string dataobject = "d_sel_duplica_attrez_ricerca"
end type

event ue_cod_attr();if not isnull(getitemstring(getrow(),"attrezzatura_da")) then
	setitem(getrow(),"attrezzatura_a",getitemstring(getrow(),"attrezzatura_da"))
end if
end event

event itemchanged;call super::itemchanged;choose case getcolumnname()
		
	case "attrezzatura_da"
		postevent("ue_cod_attr")
		postevent("ue_carica_tipologie")
		
	case "attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		postevent("ue_carica_tipologie")

end choose

end event

event buttonclicked;call super::buttonclicked;string				ls_string

integer			li_ret


choose case dwo.name
	case "b_ricerca_att_da"
		guo_ricerca.uof_set_parent(parent)
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"attrezzatura_da")
		
	case "b_ricerca_att_a"
		guo_ricerca.uof_set_parent(parent)
		guo_ricerca.uof_ricerca_attrezzatura(dw_ricerca,"attrezzatura_a")
		
	case "b_ricerca"
		setpointer(Hourglass!)
		pcca.mdi_frame.setmicrohelp("Ricerca Attrezzature in corso ...")
		li_ret = wf_ricerca_attrezzature_dest(ls_string)
		pcca.mdi_frame.setmicrohelp("Pronto!")
		setpointer(Arrow!)
		
		if li_ret<0 then
			rollback;
			g_mb.error(ls_string)
			return
			
		elseif li_ret>0 then
			rollback;
			g_mb.warning(ls_string)
			return
		else
			
		end if
		
		
end choose
end event

type dw_sel_duplica_attrez from uo_cs_xx_dw within w_sel_duplica_attrez
integer x = 32
integer y = 20
integer width = 1902
integer height = 488
integer taborder = 10
string dataobject = "d_sel_duplica_attrez"
end type

event buttonclicked;call super::buttonclicked;string			ls_cod_attrezzatura, ls_errore

integer		li_ret



choose case dwo.name
	case "b_ricerca_att"
		guo_ricerca.uof_set_parent(parent)
		guo_ricerca.uof_ricerca_attrezzatura(dw_sel_duplica_attrez, "rs_cod_attrezzatura_da")
		
	case "b_cerca"
		//ricerca tipologie da ricercare
		dw_sel_duplica_attrez.accepttext()
		
		ls_cod_attrezzatura = dw_sel_duplica_attrez.getitemstring(1, "rs_cod_attrezzatura_da")
		li_ret = wf_ricerca_tipologie_origine(ls_cod_attrezzatura, ls_errore)
		
		if li_ret<0 then
			g_mb.error(ls_errore)
			return
			
		elseif li_ret>0 then
			g_mb.warning(ls_errore)
			return
			
		else
			
		end if
		
end choose
end event

event itemchanged;call super::itemchanged;
string				ls_errore
integer			li_ret


choose case dwo.name
	case "rs_cod_attrezzatura_da"
		if data <>"" then
			
			setpointer(Hourglass!)
			pcca.mdi_frame.setmicrohelp("Ricerca Tipologie di manutenzione in corso ...")
			li_ret = wf_ricerca_tipologie_origine(data, ls_errore)
			pcca.mdi_frame.setmicrohelp("Pronto!")
			setpointer(Arrow!)
			
			if li_ret<0 then
				g_mb.error(ls_errore)
				return
				
			elseif li_ret>0 then
				g_mb.warning(ls_errore)
				return
				
			else
				
			end if
			
		else
			dw_sel_duplica_tipiman_origine.reset()
		end if
		
		dw_lista.reset()
		
end choose
end event

