﻿$PBExportHeader$w_tab_chiavi_protocollo.srw
$PBExportComments$Finestra report protocolli chiavi
forward
global type w_tab_chiavi_protocollo from w_cs_xx_principale
end type
type dw_tab_chiavi_protocollo from uo_cs_xx_dw within w_tab_chiavi_protocollo
end type
type st_1 from statictext within w_tab_chiavi_protocollo
end type
end forward

global type w_tab_chiavi_protocollo from w_cs_xx_principale
int Width=2483
int Height=957
boolean TitleBar=true
string Title="Gestione Protocollo - Chiavi"
dw_tab_chiavi_protocollo dw_tab_chiavi_protocollo
st_1 st_1
end type
global w_tab_chiavi_protocollo w_tab_chiavi_protocollo

type variables
boolean ib_nuovo = false
end variables

on w_tab_chiavi_protocollo.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_chiavi_protocollo=create dw_tab_chiavi_protocollo
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_chiavi_protocollo
this.Control[iCurrent+2]=st_1
end on

on w_tab_chiavi_protocollo.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_chiavi_protocollo)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_chiavi_protocollo.set_dw_key("cod_azienda")
dw_tab_chiavi_protocollo.set_dw_key("num_protocollo")
dw_tab_chiavi_protocollo.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)


end event

event open;call super::open;//cb_chiavi_ricerca.enabled = false

end event

type dw_tab_chiavi_protocollo from uo_cs_xx_dw within w_tab_chiavi_protocollo
int X=23
int Y=21
int Width=2401
int Height=721
string DataObject="d_tab_chiavi_protocollo"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
double ld_num_protocollo

ld_num_protocollo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_protocollo")

ll_errore = retrieve(s_cs_xx.cod_azienda, ld_num_protocollo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


w_tab_chiavi_protocollo.title = "Num. Protocollo:" + string(ld_num_protocollo)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
double ld_num_protocollo, ld_num_protocollo_riga

ld_num_protocollo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_protocollo")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	ld_num_protocollo_riga = this.getitemnumber(ll_i, "num_protocollo")
   if isnull(ld_num_protocollo_riga) or ld_num_protocollo_riga = 0 then
      this.setitem(ll_i, "num_protocollo", ld_num_protocollo)
   end if
next
end event

event pcd_save;call super::pcd_save;accepttext()
end event

event pcd_new;call super::pcd_new;ib_nuovo = true
end event

event updateend;call super::updateend;ib_nuovo = false

end event

event ue_key;call super::ue_key;if ib_nuovo then

	if key = keyF1!  and keyflags = 1 then
		s_cs_xx.parametri.parametro_uo_dw_1 = dw_tab_chiavi_protocollo
		s_cs_xx.parametri.parametro_s_1 = "cod_famiglia_chiavi"
		s_cs_xx.parametri.parametro_s_2 = "cod_chiave"
		
		if not isvalid(w_ricerca_chiavi_doc) then
			window_open(w_ricerca_chiavi_doc, 0)
		end if
	end if	

end if
end event

type st_1 from statictext within w_tab_chiavi_protocollo
int X=46
int Y=761
int Width=983
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="~"Shift + F1~" per ricerca chiavi"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

