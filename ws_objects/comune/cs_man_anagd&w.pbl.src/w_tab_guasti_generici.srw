﻿$PBExportHeader$w_tab_guasti_generici.srw
forward
global type w_tab_guasti_generici from w_cs_xx_principale
end type
type dw_guasti_generici_det from uo_cs_xx_dw within w_tab_guasti_generici
end type
type dw_guasti_generici_lista from uo_cs_xx_dw within w_tab_guasti_generici
end type
end forward

global type w_tab_guasti_generici from w_cs_xx_principale
integer width = 2002
integer height = 1344
string title = "Guasti Generici Attrezzature"
boolean maxbox = false
boolean resizable = false
dw_guasti_generici_det dw_guasti_generici_det
dw_guasti_generici_lista dw_guasti_generici_lista
end type
global w_tab_guasti_generici w_tab_guasti_generici

on w_tab_guasti_generici.create
int iCurrent
call super::create
this.dw_guasti_generici_det=create dw_guasti_generici_det
this.dw_guasti_generici_lista=create dw_guasti_generici_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_guasti_generici_det
this.Control[iCurrent+2]=this.dw_guasti_generici_lista
end on

on w_tab_guasti_generici.destroy
call super::destroy
destroy(this.dw_guasti_generici_det)
destroy(this.dw_guasti_generici_lista)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_guasti_generici_lista

dw_guasti_generici_lista.change_dw_current()

dw_guasti_generici_lista.set_dw_options(sqlca, &
													 pcca.null_object, &
													 c_default, &
													 c_default)

dw_guasti_generici_det.set_dw_options(sqlca, &
												  dw_guasti_generici_lista, &
												  c_sharedata + c_scrollparent, &
												  c_default)
end event

type dw_guasti_generici_det from uo_cs_xx_dw within w_tab_guasti_generici
integer x = 23
integer y = 660
integer width = 1943
integer height = 580
integer taborder = 10
string dataobject = "d_guasti_generici_det"
end type

type dw_guasti_generici_lista from uo_cs_xx_dw within w_tab_guasti_generici
integer x = 23
integer y = 20
integer width = 1943
integer height = 620
integer taborder = 10
string dataobject = "d_guasti_generici_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
next
end event

event updatestart;call super::updatestart;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		continue
	end if
	
	if isnull(getitemstring(ll_i,"cod_guasto")) then
		g_mb.messagebox("OMNIA","Inserire il codice del guasto generico!",exclamation!)
		return 1
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "tab_guasti_generici"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_guasto")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_guasto")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

