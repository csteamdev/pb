﻿$PBExportHeader$w_tab_tipi_manut_scollegate.srw
$PBExportComments$Tipi di Manutenzione scollegate
forward
global type w_tab_tipi_manut_scollegate from w_cs_xx_principale
end type
type dw_tipi_manutenzione_lista from uo_cs_xx_dw within w_tab_tipi_manut_scollegate
end type
end forward

global type w_tab_tipi_manut_scollegate from w_cs_xx_principale
integer width = 2798
integer height = 1040
string title = "Tipologie Manutenzione Scollegate"
dw_tipi_manutenzione_lista dw_tipi_manutenzione_lista
end type
global w_tab_tipi_manut_scollegate w_tab_tipi_manut_scollegate

type variables
long il_x[],il_y[]
boolean is_colore = false
end variables

event pc_setwindow;call super::pc_setwindow;dw_tipi_manutenzione_lista.set_dw_key("cod_azienda")
dw_tipi_manutenzione_lista.set_dw_options( sqlca, pcca.null_object, c_default, c_default)

iuo_dw_main = dw_tipi_manutenzione_lista

end event

on w_tab_tipi_manut_scollegate.create
int iCurrent
call super::create
this.dw_tipi_manutenzione_lista=create dw_tipi_manutenzione_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_manutenzione_lista
end on

on w_tab_tipi_manut_scollegate.destroy
call super::destroy
destroy(this.dw_tipi_manutenzione_lista)
end on

event pc_setddlb;call super::pc_setddlb;string ls_prova

select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';

if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	//sintexcal
	f_PO_LoadDDDW_DW(dw_tipi_manutenzione_lista,"cod_tipo_attrez",sqlca,&
                 "tab_tipologie_attrez","cod_tipo_attrez","des_tipo_attrez", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
	
	f_PO_LoadDDDW_DW(dw_tipi_manutenzione_lista,"cod_tipo_pianificazione",sqlca,&
                 "parametri_pianificazione","cod_tipo_pianificazione","des_tipo_pianificazione", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
	
else
	//no sintexcal
end if
end event

type dw_tipi_manutenzione_lista from uo_cs_xx_dw within w_tab_tipi_manut_scollegate
event ue_seleziona_prima ( )
integer x = 23
integer y = 20
integer width = 2720
integer height = 900
integer taborder = 40
string dataobject = "d_tab_tipi_manut_scollegate_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
	pcca.error = c_fatal
end if

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

event itemchanged;call super::itemchanged;if dwo.name = "cod_tipo_manut_scollegata" then
	
	if not isnull(data) and data <> "" then
		setitem( row, "cod_tipo_manut_collegata", data)
	end if
	
end if
end event

