﻿$PBExportHeader$w_tab_tipi_manutenzione.srw
$PBExportComments$Finestra Gestione Tipi Manutenzioni
forward
global type w_tab_tipi_manutenzione from w_cs_xx_principale
end type
type cb_cerca from commandbutton within w_tab_tipi_manutenzione
end type
type cb_modifica from commandbutton within w_tab_tipi_manutenzione
end type
type cb_trasferisci from commandbutton within w_tab_tipi_manutenzione
end type
type cb_ricambi from commandbutton within w_tab_tipi_manutenzione
end type
type cb_duplica from commandbutton within w_tab_tipi_manutenzione
end type
type cb_risorse from commandbutton within w_tab_tipi_manutenzione
end type
type cb_documento from commandbutton within w_tab_tipi_manutenzione
end type
type cb_richiama_predefinito from commandbutton within w_tab_tipi_manutenzione
end type
type cb_predefinito from commandbutton within w_tab_tipi_manutenzione
end type
type dw_tipi_manutenzione_det_4 from uo_cs_xx_dw within w_tab_tipi_manutenzione
end type
type dw_filtro from u_dw_search within w_tab_tipi_manutenzione
end type
type dw_tipi_manutenzione_det_2 from uo_cs_xx_dw within w_tab_tipi_manutenzione
end type
type tv_1 from treeview within w_tab_tipi_manutenzione
end type
type dw_folder_tree from u_folder within w_tab_tipi_manutenzione
end type
type dw_tipi_manutenzione_det_5 from uo_cs_xx_dw within w_tab_tipi_manutenzione
end type
type dw_tipi_manutenzione_det_3 from uo_cs_xx_dw within w_tab_tipi_manutenzione
end type
type dw_folder from u_folder within w_tab_tipi_manutenzione
end type
type dw_tipi_manutenzione_det_1 from uo_cs_xx_dw within w_tab_tipi_manutenzione
end type
end forward

global type w_tab_tipi_manutenzione from w_cs_xx_principale
integer width = 4215
integer height = 2072
string title = "Tipologie Manutenzione"
event pc_menu_refresh ( )
cb_cerca cb_cerca
cb_modifica cb_modifica
cb_trasferisci cb_trasferisci
cb_ricambi cb_ricambi
cb_duplica cb_duplica
cb_risorse cb_risorse
cb_documento cb_documento
cb_richiama_predefinito cb_richiama_predefinito
cb_predefinito cb_predefinito
dw_tipi_manutenzione_det_4 dw_tipi_manutenzione_det_4
dw_filtro dw_filtro
dw_tipi_manutenzione_det_2 dw_tipi_manutenzione_det_2
tv_1 tv_1
dw_folder_tree dw_folder_tree
dw_tipi_manutenzione_det_5 dw_tipi_manutenzione_det_5
dw_tipi_manutenzione_det_3 dw_tipi_manutenzione_det_3
dw_folder dw_folder
dw_tipi_manutenzione_det_1 dw_tipi_manutenzione_det_1
end type
global w_tab_tipi_manutenzione w_tab_tipi_manutenzione

type variables
long il_x[],il_y[]
boolean is_colore = false
boolean ib_tree_delete = false

// filtri
string is_sql_filtro, is_cod_attrezzatura, is_cod_tipo_manutenzione
long il_livello_corrente, il_ultimo_handle
treeviewitem tvi_campo

// campi automatici
string is_divisione, is_reparto, is_categoria, is_area
end variables

forward prototypes
public function integer wf_show_butt (commandbutton fcb_list[])
public function integer wf_tipo_manutenzione (string flag_prod_codificato)
public function integer wf_hide_butt (commandbutton fcb_list[])
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_treeview ()
public function string wf_leggi_parent (long fl_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura)
public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura_da, string fs_cod_attrezzatura_a)
public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria)
public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione)
public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto)
public function integer wf_inserisci_manutenzioni (long al_handle, string as_cod_attrezzatura)
public subroutine wf_cancella_livello (long fl_handle)
public subroutine wf_inserisci_singola_manutenzione (long row)
public subroutine wf_cancella_manutenzione ()
public function integer wf_aggiorna_programmi (long fl_row)
end prototypes

event pc_menu_refresh();long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_data

// ritrovo handle corrente
ll_handle = tv_1.finditem(currenttreeitem!, 0)
// chiudo il nodo se aperto
tv_1.CollapseItem(ll_handle)
// prelevo le informazioni
tv_1.getitem(ll_handle, tvi_item)
// pulisco i figli
wf_cancella_livello(ll_handle)
// dichiaro che ha figli
tvi_item.children = true
// dico che non è mai stato aperto, così rilancia l'evento itempopulate
tvi_item.ExpandedOnce = false
// imposto il figlio
tv_1.setitem(ll_handle, tvi_item)
end event

public function integer wf_show_butt (commandbutton fcb_list[]);integer ll_i

if upperbound(il_x[])=0 then return 0

for ll_i=1 to upperbound(fcb_list[])
	fcb_list[ll_i].x=il_x[ll_i]
	fcb_list[ll_i].y=il_y[ll_i]
next

return 0
end function

public function integer wf_tipo_manutenzione (string flag_prod_codificato);//commandbutton lcb_list[]
//
//lcb_list[1]=cb_ricambio
//lcb_list[2]=cb_ricambio_alt

choose case flag_prod_codificato
case "S"
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_t.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_alternativo_t.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("cf_cod_ricambio.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("cf_cod_ricambio_alternativo.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio.Visible=1")
	dw_tipi_manutenzione_det_1.Modify("cod_versione_t.Visible=1")
	dw_tipi_manutenzione_det_1.Modify("cod_versione.Visible=1")
	dw_tipi_manutenzione_det_1.Modify("cf_cod_versione.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_alternativo.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("des_ricambio_non_codificato_t.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("des_ricambio_non_codificato.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("r_1.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("r_2.Visible=1")
	// wf_show_butt(lcb_list[])
case "N"
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_t.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_alternativo_t.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("cf_cod_ricambio.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("cf_cod_ricambio_alternativo.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio.Visible=0")
	dw_tipi_manutenzione_det_1.Modify("cod_versione_t.Visible=0")
	dw_tipi_manutenzione_det_1.Modify("cod_versione.Visible=0")
	dw_tipi_manutenzione_det_1.Modify("cf_cod_versione.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("cod_ricambio_alternativo.Visible=0")
   dw_tipi_manutenzione_det_1.Modify("des_ricambio_non_codificato_t.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("des_ricambio_non_codificato.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("r_1.Visible=1")
   dw_tipi_manutenzione_det_1.Modify("r_2.Visible=0")
	// wf_hide_butt(lcb_list[])
end choose
return 0   



end function

public function integer wf_hide_butt (commandbutton fcb_list[]);integer ll_i

for ll_i=1 to upperbound(fcb_list[])
	il_x[ll_i]=fcb_list[ll_i].x
	il_y[ll_i]=fcb_list[ll_i].y
	fcb_list[ll_i].x=32000
	fcb_list[ll_i].y=32000
next

return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_filtro.getitemdatetime(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_filtro.getitemstring(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_filtro.getitemnumber(dw_filtro.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'TIPIMA';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'TIPIMA';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'TIPIMA');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_filtro.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'TIPIMA';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_filtro.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_filtro.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_filtro.setitem(dw_filtro.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_cancella_treeview ();ib_tree_delete = true
tv_1.deleteitem(0)
ib_tree_delete = false
end subroutine

public subroutine wf_imposta_treeview ();string ls_cod_da, ls_cod_a
setpointer(HourGlass!)
tv_1.setredraw(false)


// se non è stato impostato alcun livello ed è stata specificata una attrezzatura
// procedo con il caricamento delle registrazioni della sola attrezzatura richiesta 
if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_1") = "N" then
	ls_cod_da = trim(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da"))
	ls_cod_a  = trim(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a"))
	
	if (isnull(ls_cod_da) or ls_cod_da = "") and (isnull(ls_cod_a) or ls_cod_a = "") then
		wf_inserisci_manutenzioni(0, "")
	else
		wf_inserisci_attrezzature(0, ls_cod_da, ls_cod_a)
	end if
	
	tv_1.setredraw(true)
	return

end if
// --------------------------------

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "D"
		 wf_inserisci_divisioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione"))
		// caricamento divisioni
	case "R"
		wf_inserisci_reparti(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto"))
		// caricamento reparti
	case "C"
		wf_inserisci_categorie(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura"))
		// caricamento categorie
	case "E" 
		wf_inserisci_aree(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale"))
		// caricamento aree
	case "A"
		wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da"), dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a"))
		// caricamento attrezzature
	case "N" 
		//wf_inserisci_registrazioni(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura"))
		wf_inserisci_attrezzature(0, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da"), dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a"))
end choose
// --------------------------------

tv_1.setredraw(true)
setpointer(arrow!)
end subroutine

public function string wf_leggi_parent (long fl_handle);str_attrezzature_tree lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = fl_handle
if ll_item = 0 then
	return ""
end if

do
	tv_1.getitem(ll_item,ltv_item)
	lstr_item = ltv_item.data
	return lstr_item.tipo_livello	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
loop while ll_item <> -1

return ""
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);str_attrezzature_tree lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = al_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello
		case "D"
			as_sql += " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + lstr_item.codice + "' ) "			
		case "R"
			as_sql += " and cod_reparto = '" + lstr_item.codice + "' "
		case "C"
			as_sql += " and cod_cat_attrezzature = '" + lstr_item.codice + "' "
		case "E"
			as_sql += " and cod_area_aziendale = '" + lstr_item.codice + "' "
		case "A"
			as_sql += " and cod_attrezzatura = '" + lstr_item.codice + "' "
		case "N"
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_aree (long fl_handle, string fs_cod_area);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_area_aziendale, ls_des_area, ls_null, ls_appoggio, ls_sql_livello , &
		ls_cod_attrezzatura_da, ls_cod_attrezzatura_a
long ll_risposta, ll_i, ll_livello
str_attrezzature_tree lstr_record
treeviewitem ltvi_campo
setnull(ls_null)
declare cu_aree dynamic cursor for sqlsa;

ls_cod_attrezzatura_da = dw_filtro.getitemstring(1, "cod_attrezzatura_da")
ls_cod_attrezzatura_a = dw_filtro.getitemstring(1, "cod_attrezzatura_a")

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_area_aziendale, des_area FROM tab_aree_aziendali WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_area) and fs_cod_area <> "" then
	ls_sql += " and cod_area_aziendale = '" + fs_cod_area + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_area_aziendale in (select cod_area_aziendale from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + ") "
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by des_area"
	case "C"
		ls_sql += " order by cod_area_aziendale"
end choose
//ls_sql = ls_sql + "ORDER BY des_area"

prepare sqlsa from :ls_sql;
open dynamic cu_aree;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore apertura cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
	g_mb.messagebox("OMNIA",ls_sql)
end if

do while 1=1
   fetch cu_aree into :ls_cod_area_aziendale, :ls_des_area;
   if (sqlca.sqlcode = 100) then
		close cu_aree;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_aree (w_manutenzioni:wf_inserisci_aree)~r~n" + sqlca.sqlerrtext)
		close cu_aree;
		return -1
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_area_aziendale
	lstr_record.descrizione = ls_des_area
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "E"
	ltvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			ltvi_campo.label = ls_des_area + ", " + ls_cod_area_aziendale
		case else
			ltvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area
	end choose
	//ltvi_campo.label = ls_cod_area_aziendale + ", " + ls_des_area

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello AREA")
		close cu_aree;
		return 0
	end if

	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_attrezzature(ll_risposta, ls_cod_attrezzatura_da, ls_cod_attrezzatura_a) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
loop
close cu_aree;
return 0
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

return wf_inserisci_attrezzature(fl_handle, fs_cod_attrezzatura, "")
end function

public function integer wf_inserisci_attrezzature (long fl_handle, string fs_cod_attrezzatura_da, string fs_cod_attrezzatura_a);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean 	lb_dati = false, lb_rosso = false
string 	ls_sql, ls_cod_attrezzatura, ls_des_attrezzatura, ls_des_identificativa, ls_flag_eseguito, &
			ls_appoggio, ls_flag_visione_strumenti, ls_qualita_attrezzatura
long		ll_risposta, ll_i, ll_num_righe, ll_livello
datetime ldt_data_intervento
str_attrezzature_tree lstr_record

ll_livello = il_livello_corrente + 1

declare cu_attrezzature dynamic cursor for sqlsa;

ls_sql = "select cod_attrezzatura, descrizione, qualita_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

// -- stefanop 28/05/2009: Sistemo il becanotto che ho fatto, fortuna che c'è marco :D
if not isnull(fs_cod_attrezzatura_da) and fs_cod_attrezzatura_da <> "" then
	if not isnull(fs_cod_attrezzatura_a) and fs_cod_attrezzatura_a <> "" then
		ls_sql += " and (cod_attrezzatura >= '" + fs_cod_attrezzatura_da + "' and cod_attrezzatura <= '" + fs_cod_attrezzatura_a + "') "
	else
		ls_sql += " and cod_attrezzatura >= '" + fs_cod_attrezzatura_da + "' "
	end if
elseif isnull(fs_cod_attrezzatura_da) and not isnull(fs_cod_attrezzatura_a) then
	ls_sql += " and cod_attrezzatura <= '" + fs_cod_attrezzatura_a + "' "
end if
// --------------

ls_sql += is_sql_filtro

ls_flag_visione_strumenti = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_visione_strumenti")
if not isnull(ls_flag_visione_strumenti) then
	choose case ls_flag_visione_strumenti
		case "A"
			ls_sql += " and qualita_attrezzatura = 'A' "
		case "S"
			ls_sql += " and qualita_attrezzatura in ('P','S') "
	end choose
end if

wf_leggi_parent(fl_handle,ls_sql)

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by descrizione"
	case "C"
		ls_sql += " order by cod_attrezzatura"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_attrezzature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_tab_tipi_manutenzione:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_attrezzature into :ls_cod_attrezzatura, :ls_des_attrezzatura, :ls_qualita_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_attrezzature;
		if lb_dati then return 1
		return 1
	end if
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_attrezzature (w_manutenzioni:wf_inserisci_attrezzature)~r~n" + sqlca.sqlerrtext)
		close cu_attrezzature;
		return 1
	end if
	
	lb_dati = true
	if isnull(ls_des_attrezzatura) then ls_des_attrezzatura = ""

	lstr_record.codice = ls_cod_attrezzatura
	lstr_record.descrizione = ls_des_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "A"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			tvi_campo.label = ls_des_attrezzatura + ", " + ls_cod_attrezzatura
		case else
			tvi_campo.label = ls_cod_attrezzatura + ", " + ls_des_attrezzatura
	end choose

	// Controllo
	/*lb_rosso = false

	declare cur_manut cursor for
		select flag_eseguito  , data_registrazione
		from manutenzioni
	 	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura;
		 
	open cur_manut;
	fetch cur_manut into :ls_flag_eseguito, :ldt_data_intervento;
	do while sqlca.sqlcode = 0
		if ls_flag_eseguito = 'N' then
			if ldt_data_intervento <= datetime(today()) then lb_rosso = true
		end if
		fetch cur_manut into :ls_flag_eseguito,:ldt_data_intervento;
	loop
	
	close cur_manut;
	if lb_rosso then
		//sle_1.BackColor = RGB(255, 0, 0) //Red
		tvi_campo.pictureindex = 1
		tvi_campo.selectedpictureindex = 1
		tvi_campo.overlaypictureindex = 2
	else
		//sle_1.BackColor = RGB(0, 255, 0) //Green
		tvi_campo.pictureindex = 2
		tvi_campo.selectedpictureindex = 2
		tvi_campo.overlaypictureindex = 2
	end if
	*/
	
	/*
	if ls_qualita_attrezzatura = "P" then
		tvi_campo.pictureindex = 5
		tvi_campo.selectedpictureindex = 5
		tvi_campo.overlaypictureindex = 5
	else
		tvi_campo.pictureindex = 4
		tvi_campo.selectedpictureindex = 4
		tvi_campo.overlaypictureindex = 4
	end if
	*/
	
	tvi_campo.pictureindex = 2
	tvi_campo.selectedpictureindex = 2
	tvi_campo.overlaypictureindex = 2
		
	tvi_campo.children = true
	tvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
loop
close cu_attrezzature;

return 0
end function

public function integer wf_inserisci_categorie (long fl_handle, string fs_cod_categoria);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_cat_attrezzatura, ls_des_cat_attrezzatura, ls_null, ls_appoggio, ls_sql_livello, &
	ls_cod_attrezzatura_da, ls_cod_attrezzatura_a
long ll_risposta, ll_i,ll_livello
treeviewitem ltvi_campo
str_attrezzature_tree lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1
ls_cod_attrezzatura_da = dw_filtro.getitemstring(1, "cod_attrezzatura_da")
ls_cod_attrezzatura_a = dw_filtro.getitemstring(1, "cod_attrezzatura_a")

declare cu_categorie dynamic cursor for sqlsa;
ls_sql = "SELECT cod_cat_attrezzature, des_cat_attrezzature FROM tab_cat_attrezzature WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_categoria) and fs_cod_categoria <> "" then
	ls_sql += " and cod_cat_attrezzature = '" + fs_cod_categoria + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_cat_attrezzature in (select cod_cat_attrezzature from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + ") "
	//ls_sql += " and cod_cat_attrezzature in (select cod_cat_attrezzature from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + ls_sql_livello + is_sql_filtro + ") "
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by des_cat_attrezzature"
	case "C"
		ls_sql += " order by cod_cat_attrezzature"
end choose

prepare sqlsa from :ls_sql;
open dynamic cu_categorie;
do while 1=1
   fetch cu_categorie into :ls_cod_cat_attrezzatura, :ls_des_cat_attrezzatura;
   if (sqlca.sqlcode = 100) then
		close cu_categorie;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_categorie (w_manutenzioni:wf_inserisci_categorie)~r~n" + sqlca.sqlerrtext)
		close cu_categorie;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_cat_attrezzatura) then ls_des_cat_attrezzatura = ""
	
	ltvi_campo.itemhandle = fl_handle
	lstr_record.codice = ls_cod_cat_attrezzatura
	lstr_record.descrizione = ls_des_cat_attrezzatura
	lstr_record.handle_padre = fl_handle
	lstr_record.tipo_livello = "C"
	lstr_record.livello = ll_livello
	ltvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			ltvi_campo.label = ls_des_cat_attrezzatura + ", " + ls_cod_cat_attrezzatura
		case else
			ltvi_campo.label = ls_cod_cat_attrezzatura + ", " + ls_des_cat_attrezzatura
	end choose
	//ltvi_campo.label = ls_cod_cat_attrezzatura + ", " + ls_des_cat_attrezzatura

	ltvi_campo.pictureindex = 4
	ltvi_campo.selectedpictureindex = 4
	ltvi_campo.overlaypictureindex = 4
	ltvi_campo.children = true
	ltvi_campo.selected = false
	
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	
	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_attrezzature(ll_risposta, ls_cod_attrezzatura_da, ls_cod_attrezzatura_a) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
	
loop
close cu_categorie;
return 0
end function

public function integer wf_inserisci_divisioni (long fl_handle, string fs_cod_divisione);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello aree aziendali
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false

string    ls_sql, ls_cod_divisione, ls_des_divisione, ls_null, ls_appoggio, ls_sql_livello, ls_padre

long      ll_risposta, ll_i, ll_livello

str_attrezzature_tree lstr_record

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_divisione, des_divisione FROM anag_divisioni WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_divisione)  and len(fs_cod_divisione) > 0 then
	ls_sql += " and cod_divisione = '" + fs_cod_divisione + "' "
end if


// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

if ll_livello > 1 then
	ls_padre = wf_leggi_parent(fl_handle)
end if

if ls_padre = "E" then
	wf_leggi_parent(fl_handle,ls_sql_livello)
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by des_divisione"
	case "C"
		ls_sql += " order by cod_divisione"
end choose

declare cu_divisioni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_divisioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_divisioni: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_divisioni;
	return 0
end if

do while 1=1
   fetch cu_divisioni into :ls_cod_divisione, 
								   :ls_des_divisione;
									
   if (sqlca.sqlcode = 100) then
		close cu_divisioni;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_divisioni (w_manutenzioni:wf_inserisci_divisioni)~r~n" + sqlca.sqlerrtext)
		close cu_divisioni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.codice = ls_cod_divisione
	lstr_record.descrizione = ls_des_divisione
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "D"
	tvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			tvi_campo.label = ls_des_divisione + ", " + ls_cod_divisione
		case else
			tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
	end choose
	//tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione

	tvi_campo.pictureindex = 5
	tvi_campo.selectedpictureindex = 5
	tvi_campo.overlaypictureindex = 5
	tvi_campo.children = true
	tvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, tvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW al livello DIVISIONE")
		close cu_divisioni;
		return 0
	end if

loop
close cu_divisioni;
return 0
end function

public function integer wf_inserisci_reparti (long fl_handle, string fs_cod_reparto);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento livello categorie attrezzature
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean lb_dati = false
string ls_sql, ls_cod_reparto, ls_des_reparto, ls_null, ls_appoggio, ls_sql_livello, &
		ls_cod_attrezzatura_da, ls_cod_attrezzatura_a
long ll_risposta, ll_i,ll_livello
str_attrezzature_tree lstr_record
treeviewitem ltvi_campo
setnull(ls_null)

ll_livello = il_livello_corrente + 1
ls_cod_attrezzatura_da = dw_filtro.getitemstring(1, "cod_attrezzatura_da")
ls_cod_attrezzatura_a = dw_filtro.getitemstring(1, "cod_attrezzatura_a")

declare cu_reparti dynamic cursor for sqlsa;
ls_sql = "SELECT cod_reparto, des_reparto FROM anag_reparti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_reparto) then
	ls_sql += " and cod_reparto = '" + fs_cod_reparto + "' "
end if

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_reparto in (select cod_reparto from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + ") "
	// ls_sql += " and cod_reparto in (select cod_reparto from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + is_sql_filtro + ") "
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by des_reparto"
	case "C"
		ls_sql += " order by cod_reparto"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_reparti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
	return -1
end if

do while 1=1
   fetch cu_reparti into :ls_cod_reparto, :ls_des_reparto;
   if (sqlca.sqlcode = 100) then
		close cu_reparti;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_reparti (w_manutenzioni:wf_inserisci_reparti)~r~n" + sqlca.sqlerrtext)
		close cu_reparti;
		return -1
	end if
	
	if isnull(ls_des_reparto) then ls_des_reparto = ""
	lb_dati = true
	lstr_record.codice = ls_cod_reparto
	lstr_record.descrizione = ls_des_reparto
	lstr_record.tipo_livello = "R"
	lstr_record.livello = ll_livello
	lstr_record.handle_padre = fl_handle

	ltvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			ltvi_campo.label = ls_des_reparto + ", " + ls_cod_reparto
		case else
			ltvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto
	end choose
	//ltvi_campo.label = ls_cod_reparto + ", " + ls_des_reparto

	ltvi_campo.pictureindex = 6
	ltvi_campo.selectedpictureindex = 6
	ltvi_campo.overlaypictureindex = 6
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento reparti nel TREEVIEW")
		close cu_reparti;
		return 0
	end if
	
	// -- stefanop 08/02/2010
	if (ll_livello + 1) < 4 and dw_filtro.getitemstring(1, "flag_tipo_livello_" + string(ll_livello + 1)) = "N" then
		if wf_inserisci_attrezzature(ll_risposta, ls_cod_attrezzatura_da, ls_cod_attrezzatura_a) = 1 then
			tv_1.deleteitem(ll_risposta)
		else
			ltvi_campo.ExpandedOnce = true
			tv_1.setitem(ll_risposta, ltvi_campo)
		end if
	end if
	// ----
	
loop
close cu_reparti;
return 0
end function

public function integer wf_inserisci_manutenzioni (long al_handle, string as_cod_attrezzatura);string 					ls_sql, ls_cod_tipo_manutenzione, ls_des_tipo_manutenzione, ls_flag_blocco, ls_cod_attrezzatura
long						ll_livello
boolean					lb_dati
str_attrezzature_tree lstr_record

lb_dati = false
ll_livello = il_livello_corrente + 1

declare cu_manutenzioni dynamic cursor for sqlsa;

ls_sql = "select cod_tipo_manutenzione, des_tipo_manutenzione, cod_attrezzatura  from tab_tipi_manutenzione where cod_azienda='" + s_cs_xx.cod_azienda +"' " + is_sql_filtro
if not isnull(as_cod_attrezzatura) AND  len(trim(as_cod_attrezzatura)) > 0 then
	ls_sql += " and cod_attrezzatura ='" + as_cod_attrezzatura +"' "
end if

ls_flag_blocco = dw_filtro.getitemstring(1, "flag_blocco")
if ls_flag_blocco = "S" then
	ls_sql +=  " AND (flag_blocco = 'S' AND (data_blocco is null OR data_blocco <= " + string(datetime(today(), now()), s_cs_xx.db_funzioni.formato_data) + ") ) "
elseif ls_flag_blocco = "N" then
	ls_sql +=  " AND flag_blocco = 'N' "
end if

choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
	case "D"
		ls_sql += " order by des_tipo_manutenzione ASC, cod_tipo_manutenzione ASC"
	case "C"
		ls_sql += " order by cod_tipo_manutenzione ASC, cod_attrezzatura ASC"
end choose

prepare sqlsa from :ls_sql;

open dynamic cu_manutenzioni;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_manutenzioni (w_tab_tipi_manutenzione:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
	g_mb.messagebox("OMNIA", ls_sql)
	return -1
end if

do while true
	fetch cu_manutenzioni into :ls_cod_tipo_manutenzione, :ls_des_tipo_manutenzione, :ls_cod_attrezzatura;
	
   if sqlca.sqlcode = 100 then
		close cu_manutenzioni;
		exit
	end if
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_manutenzioni (w_manutenzioni:wf_inserisci_manutenzioni)~r~n" + sqlca.sqlerrtext)
		close cu_manutenzioni;
		return -1
	end if
	
	lb_dati = true
	if isnull(ls_des_tipo_manutenzione) then ls_des_tipo_manutenzione = "<descrizione mancate>"

	lstr_record.codice = ls_cod_tipo_manutenzione
	lstr_record.cod_attrezzatura = ls_cod_attrezzatura
	
	if isnull(ls_des_tipo_manutenzione) or ls_des_tipo_manutenzione = "" then
		lstr_record.descrizione = "<descrizione mancante>"
	elseif len(ls_des_tipo_manutenzione) > 30 then
		lstr_record.descrizione = left(ls_des_tipo_manutenzione, 30) + "..."
	else
		lstr_record.descrizione = ls_des_tipo_manutenzione
	end if
	lstr_record.handle_padre = al_handle
	lstr_record.tipo_livello = "M"
	lstr_record.livello = ll_livello
	tvi_campo.data = lstr_record
	
	choose case dw_filtro.getitemstring(1, "flag_ordinamento_dati")
		case "D"
			tvi_campo.label = lstr_record.descrizione + ", " + ls_cod_tipo_manutenzione
		case else
			tvi_campo.label = ls_cod_tipo_manutenzione + ", " + lstr_record.descrizione
	end choose
	//tvi_campo.label = ls_cod_tipo_manutenzione + ", " + lstr_record.descrizione
	
	tvi_campo.pictureindex = 7
	tvi_campo.selectedpictureindex = 7
	tvi_campo.overlaypictureindex = 7
	
	tvi_campo.children = false
	tvi_campo.selected = false
	
	tv_1.insertitemlast(al_handle, tvi_campo)

loop

close cu_manutenzioni;

if lb_dati then
	return 0
else
	return 1
end if
end function

public subroutine wf_cancella_livello (long fl_handle);long tvi_hdl = 0

tvi_hdl = tv_1.FindItem (ChildTreeItem!, fl_handle)
DO UNTIL tvi_hdl <= 0
	tv_1.deleteitem(tvi_hdl)
	tvi_hdl = tv_1.FindItem (ChildTreeItem!, fl_handle)
LOOP

return
end subroutine

public subroutine wf_inserisci_singola_manutenzione (long row);// return
long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_record

// -- Controllo il nodo selezionato
ll_handle = tv_1.finditem(currenttreeitem!, 0)
if ll_handle = -1 then return

if tv_1.getitem(ll_handle, tvi_item) = -1 then return

lstr_record = tvi_item.data
if lstr_record.tipo_livello = "M" then
	ll_handle = tv_1.finditem(parenttreeitem!, ll_handle)
end if
// ---------------------

lstr_record.codice = dw_tipi_manutenzione_det_1.getitemstring(row, "cod_tipo_manutenzione")
lstr_record.cod_attrezzatura = dw_tipi_manutenzione_det_1.getitemstring(row, "cod_attrezzatura")
lstr_record.descrizione = left(dw_tipi_manutenzione_det_1.getitemstring(row, "des_tipo_manutenzione"), 30) + "..."
lstr_record.handle_padre = ll_handle
lstr_record.tipo_livello = "M"
lstr_record.livello = il_livello_corrente + 1
tvi_campo.data = lstr_record

if isnull(lstr_record.descrizione) or lstr_record.descrizione = "" then
	lstr_record.descrizione = "<descrizione mancante>"
elseif len(lstr_record.descrizione) > 30 then
	lstr_record.descrizione = left(lstr_record.descrizione, 30) + "..."
end if
tvi_campo.label = lstr_record.codice + ", " + lstr_record.descrizione

tvi_campo.pictureindex = 7
tvi_campo.selectedpictureindex = 7
tvi_campo.overlaypictureindex = 7

tvi_campo.children = false
tvi_campo.selected = false

tv_1.insertitemlast(ll_handle, tvi_campo)
end subroutine

public subroutine wf_cancella_manutenzione ();long ll_handle
treeviewitem tvi_item
str_attrezzature_tree lstr_data

ll_handle = tv_1.finditem(currenttreeitem!, 0)
if ll_handle = -1 then return

tv_1.getitem(ll_handle, tvi_item)
lstr_data = tvi_item.data

if lstr_data.tipo_livello = "M" then
	tv_1.deleteitem(ll_handle)
end if
end subroutine

public function integer wf_aggiorna_programmi (long fl_row);string ls_cod_tipo_manutenzione, ls_cod_attrezzatura
decimal ld_num_gg_avviso, ld_num_gg_promemoria, ld_frequenza
string ls_flag_giorno_prec, ls_flag_festivi, ls_periodicita

//tranquillo, questa funzione è chiamata solo per sintexcal (parametro AZP='S')

ls_cod_tipo_manutenzione = dw_tipi_manutenzione_det_1.getitemstring(fl_row, "cod_tipo_manutenzione")
ls_cod_attrezzatura = dw_tipi_manutenzione_det_1.getitemstring(fl_row, "cod_attrezzatura")

if not isnull(ls_cod_tipo_manutenzione) and ls_cod_tipo_manutenzione<>"" and &
	not isnull(ls_cod_attrezzatura) and ls_cod_attrezzatura<>"" then
	
	ls_periodicita = dw_tipi_manutenzione_det_5.getitemstring(fl_row,"periodicita")
	ld_frequenza = dw_tipi_manutenzione_det_5.getitemnumber(fl_row,"frequenza")
//	ld_num_gg_avviso = dw_tipi_manutenzione_det_5.getitemnumber(fl_row,"num_giorni_avviso")
//	ld_num_gg_promemoria = dw_tipi_manutenzione_det_5.getitemnumber(fl_row,"num_giorni_promemoria")
//	ls_flag_giorno_prec = dw_tipi_manutenzione_det_5.getitemstring(fl_row,"flag_giorno_prec")
//	ls_flag_festivi = dw_tipi_manutenzione_det_5.getitemstring(fl_row,"flag_festivi")
	
	//aggiorna i programmi manutenzione
	update programmi_manutenzione
	set periodicita=:ls_periodicita,
		frequenza=:ld_frequenza
	where cod_azienda=:s_cs_xx.cod_azienda and
		cod_tipo_manutenzione=:ls_cod_tipo_manutenzione and cod_attrezzatura=:ls_cod_attrezzatura;
		
	if sqlca.sqlcode<0 then
		g_mb.messagebox("","Errore durante l'aggiornamento dei programmi manutenzione: "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
//	//aggiorna le manutenzioni ancora aperte
//	update manutenzioni
//	set num_giorni_avviso=:ld_num_gg_avviso,
//		num_giorni_promemoria=:ld_num_gg_promemoria,
//		flag_giorno_prec=:ls_flag_giorno_prec,
//		flag_festivi=:ls_flag_festivi
//	where cod_azienda=:s_cs_xx.cod_azienda and
//		cod_tipo_manutenzione=:ls_cod_tipo_manutenzione and cod_attrezzatura=:ls_cod_attrezzatura and
//		flag_eseguito='N';
//	
//	if sqlca.sqlcode<0 then
//		g_mb.messagebox("","Errore durante l'aggiornamento delle manutenzioni: "+sqlca.sqlerrtext, StopSign!)
//		return -1
//	end if
	
end if
end function

event pc_setwindow;call super::pc_setwindow;string ls_flag_manutenzione, ls_flag_manutenzioni_avanzate
windowobject l_objects[ ], l_objects1[ ], l_objects2[]


l_objects[1] = dw_tipi_manutenzione_det_5
dw_folder.fu_AssignTab(2, "&Programmazione", l_Objects[])

l_objects[1] = dw_tipi_manutenzione_det_3
dw_folder.fu_AssignTab(4, "&Magazzino / Varie", l_Objects[])

l_objects[1] = dw_tipi_manutenzione_det_4
dw_folder.fu_AssignTab(5, "&Dati Tarature", l_Objects[])

l_objects[1] = dw_tipi_manutenzione_det_2
dw_folder.fu_AssignTab(3, "&Istruzioni di Lavoro", l_Objects[])

l_objects[1] = dw_tipi_manutenzione_det_1
dw_folder.fu_AssignTab(1, "&Dati Generali", l_Objects[])

dw_tipi_manutenzione_det_1.set_dw_key("cod_azienda")
dw_tipi_manutenzione_det_1.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_tipi_manutenzione_det_2.set_dw_options(sqlca,dw_tipi_manutenzione_det_1,c_sharedata+c_scrollparent,c_default)
dw_tipi_manutenzione_det_3.set_dw_options(sqlca,dw_tipi_manutenzione_det_1,c_sharedata+c_scrollparent,c_default)
dw_tipi_manutenzione_det_4.set_dw_options(sqlca,dw_tipi_manutenzione_det_1,c_sharedata+c_scrollparent,c_default)
dw_tipi_manutenzione_det_5.set_dw_options(sqlca,dw_tipi_manutenzione_det_1,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tipi_manutenzione_det_1

dw_folder.fu_FolderCreate(5,5)
dw_folder.fu_SelectTab(1)

declare cu_cursore cursor for
select flag_manutenzione   	
  from tab_tipi_manutenzione
 where cod_azienda = :s_cs_xx.cod_azienda;
 
open cu_cursore;

fetch cu_cursore into :ls_flag_manutenzione;
 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Omnia", "Errore in lettora dati da tabella tab_tipi_manutenzione " + sqlca.sqlerrtext)
	return
end if	

if sqlca.sqlcode = 100 then ls_flag_manutenzione = 'M'

close cu_cursore;

select flag_gest_manutenzione
into :ls_flag_manutenzioni_avanzate
from parametri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	ls_flag_manutenzioni_avanzate = "N"
end if

if ls_flag_manutenzioni_avanzate = "N" then
	cb_risorse.visible = false
else
	cb_risorse.visible = true
end if



// Filtro
dw_filtro.insertrow(0)
l_objects2[1] = tv_1
dw_folder_tree.fu_assigntab(2, "Manutenzioni", l_objects2[])
l_objects2[1] = dw_filtro
l_objects2[2] = cb_cerca
l_objects2[3] = cb_richiama_predefinito
l_objects2[4] = cb_predefinito
dw_folder_tree.fu_assigntab(1, "Filtro", l_objects2[])
dw_folder_tree.fu_foldercreate(2, 2)
dw_folder_tree.fu_selecttab(1)

// Icone Treeview
tv_1.deletepictures()
// 1, 2 -- attrezzatura
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_off.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_attr_on.png")

// 3 -- area
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

// 4 -- categoria
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_categorie.png")

// 5 -- divisioni
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_divisioni.png")

// 6 -- reparti
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_reparti.png")

// 7 -- manutenzioni
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_man.png")


dw_filtro.postevent("ue_leggi_filtro")

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_1,"cod_ricambio_alternativo",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')"  )

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_1, &
                 "cod_tipo_lista_dist", &
					  sqlca, &
                 "tab_tipi_liste_dist", &
					  "cod_tipo_lista_dist", &
					  "descrizione", &
					  "")
	
// stefanop 04/02/2011: non bisogna caricarle qui, ma alla fine della retrieve della dw_tipi_manutenzione_det_1				  
//f_po_loaddddw_dw( dw_tipi_manutenzione_det_1, "cod_lista_dist", sqlca, &
//						"tes_liste_dist", &
//						"cod_lista_dist", &
//						"des_lista_dist", &
//						"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_tipo_off_acq",sqlca,&
                 "tab_tipi_off_acq","cod_tipo_off_acq","des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_tipo_ord_acq",sqlca,&
                 "tab_tipi_ord_acq","cod_tipo_ord_acq","des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_tipo_det_acq",sqlca,&
                 "tab_tipi_det_acq","cod_tipo_det_acq","des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_tipo_movimento",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_cat_risorse_esterne",sqlca,&
                 "tab_cat_risorse_esterne","cod_cat_risorse_esterne","des_cat_risorse_esterne", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_4,"cod_primario",sqlca,&
                 "anag_attrezzature","cod_attrezzatura","descrizione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_4,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


// stefanop 19/03/2009: Liste per il filtro
f_PO_LoadDDDW_DW(dw_filtro,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_filtro,"cod_cat_attrezzatura",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_risorsa_umana='N'")
					  
f_PO_LoadDDDW_DW(dw_filtro,"cod_divisione",sqlca,&
                 "anag_divisioni","cod_divisione","des_divisione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW (dw_filtro,"cod_tipo_manutenzione",sqlca,&
                 "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
// -------------------------

					  
end event

on w_tab_tipi_manutenzione.create
int iCurrent
call super::create
this.cb_cerca=create cb_cerca
this.cb_modifica=create cb_modifica
this.cb_trasferisci=create cb_trasferisci
this.cb_ricambi=create cb_ricambi
this.cb_duplica=create cb_duplica
this.cb_risorse=create cb_risorse
this.cb_documento=create cb_documento
this.cb_richiama_predefinito=create cb_richiama_predefinito
this.cb_predefinito=create cb_predefinito
this.dw_tipi_manutenzione_det_4=create dw_tipi_manutenzione_det_4
this.dw_filtro=create dw_filtro
this.dw_tipi_manutenzione_det_2=create dw_tipi_manutenzione_det_2
this.tv_1=create tv_1
this.dw_folder_tree=create dw_folder_tree
this.dw_tipi_manutenzione_det_5=create dw_tipi_manutenzione_det_5
this.dw_tipi_manutenzione_det_3=create dw_tipi_manutenzione_det_3
this.dw_folder=create dw_folder
this.dw_tipi_manutenzione_det_1=create dw_tipi_manutenzione_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cerca
this.Control[iCurrent+2]=this.cb_modifica
this.Control[iCurrent+3]=this.cb_trasferisci
this.Control[iCurrent+4]=this.cb_ricambi
this.Control[iCurrent+5]=this.cb_duplica
this.Control[iCurrent+6]=this.cb_risorse
this.Control[iCurrent+7]=this.cb_documento
this.Control[iCurrent+8]=this.cb_richiama_predefinito
this.Control[iCurrent+9]=this.cb_predefinito
this.Control[iCurrent+10]=this.dw_tipi_manutenzione_det_4
this.Control[iCurrent+11]=this.dw_filtro
this.Control[iCurrent+12]=this.dw_tipi_manutenzione_det_2
this.Control[iCurrent+13]=this.tv_1
this.Control[iCurrent+14]=this.dw_folder_tree
this.Control[iCurrent+15]=this.dw_tipi_manutenzione_det_5
this.Control[iCurrent+16]=this.dw_tipi_manutenzione_det_3
this.Control[iCurrent+17]=this.dw_folder
this.Control[iCurrent+18]=this.dw_tipi_manutenzione_det_1
end on

on w_tab_tipi_manutenzione.destroy
call super::destroy
destroy(this.cb_cerca)
destroy(this.cb_modifica)
destroy(this.cb_trasferisci)
destroy(this.cb_ricambi)
destroy(this.cb_duplica)
destroy(this.cb_risorse)
destroy(this.cb_documento)
destroy(this.cb_richiama_predefinito)
destroy(this.cb_predefinito)
destroy(this.dw_tipi_manutenzione_det_4)
destroy(this.dw_filtro)
destroy(this.dw_tipi_manutenzione_det_2)
destroy(this.tv_1)
destroy(this.dw_folder_tree)
destroy(this.dw_tipi_manutenzione_det_5)
destroy(this.dw_tipi_manutenzione_det_3)
destroy(this.dw_folder)
destroy(this.dw_tipi_manutenzione_det_1)
end on

type cb_cerca from commandbutton within w_tab_tipi_manutenzione
integer x = 800
integer y = 1840
integer width = 343
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_flag_blocco, ls_categoria, ls_reparto, ls_area, ls_divisione, ls_where_attrezzatura

dw_filtro.accepttext()

is_sql_filtro = ""

// -------------  compongo SQL con filtri ---------------------

// cod attrezzatura da
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da")) then
	is_sql_filtro += " and cod_attrezzatura >= '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da") + "' "
end if

// cod attrezzatura a
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a")) then
	is_sql_filtro += " and cod_attrezzatura <= '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a") + "' "
end if

// cod cat attrezzatura
//if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) then
//	is_sql_filtro += " and cod_cat_attrezzatura = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura") + "' "
//end if

// cod tipo manutenzione
if not isnull(dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione")) then
	is_sql_filtro += " and cod_tipo_manutenzione = '" + dw_filtro.getitemstring(dw_filtro.getrow(),"cod_tipo_manutenzione") + "' "
end if

// flag blocco
ls_flag_blocco = dw_filtro.getitemstring(dw_filtro.getrow(),"flag_blocco")
if ls_flag_blocco <> "T" and not isnull(ls_flag_blocco) then
	if ls_flag_blocco = "S" then
		is_sql_filtro +=  " AND flag_blocco = 'S' AND (data_blocco is null OR data_blocco <= " + string(datetime(today(), now()), s_cs_xx.db_funzioni.formato_data) + ") ) "
	elseif ls_flag_blocco = "N" then
		is_sql_filtro +=  " AND flag_blocco = 'N' "
	end if
	
end if

ls_categoria = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")
ls_reparto	= dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")
ls_area		= dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")
ls_divisione	= dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")

if not isnull(ls_categoria) or not isnull(ls_reparto) or  not isnull(ls_area)  or not isnull(ls_divisione) then
	
	ls_where_attrezzatura = " and cod_attrezzatura in ( select cod_attrezzatura from anag_attrezzature where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if not isnull(ls_categoria) then
		ls_where_attrezzatura += " and cod_cat_attrezzature = '" + ls_categoria + "' "
	end if
	
	if not isnull(ls_reparto) then
		ls_where_attrezzatura +=  " and cod_reparto = '" + ls_reparto + "' "
	end if
	
	if not isnull(ls_area) then
		ls_where_attrezzatura +=  " and cod_area_aziendale = '" + ls_area + "' "
	end if
	
	if not isnull(ls_divisione) then
		ls_where_attrezzatura +=  " and cod_area_aziendale IN ( select cod_area_aziendale from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ) "
	end if	
	
	ls_where_attrezzatura += ")"
	
end if
// ------------------------------------------------------------

is_sql_filtro += ls_where_attrezzatura

wf_cancella_treeview()

wf_imposta_treeview()

dw_folder_tree.fu_selecttab(2)

tv_1.setfocus()
end event

type cb_modifica from commandbutton within w_tab_tipi_manutenzione
integer x = 1600
integer y = 1860
integer width = 379
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mod. Codice"
end type

event clicked;long ll_row


ll_row = dw_tipi_manutenzione_det_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare un tipo manutenzione prima di continuare!")
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_attrezzatura")

s_cs_xx.parametri.parametro_s_2 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_tipo_manutenzione")

window_open(w_mod_tipo_manut,0)

if s_cs_xx.parametri.parametro_d_1 = 0 then 
	dw_tipi_manutenzione_det_1.postevent("pcd_retrieve")
end if
end event

type cb_trasferisci from commandbutton within w_tab_tipi_manutenzione
integer x = 1211
integer y = 1860
integer width = 384
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Trasf. Mod."
end type

event clicked;long ll_row


ll_row = dw_tipi_manutenzione_det_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare un tipo manutenzione prima di continuare!")
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_attrezzatura")

s_cs_xx.parametri.parametro_s_2 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_tipo_manutenzione")

window_open(w_duplica_dati_tipi_man,0)

if s_cs_xx.parametri.parametro_d_1 = 0 then 
	dw_tipi_manutenzione_det_1.postevent("pcd_retrieve")
end if
end event

type cb_ricambi from commandbutton within w_tab_tipi_manutenzione
integer x = 3017
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ricambi"
end type

event clicked;long ll_row


ll_row = dw_tipi_manutenzione_det_1.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("OMNIA","Selezionare un tipo manutenzione prima di continuare!")
	return -1
end if

s_cs_xx.parametri.parametro_s_1 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_attrezzatura")

s_cs_xx.parametri.parametro_s_2 = dw_tipi_manutenzione_det_1.getitemstring(ll_row,"cod_tipo_manutenzione")

window_open(w_tipi_manut_ricambi,0)
end event

type cb_duplica from commandbutton within w_tab_tipi_manutenzione
integer x = 3406
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica"
end type

event clicked;window_open(w_sel_duplica_attrez, 0)
end event

type cb_risorse from commandbutton within w_tab_tipi_manutenzione
integer x = 2629
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Risorse"
end type

event clicked;decimal ld_ore, ld_minuti
time lt_tempo

dw_tipi_manutenzione_det_1.accepttext()

if (isnull(dw_tipi_manutenzione_det_1.getrow()) or dw_tipi_manutenzione_det_1.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna Tipologia di manutenzione")
	return
end if	

if isnull(dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")) then
	g_mb.messagebox("Omnia", "Attenzione non è stata selezionata nessuna Tipologia di manutenzione")
	return
end if	

s_cs_xx.parametri.parametro_s_10 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_attrezzatura")
s_cs_xx.parametri.parametro_s_11 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")

//window_open_parm(w_elenco_risorse, -1, dw_tipi_manutenzione_det_1)

window_open(w_elenco_risorse, 0)

ld_ore = int(s_cs_xx.parametri.parametro_d_8 / 60)
ld_minuti = s_cs_xx.parametri.parametro_d_8 - (ld_ore * 60)

lt_tempo = time(string(ld_ore) + ":" + string(ld_minuti))

dw_tipi_manutenzione_det_1.setitem(dw_tipi_manutenzione_det_1.getrow(), "tempo_previsto", datetime(Date("2000/01/01"), lt_tempo))

setnull(s_cs_xx.parametri.parametro_d_8)

dw_tipi_manutenzione_det_1.postevent("pcd_save")
end event

type cb_documento from commandbutton within w_tab_tipi_manutenzione
integer x = 3794
integer y = 1860
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_tipi_manutenzione_det_1.accepttext()

if (isnull(dw_tipi_manutenzione_det_1.getrow()) or dw_tipi_manutenzione_det_1.getrow() < 1) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato nessun tipo di manutenzione")
	return
end if	

if isnull(dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")) then
	g_mb.messagebox("Omnia", "Attenzione non è stato selezionato nessun di manutenzione")
	return
end if	

/*s_cs_xx.parametri.parametro_s_8 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_attrezzatura")
s_cs_xx.parametri.parametro_s_9 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")

window_open_parm(w_det_tipi_manutenzioni, -1, dw_tipi_manutenzione_det_1)
*/

s_cs_xx.parametri.parametro_s_8 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_attrezzatura")
s_cs_xx.parametri.parametro_s_9 = dw_tipi_manutenzione_det_1.getitemstring(dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")

open(w_tab_tipi_manutenzione_doc)
end event

type cb_richiama_predefinito from commandbutton within w_tab_tipi_manutenzione
integer x = 434
integer y = 1840
integer width = 361
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()

//------------ Michele 22/04/2004 Gestione dipartimentale Sintexcal ----------------------
//if ib_fam then
//	dw_filtro.setitem(1,"cod_area_aziendale",is_area)
//end if
//----------------------------- 22/04/2004 Fine -----------------------------------

dw_filtro.postevent("ue_reset")
end event

type cb_predefinito from commandbutton within w_tab_tipi_manutenzione
integer x = 46
integer y = 1840
integer width = 379
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Pre."
end type

event clicked;dw_filtro.accepttext()
wf_memorizza_filtro()
end event

type dw_tipi_manutenzione_det_4 from uo_cs_xx_dw within w_tab_tipi_manutenzione
integer x = 1234
integer y = 140
integer width = 2880
integer height = 520
integer taborder = 50
string dataobject = "d_tipi_manutenzione_det_4"
boolean border = false
end type

type dw_filtro from u_dw_search within w_tab_tipi_manutenzione
event ue_key pbm_dwnkey
event ue_cerca_attrezzatura ( string as_column )
event ue_reset ( )
event ue_carica_tipologie ( )
event ue_cod_attr ( string data )
event ue_aree_aziendali ( )
event ue_leggi_filtro ( )
integer x = 46
integer y = 120
integer width = 1097
integer height = 1720
integer taborder = 70
string dataobject = "d_tipi_manutenzione_tree"
boolean border = false
end type

event ue_key;if key = keyenter! then
	accepttext()
	cb_cerca.triggerevent("clicked")
	postevent("ue_reset")
end if

end event

event ue_cerca_attrezzatura(string as_column);guo_ricerca.uof_ricerca_attrezzatura(dw_filtro,as_column)
end event

event ue_reset();dw_filtro.resetupdate()
end event

event ue_carica_tipologie();string ls_where, ls_null, ls_reparto, ls_categoria, ls_area, ls_attr_da, ls_attr_a, ls_flag_blocco


ls_attr_da = getitemstring(getrow(),"cod_attrezzatura_da")
ls_attr_a = getitemstring(getrow(),"cod_attrezzatura_a")
ls_reparto = getitemstring(getrow(),"cod_reparto")
ls_categoria = getitemstring(getrow(),"cod_cat_attrezzatura")
ls_area = getitemstring(getrow(),"cod_area_aziendale")
ls_flag_blocco = getitemstring(getrow(),"flag_blocco")

setnull(ls_null)

setitem(getrow(),"cod_tipo_manutenzione",ls_null)
ls_where = "a.cod_azienda = b.cod_azienda and a.cod_attrezzatura = b.cod_attrezzatura and " + &
			  "a.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_attr_da) then
	ls_where += " and a.cod_attrezzatura >= '" + ls_attr_da + "' "
end if

if not isnull(ls_attr_a) then
	ls_where += " and a.cod_attrezzatura <= '" + ls_attr_a + "' "
end if

if not isnull(ls_reparto) then
	ls_where += " and b.cod_reparto = '" + ls_reparto + "' "
end if

if not isnull(ls_categoria) then
	ls_where += " and b.cod_cat_attrezzature = '" + ls_categoria + "' "
end if

if not isnull(ls_area) then
	ls_where += " and b.cod_area_aziendale = '" + ls_area + "' "
end if

if ls_flag_blocco <> "I" then
	ls_where += " and b.flag_blocco = '" + ls_flag_blocco + "' "
end if


f_po_loaddddw_dw(this,"cod_tipo_manutenzione",sqlca,"tab_tipi_manutenzione a,anag_attrezzature b", &
					  "a.cod_tipo_manutenzione", "a.des_tipo_manutenzione",ls_where)
end event

event ue_cod_attr(string data);if not isnull(data) then
	setitem(getrow(),"cod_attrezzatura_a",data)
end if
end event

event ue_aree_aziendali();string ls_divisione

ls_divisione = getitemstring(getrow(),"cod_divisione")
setitem(getrow(), "cod_area_aziendale", "")

if not isnull(ls_divisione) then	
	f_PO_LoadDDDW_sort (dw_filtro,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione = '" + ls_divisione + "' ", " ordinamento ASC ")					  		
else
	f_PO_LoadDDDW_sort (dw_filtro,"cod_area_aziendale",sqlca,&
						  "tab_aree_aziendali","cod_area_aziendale","des_area",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", " ordinamento ASC ")
					  	
end if

triggerevent("ue_carica_tipologie")
end event

event ue_leggi_filtro();wf_leggi_filtro()
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "b_attrezzatura_da"
		event ue_cerca_attrezzatura("cod_attrezzatura_da")

	case "b_attrezzatura_a"
		event ue_cerca_attrezzatura("cod_attrezzatura_a")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_null, ls_text

setnull(ls_null)
ls_text = gettext()

choose case getcolumnname()
	case "cod_attrezzatura"
		if ls_text = "" or isnull(ls_text) then
			dw_filtro.object.cod_guasto_t.visible = false
			dw_filtro.object.compute_5.visible = false
			dw_filtro.object.cod_guasto.visible = false
			dw_filtro.object.cod_tipo_manutenzione_t.visible = false
			dw_filtro.object.compute_6.visible = false
			dw_filtro.object.cod_tipo_manutenzione.visible = false
			setitem(getrow(),"cod_guasto",ls_null)
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
		else
			dw_filtro.object.cod_guasto_t.visible = true
			dw_filtro.object.compute_5.visible = true
			dw_filtro.object.cod_guasto.visible = true
			dw_filtro.object.cod_tipo_manutenzione_t.visible = true
			dw_filtro.object.compute_6.visible = true
			dw_filtro.object.cod_tipo_manutenzione.visible = true
			setitem(getrow(),"cod_guasto",ls_null)
			setitem(getrow(),"cod_tipo_manutenzione",ls_null)
			f_PO_LoadDDDW_DW(dw_filtro,"cod_tipo_manutenzione",sqlca,&
								  "tab_tipi_manutenzione","cod_tipo_manutenzione","des_tipo_manutenzione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_text + "'")

			f_PO_LoadDDDW_DW(dw_filtro,"cod_guasto",sqlca,&
								  "tab_guasti","cod_guasto","des_guasto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_attrezzatura = '" + ls_text + "'")
			
		end if
	case "flag_tipo_livello_1"
		setitem(getrow(),"flag_tipo_livello_2", "N")
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_2", 150)
		end if
		
	case "flag_tipo_livello_2"
		setitem(getrow(),"flag_tipo_livello_3", "N")
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_3", 0)
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_3", 160)
		end if
		
	case "flag_tipo_livello_3"
		setitem(getrow(),"flag_tipo_livello_4", "N")
		settaborder("flag_tipo_livello_4", 0)
		if ls_text <> "N" then
			settaborder("flag_tipo_livello_4", 170)
		end if
		
	case "cod_attrezzatura_da"
		event ue_cod_attr(data)
		postevent("ue_carica_tipologie")
		
	case "cod_attrezzatura_a","cod_reparto","cod_categoria","cod_area"
		postevent("ue_carica_tipologie")
		
	case "cod_divisione"
		postevent("ue_aree_aziendali")
		
end choose

postevent("ue_reset")

end event

type dw_tipi_manutenzione_det_2 from uo_cs_xx_dw within w_tab_tipi_manutenzione
integer x = 1211
integer y = 120
integer width = 2903
integer height = 1700
integer taborder = 60
string dataobject = "d_tipi_manutenzione_det_2"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name
	case "cb_note_1"
//		if not isnull(s_cs_xx.parametri.parametro_s_1) then
//			string ls_cod_attrezzatura, ls_cod_tipo_manutenzione
//			long   ll_row
//			ls_cod_attrezzatura = dw_tipi_manutenzione_det_1.getitemstring( dw_tipi_manutenzione_det_1.getrow(), "cod_attrezzatura")
//			ls_cod_tipo_manutenzione = dw_tipi_manutenzione_det_1.getitemstring( dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")
//		
//			update tab_tipi_manutenzione
//			set modalita_esecuzione = :s_cs_xx.parametri.parametro_s_1
//			where
//				cod_azienda = :s_cs_xx.cod_azienda and
//				cod_attrezzatura = :ls_cod_attrezzatura and
//				cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
//				
//			if sqlca.sqlcode <> 0 then
//				g_mb.messagebox("OMNIA", "Errore in inserimento della nota.~r~n"+sqlca.sqlerrtext)
//				rollback;
//				return
//			else
//				commit;
//			end if
//			
//			ll_row = dw_tipi_manutenzione_det_1.getrow()
//			dw_tipi_manutenzione_det_1.change_dw_current()
//			parent.triggerevent("pc_retrieve")
//			
//			dw_tipi_manutenzione_det_1.scrolltorow(ll_row)
//		end if
//		
//		triggerevent("ue_carica_valori")
//		window_open(w_ext_note_mss,0)

		// stefanop 11/10/2010: ticket 2010/264
		string ls_cod_attrezzatura, ls_cod_tipo_manutenzione
		long   ll_row
	
		s_cs_xx.parametri.parametro_s_1 = getitemstring(row, "modalita_esecuzione")
		window_open(w_ext_note_mss, 0)
		
		ls_cod_attrezzatura = dw_tipi_manutenzione_det_1.getitemstring( dw_tipi_manutenzione_det_1.getrow(), "cod_attrezzatura")
		ls_cod_tipo_manutenzione = dw_tipi_manutenzione_det_1.getitemstring( dw_tipi_manutenzione_det_1.getrow(), "cod_tipo_manutenzione")
	
		update tab_tipi_manutenzione
		set modalita_esecuzione = :s_cs_xx.parametri.parametro_s_1
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_attrezzatura = :ls_cod_attrezzatura and
			cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("OMNIA", "Errore in inserimento della nota.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		else
			commit;
		end if

		ll_row = dw_tipi_manutenzione_det_1.getrow()
		dw_tipi_manutenzione_det_1.change_dw_current()
		parent.triggerevent("pc_retrieve")
		triggerevent("ue_carica_valori")
		
		dw_tipi_manutenzione_det_1.scrolltorow(ll_row)

end choose
end event

type tv_1 from treeview within w_tab_tipi_manutenzione
integer x = 46
integer y = 120
integer width = 1097
integer height = 1800
integer taborder = 80
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
boolean hideselection = false
string picturename[] = {"","","","","",""}
long picturemaskcolor = 553648127
string statepicturename[] = {"SetVariable!","DeclareVariable!"}
long statepicturemaskcolor = 553648127
end type

event itempopulate;string ls_null, ls_cat_1, ls_cat_2
long ll_livello = 0, ll_ret
treeviewitem ltv_item
str_attrezzature_tree lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 4 then
	
	if dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(ll_livello + 1)) = "N" and lws_record.tipo_livello ="A" then
		if wf_inserisci_manutenzioni(handle, lws_record.codice) = 1 then
			ltv_item.children = false
			tv_1.setitem(handle, ltv_item)
		end if
	else
		
	// controlla cosa c'è al livello successivo ???
	choose case dw_filtro.getitemstring(dw_filtro.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "D"
			// caricamento divisioni
			if wf_inserisci_divisioni(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_divisione")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento reparti
			if wf_inserisci_reparti(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_reparto")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento categorie
			if wf_inserisci_categorie(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_cat_attrezzatura")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "E" 
			// caricamento aree
			if wf_inserisci_aree(handle, dw_filtro.getitemstring(dw_filtro.getrow(),"cod_area_aziendale")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A", "N"
			// caricamento attrezzature
			ls_cat_1 =dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_da")
			ls_cat_2 = dw_filtro.getitemstring(dw_filtro.getrow(),"cod_attrezzatura_a")
			
			if wf_inserisci_attrezzature(handle, ls_cat_1, ls_cat_2)  = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if

		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
	
	end if
else
	// inserisco le registrazioni
	if lws_record.tipo_livello ="A" then
		if wf_inserisci_manutenzioni(handle, lws_record.codice) = 1 then
			ltv_item.children = false
			tv_1.setitem(handle, ltv_item)
		end if
	else
		if wf_inserisci_attrezzature(handle, dw_filtro.getitemstring(1, "cod_attrezzatura_da"), dw_filtro.getitemstring(1, "cod_attrezzatura_a")) = 1 then
			ltv_item.children = false
			tv_1.setitem(handle, ltv_item)
		end if
	end if
	
end if

commit;


// Dati automatici
choose case lws_record.tipo_livello 
	case "D"
		// divisione
		is_divisione = lws_record.codice
		
	case "R"
		// reparti
		is_reparto = lws_record.codice
		
	case "C"
		// categorie
		is_categoria = lws_record.codice
		
	case "E" 
		// area
		is_area = lws_record.codice
		
	case "A"
		// attrezzatura
		is_cod_attrezzatura = lws_record.codice
end choose
end event

event rightclicked;treeviewitem ltv_item
str_attrezzature_tree lws_record

if handle < 1 then return

getitem(handle,ltv_item)
lws_record = ltv_item.data
is_cod_attrezzatura = lws_record.codice

// non sono im modifica o nuovo
if not dw_tipi_manutenzione_det_1.ib_stato_nuovo and not dw_tipi_manutenzione_det_1.ib_stato_modifica then 
	
	m_manutenzione_menu lm_menu
	lm_menu = create m_manutenzione_menu
		
	// manutenzione
	if lws_record.tipo_livello = "M" then
		lm_menu.m_refresh.visible = false
	else
		lm_menu.m_refresh.visible = true
	end if
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	destroy lm_menu

end if
end event

event selectionchanged;treeviewitem ltv_item
str_attrezzature_tree lws_record

if ib_tree_delete then return 0

if isnull(newhandle) or newhandle <= 0 then
	return 0
end if

il_ultimo_handle = newhandle

getitem(newhandle,ltv_item)
lws_record = ltv_item.data

// Dati automatici
choose case lws_record.tipo_livello 
	case "D"
		// divisione
		is_divisione = lws_record.codice
		
	case "R"
		// reparti
		is_reparto = lws_record.codice
		
	case "C"
		// categorie
		is_categoria = lws_record.codice
		
	case "E" 
		// area
		is_area = lws_record.codice
		
	case "A"
		// attrezzatura
		is_cod_attrezzatura = lws_record.codice
		
	case "M"
		is_cod_attrezzatura = lws_record.cod_attrezzatura
		is_cod_tipo_manutenzione = lws_record.codice
		dw_tipi_manutenzione_det_1.change_dw_current()
		parent.postevent("pc_retrieve")
		
end choose

end event

type dw_folder_tree from u_folder within w_tab_tipi_manutenzione
integer x = 23
integer y = 20
integer width = 1143
integer height = 1920
integer taborder = 60
end type

type dw_tipi_manutenzione_det_5 from uo_cs_xx_dw within w_tab_tipi_manutenzione
integer x = 1257
integer y = 140
integer width = 2857
integer height = 808
integer taborder = 60
string dataobject = "d_tipi_manutenzione_det_5"
boolean border = false
end type

type dw_tipi_manutenzione_det_3 from uo_cs_xx_dw within w_tab_tipi_manutenzione
integer x = 1234
integer y = 140
integer width = 2880
integer height = 660
integer taborder = 41
string dataobject = "d_tipi_manutenzione_det_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_null

if i_extendmode then
	choose case i_colname
		case "flag_ricambio_codificato"
			wf_tipo_manutenzione(i_coltext)
			
		case "cod_cat_risorse_esterne"
			
			f_PO_LoadDDDW_DW(this,"cod_risorsa_esterna",sqlca,&
								  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + i_coltext + "'"+" and not (flag_blocco='S' and data_blocco <= "+s_cs_xx.db_funzioni.oggi +")")
								  
			setnull(ls_null)
			
			setitem(getrow(),"cod_risorsa_esterna",ls_null)

	end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_null

if i_extendmode and currentrow > 0 then
	
	wf_tipo_manutenzione(getitemstring(getrow(),"flag_ricambio_codificato"))

end if

end event

on pcd_validatecol;call uo_cs_xx_dw::pcd_validatecol;long ll_num_reg_lista, ll_versione, ll_edizione


choose case i_colname

case "num_reg_lista"
   ll_num_reg_lista = long(i_coltext)
   if ll_num_reg_lista > 0 then
      if f_ultima_versione_edizione(ll_num_reg_lista, ll_versione, ll_edizione) = 0 then
         setitem(getrow(), "num_versione", ll_versione)
         setitem(getrow(), "num_edizione", ll_edizione)
      end if
   end if

end choose
  
end on

type dw_folder from u_folder within w_tab_tipi_manutenzione
integer x = 1189
integer y = 20
integer width = 2971
integer height = 1820
integer taborder = 30
end type

type dw_tipi_manutenzione_det_1 from uo_cs_xx_dw within w_tab_tipi_manutenzione
event ue_ultimo ( )
integer x = 1234
integer y = 140
integer width = 2880
integer height = 1660
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_tipi_manutenzione_det_1"
boolean border = false
end type

event ue_ultimo;// QUESTA COGLIONAGGINE L'HA FATTA IL TESTA DI KAZZO DI NICOLA FERRARI E IO HO DOVUTO PERDERCI TEMPO PER 
// SISTEMARE QUESTA CAGATA DEL KAZZO IL GIORNO 26/02/2001 FINCHE' MARCO LAZZARI ERA DAL CLIENTE. KAZZO!
// TRA L'ALTRO HO NOTATO CHE NON E' STATO EFFEUTATO IL TEST POICHE' NON FUNZIONAVA KAZZO ANCHE A MARCO CHE 
// DOVEVA FARE I TEST


//string ls_stringa
//uo_dividi_stringa luo_dividi_stringa
//
//luo_dividi_stringa = create uo_dividi_stringa
//ls_stringa = i_coltext
//luo_dividi_stringa.wf_dividi_stringa(ref ls_stringa)		
//dw_tipi_manutenzione_det_1.setitem(dw_tipi_manutenzione_lista.getrow(), "des_tipo_manutenzione", ls_stringa)
//destroy luo_dividi_stringa
end event

event pcd_validatecol;call super::pcd_validatecol;long ll_num_reg_lista, ll_versione, ll_edizione


choose case i_colname

case "num_reg_lista"
   ll_num_reg_lista = long(i_coltext)
   if ll_num_reg_lista > 0 then
      if f_ultima_versione_edizione(ll_num_reg_lista, ll_versione, ll_edizione) = 0 then
         setitem(getrow(), "num_versione", ll_versione)
         setitem(getrow(), "num_edizione", ll_edizione)
      end if
   end if

end choose
  
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode and currentrow > 0 then
	wf_tipo_manutenzione(getitemstring(getrow(),"flag_ricambio_codificato"))
end if

if currentrow > 0 then
	
	f_po_loaddddw_dw(this,"cod_versione",sqlca,"distinta_padri","cod_versione","des_versione",&
									 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + &
									 getitemstring(currentrow,"cod_ricambio") + "' and flag_esplodi_in_doc = 'S'")
	
end if


//string ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_null
/*
if i_extendmode and currentrow > 0 then
	wf_tipo_manutenzione(this.getitemstring(currentrow,"flag_ricambio_codificato"))
	
	ls_cod_cat_risorse_esterne = getitemstring(currentrow, "cod_cat_risorse_esterne")
	
	if not isnull(ls_cod_cat_risorse_esterne) and len(ls_cod_cat_risorse_esterne) > 0 then
	
		f_PO_LoadDDDW_DW(dw_tipi_manutenzione_det_3,"cod_risorsa_esterna",sqlca,&
							  "anag_risorse_esterne","cod_risorsa_esterna","descrizione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_risorse_esterne = '" + ls_cod_cat_risorse_esterne + "'"+" and not (flag_blocco='S' and data_blocco <= "+s_cs_xx.db_funzioni.oggi +")")
	end if		



end if
*/
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	
	choose case i_colname
			
		case "des_tipo_manutenzione"
			
			dw_tipi_manutenzione_det_1.postevent("ue_ultimo")
			
		case "flag_ricambio_codificato"
			
			wf_tipo_manutenzione(i_coltext)
			
		case "flag_manutenzione"
			
			dw_tipi_manutenzione_det_1.accepttext()		
			
			if dw_tipi_manutenzione_det_1.Getitemstring(dw_tipi_manutenzione_det_1.getrow(), "flag_manutenzione") = 'M' then
				
				dw_folder.fu_FolderCreate(5,5)
				
				dw_folder.fu_SelectTab(1)				
				
				dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.TabSequence = 0
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio.TabSequence = 0
				
				dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.TabSequence=0
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.TabSequence=0
				
				dw_tipi_manutenzione_det_1.Object.quan_ricambio.TabSequence=0
				
				dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.TabSequence=0				
				
				dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.Background.Color = RGB(192, 192, 192)
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio.Background.Color = RGB(192, 192, 192)
				
				dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.Background.Color = RGB(192, 192, 192)
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.Background.Color = RGB(192, 192, 192)
				
				dw_tipi_manutenzione_det_1.Object.quan_ricambio.Background.Color = RGB(192, 192, 192)
				
				dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.Background.Color = RGB(192, 192, 192)				
				
				object.b_ricambio.enabled = false
				
				object.b_ricambio_alt.enabled = false				
				
			else 
				
				dw_folder.fu_FolderCreate(4,5)
				
				dw_folder.fu_SelectTab(1)								
				
				dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.TabSequence=90
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio.TabSequence=100
				
				dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.TabSequence=120
				
				dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.TabSequence=110		
				
				dw_tipi_manutenzione_det_1.Object.quan_ricambio.TabSequence=130
				
				dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.TabSequence=140
				
				if is_colore = true then
				
					dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.Background.Color = RGB(255, 255, 255)
				
					dw_tipi_manutenzione_det_1.Object.cod_ricambio.Background.Color = RGB(255, 255, 255)
					
					dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.Background.Color = RGB(255, 255, 255)
					
					dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.Background.Color = RGB(255, 255, 255)
					
					dw_tipi_manutenzione_det_1.Object.quan_ricambio.Background.Color = RGB(255, 255, 255)
					
					dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.Background.Color = RGB(255, 255, 255)					

				end if
				
				object.b_ricambio.enabled = true
				
				object.b_ricambio_alt.enabled = true
				
			end if
			
		case "cod_ricambio"
			
			string ls_null
			
			
			setnull(ls_null)
			
			setitem(row,"cod_versione",ls_null)
			
			f_po_loaddddw_dw( this, &
			                  "cod_versione", &
									sqlca, &
									"distinta_padri", &
									"cod_versione", &
									"des_versione", &
								   "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + &
								   i_coltext + "' and flag_esplodi_in_doc = 'S'")
								 
		case "cod_tipo_lista_dist"
			
			setitem( getrow(), "cod_lista_dist", ls_null)
			
			f_po_loaddddw_dw( dw_tipi_manutenzione_det_1, "cod_lista_dist", sqlca, &
									"tes_liste_dist", &
									"cod_lista_dist", &
									"des_lista_dist", &
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")
										 
			
	end choose
	
end if
end event

event clicked;call super::clicked;accepttext()

//if ib_stato_nuovo or ib_stato_modifica then
//end if

if Getitemstring(getrow(), "flag_manutenzione") = 'T' then
	
	dw_folder.fu_enabletab(5)
	
	Object.flag_ricambio_codificato.TabSequence = 0
	Object.cod_ricambio.TabSequence = 0
	Object.des_ricambio_non_codificato.TabSequence=0
	Object.cod_ricambio_alternativo.TabSequence=0
	Object.quan_ricambio.TabSequence=0
	Object.costo_unitario_ricambio.TabSequence=0

	Object.flag_ricambio_codificato.Background.Color = RGB(192, 192, 192)
	Object.cod_ricambio.Background.Color = RGB(192, 192, 192)
	Object.des_ricambio_non_codificato.Background.Color = RGB(192, 192, 192)
	Object.cod_ricambio_alternativo.Background.Color = RGB(192, 192, 192)
	Object.quan_ricambio.Background.Color = RGB(192, 192, 192)
	Object.costo_unitario_ricambio.Background.Color = RGB(192, 192, 192)

else 
	
	dw_folder.fu_disabletab(5)
	
	Object.flag_ricambio_codificato.TabSequence=90
	Object.cod_ricambio.TabSequence=100
	Object.des_ricambio_non_codificato.TabSequence=120
	Object.cod_ricambio_alternativo.TabSequence=110		
	Object.quan_ricambio.TabSequence=130
	Object.costo_unitario_ricambio.TabSequence=140
	
	if is_colore = true then
		Object.flag_ricambio_codificato.Background.Color = RGB(255, 255, 255)
		Object.cod_ricambio.Background.Color = RGB(255, 255, 255)
		Object.des_ricambio_non_codificato.Background.Color = RGB(255, 255, 255)
		Object.cod_ricambio_alternativo.Background.Color = RGB(255, 255, 255)
		Object.quan_ricambio.Background.Color = RGB(255, 255, 255)
		Object.costo_unitario_ricambio.Background.Color = RGB(255, 255, 255)	
	end if	
end if

end event

event doubleclicked;call super::doubleclicked;if row < 1 then return

if s_cs_xx.admin then

	str_des_multilingua lstr_des_multilingua
	
	lstr_des_multilingua.nome_tabella = "tab_tipi_manutenzione"
	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_tipo_manutenzione")
	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_attrezzatura")
	lstr_des_multilingua.chiave_str_2 = getitemstring(row,"cod_tipo_manutenzione")
	
	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
	
	openwithparm(w_des_multilingua, lstr_des_multilingua)

end if
end event

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------
//--------------------------------- Modifica Nicola --------------------------------------------
long ll_count, ld_num_protocollo
string ls_cod_attrezzatura, ls_cod_tipo_manutenzione

ls_cod_attrezzatura = getitemstring(getrow(), "cod_attrezzatura")
ls_cod_tipo_manutenzione = getitemstring(getrow(), "cod_tipo_manutenzione")

delete from elenco_risorse 
      where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_attrezzatura = :ls_cod_attrezzatura
		  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
		  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Omnia", "Errore in fase di cancellazione dati dalla tabella Elenco_risorse " + sqlca.sqlerrtext)
end if

declare cu_det_tipi_manutenzioni cursor for
	select num_protocollo
	  from det_tipi_manutenzioni
	 where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_attrezzatura = :ls_cod_attrezzatura
		  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;

open cu_det_tipi_manutenzioni;

do while 1 = 1
	fetch cu_det_tipi_manutenzioni into :ld_num_protocollo;
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Omnia", "Errore in fase di lettura dati dalla tabella Det_tipi_manutenzioni " + sqlca.sqlerrtext)
		return
	end if	
	
	if sqlca.sqlcode = 0 then
		delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if		  

		delete from det_tipi_manutenzioni         //cancellazione dettaglio tipi manutenzioni
				where cod_azienda = :s_cs_xx.cod_azienda
				  and cod_attrezzatura = :ls_cod_attrezzatura
				  and cod_tipo_manutenzione = :ls_cod_tipo_manutenzione;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Omnia", "Errore in fase di cancellazione dati dalla tabella Det_tipi_manutenzioni " + sqlca.sqlerrtext)
		end if				
		
		delete from tab_protocolli  //cancellazione protocollo
		where cod_azienda = :s_cs_xx.cod_azienda
		  and num_protocollo = :ld_num_protocollo;
		  
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Errore cancellazione procollo:" + string(ld_num_protocollo), "Errore: " + sqlca.sqlerrtext)
			return
		end if
	end if
loop	
close cu_det_tipi_manutenzioni;


//---------------------------------- Fine Modifica ---------------------------------------------			
		
		
IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse


// stefano
triggerevent("pcd_save")
end event

event pcd_modify;call super::pcd_modify;cb_modifica.enabled = false
cb_trasferisci.enabled = false

cb_ricambi.enabled = false

dw_tipi_manutenzione_det_2.object.cb_note_1.enabled = false
dw_tipi_manutenzione_det_1.object.b_ricambio.enabled=true
dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled=true
dw_tipi_manutenzione_det_1.object.b_ricerca_att.enabled=true
is_colore = true

dw_tipi_manutenzione_det_1.accepttext()
if dw_tipi_manutenzione_det_1.Getitemstring(dw_tipi_manutenzione_det_1.getrow(), "flag_manutenzione") = 'T' then
	
	dw_folder.fu_FolderCreate(5,5)
	dw_folder.fu_SelectTab(1)				
	
	dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.TabSequence = 0
	dw_tipi_manutenzione_det_1.Object.cod_ricambio.TabSequence = 0
	dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.TabSequence=0
	dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.TabSequence=0
	dw_tipi_manutenzione_det_1.Object.quan_ricambio.TabSequence=0
	dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.TabSequence=0
	
	dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.Background.Color = RGB(192, 192, 192)
	dw_tipi_manutenzione_det_1.Object.cod_ricambio.Background.Color = RGB(192, 192, 192)
	dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.Background.Color = RGB(192, 192, 192)
	dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.Background.Color = RGB(192, 192, 192)
	dw_tipi_manutenzione_det_1.Object.quan_ricambio.Background.Color = RGB(192, 192, 192)
	dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.Background.Color = RGB(192, 192, 192)	
	
	dw_tipi_manutenzione_det_1.object.b_ricambio.enabled = false
	dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled = false
else 
	
	dw_folder.fu_FolderCreate(4,5)
	dw_folder.fu_SelectTab(1)					
	
	dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.TabSequence=90
	dw_tipi_manutenzione_det_1.Object.cod_ricambio.TabSequence=100
	dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.TabSequence=120
	dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.TabSequence=110		
	dw_tipi_manutenzione_det_1.Object.quan_ricambio.TabSequence=130
	dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.TabSequence=140
	
	dw_tipi_manutenzione_det_1.Object.flag_ricambio_codificato.Background.Color = RGB(255, 255, 255)
	dw_tipi_manutenzione_det_1.Object.cod_ricambio.Background.Color = RGB(255, 255, 255)
	dw_tipi_manutenzione_det_1.Object.des_ricambio_non_codificato.Background.Color = RGB(255, 255, 255)
	dw_tipi_manutenzione_det_1.Object.cod_ricambio_alternativo.Background.Color = RGB(255, 255, 255)
	dw_tipi_manutenzione_det_1.Object.quan_ricambio.Background.Color = RGB(255, 255, 255)
	dw_tipi_manutenzione_det_1.Object.costo_unitario_ricambio.Background.Color = RGB(255, 255, 255)		
	
	dw_tipi_manutenzione_det_1.object.b_ricambio.enabled = true
	dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled = true
end if		
end event

event pcd_new;call super::pcd_new;cb_modifica.enabled = false
cb_trasferisci.enabled = false
dw_tipi_manutenzione_det_2.object.cb_note_1.enabled = false
cb_ricambi.enabled = false

dw_tipi_manutenzione_det_1.object.b_ricambio.enabled=true
dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled=true
dw_tipi_manutenzione_det_1.object.b_ricerca_att.enabled=true
is_colore = true

// dati automatici
if not isnull(is_cod_attrezzatura) and is_cod_attrezzatura <> "" then setitem(getrow(), "cod_attrezzatura", is_cod_attrezzatura)


setitem(getrow(),"flag_blocco","N")

end event

event pcd_save;call super::pcd_save;cb_ricambi.enabled = true
dw_tipi_manutenzione_det_1.object.b_ricambio.enabled=false
dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled=false
dw_tipi_manutenzione_det_1.object.b_ricerca_att.enabled=false
is_colore = false
dw_tipi_manutenzione_det_2.object.cb_note_1.enabled = true




end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;cb_modifica.enabled = true
cb_trasferisci.enabled = true

cb_ricambi.enabled = true

dw_tipi_manutenzione_det_1.object.b_ricambio.enabled=false
dw_tipi_manutenzione_det_1.object.b_ricambio_alt.enabled=false
dw_tipi_manutenzione_det_1.object.b_ricerca_att.enabled=false
dw_tipi_manutenzione_det_2.object.cb_note_1.enabled = true
is_colore = false

end event

event updateend;call super::updateend;cb_modifica.enabled = true
cb_trasferisci.enabled = true

long   ll_i

string ls_messaggio

uo_ricambi luo_ricambi


for ll_i = 1 to rowcount()
	
	if getitemstatus(ll_i,"cod_ricambio",primary!) = datamodified! or &
		getitemstatus(ll_i,"cod_versione",primary!) = datamodified! then
		
		luo_ricambi = create uo_ricambi
		
		if luo_ricambi.uof_ricambi_tipologia(getitemstring(ll_i,"cod_attrezzatura"),&
														 getitemstring(ll_i,"cod_tipo_manutenzione"),&
														 ls_messaggio) <> 0 then
			g_mb.messagebox("OMNIA",ls_messaggio)
			destroy luo_ricambi
			return -1
		end if
		
		destroy luo_ricambi
		
	end if
	
next

// stefano
if rowsinserted > 0 then
	wf_inserisci_singola_manutenzione(getrow())
end if

if rowsdeleted > 0 then
	wf_cancella_manutenzione()
end if

//solo per sintexcal -------------------------------------------
string ls_prova

select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
			 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	//sintexcal
	if rowsupdated > 0 and getrow()>0 then
		if wf_aggiorna_programmi(getrow()) = -1 then return 1
	end if
end if
//-----------------------------------------------------------------
end event

event updatestart;call super::updatestart;long ll_i
string ls_des

for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(ll_i, "des_tipo_manutenzione_storico")) then
		
		if getitemstatus(ll_i,"des_tipo_manutenzione",primary!) = DataModified! then
			
			ls_des = getitemstring(ll_i, "des_tipo_manutenzione", primary!, true)
			setitem(ll_i, "des_tipo_manutenzione_storico", ls_des)

		end if
		
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_lista_dist
long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_attrezzatura, is_cod_tipo_manutenzione)

if ll_errore < 0 then
   pcca.error = c_fatal
elseif ll_errore > 0 then
	
	// carico cod_tipo_lista_dist
	ls_cod_tipo_lista_dist = getitemstring(getrow(), "cod_tipo_lista_dist")
	
	if not isnull(ls_cod_tipo_lista_dist) and ls_cod_tipo_lista_dist <> "" then
		
		f_po_loaddddw_dw( dw_tipi_manutenzione_det_1, "cod_lista_dist", sqlca, &
							"tes_liste_dist", &
							"cod_lista_dist", &
							"des_lista_dist", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + ls_cod_tipo_lista_dist + "'")
							
	end if
	
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_attrezzatura, ls_cod_tipo_manutenzione

ls_cod_attrezzatura  = trim(getitemstring(getrow(), "cod_attrezzatura"))
ls_cod_tipo_manutenzione  = trim(getitemstring(getrow(), "cod_tipo_manutenzione"))

if isnull(ls_cod_attrezzatura) or ls_cod_attrezzatura = "" then
	g_mb.messagebox("OMNIA", "Codice attrezzatura è un campo obbligatorio")
	pcca.error = c_fatal
end if

if isnull(ls_cod_tipo_manutenzione) or ls_cod_tipo_manutenzione = "" then
	g_mb.messagebox("OMNIA", "Codice tipo manutenzione è un campo obbligatorio")
	pcca.error = c_fatal
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name 		
	case "b_ricerca_att"
		dw_tipi_manutenzione_det_1.change_dw_current()
		guo_ricerca.uof_ricerca_attrezzatura(dw_tipi_manutenzione_det_1,"cod_attrezzatura")
	case "b_ricambio"
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_prodotto(dw_tipi_manutenzione_det_1,"cod_ricambio")		
	case "b_ricambio_alt"
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_prodotto(dw_tipi_manutenzione_det_1,"cod_ricambio_alternativo")
		
end choose
end event

