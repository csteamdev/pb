﻿$PBExportHeader$w_tab_tipi_manutenzione_doc.srw
forward
global type w_tab_tipi_manutenzione_doc from w_ole_v2
end type
end forward

global type w_tab_tipi_manutenzione_doc from w_ole_v2
string title = "Documenti"
end type
global w_tab_tipi_manutenzione_doc w_tab_tipi_manutenzione_doc

type variables
string is_cod_attrezzatura
string is_cod_tipo_manutenzione
end variables

forward prototypes
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public subroutine wf_load_documents ()
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
private function integer wf_get_num_protocollo ()
public function integer wf_delete_protocollo (long al_num_protocollo)
end prototypes

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);long ll_num_protocollo

select num_protocollo
into :ll_num_protocollo
from det_tipi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione and
	prog_tipo_manutenzione = :astr_data.progressivo;
	
delete from det_tipi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione and
	prog_tipo_manutenzione = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
end if


if not isnull(ll_num_protocollo) and ll_num_protocollo > 0 then 
	if wf_delete_protocollo(ll_num_protocollo) < 0 then return false
end if

return true


end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);selectblob blob
into :ab_file_blob
from det_tipi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione and
	prog_tipo_manutenzione = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante il prelievo del file." + sqlca.sqlerrtext)
	return false
end if
end function

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	prog_tipo_manutenzione, &
	descrizione, &
	prog_mimetype &
FROM det_tipi_manutenzioni &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	cod_attrezzatura ='" + string(is_cod_attrezzatura) + "' and &
	cod_tipo_manutenzione ='" + string(is_cod_tipo_manutenzione) + "' &
ORDER BY &
	cod_azienda ASC, &
	cod_attrezzatura ASC, &
	cod_tipo_manutenzione ASC, &
	prog_tipo_manutenzione ASC "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	//lstr_data.anno_registrazione = il_anno_registrazione
	//lstr_data.num_registrazione = il_num_registrazione
	lstr_data.progressivo = lds_documenti.getitemnumber(li_i, "prog_tipo_manutenzione")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "descrizione"), lstr_data)
next


end subroutine

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);update det_tipi_manutenzioni
set descrizione = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione and
	prog_tipo_manutenzione = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	return false
else
	return true
end if
end function

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);long ll_progressivo, ll_num_protocollo
str_ole lstr_data

select max(prog_tipo_manutenzione)
into :ll_progressivo
from det_tipi_manutenzioni
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("OMNIA", "Errore durante il calcolo del progressivo del nuovo documento. " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo = 0 then
	ll_progressivo = 0
end if

ll_progressivo++

ll_num_protocollo = wf_get_num_protocollo()
if ll_num_protocollo < 1 then return false

insert into det_tipi_manutenzioni (
	cod_azienda,
	cod_attrezzatura,
	cod_tipo_manutenzione,
	prog_tipo_manutenzione,
	descrizione,
	prog_mimetype,
	num_protocollo)
values (
	:s_cs_xx.cod_azienda, 
	:is_cod_attrezzatura,
	:is_cod_tipo_manutenzione,
	:ll_progressivo,
	:as_file_name,
	:al_prog_mimetype,
	:ll_num_protocollo);
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Omnia", "Errore durante la creazione del record per il nuovo documento." + sqlca.sqlerrtext)
	return false
end if

updateblob det_tipi_manutenzioni
set blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_attrezzatura = :is_cod_attrezzatura and
	cod_tipo_manutenzione = :is_cod_tipo_manutenzione and 
	prog_tipo_manutenzione = :ll_progressivo;
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Omnia", "Errore durante l'inserimento del documento." + sqlca.sqlerrtext)
	return false
end if

//lstr_data.anno_registrazione = il_anno_registrazione
//lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo = ll_progressivo
lstr_data.prog_mimetype = al_prog_mimetype

wf_add_document(as_file_name, lstr_data)

return true
			  
end function

private function integer wf_get_num_protocollo ();long ld_num_protocollo

// Assegnare un nuovo numero di protocollo
setnull(ld_num_protocollo)

select max(num_protocollo)
into :ld_num_protocollo
from tab_protocolli
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore", "Impossibile Leggere la tabella protocolli " + sqlca.sqlerrtext )
	return -1
end if

if (sqlca.sqlcode <> 0 ) or isnull(ld_num_protocollo) then ld_num_protocollo = 0

ld_num_protocollo = ld_num_protocollo + 1	

insert into tab_protocolli (cod_azienda, num_protocollo)
values (:s_cs_xx.cod_azienda, :ld_num_protocollo);

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Errore", "Impossibile Aggiornare la tabella protocolli" + sqlca.sqlerrtext)
	return -1
end if

return ld_num_protocollo

end function

public function integer wf_delete_protocollo (long al_num_protocollo);delete from tab_chiavi_protocollo  //cancellazione tabella chiavi protocollo
where cod_azienda = :s_cs_xx.cod_azienda
  and num_protocollo = :al_num_protocollo;
  
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore cancellazione chiavi procollo:" + string(al_num_protocollo), "Errore: " + sqlca.sqlerrtext)
	return -1
end if		  

delete from tab_protocolli  //cancellazione tabella protocollo
where cod_azienda = :s_cs_xx.cod_azienda
  and num_protocollo = :al_num_protocollo;
  
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore cancellazione procollo:" + string(al_num_protocollo), "Errore: " + sqlca.sqlerrtext)
	return -1
end if

return 1
end function

on w_tab_tipi_manutenzione_doc.create
call super::create
end on

on w_tab_tipi_manutenzione_doc.destroy
call super::destroy
end on

event open;call super::open;is_cod_attrezzatura =  s_cs_xx.parametri.parametro_s_8
is_cod_tipo_manutenzione =  s_cs_xx.parametri.parametro_s_9
end event

type st_loading from w_ole_v2`st_loading within w_tab_tipi_manutenzione_doc
end type

type cb_cancella from w_ole_v2`cb_cancella within w_tab_tipi_manutenzione_doc
end type

type lv_documenti from w_ole_v2`lv_documenti within w_tab_tipi_manutenzione_doc
end type

type ddlb_style from w_ole_v2`ddlb_style within w_tab_tipi_manutenzione_doc
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_tab_tipi_manutenzione_doc
end type

