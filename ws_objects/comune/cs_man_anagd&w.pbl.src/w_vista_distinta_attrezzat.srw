﻿$PBExportHeader$w_vista_distinta_attrezzat.srw
$PBExportComments$Finestra Elenco Componenti Attrezzatura
forward
global type w_vista_distinta_attrezzat from w_cs_xx_risposta
end type
type dw_vista_distinta_attrezzat from uo_cs_xx_dw within w_vista_distinta_attrezzat
end type
type pb_1 from picturebutton within w_vista_distinta_attrezzat
end type
type em_1 from editmask within w_vista_distinta_attrezzat
end type
type st_1 from statictext within w_vista_distinta_attrezzat
end type
type em_2 from editmask within w_vista_distinta_attrezzat
end type
end forward

global type w_vista_distinta_attrezzat from w_cs_xx_risposta
int Width=2963
int Height=753
boolean TitleBar=true
string Title="Elenco Componenti Attrezzature"
dw_vista_distinta_attrezzat dw_vista_distinta_attrezzat
pb_1 pb_1
em_1 em_1
st_1 st_1
em_2 em_2
end type
global w_vista_distinta_attrezzat w_vista_distinta_attrezzat

type variables
string is_prodotto_padre[]
long il_livello
end variables

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_vista_distinta_attrezzat.set_dw_options(sqlca,i_openparm,c_nonew+c_nomodify+c_nodelete,c_default)

save_on_close(c_socnosave)

il_livello = 1
is_prodotto_padre[il_livello] = s_cs_xx.parametri.parametro_s_1
end on

on pc_setddlb;call w_cs_xx_risposta::pc_setddlb;f_PO_LoadDDDW_DW(dw_vista_distinta_attrezzat,"cod_prodotto_figlio",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_vista_distinta_attrezzat,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on w_vista_distinta_attrezzat.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_vista_distinta_attrezzat=create dw_vista_distinta_attrezzat
this.pb_1=create pb_1
this.em_1=create em_1
this.st_1=create st_1
this.em_2=create em_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_vista_distinta_attrezzat
this.Control[iCurrent+2]=pb_1
this.Control[iCurrent+3]=em_1
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=em_2
end on

on w_vista_distinta_attrezzat.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_vista_distinta_attrezzat)
destroy(this.pb_1)
destroy(this.em_1)
destroy(this.st_1)
destroy(this.em_2)
end on

type dw_vista_distinta_attrezzat from uo_cs_xx_dw within w_vista_distinta_attrezzat
int X=23
int Y=121
int Width=2881
int Height=501
int TabOrder=20
string DataObject="d_vista_distinta_attrezzat"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on doubleclicked;long ll_num_records
string ls_str

//if this.getclickedcolumn() = 3 then

     ls_str = this.getitemstring(this.getrow(),"cod_prodotto_figlio")
     SELECT count(num_sequenza)  
     INTO   :ll_num_records  
     FROM   distinta  
     WHERE  ( distinta.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( distinta.cod_prodotto_padre = :ls_str )   ;
     if sqlca.sqlcode <> 0 then
        g_mb.messagebox("Elenco Componenti","Non ci sono componenti per questo prodotto")
     else
        if ll_num_records > 0 then
           il_livello = il_livello + 1
           is_prodotto_padre[il_livello] = this.getitemstring(this.getrow(),"cod_prodotto_figlio")
           parent.triggerevent("pc_retrieve")
        else
           g_mb.messagebox("Elenco Componenti","Non ci sono componenti per questo prodotto")
        end if   
     end if
//end if
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error
string ls_str

l_Error = Retrieve(s_cs_xx.cod_azienda, is_prodotto_padre[il_livello])

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

SELECT anag_prodotti.des_prodotto  
INTO   :ls_str  
FROM   anag_prodotti  
WHERE  (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (anag_prodotti.cod_prodotto = :is_prodotto_padre[il_livello] )   ;

em_1.text = is_prodotto_padre[il_livello]
em_2.text = ls_str
end on

type pb_1 from picturebutton within w_vista_distinta_attrezzat
int X=23
int Y=21
int Width=101
int Height=85
int TabOrder=10
string PictureName="\cs_xx\risorse\ritorna.bmp"
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if il_livello > 1 then
   il_livello = il_livello - 1
   parent.triggerevent("pc_retrieve")
end if
end on

type em_1 from editmask within w_vista_distinta_attrezzat
int X=709
int Y=21
int Width=595
int Height=81
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleRaised!
string Mask="XXXXXXXXXXXXXXX"
MaskDataType MaskDataType=StringMask!
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_vista_distinta_attrezzat
int X=252
int Y=21
int Width=435
int Height=81
boolean Enabled=false
string Text="Prodotto Padre:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_2 from editmask within w_vista_distinta_attrezzat
int X=1326
int Y=21
int Width=1578
int Height=81
string Mask="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
MaskDataType MaskDataType=StringMask!
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

