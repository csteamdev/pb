﻿$PBExportHeader$uo_calcola_sconto.sru
forward
global type uo_calcola_sconto from nonvisualobject
end type
end forward

global type uo_calcola_sconto from nonvisualobject
end type
global uo_calcola_sconto uo_calcola_sconto

forward prototypes
public function double wf_calcola_sconto (double fs_sconto_1, double fs_sconto_2, double fs_sconto_3, double fs_sconto_4, double fs_sconto_5, double fs_sconto_6, double fs_sconto_7, double fs_sconto_8, double fs_sconto_9, double fs_sconto_10)
end prototypes

public function double wf_calcola_sconto (double fs_sconto_1, double fs_sconto_2, double fs_sconto_3, double fs_sconto_4, double fs_sconto_5, double fs_sconto_6, double fs_sconto_7, double fs_sconto_8, double fs_sconto_9, double fs_sconto_10);double ld_prezzo_scontato, ld_sconto

if not isnull(fs_sconto_1) then
	ld_prezzo_scontato = 100 - (100 *  fs_sconto_1 / 100)
end if

if not isnull(fs_sconto_2) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_2 / 100)
end if	

if not isnull(fs_sconto_3) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_3 / 100)
end if	

if not isnull(fs_sconto_4) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_4 / 100)
end if	

if not isnull(fs_sconto_5) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_5 / 100)
end if	

if not isnull(fs_sconto_6) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_6 / 100)
end if	

if not isnull(fs_sconto_7) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_7 / 100)
end if	

if not isnull(fs_sconto_8) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_8 / 100)
end if	

if not isnull(fs_sconto_9) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_9 / 100)
end if	

if not isnull(fs_sconto_10) then
	ld_prezzo_scontato = ld_prezzo_scontato - (ld_prezzo_scontato * fs_sconto_10 / 100)
end if	

ld_sconto = 100 - ld_prezzo_scontato

return ld_sconto

end function

on uo_calcola_sconto.create
TriggerEvent( this, "constructor" )
end on

on uo_calcola_sconto.destroy
TriggerEvent( this, "destructor" )
end on

