﻿$PBExportHeader$uo_calcolo_lifo.sru
$PBExportComments$UO per calcolo LIFO
forward
global type uo_calcolo_lifo from nonvisualobject
end type
end forward

global type uo_calcolo_lifo from nonvisualobject
end type
global uo_calcolo_lifo uo_calcolo_lifo

forward prototypes
public function integer wf_calcolo_lifo (string fs_cod_prodotto, date fdt_data_calcolo, ref double fd_cmp, ref double fd_quantita, ref double fd_valore_lifo, ref double fd_strato[], ref long fd_anno[])
end prototypes

public function integer wf_calcolo_lifo (string fs_cod_prodotto, date fdt_data_calcolo, ref double fd_cmp, ref double fd_quantita, ref double fd_valore_lifo, ref double fd_strato[], ref long fd_anno[]);string ls_error, ls_chiave[], ls_cod_prodotto_lifo
long ll_i, ll_anno_lifo, ll_vuoto[]
dec{4} ld_quant_val[], ld_giacenza[], ld_qta_strato_lifo, &
		 ld_qta_fine_lifo, ld_anno_lifo, ld_qta_strato_calcolo, ld_cmp_calcolo, &
		 ld_qta_rimasta, ld_somma_strato, ld_differenza_qta, ld_vuoto[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
uo_magazzino luo_magazzino

//-------- Michele 10/06/2004 pulizia eventuali valori precedenti ----------
fd_anno[] = ll_vuoto[]

fd_strato[] = ld_vuoto[]
//----------------------------------------------------------------------------------------

ll_i = 2
fd_valore_lifo = 0
ld_somma_strato = 0

SELECT saldo_quan_inizio_anno,   
		 val_inizio_anno  
 INTO :ld_quant_val[1],   
		:ld_quant_val[2]  
 FROM anag_prodotti  
WHERE cod_prodotto = :s_cs_xx.cod_azienda AND  
		cod_prodotto = :fs_cod_prodotto ;

ld_quant_val[2] = ld_quant_val[1] * ld_quant_val[2]

luo_magazzino = CREATE uo_magazzino

luo_magazzino.uof_saldo_prod_date_decimal(fs_cod_prodotto, datetime(fdt_data_calcolo), "",ld_quant_val[], ls_error, "N", ls_chiave[], ld_giacenza[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])

destroy luo_magazzino

fd_quantita = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
if ld_quant_val[12] <> 0 then
	fd_cmp = ld_quant_val[13] / ld_quant_val[12]
else
	
	select costo_medio_ponderato
	into   :fd_cmp
	from   lifo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :fs_cod_prodotto and
			 anno_lifo = (select max(anno_lifo)
							  from   lifo
							  where  cod_azienda = :s_cs_xx.cod_azienda and
										cod_prodotto = :fs_cod_prodotto);
	
	if sqlca.sqlcode <> 0 then
		fd_cmp = 0
	end if
	
end if

select max(anno_lifo)
  into :ld_anno_lifo
  from lifo
 where cod_azienda = :s_cs_xx.cod_azienda
	and cod_prodotto = :fs_cod_prodotto;
	
if sqlca.sqlcode < 0 then return -1

if sqlca.sqlcode = 0 then		
	select cod_prodotto, 
			 rimanenza_strato,
			 giacenza_finale
	  into :ls_cod_prodotto_lifo,	
			 :ld_qta_strato_lifo,
			 :ld_qta_fine_lifo
	  from lifo
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_prodotto = :fs_cod_prodotto
		and anno_lifo = :ld_anno_lifo;
		
		if sqlca.sqlcode < 0 then return -1
end if

//if isnull(ld_qta_fine_lifo) then ld_qta_fine_lifo = 0
//if (fd_quantita - ld_qta_fine_lifo) < 0 and sqlca.sqlcode <> 100 then
//	messagebox("Calcolo", "Attenzione il prodotto " + fs_cod_prodotto + " non ha sufficienti scorte in magazzino.")
//	fd_quantita = ld_qta_fine_lifo
//end if	

if sqlca.sqlcode = 100 then
	fd_valore_lifo = fd_quantita * fd_cmp
	fd_strato[1] = fd_quantita
elseif sqlca.sqlcode = 0 then
	
	declare cu_tab_lifo cursor for  
	 select anno_lifo,
	 		  rimanenza_strato, 
			  costo_medio_ponderato
		from lifo
	  where cod_azienda = :s_cs_xx.cod_azienda
		 and cod_prodotto = :fs_cod_prodotto
  order by anno_lifo DESC ;			
	
	if fd_quantita = ld_qta_fine_lifo then		// se qta = qta_fine
		open cu_tab_lifo;
		
		do while 1 = 1
			fetch cu_tab_lifo into :ll_anno_lifo, :ld_qta_strato_calcolo, :ld_cmp_calcolo;
			if sqlca.sqlcode < 0 then return -1
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then
				if ld_qta_strato_calcolo <> 0 and ld_cmp_calcolo <> 0 then
					fd_valore_lifo = fd_valore_lifo + (ld_qta_strato_calcolo * ld_cmp_calcolo)
				end if
			end if
		loop
		fd_strato[1] = 0
		close cu_tab_lifo;
	elseif fd_quantita > ld_qta_fine_lifo then // se qta > qta_fine	
		fd_strato[1] = fd_quantita - ld_qta_fine_lifo
		fd_valore_lifo = fd_valore_lifo + (fd_cmp * fd_strato[1])
		open cu_tab_lifo;
		
		do while 1 = 1
			fetch cu_tab_lifo into :ll_anno_lifo, :ld_qta_strato_calcolo, :ld_cmp_calcolo;
			if sqlca.sqlcode < 0 then return -1
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then
				if ld_qta_strato_calcolo <> 0 and ld_cmp_calcolo <> 0 then
					fd_valore_lifo = fd_valore_lifo + (ld_qta_strato_calcolo * ld_cmp_calcolo)							
				end if
			end if
		loop
		close cu_tab_lifo;
	elseif fd_quantita < ld_qta_fine_lifo then // se qta < qta_fine	
		ld_qta_rimasta = ld_qta_fine_lifo - fd_quantita
		fd_strato[1] = 0
		open cu_tab_lifo;
		
		do while 1 = 1
			fetch cu_tab_lifo into :ll_anno_lifo, :ld_qta_strato_calcolo, :ld_cmp_calcolo;
			if sqlca.sqlcode < 0 then return -1
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = 0 then
				if ld_qta_strato_calcolo <> 0 and ld_cmp_calcolo <> 0 then				
					ld_differenza_qta = (ld_qta_fine_lifo - fd_quantita)
					if (ld_qta_rimasta - ld_qta_strato_calcolo) >= 0 then
//						fd_valore_lifo = fd_valore_lifo + (ld_qta_strato_calcolo * ld_cmp_calcolo)
						ld_qta_rimasta = ld_qta_rimasta - ld_qta_strato_calcolo
						ld_somma_strato = ld_somma_strato + ld_qta_strato_calcolo
						fd_anno[ll_i] = ll_anno_lifo
						fd_strato[ll_i] = 0
						ll_i ++
					elseif ((ld_qta_rimasta - ld_qta_strato_calcolo) < 0) and &
						(ld_qta_strato_calcolo <> ((fd_quantita - ld_qta_strato_calcolo) * (-1))) then
//						fd_valore_lifo = fd_valore_lifo + (ld_cmp_calcolo * (ld_qta_strato_calcolo - ld_somma_strato))
						fd_valore_lifo = fd_valore_lifo + (ld_cmp_calcolo * (ld_qta_strato_calcolo - ld_qta_rimasta))
						fd_strato[ll_i] = ld_qta_strato_calcolo - ld_qta_rimasta
						fd_anno[ll_i] = ll_anno_lifo
						ll_i ++
						ld_qta_rimasta = 0
					end if	
				end if	
			end if
		loop
		close cu_tab_lifo;
	end if
end if	

return 1
end function

on uo_calcolo_lifo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_calcolo_lifo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

