﻿$PBExportHeader$uo_magazzino.sru
forward
global type uo_magazzino from nonvisualobject
end type
end forward

global type uo_magazzino from nonvisualobject
end type
global uo_magazzino uo_magazzino

type variables
boolean ib_negativo = false, ib_chiusura = false, ib_flag_agg_costo_ultimo = false, ib_salta_controllo = false, ib_avvisa = false, ib_mov_raggruppato=true, ib_silentmode=false

// stefanop 28/03/2012
/**
 * Parametro per indicare quali depositi saranno presi in considerazione dell'analisi del magazzino
 * per l'inventario.
 * 
 * Un deposito è di conto deposito fornitore quando ha il flag_tipo_deposito = "C"
 * 
 * Valori: 
 * S = vengono considerati solo i movimenti eseguiti da depositi di fornitori
 * N = vengono considerati tutti movimenti dei depositi NON fornitori
 * I = vengono considerati tutti i movimenti dei depositi passati nella IN
 * T = vengono considerati tutti i movimenti di tutti i depositi
 **/

public string is_considera_depositi_fornitori = "N"
public string is_cod_depositi_in
public string is_silentmode_error=""

public boolean ib_log = false

private:
	uo_log iuo_log
	string is_flag_commessa = "N"
end variables

forward prototypes
public function integer uof_tipo_movimento (string fs_cod_tipo_mov_det, ref long fl_tipo_operazione, ref boolean fb_crea_nuovo_stock, ref boolean fb_aggiorna_costo_medio, ref boolean fb_aggiorna_costo_ultimo)
public function integer uof_aggiorna_stock (string fs_cod_prodotto, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, ref string fs_cod_fornitore, ref string fs_cod_cliente, decimal fd_quantita, decimal fd_valore_unitario, boolean fb_primo_movimento, readonly boolean fb_storno, readonly boolean fb_update)
public function integer uof_new_con_magazzino (datetime fdd_data_registrazione, ref long fl_prog_mov)
public function integer uof_aggiorna_prodotti (string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, string fs_cod_fornitore, boolean fb_storno)
public function integer uof_single_mov_mag_decimal (string fs_cod_tipo_movimento_det, decimal fd_quan_movimento, decimal fd_val_movimento, ref decimal fd_quant_val[], ref decimal fd_quant_val_stock[], ref string fs_error, string fs_cod_tipo_movimento, ref decimal fd_quan_mov)
public function integer uof_saldo_prod_date_decimal (string fs_cod_prodotto, datetime fdt_data_rif, string fs_where, ref decimal fd_quant_val[], ref string fs_error, string fs_flag_stock, ref string fs_chiave[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_stock[], ref decimal fd_quan_costo_medio_stock[])
public function integer uof_saldo_da_chiusura_a_data (string fs_cod_prodotto, datetime fdt_data_fine, ref decimal fd_saldo_al, ref string fs_errore)
public function integer uof_disimpegna_mp_commessa (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer uof_impegna_mp_commessa (boolean fb_verifica_fasi_avanzamento, boolean fb_pos, boolean fb_impegna_prodotto_finito, string fs_cod_prodotto_finito, string fs_cod_versione, decimal fd_quan_prodotto_finito, long fl_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer uof_impegna_mp_commessa (boolean fb_pos, boolean fb_impegna_prodotto_finito, string fs_cod_prodotto_finito, string fs_cod_versione, decimal fd_quan_prodotto_finito, long fl_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer uof_saldo_prod_date_decimal_comm (string fs_cod_prodotto, datetime fdt_data_rif, string fs_where, ref decimal fd_quant_val[], ref string fs_error, string fs_flag_stock, ref string fs_chiave[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_stock[], ref decimal fd_quan_costo_medio_stock[])
private function integer uof_estrai_prodotti_collegati (string fs_selezione, ref s_prodotti_collegati fstr_prodotti[], ref s_prodotti_collegati fstr_prodotti_raggruppati[], ref string fs_errore)
public function integer uof_ricalcola_impegnato (string fs_selezione, ref string fs_errore)
public function integer uof_ricalcola_ordinato_fornitore (string fs_selezione, ref string fs_errore)
public function integer uof_registra_movimento_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, ref string fs_cod_fornitore, ref string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine)
public function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[])
public function integer uof_movimenti_mag_ragguppato (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine)
public function boolean uof_stampa_report_scarico_mag_for (long al_anno_bolla_acq, long al_num_bolla_acq, ref string as_error)
public function integer uof_verifica_dest_mov_magazzino (string as_table, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_cod_tipo_movimento, string as_cod_prodotto, string as_cod_deposito[], string as_cod_ubicazione[], string as_cod_lotto[], datetime adt_data_stock[], long al_prog_stock[], string as_cod_cliente[], string as_cod_fornitore[], ref long al_anno_reg_des_mov, ref long al_num_reg_des_mov, ref string as_error)
public function integer uof_aggiorna_mov_mag_raggruppato (long al_anno_mov_mag_orig, long al_num_mov_mag_orig, string as_cod_tipo_movimento, string as_cod_tipo_mov_det, string as_cod_prodotto, decimal ad_quantita, decimal ad_valore_unitario, string as_cod_fornitore, boolean ab_storno, ref string as_error)
private function integer uof_crea_nuovo_stock (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, readonly string fs_cod_lotto, datetime fdd_data_stock, long fl_prog_stock, string fs_cod_fornitore, string fs_cod_cliente)
public function boolean uof_stampa_report_scarico_mag_for (long al_anno_bolla_acq, long al_num_bolla_acq, string as_note_aggiuntive, ref string as_error)
public function integer uof_elimina_movimenti (integer fi_anno_registrazione, long fl_num_registrazione, boolean fb_elimina_records)
public function integer uof_movimenti_mag_ragguppato (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine, ref string as_error)
public function integer uof_aggiorna_mov_magazzino (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdd_data_stock, long fl_prog_stock, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, string fs_cod_cliente, string fs_cod_fornitore, boolean fb_storno, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine)
private function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine)
public function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], ref string fs_errore)
public function integer uof_aggiorna_prodotti (string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, string fs_cod_fornitore, boolean fb_storno, ref string fs_errore)
public function integer uof_registra_movimento_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, ref string fs_cod_fornitore, ref string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine, ref string as_errore)
public function integer uof_reparti_sl (integer ai_anno_reg_ord_ven, long al_num_reg_ord_ven, long al_prog_riga_ord_ven, ref string as_reparti[])
public function integer uof_inventario_magazzino (datetime adt_data_riferimento, string as_cod_prodotto_da, string as_cod_prodotto_a, string as_cod_deposito, string as_flag_fiscale, string as_flag_lifo, string as_tipo_valorizzazione, ref decimal ad_result, ref string as_error)
public function integer uof_costo_impegnato_ordine (integer ai_anno_registrazione, long al_num_registrazione, datetime ad_data_riferimento, integer ai_gg_prima, string as_flag_aperti_evasi, ref decimal ad_costo_mat_prime, ref string as_error)
public function decimal uof_costo_impegnato_ordini (datetime adt_data_da, datetime adt_data_a, string as_flag_aperti_evasi, ref decimal ad_costo_impegnato, ref string as_error)
public function decimal uof_costo_fatturato (datetime adt_data_da, datetime adt_data_a, ref decimal ad_costo_fatturato, ref string as_error)
public function integer uof_costo_fatturato_fattura (integer ai_anno_registrazione, long al_num_registrazione, datetime ad_data_riferimento, integer ai_gg_prima, ref decimal ad_costo_mat_prime, ref string as_error)
public subroutine uof_set_flag_commessa (string as_flag_commessa)
public function integer uof_modifica_movimento (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[])
end prototypes

public function integer uof_tipo_movimento (string fs_cod_tipo_mov_det, ref long fl_tipo_operazione, ref boolean fb_crea_nuovo_stock, ref boolean fb_aggiorna_costo_medio, ref boolean fb_aggiorna_costo_ultimo);//-------------------------------------------------------------------------------------
//	funzione che determina se il movimento indicato è di carico o di scarico 
//
//
//	redatta da: Enrico Menegotto
// data redazione: 11/7/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//																	o aggiornare quelli passati
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------------------------
// Ricorda: quando si esegue un movimento di storno i campi cliente, fornitore e
//          flag_primo_movimento  sono ininfluenti.
//-------------------------------------------------------------------------------------
string ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita,   &
       ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
       ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_cod_cliente, ls_flag_cliente, &
		 ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_flag_fornitore, ls_cod_fornitore,&
		 ls_flag_saldo_quan_inizio_anno, ls_flag_val_inizio_anno, ls_flag_saldo_quan_ultima_chius, &
		 ls_flag_costo_ultimo, ls_flag_costo_medio
		 

SELECT tab_tipi_movimenti_det.flag_saldo_quan_inizio_anno,   
		 tab_tipi_movimenti_det.flag_val_inizio_anno,
		 tab_tipi_movimenti_det.flag_saldo_quan_ultima_chius,   
		 tab_tipi_movimenti_det.flag_prog_quan_entrata,   
		 tab_tipi_movimenti_det.flag_val_quan_entrata,   
		 tab_tipi_movimenti_det.flag_prog_quan_uscita,   
		 tab_tipi_movimenti_det.flag_val_quan_uscita,   
		 tab_tipi_movimenti_det.flag_prog_quan_acquistata,   
		 tab_tipi_movimenti_det.flag_val_quan_acquistata,   
		 tab_tipi_movimenti_det.flag_prog_quan_venduta,   
		 tab_tipi_movimenti_det.flag_val_quan_venduta,
		 tab_tipi_movimenti_det.flag_costo_ultimo,
		 tab_tipi_movimenti_det.flag_costo_medio
 INTO  :ls_flag_saldo_quan_inizio_anno,   
		 :ls_flag_val_inizio_anno,
		 :ls_flag_saldo_quan_ultima_chius,   
		 :ls_flag_prog_quan_entrata,   
		 :ls_flag_val_quan_entrata,   
		 :ls_flag_prog_quan_uscita,   
		 :ls_flag_val_quan_uscita,   
		 :ls_flag_prog_quan_acquistata,   
		 :ls_flag_val_quan_acquistata,   
		 :ls_flag_prog_quan_venduta,   
		 :ls_flag_val_quan_venduta,
		 :ls_flag_costo_ultimo,
		 :ls_flag_costo_medio
 FROM  tab_tipi_movimenti_det  
WHERE  ( tab_tipi_movimenti_det.cod_azienda = :s_cs_xx.cod_azienda      ) AND  
		 ( tab_tipi_movimenti_det.cod_tipo_mov_det = :fs_cod_tipo_mov_det )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante caricamento configurazione movimenti", stopsign!)
	return -1
end if

fl_tipo_operazione = 0
fb_crea_nuovo_stock = false

choose case ls_flag_prog_quan_entrata
	case "+"
		fl_tipo_operazione = 1					// carico magazzino
		fb_crea_nuovo_stock = true
   case "-"
		fl_tipo_operazione = 2					// rettifica carico magazzino
		fb_crea_nuovo_stock = false
end choose

choose case ls_flag_prog_quan_uscita
	case "+"
		fl_tipo_operazione = 3					// scarico magazzino
		fb_crea_nuovo_stock = false
   case "-"
		fl_tipo_operazione = 4					// rettifica scarico magazzino
		fb_crea_nuovo_stock = false
end choose

choose case ls_flag_prog_quan_acquistata
	case "+"
		fl_tipo_operazione = 5					// carico acquisto magazzino
		fb_crea_nuovo_stock = true
   case "-"
		fl_tipo_operazione = 6					// rettifica carico acquisto magazzino
		fb_crea_nuovo_stock = false
end choose

choose case ls_flag_prog_quan_venduta
	case "+"
		fl_tipo_operazione = 7					// scarico vendita magazzino
		fb_crea_nuovo_stock = false
   case "-"
		fl_tipo_operazione = 8					// rettifica scarico vendita magazzino
		fb_crea_nuovo_stock = false
end choose

choose case ls_flag_saldo_quan_ultima_chius
	case "+"
		fl_tipo_operazione = 19					// quantita ultima chiusura
		fb_crea_nuovo_stock = true
   case "-"
		fl_tipo_operazione = 20					// rettifica quantità ultima chiusura
		fb_crea_nuovo_stock = false
end choose

choose case ls_flag_saldo_quan_inizio_anno
	case "+"
		fl_tipo_operazione = 13					// carico iniziale
		fb_crea_nuovo_stock = true
   case "-"
		fl_tipo_operazione = 14					// rettifica carico iniziale
		fb_crea_nuovo_stock = false
end choose

if fl_tipo_operazione = 0 then 				// nessun movimento di quantità; verifico
														// se c'è qualche movimento di solo valore				
	choose case ls_flag_val_quan_entrata
		case "+"										// rettifica in incremento valore entrata
			fl_tipo_operazione = 9
			fb_crea_nuovo_stock = false
		case "-"										// rettifica in decremento valore entrata
			fl_tipo_operazione = 15
			fb_crea_nuovo_stock = false
	end choose
	
	choose case ls_flag_val_quan_acquistata
		case "+"										// rettifica in incremento valore acquistato
			fl_tipo_operazione = 10
			fb_crea_nuovo_stock = false
		case "-"										// rettifica in decremento valore acquistato
			fl_tipo_operazione = 16
			fb_crea_nuovo_stock = false
	end choose

	choose case ls_flag_val_quan_uscita
		case "+"										// rettifica in incremento valore uscita
			fl_tipo_operazione = 11
			fb_crea_nuovo_stock = false
		case "-"										// rettifica in decremento valore uscita
			fl_tipo_operazione = 17
			fb_crea_nuovo_stock = false
	end choose

	choose case ls_flag_val_quan_venduta
		case "+"										// rettifica in incremento valore venduto
			fl_tipo_operazione = 12
			fb_crea_nuovo_stock = false
		case "-"										// rettifica in decremento valore venduto
			fl_tipo_operazione = 18
			fb_crea_nuovo_stock = false
	end choose

end if

choose case ls_flag_costo_ultimo
	case "+"
		fb_aggiorna_costo_ultimo = true
	case "-"
		fb_aggiorna_costo_ultimo = true
	case "="
		fb_aggiorna_costo_ultimo = false
end choose

choose case ls_flag_costo_medio
	case "+"
		fb_aggiorna_costo_medio = true
	case "-"
		fb_aggiorna_costo_medio = true
	case "="
		fb_aggiorna_costo_medio = false
end choose
		
if fl_tipo_operazione = 0 then
	g_mb.messagebox("APICE","Il movimento non compie alcuna azione ne di carico ne di scarico",StopSign!)
	return -1
end if
return 0

end function

public function integer uof_aggiorna_stock (string fs_cod_prodotto, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, ref string fs_cod_fornitore, ref string fs_cod_cliente, decimal fd_quantita, decimal fd_valore_unitario, boolean fb_primo_movimento, readonly boolean fb_storno, readonly boolean fb_update);//-------------------------------------------------------------------------------------
//	funzione di creazione - aggiornamento stock prodotti
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_prodotto				string				--	
//		fs_cod_deposito				string				   \
//		fs_cod_ubicazione				string                \ ____  riferimento stock
//		fs_cod_lotto					string				    /
//		fdd_data_stock					date					   /
//		fl_prog_stock					long					--	
//		fs_cod_tipo_movimento		string				tipo movimento magazzino
//		fs_cod_tipo_mov_det			string				tipo configurazione movimento
//		fs_cod_fornitore				string				codice fornitore terzista
//		fs_cod_cliente					string				codice cliente   terzista
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fb_primo_movimento			boolean				è il primo movimento?  TRUE/FALSE
//		fb_storno						boolean				movimento di storno? TRUE / FALSE
//		fb_update						boolean				flag indicante se deve creare nuovi stock
//																	o aggiornare quelli passati
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_deposito				string				   \
//		fs_cod_ubicazione				string                \ ____  riferimento stock
//		fs_cod_lotto					string				    /
//		fdd_data_stock					date					   /
//		fl_prog_stock					long					--	
//		fs_cod_fornitore				string				codice fornitore terzista
//		fs_cod_cliente					string				codice cliente   terzista
//
//
//
//-------------------------------------------------------------------------------------
// Ricorda: quando si esegue un movimento di storno i campi cliente, fornitore e
//          flag_primo_movimento  sono ininfluenti.
//-------------------------------------------------------------------------------------

boolean						lb_crea_nuovo_stock, lb_aggiorna_costo_medio, lb_aggiorna_costo_ultimo

string						ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita, ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
								ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_cod_cliente, ls_flag_cliente, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_flag_fornitore, ls_cod_fornitore, &
								ls_error
								
long							ll_tipo_operazione, ll_prog_stock, ll_tipo_errore

date							ldd_data_stock

double						ld_giacenza_stock, ld_costo_medio, ld_valore_giacenza, ld_quan_assegnata, ld_quan_in_spedizione, ld_max_12_4, ld_max_16_4


fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

lb_crea_nuovo_stock = false
ll_tipo_operazione  = 0
setnull(ls_cod_lotto)
setnull(ls_cod_deposito)
setnull(ls_cod_ubicazione)

ld_max_12_4 = 999999999999.9999
ld_max_16_4 = 9999999999999999.9999


if uof_tipo_movimento(fs_cod_tipo_mov_det,ll_tipo_operazione,lb_crea_nuovo_stock,lb_aggiorna_costo_medio,lb_aggiorna_costo_ultimo) = -1 then
	return -1
end if

if lb_crea_nuovo_stock and not(fb_storno) then

	if isnull(fs_cod_deposito) then
		g_mb.messagebox("APICE","Attenzione: codice deposito mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
		return -1
	end if
	
	if isnull(fs_cod_ubicazione) then
		g_mb.messagebox("APICE","Attenzione: codice ubicazione mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
		return -1
	end if
	
	if isnull(fs_cod_lotto) then
		g_mb.messagebox("APICE","Attenzione: codice lotto mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
		return -1
	end if
	
	select prog_stock
	into   :ll_prog_stock
	from	 stock
	where  stock.cod_azienda    = :s_cs_xx.cod_azienda and
			 stock.cod_prodotto   = :fs_cod_prodotto and
			 stock.cod_deposito   = :fs_cod_deposito and
			 stock.cod_ubicazione = :fs_cod_ubicazione and
			 stock.cod_lotto      = :fs_cod_lotto and
			 stock.data_stock     = :fdd_data_stock and
			 stock.prog_stock     = :fl_prog_stock;

	if (not(fb_update) and sqlca.sqlcode <> 0) or (fb_update and sqlca.sqlcode <> 0) then
		
		if uof_crea_nuovo_stock(fs_cod_prodotto,fs_cod_deposito,fs_cod_ubicazione,fs_cod_lotto,fdd_data_stock,fl_prog_stock,fs_cod_fornitore,fs_cod_cliente) < 0 then
			return -1
		end if
	
	end if
end if

if isnull(fs_cod_deposito) then
	g_mb.messagebox("APICE","Attenzione: codice deposito mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
	return -1
end if
if isnull(fs_cod_ubicazione) then
	g_mb.messagebox("APICE","Attenzione: codice ubicazione mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
	return -1
end if
if isnull(fs_cod_lotto) then
	g_mb.messagebox("APICE","Attenzione: codice lotto mancante in creazione stock con codice movimento:" + fs_cod_tipo_movimento + "/" + fs_cod_tipo_mov_det, Stopsign!)
	return -1
end if

select stock.giacenza_stock,   
		 stock.costo_medio,
		 stock.quan_assegnata,
		 stock.quan_in_spedizione
into   :ld_giacenza_stock,   
		 :ld_costo_medio,
		 :ld_quan_assegnata,
		 :ld_quan_in_spedizione
from   stock 
where  stock.cod_azienda    = :s_cs_xx.cod_azienda and
		 stock.cod_prodotto   = :fs_cod_prodotto and
		 stock.cod_deposito   = :fs_cod_deposito and
		 stock.cod_ubicazione = :fs_cod_ubicazione and
		 stock.cod_lotto      = :fs_cod_lotto and
		 stock.data_stock     = :fdd_data_stock and
		 stock.prog_stock     = :fl_prog_stock;
//if sqlca.sqlcode <> 0 then
//	g_mb.messagebox("APICE","Errore durante ricerca ed identificazione stock", stopsign!)
//	return -1
//end if

// stefanop 26/04/2012: Provo a creare lo stock se non esiste
// nel caso lo stock non esista lo provo a creare
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante ricerca ed identificazione stock", sqlca)
	return -1
elseif sqlca.sqlcode = 100 then
	
	//correggo e basta (ci provo) senza chiedere
//	if g_mb.confirm("Errore durante la ricerca ed idenficazione stock.~r~nProvo a correggere automaticamente l'errore?") = false then
//		return -1
//	end if
	
	if uof_crea_nuovo_stock(fs_cod_prodotto,fs_cod_deposito,fs_cod_ubicazione,fs_cod_lotto,fdd_data_stock,fl_prog_stock,fs_cod_fornitore,fs_cod_cliente) < 0 then
		return -1
	end if
	
	// riseleziono tutto
	select stock.giacenza_stock,   
			stock.costo_medio,
			stock.quan_assegnata,
			stock.quan_in_spedizione
	into   :ld_giacenza_stock,   
			:ld_costo_medio,
			:ld_quan_assegnata,
			:ld_quan_in_spedizione
	from stock 
	where stock.cod_azienda = :s_cs_xx.cod_azienda and
			stock.cod_prodotto = :fs_cod_prodotto and
			stock.cod_deposito = :fs_cod_deposito and
			stock.cod_ubicazione = :fs_cod_ubicazione and
			stock.cod_lotto = :fs_cod_lotto and
			stock.data_stock = :fdd_data_stock and
			stock.prog_stock = :fl_prog_stock;
			
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","L'autocorrezzione non ha avuto esito positivo. Provare a sistemare manualmente", stopsign!)
		return -1
	end if
	
end if

choose case ll_tipo_operazione
	case 1, 5, 4, 8, 13					// aggiorno in incremento giacenza e costo medio
		if fb_storno then
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_giacenza_stock  = ld_giacenza_stock - fd_quantita
			ld_valore_giacenza = ld_valore_giacenza - ( fd_quantita * fd_valore_unitario )
		else
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_giacenza_stock  = ld_giacenza_stock + fd_quantita
			ld_valore_giacenza = ld_valore_giacenza + ( fd_quantita * fd_valore_unitario )
		end if
	case 2, 3, 6, 7, 14
		if fb_storno then
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_giacenza_stock = ld_giacenza_stock + fd_quantita
			ld_valore_giacenza = ld_valore_giacenza + ( fd_quantita * fd_valore_unitario )
		else
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_giacenza_stock = ld_giacenza_stock - fd_quantita
			ld_valore_giacenza = ld_valore_giacenza - ( fd_quantita * fd_valore_unitario )
		end if
	case 9, 10, 17, 18			// incremento del solo valore giacenza (aumento costo medio ) - ( rettifica )
		if fb_storno then
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_valore_giacenza = ld_valore_giacenza - ( fd_quantita * fd_valore_unitario )
		else
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_valore_giacenza = ld_valore_giacenza + ( fd_quantita * fd_valore_unitario )
		end if
	case 15, 16, 11, 12			// decremento del solo valore giacenza (diminuzione costo medio ) - ( rettifica )
		if fb_storno then
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_valore_giacenza = ld_valore_giacenza + ( fd_quantita * fd_valore_unitario )
		else			
			ld_valore_giacenza = ld_giacenza_stock * ld_costo_medio
			ld_valore_giacenza = ld_valore_giacenza - ( fd_quantita * fd_valore_unitario )
		end if
end choose

if lb_aggiorna_costo_medio then     // aggiorno costo medio giacenza stock
	if ld_giacenza_stock <> 0   then
		ld_costo_medio = round(ld_valore_giacenza / ld_giacenza_stock, 4 )
	else
		ld_costo_medio = 0
	end if
end if

ld_giacenza_stock = round(ld_giacenza_stock,4)
ld_quan_assegnata = round(ld_quan_assegnata,4)
ld_quan_in_spedizione = round(ld_quan_in_spedizione,4)


// *** michela 11/07/2005. decido se controllare oppure no la qtà assegnata e la qtà in spedizione
if ib_salta_controllo = false then
	if not ib_negativo and (ld_giacenza_stock - ( ld_quan_assegnata + ld_quan_in_spedizione )) < 0 then
		g_mb.messagebox("APICE","Giacenza negativa aggiornando stock da movimento  " + fs_cod_tipo_movimento+" / "+fs_cod_tipo_mov_det + "   : verificare",StopSign!)
		return -1
	end if
else
	if ib_avvisa then		
		if not ib_negativo and (ld_giacenza_stock - ( ld_quan_assegnata + ld_quan_in_spedizione )) < 0 then
			g_mb.messagebox("APICE","Giacenza negativa aggiornando stock da movimento  " + fs_cod_tipo_movimento+" / "+fs_cod_tipo_mov_det + "   : verificare prodotto " + fs_cod_prodotto,StopSign!)
		end if				
	end if
end if

//---------------------------------------------------------------------------------------------------------------------------------
//impedisco che vengano superati i valori massimi per evitare errore in salvataggio
if ld_giacenza_stock > ld_max_12_4 then
	ld_giacenza_stock = ld_max_12_4
end if

if ld_costo_medio > ld_max_16_4 then
	ld_costo_medio = ld_max_16_4
end if

if isnull(ld_giacenza_stock) or ld_giacenza_stock<0 then ld_giacenza_stock = 0

//---------------------------------------------------------------------------------------------------------------------------------


if fb_storno then
	
	ll_tipo_errore = 1
	
	update stock
	set    giacenza_stock = :ld_giacenza_stock,
			 costo_medio    = :ld_costo_medio,
			 cod_cliente    = :fs_cod_cliente,
			 cod_fornitore  = :fs_cod_fornitore
	where  stock.cod_azienda    = :s_cs_xx.cod_azienda and
			 stock.cod_prodotto   = :fs_cod_prodotto and
			 stock.cod_deposito   = :fs_cod_deposito and
			 stock.cod_ubicazione = :fs_cod_ubicazione and
			 stock.cod_lotto      = :fs_cod_lotto and
			 stock.data_stock     = :fdd_data_stock and
			 stock.prog_stock     = :fl_prog_stock;
else
	
	ll_tipo_errore = 2
	
	update stock
	set    giacenza_stock = :ld_giacenza_stock,
			 costo_medio    = :ld_costo_medio
	where  stock.cod_azienda    = :s_cs_xx.cod_azienda and
			 stock.cod_prodotto   = :fs_cod_prodotto and
			 stock.cod_deposito   = :fs_cod_deposito and
			 stock.cod_ubicazione = :fs_cod_ubicazione and
			 stock.cod_lotto      = :fs_cod_lotto and
			 stock.data_stock     = :fdd_data_stock and
			 stock.prog_stock     = :fl_prog_stock;
end if	

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", string(ll_tipo_errore) + ".Errore durante aggiornamento valori stock (" + fs_cod_prodotto + "). Dettaglio errore:" + sqlca.sqlerrtext, stopsign!)
	return -1
end if

return 0
end function

public function integer uof_new_con_magazzino (datetime fdd_data_registrazione, ref long fl_prog_mov);//-------------------------------------------------------------------------------------
//	funzione di gestione progressivo registrazione movimento magazzino
//
//
//	redatta da: Enrico Menegotto
// data redazione: 23/7/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data registrazione movimento
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fl_prog_mov						long					nuovo progressivo registrazione
//
//
//-------------------------------------------------------------------------------------
long     ll_prog_mov
datetime ldd_data_chiusura_periodica


ldd_data_chiusura_periodica = datetime(today(),time('00:00:00'))
select data_chiusura_periodica
into   :ldd_data_chiusura_periodica
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Anomalia nella tabella Controllo Magazzino",StopSign!)
	return -1
end if
if ldd_data_chiusura_periodica >= fdd_data_registrazione and not ib_chiusura then
	g_mb.messagebox("APICE","Data di registrazione movimento minore o uguale alla data di ultima chiusura",StopSign!)
	return -1
end if

select prog_mov
into   :ll_prog_mov
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Anomalia nella tabella Controllo Magazzino",StopSign!)
	return -1
end if

if isnull(ll_prog_mov) then
	ll_prog_mov = 1
else
	ll_prog_mov ++
end if
fl_prog_mov = ll_prog_mov

update con_magazzino
set    prog_mov = :ll_prog_mov
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Anomalia nella tabella Controllo Magazzino",StopSign!)
	return -1
end if
return 0
end function

public function integer uof_aggiorna_prodotti (string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, string fs_cod_fornitore, boolean fb_storno);integer			li_ret
string				ls_errore


li_ret = uof_aggiorna_prodotti(	fs_cod_tipo_movimento, fs_cod_tipo_mov_det, fs_cod_prodotto, fd_quantita, fd_valore_unitario, fs_cod_fornitore, fb_storno, ls_errore)

if li_ret<0 then
	g_mb.error(ls_errore)
	return -1
end if

return 0




//-------------------------------------------------------------------------------------
//	funzione di aggiornamento anagrafica prodotti magazzino
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:    0 = tutto OK
//  												-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// -----------------------------------------------------------------------------
//		fs_cod_tipo_mov_det			string				tipo configurazione movimento
//		fs_cod_prodotto				string				--	
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fs_cod_fornitore				string				codice fornitore ( per ora non usato )
//		fb_storno						boolean				TRUE = movimento di storno 
//
// variabili in ingresso:			tipo di dato		commento
// -----------------------------------------------------------------------------
//	
//-------------------------------------------------------------------------------------

string ls_flag_saldo_quan_inizio_anno, ls_flag_val_inizio_anno, ls_flag_saldo_quan_ultima_chius, &  
       ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita,   &
       ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
       ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_flag_costo_ultimo,   &
       ls_flag_costo_medio, ls_flag_fornitore

dec{4} ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chius, &  
       ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita,   &
       ld_val_quan_uscita, ld_prog_quan_acquistata, ld_val_quan_acquistata,   &
       ld_prog_quan_venduta, ld_val_quan_venduta, ld_costo_ultimo

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

ld_saldo_quan_inizio_anno = 0
ld_val_inizio_anno = 0
ld_saldo_quan_ultima_chius = 0
ld_prog_quan_entrata = 0
ld_val_quan_entrata = 0
ld_prog_quan_uscita = 0
ld_val_quan_uscita = 0
ld_prog_quan_acquistata = 0
ld_val_quan_acquistata = 0
ld_prog_quan_venduta = 0
ld_val_quan_venduta = 0
ld_costo_ultimo = 0

select flag_fornitore  
  into :ls_flag_fornitore  
  from det_tipi_movimenti
 where cod_azienda = :s_cs_xx.cod_azienda  AND  
       cod_tipo_movimento = :fs_cod_tipo_movimento AND  
       cod_tipo_mov_det = :fs_cod_tipo_mov_det  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca su DET_TIPI_MOVIMENTI: verificare!",StopSign!)
	return -1
end if


// ------ se è attiva l'indicazione flag_fornitore non aggiorno il mio magazzino --------
// -----------  perchè il movimento riguarda il magazzino del mio terzista   ------------

if ls_flag_fornitore = "S" then return 0

// ---------------------------------------------------------------------------------------

select flag_saldo_quan_inizio_anno,   
 		 flag_val_inizio_anno,   
	 	 flag_saldo_quan_ultima_chius,   
		 flag_prog_quan_entrata,   
		 flag_val_quan_entrata,   
		 flag_prog_quan_uscita,   
		 flag_val_quan_uscita,   
		 flag_prog_quan_acquistata,   
		 flag_val_quan_acquistata,   
		 flag_prog_quan_venduta,   
		 flag_val_quan_venduta,   
		 flag_costo_ultimo,   
		 flag_costo_medio  
 into  :ls_flag_saldo_quan_inizio_anno,   
		 :ls_flag_val_inizio_anno,   
		 :ls_flag_saldo_quan_ultima_chius,   
		 :ls_flag_prog_quan_entrata,   
		 :ls_flag_val_quan_entrata,   
		 :ls_flag_prog_quan_uscita,   
		 :ls_flag_val_quan_uscita,   
		 :ls_flag_prog_quan_acquistata,   
		 :ls_flag_val_quan_acquistata,   
		 :ls_flag_prog_quan_venduta,   
		 :ls_flag_val_quan_venduta,   
		 :ls_flag_costo_ultimo,   
		 :ls_flag_costo_medio  
 from  tab_tipi_movimenti_det
where  ( cod_azienda = :s_cs_xx.cod_azienda      ) AND  
		 ( cod_tipo_mov_det = :fs_cod_tipo_mov_det )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca della configurazione del movimento: verificare!",StopSign!)
	return -1
end if


choose case ls_flag_saldo_quan_inizio_anno
	case "+"
		ld_saldo_quan_inizio_anno = fd_quantita
	case "-"
		ld_saldo_quan_inizio_anno = - fd_quantita
	case "="
		ld_saldo_quan_inizio_anno = 0
end choose

choose case ls_flag_val_inizio_anno
	case "+"
		ld_val_inizio_anno = fd_valore_unitario * fd_quantita
	case "-"
		ld_val_inizio_anno = - fd_valore_unitario * fd_quantita
	case "="
		ld_val_inizio_anno = 0
		
end choose

choose case ls_flag_saldo_quan_ultima_chius
	case "+"
		ld_saldo_quan_ultima_chius = fd_quantita
	case "-"
		ld_saldo_quan_ultima_chius = - fd_quantita
	case "="
		ld_saldo_quan_ultima_chius = 0
end choose

choose case ls_flag_prog_quan_entrata
	case "+"
		ld_prog_quan_entrata = fd_quantita
	case "-"
		ld_prog_quan_entrata = - fd_quantita
	case "="
		ld_prog_quan_entrata = 0
end choose

choose case ls_flag_val_quan_entrata
	case "+"
		ld_val_quan_entrata = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_entrata = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_entrata = 0
end choose

choose case ls_flag_prog_quan_uscita
	case "+"
		ld_prog_quan_uscita = fd_quantita
	case "-"
		ld_prog_quan_uscita = - fd_quantita
	case "="
		ld_prog_quan_uscita = 0
end choose

choose case ls_flag_val_quan_uscita
	case "+"
		ld_val_quan_uscita = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_uscita = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_uscita = 0
end choose

choose case ls_flag_prog_quan_acquistata
	case "+"
		ld_prog_quan_acquistata = fd_quantita
	case "-"
		ld_prog_quan_acquistata = - fd_quantita
	case "="
		ld_prog_quan_acquistata = 0
end choose

choose case ls_flag_val_quan_acquistata
	case "+"
		ld_val_quan_acquistata = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_acquistata = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_acquistata = 0
end choose

choose case ls_flag_prog_quan_venduta
	case "+"
		ld_prog_quan_venduta = fd_quantita
	case "-"
		ld_prog_quan_venduta = - fd_quantita
	case "="
		ld_prog_quan_venduta = 0
end choose

choose case ls_flag_val_quan_venduta
	case "+"
		ld_val_quan_venduta = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_venduta = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_venduta = 0
end choose

choose case ls_flag_costo_ultimo
	case "+"		

		// *** michela 18/03/04: se ib_flag_agg_costo_ultimo è true e il movimento di magazzino lo consente allora aggiorno
		if ib_flag_agg_costo_ultimo then 
			ld_costo_ultimo = fd_valore_unitario
		else
			select anag_prodotti.costo_ultimo
			into   :ld_costo_ultimo
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
					 anag_prodotti.cod_prodotto = :fs_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Impossibile trovare il prodotto a magazzino. " + sqlca.sqlerrtext,StopSign!)
				return -1
			end if			
		end if
		
	case "-"

		// *** michela 18/03/04: se ib_flag_agg_costo_ultimo è true e il movimento di magazzino lo consente allora aggiorno
		if ib_flag_agg_costo_ultimo then 		
			ld_costo_ultimo = fd_valore_unitario
		else
			select anag_prodotti.costo_ultimo
			into   :ld_costo_ultimo
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
					 anag_prodotti.cod_prodotto = :fs_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Impossibile trovare il prodotto a magazzino. " + sqlca.sqlerrtext,StopSign!)
				return -1
			end if			
		end if			
		
	case "="
		select anag_prodotti.costo_ultimo
		into   :ld_costo_ultimo
		from   anag_prodotti
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
				 anag_prodotti.cod_prodotto = :fs_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Impossibile trovare il prodotto a magazzino",StopSign!)
			return -1
		end if
end choose

if fb_storno then
	ld_saldo_quan_inizio_anno = ld_saldo_quan_inizio_anno * -1
	ld_val_inizio_anno = ld_val_inizio_anno * -1 
	ld_saldo_quan_ultima_chius = ld_saldo_quan_ultima_chius * -1
	ld_prog_quan_entrata = ld_prog_quan_entrata * -1
	ld_val_quan_entrata = ld_val_quan_entrata * -1
	ld_prog_quan_uscita = ld_prog_quan_uscita * -1
	ld_val_quan_uscita = ld_val_quan_uscita * -1
	ld_prog_quan_acquistata = ld_prog_quan_acquistata * -1
	ld_val_quan_acquistata = ld_val_quan_acquistata * -1
	ld_prog_quan_venduta = ld_prog_quan_venduta * -1
	ld_val_quan_venduta = ld_val_quan_venduta * -1
end if
	
update anag_prodotti
   set saldo_quan_inizio_anno = saldo_quan_inizio_anno + :ld_saldo_quan_inizio_anno,   
       val_inizio_anno = val_inizio_anno + :ld_val_inizio_anno,   
		 saldo_quan_ultima_chiusura = saldo_quan_ultima_chiusura + :ld_saldo_quan_ultima_chius,
       prog_quan_entrata = prog_quan_entrata + :ld_prog_quan_entrata,   
       val_quan_entrata = val_quan_entrata + :ld_val_quan_entrata,   
       prog_quan_uscita = prog_quan_uscita + :ld_prog_quan_uscita,   
       val_quan_uscita = val_quan_uscita + :ld_val_quan_uscita,   
       prog_quan_acquistata = prog_quan_acquistata + :ld_prog_quan_acquistata,   
       val_quan_acquistata = val_quan_acquistata + :ld_val_quan_acquistata,   
       prog_quan_venduta = prog_quan_venduta + :ld_prog_quan_venduta,   
       val_quan_venduta = val_quan_venduta + :ld_val_quan_venduta,   
       costo_ultimo = :ld_costo_ultimo  
WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( anag_prodotti.cod_prodotto = :fs_cod_prodotto )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante aggiornamento anagrafica magazzino: verificare",Stopsign!)
	return -1
else
	return 0
end if
end function

public function integer uof_single_mov_mag_decimal (string fs_cod_tipo_movimento_det, decimal fd_quan_movimento, decimal fd_val_movimento, ref decimal fd_quant_val[], ref decimal fd_quant_val_stock[], ref string fs_error, string fs_cod_tipo_movimento, ref decimal fd_quan_mov);//f_single_mov_mag ( string fs_cod_tipo_movimento, double fd_quan_movimento, 
// 	double fd_val_movimento, by reference double fd_quant_val[], 
// by reference string fs_error, fs_cod_tipo_movimento, fd_quan_mov)
// usata anche nella chiusura annuale
// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=costo_ultimo

// ritorna 1 se flag_fornitore = S (caso terzista)
// aggiorna le quantità e le rispettive valorizzazioni del vettore ld_quant_val in base 
// al tipo di movimento 
// nota: fd_val_movimento è il valore unitario del movimento
// se flag_fornitore=S, non modifica fd_quant_val[], ma scrive quantità mov. in fd_quan_mov, con segno
// (fd_quan_mov usata per calcolo giacenza per deposito/stock, non totale, ma senza utilizzare la fs_where di f_saldo_prodo_date)


// 14-06-2004 Enrico
//	Aggiunto fd_quan_val_stock[] per il calcolo della movimentazione quantità valore del singolo stock

string ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita,   &
       ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
       ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_flag_fornitore, ls_flag_cliente, &
		 ls_flag_saldo_quan_inizio_anno, ls_flag_val_inizio_anno, ls_flag_saldo_quan_ultima_chius, &
		 ls_flag_costo_medio
boolean lb_terzista = false		 

select flag_fornitore, flag_cliente, flag_costo_medio  
  into :ls_flag_fornitore, :ls_flag_cliente, :ls_flag_costo_medio  
  from det_tipi_movimenti
 where cod_azienda = :s_cs_xx.cod_azienda AND  
       cod_tipo_movimento = :fs_cod_tipo_movimento AND  
       cod_tipo_mov_det = :fs_cod_tipo_movimento_det ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca su DET_TIPI_MOVIMENTI: verificare!",StopSign!)
	return -1
end if
// ------ se è attiva l'indicazione flag_fornitore non aggiorno il mio magazzino --------
// -----------  perchè il movimento riguarda il magazzino del mio terzista   ------------

SELECT flag_saldo_quan_inizio_anno, flag_val_inizio_anno, flag_saldo_quan_ultima_chius,   
		 flag_prog_quan_entrata, flag_val_quan_entrata, flag_prog_quan_uscita, 
		 flag_val_quan_uscita, flag_prog_quan_acquistata, flag_val_quan_acquistata,   
		 flag_prog_quan_venduta, flag_val_quan_venduta
 INTO  :ls_flag_saldo_quan_inizio_anno,   
		 :ls_flag_val_inizio_anno,
		 :ls_flag_saldo_quan_ultima_chius,   
		 :ls_flag_prog_quan_entrata,   
		 :ls_flag_val_quan_entrata,   
		 :ls_flag_prog_quan_uscita,   
		 :ls_flag_val_quan_uscita,   
		 :ls_flag_prog_quan_acquistata,   
		 :ls_flag_val_quan_acquistata,   
		 :ls_flag_prog_quan_venduta,   
		 :ls_flag_val_quan_venduta
 FROM  tab_tipi_movimenti_det  
WHERE  ( tab_tipi_movimenti_det.cod_azienda = :s_cs_xx.cod_azienda      ) AND  
		 ( tab_tipi_movimenti_det.cod_tipo_mov_det = :fs_cod_tipo_movimento_det)   ;
if sqlca.sqlcode <> 0 then
	fs_error="Errore durante caricamento configurazione movimenti"
	return -1
end if

// if ls_flag_cliente = "S" then return 1
if ls_flag_fornitore = "N" then	// non terzista
	
	if ls_flag_saldo_quan_inizio_anno="+" then
		fd_quant_val[1] = fd_quant_val[1] + fd_quan_movimento
		fd_quant_val_stock[1] = fd_quant_val_stock[1] + fd_quan_movimento
		if ls_flag_costo_medio="S" then	//costo medio
			fd_quant_val[12] = fd_quant_val[12] + fd_quan_movimento
			fd_quant_val_stock[12] = fd_quant_val_stock[12] + fd_quan_movimento
		end if
	elseif ls_flag_saldo_quan_inizio_anno="-" then
		fd_quant_val[1] = fd_quant_val[1] - fd_quan_movimento
		fd_quant_val_stock[1] = fd_quant_val_stock[1] - fd_quan_movimento
		if ls_flag_costo_medio="S" then
			fd_quant_val[12] = fd_quant_val[12] - fd_quan_movimento
			fd_quant_val_stock[12] = fd_quant_val_stock[12] - fd_quan_movimento
		end if
	end if
	
	if ls_flag_val_inizio_anno="+" then
		fd_quant_val[2] = fd_quant_val[2] + fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[2] = fd_quant_val_stock[2] + fd_quan_movimento * fd_val_movimento
		if ls_flag_costo_medio="S" then	//costo medio
			fd_quant_val[13] = fd_quant_val[13] + fd_quan_movimento * fd_val_movimento
			fd_quant_val_stock[13] = fd_quant_val_stock[13] + fd_quan_movimento * fd_val_movimento
		end if
	elseif ls_flag_val_inizio_anno="-" then
		fd_quant_val[2] = fd_quant_val[2] - fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[2] = fd_quant_val_stock[2] - fd_quan_movimento * fd_val_movimento
		if ls_flag_costo_medio="S" then
			fd_quant_val[13] = fd_quant_val[13] - fd_quan_movimento * fd_val_movimento
			fd_quant_val_stock[13] = fd_quant_val_stock[13] - fd_quan_movimento * fd_val_movimento
		end if
	end if
	
	if ls_flag_saldo_quan_ultima_chius="+" then
		fd_quant_val[3] = fd_quant_val[3] + fd_quan_movimento
		fd_quant_val_stock[3] = fd_quant_val_stock[3] + fd_quan_movimento
	elseif ls_flag_saldo_quan_ultima_chius="-" then
		fd_quant_val[3] = fd_quant_val[3] - fd_quan_movimento
		fd_quant_val_stock[3] = fd_quant_val_stock[3] - fd_quan_movimento
	end if
	
	if ls_flag_prog_quan_entrata="+" then
		fd_quant_val[4] = fd_quant_val[4] + fd_quan_movimento
		fd_quant_val_stock[4] = fd_quant_val_stock[4] + fd_quan_movimento
		if ls_flag_costo_medio="S" then	// costo medio
			fd_quant_val[12] = fd_quant_val[12] + fd_quan_movimento
			fd_quant_val_stock[12] = fd_quant_val_stock[12] + fd_quan_movimento
		end if
	elseif ls_flag_prog_quan_entrata="-" then
		fd_quant_val[4] = fd_quant_val[4] - fd_quan_movimento
		fd_quant_val_stock[4] = fd_quant_val_stock[4] - fd_quan_movimento
		if ls_flag_costo_medio="S" then
			fd_quant_val[12] = fd_quant_val[12] - fd_quan_movimento
			fd_quant_val_stock[12] = fd_quant_val_stock[12] - fd_quan_movimento
		end if
	end if
	
	if ls_flag_val_quan_entrata="+" then
		fd_quant_val[5] = fd_quant_val[5] + fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[5] = fd_quant_val_stock[5] + fd_quan_movimento * fd_val_movimento
		if ls_flag_costo_medio="S" then	//costo medio
			fd_quant_val[13] = fd_quant_val[13] + fd_quan_movimento * fd_val_movimento
			fd_quant_val_stock[13] = fd_quant_val_stock[13] + fd_quan_movimento * fd_val_movimento
		end if
	elseif ls_flag_val_quan_entrata="-" then
		fd_quant_val[5] = fd_quant_val[5] - fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[5] = fd_quant_val_stock[5] - fd_quan_movimento * fd_val_movimento
		if ls_flag_costo_medio="S" then
			fd_quant_val[13] = fd_quant_val[13] - fd_quan_movimento * fd_val_movimento
			fd_quant_val_stock[13] = fd_quant_val_stock[13] - fd_quan_movimento * fd_val_movimento
		end if
	end if
	
	if ls_flag_prog_quan_uscita="+" then
		fd_quant_val[6] = fd_quant_val[6] + fd_quan_movimento
		fd_quant_val_stock[6] = fd_quant_val_stock[6] + fd_quan_movimento
	elseif ls_flag_prog_quan_uscita="-" then
		fd_quant_val[6] = fd_quant_val[6] - fd_quan_movimento
		fd_quant_val_stock[6] = fd_quant_val_stock[6] - fd_quan_movimento
	end if
	
	if ls_flag_val_quan_uscita="+" then
		fd_quant_val[7] = fd_quant_val[7] + fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[7] = fd_quant_val_stock[7] + fd_quan_movimento * fd_val_movimento
	elseif ls_flag_val_quan_uscita="-" then
		fd_quant_val[7] = fd_quant_val[7] - fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[7] = fd_quant_val_stock[7] - fd_quan_movimento * fd_val_movimento
	end if
	
	if ls_flag_prog_quan_acquistata="+" then
		fd_quant_val[8] = fd_quant_val[8] + fd_quan_movimento
		fd_quant_val_stock[8] = fd_quant_val_stock[8] + fd_quan_movimento
		if ls_flag_costo_medio="S" then	// sovrascrive ultimo prezzo acq
			fd_quant_val[14] = fd_val_movimento
			fd_quant_val_stock[14] = fd_val_movimento
		end if
	elseif ls_flag_prog_quan_acquistata="-" then
		fd_quant_val[8] = fd_quant_val[8] - fd_quan_movimento
		fd_quant_val_stock[8] = fd_quant_val_stock[8] - fd_quan_movimento
	end if
	
	if ls_flag_val_quan_acquistata="+" then
		fd_quant_val[9] = fd_quant_val[9] + fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[9] = fd_quant_val_stock[9] + fd_quan_movimento * fd_val_movimento
	elseif ls_flag_val_quan_acquistata="-" then
		fd_quant_val[9] = fd_quant_val[9] - fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[9] = fd_quant_val_stock[9] - fd_quan_movimento * fd_val_movimento
	end if
	
	if ls_flag_prog_quan_venduta="+" then
		fd_quant_val[10] = fd_quant_val[10] + fd_quan_movimento
		fd_quant_val_stock[10] = fd_quant_val_stock[10] + fd_quan_movimento
	elseif ls_flag_prog_quan_venduta="-" then
		fd_quant_val[10] = fd_quant_val[10] - fd_quan_movimento
		fd_quant_val_stock[10] = fd_quant_val_stock[10] - fd_quan_movimento
	end if
	
	if ls_flag_val_quan_venduta="+" then
		fd_quant_val[11] = fd_quant_val[11] + fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[11] = fd_quant_val_stock[11] + fd_quan_movimento * fd_val_movimento
	elseif ls_flag_val_quan_venduta="-" then
		fd_quant_val[11] = fd_quant_val[11] - fd_quan_movimento * fd_val_movimento
		fd_quant_val_stock[11] = fd_quant_val_stock[11] - fd_quan_movimento * fd_val_movimento
	end if
else	// caso terzista
	// nel caso stia calcolando la giacenza di stock/deposito: 
	fd_quan_mov = 0
	if ls_flag_prog_quan_entrata="+" then	// deve aumentare giacenza
		fd_quan_mov  = fd_quan_movimento
	elseif ls_flag_prog_quan_entrata="-" then	// deve diminuire giacenza (rettifiche entrata)
		fd_quan_mov  = (- 1) * fd_quan_movimento
	end if
	if ls_flag_prog_quan_uscita="+" then	// deve diminuire giacenza (uscita)
		fd_quan_mov  = (- 1) * fd_quan_movimento
	elseif ls_flag_prog_quan_uscita="-" then	// deve aumentare giacenza (rettifiche uscita)
		fd_quan_mov  = fd_quan_movimento
	end if
	
	return 1	
end if

return 0
end function

public function integer uof_saldo_prod_date_decimal (string fs_cod_prodotto, datetime fdt_data_rif, string fs_where, ref decimal fd_quant_val[], ref string fs_error, string fs_flag_stock, ref string fs_chiave[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_stock[], ref decimal fd_quan_costo_medio_stock[]);// f_saldo_prod_data(fs_cod_prodotto, fdt_data_rif, fs_where, by reference fd_quant_val[], 
// by reference fs_error, fs_flag_stock, by reference fs_chiave[], by reference fd_giacenza_stock[])
// calcola i saldi del prodotto indicato in fs_cod_prodotto alla data fdt_data_rif
// metodo utilizzato: calcola da mov_magazzino le  quantita, da data_chiusura_annuale fino data_rif
// ritorna -1 se errore, 0 se corretto
// la data_rif deve essere successiva all'ultima chiusura annuale
// il parametro fs_where è una stringa che serve per filtrare i movimenti magazzino
// per es. in base al flag_storico: vengono allora calcolati le quantità alla data
// indicata basandosi solo su movimenti ( dopo tale data) che devono subire la chiusura periodica
// l'argomento fs_where vale " and flag_storico='N' "
// le quantità sono tornate in fd_quant_val[]

// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=val_costo_ultimo

// costo medio = [13]/[12]; [12] e [13]: mov_magazzino con flag_costo_ultimo=S
// costo ultimo: ultimo costo di acquisto alla data di riferimento
// fs_flag_stock = N: non suddivide per stock/deposito; 
// fs_flag_stock = S: suddivide per stock, e scrive risultati parziali in fs_chiave[], fd_giacenza_stock[]
// fs_flag_stock = D: suddivide per Deposito, e scrive risultati parziali fs_chiave[], fd_giacenza_stock[]
// fs_chiave: vale cod_deposito oppure stock a seconda fs_flag_stock


// 14-06/2004 Enrico
// Aggiunto argomento fd_valore_stock per ottenere il valore dello stock utile al calcolo del costo medio del singolo stock

datastore lds_mov_magazzino

string ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_chiave, &
		 ls_chiave_old, 	ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto , &
		 ls_mod_string, ls_original_select , ls_rc, ls_chiave_1[], ls_cod_deposito_tilde
long ll_num_mov_prod, ll_cont, ll_num_stock, ll_file, ll_i
integer li_ret_funz
datetime ldt_data_chiusura_annuale, ldt_data_stock
dec{4} ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, &
		 ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
		 ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, ld_val_quan_venduta, &
		 ld_saldo_quan_anno_prec, ld_quant_val[], ld_giacenza_new,  ld_giacenza_old, &
		 ld_quan_movimento, ld_val_movimento, ld_progressivo_stock, ld_quan_terzista, ld_giacenza_stock[], &
		 ld_giac_terz, ld_quant_val_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if fdt_data_rif < ldt_data_chiusura_annuale then
	fs_error="Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale"
	return -1
end if

ll_num_stock = 0

lds_mov_magazzino= create datastore
lds_mov_magazzino.DataObject = 'd_ds_saldo_mag_2'
lds_mov_magazzino.SetTransObject (sqlca)

ls_original_select = lds_mov_magazzino.Describe("DataWindow.Table.Select")

// stefanop: seleziono quali depositi considerare
choose case is_considera_depositi_fornitori
	case "N"
		// Solo quelli che NON sono di conto deposito
		ls_original_select += " AND cod_deposito NOT IN (SELECT cod_deposito FROM anag_depositi WHERE cod_azienda=~~'A01~~' AND flag_tipo_deposito=~~'C~~') "
		
	case "S"
		// Solo quelli di conto deposito fornitore
		ls_original_select += " AND cod_deposito IN (SELECT cod_deposito FROM anag_depositi WHERE cod_azienda=~~'A01~~' AND flag_tipo_deposito=~~'C~~') "
	
	case "I"
		if not isnull(is_cod_depositi_in) and is_cod_depositi_in <> "" then
			ls_cod_deposito_tilde = is_cod_depositi_in
			guo_functions.uof_replace_string(ls_cod_deposito_tilde, "'", "~~'")
			ls_original_select += "AND cod_deposito IN (" + ls_cod_deposito_tilde + ")"
		end if
	// T
		// Considera tutti i depositi

end choose

if isnull(fs_where) then fs_where = ""
// ----

ls_mod_string = "DataWindow.Table.Select='"  &
	+ ls_original_select + fs_where + "'"

ls_rc = lds_mov_magazzino.Modify(ls_mod_string)
IF ls_rc = "" THEN
	lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fdt_data_rif, ldt_data_chiusura_annuale)
ELSE
	fs_error = "Errore in operazione Modify " + ls_rc
	return -1
END IF

if (fs_flag_stock = "N") then
	lds_mov_magazzino.setsort("anno_registrazione a, num_registrazione a")
	lds_mov_magazzino.sort()
else
	lds_mov_magazzino.setsort("cod_deposito a, cod_ubicazione a, cod_lotto a, data_stock a, prog_stock a")
	lds_mov_magazzino.sort()
end if

ll_num_mov_prod = lds_mov_magazzino.rowcount()  // numero di movimenti selezionati dopo data_rif

for ll_i=1 to 14
	ld_quant_val[ll_i] = 0
	ld_quant_val_stock[ll_i] = 0
next

fd_costo_medio_stock = ld_costo_medio_stock
fd_quan_costo_medio_stock = ld_quan_costo_medio_stock
ld_giacenza_old = 0
ld_giac_terz = 0
ld_giacenza_new = 0

if (fs_flag_stock <> "N") and (ll_num_mov_prod > 0) then	// devo suddividere per stock/deposito: inizializzo ls_chiave_old
	if fs_flag_stock = "S" then	// devo suddividere per stock
		ls_cod_deposito = lds_mov_magazzino.GetItemstring(1,"cod_deposito")
		if isnull(ls_cod_deposito) or ls_cod_deposito = "" then ls_cod_deposito = "***"
		ls_cod_ubicazione = lds_mov_magazzino.GetItemstring(1,"cod_ubicazione")
		if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then ls_cod_ubicazione = "**********"
		ls_cod_lotto = lds_mov_magazzino.GetItemstring(1,"cod_lotto")
		if isnull(ls_cod_lotto) or ls_cod_lotto = "" then ls_cod_lotto = "**********"
		ldt_data_stock = lds_mov_magazzino.GetItemdatetime(1,"data_stock")
		ld_progressivo_stock = lds_mov_magazzino.GetItemnumber(1,"prog_stock")
		ls_chiave_old = ls_cod_deposito + "-" + ls_cod_ubicazione + "-" + ls_cod_lotto + "-" + &
		string(ldt_data_stock, "yyyy/mm/dd") + "-" + string(ld_progressivo_stock)
	else	// devo suddividere per deposito
		ls_cod_deposito = lds_mov_magazzino.GetItemstring(1,"cod_deposito")
		ls_chiave_old = ls_cod_deposito
	end if
end if

for ll_cont=1 to ll_num_mov_prod
	
	setnull(ls_cod_deposito)
	setnull(ls_cod_lotto)
	setnull(ls_cod_ubicazione)
	
	ls_cod_tipo_movimento_det = lds_mov_magazzino.getItemString(ll_cont,"cod_tipo_mov_det")
	ls_cod_tipo_movimento = lds_mov_magazzino.getItemString(ll_cont,"cod_tipo_movimento")
	ld_quan_movimento = lds_mov_magazzino.GetItemNumber(ll_cont,"quan_movimento")
	if isnull(ld_quan_movimento) then ld_quan_movimento = 0
	ld_val_movimento = lds_mov_magazzino.GetItemNumber(ll_cont,"val_movimento")
	if isnull(ld_val_movimento) then ld_val_movimento = 0

	if fs_flag_stock <> "N" then	// devo suddividere per stock/deposito
		
		if fs_flag_stock = "S" then	// devo suddividere per stock
			ls_cod_deposito = lds_mov_magazzino.GetItemstring(ll_cont,"cod_deposito")
			if isnull(ls_cod_deposito) or ls_cod_deposito = "" then ls_cod_deposito = "***"
			ls_cod_ubicazione = lds_mov_magazzino.GetItemstring(ll_cont,"cod_ubicazione")
			if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then ls_cod_ubicazione = "**********"
			ls_cod_lotto = lds_mov_magazzino.GetItemstring(ll_cont,"cod_lotto")
			if isnull(ls_cod_lotto) or ls_cod_lotto = "" then ls_cod_lotto = "**********"
			ldt_data_stock = lds_mov_magazzino.GetItemdatetime(ll_cont,"data_stock")
			ld_progressivo_stock = lds_mov_magazzino.GetItemnumber(ll_cont,"prog_stock")
			ls_chiave = ls_cod_deposito + "-" + ls_cod_ubicazione + "-" + ls_cod_lotto + "-" + &
			string(ldt_data_stock, "yyyy/mm/dd") + "-" + string(ld_progressivo_stock)
		else	// devo suddividere per deposito
			ls_cod_deposito = lds_mov_magazzino.GetItemstring(ll_cont,"cod_deposito")
			ls_chiave = ls_cod_deposito
		end if
		
		if ls_chiave_old <> ls_chiave then
			
			ll_num_stock = ll_num_stock + 1
			ls_chiave_1[ll_num_stock] = ls_chiave_old
			ld_giacenza_stock[ll_num_stock] = ld_giacenza_new - ld_giacenza_old + ld_giac_terz
			ld_giacenza_old = ld_giacenza_new
			ls_chiave_old = ls_chiave
			ld_giac_terz = 0
			
			if ld_quant_val_stock[12] <> 0 then
				fd_costo_medio_stock[ll_num_stock] = round(ld_quant_val_stock[13] / ld_quant_val_stock[12], 4)
			else
				fd_costo_medio_stock[ll_num_stock] = 0
			end if
			
			fd_quan_costo_medio_stock[ll_num_stock] = ld_quant_val_stock[12]
			
			for ll_i=1 to 14
				ld_quant_val_stock[ll_i]=0
			next
			
		end if
		
	end if	//fine devo suddividere per stock/deposito
	
	ld_quan_terzista = 0
	
	li_ret_funz = uof_single_mov_mag_decimal(ls_cod_tipo_movimento_det, ld_quan_movimento,ld_val_movimento, ld_quant_val, ld_quant_val_stock, ls_error, ls_cod_tipo_movimento, ld_quan_terzista)
	if li_ret_funz < 0 then 
		fs_error = ls_error
		return -1
	end if
	
	if fs_flag_stock <> "N" then	// devo suddividere per stock/deposito
		if li_ret_funz = 0 then
			ld_giacenza_new = (ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6])
		elseif li_ret_funz = 1 then //terzista: non viene conteggiato nella giacenza generale del prodotto
			ld_giac_terz = ld_giac_terz + ld_quan_terzista
		end if
	end if
	
next


// MEMORIZZO l'ultimo vettore in chiave e restituisce argomenti

if (fs_flag_stock <> "N") then
	
	ll_num_stock = ll_num_stock + 1
	ls_chiave_1[ll_num_stock] = ls_chiave_old
	ld_giacenza_stock[ll_num_stock] = ld_giacenza_new - ld_giacenza_old + ld_giac_terz
	fs_chiave = ls_chiave_1
	fd_giacenza_stock = ld_giacenza_stock
	
	if ld_quant_val_stock[12] <> 0 then
		fd_costo_medio_stock[ll_num_stock] = round(ld_quant_val_stock[13] / ld_quant_val_stock[12], 4)
	else
		fd_costo_medio_stock[ll_num_stock] = 0
	end if
	
	fd_quan_costo_medio_stock[ll_num_stock] = ld_quant_val_stock[12]
	
end if
	

// ARROTONDO ALLA QUARTA CIFRA DOPO LA VIRGOLA

for ll_cont = 1 to 14
	fd_quant_val[ll_cont]= round(ld_quant_val[ll_cont], 4) 
next

destroy lds_mov_magazzino
return 0

end function

public function integer uof_saldo_da_chiusura_a_data (string fs_cod_prodotto, datetime fdt_data_fine, ref decimal fd_saldo_al, ref string fs_errore);// *** Michela 10/01/2006: nuova funzione che mi calcola il saldo (fra movimenti di scarico e movimenti di carico) 
//                         di un determinato prodotto, dall'ultima chiusura ad una data passata. Attenzione, escludo
//                         i movimenti con flag_storico a S

string    ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_comodo_2, ls_cod_deposito, ls_flag_classe, ls_flag_valorizza, ls_cod_prodotto_mov, &
          ls_des_movimento, ls_null, ls_cod_tipo_movimento
datetime  ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_chiusura_periodica, ldt_data_chiusura_annuale
datastore lds_movimenti
long      ll_return, ll_i, ll_j, ll_tipo_operazione
dec{4}    ld_quantita, ld_carico_tot, ld_scarico_tot, ld_saldo_tot
boolean   lb_1, lb_2, lb_3

setnull(ls_null)

// prendo le date di ultima chiusura

select data_chiusura_periodica,
       data_chiusura_annuale
into   :ldt_data_chiusura_periodica,
       :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;


lds_movimenti = create datastore
lds_movimenti.dataobject = "d_ds_report_scheda_magazzino_mov"
lds_movimenti.settransobject( sqlca)

ll_return = lds_movimenti.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, ldt_data_chiusura_annuale, fdt_data_fine, ls_null)

if ll_return < 0 then
	fs_errore = "Errore durante la ricerca dei movimenti di magazzino: " + sqlca.sqlerrtext
	destroy lds_movimenti	
	return -1
end if
	
ld_carico_tot = 0
ld_scarico_tot = 0
ld_saldo_tot = 0
	
for ll_j = 1 to lds_movimenti.rowcount()
		
	ldt_data_registrazione = lds_movimenti.getitemdatetime( ll_j, "data_registrazione")
	ls_cod_tipo_movimento = lds_movimenti.getitemstring( ll_j, "cod_tipo_mov_det")
	ld_quantita = lds_movimenti.getitemnumber( ll_j, "quantita")
		
	uof_tipo_movimento(ls_cod_tipo_movimento, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
	
	ld_carico_tot = 0
	ld_scarico_tot = 0
		
	choose case ll_tipo_operazione
		case 1,5,19,13  		// movimenti di carico
			ld_carico_tot = ld_quantita
		case 2,6,20,14	 		// rettifiche carico
			ld_scarico_tot = ld_quantita
		case 3,7	 				// movimenti si scarico
			ld_scarico_tot = ld_quantita
		case 4,8	 				// rettifiche scarico
			ld_carico_tot = ld_quantita
		case else  // 9,15,10,16,11,17,12,18			
	end choose		
	ld_saldo_tot = ld_saldo_tot + ld_carico_tot - ld_scarico_tot
		
next
	
destroy lds_movimenti
fd_saldo_al = ld_saldo_tot
fs_errore = ""
rollback;
return 0
end function

public function integer uof_disimpegna_mp_commessa (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore);/*					FUNZIONE DI IMPEGNO/DISIMPEGNO MATERIE PRIME A MAGAZZINO

	redatta da: Enrico Menegotto
	data redazione: 16/11/2006

 valori restituiti dalla funzione:  		 0 = tutto OK
   													-1 = si è verificato un errore

 variabili in ingresso:			tipo di dato		commento
 --------------------------------------------------------------------------------
		fl_anno_commessa			long					anno commessa produzione
		fl_num_commessa			long					num  commessa produzione

 variabili in uscita: 			tipo di dato		commento
 ---------------------------------------------------------------------------------
		fs_errore					long					eventuale errore

*/
boolean fb_muovi_sl=false
string  ls_errore,ls_test,ls_messaggio,ls_cod_tipo_commessa,ls_flag_muovi_sl,ls_cod_prodotto_raggruppato, ls_cod_prodotto, ls_cod_versione, ls_null
integer li_risposta, li_cont
long ll_lead_time_cumulato
date ld_data
datetime ldt_data_fabbisogno,ldt_data_consegna, ldt_null
dec{4}   ld_quan_disimpegno,ld_quan_prodotta, ld_quan_in_produzione,ld_quan_impegnata_attuale, ld_quan_raggruppo
uo_mrp luo_mrp
s_fabbisogno_commessa lstr_fabbisogno[]


setnull(ls_null)
setnull(ldt_null)


// calcolo data fabbisogno
select cod_tipo_commessa, 
		 data_consegna, 
		 cod_prodotto, 
		 cod_versione,
		 quan_prodotta,
		 quan_in_produzione
into   :ls_cod_tipo_commessa, 
 		 :ldt_data_consegna, 
		 :ls_cod_prodotto, 
		 :ls_cod_versione,
		 :ld_quan_prodotta,
		 :ld_quan_in_produzione
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa = :fl_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca data consegna nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa) + ".~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ldt_data_consegna) then
	fs_errore = "Data consegna non impostata nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa)
	return -1
end if

if isnull(ls_cod_tipo_commessa) then
	fs_errore = "Tipo commessa non impostata nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa)
	return -1
end if


ld_quan_disimpegno = ld_quan_in_produzione - ld_quan_prodotta


select flag_muovi_sl
into   :ls_flag_muovi_sl  
from   tab_tipi_commessa
where  cod_azienda       = :s_cs_xx.cod_azienda and
       cod_tipo_commessa = :ls_cod_tipo_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipo commessa " + ls_cod_tipo_commessa + ".~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_flag_muovi_sl) then
	fs_errore = "Flag muovi Semilavorati non impostata nel tipo commessa " + ls_cod_tipo_commessa
	return -1
end if

if ls_flag_muovi_sl = "S" then fb_muovi_sl=true

// ------------  carico la lista delle materie prime -------------------------------------

luo_mrp = create uo_mrp

luo_mrp.uof_calcolo_fabbisogni_comm(fb_muovi_sl, &
												"varianti_commesse", &
												fl_anno_commessa, &
												fl_num_commessa, &
												0, &
												ls_cod_prodotto, &
												ls_cod_versione, &
												ld_quan_disimpegno, &
												0, &
												ls_null, &
												ldt_null, &
												ref ll_lead_time_cumulato, &
												ref lstr_fabbisogno[], &
												ref ls_messaggio )		

if li_risposta = -1 then
	fs_errore = ls_messaggio
	destroy uo_mrp
	return -1
end if

destroy uo_mrp

// ------------------------  procedo con disimpegno delle materie prime -------------------

for li_cont = 1 to upperbound(lstr_fabbisogno)
	
	update anag_prodotti
	set    quan_impegnata = quan_impegnata - :lstr_fabbisogno[li_cont].quan_fabbisogno
	where  cod_azienda  = :s_cs_xx.cod_azienda   and
			 cod_prodotto = :lstr_fabbisogno[li_cont].cod_prodotto;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in UPDATE anagrafica prodotti prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+"~r~n"+sqlca.sqlerrtext
		return -1
	end if

	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :lstr_fabbisogno[li_cont].cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, lstr_fabbisogno[li_cont].quan_fabbisogno, "M")

		update anag_prodotti
		set    quan_impegnata = quan_impegnata - :ld_quan_raggruppo
		where  cod_azienda  = :s_cs_xx.cod_azienda   and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in UPDATE anagrafica prodotti prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+"~r~n"+sqlca.sqlerrtext
			return -1
		end if
		
	end if
	
	select quan_impegnata_attuale
	into   :ld_quan_impegnata_attuale
	from   impegno_mat_prime_commessa
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :fl_anno_commessa and
			 num_commessa  = :fl_num_commessa and
			 cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;
			 
	// SE sqlcode = 100 non fa niente, potrei averlo cancellato in un passaggio precedente
	if sqlca.sqlcode = 0 then
		
		if ld_quan_impegnata_attuale >= lstr_fabbisogno[li_cont].quan_fabbisogno then

			update impegno_mat_prime_commessa
			set    quan_impegnata_attuale = quan_impegnata_attuale - :lstr_fabbisogno[li_cont].quan_fabbisogno
			where  cod_azienda  = :s_cs_xx.cod_azienda and
					anno_commessa = :fl_anno_commessa and
					num_commessa  = :fl_num_commessa and
					cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;
		
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in UPDATE su tabella impegno_mat_prime_commessa~r~n "+sqlca.sqlerrtext
				return -1
			end if
			
		else
			
			delete impegno_mat_prime_commessa
			where  cod_azienda  = :s_cs_xx.cod_azienda and
					anno_commessa = :fl_anno_commessa and
					num_commessa  = :fl_num_commessa and
					cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in UPDATE su tabella impegno_mat_prime_commessa~r~n "+sqlca.sqlerrtext
				return -1
			end if
			
		end if			
	end if
	
next

return 0
end function

public function integer uof_impegna_mp_commessa (boolean fb_verifica_fasi_avanzamento, boolean fb_pos, boolean fb_impegna_prodotto_finito, string fs_cod_prodotto_finito, string fs_cod_versione, decimal fd_quan_prodotto_finito, long fl_anno_commessa, long fl_num_commessa, ref string fs_errore);/*					FUNZIONE DI IMPEGNO/DISIMPEGNO MATERIE PRIME A MAGAZZINO

	redatta da: Enrico Menegotto
	data redazione: 16/11/2006

 valori restituiti dalla funzione:  		 0 = tutto OK
   													-1 = si è verificato un errore

 variabili in ingresso:			tipo di dato		commento
 --------------------------------------------------------------------------------
		fb_pos						boolean				TRUE = impegno
																FALSE= disimpegno
		fb_impegna_prodotto_finito boolean			TRUE = impegna anche il prodotto finito
																FALSE = non impegna il prodotto finito
		fs_cod_prodotto_finito	string				prodotto finito
		fs_cod_versione			string				versione prodotto finito
		fd_quan_prodotto_finito decimal				quantità prodotto finito (moltiplicatore per MP)
		fl_anno_commessa			long					anno commessa produzione
		fl_num_commessa			long					num  commessa produzione

 variabili in uscita: 			tipo di dato		commento
 ---------------------------------------------------------------------------------
		fl_anno_registrazione		long					chiave registrazione movimento
		fl_num_registrazione			long

*/
boolean		fb_muovi_sl=false
string			ls_errore,ls_test, ls_messaggio, ls_cod_tipo_commessa, ls_flag_muovi_sl, ls_cod_prodotto_raggruppato, ls_null, &
				ls_cod_reparto_old, ls_cod_reparto, ls_cod_deposito, ls_cod_deposito_old, ls_reparti_sl[], ls_reparto_iniziale, ls_flag_stampa_bcl, ls_temp, ls_reparto_sfuso
integer		li_risposta, li_cont, li_anno_reg_ord_ven
long			ll_lead_time_cumulato, ll_num_reg_ord_ven, ll_prog_riga_ord_ven
date			ld_data
datetime		ldt_data_fabbisogno, ldt_data_consegna, ldt_null
decimal		ld_quan_raggruppo

uo_mrp 	luo_mrp

s_fabbisogno_commessa lstr_fabbisogno[]
setnull(ls_null)
setnull(ldt_null)
setnull(ls_cod_reparto_old)
setnull(ls_cod_reparto)
setnull(ls_cod_deposito)
setnull(ls_cod_deposito_old)


// calcolo data fabbisogno
select cod_tipo_commessa, data_consegna
into   :ls_cod_tipo_commessa, :ldt_data_consegna
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :fl_anno_commessa and
		 num_commessa = :fl_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca data consegna nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa) + ".~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ldt_data_consegna) then
	fs_errore = "Data consegna non impostata nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa)
	return -1
end if

if isnull(ls_cod_tipo_commessa) then
	fs_errore = "Tipo commessa non impostata nella commessa " + string(fl_anno_commessa) + "/" + string(fl_num_commessa)
	return -1
end if

select flag_muovi_sl
into   :ls_flag_muovi_sl  
from   tab_tipi_commessa
where  cod_azienda       = :s_cs_xx.cod_azienda and
       cod_tipo_commessa = :ls_cod_tipo_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipo commessa " + ls_cod_tipo_commessa + ".~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_flag_muovi_sl) then
	fs_errore = "Flag muovi Semilavorati non impostata nel tipo commessa " + ls_cod_tipo_commessa
	return -1
end if

if ls_flag_muovi_sl = "S" then fb_muovi_sl=true

// ------------  carico la lista delle materie prime -------------------------------------

luo_mrp = create uo_mrp

luo_mrp.ib_verifica_avanz_fasi = fb_verifica_fasi_avanzamento

//**********************************************************************************
//se la commessa è da ordine di vendita memorizzo in variabili di istanza dell'oggetto la riga ordine di vendita

setnull(ls_reparto_iniziale)
setnull(ls_reparto_sfuso)

select det_ord_ven.anno_registrazione,det_ord_ven.num_registrazione,det_ord_ven.prog_riga_ord_ven,tab_tipi_ord_ven.flag_tipo_bcl
into :li_anno_reg_ord_ven, :ll_num_reg_ord_ven, :ll_prog_riga_ord_ven, :ls_flag_stampa_bcl
from det_ord_ven
join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
							tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
							tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
join tab_tipi_ord_ven on 	tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and
								tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven
where	det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			det_ord_ven.anno_commessa=:fl_anno_commessa and
			det_ord_ven.num_commessa=:fl_num_commessa;
			
if sqlca.sqlcode=0 and li_anno_reg_ord_ven>0 then
	
	choose case ls_flag_stampa_bcl
		case "A","C"
			luo_mrp.uof_set_riga_ord_ven(li_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_riga_ord_ven)
			
			//se il prodotto finito della riga ha una fase (ad esempio per gibus sarebbe la vendita del semilavorato)
			//imponi la condizione inizile del reparto nell'oggetto uo_mrp per la funzione "uof_calcolo_fabbisogni_comm"
			uof_reparti_sl(li_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_riga_ord_ven, ls_reparti_sl[])
			
			if upperbound(ls_reparti_sl[])>0 then
				//ai fini dell'impegnato considero solo il primo reparto di una stessa fase
				ls_reparto_iniziale = ls_reparti_sl[1]
				if ls_reparto_iniziale="" then setnull(ls_reparto_iniziale)
			else
				setnull(ls_reparto_iniziale)
			end if
			
			
		case "B"
			if f_reparti_stabilimenti(li_anno_reg_ord_ven, ll_num_reg_ord_ven, fs_cod_prodotto_finito, "", ls_temp, "", "", ls_reparti_sl[], ls_temp)<0 then
				setnull(ls_reparto_sfuso)
			else
				if upperbound(ls_reparti_sl[])>0 then
					//ai fini dell'impegnato considero solo il primo reparto di una stessa fase
					ls_reparto_sfuso = ls_reparti_sl[1]
					if ls_reparto_sfuso="" then setnull(ls_reparto_sfuso)
				else
					setnull(ls_reparto_sfuso)
				end if
			end if
	end choose
end if
//**********************************************************************************
			
luo_mrp.uof_calcolo_fabbisogni_comm(fb_muovi_sl, &
												"varianti_commesse", &
												fl_anno_commessa, &
												fl_num_commessa, &
												0, &
												fs_cod_prodotto_finito, &
												fs_cod_versione, &
												fd_quan_prodotto_finito, &
												0, &
												ls_reparto_iniziale, &
												ldt_null, &
												ref ll_lead_time_cumulato, &
												ref lstr_fabbisogno[], &
												ref ls_messaggio )

if li_risposta = -1 then
	fs_errore = ls_messaggio
	destroy uo_mrp
	return -1
end if

destroy uo_mrp

// --------------- procedo con impegno/disimpegno del prodotto finito --------------------
if fb_pos then // Impegno il Prodotto

	// EnMe 10/04/2008 effettuata correzione; in passato effettuava l'impegno del prodotto finito.
	// ora incrementa il nuovo campo quan_in_produzione
	update anag_prodotti
	set    quan_in_produzione = quan_in_produzione + :fd_quan_prodotto_finito
	where  cod_azienda    = :s_cs_xx.cod_azienda   and
			 cod_prodotto   = :fs_cod_prodotto_finito;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in aggiornamento quan_in_produzione del prodotto finito in anagrafica prodotti~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :fs_cod_prodotto_finito;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(fs_cod_prodotto_finito, fd_quan_prodotto_finito, "M")
		
		update anag_prodotti
		set    quan_in_produzione = quan_in_produzione + :ld_quan_raggruppo
		where  cod_azienda    = :s_cs_xx.cod_azienda   and
				 cod_prodotto   = :ls_cod_prodotto_raggruppato;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in aggiornamento impegnato del prodotto raggruppato in anagrafica prodotti~r~n"+sqlca.sqlerrtext
			return -1
		end if
	end if
	
else
	
	// EnMe 10/04/2008 effettuata correzione; in passato effettuava l'impegno del prodotto finito.
	// ora incrementa il nuovo campo quan_in_produzione
	update anag_prodotti
	set    quan_in_produzione = quan_in_produzione - :fd_quan_prodotto_finito
	where  cod_azienda  = :s_cs_xx.cod_azienda   and
			 cod_prodotto = :fs_cod_prodotto_finito;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in aggiornamento quan_in_produzione del prodotto finito in anagrafica prodotti~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :fs_cod_prodotto_finito;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(fs_cod_prodotto_finito, fd_quan_prodotto_finito, "M")

		update anag_prodotti
		set    quan_in_produzione = quan_in_produzione - :ld_quan_raggruppo
		where  cod_azienda  = :s_cs_xx.cod_azienda   and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in aggiornamento impegnato del prodotto raggruppato in anagrafica prodotti~r~n"+sqlca.sqlerrtext
			return -1
		end if
	end if
end if

// ------------------------  procedo con impegno delle materie prime eventuali -------------------

for li_cont = 1 to upperbound(lstr_fabbisogno)
	
	if fb_pos then // Impegno il Prodotto
	
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :lstr_fabbisogno[li_cont].quan_fabbisogno
		where  cod_azienda    = :s_cs_xx.cod_azienda   and
			    cod_prodotto   = :lstr_fabbisogno[li_cont].cod_prodotto;
	
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in UPDATE anagrafica prodotti prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+"~r~n"+sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :lstr_fabbisogno[li_cont].cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(lstr_fabbisogno[li_cont].cod_prodotto, lstr_fabbisogno[li_cont].quan_fabbisogno, "M")

			update anag_prodotti
			set    quan_impegnata = quan_impegnata + :ld_quan_raggruppo
			where  cod_azienda    = :s_cs_xx.cod_azienda   and
					 cod_prodotto   = :ls_cod_prodotto_raggruppato;
		
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in UPDATE anagrafica prodotti prodotto "+ls_cod_prodotto_raggruppato+"~r~n"+sqlca.sqlerrtext
				return -1
			end if
		end if
		
		
		//**************************************************
		//inizio insert/update su tabella impegno_mat_prime_commessa
		//**************************************************

		setnull(ls_test)
		
		select cod_azienda
		into   :ls_test
		from   impegno_mat_prime_commessa
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :fl_anno_commessa and
				 num_commessa  = :fl_num_commessa and
				 cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca su tabella impegno_mat_prime_commessa~r~n "+sqlca.sqlerrtext
			return -1
		end if
		
		//-------------------------------------------------------------------------------------------------------------------
		//08/09/2014 SR Impegnato_commessa
		//memorizzazione reparto e deposito impegnato
		
		if ls_flag_stampa_bcl="B" then
			ls_cod_reparto = ls_reparto_sfuso
			
			if not isnull( ls_cod_reparto) then
				select cod_deposito
				into :ls_cod_deposito
				from anag_reparti
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_reparto=:ls_cod_reparto;
							
				if ls_cod_deposito="" then setnull(ls_cod_deposito)
				
			else
				setnull(ls_cod_deposito)
			end if
			
		elseif ls_flag_stampa_bcl="A" or ls_flag_stampa_bcl="C" then
			//rileggo i valori precedenti
			ls_cod_reparto = ls_cod_reparto_old
			ls_cod_deposito = ls_cod_deposito_old
			
			//recupero il deposito del reparto, se c'è
			if lstr_fabbisogno[li_cont].cod_reparto="" or isnull(lstr_fabbisogno[li_cont].cod_reparto) then
				//non c'è il reparto, metto le variabili a NULL
				ls_cod_reparto = ls_null
				ls_cod_deposito = ls_null
			else
				//c'è il reparto specificato
				//se il reparto è lo stesso di prima allora anche il deposito lo sarà, inutile fare la select
				if lstr_fabbisogno[li_cont].cod_reparto<>ls_cod_reparto_old or isnull(ls_cod_reparto_old) then
					select cod_deposito
					into :ls_cod_deposito
					from anag_reparti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_reparto=:lstr_fabbisogno[li_cont].cod_reparto;
					
					if ls_cod_deposito="" then setnull(ls_cod_deposito)
					ls_cod_reparto = lstr_fabbisogno[li_cont].cod_reparto
				end if
			end if
		
			//salvo il valore per la prossima iterata
			ls_cod_reparto_old = ls_cod_reparto
			ls_cod_deposito_old = ls_cod_deposito
		end if
		//-------------------------------------------------------------------------------------------------------------------
	
		
		if isnull(ls_test) then
			ld_data = date(ldt_data_consegna)
			ld_data = relativedate(ld_data, lstr_fabbisogno[li_cont].lead_time * -1)
			ldt_data_fabbisogno = datetime(ld_data, 00:00:00)
			
			insert into impegno_mat_prime_commessa  
				(cod_azienda,   
				anno_commessa,   
				num_commessa,   
				cod_prodotto,   
				quan_impegnata_iniziale,   
				quan_impegnata_attuale,
				lead_time,
				data_fabbisogno,
				cod_reparto,
				cod_deposito)  
			values
				(:s_cs_xx.cod_azienda,   
				:fl_anno_commessa,   
				:fl_num_commessa,   
				:lstr_fabbisogno[li_cont].cod_prodotto,   
				:lstr_fabbisogno[li_cont].quan_fabbisogno,   
				:lstr_fabbisogno[li_cont].quan_fabbisogno,
				:lstr_fabbisogno[li_cont].lead_time,
				:ldt_data_fabbisogno,
				:ls_cod_reparto,
				:ls_cod_deposito);
				
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in INSERT impegno_mat_prime_commessa (prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+") : "+sqlca.sqlerrtext
				return -1
			end if
			
		else
			
			update impegno_mat_prime_commessa
			set    quan_impegnata_iniziale = quan_impegnata_iniziale + :lstr_fabbisogno[li_cont].quan_fabbisogno,
				    quan_impegnata_attuale  = quan_impegnata_attuale  + :lstr_fabbisogno[li_cont].quan_fabbisogno
			where  cod_azienda   = :s_cs_xx.cod_azienda and
					 anno_commessa = :fl_anno_commessa and
					 num_commessa  = :fl_num_commessa and
					 cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;
					
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in UPDATE impegno_mat_prime_commessa (prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+") : "+sqlca.sqlerrtext
				return -1
			end if
			
		end if
//**************************************************************
//fine insert/update su tabella impegno_mat_prime_commessa
//**************************************************************
		
	else // DisImpegno il Prodotto
		
		update anag_prodotti
		set    quan_impegnata = quan_impegnata - :lstr_fabbisogno[li_cont].quan_fabbisogno
		where  cod_azienda  = :s_cs_xx.cod_azienda   and
				 cod_prodotto = :lstr_fabbisogno[li_cont].cod_prodotto;

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in UPDATE anagrafica prodotti (prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+") :"+sqlca.sqlerrtext
			return -1
		end if

		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :lstr_fabbisogno[li_cont].cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then

			ld_quan_raggruppo = f_converti_qta_raggruppo(lstr_fabbisogno[li_cont].cod_prodotto, lstr_fabbisogno[li_cont].quan_fabbisogno, "M")

			update anag_prodotti
			set    quan_impegnata = quan_impegnata - :ld_quan_raggruppo
			where  cod_azienda  = :s_cs_xx.cod_azienda   and
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
	
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in UPDATE anagrafica prodotti (prodotto raggruppato "+ls_cod_prodotto_raggruppato+") :"+sqlca.sqlerrtext
				return -1
			end if
			
		end if


//***************************************************************
//ELIMINAZIONE RECORDS IN TABELLA impegno_mat_prime_commessa
//***************************************************************
		delete impegno_mat_prime_commessa
		where  cod_azienda  = :s_cs_xx.cod_azienda and
				anno_commessa = :fl_anno_commessa and
				num_commessa  = :fl_num_commessa and
				cod_prodotto  = :lstr_fabbisogno[li_cont].cod_prodotto;

		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in DELETE impegno_mat_prime_commessa (prodotto "+lstr_fabbisogno[li_cont].cod_prodotto+") : "+sqlca.sqlerrtext
			return -1
		end if
	end if
	
next

return 0
end function

public function integer uof_impegna_mp_commessa (boolean fb_pos, boolean fb_impegna_prodotto_finito, string fs_cod_prodotto_finito, string fs_cod_versione, decimal fd_quan_prodotto_finito, long fl_anno_commessa, long fl_num_commessa, ref string fs_errore);return uof_impegna_mp_commessa(false, fb_pos, fb_impegna_prodotto_finito,fs_cod_prodotto_finito, fs_cod_versione, fd_quan_prodotto_finito, fl_anno_commessa, fl_num_commessa, ref fs_errore)
end function

public function integer uof_saldo_prod_date_decimal_comm (string fs_cod_prodotto, datetime fdt_data_rif, string fs_where, ref decimal fd_quant_val[], ref string fs_error, string fs_flag_stock, ref string fs_chiave[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_stock[], ref decimal fd_quan_costo_medio_stock[]);// f_saldo_prod_data(fs_cod_prodotto, fdt_data_rif, fs_where, by reference fd_quant_val[], 
// by reference fs_error, fs_flag_stock, by reference fs_chiave[], by reference fd_giacenza_stock[])
// calcola i saldi del prodotto indicato in fs_cod_prodotto alla data fdt_data_rif
// metodo utilizzato: calcola da mov_magazzino le  quantita, da data_chiusura_annuale fino data_rif
// ritorna -1 se errore, 0 se corretto
// la data_rif deve essere successiva all'ultima chiusura annuale
// il parametro fs_where è una stringa che serve per filtrare i movimenti magazzino
// per es. in base al flag_storico: vengono allora calcolati le quantità alla data
// indicata basandosi solo su movimenti ( dopo tale data) che devono subire la chiusura periodica
// l'argomento fs_where vale " and flag_storico='N' "
// le quantità sono tornate in fd_quant_val[]

// fd_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
// [12]=qta_costo_medio, [13]=val_costo_medio, [14]=val_costo_ultimo

// costo medio = [13]/[12]; [12] e [13]: mov_magazzino con flag_costo_ultimo=S
// costo ultimo: ultimo costo di acquisto alla data di riferimento
// fs_flag_stock = N: non suddivide per stock/deposito; 
// fs_flag_stock = S: suddivide per stock, e scrive risultati parziali in fs_chiave[], fd_giacenza_stock[]
// fs_flag_stock = D: suddivide per Deposito, e scrive risultati parziali fs_chiave[], fd_giacenza_stock[]
// fs_chiave: vale cod_deposito oppure stock a seconda fs_flag_stock


// 14-06/2004 Enrico
// Aggiunto argomento fd_valore_stock per ottenere il valore dello stock utile al calcolo del costo medio del singolo stock

datastore lds_mov_magazzino

string ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_chiave, &
		 ls_chiave_old, 	ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto , &
		 ls_mod_string, ls_original_select , ls_rc, ls_chiave_1[]
long ll_num_mov_prod, ll_cont, ll_num_stock, ll_file, ll_i
integer li_ret_funz
datetime ldt_data_chiusura_annuale, ldt_data_stock
dec{4} ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, &
		 ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
		 ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, ld_val_quan_venduta, &
		 ld_saldo_quan_anno_prec, ld_quant_val[], ld_giacenza_new,  ld_giacenza_old, &
		 ld_quan_movimento, ld_val_movimento, ld_progressivo_stock, ld_quan_terzista, ld_giacenza_stock[], &
		 ld_giac_terz, ld_quant_val_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if fdt_data_rif < ldt_data_chiusura_annuale then
	fs_error="Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale"
	return -1
end if

ll_num_stock = 0

lds_mov_magazzino= create datastore
lds_mov_magazzino.DataObject = 'd_ds_saldo_mag_3'
lds_mov_magazzino.SetTransObject (sqlca)


ls_original_select = lds_mov_magazzino.Describe("DataWindow.Table.Select")
ls_mod_string = "DataWindow.Table.Select='"  &
	+ ls_original_select + fs_where + "'"

ls_rc = lds_mov_magazzino.Modify(ls_mod_string)
IF ls_rc = "" THEN
	lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fdt_data_rif, ldt_data_chiusura_annuale,"N")
ELSE
	fs_error = "Errore in operazione Modify " + ls_rc
END IF

//lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fdt_data_rif, ldt_data_chiusura_annuale)

if (fs_flag_stock = "N") then
	lds_mov_magazzino.setsort("anno_registrazione a, num_registrazione a")
	lds_mov_magazzino.sort()
else
	lds_mov_magazzino.setsort("cod_deposito a, cod_ubicazione a, cod_lotto a, data_stock a, prog_stock a")
	lds_mov_magazzino.sort()
end if

ll_num_mov_prod = lds_mov_magazzino.rowcount()  // numero di movimenti selezionati dopo data_rif

for ll_i=1 to 14
	ld_quant_val[ll_i] = 0
	ld_quant_val_stock[ll_i] = 0
next

fd_costo_medio_stock = ld_costo_medio_stock
fd_quan_costo_medio_stock = ld_quan_costo_medio_stock
ld_giacenza_old = 0
ld_giac_terz = 0
ld_giacenza_new = 0

if (fs_flag_stock <> "N") and (ll_num_mov_prod > 0) then	// devo suddividere per stock/deposito: inizializzo ls_chiave_old
	if fs_flag_stock = "S" then	// devo suddividere per stock
		ls_cod_deposito = lds_mov_magazzino.GetItemstring(1,"cod_deposito")
		if isnull(ls_cod_deposito) or ls_cod_deposito = "" then ls_cod_deposito = "***"
		ls_cod_ubicazione = lds_mov_magazzino.GetItemstring(1,"cod_ubicazione")
		if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then ls_cod_ubicazione = "**********"
		ls_cod_lotto = lds_mov_magazzino.GetItemstring(1,"cod_lotto")
		if isnull(ls_cod_lotto) or ls_cod_lotto = "" then ls_cod_lotto = "**********"
		ldt_data_stock = lds_mov_magazzino.GetItemdatetime(1,"data_stock")
		ld_progressivo_stock = lds_mov_magazzino.GetItemnumber(1,"prog_stock")
		ls_chiave_old = ls_cod_deposito + "-" + ls_cod_ubicazione + "-" + ls_cod_lotto + "-" + &
		string(ldt_data_stock, "yyyy/mm/dd") + "-" + string(ld_progressivo_stock)
	else	// devo suddividere per deposito
		ls_cod_deposito = lds_mov_magazzino.GetItemstring(1,"cod_deposito")
		ls_chiave_old = ls_cod_deposito
	end if
end if

for ll_cont=1 to ll_num_mov_prod
	
	setnull(ls_cod_deposito)
	setnull(ls_cod_lotto)
	setnull(ls_cod_ubicazione)
	
	ls_cod_tipo_movimento_det = lds_mov_magazzino.getItemString(ll_cont,"cod_tipo_mov_det")
	ls_cod_tipo_movimento = lds_mov_magazzino.getItemString(ll_cont,"cod_tipo_movimento")
	ld_quan_movimento = lds_mov_magazzino.GetItemNumber(ll_cont,"quan_movimento")
	ld_val_movimento = lds_mov_magazzino.GetItemNumber(ll_cont,"val_movimento")

	if fs_flag_stock <> "N" then	// devo suddividere per stock/deposito
		
		if fs_flag_stock = "S" then	// devo suddividere per stock
			ls_cod_deposito = lds_mov_magazzino.GetItemstring(ll_cont,"cod_deposito")
			if isnull(ls_cod_deposito) or ls_cod_deposito = "" then ls_cod_deposito = "***"
			ls_cod_ubicazione = lds_mov_magazzino.GetItemstring(ll_cont,"cod_ubicazione")
			if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then ls_cod_ubicazione = "**********"
			ls_cod_lotto = lds_mov_magazzino.GetItemstring(ll_cont,"cod_lotto")
			if isnull(ls_cod_lotto) or ls_cod_lotto = "" then ls_cod_lotto = "**********"
			ldt_data_stock = lds_mov_magazzino.GetItemdatetime(ll_cont,"data_stock")
			ld_progressivo_stock = lds_mov_magazzino.GetItemnumber(ll_cont,"prog_stock")
			ls_chiave = ls_cod_deposito + "-" + ls_cod_ubicazione + "-" + ls_cod_lotto + "-" + &
			string(ldt_data_stock, "yyyy/mm/dd") + "-" + string(ld_progressivo_stock)
		else	// devo suddividere per deposito
			ls_cod_deposito = lds_mov_magazzino.GetItemstring(ll_cont,"cod_deposito")
			ls_chiave = ls_cod_deposito
		end if
		
		if ls_chiave_old <> ls_chiave then
			
			ll_num_stock = ll_num_stock + 1
			ls_chiave_1[ll_num_stock] = ls_chiave_old
			ld_giacenza_stock[ll_num_stock] = ld_giacenza_new - ld_giacenza_old + ld_giac_terz
			ld_giacenza_old = ld_giacenza_new
			ls_chiave_old = ls_chiave
			ld_giac_terz = 0
			
			if ld_quant_val_stock[12] <> 0 then
				fd_costo_medio_stock[ll_num_stock] = round(ld_quant_val_stock[13] / ld_quant_val_stock[12], 4)
			else
				fd_costo_medio_stock[ll_num_stock] = 0
			end if
			
			fd_quan_costo_medio_stock[ll_num_stock] = ld_quant_val_stock[12]
			
			for ll_i=1 to 14
				ld_quant_val_stock[ll_i]=0
			next
			
		end if
		
	end if	//fine devo suddividere per stock/deposito
	
	ld_quan_terzista = 0
	
	li_ret_funz = uof_single_mov_mag_decimal(ls_cod_tipo_movimento_det, ld_quan_movimento,ld_val_movimento, ld_quant_val, ld_quant_val_stock, ls_error, ls_cod_tipo_movimento, ld_quan_terzista)
	if li_ret_funz < 0 then 
		fs_error = ls_error
		return -1
	end if
	
	if fs_flag_stock <> "N" then	// devo suddividere per stock/deposito
		if li_ret_funz = 0 then
			ld_giacenza_new = (ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6])
		elseif li_ret_funz = 1 then //terzista: non viene conteggiato nella giacenza generale del prodotto
			ld_giac_terz = ld_giac_terz + ld_quan_terzista
		end if
	end if
	
next


// MEMORIZZO l'ultimo vettore in chiave e restituisce argomenti

if (fs_flag_stock <> "N") then
	
	ll_num_stock = ll_num_stock + 1
	ls_chiave_1[ll_num_stock] = ls_chiave_old
	ld_giacenza_stock[ll_num_stock] = ld_giacenza_new - ld_giacenza_old + ld_giac_terz
	fs_chiave = ls_chiave_1
	fd_giacenza_stock = ld_giacenza_stock
	
	if ld_quant_val_stock[12] <> 0 then
		fd_costo_medio_stock[ll_num_stock] = round(ld_quant_val_stock[13] / ld_quant_val_stock[12], 4)
	else
		fd_costo_medio_stock[ll_num_stock] = 0
	end if
	
	fd_quan_costo_medio_stock[ll_num_stock] = ld_quant_val_stock[12]
	
end if
	

// ARROTONDO ALLA QUARTA CIFRA DOPO LA VIRGOLA

for ll_cont = 1 to 14
	fd_quant_val[ll_cont]= round(ld_quant_val[ll_cont], 4) 
next

destroy lds_mov_magazzino
return 0

end function

private function integer uof_estrai_prodotti_collegati (string fs_selezione, ref s_prodotti_collegati fstr_prodotti[], ref s_prodotti_collegati fstr_prodotti_raggruppati[], ref string fs_errore);string ls_sql, ls_prodotti_raggruppati, ls_cod_prodotto_selezionato, ls_cod_prodotto_raggruppato, ls_prodotti
long ll_i, ll_y
s_prodotti_collegati lstr_prodotti[]

declare cu_prodotti_selezionati cursor for 
select distinct cod_prodotto, cod_prodotto_raggruppato
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto like :fs_selezione;


declare cu_raggruppati dynamic cursor for sqlsa;

declare cu_prodotti_collegati dynamic cursor for sqlsa;

declare cu_prodotti_principali dynamic cursor for sqlsa;


ll_i = 0
ls_prodotti_raggruppati = ""

open cu_prodotti_selezionati;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN curosore cu_prodotti_selezionati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_prodotti_selezionati into :ls_cod_prodotto_selezionato, :ls_cod_prodotto_raggruppato;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH curosore cu_prodotti_selezionati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	
	ll_i ++
	
	fstr_prodotti[ll_i].cod_prodotto = ls_cod_prodotto_selezionato
	
	if not isnull(ls_cod_prodotto_raggruppato) then
		if len(ls_prodotti_raggruppati) > 0 then ls_prodotti_raggruppati = ls_prodotti_raggruppati + ","
		ls_prodotti_raggruppati += "'" + ls_cod_prodotto_raggruppato + "'"
	end if	
loop

close cu_prodotti_selezionati;


// procedo con passaggio prodotti raggruppati per metterli in ordine e togliere eventuali doppioni

ll_i = 0


if len(ls_prodotti_raggruppati) <= 0 then return 0

ls_sql = " select distinct cod_prodotto " + &
			" from anag_prodotti " + &
			" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + & 
		   " cod_prodotto in ( " + ls_prodotti_raggruppati + ") " + &
			" order by cod_prodotto"



prepare sqlsa from :ls_sql;

open dynamic  cu_raggruppati;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN curosore cu_prodotti_raggruppati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_raggruppati into :ls_cod_prodotto_selezionato;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH curosore cu_prodotti_raggruppati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	
	ll_i ++
	
	fstr_prodotti_raggruppati[ll_i].cod_prodotto = ls_cod_prodotto_selezionato
	
loop

close cu_raggruppati;


ls_prodotti_raggruppati =""

for ll_i = 1 to upperbound( fstr_prodotti_raggruppati )

	if len(ls_prodotti_raggruppati) > 0 then ls_prodotti_raggruppati = ls_prodotti_raggruppati + ","
	ls_prodotti_raggruppati += "'" + fstr_prodotti_raggruppati[ll_i].cod_prodotto + "'"

next


// estraggo i prodotti collegati ai prodotti raggruppati

ll_i = upperbound(fstr_prodotti)

ls_sql = " select distinct cod_prodotto from   anag_prodotti where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" cod_prodotto_raggruppato in (" + ls_prodotti_raggruppati + ") order by cod_prodotto "

prepare sqlsa from :ls_sql;

open dynamic cu_prodotti_collegati;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN curosore cu_prodotti_collegati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_prodotti_collegati into :ls_cod_prodotto_selezionato;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH curosore cu_prodotti_collegati (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	
	ll_i ++
	
	fstr_prodotti[ll_i].cod_prodotto = ls_cod_prodotto_selezionato
	
loop

close cu_prodotti_selezionati;


// rimetto in ordine tutti i codici escludendo eventuali doppioni
ls_prodotti = ""

for ll_i = 1 to upperbound( fstr_prodotti )

	if len(ls_prodotti) > 0 then ls_prodotti = ls_prodotti + ","
	ls_prodotti += "'" + fstr_prodotti[ll_i].cod_prodotto + "'"

next

fstr_prodotti[] = lstr_prodotti[]

ll_i = 0

ls_sql = " select distinct cod_prodotto from   anag_prodotti where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
         " cod_prodotto in (" + ls_prodotti + ") order by cod_prodotto "

prepare sqlsa from :ls_sql;


open dynamic cu_prodotti_principali;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN curosore cu_prodotti_principali (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_prodotti_principali into :ls_cod_prodotto_selezionato;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH curosore cu_prodotti_principali (uof_estrai_prodotti_collegati)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// se si tratta di un prodotto messo nella lista dei raggruppati lo salto
	for ll_y = 1 to upperbound(fstr_prodotti_raggruppati)
		if fstr_prodotti_raggruppati[ll_y].cod_prodotto = ls_cod_prodotto_selezionato then continue
	next
	
	ll_i ++
	
	fstr_prodotti[ll_i].cod_prodotto = ls_cod_prodotto_selezionato
	
loop

close cu_prodotti_selezionati;


return 0
end function

public function integer uof_ricalcola_impegnato (string fs_selezione, ref string fs_errore);string ls_sql, ls_cod_prodotto, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_errore, ls_cod_prodotto_corrente, &
       ls_cod_prodotto_raggruppato
long ll_anno_registrazione, ll_num_registrazione, ll_ret, ll_i
dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_quan_impegnata, ld_quan_impegnata_attuale, ld_quan_raggruppo
s_prodotti_collegati lstr_prodotti[], lstr_prodotti_raggruppati[]

declare cu_dettagli cursor for 
select quan_ordine, 
		 quan_in_evasione, 
		 quan_evasa, 
		 cod_tipo_det_ven
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
       flag_blocco = 'N' and
		 flag_evasione <> 'E' and
		 cod_prodotto = :ls_cod_prodotto_corrente;

declare cu_commesse cursor for 
select quan_impegnata_attuale
from   impegno_mat_prime_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :ls_cod_prodotto_corrente;


if isnull(fs_selezione) or len(fs_selezione) < 1 then fs_selezione = "%"

ll_ret = uof_estrai_prodotti_collegati(fs_selezione, ref lstr_prodotti[], ref lstr_prodotti_raggruppati[], ref ls_errore)

if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if upperbound(lstr_prodotti) < 1 then
	fs_errore = "Nulla da ricalcolare"
	return -1
end if


// Azzero e ricalcolo SOLO i prodotti raggruppati.

for ll_i = 1 to upperbound( lstr_prodotti_raggruppati[] )
	
	ls_cod_prodotto_corrente = lstr_prodotti_raggruppati[ll_i].cod_prodotto

	update anag_prodotti
	set    quan_impegnata = 0 
	where  cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in azzeramento impegnato prodotto " +ls_cod_prodotto_corrente+ "~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if


	open cu_dettagli;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_dettagli prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_dettagli into :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_cod_tipo_det_ven;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_dettagli. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
			
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven ;
				 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca tipi dettagli vendita. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
			
			ld_quan_impegnata  = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
			if ld_quan_impegnata < 0 then ld_quan_impegnata = 0
			
			update anag_prodotti
			set    quan_impegnata = quan_impegnata + :ld_quan_impegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente ;
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento prodotti. ~r~nDettaglio " + sqlca.sqlerrtext
				return -1
			end if
		
		end if
	loop
		
	close cu_dettagli;

	//
	// --------------- calcolo quantità impegnata da commesse
	
	open cu_commesse;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_commesse. ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_commesse into :ld_quan_impegnata_attuale;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH impegnato commesse del prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ld_quan_impegnata_attuale <=0 or isnull(ld_quan_impegnata_attuale) then continue
		
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :ld_quan_impegnata_attuale
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_corrente ;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in aggiornamento impegnato del prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
	loop	
		
	close cu_commesse	;
	
next	


	
// procedo ora con il ricalcolo impegnato per tutti i prodotti che non sono codici di raggruppo.	
	
for ll_i = 1 to upperbound( lstr_prodotti[] )
	
	ls_cod_prodotto_corrente = lstr_prodotti[ll_i].cod_prodotto

	update anag_prodotti
	set    quan_impegnata = 0 
	where  cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in azzeramento impegnato prodotto " +ls_cod_prodotto_corrente+ "~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if


	open cu_dettagli;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_dettagli prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_dettagli into :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_cod_tipo_det_ven;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_dettagli. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
			
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven ;
				 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca tipi dettagli vendita. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_det_ven = "M" then
			
			ld_quan_impegnata  = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
			if ld_quan_impegnata < 0 then ld_quan_impegnata = 0
			
			update anag_prodotti
			set    quan_impegnata = quan_impegnata + :ld_quan_impegnata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente ;
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento prodotti. ~r~nDettaglio " + sqlca.sqlerrtext
				return -1
			end if
			
			
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_corrente, ld_quan_impegnata, "M")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					ls_errore = "Errore in fase di update prodotto raggruppato (1) "+ls_cod_prodotto_raggruppato+". Dettaglio " + sqlca.sqlerrtext
					return -1
				end if
			end if
			
		
		end if
	loop
		
	close cu_dettagli;

	//
	// --------------- calcolo quantità impegnata da commesse
	
	open cu_commesse;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_commesse. ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_commesse into :ld_quan_impegnata_attuale;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH impegnato commesse del prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ld_quan_impegnata_attuale <=0 or isnull(ld_quan_impegnata_attuale) then continue
		
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :ld_quan_impegnata_attuale
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_corrente ;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in aggiornamento impegnato del prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_corrente;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then

			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_corrente, ld_quan_impegnata, "M")

			update anag_prodotti  
				set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;

			if sqlca.sqlcode = -1 then
				ls_errore = "Errore in fase di update prodotto raggruppato (1) "+ls_cod_prodotto_raggruppato+". Dettaglio " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		
	loop	
		
	close cu_commesse	;
	
next	
	
return 0

end function

public function integer uof_ricalcola_ordinato_fornitore (string fs_selezione, ref string fs_errore);string ls_sql, ls_cod_prodotto, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, ls_errore, ls_cod_prodotto_corrente, &
       ls_cod_prodotto_raggruppato, ls_flag_saldo
long ll_anno_registrazione, ll_num_registrazione, ll_ret, ll_i
dec{4} ld_quan_ordine, ld_quan_arrivata, ld_quan_ordinata, ld_quan_ordinata_attuale, ld_quan_raggruppo
s_prodotti_collegati lstr_prodotti[], lstr_prodotti_raggruppati[]

declare cu_dettagli cursor for 
select quan_ordinata,
		 quan_arrivata,
		 flag_saldo,
		 cod_tipo_det_acq
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and 
       flag_blocco = 'N' and
		 cod_prodotto = :ls_cod_prodotto_corrente;


if isnull(fs_selezione) or len(fs_selezione) < 1 then fs_selezione = "%"

ll_ret = uof_estrai_prodotti_collegati(fs_selezione, ref lstr_prodotti[], ref lstr_prodotti_raggruppati[], ref ls_errore)

if ll_ret < 0 then
	fs_errore = ls_errore
	return -1
end if

if upperbound(lstr_prodotti) < 1 then
	fs_errore = "Nulla da ricalcolare"
	return -1
end if


// Azzero e ricalcolo SOLO i prodotti raggruppati.

for ll_i = 1 to upperbound( lstr_prodotti_raggruppati[] )
	
	ls_cod_prodotto_corrente = lstr_prodotti_raggruppati[ll_i].cod_prodotto

	update anag_prodotti
	set    quan_ordinata = 0 
	where  cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in azzeramento ordinato prodotto " +ls_cod_prodotto_corrente+ "~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if


	open cu_dettagli;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_dettagli prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_dettagli into :ld_quan_ordine, :ld_quan_arrivata, :ls_flag_saldo, :ls_cod_tipo_det_acq;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_dettagli. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_saldo = "S" then continue
		
			
		select flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_acq = :ls_cod_tipo_det_acq ;
				 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca tipi dettagli vendita. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_det_acq = "M" then
			
			ld_quan_ordinata  = ld_quan_ordine - ld_quan_arrivata
			
			if ld_quan_ordinata < 0 then ld_quan_ordinata = 0
			
			update anag_prodotti
			set    quan_ordinata = quan_ordinata + :ld_quan_ordinata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente ;
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento prodotti. ~r~nDettaglio " + sqlca.sqlerrtext
				return -1
			end if
		
		end if
	loop
		
	close cu_dettagli;

	
next	


	
// procedo ora con il ricalcolo ordinato per tutti i prodotti che non sono codici di raggruppo.	
	
for ll_i = 1 to upperbound( lstr_prodotti[] )
	
	ls_cod_prodotto_corrente = lstr_prodotti[ll_i].cod_prodotto

	update anag_prodotti
	set    quan_ordinata = 0 
	where  cod_azienda  = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto_corrente;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in azzeramento ordinato prodotto " +ls_cod_prodotto_corrente+ "~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if


	open cu_dettagli;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in OPEN cu_dettagli prodotto "+ls_cod_prodotto_corrente+". ~r~nDettaglio " + sqlca.sqlerrtext
		return -1
	end if
		
	do while true
		fetch cu_dettagli into :ld_quan_ordine, :ld_quan_arrivata, :ls_flag_saldo, :ls_cod_tipo_det_acq;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in FETCH cu_dettagli. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_saldo = "S" then continue
		
			
		select flag_tipo_det_acq
		into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_acq = :ls_cod_tipo_det_acq ;
				 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca tipi dettagli vendita. ~r~nDettaglio " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_det_acq = "M" then
			
			ld_quan_ordinata  = ld_quan_ordine - ld_quan_arrivata
			if ld_quan_ordinata < 0 then ld_quan_ordinata = 0
			
			update anag_prodotti
			set    quan_ordinata = quan_ordinata + :ld_quan_ordinata
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente ;
			if sqlca.sqlcode = -1 then
				fs_errore = "Errore in aggiornamento prodotti. ~r~nDettaglio " + sqlca.sqlerrtext
				return -1
			end if
			
			
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_corrente;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_corrente, ld_quan_ordinata, "M")

				update anag_prodotti  
					set quan_ordinata = quan_ordinata + :ld_quan_raggruppo  
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					ls_errore = "Errore in fase di update prodotto raggruppato (1) "+ls_cod_prodotto_raggruppato+". Dettaglio " + sqlca.sqlerrtext
					return -1
				end if
			end if
			
		
		end if
	loop
		
	close cu_dettagli;

	
next	
	
return 0

end function

public function integer uof_registra_movimento_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, ref string fs_cod_fornitore, ref string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine);
integer		li_ret
string			ls_errore




li_ret = uof_registra_movimento_mag(	fdd_data_registrazione, fs_cod_tipo_movimento, fs_cod_tipo_mov_det, fs_flag_mov_manuale, &
													fs_cod_prodotto, fd_quantita, fd_valore_unitario, fl_prog_mov, fl_num_documento, fdd_data_documento, &
													fs_referenza, fb_primo_movimento, fs_cod_deposito, fs_cod_ubicazione, fs_cod_lotto, fdd_data_stock, fl_prog_stock, &
													fs_cod_fornitore, fs_cod_cliente, fl_anno_registrazione, fl_num_registrazione, al_anno_documento, al_anno_reg_mov_origine, al_num_reg_mov_origine, &
													ls_errore)

if li_ret<0 then
	return -1
end if





////-------------------------------------------------------------------------------------
////	funzione di movimentazione prodotti a magazzino stock prodotti
////
////
////	redatta da: Enrico Menegotto
//// data redazione: 10/6/97
////
//// valori restituiti dalla funzione:  		 0 = tutto OK
////   													-1 = si è verificato un errore
////
//// variabili in ingresso:			tipo di dato		commento
//// ------------------------------------------------------------------------------
////		fdd_data_registrazione		date					data registrazione
////		fs_cod_tipo_movimento		string				tipo movimento magazzino
////		fs_cod_tipo_mov_det			string				tipo configurazione movimento
////		fs_flag_mov_manuale			string				S=movimento manuale N=movimento automatico
////																	 generato da bolle/fatture ecc......
////		fs_cod_prodotto				string					
////		fd_quantita						double				quantità movimento
////		fd_valore_unitario			double				costo dell'unità di movimentazione
////		fl_prog_mov						long					codice identificativo registrazione
////		fl_num_documento				long					riferimento al numero documento
////		fdd_data_documento			date					riferimento alla data documento
////		fs_referenza					string				riferimento libero
////		fb_primo_movimento			boolean				è il primo movimento ??
////		fs_cod_deposito				string				   \
////		fs_cod_ubicazione				string                \ 
////		fs_cod_lotto					string				     >  riferimento stock
////		fdd_data_stock					date					    /
////		fl_prog_stock					long					   /
////		fs_cod_fornitore				string				codice fornitore terzista
////		fs_cod_cliente					string				codice cliente   terzista
////
//// variabili in uscita:				tipo di dato		commento
//// ------------------------------------------------------------------------------
////		fs_cod_deposito				string				   \
////		fs_cod_ubicazione				string                \ 
////		fs_cod_lotto					string				     >  riferimento stock
////		fdd_data_stock					date					    /
////		fl_prog_stock					long					   /
////		fs_cod_fornitore				string				codice fornitore terzista
////		fs_cod_cliente					string				codice cliente terzista
//// 	fl_anno_registrazione		long					registrazione movimento
////		fl_num_registrazione			long
////
////
////
////-------------------------------------------------------------------------------------
//
//fd_quantita=round(fd_quantita,4)
//fd_valore_unitario=round(fd_valore_unitario,4)
//
//if uof_aggiorna_prodotti(fs_cod_tipo_movimento, &
//							  fs_cod_tipo_mov_det, &
//							  fs_cod_prodotto, &
//							  fd_quantita, &
//							  fd_valore_unitario, &
//							  fs_cod_fornitore, &
//							  false)      = -1 then return -1
//
//if  uof_aggiorna_stock(fs_cod_prodotto, &
//							fs_cod_deposito, &
//							fs_cod_ubicazione, &
//							fs_cod_lotto, &
//							fdd_data_stock, &
//							fl_prog_stock, &
//							fs_cod_tipo_movimento, &
//							fs_cod_tipo_mov_det, &
//							fs_cod_fornitore, &
//							fs_cod_cliente, &
//							fd_quantita, &
//							fd_valore_unitario, &
//							fb_primo_movimento,&
//							false, &
//							false)         = -1 then return -1
//
//if uof_aggiorna_mov_magazzino(fdd_data_registrazione, &
//						fs_cod_tipo_movimento, &
//						fs_cod_tipo_mov_det, &
//						fs_flag_mov_manuale, &
//						fs_cod_prodotto, &
//						fs_cod_deposito, &
//						fs_cod_ubicazione, &
//						fs_cod_lotto, &
//						fdd_data_stock, &
//						fl_prog_stock, &
//						fd_quantita, &
//						fd_valore_unitario, &
//						fl_prog_mov, &
//						fl_num_documento, &
//						fdd_data_documento, &
//						fs_referenza, &
//						fb_primo_movimento, &
//						fs_cod_cliente, &
//						fs_cod_fornitore, &
//						false, &
//						fl_anno_registrazione, &
//						fl_num_registrazione,&
//						al_anno_documento,&
//						al_anno_reg_mov_origine,&
//						al_num_reg_mov_origine) = -1 then return -1
//
//return 0
end function

public function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[]);string				ls_errore
integer			li_ret


li_ret = uof_movimenti_mag(	fdd_data_registrazione, fs_cod_tipo_movimento, fs_flag_mov_manuale, fs_cod_prodotto, fd_quantita, fd_valore_unitario, &
										fl_num_documento, fdd_data_documento, fs_referenza, fl_anno_reg_dest_stock, fl_num_reg_dest_stock, &
										fs_cod_deposito[], fs_cod_ubicazione[], fs_cod_lotto[], fdd_data_stock[], fl_prog_stock[], fs_cod_fornitore[], fs_cod_cliente[], &
										fl_anno_registrazione[], fl_num_registrazione[], ls_errore)


if li_ret<0 then
	if not ib_silentmode then
		g_mb.error(ls_errore)
	else
		is_silentmode_error = ls_errore
	end if
	rollback;
	return -1
end if

return 0


end function

public function integer uof_movimenti_mag_ragguppato (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine);string ls_error

return uof_movimenti_mag_ragguppato(fdd_data_registrazione, &
	fs_cod_tipo_movimento, &
	fs_flag_mov_manuale, &
	fs_cod_prodotto, &
	fd_quantita, &
	fd_valore_unitario, &
	fl_num_documento, &
	fdd_data_documento, &
	fs_referenza, &
	fl_anno_reg_dest_stock, &
	fl_num_reg_dest_stock, &
	fs_cod_deposito, &
	fs_cod_ubicazione, &
	fs_cod_lotto, &
	fdd_data_stock, &
	fl_prog_stock, &
	fs_cod_fornitore, &
	fs_cod_cliente, &
	fl_anno_registrazione, &
	fl_num_registrazione, &
	al_anno_documento, &
	al_anno_reg_mov_origine, &
	al_num_reg_mov_origine, &
	ls_error)
end function

public function boolean uof_stampa_report_scarico_mag_for (long al_anno_bolla_acq, long al_num_bolla_acq, ref string as_error);/**
 * stefanop
 * 29/03/2012
 *
 * Stampa la ricevuta di scarico magazzino in conto deposito del fornitore
 **/
 
string ls_null
setnull(ls_null)
 
return uof_stampa_report_scarico_mag_for(al_anno_bolla_acq, al_num_bolla_acq, ls_null, as_error)
end function

public function integer uof_verifica_dest_mov_magazzino (string as_table, long al_anno_documento, long al_num_documento, long al_prog_riga_documento, string as_cod_tipo_movimento, string as_cod_prodotto, string as_cod_deposito[], string as_cod_ubicazione[], string as_cod_lotto[], datetime adt_data_stock[], long al_prog_stock[], string as_cod_cliente[], string as_cod_fornitore[], ref long al_anno_reg_des_mov, ref long al_num_reg_des_mov, ref string as_error);/**
 * stefanop
 * 19/04/2012
 *
 * Verifico se esiste la destinazione di magazzino e nel caso chiedo all'utente se il sistema
 * può provare con la creazione automatica.
 *
 * Ritorna:
 * -1 = errore
 *  0 = tutto ok
 *  1 = L'operatore ha annullato
 **/

string ls_test, ls_query, ls_message, ls_sql, ls_cod_tipo_mov_det, ls_cod_depositi[], ls_cod_deposito_tras
int li_count, ll_i, ll_y
datastore lds_store

// stefanop 06/08/2012: converto in un datastore per avere più informazioni
// -------------------------------------------------------------
//SELECT count(*)
//INTO :li_count
//FROM det_tipi_movimenti 
//WHERE cod_azienda = :s_cs_xx.cod_azienda AND
//		  cod_tipo_movimento = :as_cod_tipo_movimento;
//		  
//if sqlca.sqlcode <> 0 then
//	as_error = "Errore durante il conteggio nel dettaglio tipi movimenti."
//	if sqlca.sqlcode < 0 then as_error += " " + sqlca.sqlerrtext
//	return -1
//end if
// -------------------------------------------------------------

ls_sql = "SELECT cod_tipo_mov_det, cod_deposito FROM det_tipi_movimenti WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
			" AND cod_tipo_movimento = '" + as_cod_tipo_movimento + "' "
			
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for ll_i = 1 to li_count
	
	ls_cod_tipo_mov_det = lds_store.getitemstring(ll_i, 1)
	
	SELECT cod_deposito
	INTO :ls_test
	FROM dest_mov_magazzino  
	WHERE 	cod_azienda = :s_cs_xx.cod_azienda AND  
				anno_registrazione = :al_anno_reg_des_mov AND  
				num_registrazione = :al_num_reg_des_mov AND  
				prog_registrazione = :ll_i;
				
	if sqlca.sqlcode = 0 then
		// E' tutto ok
		continue
	elseif sqlca.sqlcode = 100 then
		// La destinazione stock NON esiste, chiedo all'utente di provare a correggere l'errore creando e 
		// assegnando un nuovo stock.
		ls_message = g_str.format("Ricerca destinazione stock fallita ($5).~r~n- Tipo mov.det: $1~r~n- Rif des.mov $2/$3/$4.", ls_cod_tipo_mov_det, al_anno_reg_des_mov, al_num_reg_des_mov, ll_i, li_count)
		ls_message +="~r~nProvo a correggere automaticamente l'errore?"
		
		//provo a correggere automaticamente sempre
//		if g_mb.confirm(ls_message, 1) = false then
//			// L'utente vuole risolvere da solo
//			return 1
//		end if
		
		// Se arrivo fin qui vuol dire che l'utente vuole correggere automaticamente
		// Pulisco tutto perchè faccio reinserire i dest mov dalla funzione 
		// f_crea_dest_mov_magazzino da 0
		DELETE FROM dest_mov_magazzino
		WHERE 	cod_azienda = :s_cs_xx.cod_azienda AND  
					anno_registrazione = :al_anno_reg_des_mov AND  
					num_registrazione = :al_num_reg_des_mov;
				
		// Se si tratta di una bolla di trasferimento  recuper il deposito trasf
		if as_table = "det_bol_ven" then
			
			select cod_deposito_tras
			into :ls_cod_deposito_tras
			from tes_bol_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :al_anno_documento and
					 num_registrazione = :al_num_documento and
					 cod_tipo_bol_ven in (select cod_tipo_bol_ven from tab_tipi_bol_ven where cod_azienda=:s_cs_xx.cod_azienda and flag_tipo_bol_ven='T');
					 
			if not isnull(ls_cod_deposito_tras) and ls_cod_deposito_tras <> "" then
				as_cod_deposito[2] = ls_cod_deposito_tras
			end if
		end if
		
		// creo eventiali fornitori o clienti in modo da non dare errore
		for ll_y = 1 to upperbound(as_cod_deposito)
			if upperbound(as_cod_fornitore) < ll_y then setnull(as_cod_fornitore[ll_y])
			if upperbound(as_cod_cliente) < ll_y then setnull(as_cod_cliente[ll_y])
			if upperbound(as_cod_ubicazione) < ll_y then setnull(as_cod_ubicazione[ll_y])
			if upperbound(as_cod_lotto) < ll_y then setnull(as_cod_lotto[ll_y])
			if upperbound(adt_data_stock) < ll_y then setnull(adt_data_stock[ll_y])
			if upperbound(al_prog_stock) < ll_y then setnull(al_prog_stock[ll_y])
		next
	
		// creo il nuovo des_mov_magazzino
		if f_crea_dest_mov_magazzino(as_cod_tipo_movimento, as_cod_prodotto, as_cod_deposito, &
										as_cod_ubicazione, as_cod_lotto, adt_data_stock, al_prog_stock, &
										as_cod_cliente, as_cod_fornitore,ref  al_anno_reg_des_mov, &
										ref al_num_reg_des_mov) = -1 then
			return -1
		end if
		
		// aggiorno la riga di dettaglio con il nuovo des_mov_magazzino
		ls_query = "UPDATE " + as_table + &
						" SET anno_reg_des_mov=" + string(al_anno_reg_des_mov) + &
						" , num_reg_des_mov=" + string(al_num_reg_des_mov) + " WHERE "
		
		choose case as_table
			case "det_bol_acq"
				ls_query += " anno_bolla_acq=" + string(al_anno_documento)
				ls_query += " AND num_bolla_acq=" + string(al_num_documento)
				ls_query += " AND prog_riga_bolla_acq=" + string(al_prog_riga_documento)
				
			case "det_bol_ven"
				ls_query += " anno_registrazione=" + string(al_anno_documento)
				ls_query += " AND num_registrazione=" + string(al_num_documento)
				ls_query += " AND prog_riga_bol_ven=" + string(al_prog_riga_documento)
				
			case "det_fat_acq"
				ls_query += " anno_registrazione=" + string(al_anno_documento)
				ls_query += " AND num_registrazione=" + string(al_num_documento)
				ls_query += " AND prog_riga_fat_acq=" + string(al_prog_riga_documento)
				
			case "det_fat_ven"
				ls_query += " anno_registrazione=" + string(al_anno_documento)
				ls_query += " AND num_registrazione=" + string(al_num_documento)
				ls_query += " AND prog_riga_fat_ven=" + string(al_prog_riga_documento)
				
			case else
				as_error = "Il nome della tabella " + as_table + " non è codificato per l'aggiornamento del nuovo stock!"
				return -1
				
		end choose
		
		execute immediate :ls_query;
		
		if sqlca.sqlcode <> 0 then
			as_error = "Errore durante l'aggiornamento del nuovo stock! " + sqlca.sqlerrtext
			return -1
		end if
		
		// Ho sistemato tutto automaticamente esco fuori
		return 0

	elseif sqlca.sqlcode < 0 then
		as_error = g_str.format("Ricerca destinazione stock fallita ($5); Tipo mov.det: $1; Rif des.mov $2/$3/$4.", ls_cod_tipo_mov_det, al_anno_reg_des_mov, al_num_reg_des_mov, ll_i, li_count)
		as_error += "~r~n" + sqlca.sqlerrtext
		return -1
	end if
		
next

return 0
end function

public function integer uof_aggiorna_mov_mag_raggruppato (long al_anno_mov_mag_orig, long al_num_mov_mag_orig, string as_cod_tipo_movimento, string as_cod_tipo_mov_det, string as_cod_prodotto, decimal ad_quantita, decimal ad_valore_unitario, string as_cod_fornitore, boolean ab_storno, ref string as_error);/**
 * stefanop
 * 20/04/2012
 *
 * Quando aggiorno il movimento di magazzino devo controllare se devo modificare
 * anche il movimento di magazzino del prodotto raggruppato eseguendo le 
 * conversioni necessarie
 **/
 
 
string 	ls_cod_prodotto_raggruppato, ls_msg
long 		ll_anno_reg_mov, ll_num_reg_mov


if not ib_mov_raggruppato then
	return 0
end if


// Controllo se il prodotto ha un prodotto raggruppato inserito
select cod_prodotto_raggruppato
into :ls_cod_prodotto_raggruppato
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore durante il controllo del prodotto raggruppato per il prodotto " + as_cod_prodotto
	return -1
elseif isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato = "" then
	return 0
end if

// Applico il fattore di conversione
ad_valore_unitario = f_converti_qta_raggruppo(as_cod_prodotto, ad_valore_unitario, "D")
ad_quantita = f_converti_qta_raggruppo(as_cod_prodotto, ad_quantita, "M")

if uof_aggiorna_prodotti(as_cod_tipo_movimento, as_cod_tipo_mov_det, ls_cod_prodotto_raggruppato, ad_quantita, ad_valore_unitario, as_cod_fornitore, false, as_error) = -1  then
	if as_error<>"" and not isnull(as_error) then
		//in as_error c'è il messaggioio
	else
		as_error = "(GEN.) Errore in Aggiornamento Anagrafica prodotti (prodotto raggruppato). " + sqlca.sqlerrtext
	end if
	//as_error = "Errore in Aggiornamento Anagrafica prodotti (prodotto raggruppato). " + sqlca.sqlerrtext
	return -1				
end if

// Recupero movimento del prodotto raggruppato
select anno_registrazione, num_registrazione
into :ll_anno_reg_mov, :ll_num_reg_mov
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_reg_mov_origine = :al_anno_mov_mag_orig and
		 num_reg_mov_origine = :al_num_mov_mag_orig;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante il controllo del movimento collegato al movimento " + string(al_anno_mov_mag_orig) + "/" + string(al_num_mov_mag_orig)
	as_error += ".~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_anno_reg_mov) or isnull(ll_num_reg_mov) then
	
	// Stefanop 18/05/2012: aggiunto il messaggio di conferma per far continuare la procedura, altimenti il pulsante Agg.Mov in w_det_fat_acq
	// non funziona correttamente quando non c'è il movimento raggruppato
	// ------------------------------------
	//as_error = "Il movimento " + string(al_anno_mov_mag_orig) + "/" + string(al_num_mov_mag_orig) + " non ha nessun movimento collegato!"
	//return -1
	ls_msg =  "Il movimento " + string(al_anno_mov_mag_orig) + "/" + string(al_num_mov_mag_orig) + " non ha nessun movimento collegato!~r~n" + &
				   "CONTINUO LO STESSO?"
	if ib_silentmode then
		return 0
		if g_mb.confirm(ls_msg) then
			return 0
		else
			as_error = "Il movimento " + string(al_anno_mov_mag_orig) + "/" + string(al_num_mov_mag_orig) + " non ha nessun movimento collegato!"
			return -1
		end if
	end if
		// ------------------------------------
end if

update mov_magazzino
set val_movimento = :ad_valore_unitario,
	quan_movimento = :ad_quantita
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg_mov and
		 num_registrazione  = :ll_num_reg_mov;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore in aggiornamento movimento di magazzino ("+string(ll_anno_reg_mov)+ "/" +string(ll_num_reg_mov)+"): " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

private function integer uof_crea_nuovo_stock (string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, readonly string fs_cod_lotto, datetime fdd_data_stock, long fl_prog_stock, string fs_cod_fornitore, string fs_cod_cliente);string ls_error

INSERT INTO stock (
	cod_azienda,   
	cod_prodotto,   
	cod_deposito,   
	cod_ubicazione,   
	cod_lotto,   
	data_stock,   
	prog_stock,   
	giacenza_stock,   
	quan_assegnata,   
	quan_in_spedizione,   
	costo_medio,   
	cod_fornitore,
	cod_cliente
) VALUES (
	:s_cs_xx.cod_azienda,   
	:fs_cod_prodotto,   
	:fs_cod_deposito,   
	:fs_cod_ubicazione,   
	:fs_cod_lotto,   
	:fdd_data_stock,   
	:fl_prog_stock,   
	0,   
	0,   
	0,   
	0,   
	:fs_cod_fornitore,
	:fs_cod_cliente);
	
if sqlca.sqlcode <> 0 then
	// Stefanop: 19/04/2012: migliorato il messaggio di errore
	// --------------------------------------------------------------------------------------
	//g_mb.messagebox("APICE","Errore durante creazione nuovo stock", stopsign!)
	ls_error = "Errore durante creazione nuovo stock~r~nDettagli:~r~n"
	
	if not isnull(fs_cod_prodotto) then ls_error+= "- Prodotto: " + fs_cod_prodotto + "~r~n"
	if not isnull(fs_cod_deposito) then ls_error+= "- Deposito: " + fs_cod_deposito + "~r~n"
	if not isnull(fs_cod_ubicazione) then ls_error+= "- Ubicazione: " + fs_cod_ubicazione + "~r~n"
	if not isnull(fs_cod_lotto) then ls_error+= "- Lotto: " + fs_cod_lotto + "~r~n"
	if not isnull(fdd_data_stock) then ls_error+= "- Data Stock: " + string(fdd_data_stock, "dd/mm/yyyy") + "~r~n"
	if not isnull(fl_prog_stock) then ls_error+= "- Prog. Stock: " + string(fl_prog_stock) + "~r~n"
	
	g_mb.error(ls_error, sqlca)
	// --------------------------------------------------------------------------------------
	return -1
end if

return 0
end function

public function boolean uof_stampa_report_scarico_mag_for (long al_anno_bolla_acq, long al_num_bolla_acq, string as_note_aggiuntive, ref string as_error);/**
 * stefanop
 * 29/03/2012
 *
 * Stampa la ricevuta di scarico magazzino in conto deposito del fornitore
 **/
 
string ls_cod_prodotto, ls_des_prodotto, ls_path_logo_lo5, ls_cod_deposito,ls_cod_tipo_bol_acq
long ll_prog_riga_bolla_acq, ll_i
decimal{4} ld_quan_arrivata
datastore lds_store
 
lds_store = create datastore
lds_store.settransobject(sqlca)
lds_store.dataobject = "d_report_scarico_conto_dep_for"
lds_store.modify("DataWindow.Print.DocumentName='Ordine_Acquisto_" + string(al_anno_bolla_acq) + "_" + string(al_num_bolla_acq) + "'")

// Imposto logo LO5
select parametri_azienda.stringa
into :ls_path_logo_lo5
from parametri_azienda
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		parametri_azienda.flag_parametro = 'S' and &
		parametri_azienda.cod_parametro = 'LO5';

if sqlca.sqlcode = 0 and ls_path_logo_lo5 <> "" then
	lds_store.modify("intestazione.filename='" + s_cs_xx.volume + ls_path_logo_lo5 + "'")
end if
// -----

// -- Testata
select cod_deposito, cod_tipo_bol_acq
into :ls_cod_deposito, :ls_cod_tipo_bol_acq
from tes_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_bolla_acq = :al_anno_bolla_acq and
		 num_bolla_acq = :al_num_bolla_acq;
		 
if sqlca.sqlcode < 0 then
	as_error  = "Erorre durante la lettura della testa bolla acquisto." + sqlca.sqlerrtext
	return false
elseif sqlca.sqlcode = 100 then
	as_error = "Bolla di acquisto " + string(al_anno_bolla_acq) + "/" + string(al_num_bolla_acq) + " NON trovata!"
	return false
end if
// -----

declare cu_det_bol_acq cursor for  
select prog_riga_bolla_acq,   
	cod_prodotto,   
	des_prodotto,   
	quan_arrivata  
from det_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_bolla_acq = :al_anno_bolla_acq and
		 num_bolla_acq = :al_num_bolla_acq;
		 
open cu_det_bol_acq;

do while true
	
	fetch cu_det_bol_acq into 
		:ll_prog_riga_bolla_acq,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_arrivata;
		
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la lettura dei dettagli." + sqlca.sqlerrtext
		close cu_det_bol_acq;
		return false
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	

	ll_i = lds_store.insertrow(0)
	lds_store.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	lds_store.setitem(ll_i, "des_prodotto", ls_des_prodotto)
	lds_store.setitem(ll_i, "quantita", ld_quan_arrivata)
	// ---
	lds_store.setitem(ll_i, "cod_deposito", ls_cod_deposito)
	lds_store.setitem(ll_i, "cod_tipo_bol_acq", ls_cod_tipo_bol_acq)
	lds_store.setitem(ll_i, "anno_registrazione", al_anno_bolla_acq)
	lds_store.setitem(ll_i, "num_registrazione", al_num_bolla_acq)
	lds_store.setitem(ll_i, "note", as_note_aggiuntive)
	
	
loop

close cu_det_bol_acq;

lds_store.print()

return true
end function

public function integer uof_elimina_movimenti (integer fi_anno_registrazione, long fl_num_registrazione, boolean fb_elimina_records);//-------------------------------------------------------------------------------------
//	funzione di eliminazione movimenti di magazzino stock prodotti
//E' STATA RICOPIATA PARI PARI DALLA GLOBAL FUNCTION f_elimina_movimenti
//SUCCESSIVAMENTE LA GLOBAL FUNCTION E' STATA RIMOSSA DAL SOURCE CONTROL ....
//
//	redatta da: Enrico Menegotto
// data redazione: 2/7/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//														 1 = ripristino tabelle corretto, ma non sono
//															  stati cancellati i records movimenti a causa
//															  di qualche errore SQL
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fl_anno_registrazione		long					registrazione movimento
//		fl_num_registrazione			long					registrazione movimento
//		fb_elimina_records			boolean				elimino anche i records registrazioni, stock e movimenti ?
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------------------------

boolean  lb_primo_movimento
string   ls_cod_cliente, ls_cod_fornitore, ls_cod_prodotto, ls_cod_deposito, ls_cod_lotto, &
         ls_cod_ubicazione, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_cod_tipo_mov_det_1[],&
		   ls_sql, ls_str
datetime ldd_data_stock
long ll_i, ll_y, ll_x, ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_prog_mov
double ld_quan_movimento ,ld_val_movimento
datastore lds_mov_magazzino
//uo_magazzino luo_mag



select mov_magazzino.prog_mov  
into   :ll_prog_mov  
from   mov_magazzino
where  (mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda )   and
       (mov_magazzino.anno_registrazione = :fi_anno_registrazione) and
       (mov_magazzino.num_registrazione = :fl_num_registrazione)   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Impossibile trovare movimento magazzino indicato",StopSign!)
	return -1
end if

if ll_prog_mov < 1 or isnull(ll_prog_mov) then
	g_mb.messagebox("APICE","Progressivo Movimento non indicato", stopsign!)
	return -1
end if

setpointer(hourglass!)

lds_mov_magazzino = create datastore
lds_mov_magazzino.dataobject = "d_mov_magazzino"

lds_mov_magazzino.settransobject(sqlca)
ll_y = lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, ll_prog_mov, fi_anno_registrazione)
ll_x = ll_y
if ll_y < 1 then
	g_mb.messagebox("APICE","Nessun movimento presente con il numero registrazione " + string(ll_prog_mov),StopSign!)
	goto ERRORE
end if

//  -----------------  ripristino valori in stock e prodotti  -----------------------------
//  ------------- passando una ad una le righe movimenti e stornando ----------------------

for ll_i = 1 to ll_y
	ls_cod_prodotto   = lds_mov_magazzino.object.cod_prodotto[ll_i]
	ls_cod_deposito   = lds_mov_magazzino.object.cod_deposito[ll_i]
	ls_cod_ubicazione = lds_mov_magazzino.object.cod_ubicazione[ll_i]
	ls_cod_lotto      = lds_mov_magazzino.object.cod_lotto[ll_i]
	ldd_data_stock    = lds_mov_magazzino.object.data_stock[ll_i]
	ll_prog_stock     = lds_mov_magazzino.object.prog_stock[ll_i]
	ls_cod_tipo_movimento = lds_mov_magazzino.object.cod_tipo_movimento[ll_i]
	ls_cod_tipo_mov_det   = lds_mov_magazzino.object.cod_tipo_mov_det[ll_i]
	ld_quan_movimento = lds_mov_magazzino.object.quan_movimento[ll_i]
	ld_val_movimento  = lds_mov_magazzino.object.val_movimento[ll_i]
	ls_cod_cliente    = lds_mov_magazzino.object.cod_cliente[ll_i]
	ls_cod_fornitore  = lds_mov_magazzino.object.cod_fornitore[ll_i]
	ll_anno_registrazione = lds_mov_magazzino.object.anno_registrazione[ll_i]
	ll_num_registrazione  = lds_mov_magazzino.object.num_registrazione[ll_i]
	
	if uof_aggiorna_stock (ls_cod_prodotto, &
								ls_cod_deposito, &
								ls_cod_ubicazione, &
								ls_cod_lotto, &
								ldd_data_stock, &
								ll_prog_stock, &
								ls_cod_tipo_movimento, &
								ls_cod_tipo_mov_det, &
								ls_cod_fornitore, &
								ls_cod_cliente, &
								ld_quan_movimento, &
								ld_val_movimento, &
								false, &
								true, &
								true) = -1 then goto ERRORE
	
	if uof_aggiorna_prodotti ( ls_cod_tipo_movimento, &
									 ls_cod_tipo_mov_det, &
									 ls_cod_prodotto, &
									 ld_quan_movimento, &
									 ld_val_movimento, &
									 ls_cod_fornitore, &
									 true ) = -1 then goto ERRORE
									 
	update mov_magazzino
	set quan_movimento = 0, val_movimento = 0
	where mov_magazzino.cod_azienda        = :s_cs_xx.cod_azienda and
			mov_magazzino.anno_registrazione = :ll_anno_registrazione and
			mov_magazzino.num_registrazione  = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then goto ERRORE

next
	

// -------------- eseguo tentativo di cancellazione records ( se richiesto ) ------------
for ll_i = 1 to ll_y
	ls_cod_prodotto   = lds_mov_magazzino.object.cod_prodotto[ll_i]
	ls_cod_deposito   = lds_mov_magazzino.object.cod_deposito[ll_i]
	ls_cod_ubicazione = lds_mov_magazzino.object.cod_ubicazione[ll_i]
	ls_cod_lotto      = lds_mov_magazzino.object.cod_lotto[ll_i]
	ldd_data_stock    = lds_mov_magazzino.object.data_stock[ll_i]
	ll_prog_stock     = lds_mov_magazzino.object.prog_stock[ll_i]
	ls_cod_tipo_movimento = lds_mov_magazzino.object.cod_tipo_movimento[ll_i]
	ls_cod_tipo_mov_det   = lds_mov_magazzino.object.cod_tipo_mov_det[ll_i]
	ld_quan_movimento = lds_mov_magazzino.object.quan_movimento[ll_i]
	ld_val_movimento  = lds_mov_magazzino.object.val_movimento[ll_i]
	ls_cod_cliente    = lds_mov_magazzino.object.cod_cliente[ll_i]
	ls_cod_fornitore  = lds_mov_magazzino.object.cod_fornitore[ll_i]
	ll_anno_registrazione = lds_mov_magazzino.object.anno_registrazione[ll_i]
	ll_num_registrazione  = lds_mov_magazzino.object.num_registrazione[ll_i]

	if fb_elimina_records then
		delete from mov_magazzino
		where mov_magazzino.cod_azienda        = :s_cs_xx.cod_azienda and
				mov_magazzino.anno_registrazione = :ll_anno_registrazione and
				mov_magazzino.num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then goto errore
	
	//09/07/2012 Donato: Enrico mi ha detto di commentare questo update sulla tabella dello stock perchè non serve ad un cax
//		delete from stock
//		where stock.cod_azienda    = :s_cs_xx.cod_azienda and
//				stock.cod_prodotto   = :ls_cod_prodotto and
//				stock.cod_deposito   = :ls_cod_deposito and
//				stock.cod_ubicazione = :ls_cod_ubicazione and
//				stock.cod_lotto      = :ls_cod_lotto and
//				stock.data_stock     = :ldd_data_stock and
//				stock.prog_stock     = :ll_prog_stock and
//				giacenza_stock       = 0;

	end if
next	

destroy lds_mov_magazzino
setpointer(arrow!)
return 0

// --------------------------- uscita per causa errore -------------------------------------
// ho usato il comando goto per non ripetere lo stesso codice decine di volte; percui quelli
// standard non devono rognare: il goto esisterè pure per qualche motivo.
// -----------------------------------------------------------------------------------------

ERRORE:
	ROLLBACK;
	destroy lds_mov_magazzino
	setpointer(arrow!)
	return -1

end function

public function integer uof_movimenti_mag_ragguppato (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine, ref string as_error);/**
 * stefanop
 * 07/11/2011
 *
 * Creo il movimento anche per il prodotto raggruppato
 **/
 
string ls_cod_prodotto_raggruppato, ls_empty[], ls_test
long ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_empty[]
decimal ld_fat_conversione_rag_mag, ld_quantita
datetime ldt_empty[]


if not ib_mov_raggruppato then
	return 0
end if


select cod_prodotto_raggruppato, fat_conversione_rag_mag
into :ls_cod_prodotto_raggruppato, :ld_fat_conversione_rag_mag
from anag_prodotti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_prodotto = :fs_cod_prodotto;
	
if isnull(ls_cod_prodotto_raggruppato) or len(ls_cod_prodotto_raggruppato) < 1 then return 0

// stefanop 01/08/2012: controllo che il codice raggrupato esista perchè in alcuni casi da GIBUS capita che non c'è ma è ssegato al prodotto
// ---------------------------------------
select cod_prodotto
into :ls_test
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto_raggruppato;
		 
if sqlca.sqlcode < 0 then
	return -1
elseif sqlca.sqlcode = 100 then
	as_error = g_str.format("Il codice raggruppato $1 per il prodotto $2 NON esiste e per tale non verranno fatti i movimenti di magazzino", ls_cod_prodotto_raggruppato, fs_cod_prodotto)
	f_log_sistema(as_error, "ERRMOVMAGRAG")
	return -1
end if
// ---------------------------------------
	
fd_valore_unitario = f_converti_qta_raggruppo(fs_cod_prodotto, fd_valore_unitario, "D")
ld_quantita = f_converti_qta_raggruppo(fs_cod_prodotto, fd_quantita, "M")
	
//fs_cod_deposito = ls_empty
//fs_cod_ubicazione = ls_empty
//fdd_data_stock = ldt_empty
//fl_prog_stock = ll_empty
//fs_cod_fornitore = ls_empty
setnull(ll_anno_reg_des_mov)
setnull(ll_num_reg_des_mov)


if f_crea_dest_mov_magazzino ( &
	fs_cod_tipo_movimento, &
	ls_cod_prodotto_raggruppato, &
	fs_cod_deposito[], &
	fs_cod_ubicazione[], &
	fs_cod_lotto[], &
	fdd_data_stock[], &
	fl_prog_stock[], &
	fs_cod_cliente[], &
	fs_cod_fornitore[], &
	ll_anno_reg_des_mov, &
	ll_num_reg_des_mov ) = -1 then
	
	return -1
end if

if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, fs_cod_tipo_movimento, ls_cod_prodotto_raggruppato) = -1 then
	return -1
end if
	
if uof_movimenti_mag (fdd_data_registrazione, &
							fs_cod_tipo_movimento, &
							"S", &
							ls_cod_prodotto_raggruppato, &
							ld_quantita, &
							fd_valore_unitario, &
							fl_num_documento, &
							fdd_data_documento, &
							fs_referenza, &
							ll_anno_reg_des_mov, &
							ll_num_reg_des_mov, &
							fs_cod_deposito[], &
							fs_cod_ubicazione[], &
							fs_cod_lotto[], &
							fdd_data_stock[], &
							fl_prog_stock[], &
							fs_cod_fornitore[], &
							fs_cod_cliente[], &
							fl_anno_registrazione[], &
							fl_num_registrazione[],&
							al_anno_documento,&
							al_anno_reg_mov_origine,&
							al_num_reg_mov_origine) = -1 then

	return -1
end if
		
f_elimina_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov)

return 0
end function

public function integer uof_aggiorna_mov_magazzino (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdd_data_stock, long fl_prog_stock, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, string fs_cod_cliente, string fs_cod_fornitore, boolean fb_storno, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine);//-------------------------------------------------------------------------------------
//	funzione di creazione / modifica movimento in magazzino
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// --------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data di registrazione movimento
//		fs_cod_tipo_movimento		string				tipo movimento
//		fs_cod_tipo_mov_det			string				tipo movimento dettaglio
//		fs_flag_mov_manuale			string				S=movimento manuale N=movimento automatico
//																	 generato da bolle/fatture ecc......
//		fs_cod_prodotto				string				--	
//		fs_cod_deposito				string				   \
//		fs_cod_ubicazione				string                \ ____  riferimento stock
//		fs_cod_lotto					string				    /
//		fdd_data_stock					date					   /
//		fl_prog_stock					long					--	
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fl_prog_mov						long					codice identificativo registrazione
//		fl_num_documento				long					riferimento al numero documento
//		fdd_data_documento			date					riferimento alla data documento
//		fs_referenza					string				riferimento libero
//		fb_primo_movimento			boolean				flag primo movimento 
//		fs_cod_cliente					string				codice cliente
//		fs_cod_fornitore				string				codice fornitore
//		fb_storno						boolean				sono in update record
//
// variabili in uscita: 			tipo di dato		commento
// ---------------------------------------------------------------------------------
//		fl_anno_registrazione		long					chiave registrazione movimento
//		fl_num_registrazione			long
// ---
// stefanop: 12/12/11: aggiunti valori
// anno_documento,
// anno_reg_mov_origine = anno di riferimento del movimento (non raggruppato)
// num_reg_mov_origine = num di riferimento del movimento (non raggruppato)
//-------------------------------------------------------------------------------------


long ll_anno_registrazione, ll_num_registrazione, ll_max_mov_mag
string ls_flag_manuale

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

// stefanop: 18/02/2013: Controllo che il valore unitario del movimento non sia NULL altrimenti
// non esce nella maschera mov_magazzino.
if isnull(fd_valore_unitario) then fd_valore_unitario = 0
// --------------

if fb_storno then
	update mov_magazzino
	set data_registrazione = :fdd_data_registrazione,
		 cod_tipo_movimento = :fs_cod_tipo_movimento,
		 cod_prodotto = :fs_cod_prodotto ,
		 cod_deposito = :fs_cod_deposito ,
		 cod_ubicazione = :fs_cod_ubicazione ,
		 cod_lotto = :fs_cod_lotto ,
		 quan_movimento = :fd_quantita ,
		 val_movimento = :fd_valore_unitario ,
		 num_documento = :fl_num_documento ,
		 data_documento = :fdd_data_documento ,
		 referenza = :fs_referenza ,
		 data_stock = :fdd_data_stock ,
		 prog_stock = :fl_prog_stock ,
		 cod_tipo_mov_det = :fs_cod_tipo_mov_det ,
		 cod_cliente = :fs_cod_cliente ,
		 cod_fornitore = :fs_cod_fornitore,
		 anno_documento = :al_anno_documento,
		 anno_reg_mov_origine = :al_anno_reg_mov_origine,
		 num_reg_mov_origine = :al_num_reg_mov_origine
	where
		mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda and
		mov_magazzino.anno_registrazione = :fl_anno_registrazione and
		mov_magazzino.num_registrazione = :fl_num_registrazione ;
	if sqlca.sqlcode <> 0 then
		if not ib_silentmode then 
			g_mb.error(g_str.format("Errore durante l'aggiornamento del movimento di magazzino $1/$2", fl_anno_registrazione, fl_num_registrazione), sqlca)
		else
			is_silentmode_error = g_str.format("Errore durante l'aggiornamento del movimento di magazzino $1/$2. Errore SQL: $3", fl_anno_registrazione, fl_num_registrazione, sqlca.sqlerrtext)
		end if
		return -1
	end if
else
	ll_anno_registrazione = f_anno_esercizio()
	select num_registrazione
	into :ll_num_registrazione
	from con_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		if not ib_silentmode then 
			g_mb.error("Anomalia nella tabella Controllo Magazzino", sqlca)
		else
			is_silentmode_error = "Anomalia nella tabella Controllo Magazzino"
		end if
		return -1
	elseif isnull(ll_num_registrazione) or ll_num_registrazione < 0 then
		ll_num_registrazione = 0
	end if
	
	// stefanop 07/08/2012: confronto il max dei movimenti di magazzino con il contenuto
	// della con_magazzino per verificare eventuali discrepanze.
	// {
	select max(num_registrazione)
	into :ll_max_mov_mag
	from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione;
			 
	if sqlca.sqlcode < 0 then
		if not ib_silentmode then 
			g_mb.error("Errore il lettura max num_registrazione dalla tabella movimenti.", sqlca)
		else
			is_silentmode_error = "Errore il lettura max num_registrazione dalla tabella movimenti. Errore specifico DB: " + sqlca.sqlerrtext
		end if
		return -1
	elseif isnull(ll_max_mov_mag) or ll_max_mov_mag < 0 then
		ll_max_mov_mag = 0
	end if
	
	// allineo la con magazzino con il numero movimento maggiore
	ll_num_registrazione = max(ll_num_registrazione, ll_max_mov_mag)
	// }
	
	if isnull(ll_num_registrazione) then
		ll_num_registrazione = 1
	else
		ll_num_registrazione ++
	end if
	
	update con_magazzino
	set num_registrazione = :ll_num_registrazione
	where  cod_azienda = :s_cs_xx.cod_azienda;
	if sqlca.sqlcode <> 0 then
		if not ib_silentmode then 
			g_mb.messagebox("APICE","Anomalia nella tabella Controllo Magazzino",StopSign!)
		else
			is_silentmode_error = "Anomalia nella tabella Controllo Magazzino. Errore specifico DB: " + sqlca.sqlerrtext
		end if
		return -1
	end if
	
	if fb_primo_movimento and fs_flag_mov_manuale = "S" then
		ls_flag_manuale = "S"
	else
		ls_flag_manuale = "N"
	end if
	
	INSERT INTO mov_magazzino
			(cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			data_registrazione,   
			cod_tipo_movimento,   
			cod_prodotto,   
			cod_deposito,   
			cod_ubicazione,   
			cod_lotto,   
			quan_movimento,   
			val_movimento,   
			num_documento,   
			data_documento,   
			referenza,   
			flag_manuale,   
			flag_storico,   
			data_stock,   
			prog_stock,
			prog_mov,
			cod_tipo_mov_det,
			cod_cliente,
			cod_fornitore,
			anno_documento,
			anno_reg_mov_origine,
			num_reg_mov_origine,
			flag_commessa)  
	VALUES ( :s_cs_xx.cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			:fdd_data_registrazione,   
			:fs_cod_tipo_movimento,   
			:fs_cod_prodotto,   
			:fs_cod_deposito,   
			:fs_cod_ubicazione,   
			:fs_cod_lotto,   
			:fd_quantita,   
			:fd_valore_unitario,   
			:fl_num_documento,   
			:fdd_data_documento,   
			:fs_referenza,   
			:ls_flag_manuale,   
			'N',   
			:fdd_data_stock,   
			:fl_prog_stock,
			:fl_prog_mov,
			:fs_cod_tipo_mov_det,
			:fs_cod_cliente, 
			:fs_cod_fornitore,
			:al_anno_documento,
			:al_anno_reg_mov_origine,
			:al_num_reg_mov_origine,
			:is_flag_commessa)  ;
	if sqlca.sqlcode <> 0 then
		if not ib_silentmode then 
			g_mb.error(g_str.format("Errore durante l'inserimento del movimenti magazzino $1/$2", ll_anno_registrazione, ll_num_registrazione), sqlca)
		else
			is_silentmode_error = g_str.format("Errore durante l'inserimento del movimenti magazzino $1/$2. Errore specifico DB: $3", ll_anno_registrazione, ll_num_registrazione, sqlca.sqlerrtext)
		end if
		return -1
	else
		fl_anno_registrazione = ll_anno_registrazione
		fl_num_registrazione  = ll_num_registrazione
	end if
end if
return 0
end function

private function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine);//-------------------------------------------------------------------------------------
//	funzione di creazione movimenti di magazzino stock prodotti
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data registrazione movimento
//		fs_cod_tipo_movimento		string				tipo movimento magazzino
//		fs_flag_mov_manuale			string				S=movimento manuale N=movimento automatico
//																	 generato da bolle/fatture ecc......
//		fs_cod_prodotto				string					
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fl_num_documento				long					rif documento
//		fdd_data_documento			date					rif documento
//		fs_referenza					string				referenza
//		fl_anno_reg_dest_stock		long					anno destinazione stock
//		fl_num_reg_dest_stock		long					num destinazione stock
//		fs_cod_deposito[1]			string				   \
//		fs_cod_ubicazione[1]			string                \ 
//		fs_cod_lotto[1]				string				     >  riferimento stock
//		fdd_data_stock[1]				date					    /
//		fl_prog_stock[1]				long					   /
//		fs_cod_fornitore[1]			string				codice fornitore terzista
//		fs_cod_cliente[1]				string				codice cliente   terzista
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_deposito[]				string				   \
//		fs_cod_ubicazione[]			string                \ 
//		fs_cod_lotto[]					string				     >  riferimento stock
//		fdd_data_stock[]				date					    /
//		fl_prog_stock[]				long					   /
//		fs_cod_fornitore[]			string				codice fornitore terzista
//		fs_cod_cliente[]				string				codice cliente   terzista
// 	fl_anno_registrazione[]		long					registrazioni dei movimento
//		fl_num_registrazione[]		long
//
//
//
//-------------------------------------------------------------------------------------

boolean  ib_primo_movimento
string   ls_str, ls_cod_tipo_mov_det[], ls_sql, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
         ls_cod_fornitore, ls_cod_cliente
long     ll_i, ll_y, ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_prog_mov
datetime ldd_data_stock, ldd_data_chiusura_periodica

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

setnull(ls_cod_cliente)
setnull(ls_cod_fornitore)
setnull(ls_cod_deposito)
setnull(ls_cod_ubicazione)
setnull(ls_cod_lotto)
ib_primo_movimento = true

setpointer(hourglass!)

ll_i = 0
declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.cod_tipo_mov_det FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + fs_cod_tipo_movimento + "')  ORDER BY ordinamento"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;
do while 1=1
   fetch cu_det_movimenti into :ls_str;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	ls_cod_tipo_mov_det[ll_i] = ls_str
loop
close cu_det_movimenti;

if ll_i = 0 then
	setpointer(arrow!)
	return -1
end if

if uof_new_con_magazzino(fdd_data_registrazione, ll_prog_mov) = -1 then
	ROLLBACK;
	setpointer(arrow!)
	return -1
end if


for ll_y = 1 to ll_i
	SELECT dest_mov_magazzino.cod_deposito,   
          dest_mov_magazzino.cod_ubicazione,   
          dest_mov_magazzino.cod_lotto,   
          dest_mov_magazzino.data_stock,   
          dest_mov_magazzino.prog_stock,   
          dest_mov_magazzino.cod_cliente,   
          dest_mov_magazzino.cod_fornitore  
   INTO	 :ls_cod_deposito,   
      	 :ls_cod_ubicazione,   
        	 :ls_cod_lotto,   
        	 :ldd_data_stock,   
        	 :ll_prog_stock,   
        	 :ls_cod_cliente,   
        	 :ls_cod_fornitore  
   FROM	 dest_mov_magazzino  
   WHERE  (dest_mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (dest_mov_magazzino.anno_registrazione = :fl_anno_reg_dest_stock ) AND  
          (dest_mov_magazzino.num_registrazione = :fl_num_reg_dest_stock ) AND  
          (dest_mov_magazzino.prog_registrazione = :ll_y )   ;
	if sqlca.sqlcode <> 0 then
		if not ib_silentmode then
			g_mb.messagebox("APICE","Ricerca destinazione stock fallita: verificare (uof_movimenti_mag). Dettaglio errore:" + sqlca.sqlerrtext,stopsign!)
		else
			is_silentmode_error = "Ricerca destinazione stock fallita: verificare. (uof_movimenti_mag) . Dettaglio errore:" + sqlca.sqlerrtext
		end if
		ROLLBACK;
		setpointer(arrow!)
		return -1
	end if
	
	if uof_registra_movimento_mag(fdd_data_registrazione, &
										 fs_cod_tipo_movimento, &
										 ls_cod_tipo_mov_det[ll_y], &
										 fs_flag_mov_manuale, &
										 fs_cod_prodotto, &
										 fd_quantita, &
										 fd_valore_unitario, &
										 ll_prog_mov, &
										 fl_num_documento, &
										 fdd_data_documento, &
										 fs_referenza, &
										 ib_primo_movimento, &
										 ls_cod_deposito, &
										 ls_cod_ubicazione, &
										 ls_cod_lotto, &
										 ldd_data_stock, &
										 ll_prog_stock, &
										 ls_cod_fornitore, &
										 ls_cod_cliente, &
										 ll_anno_registrazione, &
										 ll_num_registrazione,&
										 al_anno_documento,&
										 al_anno_reg_mov_origine,&
										 al_num_reg_mov_origine) = -1 then
		ROLLBACK;
		setpointer(arrow!)
		return -1
	end if
	ib_primo_movimento = false
	fs_cod_deposito[ll_y]       = ls_cod_deposito
	fs_cod_ubicazione[ll_y]     = ls_cod_ubicazione
	fs_cod_lotto[ll_y]          = ls_cod_lotto
	fdd_data_stock[ll_y]        = ldd_data_stock
	fl_prog_stock[ll_y]         = ll_prog_stock
	fs_cod_fornitore[ll_y]      = ls_cod_fornitore
	fs_cod_cliente[ll_y]        = ls_cod_cliente
	fl_anno_registrazione[ll_y] = ll_anno_registrazione
	fl_num_registrazione[ll_y]  = ll_num_registrazione
next

setpointer(arrow!)

return 0
end function

public function integer uof_movimenti_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, long fl_anno_reg_dest_stock, long fl_num_reg_dest_stock, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[], ref string fs_errore);//-------------------------------------------------------------------------------------
//	funzione di creazione movimenti di magazzino stock prodotti
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data registrazione movimento
//		fs_cod_tipo_movimento		string				tipo movimento magazzino
//		fs_flag_mov_manuale			string				S=movimento manuale N=movimento automatico
//																	 generato da bolle/fatture ecc......
//		fs_cod_prodotto				string					
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fl_num_documento				long					rif documento
//		fdd_data_documento			date					rif documento
//		fs_referenza					string				referenza
//		fl_anno_reg_dest_stock		long					anno destinazione stock
//		fl_num_reg_dest_stock		long					num destinazione stock
//		fs_cod_deposito[1]			string				   \
//		fs_cod_ubicazione[1]			string                \ 
//		fs_cod_lotto[1]				string				     >  riferimento stock
//		fdd_data_stock[1]				date					    /
//		fl_prog_stock[1]				long					   /
//		fs_cod_fornitore[1]			string				codice fornitore terzista
//		fs_cod_cliente[1]				string				codice cliente   terzista
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_deposito[]				string				   \
//		fs_cod_ubicazione[]			string                \ 
//		fs_cod_lotto[]					string				     >  riferimento stock
//		fdd_data_stock[]				date					    /
//		fl_prog_stock[]				long					   /
//		fs_cod_fornitore[]			string				codice fornitore terzista
//		fs_cod_cliente[]				string				codice cliente   terzista
// 	fl_anno_registrazione[]		long					registrazioni dei movimento
//		fl_num_registrazione[]		long
//		fs_errore
//
//
//-------------------------------------------------------------------------------------

boolean  ib_primo_movimento
string   ls_str, ls_cod_tipo_mov_det[], ls_sql, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
         ls_cod_fornitore, ls_cod_cliente
long     ll_i, ll_y, ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_prog_mov, ll_anno_documento, ll_empty
datetime ldd_data_stock, ldd_data_chiusura_periodica

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

setnull(ls_cod_cliente)
setnull(ls_cod_fornitore)
setnull(ls_cod_deposito)
setnull(ls_cod_ubicazione)
setnull(ls_cod_lotto)
ib_primo_movimento = true

setpointer(hourglass!)

ll_i = 0
declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.cod_tipo_mov_det FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + fs_cod_tipo_movimento + "')  ORDER BY ordinamento"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;
do while 1=1
   fetch cu_det_movimenti into :ls_str;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_i = ll_i + 1
	ls_cod_tipo_mov_det[ll_i] = ls_str
loop
close cu_det_movimenti;

if ll_i = 0 then
	fs_errore = "Nessun elemento trovato in det_tipi_movimenti per cod_tipo_movimento='"+fs_cod_tipo_movimento+"'"
	setpointer(arrow!)
	return -1
end if

if uof_new_con_magazzino(fdd_data_registrazione, ll_prog_mov) = -1 then
	ROLLBACK;
	fs_errore = "Errore in funzione uof_new_con_magazzino all'interno di uof_movimenti_mag"
	setpointer(arrow!)
	return -1
end if


for ll_y = 1 to ll_i
	SELECT dest_mov_magazzino.cod_deposito,   
          dest_mov_magazzino.cod_ubicazione,   
          dest_mov_magazzino.cod_lotto,   
          dest_mov_magazzino.data_stock,   
          dest_mov_magazzino.prog_stock,   
          dest_mov_magazzino.cod_cliente,   
          dest_mov_magazzino.cod_fornitore  
   INTO	 :ls_cod_deposito,   
      	 :ls_cod_ubicazione,   
        	 :ls_cod_lotto,   
        	 :ldd_data_stock,   
        	 :ll_prog_stock,   
        	 :ls_cod_cliente,   
        	 :ls_cod_fornitore  
   FROM	 dest_mov_magazzino  
   WHERE  (dest_mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (dest_mov_magazzino.anno_registrazione = :fl_anno_reg_dest_stock ) AND  
          (dest_mov_magazzino.num_registrazione = :fl_num_reg_dest_stock ) AND  
          (dest_mov_magazzino.prog_registrazione = :ll_y )   ;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Ricerca destinazione stock fallita: verificare "
		ROLLBACK;
		setpointer(arrow!)
		return -1
	end if
	
	 ll_anno_documento = long(fs_referenza)
	 if ll_anno_documento>9999 then ll_anno_documento=9999
	 
	 setnull(ll_empty)
	
	if uof_registra_movimento_mag(fdd_data_registrazione, &
										 fs_cod_tipo_movimento, &
										 ls_cod_tipo_mov_det[ll_y], &
										 fs_flag_mov_manuale, &
										 fs_cod_prodotto, &
										 fd_quantita, &
										 fd_valore_unitario, &
										 ll_prog_mov, &
										 fl_num_documento, &
										 fdd_data_documento, &
										 fs_referenza, &
										 ib_primo_movimento, &
										 ls_cod_deposito, &
										 ls_cod_ubicazione, &
										 ls_cod_lotto, &
										 ldd_data_stock, &
										 ll_prog_stock, &
										 ls_cod_fornitore, &
										 ls_cod_cliente, &
										 ll_anno_registrazione, &
										 ll_num_registrazione,&
										 ll_anno_documento,&
										 ll_empty,&
										 ll_empty) = -1 then
		
		fs_errore = "Errore in esecuzione funzione uof_registra_movimento_mag!"
		ROLLBACK;
		setpointer(arrow!)
		return -1
	end if
	ib_primo_movimento = false
	fs_cod_deposito[ll_y]       = ls_cod_deposito
	fs_cod_ubicazione[ll_y]     = ls_cod_ubicazione
	fs_cod_lotto[ll_y]          = ls_cod_lotto
	fdd_data_stock[ll_y]        = ldd_data_stock
	fl_prog_stock[ll_y]         = ll_prog_stock
	fs_cod_fornitore[ll_y]      = ls_cod_fornitore
	fs_cod_cliente[ll_y]        = ls_cod_cliente
	fl_anno_registrazione[ll_y] = ll_anno_registrazione
	fl_num_registrazione[ll_y]  = ll_num_registrazione
next

setpointer(arrow!)

return 0
end function

public function integer uof_aggiorna_prodotti (string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, string fs_cod_fornitore, boolean fb_storno, ref string fs_errore);//-------------------------------------------------------------------------------------
//	funzione di aggiornamento anagrafica prodotti magazzino
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:    0 = tutto OK
//  												-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// -----------------------------------------------------------------------------
//		fs_cod_tipo_mov_det			string				tipo configurazione movimento
//		fs_cod_prodotto				string				--	
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fs_cod_fornitore				string				codice fornitore ( per ora non usato )
//		fb_storno						boolean				TRUE = movimento di storno 
//
// variabili in ingresso:			tipo di dato		commento
// -----------------------------------------------------------------------------
//	
//-------------------------------------------------------------------------------------

string ls_flag_saldo_quan_inizio_anno, ls_flag_val_inizio_anno, ls_flag_saldo_quan_ultima_chius, &  
       ls_flag_prog_quan_entrata, ls_flag_val_quan_entrata, ls_flag_prog_quan_uscita,   &
       ls_flag_val_quan_uscita, ls_flag_prog_quan_acquistata, ls_flag_val_quan_acquistata,   &
       ls_flag_prog_quan_venduta, ls_flag_val_quan_venduta, ls_flag_costo_ultimo,   &
       ls_flag_costo_medio, ls_flag_fornitore

dec{4} ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chius, &  
       ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita,   &
       ld_val_quan_uscita, ld_prog_quan_acquistata, ld_val_quan_acquistata,   &
       ld_prog_quan_venduta, ld_val_quan_venduta, ld_costo_ultimo

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)

ld_saldo_quan_inizio_anno = 0
ld_val_inizio_anno = 0
ld_saldo_quan_ultima_chius = 0
ld_prog_quan_entrata = 0
ld_val_quan_entrata = 0
ld_prog_quan_uscita = 0
ld_val_quan_uscita = 0
ld_prog_quan_acquistata = 0
ld_val_quan_acquistata = 0
ld_prog_quan_venduta = 0
ld_val_quan_venduta = 0
ld_costo_ultimo = 0

select flag_fornitore  
  into :ls_flag_fornitore  
  from det_tipi_movimenti
 where cod_azienda = :s_cs_xx.cod_azienda  AND  
       cod_tipo_movimento = :fs_cod_tipo_movimento AND  
       cod_tipo_mov_det = :fs_cod_tipo_mov_det;
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca su DET_TIPI_MOVIMENTI: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	fs_errore = "Cod_tipo_movimento/cod_tipo_mov_det ("+fs_cod_tipo_movimento+"/"+fs_cod_tipo_mov_det+" non trovati in det_tipi_movimenti)"
	return -1
	
end if


// ------ se è attiva l'indicazione flag_fornitore non aggiorno il mio magazzino --------
// -----------  perchè il movimento riguarda il magazzino del mio terzista   ------------

if ls_flag_fornitore = "S" then return 0

// ---------------------------------------------------------------------------------------

select flag_saldo_quan_inizio_anno,   
 		 flag_val_inizio_anno,   
	 	 flag_saldo_quan_ultima_chius,   
		 flag_prog_quan_entrata,   
		 flag_val_quan_entrata,   
		 flag_prog_quan_uscita,   
		 flag_val_quan_uscita,   
		 flag_prog_quan_acquistata,   
		 flag_val_quan_acquistata,   
		 flag_prog_quan_venduta,   
		 flag_val_quan_venduta,   
		 flag_costo_ultimo,   
		 flag_costo_medio  
 into  :ls_flag_saldo_quan_inizio_anno,   
		 :ls_flag_val_inizio_anno,   
		 :ls_flag_saldo_quan_ultima_chius,   
		 :ls_flag_prog_quan_entrata,   
		 :ls_flag_val_quan_entrata,   
		 :ls_flag_prog_quan_uscita,   
		 :ls_flag_val_quan_uscita,   
		 :ls_flag_prog_quan_acquistata,   
		 :ls_flag_val_quan_acquistata,   
		 :ls_flag_prog_quan_venduta,   
		 :ls_flag_val_quan_venduta,   
		 :ls_flag_costo_ultimo,   
		 :ls_flag_costo_medio  
 from  tab_tipi_movimenti_det
where  ( cod_azienda = :s_cs_xx.cod_azienda      ) AND  
		 ( cod_tipo_mov_det = :fs_cod_tipo_mov_det );
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca della configurazione del movimento in tab_tipi_movimenti_det: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	fs_errore = "Cod_tipo_mov_det ("+fs_cod_tipo_mov_det+" non trovato in tab_tipi_movimenti_det)"
	return -1
	
end if

choose case ls_flag_saldo_quan_inizio_anno
	case "+"
		ld_saldo_quan_inizio_anno = fd_quantita
	case "-"
		ld_saldo_quan_inizio_anno = - fd_quantita
	case "="
		ld_saldo_quan_inizio_anno = 0
end choose

choose case ls_flag_val_inizio_anno
	case "+"
		ld_val_inizio_anno = fd_valore_unitario * fd_quantita
	case "-"
		ld_val_inizio_anno = - fd_valore_unitario * fd_quantita
	case "="
		ld_val_inizio_anno = 0
		
end choose

choose case ls_flag_saldo_quan_ultima_chius
	case "+"
		ld_saldo_quan_ultima_chius = fd_quantita
	case "-"
		ld_saldo_quan_ultima_chius = - fd_quantita
	case "="
		ld_saldo_quan_ultima_chius = 0
end choose

choose case ls_flag_prog_quan_entrata
	case "+"
		ld_prog_quan_entrata = fd_quantita
	case "-"
		ld_prog_quan_entrata = - fd_quantita
	case "="
		ld_prog_quan_entrata = 0
end choose

choose case ls_flag_val_quan_entrata
	case "+"
		ld_val_quan_entrata = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_entrata = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_entrata = 0
end choose

choose case ls_flag_prog_quan_uscita
	case "+"
		ld_prog_quan_uscita = fd_quantita
	case "-"
		ld_prog_quan_uscita = - fd_quantita
	case "="
		ld_prog_quan_uscita = 0
end choose

choose case ls_flag_val_quan_uscita
	case "+"
		ld_val_quan_uscita = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_uscita = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_uscita = 0
end choose

choose case ls_flag_prog_quan_acquistata
	case "+"
		ld_prog_quan_acquistata = fd_quantita
	case "-"
		ld_prog_quan_acquistata = - fd_quantita
	case "="
		ld_prog_quan_acquistata = 0
end choose

choose case ls_flag_val_quan_acquistata
	case "+"
		ld_val_quan_acquistata = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_acquistata = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_acquistata = 0
end choose

choose case ls_flag_prog_quan_venduta
	case "+"
		ld_prog_quan_venduta = fd_quantita
	case "-"
		ld_prog_quan_venduta = - fd_quantita
	case "="
		ld_prog_quan_venduta = 0
end choose

choose case ls_flag_val_quan_venduta
	case "+"
		ld_val_quan_venduta = fd_quantita * fd_valore_unitario
	case "-"
		ld_val_quan_venduta = - ( fd_quantita * fd_valore_unitario )
	case "="
		ld_val_quan_venduta = 0
end choose

choose case ls_flag_costo_ultimo
	case "+"		

		// *** michela 18/03/04: se ib_flag_agg_costo_ultimo è true e il movimento di magazzino lo consente allora aggiorno
		if ib_flag_agg_costo_ultimo then 
			ld_costo_ultimo = fd_valore_unitario
		else
			select anag_prodotti.costo_ultimo
			into   :ld_costo_ultimo
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
					 anag_prodotti.cod_prodotto = :fs_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "(1) Errore in ricerca costo_ultimo in anag_prodotti per prodotto "+fs_cod_prodotto+" : "+sqlca.sqlerrtext
				return -1
				
			elseif sqlca.sqlcode = 100 then
				fs_errore = "(1) Prodotto "+fs_cod_prodotto+" non trovato in anag_prodotti"
				return -1
				
			end if
			
		end if
		
	case "-"

		// *** michela 18/03/04: se ib_flag_agg_costo_ultimo è true e il movimento di magazzino lo consente allora aggiorno
		if ib_flag_agg_costo_ultimo then 		
			ld_costo_ultimo = fd_valore_unitario
		else
			select anag_prodotti.costo_ultimo
			into   :ld_costo_ultimo
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
					 anag_prodotti.cod_prodotto = :fs_cod_prodotto;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "(2) Errore in ricerca costo_ultimo in anag_prodotti per prodotto "+fs_cod_prodotto+" : "+sqlca.sqlerrtext
				return -1
				
			elseif sqlca.sqlcode = 100 then
				fs_errore = "(2) Prodotto "+fs_cod_prodotto+" non trovato in anag_prodotti"
				return -1
				
			end if

		end if			
		
	case "="
		select anag_prodotti.costo_ultimo
		into   :ld_costo_ultimo
		from   anag_prodotti
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and
				 anag_prodotti.cod_prodotto = :fs_cod_prodotto;

		if sqlca.sqlcode < 0 then
			fs_errore = "(3) Errore in ricerca costo_ultimo in anag_prodotti per prodotto "+fs_cod_prodotto+" : "+sqlca.sqlerrtext
			return -1
			
		elseif sqlca.sqlcode = 100 then
			fs_errore = "(3) Prodotto "+fs_cod_prodotto+" non trovato in anag_prodotti"
			return -1
			
		end if

end choose

if fb_storno then
	ld_saldo_quan_inizio_anno = ld_saldo_quan_inizio_anno * -1
	ld_val_inizio_anno = ld_val_inizio_anno * -1 
	ld_saldo_quan_ultima_chius = ld_saldo_quan_ultima_chius * -1
	ld_prog_quan_entrata = ld_prog_quan_entrata * -1
	ld_val_quan_entrata = ld_val_quan_entrata * -1
	ld_prog_quan_uscita = ld_prog_quan_uscita * -1
	ld_val_quan_uscita = ld_val_quan_uscita * -1
	ld_prog_quan_acquistata = ld_prog_quan_acquistata * -1
	ld_val_quan_acquistata = ld_val_quan_acquistata * -1
	ld_prog_quan_venduta = ld_prog_quan_venduta * -1
	ld_val_quan_venduta = ld_val_quan_venduta * -1
end if
	
update anag_prodotti
   set saldo_quan_inizio_anno = saldo_quan_inizio_anno + :ld_saldo_quan_inizio_anno,   
       val_inizio_anno = val_inizio_anno + :ld_val_inizio_anno,   
		 saldo_quan_ultima_chiusura = saldo_quan_ultima_chiusura + :ld_saldo_quan_ultima_chius,
       prog_quan_entrata = prog_quan_entrata + :ld_prog_quan_entrata,   
       val_quan_entrata = val_quan_entrata + :ld_val_quan_entrata,   
       prog_quan_uscita = prog_quan_uscita + :ld_prog_quan_uscita,   
       val_quan_uscita = val_quan_uscita + :ld_val_quan_uscita,   
       prog_quan_acquistata = prog_quan_acquistata + :ld_prog_quan_acquistata,   
       val_quan_acquistata = val_quan_acquistata + :ld_val_quan_acquistata,   
       prog_quan_venduta = prog_quan_venduta + :ld_prog_quan_venduta,   
       val_quan_venduta = val_quan_venduta + :ld_val_quan_venduta,   
       costo_ultimo = :ld_costo_ultimo  
WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       ( anag_prodotti.cod_prodotto = :fs_cod_prodotto )   ;


if sqlca.sqlcode < 0 then
	fs_errore = "Errore in aggiornamento valori in anag_prodotti per prodotto "+fs_cod_prodotto+" : "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	fs_errore = "(4) Prodotto "+fs_cod_prodotto+" non trovato in anag_prodotti"
	return -1
	
end if

return 0
end function

public function integer uof_registra_movimento_mag (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_tipo_mov_det, string fs_flag_mov_manuale, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_prog_mov, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, boolean fb_primo_movimento, ref string fs_cod_deposito, ref string fs_cod_ubicazione, ref string fs_cod_lotto, ref datetime fdd_data_stock, ref long fl_prog_stock, ref string fs_cod_fornitore, ref string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione, long al_anno_documento, long al_anno_reg_mov_origine, long al_num_reg_mov_origine, ref string as_errore);//-------------------------------------------------------------------------------------
//	funzione di movimentazione prodotti a magazzino stock prodotti
//
//
//	redatta da: Enrico Menegotto
// data redazione: 10/6/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data registrazione
//		fs_cod_tipo_movimento		string				tipo movimento magazzino
//		fs_cod_tipo_mov_det			string				tipo configurazione movimento
//		fs_flag_mov_manuale			string				S=movimento manuale N=movimento automatico
//																	 generato da bolle/fatture ecc......
//		fs_cod_prodotto				string					
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fl_prog_mov						long					codice identificativo registrazione
//		fl_num_documento				long					riferimento al numero documento
//		fdd_data_documento			date					riferimento alla data documento
//		fs_referenza					string				riferimento libero
//		fb_primo_movimento			boolean				è il primo movimento ??
//		fs_cod_deposito				string				   \
//		fs_cod_ubicazione				string                \ 
//		fs_cod_lotto					string				     >  riferimento stock
//		fdd_data_stock					date					    /
//		fl_prog_stock					long					   /
//		fs_cod_fornitore				string				codice fornitore terzista
//		fs_cod_cliente					string				codice cliente   terzista
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_deposito				string				   \
//		fs_cod_ubicazione				string                \ 
//		fs_cod_lotto					string				     >  riferimento stock
//		fdd_data_stock					date					    /
//		fl_prog_stock					long					   /
//		fs_cod_fornitore				string				codice fornitore terzista
//		fs_cod_cliente					string				codice cliente terzista
// 	fl_anno_registrazione		long					registrazione movimento
//		fl_num_registrazione			long
//
//
//
//-------------------------------------------------------------------------------------

integer			li_ret

fd_quantita=round(fd_quantita,4)
fd_valore_unitario=round(fd_valore_unitario,4)


li_ret =  uof_aggiorna_prodotti(fs_cod_tipo_movimento, &
							  fs_cod_tipo_mov_det, &
							  fs_cod_prodotto, &
							  fd_quantita, &
							  fd_valore_unitario, &
							  fs_cod_fornitore, &
							  false, &
							  as_errore)
if li_ret = -1 then
	//as_errore il messaggio di errore
	return -1
end if

as_errore = ""

if  uof_aggiorna_stock(fs_cod_prodotto, &
							fs_cod_deposito, &
							fs_cod_ubicazione, &
							fs_cod_lotto, &
							fdd_data_stock, &
							fl_prog_stock, &
							fs_cod_tipo_movimento, &
							fs_cod_tipo_mov_det, &
							fs_cod_fornitore, &
							fs_cod_cliente, &
							fd_quantita, &
							fd_valore_unitario, &
							fb_primo_movimento,&
							false, &
							false)         = -1 then return -1

if uof_aggiorna_mov_magazzino(fdd_data_registrazione, &
						fs_cod_tipo_movimento, &
						fs_cod_tipo_mov_det, &
						fs_flag_mov_manuale, &
						fs_cod_prodotto, &
						fs_cod_deposito, &
						fs_cod_ubicazione, &
						fs_cod_lotto, &
						fdd_data_stock, &
						fl_prog_stock, &
						fd_quantita, &
						fd_valore_unitario, &
						fl_prog_mov, &
						fl_num_documento, &
						fdd_data_documento, &
						fs_referenza, &
						fb_primo_movimento, &
						fs_cod_cliente, &
						fs_cod_fornitore, &
						false, &
						fl_anno_registrazione, &
						fl_num_registrazione,&
						al_anno_documento,&
						al_anno_reg_mov_origine,&
						al_num_reg_mov_origine) = -1 then return -1

return 0
end function

public function integer uof_reparti_sl (integer ai_anno_reg_ord_ven, long al_num_reg_ord_ven, long al_prog_riga_ord_ven, ref string as_reparti[]);string					ls_cod_prodotto, ls_cod_versione, ls_cod_reparto, ls_cod_deposito_origine,&
						ls_flag_stampa_sempre, ls_vuoto[], ls_errore


select cod_prodotto, cod_versione
into		:ls_cod_prodotto, :ls_cod_versione
from det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda  and    
			anno_registrazione=:ai_anno_reg_ord_ven and
			num_registrazione=:al_num_reg_ord_ven and
			prog_riga_ord_ven =:al_prog_riga_ord_ven and
			cod_versione is not null;
		
select		cod_reparto, cod_deposito_origine, flag_stampa_sempre
into		:ls_cod_reparto, :ls_cod_deposito_origine, :ls_flag_stampa_sempre
from		tes_fasi_lavorazione
where	cod_azienda = :s_cs_xx.cod_azienda  and    
			cod_prodotto = :ls_cod_prodotto and
			cod_versione = :ls_cod_versione and
			flag_stampa_fase = 'S';

as_reparti[] = ls_vuoto[]

//if sqlca.sqlcode = 0 and ls_cod_deposito_origine<>"" and not isnull(ls_cod_deposito_origine) then
if sqlca.sqlcode = 0 then 
	if f_reparti_stabilimenti(		ai_anno_reg_ord_ven, al_num_reg_ord_ven, ls_cod_prodotto, ls_cod_versione, &
										ls_cod_reparto, ls_cod_deposito_origine, ls_flag_stampa_sempre, as_reparti[], ls_errore)<0 then
		return -1
	end if
end if

return 0
end function

public function integer uof_inventario_magazzino (datetime adt_data_riferimento, string as_cod_prodotto_da, string as_cod_prodotto_a, string as_cod_deposito, string as_flag_fiscale, string as_flag_lifo, string as_tipo_valorizzazione, ref decimal ad_result, ref string as_error);/**
 * stefanop
 * 19/09/2014
 *
 * Esegue l'inventario di magazzino con i parametri passati
 *
 * Ritorno: -1 errore, 0 ok
 **/

string ls_sql, ls_cod_prodotto, ls_where, ls_chiave[]
int li_return, li_cont
long ll_prodotti, ll_i
dec{4} ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_costo_prodotto, ld_giacenza, ld_costo_medio_continuo_stock[]
datastore lds_prodotti
uo_trova_prezzi luo_prezzi
uo_costo_medio_continuo luo_costo_medio

ls_sql = "select cod_prodotto, des_prodotto, cod_misura_mag from anag_prodotti " &
		+ " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " &
		+ " and cod_prodotto >= '" + as_cod_prodotto_da + "' " &
		+ " and cod_prodotto <= '" + as_cod_prodotto_a + "' "
		
if as_flag_fiscale="N" or as_flag_fiscale="S" then
	ls_sql += " and flag_articolo_fiscale = '"+as_flag_fiscale+"' "
end if

if as_flag_lifo="N" or as_flag_lifo="S" then
	ls_sql += " and flag_lifo='"+as_flag_lifo+"' "
end if

ll_prodotti = guo_functions.uof_crea_datastore(lds_prodotti, ls_sql, as_error)
if ll_prodotti < 0 then
	destroy lds_prodotti
	return -1
end if

// Reset variabili
ad_result = 0
ls_where = ""
is_considera_depositi_fornitori = "I" 
luo_prezzi = create uo_trova_prezzi
luo_costo_medio = create uo_costo_medio_continuo

// Impostazione dei depositi
if g_str.isnotempty(as_cod_deposito) and as_cod_deposito <> "%" then
	if not g_str.start_with(as_cod_deposito, "'") then
		as_cod_deposito = "'" + as_cod_deposito
	end if
	
	if not g_str.end_with(as_cod_deposito, "'") then
		as_cod_deposito += "'"
	end if
	
	is_cod_depositi_in = as_cod_deposito
end if

for ll_i = 1 to ll_prodotti
	
	ls_cod_prodotto = lds_prodotti.getitemstring(ll_i, "cod_prodotto")
	
	// clear
	for li_cont = 1 to 14
		ld_quant_val[li_cont] = 0
	next
	
	/**
	 * ld_quant_val : 
	 * [1] =quan_inizio_anno
	 * [2] =val_inizio_anno,  
	 * [3] =quan_ultima_chius, 
	 * [4] =qta_entrata,
	 * [5] =val_entrata,
	 * [6] =qta_uscita, 
	 * [7] =val_uscita, 
	 * [8] =qta_acq,
	 * [9] =val_acq,
	 * [10]=qta_ven,
	 * [11]=val_ven
	 **/
	li_return = uof_saldo_prod_date_decimal(ls_cod_prodotto, adt_data_riferimento, ls_where, ld_quant_val, as_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
	ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]

	choose case as_tipo_valorizzazione
			case "S"
				if luo_prezzi.uof_costo_standard( ls_cod_prodotto, ld_costo_prodotto, as_error) < 0 then
					destroy luo_prezzi
					destroy lds_prodotti
					destroy luo_costo_medio
					return -1
				end if
				
			case "A"
				//ultimo costo acquisto
				// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
				if ld_quant_val[14] >0 then
					ld_costo_prodotto = ld_quant_val[14]
				else
					ld_costo_prodotto = 0
				end if
				
			case "M"
				//costo medio di acquisto
				if ld_quant_val[12] > 0 then
					ld_costo_prodotto = ld_quant_val[13] / ld_quant_val[12]
				else
					select costo_medio_ponderato
					into :ld_costo_prodotto
					from lifo
					where cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto and
							 anno_lifo = (select max(anno_lifo)
											  from   lifo
											  where  cod_azienda = :s_cs_xx.cod_azienda and
														cod_prodotto = :ls_cod_prodotto);
																
					if sqlca.sqlcode < 0 then
						as_error = "Inventario prodotto: " + ls_cod_prodotto + "Errore in lettura tabella lifo: " + sqlca.sqlerrtext
						destroy luo_prezzi
						destroy lds_prodotti
						destroy luo_costo_medio
						return -1
					elseif sqlca.sqlcode = 100 or isnull(ld_costo_prodotto) then
						ld_costo_prodotto = 0
					end if
				end if
				
			case "C"
				// costo medio continuo
				if luo_costo_medio.uof_costo_medio_continuo(&
						ls_cod_prodotto, adt_data_riferimento, ref ld_giacenza, ref ld_costo_prodotto, ref ls_chiave[], &
						ref ld_giacenza_stock[], ref ld_costo_medio_continuo_stock[], "" ,ref as_error) < 0 then
						
					destroy luo_prezzi
					destroy lds_prodotti
					destroy luo_costo_medio
					return -1
				end if
						
		end choose
		
		ad_result += ld_giacenza * ld_costo_prodotto
	
next

destroy luo_prezzi
destroy lds_prodotti
destroy luo_costo_medio
 
return 0
end function

public function integer uof_costo_impegnato_ordine (integer ai_anno_registrazione, long al_num_registrazione, datetime ad_data_riferimento, integer ai_gg_prima, string as_flag_aperti_evasi, ref decimal ad_costo_mat_prime, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo valore materie prime ordine
 *
 * Ritorna: -1 errore, 0 tutto ok
 **/

string ls_sql, ls_flag_gen_commessa, ls_cod_prodotto, ls_tipo_costo
int li_anno_commessa, li_prog_riga_ord_ven
long ll_rows, ll_i, ll_num_commessa, ll_rows_commesse, ll_j
decimal{4} ld_quantita_iniziale, ld_prezzo_unitario, ld_imponibile_iva, ld_val_riga
datastore lds_store, lds_commesse
uo_trova_prezzi luo_prezzi

ad_costo_mat_prime = 0
ls_sql = "select det_ord_ven.prog_riga_ord_ven, det_ord_ven.imponibile_iva, det_ord_ven.flag_gen_commessa, det_ord_ven.anno_commessa, det_ord_ven.num_commessa, det_ord_ven.cod_prodotto, det_ord_ven.quan_ordine  from det_ord_ven " &
		+ "join tab_tipi_det_ven on " &
		+ "tab_tipi_det_ven.cod_azienda = det_ord_ven.cod_azienda and " &
		+ "tab_tipi_det_ven.cod_tipo_det_ven = det_ord_ven.cod_tipo_det_ven " &
		+ "where det_ord_ven.cod_azienda ='" + s_cs_xx.cod_azienda + "' and " &
		+ "det_ord_ven.anno_registrazione=" + string(ai_anno_registrazione) + " and " &
		+ "det_ord_ven.num_registrazione=" + string(al_num_registrazione) + " and " &
		+ "tab_tipi_det_ven.flag_tipo_det_ven <> 'N' and " &
		+ "det_ord_ven.flag_blocco='N' and "
if as_flag_aperti_evasi ="A" then 
	ls_sql += "det_ord_ven.flag_evasione IN ('A', 'P') "
else
	ls_sql += "det_ord_ven.flag_evasione ='E' "
end if

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)
luo_prezzi = create uo_trova_prezzi

if ib_log then 
	iuo_log.info("Elaboro ordine " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione))
	iuo_log.ident()
end if

for ll_i = 1 to ll_rows
	
	li_prog_riga_ord_ven = lds_store.getitemnumber(ll_i, 1)
	ld_imponibile_iva = lds_store.getitemnumber(ll_i, 2)
	ls_flag_gen_commessa = lds_store.getitemstring(ll_i, 3)
	li_anno_commessa = lds_store.getitemnumber(ll_i, 4)
	ll_num_commessa = lds_store.getitemnumber(ll_i, 5)
	ld_val_riga = 0
	
	if ib_log then
		iuo_log.info(g_str.format("Riga ordine $1, flag_gen_commessa=$2, commessa=$3/$4", li_prog_riga_ord_ven, ls_flag_gen_commessa, li_anno_commessa, ll_num_commessa))
		iuo_log.ident()
	end if
	
	// donato mi ha detto che non serve a controllare il flag_gen_commessa
	if ls_flag_gen_commessa = "S" and not isnull(li_anno_commessa) then
		// Elaboro commessa
		ls_sql = g_str.format("select cod_prodotto, quan_impegnata_iniziale from impegno_mat_prime_commessa where cod_azienda='$1' and anno_commessa=$2 and num_commessa=$3 and quan_impegnata_iniziale>0" , s_cs_xx.cod_azienda, li_anno_commessa, ll_num_commessa)
		ll_rows_commesse = guo_functions.uof_crea_datastore(lds_commesse, ls_sql, as_error)
		
		if ll_rows_commesse < 0 then
			if ib_log then iuo_log.outdent().outdent()
			return -1
		end if
		
		for ll_j = 1 to ll_rows_commesse
			ls_cod_prodotto = lds_commesse.getitemstring(ll_j, 1)
			ld_quantita_iniziale = lds_commesse.getitemnumber(ll_j, 2)
			ld_prezzo_unitario = 0
			
			if luo_prezzi.uof_prezzo_prodotto(ls_cod_prodotto, ad_data_riferimento, ai_gg_prima, ld_prezzo_unitario, ls_tipo_costo, as_error) < 0 then
				destroy lds_commesse
				destroy lds_store
				destroy luo_prezzi
				if ib_log then iuo_log.outdent().outdent()
				return -1
			end if
			
			if ib_log then iuo_log.info(g_str.format("Prodotto $1, quantita=$2, costo unitario=$3, tipo=$4", ls_cod_prodotto, ld_quantita_iniziale, ld_prezzo_unitario, ls_tipo_costo))
						
			ld_val_riga += (ld_prezzo_unitario * ld_quantita_iniziale)
		next
		
		destroy lds_commesse
		
	elseif ls_flag_gen_commessa = "S" and (isnull(li_anno_commessa) or li_anno_commessa < 1900) then
		// SALTO LA CAZZO DI RIGA
		if ib_log then iuo_log.outdent()
		continue
	else
		// Elaboro riga come sfuso
		ls_cod_prodotto = lds_store.getitemstring(ll_i, 6)
		ld_quantita_iniziale = lds_store.getitemnumber(ll_i, 7)
		ld_prezzo_unitario = 0
		
		if g_str.isnotempty(ls_cod_prodotto) then
			if luo_prezzi.uof_prezzo_prodotto(ls_cod_prodotto, ad_data_riferimento, ai_gg_prima, ld_prezzo_unitario, ls_tipo_costo, as_error) < 0 then
				destroy lds_store
				destroy luo_prezzi
				return -1
			end if
		end if
					
		ld_val_riga += (ld_prezzo_unitario * ld_quantita_iniziale)
		
		if ib_log then iuo_log.info(g_str.format("Prodotto $1, quantita=$2, costo unitario=$3, tipo=$4", ls_cod_prodotto, ld_quantita_iniziale, ld_prezzo_unitario, ls_tipo_costo))
	end if
	
	ad_costo_mat_prime += ld_val_riga
	
	if ib_log then
		if ld_val_riga >= ld_imponibile_iva then
			iuo_log.warning(g_str.format("*** Costo impegnato: $1, Imponibile iva: $2", ld_val_riga, ld_imponibile_iva))
		else
			iuo_log.info(g_str.format("Costo impegnato: $1, Imponibile iva: $2", ld_val_riga, ld_imponibile_iva))
		end if
		
		iuo_log.outdent()	
	end if
next

if ib_log then iuo_log.outdent()
 
destroy lds_store
destroy luo_prezzi

return 0
end function

public function decimal uof_costo_impegnato_ordini (datetime adt_data_da, datetime adt_data_a, string as_flag_aperti_evasi, ref decimal ad_costo_impegnato, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo l'impegnato degli ordini tra le due date
 *
 * Ritorno: -1 errore, 0 ok
 **/
 
string ls_sql
long ll_rows, ll_i, ll_giorni_prima, ll_anno, ll_num
decimal{4} ld_costo_ordine
datastore lds_store

if ib_log then
	iuo_log = create uo_log
	iuo_log.create_to_desktop("costo_impegnato_ordini.txt")
end if

ad_costo_impegnato = 0

ls_sql = "SELECT anno_registrazione, num_registrazione FROM tes_ord_ven WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and flag_blocco ='N' " &
		+ "AND data_registrazione>='" + guo_functions.uof_data_db(adt_data_da) + "' AND " &
		+ "data_registrazione<='" + guo_functions.uof_data_db(adt_data_a) + "' "
		
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

if ib_log then iuo_log.info("Trovati " + string(ll_rows) + " ordini da elaborare....")

for ll_i = 1 to ll_rows
	 
	 ll_anno = lds_store.getitemnumber(ll_i, 1)
	 ll_num = lds_store.getitemnumber(ll_i, 2)
	 ld_costo_ordine = 0
	 
	if uof_costo_impegnato_ordine(ll_anno, ll_num, adt_data_a, 365, as_flag_aperti_evasi, ld_costo_ordine, as_error) < 0 then
		as_error = g_str.format("Errore durante il calcolo costo impegnato per l'ordine $1/$2", ll_anno, ll_num) + " " + g_str.safe(as_error)
		if ib_log then iuo_log.error(as_error)
		destroy lds_store
		destroy iuo_log
		return -1
	end if
	
	ad_costo_impegnato += ld_costo_ordine
	if ib_log then iuo_log.info("Costo impegnato totale: " + string(ad_costo_impegnato, "###,###,###,##0.0000"))

	
next

destroy iuo_log
destroy lds_store

return 0
end function

public function decimal uof_costo_fatturato (datetime adt_data_da, datetime adt_data_a, ref decimal ad_costo_fatturato, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo l'impegnato degli ordini tra le due date
 *
 * Ritorno: -1 errore, 0 ok
 **/
 
string ls_sql, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven
long ll_rows, ll_i, ll_giorni_prima, ll_anno, ll_num
decimal{4} ld_costo_ordine
datastore lds_store

if ib_log then
	iuo_log = create uo_log
	iuo_log.create_to_desktop("costo_fatturato_ordini.txt")
end if

ad_costo_fatturato = 0

// fatture proforma sono sempre escluse.

ls_sql = "SELECT anno_registrazione, num_registrazione, cod_tipo_fat_ven  FROM tes_fat_ven WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and flag_blocco ='N' and cod_documento is not null " &
		+ "and data_fattura >='" + guo_functions.uof_data_db(adt_data_da) + "' and data_fattura <='" + guo_functions.uof_data_db(adt_data_a) + "' " &
		+ "and cod_tipo_fat_ven in (select cod_tipo_fat_ven from tab_tipi_fat_ven where cod_azienda='" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven in ('I','D','N') ) "
		
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

if ib_log then iuo_log.info("Trovate " + string(ll_rows) + " fatture da elaborare....")

for ll_i = 1 to ll_rows
	 
	 ll_anno = lds_store.getitemnumber(ll_i, 1)
	 ll_num = lds_store.getitemnumber(ll_i, 2)
	 ls_cod_tipo_fat_ven = lds_store.getitemstring(ll_i,3)
	 ld_costo_ordine = 0
	 
	if uof_costo_fatturato_fattura(ll_anno, ll_num, adt_data_a, 365, ld_costo_ordine, as_error) < 0 then
		as_error = g_str.format("Errore durante il calcolo costo fatturato per la fattura $1/$2", ll_anno, ll_num) + " " + g_str.safe(as_error)
		if ib_log then iuo_log.error(as_error)
		destroy lds_store
		destroy iuo_log
		return -1
	end if 
	
	ls_flag_tipo_fat_ven=""
	
	select 	flag_tipo_fat_ven
	into		:ls_flag_tipo_fat_ven
	from		tab_tipi_fat_ven
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	// se NC metto in negativo
	if ls_flag_tipo_fat_ven = "N" then ld_costo_ordine = ld_costo_ordine * -1
	
	ad_costo_fatturato += ld_costo_ordine
	if ib_log then iuo_log.info("Costo fatturato totale: " + string(ad_costo_fatturato, "###,###,###,##0.0000"))
	
next

destroy iuo_log
destroy lds_store

return 0
end function

public function integer uof_costo_fatturato_fattura (integer ai_anno_registrazione, long al_num_registrazione, datetime ad_data_riferimento, integer ai_gg_prima, ref decimal ad_costo_mat_prime, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo valore materie prime ordine
 *
 * Ritorna: -1 errore, 0 tutto ok
 **/

string ls_sql, ls_cod_prodotto, ls_tipo_costo
int li_anno_commessa, li_prog_riga_ord_ven
long ll_rows, ll_i, ll_num_commessa, ll_rows_commesse, ll_j, ll_anno_reg_ord_ven,ll_num_reg_ord_ven
decimal{4} ld_quantita_iniziale, ld_prezzo_unitario, ld_imponibile_iva, ld_val_riga
datastore lds_store, lds_commesse
uo_trova_prezzi luo_prezzi

ad_costo_mat_prime = 0
ls_sql = "select det_fat_ven.anno_reg_ord_ven, det_fat_ven.num_reg_ord_ven, det_fat_ven.prog_riga_ord_ven, det_fat_ven.imponibile_iva, det_fat_ven.anno_commessa, det_fat_ven.num_commessa, det_fat_ven.cod_prodotto, det_fat_ven.quan_fatturata from det_fat_ven " &
		+ "join tab_tipi_det_ven on " &
		+ "tab_tipi_det_ven.cod_azienda = det_fat_ven.cod_azienda and tab_tipi_det_ven.cod_tipo_det_ven = det_fat_ven.cod_tipo_det_ven " &
		+ "where det_fat_ven.cod_azienda ='" + s_cs_xx.cod_azienda + "' and " &
		+ "det_fat_ven.anno_registrazione=" + string(ai_anno_registrazione) + " and " &
		+ "det_fat_ven.num_registrazione=" + string(al_num_registrazione) + " and " &
		+ "tab_tipi_det_ven.flag_tipo_det_ven <> 'N'  " 

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)

if ll_rows < 0 then
	return -1
end if
	
luo_prezzi = create uo_trova_prezzi

if ib_log then 
	iuo_log.info("Elaboro fattura " + string(ai_anno_registrazione) + "/" + string(al_num_registrazione))
	iuo_log.ident()
end if

for ll_i = 1 to ll_rows
	
	ll_anno_reg_ord_ven =  lds_store.getitemnumber(ll_i, 1)
	ll_num_reg_ord_ven =  lds_store.getitemnumber(ll_i, 2)
	li_prog_riga_ord_ven = lds_store.getitemnumber(ll_i, 3)
	ld_imponibile_iva = lds_store.getitemnumber(ll_i, 4)
	li_anno_commessa = lds_store.getitemnumber(ll_i, 5)
	ll_num_commessa = lds_store.getitemnumber(ll_i, 6)
	ld_val_riga = 0

	if ib_log then
		iuo_log.info(g_str.format("Riga fattura $1, commessa=$3/$4", li_prog_riga_ord_ven, li_anno_commessa, ll_num_commessa))
		iuo_log.ident()
	end if
	
	// donato mi ha detto che non serve a controllare il flag_gen_commessa
	if not isnull(li_anno_commessa) then
		// Elaboro commessa
		ls_sql = g_str.format("select cod_prodotto, quan_impegnata_iniziale from impegno_mat_prime_commessa where cod_azienda='$1' and anno_commessa=$2 and num_commessa=$3 and quan_impegnata_iniziale>0" , s_cs_xx.cod_azienda, li_anno_commessa, ll_num_commessa)
		ll_rows_commesse = guo_functions.uof_crea_datastore(lds_commesse, ls_sql, as_error)
		
		if ll_rows_commesse < 0 then
			if ib_log then iuo_log.outdent().outdent()
			return -1
		end if
		
		for ll_j = 1 to ll_rows_commesse
			ls_cod_prodotto = lds_commesse.getitemstring(ll_j, 1)
			ld_quantita_iniziale = lds_commesse.getitemnumber(ll_j, 2)
			ld_prezzo_unitario = 0
			
			if luo_prezzi.uof_prezzo_prodotto(ls_cod_prodotto, ad_data_riferimento, ai_gg_prima, ld_prezzo_unitario, ls_tipo_costo, as_error) < 0 then
				destroy lds_commesse
				destroy lds_store
				destroy luo_prezzi
				if ib_log then iuo_log.outdent().outdent()
				return -1
			end if
			
			if ib_log then iuo_log.info(g_str.format("Prodotto $1, quantita=$2, costo unitario=$3, tipo=$4", ls_cod_prodotto, ld_quantita_iniziale, ld_prezzo_unitario, ls_tipo_costo))
						
			ld_val_riga += (ld_prezzo_unitario * ld_quantita_iniziale)
		next
		destroy lds_commesse
		
	else
		// Elaboro riga come sfuso
		ls_cod_prodotto = lds_store.getitemstring(ll_i, 7)
		ld_quantita_iniziale = lds_store.getitemnumber(ll_i, 8)
		ld_prezzo_unitario = 0
		
		if g_str.isnotempty(ls_cod_prodotto) then
			if luo_prezzi.uof_prezzo_prodotto(ls_cod_prodotto, ad_data_riferimento, ai_gg_prima, ld_prezzo_unitario, ls_tipo_costo, as_error) < 0 then
				destroy lds_store
				destroy luo_prezzi
				return -1
			end if
		end if
					
		ld_val_riga += (ld_prezzo_unitario * ld_quantita_iniziale)
		
		if ib_log then iuo_log.info(g_str.format("Prodotto $1, quantita=$2, costo unitario=$3, tipo=$4", ls_cod_prodotto, ld_quantita_iniziale, ld_prezzo_unitario, ls_tipo_costo))
	end if
	
	ad_costo_mat_prime += ld_val_riga
	
	if ib_log then
		if ld_val_riga >= ld_imponibile_iva then
			iuo_log.warning(g_str.format("*** Costo impegnato: $1, Imponibile iva: $2", ld_val_riga, ld_imponibile_iva))
		else
			iuo_log.info(g_str.format("Costo impegnato: $1, Imponibile iva: $2", ld_val_riga, ld_imponibile_iva))
		end if
		
		iuo_log.outdent()	
	end if
next

if ib_log then iuo_log.outdent()
 
destroy lds_store
destroy luo_prezzi

return 0
end function

public subroutine uof_set_flag_commessa (string as_flag_commessa);
if as_flag_commessa<>"S" and as_flag_commessa<>"N" then
	as_flag_commessa = "N"
end if


is_flag_commessa = as_flag_commessa
end subroutine

public function integer uof_modifica_movimento (datetime fdd_data_registrazione, string fs_cod_tipo_movimento, string fs_cod_prodotto, decimal fd_quantita, decimal fd_valore_unitario, long fl_num_documento, datetime fdd_data_documento, string fs_referenza, ref string fs_cod_deposito[], ref string fs_cod_ubicazione[], ref string fs_cod_lotto[], ref datetime fdd_data_stock[], ref long fl_prog_stock[], ref string fs_cod_fornitore[], ref string fs_cod_cliente[], ref long fl_anno_registrazione[], ref long fl_num_registrazione[]);//-------------------------------------------------------------------------------------
//	funzione di modifica movimenti di magazzino stock prodotti
//
//
//	redatta da: Enrico Menegotto
// data redazione: 2/7/97
//
// valori restituiti dalla funzione:  		 0 = tutto OK
//   													-1 = si è verificato un errore
//
// variabili in ingresso:			tipo di dato		commento
// ------------------------------------------------------------------------------
//		fdd_data_registrazione		date					data registrazione movimento
//		fs_cod_tipo_movimento		string				tipo movimento magazzino
//		fs_cod_prodotto				string					
//		fd_quantita						double				quantità movimento
//		fd_valore_unitario			double				costo dell'unità di movimentazione
//		fl_num_documento				long					numero documento riferimento
//		fdd_data_documento			date					data documento riferimento
//		fs_referenza					string				riferimento
//		fs_cod_deposito[1]			string				   \
//		fs_cod_ubicazione[1]			string                \ 
//		fs_cod_lotto[1]				string				     >  riferimento stock
//		fdd_data_stock[1]				date					    /
//		fl_prog_stock[1]				long					   /
//		fs_cod_fornitore[1]			string				codice fornitore terzista
//		fs_cod_cliente[1]				string				codice cliente   terzista
// 	fl_anno_registrazione[1]	long					movimento da modificare
//		fl_num_registrazione[1]		long
//
// variabili in uscita:				tipo di dato		commento
// ------------------------------------------------------------------------------
//		fs_cod_deposito[]				string				   \
//		fs_cod_ubicazione[]			string                \ 
//		fs_cod_lotto[]					string				     >  riferimento stock
//		fdd_data_stock[]				date					    /
//		fl_prog_stock[]				long					   /
//		fs_cod_fornitore[]			string				codice fornitore terzista
//		fs_cod_cliente[]				string				codice cliente   terzista
// 	fl_anno_registrazione[]		long					registrazioni dei movimento
//		fl_num_registrazione[]		long
//
//
//
//-------------------------------------------------------------------------------------

boolean  		lb_primo_movimento
string   		ls_cod_cliente, ls_cod_fornitore, ls_cod_prodotto, ls_cod_deposito, ls_cod_lotto, &
         		ls_cod_ubicazione, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_cod_tipo_mov_det_1[],&
		   		ls_sql, ls_str, ls_str_1, ls_cod_tipo_movimento_1[]
datetime		ldd_data_stock
long     		ll_i, ll_y, ll_x, ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_prog_mov, ll_null
dec{4}   		ld_quan_movimento ,ld_val_movimento
datastore 	lds_mov_magazzino

select mov_magazzino.prog_mov  
into   :ll_prog_mov  
from   mov_magazzino
where  (mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda )   and
       (mov_magazzino.anno_registrazione = :fl_anno_registrazione[1] ) and
       (mov_magazzino.num_registrazione = :fl_num_registrazione[1] )   ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Impossibile trovare movimento magazzino indicato",StopSign!)
	return -1
end if

if ll_prog_mov < 1 or isnull(ll_prog_mov) then
	g_mb.messagebox("APICE","Progressivo Movimento ( prog_mov ) non corretto oppure non indicato", StopSign!)
	return -1
end if

setpointer(hourglass!)
lds_mov_magazzino = create datastore
lds_mov_magazzino.dataobject = "d_mov_magazzino"

lds_mov_magazzino.settransobject(sqlca)
ll_y = lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, ll_prog_mov, fl_anno_registrazione[1])
ll_x = ll_y
if ll_y < 1 then
	g_mb.messagebox("APICE","Nessun movimento presente con il numero registrazione " + string(ll_prog_mov),StopSign!)
	goto ERRORE
end if

//  -----------------  ripristino valori in stock e prodotti  -----------------------------
//  ------------- passando una ad una le righe movimenti e stornando ----------------------

for ll_i = ll_y to 1 step -1
	ls_cod_prodotto   = lds_mov_magazzino.object.cod_prodotto[ll_i]
	ls_cod_deposito   = lds_mov_magazzino.object.cod_deposito[ll_i]
	ls_cod_ubicazione = lds_mov_magazzino.object.cod_ubicazione[ll_i]
	ls_cod_lotto      = lds_mov_magazzino.object.cod_lotto[ll_i]
	ldd_data_stock    = lds_mov_magazzino.object.data_stock[ll_i]
	ll_prog_stock     = lds_mov_magazzino.object.prog_stock[ll_i]
	ls_cod_tipo_movimento = lds_mov_magazzino.object.cod_tipo_movimento[ll_i]
	ls_cod_tipo_mov_det   = lds_mov_magazzino.object.cod_tipo_mov_det[ll_i]
	ld_quan_movimento = lds_mov_magazzino.object.quan_movimento[ll_i]
	ld_val_movimento  = lds_mov_magazzino.object.val_movimento[ll_i]
	ls_cod_cliente    = lds_mov_magazzino.object.cod_cliente[ll_i]
	ls_cod_fornitore  = lds_mov_magazzino.object.cod_fornitore[ll_i]
	ll_anno_registrazione = lds_mov_magazzino.object.anno_registrazione[ll_i]
	ll_num_registrazione = lds_mov_magazzino.object.num_registrazione[ll_i]
	
	
	if uof_aggiorna_stock (ls_cod_prodotto, &
								ls_cod_deposito, &
								ls_cod_ubicazione, &
								ls_cod_lotto, &
								ldd_data_stock, &
								ll_prog_stock, &
								ls_cod_tipo_movimento, &
								ls_cod_tipo_mov_det, &
								ls_cod_fornitore, &
								ls_cod_cliente, &
								ld_quan_movimento, &
								ld_val_movimento, &
								false, &
								true, &
								true) = -1 then 
		ROLLBACK;
		goto ERRORE
	end if
	
	if uof_aggiorna_prodotti ( ls_cod_tipo_movimento, &
									 ls_cod_tipo_mov_det, &
									 ls_cod_prodotto, &
									 ld_quan_movimento, &
									 ld_val_movimento, &
									 ls_cod_fornitore, &
									 true ) = -1 then 
		ROLLBACK;
		goto ERRORE
	end if
	
next


//  eseguo movimenti corretti   ------------------------------------------------------------

setnull(ls_cod_cliente)
setnull(ls_cod_fornitore)
setnull(ls_cod_deposito)
setnull(ls_cod_ubicazione)
setnull(ls_cod_lotto)
lb_primo_movimento = true
ll_y = 0
ll_i = 0

declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.cod_tipo_mov_det, det_tipi_movimenti.cod_tipo_movimento FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + fs_cod_tipo_movimento + "')  ORDER BY ordinamento"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;
do while 1=1
   fetch cu_det_movimenti into :ls_str, :ls_str_1;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_y ++
	ls_cod_tipo_mov_det_1[ll_y] = ls_str
	ls_cod_tipo_movimento_1[ll_y] = ls_str_1
loop
close cu_det_movimenti;

if ll_y < 1 then
	g_mb.messagebox("APICE","Nessun dettaglio per il tipo movimento " + fs_cod_tipo_movimento, StopSign!)
	ROLLBACK;
	goto ERRORE
end if

if isnull(fs_cod_deposito[1]) then
	g_mb.messagebox("APICE","Impossibile eseguire un movimento senza il deposito",StopSign!)
	ROLLBACK;
	goto ERRORE
else
	ls_cod_deposito = fs_cod_deposito[1]
end if

if isnull(fs_cod_ubicazione[1]) then
	g_mb.messagebox("APICE","Impossibile eseguire un movimento senza codice ubicazione",StopSign!)
	ROLLBACK;
	goto ERRORE
else
	ls_cod_ubicazione = fs_cod_ubicazione[1]
end if

if isnull(fs_cod_lotto[1]) then
	g_mb.messagebox("APICE","Impossibile eseguire un movimento senza codice lotto",StopSign!)
	ROLLBACK;
	goto ERRORE
else
	ls_cod_lotto = fs_cod_lotto[1]
end if

for ll_i = 1 to ll_y

	if ll_i = 1 then
		lb_primo_movimento = true
		ls_cod_deposito = fs_cod_deposito[1]
		ls_cod_ubicazione = fs_cod_ubicazione[1]
		ls_cod_lotto = fs_cod_lotto[1]
		ldd_data_stock = fdd_data_stock[1]
		ll_prog_stock = fl_prog_stock[1]
		ls_cod_cliente = fs_cod_cliente[1]
		ls_cod_fornitore = fs_cod_fornitore[1]
	else
		lb_primo_movimento = false
		ls_cod_deposito   = lds_mov_magazzino.object.cod_deposito[ll_i]
		ls_cod_ubicazione = lds_mov_magazzino.object.cod_ubicazione[ll_i]
		ls_cod_lotto      = lds_mov_magazzino.object.cod_lotto[ll_i]
		ldd_data_stock    = lds_mov_magazzino.object.data_stock[ll_i]
		ll_prog_stock     = lds_mov_magazzino.object.prog_stock[ll_i]
		ls_cod_cliente    = lds_mov_magazzino.object.cod_cliente[ll_i]
		ls_cod_fornitore  = lds_mov_magazzino.object.cod_fornitore[ll_i]
	end if
	
	if uof_aggiorna_prodotti(ls_cod_tipo_movimento_1[ll_i], &
								  ls_cod_tipo_mov_det_1[ll_i], &
								  fs_cod_prodotto, &
								  fd_quantita, &
								  fd_valore_unitario, &
								  ls_cod_fornitore, &
								  false)      = -1 then
		ROLLBACK;
		goto ERRORE
	end if
	
	if ll_i <= ll_x then
		
		if uof_aggiorna_stock(fs_cod_prodotto, &
									ls_cod_deposito, &
									ls_cod_ubicazione, &
									ls_cod_lotto, &
									ldd_data_stock, &
									ll_prog_stock, &
									fs_cod_tipo_movimento, &
									ls_cod_tipo_mov_det_1[ll_i], &
									ls_cod_fornitore, &
									ls_cod_cliente, &
									fd_quantita, &
									fd_valore_unitario, &
									lb_primo_movimento,&
									false, &
									true)         = -1 then
			ROLLBACK;
			goto ERRORE
		end if
		
		ll_anno_registrazione = lds_mov_magazzino.object.anno_registrazione[ll_i]
		ll_num_registrazione  = lds_mov_magazzino.object.num_registrazione[ll_i]
		
		if uof_aggiorna_mov_magazzino(fdd_data_registrazione, &
								fs_cod_tipo_movimento, &
								ls_cod_tipo_mov_det_1[ll_i], &
								"S", &
								fs_cod_prodotto, &
								ls_cod_deposito, &
								ls_cod_ubicazione, &
								ls_cod_lotto, &
								ldd_data_stock, &
								ll_prog_stock, &
								dec(fd_quantita), &
								dec(fd_valore_unitario), &
								ll_prog_mov, &
								fl_num_documento, &
								fdd_data_documento, &
								fs_referenza, &
								lb_primo_movimento, &
								ls_cod_cliente, &
								ls_cod_fornitore, &
								true, &
								ll_anno_registrazione, &
								ll_num_registrazione,&
								ll_null,&
								ll_null,&
								ll_null) = -1 then
			ROLLBACK;
			goto ERRORE
		end if
		
	else
		
		if  uof_aggiorna_stock(fs_cod_prodotto, &
									ls_cod_deposito, &
									ls_cod_ubicazione, &
									ls_cod_lotto, &
									ldd_data_stock, &
									ll_prog_stock, &
									fs_cod_tipo_movimento, &
									ls_cod_tipo_mov_det_1[ll_i], &
									ls_cod_fornitore, &
									ls_cod_cliente, &
									fd_quantita, &
									fd_valore_unitario, &
									lb_primo_movimento,&
									false, &
									false)         = -1 then
			ROLLBACK;
			goto ERRORE
		end if
		
		if uof_aggiorna_mov_magazzino(fdd_data_registrazione, &
								fs_cod_tipo_movimento, &
								ls_cod_tipo_mov_det_1[ll_i], &
								"S", &
								fs_cod_prodotto, &
								ls_cod_deposito, &
								ls_cod_ubicazione, &
								ls_cod_lotto, &
								ldd_data_stock, &
								ll_prog_stock, &
								fd_quantita, &
								fd_valore_unitario, &
								ll_prog_mov, &
								fl_num_documento, &
								fdd_data_documento, &
								fs_referenza, &
								lb_primo_movimento, &
								ls_cod_cliente, &
								ls_cod_fornitore, &
								false, &
								ll_anno_registrazione, &
								ll_num_registrazione,&
								ll_null, ll_null, ll_null) = -1 then
			ROLLBACK;
			goto ERRORE
		end if
	end if
	

	lb_primo_movimento = false
	fs_cod_deposito[ll_y]       = ls_cod_deposito
	fs_cod_ubicazione[ll_y]     = ls_cod_ubicazione
	fs_cod_lotto[ll_y]          = ls_cod_lotto
	fdd_data_stock[ll_y]        = ldd_data_stock
	fl_prog_stock[ll_y]         = ll_prog_stock
	fs_cod_fornitore[ll_y]      = ls_cod_fornitore
	fs_cod_cliente[ll_y]        = ls_cod_cliente
	fl_anno_registrazione[ll_y] = ll_anno_registrazione
	fl_num_registrazione[ll_y]  = ll_num_registrazione
next
destroy lds_mov_magazzino
setpointer(arrow!)
return 0


// --------------------------- uscita per causa errore -------------------------------------
// ho usato il comando goto per non ripetere lo stesso codice decine di volte; percui quelli
// standard non devono rognare: il goto esisterà pure per qualche motivo.
// Sì, esiste per i pajassi integrali!
// -----------------------------------------------------------------------------------------

ERRORE:
	destroy lds_mov_magazzino
	setpointer(arrow!)
	return -1

end function

on uo_magazzino.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_magazzino.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;string				ls_flag_negativo, ls_flag_mov_raggruppo

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
	ib_negativo = false
elseif sqlca.sqlcode = 100 or ls_flag_negativo <> 'S' or isnull(ls_flag_negativo) then
	ib_negativo = false
elseif sqlca.sqlcode = 0 and ls_flag_negativo = 'S' then
	ib_negativo = true
end if

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
ib_flag_agg_costo_ultimo = false


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Disattiva Movimento Raggruppato (SR Prodotto_raggruppato per Gibus)
select flag
into   :ls_flag_mov_raggruppo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DMR' and
		 flag_parametro = 'F';

if sqlca.sqlcode=0 and ls_flag_mov_raggruppo="S" then
	ib_mov_raggruppato = false
else
	//condizione di default: esegui movimento raggruppato
	ib_mov_raggruppato = true
end if



end event

