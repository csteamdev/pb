﻿$PBExportHeader$uo_ref_mov_mag.sru
$PBExportComments$UO per ricerca documenti reerenziati ai movimenti di magazzino
forward
global type uo_ref_mov_mag from nonvisualobject
end type
end forward

global type uo_ref_mov_mag from nonvisualobject
end type
global uo_ref_mov_mag uo_ref_mov_mag

forward prototypes
public function integer uof_ref_mov_mag (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_return)
public function integer uof_prodotto_ref_mov_mag (long al_anno_registrazione, long al_num_registrazione, ref string as_cod_prodotto, ref string as_errore)
public function integer uof_ref_mov_mag (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore)
public function integer uof_movimenti_per_cli_for (long fl_anno_reg_mov, long fl_num_reg_mov, string fs_cod_cliente, string fs_cod_fornitore, ref string fs_errore)
public function integer uof_ref_mov_mag_prog_mov (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore)
public function integer uof_ref_mov_mag_commessa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore)
end prototypes

public function integer uof_ref_mov_mag (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_return);string ls_smal_doc_ref, ls_error

return this.uof_ref_mov_mag(fl_anno_registrazione, fl_num_registrazione, ref fs_return, ref ls_smal_doc_ref, ref ls_error)

end function

public function integer uof_prodotto_ref_mov_mag (long al_anno_registrazione, long al_num_registrazione, ref string as_cod_prodotto, ref string as_errore);/**
 * stefanop
 * 21/06/2010
 *
 * Recupero codice prodotto associato alla fattura o bolla di acquisto
 **/
 
string ls_cod_prodotto, ls_tipo
long	ll_anno_bol_acq, ll_num_bol_acq, ll_prog_riga_bol_acq, ll_anno_reg_bol_acq

setnull(as_cod_prodotto)

select cod_prodotto, anno_bolla_acq, num_bolla_acq, prog_riga_bolla_acq
into :ls_cod_prodotto, :ll_anno_bol_acq, :ll_num_bol_acq, :ll_prog_riga_bol_acq
from  det_fat_acq  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione_mov_mag = :al_anno_registrazione and  
		 num_registrazione_mov_mag = :al_num_registrazione ;
	
if sqlca.sqlcode < 0 then
	as_errore = "Errore durante il controllo del dettaglio fatture acquisto"
	return -1
elseif sqlca.sqlcode = 100 then // non ho trovato la fattura, provo con la bolla
	
	select cod_prodotto
	into :ls_cod_prodotto
	from  det_bol_acq  
	where 
		cod_azienda = :s_cs_xx.cod_azienda and  
		anno_registrazione_mov_mag = :al_anno_registrazione and
		num_registrazione_mov_mag = :al_num_registrazione ;
		
	if sqlca.sqlcode < 0 then
		as_errore = "Errore durante il controllo del dettglio bolle acquisto"
		return -1
	elseif sqlca.sqlcode = 100 then // non ho trovato neanche la bolla..questo caso non dovrebbe mai capitare
		return 1
	else
		as_cod_prodotto = ls_cod_prodotto
		return 0
	end if
	
else // trovata la fattura, ma controllo se è stata generata da una bolla
	if not isnull(ll_anno_bol_acq) and not isnull(ll_num_bol_acq) and not isnull(ll_prog_riga_bol_acq) then // controllo la bolla
		
		select cod_prodotto
		into :ls_cod_prodotto
		from  det_bol_acq  
		where
			cod_azienda = :s_cs_xx.cod_azienda and  
			anno_bolla_acq = :ll_anno_bol_acq and
			num_bolla_acq = :ll_num_bol_acq and
			prog_riga_bolla_acq = :ll_prog_riga_bol_acq;
			
		if sqlca.sqlcode < 0 then
			as_errore = "Errore durante il controllo del dettglio bolle acquisto"
			return -1
		elseif sqlca.sqlcode = 100 then // non ho trovato neanche la bolla..questo caso non dovrebbe mai capitare
			return 1
		else
			as_cod_prodotto = ls_cod_prodotto
			return 0
		end if
	else
		
		as_cod_prodotto = ls_cod_prodotto
		return 0
	end if
end if

return -1
end function

public function integer uof_ref_mov_mag (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore);/*						FUNZIONE DI RICERCA REFERENZE DEL MOVIMENTO DI MAGAZZINO
							--------------------------------------------------------							

fl_anno_registrazione		long		anno registrazione del movimento di magazzino
fl_num_registrazione			long		numero di registrazione del movimento di magazzino

Questa ricerca tutte (o per meglio dire quelle previste dalla funzione) i punti in cui il movimento di magazzino
è stato referenziato fornendo qualche informazione sulla rintracciabilità del dato. */


datetime ldt_data_bolla, ldt_data_fattura, ldt_data_reg

string 	ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_fornitore, ls_cod_documento, ls_numeratore_documento, &
			ls_rag_soc_1, ls_flag_tipo_bol_ven, ls_flag_tipo_fat_ven, ls_cod_tipo_fat_ven, ls_cod_tipo_bol_acq, &
			ls_flag_tipo_bol_acq, ls_num_bolla_fornitore,ls_flag_agg_mov, ls_line, ls_cli_for, ls_title, ls_cod_prodotto, ls_flag_commessa
			
long 	 	ll_anno_reg_bol_ven, ll_num_reg_bol_ven, ll_prog_riga_bol_ven, ll_anno_documento, ll_num_documento, &
			ll_anno_reg_fat_ven, ll_num_reg_fat_ven, ll_prog_riga_fat_ven, ll_anno_reg_bol_acq, ll_num_reg_bol_acq, &
			ll_prog_riga_bol_acq, ll_anno_reg_fat_acq, ll_num_reg_fat_acq, ll_prog_riga_fat_acq, ll_prog_riga_ricambio, &
			ll_anno_man, ll_num_man
			
boolean	lb_first_title = true

fs_errore = ""
fs_full_doc_ref = ""
fs_small_doc_ref = "" 


//COMMESSE PRODUZIONE
//NOTA: se trovi qualcosa esci subito dalla funzione ....
select flag_commessa
into	:ls_flag_commessa
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

if ls_flag_commessa="S" then
	//non mi interessa gestire l'errore ...
	uof_ref_mov_mag_commessa(fl_anno_registrazione, fl_num_registrazione, fs_full_doc_ref, fs_small_doc_ref, fs_errore)
	
	//quindi esci subito
	return 0
end if


// BOLLE DI VENDITA
SELECT anno_registrazione,   
		 num_registrazione,   
		 prog_riga_bol_ven
 INTO  :ll_anno_reg_bol_ven,   
		 :ll_num_reg_bol_ven,   
		 :ll_prog_riga_bol_ven
 FROM  det_bol_ven  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione_mov_mag = :fl_anno_registrazione AND  
		 num_registrazione_mov_mag = :fl_num_registrazione ;
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 0 then

	SELECT cod_tipo_bol_ven,   
			cod_cliente,   
			cod_fornitore,   
			cod_documento,   
			numeratore_documento,   
			anno_documento,   
			num_documento,   
			data_bolla,
			data_registrazione
	 INTO :ls_cod_tipo_bol_ven,   
			:ls_cod_cliente,   
			:ls_cod_fornitore,   
			:ls_cod_documento,   
			:ls_numeratore_documento,   
			:ll_anno_documento,   
			:ll_num_documento,   
			:ldt_data_bolla,
			:ldt_data_reg
	 FROM tes_bol_ven  
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = :ll_anno_reg_bol_ven AND  
			num_registrazione = :ll_num_reg_bol_ven ;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if
	
	select flag_tipo_bol_ven
	into :ls_flag_tipo_bol_ven
	from tab_tipi_bol_ven
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	if sqlca.sqlcode <> 0 then 
		fs_errore = sqlca.sqlerrtext
		return -1
	end if	
	
	if ls_flag_tipo_bol_ven = "T" then
		ls_title =  "~r~n********** BOLLA DI TRASFERIMENTO ********~r~n"
	else
		ls_title =  "~r~n************* BOLLA DI USCITA ************~r~n"
	end if
	
	if not isnull(ls_cod_documento) and not isnull(ll_anno_documento) and not isnull(ls_numeratore_documento) and not isnull(ll_num_documento) and not isnull(ldt_data_bolla) then
		ls_line = "NUMERO " + ls_cod_documento + "-" + string(ll_anno_documento) + "/" + ls_numeratore_documento + "-" + string(ll_num_documento) + " del " + string(ldt_data_bolla,"dd/mm/yyyy") + "~r~n"
	else
		ls_line = "NUMERO " + string(ll_anno_reg_bol_ven) + "/" + string(ll_num_reg_bol_ven) + " del " + string(ldt_data_reg,"dd/mm/yyyy") + "~r~n"
	end if
	
	// stefanop: 29/02/2012: bolla di traferimento
	//if ls_flag_tipo_bol_ven = "V" then
	if ls_flag_tipo_bol_ven = "V" or ls_flag_tipo_bol_ven = "T" then
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
		ls_cli_for = "CLIENTE: " + ls_cod_cliente + "  " + ls_rag_soc_1 + "~r~n"
	else
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
		ls_cli_for = "FORNITORE: " + ls_cod_fornitore + "  " + ls_rag_soc_1 + "~r~n"
	end if
	
	// stefanop 18/06/2010: progetto 140
	fs_full_doc_ref += ls_title + ls_line + ls_cli_for
	if lb_first_title then
		fs_small_doc_ref += ls_cli_for
		lb_first_title = false
	end if 
	
	if ls_flag_tipo_bol_ven = "T" then
		fs_small_doc_ref += "BOLLA DI TRASFERIMENTO~r~n     " + ls_line
	else
		fs_small_doc_ref += "BOLLA DI USCITA~r~n     " + ls_line
	end if
	// ----
end if

// FATTURE DI VENDITA

SELECT anno_registrazione,   
		 num_registrazione,   
		 prog_riga_fat_ven
 INTO  :ll_anno_reg_fat_ven,   
		 :ll_num_reg_fat_ven,   
		 :ll_prog_riga_fat_ven
 FROM  det_fat_ven  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione_mov_mag = :fl_anno_registrazione AND  
		 num_registrazione_mov_mag = :fl_num_registrazione ;
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 0 then

	SELECT cod_tipo_fat_ven,   
			cod_cliente,   
			cod_fornitore,   
			cod_documento,   
			numeratore_documento,   
			anno_documento,   
			num_documento,   
			data_fattura,
			data_registrazione
	 INTO :ls_cod_tipo_fat_ven,   
			:ls_cod_cliente,   
			:ls_cod_fornitore,   
			:ls_cod_documento,   
			:ls_numeratore_documento,   
			:ll_anno_documento,   
			:ll_num_documento,   
			:ldt_data_fattura,
			:ldt_data_reg
	 FROM tes_fat_ven  
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = :ll_anno_reg_fat_ven AND  
			num_registrazione = :ll_num_reg_fat_ven ;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if
	
	select flag_tipo_fat_ven
	into :ls_flag_tipo_fat_ven
	from tab_tipi_fat_ven
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if	
	
	ls_title =  "~r~n************* FATTURA DI VENDITA ************~r~n"
	if not isnull(ls_cod_documento) and not isnull(ll_anno_documento) and not isnull(ls_numeratore_documento) and not isnull(ll_num_documento) and not isnull(ldt_data_fattura) then
		ls_line = "NUMERO " + ls_cod_documento + "-" + string(ll_anno_documento) + "/" + ls_numeratore_documento + "-" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy") + "~r~n"
	else
		ls_line = "NUMERO " + string(ll_anno_reg_fat_ven) + "/" + string(ll_num_reg_fat_ven) + " del " + string(ldt_data_reg,"dd/mm/yyyy") + "~r~n"
	end if
	
	if ls_flag_tipo_fat_ven <> "F" then
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
		ls_cli_for = "CLIENTE: " + ls_cod_cliente + "  " + ls_rag_soc_1 + "~r~n"
	else
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
		ls_cli_for = "FORNITORE: " + ls_cod_fornitore + "  " + ls_rag_soc_1 + "~r~n"
	end if
	
	// stefanop 18/06/2010: progetto 140
	fs_full_doc_ref += ls_title + ls_line + ls_cli_for
	if lb_first_title then
		fs_small_doc_ref += ls_cli_for
		lb_first_title = false
	end if 
	fs_small_doc_ref += "FATTURE DI VENDITA~r~n     " + ls_line
	// ----
	
end if

// BOLLE DI ACQUISTO

SELECT anno_bolla_acq,   
		 num_bolla_acq,   
		 prog_riga_bolla_acq
 INTO  :ll_anno_reg_bol_acq,   
		 :ll_num_reg_bol_acq,   
		 :ll_prog_riga_bol_acq
 FROM  det_bol_acq  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione_mov_mag = :fl_anno_registrazione AND  
		 num_registrazione_mov_mag = :fl_num_registrazione ;
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 0 then

	SELECT cod_tipo_bol_acq,   
			cod_cliente,   
			cod_fornitore,   
			num_bolla_fornitore,   
			data_bolla,
			data_registrazione
	 INTO :ls_cod_tipo_bol_acq,   
			:ls_cod_cliente,   
			:ls_cod_fornitore,   
			:ls_num_bolla_fornitore,   
			:ldt_data_bolla,
			:ldt_data_reg
	 FROM tes_bol_acq  
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_bolla_acq = :ll_anno_reg_bol_acq AND  
			num_bolla_acq = :ll_num_reg_bol_acq ;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_num_bolla_fornitore) then
		ls_num_bolla_fornitore = ""
	else
		ls_num_bolla_fornitore = " (" + ls_num_bolla_fornitore + ")"
	end if
	
	if isnull(ldt_data_bolla) then
		ldt_data_bolla = ldt_data_reg
	end if
	
	select tipo_bol_acq
	into :ls_flag_tipo_bol_acq
	from tab_tipi_bol_acq
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if	
	
	ls_title =  "~r~n************* BOLLA DI ENTRATA ************~r~n"
	ls_line = "NUMERO " + string(ll_anno_reg_bol_acq) + "/" + string(ll_num_reg_bol_acq) + ls_num_bolla_fornitore + " del " + string(ldt_data_bolla,"dd/mm/yyyy") + "~r~n"
	
	if ls_flag_tipo_bol_acq = "R" then
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
		ls_cli_for = "CLIENTE: " + ls_cod_cliente + "  " + ls_rag_soc_1 + "~r~n"
	else
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
		ls_cli_for = "FORNITORE: " + ls_cod_fornitore + "  " + ls_rag_soc_1 + "~r~n"
	end if
	
	// stefanop 18/06/2010: progetto 140
	fs_full_doc_ref += ls_title + ls_line + ls_cli_for
	if lb_first_title then
		fs_small_doc_ref += ls_cli_for
		lb_first_title = false
	end if 
	fs_small_doc_ref += "BOLLE DI ENTRATA~r~n     " + ls_line
	// ----
end if

// FATTURE DI ACQUISTO
// stefanop 18/06/2010: progetto 140 funzione 4. aggiunto controllo su AGG.MOV
SELECT anno_registrazione,   
		 num_registrazione,   
		 prog_riga_fat_acq,
		 flag_agg_mov
 INTO  :ll_anno_reg_fat_acq,   
		 :ll_num_reg_fat_acq,   
		 :ll_prog_riga_fat_acq,
		 :ls_flag_agg_mov
 FROM  det_fat_acq  
WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
		 anno_registrazione_mov_mag = :fl_anno_registrazione AND  
		 num_registrazione_mov_mag = :fl_num_registrazione ;
if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 0 then

	SELECT cod_fornitore,   
			cod_documento,   
			numeratore_documento,   
			anno_documento,   
			num_documento,   
			data_doc_origine,
			data_registrazione
	 INTO :ls_cod_fornitore,   
			:ls_cod_documento,   
			:ls_numeratore_documento,   
			:ll_anno_documento,   
			:ll_num_documento,   
			:ldt_data_fattura,
			:ldt_data_reg
	 FROM tes_fat_acq  
	WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = :ll_anno_reg_fat_acq AND  
			num_registrazione = :ll_num_reg_fat_acq ;
	if sqlca.sqlcode <> 0 then
		fs_errore = sqlca.sqlerrtext
		return -1
	end if
	
	ls_title =  "~r~n************* FATTURA DI ACQUISTO ************~r~n"
	if not isnull(ls_cod_documento) and not isnull(ll_anno_documento) and not isnull(ls_numeratore_documento) and not isnull(ll_num_documento) and not isnull(ldt_data_fattura) then
		ls_line = "NUMERO " + ls_cod_documento + "-" + string(ll_anno_documento) + "/" + ls_numeratore_documento + "-" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy") + "~r~n"
	else
		ls_line = "NUMERO " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) + " del " + string(ldt_data_reg,"dd/mm/yyyy") + "~r~n"
	end if
		
	select rag_soc_1
	into   :ls_rag_soc_1
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	ls_cli_for = "FORNITORE: " + ls_cod_fornitore + "  " + ls_rag_soc_1 + "~r~n"
	
	// stefanop 18/06/2010: progetto 140
	fs_full_doc_ref += ls_title + ls_line + ls_cli_for
	if lb_first_title then
		fs_small_doc_ref += ls_cli_for
		lb_first_title = false
	end if 
	fs_small_doc_ref += "FATTURE DI ACQUISTO~r~n     " + ls_line
	// ----
	
		// stefanop 18/06/2010
	if ls_flag_agg_mov = "S" then
		fs_small_doc_ref += "     AGG.MOV.: SI"
	else
		fs_small_doc_ref += "     AGG.MOV.: NO"
	end if
	// ----
end if


// MANUTENZIONI  (da tabella manutenzioni ricambi)
SELECT a.anno_registrazione,   
		 a.num_registrazione,   
		 a.prog_riga_ricambio,
		 a.cod_prodotto,
		 m.data_registrazione
 INTO  :ll_anno_man,   
		 :ll_num_man,   
		 :ll_prog_riga_ricambio,
		 :ls_cod_prodotto,
		 :ldt_data_reg
 FROM  manutenzioni_ricambi  as a
 JOIN anag_prodotti as p on p.cod_azienda=a.cod_azienda and
 										p.cod_prodotto=a.cod_prodotto
JOIN manutenzioni as m on m.cod_azienda=a.cod_azienda and
 										m.anno_registrazione=a.anno_registrazione and
										 m.num_registrazione=a.num_registrazione
WHERE  a.cod_azienda = :s_cs_xx.cod_azienda AND  
		 a.anno_reg_mov_mag = :fl_anno_registrazione AND  
		 a.num_reg_mov_mag = :fl_num_registrazione ;

if sqlca.sqlcode = 0 then
	ls_title =  "~r~n************* MANUTENZIONE ************~r~n"
	
	ls_line = "NUMERO " + string(ll_anno_man) + "/" + string(ll_num_man)
	if not isnull(ldt_data_reg) and year(date(ldt_data_reg))>1980 then ls_line += " del " + string(ldt_data_reg,"dd/mm/yyyy")
	
	ls_line += "~r~n"
	
	fs_full_doc_ref += ls_title + ls_line + ls_cli_for
	
	fs_small_doc_ref += "MANUTENZIONE~r~n     " + ls_line
end if


return 0















end function

public function integer uof_movimenti_per_cli_for (long fl_anno_reg_mov, long fl_num_reg_mov, string fs_cod_cliente, string fs_cod_fornitore, ref string fs_errore);/*						FUNZIONE DI RICERCA MOVIMENTI per cliente/fornitore
							--------------------------------------------------------							

fl_anno_registrazione			long		anno registrazione del movimento di magazzino
fl_num_registrazione			long		numero di registrazione del movimento di magazzino

Passando il fornitore o il cliente ricerca se il movimento è referenziato con un documento fiscale relativo al fornitore o cleinte passato
Torna
	1		movimento verifica la condizione per cliente/fornitore
	0		movimento NON verifica la condizione per cliente/fornitore
	-1		un errore è avvenuto

RICERCA IN
TIPO DOCUMENTO				fs_cod_cliente			fs_cod_fornitore
------------------------------------------------------------------------------------------
bolla vendita							SI								SI
fattura vendita							SI								SI
bolla acquisto							SI								SI
fattura acquisto						NO							SI

*/

integer		li_anno_documento
long			ll_num_documento, ll_prog_riga_documento, ll_count
string			fs_tipo_ricerca

fs_errore = ""
setnull(ll_count)

if fs_cod_cliente<>"" and not isnull(fs_cod_cliente) then
	//ricerca per cod_cliente attivata
	fs_tipo_ricerca="C"
	
elseif fs_cod_fornitore<>"" and not isnull(fs_cod_fornitore) then
	//ricerca per cod_fornitore attivata
	fs_tipo_ricerca="F"
	
else
	//ritorno come se fosse tutto OK (cioè il movimento verifica la condizione)
	return 1
end if

// ricerca in BOLLE DI VENDITA ---------------------------------------------------------------------
if fs_tipo_ricerca="C" then
	
	select 	count(*)
	into		:ll_count
	from tes_bol_ven as t
	join det_bol_ven as d on d.cod_azienda=t.cod_azienda and
									d.anno_registrazione=t.anno_registrazione and
									d.num_registrazione=t.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			t.cod_cliente=:fs_cod_cliente;

else
	
	select 	count(*)
	into		:ll_count
	from tes_bol_ven as t
	join det_bol_ven as d on d.cod_azienda=t.cod_azienda and
									d.anno_registrazione=t.anno_registrazione and
									d.num_registrazione=t.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			 t.cod_fornitore=:fs_cod_fornitore;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	//il movimento verifica la condizione
	return 1
end if
//-----------------------------------------------------------------------------------------------------------------------------------

// ricerca in FATTURA DI VENDITA ---------------------------------------------------------------------
if fs_tipo_ricerca="C" then
	
	select 	count(*)
	into		:ll_count
	from tes_fat_ven as t
	join det_fat_ven as d on d.cod_azienda=t.cod_azienda and
									d.anno_registrazione=t.anno_registrazione and
									d.num_registrazione=t.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			t.cod_cliente=:fs_cod_cliente;

else
	
	select 	count(*)
	into		:ll_count
	from tes_fat_ven as t
	join det_fat_ven as d on d.cod_azienda=t.cod_azienda and
									d.anno_registrazione=t.anno_registrazione and
									d.num_registrazione=t.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			 t.cod_fornitore=:fs_cod_fornitore;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	//il movimento verifica la condizione
	return 1
end if
//-----------------------------------------------------------------------------------------------------------------------------------

// ricerca in BOLLE DI ACQUISTO ---------------------------------------------------------------------
if fs_tipo_ricerca="C" then
	
	select 	count(*)
	into		:ll_count
	from tes_bol_acq as t
	join det_bol_acq as d on d.cod_azienda=t.cod_azienda and
									d.anno_bolla_acq=t.anno_bolla_acq and
									d.num_bolla_acq=t.num_bolla_acq
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			t.cod_cliente=:fs_cod_cliente;

else
	
	select 	count(*)
	into		:ll_count
	from tes_bol_acq as t
	join det_bol_acq as d on d.cod_azienda=t.cod_azienda and
									d.anno_bolla_acq=t.anno_bolla_acq and
									d.num_bolla_acq=t.num_bolla_acq
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			 t.cod_fornitore=:fs_cod_fornitore;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	//il movimento verifica la condizione
	return 1
end if
//-----------------------------------------------------------------------------------------------------------------------------------

// ricerca in FATTURE DI ACQUISTO (solo fornitore )---------------------------------------------------------------------
if fs_tipo_ricerca="F" then
	
	select 	count(*)
	into		:ll_count
	from tes_fat_acq as t
	join det_fat_acq as d on d.cod_azienda=t.cod_azienda and
									d.anno_registrazione=t.anno_registrazione and
									d.num_registrazione=t.num_registrazione
	where d.cod_azienda = :s_cs_xx.cod_azienda and  
			d.anno_registrazione_mov_mag = :fl_anno_reg_mov and  
		 	d.num_registrazione_mov_mag = :fl_num_reg_mov and
			 t.cod_fornitore=:fs_cod_fornitore;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	//il movimento verifica la condizione
	return 1
end if
//-----------------------------------------------------------------------------------------------------------------------------------


//se arrivi fin qui vuol dire che non hai beccato niente
return 0
end function

public function integer uof_ref_mov_mag_prog_mov (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore);string			ls_smal_doc_ref, ls_error, ls_sql

long				ll_prog_mov, ll_count, ll_index

datastore		lds_data


fs_full_doc_ref=""
fs_full_doc_ref = ""


select prog_mov
into	:ll_prog_mov
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

if ll_prog_mov>0 then
else
	return 0
end if


ls_sql = "select anno_registrazione, num_registrazione "+&
			"from mov_magazzino "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					   "prog_mov="+string(ll_prog_mov) + " and "+&
						"num_registrazione<>"+string(fl_num_registrazione)

ls_sql += " and anno_registrazione="+string(fl_anno_registrazione)

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_error)

for ll_index=1 to ll_count	
	uof_ref_mov_mag(lds_data.getitemnumber(ll_index, 1), lds_data.getitemnumber(ll_index, 2), fs_full_doc_ref, fs_small_doc_ref, ls_error)
	if isnull(fs_full_doc_ref) then fs_full_doc_ref=""
	if isnull(fs_small_doc_ref) then fs_small_doc_ref=""
	
	//non appena ne trovi uno esci
	if fs_full_doc_ref<>"" or fs_small_doc_ref<>"" then return 0
	
next

fs_full_doc_ref=""
fs_full_doc_ref = ""

return 0
end function

public function integer uof_ref_mov_mag_commessa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_full_doc_ref, ref string fs_small_doc_ref, ref string fs_errore);/*						FUNZIONE DI RICERCA REFERENZE DEL MOVIMENTO DI MAGAZZINO SU COMMESSA
							--------------------------------------------------------							

fl_anno_registrazione			long		anno registrazione del movimento di magazzino
fl_num_registrazione			long		numero di registrazione del movimento di magazzino

*/

string				ls_referenza, ls_cod_prodotto, ls_des_prodotto
long				ll_num_commessa, ll_num_doc, ll_riga_doc
integer			li_anno_commessa, li_count, li_anno_doc



fs_errore = ""
fs_full_doc_ref = ""
fs_small_doc_ref = "" 


//COMMESSE PRODUZIONE
//NOTA: se trovi qualcosa esci subito dalla funzione ....
select referenza, num_documento
into	:ls_referenza, :ll_num_commessa
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

li_anno_commessa = integer(ls_referenza)

if li_anno_commessa>0 and ll_num_commessa>0 then
	
	select anag_commesse.cod_prodotto, anag_prodotti.des_prodotto
	into :ls_cod_prodotto, :ls_des_prodotto
	from anag_commesse
	left outer join anag_prodotti on		anag_prodotti.cod_azienda = anag_commesse.cod_azienda and
												anag_prodotti.cod_prodotto = anag_commesse.cod_prodotto
	where	anag_commesse.cod_azienda=:s_cs_xx.cod_azienda and
				anag_commesse.anno_commessa=:li_anno_commessa and
				anag_commesse.num_commessa=:ll_num_commessa;

	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
				
	if sqlca.sqlcode=0 then
		
		fs_small_doc_ref = "COMMESSA PRODUZIONE N. "+string(li_anno_commessa) + "/" + string(ll_num_commessa) + "~r~n"
		fs_small_doc_ref += "Prodotto Commessa: " + ls_cod_prodotto
		if ls_des_prodotto<>"" then fs_small_doc_ref+= "~r~n" + ls_des_prodotto
		
		//cerca inoltre se per caso la commessa è riferita anche a riga ordine di vendita o ddt di uscita (caso tarsferimenti di kit)
		
		setnull(li_anno_doc)
		setnull(ll_num_doc)
		setnull(ll_riga_doc)
		
		//riga ordine vendita
		select anno_registrazione, num_registrazione, prog_riga_ord_ven
		into	:li_anno_doc, :ll_num_doc, :ll_riga_doc
		from   det_ord_ven
		where	cod_azienda=:s_cs_xx.cod_azienda and
					anno_commessa=:li_anno_commessa and
					num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode=0 and li_anno_doc>0 and ll_num_doc>0 and ll_riga_doc>0 then
			//commessa su riga ordine vendita
			fs_small_doc_ref += "~r~n~r~n"+"RIGA ORDINE VENDITA N. "+string(li_anno_doc) + " / " + string(ll_num_doc) + " / " + string(ll_riga_doc)
		else
			//prova su riga ddt uscita
			select anno_registrazione, num_registrazione, prog_riga_bol_ven
			into		:li_anno_doc, :ll_num_doc, :ll_riga_doc
			from   det_bol_ven
			where	cod_azienda=:s_cs_xx.cod_azienda and
						anno_commessa=:li_anno_commessa and
						num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode=0 and li_anno_doc>0 and ll_num_doc>0 and ll_riga_doc>0 then
				//commessa su riga ddt di uscita
			fs_small_doc_ref += "~r~n~r~n"+"RIGA D.D.T. USCITA N. "+string(li_anno_doc) + " / " + string(ll_num_doc) + " / " + string(ll_riga_doc)
			else
				//nè riga ordine vendita nè riga ddt uscita ...
			end if
		
		end if
	end if
end if

fs_full_doc_ref = fs_small_doc_ref

return 0















end function

on uo_ref_mov_mag.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ref_mov_mag.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

