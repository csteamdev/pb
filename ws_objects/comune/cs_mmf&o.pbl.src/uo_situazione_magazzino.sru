﻿$PBExportHeader$uo_situazione_magazzino.sru
$PBExportComments$UO per creazione report situazione di magazzino
forward
global type uo_situazione_magazzino from nonvisualobject
end type
end forward

global type uo_situazione_magazzino from nonvisualobject
end type
global uo_situazione_magazzino uo_situazione_magazzino

type variables
// filtro per selezionare solo i prodotti di un certo deposito.
string is_cod_deposito_filtro

// filtro per selezionare solo le MP delle commesse incluse fra 2 date di consegna
boolean ib_filtro_commesse = false
datetime idt_data_filtro_commessa_inizio, idt_data_filtro_commessa_fine

// filtro per prodotti bloccati S=estrai solo bloccati N=estrai prodotti non bloccati T=Tutti
string		is_prodotti_bloccati="T"

private:
string is_cod_nomenclatura
end variables

forward prototypes
public function integer wf_bolle_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer wf_commesse (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer wf_fatture_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer wf_ordini_acquisto (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer wf_ordini_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer wf_prodotti_generale (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, ref datastore ids_situazione_mag, string fs_des_prodotto)
public function integer uf_bolle_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public function integer uf_commesse_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public function integer uf_fatture_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public function integer uf_ordini_acquisto_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public function integer uf_ordini_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public function integer uf_prodotti_generale_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag)
public subroutine uof_set_nomenclatura (string as_cod_nomenclatura)
end prototypes

public function integer wf_bolle_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_sql, ls_flag_blocco, ls_flag_movimenti, &
		 ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_anno_reg_acq, ll_anno_reg_ven, ll_num_reg_acq, ll_num_reg_ven, ll_found, ll_anno_registrazione, ll_num_registrazione, &
		 ll_anno_registrazione_prec, ll_num_registrazione_prec
dec{4} ld_quan_bolle_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com
		 
ll_anno_registrazione_prec= 0
ll_num_registrazione_prec = 0

declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select a.anno_registrazione, a.num_registrazione, a.cod_prodotto, a.quan_consegnata, a.cod_tipo_det_ven from det_bol_ven a, det_ord_ven b "

ls_sql = ls_sql + "where a.cod_azienda = '" + s_cs_xx.cod_azienda + "' and a.anno_registrazione = " + string(ll_anno_reg_ven) + " and a.num_registrazione = " + string(ll_num_reg_ven) + " "

if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and a.cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if

if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and a.cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like


if			(not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) or &
			(not isnull(is_cod_deposito_filtro) and len(trim(is_cod_deposito_filtro)) > 0) or &
			(not isnull(is_cod_nomenclatura) or len(is_cod_nomenclatura) > 0)	or &
			(not isnull(is_prodotti_bloccati) or len(is_prodotti_bloccati) > 0)						then
	
	ls_sql += " and a.cod_prodotto in (SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if (not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) then
		ls_sql += " and cod_prodotto like '%" + fs_des_prodotto + "%' "
	end if
	if not isnull(is_cod_deposito_filtro) and len(is_cod_deposito_filtro) > 0 then
		ls_sql = ls_sql + " and cod_deposito = '" + is_cod_deposito_filtro + "' "
	end if
	if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
		ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "' "
	end if
	if is_prodotti_bloccati<> "T" then
		ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
	end if		
	ls_sql += " ) "
	
end if

if fs_flag_consegna = 'S' then    //senza data inizio
	ls_sql = ls_sql + " and (b.data_consegna <= '" + fs_data_fine + "' or b.data_consegna is null) and a.anno_registrazione_ord_ven = b.anno_registrazione and a.num_registrazione_ord_ven = b.num_registrazione and a.prog_riga_ord_ven = b.prog_riga_ord_ven and a.cod_azienda = b.cod_azienda "
elseif fs_flag_consegna = 'N' then
	ls_sql = ls_sql + " and b.data_consegna <= '" + fs_data_fine + "' and a.anno_registrazione_ord_ven = b.anno_registrazione and a.num_registrazione_ord_ven = b.num_registrazione and a.prog_riga_ord_ven = b.prog_riga_ord_ven and a.cod_azienda = b.cod_azienda "
end if			

// seleziono solo prodotti provenienti dall'impegnato delle commesse
// richiesto da Unifast
if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and a.cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
	ls_sql += " and a.cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

end if


prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_vendite;

do while true
	
	fetch cu_dettaglio_vendite into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_prodotto_ven, :ld_quan_bolle_ven, :ls_cod_tipo_det_ven;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio bolle vendita " + sqlca.sqlerrtext)
		return -1
	end if		
	
	if sqlca.sqlcode = 100 then exit
	
	if ll_anno_registrazione <> ll_anno_registrazione_prec or ll_num_registrazione <> ll_num_registrazione_prec then
		
		select flag_blocco, 
			    flag_movimenti
		into   :ls_flag_blocco,
		       :ls_flag_movimenti
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca DDT " +string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
		
		// salto le fatture bollcate e quelle già confermate
		if ls_flag_blocco = "S" or ls_flag_movimenti = "S" then continue	
				
		ll_anno_registrazione_prec = ll_anno_registrazione
		ll_num_registrazione_prec = ll_num_registrazione
			
	end if
		
	select flag_tipo_det_ven
	  into :ls_flag_tipo_det_ven
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode <> 0 then	
		g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
		return -1				
	end if
	
	if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
	
		if fs_cod_cat_merc = '%' then
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_ven,
					 :ls_cod_cat_mer_ven,
					 :ld_giacenza_ven,
					 :ld_scorta_minima_ven, 
					 :ld_scorta_max_ven,
					 :ld_lotto_economico_ven,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and (cod_cat_mer like :fs_cod_cat_merc or cod_cat_mer is null);
		else	
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_ven,
					 :ls_cod_cat_mer_ven,
					 :ld_giacenza_ven,
					 :ld_scorta_minima_ven, 
					 :ld_scorta_max_ven,
					 :ld_lotto_economico_ven,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and cod_cat_mer = :fs_cod_cat_merc;
		end if		
			
//					if sqlca.sqlcode = 100 then	
//						messagebox("Apice", "Il prodotto " + ls_cod_prodotto_ven + " con cod_cat_merc " + fs_cod_cat_merc + " non esiste in anag_prodotti")
//					end if							
			
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
			return -1
		end if					
		if sqlca.sqlcode = 0 then
			
			if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
			if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
			if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
			if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
			if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
			
			ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
			if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
				ids_situazione_mag.insertrow(0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
				
				ld_impegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)
				
				ld_assegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)
				
				ld_spedizione_ven = ld_quan_bolle_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
				
				ld_disp_teorica_ven = ld_disp_reale_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
			elseif ll_found > 0 then  //se il prodotto esiste già in datastore
				ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
				ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
				ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")
				ld_spedizione_ven = ld_spedizione_ven + ld_quan_bolle_ven
				
//							if ld_spedizione_ven <> 0 then
//								ld_assegnato_ven = ld_assegnato_ven - ld_spedizione_ven
//								ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)								
//							end if	
											
				ids_situazione_mag.setitem(ll_found, "spedizione_com", ld_spedizione_ven)
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

				ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
				ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
				
				ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com					
				ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose

				ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
			end if
		end if					
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------

loop

close cu_dettaglio_vendite;

return 1
end function

public function integer wf_commesse (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto);string ls_cod_prodotto_com, ls_flag_blocco_com, ls_flag_evasione_com, &
		 ls_sql, ls_cod_cat_mer_com, ls_des_prodotto_com, ls_cod_tipo_det_com, ls_flag_tipo_det_com, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long ll_anno_reg_acq, ll_anno_reg_com, ll_num_reg_acq, ll_num_reg_com, ll_found
double ld_quan_prodotta, ld_quan_ordinata, ld_giacenza_com, &
		 ld_impegnato_com, ld_assegnato_com, ld_spedizione_com, ld_disp_reale_com, ld_disp_teorica_com, &
		 ld_scorta_minima_com, ld_scorta_max_com, ld_lotto_economico_com, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com
		 

declare cu_commesse dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, quan_prodotta, quan_ordine from anag_commesse "

ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' " 

if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if

if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like
if			(not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) or &
			(not isnull(is_cod_deposito_filtro) and len(trim(is_cod_deposito_filtro)) > 0) or &
			(not isnull(is_cod_nomenclatura) and len(trim(is_cod_nomenclatura)) > 0)	or &
			(not isnull(is_prodotti_bloccati) and len(is_prodotti_bloccati) > 0)	then
	ls_sql += " and cod_prodotto in (SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if (not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) then
		ls_sql += "and cod_prodotto like '%" + fs_des_prodotto + "%') "
	end if
	if not isnull(is_cod_deposito_filtro) and len(is_cod_deposito_filtro) > 0 then
		ls_sql = ls_sql + " and cod_deposito = '" + is_cod_deposito_filtro + "'"
	end if
	if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
		ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "'"
	end if
	if is_prodotti_bloccati<> "T" then
		ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
	end if		
	ls_sql += " ) "
end if

if fs_flag_consegna = 'S' then // senza data inizio
	ls_sql = ls_sql + " and (data_consegna <= '" + fs_data_fine + "' or data_consegna is null) "
elseif fs_flag_consegna = 'N' then
	ls_sql = ls_sql + " and data_consegna <= '" + fs_data_fine + "' "
end if

// seleziono solo prodotti provenienti dall'impegnato delle commesse
// richiesto da Unifast
if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
	ls_sql += " and cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "
end if



prepare sqlsa from :ls_sql;

open dynamic cu_commesse;

do while 1=1
	fetch cu_commesse into :ls_cod_prodotto_com, :ld_quan_prodotta, :ld_quan_ordinata;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura anagrafica commesse " + sqlca.sqlerrtext)
		return -1
	end if						
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		if not isnull(ls_cod_prodotto_com) then
		
			if fs_cod_cat_merc = '%' then
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_com,
						 :ls_cod_cat_mer_com,
						 :ld_giacenza_com,
						 :ld_scorta_minima_com, 
						 :ld_scorta_max_com,
						 :ld_lotto_economico_com,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_com
					and (cod_cat_mer like :fs_cod_cat_merc or cod_cat_mer is null);
			else	
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_com,
						 :ls_cod_cat_mer_com,
						 :ld_giacenza_com,
						 :ld_scorta_minima_com, 
						 :ld_scorta_max_com,
						 :ld_lotto_economico_com,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_com
					and cod_cat_mer = :fs_cod_cat_merc;
			end if		
				
//			if sqlca.sqlcode = 100 then	
//				messagebox("Apice", "Il prodotto " + ls_cod_prodotto_com + " con cod_cat_merc " + fs_cod_cat_merc + " non esiste in anag_prodotti")
//			end if							
				
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_com + " non esiste in anag_prodotti")
				return -1
			end if					
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_com) then ld_giacenza_com = 0
				if isnull(ld_scorta_minima_com) then ld_scorta_minima_com = 0
				if isnull(ld_scorta_max_com) then ld_scorta_max_com = 0 
				if isnull(ld_lotto_economico_com) then ld_lotto_economico_com = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
				
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_com + "'", 1, ids_situazione_mag.RowCount( ))	
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_com)
					
					ld_impegnato_com = 0
	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_com)
					
					ld_assegnato_com = 0
	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_com)
					
					ld_spedizione_com = 0
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_com = ld_giacenza_com - ld_impegnato_com - ld_assegnato_com - ld_spedizione_com
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
					
					if ld_quan_prodotta < ld_quan_ordinata then
						ld_prod_lanciata_com = ld_quan_ordinata - ld_quan_prodotta
					else 
						ld_prod_lanciata_com = 0
					end if	
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", ld_prod_lanciata_com)
					
					ld_disp_teorica_com = ld_disp_reale_com + ld_prod_lanciata_com
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_com)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_com)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_com)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_com
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_scorta_max_com - ld_disp_teorica_com
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_com < 0 then
								ld_da_produrre = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_da_ordinare = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_c_lavorazione = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore
					ld_disp_reale_com = ids_situazione_mag.getitemdecimal(ll_found, "disp_reale_com")
					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					
					if ld_quan_prodotta < ld_quan_ordinata then
						ld_prod_lanciata_com = ld_prod_lanciata_com + (ld_quan_ordinata - ld_quan_prodotta)
					else 
						ld_prod_lanciata_com = ld_prod_lanciata_com
					end if	
					
					ids_situazione_mag.setitem(ll_found, "prod_lanciata_com", ld_prod_lanciata_com)
	
					ld_disp_teorica_com = ld_disp_reale_com + ld_ord_a_fornitore + ld_prod_lanciata_com			
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_com)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_com
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_scorta_max_com - ld_disp_teorica_com
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_com < 0 then
								ld_da_produrre = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_da_ordinare = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_c_lavorazione = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					end choose
	
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
				end if					
			end if	
		end if	
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_com = 0
	ld_impegnato_com = 0
	ld_assegnato_com = 0
	ld_spedizione_com = 0
	ld_prod_lanciata_com = 0
	ld_disp_reale_com = 0
	ld_disp_teorica_com = 0
	ld_scorta_minima_com	= 0
	ld_scorta_max_com = 0
	ld_lotto_economico_com = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------

loop
close cu_commesse;

return 1
end function

public function integer wf_fatture_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_cod_tipo_fat_ven, &
		 ls_sql, ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino, &
		 ls_flag_tipo_fat_ven,ls_flag_blocco,ls_flag_movimenti
long   ll_anno_reg_acq, ll_anno_reg_ven, ll_num_reg_acq, ll_num_reg_ven, ll_found,ll_anno_registrazione_prec, &
       ll_num_registrazione_prec,ll_anno_registrazione,ll_num_registrazione
dec{4} ld_quan_fatture_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com


declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select a.anno_registrazione, a.num_registrazione, a.cod_prodotto, a.quan_fatturata, a.cod_tipo_det_ven from det_fat_ven a, det_ord_ven b "

ls_sql = ls_sql + "where a.cod_azienda = '" + s_cs_xx.cod_azienda + "' and a.anno_registrazione = " + string(ll_anno_reg_ven) + " and a.num_registrazione = " + string(ll_num_reg_ven) + " and a.anno_reg_ord_ven is null and a.num_reg_ord_ven is null and a.anno_registrazione_bol_ven is null and a.num_registrazione is null "

if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and a.cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if

if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and a.cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like

if			(not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) or &
			(not isnull(is_cod_deposito_filtro) and len(trim(is_cod_deposito_filtro)) > 0) or &
			(not isnull(is_cod_nomenclatura) and len(trim(is_cod_nomenclatura)) > 0)	or &
			(not isnull(is_prodotti_bloccati) and len(trim(is_prodotti_bloccati)) > 0)			then
	
	ls_sql += " and a.cod_prodotto in (SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if (not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) then
		ls_sql += " and cod_prodotto like '%" + fs_des_prodotto + "%' "
	end if
	if not isnull(is_cod_deposito_filtro) and len(is_cod_deposito_filtro) > 0 then
		ls_sql = ls_sql + " and cod_deposito = '" + is_cod_deposito_filtro + "' "
	end if
	if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
		ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "' "
	end if
	if is_prodotti_bloccati<> "T" then
		ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
	end if		
	ls_sql += " ) "
	
end if

if fs_flag_consegna = 'S' then 
	ls_sql = ls_sql + " and (b.data_consegna <= '" + fs_data_fine + "' or b.data_consegna is null) and a.anno_reg_ord_ven = b.anno_registrazione and a.num_reg_ord_ven = b.num_registrazione and a.prog_riga_ord_ven = b.prog_riga_ord_ven and a.cod_azienda = b.cod_azienda "
elseif fs_flag_consegna = 'N' then
	ls_sql = ls_sql + " and b.data_consegna <= '" + fs_data_fine + "' and a.anno_reg_ord_ven = b.anno_registrazione and a.num_reg_ord_ven = b.num_registrazione and a.prog_riga_ord_ven = b.prog_riga_ord_ven and a.cod_azienda = b.cod_azienda "
end if	

if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and a.cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
	ls_sql += " and a.cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

end if

ll_anno_registrazione_prec = 0
ll_num_registrazione_prec  = 0

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_vendite;

do while true
	
	fetch cu_dettaglio_vendite into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_prodotto_ven, :ld_quan_fatture_ven, :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio fatture vendita " + sqlca.sqlerrtext)
		return -1
	end if	
	
	if sqlca.sqlcode = 100 then exit
		
	if ll_anno_registrazione <> ll_anno_registrazione_prec or ll_num_registrazione <> ll_num_registrazione_prec then
		
		select flag_blocco, 
			    flag_movimenti,
			    cod_tipo_fat_ven	
		into   :ls_flag_blocco,
		       :ls_flag_movimenti,
				 :ls_cod_tipo_fat_ven
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca fattura " +string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
		
		// salto le fatture bollcate e quelle già confermate
		if ls_flag_blocco = "S" or ls_flag_movimenti = "S" then continue	
				
		select flag_tipo_fat_ven
		  into :ls_flag_tipo_fat_ven
		  from tab_tipi_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and
			    cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca tipo fattura " +ls_cod_tipo_fat_ven + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
			
		if sqlca.sqlcode = 100 then ls_flag_tipo_fat_ven = 'I'
		
		// considero solo le fatture immediate
		if ls_flag_tipo_fat_ven <> "I" then continue
		
		ll_anno_registrazione_prec = ll_anno_registrazione
		ll_num_registrazione_prec = ll_num_registrazione
			
	end if
		
		
	select flag_tipo_det_ven
	  into :ls_flag_tipo_det_ven
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode <> 0 then	
		g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
		return -1				
	end if
	if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
	
		if fs_cod_cat_merc = "%" then
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_ven,
					 :ls_cod_cat_mer_ven,
					 :ld_giacenza_ven,
					 :ld_scorta_minima_ven, 
					 :ld_scorta_max_ven,
					 :ld_lotto_economico_ven,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and (cod_cat_mer like :fs_cod_cat_merc or cod_cat_mer is null);
		else
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_ven,
					 :ls_cod_cat_mer_ven,
					 :ld_giacenza_ven,
					 :ld_scorta_minima_ven, 
					 :ld_scorta_max_ven,
					 :ld_lotto_economico_ven,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and cod_cat_mer = :fs_cod_cat_merc;
		end if		
				
//					if sqlca.sqlcode = 100 then	
//						messagebox("Apice", "Il prodotto " + ls_cod_prodotto_ven + " con cod_cat_merc " + fs_cod_cat_merc + " non esiste in anag_prodotti")
//					end if												
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
			return -1
		end if					
		if sqlca.sqlcode = 0 then
			
			if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
			if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
			if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
			if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
			if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
			
			ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
			if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
				ids_situazione_mag.insertrow(0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
				
				ld_impegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)
				
				ld_assegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)

				if ls_flag_tipo_fat_ven <> 'N' then
					ld_spedizione_ven = ld_quan_fatture_ven
				else
					ld_spedizione_ven = ld_quan_fatture_ven * (-1)
				end if	
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
				
				ld_disp_teorica_ven = ld_disp_reale_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
			elseif ll_found > 0 then  //se il prodotto esiste già in datastore							
				ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
				ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
				ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")							
				if ls_flag_tipo_fat_ven <> 'N' then
					ld_spedizione_ven = ld_spedizione_ven + ld_quan_fatture_ven
				else
					ld_spedizione_ven = ld_spedizione_ven + (ld_quan_fatture_ven * (-1))
				end if	
				
//							if ld_spedizione_ven <> 0 then
//								ld_assegnato_ven = ld_assegnato_ven - ld_spedizione_ven
//								ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)								
//							end if	
				
				ids_situazione_mag.setitem(ll_found, "spedizione_com", ld_spedizione_ven)
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

				ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
				ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
				ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com
				ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")			
			end if
		end if					
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop

close cu_dettaglio_vendite;	

return 1
end function

public function integer wf_ordini_acquisto (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto);string ls_cod_prodotto_acq, ls_flag_blocco_acq, ls_flag_evasione_acq, ls_sql, &
		 ls_cod_cat_mer_acq, ls_des_prodotto_acq, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_found
dec{4} ld_impegnato_acq, ld_assegnato_acq, ld_spedizione_acq, ld_disp_reale_acq, ld_disp_teorica_acq, &
		 ld_scorta_minima_acq, ld_scorta_max_acq, ld_lotto_economico_acq, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_quan_ordinata, ld_quan_arrivata, ld_giacenza_acq, ld_ord_a_fornitore, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_prod_lanciata_com

declare cu_dettaglio_acquisti dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, quan_ordinata, quan_arrivata, cod_tipo_det_acq from det_ord_acq "

ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' and flag_saldo = 'N' "

if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if

if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like
if 	(not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) or &
		(not isnull(is_cod_deposito_filtro) and len(trim(is_cod_deposito_filtro)) > 0) or &
		(not isnull(is_cod_nomenclatura) and len(trim(is_cod_nomenclatura)) > 0)  or &
		(not isnull(is_prodotti_bloccati) and len(trim(is_prodotti_bloccati)) > 0)  then
		
	ls_sql += " and cod_prodotto in (SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if (not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) then
		ls_sql += "and cod_prodotto like '%" + fs_des_prodotto + "%') "
	end if
	if not isnull(is_cod_deposito_filtro) and len(is_cod_deposito_filtro) > 0 then
		ls_sql = ls_sql + " and cod_deposito = '" + is_cod_deposito_filtro + "'"
	end if
	if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
		ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "'"
	end if
	if is_prodotti_bloccati<> "T" then
		ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
	end if		
	ls_sql += " ) "
end if



if fs_flag_consegna = 'S' then 
	ls_sql = ls_sql + " and (data_consegna <= '" + fs_data_fine + "' or data_consegna is null) "
elseif fs_flag_consegna = 'N' then
	ls_sql = ls_sql + " and data_consegna <= '" + fs_data_fine + "' "
end if	

if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
	ls_sql += " and cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "
end if

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_acquisti;

do while true
	fetch cu_dettaglio_acquisti into :ls_cod_prodotto_acq, :ld_quan_ordinata, :ld_quan_arrivata, :ls_cod_tipo_det_acq;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio ordini acquisto " + sqlca.sqlerrtext)
		return -1
	end if		
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = 0 then
		
		select flag_tipo_det_acq
		  into :ls_flag_tipo_det_acq
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_acq " + sqlca.sqlerrtext)
			return -1				
		end if
		if sqlca.sqlcode = 0 and ls_flag_tipo_det_acq = "M" then
		
			if fs_cod_cat_merc = "%" then
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_acq,
						 :ls_cod_cat_mer_acq,
						 :ld_giacenza_acq,
						 :ld_scorta_minima_acq, 
						 :ld_scorta_max_acq,
						 :ld_lotto_economico_acq,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_acq
					and (cod_cat_mer like :fs_cod_cat_merc or cod_cat_mer is null);
			else 	
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_acq,
						 :ls_cod_cat_mer_acq,
						 :ld_giacenza_acq,
						 :ld_scorta_minima_acq, 
						 :ld_scorta_max_acq,
						 :ld_lotto_economico_acq,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_acq
					and cod_cat_mer = :fs_cod_cat_merc;						
			end if		
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_acq + " non esiste in anag_prodotti")
				return -1
			end if	
			
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_acq) then ld_giacenza_acq = 0
				if isnull(ld_scorta_minima_acq) then ld_scorta_minima_acq = 0
				if isnull(ld_scorta_max_acq) then ld_scorta_max_acq = 0 
				if isnull(ld_lotto_economico_acq) then ld_lotto_economico_acq = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
				
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_acq + "'", 1, ids_situazione_mag.RowCount( ))	
				
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_acq)

					ld_impegnato_acq = 0
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_acq)
					
					ld_assegnato_acq = 0

					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_acq)
					
					ld_spedizione_acq = 0							
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_acq = ld_giacenza_acq - ld_impegnato_acq - ld_assegnato_acq - ld_spedizione_acq
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_acq)

					ld_ord_a_fornitore = ld_quan_ordinata - ld_quan_arrivata
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", ld_ord_a_fornitore)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
					
					ld_disp_teorica_acq = ld_disp_reale_acq + ld_ord_a_fornitore
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_acq)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_acq)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_acq)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_acq
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_scorta_max_acq - ld_disp_teorica_acq
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_acq < 0 then
								ld_da_produrre = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_da_ordinare = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_c_lavorazione = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
					
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore								
					
					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					ld_ord_a_fornitore = ld_ord_a_fornitore + (ld_quan_ordinata - ld_quan_arrivata)
					
					ids_situazione_mag.setitem(ll_found, "ord_a_fornitore_com", ld_ord_a_fornitore)							

					ld_disp_reale_acq = ids_situazione_mag.getitemdecimal(ll_found, "disp_reale_com")
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					ld_disp_teorica_acq = ld_disp_reale_acq + ld_ord_a_fornitore + ld_prod_lanciata_com
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_acq)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_acq
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_scorta_max_acq - ld_disp_teorica_acq
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_acq < 0 then
								ld_da_produrre = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_da_ordinare = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_c_lavorazione = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")
				end if
			end if					
		end if	
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_acq = 0
	ld_impegnato_acq = 0
	ld_assegnato_acq = 0
	ld_spedizione_acq = 0
	ld_disp_reale_acq = 0
	ld_ord_a_fornitore = 0
	ld_disp_teorica_acq = 0
	ld_scorta_minima_acq	= 0
	ld_scorta_max_acq = 0
	ld_lotto_economico_acq = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop
close cu_dettaglio_acquisti;

return 1
end function

public function integer wf_ordini_vendita (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, string fs_flag_consegna, string fs_data_inizio, string fs_data_fine, ref datastore ids_situazione_mag, string fs_des_prodotto);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_sql,&
		 ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_num_reg_acq, ll_num_reg_ven, ll_found
dec{4} ld_quan_evasa_ven, ld_quan_ordine_ven, ld_quan_in_evasione_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com

declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, flag_blocco, flag_evasione, quan_in_evasione, quan_evasa, quan_ordine, cod_tipo_det_ven from det_ord_ven "

ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N' "

if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if

if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like
if			(not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) or &
			(not isnull(is_cod_deposito_filtro) and len(trim(is_cod_deposito_filtro)) > 0) or &
			(not isnull(is_cod_nomenclatura) or len(is_cod_nomenclatura) > 0)   or &
			(not isnull(is_prodotti_bloccati) or len(is_prodotti_bloccati) > 0)       then
	ls_sql += " and cod_prodotto in (SELECT cod_prodotto FROM anag_prodotti WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
	if (not isnull(fs_des_prodotto) and len(trim(fs_des_prodotto)) > 0) then
		ls_sql += "and cod_prodotto like '%" + fs_des_prodotto + "%') "
	end if
	if not isnull(is_cod_deposito_filtro) and len(is_cod_deposito_filtro) > 0 then
		ls_sql = ls_sql + " and cod_deposito = '" + is_cod_deposito_filtro + "' "
	end if
	if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
		ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "' "
	end if
	if is_prodotti_bloccati<> "T" then
		ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
	end if		
	ls_sql += " ) "
end if

if fs_flag_consegna = 'S' then 
	ls_sql = ls_sql + " and (data_consegna <= '" + fs_data_fine + "' or data_consegna is null) "
elseif fs_flag_consegna = 'N' then
	ls_sql = ls_sql + " and data_consegna <= '" + fs_data_fine + "' "
end if	

// seleziono solo prodotti provenienti dall'impegnato delle commesse
// richiesto da Unifast
if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
	ls_sql += " and cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "
end if

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_vendite;

do while true
	fetch cu_dettaglio_vendite into :ls_cod_prodotto_ven, 
											  :ls_flag_blocco_ven, 
											  :ls_flag_evasione_ven, 
											  :ld_quan_in_evasione_ven, 
											  :ld_quan_evasa_ven, 
											  :ld_quan_ordine_ven, 
											  :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio ordini vendita " + sqlca.sqlerrtext)
		return -1
	end if			
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		select flag_tipo_det_ven
		  into :ls_flag_tipo_det_ven
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
			return -1				
		end if
		if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
			if fs_cod_cat_merc = "%" then
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_ven,
						 :ls_cod_cat_mer_ven,
						 :ld_giacenza_ven,
						 :ld_scorta_minima_ven, 
						 :ld_scorta_max_ven,
						 :ld_lotto_economico_ven,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_ven
					and (cod_cat_mer like :fs_cod_cat_merc or cod_cat_mer is null);
			else 		
				select des_prodotto,
						 cod_cat_mer,
						 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
						 scorta_minima,
						 scorta_massima,
						 lotto_economico,
						 cod_tipo_politica_riordino,
						 livello_riordino
				  into :ls_des_prodotto_ven,
						 :ls_cod_cat_mer_ven,
						 :ld_giacenza_ven,
						 :ld_scorta_minima_ven, 
						 :ld_scorta_max_ven,
						 :ld_lotto_economico_ven,
						 :ls_cod_tipo_politica_riordino,
						 :ld_livello_riordino
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_ven
					and cod_cat_mer = :fs_cod_cat_merc;
			end if
				
//					if sqlca.sqlcode = 100 then	
//						messagebox("Apice", "Il prodotto " + ls_cod_prodotto_ven + " con cod_cat_merc " + fs_cod_cat_merc + " non esiste in anag_prodotti")
//					end if	
				
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
				return -1
			end if					
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
				if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
				if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
				if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0
										
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
					
					if ls_flag_evasione_ven = "A" then
						if ld_quan_in_evasione_ven = 0 then
							ld_impegnato_ven = ld_quan_ordine_ven
						else
							ld_impegnato_ven = ld_quan_ordine_ven - ld_quan_in_evasione_ven
						end if	
					elseif ls_flag_evasione_ven = "P" then
						ld_impegnato_ven = ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven 
					else
						ld_impegnato_ven = 0
					end if	
					
					ld_assegnato_ven = ld_quan_in_evasione_ven

//							if ld_assegnato_ven <> 0 and ls_flag_evasione_ven = "A" then
//								ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven)
//							end if	

					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)
					
					ld_spedizione_ven = 0							
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
					
					ld_disp_teorica_ven = ld_disp_reale_ven
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					into   :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					from   tab_tipi_politiche_riordino
					where  cod_azienda = :s_cs_xx.cod_azienda and 
							 cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_ven
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_ven < 0 then
								ld_da_produrre = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_da_ordinare = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_c_lavorazione = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore
					
					ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
					if ls_flag_evasione_ven = "A" then
						if ld_quan_in_evasione_ven = 0 then
							ld_impegnato_ven = ld_impegnato_ven + ld_quan_ordine_ven
						else
							ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_in_evasione_ven)
						end if								
//								ld_impegnato_ven = ld_impegnato_ven + ld_quan_ordine_ven
					elseif ls_flag_evasione_ven = "P" then
						ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven)
					else
						ld_impegnato_ven = ld_impegnato_ven
					end if	
					
					ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
					ld_assegnato_ven = ld_assegnato_ven + ld_quan_in_evasione_ven
					
					ids_situazione_mag.setitem(ll_found, "impegnato_com", ld_impegnato_ven)
					ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)							

					ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")
					ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
					ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					
					ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_ven
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_ven < 0 then
								ld_da_produrre = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_da_ordinare = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven										
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_c_lavorazione = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven										
							end if	
						end if								
					end choose
					
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
				end if
			end if					
		end if	
	end if	
	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop
close cu_dettaglio_vendite;

return 1
end function

public function integer wf_prodotti_generale (string fs_cod_prodotto_da, string fs_cod_prodotto_a, string fs_cod_cat_merc, ref datastore ids_situazione_mag, string fs_des_prodotto);string   ls_sql, ls_cod_prodotto_ven, ls_des_prodotto_ven, ls_cod_cat_mer_ven, ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long     ll_found
dec{4}   ld_giacenza_ven, ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_livello_riordino, ld_livello_magazzino

declare cu_prodotti dynamic cursor for sqlsa;
		
ls_sql = " select cod_prodotto, " + & 
" des_prodotto, " + &
" cod_cat_mer,  " + & 
" (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),  " + &
" scorta_minima,  " + &
" scorta_massima,  " + & 
" lotto_economico, " + &
" cod_tipo_politica_riordino,  " + &
" livello_riordino " + &
" from   anag_prodotti " + &
" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "
		
if not isnull(fs_cod_prodotto_da) or fs_cod_prodotto_da <> "" then
	ls_sql = ls_sql + " and cod_prodotto >= '" + fs_cod_prodotto_da + "'"
end if
		
if not isnull(fs_cod_prodotto_a) or fs_cod_prodotto_a <> "" then
	ls_sql = ls_sql + " and cod_prodotto <= '" + fs_cod_prodotto_a + "'"
end if
		
if not isnull(fs_cod_cat_merc) and fs_cod_cat_merc <> "" and fs_cod_cat_merc <> '%' then
	ls_sql = ls_sql + " and cod_cat_mer = '" + fs_cod_cat_merc + "'"
end if		

// *** Michela 23/01/2006: devo poter ricercare anche con la descrizione del prodotto. richiesta di marco per
//                         Giorgio. aggiungo qui direttamente il percentuale del like
if not isnull(fs_des_prodotto) and fs_des_prodotto <> "" and len(trim(fs_des_prodotto)) > 0 then
	ls_sql += " and cod_prodotto like '%" + fs_des_prodotto + "%' "
end if
		
if is_prodotti_bloccati<> "T" then
	ls_sql = ls_sql + " and flag_blocco = '" + is_prodotti_bloccati + "' "
end if		

if not isnull(is_cod_nomenclatura) and len(is_cod_nomenclatura) > 0 then
	ls_sql = ls_sql + " and cod_nomenclatura = '" + is_cod_nomenclatura + "' "
end if
		
// seleziono solo prodotti provenienti dall'impegnato delle commesse
// richiesto da Unifast
if ib_filtro_commesse then
	
	if isnull(idt_data_filtro_commessa_inizio) then idt_data_filtro_commessa_inizio = datetime(date("01/01/1900"), 00:00:00)
	if isnull(idt_data_filtro_commessa_fine)   then idt_data_filtro_commessa_fine   = datetime(date("31/12/2099"), 00:00:00)
	
	ls_sql += " and cod_prodotto in ("
	ls_sql += " SELECT DISTINCT impegno_mat_prime_commessa.cod_prodotto  " + &
				 " FROM impegno_mat_prime_commessa LEFT OUTER JOIN anag_commesse ON impegno_mat_prime_commessa.cod_azienda = anag_commesse.cod_azienda AND impegno_mat_prime_commessa.anno_commessa = anag_commesse.anno_commessa AND impegno_mat_prime_commessa.num_commessa = anag_commesse.num_commessa " + &
				 " WHERE impegno_mat_prime_commessa.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND  " + &
				 " anag_commesse.data_consegna >= '"+ string(idt_data_filtro_commessa_inizio,s_cs_xx.db_funzioni.formato_data) +"' AND anag_commesse.data_consegna <= '"+ string(idt_data_filtro_commessa_fine,s_cs_xx.db_funzioni.formato_data) +"' AND  " + &
				 " anag_commesse.data_consegna is not null ) "

	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati, ma solo 
	ls_sql += " and cod_prodotto not in (" + &
				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

end if

//

ls_sql += " order by cod_prodotto ASC "		
		
prepare sqlsa from :ls_sql;

open dynamic cu_prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore prodotti: " + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	fetch cu_prodotti into 	:ls_cod_prodotto_ven, 
	                       		:ls_des_prodotto_ven, 
								:ls_cod_cat_mer_ven, 
								:ld_giacenza_ven, 
								:ld_scorta_minima_ven, 
								:ld_scorta_max_ven, 
								:ld_lotto_economico_ven,
								:ls_cod_tipo_politica_riordino,
								:ld_livello_riordino;
								  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore apertura cursore cu_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if			
	if sqlca.sqlcode = 100 then exit
	
	ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
	if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore	
	
		if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
		if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
		if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
		if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
		if isnull(ld_livello_riordino) then ld_livello_riordino = 0
												
		ids_situazione_mag.insertrow(0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", 0)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				
		ld_giacenza_ven = 0
		ld_scorta_minima_ven	= 0
		ld_scorta_max_ven = 0
		ld_lotto_economico_ven = 0
	end if				
loop

close cu_prodotti;

return 1
end function

public function integer uf_bolle_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_sql, ls_flag_blocco, ls_flag_movimenti, &
		 ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_anno_reg_acq, ll_anno_reg_ven, ll_num_reg_acq, ll_num_reg_ven, ll_found, ll_anno_registrazione, ll_num_registrazione, &
		 ll_anno_registrazione_prec, ll_num_registrazione_prec, ll_index
dec{4} ld_quan_bolle_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com
		 
ll_anno_registrazione_prec= 0
ll_num_registrazione_prec = 0

declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select a.anno_registrazione, a.num_registrazione, a.cod_prodotto, a.quan_consegnata, a.cod_tipo_det_ven from det_bol_ven a, det_ord_ven b "

ls_sql = ls_sql + "where a.cod_azienda = '" + s_cs_xx.cod_azienda + "' and a.anno_registrazione = " + string(ll_anno_reg_ven) + " and a.num_registrazione = " + string(ll_num_reg_ven) + " "

//selezionare solo prodotti provenienti dall'impegnato delle commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and a.cod_prodotto in ("+&
							"SELECT DISTINCT cod_prodotto  " + &
			 				"FROM impegno_mat_prime_commessa " + &
			 				"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "+&
							  "and ("+&
							  		"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += "))"
	end if
next
//	// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//	// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
//	ls_sql += " and a.cod_prodotto not in (" + &
//				 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//				 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "


prepare sqlsa from :ls_sql;
open dynamic cu_dettaglio_vendite;

do while true
	fetch cu_dettaglio_vendite into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_prodotto_ven, :ld_quan_bolle_ven, :ls_cod_tipo_det_ven;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio bolle vendita " + sqlca.sqlerrtext)
		return -1
	end if		
	
	if sqlca.sqlcode = 100 then exit
	
	if ll_anno_registrazione <> ll_anno_registrazione_prec or ll_num_registrazione <> ll_num_registrazione_prec then
		
		select flag_blocco, 
			    flag_movimenti
		into   :ls_flag_blocco,
		       :ls_flag_movimenti
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca DDT " +string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
		
		// salto le fatture bloccate e quelle già confermate
		if ls_flag_blocco = "S" or ls_flag_movimenti = "S" then continue	
			
		ll_anno_registrazione_prec = ll_anno_registrazione
		ll_num_registrazione_prec = ll_num_registrazione
	end if
		
	select flag_tipo_det_ven
	  into :ls_flag_tipo_det_ven
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode <> 0 then	
		g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
		return -1				
	end if
	
	if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
	
		select des_prodotto,
				 cod_cat_mer,
				 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
				 scorta_minima,
				 scorta_massima,
				 lotto_economico,
				 cod_tipo_politica_riordino,
				 livello_riordino
		into :ls_des_prodotto_ven,
			 :ls_cod_cat_mer_ven,
			 :ld_giacenza_ven,
			 :ld_scorta_minima_ven, 
			 :ld_scorta_max_ven,
			 :ld_lotto_economico_ven,
			 :ls_cod_tipo_politica_riordino,
			 :ld_livello_riordino
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and (cod_cat_mer like '%' or cod_cat_mer is null);
									
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
			return -1
		end if					
		if sqlca.sqlcode = 0 then
			
			if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
			if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
			if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
			if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
			if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
			
			ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
			if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
				ids_situazione_mag.insertrow(0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
				
				ld_impegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)
				
				ld_assegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)
				
				ld_spedizione_ven = ld_quan_bolle_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
				
				ld_disp_teorica_ven = ld_disp_reale_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
			elseif ll_found > 0 then  //se il prodotto esiste già in datastore
				ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
				ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
				ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")
				ld_spedizione_ven = ld_spedizione_ven + ld_quan_bolle_ven
				
//							if ld_spedizione_ven <> 0 then
//								ld_assegnato_ven = ld_assegnato_ven - ld_spedizione_ven
//								ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)								
//							end if	
											
				ids_situazione_mag.setitem(ll_found, "spedizione_com", ld_spedizione_ven)
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

				ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
				ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
				
				ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com					
				ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose

				ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
			end if
		end if					
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------

loop

close cu_dettaglio_vendite;

return 1
end function

public function integer uf_commesse_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string ls_cod_prodotto_com, ls_flag_blocco_com, ls_flag_evasione_com, &
		 ls_sql, ls_cod_cat_mer_com, ls_des_prodotto_com, ls_cod_tipo_det_com, ls_flag_tipo_det_com, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long ll_anno_reg_acq, ll_anno_reg_com, ll_num_reg_acq, ll_num_reg_com, ll_found, ll_index
double ld_quan_prodotta, ld_quan_ordinata, ld_giacenza_com, &
		 ld_impegnato_com, ld_assegnato_com, ld_spedizione_com, ld_disp_reale_com, ld_disp_teorica_com, &
		 ld_scorta_minima_com, ld_scorta_max_com, ld_lotto_economico_com, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com


declare cu_commesse dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, quan_prodotta, quan_ordine from anag_commesse "
ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' " 

//selezionare le commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and ("+&
							  	"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += ")"
	end if
next
//// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
//ls_sql += " and cod_prodotto not in (" + &
//			 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//			 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

prepare sqlsa from :ls_sql;

open dynamic cu_commesse;

do while 1=1
	fetch cu_commesse into :ls_cod_prodotto_com, :ld_quan_prodotta, :ld_quan_ordinata;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura anagrafica commesse " + sqlca.sqlerrtext)
		return -1
	end if						
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		if not isnull(ls_cod_prodotto_com) then
		
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
		  into :ls_des_prodotto_com,
				 :ls_cod_cat_mer_com,
				 :ld_giacenza_com,
				 :ld_scorta_minima_com, 
				 :ld_scorta_max_com,
				 :ld_lotto_economico_com,
				 :ls_cod_tipo_politica_riordino,
				 :ld_livello_riordino
		  from anag_prodotti
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto = :ls_cod_prodotto_com
			and (cod_cat_mer like '%' or cod_cat_mer is null);			
				
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_com + " non esiste in anag_prodotti")
				return -1
			end if					
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_com) then ld_giacenza_com = 0
				if isnull(ld_scorta_minima_com) then ld_scorta_minima_com = 0
				if isnull(ld_scorta_max_com) then ld_scorta_max_com = 0 
				if isnull(ld_lotto_economico_com) then ld_lotto_economico_com = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
				
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_com + "'", 1, ids_situazione_mag.RowCount( ))	
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_com)
					
					ld_impegnato_com = 0
	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_com)
					
					ld_assegnato_com = 0
	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_com)
					
					ld_spedizione_com = 0
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_com = ld_giacenza_com - ld_impegnato_com - ld_assegnato_com - ld_spedizione_com
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
					
					if ld_quan_prodotta < ld_quan_ordinata then
						ld_prod_lanciata_com = ld_quan_ordinata - ld_quan_prodotta
					else 
						ld_prod_lanciata_com = 0
					end if	
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", ld_prod_lanciata_com)
					
					ld_disp_teorica_com = ld_disp_reale_com + ld_prod_lanciata_com
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_com)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_com)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_com)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_com)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_com
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_scorta_max_com - ld_disp_teorica_com
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_com < 0 then
								ld_da_produrre = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_da_ordinare = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_c_lavorazione = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore
					ld_disp_reale_com = ids_situazione_mag.getitemdecimal(ll_found, "disp_reale_com")
					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					
					if ld_quan_prodotta < ld_quan_ordinata then
						ld_prod_lanciata_com = ld_prod_lanciata_com + (ld_quan_ordinata - ld_quan_prodotta)
					else 
						ld_prod_lanciata_com = ld_prod_lanciata_com
					end if	
					
					ids_situazione_mag.setitem(ll_found, "prod_lanciata_com", ld_prod_lanciata_com)
	
					ld_disp_teorica_com = ld_disp_reale_com + ld_ord_a_fornitore + ld_prod_lanciata_com			
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_com)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;
	
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_com
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_scorta_max_com - ld_disp_teorica_com
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_com < 0 then
								ld_da_produrre = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_da_ordinare = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_com <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_com
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_scorta_max_com - ld_disp_teorica_com
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_com < 0 then
								ld_c_lavorazione = ld_disp_teorica_com * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_com <= ld_scorta_minima_com then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_com
							end if	
						end if								
					end choose
	
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
				end if					
			end if	
		end if	
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_com = 0
	ld_impegnato_com = 0
	ld_assegnato_com = 0
	ld_spedizione_com = 0
	ld_prod_lanciata_com = 0
	ld_disp_reale_com = 0
	ld_disp_teorica_com = 0
	ld_scorta_minima_com	= 0
	ld_scorta_max_com = 0
	ld_lotto_economico_com = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------

loop
close cu_commesse;

return 1
end function

public function integer uf_fatture_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_cod_tipo_fat_ven, &
		 ls_sql, ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino, &
		 ls_flag_tipo_fat_ven,ls_flag_blocco,ls_flag_movimenti
long   ll_anno_reg_acq, ll_anno_reg_ven, ll_num_reg_acq, ll_num_reg_ven, ll_found,ll_anno_registrazione_prec, &
       ll_num_registrazione_prec,ll_anno_registrazione,ll_num_registrazione
dec{4} ld_quan_fatture_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com
long	ll_index

declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select a.anno_registrazione, a.num_registrazione, a.cod_prodotto, a.quan_fatturata, a.cod_tipo_det_ven from det_fat_ven a, det_ord_ven b "

ls_sql = ls_sql + "where a.cod_azienda = '" + s_cs_xx.cod_azienda + "' and a.anno_registrazione = " + string(ll_anno_reg_ven) + " and a.num_registrazione = " + string(ll_num_reg_ven) + " and a.anno_reg_ord_ven is null and a.num_reg_ord_ven is null and a.anno_registrazione_bol_ven is null and a.num_registrazione is null "

//selezionare solo prodotti provenienti dall'impegnato delle commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and a.cod_prodotto in ("+&
							"SELECT DISTINCT cod_prodotto  " + &
			 				"FROM impegno_mat_prime_commessa " + &
			 				"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "+&
							  "and ("+&
							  		"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += "))"
	end if
next
//// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
//ls_sql += " and a.cod_prodotto not in (" + &
//			 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//			 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "


ll_anno_registrazione_prec = 0
ll_num_registrazione_prec  = 0

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_vendite;

do while true
	
	fetch cu_dettaglio_vendite into :ll_anno_registrazione, :ll_num_registrazione, :ls_cod_prodotto_ven, :ld_quan_fatture_ven, :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio fatture vendita " + sqlca.sqlerrtext)
		return -1
	end if	
	
	if sqlca.sqlcode = 100 then exit
		
	if ll_anno_registrazione <> ll_anno_registrazione_prec or ll_num_registrazione <> ll_num_registrazione_prec then
		
		select flag_blocco, 
			    flag_movimenti,
			    cod_tipo_fat_ven	
		into   :ls_flag_blocco,
		       :ls_flag_movimenti,
				 :ls_cod_tipo_fat_ven
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca fattura " +string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
		
		// salto le fatture bollcate e quelle già confermate
		if ls_flag_blocco = "S" or ls_flag_movimenti = "S" then continue	
				
		select flag_tipo_fat_ven
		  into :ls_flag_tipo_fat_ven
		  from tab_tipi_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and
			    cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in ricerca tipo fattura " +ls_cod_tipo_fat_ven + "~r~n" + sqlca.sqlerrtext)
			return -1		
		end if	
			
		if sqlca.sqlcode = 100 then ls_flag_tipo_fat_ven = 'I'
		
		// considero solo le fatture immediate
		if ls_flag_tipo_fat_ven <> "I" then continue
		
		ll_anno_registrazione_prec = ll_anno_registrazione
		ll_num_registrazione_prec = ll_num_registrazione
			
	end if
		
		
	select flag_tipo_det_ven
	  into :ls_flag_tipo_det_ven
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode <> 0 then	
		g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
		return -1				
	end if
	if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
	
		select des_prodotto,
				 cod_cat_mer,
				 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
				 scorta_minima,
				 scorta_massima,
				 lotto_economico,
				 cod_tipo_politica_riordino,
				 livello_riordino
	  into :ls_des_prodotto_ven,
			 :ls_cod_cat_mer_ven,
			 :ld_giacenza_ven,
			 :ld_scorta_minima_ven, 
			 :ld_scorta_max_ven,
			 :ld_lotto_economico_ven,
			 :ls_cod_tipo_politica_riordino,
			 :ld_livello_riordino
	  from anag_prodotti
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_prodotto = :ls_cod_prodotto_ven
		and (cod_cat_mer like '%' or cod_cat_mer is null);										
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
			return -1
		end if					
		if sqlca.sqlcode = 0 then
			
			if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
			if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
			if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
			if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
			if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
			
			ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
			if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
				ids_situazione_mag.insertrow(0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
				
				ld_impegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)
				
				ld_assegnato_ven = 0

				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)

				if ls_flag_tipo_fat_ven <> 'N' then
					ld_spedizione_ven = ld_quan_fatture_ven
				else
					ld_spedizione_ven = ld_quan_fatture_ven * (-1)
				end if	
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
				
				ld_disp_teorica_ven = ld_disp_reale_ven
				
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
			elseif ll_found > 0 then  //se il prodotto esiste già in datastore							
				ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
				ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
				ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")							
				if ls_flag_tipo_fat_ven <> 'N' then
					ld_spedizione_ven = ld_spedizione_ven + ld_quan_fatture_ven
				else
					ld_spedizione_ven = ld_spedizione_ven + (ld_quan_fatture_ven * (-1))
				end if	
				
//							if ld_spedizione_ven <> 0 then
//								ld_assegnato_ven = ld_assegnato_ven - ld_spedizione_ven
//								ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)								
//							end if	
				
				ids_situazione_mag.setitem(ll_found, "spedizione_com", ld_spedizione_ven)
				
				ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
				ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

				ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
				ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
				ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com
				ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
				
				select flag_tipo_riordino,
						 flag_modo_riordino, 
						 livello_magazzino
				  into :ls_flag_tipo_riordino,
						 :ls_flag_modo_riordino,
						 :ld_livello_magazzino
				  from tab_tipi_politiche_riordino
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
					return -1				
				end if	
				if sqlca.sqlcode = 100 then
					ls_flag_tipo_riordino = "A"
					ls_flag_modo_riordino = "C"
				end if	
				
				choose case ls_flag_tipo_riordino
				case "C"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_produrre = ld_lotto_economico_ven
						end if	
					elseif ls_flag_modo_riordino = "M" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
						end if	
					elseif ls_flag_modo_riordino = "C" then
						if ld_disp_teorica_ven < 0 then
							ld_da_produrre = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if	
				case "A"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_da_ordinare = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_da_ordinare = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				case "T"
					if ls_flag_modo_riordino = "R" then
						if ld_disp_teorica_ven <= ld_livello_riordino then
							ld_c_lavorazione = ld_lotto_economico_ven
						end if										
					elseif ls_flag_modo_riordino = "M" then	
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
						end if										
					elseif ls_flag_modo_riordino = "C" then	
						if ld_disp_teorica_ven < 0 then
							ld_c_lavorazione = ld_disp_teorica_ven * (-1)
						end if	
					elseif ls_flag_modo_riordino = "L" then
						if ld_disp_teorica_ven <= ld_scorta_minima_ven then
							ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
						end if	
					end if								
				end choose
					
				ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
				ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
				ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
				ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")			
			end if
		end if					
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop

close cu_dettaglio_vendite;	

return 1
end function

public function integer uf_ordini_acquisto_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string ls_cod_prodotto_acq, ls_flag_blocco_acq, ls_flag_evasione_acq, ls_sql, &
		 ls_cod_cat_mer_acq, ls_des_prodotto_acq, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_found
dec{4} ld_impegnato_acq, ld_assegnato_acq, ld_spedizione_acq, ld_disp_reale_acq, ld_disp_teorica_acq, &
		 ld_scorta_minima_acq, ld_scorta_max_acq, ld_lotto_economico_acq, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_quan_ordinata, ld_quan_arrivata, ld_giacenza_acq, ld_ord_a_fornitore, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_prod_lanciata_com
long	ll_index

declare cu_dettaglio_acquisti dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, quan_ordinata, quan_arrivata, cod_tipo_det_acq from det_ord_acq "

ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' and flag_saldo = 'N' "

//selezionare solo prodotti provenienti dall'impegnato delle commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and det_ord_acq.cod_prodotto in ("+&
							"SELECT DISTINCT cod_prodotto  " + &
			 				"FROM impegno_mat_prime_commessa " + &
			 				"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "+&
							  "and ("+&
							  		"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += "))"
	end if
next
//// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
//ls_sql += " and cod_prodotto not in (" + &
//			 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//			 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

prepare sqlsa from :ls_sql;
open dynamic cu_dettaglio_acquisti;

do while true
	fetch cu_dettaglio_acquisti into :ls_cod_prodotto_acq, :ld_quan_ordinata, :ld_quan_arrivata, :ls_cod_tipo_det_acq;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio ordini acquisto " + sqlca.sqlerrtext)
		return -1
	end if		
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = 0 then
		
		select flag_tipo_det_acq
		  into :ls_flag_tipo_det_acq
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_acq " + sqlca.sqlerrtext)
			return -1				
		end if
		if sqlca.sqlcode = 0 and ls_flag_tipo_det_acq = "M" then
		
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_acq,
					 :ls_cod_cat_mer_acq,
					 :ld_giacenza_acq,
					 :ld_scorta_minima_acq, 
					 :ld_scorta_max_acq,
					 :ld_lotto_economico_acq,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_acq
				and (cod_cat_mer like '%' or cod_cat_mer is null);
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_acq + " non esiste in anag_prodotti")
				return -1
			end if	
			
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_acq) then ld_giacenza_acq = 0
				if isnull(ld_scorta_minima_acq) then ld_scorta_minima_acq = 0
				if isnull(ld_scorta_max_acq) then ld_scorta_max_acq = 0 
				if isnull(ld_lotto_economico_acq) then ld_lotto_economico_acq = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0						
				
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_acq + "'", 1, ids_situazione_mag.RowCount( ))	
				
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_acq)

					ld_impegnato_acq = 0
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_acq)
					
					ld_assegnato_acq = 0

					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_acq)
					
					ld_spedizione_acq = 0							
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_acq = ld_giacenza_acq - ld_impegnato_acq - ld_assegnato_acq - ld_spedizione_acq
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_acq)

					ld_ord_a_fornitore = ld_quan_ordinata - ld_quan_arrivata
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", ld_ord_a_fornitore)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
					
					ld_disp_teorica_acq = ld_disp_reale_acq + ld_ord_a_fornitore
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_acq)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_acq)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_acq)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_acq)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_acq
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_scorta_max_acq - ld_disp_teorica_acq
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_acq < 0 then
								ld_da_produrre = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_da_ordinare = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_c_lavorazione = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
					
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore								
					
					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					ld_ord_a_fornitore = ld_ord_a_fornitore + (ld_quan_ordinata - ld_quan_arrivata)
					
					ids_situazione_mag.setitem(ll_found, "ord_a_fornitore_com", ld_ord_a_fornitore)							

					ld_disp_reale_acq = ids_situazione_mag.getitemdecimal(ll_found, "disp_reale_com")
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					ld_disp_teorica_acq = ld_disp_reale_acq + ld_ord_a_fornitore + ld_prod_lanciata_com
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_acq)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_acq
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_scorta_max_acq - ld_disp_teorica_acq
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_acq < 0 then
								ld_da_produrre = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_da_ordinare = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_acq <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_acq
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_scorta_max_acq - ld_disp_teorica_acq
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_acq < 0 then
								ld_c_lavorazione = ld_disp_teorica_acq * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_acq <= ld_scorta_minima_acq then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_acq
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")
				end if
			end if					
		end if	
	end if	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_acq = 0
	ld_impegnato_acq = 0
	ld_assegnato_acq = 0
	ld_spedizione_acq = 0
	ld_disp_reale_acq = 0
	ld_ord_a_fornitore = 0
	ld_disp_teorica_acq = 0
	ld_scorta_minima_acq	= 0
	ld_scorta_max_acq = 0
	ld_lotto_economico_acq = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop
close cu_dettaglio_acquisti;

return 1
end function

public function integer uf_ordini_vendita_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string ls_cod_prodotto_ven, ls_flag_blocco_ven, ls_flag_evasione_ven, ls_sql,&
		 ls_cod_cat_mer_ven, ls_des_prodotto_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long   ll_num_reg_acq, ll_num_reg_ven, ll_found
dec{4} ld_quan_evasa_ven, ld_quan_ordine_ven, ld_quan_in_evasione_ven, ld_giacenza_ven, &
		 ld_impegnato_ven, ld_assegnato_ven, ld_spedizione_ven, ld_disp_reale_ven, ld_disp_teorica_ven, &
		 ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_da_produrre, &
		 ld_da_ordinare, ld_c_lavorazione, ld_livello_riordino, ld_livello_magazzino, &
		 ld_ord_a_fornitore, ld_prod_lanciata_com
long	ll_index

declare cu_dettaglio_vendite dynamic cursor for sqlsa;

ls_sql = "select cod_prodotto, flag_blocco, flag_evasione, quan_in_evasione, quan_evasa, quan_ordine, cod_tipo_det_ven from det_ord_ven "
ls_sql = ls_sql + "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N' "

//selezionare solo prodotti provenienti dall'impegnato delle commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and cod_prodotto in ("+&
							"SELECT DISTINCT cod_prodotto  " + &
			 				"FROM impegno_mat_prime_commessa " + &
			 				"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "+&
							  "and ("+&
							  		"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += "))"
	end if
next
//// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati
//ls_sql += " and cod_prodotto not in (" + &
//			 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//			 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio_vendite;

do while true
	fetch cu_dettaglio_vendite into :ls_cod_prodotto_ven, 
											  :ls_flag_blocco_ven, 
											  :ls_flag_evasione_ven, 
											  :ld_quan_in_evasione_ven, 
											  :ld_quan_evasa_ven, 
											  :ld_quan_ordine_ven, 
											  :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dettaglio ordini vendita " + sqlca.sqlerrtext)
		return -1
	end if			
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		select flag_tipo_det_ven
		  into :ls_flag_tipo_det_ven
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
		if sqlca.sqlcode <> 0 then	
			g_mb.messagebox("Apice", "Errore in lettura tab_tipi_det_ven " + sqlca.sqlerrtext)
			return -1				
		end if
		if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
			select des_prodotto,
					 cod_cat_mer,
					 (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),
					 scorta_minima,
					 scorta_massima,
					 lotto_economico,
					 cod_tipo_politica_riordino,
					 livello_riordino
			  into :ls_des_prodotto_ven,
					 :ls_cod_cat_mer_ven,
					 :ld_giacenza_ven,
					 :ld_scorta_minima_ven, 
					 :ld_scorta_max_ven,
					 :ld_lotto_economico_ven,
					 :ls_cod_tipo_politica_riordino,
					 :ld_livello_riordino
			  from anag_prodotti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto_ven
				and (cod_cat_mer like '%' or cod_cat_mer is null);
				
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice", "Errore, il prodotto " + ls_cod_prodotto_ven + " non esiste in anag_prodotti")
				return -1
			end if					
			if sqlca.sqlcode = 0 then
				
				if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
				if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
				if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
				if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
				if isnull(ld_livello_riordino) then ld_livello_riordino = 0
										
				ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
				if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore
					ids_situazione_mag.insertrow(0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
					
					if ls_flag_evasione_ven = "A" then
						if ld_quan_in_evasione_ven = 0 then
							ld_impegnato_ven = ld_quan_ordine_ven
						else
							ld_impegnato_ven = ld_quan_ordine_ven - ld_quan_in_evasione_ven
						end if	
					elseif ls_flag_evasione_ven = "P" then
						ld_impegnato_ven = ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven 
					else
						ld_impegnato_ven = 0
					end if	
					
					ld_assegnato_ven = ld_quan_in_evasione_ven

//							if ld_assegnato_ven <> 0 and ls_flag_evasione_ven = "A" then
//								ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven)
//							end if	

					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", ld_impegnato_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", ld_assegnato_ven)
					
					ld_spedizione_ven = 0							
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", ld_spedizione_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
					
					ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_disp_reale_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
					
					ld_disp_teorica_ven = ld_disp_reale_ven
					
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_disp_teorica_ven)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					into   :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					from   tab_tipi_politiche_riordino
					where  cod_azienda = :s_cs_xx.cod_azienda and 
							 cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_ven
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_ven < 0 then
								ld_da_produrre = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_da_ordinare = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_c_lavorazione = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if								
					end choose
						
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				elseif ll_found > 0 then  //se il prodotto esiste già in datastore
					
					ld_impegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "impegnato_com")
					if ls_flag_evasione_ven = "A" then
						if ld_quan_in_evasione_ven = 0 then
							ld_impegnato_ven = ld_impegnato_ven + ld_quan_ordine_ven
						else
							ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_in_evasione_ven)
						end if								
//								ld_impegnato_ven = ld_impegnato_ven + ld_quan_ordine_ven
					elseif ls_flag_evasione_ven = "P" then
						ld_impegnato_ven = ld_impegnato_ven + (ld_quan_ordine_ven - ld_quan_evasa_ven - ld_quan_in_evasione_ven)
					else
						ld_impegnato_ven = ld_impegnato_ven
					end if	
					
					ld_assegnato_ven = ids_situazione_mag.getitemdecimal(ll_found, "assegnato_com")
					ld_assegnato_ven = ld_assegnato_ven + ld_quan_in_evasione_ven
					
					ids_situazione_mag.setitem(ll_found, "impegnato_com", ld_impegnato_ven)
					ids_situazione_mag.setitem(ll_found, "assegnato_com", ld_assegnato_ven)							

					ld_spedizione_ven = ids_situazione_mag.getitemdecimal(ll_found, "spedizione_com")
					ld_disp_reale_ven = ld_giacenza_ven - ld_impegnato_ven - ld_assegnato_ven - ld_spedizione_ven
					ids_situazione_mag.setitem(ll_found, "disp_reale_com", ld_disp_reale_ven)							

					ld_ord_a_fornitore = ids_situazione_mag.getitemdecimal(ll_found, "ord_a_fornitore_com")
					ld_prod_lanciata_com = ids_situazione_mag.getitemdecimal(ll_found, "prod_lanciata_com")
					
					ld_disp_teorica_ven = ld_disp_reale_ven + ld_ord_a_fornitore + ld_prod_lanciata_com
					ids_situazione_mag.setitem(ll_found, "disp_teorica_com", ld_disp_teorica_ven)
					
					select flag_tipo_riordino,
							 flag_modo_riordino, 
							 livello_magazzino
					  into :ls_flag_tipo_riordino,
							 :ls_flag_modo_riordino,
							 :ld_livello_magazzino
					  from tab_tipi_politiche_riordino
					 where cod_azienda = :s_cs_xx.cod_azienda
						and cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino;

					if sqlca.sqlcode < 0 then
						g_mb.messagebox("Apice", "Errore in lettura tab_tipi_politiche_riordino " + sqlca.sqlerrtext)
						return -1				
					end if	
					if sqlca.sqlcode = 100 then
						ls_flag_tipo_riordino = "A"
						ls_flag_modo_riordino = "C"
					end if	
					
					choose case ls_flag_tipo_riordino
					case "C"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_produrre = ld_lotto_economico_ven
							end if	
						elseif ls_flag_modo_riordino = "M" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_scorta_max_ven - ld_disp_teorica_ven
							end if	
						elseif ls_flag_modo_riordino = "C" then
							if ld_disp_teorica_ven < 0 then
								ld_da_produrre = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_produrre = ld_livello_magazzino - ld_disp_teorica_ven
							end if	
						end if	
					case "A"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_da_ordinare = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_da_ordinare = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_da_ordinare = ld_livello_magazzino - ld_disp_teorica_ven										
							end if	
						end if								
					case "T"
						if ls_flag_modo_riordino = "R" then
							if ld_disp_teorica_ven <= ld_livello_riordino then
								ld_c_lavorazione = ld_lotto_economico_ven
							end if										
						elseif ls_flag_modo_riordino = "M" then	
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_scorta_max_ven - ld_disp_teorica_ven
							end if										
						elseif ls_flag_modo_riordino = "C" then	
							if ld_disp_teorica_ven < 0 then
								ld_c_lavorazione = ld_disp_teorica_ven * (-1)
							end if	
						elseif ls_flag_modo_riordino = "L" then
							if ld_disp_teorica_ven <= ld_scorta_minima_ven then
								ld_c_lavorazione = ld_livello_magazzino - ld_disp_teorica_ven										
							end if	
						end if								
					end choose
					
					ids_situazione_mag.setitem(ll_found, "da_produrre_com", ld_da_produrre)
					ids_situazione_mag.setitem(ll_found, "da_ordinare_com", ld_da_ordinare)
					ids_situazione_mag.setitem(ll_found, "c_lavorazione_com", ld_c_lavorazione)
					ids_situazione_mag.setitem(ll_found, "flag_eseguito_com", "N")							
				end if
			end if					
		end if	
	end if	
	
//------------------------------------- Azzero le variabili di comodo ---------------------
	ld_giacenza_ven = 0
	ld_impegnato_ven = 0
	ld_assegnato_ven = 0
	ld_spedizione_ven = 0
	ld_disp_reale_ven = 0
	ld_disp_teorica_ven = 0
	ld_scorta_minima_ven	= 0
	ld_scorta_max_ven = 0
	ld_lotto_economico_ven = 0
	ld_da_produrre = 0
	ld_da_ordinare = 0
	ld_c_lavorazione = 0
//-----------------------------------------------------------------------------------------
	
loop
close cu_dettaglio_vendite;

return 1
end function

public function integer uf_prodotti_generale_commesse (long fl_anno_commessa[], long fl_num_commessa[], ref datastore ids_situazione_mag);string   ls_sql, ls_cod_prodotto_ven, ls_des_prodotto_ven, ls_cod_cat_mer_ven, ls_cod_tipo_politica_riordino, ls_flag_tipo_riordino, ls_flag_modo_riordino
long     ll_found
dec{4}   ld_giacenza_ven, ld_scorta_minima_ven, ld_scorta_max_ven, ld_lotto_economico_ven, ld_livello_riordino, ld_livello_magazzino
long		ll_index

declare cu_prodotti dynamic cursor for sqlsa;
		
ls_sql = " select cod_prodotto, " + & 
         "        des_prodotto, " + &
         "        cod_cat_mer,  " + & 
			"        (saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita),  " + &
			"        scorta_minima,  " + &
			"        scorta_massima,  " + & 
			"        lotto_economico, " + &
			"        cod_tipo_politica_riordino,  " + &
			"        livello_riordino " + &
			" from   anag_prodotti " + &
         " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			"        flag_blocco = 'N' "
		
//selezionare solo prodotti provenienti dall'impegnato delle commesse in esame
for ll_index = 1 to upperbound(fl_anno_commessa)
	if ll_index = 1 then
		ls_sql += " and cod_prodotto in ("+&
							"SELECT DISTINCT cod_prodotto  " + &
			 				"FROM impegno_mat_prime_commessa " + &
			 				"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "+&
							  "and ("+&
							  		"(anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	else
		ls_sql +=" or (anno_commessa="+string(fl_anno_commessa[ll_index])+" and num_commessa="+string(fl_num_commessa[ll_index])+") "
	end if
	
	if ll_index = upperbound(fl_anno_commessa) then
		ls_sql += "))"
	end if
next
//// aggiunto da EnMe 02/07/2008 su richiesta di Unifast; specifica "report_situazione_mag_modifica"
//// se attivato il flag_mp_commesse il sistema NON deve visualizzare i semilavorati, ma solo 
//ls_sql += " and cod_prodotto not in (" + &
//			 " SELECT DISTINCT cod_prodotto FROM avan_produzione_com " + &
//			 " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N') "

ls_sql += " order by cod_prodotto ASC "		
		
prepare sqlsa from :ls_sql;
open dynamic cu_prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore prodotti: " + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	fetch cu_prodotti into :ls_cod_prodotto_ven, 
	                       :ls_des_prodotto_ven, 
								  :ls_cod_cat_mer_ven, 
								  :ld_giacenza_ven, 
								  :ld_scorta_minima_ven, 
								  :ld_scorta_max_ven, 
								  :ld_lotto_economico_ven,
								  :ls_cod_tipo_politica_riordino,
								  :ld_livello_riordino;
								  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore apertura cursore cu_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if			
	if sqlca.sqlcode = 100 then exit
	
	ll_found = ids_situazione_mag.Find("cod_prodotto_com = '" + ls_cod_prodotto_ven + "'", 1, ids_situazione_mag.RowCount( ))	
	if ll_found = 0 or isnull(ll_found) then  // se il prodotto non esiste in datastore	
	
		if isnull(ld_giacenza_ven) then ld_giacenza_ven = 0
		if isnull(ld_scorta_minima_ven) then ld_scorta_minima_ven = 0
		if isnull(ld_scorta_max_ven) then ld_scorta_max_ven = 0 
		if isnull(ld_lotto_economico_ven) then ld_lotto_economico_ven = 0
		if isnull(ld_livello_riordino) then ld_livello_riordino = 0
												
		ids_situazione_mag.insertrow(0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "cod_prodotto_com", ls_cod_prodotto_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "des_prodotto_com", ls_des_prodotto_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "giacenza_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "impegnato_com", 0)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "assegnato_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "spedizione_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "anticipi_com", 0)						
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_reale_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "ord_a_fornitore_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "prod_lanciata_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "disp_teorica_com", ld_giacenza_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_minima_com", ld_scorta_minima_ven)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "scorta_max_com", ld_scorta_max_ven)	
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "lotto_economico_acq_com", ld_lotto_economico_ven)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_produrre_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "da_ordinare_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "c_lavorazione_com", 0)
		ids_situazione_mag.setitem(ids_situazione_mag.RowCount(), "flag_eseguito_com", "N")
				
		ld_giacenza_ven = 0
		ld_scorta_minima_ven	= 0
		ld_scorta_max_ven = 0
		ld_lotto_economico_ven = 0
	end if				
loop

close cu_prodotti;

return 1
end function

public subroutine uof_set_nomenclatura (string as_cod_nomenclatura);

is_cod_nomenclatura = as_cod_nomenclatura
end subroutine

on uo_situazione_magazzino.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_situazione_magazzino.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

