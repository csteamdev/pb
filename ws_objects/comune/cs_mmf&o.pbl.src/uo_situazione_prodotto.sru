﻿$PBExportHeader$uo_situazione_prodotto.sru
$PBExportComments$User Object Visione situazione Prodotto
forward
global type uo_situazione_prodotto from userobject
end type
type st_message from statictext within uo_situazione_prodotto
end type
type dw_anagrafica from datawindow within uo_situazione_prodotto
end type
end forward

global type uo_situazione_prodotto from userobject
integer width = 3301
integer height = 264
boolean border = true
long backcolor = 12632256
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
st_message st_message
dw_anagrafica dw_anagrafica
end type
global uo_situazione_prodotto uo_situazione_prodotto

forward prototypes
public subroutine uof_aggiorna (string as_cod_prodotto)
public subroutine uof_message (string as_message)
public subroutine uof_ultimo_prezzo (string as_cod_cliente, string as_cod_prodotto)
end prototypes

public subroutine uof_aggiorna (string as_cod_prodotto);long ll_ret

dw_anagrafica.settransobject(sqlca)
ll_ret = dw_anagrafica.retrieve(s_cs_xx.cod_azienda, as_cod_prodotto)

this.show()
return
end subroutine

public subroutine uof_message (string as_message);st_message.text = as_message
return
end subroutine

public subroutine uof_ultimo_prezzo (string as_cod_cliente, string as_cod_prodotto);string ls_messaggio
double ld_ultimo_prezzo

if f_cerca_ultimo_prezzo_vendita_fatture (as_cod_cliente, as_cod_prodotto, ld_ultimo_prezzo, ls_messaggio ) = 0 then
	st_message.text = ls_messaggio
else
	st_message.text = ""
end if

this.show()

return

end subroutine

on uo_situazione_prodotto.create
this.st_message=create st_message
this.dw_anagrafica=create dw_anagrafica
this.Control[]={this.st_message,&
this.dw_anagrafica}
end on

on uo_situazione_prodotto.destroy
destroy(this.st_message)
destroy(this.dw_anagrafica)
end on

type st_message from statictext within uo_situazione_prodotto
integer y = 8
integer width = 3278
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_anagrafica from datawindow within uo_situazione_prodotto
integer y = 92
integer width = 3296
integer height = 172
integer taborder = 10
string title = "none"
string dataobject = "d_situazione_prodotto_anag_prodotto"
boolean border = false
end type

