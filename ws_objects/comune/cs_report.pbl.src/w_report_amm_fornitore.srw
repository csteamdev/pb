﻿$PBExportHeader$w_report_amm_fornitore.srw
forward
global type w_report_amm_fornitore from w_cs_xx_principale
end type
type dw_report_amm_fornitore from uo_cs_xx_dw within w_report_amm_fornitore
end type
end forward

global type w_report_amm_fornitore from w_cs_xx_principale
integer x = 18
integer y = 20
integer width = 3598
integer height = 1900
string title = "Reort Dati Amministrativi Fornitore"
dw_report_amm_fornitore dw_report_amm_fornitore
end type
global w_report_amm_fornitore w_report_amm_fornitore

on w_report_amm_fornitore.create
int iCurrent
call super::create
this.dw_report_amm_fornitore=create dw_report_amm_fornitore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_amm_fornitore
end on

on w_report_amm_fornitore.destroy
call super::destroy
destroy(this.dw_report_amm_fornitore)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_amm_fornitore.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_amm_fornitore.set_dw_key("cod_azienda")
dw_report_amm_fornitore.set_dw_key("cod_fornitore")

dw_report_amm_fornitore.set_dw_options(sqlca, &
                                i_openparm, &
                                c_scrollparent + &
                                c_nomodify + &
										  c_nonew + &
                                c_nodelete + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

iuo_dw_main = dw_report_amm_fornitore
end event

type dw_report_amm_fornitore from uo_cs_xx_dw within w_report_amm_fornitore
integer x = 23
integer y = 20
integer width = 3456
integer height = 1932
string dataobject = "d_report_amm_fornitore"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_fornitore

ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

