﻿$PBExportHeader$w_report_gener_cliente.srw
forward
global type w_report_gener_cliente from w_cs_xx_principale
end type
type dw_report_gener_cliente from uo_cs_xx_dw within w_report_gener_cliente
end type
end forward

global type w_report_gener_cliente from w_cs_xx_principale
integer x = 18
integer y = 20
integer width = 3598
integer height = 1900
string title = "Report Dati Generali Cliente"
dw_report_gener_cliente dw_report_gener_cliente
end type
global w_report_gener_cliente w_report_gener_cliente

type variables
string is_cod_cliente
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_gener_cliente.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_gener_cliente.set_dw_key("cod_azienda")
dw_report_gener_cliente.set_dw_key("cod_cliente")

dw_report_gener_cliente.set_dw_options(sqlca, &
                                i_openparm, &
                                c_scrollparent + &
                                c_nomodify + &
										  c_nonew + &
                                c_nodelete + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

iuo_dw_main = dw_report_gener_cliente
end event

on w_report_gener_cliente.create
int iCurrent
call super::create
this.dw_report_gener_cliente=create dw_report_gener_cliente
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_gener_cliente
end on

on w_report_gener_cliente.destroy
call super::destroy
destroy(this.dw_report_gener_cliente)
end on

type dw_report_gener_cliente from uo_cs_xx_dw within w_report_gener_cliente
integer x = 23
integer y = 20
integer width = 3557
integer height = 1932
integer taborder = 10
string dataobject = "d_report_gener_cliente"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_cliente
date ldd_dataoggi

ldd_dataoggi=today()

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente,ldd_dataoggi)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

