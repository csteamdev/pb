﻿$PBExportHeader$w_report_gener_fornitore.srw
forward
global type w_report_gener_fornitore from w_cs_xx_principale
end type
type dw_report_fornitore_gen from uo_cs_xx_dw within w_report_gener_fornitore
end type
end forward

global type w_report_gener_fornitore from w_cs_xx_principale
integer x = 18
integer y = 20
integer width = 3598
integer height = 1900
string title = "Reort Dati Generali Fornitore"
dw_report_fornitore_gen dw_report_fornitore_gen
end type
global w_report_gener_fornitore w_report_gener_fornitore

on w_report_gener_fornitore.create
int iCurrent
call super::create
this.dw_report_fornitore_gen=create dw_report_fornitore_gen
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_fornitore_gen
end on

on w_report_gener_fornitore.destroy
call super::destroy
destroy(this.dw_report_fornitore_gen)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_fornitore_gen.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_fornitore_gen.set_dw_key("cod_azienda")
dw_report_fornitore_gen.set_dw_key("cod_fornitore")

dw_report_fornitore_gen.set_dw_options(sqlca, &
                                i_openparm, &
                                c_scrollparent + &
                                c_nomodify + &
										  c_nonew + &
                                c_nodelete + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

iuo_dw_main = dw_report_fornitore_gen
end event

type dw_report_fornitore_gen from uo_cs_xx_dw within w_report_gener_fornitore
integer x = 23
integer y = 20
integer width = 3557
integer height = 1932
string dataobject = "d_report_fornitore_gen"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_cliente
// date ldd_dataoggi

// ldd_dataoggi=today()

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
// ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente,ldd_dataoggi)
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

