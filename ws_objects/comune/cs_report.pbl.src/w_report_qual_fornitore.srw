﻿$PBExportHeader$w_report_qual_fornitore.srw
forward
global type w_report_qual_fornitore from w_cs_xx_principale
end type
type dw_report_fornitore_quality from uo_cs_xx_dw within w_report_qual_fornitore
end type
end forward

global type w_report_qual_fornitore from w_cs_xx_principale
integer x = 18
integer y = 20
integer width = 3886
integer height = 2080
string title = "Reort Dati Qualità Fornitore"
dw_report_fornitore_quality dw_report_fornitore_quality
end type
global w_report_qual_fornitore w_report_qual_fornitore

type variables
string is_AZP = "0"
end variables

on w_report_qual_fornitore.create
int iCurrent
call super::create
this.dw_report_fornitore_quality=create dw_report_fornitore_quality
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_fornitore_quality
end on

on w_report_qual_fornitore.destroy
call super::destroy
destroy(this.dw_report_fornitore_quality)
end on

event pc_setwindow;call super::pc_setwindow;string			ls_path_logo, ls_modify


dw_report_fornitore_quality.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_fornitore_quality.set_dw_key("cod_azienda")
dw_report_fornitore_quality.set_dw_key("cod_fornitore")

dw_report_fornitore_quality.set_dw_options(sqlca, &
                                i_openparm, &
                                c_scrollparent + &
                                c_nomodify + &
										  c_nonew + &
                                c_nodelete + &
                                c_disableCC, &
                                c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowpointer +&
                                c_nocursorrowfocusrect )

iuo_dw_main = dw_report_fornitore_quality

//se cliente sintexcal il path del logo non si compone come volume + parametro LOA
//bensi è tutto nel parametro LOA
//inoltre per sintexcal visualizza la revisione del documento e modifica le dimensioni dell'oggetto che conterrà il logo
select stringa
into   :is_AZP
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull( is_AZP ) and is_AZP<>"" then
	//sintexcal
	is_AZP = "1"
else
	//no sintexcal
	is_AZP = "0"
end if

select parametri_azienda.stringa
into   :ls_path_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LOA';

//if sqlca.sqlcode < 0 then g_mb.messagebox("OMNIA","Errore sul DB:"+ sqlca.sqlerrtext,stopsign!)
//if sqlca.sqlcode = 100 then g_mb.messagebox("OMNIA","manca il parametro LOA in parametri azienda, pertanto non apparirà il logo aziendale",stopsign!)

if is_AZP="1" then
	//sintexcal
	ls_modify = "logo.filename='" + ls_path_logo + "'"
	dw_report_fornitore_quality.object.cf_revisione.visible=1
	dw_report_fornitore_quality.object.cf_azienda.visible=0
	
	dw_report_fornitore_quality.Object.logo.Width = 220
	dw_report_fornitore_quality.Object.logo.Height = 45
else
	//standard
	//rendi invisible la revisione e la data del documento
	dw_report_fornitore_quality.object.cf_revisione.visible=0
	dw_report_fornitore_quality.object.cf_azienda.visible=1
	//ls_modify = "logo.filename='" + s_cs_xx.volume + ls_path_logo + "'"
	ls_modify = "logo.filename=''"
end if

dw_report_fornitore_quality.modify(ls_modify)


//-------------------------------------------------------------------------------------------
end event

type dw_report_fornitore_quality from uo_cs_xx_dw within w_report_qual_fornitore
integer x = 23
integer y = 20
integer width = 3790
integer height = 1932
string dataobject = "d_report_fornitore_quality"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_fornitore
// date ldd_dataoggi

// ldd_dataoggi=today()

ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
// ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cliente,ldd_dataoggi)
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

