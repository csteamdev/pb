﻿$PBExportHeader$w_report_std.srw
$PBExportComments$Window Standard per Report ( da copiare )
forward
global type w_report_std from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_std
end type
type cb_report from commandbutton within w_report_std
end type
type cb_selezione from commandbutton within w_report_std
end type
type dw_selezione from uo_cs_xx_dw within w_report_std
end type
type dw_report from uo_cs_xx_dw within w_report_std
end type
type dw_folder from u_folder within w_report_std
end type
end forward

global type w_report_std from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Materiali Accettati"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_std w_report_std

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_annulla
lw_oggetti[3] = cb_report
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)

dw_folder.fu_selecttab(1)

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)


//set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

end event

on w_report_std.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_std.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

type cb_annulla from commandbutton within w_report_std
integer x = 1349
integer y = 420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_std
integer x = 1737
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

on clicked;//string ls_prodotto, ls_cat_mer
//date   ld_da_data, ld_a_data
//
//
//ls_prodotto = dw_selezione.getitemstring(1,"rs_cod_prodotto")
//ls_cat_mer  = dw_selezione.getitemstring(1,"rs_cat_prodotto")
//ld_da_data  = dw_selezione.getitemdate(1,"rd_da_data")
//ld_a_data   = dw_selezione.getitemdate(1,"rd_a_data")
//
//if isnull(ls_prodotto) then ls_prodotto = "%"
//if isnull(ls_cat_mer) then  ls_cat_mer  = "%"
//if isnull(ld_da_data) then  ld_da_data  = date("01/01/1900")
//if isnull(ld_a_data)  then  ld_a_data   = date("31/12/2999")
//
//dw_selezione.hide()
//
//parent.x = 100
//parent.y = 50
//parent.width = 3553
//parent.height = 1665
//
//dw_report.show()
////dw_report.retrieve(s_cs_xx.cod_azienda, ls_prodotto, ld_da_data, ld_a_data, ls_cat_mer)
//cb_selezione.show()
//
//dw_report.change_dw_current()
end on

type cb_selezione from commandbutton within w_report_std
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;string ls_prodotto, ls_cat_mer

dw_selezione.show()

//parent.x = 741
//parent.y = 885
//parent.width = 2172
//parent.height = 633

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_std
integer x = 23
integer y = 20
integer width = 2080
integer height = 380
integer taborder = 20
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_std
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_std
integer x = 27
integer y = 20
integer width = 3470
integer height = 1520
integer taborder = 40
end type

