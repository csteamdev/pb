﻿$PBExportHeader$w_agenda.srw
$PBExportComments$Finestra Agenda
forward
global type w_agenda from w_cs_xx_principale
end type
type ddlb_1 from dropdownlistbox within w_agenda
end type
type st_1 from statictext within w_agenda
end type
type pb_3 from picturebutton within w_agenda
end type
type pb_4 from picturebutton within w_agenda
end type
type pb_1 from picturebutton within w_agenda
end type
type pb_2 from picturebutton within w_agenda
end type
type dw_allocazione_giornata from datawindow within w_agenda
end type
type st_informazioni from statictext within w_agenda
end type
type em_data from editmask within w_agenda
end type
type st_2 from statictext within w_agenda
end type
end forward

shared variables

end variables

global type w_agenda from w_cs_xx_principale
int Width=1989
int Height=1641
boolean TitleBar=true
string Title="Agenda"
ddlb_1 ddlb_1
st_1 st_1
pb_3 pb_3
pb_4 pb_4
pb_1 pb_1
pb_2 pb_2
dw_allocazione_giornata dw_allocazione_giornata
st_informazioni st_informazioni
em_data em_data
st_2 st_2
end type
global w_agenda w_agenda

type variables
string is_cod_tipo_agenda
datetime idt_data_giorno, idt_ora_inizio, idt_ora_fine, &
idt_intervallo
datastore ids_impegni_giornalieri, ids_impegni_settimanali, &
ids_impegni_mensili
end variables

forward prototypes
public function integer wf_crea_pagina_agenda ()
public function integer wf_carica_ddlb ()
public function integer wf_crea_datastore ()
public function integer wf_ricerca_impegni (datetime wdt_data, ref datetime wdt_ora_impegno[], ref string ws_des_impegno[], ref datetime wdt_data_impegno[])
end prototypes

public function integer wf_crea_pagina_agenda ();datetime ldt_data_impegno[], ldt_ora_step, ldt_ora_agenda[], ldt_ora_impegno[], ldt_orario_agenda
long ll_riga, ll_cont, ll_cont_1
string ls_note_agenda[], ls_flag_fatto[], ls_sql, ls_nota_impegno[], ls_str


dw_allocazione_giornata.setredraw(false)
dw_allocazione_giornata.reset()


/////
declare cu_agenda dynamic cursor for sqlsa;
ll_cont = 1
ll_cont_1 = 1

ls_sql = "select agende.ora_agenda, " + &
         "agende.note, " + &
         "agende.flag_fatto " + &
         "from   agende " + &
         "where agende.cod_azienda = '"+ s_cs_xx.cod_azienda + "' and " + &
         "agende.cod_tipo_agenda = '"+ is_cod_tipo_agenda +"' and " + & 
		   "agende.cod_utente = '"+ s_cs_xx.cod_utente +"' and " + &
		   "agende.data_agenda = '" + STRING(idt_data_giorno,s_cs_xx.db_funzioni.formato_data) + "'  " + &
			"ORDER BY ora_agenda ASC"

prepare sqlsa from :ls_sql;

open dynamic cu_agenda;

do while 1=1
   fetch cu_agenda into :ldt_ora_agenda[ll_cont], :ls_note_agenda[ll_cont], :ls_flag_fatto[ll_cont];
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_cont = ll_cont + 1
loop
close cu_agenda;


wf_ricerca_impegni(idt_data_giorno, ldt_ora_impegno[], ls_nota_impegno[], ldt_data_impegno[])
ll_cont = 1
ldt_ora_step = idt_ora_inizio
ldt_orario_agenda = idt_ora_inizio

do
	if (time(ldt_ora_step) = time(ldt_orario_agenda))	then
		ll_riga = dw_allocazione_giornata.insertrow(0)
		dw_allocazione_giornata.object.orario[ll_riga] = ldt_ora_step
		dw_allocazione_giornata.object.data_giorno[ll_riga] = idt_data_giorno
		dw_allocazione_giornata.setitem(ll_riga,"ripetitivo","N")
		ldt_ora_step = datetime(date(s_cs_xx.db_funzioni.data_neutra), &
							relativetime(time(ldt_ora_step), hour(time(idt_intervallo))*3600 + minute(time(idt_intervallo))*60 ))
	end if
	if (time(ldt_ora_agenda[ll_cont]) = time(ldt_orario_agenda)) then
		if (time(ldt_ora_step) <> time(ldt_orario_agenda))	then
			ll_riga = dw_allocazione_giornata.insertrow(0)
			dw_allocazione_giornata.object.orario[ll_riga] = ldt_orario_agenda
			dw_allocazione_giornata.object.data_giorno[ll_riga] = idt_data_giorno
			dw_allocazione_giornata.setitem(ll_riga,"ripetitivo","N")
			dw_allocazione_giornata.setitem(ll_riga,"flag_fatto",ls_flag_fatto[ll_cont])
		end if
		if isnull(dw_allocazione_giornata.object.des_impegno[ll_riga]) then
			dw_allocazione_giornata.setitem(ll_riga,"des_impegno",ls_note_agenda[ll_cont])
		else
			ls_str = ls_note_agenda[ll_cont] + char(13) + char(10) +  dw_allocazione_giornata.getitemstring(ll_riga,"des_impegno")
			dw_allocazione_giornata.setitem(ll_riga,"des_impegno",ls_str)
		end if				
		ll_cont = ll_cont + 1
	end if
	if ll_cont_1 <= upperbound(ldt_ora_impegno) then
		if time(ldt_ora_impegno[ll_cont_1]) = time(ldt_orario_agenda) then
			if (time(ldt_ora_step) <> time(ldt_orario_agenda))	then
				ll_riga = dw_allocazione_giornata.insertrow(0)
				dw_allocazione_giornata.object.orario[ll_riga] = ldt_orario_agenda
				dw_allocazione_giornata.object.data_giorno[ll_riga] = idt_data_giorno
				dw_allocazione_giornata.setitem(ll_riga,"ripetitivo","N")
			end if
			if isnull(dw_allocazione_giornata.object.des_impegno[ll_riga]) then
				dw_allocazione_giornata.setitem(ll_riga,"des_impegno",ls_nota_impegno[ll_cont_1])
				dw_allocazione_giornata.setitem(ll_riga,"ripetitivo","S")
				dw_allocazione_giornata.setitem(ll_riga,"data_origine_impegno",ldt_data_impegno[ll_cont_1])
			else
				if dw_allocazione_giornata.getitemstring(ll_riga,"des_impegno") <> ls_nota_impegno[ll_cont_1] then
					ls_str = ls_nota_impegno[ll_cont_1] + char(13) + char(10) +  dw_allocazione_giornata.getitemstring(ll_riga,"des_impegno")
				else
					ls_str = ls_nota_impegno[ll_cont_1]
				end if	
				dw_allocazione_giornata.setitem(ll_riga,"des_impegno",ls_str)
				dw_allocazione_giornata.setitem(ll_riga,"ripetitivo","S")
				dw_allocazione_giornata.setitem(ll_riga,"data_origine_impegno",ldt_data_impegno[ll_cont_1])
			end if
			ll_cont_1 = ll_cont_1 + 1
		end if
	end if
	ldt_orario_agenda = datetime(date(s_cs_xx.db_funzioni.data_neutra), &
									          relativetime(time(ldt_orario_agenda), 60 ))
loop until time(idt_ora_fine) <= time(ldt_ora_step)

dw_allocazione_giornata.setredraw(true)
return 0

end function

public function integer wf_carica_ddlb ();string ls_cod_tipo_agenda, ls_des_tipo_agenda, ls_sql
long ll_num

declare cu_ddlb dynamic cursor for sqlsa;

ls_sql = "SELECT tab_tipi_agende.cod_tipo_agenda, " + &
  			       "tab_tipi_agende.des_tipo_agenda  " + &
         " FROM tab_tipi_agende " + &  
         " WHERE tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

prepare sqlsa from :ls_sql;

open dynamic cu_ddlb;

ll_num = 0
ddlb_1.clear()
do while 1=1
   fetch cu_ddlb into :ls_cod_tipo_agenda, :ls_des_tipo_agenda;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ddlb_1.additem(ls_des_tipo_agenda)
   ll_num = ll_num + 1
loop
close cu_ddlb;

return ll_num
end function

public function integer wf_crea_datastore ();ids_impegni_giornalieri = create datastore
ids_impegni_settimanali = create datastore
ids_impegni_mensili = create datastore
ids_impegni_giornalieri.DataObject = "d_ricerca_impegni_giornalieri"
ids_impegni_settimanali.DataObject = "d_ricerca_impegni_settimanali"
ids_impegni_mensili.DataObject = "d_ricerca_impegni_mensili"
ids_impegni_giornalieri.settransobject(sqlca)
ids_impegni_settimanali.settransobject(sqlca)
ids_impegni_mensili.settransobject(sqlca)
ids_impegni_giornalieri.retrieve(s_cs_xx.cod_azienda, &
                               is_cod_tipo_agenda, &
 										 s_cs_xx.cod_utente)
ids_impegni_settimanali.retrieve(s_cs_xx.cod_azienda, &
                               is_cod_tipo_agenda, &
 										 s_cs_xx.cod_utente)
ids_impegni_mensili.retrieve(s_cs_xx.cod_azienda, &
                               is_cod_tipo_agenda, &
 										 s_cs_xx.cod_utente)
return 0
end function

public function integer wf_ricerca_impegni (datetime wdt_data, ref datetime wdt_ora_impegno[], ref string ws_des_impegno[], ref datetime wdt_data_impegno[]);long ll_i, ll_cont, ll_new_row
string ls_str
datastore lds_totale_impegni

ll_cont = 1
lds_totale_impegni = create datastore
lds_totale_impegni.DataObject = "d_impegni_periodici_giorno"

// ------------  inizio con il caricare gli impegni giornalieri  -----------------------

if ids_impegni_giornalieri.rowcount() > 0 then
 	for ll_i = 1 to ids_impegni_giornalieri.rowcount()
		if ids_impegni_giornalieri.object.data_scad_periodico[ll_i] >= wdt_data then
			if lds_totale_impegni.find("rd_ora_giorno = " + string(ids_impegni_giornalieri.object.ora_agenda[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ) > 0 then
				ls_str = lds_totale_impegni.getitemstring(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno")
				ls_str = ls_str + char(13) + char(10) + ids_impegni_giornalieri.object.note[ll_i]
				lds_totale_impegni.setitem(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno", ls_str)
			else
				ll_new_row = lds_totale_impegni.insertrow(0)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_giorno",wdt_data)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_origine",ids_impegni_giornalieri.object.data_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rt_ora_giorno",ids_impegni_giornalieri.object.ora_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rs_nota_impegno",ids_impegni_giornalieri.object.note[ll_i])
			end if
		end if
	next 
end if

// ------------------------- carico gli impegni settimanali  ----------------------------

if ids_impegni_settimanali.rowcount() > 0 then
	for ll_i = 1 to ids_impegni_settimanali.rowcount()
		if dayname(date(wdt_data)) = dayname(date(ids_impegni_settimanali.object.data_agenda[ll_i])) and &
			ids_impegni_settimanali.object.data_scad_periodico[ll_i] >= wdt_data then
			if lds_totale_impegni.find("rd_ora_giorno = " + string(ids_impegni_settimanali.object.ora_agenda[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ) > 0 then
				ls_str = lds_totale_impegni.getitemstring(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno")
				ls_str = ls_str + char(13) + char(10) + ids_impegni_settimanali.object.note[ll_i]
				lds_totale_impegni.setitem(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno", ls_str)
			else
				ll_new_row = lds_totale_impegni.insertrow(0)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_giorno",wdt_data)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_origine",ids_impegni_settimanali.object.data_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rt_ora_giorno",ids_impegni_settimanali.object.ora_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rs_nota_impegno",ids_impegni_settimanali.object.note[ll_i])
			end if
		end if
   next 
end if

// ------------------------- carico gli impegni mensili  ---------------------------------

if ids_impegni_mensili.rowcount() > 0 then
	for ll_i = 1 to ids_impegni_mensili.rowcount()
		if day(date(wdt_data)) = day(ids_impegni_mensili.object.data_agenda[ll_i]) and &
			ids_impegni_mensili.object.data_scad_periodico[ll_i] >= wdt_data then
			if lds_totale_impegni.find("rd_ora_giorno = " + string(ids_impegni_mensili.object.ora_agenda[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ) > 0 then
				ls_str = lds_totale_impegni.getitemstring(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno")
				ls_str = ls_str + char(13) + char(10) + ids_impegni_mensili.object.note[ll_i]
				lds_totale_impegni.setitem(lds_totale_impegni.find("rd_ora_giorno = " + string(wdt_ora_impegno[ll_i], "hh:mm"),1 ,lds_totale_impegni.rowcount() ),"rs_nota_impegno", ls_str)
			else
				ll_new_row = lds_totale_impegni.insertrow(0)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_giorno",wdt_data)
				lds_totale_impegni.setitem(ll_new_row,"rd_data_origine",ids_impegni_mensili.object.data_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rt_ora_giorno",ids_impegni_mensili.object.ora_agenda[ll_i])
				lds_totale_impegni.setitem(ll_new_row,"rs_nota_impegno",ids_impegni_mensili.object.note[ll_i])
			end if
		end if
   next 
end if

// ------  eseguo unione degli impegni in una unica lista ordinata per orario ---------------

for ll_i = 1 to lds_totale_impegni.rowcount()
	wdt_data_impegno[ll_i] = lds_totale_impegni.object.rd_data_origine[ll_i]
	wdt_ora_impegno[ll_i] = lds_totale_impegni.object.rt_ora_giorno[ll_i]
	ws_des_impegno[ll_i] = lds_totale_impegni.object.rs_nota_impegno[ll_i]
next

return 0

end function

on w_agenda.create
int iCurrent
call w_cs_xx_principale::create
this.ddlb_1=create ddlb_1
this.st_1=create st_1
this.pb_3=create pb_3
this.pb_4=create pb_4
this.pb_1=create pb_1
this.pb_2=create pb_2
this.dw_allocazione_giornata=create dw_allocazione_giornata
this.st_informazioni=create st_informazioni
this.em_data=create em_data
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=ddlb_1
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=pb_3
this.Control[iCurrent+4]=pb_4
this.Control[iCurrent+5]=pb_1
this.Control[iCurrent+6]=pb_2
this.Control[iCurrent+7]=dw_allocazione_giornata
this.Control[iCurrent+8]=st_informazioni
this.Control[iCurrent+9]=em_data
this.Control[iCurrent+10]=st_2
end on

on w_agenda.destroy
call w_cs_xx_principale::destroy
destroy(this.ddlb_1)
destroy(this.st_1)
destroy(this.pb_3)
destroy(this.pb_4)
destroy(this.pb_1)
destroy(this.pb_2)
destroy(this.dw_allocazione_giornata)
destroy(this.st_informazioni)
destroy(this.em_data)
destroy(this.st_2)
end on

event open;call super::open;string ls_des_tipo_agenda

wf_carica_ddlb()

pb_1.enabled = false
pb_2.enabled = false
pb_3.enabled = false
pb_4.enabled = false

choose case s_cs_xx.agende_rubriche.tipo_operazione
	case 3
		select des_tipo_agenda
		into   :ls_des_tipo_agenda
		from   tab_tipi_agende
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_agenda = :s_cs_xx.agende_rubriche.tipo_agenda;
		if sqlca.sqlcode = 0 then
			ddlb_1.selectitem(ls_des_tipo_agenda, 1)
			ddlb_1.postevent("selectionchanged")
		end if
	case 4
		select des_tipo_agenda
		into   :ls_des_tipo_agenda
		from   tab_tipi_agende
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_agenda = :s_cs_xx.agende_rubriche.tipo_agenda;
		if sqlca.sqlcode = 0 then
			ddlb_1.selectitem(ls_des_tipo_agenda, 1)
			ddlb_1.postevent("selectionchanged")
		end if
end choose
end event

event activate;call super::activate;if s_cs_xx.agende_rubriche.refresh then
	wf_crea_pagina_agenda()
	s_cs_xx.agende_rubriche.refresh = false
end if
end event

type ddlb_1 from dropdownlistbox within w_agenda
int X=389
int Y=21
int Width=938
int Height=481
int TabOrder=10
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_str

ls_str = this.text

select cod_tipo_agenda, ora_inizio, ora_fine, ora_intervallo
into   :is_cod_tipo_agenda, :idt_ora_inizio, :idt_ora_fine, :idt_intervallo
from   tab_tipi_agende
where  des_tipo_agenda = :ls_str;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("AGENDA", "E' stata selezionata una tipologia di agenda non valida", Stopsign!)
	return
end if
idt_data_giorno = datetime(today())
wf_crea_datastore()
wf_crea_pagina_agenda()
em_data.text = string(idt_data_giorno,"dd/mm/yyyy")
pb_1.enabled = true
pb_2.enabled = true
pb_3.enabled = true
pb_4.enabled = true

end event

type st_1 from statictext within w_agenda
int X=23
int Y=41
int Width=343
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Agenda:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type pb_3 from picturebutton within w_agenda
int X=69
int Y=281
int Width=101
int Height=85
int TabOrder=20
string PictureName="\cs_xx\risorse\prim.bmp"
Alignment HTextAlign=Left!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;idt_data_giorno = datetime(relativedate(date(idt_data_giorno), -7))
wf_crea_pagina_agenda()
end event

type pb_4 from picturebutton within w_agenda
int X=1669
int Y=281
int Width=101
int Height=85
int TabOrder=40
string PictureName="\cs_xx\risorse\ult.bmp"
Alignment HTextAlign=Left!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;idt_data_giorno = datetime(relativedate(date(idt_data_giorno), +7))
wf_crea_pagina_agenda()
end event

type pb_1 from picturebutton within w_agenda
int X=69
int Y=181
int Width=101
int Height=85
int TabOrder=60
boolean BringToTop=true
string PictureName="\cs_xx\risorse\prec.bmp"
Alignment HTextAlign=Left!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;idt_data_giorno = datetime(relativedate(date(idt_data_giorno), -1))
wf_crea_pagina_agenda()
end event

type pb_2 from picturebutton within w_agenda
int X=1669
int Y=181
int Width=101
int Height=85
int TabOrder=50
boolean BringToTop=true
string PictureName="\cs_xx\risorse\suc.bmp"
Alignment HTextAlign=Left!
boolean OriginalSize=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;idt_data_giorno = datetime(relativedate(date(idt_data_giorno), +1))
wf_crea_pagina_agenda()
end event

type dw_allocazione_giornata from datawindow within w_agenda
int X=23
int Y=121
int Width=1898
int Height=1301
int TabOrder=30
string DataObject="d_allocazione_giornata"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event doubleclicked;if row > 0 then
   s_cs_xx.parametri.parametro_s_10 = is_cod_tipo_agenda
	s_cs_xx.parametri.parametro_data_2 = idt_data_giorno
	s_cs_xx.parametri.parametro_t_1 = this.object.orario[row]
	s_cs_xx.parametri.parametro_s_11 = this.object.ripetitivo[row]
	s_cs_xx.parametri.parametro_s_11 = this.object.ripetitivo[row]
	s_cs_xx.parametri.parametro_data_3 = this.object.data_origine_impegno[row]
	s_cs_xx.parametri.parametro_b_2 = true
	window_open(w_agenda_impegno, -1)
end if
end event

event clicked;if row > 0 then
   if this.object.ripetitivo[row] = "S" then
		st_informazioni.text = "Appuntamento periodico creato il "+string(this.object.data_origine_impegno[row],"dd/mm/yyyy")
	else
		st_informazioni.text = ""
	end if
end if
end event

type st_informazioni from statictext within w_agenda
int X=23
int Y=1441
int Width=1898
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_data from editmask within w_agenda
int X=1555
int Y=21
int Width=366
int Height=81
int TabOrder=11
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=" "
string MinMax="01/01/1900~~31/12/2999"
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;idt_data_giorno = datetime(date(em_data.text))
wf_crea_pagina_agenda()
end event

type st_2 from statictext within w_agenda
int X=1372
int Y=21
int Width=183
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Vai al:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

