﻿$PBExportHeader$w_agende_corrispondenze.srw
$PBExportComments$Finestra Agende Corrispondenze
forward
global type w_agende_corrispondenze from w_cs_xx_principale
end type
type dw_agende_corrispondenze_lista from uo_cs_xx_dw within w_agende_corrispondenze
end type
type dw_agende_corrispondenze_det from uo_cs_xx_dw within w_agende_corrispondenze
end type
type cb_controllo from commandbutton within w_agende_corrispondenze
end type
type cb_note_esterne from commandbutton within w_agende_corrispondenze
end type
end forward

global type w_agende_corrispondenze from w_cs_xx_principale
integer width = 3173
integer height = 1680
string title = "Corrispondenze"
dw_agende_corrispondenze_lista dw_agende_corrispondenze_lista
dw_agende_corrispondenze_det dw_agende_corrispondenze_det
cb_controllo cb_controllo
cb_note_esterne cb_note_esterne
end type
global w_agende_corrispondenze w_agende_corrispondenze

event pc_setwindow;call super::pc_setwindow;dw_agende_corrispondenze_lista.set_dw_key("cod_azienda")
dw_agende_corrispondenze_lista.set_dw_key("cod_tipo_agenda")
dw_agende_corrispondenze_lista.set_dw_key("cod_utente")
dw_agende_corrispondenze_lista.set_dw_key("data_agenda")
dw_agende_corrispondenze_lista.set_dw_key("ora_agenda")
dw_agende_corrispondenze_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_agende_corrispondenze_det.set_dw_options(sqlca,dw_agende_corrispondenze_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_agende_corrispondenze_lista
end event

on w_agende_corrispondenze.create
int iCurrent
call super::create
this.dw_agende_corrispondenze_lista=create dw_agende_corrispondenze_lista
this.dw_agende_corrispondenze_det=create dw_agende_corrispondenze_det
this.cb_controllo=create cb_controllo
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_agende_corrispondenze_lista
this.Control[iCurrent+2]=this.dw_agende_corrispondenze_det
this.Control[iCurrent+3]=this.cb_controllo
this.Control[iCurrent+4]=this.cb_note_esterne
end on

on w_agende_corrispondenze.destroy
call super::destroy
destroy(this.dw_agende_corrispondenze_lista)
destroy(this.dw_agende_corrispondenze_det)
destroy(this.cb_controllo)
destroy(this.cb_note_esterne)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_agende_corrispondenze_lista, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_agende_corrispondenze_det, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_PO_LoadDDDW_DW(dw_agende_corrispondenze_det,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )


end event

type dw_agende_corrispondenze_lista from uo_cs_xx_dw within w_agende_corrispondenze
integer x = 23
integer y = 20
integer width = 2697
integer height = 500
integer taborder = 20
string dataobject = "d_agende_corrispondenze_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog_corrispondenza
string ls_cod_corrispondenza, ls_cod_tipo_agenda, ls_cod_utente
datetime ldt_data_agenda, ldt_ora_agenda

ls_cod_tipo_agenda = i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]]
ls_cod_utente = i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]]
ldt_data_agenda = i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]]
ldt_ora_agenda = i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]]

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_agenda")) then
      this.setitem(ll_i, "cod_tipo_agenda", ls_cod_tipo_agenda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_utente")) then
      this.setitem(ll_i, "cod_utente", ls_cod_utente)
   end if
   if isnull(this.getitemdatetime(ll_i, "data_agenda")) then
      this.setitem(ll_i, "data_agenda", ldt_data_agenda)
   end if
   if isnull(this.getitemdatetime(ll_i, "ora_agenda")) then
      this.setitem(ll_i, "ora_agenda", ldt_ora_agenda)
   end if
   if isnull(this.getitemnumber(ll_i, "prog_corrispondenza")) or &
      this.getitemnumber(ll_i, "prog_corrispondenza") = 0 then

      ls_cod_corrispondenza = this.getitemstring(ll_i,"cod_corrispondenza")

      select max(agende_corrispondenze.prog_corrispondenza)
      into   :ll_prog_corrispondenza
      from   agende_corrispondenze
      where  agende_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
				 agende_corrispondenze.cod_tipo_agenda = :ls_cod_tipo_agenda and
				 agende_corrispondenze.cod_utente = :ls_cod_utente and
				 agende_corrispondenze.data_agenda = :ldt_data_agenda and
				 agende_corrispondenze.ora_agenda = :ldt_ora_agenda and
				 agende_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza;

      if not isnull(ll_prog_corrispondenza) then
         this.setitem(ll_i, "prog_corrispondenza", ll_prog_corrispondenza + 1)
      else
         this.setitem(ll_i, "prog_corrispondenza", 1)
      end if
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]], &
                   i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]])

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_new;call super::pcd_new;cb_controllo.enabled = false
cb_note_esterne.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_controllo.enabled = false
cb_note_esterne.enabled = false
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
      cb_controllo.enabled = true
   else
      cb_controllo.enabled = false
   end if
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

event updateend;call super::updateend;integer li_i
long ll_num_reg_lista, ll_prog_liste_con_comp

for li_i = 1 to this.deletedcount()
   ll_num_reg_lista = this.getitemnumber(li_i, "num_reg_lista_comp", delete!, true)
   ll_prog_liste_con_comp = this.getitemnumber(li_i, "prog_liste_con_comp", delete!, true)
   f_cancella_liste_con_comp(ll_num_reg_lista, ll_prog_liste_con_comp)
next

end event

type dw_agende_corrispondenze_det from uo_cs_xx_dw within w_agende_corrispondenze
integer x = 23
integer y = 540
integer width = 3086
integer height = 1020
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_agende_corrispondenze_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   integer li_null, li_num_reg_lista, li_num_versione, li_num_edizione
   setnull(li_null)


   choose case i_colname
      case "cod_corrispondenza"
         this.setitem(i_rownbr, "num_reg_lista", li_null)

         select tab_corrispondenze.num_reg_lista
         into   :li_num_reg_lista
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext; 

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "num_reg_lista", li_num_reg_lista)
            select   tes_liste_controllo.num_versione,
                     tes_liste_controllo.num_edizione
            into     :li_num_versione, 
                     :li_num_edizione
            from     tes_liste_controllo
            where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                     tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                     tes_liste_controllo.flag_valido = 'S';

            this.setitem(this.getrow(), "num_versione", li_num_versione)
            this.setitem(this.getrow(), "num_edizione", li_num_edizione)
         end if

      case "num_reg_lista"
         li_num_reg_lista = integer(i_coltext)
         select   tes_liste_controllo.num_versione,
                  tes_liste_controllo.num_edizione
         into     :li_num_versione, 
                  :li_num_edizione
         from     tes_liste_controllo
         where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                  tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                  tes_liste_controllo.flag_valido = 'S';

         this.setitem(this.getrow(), "num_versione", li_num_versione)
         this.setitem(this.getrow(), "num_edizione", li_num_edizione)
   end choose
end if
end event

type cb_controllo from commandbutton within w_agende_corrispondenze
integer x = 2743
integer y = 120
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Controllo"
end type

event clicked;string ls_cod_corrispondenza, ls_cod_tipo_agenda, ls_cod_utente

long   ll_prog_corrispondenza, ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista_comp, ll_prog_riga
datetime ldt_data_agenda, ldt_ora_agenda
   
ll_i[1] = dw_agende_corrispondenze_lista.getrow()
ll_num_reg_lista_comp = dw_agende_corrispondenze_lista.object.num_reg_lista_comp[ll_i[1]]

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ls_cod_tipo_agenda = dw_agende_corrispondenze_lista.object.cod_tipo_agenda[ll_i[1]]
   ls_cod_utente = dw_agende_corrispondenze_lista.object.cod_utente[ll_i[1]]
   ldt_data_agenda = dw_agende_corrispondenze_lista.object.data_agenda[ll_i[1]]
   ldt_ora_agenda = dw_agende_corrispondenze_lista.object.ora_agenda[ll_i[1]]
   ls_cod_corrispondenza = dw_agende_corrispondenze_lista.object.cod_corrispondenza[ll_i[1]]
   ll_prog_corrispondenza = dw_agende_corrispondenze_lista.object.prog_corrispondenza[ll_i[1]]
   ll_num_reg_lista_comp = dw_agende_corrispondenze_lista.object.num_reg_lista[ll_i[1]]
   
   update agende_corrispondenze
   set    agende_corrispondenze.num_versione = :ll_num_versione,
          agende_corrispondenze.num_edizione = :ll_num_edizione,
          agende_corrispondenze.num_reg_lista_comp = :ll_num_reg_lista_comp,
          agende_corrispondenze.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  agende_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
          agende_corrispondenze.cod_tipo_agenda = :ls_cod_tipo_agenda and 
          agende_corrispondenze.cod_utente = :ls_cod_utente and 
          agende_corrispondenze.data_agenda = :ldt_data_agenda and 
          agende_corrispondenze.ora_agenda = :ldt_ora_agenda and
          agende_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and 
          agende_corrispondenze.prog_corrispondenza = :ll_prog_corrispondenza;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_agende_corrispondenze_lista.triggerevent("pcd_retrieve")
   dw_agende_corrispondenze_lista.set_selected_rows(1, &
                                                           ll_i[], &
                                                           c_ignorechanges, &
                                                           c_refreshchildren, &
                                                           c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_agende_corrispondenze_lista)

end event

type cb_note_esterne from commandbutton within w_agende_corrispondenze
integer x = 2743
integer y = 20
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;string ls_cod_corrispondenza, ls_cod_tipo_agenda, ls_cod_utente, ls_db
integer li_i, li_prog_corrispondenza, li_risposta
datetime ldt_data_agenda, ldt_ora_agenda

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_agende_corrispondenze_lista.getrow()
ls_cod_tipo_agenda = dw_agende_corrispondenze_lista.object.cod_tipo_agenda[li_i]
ls_cod_utente = dw_agende_corrispondenze_lista.object.cod_utente[li_i]
ldt_data_agenda = dw_agende_corrispondenze_lista.object.data_agenda[li_i]
ldt_ora_agenda = dw_agende_corrispondenze_lista.object.ora_agenda[li_i]
ls_cod_corrispondenza = dw_agende_corrispondenze_lista.object.cod_corrispondenza[li_i]
li_prog_corrispondenza = dw_agende_corrispondenze_lista.object.prog_corrispondenza[li_i]

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob agende_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       agende_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_tipo_agenda = :ls_cod_tipo_agenda and
	           od_utente = :ls_cod_utente and
	           data_agenda = :ldt_data_agenda and
	           ora_agenda = :ldt_ora_agenda and
	           cod_corrispondenza = :ls_cod_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza
	using      sqlcb;
				  
   if sqlcb.sqlcode <> 0 and sqlcb.sqlcode <> 100 then
      g_mb.messagebox("AGENDA","Si è verificato un errore durante la lettura del documento esterno: verificare", Information!)
		destroy sqlcb;
		return
   end if

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob agende_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       agende_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_tipo_agenda = :ls_cod_tipo_agenda and
	           od_utente = :ls_cod_utente and
	           data_agenda = :ldt_data_agenda and
	           ora_agenda = :ldt_ora_agenda and
	           cod_corrispondenza = :ls_cod_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza;
				  
	   if sqlca.sqlcode <> 0 and sqlca.sqlcode <> 100 then
	      g_mb.messagebox("AGENDA","Si è verificato un errore durante la lettura del documento esterno: verificare", Information!)
			return
	   end if

		if sqlca.sqlcode <> 0 then
		   s_cs_xx.parametri.parametro_bl_1 = lbl_null
		end if
		
end if


window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob agende_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_tipo_agenda = :ls_cod_tipo_agenda and
					  cod_utente = :ls_cod_utente and
					  data_agenda = :ldt_data_agenda and
					  ora_agenda = :ldt_ora_agenda and
					  cod_corrispondenza = :ls_cod_corrispondenza and
					  prog_corrispondenza = :li_prog_corrispondenza
		using      sqlcb;
					  
	   if sqlcb.sqlcode <> 0 then
	      g_mb.messagebox("AGENDA","Si è verificato un errore durante la memorizzazione del documento esterno: verificare", Information!)
	   end if
		
		destroy sqlcb;
		
	else
		
	   updateblob agende_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  cod_tipo_agenda = :ls_cod_tipo_agenda and
					  cod_utente = :ls_cod_utente and
					  data_agenda = :ldt_data_agenda and
					  ora_agenda = :ldt_ora_agenda and
					  cod_corrispondenza = :ls_cod_corrispondenza and
					  prog_corrispondenza = :li_prog_corrispondenza;
					  
	   if sqlca.sqlcode <> 0 then
	      g_mb.messagebox("AGENDA","Si è verificato un errore durante la memorizzazione del documento esterno: verificare", Information!)
	   end if
		
	end if
	
   commit;
end if
end event

