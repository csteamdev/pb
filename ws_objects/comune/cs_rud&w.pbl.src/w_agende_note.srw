﻿$PBExportHeader$w_agende_note.srw
$PBExportComments$Finestra Agende Note
forward
global type w_agende_note from w_cs_xx_principale
end type
type dw_agende_note_lista from uo_cs_xx_dw within w_agende_note
end type
type dw_agende_note_det from uo_cs_xx_dw within w_agende_note
end type
end forward

global type w_agende_note from w_cs_xx_principale
int Width=2620
int Height=1625
boolean TitleBar=true
string Title="Note Impegno"
dw_agende_note_lista dw_agende_note_lista
dw_agende_note_det dw_agende_note_det
end type
global w_agende_note w_agende_note

event pc_setwindow;call super::pc_setwindow;dw_agende_note_lista.set_dw_key("cod_azienda")
dw_agende_note_lista.set_dw_key("cod_tipo_agenda")
dw_agende_note_lista.set_dw_key("cod_utente")
dw_agende_note_lista.set_dw_key("data_agenda")
dw_agende_note_lista.set_dw_key("ora_agenda")
dw_agende_note_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_agende_note_det.set_dw_options(sqlca,dw_agende_note_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_agende_note_lista
end event

on w_agende_note.create
int iCurrent
call w_cs_xx_principale::create
this.dw_agende_note_lista=create dw_agende_note_lista
this.dw_agende_note_det=create dw_agende_note_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_agende_note_lista
this.Control[iCurrent+2]=dw_agende_note_det
end on

on w_agende_note.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_agende_note_lista)
destroy(this.dw_agende_note_det)
end on

type dw_agende_note_lista from uo_cs_xx_dw within w_agende_note
int X=23
int Y=21
int Width=2538
int Height=501
string DataObject="d_agende_note_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_agenda")) THEN
      SetItem(l_Idx, "cod_tipo_agenda", i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_utente")) THEN
      SetItem(l_Idx, "cod_utente", i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "data_agenda")) or GetItemdatetime(l_Idx, "data_agenda") <= datetime(date("01/01/1900")) THEN
      SetItem(l_Idx, "data_agenda", i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "ora_agenda")) or GetItemdatetime(l_Idx, "ora_agenda") <= datetime(date(s_cs_xx.db_funzioni.data_neutra), 00:00:00) THEN
      SetItem(l_Idx, "ora_agenda", i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]], &
                   i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]])

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_agende_note_det from uo_cs_xx_dw within w_agende_note
int X=23
int Y=541
int Width=2538
int Height=961
boolean BringToTop=true
string DataObject="d_agende_note_det"
BorderStyle BorderStyle=StyleRaised!
end type

