﻿$PBExportHeader$w_agende_utenti.srw
$PBExportComments$Finestra Agente Utenti - Partecipanti Impegno Agenda
forward
global type w_agende_utenti from w_cs_xx_principale
end type
type dw_agende_utenti from uo_cs_xx_dw within w_agende_utenti
end type
end forward

global type w_agende_utenti from w_cs_xx_principale
int Width=883
int Height=1141
boolean TitleBar=true
string Title="Partecipanti"
dw_agende_utenti dw_agende_utenti
end type
global w_agende_utenti w_agende_utenti

event pc_setwindow;call super::pc_setwindow;dw_agende_utenti.set_dw_key("cod_azienda")
dw_agende_utenti.set_dw_key("cod_tipo_agenda")
dw_agende_utenti.set_dw_key("cod_utente")
dw_agende_utenti.set_dw_key("data_agenda")
dw_agende_utenti.set_dw_key("ora_agenda")
dw_agende_utenti.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)

iuo_dw_main = dw_agende_utenti
dw_agende_utenti.ib_proteggi_chiavi = false
end event

on w_agende_utenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_agende_utenti=create dw_agende_utenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_agende_utenti
end on

on w_agende_utenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_agende_utenti)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDLB_DW(dw_agende_utenti,"cod_partecipante",sqlca,&
                 "utenti","cod_utente","cod_utente", &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_agende_utenti from uo_cs_xx_dw within w_agende_utenti
int X=23
int Y=21
int Width=801
int Height=1001
string DataObject="d_agende_utenti"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_agenda")) THEN
      SetItem(l_Idx, "cod_tipo_agenda", i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_utente")) THEN
      SetItem(l_Idx, "cod_utente", i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "data_agenda")) or GetItemdatetime(l_Idx, "data_agenda") <= datetime(date("01/01/1900")) THEN
      SetItem(l_Idx, "data_agenda", i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "ora_agenda")) or GetItemdatetime(l_Idx, "ora_agenda") <= datetime(date(s_cs_xx.db_funzioni.data_neutra) ,00:00:00) THEN
      SetItem(l_Idx, "ora_agenda", i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]])
   END IF
NEXT


end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   i_parentdw.object.cod_tipo_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.cod_utente[i_parentdw.i_selectedrows[1]], &
                   i_parentdw.object.data_agenda[i_parentdw.i_selectedrows[1]], &
						 i_parentdw.object.ora_agenda[i_parentdw.i_selectedrows[1]])

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

