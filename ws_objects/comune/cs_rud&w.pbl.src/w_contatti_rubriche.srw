﻿$PBExportHeader$w_contatti_rubriche.srw
$PBExportComments$Finesta Collegamento Contatti Rubriche
forward
global type w_contatti_rubriche from w_cs_xx_principale
end type
type dw_contatti_rubriche from uo_cs_xx_dw within w_contatti_rubriche
end type
end forward

global type w_contatti_rubriche from w_cs_xx_principale
int Width=2437
int Height=641
boolean TitleBar=true
string Title="Contatti Rubriche"
dw_contatti_rubriche dw_contatti_rubriche
end type
global w_contatti_rubriche w_contatti_rubriche

event pc_setwindow;call super::pc_setwindow;dw_contatti_rubriche.set_dw_options(sqlca, &
                                    i_openparm, &
												c_nodelete+ &
												c_nomodify+ &
												c_disableCC, &
												c_default)
iuo_dw_main = dw_contatti_rubriche
end event

on w_contatti_rubriche.create
int iCurrent
call w_cs_xx_principale::create
this.dw_contatti_rubriche=create dw_contatti_rubriche
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_contatti_rubriche
end on

on w_contatti_rubriche.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_contatti_rubriche)
end on

type dw_contatti_rubriche from uo_cs_xx_dw within w_contatti_rubriche
int X=23
int Y=21
int Width=2355
int Height=501
string DataObject="d_contatti_rubriche"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_contatto

ls_cod_contatto = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_contatto")
l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 s_cs_xx.cod_utente,  &
						 ls_cod_contatto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
   s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_s_2 = this.object.cod_utente[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
	s_cs_xx.parametri.parametro_d_2 = 1
   s_cs_xx.parametri.parametro_s_4 = "INSERT_CONTATTI_RUBRICHE"
   s_cs_xx.parametri.parametro_s_6 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_contatto")
	if isnull(s_cs_xx.parametri.parametro_s_1) or &
	   isnull(s_cs_xx.parametri.parametro_s_2) or &
		isnull(s_cs_xx.parametri.parametro_d_1) or &
		s_cs_xx.parametri.parametro_d_1 = 0 then  return
	if isvalid(w_rubrica_alfa_dettaglio) then
		w_rubrica_alfa_dettaglio.triggerevent("pc_new")
	else
		window_open(w_rubrica_alfa_dettaglio, -1 )
	end if
end if
end event

event pcd_new;window_open(w_selezione_tipo_rubrica, 0)
s_cs_xx.parametri.parametro_s_2 = s_cs_xx.cod_utente
s_cs_xx.parametri.parametro_d_2 = 2
if isnull(s_cs_xx.parametri.parametro_s_1) then return
if isnull(s_cs_xx.parametri.parametro_s_2) then return
s_cs_xx.parametri.parametro_s_4 = "INSERT_CONTATTI_RUBRICHE"
s_cs_xx.parametri.parametro_s_6 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_contatto")
if isvalid(w_rubrica_alfa_dettaglio) then
	w_rubrica_alfa_dettaglio.triggerevent("pc_new")
else
	window_open(w_rubrica_alfa_dettaglio, -1 )
end if
end event

event clicked;call super::clicked;if row > 0 then
   s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_s_2 = this.object.cod_utente[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
	s_cs_xx.parametri.parametro_d_2 = 1
   s_cs_xx.parametri.parametro_s_4 = "INSERT_CONTATTI_RUBRICHE"
   s_cs_xx.parametri.parametro_s_6 = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1],"cod_contatto")
end if
end event

