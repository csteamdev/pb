﻿$PBExportHeader$w_dettaglio_impegno.srw
$PBExportComments$Finestra Dettaglio Impegno
forward
global type w_dettaglio_impegno from w_cs_xx_principale
end type
type dw_agenda_impegno from uo_cs_xx_dw within w_dettaglio_impegno
end type
type cb_documento from commandbutton within w_dettaglio_impegno
end type
type cb_corrispondenze from commandbutton within w_dettaglio_impegno
end type
type cb_partecipanti from commandbutton within w_dettaglio_impegno
end type
type cb_note from commandbutton within w_dettaglio_impegno
end type
type cb_annulla_periodicita from commandbutton within w_dettaglio_impegno
end type
end forward

global type w_dettaglio_impegno from w_cs_xx_principale
integer width = 2208
integer height = 1224
string title = "Impegno Agenda"
dw_agenda_impegno dw_agenda_impegno
cb_documento cb_documento
cb_corrispondenze cb_corrispondenze
cb_partecipanti cb_partecipanti
cb_note cb_note
cb_annulla_periodicita cb_annulla_periodicita
end type
global w_dettaglio_impegno w_dettaglio_impegno

type variables
boolean ib_new=false
end variables

forward prototypes
public function integer wf_blocca_tipo_periodo (string ws_flag_periodico)
end prototypes

public function integer wf_blocca_tipo_periodo (string ws_flag_periodico);choose case ws_flag_periodico
	case "S"
  		dw_agenda_impegno.object.flag_tipo_periodo.background.mode = 0
  		dw_agenda_impegno.object.flag_tipo_periodo.background.color = rgb(255,255,255)
  		dw_agenda_impegno.object.flag_tipo_periodo.border = 5
   	dw_agenda_impegno.Object.flag_tipo_periodo.TabSequence = 40
  		dw_agenda_impegno.object.data_scad_periodico.background.mode = 0
  		dw_agenda_impegno.object.data_scad_periodico.background.color = rgb(255,255,255)
  		dw_agenda_impegno.object.data_scad_periodico.border = 5
   	dw_agenda_impegno.Object.data_scad_periodico.TabSequence = 50
	case "N"
  		dw_agenda_impegno.object.flag_tipo_periodo.background.mode = 1
  		dw_agenda_impegno.object.flag_tipo_periodo.border = 6
   	dw_agenda_impegno.Object.flag_tipo_periodo.TabSequence = 0
  		dw_agenda_impegno.object.data_scad_periodico.background.mode = 1
  		dw_agenda_impegno.object.data_scad_periodico.border = 6
   	dw_agenda_impegno.Object.data_scad_periodico.TabSequence = 0
end choose

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_agenda_impegno.set_dw_key("cod_azienda")
dw_agenda_impegno.set_dw_key("cod_tipo_agenda")
dw_agenda_impegno.set_dw_key("cod_utente")
dw_agenda_impegno.set_dw_key("data_agenda")
dw_agenda_impegno.set_dw_key("ora_agenda")
dw_agenda_impegno.set_dw_options(sqlca,i_openparm,c_nonew,c_default)

iuo_dw_main = dw_agenda_impegno
if s_cs_xx.parametri.parametro_s_11 = "S" then
   cb_annulla_periodicita.enabled = true
else
   cb_annulla_periodicita.enabled = false
end if
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_agenda_impegno,"cod_tipo_agenda",sqlca,&
                 "tab_tipi_agende","cod_tipo_agenda","des_tipo_agenda", &
                 "tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDLB_DW(dw_agenda_impegno,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente", "")
					  
end event

on w_dettaglio_impegno.create
int iCurrent
call super::create
this.dw_agenda_impegno=create dw_agenda_impegno
this.cb_documento=create cb_documento
this.cb_corrispondenze=create cb_corrispondenze
this.cb_partecipanti=create cb_partecipanti
this.cb_note=create cb_note
this.cb_annulla_periodicita=create cb_annulla_periodicita
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_agenda_impegno
this.Control[iCurrent+2]=this.cb_documento
this.Control[iCurrent+3]=this.cb_corrispondenze
this.Control[iCurrent+4]=this.cb_partecipanti
this.Control[iCurrent+5]=this.cb_note
this.Control[iCurrent+6]=this.cb_annulla_periodicita
end on

on w_dettaglio_impegno.destroy
call super::destroy
destroy(this.dw_agenda_impegno)
destroy(this.cb_documento)
destroy(this.cb_corrispondenze)
destroy(this.cb_partecipanti)
destroy(this.cb_note)
destroy(this.cb_annulla_periodicita)
end on

type dw_agenda_impegno from uo_cs_xx_dw within w_dettaglio_impegno
integer x = 23
integer y = 20
integer width = 2126
integer height = 980
integer taborder = 20
string dataobject = "d_agenda_impegno"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   s_cs_xx.agende_rubriche.tipo_agenda, &
						 s_cs_xx.agende_rubriche.utente, &
                   s_cs_xx.agende_rubriche.data_agenda, &
						 s_cs_xx.agende_rubriche.ora_agenda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_agenda")) THEN
      SetItem(l_Idx, "cod_tipo_agenda", s_cs_xx.parametri.parametro_s_10)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_utente")) THEN
      SetItem(l_Idx, "cod_utente", s_cs_xx.cod_utente)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "data_agenda")) or GetItemdatetime(l_Idx, "data_agenda") <= datetime(date("01/01/1900")) THEN
      SetItem(l_Idx, "data_agenda", s_cs_xx.parametri.parametro_data_2)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemdatetime(l_Idx, "ora_agenda")) or GetItemdatetime(l_Idx, "ora_agenda") <= datetime(date(s_cs_xx.db_funzioni.data_neutra), 00:00:00) THEN
      SetItem(l_Idx, "ora_agenda", s_cs_xx.parametri.parametro_t_1)
   END IF
NEXT


end event

event pcd_new;call super::pcd_new;this.setitem(getrow(),"cod_tipo_agenda", s_cs_xx.parametri.parametro_s_10)
this.setitem(getrow(),"cod_utente", s_cs_xx.cod_utente)
this.setitem(getrow(),"data_agenda", s_cs_xx.parametri.parametro_data_2)
this.setitem(getrow(),"ora_agenda", s_cs_xx.parametri.parametro_t_1)

wf_blocca_tipo_periodo(getitemstring(getrow(),"flag_periodico"))
cb_annulla_periodicita.enabled = false
cb_corrispondenze.enabled = false
cb_documento.enabled = false
cb_note.enabled = false
cb_partecipanti.enabled = false
ib_new = true
end event

event itemchanged;call super::itemchanged;if i_extendmode and i_colname="flag_periodico" then
	wf_blocca_tipo_periodo(i_coltext)
end if
end event

event pcd_modify;call super::pcd_modify;wf_blocca_tipo_periodo(getitemstring(getrow(),"flag_periodico"))
cb_annulla_periodicita.enabled = false
cb_corrispondenze.enabled = false
cb_documento.enabled = false
cb_note.enabled = false
cb_partecipanti.enabled = false
end event

event pcd_view;call super::pcd_view;cb_corrispondenze.enabled = true
cb_documento.enabled = true
cb_note.enabled = true
cb_partecipanti.enabled = true
ib_new = false
if this.object.flag_periodico[getrow()] = "S" then
	cb_annulla_periodicita.enabled = true
end if

end event

event updateend;call super::updateend;datetime ldt_data_agenda, ldt_ora_agenda

if s_cs_xx.agende_rubriche.tipo_operazione = 3 and ib_new then
	ldt_data_agenda = this.object.data_agenda[getrow()]
	ldt_ora_agenda = this.object.ora_agenda[getrow()]
	INSERT INTO contatti_agende
				( cod_azienda,   
				  cod_contatto,   
				  cod_tipo_agenda,   
				  cod_utente,   
				  data_agenda,   
				  ora_agenda )  
	VALUES   ( :s_cs_xx.cod_azienda,   
				  :s_cs_xx.agende_rubriche.cod_contatto,   
				  :s_cs_xx.agende_rubriche.tipo_agenda,   
				  :s_cs_xx.agende_rubriche.utente,   
				  :ldt_data_agenda,   
				  :ldt_ora_agenda) ;
end if

end event

type cb_documento from commandbutton within w_dettaglio_impegno
integer x = 1029
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Documento"
end type

event clicked;string ls_cod_tipo_agenda, ls_cod_utente, ls_db
integer li_i, li_risposta
datetime ldt_data_agenda, ldt_ora_agenda

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_agenda_impegno.getrow()
ls_cod_tipo_agenda = dw_agenda_impegno.object.cod_tipo_agenda[li_i]
ls_cod_utente = dw_agenda_impegno.object.cod_utente[li_i]
ldt_data_agenda = dw_agenda_impegno.object.data_agenda[li_i]
ldt_ora_agenda = dw_agenda_impegno.object.ora_agenda[li_i]

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob agende.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       agende
	where      cod_azienda     = :s_cs_xx.cod_azienda and
	           cod_tipo_agenda = :ls_cod_tipo_agenda and
				  cod_utente      = :ls_cod_utente and
				  data_agenda     = :ldt_data_agenda and
				  ora_agenda      = :ldt_ora_agenda
	using sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob agende.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       agende
	where      cod_azienda     = :s_cs_xx.cod_azienda and
	           cod_tipo_agenda = :ls_cod_tipo_agenda and
				  cod_utente      = :ls_cod_utente and
				  data_agenda     = :ldt_data_agenda and
				  ora_agenda      = :ldt_ora_agenda;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob agende
	   set        note_esterne    = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda     = :s_cs_xx.cod_azienda and
					  cod_tipo_agenda = :ls_cod_tipo_agenda and
					  cod_utente      = :ls_cod_utente and
					  data_agenda     = :ldt_data_agenda and
					  ora_agenda      = :ldt_ora_agenda
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob agende
	   set        note_esterne    = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda     = :s_cs_xx.cod_azienda and
					  cod_tipo_agenda = :ls_cod_tipo_agenda and
					  cod_utente      = :ls_cod_utente and
					  data_agenda     = :ldt_data_agenda and
					  ora_agenda      = :ldt_ora_agenda;

	end if
	
   commit;
end if
end event

type cb_corrispondenze from commandbutton within w_dettaglio_impegno
integer x = 1417
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Corr."
end type

event clicked;window_open_parm(w_agende_corrispondenze, -1 ,dw_agenda_impegno)
end event

type cb_partecipanti from commandbutton within w_dettaglio_impegno
integer x = 1806
integer y = 1020
integer width = 343
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Partecipanti"
end type

event clicked;window_open_parm(w_agende_utenti, -1 ,dw_agenda_impegno)
end event

type cb_note from commandbutton within w_dettaglio_impegno
integer x = 640
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Note"
end type

event clicked;window_open_parm(w_agende_note, -1, dw_agenda_impegno)
end event

type cb_annulla_periodicita from commandbutton within w_dettaglio_impegno
event clicked pbm_bnclicked
integer x = 251
integer y = 1020
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stop Period."
end type

event clicked;update agende
set flag_periodico = 'N'
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_tipo_agenda = :s_cs_xx.parametri.parametro_s_10 and
		cod_utente = :s_cs_xx.cod_utente and
		data_agenda = :s_cs_xx.parametri.parametro_data_3 and
      ora_agenda = :s_cs_xx.parametri.parametro_t_1;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Agenda","Impegno di inizio periodicità non trovato", StopSign!)
else
	g_mb.messagebox("Agenda","Periodicità impegno eliminata", Information!)
end if
end event

