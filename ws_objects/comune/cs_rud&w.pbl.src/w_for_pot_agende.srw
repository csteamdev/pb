﻿$PBExportHeader$w_for_pot_agende.srw
$PBExportComments$Finestra Visualizzazione Impegni Creati dalla Scheda Fornitori Potenziali
forward
global type w_for_pot_agende from w_cs_xx_principale
end type
type dw_for_pot_agende from uo_cs_xx_dw within w_for_pot_agende
end type
type st_prossima_chiamata from statictext within w_for_pot_agende
end type
type em_prossima_chiamata from editmask within w_for_pot_agende
end type
end forward

global type w_for_pot_agende from w_cs_xx_principale
int Width=2798
int Height=745
boolean TitleBar=true
string Title="Agenda Fornitori Potenziali"
dw_for_pot_agende dw_for_pot_agende
st_prossima_chiamata st_prossima_chiamata
em_prossima_chiamata em_prossima_chiamata
end type
global w_for_pot_agende w_for_pot_agende

event pc_setwindow;call super::pc_setwindow;dw_for_pot_agende.set_dw_options(sqlca, &
                                    i_openparm, &
												c_nodelete+ &
												c_nomodify+ &
												c_disableCC, &
												c_default)
iuo_dw_main = dw_for_pot_agende
end event

on w_for_pot_agende.create
int iCurrent
call w_cs_xx_principale::create
this.dw_for_pot_agende=create dw_for_pot_agende
this.st_prossima_chiamata=create st_prossima_chiamata
this.em_prossima_chiamata=create em_prossima_chiamata
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_for_pot_agende
this.Control[iCurrent+2]=st_prossima_chiamata
this.Control[iCurrent+3]=em_prossima_chiamata
end on

on w_for_pot_agende.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_for_pot_agende)
destroy(this.st_prossima_chiamata)
destroy(this.em_prossima_chiamata)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDLB_DW(dw_for_pot_agende,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente", &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_for_pot_agende,"cod_tipo_agenda",sqlca,&
                 "tab_tipi_agende","cod_tipo_agenda","des_tipo_agenda", &
                 "tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_close;call super::pc_close;setnull(s_cs_xx.agende_rubriche.cod_contatto)
setnull(s_cs_xx.agende_rubriche.cod_for_pot)
setnull(s_cs_xx.agende_rubriche.tipo_agenda)
setnull(s_cs_xx.agende_rubriche.utente)
setnull(s_cs_xx.agende_rubriche.data_agenda)
setnull(s_cs_xx.agende_rubriche.ora_agenda)
s_cs_xx.agende_rubriche.tipo_operazione = 0


end event

type dw_for_pot_agende from uo_cs_xx_dw within w_for_pot_agende
int X=23
int Y=21
int Width=2721
int Height=501
string DataObject="d_for_pot_agende"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_contatto

ls_cod_contatto = i_parentdw.object.cod_for_pot[i_parentdw.i_selectedrows[1]]
l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 ls_cod_contatto)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if l_error > 0 then 	this.postevent("clicked")
end event

event doubleclicked;call super::doubleclicked;if row > 0 then
   s_cs_xx.agende_rubriche.cod_for_pot = i_parentdw.object.cod_for_pot[i_parentdw.i_selectedrows[1]]
	setnull(s_cs_xx.agende_rubriche.cod_contatto)
	s_cs_xx.agende_rubriche.tipo_operazione = 2
	s_cs_xx.agende_rubriche.tipo_agenda = this.object.cod_tipo_agenda[row]
	s_cs_xx.agende_rubriche.utente = this.object.cod_utente[row]
	s_cs_xx.agende_rubriche.data_agenda = this.object.data_agenda[row]
	s_cs_xx.agende_rubriche.ora_agenda = this.object.ora_agenda[row]
	if isnull(s_cs_xx.agende_rubriche.cod_for_pot) or &
	   isnull(s_cs_xx.agende_rubriche.tipo_agenda) or &
		isnull(s_cs_xx.agende_rubriche.utente) or &
		isnull(s_cs_xx.agende_rubriche.data_agenda) or (date(s_cs_xx.agende_rubriche.data_agenda) <= date("01/01/1900")) or &
		isnull(s_cs_xx.agende_rubriche.ora_agenda) or (time(s_cs_xx.agende_rubriche.ora_agenda) <= time("00:00")) then  return
		window_open_parm(w_dettaglio_impegno, -1, dw_for_pot_agende )
end if

end event

event pcd_new;window_open(w_selezione_tipo_agenda, 0)
s_cs_xx.agende_rubriche.utente = s_cs_xx.cod_utente
if isnull(s_cs_xx.agende_rubriche.tipo_agenda) then return
if isnull(s_cs_xx.agende_rubriche.utente) then return
s_cs_xx.agende_rubriche.tipo_operazione = 4
s_cs_xx.agende_rubriche.cod_for_pot = i_parentdw.object.cod_for_pot[i_parentdw.i_selectedrows[1]]
window_open(w_agenda, -1 )

end event

event clicked;call super::clicked;//if row > 0 then
//end if
boolean lb_uscita
string ls_cod_tipo_agenda, ls_cod_utente, ls_flag_periodico, ls_flag_tipo_periodo  
datetime ldt_data_scadenza_periodico, ldt_data_agenda, ldt_data, ldt_ora_agenda
integer li_mese

if row > 0 then
   s_cs_xx.agende_rubriche.cod_for_pot = i_parentdw.object.cod_for_pot[i_parentdw.i_selectedrows[1]]
	setnull(s_cs_xx.agende_rubriche.cod_contatto)
	s_cs_xx.agende_rubriche.tipo_operazione = 2
	s_cs_xx.agende_rubriche.tipo_agenda = this.object.cod_tipo_agenda[row]
	s_cs_xx.agende_rubriche.utente = this.object.cod_utente[row]
	s_cs_xx.agende_rubriche.data_agenda = this.object.data_agenda[row]
	s_cs_xx.agende_rubriche.ora_agenda = this.object.ora_agenda[row]

	ls_cod_tipo_agenda = this.object.cod_tipo_agenda[row]
	ls_cod_utente = this.object.cod_utente[row]
	ldt_data_agenda = this.object.data_agenda[row]
	ldt_ora_agenda = this.object.ora_agenda[row]
	
	SELECT flag_periodico,   
			 flag_tipo_periodo,   
			 data_scad_periodico  
	 INTO  :ls_flag_periodico,   
			 :ls_flag_tipo_periodo,   
			 :ldt_data_scadenza_periodico  
	 FROM  agende  
	WHERE  ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			 ( cod_tipo_agenda = :ls_cod_tipo_agenda ) AND  
			 ( cod_utente = :ls_cod_utente ) AND  
			 ( data_agenda = :ldt_data_agenda ) AND  
			 ( ora_agenda = :ldt_ora_agenda )   ;
	if sqlca.sqlcode = 0 and ls_flag_periodico = "S" then
		st_prossima_chiamata.visible = true
		em_prossima_chiamata.visible = true
		lb_uscita = false
		choose case ls_flag_tipo_periodo
		case "G"
			em_prossima_chiamata.text = string(today(),"dd/mm/yyyy")
		CASE "S"
			ldt_data = datetime(today())
			do
				if dayname(date(ldt_data)) = dayname(date(ldt_data_agenda)) then
					em_prossima_chiamata.text = string(ldt_data,"dd/mm/yyyy")
					lb_uscita = true
				else
					ldt_data = datetime(relativedate(date(ldt_data), 1))
				end if
			loop until lb_uscita
		CASE "E"
			if day(today()) < day(date(ldt_data_agenda)) then
				em_prossima_chiamata.text = string(day(date(ldt_data_agenda))) + "/" + &
				string( month(today()) )  + "/" + &
				string( year(today()) )
			else
				li_mese = month(today()) + 1
				if li_mese > 12 then li_mese = 1
				em_prossima_chiamata.text = string(day(date(ldt_data_agenda))) + "/" + &
													 string(li_mese) + "/" + &
													 string(year(today()))
			end if
		end choose
	else
		st_prossima_chiamata.visible = false
		em_prossima_chiamata.visible = false
	end if
end if

end event

type st_prossima_chiamata from statictext within w_for_pot_agende
int X=1669
int Y=541
int Width=673
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Data Prossima Chiamata:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_prossima_chiamata from editmask within w_for_pot_agende
int X=2355
int Y=541
int Width=389
int Height=81
int TabOrder=2
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
string DisplayData="Ä"
long BackColor=79741120
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

