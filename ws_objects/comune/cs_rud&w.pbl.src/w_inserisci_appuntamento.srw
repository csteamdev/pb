﻿$PBExportHeader$w_inserisci_appuntamento.srw
$PBExportComments$Finestra Input Dati per Appuntamento in Agenda
forward
global type w_inserisci_appuntamento from w_cs_xx_risposta
end type
type dw_inserisci_appuntamento from uo_cs_xx_dw within w_inserisci_appuntamento
end type
type cb_1 from commandbutton within w_inserisci_appuntamento
end type
type cb_2 from commandbutton within w_inserisci_appuntamento
end type
end forward

global type w_inserisci_appuntamento from w_cs_xx_risposta
int Width=1989
int Height=641
boolean TitleBar=true
string Title="DATI APPUNTAMENTO"
dw_inserisci_appuntamento dw_inserisci_appuntamento
cb_1 cb_1
cb_2 cb_2
end type
global w_inserisci_appuntamento w_inserisci_appuntamento

event pc_setwindow;call super::pc_setwindow;dw_inserisci_appuntamento.set_dw_options(sqlca, &
                                         pcca.null_object, &
													  c_disableCC,&
													  c_default)

dw_inserisci_appuntamento.postevent("pcd_new")

save_on_close(c_socnosave)
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDLB_DW(dw_inserisci_appuntamento,"cod_utente_1",sqlca,&
//                 "utenti","cod_utente","cod_utente", &
//					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > {fn curdate()}))")

f_PO_LoadDDLB_DW(dw_inserisci_appuntamento,"cod_utente_2",sqlca,&
                 "utenti","cod_utente","cod_utente", &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")

f_PO_LoadDDDW_DW(dw_inserisci_appuntamento,"cod_tipo_agenda",sqlca,&
                 "tab_tipi_agende","cod_tipo_agenda","des_tipo_agenda", &
                 "tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on w_inserisci_appuntamento.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_inserisci_appuntamento=create dw_inserisci_appuntamento
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_inserisci_appuntamento
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
end on

on w_inserisci_appuntamento.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_inserisci_appuntamento)
destroy(this.cb_1)
destroy(this.cb_2)
end on

type dw_inserisci_appuntamento from uo_cs_xx_dw within w_inserisci_appuntamento
int X=23
int Y=21
int Width=1898
int Height=401
int TabOrder=10
string DataObject="d_inserisci_appuntamento"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_str
	
	select stringa
	into   :ls_str
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_parametro = 'AGD';
	if sqlca.sqlcode = 0 then
		setitem(1, "cod_tipo_agenda", ls_str)
	end if
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		setitem(1, "cod_utente_1", s_cs_xx.cod_utente)
	end if
end if		
end event

type cb_1 from commandbutton within w_inserisci_appuntamento
int X=1555
int Y=441
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_tipo_agenda, ls_cod_utente_1, ls_cod_utente_2, ls_flag_utente_1

if dw_inserisci_appuntamento.getrow() < 1 then return

ls_cod_tipo_agenda = dw_inserisci_appuntamento.getitemstring(dw_inserisci_appuntamento.getrow(),"cod_tipo_agenda")
ls_cod_utente_1 = dw_inserisci_appuntamento.getitemstring(dw_inserisci_appuntamento.getrow(),"cod_utente_1")
ls_cod_utente_2 = dw_inserisci_appuntamento.getitemstring(dw_inserisci_appuntamento.getrow(),"cod_utente_2")
ls_flag_utente_1 = dw_inserisci_appuntamento.getitemstring(dw_inserisci_appuntamento.getrow(),"flag_utente")

if isnull(ls_cod_tipo_agenda) then return
if (isnull(ls_cod_utente_1) or ls_flag_utente_1 = "N") and isnull(ls_cod_utente_2) then return

s_cs_xx.parametri.parametro_s_1 = ls_cod_tipo_agenda
s_cs_xx.parametri.parametro_s_2 = ls_cod_utente_1
s_cs_xx.parametri.parametro_s_3 = ls_cod_utente_2
s_cs_xx.parametri.parametro_s_4 = ls_flag_utente_1

save_on_close(c_socnosave)
parent.postevent("pc_close")

end event

type cb_2 from commandbutton within w_inserisci_appuntamento
int X=1166
int Y=441
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;save_on_close(c_socnosave)
setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
setnull(s_cs_xx.parametri.parametro_s_3)
parent.postevent("pc_close")
end event

