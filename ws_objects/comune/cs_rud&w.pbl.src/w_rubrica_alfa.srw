﻿$PBExportHeader$w_rubrica_alfa.srw
$PBExportComments$Finestra Rubrica Alfabetica
forward
global type w_rubrica_alfa from Window
end type
type cb_nuovo from commandbutton within w_rubrica_alfa
end type
type cb_cancella from commandbutton within w_rubrica_alfa
end type
type cb_modifica from commandbutton within w_rubrica_alfa
end type
type st_utente from statictext within w_rubrica_alfa
end type
type st_4 from statictext within w_rubrica_alfa
end type
type st_3 from statictext within w_rubrica_alfa
end type
type ddlb_1 from dropdownlistbox within w_rubrica_alfa
end type
type p_notebook from picture within w_rubrica_alfa
end type
type dw_right from datawindow within w_rubrica_alfa
end type
type st_event from statictext within w_rubrica_alfa
end type
type st_2 from statictext within w_rubrica_alfa
end type
type st_current_tab from statictext within w_rubrica_alfa
end type
type st_1 from statictext within w_rubrica_alfa
end type
type dw_left from datawindow within w_rubrica_alfa
end type
type tab_1 from tab within w_rubrica_alfa
end type
type tabpage_a from userobject within tab_1
end type
type tabpage_b from userobject within tab_1
end type
type tabpage_c from userobject within tab_1
end type
type tabpage_d from userobject within tab_1
end type
type tabpage_e from userobject within tab_1
end type
type tabpage_f from userobject within tab_1
end type
type tabpage_g from userobject within tab_1
end type
type tabpage_h from userobject within tab_1
end type
type tabpage_i from userobject within tab_1
end type
type tabpage_j from userobject within tab_1
end type
type tabpage_k from userobject within tab_1
end type
type tabpage_l from userobject within tab_1
end type
type tabpage_m from userobject within tab_1
end type
type tabpage_n from userobject within tab_1
end type
type tabpage_o from userobject within tab_1
end type
type tabpage_p from userobject within tab_1
end type
type tabpage_q from userobject within tab_1
end type
type tabpage_r from userobject within tab_1
end type
type tabpage_s from userobject within tab_1
end type
type tabpage_t from userobject within tab_1
end type
type tabpage_u from userobject within tab_1
end type
type tabpage_v from userobject within tab_1
end type
type tabpage_w from userobject within tab_1
end type
type tabpage_x from userobject within tab_1
end type
type tabpage_y from userobject within tab_1
end type
type tabpage_z from userobject within tab_1
end type
type tabpage_a from userobject within tab_1
end type
type tabpage_b from userobject within tab_1
end type
type tabpage_c from userobject within tab_1
end type
type tabpage_d from userobject within tab_1
end type
type tabpage_e from userobject within tab_1
end type
type tabpage_f from userobject within tab_1
end type
type tabpage_g from userobject within tab_1
end type
type tabpage_h from userobject within tab_1
end type
type tabpage_i from userobject within tab_1
end type
type tabpage_j from userobject within tab_1
end type
type tabpage_k from userobject within tab_1
end type
type tabpage_l from userobject within tab_1
end type
type tabpage_m from userobject within tab_1
end type
type tabpage_n from userobject within tab_1
end type
type tabpage_o from userobject within tab_1
end type
type tabpage_p from userobject within tab_1
end type
type tabpage_q from userobject within tab_1
end type
type tabpage_r from userobject within tab_1
end type
type tabpage_s from userobject within tab_1
end type
type tabpage_t from userobject within tab_1
end type
type tabpage_u from userobject within tab_1
end type
type tabpage_v from userobject within tab_1
end type
type tabpage_w from userobject within tab_1
end type
type tabpage_x from userobject within tab_1
end type
type tabpage_y from userobject within tab_1
end type
type tabpage_z from userobject within tab_1
end type
type tab_1 from tab within w_rubrica_alfa
tabpage_a tabpage_a
tabpage_b tabpage_b
tabpage_c tabpage_c
tabpage_d tabpage_d
tabpage_e tabpage_e
tabpage_f tabpage_f
tabpage_g tabpage_g
tabpage_h tabpage_h
tabpage_i tabpage_i
tabpage_j tabpage_j
tabpage_k tabpage_k
tabpage_l tabpage_l
tabpage_m tabpage_m
tabpage_n tabpage_n
tabpage_o tabpage_o
tabpage_p tabpage_p
tabpage_q tabpage_q
tabpage_r tabpage_r
tabpage_s tabpage_s
tabpage_t tabpage_t
tabpage_u tabpage_u
tabpage_v tabpage_v
tabpage_w tabpage_w
tabpage_x tabpage_x
tabpage_y tabpage_y
tabpage_z tabpage_z
end type
end forward

global type w_rubrica_alfa from Window
int X=10
int Y=141
int Width=2812
int Height=1621
boolean TitleBar=true
string Title="Rubrica"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
ToolBarAlignment ToolBarAlignment=AlignAtLeft!
string Icon="People.ico"
event ue_retrieve_both pbm_custom01
cb_nuovo cb_nuovo
cb_cancella cb_cancella
cb_modifica cb_modifica
st_utente st_utente
st_4 st_4
st_3 st_3
ddlb_1 ddlb_1
p_notebook p_notebook
dw_right dw_right
st_event st_event
st_2 st_2
st_current_tab st_current_tab
st_1 st_1
dw_left dw_left
tab_1 tab_1
end type
global w_rubrica_alfa w_rubrica_alfa

type variables
string is_alfabeto="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
string is_cod_tipo_rubrica
end variables

forward prototypes
public subroutine wf_turn_page (readonly integer ai_right_page)
public function long wf_name_clicked (ref datawindow a_dw, readonly long a_row)
public function integer wf_carica_ddlb ()
end prototypes

event ue_retrieve_both;dw_left.Retrieve(s_cs_xx.cod_azienda, &
					  is_cod_tipo_rubrica, &
					  st_utente.text)

dw_right.Retrieve(s_cs_xx.cod_azienda, &
					  is_cod_tipo_rubrica, &
					  st_utente.text)
wf_turn_page (1)	// Start on the "A" page
tab_1.SelectTab (1)
end event

public subroutine wf_turn_page (readonly integer ai_right_page);// Subroutine wf_turn_page (ai_right_page)

// Selects (by filtering) proper "pages" for dw_left and dw_right

// Argument:  ai_right_page   Index (number) of letter to be displayed
//                            on the right-hand page

string	ls_last_initial, ls_previous_last_initial

// Get letter (that is, Last Initial) corresponding to this page
ls_last_initial = Mid(is_alfabeto, ai_right_page, 1)

// Fill the DW with names beginning with selected initial
//SetRedraw(FALSE)
dw_right.SetFilter('Left(cognome,1)="' + ls_last_initial + '"')
dw_right.Filter( )

// If not on "A", show left-hand DW
if ai_right_page > 1 then
	ls_previous_last_initial = Mid(is_alfabeto, ai_right_page -1, 1)
	dw_left.SetFilter('Left(cognome,1)="' + ls_previous_last_initial &
			+ '"')
	dw_left.Filter( )
	dw_left.Show( )
else
	dw_left.Hide( )
end if

//SetRedraw(TRUE)

end subroutine

public function long wf_name_clicked (ref datawindow a_dw, readonly long a_row);// Window function wf_name_clicked (datawindow a_dw, long a_row) 
// returns long

if a_row <= 0 then
	Beep(1)
	Return a_row
end if

a_dw.SelectRow (0, FALSE)
a_dw.SelectRow (a_row, TRUE)

return a_row


end function

public function integer wf_carica_ddlb ();string ls_cod_tipo_rubrica, ls_des_tipo_rubrica, ls_sql
long ll_num

declare cu_ddlb dynamic cursor for sqlsa;

ls_sql = "SELECT tab_tipi_rubriche.cod_tipo_rubrica, " + &
  			       "tab_tipi_rubriche.des_tipo_rubrica  " + &
         " FROM tab_tipi_rubriche " + &  
         " WHERE tab_tipi_rubriche.cod_azienda = '" + s_cs_xx.cod_azienda + "'"

prepare sqlsa from :ls_sql;

open dynamic cu_ddlb;

ll_num = 0
ddlb_1.clear()
do while 1=1
   fetch cu_ddlb into :ls_cod_tipo_rubrica, :ls_des_tipo_rubrica;
	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ddlb_1.additem(ls_des_tipo_rubrica)
   ll_num = ll_num + 1
loop
close cu_ddlb;

return ll_num
end function

on w_rubrica_alfa.create
this.cb_nuovo=create cb_nuovo
this.cb_cancella=create cb_cancella
this.cb_modifica=create cb_modifica
this.st_utente=create st_utente
this.st_4=create st_4
this.st_3=create st_3
this.ddlb_1=create ddlb_1
this.p_notebook=create p_notebook
this.dw_right=create dw_right
this.st_event=create st_event
this.st_2=create st_2
this.st_current_tab=create st_current_tab
this.st_1=create st_1
this.dw_left=create dw_left
this.tab_1=create tab_1
this.Control[]={ this.cb_nuovo,&
this.cb_cancella,&
this.cb_modifica,&
this.st_utente,&
this.st_4,&
this.st_3,&
this.ddlb_1,&
this.p_notebook,&
this.dw_right,&
this.st_event,&
this.st_2,&
this.st_current_tab,&
this.st_1,&
this.dw_left,&
this.tab_1}
end on

on w_rubrica_alfa.destroy
destroy(this.cb_nuovo)
destroy(this.cb_cancella)
destroy(this.cb_modifica)
destroy(this.st_utente)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.ddlb_1)
destroy(this.p_notebook)
destroy(this.dw_right)
destroy(this.st_event)
destroy(this.st_2)
destroy(this.st_current_tab)
destroy(this.st_1)
destroy(this.dw_left)
destroy(this.tab_1)
end on

event open;// Open script for w_tab_notebook5

dw_left.SetTransObject(sqlca)
dw_right.SetTransObject (sqlca)
st_utente.text = s_cs_xx.cod_utente
if wf_carica_ddlb() > 0 then
	ddlb_1.selectitem(1)
	ddlb_1.triggerevent("selectionchanged")
	TriggerEvent ("ue_retrieve_both")
end if

end event

event activate;if s_cs_xx.parametri.parametro_b_1 then
	triggerevent("ue_retrieve_both")
end if
end event

type cb_nuovo from commandbutton within w_rubrica_alfa
event clicked pbm_bnclicked
int X=823
int Y=1421
int Width=366
int Height=81
int TabOrder=21
string Text="Nuovo"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = is_cod_tipo_rubrica
s_cs_xx.parametri.parametro_s_2 = st_utente.text
s_cs_xx.parametri.parametro_d_2 = 2
if isnull(s_cs_xx.parametri.parametro_s_1) then return
if isnull(s_cs_xx.parametri.parametro_s_2) then return
if isvalid(w_rubrica_alfa_dettaglio) then
	w_rubrica_alfa_dettaglio.triggerevent("pc_new")
else
	window_open(w_rubrica_alfa_dettaglio, -1 )
end if
end event

type cb_cancella from commandbutton within w_rubrica_alfa
int X=1601
int Y=1421
int Width=366
int Height=81
int TabOrder=40
string Text="Cancella"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if g_mb.messagebox("Rubrica","Cancello il nominativo selezionato?", Question!, YesNo!, 2) = 1 then
	s_cs_xx.parametri.parametro_s_1 = is_cod_tipo_rubrica
	s_cs_xx.parametri.parametro_s_2 = st_utente.text
	s_cs_xx.parametri.parametro_d_2 = 3
	if isnull(s_cs_xx.parametri.parametro_s_1) then return
	if isnull(s_cs_xx.parametri.parametro_s_2) then return
	delete 
	from rubriche
	where cod_azienda       = :s_cs_xx.cod_azienda and
			cod_tipo_rubrica  = :is_cod_tipo_rubrica and
			cod_utente        = :st_utente.text and
			num_registrazione = :s_cs_xx.parametri.parametro_d_1;
	if sqlca.sqlcode = 0 then
		commit;
		parent.triggerevent("ue_retrieve_both")
	end if
end if
   	
end event

type cb_modifica from commandbutton within w_rubrica_alfa
int X=1212
int Y=1421
int Width=366
int Height=81
int TabOrder=30
string Text="Modifica"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = is_cod_tipo_rubrica
s_cs_xx.parametri.parametro_s_2 = st_utente.text
s_cs_xx.parametri.parametro_d_2 = 3
if isnull(s_cs_xx.parametri.parametro_s_1) then return
if isnull(s_cs_xx.parametri.parametro_s_2) then return
if isvalid(w_rubrica_alfa_dettaglio) then
	w_rubrica_alfa_dettaglio.triggerevent("pc_retrieve")
	w_rubrica_alfa_dettaglio.triggerevent("pc_modify")
else
	window_open(w_rubrica_alfa_dettaglio, -1 )
end if

end event

type st_utente from statictext within w_rubrica_alfa
int X=1738
int Y=21
int Width=343
int Height=81
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_rubrica_alfa
int X=1532
int Y=21
int Width=183
int Height=61
boolean Enabled=false
string Text="Utente:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_rubrica_alfa
int X=23
int Y=21
int Width=366
int Height=61
boolean Enabled=false
string Text="Tipo Rubrica:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_1 from dropdownlistbox within w_rubrica_alfa
int X=389
int Y=21
int Width=1098
int Height=401
int TabOrder=10
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;string ls_str

ls_str = this.text

select cod_tipo_rubrica
into   :is_cod_tipo_rubrica
from   tab_tipi_rubriche
where  des_tipo_rubrica = :ls_str;

parent.triggerEvent("ue_retrieve_both")
//wf_turn_page (1)

end event

type p_notebook from picture within w_rubrica_alfa
int X=1313
int Y=145
int Width=110
int Height=1221
string PictureName="\cs_xx\risorse\spirlnvg.bmp"
boolean FocusRectangle=false
end type

type dw_right from datawindow within w_rubrica_alfa
int X=1418
int Y=137
int Width=1057
int Height=1241
int TabOrder=20
string DataObject="d_rubrica_alfa_lista"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;// Clicked(xpos, ypos, row, dwo) script for dw_right

dw_left.SelectRow ( 0, FALSE )

datawindow	dw
dw = this
wf_name_clicked ( dw, row )

if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
end if
end event

event doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
	s_cs_xx.parametri.parametro_d_2 = 1   // visualizzazione
	if isnull(s_cs_xx.parametri.parametro_s_1) then return
	if isnull(s_cs_xx.parametri.parametro_s_2) then return
	if isnull(s_cs_xx.parametri.parametro_d_1) or (s_cs_xx.parametri.parametro_d_1 = 0) then return
	window_open(w_rubrica_alfa_dettaglio, -1 )
end if
end event

type st_event from statictext within w_rubrica_alfa
int X=389
int Y=1465
int Width=1463
int Height=73
boolean Visible=false
boolean Enabled=false
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_rubrica_alfa
int X=179
int Y=1465
int Width=247
int Height=73
boolean Visible=false
boolean Enabled=false
string Text="Event:"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_current_tab from statictext within w_rubrica_alfa
int X=389
int Y=1381
int Width=604
int Height=73
boolean Visible=false
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_rubrica_alfa
int X=133
int Y=1381
int Width=243
int Height=73
boolean Visible=false
boolean Enabled=false
string Text="Current:"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_left from datawindow within w_rubrica_alfa
int X=266
int Y=137
int Width=1043
int Height=1241
int TabOrder=50
string DataObject="d_rubrica_alfa_lista"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;// Clicked(xpos, ypos, row, dwo) script for dw_left

dw_right.SelectRow ( 0, FALSE )

datawindow	dw
dw = this
wf_name_clicked ( dw, row )


if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
end if
end event

event doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = this.object.cod_tipo_rubrica[row]
	s_cs_xx.parametri.parametro_d_1 = this.object.num_registrazione[row]
	s_cs_xx.parametri.parametro_d_2 = 1   // visualizzazione
	if isnull(s_cs_xx.parametri.parametro_s_1) then return
	if isnull(s_cs_xx.parametri.parametro_s_2) then return
	if isnull(s_cs_xx.parametri.parametro_d_1) or (s_cs_xx.parametri.parametro_d_1 = 0) then return
	window_open(w_rubrica_alfa_dettaglio, -1 )
end if
end event

type tab_1 from tab within w_rubrica_alfa
int Y=121
int Width=2743
int Height=1269
int TabOrder=60
boolean MultiLine=true
boolean PerpendicularText=true
boolean RaggedRight=true
int SelectedTab=1
TabPosition TabPosition=TabsOnRightAndLeft!
long BackColor=74477680
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_a tabpage_a
tabpage_b tabpage_b
tabpage_c tabpage_c
tabpage_d tabpage_d
tabpage_e tabpage_e
tabpage_f tabpage_f
tabpage_g tabpage_g
tabpage_h tabpage_h
tabpage_i tabpage_i
tabpage_j tabpage_j
tabpage_k tabpage_k
tabpage_l tabpage_l
tabpage_m tabpage_m
tabpage_n tabpage_n
tabpage_o tabpage_o
tabpage_p tabpage_p
tabpage_q tabpage_q
tabpage_r tabpage_r
tabpage_s tabpage_s
tabpage_t tabpage_t
tabpage_u tabpage_u
tabpage_v tabpage_v
tabpage_w tabpage_w
tabpage_x tabpage_x
tabpage_y tabpage_y
tabpage_z tabpage_z
end type

on tab_1.create
this.tabpage_a=create tabpage_a
this.tabpage_b=create tabpage_b
this.tabpage_c=create tabpage_c
this.tabpage_d=create tabpage_d
this.tabpage_e=create tabpage_e
this.tabpage_f=create tabpage_f
this.tabpage_g=create tabpage_g
this.tabpage_h=create tabpage_h
this.tabpage_i=create tabpage_i
this.tabpage_j=create tabpage_j
this.tabpage_k=create tabpage_k
this.tabpage_l=create tabpage_l
this.tabpage_m=create tabpage_m
this.tabpage_n=create tabpage_n
this.tabpage_o=create tabpage_o
this.tabpage_p=create tabpage_p
this.tabpage_q=create tabpage_q
this.tabpage_r=create tabpage_r
this.tabpage_s=create tabpage_s
this.tabpage_t=create tabpage_t
this.tabpage_u=create tabpage_u
this.tabpage_v=create tabpage_v
this.tabpage_w=create tabpage_w
this.tabpage_x=create tabpage_x
this.tabpage_y=create tabpage_y
this.tabpage_z=create tabpage_z
this.Control[]={ this.tabpage_a,&
this.tabpage_b,&
this.tabpage_c,&
this.tabpage_d,&
this.tabpage_e,&
this.tabpage_f,&
this.tabpage_g,&
this.tabpage_h,&
this.tabpage_i,&
this.tabpage_j,&
this.tabpage_k,&
this.tabpage_l,&
this.tabpage_m,&
this.tabpage_n,&
this.tabpage_o,&
this.tabpage_p,&
this.tabpage_q,&
this.tabpage_r,&
this.tabpage_s,&
this.tabpage_t,&
this.tabpage_u,&
this.tabpage_v,&
this.tabpage_w,&
this.tabpage_x,&
this.tabpage_y,&
this.tabpage_z}
end on

on tab_1.destroy
destroy(this.tabpage_a)
destroy(this.tabpage_b)
destroy(this.tabpage_c)
destroy(this.tabpage_d)
destroy(this.tabpage_e)
destroy(this.tabpage_f)
destroy(this.tabpage_g)
destroy(this.tabpage_h)
destroy(this.tabpage_i)
destroy(this.tabpage_j)
destroy(this.tabpage_k)
destroy(this.tabpage_l)
destroy(this.tabpage_m)
destroy(this.tabpage_n)
destroy(this.tabpage_o)
destroy(this.tabpage_p)
destroy(this.tabpage_q)
destroy(this.tabpage_r)
destroy(this.tabpage_s)
destroy(this.tabpage_t)
destroy(this.tabpage_u)
destroy(this.tabpage_v)
destroy(this.tabpage_w)
destroy(this.tabpage_x)
destroy(this.tabpage_y)
destroy(this.tabpage_z)
end on

event selectionchanged;// SelectionChanged(oldindex,newindex) script for tab_1

int		li_current_page

//li_current_page = this.selectedtab
li_current_page = newindex

if oldindex = -2 then		// Special-case: Called from w_person_details5
//	this.SelectTab (li_current_page)
// Temporary kludge to get the right tab selected:
	this.SelectTab (li_current_page -1)

end if

wf_turn_page (li_current_page)


end event

type tabpage_a from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="A"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_b from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="B"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_c from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="C"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_d from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="D"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_e from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="E"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_f from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="F"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_g from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="G"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_h from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="H"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_i from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="I"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_j from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="J"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_k from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="K"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_l from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="L"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_m from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="M"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_n from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="N"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_o from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="O"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_p from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="P"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_q from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="Q"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_r from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="R"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_s from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="S"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_t from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="T"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_u from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="U"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_v from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="V"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_w from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="W"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_x from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="X"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_y from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="Y"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

type tabpage_z from userobject within tab_1
int X=247
int Y=17
int Width=2250
int Height=1237
long BackColor=79741120
string Text="Z"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=25166016
end type

