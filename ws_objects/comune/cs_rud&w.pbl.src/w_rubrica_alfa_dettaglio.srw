﻿$PBExportHeader$w_rubrica_alfa_dettaglio.srw
$PBExportComments$Finestra Dati Dettaglio Nominativo Rubrica
forward
global type w_rubrica_alfa_dettaglio from w_cs_xx_principale
end type
type dw_rubriche_det_1 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
end type
type dw_rubriche_det_2 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
end type
type dw_folder from u_folder within w_rubrica_alfa_dettaglio
end type
type dw_rubriche_det_3 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
end type
end forward

global type w_rubrica_alfa_dettaglio from w_cs_xx_principale
int Width=3466
int Height=1281
boolean TitleBar=true
string Title="Rubrica"
dw_rubriche_det_1 dw_rubriche_det_1
dw_rubriche_det_2 dw_rubriche_det_2
dw_folder dw_folder
dw_rubriche_det_3 dw_rubriche_det_3
end type
global w_rubrica_alfa_dettaglio w_rubrica_alfa_dettaglio

type variables
boolean ib_new=FALSE, ib_modify=FALSE, ib_open=FALSE
string is_cod_utente

end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_rubriche_det_1, &
                 "cod_tipo_nominativo", &
                 sqlca, &
                 "tab_tipi_nominativi", &
                 "cod_tipo_nominativo", &
                 "des_nominativo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]


lw_oggetti[1] = dw_rubriche_det_1
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
lw_oggetti[1] = dw_rubriche_det_2
dw_folder.fu_assigntab(2, "Note", lw_oggetti[])
lw_oggetti[1] = dw_rubriche_det_3
dw_folder.fu_assigntab(3, "Reperibilità", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_rubriche_det_1.set_dw_key("cod_azienda")
dw_rubriche_det_1.set_dw_key("cod_tipo_rubrica")
dw_rubriche_det_1.set_dw_key("cod_utente")

choose case s_cs_xx.parametri.parametro_d_2
	case 1 //  visualizza nominativo
		dw_rubriche_det_1.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)
	case 2 // nuovo nominativo
		dw_rubriche_det_1.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_newonopen + &
										  c_noretrieveonopen, &
                                c_default)
	case 3 // modifica nominativo
		dw_rubriche_det_1.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_modifyonopen + &
										  c_nodelete , &
                                c_default)
end choose	
		
dw_rubriche_det_2.set_dw_options(sqlca, &
                                dw_rubriche_det_1, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_rubriche_det_3.set_dw_options(sqlca, &
                                dw_rubriche_det_1, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main = dw_rubriche_det_1


if isnull(s_cs_xx.cod_utente) or (s_cs_xx.cod_utente = "CS_SYSTEM") then
	g_mb.messagebox("SME","Attenzione! non risulta essere stato selezionato alcun utente in fase " + &
	                 "di ingresso nella procedura oppure l'accesso è stato ottenuto tramite " + &
						  "l'utente di sistema: in questo modo l'agenda potrebbe non essere utilizzabile" &
						  , Information!)
end if
is_cod_utente = s_cs_xx.cod_utente
end event

on w_rubrica_alfa_dettaglio.create
int iCurrent
call w_cs_xx_principale::create
this.dw_rubriche_det_1=create dw_rubriche_det_1
this.dw_rubriche_det_2=create dw_rubriche_det_2
this.dw_folder=create dw_folder
this.dw_rubriche_det_3=create dw_rubriche_det_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_rubriche_det_1
this.Control[iCurrent+2]=dw_rubriche_det_2
this.Control[iCurrent+3]=dw_folder
this.Control[iCurrent+4]=dw_rubriche_det_3
end on

on w_rubrica_alfa_dettaglio.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_rubriche_det_1)
destroy(this.dw_rubriche_det_2)
destroy(this.dw_folder)
destroy(this.dw_rubriche_det_3)
end on

event activate;call super::activate;if not(ib_new) and not(ib_modify) and not(ib_open) and &
   s_cs_xx.parametri.parametro_d_1 > 0 and not isnull(s_cs_xx.parametri.parametro_d_1) then
	dw_rubriche_det_1.change_dw_current()
	triggerevent("pc_retrieve")
end if
ib_open = false
s_cs_xx.parametri.parametro_b_1 = false
end event

event open;call super::open;ib_open = true
end event

type dw_rubriche_det_1 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
int X=46
int Y=121
int Width=3315
int Height=801
int TabOrder=40
string DataObject="d_rubriche_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
						 s_cs_xx.parametri.parametro_s_1, &
						 s_cs_xx.parametri.parametro_s_2, &
						 s_cs_xx.parametri.parametro_d_1)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_rubrica")) THEN
      SetItem(l_Idx, "cod_tipo_rubrica", s_cs_xx.parametri.parametro_s_1)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_utente")) THEN
      SetItem(l_Idx, "cod_utente", s_cs_xx.cod_utente)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;long ll_anno,ll_num_registrazione

select max(num_registrazione)
  into :ll_num_registrazione
  from rubriche
  where (cod_azienda      = :s_cs_xx.cod_azienda) and 
        (cod_tipo_rubrica = :s_cs_xx.parametri.parametro_s_1) and
		  (cod_utente       = :s_cs_xx.cod_utente);

if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
	ll_num_registrazione = 1
else
	ll_num_registrazione = ll_num_registrazione + 1
end if
this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)

if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
	this.SetItem (this.GetRow ( ), "num_registrazione", 1)
end if

ib_new = true

end event

event pcd_modify;call super::pcd_modify;ib_modify = true
end event

event pcd_view;call super::pcd_view;ib_modify = false
ib_new = false
end event

event updateend;call super::updateend;string ls_cod_tipo_rubrica
long   ll_num_registrazione

s_cs_xx.parametri.parametro_b_1 = true
if s_cs_xx.parametri.parametro_s_4 = "INSERT_CONTATTI_RUBRICHE" and ib_new then

	ls_cod_tipo_rubrica = dw_rubriche_det_1.object.cod_tipo_rubrica[getrow()]
	ll_num_registrazione = dw_rubriche_det_1.object.num_registrazione[getrow()]
	INSERT INTO contatti_rubriche
				( cod_azienda,   
				  cod_contatto,   
				  cod_tipo_rubrica,   
				  cod_utente,   
				  num_registrazione )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :s_cs_xx.parametri.parametro_s_6,   
				  :ls_cod_tipo_rubrica,   
				  :s_cs_xx.cod_utente,   
				  :ll_num_registrazione )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SME","Errore durante l'aggiornamento del collegamento contatti rubriche: " + &
		           sqlca.sqlerrtext, Information!)
	end if
end if

if s_cs_xx.parametri.parametro_s_4 = "INSERT_FOR_POT_RUBRICHE" and ib_new then

	ls_cod_tipo_rubrica = dw_rubriche_det_1.object.cod_tipo_rubrica[getrow()]
	ll_num_registrazione = dw_rubriche_det_1.object.num_registrazione[getrow()]
	INSERT INTO for_pot_rubriche
				( cod_azienda,   
				  cod_for_pot,
				  cod_tipo_rubrica,   
				  cod_utente,   
				  num_registrazione )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :s_cs_xx.parametri.parametro_s_6,   
				  :ls_cod_tipo_rubrica,   
				  :s_cs_xx.cod_utente,   
				  :ll_num_registrazione )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SME","Errore durante l'aggiornamento del collegamento fornitori potenziali rubriche: " + &
		           sqlca.sqlerrtext, Information!)
	end if
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_str

if len(getitemstring(getrow(),"cognome")) > 0 then
	ls_str = mid(getitemstring(getrow(),"cognome"), 1, 1)
	ls_str = upper(ls_str)
	ls_str = ls_str + mid(getitemstring(getrow(),"cognome"), 2)
	this.setitem(getrow(),"cognome", ls_str)
end if

if len(getitemstring(getrow(),"nome")) > 0 then
	ls_str = mid(getitemstring(getrow(),"nome"), 1, 1)
	ls_str = upper(ls_str)
	ls_str = ls_str + mid(getitemstring(getrow(),"nome"), 2)
	this.setitem(getrow(),"nome", ls_str)
end if

end event

type dw_rubriche_det_2 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
int X=46
int Y=121
int Width=2926
int Height=881
int TabOrder=30
string DataObject="d_rubriche_det_2"
end type

type dw_folder from u_folder within w_rubrica_alfa_dettaglio
int X=23
int Y=21
int Width=3383
int Height=1141
int TabOrder=20
end type

type dw_rubriche_det_3 from uo_cs_xx_dw within w_rubrica_alfa_dettaglio
int X=46
int Y=121
int Width=2789
int Height=1001
int TabOrder=10
string DataObject="d_rubriche_det_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

