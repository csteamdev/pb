﻿$PBExportHeader$w_rubriche.srw
$PBExportComments$Finestra Rubriche
forward
global type w_rubriche from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_rubriche
end type
type cb_ricerca from commandbutton within w_rubriche
end type
type dw_rubriche_det_1 from uo_cs_xx_dw within w_rubriche
end type
type dw_rubriche_det_2 from uo_cs_xx_dw within w_rubriche
end type
type dw_folder from u_folder within w_rubriche
end type
type dw_rubriche_lista from uo_cs_xx_dw within w_rubriche
end type
type dw_folder_search from u_folder within w_rubriche
end type
type dw_ricerca from u_dw_search within w_rubriche
end type
type dw_rubriche_det_3 from uo_cs_xx_dw within w_rubriche
end type
end forward

global type w_rubriche from w_cs_xx_principale
int Width=3621
int Height=1861
boolean TitleBar=true
string Title="Rubrica"
cb_reset cb_reset
cb_ricerca cb_ricerca
dw_rubriche_det_1 dw_rubriche_det_1
dw_rubriche_det_2 dw_rubriche_det_2
dw_folder dw_folder
dw_rubriche_lista dw_rubriche_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
dw_rubriche_det_3 dw_rubriche_det_3
end type
global w_rubriche w_rubriche

type variables
boolean ib_new=FALSE
string is_cod_utente
end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_rubriche_lista, &
                 "cod_tipo_nominativo", &
                 sqlca, &
                 "tab_tipi_nominativi", &
                 "cod_tipo_nominativo", &
                 "des_nominativo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_rubriche_det_1, &
                 "cod_tipo_nominativo", &
                 sqlca, &
                 "tab_tipi_nominativi", &
                 "cod_tipo_nominativo", &
                 "des_nominativo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

dw_ricerca.fu_loadcode("rs_cod_tipo_rubrica", &
                        "tab_tipi_rubriche", &
								"cod_tipo_rubrica", &
								"des_tipo_rubrica", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", "" )
end event

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[]


lw_oggetti[1] = dw_rubriche_det_1
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
lw_oggetti[1] = dw_rubriche_det_2
dw_folder.fu_assigntab(2, "Note", lw_oggetti[])
lw_oggetti[1] = dw_rubriche_det_3
dw_folder.fu_assigntab(3, "Reperibilità", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_rubriche_lista.set_dw_key("cod_azienda")
dw_rubriche_lista.set_dw_key("cod_tipo_rubrica")
dw_rubriche_lista.set_dw_key("cod_utente")

dw_rubriche_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
dw_rubriche_det_1.set_dw_options(sqlca, &
                                dw_rubriche_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_rubriche_det_2.set_dw_options(sqlca, &
                                dw_rubriche_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_rubriche_det_3.set_dw_options(sqlca, &
                                dw_rubriche_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main=dw_rubriche_lista



//-----------------------------  aggiunto per ricerca prodotti ----------------------------
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

l_criteriacolumn[1] = "rs_cod_tipo_rubrica"
l_criteriacolumn[2] = "rs_cod_tipo_nominativo"
l_criteriacolumn[3] = "rs_cognome"
l_criteriacolumn[4] = "rs_nome"
l_criteriacolumn[5] = "rs_posizione"
l_criteriacolumn[6] = "rs_socita"
l_criteriacolumn[7] = "rs_ufficio"
l_criteriacolumn[8] = "rs_localita"
l_criteriacolumn[9] = "rs_cap"
l_criteriacolumn[10] = "rs_provincia"

l_searchtable[1] = "rubriche"
l_searchtable[2] = "rubriche"
l_searchtable[3] = "rubriche"
l_searchtable[4] = "rubriche"
l_searchtable[5] = "rubriche"
l_searchtable[6] = "rubriche"
l_searchtable[7] = "rubriche"
l_searchtable[8] = "rubriche"
l_searchtable[9] = "rubriche"
l_searchtable[10] = "rubriche"

l_searchcolumn[1] = "cod_tipo_rubrica"
l_searchcolumn[2] = "cod_tipo_nominativo"
l_searchcolumn[3] = "cognome"
l_searchcolumn[4] = "nome"
l_searchcolumn[5] = "posizione"
l_searchcolumn[6] = "socita"
l_searchcolumn[7] = "ufficio"
l_searchcolumn[8] = "localita"
l_searchcolumn[9] = "cap"
l_searchcolumn[10] = "provincia"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_rubriche_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_rubriche_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_rubriche_lista.change_dw_current()

if isnull(s_cs_xx.cod_utente) or (s_cs_xx.cod_utente = "CS_SYSTEM") then
	g_mb.messagebox("SME","Attenzione! non risulta essere stato selezionato alcun utente in fase " + &
	                 "di ingresso nella procedura oppure l'accesso è stato ottenuto tramite " + &
						  "l'utente di sistema: in questo modo l'agenda potrebbe non essere utilizzabile" &
						  , Information!)
end if
is_cod_utente = s_cs_xx.cod_utente
end event

on w_rubriche.create
int iCurrent
call w_cs_xx_principale::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.dw_rubriche_det_1=create dw_rubriche_det_1
this.dw_rubriche_det_2=create dw_rubriche_det_2
this.dw_folder=create dw_folder
this.dw_rubriche_lista=create dw_rubriche_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
this.dw_rubriche_det_3=create dw_rubriche_det_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_reset
this.Control[iCurrent+2]=cb_ricerca
this.Control[iCurrent+3]=dw_rubriche_det_1
this.Control[iCurrent+4]=dw_rubriche_det_2
this.Control[iCurrent+5]=dw_folder
this.Control[iCurrent+6]=dw_rubriche_lista
this.Control[iCurrent+7]=dw_folder_search
this.Control[iCurrent+8]=dw_ricerca
this.Control[iCurrent+9]=dw_rubriche_det_3
end on

on w_rubriche.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.dw_rubriche_det_1)
destroy(this.dw_rubriche_det_2)
destroy(this.dw_folder)
destroy(this.dw_rubriche_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
destroy(this.dw_rubriche_det_3)
end on

type cb_reset from commandbutton within w_rubriche
event clicked pbm_bnclicked
int X=3155
int Y=41
int Width=366
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="Annulla Ric."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_rubriche
event clicked pbm_bnclicked
int X=3155
int Y=141
int Width=366
int Height=81
int TabOrder=70
boolean BringToTop=true
string Text="Cerca"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if isnull(dw_ricerca.getitemstring(1,"rs_cod_tipo_rubrica")) then
	g_mb.messagebox("AGENDA","La selezione del tipo agenda è obbligatorio")
	return
end if

dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_rubriche_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_rubriche_det_1 from uo_cs_xx_dw within w_rubriche
int X=46
int Y=701
int Width=3315
int Height=801
int TabOrder=90
string DataObject="d_rubriche_det_1"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_rubriche_det_2 from uo_cs_xx_dw within w_rubriche
int X=46
int Y=701
int Width=2926
int Height=881
int TabOrder=50
string DataObject="d_rubriche_det_2"
end type

type dw_folder from u_folder within w_rubriche
int X=23
int Y=601
int Width=3543
int Height=1141
int TabOrder=40
end type

type dw_rubriche_lista from uo_cs_xx_dw within w_rubriche
int X=138
int Y=41
int Width=3383
int Height=501
int TabOrder=80
string DataObject="d_rubriche_lista"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_tipo_rubrica

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_utente")) then
      this.setitem(ll_i, "cod_utente", is_cod_utente)
   end if
next

ls_tipo_rubrica = dw_ricerca.getitemstring(1,"rs_cod_tipo_rubrica")
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_tipo_rubrica")) then
      this.setitem(ll_i, "cod_tipo_rubrica", ls_tipo_rubrica)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda, s_cs_xx.cod_utente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;string ls_tipo_rubrica

this.setitem(1, "cod_utente", is_cod_utente)
ls_tipo_rubrica = dw_ricerca.getitemstring(1,"rs_cod_tipo_rubrica")
this.setitem(1, "cod_tipo_rubrica", ls_tipo_rubrica)


   long ll_anno,ll_num_registrazione

   select max(rubriche.num_registrazione)
     into :ll_num_registrazione
     from rubriche
     where rubriche.cod_azienda = :s_cs_xx.cod_azienda;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
      ll_num_registrazione = 1
   else
      ll_num_registrazione = ll_num_registrazione + 1
   end if
   this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)

   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
      this.SetItem (this.GetRow ( ), "num_registrazione", 1)
   end if

end event

event pcd_view;call super::pcd_view;ib_new = false
end event

event updatestart;call super::updatestart;//if ib_new then
//
//   long ll_anno,ll_num_registrazione
//
//   select max(rubriche.num_registrazione)
//     into :ll_num_registrazione
//     from rubriche
//     where rubriche.cod_azienda = :s_cs_xx.cod_azienda;
//
//   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_num_registrazione) then
//      ll_num_registrazione = 1
//   else
//      ll_num_registrazione = ll_num_registrazione + 1
//   end if
//   this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)
//
//   if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
//      this.SetItem (this.GetRow ( ), "num_registrazione", 1)
//   end if
//
//   ib_new = false
//end if
//
end event

event pcd_validaterow;call super::pcd_validaterow;if i_extendmode then
	string ls_str
	if len(getitemstring(getrow(),"cognome")) > 0 then
		ls_str = mid(getitemstring(getrow(),"cognome"), 1, 1)
		ls_str = upper(ls_str)
		ls_str = ls_str + mid(getitemstring(getrow(),"cognome"), 2)
		this.setitem(getrow(),"cognome", ls_str)
	end if

	if len(getitemstring(getrow(),"nome")) > 0 then
		ls_str = mid(getitemstring(getrow(),"nome"), 1, 1)
		ls_str = upper(ls_str)
		ls_str = ls_str + mid(getitemstring(getrow(),"nome"), 2)
		this.setitem(getrow(),"nome", ls_str)
	end if
end if
end event

type dw_folder_search from u_folder within w_rubriche
int X=23
int Y=21
int Width=3543
int Height=561
int TabOrder=20
end type

type dw_ricerca from u_dw_search within w_rubriche
int X=138
int Y=41
int Width=2972
int Height=521
int TabOrder=10
string DataObject="d_rubriche_search"
end type

type dw_rubriche_det_3 from uo_cs_xx_dw within w_rubriche
int X=46
int Y=701
int Width=2789
int Height=1001
int TabOrder=30
string DataObject="d_rubriche_det_3"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

