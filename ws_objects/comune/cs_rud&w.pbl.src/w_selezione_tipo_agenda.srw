﻿$PBExportHeader$w_selezione_tipo_agenda.srw
$PBExportComments$Finestra Selezione Tipo Agenda in Fase di Creazione Impegno da Contatti o Fornitori Potenziali
forward
global type w_selezione_tipo_agenda from w_cs_xx_risposta
end type
type dw_selezione_tipi_agende from uo_cs_xx_dw within w_selezione_tipo_agenda
end type
end forward

global type w_selezione_tipo_agenda from w_cs_xx_risposta
int Width=1582
int Height=645
boolean TitleBar=true
string Title="Selezione Tipo Rubrica"
dw_selezione_tipi_agende dw_selezione_tipi_agende
end type
global w_selezione_tipo_agenda w_selezione_tipo_agenda

event pc_setwindow;call super::pc_setwindow;dw_selezione_tipi_agende.set_dw_options(sqlca, &
                                         pcca.null_object, &
													  c_nonew + &
													  c_nodelete + &
													  c_nomodify, &
													  c_default)


end event

on w_selezione_tipo_agenda.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_selezione_tipi_agende=create dw_selezione_tipi_agende
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione_tipi_agende
end on

on w_selezione_tipo_agenda.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_selezione_tipi_agende)
end on

type dw_selezione_tipi_agende from uo_cs_xx_dw within w_selezione_tipo_agenda
int X=23
int Y=21
int Width=1509
int Height=501
string DataObject="d_selezione_tipi_agende"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event doubleclicked;call super::doubleclicked;s_cs_xx.agende_rubriche.tipo_agenda = dw_selezione_tipi_agende.object.cod_tipo_agenda[row]
parent.triggerevent("pc_close")

end event

