﻿$PBExportHeader$w_selezione_tipo_rubrica.srw
$PBExportComments$Finestra Response Selezione Tipo Rubrica
forward
global type w_selezione_tipo_rubrica from w_cs_xx_risposta
end type
type dw_selezione_tipo_rubrica from uo_cs_xx_dw within w_selezione_tipo_rubrica
end type
end forward

global type w_selezione_tipo_rubrica from w_cs_xx_risposta
int Width=1587
int Height=645
boolean TitleBar=true
string Title="Selezione Tipo Rubrica"
dw_selezione_tipo_rubrica dw_selezione_tipo_rubrica
end type
global w_selezione_tipo_rubrica w_selezione_tipo_rubrica

event pc_setwindow;call super::pc_setwindow;dw_selezione_tipo_rubrica.set_dw_options(sqlca, &
                                         pcca.null_object, &
													  c_nonew + &
													  c_nodelete + &
													  c_nomodify, &
													  c_default)


end event

on w_selezione_tipo_rubrica.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_selezione_tipo_rubrica=create dw_selezione_tipo_rubrica
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione_tipo_rubrica
end on

on w_selezione_tipo_rubrica.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_selezione_tipo_rubrica)
end on

type dw_selezione_tipo_rubrica from uo_cs_xx_dw within w_selezione_tipo_rubrica
int X=23
int Y=21
int Width=1509
int Height=501
string DataObject="d_selezione_tipo_rubrica"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event doubleclicked;call super::doubleclicked;s_cs_xx.parametri.parametro_s_1 = dw_selezione_tipo_rubrica.object.cod_tipo_rubrica[row]
parent.triggerevent("pc_close")
end event

