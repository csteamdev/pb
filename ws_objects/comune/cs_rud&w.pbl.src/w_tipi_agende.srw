﻿$PBExportHeader$w_tipi_agende.srw
$PBExportComments$Finestra Tipi Agende
forward
global type w_tipi_agende from w_cs_xx_principale
end type
type dw_tipi_agende from uo_cs_xx_dw within w_tipi_agende
end type
end forward

global type w_tipi_agende from w_cs_xx_principale
int Width=2638
int Height=1145
boolean TitleBar=true
string Title="Tipi Agende"
dw_tipi_agende dw_tipi_agende
end type
global w_tipi_agende w_tipi_agende

event pc_setwindow;call super::pc_setwindow;dw_tipi_agende.set_dw_key("cod_azienda")
dw_tipi_agende.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tipi_agende
end event

on w_tipi_agende.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_agende=create dw_tipi_agende
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_agende
end on

on w_tipi_agende.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_agende)
end on

type dw_tipi_agende from uo_cs_xx_dw within w_tipi_agende
int X=23
int Y=21
int Width=2561
int Height=1001
int TabOrder=1
string DataObject="d_tipi_agende"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

