﻿$PBExportHeader$w_tipi_nominativi.srw
$PBExportComments$Finestra Tipi Nominativi
forward
global type w_tipi_nominativi from w_cs_xx_principale
end type
type dw_tipi_nominativi_lista from uo_cs_xx_dw within w_tipi_nominativi
end type
end forward

global type w_tipi_nominativi from w_cs_xx_principale
int Width=2300
int Height=1149
boolean TitleBar=true
string Title="Tipi Nominativi"
dw_tipi_nominativi_lista dw_tipi_nominativi_lista
end type
global w_tipi_nominativi w_tipi_nominativi

event pc_setwindow;call super::pc_setwindow;dw_tipi_nominativi_lista.set_dw_key("cod_azienda")
dw_tipi_nominativi_lista.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_default, &
                                        c_default)
iuo_dw_main = dw_tipi_nominativi_lista
end event

on w_tipi_nominativi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_nominativi_lista=create dw_tipi_nominativi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_nominativi_lista
end on

on w_tipi_nominativi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_nominativi_lista)
end on

type dw_tipi_nominativi_lista from uo_cs_xx_dw within w_tipi_nominativi
int X=23
int Y=21
int Width=2218
int Height=1001
int TabOrder=10
string DataObject="d_tipi_nominativi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

