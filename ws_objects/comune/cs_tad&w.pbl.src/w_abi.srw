﻿$PBExportHeader$w_abi.srw
$PBExportComments$Finestra Gestione  ABI
forward
global type w_abi from w_cs_xx_principale
end type
type dw_abi from uo_cs_xx_dw within w_abi
end type
end forward

global type w_abi from w_cs_xx_principale
int Width=1564
int Height=1149
boolean TitleBar=true
string Title="Gestione ABI"
dw_abi dw_abi
end type
global w_abi w_abi

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_abi.set_dw_options(sqlca, &
                      pcca.null_object, &
                      c_default, &
                      c_default)
end on

on w_abi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_abi=create dw_abi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_abi
end on

on w_abi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_abi)
end on

type dw_abi from uo_cs_xx_dw within w_abi
int X=23
int Y=21
int Width=1486
int Height=1001
int TabOrder=20
string DataObject="d_abi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

