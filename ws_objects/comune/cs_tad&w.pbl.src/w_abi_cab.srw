﻿$PBExportHeader$w_abi_cab.srw
$PBExportComments$Finestra Gestione ABI CAB
forward
global type w_abi_cab from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_abi_cab
end type
type shl_abicab from statichyperlink within w_abi_cab
end type
type st_1 from statictext within w_abi_cab
end type
type cb_2 from commandbutton within w_abi_cab
end type
type st_file from statictext within w_abi_cab
end type
type dw_abi_cab_det from uo_cs_xx_dw within w_abi_cab
end type
type dw_abi_cab_lista from uo_cs_xx_dw within w_abi_cab
end type
end forward

global type w_abi_cab from w_cs_xx_principale
integer width = 2569
integer height = 1736
string title = "Gestione ABI CAB"
cb_1 cb_1
shl_abicab shl_abicab
st_1 st_1
cb_2 cb_2
st_file st_file
dw_abi_cab_det dw_abi_cab_det
dw_abi_cab_lista dw_abi_cab_lista
end type
global w_abi_cab w_abi_cab

type variables
string is_path_file
long il_file


end variables

forward prototypes
public subroutine wf_getvalue (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, string fs_tipo)
public function string wf_nome_cella (long fl_riga, long fl_colonna)
end prototypes

public subroutine wf_getvalue (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, string fs_tipo);//OLEObject		lole_foglio
//any				lany_ret
//string ls_1
//
////legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
///*
//prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
//poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"
//
//se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
//*/
//
//fs_tipo = ""
//setnull(fd_valore)
//setnull(fs_valore)
//
//lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
//ls_1 = wf_nome_cella(fl_riga, fl_colonna)
//
////lany_ret = lole_foglio.range(wf_nome_cella(fl_riga, fl_colonna) + ":" + wf_nome_cella(fl_riga , fl_colonna) ).value
//lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value
//
//if isnull(lany_ret) then 
//	fs_tipo = "S"
//	setnull(fs_valore)
//	return
//end if
//
//try
//	//prova con valore stringa
//	fs_valore = lany_ret
//	fs_tipo = "S"
//catch (RuntimeError rte2)
//	setnull(fs_valore)
//	setnull(fs_tipo)
//end try
//
//
////try
////	//prova con valore decimale
////	fd_valore = lany_ret
////	fs_tipo = "D"
////catch (RuntimeError rte1)
////	
////	setnull(fd_valore)
////	setnull(fs_tipo)
////	try
////		//prova con valore stringa
////		fs_valore = lany_ret
////		fs_tipo = "S"
////	catch (RuntimeError rte2)
////		setnull(fs_valore)
////		setnull(fs_tipo)
////	end try
////	
////end try
end subroutine

public function string wf_nome_cella (long fl_riga, long fl_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_abi_cab_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_default, &
                                c_default)
dw_abi_cab_det.set_dw_options(sqlca, &
                              dw_abi_cab_lista, &
                              c_sharedata + c_scrollparent, &
                              c_default)
iuo_dw_main = dw_abi_cab_lista
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_abi_cab_det, &
//                 "cod_abi", &
//                 sqlca, &
//                 "tab_abi", &
//                 "cod_abi", &
//                 "des_abi", &
//                 "")
//
end event

on w_abi_cab.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.shl_abicab=create shl_abicab
this.st_1=create st_1
this.cb_2=create cb_2
this.st_file=create st_file
this.dw_abi_cab_det=create dw_abi_cab_det
this.dw_abi_cab_lista=create dw_abi_cab_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.shl_abicab
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.st_file
this.Control[iCurrent+6]=this.dw_abi_cab_det
this.Control[iCurrent+7]=this.dw_abi_cab_lista
end on

on w_abi_cab.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.shl_abicab)
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.st_file)
destroy(this.dw_abi_cab_det)
destroy(this.dw_abi_cab_lista)
end on

event closequery;call super::closequery;fileclose(il_file)
end event

type cb_1 from commandbutton within w_abi_cab
integer x = 1966
integer y = 1480
integer width = 535
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Aggiorna"
end type

event clicked;string ls_valore, ls_tipo, ls_abi, ls_cab, ls_indirizzo,  ls_des_banca, ls_agenzia, ls_str, ls_tipo_riga, ls_localita, ls_cap, ls_provincia
long ll_errore, ll_count, ll_index, ll_riga, ll_cont_abi, ll_cont_cab, ll_aggiornati, ll_nuovi, ll_ret

fileclose(il_file)
il_file = fileopen(is_path_file)

ll_aggiornati = 0
ll_nuovi = 0
ll_riga = 0

do while true	
	ll_ret = fileread(il_file, ls_str)
	if ll_ret < 0 then exit
	ll_riga ++
	
	ls_tipo_riga = left(ls_str,1)
	
	choose case ls_tipo_riga
		case "1"		// cambio banca ABI
			ls_abi = mid(ls_str,3,5)
			ls_des_banca = trim(mid(ls_str,14,40))
			
			select count(*)
			into : ll_cont_abi
			from tab_abi
			where cod_abi = :ls_abi;
			
			if ll_cont_abi > 0 then
				
				update tab_abi
				set des_abi = :ls_des_banca
				where cod_abi = :ls_abi;
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore aggiornamento banche" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
			else
				
				insert into tab_abi
				(cod_abi,
				des_abi)
				values (
				:ls_abi,
				:ls_des_banca);
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore aggiornamento banche" + sqlca.sqlerrtext)
					rollback;
					return
				end if
			end if
			
			commit;
			
			continue
			
		case "2"
			ls_cab =mid(ls_str,8,5)
			ls_indirizzo = trim (mid(ls_str,21,40) )
			ls_localita = trim( mid(ls_str,141,40) )
			ls_cap = trim( mid(ls_str,181,5) )
			ls_provincia = trim( mid(ls_str,186,2) )
			
			continue
			
		case "3"
			ls_agenzia  = trim( mid(ls_str,13,40) )

			select count(*)
			into : ll_cont_cab
			from tab_abicab
			where cod_abi = :ls_abi and cod_cab = :ls_cab;
			
			if ll_cont_cab > 0 then
				
				update tab_abicab
				set 	indirizzo = :ls_indirizzo,
						localita = :ls_localita,
						cap = :ls_cap,
						provincia = :ls_provincia,
						agenzia = :ls_agenzia
				where cod_abi = :ls_abi and cod_cab = :ls_cab;
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore aggiornamento banche" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
				ll_aggiornati ++
				
			else
				
				INSERT INTO tab_abicab  
						( cod_abi,   
						  cod_cab,   
						  indirizzo,   
						  localita,   
						  frazione,   
						  cap,   
						  provincia,   
						  telefono,   
						  fax,   
						  telex,   
						  agenzia )  
				VALUES ( :ls_abi,   
						  :ls_cab,   
						  :ls_indirizzo,   
						  :ls_localita,   
						  null,   
						  :ls_cap,   
						  :ls_provincia,   
						  null,   
						  null,   
						  null,   
						  :ls_agenzia )  ;
						  
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore aggiornamento banche" + sqlca.sqlerrtext)
					rollback;
					return
				end if
				
				ll_nuovi ++

			end if
			
		
			commit;

	end choose			
	
	st_1.text = STRING(ll_riga) + " - " + ls_abi + " - " + ls_cab + " - " + ls_des_banca + " - " + ls_agenzia
	yield()
	
loop

fileclose(il_file)

commit;

g_mb.messagebox("APICE", "Aggiornamento Eseguito~r~nISTITUTI NUOVI=" + string(ll_nuovi) +"~r~n ISTITUTI AGGIORNATI=" + string(ll_aggiornati) )


return 0
end event

type shl_abicab from statichyperlink within w_abi_cab
integer x = 23
integer y = 1260
integer width = 2446
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
string pointer = "HyperLink!"
long textcolor = 134217856
string text = "File ABI-CAB ITalia"
boolean focusrectangle = false
string url = "https://tesoreria.unicreditbanca.it/tesoreria/abicab/abicab.html"
end type

type st_1 from statictext within w_abi_cab
integer x = 32
integer y = 1488
integer width = 1902
integer height = 84
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_abi_cab
integer x = 2267
integer y = 1340
integer width = 233
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "File"
end type

event clicked;string		ls_path, docpath, docname[]
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File TXT da importare", docpath, docname[], "TXT", 	+ "File di testo (*.TXT),*.TXT,", ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	st_file.text = docpath
	is_path_file = docpath
end if
end event

type st_file from statictext within w_abi_cab
integer x = 32
integer y = 1344
integer width = 2222
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Selezionare il file da elaborare"
boolean focusrectangle = false
end type

type dw_abi_cab_det from uo_cs_xx_dw within w_abi_cab
integer x = 23
integer y = 540
integer width = 2469
integer height = 680
integer taborder = 20
string dataobject = "d_abi_cab_det"
borderstyle borderstyle = styleraised!
end type

type dw_abi_cab_lista from uo_cs_xx_dw within w_abi_cab
integer x = 23
integer y = 20
integer width = 2469
integer height = 500
integer taborder = 10
string dataobject = "d_abi_cab_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_error

ll_error = retrieve()

if ll_error < 0 then
   pcca.error = c_fatal
end if
end on

