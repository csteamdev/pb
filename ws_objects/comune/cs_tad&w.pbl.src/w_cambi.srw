﻿$PBExportHeader$w_cambi.srw
$PBExportComments$Finestra Gestione Cambi
forward
global type w_cambi from w_cs_xx_principale
end type
type dw_cambi from uo_cs_xx_dw within w_cambi
end type
end forward

global type w_cambi from w_cs_xx_principale
int Width=1477
int Height=1149
boolean TitleBar=true
string Title="Gestione Cambi"
dw_cambi dw_cambi
end type
global w_cambi w_cambi

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_cambi.set_dw_key("cod_azienda")
dw_cambi.set_dw_key("cod_valuta")
dw_cambi.set_dw_options(sqlca, &
                        i_openparm, &
                        c_scrollparent, &
                        c_default)

end on

on w_cambi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_cambi=create dw_cambi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cambi
end on

on w_cambi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_cambi)
end on

event pc_new;call super::pc_new;dw_cambi.setitem(dw_cambi.getrow(), "data_cambio", datetime(today()))
end event

type dw_cambi from uo_cs_xx_dw within w_cambi
int X=23
int Y=21
int Width=1395
int Height=1001
string DataObject="d_cambi"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_valuta

ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_valuta")) then
      this.setitem(ll_i, "cod_valuta", ls_cod_valuta)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_valuta


ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_valuta)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

