﻿$PBExportHeader$w_cat_risorse_umane.srw
$PBExportComments$Window cat_risorse_umane
forward
global type w_cat_risorse_umane from w_cs_xx_principale
end type
type dw_cat_attrezzature_lista from uo_cs_xx_dw within w_cat_risorse_umane
end type
type dw_cat_attrezzature_1 from uo_cs_xx_dw within w_cat_risorse_umane
end type
end forward

global type w_cat_risorse_umane from w_cs_xx_principale
int Width=1751
int Height=1585
boolean TitleBar=true
string Title="Tabella Categorie Risorse Umane"
dw_cat_attrezzature_lista dw_cat_attrezzature_lista
dw_cat_attrezzature_1 dw_cat_attrezzature_1
end type
global w_cat_risorse_umane w_cat_risorse_umane

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_cat_attrezzature_lista.set_dw_key("cod_azienda")
dw_cat_attrezzature_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_cat_attrezzature_1.set_dw_options(sqlca,dw_cat_attrezzature_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_cat_attrezzature_lista
end on

on w_cat_risorse_umane.create
int iCurrent
call w_cs_xx_principale::create
this.dw_cat_attrezzature_lista=create dw_cat_attrezzature_lista
this.dw_cat_attrezzature_1=create dw_cat_attrezzature_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cat_attrezzature_lista
this.Control[iCurrent+2]=dw_cat_attrezzature_1
end on

on w_cat_risorse_umane.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_cat_attrezzature_lista)
destroy(this.dw_cat_attrezzature_1)
end on

type dw_cat_attrezzature_lista from uo_cs_xx_dw within w_cat_risorse_umane
int X=23
int Y=21
int Width=1669
int Height=501
int TabOrder=10
string DataObject="d_cat_risorse_umane_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "flag_risorsa_umana", "S")
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_cat_attrezzature_1 from uo_cs_xx_dw within w_cat_risorse_umane
int X=23
int Y=541
int Width=1669
int Height=921
int TabOrder=20
string DataObject="d_cat_risorse_umane_1"
BorderStyle BorderStyle=StyleRaised!
end type

