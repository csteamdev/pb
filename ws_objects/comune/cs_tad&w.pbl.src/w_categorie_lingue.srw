﻿$PBExportHeader$w_categorie_lingue.srw
$PBExportComments$Finestra Gestione Categorie Lingue
forward
global type w_categorie_lingue from w_cs_xx_principale
end type
type dw_categorie_lingue from uo_cs_xx_dw within w_categorie_lingue
end type
end forward

global type w_categorie_lingue from w_cs_xx_principale
int Width=2277
int Height=1149
boolean TitleBar=true
string Title="Gestione Categorie Lingue"
dw_categorie_lingue dw_categorie_lingue
end type
global w_categorie_lingue w_categorie_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_categorie_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_categorie_lingue.set_dw_key("cod_azienda")
dw_categorie_lingue.set_dw_key("cod_categoria")
dw_categorie_lingue.set_dw_options(sqlca, &
                                   i_openparm, &
                                   c_scrollparent, &
                                   c_default)

end on

on w_categorie_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_categorie_lingue=create dw_categorie_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_categorie_lingue
end on

on w_categorie_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_categorie_lingue)
end on

type dw_categorie_lingue from uo_cs_xx_dw within w_categorie_lingue
int X=23
int Y=21
int Width=2195
int Height=1001
string DataObject="d_categorie_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_categoria


ls_cod_categoria = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_categoria")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_categoria)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_categoria

ls_cod_categoria = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_categoria")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_categoria")) then
      this.setitem(ll_i, "cod_categoria", ls_cod_categoria)
   end if
next
end on

