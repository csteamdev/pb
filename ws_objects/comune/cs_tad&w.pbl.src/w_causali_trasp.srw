﻿$PBExportHeader$w_causali_trasp.srw
$PBExportComments$Finestra Causali Trasporto
forward
global type w_causali_trasp from w_cs_xx_principale
end type
type dw_causali_trasp from uo_cs_xx_dw within w_causali_trasp
end type
type cb_lingue from commandbutton within w_causali_trasp
end type
end forward

global type w_causali_trasp from w_cs_xx_principale
integer width = 2734
integer height = 1240
string title = "Causali Trasporto"
dw_causali_trasp dw_causali_trasp
cb_lingue cb_lingue
end type
global w_causali_trasp w_causali_trasp

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_causali_trasp.set_dw_key("cod_azienda")
dw_causali_trasp.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)
cb_lingue.enabled=false
end event

on w_causali_trasp.create
int iCurrent
call super::create
this.dw_causali_trasp=create dw_causali_trasp
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_causali_trasp
this.Control[iCurrent+2]=this.cb_lingue
end on

on w_causali_trasp.destroy
call super::destroy
destroy(this.dw_causali_trasp)
destroy(this.cb_lingue)
end on

type dw_causali_trasp from uo_cs_xx_dw within w_causali_trasp
integer x = 23
integer y = 20
integer width = 2651
integer height = 1000
integer taborder = 10
string dataobject = "d_causali_trasp"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_causale

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_causali_trasp_lingue"
      ls_codice  = "cod_causale"
      ls_cod_causale = this.getitemstring(li_i, "cod_causale", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_causale)
   next
end if
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

type cb_lingue from commandbutton within w_causali_trasp
integer x = 2286
integer y = 1040
integer width = 375
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;window_open_parm(w_causali_trasp_lingue, -1, dw_causali_trasp)

end event

