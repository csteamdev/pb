﻿$PBExportHeader$w_causali_trasp_lingue.srw
$PBExportComments$Finestra Causali Trasporto Lingue
forward
global type w_causali_trasp_lingue from w_cs_xx_principale
end type
type dw_causali_trasp_lingue from uo_cs_xx_dw within w_causali_trasp_lingue
end type
end forward

global type w_causali_trasp_lingue from w_cs_xx_principale
int Width=2277
int Height=1149
boolean TitleBar=true
string Title="Lingue Causali Trasporto "
dw_causali_trasp_lingue dw_causali_trasp_lingue
end type
global w_causali_trasp_lingue w_causali_trasp_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_causali_trasp_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event pc_setwindow;call super::pc_setwindow;dw_causali_trasp_lingue.set_dw_key("cod_azienda")
dw_causali_trasp_lingue.set_dw_key("cod_causale")
dw_causali_trasp_lingue.set_dw_options(sqlca, &
                            			  i_openparm, &
                            			  c_scrollparent, &
                            			  c_default)

end event

on w_causali_trasp_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_causali_trasp_lingue=create dw_causali_trasp_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_causali_trasp_lingue
end on

on w_causali_trasp_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_causali_trasp_lingue)
end on

type dw_causali_trasp_lingue from uo_cs_xx_dw within w_causali_trasp_lingue
int X=23
int Y=21
int Width=2195
int Height=1001
string DataObject="d_causali_trasp_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_causale


ls_cod_causale = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_causale")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_causale)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_causale

ls_cod_causale = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_causale")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_causale")) then
      this.setitem(ll_i, "cod_causale", ls_cod_causale)
   end if
next
end event

