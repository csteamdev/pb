﻿$PBExportHeader$w_centri_costo.srw
$PBExportComments$Finestra Gestione Centri di Costo
forward
global type w_centri_costo from w_cs_xx_principale
end type
type dw_centri_costo from uo_cs_xx_dw within w_centri_costo
end type
end forward

global type w_centri_costo from w_cs_xx_principale
integer width = 3785
integer height = 1868
string title = "Gestione Centri di Costo"
dw_centri_costo dw_centri_costo
end type
global w_centri_costo w_centri_costo

event pc_setwindow;call super::pc_setwindow;string ls_odbc_presenze

dw_centri_costo.set_dw_key("cod_azienda")
dw_centri_costo.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
										 
select stringa
into :ls_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if ls_odbc_presenze<>"" and not isnull(ls_odbc_presenze) then
	//rendi visibili i campi
	dw_centri_costo.object.flag_ripartisci_importi.visible = 1
	dw_centri_costo.object.flag_considera_timbrature.visible = 1
	dw_centri_costo.object.sigla_cdc.visible = 1
else
	//rendi invisibili i campi
	dw_centri_costo.object.flag_ripartisci_importi.visible = 0
	dw_centri_costo.object.flag_considera_timbrature.visible = 0
	dw_centri_costo.object.sigla_cdc.visible = 0
end if

end event

on w_centri_costo.create
int iCurrent
call super::create
this.dw_centri_costo=create dw_centri_costo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_centri_costo
end on

on w_centri_costo.destroy
call super::destroy
destroy(this.dw_centri_costo)
end on

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_centri_costo, &
						  "codice_impresa", &
						  sqlci, &
						  "cc_conto", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

type dw_centri_costo from uo_cs_xx_dw within w_centri_costo
integer x = 23
integer y = 20
integer width = 3703
integer height = 1720
string dataobject = "d_centri_costo"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

