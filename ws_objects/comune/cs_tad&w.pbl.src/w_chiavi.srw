﻿$PBExportHeader$w_chiavi.srw
forward
global type w_chiavi from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_chiavi
end type
type dw_chiavi from uo_cs_xx_dw within w_chiavi
end type
type cb_lingue from commandbutton within w_chiavi
end type
end forward

global type w_chiavi from w_cs_xx_principale
integer width = 2528
integer height = 1256
string title = "Gestioni Chiavi"
cb_1 cb_1
dw_chiavi dw_chiavi
cb_lingue cb_lingue
end type
global w_chiavi w_chiavi

on w_chiavi.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_chiavi=create dw_chiavi
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_chiavi
this.Control[iCurrent+3]=this.cb_lingue
end on

on w_chiavi.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_chiavi)
destroy(this.cb_lingue)
end on

event pc_setwindow;call super::pc_setwindow;dw_chiavi.set_dw_key("cod_azienda")
dw_chiavi.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)

end event

type cb_1 from commandbutton within w_chiavi
integer x = 1669
integer y = 1040
integer width = 375
integer height = 80
integer taborder = 12
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Valori"
end type

event clicked;window_open_parm(w_chiavi_valori, -1, dw_chiavi)

end event

type dw_chiavi from uo_cs_xx_dw within w_chiavi
integer x = 23
integer y = 16
integer width = 2441
integer height = 1000
string dataobject = "d_chiavi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_lingue from commandbutton within w_chiavi
integer x = 2080
integer y = 1040
integer width = 375
integer height = 80
integer taborder = 2
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

event clicked;window_open_parm(w_chiavi_lingue, -1, dw_chiavi)

end event

