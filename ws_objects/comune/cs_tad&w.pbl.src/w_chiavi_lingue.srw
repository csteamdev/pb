﻿$PBExportHeader$w_chiavi_lingue.srw
forward
global type w_chiavi_lingue from w_cs_xx_principale
end type
type dw_chiavi_lingua from uo_cs_xx_dw within w_chiavi_lingue
end type
end forward

global type w_chiavi_lingue from w_cs_xx_principale
int Width=2277
int Height=1149
boolean TitleBar=true
string Title="Gestione Chiavi Lingue"
dw_chiavi_lingua dw_chiavi_lingua
end type
global w_chiavi_lingue w_chiavi_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_chiavi_lingua, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_chiavi_lingua.set_dw_key("cod_azienda")
dw_chiavi_lingua.set_dw_key("cod_chiave")
dw_chiavi_lingua.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_scrollparent, &
                                 c_default)

end event

on w_chiavi_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_chiavi_lingua=create dw_chiavi_lingua
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_chiavi_lingua
end on

on w_chiavi_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_chiavi_lingua)
end on

type dw_chiavi_lingua from uo_cs_xx_dw within w_chiavi_lingue
int X=23
int Y=21
int Width=2195
int Height=1001
string DataObject="d_chiavi_lingua"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_chiave


ls_cod_chiave = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_chiave")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_chiave)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_chiave

ls_cod_chiave = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_chiave")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_chiave")) then
      this.setitem(ll_i, "cod_chiave", ls_cod_chiave)
   end if
next
end event

