﻿$PBExportHeader$w_chiavi_valori.srw
forward
global type w_chiavi_valori from w_cs_xx_principale
end type
type dw_chiavi_valori from uo_cs_xx_dw within w_chiavi_valori
end type
end forward

global type w_chiavi_valori from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2272
integer height = 1148
string title = "Gestione Valore Chiavi"
dw_chiavi_valori dw_chiavi_valori
end type
global w_chiavi_valori w_chiavi_valori

event pc_setwindow;call super::pc_setwindow;dw_chiavi_valori.set_dw_key("cod_azienda")
dw_chiavi_valori.set_dw_key("cod_chiave")
dw_chiavi_valori.set_dw_key("progressivo")
dw_chiavi_valori.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_scrollparent, &
                                 c_default)

end event

on w_chiavi_valori.create
int iCurrent
call super::create
this.dw_chiavi_valori=create dw_chiavi_valori
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_chiavi_valori
end on

on w_chiavi_valori.destroy
call super::destroy
destroy(this.dw_chiavi_valori)
end on

type dw_chiavi_valori from uo_cs_xx_dw within w_chiavi_valori
integer x = 23
integer y = 20
integer width = 2194
integer height = 1000
string dataobject = "d_chiavi_valori"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_chiave


ls_cod_chiave = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_chiave")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_chiave)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_max
string ls_cod_chiave

ls_cod_chiave = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_chiave")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_chiave")) then
      this.setitem(ll_i, "cod_chiave", ls_cod_chiave)
   end if
	if isnull(this.getitemnumber(ll_i, "progressivo")) then
		
		select max(progressivo)
		into   :ll_max
		from   tab_chiavi_valori
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_chiave = :ls_cod_chiave;
				 
		if isnull(ll_max) then ll_max = 0
		ll_max ++
      this.setitem(ll_i, "progressivo", ll_max)
   end if
next
end event

