﻿$PBExportHeader$w_corrispondenze.srw
$PBExportComments$Finestra Gestione Corrispondenze
forward
global type w_corrispondenze from w_cs_xx_principale
end type
type dw_corrispondenze from uo_cs_xx_dw within w_corrispondenze
end type
end forward

global type w_corrispondenze from w_cs_xx_principale
int Width=3575
int Height=1145
boolean TitleBar=true
string Title="Gestione Corrispondenze"
dw_corrispondenze dw_corrispondenze
end type
global w_corrispondenze w_corrispondenze

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_corrispondenze, &
                 "num_reg_lista", &
                 sqlca, &
                 "tes_liste_controllo", &
                 "num_reg_lista", &
                 "des_lista", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_corrispondenze.set_dw_key("cod_azienda")
dw_corrispondenze.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)

end on

on w_corrispondenze.create
int iCurrent
call w_cs_xx_principale::create
this.dw_corrispondenze=create dw_corrispondenze
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_corrispondenze
end on

on w_corrispondenze.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_corrispondenze)
end on

type dw_corrispondenze from uo_cs_xx_dw within w_corrispondenze
int X=23
int Y=21
int Width=3498
int Height=1001
int TabOrder=10
string DataObject="d_corrispondenze"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   integer li_num_reg_lista, li_num_versione, li_num_edizione


   choose case i_colname
      case "num_reg_lista"
         li_num_reg_lista = integer(i_coltext)
         select   tes_liste_controllo.num_versione,
                  tes_liste_controllo.num_edizione
         into     :li_num_versione, 
                  :li_num_edizione
         from     tes_liste_controllo
         where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                  tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                  tes_liste_controllo.flag_valido = 'S';

         this.setitem(this.getrow(), "num_versione", li_num_versione)
         this.setitem(this.getrow(), "num_edizione", li_num_edizione)
   end choose
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

