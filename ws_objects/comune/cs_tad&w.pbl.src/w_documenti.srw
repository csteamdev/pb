﻿$PBExportHeader$w_documenti.srw
$PBExportComments$Finestra Gestione Documenti
forward
global type w_documenti from w_cs_xx_principale
end type
type dw_documenti_lista from uo_cs_xx_dw within w_documenti
end type
type dw_documenti_det from uo_cs_xx_dw within w_documenti
end type
type cb_lingue from commandbutton within w_documenti
end type
end forward

global type w_documenti from w_cs_xx_principale
int Width=1747
int Height=1409
boolean TitleBar=true
string Title="Gestione Documenti"
dw_documenti_lista dw_documenti_lista
dw_documenti_det dw_documenti_det
cb_lingue cb_lingue
end type
global w_documenti w_documenti

type variables

end variables

on pc_delete;call w_cs_xx_principale::pc_delete;cb_lingue.enabled=false
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_documenti_lista.set_dw_key("cod_azienda")
dw_documenti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_documenti_det.set_dw_options(sqlca, &
                                dw_documenti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
iuo_dw_main = dw_documenti_lista
cb_lingue.enabled=false
end on

on w_documenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_documenti_lista=create dw_documenti_lista
this.dw_documenti_det=create dw_documenti_det
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_documenti_lista
this.Control[iCurrent+2]=dw_documenti_det
this.Control[iCurrent+3]=cb_lingue
end on

on w_documenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_documenti_lista)
destroy(this.dw_documenti_det)
destroy(this.cb_lingue)
end on

type dw_documenti_lista from uo_cs_xx_dw within w_documenti
int X=23
int Y=21
int Width=1669
int Height=501
int TabOrder=10
string DataObject="d_tab_documenti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_documento")) then
      cb_lingue.enabled = true
   else
      cb_lingue.enabled = false
   end if
end if
end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_documento

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_documenti_lingue"
      ls_codice  = "cod_documento"
      ls_cod_documento = this.getitemstring(li_i, "cod_documento", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_documento)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_documento")) then
      cb_lingue.enabled = true
   else
      cb_lingue.enabled = false
   end if
end if
end on

type dw_documenti_det from uo_cs_xx_dw within w_documenti
int X=23
int Y=541
int Width=1669
int Height=641
int TabOrder=20
string DataObject="d_documenti_det"
BorderStyle BorderStyle=StyleRaised!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   dw_documenti_lista.postevent(updatestart!)
end if
end on

type cb_lingue from commandbutton within w_documenti
int X=1326
int Y=1201
int Width=366
int Height=81
int TabOrder=30
string Text="&Lingue"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_documenti_lingue, -1, dw_documenti_lista)
end on

