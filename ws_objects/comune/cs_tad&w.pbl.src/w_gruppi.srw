﻿$PBExportHeader$w_gruppi.srw
$PBExportComments$Finestra Gestione Grupppi Contabili
forward
global type w_gruppi from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_gruppi
end type
type dw_gruppi_lista from uo_cs_xx_dw within w_gruppi
end type
type dw_gruppi_det_1 from uo_cs_xx_dw within w_gruppi
end type
type dw_gruppi_det_2 from uo_cs_xx_dw within w_gruppi
end type
type dw_folder from u_folder within w_gruppi
end type
end forward

global type w_gruppi from w_cs_xx_principale
integer width = 2245
integer height = 1868
string title = "Gestione Gruppi Contabili"
cb_2 cb_2
dw_gruppi_lista dw_gruppi_lista
dw_gruppi_det_1 dw_gruppi_det_1
dw_gruppi_det_2 dw_gruppi_det_2
dw_folder dw_folder
end type
global w_gruppi w_gruppi

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;windowobject lw_oggetti[]


lw_oggetti[1] = dw_gruppi_det_1
dw_folder.fu_assigntab(1, "Vendite", lw_oggetti[])
lw_oggetti[1] = dw_gruppi_det_2
dw_folder.fu_assigntab(2, "Acquisti", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_gruppi_lista.set_dw_key("cod_azienda")
dw_gruppi_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_gruppi_det_1.set_dw_options(sqlca, &
                               dw_gruppi_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
dw_gruppi_det_2.set_dw_options(sqlca, &
                               dw_gruppi_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)

iuo_dw_main=dw_gruppi_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_fat_ital", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_fat_est", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_fat_cee", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_not_ital", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_not_est", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_1, &
                 "cod_conto_ven_not_cee", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_fat_ital", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_fat_est", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_fat_cee", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_not_ital", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_not_est", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_gruppi_det_2, &
                 "cod_conto_acq_not_cee", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_gruppi.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.dw_gruppi_lista=create dw_gruppi_lista
this.dw_gruppi_det_1=create dw_gruppi_det_1
this.dw_gruppi_det_2=create dw_gruppi_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.dw_gruppi_lista
this.Control[iCurrent+3]=this.dw_gruppi_det_1
this.Control[iCurrent+4]=this.dw_gruppi_det_2
this.Control[iCurrent+5]=this.dw_folder
end on

on w_gruppi.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.dw_gruppi_lista)
destroy(this.dw_gruppi_det_1)
destroy(this.dw_gruppi_det_2)
destroy(this.dw_folder)
end on

type cb_2 from commandbutton within w_gruppi
integer x = 1755
integer y = 1664
integer width = 430
integer height = 81
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Centro di Costo"
end type

event clicked;if dw_gruppi_lista.rowcount() > 0 then
	window_open_parm(w_gruppi_cc, -1, dw_gruppi_lista)

end if
end event

type dw_gruppi_lista from uo_cs_xx_dw within w_gruppi
integer x = 23
integer y = 20
integer width = 2171
integer height = 500
integer taborder = 30
string dataobject = "d_gruppi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_gruppi_det_1 from uo_cs_xx_dw within w_gruppi
integer x = 46
integer y = 640
integer width = 2126
integer height = 980
integer taborder = 40
string dataobject = "d_gruppi_det_1"
boolean border = false
end type

type dw_gruppi_det_2 from uo_cs_xx_dw within w_gruppi
integer x = 46
integer y = 640
integer width = 2126
integer height = 680
integer taborder = 20
string dataobject = "d_gruppi_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_gruppi
integer x = 23
integer y = 540
integer width = 2171
integer height = 1100
integer taborder = 10
end type

