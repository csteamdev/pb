﻿$PBExportHeader$w_imposta_rate.srw
forward
global type w_imposta_rate from w_cs_xx_principale
end type
type cb_importa from commandbutton within w_imposta_rate
end type
type hpb_1 from hprogressbar within w_imposta_rate
end type
type st_elemento from statictext within w_imposta_rate
end type
end forward

global type w_imposta_rate from w_cs_xx_principale
integer width = 1390
integer height = 352
string title = "Impostazione Rate Pagamenti"
boolean minbox = false
boolean maxbox = false
cb_importa cb_importa
hpb_1 hpb_1
st_elemento st_elemento
end type
global w_imposta_rate w_imposta_rate

on w_imposta_rate.create
int iCurrent
call super::create
this.cb_importa=create cb_importa
this.hpb_1=create hpb_1
this.st_elemento=create st_elemento
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_importa
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.st_elemento
end on

on w_imposta_rate.destroy
call super::destroy
destroy(this.cb_importa)
destroy(this.hpb_1)
destroy(this.st_elemento)
end on

type cb_importa from commandbutton within w_imposta_rate
integer x = 23
integer y = 20
integer width = 1303
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "AVVIA IMPOSTAZIONE AUTOMATICA RATE"
end type

event clicked;long   ll_i, ll_count, ll_partenza, ll_num_rate, ll_giorni_prima, ll_giorni_int, ll_giorni_ultima, &
		 ll_giorni_rata, ll_int_partenza, ll_rata, ll_giorno_fisso

string ls_cod_pagamento, ls_des_pagamento, ls_iva_prima_rata, ls_tipo_rata, ls_tipo_scadenza, ls_cod_tipo_pag

dec{4} ld_perc_prima, ld_perc_ultima, ld_perc_iva, ld_perc_imp, ld_iva_prima, ld_imp_prima, &
		 ld_iva_ultima, ld_imp_ultima, ld_iva_int, ld_imp_int, ld_scarto_iva, ld_scarto_imp


setpointer(hourglass!)

st_elemento.text = "Lettura elenco pagamenti"

datastore lds_pagamenti

lds_pagamenti = create datastore

lds_pagamenti.dataobject = "d_elenco_pagamenti"

lds_pagamenti.settransobject(sqlca)

lds_pagamenti.retrieve(s_cs_xx.cod_azienda)

ll_count = lds_pagamenti.rowcount()

for ll_i = 1 to ll_count
	
	ls_cod_pagamento = lds_pagamenti.getitemstring(ll_i,"cod_pagamento")
	
	ls_cod_tipo_pag = lds_pagamenti.getitemstring(ll_i,"codice_tipo_pag_impresa")
	
	ls_des_pagamento = lds_pagamenti.getitemstring(ll_i,"des_pagamento")
	
	st_elemento.text = ls_cod_pagamento + " - " + ls_des_pagamento
	
	hpb_1.position = round((ll_i * 100 / ll_count),0)
	
	delete
	from   tab_pagamenti_rate
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_pagamento = :ls_cod_pagamento;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in azzeramento rate pagamento " + ls_cod_pagamento + ": " + sqlca.sqlerrtext,stopsign!)
		rollback;
		destroy lds_pagamenti
		setpointer(arrow!)
		st_elemento.text = "ERRORE"
		return -1
	end if
	
	ls_tipo_scadenza = lds_pagamenti.getitemstring(ll_i,"flag_partenza")
	
	if ls_tipo_scadenza = "M" then
		ls_tipo_scadenza = "F"
		ll_giorno_fisso = 15
	elseif ls_tipo_scadenza = "F" then
		ls_tipo_scadenza = "M"
		setnull(ll_giorno_fisso)
	else
		setnull(ll_giorno_fisso)
	end if
	
	ll_partenza = lds_pagamenti.getitemnumber(ll_i,"int_partenza")
	
	ll_num_rate = lds_pagamenti.getitemnumber(ll_i,"num_rate_com")
	
	ld_perc_prima = lds_pagamenti.getitemnumber(ll_i,"perc_prima_rata")
	
	ll_giorni_prima = lds_pagamenti.getitemnumber(ll_i,"num_gior_prima_rata")
	
	ll_giorni_int = lds_pagamenti.getitemnumber(ll_i,"num_gior_int_rata")
	
	ld_perc_ultima = lds_pagamenti.getitemnumber(ll_i,"perc_ult_rata")
	
	ll_giorni_ultima = lds_pagamenti.getitemnumber(ll_i,"num_gior_ult_rata")
	
	ls_iva_prima_rata = lds_pagamenti.getitemstring(ll_i,"flag_iva_prima_rata")
	
	choose case ls_iva_prima_rata
			
		case "S"
			
			ld_iva_prima = 100
			
			ld_imp_prima = 0
			
			ld_iva_ultima = 0
			
			ld_imp_ultima = ld_perc_ultima
			
		case "C"
			
			ld_iva_prima = 100
			
			ld_imp_prima = ld_perc_prima
			
			ld_iva_ultima = 0
			
			ld_imp_ultima = ld_perc_ultima
			
		case "N"
			
			ld_iva_prima = ld_perc_prima
			
			ld_imp_prima = ld_perc_prima
			
			ld_iva_ultima = ld_perc_ultima
			
			ld_imp_ultima = ld_perc_ultima
			
	end choose
	
	if ll_num_rate > 2 then
		
		ld_iva_int = truncate((100 - ld_iva_prima - ld_iva_ultima) / (ll_num_rate - 2),2)
		
		ld_imp_int = truncate((100 - ld_imp_prima - ld_imp_ultima) / (ll_num_rate - 2),2)
		
	else
		
		ld_iva_int = 0
		
		ld_imp_int = 0
		
	end if
	
	ld_scarto_iva = 100 - (ld_iva_int * (ll_num_rate - 2) + ld_iva_prima + ld_iva_ultima)
		
	if ld_scarto_iva > 0 then
		ld_iva_ultima = ld_iva_ultima + ld_scarto_iva
	end if
	
	ld_scarto_imp = 100 - (ld_imp_int * (ll_num_rate - 2) + ld_imp_prima + ld_imp_ultima)
		
	if ld_scarto_imp > 0 then
		ld_imp_ultima = ld_imp_ultima + ld_scarto_imp
	end if

	ll_giorni_rata = 0
	
	if not isnull(ll_partenza) then
		ll_giorni_rata = ll_giorni_rata + ll_int_partenza
	end if
	
	for ll_rata = 1 to ll_num_rate
		
		choose case ll_rata
				
			case 1
				
				ll_giorni_rata = ll_giorni_rata + ll_giorni_prima
				
				ld_perc_iva = ld_iva_prima
						
				ld_perc_imp = ld_imp_prima
						
			case ll_num_rate
				
				ll_giorni_rata = ll_giorni_rata + ll_giorni_ultima
				
				ld_perc_iva = ld_iva_ultima
						
				ld_perc_imp = ld_imp_ultima
				
			case else
				
				ll_giorni_rata = ll_giorni_rata + ll_giorni_int
				
				ld_perc_iva = ld_iva_int
				
				ld_perc_imp = ld_imp_int
				
		end choose
		
		insert
		into   tab_pagamenti_rate
				 (cod_azienda,
				 cod_pagamento,
				 prog_rata,
				 ordine_rata,
				 giorni_decorrenza,
				 numero_rate,
				 giorni_tra_rate,
				 tipo_scadenza,
				 giorno_fisso,
				 perc_iva,
				 perc_imponibile,
				 cod_tipo_pagamento)
		values (:s_cs_xx.cod_azienda,
				 :ls_cod_pagamento,
				 :ll_rata,
				 :ll_rata,
				 :ll_giorni_rata,
				 1,
				 0,
				 :ls_tipo_scadenza,
				 :ll_giorno_fisso,
				 :ld_perc_iva,
				 :ld_perc_imp,
				 :ls_cod_tipo_pag);
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento rate pagamento " + ls_cod_pagamento + ": " + sqlca.sqlerrtext,stopsign!)
			rollback;
			destroy lds_pagamenti
			setpointer(arrow!)
			st_elemento.text = "ERRORE"
			return -1
		end if
		
	next
	
	commit;
	
next

destroy lds_pagamenti

setpointer(arrow!)

st_elemento.text = "Operazione completata"
end event

type hpb_1 from hprogressbar within w_imposta_rate
integer x = 23
integer y = 180
integer width = 1303
integer height = 40
unsignedinteger maxposition = 100
integer setstep = 1
boolean smoothscroll = true
end type

type st_elemento from statictext within w_imposta_rate
integer x = 23
integer y = 120
integer width = 1303
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

