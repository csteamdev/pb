﻿$PBExportHeader$w_ive.srw
$PBExportComments$Finestra Gestione Ive
forward
global type w_ive from w_cs_xx_principale
end type
type dw_ive from uo_cs_xx_dw within w_ive
end type
type cb_lingue from commandbutton within w_ive
end type
end forward

global type w_ive from w_cs_xx_principale
integer width = 4018
integer height = 1560
string title = "Aliquote Iva"
dw_ive dw_ive
cb_lingue cb_lingue
end type
global w_ive w_ive

type variables
uo_impresa iuo_impresa

end variables

event pc_setwindow;call super::pc_setwindow;dw_ive.set_dw_key("cod_azienda")
dw_ive.set_dw_options(sqlca, &
                      pcca.null_object, &
                      c_default, &
                      c_default)
cb_lingue.enabled = false

end event

on w_ive.create
int iCurrent
call super::create
this.dw_ive=create dw_ive
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ive
this.Control[iCurrent+2]=this.cb_lingue
end on

on w_ive.destroy
call super::destroy
destroy(this.dw_ive)
destroy(this.cb_lingue)
end on

event open;call super::open;iuo_impresa = CREATE uo_impresa

end event

event close;call super::close;destroy iuo_impresa
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ive, &
					  "cod_iva_differenziato", &
					  sqlca, &
					  "tab_ive", &
					  "cod_iva", &
					  "des_iva", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		  

end event

type dw_ive from uo_cs_xx_dw within w_ive
integer x = 23
integer y = 20
integer width = 3918
integer height = 1308
integer taborder = 10
string dataobject = "d_ive"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_iva


   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_ive_lingue"
      ls_codice  = "cod_iva"
      ls_cod_iva = this.getitemstring(li_i, "cod_iva", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_iva)
		if iuo_impresa.uof_tab_iva("D", ls_cod_iva ,"", "", 0, 0) = -1 then
			g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
			return 1
		end if
   next
	
   for li_i = 1 to rowcount()
		if getitemstatus(li_i, 0, primary!) = newmodified! and s_cs_xx.parametri.impresa then
			ls_cod_iva = this.getitemstring(li_i, "cod_iva")
			if iuo_impresa.uof_tab_iva("I", getitemstring(getrow(),"cod_iva"), getitemstring(getrow(),"des_iva"), getitemstring(getrow(),"des_estesa"), getitemnumber(getrow(),"aliquota"), 100) = -1 then
				g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
				return 1
			end if
			
		elseif getitemstatus(li_i, 0, primary!) = datamodified! and s_cs_xx.parametri.impresa then
			ls_cod_iva = this.getitemstring(li_i, "cod_iva")
			if iuo_impresa.uof_tab_iva("M", getitemstring(getrow(),"cod_iva") ,getitemstring(getrow(),"des_iva"), getitemstring(getrow(),"des_estesa"), getitemnumber(getrow(),"aliquota"), 100) = -1 then
				g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
				return 1
			end if
		end if
		
		if isnull(getitemnumber(li_i,"perc_imponibile_soggetto")) then
			g_mb.messagebox("APICE","Attenzione! Impostare la percentuale soggetto ad un valore compreso fra 0 e 100 !!")
			return 1
		end if			
		
		if getitemnumber(li_i,"perc_imponibile_soggetto") <> 0 and not isnull(getitemnumber(li_i,"perc_imponibile_soggetto")) and isnull(getitemstring(li_i,"cod_iva_differenziato"))then
			g_mb.messagebox("APICE","Attenzione! Manca il codice iva differenziato")
			return 1
		end if			
		
		if getitemstring(li_i,"cod_iva_differenziato") = getitemstring(li_i,"cod_iva") then
			g_mb.messagebox("APICE","Attenzione! Il codice iva assoggettamento differenziato non può essere uguale al codice iva della riga")
			return 1
		end if
		
   next
end if

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

event pcd_rollback;call super::pcd_rollback;if s_cs_xx.parametri.impresa then
	rollback using sqlci;
end if
end event

event pcd_commit;call super::pcd_commit;if s_cs_xx.parametri.impresa then
	commit using sqlci;
end if
end event

type cb_lingue from commandbutton within w_ive
integer x = 3566
integer y = 1348
integer width = 375
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_ive_lingue, -1, dw_ive)

end on

