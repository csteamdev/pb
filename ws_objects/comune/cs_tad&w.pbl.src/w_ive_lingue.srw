﻿$PBExportHeader$w_ive_lingue.srw
$PBExportComments$Finestra Gestione Ive Lingue
forward
global type w_ive_lingue from w_cs_xx_principale
end type
type dw_ive_lingue from uo_cs_xx_dw within w_ive_lingue
end type
end forward

global type w_ive_lingue from w_cs_xx_principale
integer width = 4069
integer height = 1148
string title = "Gestione Ive Lingue"
dw_ive_lingue dw_ive_lingue
end type
global w_ive_lingue w_ive_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ive_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_ive_lingue.set_dw_key("cod_azienda")
dw_ive_lingue.set_dw_key("cod_iva")
dw_ive_lingue.set_dw_options(sqlca, &
                             i_openparm, &
                             c_scrollparent, &
                             c_default)

end on

on w_ive_lingue.create
int iCurrent
call super::create
this.dw_ive_lingue=create dw_ive_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ive_lingue
end on

on w_ive_lingue.destroy
call super::destroy
destroy(this.dw_ive_lingue)
end on

type dw_ive_lingue from uo_cs_xx_dw within w_ive_lingue
integer x = 23
integer y = 20
integer width = 3977
integer height = 1000
string dataobject = "d_ive_lingue"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_iva

ls_cod_iva = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_iva")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_iva)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_iva

ls_cod_iva = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_iva")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_iva")) then
      this.setitem(ll_i, "cod_iva", ls_cod_iva)
   end if
next
end on

