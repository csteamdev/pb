﻿$PBExportHeader$w_lingue.srw
$PBExportComments$Finestra Gestione Lingue
forward
global type w_lingue from w_cs_xx_principale
end type
type dw_lingue from uo_cs_xx_dw within w_lingue
end type
end forward

global type w_lingue from w_cs_xx_principale
integer width = 2533
integer height = 1148
string title = "Gestione Lingue"
dw_lingue dw_lingue
end type
global w_lingue w_lingue

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_lingue.set_dw_key("cod_azienda")
dw_lingue.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_default, &
                         c_default)

end on

on w_lingue.create
int iCurrent
call super::create
this.dw_lingue=create dw_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lingue
end on

on w_lingue.destroy
call super::destroy
destroy(this.dw_lingue)
end on

type dw_lingue from uo_cs_xx_dw within w_lingue
integer x = 23
integer y = 20
integer width = 2446
integer height = 1000
string dataobject = "d_lingue"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

