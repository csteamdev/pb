﻿$PBExportHeader$w_mezzi.srw
$PBExportComments$Finestra Gestione Mezzi di Trasporto
forward
global type w_mezzi from w_cs_xx_principale
end type
type dw_mezzi from uo_cs_xx_dw within w_mezzi
end type
type cb_lingue from commandbutton within w_mezzi
end type
end forward

global type w_mezzi from w_cs_xx_principale
integer width = 4265
integer height = 1244
string title = "Gestione Mezzi di Spedizione"
dw_mezzi dw_mezzi
cb_lingue cb_lingue
end type
global w_mezzi w_mezzi

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_mezzi.set_dw_key("cod_azienda")
dw_mezzi.set_dw_options(sqlca, &
                        pcca.null_object, &
                        c_default, &
                        c_default)
cb_lingue.enabled=false
end on

on w_mezzi.create
int iCurrent
call super::create
this.dw_mezzi=create dw_mezzi
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_mezzi
this.Control[iCurrent+2]=this.cb_lingue
end on

on w_mezzi.destroy
call super::destroy
destroy(this.dw_mezzi)
destroy(this.cb_lingue)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_mezzi, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_mezzi, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
if s_cs_xx.parametri.impresa then
	
	try
		f_po_loaddddw_dw(dw_mezzi, &
							  "codice_impresa", &
							  sqlci, &
							  "trasporto_intra", &
							  "id_trasporto_intra", &
							  "descrizione", &
							  "")
	catch (runtimeerror e)
	end try
end if

end event

type dw_mezzi from uo_cs_xx_dw within w_mezzi
integer x = 23
integer y = 20
integer width = 4165
integer height = 1000
integer taborder = 10
string dataobject = "d_mezzi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_mezzo

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_mezzi_lingue"
      ls_codice  = "cod_mezzo"
      ls_cod_mezzo = this.getitemstring(li_i, "cod_mezzo", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_mezzo)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

type cb_lingue from commandbutton within w_mezzi
integer x = 3813
integer y = 1040
integer width = 375
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_mezzi_lingue, -1, dw_mezzi)

end on

