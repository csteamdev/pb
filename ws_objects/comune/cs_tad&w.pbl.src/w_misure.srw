﻿$PBExportHeader$w_misure.srw
$PBExportComments$Finestra Gestione Misure
forward
global type w_misure from w_cs_xx_principale
end type
type dw_misure from uo_cs_xx_dw within w_misure
end type
type cb_lingue from commandbutton within w_misure
end type
end forward

global type w_misure from w_cs_xx_principale
integer width = 2962
integer height = 1244
string title = "Gestione Unità di Misura"
dw_misure dw_misure
cb_lingue cb_lingue
end type
global w_misure w_misure

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_misure.set_dw_key("cod_azienda")
dw_misure.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_default, &
                         c_default)
cb_lingue.enabled=false
end on

on w_misure.create
int iCurrent
call super::create
this.dw_misure=create dw_misure
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_misure
this.Control[iCurrent+2]=this.cb_lingue
end on

on w_misure.destroy
call super::destroy
destroy(this.dw_misure)
destroy(this.cb_lingue)
end on

type dw_misure from uo_cs_xx_dw within w_misure
integer x = 23
integer y = 20
integer width = 2880
integer height = 1000
integer taborder = 10
string dataobject = "d_misure"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_misura

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_misure_lingue"
      ls_codice  = "cod_misura"
      ls_cod_misura = this.getitemstring(li_i, "cod_misura", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_misura)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

type cb_lingue from commandbutton within w_misure
integer x = 2537
integer y = 1040
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_misure_lingue, -1, dw_misure)

end on

