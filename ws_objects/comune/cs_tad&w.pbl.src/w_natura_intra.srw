﻿$PBExportHeader$w_natura_intra.srw
$PBExportComments$Finestra Gestione Mezzi di Trasporto
forward
global type w_natura_intra from w_cs_xx_principale
end type
type dw_natura_intra from uo_cs_xx_dw within w_natura_intra
end type
end forward

global type w_natura_intra from w_cs_xx_principale
integer width = 3927
integer height = 1776
string title = "Gestione Natura INTRA"
dw_natura_intra dw_natura_intra
end type
global w_natura_intra w_natura_intra

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_natura_intra.set_dw_key("cod_azienda")
dw_natura_intra.set_dw_options(sqlca, &
                        pcca.null_object, &
                        c_default, &
                        c_default)

end event

on w_natura_intra.create
int iCurrent
call super::create
this.dw_natura_intra=create dw_natura_intra
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_natura_intra
end on

on w_natura_intra.destroy
call super::destroy
destroy(this.dw_natura_intra)
end on

event pc_setddlb;call super::pc_setddlb;					  
if s_cs_xx.parametri.impresa then
	
	try
		f_po_loaddddw_dw(dw_natura_intra, &
							  "id_impresa", &
							  sqlci, &
							  "natura_intra", &
							  "id_natura_intra", &
							  "descrizione", &
							  "")
	catch (runtimeerror e)
	end try
end if

end event

type dw_natura_intra from uo_cs_xx_dw within w_natura_intra
integer x = 23
integer y = 20
integer width = 3840
integer height = 1632
integer taborder = 10
string dataobject = "d_natura_intra"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

