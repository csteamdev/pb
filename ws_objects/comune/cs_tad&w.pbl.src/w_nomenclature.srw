﻿$PBExportHeader$w_nomenclature.srw
$PBExportComments$Finestra Gestione Nomenclature
forward
global type w_nomenclature from w_cs_xx_principale
end type
type dw_nomenclature from uo_cs_xx_dw within w_nomenclature
end type
end forward

global type w_nomenclature from w_cs_xx_principale
int Width=2314
int Height=1137
boolean TitleBar=true
string Title="Gestione Nomenclature Combinate"
dw_nomenclature dw_nomenclature
end type
global w_nomenclature w_nomenclature

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_nomenclature.set_dw_key("cod_azienda")
dw_nomenclature.set_dw_options(sqlca, &
                       			 pcca.null_object, &
										 c_default, &
										 c_default)
end event

on w_nomenclature.create
int iCurrent
call w_cs_xx_principale::create
this.dw_nomenclature=create dw_nomenclature
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_nomenclature
end on

on w_nomenclature.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_nomenclature)
end on

type dw_nomenclature from uo_cs_xx_dw within w_nomenclature
int X=23
int Y=21
int Width=2241
int Height=1001
int TabOrder=10
string DataObject="d_nomenclature"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

