﻿$PBExportHeader$w_numeratori.srw
$PBExportComments$Finestra Gestione Numeratori
forward
global type w_numeratori from w_cs_xx_principale
end type
type dw_numeratori from uo_cs_xx_dw within w_numeratori
end type
end forward

global type w_numeratori from w_cs_xx_principale
integer width = 3150
integer height = 1180
string title = "Gestione Numeratori"
dw_numeratori dw_numeratori
end type
global w_numeratori w_numeratori

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_numeratori, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_numeratori.set_dw_key("cod_azienda")
dw_numeratori.ib_proteggi_chiavi = false
dw_numeratori.set_dw_options(sqlca, &
                             pcca.null_object, &
                             c_default, &
                             c_default)

end event

on w_numeratori.create
int iCurrent
call super::create
this.dw_numeratori=create dw_numeratori
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_numeratori
end on

on w_numeratori.destroy
call super::destroy
destroy(this.dw_numeratori)
end on

type dw_numeratori from uo_cs_xx_dw within w_numeratori
integer x = 14
integer y = 12
integer width = 3063
integer height = 1032
integer taborder = 11
string dataobject = "d_numeratori"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event updatestart;call super::updatestart;integer li_i
string ls_cod_documento, ls_numeratore_documento


for li_i = 1 to rowcount(this)
	ls_cod_documento = this.getitemstring(li_i, "cod_documento")
	ls_numeratore_documento = this.getitemstring(li_i, "numeratore_documento")

	if not f_numeratore(ls_numeratore_documento, ls_cod_documento) then
		g_mb.messagebox("Attenzione", "Numeratore non corretto.", exclamation!)
		return 1
	end if
next

end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

