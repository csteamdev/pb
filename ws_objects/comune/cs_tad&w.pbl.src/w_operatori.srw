﻿$PBExportHeader$w_operatori.srw
$PBExportComments$Finestra Gestione Operatori
forward
global type w_operatori from w_cs_xx_principale
end type
type dw_operatori from uo_cs_xx_dw within w_operatori
end type
type cb_lingue from commandbutton within w_operatori
end type
end forward

global type w_operatori from w_cs_xx_principale
int Width=2163
int Height=1249
boolean TitleBar=true
string Title="Gestione Operatori"
dw_operatori dw_operatori
cb_lingue cb_lingue
end type
global w_operatori w_operatori

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_operatori.set_dw_key("cod_azienda")
dw_operatori.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_default, &
                            c_default)
cb_lingue.enabled=false
end on

on w_operatori.create
int iCurrent
call w_cs_xx_principale::create
this.dw_operatori=create dw_operatori
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_operatori
this.Control[iCurrent+2]=cb_lingue
end on

on w_operatori.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_operatori)
destroy(this.cb_lingue)
end on

type dw_operatori from uo_cs_xx_dw within w_operatori
int X=23
int Y=21
int Width=2081
int Height=1001
int TabOrder=10
string DataObject="d_operatori"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_operatore

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_operatori_lingue"
      ls_codice  = "cod_operatore"
      ls_cod_operatore = this.getitemstring(li_i, "cod_operatore", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_operatore)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

type cb_lingue from commandbutton within w_operatori
int X=1738
int Y=1041
int Width=366
int Height=81
int TabOrder=20
string Text="&Lingue"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_operatori_lingue, -1, dw_operatori)

end on

