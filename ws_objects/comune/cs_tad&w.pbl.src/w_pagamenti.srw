﻿$PBExportHeader$w_pagamenti.srw
$PBExportComments$Finestra Gestione Pagamenti
forward
global type w_pagamenti from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_pagamenti
end type
type cb_simulatore from commandbutton within w_pagamenti
end type
type cb_rate from commandbutton within w_pagamenti
end type
type dw_pagamenti_lista from uo_cs_xx_dw within w_pagamenti
end type
type cb_lingue from commandbutton within w_pagamenti
end type
type dw_pagamenti_det_1 from uo_cs_xx_dw within w_pagamenti
end type
type cb_valute from commandbutton within w_pagamenti
end type
end forward

global type w_pagamenti from w_cs_xx_principale
integer width = 2683
integer height = 1880
string title = "Pagamenti"
cb_1 cb_1
cb_simulatore cb_simulatore
cb_rate cb_rate
dw_pagamenti_lista dw_pagamenti_lista
cb_lingue cb_lingue
dw_pagamenti_det_1 dw_pagamenti_det_1
cb_valute cb_valute
end type
global w_pagamenti w_pagamenti

type variables

end variables

on pc_delete;call w_cs_xx_principale::pc_delete;cb_lingue.enabled=false
end on

event pc_setwindow;call super::pc_setwindow;string ls_dsn, ls_azienda_cont
windowobject lw_oggetti[]


dw_pagamenti_lista.set_dw_key("cod_azienda")
dw_pagamenti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_pagamenti_det_1.set_dw_options(sqlca, &
                                  dw_pagamenti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main=dw_pagamenti_lista
cb_lingue.enabled=false

if s_cs_xx.admin then
	dw_pagamenti_det_1.object.codice_impresa.visible = 1
	dw_pagamenti_det_1.object.cf_codice_impresa.visible = 1
	dw_pagamenti_det_1.object.codice_impresa_t.visible = 1
	dw_pagamenti_det_1.object.codice_tipo_pag_impresa.visible = 1
	dw_pagamenti_det_1.object.cf_codice_tipo_pag_impresa.visible = 1
	dw_pagamenti_det_1.object.codice_tipo_pag_impresa_t.visible = 1
	cb_1.visible = true
else
	dw_pagamenti_det_1.object.codice_impresa.visible = 0
	dw_pagamenti_det_1.object.cf_codice_impresa.visible = 0
	dw_pagamenti_det_1.object.codice_impresa_t.visible = 0
	dw_pagamenti_det_1.object.codice_tipo_pag_impresa.visible = 0
	dw_pagamenti_det_1.object.cf_codice_tipo_pag_impresa.visible = 0
	dw_pagamenti_det_1.object.codice_tipo_pag_impresa_t.visible = 0
	cb_1.visible = false
end if
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_pagamenti_det_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda +"'")

if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_pagamenti_det_1, &
						  "codice_impresa", &
						  sqlci, &
						  "con_pagamento", &
						  "codice", &
						  "descrizione", &
						  "")
	f_po_loaddddw_dw(dw_pagamenti_det_1, &
						  "codice_tipo_pag_impresa", &
						  sqlci, &
						  "tipo_pagamento", &
						  "codice", &
						  "descrizione", &
						  "")


end if
end event

on w_pagamenti.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_simulatore=create cb_simulatore
this.cb_rate=create cb_rate
this.dw_pagamenti_lista=create dw_pagamenti_lista
this.cb_lingue=create cb_lingue
this.dw_pagamenti_det_1=create dw_pagamenti_det_1
this.cb_valute=create cb_valute
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_simulatore
this.Control[iCurrent+3]=this.cb_rate
this.Control[iCurrent+4]=this.dw_pagamenti_lista
this.Control[iCurrent+5]=this.cb_lingue
this.Control[iCurrent+6]=this.dw_pagamenti_det_1
this.Control[iCurrent+7]=this.cb_valute
end on

on w_pagamenti.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_simulatore)
destroy(this.cb_rate)
destroy(this.dw_pagamenti_lista)
destroy(this.cb_lingue)
destroy(this.dw_pagamenti_det_1)
destroy(this.cb_valute)
end on

type cb_1 from commandbutton within w_pagamenti
integer x = 2263
integer y = 420
integer width = 343
integer height = 80
integer taborder = 31
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Agg.Impresa"
end type

event clicked;string ls_cod_pagamento, ls_errore
long ll_i, ll_ret
uo_impresa luo_impresa

for ll_i = 1 to dw_pagamenti_lista.rowcount()

	ls_cod_pagamento = dw_pagamenti_lista.getitemstring(ll_i, "cod_pagamento")
	
	luo_impresa = create uo_impresa
	ll_ret = luo_impresa.uof_con_pagamento(ls_cod_pagamento, "", ls_errore)
	
	
	//se torna <0 allora c'è stato un errore (rollback in IMPRESA già fatto nell'oggetto)
	//se invece torna 1 allora il commit in IMPRESA è stato fatto
	//se torna ZERO allora probabilmente l'utente ha annullato di proposito l'operazione
	if ll_ret < 0 then
		destroy luo_impresa
		rollback using sqlci;
		g_mb.error(ls_errore)
		return 1
	end if
	
	ll_ret = luo_impresa.uof_app_pagamento(ls_cod_pagamento, "", ls_errore)
	
	//se torna <0 allora c'è stato un errore (rollback in IMPRESA già fatto nell'oggetto)
	//se invece torna ZERO allora il commit in IMPRESA è stato fatto
	if ll_ret < 0 then
		rollback using sqlci;
		destroy luo_impresa
		g_mb.error(ls_errore)
		return 1
	end if
	
	
	commit using sqlci;
	destroy luo_impresa
next
end event

type cb_simulatore from commandbutton within w_pagamenti
integer x = 2254
integer y = 316
integer width = 370
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Simulatore"
end type

event clicked;long ll_row


ll_row = dw_pagamenti_lista.getrow()

if not isnull(ll_row) and ll_row > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_pagamenti_lista.getitemstring(ll_row,"cod_pagamento")
else
	setnull(s_cs_xx.parametri.parametro_s_1)
end if

window_open(w_simulatore_rate, -1)
end event

type cb_rate from commandbutton within w_pagamenti
integer x = 2254
integer y = 216
integer width = 370
integer height = 80
integer taborder = 21
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Rate"
end type

event clicked;window_open_parm(w_pagamenti_rate, -1, dw_pagamenti_lista)
end event

type dw_pagamenti_lista from uo_cs_xx_dw within w_pagamenti
integer x = 23
integer y = 20
integer width = 2203
integer height = 500
integer taborder = 10
string dataobject = "d_pagamenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_pagamento")) then
      cb_lingue.enabled = true
   else
      cb_lingue.enabled = false
   end if
end if
end on

event updatestart;call super::updatestart;if i_extendmode then
	long li_i
	string ls_tabella, ls_codice, ls_cod_pagamento
	
	for li_i = 1 to rowcount()
		if (getitemstatus(li_i, 0, primary!) = newmodified! OR getitemstatus(li_i, 0, primary!) = datamodified!) and s_cs_xx.parametri.impresa 	then     
			/// nuovo o modificato + gestione IMPRESA
				
			ls_cod_pagamento = getitemstring(li_i, "cod_pagamento")
			
			//in codice_impresa metto lo stesso codice di APICE
			//setitem(li_i, "codice_impresa", ls_cod_pagamento)
			//accepttext()
		end if
	next
	
	for li_i = 1 to this.deletedcount()
		ls_tabella = "tab_pagamenti_lingue"
		ls_codice  = "cod_pagamento"
		ls_cod_pagamento = this.getitemstring(li_i, "cod_pagamento", delete!, true)
		f_del_lingue(ls_tabella, ls_codice, ls_cod_pagamento)
	next
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_pagamento")) then
      cb_lingue.enabled = true
   else
      cb_lingue.enabled = false
   end if
end if
end on

event updateend;call super::updateend;long			li_i, ll_ret
string			ls_cod_pagamento, ls_errore
uo_impresa	luo_impresa

//Donato 12/12/2011
//gestione import automatico delle modifiche in IMPRESA ----------------------------------------------------------------------------------------------
for li_i = 1 to rowcount()
	
	if getitemstatus(li_i, 0, primary!) = newmodified! OR getitemstatus(li_i, 0, primary!) = datamodified! then      /// nuovo o modificato
		
		//se IMPRESA è abilitato ##############################
		if s_cs_xx.parametri.impresa then
			
			ls_cod_pagamento = getitemstring(li_i, "cod_pagamento")
			
			luo_impresa = create uo_impresa
			ll_ret = luo_impresa.uof_con_pagamento(ls_cod_pagamento, "", ls_errore)
			
			
			//se torna <0 allora c'è stato un errore (rollback in IMPRESA già fatto nell'oggetto)
			//se invece torna 1 allora il commit in IMPRESA è stato fatto
			//se torna ZERO allora probabilmente l'utente ha annullato di proposito l'operazione
			if ll_ret < 0 then
				destroy luo_impresa
				rollback using sqlci;
				g_mb.error(ls_errore)
				return 1
			end if
			
			commit using sqlci;
			destroy luo_impresa
			
			//se arrivi fin qui refresh del menu dropdown
			f_po_loaddddw_dw(dw_pagamenti_det_1, &
						  "codice_impresa", &
						  sqlci, &
						  "con_pagamento", &
						  "codice", &
						  "descrizione", &
						  "")
			
		end if
		//#############################################
		
	end if
	
next
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
end event

type cb_lingue from commandbutton within w_pagamenti
integer x = 2254
integer y = 16
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_pagamenti_lingue, -1, dw_pagamenti_lista)

end on

type dw_pagamenti_det_1 from uo_cs_xx_dw within w_pagamenti
integer x = 23
integer y = 540
integer width = 2606
integer height = 1220
integer taborder = 20
string dataobject = "d_pagamenti_det_1"
borderstyle borderstyle = styleraised!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   dw_pagamenti_lista.postevent(updatestart!)
end if
end on

event itemchanged;call super::itemchanged;if i_extendmode then
   choose case i_colname
      case "num_rate_com"
         if i_coltext = "1" then
            this.setitem(i_rownbr, "perc_prima_rata", 100)
            this.setitem(i_rownbr, "perc_ult_rata", 0)
            this.setitem(i_rownbr, "num_gior_int_rata", 0)
            this.setitem(i_rownbr, "num_gior_ult_rata", 0)
         end if
      case "perc_prima_rata"
         if this.getitemnumber(i_rownbr, "num_rate_com") = 2 then
            this.setitem(i_rownbr, "perc_ult_rata", 100 - double(i_coltext))
         end if

         if this.getitemnumber(i_rownbr, "num_rate_com") = 1 then
            this.setitem(i_rownbr, "flag_iva_prima_rata", "N")
         end if
      case "flag_iva_prima_rata"
         if this.getitemnumber(i_rownbr, "num_rate_com") = 1 and i_coltext = "S" then
				g_mb.messagebox("Apice","Non ha senso indicare SOLO IVA con una unica scadenza",Information!)
            this.setitem(i_rownbr, "flag_iva_prima_rata", "N")
         end if
		case "flag_calcolo_trasporto"
			if i_coltext = "S" then
				this.object.flag_contrassegno.tabsequence = 250
			else
				this.setitem(i_rownbr, "flag_contrassegno", "N")
				this.object.flag_contrassegno.tabsequence = 0
			end if
   end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;//double ld_totale_perc
//
//
//if i_rownbr > 0 then
//   if i_insave > 0 then
//      if (this.getitemnumber(i_rownbr, "perc_prima_rata") + &
//          this.getitemnumber(i_rownbr, "perc_ult_rata")) > 100 then
//      	messagebox("Attenzione", "La Prima o l' Ultima Rata hanno le % inesatte!", &
//                    exclamation!, ok!)
//         pcca.error = c_fatal
//         return
//      end if
//
//      if this.getitemnumber(i_rownbr, "perc_prima_rata") = 0 and &
//         this.getitemstring(i_rownbr, "flag_iva_prima_rata") <> "S" then
//      	messagebox("Attenzione", "La % della Prima Rata deve essere maggiore di 0!", &
//                    exclamation!, ok!)
//         pcca.error = c_fatal
//         return
//      end if
//
//      if this.getitemnumber(i_rownbr, "num_rate_com") = 1 then
//         this.setitem(i_rownbr, "perc_prima_rata", 100)
//         this.setitem(i_rownbr, "flag_iva_prima_rata", "N")
//         this.setitem(i_rownbr, "num_gior_int_rata", 0)
//         this.setitem(i_rownbr, "perc_ult_rata", 0)
//         this.setitem(i_rownbr, "num_gior_ult_rata", 0)
//      end if
//
//      if this.getitemnumber(i_rownbr, "num_rate_com") = 2 then
//         if this.getitemnumber(i_rownbr, "perc_prima_rata") = 100 or &
//            (this.getitemnumber(i_rownbr, "perc_prima_rata") + &
//             this.getitemnumber(i_rownbr, "perc_ult_rata") > 100) then
//           	messagebox("Attenzione", "La Prima o l'Ultima Rata hanno le % inesatte!", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         if this.getitemnumber(i_rownbr, "num_gior_ult_rata") = 0 then
//           	messagebox("Attenzione", "I giorni dell' Ultima Rata devono essere maggiori di 0!", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         this.setitem(i_rownbr, "num_gior_int_rata", 0)
//      end if
//
////      if (this.getitemnumber(i_rownbr, "num_rate_com") = 3) and &
////         (this.getitemnumber(i_rownbr, "perc_prima_rata") + this.getitemnumber(i_rownbr, "perc_ult_rata") < 100) and &
////			(this.getitemstring(i_rownbr, "flag_iva_prima_rata") = "S" ) then
////           	messagebox("Attenzione", "Attenzione! Hai scelto iva su prima rata, tre rate, e % prima rata+ %ultima rata < 100%: necessitano 4 rate", exclamation!, ok!)
////            pcca.error = c_fatal
////            return
////		end if
//   
//	
//      if this.getitemnumber(i_rownbr, "num_rate_com") > 2 then
//         if this.getitemnumber(i_rownbr, "perc_prima_rata") + &
//            this.getitemnumber(i_rownbr, "perc_ult_rata") = 100 and &
//				this.getitemstring(i_rownbr, "flag_iva_prima_rata") <> "S" then
//           	messagebox("Attenzione", "La Prima o l'Ultima Rata hanno le % inesatte!", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         if this.getitemnumber(i_rownbr, "num_gior_int_rata") = 0 then
//           	messagebox("Attenzione", "I giorni delle Rate intermedie sono inesatti!", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//         if this.getitemnumber(i_rownbr, "perc_ult_rata") <> 0 and &
//            this.getitemnumber(i_rownbr, "num_gior_ult_rata") = 0 then
//           	messagebox("Attenzione", "I dati dell'Ultima Rata sono Incongruenti!", &
//                       exclamation!, ok!)
//            pcca.error = c_fatal
//            return
//         end if
//      end if
//   
//      if this.getitemnumber(i_rownbr, "perc_ult_rata") = 0 then
//         this.setitem(i_rownbr, "num_gior_ult_rata", 0)
//      end if
//   end if
//end if
//
end event

type cb_valute from commandbutton within w_pagamenti
integer x = 2254
integer y = 116
integer width = 370
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Spese Val."
end type

event clicked;window_open_parm(w_pagamenti_valute, -1, dw_pagamenti_lista)

end event

