﻿$PBExportHeader$w_pagamenti_rate.srw
forward
global type w_pagamenti_rate from w_cs_xx_principale
end type
type dw_pagamenti_rate from uo_cs_xx_dw within w_pagamenti_rate
end type
end forward

global type w_pagamenti_rate from w_cs_xx_principale
integer width = 4901
integer height = 1400
string title = "Configurazione Rate - Pagamento"
boolean maxbox = false
dw_pagamenti_rate dw_pagamenti_rate
end type
global w_pagamenti_rate w_pagamenti_rate

type variables
string is_cod_pagamento
end variables

on w_pagamenti_rate.create
int iCurrent
call super::create
this.dw_pagamenti_rate=create dw_pagamenti_rate
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pagamenti_rate
end on

on w_pagamenti_rate.destroy
call super::destroy
destroy(this.dw_pagamenti_rate)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_pagamenti_rate

dw_pagamenti_rate.setfocus()

dw_pagamenti_rate.change_dw_current()

dw_pagamenti_rate.set_dw_key("cod_azienda")

dw_pagamenti_rate.set_dw_key("cod_pagamento")

dw_pagamenti_rate.set_dw_key("prog_rata")

dw_pagamenti_rate.set_dw_options(sqlca, &
											i_openparm, &
											c_scrollparent, &
											c_nohighlightselected)
end event

event pc_setddlb;call super::pc_setddlb;if isvalid(sqlci) then
	f_po_loaddddw_dw(dw_pagamenti_rate,"cod_tipo_pagamento",sqlci,"tipo_pagamento","codice","descrizione","")
else
	f_po_loaddddw_dw(dw_pagamenti_rate,"cod_tipo_pagamento",sqlca,"tab_tipi_pagamenti", &
						 "cod_tipo_pagamento","des_tipo_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if
f_po_loaddddw_dw(dw_pagamenti_rate,"fatel_modalita_pagamento",sqlca,"tab_pagamenti_modalita", &
						 "cod_modalita_pagamento","des_modalita_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event closequery;call super::closequery;string ls_cod_pagamento
long ll_num_rate

ls_cod_pagamento = dw_pagamenti_rate.i_parentdw.getitemstring(dw_pagamenti_rate.i_parentdw.i_selectedrows[1], "cod_pagamento")

select sum(numero_rate)
into   :ll_num_rate
from   tab_pagamenti_rate
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;
		
if ll_num_rate >= 0 then
	update tab_pagamenti
	set    num_rate_com = :ll_num_rate
	where  cod_azienda = :s_cs_xx.cod_azienda and
          cod_pagamento = :ls_cod_pagamento;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento totale rate pagamento. "+sqlca.sqlerrtext)
		return
	end if
end if
end event

type dw_pagamenti_rate from uo_cs_xx_dw within w_pagamenti_rate
integer x = 23
integer y = 20
integer width = 4823
integer height = 1260
integer taborder = 10
string dataobject = "d_pagamenti_rate"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;is_cod_pagamento = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_pagamento")

parent.title = "Configurazione Rate - Pagamento " + is_cod_pagamento

retrieve(s_cs_xx.cod_azienda,is_cod_pagamento)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	
	if isnull(getitemstring(getrow(),"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemstring(getrow(),"cod_pagamento")) then
		setitem(ll_i,"cod_pagamento",is_cod_pagamento)
	end if
	
next
end event

event pcd_new;call super::pcd_new;long ll_prog_rata, ll_ordine_rata, ll_null


select max(prog_rata),
		 max(ordine_rata)
into   :ll_prog_rata,
		 :ll_ordine_rata
from   tab_pagamenti_rate
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_pagamento = :is_cod_pagamento;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura massimo progressivo rata da tab_pagamenti_rate: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then
	ll_prog_rata = 0
	ll_ordine_rata = 0
end if

if isnull(ll_prog_rata) then
	ll_prog_rata = 0
end if

if isnull(ll_ordine_rata) then
	ll_ordine_rata = 0
end if

ll_prog_rata ++

ll_ordine_rata ++

setnull(ll_null)

setitem(getrow(),"prog_rata",ll_prog_rata)

setitem(getrow(),"ordine_rata",ll_ordine_rata)

setitem(getrow(),"giorni_decorrenza",0)

setitem(getrow(),"numero_rate",1)

setitem(getrow(),"giorni_tra_rate",0)

setitem(getrow(),"tipo_scadenza","M")

setitem(getrow(),"giorno_fisso",ll_null)

setitem(getrow(),"perc_imponibile",0)

setitem(getrow(),"perc_iva",0)
end event

event ue_key;call super::ue_key;if getrow() = rowcount() and getcolumnname() = "cod_tipo_pagamento" and key = keyTab! then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if
end event

event updatestart;call super::updatestart;long			ll_i,ll_ret
string			ls_cod_pagamento, ls_errore
dec{4}		ld_iva, ld_imp, ld_tot_iva, ld_tot_imp
uo_impresa	luo_impresa
boolean		lb_aggiorna_impresa=false

ld_tot_iva = 0

ld_tot_imp = 0

for ll_i = 1 to rowcount()
	
	if getitemnumber(ll_i,"numero_rate") < 1 then
		g_mb.messagebox("APICE","Impostare il numero di rate per la riga numero " + string(ll_i),exclamation!)
		return 1
	end if
	
	if getitemstring(ll_i,"tipo_scadenza") = "F" and &
		(getitemnumber(ll_i,"giorno_fisso") = 0  or isnull(getitemnumber(ll_i,"giorno_fisso"))) then
		g_mb.messagebox("APICE","Impostare il giorno fisso per la riga numero " + string(ll_i),exclamation!)
		return 1
	end if
	
	setitem(ll_i,"ordine_rata",ll_i)
	
	ld_iva = getitemnumber(ll_i,"perc_iva")
	
	if isnull(ld_iva) then
		ld_iva = 0
	end if
	
	ld_tot_iva = ld_tot_iva + ld_iva
	
	if ld_tot_iva > 100 then
		g_mb.messagebox("APICE","Totale IVA maggiore del 100%.~nControllare le rate e correggere le percentuali",exclamation!)
		return 1
	end if
	
	ld_imp = getitemnumber(ll_i,"perc_imponibile")
	
	if isnull(ld_imp) then
		ld_imp = 0
	end if
	
	ld_tot_imp = ld_tot_imp + ld_imp
	
	if ld_tot_imp > 100 then
		g_mb.messagebox("APICE","Totale Imponibile maggiore del 100%.~nControllare le rate e correggere le percentuali",exclamation!)
		return 1
	end if
next



end event

event updateend;call super::updateend;string			ls_cod_pagamento, ls_errore
long			ll_i, ll_ret
boolean		lb_aggiorna_impresa=false
uo_impresa	luo_impresa

//Donato 12/12/2011
//gestione import automatico delle modifiche in IMPRESA ----------------------------------------------------------------------------------------------
for ll_i = 1 to rowcount()
	if (getitemstatus(ll_i, 0, primary!) = newmodified! or &
			getitemstatus(ll_i, 0, primary!) = datamodified!) and &
				not lb_aggiorna_impresa and s_cs_xx.parametri.impresa		then      /// nuovo o modificato
	
		//se IMPRESA è abilitato ##############################
		lb_aggiorna_impresa = true
			
		//memorizzo il cod pagamento per cui aggiornare in IMPRESA, mi servirà dopo
		ls_cod_pagamento = getitemstring(ll_i, "cod_pagamento")
		//#############################################
		
	end if
next

if lb_aggiorna_impresa then
	
	if ls_cod_pagamento="" or isnull(ls_cod_pagamento) then
		g_mb.error("Cod. Pagamento non impostato!")
		return 1
	end if
			
	luo_impresa = create uo_impresa
	ll_ret = luo_impresa.uof_app_pagamento(ls_cod_pagamento, "", ls_errore)
	
	//se torna <0 allora c'è stato un errore (rollback in IMPRESA già fatto nell'oggetto)
	//se invece torna ZERO allora il commit in IMPRESA è stato fatto
	if ll_ret < 0 then
		rollback using sqlci;
		destroy luo_impresa
		g_mb.error(ls_errore)
		return 1
	end if
	
	commit using sqlci;
	destroy luo_impresa
	
end if
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
end event

