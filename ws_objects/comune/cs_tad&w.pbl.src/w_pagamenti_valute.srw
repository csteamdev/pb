﻿$PBExportHeader$w_pagamenti_valute.srw
$PBExportComments$Fienestra per indicazione spese bancarie in valuta
forward
global type w_pagamenti_valute from w_cs_xx_principale
end type
type dw_pagamenti_valute from uo_cs_xx_dw within w_pagamenti_valute
end type
end forward

global type w_pagamenti_valute from w_cs_xx_principale
integer width = 2624
integer height = 1148
string title = "Gestione Pagamenti Valute"
dw_pagamenti_valute dw_pagamenti_valute
end type
global w_pagamenti_valute w_pagamenti_valute

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_pagamenti_valute, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_pagamenti_valute.set_dw_key("cod_azienda")
dw_pagamenti_valute.set_dw_key("cod_pagamento")
dw_pagamenti_valute.set_dw_options(sqlca, &
                                   i_openparm, &
                                   c_scrollparent, &
                                   c_default)

end event

on w_pagamenti_valute.create
int iCurrent
call super::create
this.dw_pagamenti_valute=create dw_pagamenti_valute
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pagamenti_valute
end on

on w_pagamenti_valute.destroy
call super::destroy
destroy(this.dw_pagamenti_valute)
end on

type dw_pagamenti_valute from uo_cs_xx_dw within w_pagamenti_valute
integer x = 23
integer y = 20
integer width = 2537
integer height = 1000
string dataobject = "d_pagamenti_valute"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_pagamento


ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_pagamento)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_pagamento

ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_pagamento")) then
      this.setitem(ll_i, "cod_pagamento", ls_cod_pagamento)
   end if
next
end on

