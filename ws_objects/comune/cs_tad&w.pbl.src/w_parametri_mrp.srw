﻿$PBExportHeader$w_parametri_mrp.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_parametri_mrp from w_cs_xx_principale
end type
type dw_parametri_azienda_lista from uo_cs_xx_dw within w_parametri_mrp
end type
end forward

global type w_parametri_mrp from w_cs_xx_principale
integer width = 2757
integer height = 1644
string title = "Parametri MRP"
dw_parametri_azienda_lista dw_parametri_azienda_lista
end type
global w_parametri_mrp w_parametri_mrp

on w_parametri_mrp.create
int iCurrent
call super::create
this.dw_parametri_azienda_lista=create dw_parametri_azienda_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parametri_azienda_lista
end on

on w_parametri_mrp.destroy
call super::destroy
destroy(this.dw_parametri_azienda_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_modify

dw_parametri_azienda_lista.set_dw_key("cod_azienda")
dw_parametri_azienda_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)

end event

type dw_parametri_azienda_lista from uo_cs_xx_dw within w_parametri_mrp
integer x = 23
integer y = 20
integer width = 2674
integer height = 1500
integer taborder = 10
string dataobject = "d_parametri_mrp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	
	choose case dwo.name
			
		case "cod_parametro"
			if left(data,2) <> "MR" then
				g_mb.error("Attenzione: il codice deve iniziare con le lettere MR!")
				return 1
			end if
			
	end choose
	
end if
end event

