﻿$PBExportHeader$w_piano_conti.srw
$PBExportComments$Finestra Gestione Piano dei Conti
forward
global type w_piano_conti from w_cs_xx_principale
end type
type dw_piano_conti_lista from uo_cs_xx_dw within w_piano_conti
end type
type dw_piano_conti_det from uo_cs_xx_dw within w_piano_conti
end type
end forward

global type w_piano_conti from w_cs_xx_principale
integer width = 2085
integer height = 1492
string title = "Gestione Piano dei Conti"
dw_piano_conti_lista dw_piano_conti_lista
dw_piano_conti_det dw_piano_conti_det
end type
global w_piano_conti w_piano_conti

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_piano_conti_lista.set_dw_key("cod_azienda")
dw_piano_conti_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_default, &
                                    c_default)
dw_piano_conti_det.set_dw_options(sqlca, &
                                  dw_piano_conti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
iuo_dw_main = dw_piano_conti_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_piano_conti_det, &
                 "cod_tipo_conto", &
                 sqlca, &
                 "tab_tipi_conti", &
                 "cod_tipo_conto", &
                 "des_tipo_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_piano_conti_det, &
						  "conto_impresa", &
						  sqlci, &
						  "conto", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

on w_piano_conti.create
int iCurrent
call super::create
this.dw_piano_conti_lista=create dw_piano_conti_lista
this.dw_piano_conti_det=create dw_piano_conti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_piano_conti_lista
this.Control[iCurrent+2]=this.dw_piano_conti_det
end on

on w_piano_conti.destroy
call super::destroy
destroy(this.dw_piano_conti_lista)
destroy(this.dw_piano_conti_det)
end on

type dw_piano_conti_lista from uo_cs_xx_dw within w_piano_conti
integer x = 23
integer y = 20
integer width = 1989
integer height = 500
integer taborder = 10
string dataobject = "d_piano_conti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_piano_conti_det from uo_cs_xx_dw within w_piano_conti
integer x = 23
integer y = 540
integer width = 1998
integer height = 724
integer taborder = 20
string dataobject = "d_piano_conti_det"
borderstyle borderstyle = styleraised!
end type

