﻿$PBExportHeader$w_politiche_riordino.srw
$PBExportComments$Finestra Politiche Riordino
forward
global type w_politiche_riordino from w_cs_xx_principale
end type
type dw_politiche_riordino_lista from uo_cs_xx_dw within w_politiche_riordino
end type
type dw_politiche_riordino_det from uo_cs_xx_dw within w_politiche_riordino
end type
end forward

global type w_politiche_riordino from w_cs_xx_principale
integer width = 2894
integer height = 1940
string title = "Politiche Riordino"
dw_politiche_riordino_lista dw_politiche_riordino_lista
dw_politiche_riordino_det dw_politiche_riordino_det
end type
global w_politiche_riordino w_politiche_riordino

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_politiche_riordino_lista.set_dw_key("cod_azienda")
dw_politiche_riordino_lista.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)
dw_politiche_riordino_det.set_dw_options(sqlca, &
                                dw_politiche_riordino_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
iuo_dw_main = dw_politiche_riordino_lista
end event

on w_politiche_riordino.create
int iCurrent
call super::create
this.dw_politiche_riordino_lista=create dw_politiche_riordino_lista
this.dw_politiche_riordino_det=create dw_politiche_riordino_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_politiche_riordino_lista
this.Control[iCurrent+2]=this.dw_politiche_riordino_det
end on

on w_politiche_riordino.destroy
call super::destroy
destroy(this.dw_politiche_riordino_lista)
destroy(this.dw_politiche_riordino_det)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_politiche_riordino_det,"cod_tipo_commessa",sqlca,&
                 "tab_tipi_commessa","cod_tipo_commessa","des_tipo_commessa",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_politiche_riordino_lista from uo_cs_xx_dw within w_politiche_riordino
integer x = 23
integer y = 20
integer width = 2811
integer height = 500
integer taborder = 20
string dataobject = "d_politiche_riordino_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event getfocus;call super::getfocus;dw_politiche_riordino_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type dw_politiche_riordino_det from uo_cs_xx_dw within w_politiche_riordino
integer x = 23
integer y = 540
integer width = 2811
integer height = 1276
integer taborder = 10
string dataobject = "d_politiche_riordino_det"
borderstyle borderstyle = styleraised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_politiche_riordino_det,"cod_fornitore")
end choose

end event

event pcd_modify;call super::pcd_modify;dw_politiche_riordino_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_new;call super::pcd_new;dw_politiche_riordino_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;dw_politiche_riordino_det.object.b_ricerca_fornitore.enabled = false
end event

event pcd_delete;call super::pcd_delete;dw_politiche_riordino_det.object.b_ricerca_fornitore.enabled=false
end event

