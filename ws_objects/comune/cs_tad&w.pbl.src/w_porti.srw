﻿$PBExportHeader$w_porti.srw
$PBExportComments$Finestra Gestione Porti
forward
global type w_porti from w_cs_xx_principale
end type
type dw_porti from uo_cs_xx_dw within w_porti
end type
type cb_lingue from commandbutton within w_porti
end type
end forward

global type w_porti from w_cs_xx_principale
integer width = 4169
integer height = 2200
string title = "Gestione Porti"
dw_porti dw_porti
cb_lingue cb_lingue
end type
global w_porti w_porti

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_porti.set_dw_key("cod_azienda")
dw_porti.set_dw_options(sqlca, &
                        pcca.null_object, &
                        c_default, &
                        c_default)
cb_lingue.enabled=false
end on

on w_porti.create
int iCurrent
call super::create
this.dw_porti=create dw_porti
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_porti
this.Control[iCurrent+2]=this.cb_lingue
end on

on w_porti.destroy
call super::destroy
destroy(this.dw_porti)
destroy(this.cb_lingue)
end on

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	
	try
		f_po_loaddddw_dw(dw_porti, &
							  "id_impresa", &
							  sqlci, &
							  "cond_consegna_intra", &
							  "id_cond_consegna_intra", &
							  "descrizione", &
							  "")
	catch (runtimeerror e)
	end try
end if
end event

type dw_porti from uo_cs_xx_dw within w_porti
integer x = 23
integer y = 20
integer width = 4078
integer height = 1920
integer taborder = 10
string dataobject = "d_porti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_porto

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_porti_lingue"
      ls_codice  = "cod_porto"
      ls_cod_porto = this.getitemstring(li_i, "cod_porto", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_porto)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

type cb_lingue from commandbutton within w_porti
integer x = 3735
integer y = 1972
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_porti_lingue, -1, dw_porti)

end on

