﻿$PBExportHeader$w_porti_lingue.srw
$PBExportComments$Finestra Gestione Porti Lingue
forward
global type w_porti_lingue from w_cs_xx_principale
end type
type dw_porti_lingue from uo_cs_xx_dw within w_porti_lingue
end type
end forward

global type w_porti_lingue from w_cs_xx_principale
int Width=2277
int Height=1149
boolean TitleBar=true
string Title="Gestione Porti Lingue"
dw_porti_lingue dw_porti_lingue
end type
global w_porti_lingue w_porti_lingue

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_porti_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_porti_lingue.set_dw_key("cod_azienda")
dw_porti_lingue.set_dw_key("cod_porto")
dw_porti_lingue.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

end on

on w_porti_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_porti_lingue=create dw_porti_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_porti_lingue
end on

on w_porti_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_porti_lingue)
end on

type dw_porti_lingue from uo_cs_xx_dw within w_porti_lingue
int X=23
int Y=21
int Width=2195
int Height=1001
string DataObject="d_porti_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_porto


ls_cod_porto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_porto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_porto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_porto

ls_cod_porto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_porto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_porto")) then
      this.setitem(ll_i, "cod_porto", ls_cod_porto)
   end if
next
end on

