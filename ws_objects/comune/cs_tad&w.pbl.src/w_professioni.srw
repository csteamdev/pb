﻿$PBExportHeader$w_professioni.srw
forward
global type w_professioni from w_cs_xx_principale
end type
type dw_professioni from uo_cs_xx_dw within w_professioni
end type
type cb_lingue from commandbutton within w_professioni
end type
end forward

global type w_professioni from w_cs_xx_principale
int Width=2433
int Height=1241
boolean TitleBar=true
string Title="Gestioni Professioni"
dw_professioni dw_professioni
cb_lingue cb_lingue
end type
global w_professioni w_professioni

on w_professioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_professioni=create dw_professioni
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_professioni
this.Control[iCurrent+2]=cb_lingue
end on

on w_professioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_professioni)
destroy(this.cb_lingue)
end on

event pc_setwindow;call super::pc_setwindow;dw_professioni.set_dw_key("cod_azienda")
dw_professioni.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)

end event

type dw_professioni from uo_cs_xx_dw within w_professioni
int X=23
int Y=17
int Width=2346
int Height=1001
int TabOrder=1
string DataObject="d_professioni"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_professione

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_professioni_lingue"
      ls_codice  = "cod_professione"
      ls_cod_professione = this.getitemstring(li_i, "cod_professione", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_professione)
   next
end if
end event

event pcd_delete;call super::pcd_delete;cb_lingue.enabled=false
end event

event pcd_modify;call super::pcd_modify;cb_lingue.enabled=false
end event

event pcd_new;call super::pcd_new;cb_lingue.enabled=false
end event

event pcd_next;call super::pcd_next;cb_lingue.enabled=false
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end event

type cb_lingue from commandbutton within w_professioni
int X=2003
int Y=1041
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="&Lingue"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_professioni_lingue, -1, dw_professioni)
end event

