﻿$PBExportHeader$w_professioni_lingue.srw
forward
global type w_professioni_lingue from w_cs_xx_principale
end type
type dw_professioni_lingue from uo_cs_xx_dw within w_professioni_lingue
end type
end forward

global type w_professioni_lingue from w_cs_xx_principale
int Width=2291
int Height=1165
boolean TitleBar=true
string Title="Gestioni Professioni Lingue"
dw_professioni_lingue dw_professioni_lingue
end type
global w_professioni_lingue w_professioni_lingue

on w_professioni_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_professioni_lingue=create dw_professioni_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_professioni_lingue
end on

on w_professioni_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_professioni_lingue)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_professioni_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_professioni_lingue.set_dw_key("cod_azienda")
dw_professioni_lingue.set_dw_key("cod_resa")
dw_professioni_lingue.set_dw_options(sqlca, &
                              i_openparm, &
                              c_scrollparent, &
                              c_default)

end event

type dw_professioni_lingue from uo_cs_xx_dw within w_professioni_lingue
int X=23
int Y=29
int Width=2195
int Height=1001
int TabOrder=1
string DataObject="d_professioni_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_professione


ls_cod_professione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_professione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_professione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_professione

ls_cod_professione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_professione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_professione")) then
      this.setitem(ll_i, "cod_professione", ls_cod_professione)
   end if
next
end event

