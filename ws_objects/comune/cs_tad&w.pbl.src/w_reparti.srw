﻿$PBExportHeader$w_reparti.srw
$PBExportComments$Window Anagrafica Reparti
forward
global type w_reparti from w_cs_xx_principale
end type
type cb_documento from commandbutton within w_reparti
end type
type dw_reparti_lista from uo_cs_xx_dw within w_reparti
end type
type dw_reparti_det from uo_cs_xx_dw within w_reparti
end type
end forward

global type w_reparti from w_cs_xx_principale
integer x = 603
integer y = 324
integer width = 2094
integer height = 2556
string title = "Tabella Reparti"
event ue_seleziona_riga ( )
cb_documento cb_documento
dw_reparti_lista dw_reparti_lista
dw_reparti_det dw_reparti_det
end type
global w_reparti w_reparti

event ue_seleziona_riga();boolean lb_trovato
long    ll_i

if dw_reparti_lista.rowcount() < 1 then return
if isnull(s_cs_xx.parametri.parametro_s_12) or s_cs_xx.parametri.parametro_s_12 = "" then return

lb_trovato = false

for ll_i = 1 to dw_reparti_lista.rowcount()
	if dw_reparti_lista.getitemstring( ll_i, "cod_reparto") = s_cs_xx.parametri.parametro_s_12 then
		lb_trovato = true
		exit
	end if
next

if lb_trovato then
	dw_reparti_lista.setrow( ll_i)
	dw_reparti_lista.SelectRow( ll_i, TRUE)
end if

return
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_reparti_det,"cod_centro_costo",sqlca,&
                 "tab_centri_costo","cod_centro_costo","des_centro_costo", &
                 "tab_centri_costo.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_reparti_det,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_reparti_det,"cod_area_aziendale",sqlca,&
                 "tab_aree_aziendali","cod_area_aziendale","des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_reparti_det,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  


end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_reparti_lista.set_dw_key("cod_azienda")
dw_reparti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_reparti_det.set_dw_options(sqlca,dw_reparti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_reparti_lista
end on

on w_reparti.create
int iCurrent
call super::create
this.cb_documento=create cb_documento
this.dw_reparti_lista=create dw_reparti_lista
this.dw_reparti_det=create dw_reparti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_documento
this.Control[iCurrent+2]=this.dw_reparti_lista
this.Control[iCurrent+3]=this.dw_reparti_det
end on

on w_reparti.destroy
call super::destroy
destroy(this.cb_documento)
destroy(this.dw_reparti_lista)
destroy(this.dw_reparti_det)
end on

type cb_documento from commandbutton within w_reparti
integer x = 1669
integer y = 2356
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documenti"
end type

event clicked;dw_reparti_lista.accepttext()

if dw_reparti_lista.getrow() > 0 and not isnull(dw_reparti_lista.getrow()) then 
//	s_cs_xx.parametri.parametro_s_10 = dw_reparti_lista.getitemstring(dw_reparti_lista.getrow(), "cod_reparto")	
//	window_open_parm(w_reparti_blob, -1, dw_reparti_lista)
	s_cs_xx.parametri.parametro_s_1 = dw_reparti_lista.getitemstring(dw_reparti_lista.getrow(), "cod_reparto")	
	open(w_reparti_ole)
end if
end event

type dw_reparti_lista from uo_cs_xx_dw within w_reparti
integer x = 23
integer y = 20
integer width = 2011
integer height = 500
integer taborder = 10
string dataobject = "d_reparti_lista"
boolean vscrollbar = true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

event doubleclicked;call super::doubleclicked;//if row < 1 then return
//
//if s_cs_xx.admin then
//
//	str_des_multilingua lstr_des_multilingua
//	
//	lstr_des_multilingua.nome_tabella = "anag_reparti"
//	lstr_des_multilingua.descrizione_origine = getitemstring(row,"des_reparto")
//	lstr_des_multilingua.chiave_str_1 = getitemstring(row,"cod_reparto")
//	setnull(lstr_des_multilingua.chiave_str_2)
//	
//	if isnull(lstr_des_multilingua.descrizione_origine) or len(lstr_des_multilingua.descrizione_origine) < 1 then return
//	
//	openwithparm(w_des_multilingua, lstr_des_multilingua)
//
//end if
end event

type dw_reparti_det from uo_cs_xx_dw within w_reparti
integer x = 23
integer y = 540
integer width = 2011
integer height = 1796
integer taborder = 20
string dataobject = "d_reparti_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_nome, ls_cognome, ls_cod_qualifica, ls_indirizzo_residenza, ls_localita_residenza, ls_frazione_residenza, ls_cap_residenza, &
		 ls_provincia_residenza, ls_telefono, ls_fax, ls_cod_operaio, ls_des_qualifica

choose case i_colname
	case "cod_operaio"
		
		ls_cod_operaio = i_coltext
	   setnull(ls_nome)
	   setnull(ls_cognome)
	   setnull(ls_cod_qualifica)
	   setnull(ls_indirizzo_residenza)
	   setnull(ls_localita_residenza)
	   setnull(ls_frazione_residenza)
	   setnull(ls_cap_residenza)
	   setnull(ls_provincia_residenza)
	   setnull(ls_telefono)
	   setnull(ls_fax)		
		
		select nome,
				 cognome,
		       cod_qualifica,
				 indirizzo_residenza,
				 localita_residenza,
				 frazione_residenza,
				 cap_residenza,
				 provincia_residenza,
				 telefono,
				 fax
		  into :ls_nome,
		  		 :ls_cognome,
		       :ls_cod_qualifica,
				 :ls_indirizzo_residenza,
				 :ls_localita_residenza,
				 :ls_frazione_residenza,
				 :ls_cap_residenza,
				 :ls_provincia_residenza,
				 :ls_telefono,
				 :ls_fax
		  from anag_operai
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_operaio = :ls_cod_operaio;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in fase di lettura dati da tabella anag_operai " + sqlca.sqlerrtext)
			return
		end if
		
		if sqlca.sqlcode = 0 then 
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "nome_responsabile", ls_nome)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "cognome_responsabile", ls_cognome)
			
			select des_qualifica
			  into :ls_des_qualifica
			  from tab_qualifiche
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_qualifica = :ls_cod_qualifica;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in fase di lettura dati da tabella anag_operai " + sqlca.sqlerrtext)
				return
			end if			
			
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "qualifica_responsabile", ls_des_qualifica)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "indirizzo", ls_indirizzo_residenza)			
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "frazione", ls_frazione_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "localita", ls_localita_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "cap", ls_cap_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "provincia", ls_provincia_residenza)						
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "telefono", ls_telefono)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "fax", ls_fax)									
		end if	
						 
end choose
end event

