﻿$PBExportHeader$w_responsabili.srw
$PBExportComments$Finestra Gestione Responsabili
forward
global type w_responsabili from w_cs_xx_principale
end type
type dw_responsabili from uo_cs_xx_dw within w_responsabili
end type
type cb_lingue from commandbutton within w_responsabili
end type
end forward

global type w_responsabili from w_cs_xx_principale
int Width=2163
int Height=1237
boolean TitleBar=true
string Title="Gestione Responsabili"
dw_responsabili dw_responsabili
cb_lingue cb_lingue
end type
global w_responsabili w_responsabili

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_responsabili.set_dw_key("cod_azienda")
dw_responsabili.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
cb_lingue.enabled=false
end on

on w_responsabili.create
int iCurrent
call w_cs_xx_principale::create
this.dw_responsabili=create dw_responsabili
this.cb_lingue=create cb_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_responsabili
this.Control[iCurrent+2]=cb_lingue
end on

on w_responsabili.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_responsabili)
destroy(this.cb_lingue)
end on

type dw_responsabili from uo_cs_xx_dw within w_responsabili
int X=23
int Y=21
int Width=2081
int Height=1001
int TabOrder=10
string DataObject="d_responsabili"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_responsabile

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_responsabili_lingue"
      ls_codice  = "cod_responsabile"
      ls_cod_responsabile = this.getitemstring(li_i, "cod_responsabile", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_responsabile)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_delete;call uo_cs_xx_dw::pcd_delete;cb_lingue.enabled=false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 then
      cb_lingue.enabled=true
   else
      cb_lingue.enabled=false
   end if
end if

end on

type cb_lingue from commandbutton within w_responsabili
int X=1738
int Y=1041
int Width=366
int Height=81
int TabOrder=20
string Text="&Lingue"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_responsabili_lingue, -1, dw_responsabili)
end on

