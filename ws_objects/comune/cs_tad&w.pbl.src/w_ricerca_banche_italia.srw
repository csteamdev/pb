﻿$PBExportHeader$w_ricerca_banche_italia.srw
forward
global type w_ricerca_banche_italia from window
end type
type cb_5 from commandbutton within w_ricerca_banche_italia
end type
type cb_4 from commandbutton within w_ricerca_banche_italia
end type
type cb_3 from commandbutton within w_ricerca_banche_italia
end type
type cb_2 from commandbutton within w_ricerca_banche_italia
end type
type cb_1 from commandbutton within w_ricerca_banche_italia
end type
type st_1 from statictext within w_ricerca_banche_italia
end type
type em_banca from editmask within w_ricerca_banche_italia
end type
type dw_1 from datawindow within w_ricerca_banche_italia
end type
end forward

global type w_ricerca_banche_italia from window
integer width = 3232
integer height = 1604
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
cb_5 cb_5
cb_4 cb_4
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
st_1 st_1
em_banca em_banca
dw_1 dw_1
end type
global w_ricerca_banche_italia w_ricerca_banche_italia

on w_ricerca_banche_italia.create
this.cb_5=create cb_5
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_1=create st_1
this.em_banca=create em_banca
this.dw_1=create dw_1
this.Control[]={this.cb_5,&
this.cb_4,&
this.cb_3,&
this.cb_2,&
this.cb_1,&
this.st_1,&
this.em_banca,&
this.dw_1}
end on

on w_ricerca_banche_italia.destroy
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.em_banca)
destroy(this.dw_1)
end on

event open;dw_1.settransobJect(sqlca)

end event

type cb_5 from commandbutton within w_ricerca_banche_italia
integer x = 2834
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;if dw_1.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_1 = dw_1.getitemstring(dw_1.getrow(),"cod_abi")
	s_cs_xx.parametri.parametro_s_2 = dw_1.getitemstring(dw_1.getrow(),"cod_cab")
else
	g_mb.messagebox("APICE","Nulla da confermare!!!")
end if

close(parent)
end event

type cb_4 from commandbutton within w_ricerca_banche_italia
integer x = 2811
integer y = 20
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "RESET"
end type

event clicked;dw_1.reset()
end event

type cb_3 from commandbutton within w_ricerca_banche_italia
integer x = 1394
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "FILTRA"
end type

event clicked;string ls_ricerca

ls_ricerca = em_banca.text
if isnull(ls_ricerca) or len(ls_ricerca) < 1 then
	ls_ricerca = "%"
else
	ls_ricerca = "%" + ls_ricerca + "%"
end if

dw_1.retrieve(ls_ricerca)

end event

type cb_2 from commandbutton within w_ricerca_banche_italia
integer x = 2446
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_1 from commandbutton within w_ricerca_banche_italia
integer x = 2834
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

type st_1 from statictext within w_ricerca_banche_italia
integer y = 20
integer width = 251
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "BANCA:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_banca from editmask within w_ricerca_banche_italia
integer x = 274
integer y = 20
integer width = 1074
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type dw_1 from datawindow within w_ricerca_banche_italia
integer x = 23
integer y = 120
integer width = 3177
integer height = 1280
integer taborder = 10
string title = "none"
string dataobject = "d_lista_abicab_ricerca"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

