﻿$PBExportHeader$w_ricerca_chiavi.srw
forward
global type w_ricerca_chiavi from w_cs_xx_principale
end type
type st_1 from statictext within w_ricerca_chiavi
end type
type cb_ok from commandbutton within w_ricerca_chiavi
end type
type st_2 from statictext within w_ricerca_chiavi
end type
type cb_codice from uo_cb_ok within w_ricerca_chiavi
end type
type cb_des_prodotto from uo_cb_ok within w_ricerca_chiavi
end type
type dw_ricerca_chiavi from datawindow within w_ricerca_chiavi
end type
type sle_descrizione from singlelineedit within w_ricerca_chiavi
end type
end forward

global type w_ricerca_chiavi from w_cs_xx_principale
int Width=2629
int Height=1461
boolean TitleBar=true
string Title="Ricerca Chiavi"
st_1 st_1
cb_ok cb_ok
st_2 st_2
cb_codice cb_codice
cb_des_prodotto cb_des_prodotto
dw_ricerca_chiavi dw_ricerca_chiavi
sle_descrizione sle_descrizione
end type
global w_ricerca_chiavi w_ricerca_chiavi

type variables
long il_row
string is_tipo_ricerca='C'
boolean ib_chiavi
end variables

forward prototypes
public function integer wf_ricerca_chiavi ()
public function integer wf_nuova_ricerca (string fs_ricerca)
end prototypes

public function integer wf_ricerca_chiavi ();string ls_ricerca,ls_cod_azienda,ls_cod_chiave,ls_des_chiave,ls_ast
boolean lb_trovato
integer li_cont,i

lb_trovato=false


ls_ricerca=sle_descrizione.text

ls_ast=mid(ls_ricerca,len(ls_ricerca),1)

if (ls_ast="*") and (len(ls_ricerca)>1) then
	lb_trovato=true
	ls_ricerca=replace(ls_ricerca,len(ls_ricerca),1,"%")
end if

if ib_chiavi=false then
	
 if ls_ricerca='*' then
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,'%')
 else
	if lb_trovato=false then
	 select cod_chiave into :ls_cod_chiave from tab_chiavi where des_chiave = :ls_ricerca order by des_chiave;
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,ls_cod_chiave)
   else
	 //select count(*) into :li_cont from tab_chiavi where des_chiave like :ls_ricerca;
	 i=1
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,"%")
	 DECLARE cu_scorri_chiavi CURSOR FOR  
    SELECT "tab_chiavi"."cod_azienda",   
         "tab_chiavi"."cod_chiave",   
         "tab_chiavi"."des_chiave"  
    FROM "tab_chiavi"  where des_chiave like :ls_ricerca order by des_chiave;
    
	 open cu_scorri_chiavi;
	 
	 fetch cu_scorri_chiavi into :ls_cod_azienda,:ls_cod_chiave,:ls_des_chiave;
	 	 
	 do
	   dw_ricerca_chiavi.setitem(i,"cod_azienda",ls_cod_azienda)
      dw_ricerca_chiavi.setitem(i,"cod_chiave",ls_cod_chiave)
		dw_ricerca_chiavi.setitem(i,"des_chiave",ls_des_chiave)
		i=i+1
	   fetch cu_scorri_chiavi into :ls_cod_azienda,:ls_cod_chiave,:ls_des_chiave;
	 loop until sqlca.sqlcode <> 0
	 //st_2.text = "TROVATI " + string(i) + " PRODOTTI"
	 close cu_scorri_chiavi;
	  for li_cont=i to dw_ricerca_chiavi.rowcount()
		dw_ricerca_chiavi.setitem(li_cont,"cod_azienda","")
      dw_ricerca_chiavi.setitem(li_cont,"cod_chiave","")
		dw_ricerca_chiavi.setitem(li_cont,"des_chiave","")
	next
	 //dw_ricerca_chiavi.reset()
   end if
 end if

elseif ib_chiavi then

 if ls_ricerca='*' then
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,'%')
 else
	if lb_trovato=false then
	 select cod_chiave into :ls_cod_chiave from tab_chiavi where cod_chiave = :ls_ricerca order by cod_chiave;
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,ls_cod_chiave)
   else
	 
	 //select count(*) into :li_cont from tab_chiavi where cod_chiave like :ls_ricerca;
	// dw_ricerca_chiavi.reset()
	 i=1
	 
	 dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,"%")
	 DECLARE cu_scorri_chiavi_2 CURSOR FOR  
    SELECT "tab_chiavi"."cod_azienda",   
         "tab_chiavi"."cod_chiave",   
         "tab_chiavi"."des_chiave"  
    FROM "tab_chiavi"  where cod_chiave like :ls_ricerca order by cod_chiave;
    
	 open cu_scorri_chiavi_2;
	 
	 fetch cu_scorri_chiavi_2 into :ls_cod_azienda,:ls_cod_chiave,:ls_des_chiave;
	 
	 do
		dw_ricerca_chiavi.setitem(i,"cod_azienda",ls_cod_azienda)
      dw_ricerca_chiavi.setitem(i,"cod_chiave",ls_cod_chiave)
		dw_ricerca_chiavi.setitem(i,"des_chiave",ls_des_chiave)
		i=i+1
		fetch cu_scorri_chiavi_2 into :ls_cod_azienda,:ls_cod_chiave,:ls_des_chiave;
	 loop until sqlca.sqlcode <> 0
	 st_2.text = "TROVATI " + string(i) + " PRODOTTI"
	 for li_cont=i to dw_ricerca_chiavi.rowcount()
		dw_ricerca_chiavi.setitem(li_cont,"cod_azienda","")
      dw_ricerca_chiavi.setitem(li_cont,"cod_chiave","")
		dw_ricerca_chiavi.setitem(li_cont,"des_chiave","")
	next
	 close cu_scorri_chiavi_2;
	 //messagebox("Controllo",string(li_cont))
   end if 
 end if
end if

sle_descrizione.setfocus()

return 0




end function

public function integer wf_nuova_ricerca (string fs_ricerca);string ls_sql
long ll_i, ll_y

g_mb.messagebox("Controllo",fs_ricerca)
ls_sql = "select cod_chiave, des_chiave from tab_chiavi where cod_azienda = ~~~'" + s_cs_xx.cod_azienda + "~~~'"
g_mb.messagebox("Controllo",ls_sql)
ll_i = len(fs_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_ricerca, ll_y, 1) = "*" then
		fs_ricerca = replace(fs_ricerca, ll_y, 1, "%")
	end if
next

if ib_chiavi then
		if not isnull(fs_ricerca) and len(fs_ricerca) > 0 then
			if pos(fs_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and cod_chiave like ~~~'" + fs_ricerca + "~~~'"
			else
				ls_sql = ls_sql + " and cod_chiave = ~~~'" + fs_ricerca + "~~~'"
			end if			
		end if
	   ls_sql = ls_sql + " order by cod_chiave"
	else
		if not isnull(fs_ricerca) and len(fs_ricerca) > 0 then
			if pos(fs_ricerca, "%") > 0 then 
				ls_sql = ls_sql + " and des_chiave like ~~~'" + fs_ricerca + "~~~'"
			else
				ls_sql = ls_sql + " and des_chiave = ~~~'" + fs_ricerca + "~~~'"
			end if			
		end if
	   ls_sql = ls_sql + " order by des_chiave"
	end if	

dw_ricerca_chiavi.Modify("DataWindow.Table.Select='" + ls_sql + "'")
ll_y = dw_ricerca_chiavi.retrieve()
if ll_y > 0 then
	st_2.text = "TROVATI " + string(ll_y) + " PRODOTTI"
else
	st_2.text = "NESSUN PRODOTTO TROVATO"
end if
	
return 0
end function

on w_ricerca_chiavi.create
int iCurrent
call w_cs_xx_principale::create
this.st_1=create st_1
this.cb_ok=create cb_ok
this.st_2=create st_2
this.cb_codice=create cb_codice
this.cb_des_prodotto=create cb_des_prodotto
this.dw_ricerca_chiavi=create dw_ricerca_chiavi
this.sle_descrizione=create sle_descrizione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=cb_codice
this.Control[iCurrent+5]=cb_des_prodotto
this.Control[iCurrent+6]=dw_ricerca_chiavi
this.Control[iCurrent+7]=sle_descrizione
end on

on w_ricerca_chiavi.destroy
call w_cs_xx_principale::destroy
destroy(this.st_1)
destroy(this.cb_ok)
destroy(this.st_2)
destroy(this.cb_codice)
destroy(this.cb_des_prodotto)
destroy(this.dw_ricerca_chiavi)
destroy(this.sle_descrizione)
end on

event open;call super::open;ib_chiavi=false

dw_ricerca_chiavi.settransobject(sqlca)
dw_ricerca_chiavi.retrieve(s_cs_xx.cod_azienda,"%")

sle_descrizione.setfocus()
end event

type st_1 from statictext within w_ricerca_chiavi
int X=23
int Y=41
int Width=686
int Height=61
boolean Enabled=false
string Text="Ricerca per Descrizione:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from commandbutton within w_ricerca_chiavi
int X=2195
int Y=1261
int Width=366
int Height=81
int TabOrder=20
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_chiave

ls_cod_chiave=dw_ricerca_chiavi.getitemstring(dw_ricerca_chiavi.getrow(),"cod_chiave")

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ls_cod_chiave)

close(w_ricerca_chiavi)
end event

type st_2 from statictext within w_ricerca_chiavi
int X=23
int Y=1261
int Width=1761
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_codice from uo_cb_ok within w_ricerca_chiavi
int X=23
int Y=141
int Width=686
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="Codice Chiave"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;string ls_ricerca,ls_cod,ls_des,ls_sql

ib_chiavi=true

st_1.text="Ricerca per Codice"

ls_ricerca=sle_descrizione.text

ls_sql="select cod_chiave,des_chiave from tab_chiavi where cod_azienda=s_cs_xx.cod_azienda and cod_chiave like :ls_ricerca order by cod_chiave"

dw_ricerca_chiavi.Modify("DataWindow.Table.Select='" + ls_sql + "'")

sle_descrizione.setfocus()
end event

type cb_des_prodotto from uo_cb_ok within w_ricerca_chiavi
int X=709
int Y=141
int Width=1651
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="Descrizione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;string ls_ricerca,ls_cod,ls_des,ls_sql

ib_chiavi=false

st_1.text="Ricerca per Descrizione"

ls_ricerca=sle_descrizione.text

ls_sql="select cod_chiave,des_chiave from tab_chiavi where cod_azienda=s_cs_xx.cod_azienda and cod_chiave like :ls_ricerca order by des_chiave"

dw_ricerca_chiavi.Modify("DataWindow.Table.Select='" + ls_sql + "'")

sle_descrizione.setfocus()






end event

type dw_ricerca_chiavi from datawindow within w_ricerca_chiavi
event ue_key pbm_dwnkey
int X=23
int Y=145
int Width=2538
int Height=1101
int TabOrder=50
string DataObject="d_chiavi_ricerca"
boolean Border=false
boolean VScrollBar=true
boolean LiveScroll=true
end type

event doubleclicked;cb_ok.postevent(clicked!)

end event

type sle_descrizione from singlelineedit within w_ricerca_chiavi
event ue_press pbm_keydown
int X=686
int Y=21
int Width=1509
int Height=93
int TabOrder=10
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event ue_press;if key = keyenter! then
 wf_ricerca_chiavi()
end if



end event

