﻿$PBExportHeader$w_scelte_lingue.srw
forward
global type w_scelte_lingue from w_cs_xx_principale
end type
type dw_scelte_lingue from uo_cs_xx_dw within w_scelte_lingue
end type
end forward

global type w_scelte_lingue from w_cs_xx_principale
int Width=2273
int Height=1145
boolean TitleBar=true
string Title="Gestione Scelte Lingue"
dw_scelte_lingue dw_scelte_lingue
end type
global w_scelte_lingue w_scelte_lingue

on w_scelte_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_scelte_lingue=create dw_scelte_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_scelte_lingue
end on

on w_scelte_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_scelte_lingue)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_scelte_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_scelte_lingue.set_dw_key("cod_azienda")
dw_scelte_lingue.set_dw_key("cod_scelta")
dw_scelte_lingue.set_dw_options(sqlca, &
                              i_openparm, &
                              c_scrollparent, &
                              c_default)

end event

type dw_scelte_lingue from uo_cs_xx_dw within w_scelte_lingue
int X=23
int Y=21
int Width=2195
int Height=1001
int TabOrder=1
string DataObject="d_scelte_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_scelta


ls_cod_scelta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_scelta")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_scelta)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_scelta

ls_cod_scelta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_scelta")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_scelta")) then
      this.setitem(ll_i, "cod_scelta", ls_cod_scelta)
   end if
next
end event

