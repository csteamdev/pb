﻿$PBExportHeader$w_settori_att_lingue.srw
forward
global type w_settori_att_lingue from w_cs_xx_principale
end type
type dw_settori_att_lingue from uo_cs_xx_dw within w_settori_att_lingue
end type
end forward

global type w_settori_att_lingue from w_cs_xx_principale
int Width=2282
int Height=1141
boolean TitleBar=true
string Title="Gestione Settori Lingue"
dw_settori_att_lingue dw_settori_att_lingue
end type
global w_settori_att_lingue w_settori_att_lingue

on w_settori_att_lingue.create
int iCurrent
call w_cs_xx_principale::create
this.dw_settori_att_lingue=create dw_settori_att_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_settori_att_lingue
end on

on w_settori_att_lingue.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_settori_att_lingue)
end on

event pc_setwindow;call super::pc_setwindow;dw_settori_att_lingue.set_dw_key("cod_azienda")
dw_settori_att_lingue.set_dw_key("cod_settore")
dw_settori_att_lingue.set_dw_options(sqlca, &
                              i_openparm, &
                              c_scrollparent, &
                              c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_settori_att_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_settori_att_lingue from uo_cs_xx_dw within w_settori_att_lingue
int X=23
int Y=17
int Width=2195
int Height=1001
string DataObject="d_settori_att_lingue"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_settore

ls_cod_settore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_settore")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_settore")) then
      this.setitem(ll_i, "cod_settore", ls_cod_settore)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_settore


ls_cod_settore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_settore")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_settore)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

