﻿$PBExportHeader$w_simulatore_rate.srw
forward
global type w_simulatore_rate from w_cs_xx_principale
end type
type dw_simulatore_rate from uo_cs_xx_dw within w_simulatore_rate
end type
type cb_simula from commandbutton within w_simulatore_rate
end type
type cb_chiudi from commandbutton within w_simulatore_rate
end type
type dw_sel_simulatore from datawindow within w_simulatore_rate
end type
end forward

global type w_simulatore_rate from w_cs_xx_principale
integer width = 2949
integer height = 1936
string title = "Simulatore Rate Pagamenti"
boolean maxbox = false
boolean resizable = false
dw_simulatore_rate dw_simulatore_rate
cb_simula cb_simula
cb_chiudi cb_chiudi
dw_sel_simulatore dw_sel_simulatore
end type
global w_simulatore_rate w_simulatore_rate

type variables
string is_cod_pagamento
end variables

on w_simulatore_rate.create
int iCurrent
call super::create
this.dw_simulatore_rate=create dw_simulatore_rate
this.cb_simula=create cb_simula
this.cb_chiudi=create cb_chiudi
this.dw_sel_simulatore=create dw_sel_simulatore
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_simulatore_rate
this.Control[iCurrent+2]=this.cb_simula
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.dw_sel_simulatore
end on

on w_simulatore_rate.destroy
call super::destroy
destroy(this.dw_simulatore_rate)
destroy(this.cb_simula)
destroy(this.cb_chiudi)
destroy(this.dw_sel_simulatore)
end on

event pc_setwindow;call super::pc_setwindow;is_cod_pagamento = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_s_1)

dw_sel_simulatore.insertrow(0)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_simulatore,"cod_pagamento",sqlca,"tab_pagamenti","cod_pagamento", &
					  "des_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
if isvalid(sqlci) then
	f_po_loaddddw_dw(dw_simulatore_rate,"cod_tipo_pagamento",sqlci,"tipo_pagamento","codice","descrizione","")
else
	f_po_loaddddw_dw(dw_simulatore_rate,"cod_tipo_pagamento",sqlca,"tab_tipi_pagamenti", &
						 "cod_tipo_pagamento","des_tipo_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if
end event

event open;call super::open;if not isnull(is_cod_pagamento) then
	
	select cod_pagamento
	into   :is_cod_pagamento
	from   tab_pagamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_pagamento = :is_cod_pagamento;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in controllo esistenza pagamento: " + sqlca.sqlerrtext)
	elseif sqlca.sqlcode <> 100 then
		dw_sel_simulatore.setitem(1,"cod_pagamento",is_cod_pagamento)
	end if
	
end if

dw_sel_simulatore.setitem(1,"data_partenza",today())

dw_sel_simulatore.setitem(1,"imponibile",0)

dw_sel_simulatore.setitem(1,"iva",0)
end event

type dw_simulatore_rate from uo_cs_xx_dw within w_simulatore_rate
integer x = 23
integer y = 240
integer width = 2880
integer height = 1580
integer taborder = 30
string dataobject = "d_simulatore_rate"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_simula from commandbutton within w_simulatore_rate
integer x = 2537
integer y = 140
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Simula"
end type

event clicked;long     ll_tot_rate, ll_i, ll_riga

date     ldd_partenza, ldd_rate_data[]

dec{4}   ld_imp, ld_iva, ld_rate_importo[]

string 	ls_cod_pagamento, ls_messaggio, ls_rate_tipo[], ls_modalita[]

uo_calcola_documento_euro luo_rate


dw_sel_simulatore.accepttext()

ls_cod_pagamento = dw_sel_simulatore.getitemstring(1,"cod_pagamento")

if isnull(ls_cod_pagamento) then
	g_mb.messagebox("APICE","Selezionare un tipo pagamento prima di continuare!",exclamation!)
	return -1
end if

ldd_partenza = date(dw_sel_simulatore.getitemdatetime(1,"data_partenza"))

if isnull(ldd_partenza) then
	g_mb.messagebox("APICE","Selezionare la data di partenza delle scadenze prima di continuare!",exclamation!)
	return -1
end if

ld_imp = dw_sel_simulatore.getitemnumber(1,"imponibile")

ld_iva = dw_sel_simulatore.getitemnumber(1,"iva")

if (isnull(ld_imp) or ld_imp = 0) and (isnull(ld_iva) or ld_iva = 0) then
	g_mb.messagebox("APICE","Impostare un importo per il calcolo delle rate!",exclamation!)
	return -1
end if

setpointer(hourglass!)

dw_simulatore_rate.reset()

luo_rate = create uo_calcola_documento_euro
if luo_rate.uof_calcola_rate(ld_imp,ld_iva,ls_cod_pagamento,ldd_partenza,ldd_rate_data, ld_rate_importo,ls_rate_tipo,ls_modalita,ll_tot_rate,ls_messaggio) <> 0 then
	g_mb.messagebox("APICE","Errore in simulazione rate.~n" + ls_messaggio,stopsign!)
	destroy luo_rate
	setpointer(arrow!)
	return -1
end if
destroy luo_rate

for ll_i = 1 to ll_tot_rate
	
	ll_riga = dw_simulatore_rate.insertrow(0)
	
	dw_simulatore_rate.setitem(ll_riga,"data_rata",ldd_rate_data[ll_i])
	
	dw_simulatore_rate.setitem(ll_riga,"importo_rata",ld_rate_importo[ll_i])
	
	dw_simulatore_rate.setitem(ll_riga,"cod_tipo_pagamento",ls_rate_tipo[ll_i])
	
next

setpointer(arrow!)
end event

type cb_chiudi from commandbutton within w_simulatore_rate
integer x = 2537
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type dw_sel_simulatore from datawindow within w_simulatore_rate
integer x = 23
integer y = 20
integer width = 2469
integer height = 200
integer taborder = 10
string dataobject = "d_sel_simulatore_rate"
boolean livescroll = true
end type

event itemfocuschanged;selecttext(1,100)
end event

event getfocus;selecttext(1,100)
end event

