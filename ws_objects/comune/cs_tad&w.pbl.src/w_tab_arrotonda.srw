﻿$PBExportHeader$w_tab_arrotonda.srw
$PBExportComments$finestra gestione arrotondamenti
forward
global type w_tab_arrotonda from w_cs_xx_principale
end type
type dw_tab_arrotonda from uo_dw_main within w_tab_arrotonda
end type
end forward

global type w_tab_arrotonda from w_cs_xx_principale
int Width=3146
int Height=961
boolean TitleBar=true
string Title="Arrotondamenti Prezzi"
dw_tab_arrotonda dw_tab_arrotonda
end type
global w_tab_arrotonda w_tab_arrotonda

on w_tab_arrotonda.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_arrotonda=create dw_tab_arrotonda
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_arrotonda
end on

on w_tab_arrotonda.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_arrotonda)
end on

event open;call super::open;dw_tab_arrotonda.set_dw_key("cod_azienda")
dw_tab_arrotonda.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
iuo_dw_main = dw_tab_arrotonda


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tab_arrotonda, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_tab_arrotonda from uo_dw_main within w_tab_arrotonda
int X=23
int Y=21
int Width=3063
int Height=821
string DataObject="d_tab_arrotonda"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

