﻿$PBExportHeader$w_tab_operatori_utenti.srw
$PBExportComments$Finestra Tabella Collegamento Operatori X Utente
forward
global type w_tab_operatori_utenti from w_cs_xx_principale
end type
type dw_tab_operatori_utenti from uo_cs_xx_dw within w_tab_operatori_utenti
end type
end forward

global type w_tab_operatori_utenti from w_cs_xx_principale
integer width = 3319
integer height = 1148
string title = "Operatori X Utente"
dw_tab_operatori_utenti dw_tab_operatori_utenti
end type
global w_tab_operatori_utenti w_tab_operatori_utenti

on w_tab_operatori_utenti.create
int iCurrent
call super::create
this.dw_tab_operatori_utenti=create dw_tab_operatori_utenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_operatori_utenti
end on

on w_tab_operatori_utenti.destroy
call super::destroy
destroy(this.dw_tab_operatori_utenti)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_operatori_utenti.set_dw_key("cod_azienda")
dw_tab_operatori_utenti.set_dw_key("cod_operatore")
dw_tab_operatori_utenti.set_dw_key("cod_utente")
dw_tab_operatori_utenti.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tab_operatori_utenti
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tab_operatori_utenti, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "'))")

f_po_loaddddw_dw(dw_tab_operatori_utenti, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "nome_cognome", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")


f_po_loaddddw_dw(dw_tab_operatori_utenti, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > '" + string(today(),s_cs_xx.db_funzioni.formato_data) + "')")

end event

type dw_tab_operatori_utenti from uo_cs_xx_dw within w_tab_operatori_utenti
integer x = 18
integer y = 16
integer width = 3246
integer height = 1008
integer taborder = 10
string dataobject = "d_tab_operatori_utenti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event itemchanged;call super::itemchanged;choose case i_colname
	case "flag_default"
		string ls_cod_utente
		long ll_i
		if i_coltext = 'S' then
			ls_cod_utente = this.getitemstring(row, "cod_utente")
			for ll_i = 1 to this.rowcount()
				if ll_i <> row and this.getitemstring(ll_i, "flag_default") = "S" and &
					this.getitemstring(ll_i, "cod_utente") = ls_cod_utente	then
					g_mb.messagebox("Operatori di Default", "Esiste già un Operatore di Default per " + ls_cod_utente)
					return 2
				end if
			next
		end if
end choose				
end event

