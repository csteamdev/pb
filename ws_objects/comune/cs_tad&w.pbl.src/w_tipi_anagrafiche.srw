﻿$PBExportHeader$w_tipi_anagrafiche.srw
$PBExportComments$Finestra Gestione Tipi Dettagli Trattative
forward
global type w_tipi_anagrafiche from w_cs_xx_principale
end type
type dw_tipi_det_trattative from uo_cs_xx_dw within w_tipi_anagrafiche
end type
end forward

global type w_tipi_anagrafiche from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2967
integer height = 1072
string title = "Tipi Anagrafiche clienti / Fornitori"
dw_tipi_det_trattative dw_tipi_det_trattative
end type
global w_tipi_anagrafiche w_tipi_anagrafiche

event pc_setwindow;call super::pc_setwindow;dw_tipi_det_trattative.set_dw_key("cod_azienda")
dw_tipi_det_trattative.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                      c_default, &
                                      c_default)


end event

on w_tipi_anagrafiche.create
int iCurrent
call super::create
this.dw_tipi_det_trattative=create dw_tipi_det_trattative
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_det_trattative
end on

on w_tipi_anagrafiche.destroy
call super::destroy
destroy(this.dw_tipi_det_trattative)
end on

type dw_tipi_det_trattative from uo_cs_xx_dw within w_tipi_anagrafiche
integer x = 23
integer y = 20
integer width = 2880
integer height = 920
string dataobject = "d_tipi_anagrafiche_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_tipo_trattativa

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

