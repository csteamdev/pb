﻿$PBExportHeader$w_tipi_cause_sospensione.srw
$PBExportComments$Finestra Tipi Cause Sospensione Riga Documento
forward
global type w_tipi_cause_sospensione from w_cs_xx_principale
end type
type dw_tipi_cause_sospensione_lista from uo_cs_xx_dw within w_tipi_cause_sospensione
end type
end forward

global type w_tipi_cause_sospensione from w_cs_xx_principale
integer width = 2715
integer height = 1136
string title = "Cause Sospensione"
dw_tipi_cause_sospensione_lista dw_tipi_cause_sospensione_lista
end type
global w_tipi_cause_sospensione w_tipi_cause_sospensione

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_tipi_cause_sospensione_lista.set_dw_key("cod_azienda")

dw_tipi_cause_sospensione_lista.set_dw_options(sqlca, &
                       pcca.null_object, &
                       c_default, &
                       c_default)

end event

on w_tipi_cause_sospensione.create
int iCurrent
call super::create
this.dw_tipi_cause_sospensione_lista=create dw_tipi_cause_sospensione_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_cause_sospensione_lista
end on

on w_tipi_cause_sospensione.destroy
call super::destroy
destroy(this.dw_tipi_cause_sospensione_lista)
end on

type dw_tipi_cause_sospensione_lista from uo_cs_xx_dw within w_tipi_cause_sospensione
integer x = 9
integer y = 8
integer width = 2651
integer height = 1000
integer taborder = 10
string dataobject = "d_tipi_cause_sospensione_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

