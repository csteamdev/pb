﻿$PBExportHeader$w_valute_euro.srw
$PBExportComments$Finestra Gestione Valute
forward
global type w_valute_euro from w_cs_xx_principale
end type
type dw_valute_lista from uo_cs_xx_dw within w_valute_euro
end type
type dw_valute_det from uo_cs_xx_dw within w_valute_euro
end type
type cb_lingue from commandbutton within w_valute_euro
end type
type cb_cambi from commandbutton within w_valute_euro
end type
end forward

global type w_valute_euro from w_cs_xx_principale
integer width = 2071
integer height = 1740
string title = "Valute"
dw_valute_lista dw_valute_lista
dw_valute_det dw_valute_det
cb_lingue cb_lingue
cb_cambi cb_cambi
end type
global w_valute_euro w_valute_euro

type variables

end variables

on pc_delete;call w_cs_xx_principale::pc_delete;cb_lingue.enabled=false
cb_cambi.enabled=false
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_valute_lista.set_dw_key("cod_azienda")
dw_valute_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)
dw_valute_det.set_dw_options(sqlca, &
                             dw_valute_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
iuo_dw_main = dw_valute_lista

cb_lingue.enabled=false
cb_cambi.enabled=false
end on

on w_valute_euro.create
int iCurrent
call super::create
this.dw_valute_lista=create dw_valute_lista
this.dw_valute_det=create dw_valute_det
this.cb_lingue=create cb_lingue
this.cb_cambi=create cb_cambi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_valute_lista
this.Control[iCurrent+2]=this.dw_valute_det
this.Control[iCurrent+3]=this.cb_lingue
this.Control[iCurrent+4]=this.cb_cambi
end on

on w_valute_euro.destroy
call super::destroy
destroy(this.dw_valute_lista)
destroy(this.dw_valute_det)
destroy(this.cb_lingue)
destroy(this.cb_cambi)
end on

event pc_new;call super::pc_new;dw_valute_det.setitem(dw_valute_det.getrow(), "data_acq", datetime(today()))
dw_valute_det.setitem(dw_valute_det.getrow(), "data_ven", datetime(today()))
end event

event pc_setddlb;call super::pc_setddlb;if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_valute_det, &
						  "codice_impresa", &
						  sqlci, &
						  "valuta", &
						  "codice", &
						  "descrizione", &
						  "")
end if
end event

type dw_valute_lista from uo_cs_xx_dw within w_valute_euro
integer x = 23
integer y = 20
integer width = 1979
integer height = 500
integer taborder = 10
string dataobject = "d_valute_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_valuta")) then
      cb_lingue.enabled = true
      cb_cambi.enabled = true
   else
      cb_lingue.enabled = false
      cb_cambi.enabled = false
   end if
end if
end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_tabella, ls_codice, ls_cod_valuta

   for li_i = 1 to this.deletedcount()
      ls_tabella = "tab_valute_lingue"
      ls_codice  = "cod_valuta"
      ls_cod_valuta = this.getitemstring(li_i, "cod_valuta", delete!, true)
      f_del_lingue(ls_tabella, ls_codice, ls_cod_valuta)
   next
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_lingue.enabled=false
cb_cambi.enabled=false
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;cb_lingue.enabled=false
cb_cambi.enabled=false
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_valuta")) then
      cb_lingue.enabled = true
      cb_cambi.enabled = true
   else
      cb_lingue.enabled = false
      cb_cambi.enabled = false
   end if
end if
end on

type dw_valute_det from uo_cs_xx_dw within w_valute_euro
integer x = 23
integer y = 540
integer width = 1989
integer height = 980
integer taborder = 20
string dataobject = "d_valute_det_euro"
borderstyle borderstyle = styleraised!
end type

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   dw_valute_lista.postevent(updatestart!)
end if
end on

event itemchanged;call super::itemchanged;// ****** Modifica Michele per impostazione automatica del campo formato inbase alla precisione specificata ******

if i_colname = "precisione" then
	
	choose case i_coltext
			
		case "0,1000"
			
			dw_valute_det.setitem(row,"formato","###,###,###,##0.0")
		
		case "0,0100"
			
			dw_valute_det.setitem(row,"formato","###,###,###,##0.00")
			
		case "0,0010"
			
			dw_valute_det.setitem(row,"formato","###,###,###,##0.000")
			
		case "0,0001"
			
			dw_valute_det.setitem(row,"formato","###,###,###,##0.0000")	
			
		case else
			
			dw_valute_det.setitem(row,"formato","###,###,###,##0")
			
	end choose		
	
end if

// ****** Fine modifica  07/06/2001 ******************************************************************************
end event

type cb_lingue from commandbutton within w_valute_euro
integer x = 1646
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lingue"
end type

on clicked;window_open_parm(w_valute_lingue, -1, dw_valute_lista)

end on

type cb_cambi from commandbutton within w_valute_euro
integer x = 1257
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cambi"
end type

on clicked;window_open_parm(w_cambi, -1, dw_valute_lista)
end on

