﻿$PBExportHeader$w_con_contatti.srw
$PBExportComments$Finestra Controllo Contatti
forward
global type w_con_contatti from w_cs_xx_principale
end type
type dw_con_contatti from uo_cs_xx_dw within w_con_contatti
end type
end forward

global type w_con_contatti from w_cs_xx_principale
integer width = 2203
integer height = 1428
string title = "Gestione Parametri Contatti"
dw_con_contatti dw_con_contatti
end type
global w_con_contatti w_con_contatti

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_tipo_trattativa", &
                 sqlca, &
                 "tab_tipi_trattative", &
                 "cod_tipo_trattativa", &
                 "des_tipo_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_trattativa = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_tipo_lista_dist", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")


f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_lista_dist", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
					  
					  
f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_tipo_lista_dist_1", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")


f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_lista_dist_1", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")			
					  
f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_tipo_lista_dist_2", &
                 sqlca, &
                 "tab_tipi_liste_dist", &
                 "cod_tipo_lista_dist", &
                 "descrizione", &
                 " ")


f_po_loaddddw_dw(dw_con_contatti, &
                 "cod_lista_dist_2", &
                 sqlca, &
                 "tes_liste_dist", &
                 "cod_lista_dist", &
                 "des_lista_dist", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					  
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_contatti.set_dw_key("cod_azienda")
dw_con_contatti.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_default, &
                               c_default)

end on

on w_con_contatti.create
int iCurrent
call super::create
this.dw_con_contatti=create dw_con_contatti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_contatti
end on

on w_con_contatti.destroy
call super::destroy
destroy(this.dw_con_contatti)
end on

event getfocus;call super::getfocus;dw_con_contatti.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_con_contatti from uo_cs_xx_dw within w_con_contatti
integer x = 23
integer y = 20
integer width = 2126
integer height = 1280
string dataobject = "d_con_contatti"
borderstyle borderstyle = styleraised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event itemchanged;call super::itemchanged;choose case i_colname
	case "cod_tipo_lista_dist"
		
		if not isnull(i_coltext) and i_coltext <> "" then
		
			f_po_loaddddw_dw(dw_con_contatti, &
								  "cod_lista_dist", &
								  sqlca, &
								  "tes_liste_dist", &
								  "cod_lista_dist", &
								  "des_lista_dist", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_lista_dist = '" + i_coltext + "' ")		
		else
			f_po_loaddddw_dw(dw_con_contatti, &
								  "cod_lista_dist", &
								  sqlca, &
								  "tes_liste_dist", &
								  "cod_lista_dist", &
								  "des_lista_dist", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")					
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_con_contatti,"cod_cliente")
end choose
end event

event pcd_view;call super::pcd_view;dw_con_contatti.object.b_ricerca_cliente.enabled=false
end event

event pcd_new;call super::pcd_new;dw_con_contatti.object.b_ricerca_cliente.enabled=true
end event

event pcd_modify;call super::pcd_modify;dw_con_contatti.object.b_ricerca_cliente.enabled=true
end event

event pcd_delete;call super::pcd_delete;dw_con_contatti.object.b_ricerca_cliente.enabled=false
end event

