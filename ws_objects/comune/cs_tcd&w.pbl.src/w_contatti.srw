﻿$PBExportHeader$w_contatti.srw
$PBExportComments$Finestra Gestione Contatti
forward
global type w_contatti from w_cs_xx_principale
end type
type dw_nominativi from uo_dw_nominativi within w_contatti
end type
type cb_rubrica from commandbutton within w_contatti
end type
type cb_corrispondenze from commandbutton within w_contatti
end type
type cb_note from commandbutton within w_contatti
end type
type cb_reset from commandbutton within w_contatti
end type
type cb_ricerca from commandbutton within w_contatti
end type
type cb_clienti from commandbutton within w_contatti
end type
type gb_1 from groupbox within w_contatti
end type
type st_1 from statictext within w_contatti
end type
type dw_contatti_det_4 from uo_cs_xx_dw within w_contatti
end type
type dw_contatti_det_3 from uo_cs_xx_dw within w_contatti
end type
type dw_folder from u_folder within w_contatti
end type
type dw_contatti_det_2 from uo_cs_xx_dw within w_contatti
end type
type dw_contatti_det_5 from uo_cs_xx_dw within w_contatti
end type
type dw_contatti_lista from uo_cs_xx_dw within w_contatti
end type
type dw_contatti_det_1 from uo_cs_xx_dw within w_contatti
end type
type dw_folder_search from u_folder within w_contatti
end type
type dw_ricerca from u_dw_search within w_contatti
end type
end forward

global type w_contatti from w_cs_xx_principale
integer width = 3625
integer height = 1952
string title = "Gestione Contatti"
dw_nominativi dw_nominativi
cb_rubrica cb_rubrica
cb_corrispondenze cb_corrispondenze
cb_note cb_note
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_clienti cb_clienti
gb_1 gb_1
st_1 st_1
dw_contatti_det_4 dw_contatti_det_4
dw_contatti_det_3 dw_contatti_det_3
dw_folder dw_folder
dw_contatti_det_2 dw_contatti_det_2
dw_contatti_det_5 dw_contatti_det_5
dw_contatti_lista dw_contatti_lista
dw_contatti_det_1 dw_contatti_det_1
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_contatti w_contatti

forward prototypes
public function integer wf_conta_contatti ()
end prototypes

public function integer wf_conta_contatti ();long ll_num_contatti

select count(anag_contatti.cod_contatto)
into   :ll_num_contatti
from   anag_contatti
where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda;
st_1.text = string(ll_num_contatti,"###,###")
return 0
end function

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_contatti_det_2, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_2, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_2, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_2, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_2, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_nazione", &
                 sqlca, &
                 "tab_nazioni", &
                 "cod_nazione", &
                 "des_nazione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_area", &
                 sqlca, &
                 "tab_aree", &
                 "cod_area", &
                 "des_area", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_zona", &
                 sqlca, &
                 "tab_zone", &
                 "cod_zona", &
                 "des_zona", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_categoria", &
                 sqlca, &
                 "tab_categorie", &
                 "cod_categoria", &
                 "des_categoria", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_3, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_4, &
                 "cod_cliente", &
                 sqlca, &
                 "anag_clienti", &
                 "cod_cliente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_det_4, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_contatti_det_5, &
                 "cod_professione", &
                 sqlca, &
                 "tab_professioni", &
                 "cod_professione", &
                 "des_professione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_contatti_det_5, &
                 "cod_settore", &
                 sqlca, &
                 "tab_settori_att", &
                 "cod_settore", &
                 "des_settore", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_contatti_det_5, &
                 "cod_scelta", &
                 sqlca, &
                 "tab_scelte", &
                 "cod_scelta", &
                 "des_scelta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

dw_ricerca.fu_loadcode("rs_cod_valuta", &
                        "tab_valute", &
								"cod_valuta", &
								"des_valuta", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_valuta asc", "(Tutti)" )
//dw_ricerca.fu_loadcode("rs_cod_contatto", &
//                        "anag_contatti", &
//								"cod_contatto", &
//								"rag_soc_1", &
//								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_contatto asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_pagamento", &
                        "tab_pagamenti", &
								"cod_pagamento", &
								"des_pagamento", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_pagamento asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_categoria", &
                        "tab_categorie", &
								"cod_categoria", &
								"des_categoria", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_categoria asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_nazione", &
                        "tab_nazioni", &
								"cod_nazione", &
								"des_nazione", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_nazione asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_area", &
                        "tab_aree", &
								"cod_area", &
								"des_area", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_area asc", "(Tutti)" )
dw_ricerca.fu_loadcode("rs_cod_zona", &
                        "tab_zone", &
								"cod_zona", &
								"des_zona", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_zona asc", "(Tutti)" )
end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_corrispondenze.enabled = false
cb_rubrica.enabled = false
cb_clienti.enabled = false
cb_note.enabled = false
end on

event pc_setwindow;call super::pc_setwindow;
windowobject lw_oggetti[], l_objects[]

lw_oggetti[1] = dw_contatti_det_1
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])
lw_oggetti[1] = dw_contatti_det_2
dw_folder.fu_assigntab(2, "Condizioni", lw_oggetti[])
lw_oggetti[1] = dw_contatti_det_3
dw_folder.fu_assigntab(3, "Tabelle", lw_oggetti[])
lw_oggetti[1] = dw_contatti_det_4
dw_folder.fu_assigntab(4, "Marketing", lw_oggetti[])
lw_oggetti[1] = dw_contatti_det_5
dw_folder.fu_assigntab(5, "Web", lw_oggetti[])
lw_oggetti[1] = dw_nominativi
dw_folder.fu_assigntab(6, "Nominativi", lw_oggetti[])

dw_folder.fu_foldercreate(6, 6)
dw_folder.fu_selecttab(1)

dw_nominativi.uof_set_dw(dw_contatti_lista, "cod_contatto", dw_nominativi.CONTATTI)

dw_contatti_lista.set_dw_key("cod_azienda")
dw_contatti_lista.set_dw_options(sqlca, &
                               pcca.null_object, &
                               c_noretrieveonopen, &
                               c_default)
dw_contatti_det_1.set_dw_options(sqlca, &
                                dw_contatti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_contatti_det_2.set_dw_options(sqlca, &
                                dw_contatti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_contatti_det_3.set_dw_options(sqlca, &
                                dw_contatti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_contatti_det_4.set_dw_options(sqlca, &
                                dw_contatti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)
dw_contatti_det_5.set_dw_options(sqlca, &
                                dw_contatti_lista, &
                                c_sharedata + c_scrollparent, &
                                c_default)

iuo_dw_main=dw_contatti_lista

cb_corrispondenze.enabled = false
cb_rubrica.enabled = false
cb_clienti.enabled = false
cb_note.enabled = false

// -----------------------------  aggiunto per ricerca prodotti ----------------------------
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]

l_criteriacolumn[1] = "rs_cod_contatto"
l_criteriacolumn[2] = "rs_partita_iva"
l_criteriacolumn[3] = "rs_cod_categoria"
l_criteriacolumn[4] = "rs_cod_fiscale"
l_criteriacolumn[5] = "rs_cap"
l_criteriacolumn[6] = "rs_localita"
l_criteriacolumn[7] = "rs_provincia"
l_criteriacolumn[8] = "rs_cod_pagamento"
l_criteriacolumn[9] = "rs_flag_tipo_cliente"
l_criteriacolumn[10] = "rs_cod_nazione"
l_criteriacolumn[11] = "rs_cod_area"
l_criteriacolumn[12] = "rs_cod_zona"
l_criteriacolumn[13] = "rs_cod_valuta"
l_criteriacolumn[14] = "rs_flag_blocco"
l_criteriacolumn[15] = "rd_data_blocco"

l_searchtable[1] = "anag_contatti"
l_searchtable[2] = "anag_contatti"
l_searchtable[3] = "anag_contatti"
l_searchtable[4] = "anag_contatti"
l_searchtable[5] = "anag_contatti"
l_searchtable[6] = "anag_contatti"
l_searchtable[7] = "anag_contatti"
l_searchtable[8] = "anag_contatti"
l_searchtable[9] = "anag_contatti"
l_searchtable[10] = "anag_contatti"
l_searchtable[11] = "anag_contatti"
l_searchtable[12] = "anag_contatti"
l_searchtable[13] = "anag_contatti"
l_searchtable[14] = "anag_contatti"
l_searchtable[15] = "anag_contatti"

l_searchcolumn[1] = "cod_contatto"
l_searchcolumn[2] = "partita_iva"
l_searchcolumn[3] = "cod_categoria"
l_searchcolumn[4] = "cod_fiscale"
l_searchcolumn[5] = "cap"
l_searchcolumn[6] = "localita"
l_searchcolumn[7] = "provincia"
l_searchcolumn[8] = "cod_pagamento"
l_searchcolumn[9] = "flag_tipo_cliente"
l_searchcolumn[10] = "cod_nazione"
l_searchcolumn[11] = "cod_area"
l_searchcolumn[12] = "cod_zona"
l_searchcolumn[13] = "cod_valuta"
l_searchcolumn[14] = "flag_blocco"
l_searchcolumn[15] = "data_blocco"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_contatti_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_contatti_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_contatto
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_contatti_lista.change_dw_current()
wf_conta_contatti()

end event

on w_contatti.create
int iCurrent
call super::create
this.dw_nominativi=create dw_nominativi
this.cb_rubrica=create cb_rubrica
this.cb_corrispondenze=create cb_corrispondenze
this.cb_note=create cb_note
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_clienti=create cb_clienti
this.gb_1=create gb_1
this.st_1=create st_1
this.dw_contatti_det_4=create dw_contatti_det_4
this.dw_contatti_det_3=create dw_contatti_det_3
this.dw_folder=create dw_folder
this.dw_contatti_det_2=create dw_contatti_det_2
this.dw_contatti_det_5=create dw_contatti_det_5
this.dw_contatti_lista=create dw_contatti_lista
this.dw_contatti_det_1=create dw_contatti_det_1
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_nominativi
this.Control[iCurrent+2]=this.cb_rubrica
this.Control[iCurrent+3]=this.cb_corrispondenze
this.Control[iCurrent+4]=this.cb_note
this.Control[iCurrent+5]=this.cb_reset
this.Control[iCurrent+6]=this.cb_ricerca
this.Control[iCurrent+7]=this.cb_clienti
this.Control[iCurrent+8]=this.gb_1
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.dw_contatti_det_4
this.Control[iCurrent+11]=this.dw_contatti_det_3
this.Control[iCurrent+12]=this.dw_folder
this.Control[iCurrent+13]=this.dw_contatti_det_2
this.Control[iCurrent+14]=this.dw_contatti_det_5
this.Control[iCurrent+15]=this.dw_contatti_lista
this.Control[iCurrent+16]=this.dw_contatti_det_1
this.Control[iCurrent+17]=this.dw_folder_search
this.Control[iCurrent+18]=this.dw_ricerca
end on

on w_contatti.destroy
call super::destroy
destroy(this.dw_nominativi)
destroy(this.cb_rubrica)
destroy(this.cb_corrispondenze)
destroy(this.cb_note)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_clienti)
destroy(this.gb_1)
destroy(this.st_1)
destroy(this.dw_contatti_det_4)
destroy(this.dw_contatti_det_3)
destroy(this.dw_folder)
destroy(this.dw_contatti_det_2)
destroy(this.dw_contatti_det_5)
destroy(this.dw_contatti_lista)
destroy(this.dw_contatti_det_1)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

type dw_nominativi from uo_dw_nominativi within w_contatti
integer x = 55
integer y = 700
integer width = 3456
integer taborder = 140
boolean border = false
end type

type cb_rubrica from commandbutton within w_contatti
integer x = 3200
integer y = 220
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Rubrica"
end type

event clicked;window_open_parm(w_contatti_rubriche, -1, dw_contatti_lista)

end event

type cb_corrispondenze from commandbutton within w_contatti
integer x = 3200
integer y = 120
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_contatti_corrispondenze, -1, dw_contatti_lista)

end on

type cb_note from commandbutton within w_contatti
integer x = 3200
integer y = 20
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_contatti_note, -1, dw_contatti_lista)

end on

type cb_reset from commandbutton within w_contatti
event clicked pbm_bnclicked
integer x = 2766
integer y = 40
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_ricerca from commandbutton within w_contatti
event clicked pbm_bnclicked
integer x = 2766
integer y = 140
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_contatti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_clienti from commandbutton within w_contatti
integer x = 3200
integer y = 320
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Clienti"
end type

event clicked;dw_contatti_lista.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = dw_contatti_lista.getitemstring(dw_contatti_lista.getrow(), "cod_contatto")
window_open(w_genera_clienti, 0)

if not isnull(dw_contatti_lista.getitemstring(dw_contatti_lista.getrow(), "cod_cliente")) then
   cb_clienti.enabled = false
   f_po_loaddddw_dw(dw_contatti_det_4, &
                    "cod_cliente", &
                    sqlca, &
                    "anag_clienti", &
                    "cod_cliente", &
                    "rag_soc_1", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end if
end event

type gb_1 from groupbox within w_contatti
integer x = 3177
integer y = 420
integer width = 379
integer height = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Nr.Contatti"
end type

type st_1 from statictext within w_contatti
integer x = 3200
integer y = 480
integer width = 320
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_contatti_det_4 from uo_cs_xx_dw within w_contatti
integer x = 46
integer y = 700
integer width = 3154
integer height = 720
integer taborder = 130
string dataobject = "d_contatti_det_4"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_contatto"
//   ls_codice = "cod_contatto"
//   ls_flag_tipo = "flag_tipo_cliente"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_contatti_det_3 from uo_cs_xx_dw within w_contatti
integer x = 46
integer y = 700
integer width = 3497
integer height = 700
integer taborder = 140
string dataobject = "d_contatti_det_3"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;//string ls_anag, ls_codice, ls_flag_tipo
//
//
//ls_anag = "anag_contatto"
//ls_codice = "cod_contatto"
//ls_flag_tipo = "flag_tipo_cliente"
//
//f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//
end event

type dw_folder from u_folder within w_contatti
integer x = 23
integer y = 600
integer width = 3543
integer height = 1240
integer taborder = 0
end type

type dw_contatti_det_2 from uo_cs_xx_dw within w_contatti
integer x = 46
integer y = 700
integer width = 3314
integer height = 900
integer taborder = 120
string dataobject = "d_contatti_det_2"
boolean border = false
end type

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_contatto"
//   ls_codice = "cod_contatto"
//   ls_flag_tipo = "flag_tipo_cliente"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if
end event

type dw_contatti_det_5 from uo_cs_xx_dw within w_contatti
integer x = 69
integer y = 720
integer width = 3109
integer height = 796
integer taborder = 2
string dataobject = "d_contatti_det_5"
end type

type dw_contatti_lista from uo_cs_xx_dw within w_contatti
integer x = 137
integer y = 40
integer width = 1509
integer height = 500
integer taborder = 30
string dataobject = "d_contatti_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_contatto")) then
      cb_corrispondenze.enabled = true
      cb_rubrica.enabled = true
      cb_note.enabled = true
      if isnull(this.getitemstring(this.getrow(), "cod_cliente")) then
         cb_clienti.enabled = true
      else
         cb_clienti.enabled = false
      end if
   else
      cb_corrispondenze.enabled = false
      cb_rubrica.enabled = false
      cb_clienti.enabled = false
      cb_note.enabled = false
   end if
end if
end on

event updateend;call super::updateend;long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, &
				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a
double		ld_sconto
string			ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_cliente, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_flag_fuori_fido, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_agente_1, ls_cod_agente_2, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_flag_riep_boll, ls_flag_riep_fatt, ls_cod_contatto, ls_cod_cliente
datetime		ldt_data_esenzione_iva


ll_i = 0
do while ll_i <= dw_contatti_lista.rowcount()
	ll_i = dw_contatti_lista.getnextmodified(ll_i, Primary!)
	
	if ll_i = 0 then
		return
	end if
	
	ls_cod_cliente = dw_contatti_lista.getitemstring(ll_i, "cod_cliente")
	
	if not isnull(ls_cod_cliente) then
		ls_rag_soc_1 = dw_contatti_lista.getitemstring(ll_i, "rag_soc_1")
		ls_rag_soc_2 = dw_contatti_lista.getitemstring(ll_i, "rag_soc_2")
		ls_indirizzo = dw_contatti_lista.getitemstring(ll_i, "indirizzo")
		ls_localita = dw_contatti_lista.getitemstring(ll_i, "localita")
		ls_frazione = dw_contatti_lista.getitemstring(ll_i, "frazione")
		ls_cap = dw_contatti_lista.getitemstring(ll_i, "cap")
		ls_provincia = dw_contatti_lista.getitemstring(ll_i, "provincia")
		ls_telefono = dw_contatti_lista.getitemstring(ll_i, "telefono")
		ls_fax = dw_contatti_lista.getitemstring(ll_i, "fax")
		ls_telex = dw_contatti_lista.getitemstring(ll_i, "telex")
		ls_cod_fiscale = dw_contatti_lista.getitemstring(ll_i, "cod_fiscale")
		ls_partita_iva = dw_contatti_lista.getitemstring(ll_i, "partita_iva")
		ls_rif_interno = dw_contatti_lista.getitemstring(ll_i, "rif_interno")
		ls_flag_tipo_cliente = dw_contatti_lista.getitemstring(ll_i, "flag_tipo_cliente")
		ls_cod_conto = dw_contatti_lista.getitemstring(ll_i, "cod_conto")
		ls_cod_iva = dw_contatti_lista.getitemstring(ll_i, "cod_iva")
		ls_num_prot_esenzione_iva = dw_contatti_lista.getitemstring(ll_i, "num_prot_esenzione_iva")
		ldt_data_esenzione_iva = dw_contatti_lista.getitemdatetime(ll_i, "data_esenzione_iva")
		ls_flag_sospensione_iva = dw_contatti_lista.getitemstring(ll_i, "flag_sospensione_iva")
		ls_cod_pagamento = dw_contatti_lista.getitemstring(ll_i, "cod_pagamento")
		ll_giorno_fisso_scadenza = dw_contatti_lista.getitemnumber(ll_i, "giorno_fisso_scadenza")
		ll_mese_esclusione_1 = dw_contatti_lista.getitemnumber(ll_i, "mese_esclusione_1")
		ll_mese_esclusione_2 = dw_contatti_lista.getitemnumber(ll_i, "mese_esclusione_2")
		
		ll_ggmm_esclusione_1_da	= dw_contatti_lista.getitemnumber(ll_i, "ggmm_esclusione_1_da")
		ll_ggmm_esclusione_1_a		= dw_contatti_lista.getitemnumber(ll_i, "ggmm_esclusione_1_a")
		ll_ggmm_esclusione_2_da	= dw_contatti_lista.getitemnumber(ll_i, "ggmm_esclusione_2_da")
		ll_ggmm_esclusione_2_a		= dw_contatti_lista.getitemnumber(ll_i, "ggmm_esclusione_2_a")
		
		ll_data_sostituzione_1 = dw_contatti_lista.getitemnumber(ll_i, "data_sostituzione_1")
		ll_data_sostituzione_2 = dw_contatti_lista.getitemnumber(ll_i, "data_sostituzione_2")
		ld_sconto = dw_contatti_lista.getitemnumber(ll_i, "sconto")
		ls_cod_tipo_listino_prodotto = dw_contatti_lista.getitemstring(ll_i, "cod_tipo_listino_prodotto")
		ll_fido = dw_contatti_lista.getitemnumber(ll_i, "fido")
		ls_flag_fuori_fido = dw_contatti_lista.getitemstring(ll_i, "flag_fuori_fido")
		ls_cod_banca_clien_for = dw_contatti_lista.getitemstring(ll_i, "cod_banca_clien_for")
		ls_conto_corrente = dw_contatti_lista.getitemstring(ll_i, "conto_corrente")
		ls_cod_lingua = dw_contatti_lista.getitemstring(ll_i, "cod_lingua")
		ls_cod_nazione = dw_contatti_lista.getitemstring(ll_i, "cod_nazione")
		ls_cod_area = dw_contatti_lista.getitemstring(ll_i, "cod_area")
		ls_cod_zona = dw_contatti_lista.getitemstring(ll_i, "cod_zona")
		ls_cod_valuta = dw_contatti_lista.getitemstring(ll_i, "cod_valuta")
		ls_cod_categoria = dw_contatti_lista.getitemstring(ll_i, "cod_categoria")
		ls_cod_agente_1 = dw_contatti_lista.getitemstring(ll_i, "cod_agente_1")
		ls_cod_agente_2 = dw_contatti_lista.getitemstring(ll_i, "cod_agente_2")
		ls_cod_imballo = dw_contatti_lista.getitemstring(ll_i, "cod_imballo")
		ls_cod_porto = dw_contatti_lista.getitemstring(ll_i, "cod_porto")
		ls_cod_resa = dw_contatti_lista.getitemstring(ll_i, "cod_resa")
		ls_cod_mezzo = dw_contatti_lista.getitemstring(ll_i, "cod_mezzo")
		ls_cod_vettore = dw_contatti_lista.getitemstring(ll_i, "cod_vettore")
		ls_cod_inoltro = dw_contatti_lista.getitemstring(ll_i, "cod_inoltro")
		ls_cod_deposito = dw_contatti_lista.getitemstring(ll_i, "cod_deposito")
		ls_flag_riep_boll = dw_contatti_lista.getitemstring(ll_i, "flag_riep_boll")
		ls_flag_riep_fatt = dw_contatti_lista.getitemstring(ll_i, "flag_riep_fatt")

		update anag_clienti
		set    rag_soc_1 = :ls_rag_soc_1,
				 rag_soc_2 = :ls_rag_soc_2,   
				 indirizzo = :ls_indirizzo,   
				 localita = :ls_localita,   
				 frazione = :ls_frazione,   
				 cap = :ls_cap,   
				 provincia = :ls_provincia,   
				 telefono = :ls_telefono,   
				 fax = :ls_fax,   
				 telex = :ls_telex,   
				 cod_fiscale = :ls_cod_fiscale,   
				 partita_iva = :ls_partita_iva,   
				 rif_interno = :ls_rif_interno,   
				 flag_tipo_cliente = :ls_flag_tipo_cliente,   
				 cod_conto = :ls_cod_conto,   
				 cod_iva = :ls_cod_iva,   
				 num_prot_esenzione_iva = :ls_num_prot_esenzione_iva,   
				 data_esenzione_iva = :ldt_data_esenzione_iva,   
				 flag_sospensione_iva = :ls_flag_sospensione_iva,   
				 cod_pagamento = :ls_cod_pagamento,   
				 giorno_fisso_scadenza = :ll_giorno_fisso_scadenza,   
				 mese_esclusione_1 = :ll_mese_esclusione_1,   
				 mese_esclusione_2 = :ll_mese_esclusione_2,
				 ggmm_esclusione_1_da =:ll_ggmm_esclusione_1_da,
				 ggmm_esclusione_1_a =:ll_ggmm_esclusione_1_a,
				 ggmm_esclusione_2_da =:ll_ggmm_esclusione_2_da,
				 ggmm_esclusione_2_a =:ll_ggmm_esclusione_2_a,
				 data_sostituzione_1 = :ll_data_sostituzione_1,   
				 data_sostituzione_2 = :ll_data_sostituzione_2,   
				 sconto = :ld_sconto,   
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto,   
				 fido = :ll_fido,   
				 flag_fuori_fido = :ls_flag_fuori_fido,   
				 cod_banca_clien_for = :ls_cod_banca_clien_for,   
				 conto_corrente = :ls_conto_corrente,
				 cod_lingua = :ls_cod_lingua,   
				 cod_nazione = :ls_cod_nazione,   
				 cod_area = :ls_cod_area,   
				 cod_zona = :ls_cod_zona,   
				 cod_valuta = :ls_cod_valuta,   
				 cod_categoria = :ls_cod_categoria,   
				 cod_agente_1 = :ls_cod_agente_1,   
				 cod_agente_2 = :ls_cod_agente_2,   
				 cod_imballo = :ls_cod_imballo,   
				 cod_porto = :ls_cod_porto,   
				 cod_resa = :ls_cod_resa,   
				 cod_mezzo = :ls_cod_mezzo,   
				 cod_vettore = :ls_cod_vettore,   
				 cod_inoltro = :ls_cod_inoltro,   
				 cod_deposito = :ls_cod_deposito,   
				 flag_riep_boll = :ls_flag_riep_boll,   
				 flag_riep_fatt = :ls_flag_riep_fatt
		where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_clienti.cod_cliente = :ls_cod_cliente;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Clienti.", exclamation!, ok!)
			rollback;
			return
		end if

		commit;
   end if
loop

wf_conta_contatti()

end event

event rowfocuschanged;call super::rowfocuschanged;dw_nominativi.retrieve()

if i_extendmode then
   string ls_cod_contatto

   if this.getrow() > 0 then
      ls_cod_contatto = this.getitemstring(this.getrow(), "cod_contatto")
      if isnull(ls_cod_contatto) then
         ls_cod_contatto = ""
      end if

      if isnull(this.getitemstring(this.getrow(), "cod_cliente")) then
         cb_clienti.enabled = true
      else
         cb_clienti.enabled = false
      end if
   end if
end if
end event

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_contatto")) then
      cb_corrispondenze.enabled = true
      cb_rubrica.enabled = true
      cb_note.enabled = true
      if isnull(this.getitemstring(this.getrow(), "cod_cliente")) then
         cb_clienti.enabled = true
      else
         cb_clienti.enabled = false
      end if
   else
      cb_corrispondenze.enabled = false
      cb_rubrica.enabled = false
      cb_clienti.enabled = false
      cb_note.enabled = false
   end if
end if
end on

event pcd_validaterow;call super::pcd_validaterow;//if i_extendmode then
//   string ls_anag, ls_codice, ls_flag_tipo
//
//   ls_anag = "anag_contatti"
//   ls_codice = "cod_contatto"
//   ls_flag_tipo = "flag_tipo_cliente"
//
//   f_clifor_valrow(ls_anag, ls_codice, ls_flag_tipo)
//end if

string ls_cod_fiscale, ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)


if getrow() > 0 then
   f_upd_partita_iva("anag_contatti", "cod_contatto", "rag_soc_1")
   if this.getitemstring(i_rownbr, "flag_tipo_cliente") <> "E" and &
      not isnull(this.getitemstring(i_rownbr, "cod_fiscale")) then
      ls_cod_fiscale = this.getitemstring(i_rownbr, "cod_fiscale")
      if not f_cod_fiscale(ls_cod_fiscale) then
        	g_mb.messagebox("Attenzione", "Inserire un codice fiscale valido!", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
	
	if isnull(getitemstring(getrow(),"cod_iva")) then
		if (dw_contatti_det_2.getcolumnname() = "num_prot_esenzione_iva" and &
		      not isnull(dw_contatti_det_2.gettext()) and len(dw_contatti_det_2.gettext()) > 0) or &
		   (dw_contatti_det_2.getcolumnname() <> "num_prot_esenzione_iva" and not isnull(this.getitemstring(getrow(), "num_prot_esenzione_iva"))) then
			g_mb.messagebox("APICE","Attenzione! Per indicare un protocollo esenzione è necessario aver indicato ~r~nanche un codice di esenzione.~r~nIl protocollo che è stato inserito e la relativa data verranno cancellati")
		end if
		this.setitem(getrow(), "num_prot_esenzione_iva", ls_null)
		this.setitem(getrow(), "data_esenzione_iva", ldt_null)
	end if

end if





	

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_corrispondenze.enabled = false
   cb_rubrica.enabled = false
   cb_clienti.enabled = false
   cb_note.enabled = false
end if
end on

event pcd_new;call super::pcd_new;if i_extendmode then
   cb_corrispondenze.enabled = false
   cb_rubrica.enabled = false
   cb_clienti.enabled = false
   cb_note.enabled = false
	
	setitem(getrow(),"data_creazione",datetime(today(),00:00:00))
	setitem(getrow(),"data_modifica",datetime(today(),00:00:00))
end if
end event

event updatestart;call super::updatestart;int li_i

if i_extendmode then

	for li_i = 1 to rowcount()
		if getitemstatus(li_i, 0, primary!) = newmodified! OR getitemstatus(li_i, 0, primary!) = datamodified! then      /// nuovo o modificato
			if getitemstatus(li_i, 0, primary!) = datamodified! then		// solo modificato
				setitem(li_i,"data_modifica", datetime(today(),00:00:00) )
			end if
		end if
	next
	
end if
end event

type dw_contatti_det_1 from uo_cs_xx_dw within w_contatti
integer x = 46
integer y = 700
integer width = 3429
integer height = 1060
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_contatti_det_1"
boolean border = false
end type

event doubleclicked;call super::doubleclicked;string ls_messaggio, ls_destinatari[], ls_allegati[],ls_errore
uo_outlook luo_outlook

choose case dwo.name
	case "casella_mail"
		
		luo_outlook = create uo_outlook
		ls_destinatari[1] = this.getitemstring(row, "casella_mail")
		if luo_outlook.uof_invio_outlook(0, "M", "", "", ls_destinatari[], ls_allegati[], false, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		end if
		destroy luo_outlook
		
end choose
end event

type dw_folder_search from u_folder within w_contatti
integer x = 23
integer y = 20
integer width = 3131
integer height = 560
integer taborder = 20
end type

type dw_ricerca from u_dw_search within w_contatti
integer x = 137
integer y = 40
integer width = 2606
integer height = 520
integer taborder = 40
string dataobject = "d_contatti_search"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_ricerca,"rs_cod_contatto")
end choose
end event

