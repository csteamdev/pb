﻿$PBExportHeader$w_contatti_corrispondenze_old.srw
$PBExportComments$Finestra Gestione Contatti Corrispondenze
forward
global type w_contatti_corrispondenze_old from w_cs_xx_principale
end type
type dw_contatti_corrispondenze_lista from uo_cs_xx_dw within w_contatti_corrispondenze_old
end type
type dw_contatti_corrispondenze_det from uo_cs_xx_dw within w_contatti_corrispondenze_old
end type
type cb_note_esterne from commandbutton within w_contatti_corrispondenze_old
end type
type cb_controllo from commandbutton within w_contatti_corrispondenze_old
end type
type cb_agenda from commandbutton within w_contatti_corrispondenze_old
end type
end forward

global type w_contatti_corrispondenze_old from w_cs_xx_principale
integer width = 3465
integer height = 1784
string title = "Gestione Corrispondenze Contatti"
dw_contatti_corrispondenze_lista dw_contatti_corrispondenze_lista
dw_contatti_corrispondenze_det dw_contatti_corrispondenze_det
cb_note_esterne cb_note_esterne
cb_controllo cb_controllo
cb_agenda cb_agenda
end type
global w_contatti_corrispondenze_old w_contatti_corrispondenze_old

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_contatti_corrispondenze_det, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_contatti_corrispondenze_det, &
                 "num_reg_lista", &
                 sqlca, &
                 "tes_liste_controllo", &
                 "num_reg_lista", &
                 "des_lista", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")
f_po_loaddddw_dw(dw_contatti_corrispondenze_det, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_contatti_corrispondenze_lista.set_dw_key("cod_azienda")
dw_contatti_corrispondenze_lista.set_dw_key("cod_contatto")
dw_contatti_corrispondenze_lista.set_dw_options(sqlca, &
                                                i_openparm, &
                                                c_scrollparent, &
                                                c_default)
dw_contatti_corrispondenze_det.set_dw_options(sqlca, &
                                              dw_contatti_corrispondenze_lista, &
                                              c_sharedata + c_scrollparent, &
                                              c_default)

iuo_dw_main=dw_contatti_corrispondenze_lista

cb_agenda.enabled = false
cb_controllo.enabled = false
cb_note_esterne.enabled = false

end on

on pc_delete;call w_cs_xx_principale::pc_delete;cb_agenda.enabled = false
cb_controllo.enabled = false
cb_note_esterne.enabled = false

end on

on w_contatti_corrispondenze_old.create
int iCurrent
call super::create
this.dw_contatti_corrispondenze_lista=create dw_contatti_corrispondenze_lista
this.dw_contatti_corrispondenze_det=create dw_contatti_corrispondenze_det
this.cb_note_esterne=create cb_note_esterne
this.cb_controllo=create cb_controllo
this.cb_agenda=create cb_agenda
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_contatti_corrispondenze_lista
this.Control[iCurrent+2]=this.dw_contatti_corrispondenze_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_controllo
this.Control[iCurrent+5]=this.cb_agenda
end on

on w_contatti_corrispondenze_old.destroy
call super::destroy
destroy(this.dw_contatti_corrispondenze_lista)
destroy(this.dw_contatti_corrispondenze_det)
destroy(this.cb_note_esterne)
destroy(this.cb_controllo)
destroy(this.cb_agenda)
end on

type dw_contatti_corrispondenze_lista from uo_cs_xx_dw within w_contatti_corrispondenze_old
integer x = 23
integer y = 20
integer width = 2994
integer height = 500
integer taborder = 10
string dataobject = "d_contatti_corrispondenze_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      cb_agenda.enabled = true
      cb_controllo.enabled = true
      cb_note_esterne.enabled = true
   else
      cb_agenda.enabled = false
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end on

on updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   long ll_num_reg_lista, ll_prog_liste_con_comp

   for li_i = 1 to this.deletedcount()
      ll_num_reg_lista = this.getitemnumber(li_i, "num_reg_lista_comp", delete!, true)
      ll_prog_liste_con_comp = this.getitemnumber(li_i, "prog_liste_con_comp", delete!, true)
      f_cancella_liste_con_comp(ll_num_reg_lista, ll_prog_liste_con_comp)
   next
end if
end on

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_contatti_corrispondenze_det.modify(ls_modify)

   cb_agenda.enabled = false
   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_contatti_corrispondenze_det.modify(ls_modify)

   cb_agenda.enabled = false
   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog_corrispondenza
string ls_cod_contatto, ls_cod_corrispondenza
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

ls_cod_contatto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_contatto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_contatto")) then
      this.setitem(ll_i, "cod_contatto", ls_cod_contatto)
   end if

   if isnull(this.getitemnumber(ll_i, "prog_corrispondenza")) or &
      this.getitemnumber(ll_i, "prog_corrispondenza") = 0 then

      ls_cod_corrispondenza = this.getitemstring(ll_i,"cod_corrispondenza")
      ldt_data_corrispondenza = this.getitemdatetime(ll_i,"data_corrispondenza")
      ldt_ora_corrispondenza = this.getitemdatetime(ll_i,"ora_corrispondenza")

      select max(anag_contatti_corrispondenze.prog_corrispondenza)
      into   :ll_prog_corrispondenza
      from   anag_contatti_corrispondenze
      where  anag_contatti_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
             anag_contatti_corrispondenze.cod_contatto = :ls_cod_contatto and
             anag_contatti_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and
             anag_contatti_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and
             anag_contatti_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza;

      if not isnull(ll_prog_corrispondenza) then
         this.setitem(ll_i, "prog_corrispondenza", ll_prog_corrispondenza + 1)
      else
         this.setitem(ll_i, "prog_corrispondenza", 1)
      end if
   end if
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_contatto


ls_cod_contatto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_contatto")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_contatto)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      cb_agenda.enabled = true
      cb_controllo.enabled = true
      cb_note_esterne.enabled = true
   else
      cb_agenda.enabled = false
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end on

type dw_contatti_corrispondenze_det from uo_cs_xx_dw within w_contatti_corrispondenze_old
integer x = 23
integer y = 540
integer width = 3383
integer height = 1120
integer taborder = 20
string dataobject = "d_contatti_corrispondenze_det"
borderstyle borderstyle = styleraised!
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   double ld_costo
   integer li_null, li_num_reg_lista, li_num_versione, li_num_edizione
   setnull(li_null)


   choose case i_colname
      case "cod_corrispondenza"
         this.setitem(i_rownbr, "num_reg_lista", li_null)

         select tab_corrispondenze.num_reg_lista
         into   :li_num_reg_lista
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext; 

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "num_reg_lista", li_num_reg_lista)
            select   tes_liste_controllo.num_versione,
                     tes_liste_controllo.num_edizione
            into     :li_num_versione, 
                     :li_num_edizione
            from     tes_liste_controllo
            where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                     tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                     tes_liste_controllo.flag_valido = 'S';

            this.setitem(this.getrow(), "num_versione", li_num_versione)
            this.setitem(this.getrow(), "num_edizione", li_num_edizione)
         end if

         select tab_corrispondenze.costo
         into   :ld_costo
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "costo", ld_costo)
         end if
      case "num_reg_lista"
         li_num_reg_lista = integer(i_coltext)
         select   tes_liste_controllo.num_versione,
                  tes_liste_controllo.num_edizione
         into     :li_num_versione, 
                  :li_num_edizione
         from     tes_liste_controllo
         where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                  tes_liste_controllo.num_reg_lista = :li_num_reg_lista and 
                  tes_liste_controllo.flag_valido = 'S';

         this.setitem(this.getrow(), "num_versione", li_num_versione)
         this.setitem(this.getrow(), "num_edizione", li_num_edizione)
   end choose
end if
end on

type cb_note_esterne from commandbutton within w_contatti_corrispondenze_old
integer x = 3040
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_contatto, ls_cod_corrispondenza, ls_db
integer li_i, li_prog_corrispondenza, li_risposta
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_contatti_corrispondenze_lista.getrow()
ls_cod_contatto = dw_contatti_corrispondenze_lista.getitemstring(li_i, "cod_contatto")
ls_cod_corrispondenza = dw_contatti_corrispondenze_lista.getitemstring(li_i, "cod_corrispondenza")
ldt_data_corrispondenza = dw_contatti_corrispondenze_lista.getitemdatetime(li_i, "data_corrispondenza")
ldt_ora_corrispondenza = dw_contatti_corrispondenze_lista.getitemdatetime(li_i, "ora_corrispondenza")
li_prog_corrispondenza = dw_contatti_corrispondenze_lista.getitemnumber(li_i, "prog_corrispondenza")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob anag_contatti_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_contatti_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_contatto = :ls_cod_contatto and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob anag_contatti_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       anag_contatti_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           cod_contatto = :ls_cod_contatto and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)

	   updateblob anag_contatti_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_contatto = :ls_cod_contatto and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob anag_contatti_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              cod_contatto = :ls_cod_contatto and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza;
					  
	end if
	
   commit;
end if
end event

type cb_controllo from commandbutton within w_contatti_corrispondenze_old
integer x = 3040
integer y = 120
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;string ls_cod_contatto, ls_cod_corrispondenza
long   ll_prog_corrispondenza, ll_i, ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista_comp
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza
   
ll_i = dw_contatti_corrispondenze_lista.getrow()
ll_num_reg_lista_comp = dw_contatti_corrispondenze_lista.getitemnumber(ll_i,"num_reg_lista_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ls_cod_contatto = dw_contatti_corrispondenze_lista.getitemstring(ll_i,"cod_contatto") 
   ls_cod_corrispondenza = dw_contatti_corrispondenze_lista.getitemstring(ll_i,"cod_corrispondenza") 
   ldt_data_corrispondenza = dw_contatti_corrispondenze_lista.getitemdatetime(ll_i,"data_corrispondenza") 
   ldt_ora_corrispondenza = dw_contatti_corrispondenze_lista.getitemdatetime(ll_i,"ora_corrispondenza") 
   ll_prog_corrispondenza = dw_contatti_corrispondenze_lista.getitemnumber(ll_i,"prog_corrispondenza") 
   ll_num_reg_lista_comp = dw_contatti_corrispondenze_lista.getitemnumber(ll_i,"num_reg_lista") 
   
   update anag_contatti_corrispondenze
   set    anag_contatti_corrispondenze.num_versione = :ll_num_versione,
          anag_contatti_corrispondenze.num_edizione = :ll_num_edizione,
          anag_contatti_corrispondenze.num_reg_lista_comp = :ll_num_reg_lista_comp,
          anag_contatti_corrispondenze.prog_liste_con_comp =:ll_prog_liste_con_comp
   where  anag_contatti_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
          anag_contatti_corrispondenze.cod_contatto = :ls_cod_contatto and 
          anag_contatti_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and 
          anag_contatti_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and 
          anag_contatti_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza and
          anag_contatti_corrispondenze.prog_corrispondenza = :ll_prog_corrispondenza;
   commit;
   pcca.window_current.triggerevent("pc_retrieve")
end if

window_open_parm(w_det_liste_con_comp, 0, dw_contatti_corrispondenze_lista)

end event

type cb_agenda from commandbutton within w_contatti_corrispondenze_old
integer x = 3040
integer y = 220
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Agenda"
end type

event clicked;window_open_parm(w_contatti_agende, -1, dw_contatti_corrispondenze_lista)

end event

