﻿$PBExportHeader$w_contatti_ricerca_response.srw
forward
global type w_contatti_ricerca_response from w_cs_xx_risposta
end type
type st_1 from statictext within w_contatti_ricerca_response
end type
type dw_find from u_dw_find within w_contatti_ricerca_response
end type
type cb_ok from commandbutton within w_contatti_ricerca_response
end type
type cb_codice from uo_cb_ok within w_contatti_ricerca_response
end type
type cb_rag_soc_1 from uo_cb_ok within w_contatti_ricerca_response
end type
type dw_folder from u_folder within w_contatti_ricerca_response
end type
type dw_clienti_lista from uo_cs_xx_dw within w_contatti_ricerca_response
end type
end forward

global type w_contatti_ricerca_response from w_cs_xx_risposta
int Width=1916
int Height=1461
st_1 st_1
dw_find dw_find
cb_ok cb_ok
cb_codice cb_codice
cb_rag_soc_1 cb_rag_soc_1
dw_folder dw_folder
dw_clienti_lista dw_clienti_lista
end type
global w_contatti_ricerca_response w_contatti_ricerca_response

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


dw_clienti_lista.set_dw_key("cod_azienda")
dw_clienti_lista.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_retrieveasneeded + c_selectonrowfocuschange, &
                                c_default)

dw_clienti_lista.setsort("cod_cliente A")
dw_find.fu_wiredw(dw_clienti_lista, "rag_soc_1")

lw_oggetti[1] = dw_clienti_lista
lw_oggetti[2] = st_1
lw_oggetti[3] = dw_find
lw_oggetti[4] = cb_codice
lw_oggetti[5] = cb_rag_soc_1
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(1, 4)
dw_folder.fu_selecttab(1)

dw_clienti_lista.setfocus()
cb_rag_soc_1.postevent("clicked")
end event

on w_contatti_ricerca_response.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.dw_find=create dw_find
this.cb_ok=create cb_ok
this.cb_codice=create cb_codice
this.cb_rag_soc_1=create cb_rag_soc_1
this.dw_folder=create dw_folder
this.dw_clienti_lista=create dw_clienti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=dw_find
this.Control[iCurrent+3]=cb_ok
this.Control[iCurrent+4]=cb_codice
this.Control[iCurrent+5]=cb_rag_soc_1
this.Control[iCurrent+6]=dw_folder
this.Control[iCurrent+7]=dw_clienti_lista
end on

on w_contatti_ricerca_response.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.dw_find)
destroy(this.cb_ok)
destroy(this.cb_codice)
destroy(this.cb_rag_soc_1)
destroy(this.dw_folder)
destroy(this.dw_clienti_lista)
end on

type st_1 from statictext within w_contatti_ricerca_response
int X=23
int Y=21
int Width=755
int Height=61
boolean Enabled=false
string Text="Ricerca per Ragione Sociale:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_find from u_dw_find within w_contatti_ricerca_response
int X=801
int Y=21
int Width=1052
int Height=81
int TabOrder=40
end type

type cb_ok from commandbutton within w_contatti_ricerca_response
event clicked pbm_bnclicked
int X=1486
int Y=1261
int Width=366
int Height=81
int TabOrder=50
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
	s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, dw_clienti_lista.getitemstring(dw_clienti_lista.getrow(),"cod_contatto"))
	s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
else
	s_cs_xx.parametri.parametro_uo_dw_search.setcolumn(s_cs_xx.parametri.parametro_s_1)
	s_cs_xx.parametri.parametro_uo_dw_search.setitem(s_cs_xx.parametri.parametro_uo_dw_search.getrow(), s_cs_xx.parametri.parametro_s_1, dw_clienti_lista.getitemstring(dw_clienti_lista.getrow(),"cod_contatto"))
	s_cs_xx.parametri.parametro_uo_dw_search.triggerevent(itemchanged!)
end if	

close(parent)
end event

type cb_codice from uo_cb_ok within w_contatti_ricerca_response
event clicked pbm_bnclicked
int X=46
int Y=221
int Width=343
int Height=81
int TabOrder=30
string Text="Codice"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_clienti_lista.setsort("cod_contatto A")
dw_clienti_lista.sort()

dw_find.fu_wiredw(dw_clienti_lista, "cod_contatto")

st_1.text = "Ricerca per Codice:"
dw_find.setfocus()



end event

type cb_rag_soc_1 from uo_cb_ok within w_contatti_ricerca_response
event clicked pbm_bnclicked
int X=389
int Y=221
int Width=1212
int Height=81
int TabOrder=10
string Text="Ragione Sociale"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;dw_find.fu_unwiredw()

dw_clienti_lista.setsort("rag_soc_1 A")
dw_clienti_lista.sort()

dw_find.fu_wiredw(dw_clienti_lista, "rag_soc_1")

st_1.text = "Ricerca per Ragione Sociale:"
dw_find.setfocus()
end event

type dw_folder from u_folder within w_contatti_ricerca_response
int X=23
int Y=121
int Width=1829
int Height=1121
int TabOrder=2
end type

type dw_clienti_lista from uo_cs_xx_dw within w_contatti_ricerca_response
event doubleclicked pbm_dwnlbuttondblclk
event pcd_retrieve pbm_custom60
int X=46
int Y=221
int Width=1669
int Height=997
int TabOrder=20
string DataObject="d_contatti_lista"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   cb_ok.postevent(clicked!)
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
datetime ldd_oggi

ldd_oggi = datetime(today())
ll_errore = retrieve(s_cs_xx.cod_azienda, ldd_oggi)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

