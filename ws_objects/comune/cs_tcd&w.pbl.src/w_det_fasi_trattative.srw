﻿$PBExportHeader$w_det_fasi_trattative.srw
$PBExportComments$Finestra Gestione Dettaglio Fasi Trattative
forward
global type w_det_fasi_trattative from w_cs_xx_principale
end type
type cb_note_esterne from commandbutton within w_det_fasi_trattative
end type
type cb_corrispondenze from commandbutton within w_det_fasi_trattative
end type
type cb_note from commandbutton within w_det_fasi_trattative
end type
type dw_det_fasi_trattative_lista from uo_cs_xx_dw within w_det_fasi_trattative
end type
type dw_det_fasi_trattative_det from uo_cs_xx_dw within w_det_fasi_trattative
end type
end forward

global type w_det_fasi_trattative from w_cs_xx_principale
integer width = 3031
integer height = 1824
string title = "Gestione Dettaglio Fasi Trattative"
cb_note_esterne cb_note_esterne
cb_corrispondenze cb_corrispondenze
cb_note cb_note
dw_det_fasi_trattative_lista dw_det_fasi_trattative_lista
dw_det_fasi_trattative_det dw_det_fasi_trattative_det
end type
global w_det_fasi_trattative w_det_fasi_trattative

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_fasi_trattative_lista.set_dw_key("cod_azienda")
dw_det_fasi_trattative_lista.set_dw_key("anno_trattativa")
dw_det_fasi_trattative_lista.set_dw_key("num_trattativa")
dw_det_fasi_trattative_lista.set_dw_key("prog_trattativa")
dw_det_fasi_trattative_lista.set_dw_key("cod_tipo_trattativa")
dw_det_fasi_trattative_lista.set_dw_options(sqlca, &
                                            i_openparm, &
                                            c_scrollparent, &
                                            c_default)
dw_det_fasi_trattative_det.set_dw_options(sqlca, &
                                          dw_det_fasi_trattative_lista, &
                                          c_sharedata + c_scrollparent, &
                                          c_default)

iuo_dw_main=dw_det_fasi_trattative_lista

cb_note_esterne.enabled = false
cb_corrispondenze.enabled = false
cb_note.enabled = false
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_fasi_trattative_det, &
                 "cod_fase_trattativa", &
                 sqlca, &
                 "tab_fasi_trattative", &
                 "cod_fase_trattativa", &
                 "des_fase_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_fasi_trattative_det, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

on pc_delete;call w_cs_xx_principale::pc_delete;cb_note_esterne.enabled = false
cb_corrispondenze.enabled = false

end on

on w_det_fasi_trattative.create
int iCurrent
call super::create
this.cb_note_esterne=create cb_note_esterne
this.cb_corrispondenze=create cb_corrispondenze
this.cb_note=create cb_note
this.dw_det_fasi_trattative_lista=create dw_det_fasi_trattative_lista
this.dw_det_fasi_trattative_det=create dw_det_fasi_trattative_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_note_esterne
this.Control[iCurrent+2]=this.cb_corrispondenze
this.Control[iCurrent+3]=this.cb_note
this.Control[iCurrent+4]=this.dw_det_fasi_trattative_lista
this.Control[iCurrent+5]=this.dw_det_fasi_trattative_det
end on

on w_det_fasi_trattative.destroy
call super::destroy
destroy(this.cb_note_esterne)
destroy(this.cb_corrispondenze)
destroy(this.cb_note)
destroy(this.dw_det_fasi_trattative_lista)
destroy(this.dw_det_fasi_trattative_det)
end on

type cb_note_esterne from commandbutton within w_det_fasi_trattative
integer x = 2606
integer y = 120
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_tipo_trattativa, ls_cod_fase_trattativa, ls_db
integer li_i, li_risposta
long ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa, ll_prog_riga

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_det_fasi_trattative_lista.getrow()
ll_anno_trattativa = dw_det_fasi_trattative_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_det_fasi_trattative_lista.getitemnumber(li_i, "num_trattativa")
ll_prog_trattativa = dw_det_fasi_trattative_lista.getitemnumber(li_i, "prog_trattativa")
ls_cod_tipo_trattativa = dw_det_fasi_trattative_lista.getitemstring(li_i, "cod_tipo_trattativa")
ls_cod_fase_trattativa = dw_det_fasi_trattative_lista.getitemstring(li_i, "cod_fase_trattativa")
ll_prog_riga = dw_det_fasi_trattative_lista.getitemnumber(li_i, "prog_riga")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)

	selectblob det_fasi_trattative.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_fasi_trattative
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           prog_trattativa = :ll_prog_trattativa and 
	           cod_tipo_trattativa = :ls_cod_tipo_trattativa and 
	           cod_fase_trattativa = :ls_cod_fase_trattativa and 
	           prog_riga = :ll_prog_riga
	using      sqlcb;

	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else

	selectblob det_fasi_trattative.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       det_fasi_trattative
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           prog_trattativa = :ll_prog_trattativa and 
	           cod_tipo_trattativa = :ls_cod_tipo_trattativa and 
	           cod_fase_trattativa = :ls_cod_fase_trattativa and 
	           prog_riga = :ll_prog_riga;

	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if

end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob det_fasi_trattative
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
	   	        anno_trattativa = :ll_anno_trattativa and 
	      	     num_trattativa = :ll_num_trattativa and 
	         	  prog_trattativa = :ll_prog_trattativa and 
		           cod_tipo_trattativa = :ls_cod_tipo_trattativa and 
	   	        cod_fase_trattativa = :ls_cod_fase_trattativa and 
	      	     prog_riga = :ll_prog_riga
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob det_fasi_trattative
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
		where      cod_azienda = :s_cs_xx.cod_azienda and
	   	        anno_trattativa = :ll_anno_trattativa and 
	      	     num_trattativa = :ll_num_trattativa and 
	         	  prog_trattativa = :ll_prog_trattativa and 
		           cod_tipo_trattativa = :ls_cod_tipo_trattativa and 
	   	        cod_fase_trattativa = :ls_cod_fase_trattativa and 
	      	     prog_riga = :ll_prog_riga;
					  
	end if
	
   commit;
end if
end event

type cb_corrispondenze from commandbutton within w_det_fasi_trattative
integer x = 2606
integer y = 220
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_det_fasi_trat_corrispondenze, -1, dw_det_fasi_trattative_lista)

end on

type cb_note from commandbutton within w_det_fasi_trattative
integer x = 2606
integer y = 20
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_det_fasi_trattative_note, -1, dw_det_fasi_trattative_lista)
end on

type dw_det_fasi_trattative_lista from uo_cs_xx_dw within w_det_fasi_trattative
integer x = 23
integer y = 20
integer width = 2560
integer height = 500
integer taborder = 10
string dataobject = "d_det_fasi_trattative_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_trattativa") > 0 then
      cb_note_esterne.enabled = true
      cb_corrispondenze.enabled = true
      cb_note.enabled = true
   else
      cb_note_esterne.enabled = false
      cb_corrispondenze.enabled = false
      cb_note.enabled = false
   end if
end if
end on

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_trattativa") > 0 then
      cb_note_esterne.enabled = true
      cb_corrispondenze.enabled = true
      cb_note.enabled = true
   else
      cb_note_esterne.enabled = false
      cb_corrispondenze.enabled = false
      cb_note.enabled = false
   end if
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_prog_corrispondenza, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa
string ls_cod_tipo_trattativa

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")
ll_prog_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_trattativa")

select tes_trattative.cod_tipo_trattativa
into   :ls_cod_tipo_trattativa
from   tes_trattative
where  tes_trattative.cod_azienda = :s_cs_xx.cod_azienda and
       tes_trattative.anno_trattativa = :ll_anno_trattativa and 
       tes_trattative.num_trattativa = :ll_num_trattativa;

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then
      this.setitem(ll_i, "anno_trattativa", ll_anno_trattativa)
   end if
   if this.getitemnumber(ll_i, "num_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "num_trattativa")) then
      this.setitem(ll_i, "num_trattativa", ll_num_trattativa)
   end if
   if isnull(this.getitemnumber(ll_i, "prog_trattativa")) or &
      this.getitemnumber(ll_i, "prog_trattativa") = 0 then
      this.setitem(ll_i, "prog_trattativa", ll_prog_trattativa)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_trattativa")) then
      this.setitem(ll_i, "cod_tipo_trattativa", ls_cod_tipo_trattativa)
   end if
next
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;if i_extendmode then
   long ll_prog_trattativa, ll_anno_trattativa, ll_num_trattativa, ll_prog_riga

   ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
   ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")
   ll_prog_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_trattativa")

   select max(det_fasi_trattative.prog_riga)
   into   :ll_prog_riga
   from   det_fasi_trattative
   where  det_fasi_trattative.cod_azienda = :s_cs_xx.cod_azienda and
          det_fasi_trattative.anno_trattativa = :ll_anno_trattativa and
          det_fasi_trattative.num_trattativa = :ll_num_trattativa and
          det_fasi_trattative.prog_trattativa = :ll_prog_trattativa;

   if not isnull(ll_prog_trattativa) then
      this.setitem(this.getrow(), "prog_riga", ll_prog_riga + 10)
   else
      this.setitem(this.getrow(), "prog_riga", 10)
   end if

   cb_note_esterne.enabled = false
   cb_corrispondenze.enabled = false
   cb_note.enabled = false
end if
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa
string ls_cod_tipo_trattativa


ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")
ll_prog_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_trattativa")

select tes_trattative.cod_tipo_trattativa
into   :ls_cod_tipo_trattativa
from   tes_trattative
where  tes_trattative.cod_azienda = :s_cs_xx.cod_azienda and
       tes_trattative.anno_trattativa = :ll_anno_trattativa and 
       tes_trattative.num_trattativa = :ll_num_trattativa;

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_trattativa, ll_num_trattativa, ll_prog_trattativa, ls_cod_tipo_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;if i_extendmode then
   cb_note_esterne.enabled = false
   cb_corrispondenze.enabled = false
   cb_note.enabled = false
end if
end on

type dw_det_fasi_trattative_det from uo_cs_xx_dw within w_det_fasi_trattative
integer x = 23
integer y = 540
integer width = 2949
integer height = 1160
integer taborder = 20
string dataobject = "d_det_fasi_trattative_det"
borderstyle borderstyle = styleraised!
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   double ld_costo


   choose case i_colname
      case "cod_fase_trattativa"
         select tab_fasi_trattative.costo
         into   :ld_costo
         from   tab_fasi_trattative
         where  tab_fasi_trattative.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_fasi_trattative.cod_fase_trattativa = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "costo", ld_costo)
         end if
   end choose
end if
end on

event pcd_modify;call super::pcd_modify;dw_det_fasi_trattative_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_new;call super::pcd_new;dw_det_fasi_trattative_det.object.b_ricerca_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;dw_det_fasi_trattative_det.object.b_ricerca_fornitore.enabled = false
end event

event updateend;call super::updateend;//cb_ricerca_fornitore.enabled = false
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_det_fasi_trattative_det,"cod_fornitore")
end choose
end event

