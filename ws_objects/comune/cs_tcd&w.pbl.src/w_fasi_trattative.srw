﻿$PBExportHeader$w_fasi_trattative.srw
$PBExportComments$Finestra Gestione Fasi Trattative
forward
global type w_fasi_trattative from w_cs_xx_principale
end type
type dw_fasi_trattative from uo_cs_xx_dw within w_fasi_trattative
end type
end forward

global type w_fasi_trattative from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2866
integer height = 1144
string title = "Gestione Fasi Trattative"
dw_fasi_trattative dw_fasi_trattative
end type
global w_fasi_trattative w_fasi_trattative

type variables

end variables

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_fasi_trattative.set_dw_key("cod_azienda")
dw_fasi_trattative.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)

end on

on w_fasi_trattative.create
int iCurrent
call super::create
this.dw_fasi_trattative=create dw_fasi_trattative
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fasi_trattative
end on

on w_fasi_trattative.destroy
call super::destroy
destroy(this.dw_fasi_trattative)
end on

type dw_fasi_trattative from uo_cs_xx_dw within w_fasi_trattative
integer x = 23
integer y = 20
integer width = 2789
integer height = 1000
integer taborder = 10
string dataobject = "d_fasi_trattative"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

