﻿$PBExportHeader$w_genera_clienti.srw
$PBExportComments$Finestra Generazione Clienti
forward
global type w_genera_clienti from w_cs_xx_risposta
end type
type dw_1 from uo_std_dw within w_genera_clienti
end type
type cb_1 from uo_cb_close within w_genera_clienti
end type
type cb_genera from uo_cb_ok within w_genera_clienti
end type
type st_2 from statictext within w_genera_clienti
end type
end forward

global type w_genera_clienti from w_cs_xx_risposta
integer width = 2135
integer height = 860
string title = "Generazione Cliente"
dw_1 dw_1
cb_1 cb_1
cb_genera cb_genera
st_2 st_2
end type
global w_genera_clienti w_genera_clienti

type variables
uo_impresa iuo_impresa
end variables

event pc_accept;call super::pc_accept;string			ls_rag_soc_1_old, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, &
				ls_rif_interno, ls_flag_tipo_cliente, ls_cod_conto, ls_cod_iva, ls_num_prot_esenzione_iva, ls_flag_sospensione_iva, ls_cod_pagamento, &
				ls_cod_tipo_listino_prodotto, ls_flag_fuori_fido, ls_cod_banca_clien_for, ls_conto_corrente, ls_cod_lingua, ls_cod_nazione, ls_cod_area, ls_cod_zona, &
				ls_cod_valuta, ls_cod_categoria, ls_cod_agente_1, ls_cod_agente_2, ls_cod_imballo, ls_cod_porto, ls_cod_resa, ls_cod_mezzo, ls_cod_vettore, ls_cod_inoltro, &
				ls_cod_deposito, ls_flag_riep_boll, ls_flag_riep_fatt, ls_cod_contatto, ls_cod_cliente, ls_casella_mail, ls_flag_raggruppo_iva_doc, ls_messaggio, ls_cod_abi, &
				ls_cod_cab, ls_cod_abi_1, ls_cod_cab_1, ls_cin, ls_cin_1
long			ll_i, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2, ll_fido, ll_risposta,li_id_anagrafica, &
				li_id_sog_commerciale, ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a, ll_null
double		ld_sconto
datetime		ldt_data_esenzione_iva, ldt_data_oggi

dw_1.accepttext()
ldt_data_oggi = datetime(today(),00:00:00)
setnull(ll_null)

ls_cod_cliente = trim(dw_1.getitemstring(1, "cod_cliente"))

ll_risposta = g_mb.messagebox("APICE","Sei sicuro di voler creare il cliente da questo contatto?",Question!, YesNo!, 2)
if ll_risposta = 2 then return

if isnull(ls_cod_cliente) or ls_cod_cliente = "" then
	g_mb.messagebox("Attenzione", "Inserire un codice cliente.", exclamation!, ok!)
	dw_1.setcolumn("cod_cliente")
	return
end if

select anag_clienti.rag_soc_1
into :ls_rag_soc_1_old
from anag_clienti
where anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 100 then
	g_mb.messagebox("Attenzione", "Codice cliente già presente in anagrafica: " + ls_rag_soc_1_old, exclamation!, ok!)
	dw_1.setcolumn("cod_cliente")
	return
end if

ll_i = s_cs_xx.parametri.parametro_uo_dw_1.getrow()
ls_cod_contatto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_contatto")
ls_rag_soc_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_1")
ls_rag_soc_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rag_soc_2")
ls_indirizzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "indirizzo")
ls_localita = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "localita")
ls_frazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "frazione")
ls_cap = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cap")
ls_provincia = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "provincia")
ls_telefono = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telefono")
ls_fax = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "fax")
ls_telex = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "telex")
ls_cod_fiscale = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_fiscale")
ls_partita_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "partita_iva")
ls_rif_interno = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "rif_interno")
ls_flag_tipo_cliente = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_tipo_cliente")
ls_cod_conto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_conto")
ls_cod_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_iva")
ls_num_prot_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "num_prot_esenzione_iva")
ldt_data_esenzione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemdatetime(ll_i, "data_esenzione_iva")
ls_flag_sospensione_iva = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_sospensione_iva")
ls_cod_pagamento = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_pagamento")
ll_giorno_fisso_scadenza = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "giorno_fisso_scadenza")
ll_mese_esclusione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_1")
ll_mese_esclusione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "mese_esclusione_2")

ll_ggmm_esclusione_1_da	= s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_da")
ll_ggmm_esclusione_1_a		= s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_1_a")
ll_ggmm_esclusione_2_da	= s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_da")
ll_ggmm_esclusione_2_a		= s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "ggmm_esclusione_2_a")

ll_data_sostituzione_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_1")
ll_data_sostituzione_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "data_sostituzione_2")
ld_sconto = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "sconto")
ls_cod_tipo_listino_prodotto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_tipo_listino_prodotto")
ll_fido = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(ll_i, "fido")
ls_flag_fuori_fido = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_fuori_fido")
ls_cod_banca_clien_for = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_banca_clien_for")
ls_conto_corrente = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "conto_corrente")
ls_cod_lingua = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_lingua")
ls_cod_nazione = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_nazione")
ls_cod_area = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_area")
ls_cod_zona = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_zona")
ls_cod_valuta = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_valuta")
ls_cod_categoria = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_categoria")
ls_cod_agente_1 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_agente_1")
ls_cod_agente_2 = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_agente_2")
ls_cod_imballo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_imballo")
ls_cod_porto = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_porto")
ls_cod_resa = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_resa")
ls_cod_mezzo = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_mezzo")
ls_cod_vettore = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_vettore")
ls_cod_inoltro = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_inoltro")
ls_cod_deposito = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "cod_deposito")
ls_flag_riep_boll = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_riep_boll")
ls_flag_riep_fatt = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_riep_fatt")
ls_casella_mail = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "casella_mail")
ls_flag_raggruppo_iva_doc = s_cs_xx.parametri.parametro_uo_dw_1.getitemstring(ll_i, "flag_raggruppo_iva_doc")

// stefanop 24/04/2012: nuova parametri in base alle modifiche fatte nella finestra w_clienti
// ---------------------------------------
if guo_functions.uof_is_gibus() then
	if not isnull(dw_1.getitemstring(1, "cod_deposito")) and dw_1.getitemstring(1, "cod_deposito") <> "" then
		ls_cod_deposito = dw_1.getitemstring(1, "cod_deposito")
	end if
	
	if not isnull(dw_1.getitemstring(1, "cod_capoconto")) and dw_1.getitemstring(1, "cod_capoconto") <> "" then
		ls_cod_conto = dw_1.getitemstring(1, "cod_capoconto")
	end if
end if
// ---------------------------------------


insert into anag_clienti  
            (cod_azienda,   
             cod_cliente,   
             rag_soc_1,   
             rag_soc_2,   
             indirizzo,   
             localita,   
             frazione,   
             cap,   
             provincia,   
             telefono,   
             fax,   
             telex,   
             cod_fiscale,   
             partita_iva,   
             rif_interno,   
             flag_tipo_cliente,   
             cod_capoconto,   
             cod_conto,   
             cod_iva,   
             num_prot_esenzione_iva,   
             data_esenzione_iva,   
             flag_sospensione_iva,   
             cod_pagamento,   
             giorno_fisso_scadenza,   
             mese_esclusione_1,   
             mese_esclusione_2,
			ggmm_esclusione_1_da,
			ggmm_esclusione_1_a,
			ggmm_esclusione_2_da,
			ggmm_esclusione_2_a,
             data_sostituzione_1,   
             data_sostituzione_2,   
             sconto,   
             cod_tipo_listino_prodotto,   
             fido,   
             flag_fuori_fido,   
             cod_banca_clien_for,   
             conto_corrente,   
             cod_lingua,   
             cod_nazione,   
             cod_area,   
             cod_zona,   
             cod_valuta,   
             cod_categoria,   
             cod_agente_1,   
             cod_agente_2,   
             cod_imballo,   
             cod_porto,   
             cod_resa,   
             cod_mezzo,   
             cod_vettore,   
             cod_inoltro,   
             cod_deposito,   
             flag_riep_boll,   
             flag_riep_fatt,   
             flag_blocco,   
             data_blocco,
			casella_mail,
			flag_raggruppo_iva_doc,
			flag_raggruppo_scadenze,
			data_creazione)
values      (:s_cs_xx.cod_azienda,
             :ls_cod_cliente,
             :ls_rag_soc_1,   
             :ls_rag_soc_2,   
             :ls_indirizzo,   
             :ls_localita,   
             :ls_frazione,   
             :ls_cap,   
             :ls_provincia,   
             :ls_telefono,   
             :ls_fax,   
             :ls_telex,   
             :ls_cod_fiscale,   
             :ls_partita_iva,   
             :ls_rif_interno,   
             :ls_flag_tipo_cliente,   
             :ls_cod_conto,   
             null,   
             :ls_cod_iva,   
             :ls_num_prot_esenzione_iva,   
             :ldt_data_esenzione_iva,   
             :ls_flag_sospensione_iva,   
             :ls_cod_pagamento,   
             :ll_giorno_fisso_scadenza,   
             :ll_mese_esclusione_1,   
             :ll_mese_esclusione_2,
			:ll_ggmm_esclusione_1_da,
			:ll_ggmm_esclusione_1_a,
			:ll_ggmm_esclusione_2_da,
			:ll_ggmm_esclusione_2_a,
             :ll_data_sostituzione_1,   
             :ll_data_sostituzione_2,   
             :ld_sconto,   
             :ls_cod_tipo_listino_prodotto,   
             :ll_fido,   
             :ls_flag_fuori_fido,   
             :ls_cod_banca_clien_for,   
             :ls_conto_corrente,   
             :ls_cod_lingua,   
             :ls_cod_nazione,   
             :ls_cod_area,   
             :ls_cod_zona,   
             :ls_cod_valuta,   
             :ls_cod_categoria,   
             :ls_cod_agente_1,   
             :ls_cod_agente_2,   
             :ls_cod_imballo,   
             :ls_cod_porto,   
             :ls_cod_resa,   
             :ls_cod_mezzo,   
             :ls_cod_vettore,   
             :ls_cod_inoltro,   
             :ls_cod_deposito,   
             :ls_flag_riep_boll,   
             :ls_flag_riep_fatt,   
             'N',   
             null,
			:ls_casella_mail,
			:ls_flag_raggruppo_iva_doc,
			'S',
			:ldt_data_oggi);

if sqlca.sqlcode <> 0 then
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione Cliente.", exclamation!, ok!)
   rollback;
	return
end if

update anag_contatti  
set    cod_cliente = :ls_cod_cliente
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_contatto = :ls_cod_contatto;

if sqlca.sqlcode = 0 then
   s_cs_xx.parametri.parametro_uo_dw_1.setitem(ll_i, "cod_cliente", ls_cod_cliente)   
   s_cs_xx.parametri.parametro_uo_dw_1.setitemstatus(ll_i, "cod_cliente", primary!, notmodified!)
   commit;
else
  	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione Cliente.", &
              exclamation!, ok!)
   rollback;
	return
end if

if dw_1.getitemstring(1, "flag_blocco") = "S"  then
	update anag_contatti  
	set    flag_blocco = 'S',
	       data_blocco = :ldt_data_oggi
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_contatto = :ls_cod_contatto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SME","Errore durante il blocco del contatto. Errore " + string(sqlca.sqlcode) + " Dettaglio: " + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

// -------------------  creazione del cliente in contabilità se abilitata -----------------------------------
if s_cs_xx.parametri.impresa then
	if isnull(ls_cod_conto) and len(ls_cod_cliente) < 4 then
		g_mb.messagebox("APICE","Codice capoconto obbligatorio !")
		rollback;
		return
	end if
	if isnull(ls_cod_banca_clien_for) then
		setnull(ls_cod_abi)
		setnull(ls_cod_cab)
		setnull(ls_cin)
	else
		select cod_abi, cod_cab, cin
		into   :ls_cod_abi, :ls_cod_cab, :ls_cin
		from   anag_banche_clien_for
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_banca_clien_for = :ls_cod_banca_clien_for;
	end if			
	setnull(ls_cod_abi_1)
	setnull(ls_cod_cab_1)
	setnull(ls_cin_1)
	
	//Donato 03-11-2008 Modifica per gestione telefono, fax e cellulare, e cod_agente_1
	//16/11/2012 Donato: Passo vuoto il campo email_amministrazione, in quanto non è previsto qui l'inserimento del dato in anag_clienti di APICE
	//		(vedi INSERT precedente)
	if iuo_impresa.uof_crea_cli_for(	"C", &
												ls_cod_cliente, &
												ls_cod_conto, &
												ls_partita_iva,&
												ls_cod_fiscale, &
												ls_rag_soc_1,&
												ls_rag_soc_2, &
												ls_indirizzo,&
												ls_localita, &
												ls_cap, &
												ls_provincia, &
												ls_frazione, &
												ls_cod_abi, &
												ls_cod_cab, &
												ls_cin, &
												ls_cod_abi_1, &
												ls_cod_cab_1, &
												ls_cin_1, &
												ls_cod_pagamento, &
												ls_cod_valuta, &
												ls_cod_nazione, &
												ll_ggmm_esclusione_1_da,   &
												ll_ggmm_esclusione_1_a,   &
												ll_ggmm_esclusione_2_da,   &
												ll_ggmm_esclusione_2_a,   &
												ll_data_sostituzione_1,  & 
												ll_data_sostituzione_2,   &
												'S',&
												ls_telefono, ls_fax, ls_telex, "", ls_cod_agente_1,&
												"", &
												"", &
												ll_null, &
												ll_null, &
												ls_flag_tipo_cliente, &
												ref li_id_anagrafica, &
												ref li_id_sog_commerciale ) = -1 then 
		g_mb.messagebox("APICE",iuo_impresa.i_messaggio)
		rollback;
		return
	end if
	
	commit using sqlci;
end if

// ----------------------------------------------------------------------------------------------------------

// stefanop 08/01/2014: spec. Pubblicazioni_dati_web
// Cambio il codice di login tra cliente e contatto
string ls_cod_login

select cod_login
into :ls_cod_login
from tab_login_esterni
where cod_azienda = :s_cs_xx.cod_azienda and cod_contatto = :ls_cod_contatto;

if sqlca.sqlcode = 0 and not isnull(ls_cod_login) and ls_cod_login <> "" then
	
	// Aggiorno la login con il codice cliente.
	update tab_login_esterni
	set cod_cliente = :ls_cod_cliente, cod_contatto = null
	where cod_azienda = :s_cs_xx.cod_azienda and cod_login = :ls_cod_login;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante l'aggiornamento della tabella dei login esterni.", sqlca)
	end if
	
end if
// ------------------

commit;
close(this)
end event

event pc_setddlb;call super::pc_setddlb;string ls_str

f_po_loaddddw_dw(dw_1, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_1, &
	"cod_clienti", &
	sqlca, &
	"anag_clienti", &
	"cod_cliente", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	
if s_cs_xx.parametri.impresa then
	
	select stringa
	into   :ls_str
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'MCL';
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Verificare l'esistenza del parametro MCL in parametri azienda")
	else
		f_po_loaddddw_dw(dw_1, &
							  "cod_capoconto", &
							  sqlci, &
							  "sottomastro", &
							  "codice", &
							  "descrizione", &
							  "id_mastro in (select id_mastro from mastro where codice = '"+ ls_str +"')")
	end if
end if

end event

on w_genera_clienti.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.cb_1=create cb_1
this.cb_genera=create cb_genera
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_genera
this.Control[iCurrent+4]=this.st_2
end on

on w_genera_clienti.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.cb_1)
destroy(this.cb_genera)
destroy(this.st_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.insertrow(0)

if not isnull(s_cs_xx.parametri.parametro_s_1) and s_cs_xx.parametri.parametro_s_1 <> "" then
	dw_1.setitem(1, "cod_cliente", s_cs_xx.parametri.parametro_s_1)
end if
end event

event open;call super::open;iuo_impresa = CREATE uo_impresa
end event

event close;call super::close;destroy iuo_impresa
end event

type dw_1 from uo_std_dw within w_genera_clienti
integer x = 23
integer y = 120
integer width = 2057
integer height = 520
integer taborder = 50
string dataobject = "d_genera_clienti"
end type

type cb_1 from uo_cb_close within w_genera_clienti
integer x = 1326
integer y = 660
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

type cb_genera from uo_cb_ok within w_genera_clienti
integer x = 1714
integer y = 660
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

type st_2 from statictext within w_genera_clienti
integer x = 23
integer y = 20
integer width = 2057
integer height = 80
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "GENERAZIONE CLIENTE DA CONTATTO"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

