﻿$PBExportHeader$w_lista_progetti.srw
$PBExportComments$Window lista progetti
forward
global type w_lista_progetti from w_cs_xx_risposta
end type
type dw_lista_progetti from uo_cs_xx_dw within w_lista_progetti
end type
type st_1 from statictext within w_lista_progetti
end type
type em_anno_progetto from editmask within w_lista_progetti
end type
type cb_ok from commandbutton within w_lista_progetti
end type
type cb_annulla from commandbutton within w_lista_progetti
end type
type cb_aggiorna from commandbutton within w_lista_progetti
end type
end forward

global type w_lista_progetti from w_cs_xx_risposta
int Width=2963
int Height=1921
boolean TitleBar=true
string Title="Lista progetti per Trattative"
dw_lista_progetti dw_lista_progetti
st_1 st_1
em_anno_progetto em_anno_progetto
cb_ok cb_ok
cb_annulla cb_annulla
cb_aggiorna cb_aggiorna
end type
global w_lista_progetti w_lista_progetti

type variables
integer ii_ok
end variables

on w_lista_progetti.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_progetti=create dw_lista_progetti
this.st_1=create st_1
this.em_anno_progetto=create em_anno_progetto
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.cb_aggiorna=create cb_aggiorna
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_progetti
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=em_anno_progetto
this.Control[iCurrent+4]=cb_ok
this.Control[iCurrent+5]=cb_annulla
this.Control[iCurrent+6]=cb_aggiorna
end on

on w_lista_progetti.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_progetti)
destroy(this.st_1)
destroy(this.em_anno_progetto)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.cb_aggiorna)
end on

event pc_setwindow;call super::pc_setwindow;em_anno_progetto.text = string(year(today()))

dw_lista_progetti.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                     c_default, &
                                     c_default)
												 

ii_ok=-1
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1
end event

type dw_lista_progetti from uo_cs_xx_dw within w_lista_progetti
int X=23
int Y=21
int Width=2881
int Height=1681
int TabOrder=20
string DataObject="d_lista_progetti"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
integer li_anno_progetti

li_anno_progetti = integer(em_anno_progetto.text)


ll_errore = retrieve(s_cs_xx.cod_azienda, li_anno_progetti)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event doubleclicked;call super::doubleclicked;cb_ok.triggerevent("clicked")
end event

type st_1 from statictext within w_lista_progetti
int X=23
int Y=1721
int Width=380
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Anno Progetti:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_anno_progetto from editmask within w_lista_progetti
int X=412
int Y=1721
int Width=321
int Height=81
int TabOrder=40
boolean BringToTop=true
Alignment Alignment=Center!
string Mask="yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData="À"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from commandbutton within w_lista_progetti
int X=2538
int Y=1721
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_2 = dw_lista_progetti.getitemnumber(dw_lista_progetti.getrow(),"anno_progetto")
s_cs_xx.parametri.parametro_ul_1 = dw_lista_progetti.getitemnumber(dw_lista_progetti.getrow(),"num_versione")
s_cs_xx.parametri.parametro_ul_2 = dw_lista_progetti.getitemnumber(dw_lista_progetti.getrow(),"num_edizione")
s_cs_xx.parametri.parametro_s_1 = dw_lista_progetti.getitemstring(dw_lista_progetti.getrow(),"cod_progetto")

s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

close (parent)

end event

type cb_annulla from commandbutton within w_lista_progetti
int X=2149
int Y=1721
int Width=366
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
ii_ok = 0
close (parent)
end event

type cb_aggiorna from commandbutton within w_lista_progetti
int X=755
int Y=1721
int Width=366
int Height=81
int TabOrder=31
boolean BringToTop=true
string Text="A&ggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;dw_lista_progetti.triggerevent("pcd_retrieve")
end event

