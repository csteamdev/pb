﻿$PBExportHeader$w_report_pros_corrispondenze.srw
$PBExportComments$Finestra Report Prossime Corrispondenze
forward
global type w_report_pros_corrispondenze from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_pros_corrispondenze
end type
type cb_report from commandbutton within w_report_pros_corrispondenze
end type
type cb_selezione from commandbutton within w_report_pros_corrispondenze
end type
type dw_selezione from uo_cs_xx_dw within w_report_pros_corrispondenze
end type
type dw_report from uo_cs_xx_dw within w_report_pros_corrispondenze
end type
end forward

global type w_report_pros_corrispondenze from w_cs_xx_principale
integer width = 3552
integer height = 1664
string title = "Report Prossime Corrispondenze"
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_selezione dw_selezione
dw_report dw_report
end type
global w_report_pros_corrispondenze w_report_pros_corrispondenze

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 741
this.y = 885
this.width = 2172
this.height = 633

end event

on w_report_pros_corrispondenze.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.cb_selezione
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_pros_corrispondenze.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_selezione)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_selezione,"rs_cod_operatore",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event resize;/* TOLTO ANCESTOR SCRIPT */

cb_selezione.move(newwidth - cb_selezione.width - 20, newheight - cb_selezione.height - 20)

dw_report.move (20, 20)
dw_report.resize(newwidth - 60, cb_selezione.y - 30)
end event

type cb_annulla from commandbutton within w_report_pros_corrispondenze
integer x = 1349
integer y = 420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_pros_corrispondenze
integer x = 1737
integer y = 420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_operaio
date   ld_da_data, ld_a_data


ls_cod_operaio = dw_selezione.getitemstring(1,"rs_cod_operatore")
ld_da_data  = dw_selezione.getitemdate(1,"rdd_da_data")
ld_a_data   = dw_selezione.getitemdate(1,"rdd_a_data")

if isnull(ls_cod_operaio) then ls_cod_operaio = "%"
if isnull(ld_da_data) then  ld_da_data  = date("01/01/1900")
if isnull(ld_a_data)  then  ld_a_data   = date("31/12/2999")

dw_selezione.hide()

parent.x = 100
parent.y = 50
parent.width = 3553
parent.height = 1665

dw_report.show()
dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_operaio, ld_da_data, ld_a_data)
cb_selezione.show()

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_pros_corrispondenze
boolean visible = false
integer x = 3131
integer y = 1460
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 741
parent.y = 885
parent.width = 2172
parent.height = 633

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_pros_corrispondenze
integer x = 23
integer y = 20
integer width = 2080
integer height = 380
integer taborder = 20
string dataobject = "d_report_prossime_corr_selezione"
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_pros_corrispondenze
boolean visible = false
integer x = 23
integer y = 20
integer width = 3474
integer height = 1420
integer taborder = 10
string dataobject = "d_report_prossime_corrispondenze"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

