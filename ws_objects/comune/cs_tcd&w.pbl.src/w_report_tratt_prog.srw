﻿$PBExportHeader$w_report_tratt_prog.srw
$PBExportComments$Report che viene aperto da w_tes_trattative
forward
global type w_report_tratt_prog from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_report_tratt_prog
end type
type dw_report from uo_cs_xx_dw within w_report_tratt_prog
end type
type cb_stampa from commandbutton within w_report_tratt_prog
end type
end forward

global type w_report_tratt_prog from w_cs_xx_principale
integer width = 3465
integer height = 3324
string title = "Report Progetti per Trattativa"
boolean hscrollbar = true
boolean vscrollbar = true
cb_annulla cb_annulla
dw_report dw_report
cb_stampa cb_stampa
end type
global w_report_tratt_prog w_report_tratt_prog

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave+c_autoposition + c_noresizewin)
dw_report.Object.DataWindow.Zoom =100
dw_report.settransobject(sqlca)

x = 100
y = 50
width = 3553
height = 2000
dw_report.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1_a)

end event

on w_report_tratt_prog.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.dw_report=create dw_report
this.cb_stampa=create cb_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.cb_stampa
end on

on w_report_tratt_prog.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.dw_report)
destroy(this.cb_stampa)
end on

type cb_annulla from commandbutton within w_report_tratt_prog
integer x = 411
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type dw_report from uo_cs_xx_dw within w_report_tratt_prog
integer x = 23
integer y = 120
integer width = 5006
integer height = 4840
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_report_trattative_progetti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_stampa from commandbutton within w_report_tratt_prog
integer x = 18
integer y = 20
integer width = 366
integer height = 80
integer taborder = 1
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long Job

Job = PrintOpen( )
PrintDataWindow(job, dw_report) 
PrintClose(job)
end event

