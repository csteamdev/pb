﻿$PBExportHeader$w_report_trattative.srw
forward
global type w_report_trattative from w_cs_xx_principale
end type
type dw_report_trattative from uo_cs_xx_dw within w_report_trattative
end type
end forward

global type w_report_trattative from w_cs_xx_principale
integer width = 3968
integer height = 2224
string title = "Report Trattative di Vendita"
boolean resizable = false
dw_report_trattative dw_report_trattative
end type
global w_report_trattative w_report_trattative

on w_report_trattative.create
int iCurrent
call super::create
this.dw_report_trattative=create dw_report_trattative
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_trattative
end on

on w_report_trattative.destroy
call super::destroy
destroy(this.dw_report_trattative)
end on

event pc_setwindow;call super::pc_setwindow;string ls_logo, ls_modify

dw_report_trattative.ib_dw_report = true

dw_report_trattative.set_dw_options(sqlca, &
	                                 i_openparm, &
   	                              c_nonew + &
       	                           c_nomodify + &
                                 	c_nodelete + &
                                 	c_noenablenewonopen + &
                                 	c_noenablemodifyonopen + &
                                 	c_scrollparent + &
												c_disablecc, &
												c_noresizedw + &
                                 	c_nohighlightselected + &
                                 	c_nocursorrowfocusrect + &
                                 	c_nocursorrowpointer)
												
select parametri_azienda.stringa
into   :ls_logo
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

ls_modify = "logo.filename='" + s_cs_xx.volume + ls_logo + "'~t"
dw_report_trattative.modify(ls_modify)

dw_report_trattative.retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_d_1,s_cs_xx.parametri.parametro_d_2)

dw_report_trattative.object.datawindow.print.preview = 'Yes'
dw_report_trattative.object.datawindow.print.preview.rulers = 'Yes'
end event

type dw_report_trattative from uo_cs_xx_dw within w_report_trattative
integer x = 23
integer y = 20
integer width = 3909
integer height = 2100
integer taborder = 10
string dataobject = "d_report_trattative"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

