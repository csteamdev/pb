﻿$PBExportHeader$w_scadenzario_trattative.srw
$PBExportComments$Finestra Scadenzario Trattative
forward
global type w_scadenzario_trattative from w_cs_xx_principale
end type
type st_indice from statictext within w_scadenzario_trattative
end type
type st_5 from statictext within w_scadenzario_trattative
end type
type dw_scadenzario_trattative from uo_cs_xx_dw within w_scadenzario_trattative
end type
type em_da_data_reg from editmask within w_scadenzario_trattative
end type
type st_1 from statictext within w_scadenzario_trattative
end type
type st_2 from statictext within w_scadenzario_trattative
end type
type em_a_data_reg from editmask within w_scadenzario_trattative
end type
type em_da_data_scad from editmask within w_scadenzario_trattative
end type
type st_3 from statictext within w_scadenzario_trattative
end type
type st_4 from statictext within w_scadenzario_trattative
end type
type em_a_data_scad from editmask within w_scadenzario_trattative
end type
type cbx_conclusa from checkbox within w_scadenzario_trattative
end type
type cbx_esito from checkbox within w_scadenzario_trattative
end type
type cbx_blocco from checkbox within w_scadenzario_trattative
end type
type cb_1 from commandbutton within w_scadenzario_trattative
end type
end forward

global type w_scadenzario_trattative from w_cs_xx_principale
integer width = 3442
integer height = 1480
string title = "Scadenzario Trattative"
st_indice st_indice
st_5 st_5
dw_scadenzario_trattative dw_scadenzario_trattative
em_da_data_reg em_da_data_reg
st_1 st_1
st_2 st_2
em_a_data_reg em_a_data_reg
em_da_data_scad em_da_data_scad
st_3 st_3
st_4 st_4
em_a_data_scad em_a_data_scad
cbx_conclusa cbx_conclusa
cbx_esito cbx_esito
cbx_blocco cbx_blocco
cb_1 cb_1
end type
global w_scadenzario_trattative w_scadenzario_trattative

forward prototypes
public function decimal wf_indice_efficacia ()
end prototypes

public function decimal wf_indice_efficacia ();long     ll_chiuse, ll_positivo

datetime ldt_reg_da, ldt_reg_a, ldt_scad_da, ldt_scad_a


ldt_reg_da = datetime(date(em_da_data_reg.text),00:00:00)

ldt_reg_a = datetime(date(em_a_data_reg.text),00:00:00)

ldt_scad_da = datetime(date(em_da_data_scad.text),00:00:00)

ldt_scad_a = datetime(date(em_a_data_scad.text),00:00:00)

select count(*)
into   :ll_chiuse
from   tes_trattative
where  cod_azienda = :s_cs_xx.cod_azienda and
		 data_registrazione between :ldt_reg_da and :ldt_reg_a and
		 data_scadenza between :ldt_scad_da and :ldt_scad_a and
		 flag_conclusa = 'S';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SME","Errore in conteggio trattative chiuse su tes_trattative: " + sqlca.sqlerrtext)
	return -1
end if

select count(*)
into   :ll_positivo
from   tes_trattative
where  cod_azienda = :s_cs_xx.cod_azienda and
		 data_registrazione between :ldt_reg_da and :ldt_reg_a and
		 data_scadenza between :ldt_scad_da and :ldt_scad_a and
		 flag_conclusa = 'S' and
		 flag_esito = 'S';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SME","Errore in conteggio trattative chiuse con esito positivo su tes_trattative: " + sqlca.sqlerrtext)
	return -1
end if

if ll_positivo = 0 or ll_chiuse = 0 then
	return 0
else	
	return round(ll_positivo/ll_chiuse,2)
end if	
end function

event pc_setwindow;call super::pc_setwindow;dw_scadenzario_trattative.set_dw_options(sqlca, &
                                         pcca.null_object, &
													  c_nonew + &
													  c_nodelete + &
													  c_nomodify + &
													  c_noretrieveonopen + &
													  c_disablecc, &
													  c_default)

iuo_dw_main = dw_scadenzario_trattative

em_da_data_reg.text = "01/01/1900"
em_a_data_reg.text = "31/12/2999"
em_da_data_scad.text = "01/01/1900"
em_a_data_scad.text = "31/12/2999"
end event

on w_scadenzario_trattative.create
int iCurrent
call super::create
this.st_indice=create st_indice
this.st_5=create st_5
this.dw_scadenzario_trattative=create dw_scadenzario_trattative
this.em_da_data_reg=create em_da_data_reg
this.st_1=create st_1
this.st_2=create st_2
this.em_a_data_reg=create em_a_data_reg
this.em_da_data_scad=create em_da_data_scad
this.st_3=create st_3
this.st_4=create st_4
this.em_a_data_scad=create em_a_data_scad
this.cbx_conclusa=create cbx_conclusa
this.cbx_esito=create cbx_esito
this.cbx_blocco=create cbx_blocco
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_indice
this.Control[iCurrent+2]=this.st_5
this.Control[iCurrent+3]=this.dw_scadenzario_trattative
this.Control[iCurrent+4]=this.em_da_data_reg
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.em_a_data_reg
this.Control[iCurrent+8]=this.em_da_data_scad
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.st_4
this.Control[iCurrent+11]=this.em_a_data_scad
this.Control[iCurrent+12]=this.cbx_conclusa
this.Control[iCurrent+13]=this.cbx_esito
this.Control[iCurrent+14]=this.cbx_blocco
this.Control[iCurrent+15]=this.cb_1
end on

on w_scadenzario_trattative.destroy
call super::destroy
destroy(this.st_indice)
destroy(this.st_5)
destroy(this.dw_scadenzario_trattative)
destroy(this.em_da_data_reg)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.em_a_data_reg)
destroy(this.em_da_data_scad)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.em_a_data_scad)
destroy(this.cbx_conclusa)
destroy(this.cbx_esito)
destroy(this.cbx_blocco)
destroy(this.cb_1)
end on

event open;call super::open;st_indice.text = string(wf_indice_efficacia())
end event

type st_indice from statictext within w_scadenzario_trattative
integer x = 526
integer y = 1280
integer width = 402
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_5 from statictext within w_scadenzario_trattative
integer x = 23
integer y = 1280
integer width = 485
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Indice di Efficacia:"
boolean focusrectangle = false
end type

type dw_scadenzario_trattative from uo_cs_xx_dw within w_scadenzario_trattative
integer x = 23
integer y = 200
integer width = 3360
integer height = 1060
integer taborder = 0
string dataobject = "d_scadenzario_trattative"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string   ls_flag_conclusa, ls_flag_esito, ls_flag_blocco
long     l_Error
date     ldd_da_data_reg, ldd_a_data_reg, ldd_da_data_scad, ldd_a_data_scad

ldd_da_data_reg = date(em_da_data_reg.text)
if ldd_da_data_reg <= date("01/01/1900") or isnull(ldd_da_data_reg) then
	ldd_da_data_reg = date("01/01/1900")
end if

ldd_a_data_reg = date(em_a_data_reg.text)
if ldd_a_data_reg <= date("01/01/1900") then
	ldd_a_data_reg = date("31/12/2999")
end if

ldd_da_data_scad = date(em_da_data_scad.text)
if ldd_da_data_scad <= date("01/01/1900") or isnull(ldd_da_data_scad) then
	ldd_da_data_scad = date("01/01/1900")
end if

ldd_a_data_scad = date(em_a_data_scad.text)
if ldd_a_data_scad <= date("01/01/1900") then
	ldd_a_data_scad = date("31/12/2999")
end if

if cbx_conclusa.thirdstate then
	ls_flag_conclusa = "%"
elseif cbx_conclusa.checked then
	ls_flag_conclusa = "S"
else
	ls_flag_conclusa = "N"
end if

if cbx_esito.thirdstate then
	ls_flag_esito = "%"
elseif cbx_esito.checked then
	ls_flag_esito = "S"
else
	ls_flag_esito = "N"
end if

if cbx_blocco.thirdstate then
	ls_flag_blocco = "%"
elseif cbx_blocco.checked then
	ls_flag_blocco = "S"
else
	ls_flag_blocco = "N"
end if


l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   ldd_da_data_reg, &
                   ldd_a_data_reg, &
                   ldd_da_data_scad, &
                   ldd_a_data_scad, &
						 "%", &
						 ls_flag_conclusa, &
						 ls_flag_esito, &
						 ls_flag_blocco)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type em_da_data_reg from editmask within w_scadenzario_trattative
integer x = 640
integer y = 20
integer width = 411
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

event modified;st_indice.text = string(wf_indice_efficacia())
end event

type st_1 from statictext within w_scadenzario_trattative
integer x = 23
integer y = 20
integer width = 599
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Da Data Registrazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_scadenzario_trattative
integer x = 1120
integer y = 20
integer width = 576
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A Data Registrazione:"
boolean focusrectangle = false
end type

type em_a_data_reg from editmask within w_scadenzario_trattative
integer x = 1714
integer y = 20
integer width = 411
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

event modified;st_indice.text = string(wf_indice_efficacia())
end event

type em_da_data_scad from editmask within w_scadenzario_trattative
integer x = 640
integer y = 100
integer width = 411
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

event modified;st_indice.text = string(wf_indice_efficacia())
end event

type st_3 from statictext within w_scadenzario_trattative
integer x = 23
integer y = 100
integer width = 599
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Da Data Scadenza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_scadenzario_trattative
integer x = 1097
integer y = 100
integer width = 576
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A Data Scadenza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_a_data_scad from editmask within w_scadenzario_trattative
integer x = 1714
integer y = 100
integer width = 411
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string displaydata = ""
end type

event modified;st_indice.text = string(wf_indice_efficacia())
end event

type cbx_conclusa from checkbox within w_scadenzario_trattative
integer x = 2217
integer y = 20
integer width = 727
integer height = 76
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Filtra Trattative Concluse"
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
borderstyle borderstyle = stylelowered!
end type

type cbx_esito from checkbox within w_scadenzario_trattative
integer x = 2514
integer y = 100
integer width = 430
integer height = 76
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Esito Positivo"
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
borderstyle borderstyle = stylelowered!
end type

type cbx_blocco from checkbox within w_scadenzario_trattative
integer x = 3086
integer y = 100
integer width = 270
integer height = 76
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Blocco"
boolean lefttext = true
boolean threestate = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_scadenzario_trattative
integer x = 3017
integer y = 1280
integer width = 375
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Es.&Ricerca"
end type

event clicked;parent.triggerevent("pc_retrieve")
end event

