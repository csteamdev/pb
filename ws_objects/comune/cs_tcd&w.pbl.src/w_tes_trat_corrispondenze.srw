﻿$PBExportHeader$w_tes_trat_corrispondenze.srw
$PBExportComments$Finestra Gestione Testate Trattative Corrispondenze
forward
global type w_tes_trat_corrispondenze from w_cs_xx_principale
end type
type dw_tes_trat_corrispondenze_lista from uo_cs_xx_dw within w_tes_trat_corrispondenze
end type
type dw_tes_trat_corrispondenze_det from uo_cs_xx_dw within w_tes_trat_corrispondenze
end type
type cb_note_esterne from commandbutton within w_tes_trat_corrispondenze
end type
type cb_controllo from commandbutton within w_tes_trat_corrispondenze
end type
end forward

global type w_tes_trat_corrispondenze from w_cs_xx_principale
integer width = 3465
integer height = 1788
string title = "Gestione Corrispondenze Testate Trattative"
dw_tes_trat_corrispondenze_lista dw_tes_trat_corrispondenze_lista
dw_tes_trat_corrispondenze_det dw_tes_trat_corrispondenze_det
cb_note_esterne cb_note_esterne
cb_controllo cb_controllo
end type
global w_tes_trat_corrispondenze w_tes_trat_corrispondenze

type variables
integer ii_new_row
end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_trat_corrispondenze_det, &
                 "cod_corrispondenza", &
                 sqlca, &
                 "tab_corrispondenze", &
                 "cod_corrispondenza", &
                 "des_corrispondenza", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trat_corrispondenze_det, &
                 "num_reg_lista", &
                 sqlca, &
                 "tes_liste_controllo", &
                 "num_reg_lista", &
                 "des_lista", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")
f_po_loaddddw_dw(dw_tes_trat_corrispondenze_det, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_tes_trat_corrispondenze_lista.set_dw_key("cod_azienda")
dw_tes_trat_corrispondenze_lista.set_dw_key("anno_trattativa")
dw_tes_trat_corrispondenze_lista.set_dw_key("num_trattativa")
dw_tes_trat_corrispondenze_lista.set_dw_options(sqlca, &
                                                i_openparm, &
                                                c_scrollparent, &
                                                c_default)
dw_tes_trat_corrispondenze_det.set_dw_options(sqlca, &
                                              dw_tes_trat_corrispondenze_lista, &
                                              c_sharedata + c_scrollparent, &
                                              c_default)

iuo_dw_main=dw_tes_trat_corrispondenze_lista

cb_controllo.enabled = false
cb_note_esterne.enabled = false

end event

event pc_delete;call super::pc_delete;cb_controllo.enabled = false
cb_note_esterne.enabled = false

end event

on w_tes_trat_corrispondenze.create
int iCurrent
call super::create
this.dw_tes_trat_corrispondenze_lista=create dw_tes_trat_corrispondenze_lista
this.dw_tes_trat_corrispondenze_det=create dw_tes_trat_corrispondenze_det
this.cb_note_esterne=create cb_note_esterne
this.cb_controllo=create cb_controllo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_trat_corrispondenze_lista
this.Control[iCurrent+2]=this.dw_tes_trat_corrispondenze_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_controllo
end on

on w_tes_trat_corrispondenze.destroy
call super::destroy
destroy(this.dw_tes_trat_corrispondenze_lista)
destroy(this.dw_tes_trat_corrispondenze_det)
destroy(this.cb_note_esterne)
destroy(this.cb_controllo)
end on

event pc_new;call super::pc_new;dw_tes_trat_corrispondenze_det.setitem(dw_tes_trat_corrispondenze_det.getrow(), "data_corrispondenza", datetime(today()))

end event

type dw_tes_trat_corrispondenze_lista from uo_cs_xx_dw within w_tes_trat_corrispondenze
integer x = 23
integer y = 20
integer width = 2994
integer height = 500
integer taborder = 10
string dataobject = "d_tes_trat_corrispondenze_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

on rowfocuschanged;call uo_cs_xx_dw::rowfocuschanged;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
      cb_controllo.enabled = true
   else
      cb_controllo.enabled = false
   end if
end if
end on

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_prog_corrispondenza, ll_anno_trattativa, ll_num_trattativa
string ls_cod_corrispondenza
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then
      this.setitem(ll_i, "anno_trattativa", ll_anno_trattativa)
   end if
   if this.getitemnumber(ll_i, "num_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "num_trattativa")) then
      this.setitem(ll_i, "num_trattativa", ll_num_trattativa)
   end if

   if isnull(this.getitemnumber(ll_i, "prog_corrispondenza")) or &
      this.getitemnumber(ll_i, "prog_corrispondenza") = 0 then

      ls_cod_corrispondenza = this.getitemstring(ll_i,"cod_corrispondenza")
      ldt_data_corrispondenza = this.getitemdatetime(ll_i,"data_corrispondenza")
      ldt_ora_corrispondenza = this.getitemdatetime(ll_i,"ora_corrispondenza")

      select max(tes_trat_corrispondenze.prog_corrispondenza)
      into   :ll_prog_corrispondenza
      from   tes_trat_corrispondenze
      where  tes_trat_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
             tes_trat_corrispondenze.anno_trattativa = :ll_anno_trattativa and
             tes_trat_corrispondenze.num_trattativa = :ll_num_trattativa and
             tes_trat_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and
             tes_trat_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and
             tes_trat_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza;

      if not isnull(ll_prog_corrispondenza) then
         this.setitem(ll_i, "prog_corrispondenza", ll_prog_corrispondenza + 1)
      else
         this.setitem(ll_i, "prog_corrispondenza", 1)
      end if
   end if
next
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_trattativa, ll_num_trattativa


ll_anno_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_trattativa")
ll_num_trattativa = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_trattativa, ll_num_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_tes_trat_corrispondenze_det.modify(ls_modify)

   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
	ii_new_row = getrow()
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   ls_modify = "num_reg_lista.protect='0~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),0,1)'~t"
   ls_modify = ls_modify + "num_reg_lista.background.color='16777215~tif((isnull(num_reg_lista_comp) or num_reg_lista_comp = 0),16777215,12632256)'~t"
   dw_tes_trat_corrispondenze_det.modify(ls_modify)

   cb_controllo.enabled = false
   cb_note_esterne.enabled = false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_corrispondenza")) then
      if this.getitemnumber(this.getrow(), "num_reg_lista") > 0 then
         cb_controllo.enabled = true
      else
         cb_controllo.enabled = false
      end if
      cb_note_esterne.enabled = true
   else
      cb_controllo.enabled = false
      cb_note_esterne.enabled = false
   end if
end if
end event

event updateend;call super::updateend;integer li_i
long ll_num_reg_lista, ll_prog_liste_con_comp

for li_i = 1 to this.deletedcount()
   ll_num_reg_lista = this.getitemnumber(li_i, "num_reg_lista_comp", delete!, true)
   ll_prog_liste_con_comp = this.getitemnumber(li_i, "prog_liste_con_comp", delete!, true)
   f_cancella_liste_con_comp(ll_num_reg_lista, ll_prog_liste_con_comp)
next

boolean lb_nuovo_appuntamento = false
string ls_cod_tipo_agenda, ls_cod_utente, ls_cod_corrispondenza
long ll_i, ll_rows, ll_anno_trattativa, ll_num_trattativa
datetime ldt_data, ldt_data_old, ldt_ora, ldt_ora_old, ldt_data_corrispondenza, &
         ldt_ora_corrispondenza, ldt_data_agenda, ldt_ora_agenda
time lt_1

ll_rows = rowcount()
if ll_rows < 1 then return
for ll_i = 1 to ll_rows
	ldt_data = getitemdatetime(ll_i,"data_pros_corrispondenza")
	ldt_data_old = getitemdatetime(ll_i,"data_pros_corrispondenza",primary!,true)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza"))
	ldt_ora = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza",primary!,true))
	ldt_ora_old = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	ll_anno_trattativa = getitemnumber(ll_i, "anno_trattativa")
	ll_num_trattativa = getitemnumber(ll_i, "num_trattativa")
	ls_cod_corrispondenza = getitemstring(ll_i, "cod_corrispondenza")
	ldt_data_corrispondenza = getitemdatetime(ll_i,"data_corrispondenza")
	ldt_ora_corrispondenza = getitemdatetime(ll_i,"ora_corrispondenza")

	if ll_i = ii_new_row and not(isnull(ldt_data)) and not(isnull(ldt_ora)) then
		if f_crea_appuntamento(ldt_data, ldt_ora, "", getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
			update tes_trat_corrispondenze
			   set cod_tipo_agenda = :ls_cod_tipo_agenda,
					 cod_utente = :ls_cod_utente,
					 data_agenda = :ldt_data,
					 ora_agenda = :ldt_ora
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_trattativa = :ll_anno_trattativa and
					 num_trattativa = :ll_num_trattativa and
					 cod_corrispondenza = :ls_cod_corrispondenza and
					 data_corrispondenza = :ldt_data_corrispondenza and
					 ora_corrispondenza = :ldt_data_corrispondenza;
					 
			if sqlca.sqlcode = -1 then
				g_mb.messagebox(string(sqlca.sqlcode), sqlca.sqlerrtext)
				g_mb.messagebox("Corrispondenze Trattative ai Fornitori", "Errore durante la Modifica Corrispondenza")	
			end if
		else
			g_mb.messagebox("Corrispondenze Trattative ai Fornitori", "Errore durante la Creazione Appuntamento")	
		end if	
	end if	
	
	if not(isnull(ldt_data)) or not(isnull(ldt_data_old)) then
		if not isnull(ldt_data_old) then
			if ldt_data_old <> ldt_data then
				// elimina appuntamento segnalando e verificando la nota se è uguale
			end if
		end if
		if not isnull(ldt_data) then
			if ldt_data_old <> ldt_data then
				if f_crea_appuntamento(ldt_data, ldt_ora, "",getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
					update tes_trat_corrispondenze
						set cod_tipo_agenda = :ls_cod_tipo_agenda,
							 cod_utente = :ls_cod_utente,
							 data_agenda = :ldt_data,
							 ora_agenda = :ldt_ora
					 where cod_azienda = :s_cs_xx.cod_azienda and
							 anno_trattativa = :ll_anno_trattativa and
							 num_trattativa = :ll_num_trattativa and
							 cod_corrispondenza = :ls_cod_corrispondenza	and
							 data_corrispondenza = :ldt_data_corrispondenza and
							 ora_corrispondenza = :ldt_ora_corrispondenza;

					if sqlca.sqlcode = -1 then
						g_mb.messagebox(string(sqlca.sqlcode), sqlca.sqlerrtext)
						g_mb.messagebox("Corrispondenze Trattative ai Fornitori", "Errore durante la Modifica Corrispondenza")	
					end if
				else
					g_mb.messagebox("Corrispondenze Trattative ai Fornitori", "Errore durante la Creazione Appuntamento")	
				end if	
			end if
		end if
	end if
next

setnull(ii_new_row)

for ll_i = 1 to this.deletedcount()
	ls_cod_tipo_agenda = this.getitemstring(ll_i, "cod_tipo_agenda", delete!, true)
	ls_cod_utente = this.getitemstring(ll_i, "cod_utente", delete!, true)
	ldt_data_agenda= this.getitemdatetime(ll_i, "data_agenda", delete!, true)		
	ldt_ora_agenda= this.getitemdatetime(ll_i, "ora_agenda", delete!, true)		
	if not isnull(ls_cod_tipo_agenda) then
	  delete
	  from  agende_corrispondenze  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_note  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_utenti
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	end if
next
end event

type dw_tes_trat_corrispondenze_det from uo_cs_xx_dw within w_tes_trat_corrispondenze
integer x = 23
integer y = 540
integer width = 3383
integer height = 1120
integer taborder = 20
string dataobject = "d_tes_trat_corrispondenze_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;long    ll_null, ll_num_reg_lista, ll_num_versione, ll_num_edizione, ll_prog_lista

decimal ld_costo
   

if i_extendmode then
	
   setnull(ll_null)

   choose case i_colname
      case "cod_corrispondenza"
         this.setitem(i_rownbr, "num_reg_lista", ll_null)

         select tab_corrispondenze.num_reg_lista
         into   :ll_num_reg_lista
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext; 

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "num_reg_lista", ll_num_reg_lista)
            select   tes_liste_controllo.num_versione,
                     tes_liste_controllo.num_edizione
            into     :ll_num_versione, 
                     :ll_num_edizione
            from     tes_liste_controllo
            where    tes_liste_controllo.cod_azienda = :s_cs_xx.cod_azienda and 
                     tes_liste_controllo.num_reg_lista = :ll_num_reg_lista and 
                     tes_liste_controllo.flag_valido = 'S';

            this.setitem(this.getrow(), "num_versione", ll_num_versione)
            this.setitem(this.getrow(), "num_edizione", ll_num_edizione)
         end if

         select tab_corrispondenze.costo
         into   :ld_costo
         from   tab_corrispondenze
         where  tab_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_corrispondenze.cod_corrispondenza = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "costo", ld_costo)
         end if
      case "num_reg_lista"
			
         if i_colname <> "num_reg_lista" then
				return
			end if

			ll_prog_lista = getitemnumber(getrow(),"prog_liste_con_comp")

			if not isnull(ll_prog_lista) then
				g_mb.messagebox("OMNIA","Non è possibile cambiare la lista di controllo associata alla manutenzione dopo aver già eseguito la compilazione")
				return 1
			end if	

			ll_num_reg_lista = long(i_coltext)

			select max(num_edizione)
			into   :ll_num_edizione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_num_reg_lista and
					 approvato_da is not null;
		 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if

			select max(num_versione)
			into   :ll_num_versione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_num_reg_lista and
					 num_edizione = :ll_num_edizione and
					 approvato_da is not null;
			 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if
	
			setnull(ll_prog_lista)
	
			setitem(getrow(),"num_edizione",ll_num_edizione)
			setitem(getrow(),"num_versione",ll_num_versione)
			setitem(getrow(),"prog_liste_con_comp",ll_prog_lista)

	end choose
end if
end event

type cb_note_esterne from commandbutton within w_tes_trat_corrispondenze
integer x = 3040
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;string ls_cod_corrispondenza, ls_db
integer li_i, li_prog_corrispondenza, li_risposta
long ll_anno_trattativa, ll_num_trattativa
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_tes_trat_corrispondenze_lista.getrow()
ll_anno_trattativa = dw_tes_trat_corrispondenze_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_tes_trat_corrispondenze_lista.getitemnumber(li_i, "num_trattativa")
ls_cod_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemstring(li_i, "cod_corrispondenza")
ldt_data_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemdatetime(li_i, "data_corrispondenza")
ldt_ora_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemdatetime(li_i, "ora_corrispondenza")
li_prog_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemnumber(li_i, "prog_corrispondenza")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob tes_trat_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_trat_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob tes_trat_corrispondenze.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_trat_corrispondenze
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa and 
	           cod_corrispondenza = :ls_cod_corrispondenza and 
	           data_corrispondenza = :ldt_data_corrispondenza and 
	           ora_corrispondenza = :ldt_ora_corrispondenza and
	           prog_corrispondenza = :li_prog_corrispondenza;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob tes_trat_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza
		using      sqlcb;
		
		destroy   sqlcb;
		
	else
		
	   updateblob tes_trat_corrispondenze
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa and 
	              cod_corrispondenza = :ls_cod_corrispondenza and 
	              data_corrispondenza = :ldt_data_corrispondenza and 
	              ora_corrispondenza = :ldt_ora_corrispondenza and
	              prog_corrispondenza = :li_prog_corrispondenza;
					  
	end if
	
   commit;
end if
end event

type cb_controllo from commandbutton within w_tes_trat_corrispondenze
integer x = 3040
integer y = 120
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controllo"
end type

event clicked;string ls_cod_corrispondenza

long   ll_prog_corrispondenza, ll_i[], ll_num_versione, ll_prog_liste_con_comp, &
       ll_num_edizione, ll_num_reg_lista_comp, ll_anno_trattativa, ll_num_trattativa
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza
   
ll_i[1] = dw_tes_trat_corrispondenze_lista.getrow()
ll_num_reg_lista_comp = dw_tes_trat_corrispondenze_lista.getitemnumber(ll_i[1],"num_reg_lista_comp") 

if isnull(ll_num_reg_lista_comp) or ll_num_reg_lista_comp = 0 then
	
	if g_mb.messagebox("OMNIA","Attenzione!~nUna volta eseguita la compilazione non sara più possibile modificare la lista associata. Continuare?",&
                 question!,yesno!,2) = 2 then
		return -1
	end if
	
   f_crea_liste_con_comp(ll_num_versione, ll_num_edizione, ll_prog_liste_con_comp)

   ll_anno_trattativa = dw_tes_trat_corrispondenze_lista.getitemnumber(ll_i[1],"anno_trattativa")
   ll_num_trattativa = dw_tes_trat_corrispondenze_lista.getitemnumber(ll_i[1],"num_trattativa")
   ls_cod_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemstring(ll_i[1],"cod_corrispondenza") 
   ldt_data_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemdatetime(ll_i[1],"data_corrispondenza") 
   ldt_ora_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemdatetime(ll_i[1],"ora_corrispondenza") 
   ll_prog_corrispondenza = dw_tes_trat_corrispondenze_lista.getitemnumber(ll_i[1],"prog_corrispondenza") 
   ll_num_reg_lista_comp = dw_tes_trat_corrispondenze_lista.getitemnumber(ll_i[1],"num_reg_lista") 
   
   update tes_trat_corrispondenze
   set    tes_trat_corrispondenze.num_versione = :ll_num_versione,
          tes_trat_corrispondenze.num_edizione = :ll_num_edizione,
          tes_trat_corrispondenze.num_reg_lista_comp = :ll_num_reg_lista_comp,
          tes_trat_corrispondenze.prog_liste_con_comp = :ll_prog_liste_con_comp
   where  tes_trat_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda and
          tes_trat_corrispondenze.anno_trattativa = :ll_anno_trattativa and 
          tes_trat_corrispondenze.num_trattativa = :ll_num_trattativa and 
          tes_trat_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza and 
          tes_trat_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza and 
          tes_trat_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza and
          tes_trat_corrispondenze.prog_corrispondenza = :ll_prog_corrispondenza;

   if sqlca.sqlcode = 0 then
      commit;
   else
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di generazione lista di controllo.", &
                 exclamation!, ok!)
      return
   end if

   dw_tes_trat_corrispondenze_lista.triggerevent("pcd_retrieve")
   dw_tes_trat_corrispondenze_lista.set_selected_rows(1, &
                                                      ll_i[], &
                                                      c_ignorechanges, &
                                                      c_refreshchildren, &
                                                      c_refreshsame)
end if

window_open_parm(w_det_liste_con_comp, 0, dw_tes_trat_corrispondenze_lista)

end event

