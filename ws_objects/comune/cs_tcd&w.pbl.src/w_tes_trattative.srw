﻿$PBExportHeader$w_tes_trattative.srw
$PBExportComments$Finestra Gestione Testata Trattative
forward
global type w_tes_trattative from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_tes_trattative
end type
type cb_corrispondenze from commandbutton within w_tes_trattative
end type
type cb_dettagli from commandbutton within w_tes_trattative
end type
type cb_note_esterne from commandbutton within w_tes_trattative
end type
type cb_note from commandbutton within w_tes_trattative
end type
type cb_genera_offerta from commandbutton within w_tes_trattative
end type
type cb_reset from commandbutton within w_tes_trattative
end type
type cb_ricerca from commandbutton within w_tes_trattative
end type
type cb_genera_contatto from commandbutton within w_tes_trattative
end type
type cb_rep_tp from commandbutton within w_tes_trattative
end type
type dw_tes_trattative_det_1 from uo_cs_xx_dw within w_tes_trattative
end type
type dw_tes_trattative_det_2 from uo_cs_xx_dw within w_tes_trattative
end type
type dw_folder from u_folder within w_tes_trattative
end type
type dw_tes_trattative_search from u_dw_search within w_tes_trattative
end type
type dw_folder_search from u_folder within w_tes_trattative
end type
type dw_tes_trattative_lista from uo_cs_xx_dw within w_tes_trattative
end type
end forward

global type w_tes_trattative from w_cs_xx_principale
integer width = 3081
integer height = 1704
string title = "Gestione Testata Trattative"
cb_1 cb_1
cb_corrispondenze cb_corrispondenze
cb_dettagli cb_dettagli
cb_note_esterne cb_note_esterne
cb_note cb_note
cb_genera_offerta cb_genera_offerta
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_genera_contatto cb_genera_contatto
cb_rep_tp cb_rep_tp
dw_tes_trattative_det_1 dw_tes_trattative_det_1
dw_tes_trattative_det_2 dw_tes_trattative_det_2
dw_folder dw_folder
dw_tes_trattative_search dw_tes_trattative_search
dw_folder_search dw_folder_search
dw_tes_trattative_lista dw_tes_trattative_lista
end type
global w_tes_trattative w_tes_trattative

type variables
boolean ib_nuovo = false
long il_nuova_riga
end variables

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]


dw_tes_trattative_lista.set_dw_key("cod_azienda")
dw_tes_trattative_lista.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_default, &
                                       c_default)
dw_tes_trattative_det_1.set_dw_options(sqlca, &
                                     dw_tes_trattative_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)
dw_tes_trattative_det_2.set_dw_options(sqlca, &
                                     dw_tes_trattative_lista, &
                                     c_sharedata + c_scrollparent, &
                                     c_default)

iuo_dw_main=dw_tes_trattative_lista


lw_oggetti[1] = dw_tes_trattative_det_2
dw_folder.fu_assigntab(2, "Note", lw_oggetti[])

lw_oggetti[1] = dw_tes_trattative_det_1
lw_oggetti[2] = cb_genera_contatto
//lw_oggetti[3] = cb_ric_clienti
//lw_oggetti[3] = cb_ric_contatti
dw_folder.fu_assigntab(1, "Principale", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note_esterne.enabled = false
cb_note.enabled = false
cb_genera_offerta.enabled = false
cb_rep_tp.enabled=false

// ------------------------------------------------------------

l_criteriacolumn[1] = "cod_contatto"
l_criteriacolumn[2] = "cod_cliente"
l_criteriacolumn[3] = "cod_tipo_trattativa"
l_criteriacolumn[4] = "flag_conclusa"
l_criteriacolumn[5] = "flag_esito_positivo"
l_criteriacolumn[6] = "flag_blocco"

l_searchtable[1] = "tes_trattative"
l_searchtable[2] = "tes_trattative"
l_searchtable[3] = "tes_trattative"
l_searchtable[4] = "tes_trattative"
l_searchtable[5] = "tes_trattative"
l_searchtable[6] = "tes_trattative"

l_searchcolumn[1] = "cod_contatto"
l_searchcolumn[2] = "cod_cliente"
l_searchcolumn[3] = "cod_tipo_trattativa"
l_searchcolumn[4] = "flag_conclusa"
l_searchcolumn[5] = "flag_esito"
l_searchcolumn[6] = "flag_blocco"

dw_tes_trattative_search.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_trattative_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_tes_trattative_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_tes_trattative_search
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_cliente
//l_objects[5] = cb_ricerca_contatto
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_tes_trattative_lista.change_dw_current()
end event

event pc_delete;call super::pc_delete;cb_dettagli.enabled = false
cb_corrispondenze.enabled = false
cb_note_esterne.enabled = false
cb_note.enabled = false
cb_genera_offerta.enabled = false
cb_genera_contatto.enabled = false
cb_rep_tp.enabled = false
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_trattative_det_1, &
                 "cod_tipo_trattativa", &
                 sqlca, &
                 "tab_tipi_trattative", &
                 "cod_tipo_trattativa", &
                 "des_tipo_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_trattativa = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_tes_trattative_det, &
//                 "cod_contatto", &
//                 sqlca, &
//                 "anag_contatti", &
//                 "cod_contatto", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trattative_det_1, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_trattative_det_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trattative_det_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trattative_det_1, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_trattative_search, &
                 "cod_tipo_trattativa", &
                 sqlca, &
                 "tab_tipi_trattative", &
                 "cod_tipo_trattativa", &
                 "des_tipo_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_trattativa = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

on w_tes_trattative.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_corrispondenze=create cb_corrispondenze
this.cb_dettagli=create cb_dettagli
this.cb_note_esterne=create cb_note_esterne
this.cb_note=create cb_note
this.cb_genera_offerta=create cb_genera_offerta
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_genera_contatto=create cb_genera_contatto
this.cb_rep_tp=create cb_rep_tp
this.dw_tes_trattative_det_1=create dw_tes_trattative_det_1
this.dw_tes_trattative_det_2=create dw_tes_trattative_det_2
this.dw_folder=create dw_folder
this.dw_tes_trattative_search=create dw_tes_trattative_search
this.dw_folder_search=create dw_folder_search
this.dw_tes_trattative_lista=create dw_tes_trattative_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_corrispondenze
this.Control[iCurrent+3]=this.cb_dettagli
this.Control[iCurrent+4]=this.cb_note_esterne
this.Control[iCurrent+5]=this.cb_note
this.Control[iCurrent+6]=this.cb_genera_offerta
this.Control[iCurrent+7]=this.cb_reset
this.Control[iCurrent+8]=this.cb_ricerca
this.Control[iCurrent+9]=this.cb_genera_contatto
this.Control[iCurrent+10]=this.cb_rep_tp
this.Control[iCurrent+11]=this.dw_tes_trattative_det_1
this.Control[iCurrent+12]=this.dw_tes_trattative_det_2
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.dw_tes_trattative_search
this.Control[iCurrent+15]=this.dw_folder_search
this.Control[iCurrent+16]=this.dw_tes_trattative_lista
end on

on w_tes_trattative.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_corrispondenze)
destroy(this.cb_dettagli)
destroy(this.cb_note_esterne)
destroy(this.cb_note)
destroy(this.cb_genera_offerta)
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_genera_contatto)
destroy(this.cb_rep_tp)
destroy(this.dw_tes_trattative_det_1)
destroy(this.dw_tes_trattative_det_2)
destroy(this.dw_folder)
destroy(this.dw_tes_trattative_search)
destroy(this.dw_folder_search)
destroy(this.dw_tes_trattative_lista)
end on

event pc_new;call super::pc_new;dw_tes_trattative_det_1.setitem(dw_tes_trattative_det_1.getrow(), "data_registrazione", datetime(today()))

end event

type cb_1 from commandbutton within w_tes_trattative
integer x = 2651
integer y = 620
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R&eport"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = dw_tes_trattative_lista.getitemnumber(dw_tes_trattative_lista.getrow(),"anno_trattativa")

s_cs_xx.parametri.parametro_d_2 = dw_tes_trattative_lista.getitemnumber(dw_tes_trattative_lista.getrow(),"num_trattativa")

window_open(w_report_trattative,-1)
end event

type cb_corrispondenze from commandbutton within w_tes_trattative
integer x = 2651
integer y = 220
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corr."
end type

on clicked;window_open_parm(w_tes_trat_corrispondenze, -1, dw_tes_trattative_lista)

end on

type cb_dettagli from commandbutton within w_tes_trattative
integer x = 2651
integer y = 120
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

on clicked;window_open_parm(w_det_trattative, -1, dw_tes_trattative_lista)

end on

type cb_note_esterne from commandbutton within w_tes_trattative
integer x = 2651
integer y = 320
integer width = 366
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "D&ocumento"
end type

event clicked;integer li_i, li_risposta
long ll_anno_trattativa, ll_num_trattativa
string ls_db

transaction sqlcb
blob lbl_null

setnull(lbl_null)

li_i = dw_tes_trattative_lista.getrow()
ll_anno_trattativa = dw_tes_trattative_lista.getitemnumber(li_i, "anno_trattativa")
ll_num_trattativa = dw_tes_trattative_lista.getitemnumber(li_i, "num_trattativa")

// 15-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

if ls_db = "MSSQL" then
	
	li_risposta = f_crea_sqlcb(sqlcb)
	
	selectblob tes_trattative.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_trattative
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa
	using      sqlcb;
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	destroy sqlcb;
	
else
	
	selectblob tes_trattative.note_esterne
	into       :s_cs_xx.parametri.parametro_bl_1
	from       tes_trattative
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_trattativa = :ll_anno_trattativa and 
	           num_trattativa = :ll_num_trattativa;
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

window_open(w_ole, 0)

if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	
	if ls_db = "MSSQL" then
		
		li_risposta = f_crea_sqlcb(sqlcb)
		
	   updateblob tes_trattative
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa
		using      sqlcb;
		
		destroy sqlcb;
		
	else
		
	   updateblob tes_trattative
	   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	   where      cod_azienda = :s_cs_xx.cod_azienda and
	              anno_trattativa = :ll_anno_trattativa and 
	              num_trattativa = :ll_num_trattativa;
					  
	end if
	
   commit;
end if
end event

type cb_note from commandbutton within w_tes_trattative
integer x = 2651
integer y = 20
integer width = 366
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

on clicked;window_open_parm(w_tes_trattative_note, -1, dw_tes_trattative_lista)

end on

type cb_genera_offerta from commandbutton within w_tes_trattative
integer x = 2651
integer y = 420
integer width = 366
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Offerta"
end type

event clicked;long ll_anno_trattativa, ll_num_trattativa, ll_i
string ls_cod_contatto, ls_cod_centro_costo, ls_cod_tipo_trattativa, ls_cod_agente_1, &
       ls_cod_agente_2, ls_flag_blocco
datetime ldt_data_scadenza, ldt_data_blocco


ll_i = dw_tes_trattative_lista.getrow()
ls_cod_contatto = dw_tes_trattative_lista.getitemstring(ll_i, "cod_contatto")
ll_anno_trattativa = dw_tes_trattative_lista.getitemnumber(ll_i, "anno_trattativa")
ll_num_trattativa = dw_tes_trattative_lista.getitemnumber(ll_i, "num_trattativa")
ldt_data_scadenza = dw_tes_trattative_lista.getitemdatetime(ll_i, "data_scadenza")
ls_cod_centro_costo = dw_tes_trattative_lista.getitemstring(ll_i, "cod_centro_costo")
ls_cod_tipo_trattativa = dw_tes_trattative_lista.getitemstring(ll_i, "cod_tipo_trattativa")
ls_cod_agente_1 = dw_tes_trattative_lista.getitemstring(ll_i, "cod_agente_1")
ls_cod_agente_2 = dw_tes_trattative_lista.getitemstring(ll_i, "cod_agente_2")
ls_flag_blocco = dw_tes_trattative_det_1.getitemstring(ll_i, "flag_blocco")
ldt_data_blocco = dw_tes_trattative_det_1.getitemdatetime(ll_i, "data_blocco")

dw_tes_trattative_det_1.change_dw_current( )

if ls_flag_blocco = 'S' and (ldt_data_blocco <= datetime(today()) or isnull(ldt_data_blocco)) then
  	g_mb.messagebox("Attenzione", "Trattativa bloccata.", &
              exclamation!, ok!)
   return
else
   f_genera_off_ven(ls_cod_contatto, ll_anno_trattativa, ll_num_trattativa, ldt_data_scadenza, ls_cod_centro_costo, ls_cod_tipo_trattativa, ls_cod_agente_1, ls_cod_agente_2)
end if

if not isnull(dw_tes_trattative_det_1.getitemnumber(ll_i, "anno_registrazione")) then
   cb_genera_offerta.enabled=false
end if
end event

type cb_reset from commandbutton within w_tes_trattative
event clicked pbm_bnclicked
integer x = 2121
integer y = 100
integer width = 361
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_tes_trattative_search.fu_Reset()



end event

type cb_ricerca from commandbutton within w_tes_trattative
event clicked pbm_bnclicked
integer x = 2121
integer y = 196
integer width = 361
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_tes_trattative_search.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_tes_trattative_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_genera_contatto from commandbutton within w_tes_trattative
integer x = 2235
integer y = 1060
integer width = 475
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Contatto"
end type

event clicked;dw_tes_trattative_lista.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = dw_tes_trattative_lista.getitemstring(dw_tes_trattative_lista.getrow(), "cod_cliente")
//window_open(w_genera_contatti, 0)
parent.triggerevent("pc_setddlb")
end event

type cb_rep_tp from commandbutton within w_tes_trattative
integer x = 2651
integer y = 520
integer width = 366
integer height = 80
integer taborder = 171
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Rep.x Prog."
end type

event clicked;long ll_i

for ll_i=1 to upperbound(s_cs_xx.parametri.parametro_d_1_a)
	setnull(s_cs_xx.parametri.parametro_d_1_a[ll_i])
next
//setnull(s_cs_xx.parametri.parametro_d_1_a)
for ll_i=1 to dw_tes_trattative_lista.rowcount()
	s_cs_xx.parametri.parametro_d_1_a[ll_i] = dw_tes_trattative_lista.getitemnumber(ll_i,"anno_trattativa")
	s_cs_xx.parametri.parametro_d_1_a[ll_i] = s_cs_xx.parametri.parametro_d_1_a[ll_i] + dw_tes_trattative_lista.getitemnumber(ll_i,"num_trattativa")*10000
next

if upperbound(s_cs_xx.parametri.parametro_d_1_a)> 0 then
	
	window_open(w_report_tratt_prog,-1)
end if
end event

type dw_tes_trattative_det_1 from uo_cs_xx_dw within w_tes_trattative
integer x = 46
integer y = 740
integer width = 2967
integer height = 820
integer taborder = 120
string dataobject = "d_tes_trattative_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_operaio, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, ls_cod_contatto, &
          ls_null, ls_rag_soc, ls_nota_testata, ls_nota_piede, ls_nota_video
	datetime ldt_data_registrazione
   setnull(ls_null)

   choose case i_colname
      case "cod_contatto"
         select anag_contatti.cod_operaio,
                anag_contatti.cod_agente_1,
                anag_contatti.cod_agente_2,
                anag_contatti.cod_cliente
         into   :ls_cod_operaio,    
                :ls_cod_agente_1,
                :ls_cod_agente_2,
                :ls_cod_cliente
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_contatto = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
            this.setitem(this.getrow(), "cod_cliente", ls_cod_cliente)
         else
            this.setitem(i_rownbr, "cod_operaio", ls_null)
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
            this.setitem(this.getrow(), "cod_cliente", ls_null)
         end if			
      case "cod_cliente"
         select anag_clienti.cod_agente_1,
                anag_clienti.cod_agente_2
         into   :ls_cod_agente_1,
                :ls_cod_agente_2
         from   anag_clienti
         where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_clienti.cod_cliente = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
         else
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
         end if

         select anag_contatti.cod_contatto, 
                anag_contatti.cod_operaio
         into   :ls_cod_contatto,
                :ls_cod_operaio
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_cliente = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_contatto", ls_cod_contatto)
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
         else
            this.setitem(i_rownbr, "cod_contatto", ls_null)
            this.setitem(i_rownbr, "cod_operaio", ls_null)
				cb_genera_contatto.enabled = true
			end if
			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(data, ls_null, ls_null, "TRAT_VEN", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
   end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_trattative_det_1,"cod_cliente")

	case "b_ricerca_contatto"
	guo_ricerca.uof_ricerca_contatto(dw_tes_trattative_det_1,"cod_contatto")
end choose
end event

type dw_tes_trattative_det_2 from uo_cs_xx_dw within w_tes_trattative
event itemchanged pbm_dwnitemchange
integer x = 64
integer y = 760
integer width = 2898
integer height = 720
integer taborder = 110
string dataobject = "d_tes_trattative_det_2"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_operaio, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, ls_cod_contatto, &
          ls_null

   setnull(ls_null)

   choose case i_colname
      case "cod_contatto"
         select anag_contatti.cod_operaio,
                anag_contatti.cod_agente_1,
                anag_contatti.cod_agente_2,
                anag_contatti.cod_cliente
         into   :ls_cod_operaio,    
                :ls_cod_agente_1,
                :ls_cod_agente_2,
                :ls_cod_cliente
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_contatto = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
            this.setitem(this.getrow(), "cod_cliente", ls_cod_cliente)
         else
            this.setitem(i_rownbr, "cod_operaio", ls_null)
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
            this.setitem(this.getrow(), "cod_cliente", ls_null)
         end if
      case "cod_cliente"
         select anag_clienti.cod_agente_1,
                anag_clienti.cod_agente_2
         into   :ls_cod_agente_1,
                :ls_cod_agente_2
         from   anag_clienti
         where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_clienti.cod_cliente = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
         else
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
         end if

         select anag_contatti.cod_contatto, 
                anag_contatti.cod_operaio
         into   :ls_cod_contatto,
                :ls_cod_operaio
         from   anag_contatti
         where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_contatti.cod_cliente = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_contatto", ls_cod_contatto)
            this.setitem(i_rownbr, "cod_operaio", ls_cod_operaio)
         else
            this.setitem(i_rownbr, "cod_contatto", ls_null)
            this.setitem(i_rownbr, "cod_operaio", ls_null)
				cb_genera_contatto.enabled = true
			end if
   end choose
end if
end event

type dw_folder from u_folder within w_tes_trattative
integer x = 18
integer y = 636
integer width = 3013
integer height = 940
integer taborder = 10
end type

type dw_tes_trattative_search from u_dw_search within w_tes_trattative
integer x = 133
integer y = 80
integer width = 1966
integer height = 416
integer taborder = 60
string dataobject = "d_tes_trattative_search"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_tes_trattative_search,"cod_contatto")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_trattative_search,"cod_cliente")
end choose
end event

type dw_folder_search from u_folder within w_tes_trattative
integer x = 18
integer y = 20
integer width = 2601
integer height = 580
integer taborder = 40
end type

type dw_tes_trattative_lista from uo_cs_xx_dw within w_tes_trattative
integer x = 160
integer y = 60
integer width = 2400
integer height = 500
integer taborder = 100
string dataobject = "d_tes_trattative_lista"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_contatto, ls_rag_soc

if i_extendmode then
	if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_genera_offerta.enabled = false
   end if
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_trattativa


   select con_contatti.cod_tipo_trattativa
   into   :ls_cod_tipo_trattativa
   from   con_contatti
   where  con_contatti.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_tipo_trattativa", ls_cod_tipo_trattativa)
   end if

   cb_dettagli.enabled = false
   cb_corrispondenze.enabled = false
   cb_note_esterne.enabled = false
   cb_note.enabled = false
   cb_genera_offerta.enabled = false
	dw_tes_trattative_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_trattative_det_1.object.b_ricerca_contatto.enabled = true
	cb_genera_contatto.enabled = false
	cb_rep_tp.enabled = false
	ib_nuovo = true
	il_nuova_riga = getrow()
end if


end event

event pcd_view;call super::pcd_view;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note_esterne.enabled = true
      cb_note.enabled = true
		cb_rep_tp.enabled = true
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note_esterne.enabled = false
      cb_note.enabled = false
      cb_genera_offerta.enabled = false
		cb_rep_tp.enabled = true
   end if
dw_tes_trattative_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_trattative_det_1.object.b_ricerca_contatto.enabled = false
	cb_genera_contatto.enabled = false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_trattativa") > 0 then
      cb_dettagli.enabled = true
      cb_corrispondenze.enabled = true
      cb_note_esterne.enabled = true
      cb_note.enabled = true
		cb_rep_tp.enabled = true
      if not isnull(this.getitemnumber(this.getrow(), "anno_registrazione")) or &
         this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
         cb_genera_offerta.enabled = false
      else
         cb_genera_offerta.enabled = true
      end if
   else
      cb_dettagli.enabled = false
      cb_corrispondenze.enabled = false
      cb_note_esterne.enabled = false
      cb_note.enabled = false
      cb_genera_offerta.enabled = false
		cb_rep_tp.enabled=false
   end if
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if

   if this.getitemnumber(ll_i, "anno_trattativa") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_trattativa")) then

      select parametri_azienda.numero
      into   :ll_anno_registrazione
      from   parametri_azienda
      where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
             parametri_azienda.flag_parametro = 'N' and &
             parametri_azienda.cod_parametro = 'ESC';
      if sqlca.sqlcode = 0 then
         this.setitem(this.getrow(), "anno_trattativa", int(ll_anno_registrazione))
      end if
      select con_contatti.num_registrazione
      into   :ll_num_registrazione
      from   con_contatti
      where  con_contatti.cod_azienda = :s_cs_xx.cod_azienda;
      if sqlca.sqlcode = 0 then
         this.setitem(this.getrow(), "num_trattativa", ll_num_registrazione + 1)
      end if
      update con_contatti
      set    con_contatti.num_registrazione = :ll_num_registrazione + 1
      where  con_contatti.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_contatto


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   cb_dettagli.enabled = false
   cb_corrispondenze.enabled = false
   cb_note_esterne.enabled = false
   cb_note.enabled = false
   cb_genera_offerta.enabled = false
	dw_tes_trattative_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_trattative_det_1.object.b_ricerca_contatto.enabled = true
	cb_genera_contatto.enabled = false
	cb_rep_tp.enabled=false
end if



end event

event updatestart;call super::updatestart;string ls_cod_cliente, ls_cod_contatto, ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, ls_nota_video
datetime ldt_data_registrazione

long ll_i, ll_anno_riesame, ll_num_riesame, li_i

if ib_nuovo then
	ls_cod_cliente = getitemstring(il_nuova_riga, "cod_cliente")
	ls_cod_contatto = getitemstring(il_nuova_riga, "cod_contatto")
	
	setnull(ls_null)
	ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
	ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
	
	if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "TRAT_VEN", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
		g_mb.error(ls_nota_testata)
	else
		ls_nota_old = this.getitemstring(this.getrow(), "note")
		if isnull(ls_nota_old) then ls_nota_old = ""
		this.setitem(this.getrow(), "note", ls_nota_piede + ls_nota_testata + ls_nota_old )
	end if

	ib_nuovo = false
end if

//Claudia 13/07/06 ///cancello tutti i dettagli
for li_i = 1 to pcca.window_currentdw.deletedcount()
	
	ll_anno_riesame = pcca.window_currentdw.getitemnumber(li_i, "anno_trattativa", delete!, true)
	ll_num_riesame = pcca.window_currentdw.getitemnumber(li_i, "num_trattativa", delete!, true)
	
	delete from det_trattative_note
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from det_fasi_trattative_note
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from det_fasi_trattative
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from det_trattative
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from tes_trattative_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from tes_trattative_firme
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from tes_trattative_note
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from tes_trattative_documenti
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;
	
	delete from det_modifiche_trattative
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_riesame and
	num_registrazione = :ll_num_riesame;
	
	delete from tes_modifiche_trattative
	where cod_azienda = :s_cs_xx.cod_azienda and
	anno_trattativa = :ll_anno_riesame and
	num_trattativa = :ll_num_riesame;


next


end event

