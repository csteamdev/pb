﻿$PBExportHeader$w_tipi_det_trattative.srw
$PBExportComments$Finestra Gestione Tipi Dettagli Trattative
forward
global type w_tipi_det_trattative from w_cs_xx_principale
end type
type cb_dettaglio from commandbutton within w_tipi_det_trattative
end type
type dw_tipi_det_trattative from uo_cs_xx_dw within w_tipi_det_trattative
end type
end forward

global type w_tipi_det_trattative from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3954
integer height = 1152
string title = "Gestione Dettagli Tipi Trattative"
cb_dettaglio cb_dettaglio
dw_tipi_det_trattative dw_tipi_det_trattative
end type
global w_tipi_det_trattative w_tipi_det_trattative

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_det_trattative, &
                 "cod_fase_trattativa", &
                 sqlca, &
                 "tab_fasi_trattative", &
                 "cod_fase_trattativa", &
                 "des_fase_trattativa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
					  
					  

f_po_loaddddw_dw(dw_tipi_det_trattative, &
                 "num_reg_lista", &
                 sqlca, &
                 "tes_liste_controllo", &
                 "num_reg_lista", &
                 "des_lista", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_valido = 'S'")					  

end event

event pc_setwindow;call super::pc_setwindow;dw_tipi_det_trattative.set_dw_key("cod_azienda")
dw_tipi_det_trattative.set_dw_key("cod_tipo_trattativa")
dw_tipi_det_trattative.set_dw_options(sqlca, &
                                      i_openparm, &
                                      c_scrollparent, &
                                      c_default)

cb_dettaglio.enabled = true
end event

on w_tipi_det_trattative.create
int iCurrent
call super::create
this.cb_dettaglio=create cb_dettaglio
this.dw_tipi_det_trattative=create dw_tipi_det_trattative
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dettaglio
this.Control[iCurrent+2]=this.dw_tipi_det_trattative
end on

on w_tipi_det_trattative.destroy
call super::destroy
destroy(this.cb_dettaglio)
destroy(this.dw_tipi_det_trattative)
end on

type cb_dettaglio from commandbutton within w_tipi_det_trattative
integer x = 3543
integer y = 960
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Utenti"
end type

event clicked;window_open_parm(w_tipi_det_trattative_utenti, -1, dw_tipi_det_trattative)

end event

type dw_tipi_det_trattative from uo_cs_xx_dw within w_tipi_det_trattative
integer x = 23
integer y = 20
integer width = 3886
integer height = 920
string dataobject = "d_tipi_det_trattative"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;long   ll_num_reg_lista, ll_num_edizione, ll_num_versione

double ld_costo

if i_extendmode then
	
   choose case i_colname
			
      case "cod_fase_trattativa"
			
         select tab_fasi_trattative.costo
         into   :ld_costo
         from   tab_fasi_trattative
         where  tab_fasi_trattative.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_fasi_trattative.cod_fase_trattativa = :i_coltext;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "costo", ld_costo)
         end if

		case "num_reg_lista"
			
         if i_colname <> "num_reg_lista" then
				return
			end if

			ll_num_reg_lista = long(i_coltext)

			select max(num_edizione)
			into   :ll_num_edizione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_num_reg_lista and
					 approvato_da is not null;
		 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if

			select max(num_versione)
			into   :ll_num_versione
			from   tes_liste_controllo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 num_reg_lista = :ll_num_reg_lista and
					 num_edizione = :ll_num_edizione and
					 approvato_da is not null;
			 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("OMNIA","Errore nella select di tes_liste_controllo: " + sqlca.sqlerrtext)
				return -1
			end if
	
			setitem(getrow(),"num_edizione",ll_num_edizione)
			setitem(getrow(),"num_versione",ll_num_versione)
			
   end choose
end if



end event

event pcd_new;call super::pcd_new;if i_extendmode then
   long ll_prog_riga
   string ls_cod_tipo_trattativa, ls_cod_fase_trattativa

   ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")

   select max(tab_tipi_det_trattative.prog_riga)
   into   :ll_prog_riga
   from   tab_tipi_det_trattative
   where  tab_tipi_det_trattative.cod_azienda = :s_cs_xx.cod_azienda and
          tab_tipi_det_trattative.cod_tipo_trattativa = :ls_cod_tipo_trattativa;

   if not isnull(ll_prog_riga) then
      this.setitem(this.getrow(), "prog_riga", ll_prog_riga + 10)
   else
      this.setitem(this.getrow(), "prog_riga", 10)
   end if
end if

cb_dettaglio.enabled = false
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i
string ls_cod_tipo_trattativa

ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_trattativa")) then
      this.setitem(ll_i, "cod_tipo_trattativa", ls_cod_tipo_trattativa)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_cod_tipo_trattativa


ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;cb_dettaglio.enabled = false
end event

event pcd_view;call super::pcd_view;cb_dettaglio.enabled = true
end event

