﻿$PBExportHeader$w_tipi_det_trattative_utenti.srw
$PBExportComments$Finestra Gestione Tipi Dettagli Trattative
forward
global type w_tipi_det_trattative_utenti from w_cs_xx_principale
end type
type dw_utenti from uo_cs_xx_dw within w_tipi_det_trattative_utenti
end type
end forward

global type w_tipi_det_trattative_utenti from w_cs_xx_principale
integer width = 2395
integer height = 1148
string title = "Utenti Fase della Trattativa"
dw_utenti dw_utenti
end type
global w_tipi_det_trattative_utenti w_tipi_det_trattative_utenti

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_utenti, &
//                 "cod_utente", &
//                 sqlca, &
//                 "utenti", &
//                 "cod_utente", &
//                 "nome_cognome", &
//                 "")

f_po_loaddddw_dw(dw_utenti, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "cod_utente + '-' + nome_cognome", &
                 "")

end event

event pc_setwindow;call super::pc_setwindow;dw_utenti.set_dw_key("cod_azienda")
dw_utenti.set_dw_key("cod_tipo_trattativa")
dw_utenti.set_dw_key("cod_fase_trattativa")
dw_utenti.set_dw_options(sqlca, &
                         i_openparm, &
                         c_scrollparent, &
                         c_default)

end event

on w_tipi_det_trattative_utenti.create
int iCurrent
call super::create
this.dw_utenti=create dw_utenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_utenti
end on

on w_tipi_det_trattative_utenti.destroy
call super::destroy
destroy(this.dw_utenti)
end on

event closequery;integer li_i, li_a
string ls_firme[]

///NON VAAAAA
dw_utenti.acceptText()
	for li_i = 1 to dw_utenti.rowcount()
		ls_firme[li_i] = dw_utenti.getitemstring(li_i, "flag_firma")
		
	next


li_a = -1

for li_i = 1 to upperbound(ls_firme[])

	if ls_firme[li_i] = 'S' then
		li_a ++
	end if

next

if ((li_a = -1   or  li_a > 0 )  and upperbound(ls_firme[]) > 0 ) then
	
	if li_a = -1 then	
		g_mb.messagebox("SME","Mettere un utente con il flag firma a si!")
		return 1
	end if
	if li_a > 0 then
		g_mb.messagebox("SME","più di un utente ha il flag firme a si!")
		return 1
	end if
else 
		call super::closequery
		return  0

end if
end event

type dw_utenti from uo_cs_xx_dw within w_tipi_det_trattative_utenti
integer x = 23
integer y = 20
integer width = 2309
integer height = 1000
string dataobject = "d_tipi_det_trattative_utenti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_cod_tipo_trattativa, ls_cod_fase_trattativa

ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")
ls_cod_fase_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fase_trattativa")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_tipo_trattativa")) then
      this.setitem(ll_i, "cod_tipo_trattativa", ls_cod_tipo_trattativa)
   end if
   if isnull(this.getitemstring(ll_i, "cod_fase_trattativa")) then
      this.setitem(ll_i, "cod_fase_trattativa", ls_cod_fase_trattativa)
   end if	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_tipo_trattativa, ls_cod_fase_trattativa


ls_cod_tipo_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_trattativa")
ls_cod_fase_trattativa = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fase_trattativa")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_trattativa, ls_cod_fase_trattativa)

if ll_errore < 0 then
   pcca.error = c_fatal
else

	parent.title = "Fase " + ls_cod_fase_trattativa + " della trattativa " + ls_cod_tipo_trattativa
	
end if


end event

event updatestart;call super::updatestart;integer li_i, li_a
string ls_firme[]

///NON VAAAAA
dw_utenti.acceptText()
	for li_i = 1 to dw_utenti.rowcount()
		ls_firme[li_i] = dw_utenti.getitemstring(li_i, "flag_firma")
		
	next

li_a = -1

for li_i = 1 to upperbound(ls_firme[])

	if ls_firme[li_i] = 'S' then
		li_a ++
	end if

next

//if li_a = -1   or  li_a > 0 then
	
	if li_a = -1  and upperbound(ls_firme[]) > 0 then	
		g_mb.messagebox("SME","Mettere un utente con il flag firma a si!")
		return 1
	end if
	if li_a > 0 and upperbound(ls_firme[]) > 0  then
		g_mb.messagebox("SME","più di un utente ha il flag firme a si!")
		return 1
	end if

//if li_a > 0 then
//	messagebox("SME","più di un utente ha il flaG firme a si!")
//	return
//	else
//		call super::closequery
//		return 0
//	end if
//	
//end if
end event

