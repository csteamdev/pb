﻿$PBExportHeader$w_tipi_modifiche_tratt.srw
forward
global type w_tipi_modifiche_tratt from w_cs_xx_principale
end type
type dw_tipi_modifiche_tratt from uo_cs_xx_dw within w_tipi_modifiche_tratt
end type
end forward

global type w_tipi_modifiche_tratt from w_cs_xx_principale
integer width = 2505
integer height = 776
dw_tipi_modifiche_tratt dw_tipi_modifiche_tratt
end type
global w_tipi_modifiche_tratt w_tipi_modifiche_tratt

on w_tipi_modifiche_tratt.create
int iCurrent
call super::create
this.dw_tipi_modifiche_tratt=create dw_tipi_modifiche_tratt
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_modifiche_tratt
end on

on w_tipi_modifiche_tratt.destroy
call super::destroy
destroy(this.dw_tipi_modifiche_tratt)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_modifiche_tratt.set_dw_key("cod_azienda")
dw_tipi_modifiche_tratt.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)

end event

type dw_tipi_modifiche_tratt from uo_cs_xx_dw within w_tipi_modifiche_tratt
integer x = 14
integer y = 16
integer width = 2437
integer height = 636
integer taborder = 10
string dataobject = "d_tipi_modifiche_tratt"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

