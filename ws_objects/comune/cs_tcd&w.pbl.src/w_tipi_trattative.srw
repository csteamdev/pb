﻿$PBExportHeader$w_tipi_trattative.srw
$PBExportComments$Finestra Gestione Tipi Trattative
forward
global type w_tipi_trattative from w_cs_xx_principale
end type
type dw_tipi_trattative_lista from uo_cs_xx_dw within w_tipi_trattative
end type
type dw_tipi_trattative_det from uo_cs_xx_dw within w_tipi_trattative
end type
type cb_dettaglio from commandbutton within w_tipi_trattative
end type
end forward

global type w_tipi_trattative from w_cs_xx_principale
int Width=2373
int Height=1485
boolean TitleBar=true
string Title="Gestione Tipi Trattative"
dw_tipi_trattative_lista dw_tipi_trattative_lista
dw_tipi_trattative_det dw_tipi_trattative_det
cb_dettaglio cb_dettaglio
end type
global w_tipi_trattative w_tipi_trattative

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_tipi_trattative_det, &
                 "cod_tipo_off_ven", &
                 sqlca, &
                 "tab_tipi_off_ven", &
                 "cod_tipo_off_ven", &
                 "des_tipo_off_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_trattative_det, &
                 "cod_tipo_off_acq", &
                 sqlca, &
                 "tab_tipi_off_acq", &
                 "cod_tipo_off_acq", &
                 "des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_delete;call w_cs_xx_principale::pc_delete;cb_dettaglio.enabled=false
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_trattative_lista.set_dw_key("cod_azienda")
dw_tipi_trattative_lista.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_default, &
                                        c_default)
dw_tipi_trattative_det.set_dw_options(sqlca, &
                                      dw_tipi_trattative_lista, &
                                      c_sharedata + c_scrollparent, &
                                      c_default)
iuo_dw_main = dw_tipi_trattative_lista

cb_dettaglio.enabled=false
end on

on w_tipi_trattative.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_trattative_lista=create dw_tipi_trattative_lista
this.dw_tipi_trattative_det=create dw_tipi_trattative_det
this.cb_dettaglio=create cb_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_trattative_lista
this.Control[iCurrent+2]=dw_tipi_trattative_det
this.Control[iCurrent+3]=cb_dettaglio
end on

on w_tipi_trattative.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_trattative_lista)
destroy(this.dw_tipi_trattative_det)
destroy(this.cb_dettaglio)
end on

type dw_tipi_trattative_lista from uo_cs_xx_dw within w_tipi_trattative
int X=23
int Y=21
int Width=2286
int Height=501
int TabOrder=10
string DataObject="d_tipi_trattative_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_modify


   ls_modify = "cod_tipo_off_ven.background.color='12632256~tif(flag_tipo_trattativa=~~'A~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "cod_tipo_off_acq.background.color='12632256~tif(flag_tipo_trattativa=~~'V~~',12632256,16777215)'~t"
   dw_tipi_trattative_det.modify(ls_modify)

   cb_dettaglio.enabled = false
end if
end event

on pcd_view;call uo_cs_xx_dw::pcd_view;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_tipo_trattativa")) then
      cb_dettaglio.enabled = true
   else
      cb_dettaglio.enabled = false
   end if
end if
end on

on pcd_save;call uo_cs_xx_dw::pcd_save;if i_extendmode then
   if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_tipo_trattativa")) then
      cb_dettaglio.enabled = true
   else
      cb_dettaglio.enabled = false
   end if
end if
end on

event updatestart;call uo_cs_xx_dw::updatestart;if i_extendmode then
   integer li_i
   string ls_cod_tipo_trattativa

   for li_i = 1 to this.deletedcount()
      ls_cod_tipo_trattativa = this.getitemstring(li_i, "cod_tipo_trattativa", delete!, true)
      delete from tab_tipi_det_trattative
      where       tab_tipi_det_trattative.cod_azienda = :s_cs_xx.cod_azienda and
                  tab_tipi_det_trattative.cod_tipo_trattativa = :ls_cod_tipo_trattativa;

      if sqlca.sqlcode = -1 then
         g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di cancellazione.", &
                    exclamation!, ok!)
         return
      end if
   next
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   string ls_modify


   ls_modify = "cod_tipo_off_ven.background.color='12632256~tif(flag_tipo_trattativa=~~'A~~',12632256,16777215)'~t"
   ls_modify = ls_modify + "cod_tipo_off_acq.background.color='12632256~tif(flag_tipo_trattativa=~~'V~~',12632256,16777215)'~t"
   dw_tipi_trattative_det.modify(ls_modify)

   cb_dettaglio.enabled = false
end if
end event

type dw_tipi_trattative_det from uo_cs_xx_dw within w_tipi_trattative
int X=23
int Y=541
int Width=2286
int Height=721
int TabOrder=20
string DataObject="d_tipi_trattative_det"
BorderStyle BorderStyle=StyleRaised!
end type

on itemchanged;call uo_cs_xx_dw::itemchanged;if i_extendmode then
   string ls_null

   setnull(ls_null)

   choose case i_colname
      case "flag_tipo_trattativa"
         this.setitem(this.getrow(), "cod_tipo_off_ven", ls_null)
         this.setitem(this.getrow(), "cod_tipo_off_acq", ls_null)
   end choose
end if
end on

type cb_dettaglio from commandbutton within w_tipi_trattative
int X=1943
int Y=1281
int Width=366
int Height=81
int TabOrder=30
string Text="&Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open_parm(w_tipi_det_trattative, -1, dw_tipi_trattative_lista)

end on

