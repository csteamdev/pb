﻿$PBExportHeader$uo_dw_nominativi.sru
forward
global type uo_dw_nominativi from uo_std_dw
end type
end forward

global type uo_dw_nominativi from uo_std_dw
integer width = 2510
integer height = 980
string dataobject = "d_nominativi"
boolean hscrollbar = true
boolean vscrollbar = true
end type
global uo_dw_nominativi uo_dw_nominativi

type variables
constant string CLIENTI = "C"
constant string FORNITORI = "F"
constant string FOR_POT = "P"
constant string CONTATTI = "O"

private:
	string is_error
	string is_flag_tipo_anagrafica
	string is_pk
	datawindow idw_parent
end variables

forward prototypes
public function long retrieve (string as_codice)
public function boolean uof_set_dw_parent (ref datawindow adw_parent)
public function boolean uof_set_tipo_anagrafica (string as_flag_tipo_anagrafica)
public function long retrieve ()
public function boolean uof_set_dw_primary_key (string as_pk)
public function boolean uof_set_dw (ref datawindow adw_parent, string as_primary_key, string as_mode)
end prototypes

public function long retrieve (string as_codice);/**
 * stefanop
 * 16/11/2011
 *
 * Lancio la retrieve dei dati.
 * USare questa funzione che aggiungi i parametri necessari per il corretto recupero.
 *
 * Si consiglia di usare il metodo .retrieve() senza parametri per lasciare la gestione all'oggetto
 **/
 
 
return retrieve(s_cs_xx.cod_azienda, is_flag_tipo_anagrafica, as_codice)
end function

public function boolean uof_set_dw_parent (ref datawindow adw_parent);/**
 * stefanop
 * 22/11/2011
 *
 * Imposto la datawindow padre da dove verranno recuperati i valori per le chiavi primarie
 **/
 
if isvalid(adw_parent) and not isnull(adw_parent) then
	idw_parent = adw_parent
	setnull(is_error)

	return true
else
	setnull(idw_parent)
	is_error = "Impostare una datawindow padre per recuperare i valori necessari"
	return false
end if


end function

public function boolean uof_set_tipo_anagrafica (string as_flag_tipo_anagrafica);/**
 * stefanop
 * 16/11/2011
 *
 * Imposto il tipo di anagrafica per cui sto recuperando i nominativi.
 * Si consiglia di impostare tramite le costanti presenti all'interno dell'oggeto per 
 * poter eliminare codice ridondande e mantenere il tutto pulito.
 *
 * Esempio:
 * dw_1.uof_set_tipo_anagrafica(dw_1.CLIENTI)
 **/
 
is_flag_tipo_anagrafica = as_flag_tipo_anagrafica

return true
end function

public function long retrieve ();/**
 * stefanop
 * 16/12/2011
 *
 * Recupero i parametri PK della tabella ed eseguo la retrieve
 **/
 
 
string ls_codice

if not isvalid(idw_parent) then return 0
if isnull(idw_parent) then return 0

// valori pk base
ls_codice = idw_parent.getitemstring(idw_parent.getrow(), is_pk)

if isnull(ls_codice) or len(ls_codice) < 1 then return 0

return super::retrieve( s_cs_xx.cod_azienda, is_flag_tipo_anagrafica, ls_codice)
end function

public function boolean uof_set_dw_primary_key (string as_pk);/**
 * stefanop
 * 22/11/2011
 *
 * Imposto i nomi delle colonne che sono chavi primarie per poter recuperare i valori
 * per eseguire l'SQL
 **/
 
if isnull(as_pk) or len(as_pk) < 1 then
	is_error = "Impostare la colonna che rappresenta la chiavi primaria."
	return false
else
	is_pk = as_pk
	setnull(is_error)
	return true
end if


end function

public function boolean uof_set_dw (ref datawindow adw_parent, string as_primary_key, string as_mode);/**
 * stefanop
 * 22/11/2011
 *
 * Funzione per inizializzare i campi base per il funzionamento
 **/
 
 
if not uof_set_dw_parent(adw_parent) then return false

if not uof_set_dw_primary_key(as_primary_key) then return false

if not uof_set_tipo_anagrafica(as_mode) then return false

return true
end function

on uo_dw_nominativi.create
call super::create
end on

on uo_dw_nominativi.destroy
call super::destroy
end on

event constructor;call super::constructor;settransobject(sqlca)
end event

