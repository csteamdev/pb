﻿$PBExportHeader$uo_importazione.sru
$PBExportComments$UO importazione anagrafiche
forward
global type uo_importazione from nonvisualobject
end type
end forward

global type uo_importazione from nonvisualobject
end type
global uo_importazione uo_importazione

type variables
string  is_path, is_modalita, is_file_fornitori, is_file_clienti, is_file_prodotti, &
		  is_file_bolle_acq, is_file_ordini_ven, is_flag_fornitori, is_flag_clienti, is_flag_prodotti, &
		  is_flag_bolle_acq, is_flag_ordini_ven, is_volume
end variables

forward prototypes
public function integer uof_importa_clienti (ref string fs_messaggio)
public function integer uof_importa_fornitori (ref string fs_messaggio)
public function integer uof_importa_prodotti (ref string fs_messaggio)
public function integer uof_importa_bolle_acq (ref string fs_messaggio)
public function integer uof_importa_dati (ref string fs_messaggio)
end prototypes

public function integer uof_importa_clienti (ref string fs_messaggio);long   ll_file, ll_return, ll_count

string ls_riga, ls_cod_cliente, ls_rag_soc, ls_cap, ls_indirizzo, ls_localita, ls_provincia, ls_partita_iva, &
		 ls_tipo_cliente, ls_cod_valuta, ls_nome_file


ls_nome_file = is_volume + is_path + is_file_clienti

if fileexists(ls_nome_file) = false then
	if is_modalita = "M" then
		fs_messaggio = "File inesistente: " + ls_nome_file
		return -1
	else
		return 0
	end if	
end if

ll_file = fileopen(ls_nome_file)

if ll_file = -1 then
	fs_messaggio = "Errore in apertura del file: " + ls_nome_file
	return -1
end if

do while true
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		fs_messaggio = "Errore in lettura del file: " + ls_nome_file
	   fileclose(ll_file)
		return -1
	elseif ll_return = -100 then		
		exit
	end if
		
	ls_cod_cliente = trim(mid(ls_riga,6,6))
	ls_rag_soc = trim(mid(ls_riga,13,35))
	ls_cap = trim(mid(ls_riga,49,5))
	ls_indirizzo = trim(mid(ls_riga,55,35))
	ls_localita = trim(mid(ls_riga,91,25))
	ls_provincia = trim(mid(ls_riga,117,2))
	ls_partita_iva = trim(mid(ls_riga,120,16))
	ls_tipo_cliente = trim(mid(ls_riga,140,1))
	ls_cod_valuta = trim(mid(ls_riga,143,3))
	
	choose case ls_tipo_cliente
		case "I"
			ls_tipo_cliente = "S"
		case "C"
			ls_tipo_cliente = "C"
		case "E"
			ls_tipo_cliente = "E"
		case ""
			ls_tipo_cliente = "S"
	end choose
	
	if ls_rag_soc = "" then
		ls_rag_soc = ls_cod_cliente
	end if
	
	if ls_cod_valuta = "" then
		setnull(ls_cod_valuta)
	else
		
		select count(*)
		into   :ll_count
		from   tab_valute
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_valuta = :ls_cod_valuta;
			 
		if sqlca.sqlcode < 0 then
		
			fs_messaggio = "Errore nella select di tab_valute: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1
		
		elseif ll_count = 0 then
		
			insert
			into   tab_valute
				    (cod_azienda,
					 cod_valuta,
					 des_valuta)
			values (:s_cs_xx.cod_azienda,
					 :ls_cod_valuta,
					 :ls_cod_valuta);
				 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di tab_valute: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1
			end if
		
			commit;
		
		end if
		
	end if
	
	select count(*)
	into   :ll_count
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = '001';
			 
	if sqlca.sqlcode < 0 then
		
		fs_messaggio = "Errore nella select di anag_depositi: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
		
	elseif ll_count = 0 then
		
		insert
		into   anag_depositi
			    (cod_azienda,
				 cod_deposito,
				 des_deposito)
		values (:s_cs_xx.cod_azienda,
				 '001',
				 '001');
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_depositi: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
		
		commit;
		
	end if
	
	select count(*)
	into   :ll_count
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode < 0 then
			
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
			
	elseif ll_count = 0 then
			
		insert
		into   anag_clienti
		       (cod_azienda,
			    cod_cliente,
			    rag_soc_1,
			    cap,
			    indirizzo,
			    localita,
			    provincia,
			    partita_iva,
			    flag_tipo_cliente,
			    cod_valuta,
				 cod_deposito)
		values (:s_cs_xx.cod_azienda,
		       :ls_cod_cliente,
				 :ls_rag_soc,
				 :ls_cap,
				 :ls_indirizzo,
				 :ls_localita,
				 :ls_provincia,
				 :ls_partita_iva,
				 :ls_tipo_cliente,
				 :ls_cod_valuta,
				 '001');
					  
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_clienti: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
			
		commit;
			
	elseif ll_count > 0 then
			
		update anag_clienti
		set    rag_soc_1 = :ls_rag_soc,
			    cap = :ls_cap,
			    indirizzo = :ls_indirizzo,
			    localita = :ls_localita,
			    provincia = :ls_provincia,
			    partita_iva = :ls_partita_iva,
			    flag_tipo_cliente = :ls_tipo_cliente,
			    cod_valuta = :ls_cod_valuta					 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
					  
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella update di anag_clienti: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
			
		commit;
			
	end if
		
loop

if fileclose(ll_file) = -1 then
	fs_messaggio = "Errore nella chiusura del file"
	return -1
end if
	
if filedelete(ls_nome_file) = false then
	fs_messaggio = "Errore nella cancellazione del file origine dopo l'importazione"
	return -1
end if

return 0
end function

public function integer uof_importa_fornitori (ref string fs_messaggio);long   ll_file, ll_return, ll_count

string ls_riga, ls_cod_fornitore, ls_rag_soc, ls_cap, ls_indirizzo, ls_provincia, ls_partita_iva, &
		 ls_tipo_fornitore, ls_cod_valuta, ls_localita, ls_nome_file


ls_nome_file = is_volume + is_path + is_file_fornitori

if fileexists(ls_nome_file) = false then
	if is_modalita = "M" then
		fs_messaggio = "File inesistente: " + ls_nome_file
		return -1
	else
		return 0
	end if
end if

ll_file = fileopen(ls_nome_file)

if ll_file = -1 then
	fs_messaggio = "Errore in apertura del file: " + ls_nome_file
	return -1
end if

do while true
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		fs_messaggio = "Errore in lettura del file: " + ls_nome_file
	   fileclose(ll_file)
		return -1
	elseif ll_return = -100 then		
		exit
	end if
		
	ls_cod_fornitore = trim(mid(ls_riga,6,6))
	ls_rag_soc = trim(mid(ls_riga,13,35))
	ls_cap = trim(mid(ls_riga,49,5))
	ls_indirizzo = trim(mid(ls_riga,55,35))
	ls_localita = trim(mid(ls_riga,91,25))
	ls_provincia = trim(mid(ls_riga,117,2))
	ls_partita_iva = trim(mid(ls_riga,120,16))
	ls_tipo_fornitore = trim(mid(ls_riga,140,1))
	ls_cod_valuta = trim(mid(ls_riga,143,3))
	
	if ls_rag_soc = "" then
		ls_rag_soc = ls_cod_fornitore
	end if
	
	choose case ls_tipo_fornitore
		case "I"
			ls_tipo_fornitore = "S"
		case "C"
			ls_tipo_fornitore = "C"
		case "E"
			ls_tipo_fornitore = "E"
		case ""
			ls_tipo_fornitore = "S"
	end choose		
	
	if ls_cod_valuta = "" then
		setnull(ls_cod_valuta)
	else
		
		select count(*)
		into   :ll_count
		from   tab_valute
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_valuta = :ls_cod_valuta;
			 
		if sqlca.sqlcode < 0 then
		
			fs_messaggio = "Errore nella select di tab_valute: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1
		
		elseif ll_count = 0 then
		
			insert
			into   tab_valute
				    (cod_azienda,
					 cod_valuta,
					 des_valuta)
			values (:s_cs_xx.cod_azienda,
					 :ls_cod_valuta,
					 :ls_cod_valuta);
				 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di tab_valute: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1
			end if
		
			commit;
		
		end if
		
	end if
	
	select count(*)
	into   :ll_count
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = '001';
			 
	if sqlca.sqlcode < 0 then
		
		fs_messaggio = "Errore nella select di anag_depositi: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
		
	elseif ll_count = 0 then
		
		insert
		into   anag_depositi
			    (cod_azienda,
				 cod_deposito,
				 des_deposito)
		values (:s_cs_xx.cod_azienda,
				 '001',
				 '001');
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_depositi: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
		
		commit;
		
	end if
	
	select count(*)
	into   :ll_count
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode < 0 then
			
		fs_messaggio = "Errore nella select di anag_fornitori: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
			
	elseif ll_count = 0 then
			
		insert
		into   anag_fornitori
		       (cod_azienda,
			    cod_fornitore,
			    rag_soc_1,
			    cap,
			    indirizzo,
			    localita,
			    provincia,
			    partita_iva,
			    flag_tipo_fornitore,
			    cod_valuta,
				 cod_deposito)
		values (:s_cs_xx.cod_azienda,
		       :ls_cod_fornitore,
				 :ls_rag_soc,
				 :ls_cap,
				 :ls_indirizzo,
				 :ls_localita,
				 :ls_provincia,
				 :ls_partita_iva,
				 :ls_tipo_fornitore,
				 :ls_cod_valuta,
				 '001');
				  
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_fornitori: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
			
		commit;
			
	elseif ll_count > 0 then
			
		update anag_fornitori
		set    rag_soc_1 = :ls_rag_soc,
			    cap = :ls_cap,
			    indirizzo = :ls_indirizzo,
			    localita = :ls_localita,
			    provincia = :ls_provincia,
			    partita_iva = :ls_partita_iva,
			    flag_tipo_fornitore = :ls_tipo_fornitore,
			    cod_valuta = :ls_cod_valuta					 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
				  
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella update di anag_fornitori: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
			
		commit;
			
	end if
		
loop

if fileclose(ll_file) = -1 then
	fs_messaggio = "Errore nella chiusura del file"
	return -1
end if
	
if filedelete(ls_nome_file) = false then
	fs_messaggio = "Errore nella cancellazione del file origine dopo l'importazione"
	return -1
end if

return 0
end function

public function integer uof_importa_prodotti (ref string fs_messaggio);long   ll_file, ll_return, ll_count

string ls_riga, ls_nome_file, ls_cod_prodotto, ls_des_prodotto, ls_um, ls_iva, ls_cod_fornitore, ls_cod_deposito

dec{4} ld_prezzo_ult_acq


ls_nome_file = is_volume + is_path + is_file_prodotti

if fileexists(ls_nome_file) = false then
	if is_modalita = "M" then
		fs_messaggio = "File inesistente: " + ls_nome_file
		return -1
	else
		return 0
	end if
end if

ll_file = fileopen(ls_nome_file)

if ll_file = -1 then
	fs_messaggio = "Errore in apertura del file: " + ls_nome_file
	return -1
end if

do while true
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		fs_messaggio = "Errore in lettura del file: " + ls_nome_file
	   fileclose(ll_file)
		return -1
	elseif ll_return = -100 then		
		exit
	end if
		
	ls_cod_prodotto = trim(mid(ls_riga,1,15))
	ls_des_prodotto = trim(mid(ls_riga,17,40))
	ls_um = trim(mid(ls_riga,58,3))
	ls_iva = trim(mid(ls_riga,62,3))
	ld_prezzo_ult_acq = dec(trim(mid(ls_riga,77,11)))
	ls_cod_fornitore = trim(mid(ls_riga,94,6))
	ls_cod_deposito = trim(mid(ls_riga,101,3))
	
	if ls_um = "" then
		ls_um = "001"
	end if
		
	select count(*)
	into   :ll_count
	from   tab_misure
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_misura = :ls_um;
			 
	if sqlca.sqlcode < 0 then
	
		fs_messaggio = "Errore nella select di tab_misure: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
	
	elseif ll_count = 0 then
		
		insert
		into   tab_misure
			    (cod_azienda,
				 cod_misura,
				 des_misura)
		values (:s_cs_xx.cod_azienda,
				 :ls_um,
				 :ls_um);
			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di tab_misure: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
	
		commit;
	
	end if	
	
	if ls_iva = "" then
		setnull(ls_iva)
	else
		
		select count(*)
		into   :ll_count
		from   tab_ive
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_iva = :ls_iva;
			 
		if sqlca.sqlcode < 0 then
		
			fs_messaggio = "Errore nella select di tab_ive: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1
		
		elseif ll_count = 0 then
		
			insert
			into   tab_ive
				    (cod_azienda,
					 cod_iva,
					 des_iva)
			values (:s_cs_xx.cod_azienda,
					 :ls_iva,
					 :ls_iva);
				 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di tab_ive: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1
			end if
		
			commit;
		
		end if
		
	end if
	
	if isnull(ld_prezzo_ult_acq) then
		ld_prezzo_ult_acq = 0
	end if
	
	if ls_cod_fornitore = "" then
		setnull(ls_cod_fornitore)
	else
		
		select count(*)
		into   :ll_count
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
			 
		if sqlca.sqlcode < 0 then		
			fs_messaggio = "Errore nella select di anag_fornitori: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1		
		elseif ll_count = 0 then		
			setnull(ls_cod_fornitore)		
		end if
		
	end if
	
	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		ls_cod_deposito = "001"
	end if
	
	select count(*)
	into   :ll_count
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = :ls_cod_deposito;
			 
	if sqlca.sqlcode < 0 then
		
		fs_messaggio = "Errore nella select di anag_depositi: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
		
	elseif ll_count = 0 then
		
		insert
		into   anag_depositi
			    (cod_azienda,
				 cod_deposito,
				 des_deposito)
		values (:s_cs_xx.cod_azienda,
				 :ls_cod_deposito,
				 :ls_cod_deposito);
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_depositi: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
		
		commit;
		
	end if
	
	select count(*)
	into   :ll_count
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
			
		fs_messaggio = "Errore nella select di anag_prodotti: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
			
	elseif ll_count = 0 then
			
		insert
		into   anag_prodotti
		       (cod_azienda,
				 cod_prodotto,
			    des_prodotto,
			    cod_misura_mag,
				 cod_misura_peso,
				 cod_misura_acq,
				 cod_misura_ven,
				 cod_iva,
				 prezzo_acquisto,
				 cod_fornitore,
				 cod_deposito,
				 flag_stato_rifiuto)
		values (:s_cs_xx.cod_azienda,
		       :ls_cod_prodotto,
				 :ls_des_prodotto,
				 :ls_um,
				 :ls_um,
				 :ls_um,
				 :ls_um,
				 :ls_iva,
				 :ld_prezzo_ult_acq,
				 :ls_cod_fornitore,
				 :ls_cod_deposito,
				 'S');

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_prodotti: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if

		commit;

	elseif ll_count > 0 then

		update anag_prodotti
		set    des_prodotto = :ls_des_prodotto,
			    cod_misura_mag = :ls_um,
				 cod_misura_peso = :ls_um,
				 cod_misura_acq = :ls_um,
				 cod_misura_ven = :ls_um,
				 cod_iva = :ls_iva,
				 prezzo_acquisto = :ld_prezzo_ult_acq,
				 cod_fornitore = :ls_cod_fornitore,
				 cod_deposito = :ls_cod_deposito			
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella update di anag_prodotti: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if

		commit;

	end if

loop

if fileclose(ll_file) = -1 then
	fs_messaggio = "Errore nella chiusura del file"
	return -1
end if
	
if filedelete(ls_nome_file) = false then
	fs_messaggio = "Errore nella cancellazione del file origine dopo l'importazione"
	return -1
end if

return 0
end function

public function integer uof_importa_bolle_acq (ref string fs_messaggio);long   	ll_file, ll_return, ll_count, ll_anno_bol_acq, ll_num_bol_acq, ll_prog_riga_bol_acq, ll_prog_lotto, &
			ll_anno_ord_acq, ll_num_ord_acq, ll_prog_riga_ord_acq, ll_anno_acc_materiali, ll_num_acc_materiali

string 	ls_riga, ls_nome_file, ls_operazione, ls_cod_fornitore, ls_cod_prodotto, ls_cod_prod_forn, ls_cod_deposito, &
		 	ls_cod_ubicazione, ls_cod_lotto, ls_flag_qualita, ls_cod_misura, ls_prog_riga_bol_acq

dec{4}   ld_quan_carico, ld_prezzo_carico

datetime ldt_data_lotto, ldt_data_ricezione, ldt_data_consegna


ls_nome_file = is_volume + is_path + is_file_bolle_acq

if fileexists(ls_nome_file) = false then
	if is_modalita = "M" then
		fs_messaggio = "File inesistente: " + ls_nome_file
		return -1
	else
		return 0
	end if	
end if

ll_file = fileopen(ls_nome_file)

if ll_file = -1 then
	fs_messaggio = "Errore in apertura del file: " + ls_nome_file
	return -1
end if

do while true
	
	ll_return = fileread(ll_file,ls_riga)
	
	if ll_return = -1 then
		fs_messaggio = "Errore in lettura del file: " + ls_nome_file
	   fileclose(ll_file)
		return -1
	elseif ll_return = -100 then		
		exit
	end if
		
	ls_operazione = trim(mid(ls_riga,1,1))
	ll_anno_bol_acq = long(trim(mid(ls_riga,2,4)))
	ll_num_bol_acq = long(trim(mid(ls_riga,6,6)))
	ll_prog_riga_bol_acq = long(trim(mid(ls_riga,12,6)))
	ls_cod_fornitore = trim(mid(ls_riga,18,6))
	ls_cod_prodotto = trim(mid(ls_riga,24,15))
	ls_cod_prod_forn = trim(mid(ls_riga,39,15))
	ld_quan_carico = dec(trim(mid(ls_riga,54,13)))
	ld_prezzo_carico = dec(trim(mid(ls_riga,67,17)))
	ls_cod_deposito = trim(mid(ls_riga,84,3))
	ls_cod_ubicazione = trim(mid(ls_riga,87,10))
	ls_cod_lotto = trim(mid(ls_riga,97,10))
	ldt_data_lotto = datetime(date(trim(mid(ls_riga,107,10))),00:00:00)
	ll_prog_lotto = long(trim(mid(ls_riga,117,6)))
	ll_anno_ord_acq = long(trim(mid(ls_riga,123,4)))
	ll_num_ord_acq = long(trim(mid(ls_riga,127,6)))
	ll_prog_riga_ord_acq = long(trim(mid(ls_riga,133,6)))
	ldt_data_ricezione = datetime(date(trim(mid(ls_riga,139,10))),00:00:00)
	ldt_data_consegna = datetime(date(trim(mid(ls_riga,149,10))),00:00:00)
	ls_flag_qualita = trim(mid(ls_riga,159,1))
	
	ls_prog_riga_bol_acq = string(ll_prog_riga_bol_acq)
	
	if ls_cod_fornitore = "" then
		setnull(ls_cod_fornitore)
	end if
	
	if ls_cod_prodotto = "" then		
		setnull(ls_cod_prodotto)		
	else
	
		select cod_misura_mag
		into   :ls_cod_misura
		from   anag_prodotti
		where  cod_azienda  = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore nella select di anag_prodotti: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1
		elseif sqlca.sqlcode = 100 then
			continue
		end if
		
	end if
	
	if isnull(ld_quan_carico) then
		ld_quan_carico = 0
	end if
	
	if isnull(ld_prezzo_carico) then
		ld_prezzo_carico = 0
	end if
	
	if ls_cod_deposito = "" then
		ls_cod_deposito = "001"
	end if
	
	select count(*)
	into   :ll_count
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = :ls_cod_deposito;
			 
	if sqlca.sqlcode < 0 then
		
		fs_messaggio = "Errore nella select di anag_depositi: " + sqlca.sqlerrtext
		fileclose(ll_file)
		return -1
		
	elseif ll_count = 0 then
		
		insert
		into   anag_depositi
			    (cod_azienda,
				 cod_deposito,
				 des_deposito)
		values (:s_cs_xx.cod_azienda,
				 :ls_cod_deposito,
				 :ls_cod_deposito);
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella insert di anag_depositi: " + sqlca.sqlerrtext
			fileclose(ll_file)
			rollback;
			return -1
		end if
		
		commit;
		
	end if
	
	if ls_cod_lotto = "" then		
		setnull(ls_cod_lotto)
		setnull(ldt_data_lotto)
		setnull(ll_prog_lotto)
	else
		
		if isnull(ll_prog_lotto) or ll_prog_lotto = 0 then
			ll_prog_lotto = 10
		end if

		select count(*)
		into   :ll_count
		from   stock
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_lotto and
				 prog_stock = :ll_prog_lotto;
			 
		if sqlca.sqlcode < 0 then
			
			fs_messaggio = "Errore nella select di stock: " + sqlca.sqlerrtext
			fileclose(ll_file)
			return -1
			
		elseif ll_count = 0 then
			
			insert
			into   stock
				    (cod_azienda,
					 cod_prodotto,
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 prog_stock)
			values (:s_cs_xx.cod_azienda,
					 :ls_cod_prodotto,
					 :ls_cod_deposito,
					 :ls_cod_ubicazione,
					 :ls_cod_lotto,
					 :ldt_data_lotto,
					 :ll_prog_lotto);
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di stock: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1
			end if
		
			commit;
		
		end if
		
	end if
	
	if ls_flag_qualita = "" then
		ls_flag_qualita = "N"
	end if	
	
	choose case ls_operazione
			
		case "I"
		
			select count(*)
			into   :ll_count
			from   acc_materiali
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ord_acq and
					 num_registrazione = :ll_num_ord_acq and
					 prog_riga_ordine_acq = :ll_prog_riga_ord_acq and
					 anno_bolla_acq = :ll_anno_bol_acq and
					 num_bolla_acq = :ll_num_bol_acq and
					 prog_riga_bolla_acq = :ls_prog_riga_bol_acq;
				 
			if sqlca.sqlcode < 0 then
				
				fs_messaggio = "Errore nella select di acc_materiali: " + sqlca.sqlerrtext
				fileclose(ll_file)
				return -1
				
			elseif ll_count = 0 then
				
				ll_anno_acc_materiali = f_anno_esercizio()
				
				select max(num_acc_materiali)
				into   :ll_num_acc_materiali
				from   acc_materiali
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_acc_materiali = :ll_anno_acc_materiali;
				
				if sqlca.sqlcode < 0 then
					fs_messaggio = "Errore nella select di acc_materiali: " + sqlca.sqlerrtext
					fileclose(ll_file)
					return -1
				elseif sqlca.sqlcode = 100	or isnull(ll_num_acc_materiali) then
					ll_num_acc_materiali = 1
				else
					ll_num_acc_materiali++
				end if
			
				insert
				into   acc_materiali
				       (cod_azienda,
						 anno_acc_materiali,
						 num_acc_materiali,
						 cod_fornitore,
						 cod_prodotto,
						 cod_prod_fornitore,
						 cod_misura,
						 quan_arrivata,
						 prezzo_acquisto,
						 data_prev_consegna,
						 data_eff_consegna,
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 prog_stock,
						 flag_cq,
						 anno_registrazione,
						 num_registrazione,
						 prog_riga_ordine_acq,
						 anno_bolla_acq,
						 num_bolla_acq,
						 prog_riga_bolla_acq)
				values (:s_cs_xx.cod_azienda,
						 :ll_anno_acc_materiali,
						 :ll_num_acc_materiali,
						 :ls_cod_fornitore,
						 :ls_cod_prodotto,
						 :ls_cod_prod_forn,
						 :ls_cod_misura,
						 :ld_quan_carico,
						 :ld_prezzo_carico,
						 :ldt_data_consegna,
						 :ldt_data_ricezione,
						 :ls_cod_deposito,
						 :ls_cod_ubicazione,
						 :ls_cod_lotto,
						 :ldt_data_lotto,
						 :ll_prog_lotto,
						 :ls_flag_qualita,
						 :ll_anno_ord_acq,
						 :ll_num_ord_acq,
						 :ll_prog_riga_ord_acq,
						 :ll_anno_bol_acq,
						 :ll_num_bol_acq,
						 :ls_prog_riga_bol_acq);
					  
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella insert di acc_materiali: " + sqlca.sqlerrtext
					fileclose(ll_file)
					rollback;
					return -1						
				end if
					
				commit;
			
			elseif ll_count > 0 then
			
				update acc_materiali
				set    cod_fornitore = :ls_cod_fornitore,
						 cod_prodotto = :ls_cod_prodotto,
						 cod_prod_fornitore = :ls_cod_prod_forn,
						 cod_misura = :ls_cod_misura,
						 quan_arrivata = :ld_quan_carico,
						 prezzo_acquisto = :ld_prezzo_carico,
						 data_prev_consegna = :ldt_data_consegna,
						 data_eff_consegna = :ldt_data_ricezione,
						 cod_deposito = :ls_cod_deposito,
						 cod_ubicazione = :ls_cod_ubicazione,
						 cod_lotto = :ls_cod_lotto,
						 data_stock = :ldt_data_lotto,
						 prog_stock = :ll_prog_lotto,
						 flag_cq = :ls_flag_qualita						 
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_ord_acq and
						 num_registrazione = :ll_num_ord_acq and
						 prog_riga_ordine_acq = :ll_prog_riga_ord_acq and
						 anno_bolla_acq = :ll_anno_bol_acq and
						 num_bolla_acq = :ll_num_bol_acq and
						 prog_riga_bolla_acq = :ls_prog_riga_bol_acq;
				  
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella update di acc_materiali: " + sqlca.sqlerrtext
					fileclose(ll_file)
					rollback;
					return -1
				end if
				
				commit;
			
			end if
		
		case "M"
		
			update acc_materiali
			set    cod_fornitore = :ls_cod_fornitore,
					 cod_prodotto = :ls_cod_prodotto,
					 cod_prod_fornitore = :ls_cod_prod_forn,
					 cod_misura = :ls_cod_misura,
					 quan_arrivata = :ld_quan_carico,
					 prezzo_acquisto = :ld_prezzo_carico,
					 data_prev_consegna = :ldt_data_consegna,
					 data_eff_consegna = :ldt_data_ricezione,
					 cod_deposito = :ls_cod_deposito,
					 cod_ubicazione = :ls_cod_ubicazione,
					 cod_lotto = :ls_cod_lotto,
					 data_stock = :ldt_data_lotto,
					 prog_stock = :ll_prog_lotto,
					 flag_cq = :ls_flag_qualita
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ord_acq and
					 num_registrazione = :ll_num_ord_acq and
					 prog_riga_ordine_acq = :ll_prog_riga_ord_acq and
					 anno_bolla_acq = :ll_anno_bol_acq and
					 num_bolla_acq = :ll_num_bol_acq and
					 prog_riga_bolla_acq = :ls_prog_riga_bol_acq;
					  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella update di acc_materiali: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1				
			end if
			
			commit;
		
		case "C"
		
			delete
			from   acc_materiali
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ord_acq and
					 num_registrazione = :ll_num_ord_acq and
					 prog_riga_ordine_acq = :ll_prog_riga_ord_acq and
					 anno_bolla_acq = :ll_anno_bol_acq and
					 num_bolla_acq = :ll_num_bol_acq and
					 prog_riba_bolla_acq = :ll_prog_riga_bol_acq;
					  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella delete di acc_materiali: " + sqlca.sqlerrtext
				fileclose(ll_file)
				rollback;
				return -1
			end if
			
			commit;
		
	end choose	
		
loop

if fileclose(ll_file) = -1 then
	fs_messaggio = "Errore nella chiusura del file"
	return -1
end if
	
if filedelete(ls_nome_file) = false then
	fs_messaggio = "Errore nella cancellazione del file origine dopo l'importazione"
	return -1
end if

return 0
end function

public function integer uof_importa_dati (ref string fs_messaggio);string ls_messaggio


if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", is_volume) = -1 then
	fs_messaggio = "Errore in lettura parametro volume dal registro di sistema"
	f_scrivi_log(fs_messaggio)
	return -1
end if

is_volume = is_volume + "\"

if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "pfi", is_path) = -1 then
	fs_messaggio = "Errore in lettura parametro PFI dal registro di sistema"
	f_scrivi_log(fs_messaggio)
	return -1
end if

if is_path <> "" then
	is_path = is_path + "\"
end if

if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "fmi", is_modalita) = -1 then
	fs_messaggio = "Errore in lettura parametro FMI dal registro di sistema"
	f_scrivi_log(fs_messaggio)
	return -1
end if

select file_fornitori,
       file_clienti,
		 file_prodotti,
		 file_bolle_acq,
		 file_ordini_vendita,
		 flag_fornitori,
		 flag_clienti,
		 flag_prodotti,
		 flag_bolle_acq,
		 flag_ordini_vendita
into   :is_file_fornitori,
       :is_file_clienti,
		 :is_file_prodotti,
		 :is_file_bolle_acq,
		 :is_file_ordini_ven,
		 :is_flag_fornitori,
		 :is_flag_clienti,
		 :is_flag_prodotti,
		 :is_flag_bolle_acq,
		 :is_flag_ordini_ven
from   import_omnia
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore nella select di import_omnia: " + sqlca.sqlerrtext
	f_scrivi_log(fs_messaggio)
	return -1
elseif sqlca.sqlcode = 100 then
	fs_messaggio = "Errore nella select di import_omnia: dati richiesti non presenti"
	f_scrivi_log(fs_messaggio)
	return -1
end if

if is_flag_fornitori = "S" then
	if uof_importa_fornitori(ls_messaggio) = -1 then
		fs_messaggio = "Errore nell'importazione dei fornitori~n" + ls_messaggio
		f_scrivi_log(fs_messaggio)
		return -1
	end if
end if

if is_flag_clienti = "S" then
	if uof_importa_clienti(ls_messaggio) = -1 then
		fs_messaggio = "Errore nell'importazione dei clienti~n" + ls_messaggio
		f_scrivi_log(fs_messaggio)
		return -1
	end if
end if

if is_flag_prodotti = "S" then
	if uof_importa_prodotti(ls_messaggio) = -1 then
		fs_messaggio = "Errore nell'importazione dei prodotti~n" + ls_messaggio
		f_scrivi_log(fs_messaggio)
		return -1
	end if
end if

if is_flag_bolle_acq = "S" then
	if uof_importa_bolle_acq(ls_messaggio) = -1 then
		fs_messaggio = "Errore nell'importazione delle bolle di acquisto~n" + ls_messaggio
		f_scrivi_log(fs_messaggio)
		return -1
	end if
end if

return 0
end function

on uo_importazione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_importazione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

