﻿$PBExportHeader$uo_mrboxer.sru
forward
global type uo_mrboxer from nonvisualobject
end type
end forward

global type uo_mrboxer from nonvisualobject
end type
global uo_mrboxer uo_mrboxer

type prototypes

end prototypes

type variables
// Variabili di 
//		Transazione
//		MultiLineEdit
//		ProgressBar
//		ComandButton
private transaction sqlimport
private multilineedit i_log
private hprogressbar i_pgr
private commandbutton i_button

// Nome dell'applicazione
private string is_message_title = "Importazione dati da portale"

// Variabile per bloccare l'importazione.
private boolean ib_stop = false

// Variabili per statistiche
private int ii_clienti_nuovi = 0
private int ii_pagamento_add, ii_pagamento_misses = 0
private int ii_ordini_nuovi, ii_ordini_errati, ii_ordini_presenti = 0
private int ii_prodotti_nuovi, ii_prodotti_esistenti = 0
private int ii_categorie_nuove = 0
private int ii_ive_nuove = 0
private int ii_righe_nuove = 0

// Variabili di controllo ordini
private dec{4} id_prezzo_totale = 0
private dec{4} id_prezzo_parziale = 0

// Variabili controllo plink
uo_shellexecute io_shell

date id_data_controllo
end variables

forward prototypes
public function integer uof_importa ()
public subroutine uof_setlog (ref multilineedit log)
private function integer uof_carica_ordini ()
private function integer uof_carica_prodotti ()
private function string uof_converti_id (string as_id)
private subroutine uof_log (string as_message, integer ai_priority)
private function integer uof_cliente_add (string as_id)
private function integer uof_cliente_update (string as_id)
private function integer uof_cliente_check (integer ai_id)
private function string uof_iva_check (integer ai_iva)
private function string uof_iva_add (integer ai_iva)
private function integer uof_categoria_add (integer ai_categoria_id)
private function integer uof_ordine_check (integer ai_id, integer ai_anno, integer ai_cliente_id)
public subroutine uof_setprogressbar (ref hprogressbar ah_progressbar)
private function integer uof_ordine_add (integer ai_id, integer ai_anno, integer ai_cod_cliente)
private function string uof_pagamento_check (string as_id)
private function string uof_pagamento_add (string as_descrizione)
private function integer uof_riga_add (integer ai_anno, integer ai_ordine_id, integer ai_riga_ord_ven)
private function integer uof_riga_check (integer ai_ordine_id, integer ai_anno)
public subroutine uof_stop ()
private function integer uof_categoria_check (integer ai_categoria_id)
private subroutine uof_log (string as_message)
private function string uof_timeafter (time at_start, time at_end)
public subroutine uof_setbutton (ref commandbutton ac_button)
public function integer uof_update_ordine_spedito (long al_anno_registrazione, long al_num_registrazione)
public function integer uof_update_ordine_chiuso (long al_anno_registrazione, long al_num_registrazione)
public function integer uof_update_stato_ordine_portale (long al_anno_registrazione, long al_num_registrazione)
public function integer uof_connect_db ()
public function integer uof_disconnect_db ()
public function integer uof_connet_without_msg ()
public function integer uof_update_prezzo_prodotto (string as_codice, double ad_prezzo, ref string as_error)
end prototypes

public function integer uof_importa ();i_log.text = ""
i_log.textcolor = 0
ib_stop = false

if uof_connect_db() = 0 then
	uof_log("Inizio sincronizzazione", 2)
	
	// Prodotti -------------------------------------------
	if not ib_stop then 
		
		if uof_carica_prodotti() > 0 then 
			commit;
			uof_log("Aggiornamento prodotti concluso con successo!", 2)
		else
			rollback;
		end if
			
	end if
	// ---------------------------------------------------
	
	
	// Ordini ---------------------------------------------
	if not ib_stop then 
		
		if uof_carica_ordini() > 0 then
			commit;
			uof_log("Aggiornamento ordini concluso con successo!", 2)
		else
			rollback;
		end if
		
	end if
	// ---------------------------------------------------
	
	
	uof_log("Chiusura canale SSH")
	
	uof_disconnect_db()
	uof_log("Disconesso")
	
	i_button.text = "Importa dati"
	return 0
end if

return 0
end function

public subroutine uof_setlog (ref multilineedit log);i_log = log
end subroutine

private function integer uof_carica_ordini ();// *** stefanop 07/07/2008: *****************************
// Funzione per scaricare tutti i nuovi ordini da remoto a locale.
// *************************************************

string 	ls_id, ls_cliente_id
int 	 	li_id, li_cliente_id, li_anno = 0
long		ll_second_end, ll_num_ordine = 0
double 	ld_totale_ordine = 0
time		lt_start
date		ld_data_ordine



uof_log("Controllo tabella ordini")
lt_start = now()

// Controllo quanti ordini ci sono in tabella per creare l'effetto avanzamento della progressbar
// {
select		count(id)
into		:ll_num_ordine
from		ordine
where		statustipo_id = 1
using		sqlimport;

if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile ")
end if

uof_log("Trovati " + string(ll_num_ordine) + " ordini", 2)
i_pgr.maxposition = ll_num_ordine
i_pgr.position = 0
ll_num_ordine = 0
// }

declare 	cu_ordini cursor for
	select		id,
				cliente_id,
				totale_finale,
				data_ordine
	from		ordine
	where		statustipo_id = 1 and data_ordine >= :id_data_controllo
	order by  id
	using 		sqlimport;
open 		cu_ordini;

if sqlimport.sqlcode <> 0 then
	uof_log("Errore analisi tabella ordini::"+sqlimport.sqlerrtext, -1)
	return -1
end if

uof_log("Sincronizzo ordini...");
Yield()

do while true
	if ib_stop then exit
	
	fetch cu_ordini into :li_id, :li_cliente_id, :ld_totale_ordine, :ld_data_ordine;
	
	// serve exit ma non so ancora perchè 
	if sqlimport.sqlcode = 100 then exit
	if sqlimport.sqlcode < 0 then
		uof_log( "Errore nella scansione ordini::"+sqlimport.sqlerrtext, -1)
		exit
	end if
	
	id_prezzo_totale = ld_totale_ordine
	id_prezzo_parziale = 0
	
	if uof_ordine_check(li_id, year(ld_data_ordine), li_cliente_id) >= 0 then
		//uof_log("flusso chiuso", 2)
		//exit
		commit;
	end if
		
	// aggiorno progressbar e mostro messaggio ogni 10 ordini
	// {
	ll_num_ordine ++
	i_pgr.position = ll_num_ordine
	if mod(ll_num_ordine, 10) =0 then uof_log("controllati "+ string(ll_num_ordine) +" ordini")
	Yield()
	// }
loop

close cu_ordini;

uof_log("Operazione completata in " + uof_timeafter( lt_start, now()) )

// mostro messaggio aggiunti se non ho fermato l'importazione
if not ib_stop then
	uof_log("Aggiunti " + string(ii_ordini_nuovi) + " nuovi ordini", 2)
	if ii_ordini_errati > 0 then
		uof_log("Non è stato possibile aggiungere " + string(ii_ordini_errati) + " ordini", 2)
	end if
end if
i_pgr.position = 0
Yield()

return 0

end function

private function integer uof_carica_prodotti ();// *** stefanop 24/06/2008: *****************************
// Funzione per scaricare tutti i nuovi prodotti da remoto a locale.
// *************************************************
boolean 	lb_taxed
string 	ls_nome_it, ls_codice, ls_fix_id, ls_locale_id, ls_id, ls_cod_iva, ls_categoria_id, ls_cod_prodotto
long 		ll_peso, ll_prezzo_listino, ll_prezzo_mb, ll_aliquota, ll_id, ll_taxed, ll_categoria_id,ll_time_offset = 0
long 		ll_step = 0
time 		lt_start
date 		ld_start_date, ld_last_modified


uof_log("Controllo tabella prodotti")
lt_start = now()

// calcolo numero prodotti per la progress bar
// {
select count(id)
into :ll_step
from public.prodotto
using sqlimport;

uof_log("Trovati " + string(ll_step) + " prodotti")
i_pgr.maxposition = ll_step
i_pgr.position = 0
ll_step = 0
// }

declare cu_prodotti cursor for
	select
		id,
		nome_it,
		categoria_id,
		peso,
		prezzo_listino,
		prezzo_mb,
		codice,
		start_date,
		last_modified,
		taxed,
		aliquota	
	from prodotto
	using sqlimport;
open cu_prodotti;

if sqlimport.sqlcode <> 0 then
	uof_log(sqlimport.sqlerrtext, -1)
	return -1
end if

uof_log("Sincronizzo prodotti...")
Yield()

// Scorro tutti gli elementi remoti e controllo se c'è ne sono di nuovi
do while true
	Yield()
	if ib_stop then exit
	
	fetch cu_prodotti into
		:ls_id,
		:ls_nome_it,
		:ll_categoria_id,
		:ll_peso,
		:ll_prezzo_listino,
		:ll_prezzo_mb,
		:ls_codice,
		:ld_start_date,
		:ld_last_modified,
		:lb_taxed,
		:ll_aliquota;
		
	if sqlimport.sqlcode = 100 then exit
	
	if sqlimport.sqlcode <> 0 then
		uof_log(sqlimport.sqlerrtext, -1)
		return -1
	end if
	
	// *** stefanop 29/08/2008: cambio l'id con il codice prodotto
	// Fisso l'id per una ricerca sicura.
	// ls_fix_id = right("000000"+ls_codice, 6)
	if len(trim(ls_codice)) > 15 then
		int li_message_return
		li_message_return = messagebox("Apice", "Attenzione il prodotto " + ls_id + "riporta  un codice troppo grande.~r~nIl limite massimo è di 15 caratteri~r~n~r~nContinuare l'importazione?", Exclamation!,YesNo! ,1)
		
		if li_message_return =  1 then
			uof_log("Il prodotto " + ls_id + " riporta un codice troppo grande. (max 15 caratteri)");
		else
			uof_log("Il prodotto " + ls_id + " riporta un codice troppo grande. (max 15 caratteri)", -1);
			ib_stop = true;
			rollback;
			return -1;
			exit;
		end if
	end if;
	
	ls_fix_id = right("000000000000000"+trim(ls_codice), 15)

	// La categoria potrebbe essere vuota
	if not isnull(ll_categoria_id) then
		if uof_categoria_check(ll_categoria_id) < 0 then 
			uof_log("Categoria errata " + string(ll_categoria_id), -1)
			continue
		end if
		ls_categoria_id = right("000"+string(ll_categoria_id), 3)
	else
		setNull(ls_categoria_id)
	end if
	
	ls_cod_iva = uof_iva_check(ll_aliquota)
	if ls_cod_iva = "-1" then 
		uof_log("Iva errata", -1)
		exit	
	end if
	
	// Seleziono l'id nell database locale e se presente non inserisco nulla.
	select 	cod_prodotto
	into 		:ls_cod_prodotto
	from 		anag_prodotti
	where 	cod_prodotto=:ls_fix_id
	using 		sqlca;
	
	if sqlca.sqlcode < 0 then
		uof_log("Impossibile trovare id: ~r~n"+sqlca.sqlerrtext, -1)
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		insert into anag_prodotti (
			cod_azienda,
			cod_prodotto,
			des_prodotto,
			cod_cat_mer,
			peso_lordo,
			prezzo_vendita,
			prezzo_acquisto,
			cod_comodo,
			data_creazione,
			cod_iva,
			cod_misura_mag,
			cod_misura_acq,
			cod_misura_ven
			)
		values (
			:s_cs_xx.cod_azienda,
			:ls_fix_id,
			:ls_nome_it,
			:ls_categoria_id,
			:ll_peso,
			:ll_prezzo_listino,
			:ll_prezzo_mb,
			:ls_codice,
			:ld_start_date,
			:ls_cod_iva,
			'PZ',
			'PZ',
			'PZ'		
			);
			
		if sqlca.sqlcode < 0 then 
			uof_log("Impossibile aggiungere prodotto.::"+sqlca.sqlerrtext, -1)
			exit
		end if

		ii_prodotti_nuovi ++
	else
		//uof_log("uguale " + ls_fix_id);
		ii_prodotti_esistenti++
	end if
		
	// mostra un messaggio ogni 100 prodotti aggiunti.
	//if mod(ll_prodotti_nuovi, 100) =0 and ll_prodotti_nuovi > 0 then uof_log("Aggiunti "+ string(ll_prodotti_nuovi) +" prodotti")
	i_pgr.position = ll_step
	ll_step++
	Yield()
loop

close cu_prodotti;
commit;


uof_log("Operazione completata " + uof_timeafter(lt_start, now()), 2)

// Mostro messaggio solo se non ho fermato l'operazione
if not ib_stop then uof_log("Aggiunti "+ string(ii_prodotti_nuovi)+ " prodotti ", 2)

i_pgr.position = 0
Yield()

return ii_prodotti_nuovi
end function

private function string uof_converti_id (string as_id);//*** Stefanop - 24/06/2008
// La funziona prende un id e lo converte in una stringa riempita a sinistra da 0
// E.S: l'id 25 corrisponderà ad 000025

string ls_new_id, ls_empty_string = "000000"

ls_new_id = ls_empty_string + as_id

return right(ls_new_id, 6)

end function

private subroutine uof_log (string as_message, integer ai_priority);string ls_message, ls_error_message, ls_error_code
string ls_new_line = "~r~n"

// Consente di usare le  funzioni anche senza impostare la finestra di log
if isnull(i_log) then return

choose case ai_priority
		
	case -1
		// controllo errore
		
		int li_pos
		li_pos = pos(as_message, "::")
		if li_pos > 0 then
			ls_error_message = mid(as_message, 0, li_pos -1)
			ls_error_code = mid(as_message, li_pos + 2)
		else
			ls_error_message = as_message
		end if
		
		ls_message  = ls_new_line + "Errore" + ls_new_line
		ls_message += "–––––––––––––––––––––––––––––––––" + ls_new_line
		ls_message += "* " + ls_error_message + ls_new_line
		ls_message += "* " + ls_error_code + ls_new_line
		ls_message += "–––––––––––––––––––––––––––––––––" + ls_new_line
		i_log.textcolor = 125

	case 1
		ls_message += "––––––––––––––––––––––––––––––––––––" + ls_new_line
		ls_message += "* " + as_message + ls_new_line
		ls_message += "––––––––––––––––––––––––––––––––––––" + ls_new_line

	case 2
		ls_message   = "- " + as_message + ls_new_line
		ls_message += "–––––––––––––––––––––––––––––––––" + ls_new_line
		
	case 3
		ls_message  = "–––––––––––––––––––––––––––––––––" + ls_new_line
		ls_message += "- " + as_message + ls_new_line
		
	case else
		ls_message = "- " + as_message + ls_new_line

end choose

try 	
	i_log.text += ls_message
	i_log.scroll(i_log.linecount())
catch (throwable toe)
	messagebox(is_message_title,"Non posso loggare")
end try
end subroutine

private function integer uof_cliente_add (string as_id);// Aggiunge un cliente al Database locale facendo distinzione se è privato o azienda
// tale distinzione avviene dalla variabile "ls_tipo" se 0 privato se 1 azienda
int 	li_id, li_remote_id
string ls_email, ls_tipo, ls_nome, ls_cognome, ls_rag_soc, ls_piva, ls_indirizzo, ls_citofono, ls_frazione, ls_comune
string ls_provincia, ls_cap, ls_stato, ls_telefono, ls_fax, ls_presso, ls_rag_soc_1, ls_rag_soc_2
date	ld_end_date, ld_start_date
char 	lc_enabled

li_remote_id = integer(as_id)

select
	id,
	email,
	tipo,
	nome,
	cognome,
	ragionesociale,
	piva,
	indirizzo,
	citofono,
	frazione,
	comune,
	provincia,
	cap,
	stato,
	telefono,
	fax,
	presso,
	enabled,
	end_date,
	start_date
into
	:li_id,
	:ls_email, 
	:ls_tipo,
	:ls_nome,
	:ls_cognome,
	:ls_rag_soc,
	:ls_piva,
	:ls_indirizzo,
	:ls_citofono,
	:ls_frazione,
	:ls_comune, 
	:ls_provincia,
	:ls_cap,
	:ls_stato, 
	:ls_telefono,
	:ls_fax, 
	:ls_presso, 
	:lc_enabled,
	:ld_end_date, 
	:ld_start_date
from
	public.cliente
where
	public.cliente.id = :li_remote_id
using sqlimport;


if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile prelevare informazioni utente da remoto.::"+sqlimport.sqlerrtext, -1)
	return -1
end if

// Preparo valori per l'inserimento
if lc_enabled = "1" then
	lc_enabled = "N"
else
	lc_enabled = "S"
end if

ls_comune = ls_comune + " - " + ls_stato

if ls_tipo = "0" then
	// Privato
	ls_rag_soc = ls_cognome + " " + ls_nome
	ls_rag_soc_1 =mid(ls_rag_soc, 0, 40)
	ls_rag_soc_2 = mid(ls_rag_soc, 40)
else
	// Azienda
	ls_rag_soc_1 =mid(ls_rag_soc , 0, 40)
	ls_rag_soc_2 = mid(ls_rag_soc, 40)
end if


insert into anag_clienti (
	cod_azienda,
	cod_cliente,
	casella_mail,
	rag_soc_1,
	rag_soc_2,
	indirizzo,
	rag_soc_abbreviata,
	frazione,
	localita,
	provincia,
	cap,
	telefono,
	fax,
	flag_blocco,
	data_blocco,
	data_creazione,
	cod_deposito,
	cod_valuta)
values (
	:s_cs_xx.cod_azienda,
	:as_id,
	:ls_email,
	:ls_rag_soc_1,
	:ls_rag_soc_2,
	:ls_indirizzo,
	:ls_citofono,
	:ls_frazione,
	:ls_comune,
	:ls_provincia,
	:ls_cap,
	:ls_telefono,
	:ls_fax,
	:lc_enabled,
	:ld_end_date,
	:ld_start_date,
	'001',
	'EUR'
	)
using sqlca;
	
if sqlca.sqlcode <> 0 then
	uof_log("Impossibiole aggiungere nuovo cliente::"+sqlca.sqlerrtext, -1)
	return -1
end if

ii_clienti_nuovi ++
return 0
end function

private function integer uof_cliente_update (string as_id);// Aggiorno dati cliente del database locale
// distinzione tra privato e azienda avviene tramite la variabile "ls_tipo" se 0 privato se 1 azienda
int li_id, li_remote_id
string ls_email, ls_tipo, ls_nome, ls_cognome, ls_rag_soc, ls_piva, ls_indirizzo, ls_citofono, ls_frazione, ls_comune
string ls_provincia, ls_cap, ls_stato, ls_telefono, ls_fax, ls_presso, ls_rag_soc_1, ls_rag_soc_2
date ld_end_date, ld_start_date
char lc_enabled

li_remote_id = integer(as_id)

select
	id,
	email,
	tipo,
	nome,
	cognome,
	ragionesociale,
	piva,
	indirizzo,
	citofono,
	frazione,
	comune,
	provincia,
	cap,
	stato,
	telefono,
	fax,
	presso,
	enabled,
	end_date,
	start_date
into
	:li_id,
	:ls_email, 
	:ls_tipo,
	:ls_nome,
	:ls_cognome,
	:ls_rag_soc,
	:ls_piva,
	:ls_indirizzo,
	:ls_citofono,
	:ls_frazione,
	:ls_comune, 
	:ls_provincia,
	:ls_cap,
	:ls_stato, 
	:ls_telefono,
	:ls_fax, 
	:ls_presso, 
	:lc_enabled,
	:ld_end_date, 
	:ld_start_date
from
	public.cliente
where
	public.cliente.id = :li_remote_id
using sqlimport;


if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile prelevare informazioni utente da remoto.::"+sqlimport.sqlerrtext, -1)
	return -1
end if

// Preparo i valori per l'inserimento
if lc_enabled = "1" then
	lc_enabled = "N"
else
	lc_enabled = "S"
end if

ls_comune = ls_comune + " - " + ls_stato

if ls_tipo = "0" then
	// Privato
	ls_rag_soc = ls_cognome + " " + ls_nome
	ls_rag_soc_1 =mid(ls_rag_soc, 0, 40)
	ls_rag_soc_2 = mid(ls_rag_soc, 40)
else
	// Azienda
	ls_rag_soc_1 =mid(ls_rag_soc , 0, 40)
	ls_rag_soc_2 = mid(ls_rag_soc, 40)
end if
	
update anag_clienti 
set
	cod_azienda = :s_cs_xx.cod_azienda,
	cod_cliente = :as_id,
	casella_mail = :ls_email,
	rag_soc_1 = :ls_rag_soc_1,
	rag_soc_2 = :ls_rag_soc_2,
	indirizzo = :ls_indirizzo,
	rag_soc_abbreviata = :ls_citofono,
	frazione = :ls_frazione,
	localita = :ls_comune,
	provincia = :ls_provincia,
	cap = :ls_cap,
	telefono = :ls_telefono,
	fax = :ls_fax,
	flag_blocco = :lc_enabled,
	data_blocco = :ld_end_date,
	data_creazione = :ld_start_date
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :as_id
using sqlca;

if sqlca.sqlcode <> 0 then
	uof_log("Impossibiole aggiungere nuovo cliente::"+sqlca.sqlerrtext, -1)
	return -1
end if

return 0
end function

private function integer uof_cliente_check (integer ai_id);// Controllo l'esistenza dell'utente, se prense aggiorno i dati mentre se non lo trovo lo aggiungo.

string ls_cod_cliente_temp, ls_cod_cliente

ls_cod_cliente = right("000000"+string(ai_id), 6)

select  cod_cliente
into	 :ls_cod_cliente_temp
from	 anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente
using 	 sqlca;

if sqlca.sqlcode < 0 then
	uof_log("Impossibile valutare il cliente::"+sqlca.sqlerrtext, -1)
	return -1
	
elseif sqlca.sqlcode = 100 then
	return uof_cliente_add(ls_cod_cliente)
	
else
	return uof_cliente_update(ls_cod_cliente)
end if

return 0
end function

private function string uof_iva_check (integer ai_iva);// Controllo l'esistenza dell'iva nella tabella tab_ive  restituisco il codice id
string ls_cod_iva

select		cod_iva
into		:ls_cod_iva
from		tab_ive
where		aliquota = :ai_iva
using		sqlca;
	
if sqlca.sqlcode < 0 then
	uof_log("Errore ive::" + sqlca.sqlerrtext, -1)
	return "-1"
elseif sqlca.sqlcode = 100 then
	return uof_iva_add(ai_iva)
else
	return ls_cod_iva
end if

return "-1"
end function

private function string uof_iva_add (integer ai_iva);// Aggiungo IVA
string ls_cod_iva

ls_cod_iva = right("000"+string(ai_iva), 3)

insert into tab_ive (
	cod_azienda,
	cod_iva,
	aliquota)
values (
	:s_cs_xx.cod_azienda,
	:ls_cod_iva,
	:ai_iva)
using
	sqlca;
	
if sqlca.sqlcode <> 0 then
	uof_log("Impossibile aggiungere IVA::"+sqlca.sqlerrtext, -1)
	return "-1"
end if

ii_ive_nuove ++
return ls_cod_iva
end function

private function integer uof_categoria_add (integer ai_categoria_id);string ls_cod_cat_mer, ls_categoria_nome, ls_categoria_descr

ls_cod_cat_mer = right("000"+string(ai_categoria_id), 3)

// da Scommentare appena si hanno i privilegi della tabella
//select
//	categoria_nome_it,
//	categoria_descr_it
//into
//	:ls_categoria_nome,
//	:ls_categoria_descr
//from
//	public.categoria
//where
//	id = :ai_categoria_id
//using
//	sqlimport;
//	
//if sqlimport.sqlcode <> 0 then
//	uof_log("Impossibile recuperare informazioni categoria::"+sqlimport.sqlerrtext, -1)
//	return -1
//end if

insert into tab_cat_mer (
	cod_azienda,
	cod_cat_mer,
	des_cat_mer,
	nota)
values (
	:s_cs_xx.cod_azienda,
	:ls_cod_cat_mer,
	:ls_categoria_nome,
	:ls_categoria_descr )
using
	sqlca;
	
if sqlca.sqlcode <> 0 then
	uof_log("Impossibile inserire nuova categoria::"+sqlca.sqlerrtext, -1)
	return -1;
end if

ii_categorie_nuove ++
return 0
end function

private function integer uof_ordine_check (integer ai_id, integer ai_anno, integer ai_cliente_id);// *** stefanop 04/07/2008: Controllo se l'ordine esiste nel DB locale

string ls_num_registrazione

select		num_registrazione
into		:ls_num_registrazione
from		tes_ord_ven
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno and
			num_registrazione = :ai_id
using		sqlca;
	
if sqlca.sqlcode < 0 then
	uof_log("Impossibile ispezionare tabella ordini::" +sqlca.sqlerrtext, -1)
	return -1
elseif sqlca.sqlcode = 100 then
	
	// Controllo che l'utente sia presente in tabella e aggiungo se necessario
	if uof_cliente_check(ai_cliente_id) < 0 then return -1
	
	// Aggiungo ordine, l'altrimenti non posso controllare le righe, perchè hanno chiave esterna sull'ordine
	if uof_ordine_add(ai_id, ai_anno, ai_cliente_id) < 0 then return -1
	
	// Controllo le righe dell'ordine e aggiungo se necessario
	if uof_riga_check(ai_id, ai_anno) < 0 then return -1
	
	if id_prezzo_totale <> id_prezzo_parziale then
		ii_ordini_errati ++
		uof_log("L'ordine " + string(ai_id) + " riporta prezzo tot.: " + string(id_prezzo_totale) + " e come prezzo parz.: " + string(id_prezzo_parziale)) 
		//rollback;
		return -1
	end if
	
	ii_ordini_nuovi ++
	return 0
else
	return 0
end if

	
return -1
end function

public subroutine uof_setprogressbar (ref hprogressbar ah_progressbar);i_pgr = ah_progressbar
end subroutine

private function integer uof_ordine_add (integer ai_id, integer ai_anno, integer ai_cod_cliente);string 	ls_nome, ls_cognome, ls_indirizzo, ls_citofono, ls_frazione, ls_comune, ls_provincia, ls_cap, ls_stato, ls_telefono
string 	ls_cellulare, ls_fax, ls_pagamento_note, ls_presso, ls_pagamento_id, ls_cod_cliente
date		ldd_data_ordine
datetime ldt_data_ordine

select	delivery_nome,
			delivery_cognome,
			delivery_presso,
			delivery_indirizzo,
			delivery_citofono,
			delivery_frazione,
			delivery_comune,
			delivery_provincia,
			delivery_cap,
			delivery_stato,
			delivery_telefono,
			delivery_cellulare,
			delivery_fax,
			pagamento_note,
			pagamento_id,
			data_ordine
into		:ls_nome,
			:ls_cognome,
			:ls_presso,
			:ls_indirizzo,
			:ls_citofono,
			:ls_frazione,
			:ls_comune,
			:ls_provincia,
			:ls_cap,
			:ls_stato,
			:ls_telefono,
			:ls_cellulare,
			:ls_fax,
			:ls_pagamento_note,
			:ls_pagamento_id,
			:ldd_data_ordine
from		ordine
where		id = :ai_id
using		sqlimport;

if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile leggere dettaglio ordine::"+sqlimport.sqlerrtext)
	return -1
end if

// Imposto e controllo i valori per il db locale
string ls_rag_soc_1, ls_nota_testata, ls_localita, ls_nota_piede, ls_cod_pagamento
ls_rag_soc_1 = ls_nome + " " + ls_cognome
ls_nota_testata = ""
if not isnull(ls_citofono) then ls_nota_testata = "CITOFONO= " + ls_citofono

ls_localita = ls_comune + " - " + ls_stato
ls_cod_cliente = right("000000" + string(ai_cod_cliente), 6)

ls_nota_piede = ""
if not isnull(ls_telefono) 			  then ls_nota_piede += "TELEFONO: " + ls_telefono + "~r~n"
if not isnull(ls_cellulare) 			  then ls_nota_piede += "CELLULARE: " + ls_cellulare + "~r~n"
if not isnull(ls_fax) 	   			  then ls_nota_piede += "FAX: " + ls_fax + "~r~n"
if not isnull(ls_pagamento_note) then ls_nota_piede += "PAGAMENTO NOTE: " + ls_pagamento_note + "~r~n"

ls_cod_pagamento = uof_pagamento_check(ls_pagamento_id)

ldt_data_ordine = datetime(ldd_data_ordine, 00:00:00)

insert into tes_ord_ven
	( 
		cod_azienda,
		anno_registrazione,   
		num_registrazione,   
		cod_tipo_ord_ven,
		cod_cliente,
		rag_soc_1,   
		rag_soc_2,   
		indirizzo,
		nota_testata,
		frazione,   
		localita,
		provincia,
		cap,   
		cod_pagamento,   
		nota_piede,
		data_registrazione,
		cod_valuta,
		cod_deposito
 	)  
values 
  (
  		:s_cs_xx.cod_azienda,
		:ai_anno,
		:ai_id,
		'001',
		:ls_cod_cliente,
		:ls_rag_soc_1,
		:ls_presso,
		:ls_indirizzo,
		:ls_nota_testata,
		:ls_frazione,
		:ls_localita,
		:ls_provincia,
		:ls_cap,
		:ls_cod_pagamento,
		:ls_nota_piede,
		:ldt_data_ordine,
		'EUR',
		'001'
)
using	sqlca;

if sqlca.sqlcode <> 0 then
	uof_log("Impossibile aggiungere ordine::" + sqlca.sqlerrtext, -1)
	return -1
end if

return 0

end function

private function string uof_pagamento_check (string as_id);// Controllo se il pagamento esiste
string ls_id

select		cod_pagamento
into		:ls_id
from 		tab_pagamenti
where 	cod_azienda = :s_cs_xx.cod_azienda and
			des_pagamento = :as_id
using 		sqlca;

if sqlca.sqlcode < 0 then
	uof_log("Impossibile controllare pagamento " + ls_id + "::" + sqlca.sqlerrtext, -1)
	return "-1"
elseif sqlca.sqlcode = 100 then
	return uof_pagamento_add(as_id)
else
	return ls_id
end if
end function

private function string uof_pagamento_add (string as_descrizione);// aggiungo il nuovo pagamento
string ls_descrizione, ls_id, ls_cod_pagamento
integer li_cod_pagamento
date ld_end_date

//select		descrizione_it,
//			end_date
//into		:ls_descrizione,
//			:ld_end_date
//from		pagamento
//where		id = :ai_id
//using 		sqlimport;
//
//if sqlimport.sqlcode <> 0 then
//	uof_log("Impossibile recuperare il pagamento "+string(ai_id)+"::"+sqlimport.sqlerrtext, -1)
//	return "-1"
//end if

// Prelevo il masismo id per il pagamento
select		max(cod_pagamento)
into 		:ls_cod_pagamento
from		tab_pagamenti
using		sqlca;

if isnull(ls_cod_pagamento) then
	li_cod_pagamento = 0
else
	li_cod_pagamento = integer(ls_cod_pagamento)
end if

li_cod_pagamento ++
ls_cod_pagamento = right("000"+ string(li_cod_pagamento), 3)


ls_id = ls_cod_pagamento
ls_descrizione = left(ls_descrizione, 40)

insert into tab_pagamenti (
	cod_azienda,
	cod_pagamento,
	des_pagamento )
values (
	:s_cs_xx.cod_azienda,
	:ls_id,
	:as_descrizione )
using
	sqlca;
	
if sqlca.sqlcode <> 0 then
	uof_log("Impossibile salvare pagamento "+ls_id+"::"+sqlca.sqlerrtext, -1)
	ii_pagamento_misses++
	return "-1"
end if

ii_pagamento_add++
return ls_id
end function

private function integer uof_riga_add (integer ai_anno, integer ai_ordine_id, integer ai_riga_ord_ven);// *** Aggiungo nuova riga ordine nel DB
char 		lc_taxed
string	ls_codice, ls_descrizione, ls_variante, ls_nota_dettaglio, ls_cod_iva, ls_fix_id, ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_misura
integer	li_prodotto_id, li_quantita
long		ll_aliquota
decimal   ld_prezzo_tassato, ld_prezzo_non_tassato

select 	prodotto_id,
			codice,
			descrizione,
			variante,
			quantita,
			prezzo_tassato,
			taxed,
			aliquota
into		:li_prodotto_id,
			:ls_codice,
			:ls_descrizione,
			:ls_variante,
			:li_quantita,
			:ld_prezzo_tassato,
			:lc_taxed,
			:ll_aliquota
from		ordine_riga
where		id = :ai_riga_ord_ven
using		sqlimport;

if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile controllare riga ordine remota::"+sqlimport.sqlerrtext, -1)
	return -1
end if
Yield()
// Preparo i campi per essere salvati nel database locale
ls_nota_dettaglio = ''
if not isnull(ls_codice) then	ls_nota_dettaglio += "CODICE INT=" + ls_codice 
if not isnull(ls_variante) then ls_nota_dettaglio += "~r~nVARIANTE=" + ls_variante

id_prezzo_parziale += ld_prezzo_tassato

if lc_taxed = "1" then
	ld_prezzo_non_tassato = ld_prezzo_tassato / 1.2
else
	ld_prezzo_non_tassato = ld_prezzo_tassato
end if

ls_cod_iva =uof_iva_check(ll_aliquota)
ls_descrizione = left(ls_descrizione, 40)

// ------------  ENME 23/7/2008   ----------------------
if isnull(li_prodotto_id) or li_prodotto_id = 0 then
	
	setnull(ls_fix_id)
	ls_cod_tipo_det_ven = "DES"
	ls_cod_misura = "PZ"
	
else
	
	// *** stefanop 289/08/2008: cambiato id del codice
	ls_fix_id = right("000000000000000"+ ls_codice, 15)

	select cod_misura_ven
	into   :ls_cod_misura
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	  		 cod_prodotto = :ls_fix_id;

	if sqlca.sqlcode <> 0 then
		messagebox("IMPORT", "Impossibile trovare il prodotto " + ls_fix_id)
		return -1
	end if
	
	if isnull(ls_cod_misura) then ls_cod_misura = "PZ"
	
	ls_cod_tipo_det_ven = "MAG"
	
end if

ls_cod_prodotto = ls_fix_id

// ----------------------------------------------------------


insert into det_ord_ven (
				cod_azienda,
				anno_registrazione,
				num_registrazione,
				prog_riga_ord_ven,
				cod_tipo_det_ven,
				cod_prodotto,
				cod_misura,
				nota_dettaglio,
				des_prodotto,
				quan_ordine,
				prezzo_vendita,
				cod_iva)
values	(
			:s_cs_xx.cod_azienda,
			:ai_anno,
			:ai_ordine_id,
			:ai_riga_ord_ven,
			:ls_cod_tipo_det_ven,
			:ls_cod_prodotto,
			:ls_cod_misura,
			:ls_nota_dettaglio,
			:ls_descrizione,
			:li_quantita,
			:ld_prezzo_non_tassato,
			:ls_cod_iva)
using		sqlca;

if sqlca.sqlcode <> 0 then
	uof_log("Impossibile aggiungere riga ordine::"+sqlca.sqlerrtext, -1)
	return -1
end if

ii_righe_nuove ++
Yield()			
return 0
end function

private function integer uof_riga_check (integer ai_ordine_id, integer ai_anno);// Controllo le righe dei prodoti
int 	li_id
string ls_codice, ls_descrizione, ls_variante, ls_ord_riga_ven, ls_mr_codice
char 	lc_taxed

long ll_num_riga_ordine = 0

// Ritorna 0 perchè nel database remoto ci sono ordini relativi alle spese spedizioni ed altro che non hanno ID
if isnull(ai_ordine_id) then return 0

declare 	cu_ordine_riga cursor for
select		id
from		ordine_riga
where		ordine_id = :ai_ordine_id
using		sqlimport;
open 		cu_ordine_riga;

if sqlimport.sqlcode <> 0 then
	uof_log("Impossibile aprire la tabella riga ordine::"+sqlimport.sqlerrtext, -1)
	return -1
end if

Yield()
	
do while true
	// Ho tutte le righe relative all'ordine con id = ai_ordine_id
	fetch cu_ordine_riga into :li_id;
	
	if sqlimport.sqlcode = 100 then exit
	if sqlimport.sqlcode <> 0 then
		uof_log("Impossibile ispezionare la riga ordine::"+sqlimport.sqlerrtext, -1)
		exit
	end if
	
	ll_num_riga_ordine ++
	
	// La riga esiste nel Database locale?
	select		prog_riga_ord_ven
	into		:ls_ord_riga_ven
	from		det_ord_ven
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ai_anno and
				num_registrazione = :ai_ordine_id and
				prog_riga_ord_ven = :li_id
	using		sqlca;
		
	if sqlca.sqlcode < 0 then
		uof_log("Impossibile controllare la riga ordine locale::"+sqlca.sqlerrtext, -1)
		exit
	elseif sqlca.sqlcode = 100 then
		uof_riga_add(ai_anno, ai_ordine_id, li_id)
	end if
	
	Yield()
loop

close cu_ordine_riga;
Yield()
return 0
end function

public subroutine uof_stop ();ib_stop = true
uof_log("Tento di chiudere la sincronizzazione")
Yield()
end subroutine

private function integer uof_categoria_check (integer ai_categoria_id);// Controllo e aggiungo, se necessario, la categoria del prodotto
string ls_id

if isnull(ai_categoria_id) then return -1

ls_id = right("000"+string(ai_categoria_id), 3)

select		cod_cat_mer
into		:ls_id
from		tab_cat_mer
where		cod_azienda = :s_cs_xx.cod_azienda and
			cod_cat_mer = :ls_id
using 		sqlca;
	
if sqlca.sqlcode < 0 then
	uof_log("Impossibile controllare categoria::"+sqlca.sqlerrtext, -1)
	return -1
	
elseif sqlca.sqlcode = 100 then
	if uof_categoria_add(ai_categoria_id) = 0 then return ai_categoria_id
	
else
	return ai_categoria_id
end if

return -1
end function

private subroutine uof_log (string as_message);uof_log( as_message, 99)
end subroutine

private function string uof_timeafter (time at_start, time at_end);// *** stefanop 08/07/2008: *****************************
// Funzione che calcola il tempo trascorso in base a due orari
// *************************************************
long ll_seconds, ll_sec, ll_min, ll_ore = 0

ll_seconds = SecondsAfter(at_start, at_end)

if ll_seconds < 0 or isnull(ll_seconds) then return "0 sec"

// ore
if ll_seconds >= 3600 then 
	ll_ore = int(ll_seconds / 3600)
	ll_seconds = ll_seconds - (ll_ore * 3600)
end if

// minuti
if ll_seconds >= 60 then 
	ll_min = int(ll_seconds / 60)
	ll_seconds = ll_seconds - (ll_min * 60)
end if

return string(ll_ore, "00") + ":" + string(ll_min, "00") + ":" + string(ll_seconds, "00")
end function

public subroutine uof_setbutton (ref commandbutton ac_button);i_button = ac_button
end subroutine

public function integer uof_update_ordine_spedito (long al_anno_registrazione, long al_num_registrazione);// *** stefanop 08/07/2008: *****************************
// Funzione per impostare lo stato spedito ad un ordine
// *************************************************

// codice e stato ordini
// 0 - Pendente
// 1 - in Lavorazione
// 2 - Spedito
// 3 - Aggiornato
// 4 - Chiuso
// 5 - in Carico

// Prima di eseguire questa operazione lanciare la connessione al database.

update ordine
set	  statustipo_id = 2
where	  id = :al_num_registrazione
using	  sqlimport;

if sqlimport.sqlcode <> 0 then
	messagebox(is_message_title, 	"Impossibile aggiornare stato ordine~r~n"+sqlimport.sqlerrtext)
	return -1
end if
	
commit;

return 0
end function

public function integer uof_update_ordine_chiuso (long al_anno_registrazione, long al_num_registrazione);// *** stefanop 08/07/2008: *****************************
// Funzione per impostare lo stato chiuso ad un ordine
// *************************************************

// codice e stato ordini
// 0 - Pendente
// 1 - in Lavorazione
// 2 - Spedito
// 3 - Aggiornato
// 4 - Chiuso
// 5 - in Carico

// Prima di eseguire questa operazione lanciare la connessione al database.

update ordine
set	  statustipo_id = 4
where	  id = :al_num_registrazione
using	  sqlimport;

if sqlimport.sqlcode <> 0 then
	messagebox(is_message_title, 	"Impossibile aggiornare stato ordine~r~n"+sqlimport.sqlerrtext)
	return -1
end if
	
commit;

return 0
end function

public function integer uof_update_stato_ordine_portale (long al_anno_registrazione, long al_num_registrazione);// *** stefanop 21/07/2008: Controllo automantico per l'impostazione dei campi statustipo_id
string	ls_cod_pagamento, ls_contrassegno

select  cod_pagamento
into	 :ls_cod_pagamento
from	 tes_ord_ven
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione
using	 sqlca;

if sqlca.sqlcode < 0 then return -1

if sqlca.sqlcode = 100 then 
	messagebox("Apice", "Il campo Codice Pagamento non esiste, Impossibile continuare!", StopSign!)
	return -1
end if

if not isnull(ls_cod_pagamento) and ls_cod_pagamento <> "" then
	
	// Controllo se è contrassegno o bonifico
	select flag_contrassegno
	into	 :ls_contrassegno
	from	 tab_pagamenti
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_pagamento = :ls_cod_pagamento
	using	 sqlca;
	
	if sqlca.sqlcode < 0 then return -1
	
	if sqlca.sqlcode = 100 then
		messagebox("Apice", "Impossibile determinare modalità di pagamento!", StopSign!)
		return -1
	end if
	
	if ls_contrassegno = "S" then
		uof_update_ordine_spedito(al_anno_registrazione, al_num_registrazione)
	else
		uof_update_ordine_chiuso(al_anno_registrazione, al_num_registrazione)
	end if
	
	return 0
	
end if

return 0
end function

public function integer uof_connect_db ();// Imposto i parametri per la connessione remota con il server
// Profilo per MrBoxer con SSH
sqlimport 				= create transaction
sqlimport.DBMS 		= "ODBC"
sqlimport.AutoCommit = False
sqlimport.DBParm 		= "ConnectString='DSN=MrBoxer SSH;UID=;PWD='"

uof_log("Connessione remota in corso...")
sleep(3)
try 
	
	uof_log("Prova ad aprire un canale SSH")
	connect using sqlimport;
	
	if sqlimport.sqlcode < 0 then
		uof_log("Impossibile aprire il tunnel, probabilmente non è stato avviato!::"+sqlimport.sqlerrtext, -1)
		return -1
	end if

	uof_log("Canale SSH OK")
	
catch (throwable t_error)
	uof_log("Impossibile aprire il tunnel SSH, probabilmente non è stato avviato!", -1)
	i_button.text = "Importa dati"
	return -1
end try

return 0
end function

public function integer uof_disconnect_db ();disconnect using sqlimport;

destroy sqlimport

return 0
end function

public function integer uof_connet_without_msg ();// Imposto i parametri per la connessione remota con il server
// Profilo per MrBoxer con SSH
sqlimport 				= create transaction
sqlimport.DBMS 		= "ODBC"
sqlimport.AutoCommit = False
sqlimport.DBParm 		= "ConnectString='DSN=MrBoxer SSH;UID=;PWD='"

sleep(3)
try 
	
	connect using sqlimport;
	
	if sqlimport.sqlcode < 0 then
		messagebox("Esportazione prezzi portale", "Impossibile aprire il tunnel, probabilmente non è stato avviato!::"+sqlimport.sqlerrtext)
		return -1
	end if
	
catch (throwable t_error)
	messagebox("Esportazione prezzi portale", "Impossibile aprire il tunnel SSH, probabilmente non è stato avviato!")
	return -1
end try

return 0
end function

public function integer uof_update_prezzo_prodotto (string as_codice, double ad_prezzo, ref string as_error);// ** stefanop 07/10/2008
//		Funzione che aggiorna un prezzo di un determanto prodotto nel database remoto.
//		La funziona aggiorna solo il campo "prezzo_mb"

// 		Valori di ritorno:
//		-1		Errore SQL
//		0		Tutto OK
//		1		Prodotto non trovato

//update 	prodotto
//set		prezzo_mb = ad_prezzo
//where		codice = as_codice
//using		sqlimport;
//
//if sqlimport.sqlcode < 0 then
//	as_error = sqlimport.sqlerrtext
//	return -1
//elseif sqlimport.sqlcode = 100 then
//	return 1
//else
//	return 0
//end if

messagebox("debug", ad_prezzo)
return -1
end function

on uo_mrboxer.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mrboxer.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// *** stefanop 14/07/2008: Apro canale SSH con plink in maniera silenziosa

// creo l'oggetto shell, che mi permette di aprire e chiudere un processo.
io_shell = create uo_shellexecute

// Comando da eseguire.
// plink -batch dbuser@mrboxer.it -pw pwd12345 -L 5432:mrboxer.it:5432 -P 7822 -N

// Se l'exe di plink esiste controllare dal task manager l'avvio del processo.
io_shell.uof_createprocess("plink -batch dbuser@mrboxer.it -pw pwd12345 -L 5432:mrboxer.it:5432 -P 7822 -N")
end event

event destructor;io_shell.uof_terminateproccess( )
end event

