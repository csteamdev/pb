﻿$PBExportHeader$uo_string_replace.sru
forward
global type uo_string_replace from nonvisualobject
end type
end forward

global type uo_string_replace from nonvisualobject
end type
global uo_string_replace uo_string_replace

type variables
//Oggetto per Regular Expression
uo_regex iuo_regex

//Stringe per formattare Decimal e Date
String is_format_decimal, is_format_date
end variables

forward prototypes
public subroutine uof_setformat_decimal (string s_format)
public subroutine uof_setformat_date (string s_format)
public function string uof_replace (string s_testo, string s_query)
private function string uof_get_stringa_datastore (ref datastore ds_dati, string s_nomecolonna)
public function string uof_replace (string s_testo, string s_tabella, string s_where)
public function string uof_replace (string s_testo, string s_tabella, string s_where, string s_orderby)
end prototypes

public subroutine uof_setformat_decimal (string s_format);is_format_decimal=s_format
end subroutine

public subroutine uof_setformat_date (string s_format);is_format_date=s_format
end subroutine

public function string uof_replace (string s_testo, string s_query);/*
* Dato un testo in input con dei tag racchiusi tra parentesi graffe "{testo}", sostituisce i tag con dei campi provenienti da una query.
* La colonna deve avere lo stesso nome del tag.
* Se la SELECT restituisce più righe, la funzione usa sempre e solo la prima.
* Se la SELECT restituisce 0 risultati, oppure se non ci sono tag da sostituire, ritorna "" (stringa vuota)
*/

integer li_i, li_j
long ll_count
String ls_contenuto, ls_elemento[], ls_errore
boolean lb_trovato
datastore lds_dati

lds_dati=create datastore

ls_contenuto = sqlca.syntaxFromSQL(s_query, "", ls_errore)
if (ls_errore<>"") Then
	g_mb.messagebox("String replacing","Si è verificato un errore:~n" + ls_errore,stopsign!)
	destroy lds_dati
	return ""
end if

lds_dati.create(ls_contenuto, ls_errore)
if (ls_errore<>"") Then
	g_mb.messagebox("String replacing","Si è verificato un errore:~n" + ls_errore,stopsign!)
	destroy lds_dati
	return ""
end if

if (lds_dati.setTransObject(SQLCA) < 0) then
	g_mb.messagebox("String replacing","Errore in impostazione transazione.",stopsign!)
	destroy lds_dati
	return ""
end if

ll_count=lds_dati.Retrieve()

if (ll_count<0) then
	g_mb.messagebox("String replacing","Errore in lettura database." ,stopsign!)
	destroy lds_dati
	return ""
else
	//Se la query non da risultati, esco subito
	if(ll_count=0) Then
		return ""
	end if
end if

iuo_regex.initialize("{(.*?)}", true, true)

ll_count = iuo_regex.search(s_testo)

//Trovo tutti gli elementi, e li inserisco in un vettore (in maniera univoca).
//Si, fa schifo, è efficiente quanto un pacchetto di coriandoli in bagno, ma avremo al massimo 20 elementi, non mi pare il caso di implementare chissà che algoritmo sofisticato...
for li_i = 1 To ll_count
	ls_contenuto=iuo_regex.match(li_i)
	ls_contenuto=Mid(ls_contenuto, 2, (LastPos(ls_contenuto, "}") - 2))
	
	lb_trovato=false
	li_j=1
	
	Do While ((Not lb_trovato) And (li_j<=UpperBound(ls_elemento)))
		if(UPPER(ls_elemento[li_j])=UPPER(ls_contenuto)) Then
			lb_trovato=true
		end if
		li_j++
	Loop
	
	if(Not lb_trovato) Then
		ls_elemento[li_j]=ls_contenuto
	end if
Next

//Se non trovo neanche una voce da sostituire...
if(UpperBound(ls_elemento[])=0) Then
	return ""
end if

ls_contenuto=s_testo

for li_i=1 To UpperBound(ls_elemento[])
	iuo_regex.initialize("{("+ls_elemento[li_i]+")}", true, true)
	ls_contenuto=iuo_regex.replace(ls_contenuto, uof_get_stringa_DataStore(lds_dati, ls_elemento[li_i]))
Next

return ls_contenuto
end function

private function string uof_get_stringa_datastore (ref datastore ds_dati, string s_nomecolonna);/*
* In base al tipo di dato, uso il GetItem corretto e restituisco una stringa.
* Se non c'è il tipo di dato, vuol dire che non c'è la colonna, quindi restituisco il nome della colonna tra parentesi graffe "{nome}"
* Tipi di dati presi da: http://www.sqlines.com/sybase-asa-to-oracle/datatypes
*/

String ls_tipoDato

if((Pos(ds_dati.Describe(s_nomeColonna+".ColType"), "(") - 1)>0) Then
	ls_tipoDato=Left(ds_dati.Describe(s_nomeColonna+".ColType"), Pos(ds_dati.Describe(s_nomeColonna+".ColType"), "(") - 1)
else
	ls_tipoDato=ds_dati.Describe(s_nomeColonna+".ColType")
end if

choose case ls_tipoDato
	
	//GetItemDate
	case "date"
		return String(ds_dati.GetItemDate(1, s_nomeColonna), is_format_date)
		
	//GetItemDateTime
	case "datetime", "datetimeoffset", "smalldatetime", "timestamp"
		return String(ds_dati.GetItemDateTime(1, s_nomeColonna), is_format_date)
		
	//GetItemDecimal
	case "decimal", "double", "float", "money", "smallmoney"
		return String(ds_dati.GetItemDecimal(1, s_nomeColonna), is_format_decimal)
		
	//GetItemNumber
	case "bigint", "bit", "int", "long varbit", "numeric", "real", "smallint", "tintyint", "varbit"
		return String(ds_dati.GetItemNumber(1, s_nomeColonna))
		
	//GetItemString
	case "binary", "char", "long binary", "long nvarchar", "long varchar", "nchar", "nvarchar", "text", "uniqueidentifier", "uniqueidentifiersr", "varbinary", "varchar", "xml"
		return String(ds_dati.GetItemString(1, s_nomeColonna))
		
	//GetItemTime
	case "time"
		return String(ds_dati.GetItemTime(1, s_nomeColonna))
		
	//Se la colonna non c'è, non restituisce uno dei tipi di dato precedenti...
	case else
		return "{"+s_nomeColonna+"}"
		
end choose

end function

public function string uof_replace (string s_testo, string s_tabella, string s_where);//Sfrutta uof_replace(s_testo, s_query): per capire il risultati, fare riferimento ai commenti su quella funzione.
return uof_replace(s_testo, "SELECT * FROM "+s_tabella+" WHERE "+s_where)
end function

public function string uof_replace (string s_testo, string s_tabella, string s_where, string s_orderby);//Sfrutta uof_replace(s_testo, s_query): per capire il risultati, fare riferimento ai commenti su quella funzione.
return uof_replace(s_testo, "SELECT * FROM "+s_tabella+" WHERE "+s_where+" ORDER BY "+s_orderby)
end function

on uo_string_replace.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_string_replace.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_regex = create uo_regex

if Not (isvalid(iuo_regex)) Then
	g_mb.messagebox("String replacing","Impossibile inizializzare l'oggetto regex.~nContattare l'amministratore di sistema.", stopsign!)
end if
end event

