﻿$PBExportHeader$uo_upload.sru
forward
global type uo_upload from nonvisualobject
end type
end forward

global type uo_upload from nonvisualobject
end type
global uo_upload uo_upload

type variables
public constant string DATE_FORMAT = "yyyymmddhhmmss"
private:
	string is_endpoint_url
	string is_api_version = "/v1" 
	
	uo_xhr2 iuo_xhr
	
	str_token istr_token
	
	boolean ib_initialized = false
	
end variables

forward prototypes
public function uo_upload setendpointurl (string as_endpoint_url)
public function uo_upload set_apiversion (string as_api_version)
private function string baseurl ()
public function string gettoken ()
private subroutine parseandthrow () throws xhrexception
public function str_token token ()
public subroutine login (string as_username, string as_password, string as_token_type) throws exception
public subroutine login (string as_username, string as_password) throws exception
private function uo_xhr2 ensure_xhr ()
public subroutine download (string as_hash, string as_folder) throws exception
private subroutine isinitialized () throws xhrexception
public function string download (string as_hash, ref blob ab_file_data, ref str_download astr_file_info) throws exception
public subroutine upload (str_file_upload_request astr_file, ref str_file_upload_response astr_response) throws exception
public function integer uof_escape_value (str_metadata astr_metadato, ref string as_escaped_value)
end prototypes

public function uo_upload setendpointurl (string as_endpoint_url);is_endpoint_url = as_endpoint_url

return this
end function

public function uo_upload set_apiversion (string as_api_version);/**
 * stefanop
 * 24/12/2014
 *
 * Imposta la versione della API web da usare.
 **/
 
is_api_version = as_api_version
return this
end function

private function string baseurl ();return is_endpoint_url + is_api_version
end function

public function string gettoken ();return istr_token.token
end function

private subroutine parseandthrow () throws xhrexception;/**
 * stefanop
 * 27/12/2014
 *
 * Recupero il messaggio di errore dalla risposta del 
 * server.
 * Considero la risposta in formato JSON di default
 **/
 
oleobject luo_json
XhrException lxe_exception
 
lxe_exception = create XhrException

lxe_exception.setexceptionvalue(iuo_xhr)

if iuo_xhr.responseCode() = 12031 then
	lxe_exception.setMessage("Server non raggiungibile")
else
	iuo_xhr.responseJson(luo_json)
	lxe_exception.setMessage(luo_json.error.message)
end if

destroy luo_json
throw lxe_exception



end subroutine

public function str_token token ();return istr_token
end function

public subroutine login (string as_username, string as_password, string as_token_type) throws exception;/**
 *  stefanop
 * 23/12/2014
 *
 * Esegue la login al sistema remoto per ottenere il token di accesso
 **/

oleobject lole_json
ensure_xhr()

// Controllo se il token è già stato recuperato.
if not isnull(istr_token) then
	// Controllo che non sia già scaduto.
	if istr_token.expires > datetime(today(), now()) then
		// Posso usare il token
		return 
	end if
end if

iuo_xhr &
	.getJson(is_endpoint_url + "/auth/login") &
	.addParam("username", as_username) &
	.addParam("password", as_password)
	
if g_str.isnotempty(as_token_type) then
	iuo_xhr.addParam("token-type", as_token_type)
end if

try	
	// Invio richiesta
	if iuo_xhr.send() = 200 then
		iuo_xhr.responseJson(lole_json)
		istr_token.token = lole_json.token
		istr_token.lifetime = lole_json.lifetime
		istr_token.userId = lole_json.user_id
		istr_token.expires = datetime(lole_json.expires)
		
		ib_initialized = true
	else
		ib_initialized = false
		parseAndThrow()
	end if
catch(RuntimeError ex)
	ib_initialized = false
	JsonException ejson
	ejson = create JsonException
	ejson.setMessage(ex.getMessage())
	throw ejson
finally
	destroy lole_json
end try


end subroutine

public subroutine login (string as_username, string as_password) throws exception;/**
 *  stefanop
 * 23/12/2014
 *
 * Esegue la login al sistema remoto per ottenere il token di accesso
 **/
 
string ls_empty

setnull(ls_empty)
 
login(as_username, as_password, ls_empty)
end subroutine

private function uo_xhr2 ensure_xhr ();if not isvalid(iuo_xhr) or isnull(iuo_xhr) then
	
	iuo_xhr = create uo_xhr2
	
end if

return iuo_xhr
end function

public subroutine download (string as_hash, string as_folder) throws exception;/**
 * stefanop
 * 23/12/2014
 *
 * Esegue l'upload del documento e lo salva nel FileSystem
 **/

str_download lstr_file_info
blob lb_file_data

download(as_hash, ref lb_file_data, ref lstr_file_info)

if not g_str.end_with(as_folder, "\") then
	as_folder += "\"
end if

if g_str.isempty(lstr_file_info.filename) then
	lstr_file_info.filename = "temp" + string(datetime('yyyyMMddhhmmss'))
end if

guo_functions.uof_blob_to_file(lb_file_data, as_folder + lstr_file_info.filename)
end subroutine

private subroutine isinitialized () throws xhrexception;/**
 * stefanop
 * 05/01/2015
 *
 * Controlla che l'oggetto sia inizializzato tramite il metodo "login"
 * in caso contrario lancia un'eccezione
 **/
 
if not ib_initialized then
	XhrException e
	e = create XhrException
	e.setMessage("Oggetto non inizializzato; eseguire login prima di lanciare altri comandi.")
	throw e
end if
end subroutine

public function string download (string as_hash, ref blob ab_file_data, ref str_download astr_file_info) throws exception;/**
 * stefanop
 * 23/12/2014
 *
 * Esegue l'upload del documento
 **/

string ls_content_length, ls_content_disposition, ls_filename
int li_pos
ulong ul_content_length, ul_blob_length

isInitialized()

ensure_xhr() &
	.clear() &
	.authorizationToken(getToken()) &
	.get(baseUrl() + "/download/" + as_hash + "/content/attachment") &
	.send()
	
if iuo_xhr.responseCode() = 200 then
	
	ls_content_disposition = iuo_xhr.getResponseHeader("Content-Disposition")
	ul_content_length = long(iuo_xhr.getResponseHeader("Content-Length"))
	
	// TODO migliorare con un  regex
	li_pos = lastpos(ls_content_disposition, "filename=")
	if li_pos > 0 then
		ls_filename = mid(ls_content_disposition, li_pos + len("filename=") + 1)
		ls_filename = mid(ls_filename, 1, len(ls_filename) - 1)
	end if
	
	astr_file_info.filename = ls_filename
	astr_file_info.size = ul_content_length
	astr_file_info.mimetype = iuo_xhr.getResponseHeader("Content-Type")

	iuo_xhr.responseBody(ab_file_data)
	ul_blob_length = len(ab_file_data)
	
	if ul_blob_length <> ul_content_length then
		XhrException downloadEx 
		downloadEx = create XhrException
		downloadEx.setMessage("La dimensione del file non corrisponde; file nel server " + string(ul_content_length) + ", file scaricato " + string(ul_blob_length))
		throw downloadEx 
	end if
		
else
	parseAndThrow()
end if

return ""
end function

public subroutine upload (str_file_upload_request astr_file, ref str_file_upload_response astr_response) throws exception;/**
 * stefanop
 * 23/12/2014
 *
 * Esegue l'upload del documento
 **/

string ls_text, ls_sep, ls_filename
string ls_json, ls_content_url, ls_upload_url
string ls_file_data[], ls_error, ls_json_value_escaped
int li_id, li_i
uo_json luo_json
oleobject lole_json
blob lblob_file
str_metadata lstr_metadato

ls_filename = g_str.replace(astr_file.name, "\", "\\")
ls_json = '{"name":"' + ls_filename + '" '

if upperbound(astr_file.metadati) > 0 then
	
	ls_json += ', "metadata":['
	ls_sep = ""
	
	for li_i = 1 to upperbound(astr_file.metadati)
		lstr_metadato = astr_file.metadati[li_i]
		
		if g_str.isnotempty(lstr_metadato.meta_key) and g_str.isnotempty(string(lstr_metadato.meta_value)) then
			
			uof_escape_value(lstr_metadato, ls_json_value_escaped)
			
			ls_json += ls_sep
			
			ls_json += '{"key":"' + astr_file.metadati[li_i].meta_key + '", "value":'  + ls_json_value_escaped + '}' 			
			ls_sep = ", "
		end if
	next
	
	ls_json += "]"
end if

ls_json += "}"

iuo_xhr &
	.clear() &
	.authorizationToken(getToken()) &
	.postJson(baseUrl() + "/upload/prepare") &
	.send(ls_json)
	
iuo_xhr.responseJson(lole_json)

if iuo_xhr.responseCode() = 200 then
	
	li_id = lole_json.id
	ls_content_url = lole_json.formdata_url
	ls_upload_url = lole_json.stream_content_url
	
	guo_functions.uof_file_to_blob(astr_file.name, lblob_file)
	
	iuo_xhr &
		.clear() &
		.authorizationtoken(getToken()) &
		.post(is_endpoint_url +ls_upload_url) &
		.send(lblob_file)
		
	ls_text = iuo_xhr.responseText()
		
	iuo_xhr.responseJson(lole_json)
	
	astr_response.name = lole_json.name
	astr_response.extension = lole_json.extension
	astr_response.mimetype = lole_json.mimetype
	astr_response.hash = lole_json.hash
else
	parseAndThrow()
end if
end subroutine

public function integer uof_escape_value (str_metadata astr_metadato, ref string as_escaped_value);/**
 * stefanop
 * 03/09/
 *
 * Esegue l'escape dei campi prima di accodarli al JSON
 **/
 
string ls_test, ls_value

ls_test = ClassName(astr_metadato.meta_value)
 
choose case ClassName(astr_metadato.meta_value)

	case "string"
		ls_value = g_str.safe(astr_metadato.meta_value)
		
		if len(ls_value) > 0 then
			ls_value = g_str.replace(ls_value, "~r~n", "\n")
		end if
		
		as_escaped_value = '"' + ls_value + '"'
		
	case "integer", "decimal", "double"
		as_escaped_value = string(astr_metadato.meta_value)
		as_escaped_value = g_str.replace(as_escaped_value, ",", ".")
		
	case "datetime"
		as_escaped_value = '"' + string(astr_metadato.meta_value, DATE_FORMAT) + '"'
		
	case "boolean"
		if astr_metadato.meta_value then
			as_escaped_value = 'true'
		else
			as_escaped_value = 'false'
		end if
		
		
end choose
 
return 0
end function

on uo_upload.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_upload.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

