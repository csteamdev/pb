﻿$PBExportHeader$uo_upload_documenti.sru
forward
global type uo_upload_documenti from nonvisualobject
end type
end forward

global type uo_upload_documenti from nonvisualobject
end type
global uo_upload_documenti uo_upload_documenti

type variables
constant string METADATO_UTENTE_CARICAMENTO = "utenteCaricamento"
constant string METADATO_DATA_CARICAMENTO = "dataCaricamento"
constant string METADATO_TIPO_STRINGA = "S"

private:
	uo_upload iuo_upload
	
	boolean ib_ready
	string is_endpoint, is_username, is_password, is_token_type
	
	string is_error
end variables

forward prototypes
private function uo_upload_exception build_exception (string as_message)
public function integer uof_archivia (str_metadata astr_metadati[], string as_filepath, ref str_file_upload_response astr_response) throws uo_upload_exception
public function integer uof_archivio_fattura_vendita (long al_anno, long al_numero, string as_filepath) throws uo_upload_exception
private subroutine uof_inizializza_parametri ()
private subroutine uof_add_userinfo (ref str_metadata astr_metadati[])
public subroutine uof_add_fk_metadata (ref integer ai_position, ref str_metadata lstr_metadati[])
public function boolean is_enabled ()
public subroutine uof_salva_metadato (str_metadata astr_metadata)
end prototypes

private function uo_upload_exception build_exception (string as_message);/**
 * stefanop
 * 27/08/2015
 *
 * Crea l'eccezione da lancaire
 **/
 
uo_upload_exception luo_exception

luo_exception = create uo_upload_exception
luo_exception.setMessage(as_message)

return luo_exception
end function

public function integer uof_archivia (str_metadata astr_metadati[], string as_filepath, ref str_file_upload_response astr_response) throws uo_upload_exception;/**
 * stefanop
 * 27/08/2015
 *
 * Archivio il documento nel documentale
 **/

str_file_upload_request lstr_request

try 
	iuo_upload.login(is_username, is_password, is_token_type)
	
	uof_add_userinfo(astr_metadati)
	
	lstr_request.name = as_filepath
	lstr_request.metadati = astr_metadati
	
	iuo_upload.upload(lstr_request, ref astr_response)
catch (Exception e)
	throw build_exception(e.getMessage())
end try
 
return 1
end function

public function integer uof_archivio_fattura_vendita (long al_anno, long al_numero, string as_filepath) throws uo_upload_exception;/**
 * stefanop
 * 27/08/2015
 *
 * Archivio il documento nel documentale
 **/

any la_value
string ls_error, ls_col_type, ls_meta_tipo
str_file_upload_response lstr_response
str_metadata lstr_metadati[]
datastore lds_store
int li_column_count, li_count, li_i, li_meta_count

li_count = guo_functions.uof_crea_datastore(lds_store, "select * from tes_fat_ven where cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
				"and anno_registrazione=" + string(al_anno) + " and num_registrazione=" + string(al_numero), ls_error)
				
if li_count < 0 then
	throw build_exception("Errore durante la creazione del datastore " + g_str.safe(ls_error))
end if

// TODO spostare dentro un metodo.
li_column_count = integer(lds_store.Describe("DataWindow.Column.Count"))
li_meta_count = 0;

for li_i = 1 to li_column_count
	
	ls_col_type = left(lds_store.Describe("#" + string(li_i) + ".ColType"), 3)
	setnull(la_value)
	
	choose case ls_col_type
			
		case "cha", "str"
			la_value = lds_store.getitemstring(1, li_i)
			ls_meta_tipo = METADATO_TIPO_STRINGA
			
		case "int", "dec", "lon", "dou"
			la_value = lds_store.getitemnumber(1, li_i)
			
			if("dou" = ls_col_type or "dec" = ls_col_type) then
				ls_meta_tipo = "N"
			else
				ls_meta_tipo = "I"
			end if
			
			
		case "dat"
			la_value = lds_store.getitemdatetime(1, li_i)
			ls_meta_tipo = "D"
			
	end choose
	
	if not isnull(la_value) then
		li_meta_count++
		lstr_metadati[li_meta_count].meta_key = lds_store.Describe("#" + string(li_i) +".name")
		lstr_metadati[li_meta_count].meta_value = la_value
		lstr_metadati[li_meta_count].tipo = ls_meta_tipo
		
		uof_salva_metadato(lstr_metadati[li_meta_count])
		
		uof_add_fk_metadata(li_meta_count, lstr_metadati)
	end if
	
next

uof_archivia(lstr_metadati, as_filepath, lstr_response)

return 1
end function

private subroutine uof_inizializza_parametri ();/**
 * stefanop
 * 27/08/2015
 *
 * Recupero i parametri di collegamento al server
 **/
 
iuo_upload = create uo_upload

guo_functions.uof_get_parametro_azienda("UAD", is_endpoint)
guo_functions.uof_get_parametro_azienda("UND", is_username)
guo_functions.uof_get_parametro_azienda("PSD", is_password)

if g_str.isempty(is_endpoint) OR g_str.isempty(is_username) OR g_str.isempty(is_password) then
	ib_ready = false
	return
	//throw build_exception("Controllare i parametri UAD (endpoint), UND (username) e PSD (password) per il webservice Docs.")
end if

ib_ready = true
iuo_upload.setEndpointUrl(is_endpoint)

end subroutine

private subroutine uof_add_userinfo (ref str_metadata astr_metadati[]);/**
 * stefanop
 * 27/08/2015
 *
 * Controllo se nell'array dei metadati sono presenti le informazioni dell'utente e della data
 **/
 
string ls_nome_cognome
int li_i, li_count
boolean ib_utente, ib_data

ib_utente = false
ib_data = false

li_count = upperbound(astr_metadati)

for li_i = 1 to li_count
	
	if METADATO_UTENTE_CARICAMENTO = astr_metadati[li_i].meta_key then
		ib_utente = true
	end if
	
	if METADATO_DATA_CARICAMENTO = astr_metadati[li_i].meta_key then
		ib_data = true
	end if
next

if not ib_utente then
	li_count++
	
	select nome_cognome
	into :ls_nome_cognome
	from utenti 
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 or g_str.isempty(ls_nome_cognome) then
		ls_nome_cognome = s_cs_xx.cod_utente
	end if
	
	astr_metadati[li_count].meta_key = METADATO_UTENTE_CARICAMENTO
	astr_metadati[li_count].meta_value = ls_nome_cognome
end if

if not ib_data then
	li_count++
	astr_metadati[li_count].meta_key = METADATO_DATA_CARICAMENTO
	astr_metadati[li_count].meta_value = string(datetime(today(), now()), "yyyymmddhhmmss")
end if

end subroutine

public subroutine uof_add_fk_metadata (ref integer ai_position, ref str_metadata lstr_metadati[]);/**
 * stefanop
 * 02/09/2015
 *
 * Controlla la posizione corrente del metadata e carica le informazioni delle FK
 **/
 
string ls_meta_value, ls_query
datastore lds_store
int li_count

setnull(ls_query)
ls_meta_value = string(lstr_metadati[ai_position].meta_value);
 
choose case lstr_metadati[ai_position].meta_key
		
	case "cod_cliente"
		ls_query = "select rag_soc_1 as des_cliente from anag_clienti where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_cliente='" + ls_meta_value + "' "
		
	case "cod_fornitore"
		ls_query = "select rag_soc_1 as des_fornitore from anag_fornitori where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_fornitore='" + ls_meta_value + "' "
		
	case "cod_tipo_fat_ven"
		ls_query = "select des_tipo_fat_ven from tab_tipi_fat_ven where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_tipo_fat_ven='" + ls_meta_value + "' "
		
	case "cod_tipo_pagamento"
		ls_query = "select des_pagamento from tab_pagamenti where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_pagamento='" + ls_meta_value + "' "
		
	case "cod_deposito"
		ls_query = "select des_deposito from anag_depositi where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_deposito='" + ls_meta_value + "' "
		
	case "cod_pagamento"
		ls_query = "select des_pagamento from tab_pagamenti where cod_azienda='" + s_cs_xx.cod_azienda + "' and cod_pagamento='" + ls_meta_value + "' "
		
end choose

if g_str.isnotempty(ls_query) then
	li_count = guo_functions.uof_crea_datastore(lds_store, ls_query)
	
	if li_count > 0 then
		ai_position++
		lstr_metadati[ai_position].meta_key = lds_store.Describe("#1.name")
		lstr_metadati[ai_position].meta_value = lds_store.getitemstring(1, 1)
		lstr_metadati[ai_position].tipo = METADATO_TIPO_STRINGA
		
		uof_salva_metadato(lstr_metadati[ai_position])
	end if

end if


end subroutine

public function boolean is_enabled ();return ib_ready
end function

public subroutine uof_salva_metadato (str_metadata astr_metadata);/**
 * stefanop
 * 05/10/2015
 *
 * Inserisco il metadato all'interno della tabella tab_metadati se non è presente
 **/
 
int li_count

select count(*)
into :li_count
from tab_metadati
where cod_azienda = :s_cs_xx.cod_azienda and
		 nome = :astr_metadata.meta_key;
		 
if sqlca.sqlcode = 100 or li_count = 0 then
	
	insert into tab_metadati(nome, label, cod_azienda, tipo)
	values(:astr_metadata.meta_key, :astr_metadata.meta_key, :s_cS_xx.cod_azienda, :astr_metadata.tipo);
	
	if sqlca.sqlcode = 0 then 
		commit;
	end if
	
end if

 
 
 
end subroutine

on uo_upload_documenti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_upload_documenti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uof_inizializza_parametri()
end event

