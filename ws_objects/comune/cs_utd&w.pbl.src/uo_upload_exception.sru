﻿$PBExportHeader$uo_upload_exception.sru
forward
global type uo_upload_exception from exception
end type
end forward

global type uo_upload_exception from exception
end type
global uo_upload_exception uo_upload_exception

type variables
 
end variables

on uo_upload_exception.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_upload_exception.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

