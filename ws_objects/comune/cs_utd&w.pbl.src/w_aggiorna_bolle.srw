﻿$PBExportHeader$w_aggiorna_bolle.srw
$PBExportComments$Procedura di aggiornamento database bolle
forward
global type w_aggiorna_bolle from Window
end type
type cb_aggiornamento from commandbutton within w_aggiorna_bolle
end type
type st_1 from statictext within w_aggiorna_bolle
end type
end forward

global type w_aggiorna_bolle from Window
int X=1075
int Y=485
int Width=1797
int Height=485
boolean TitleBar=true
string Title="Aggiornamento Tabelle"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
cb_aggiornamento cb_aggiornamento
st_1 st_1
end type
global w_aggiorna_bolle w_aggiorna_bolle

on w_aggiorna_bolle.create
this.cb_aggiornamento=create cb_aggiornamento
this.st_1=create st_1
this.Control[]={ this.cb_aggiornamento,&
this.st_1}
end on

on w_aggiorna_bolle.destroy
destroy(this.cb_aggiornamento)
destroy(this.st_1)
end on

type cb_aggiornamento from commandbutton within w_aggiorna_bolle
int X=1281
int Y=261
int Width=435
int Height=101
int TabOrder=1
string Text="Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_azienda_tes,	ls_num_bolla_acq_tes, ls_cod_fornitore_tes, ls_cod_cliente_tes, ls_cod_tipo_bol_acq_tes, &
		 ls_cod_operatore_tes, ls_cod_fil_fornitore_tes, ls_cod_deposito_tes, ls_cod_ubicazione_tes, ls_cod_valuta_tes, &
		 ls_cod_tipo_listino_prodotto_tes, ls_cod_pagamento_tes, ls_cod_banca_clien_for_tes, &
		 ls_flag_movimenti_tes, ls_flag_riorganizzabile_tes, ls_cod_banca_tes, ls_flag_blocco_tes, ls_flag_gen_fat_tes
long ll_anno_bolla_acq_tes, ll_i, ll_y, ll_progressivo_tes
datetime ldt_data_registrazione_tes, ldt_data_bolla_tes
double ld_cambio_acq_tes, ld_totale_val_bolla_tes, ld_sconto_tes
		 
string ls_cod_azienda, ls_num_bolla_acq, ls_cod_tipo_det_acq, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, &
		 ls_cod_iva, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_tipo_movimento, &
		 ls_flag_saldo, ls_flag_accettazione, ls_flag_blocco, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_doc_suc_det, ls_flag_st_note_det
long ll_prog_riga_bolla_acq, ll_num_registrazione_mov_mag, ll_anno_bolla_acq, &
     ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine_acq, ll_anno_commessa, ll_num_commessa, &
	  ll_anno_registrazione_mov_mag, ll_anno_reg_bol_ven, ll_num_reg_bol_ven, ll_prog_riga_bol_ven, ll_anno_reg_des_mov, &
	  ll_prog_stock
double ld_quan_arrivata, ld_prezzo_acquisto, ld_fat_conversione, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_fatturata, &
		 ld_val_riga, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_num_reg_des_mov, ld_num_confezioni, ld_num_pezzi_confezione
datetime ldt_data_stock

long ll_anno_comodo
                     
ll_i = 1
ll_anno_comodo = 0

delete from det_bol_acq;
delete from tes_bol_acq;

declare cu_testata cursor for 
 select cod_azienda,
		  anno_bolla_acq,
		  num_bolla_acq,
		  progressivo,
		  cod_fornitore,
		  cod_cliente,
		  cod_tipo_bol_acq,
		  data_registrazione,
		  cod_operatore,
		  cod_fil_fornitore,
		  cod_deposito,
		  cod_ubicazione,
		  cod_valuta,
		  cambio_acq,
		  cod_tipo_listino_prodotto,
		  cod_pagamento,
		  cod_banca_clien_for,
		  totale_val_bolla,
		  flag_movimenti,
		  flag_riorganizzabile,
		  cod_banca,
		  data_bolla,
		  sconto,
		  flag_blocco,
		  flag_gen_fat
	from tes_bol_acq_old
  where cod_azienda = :s_cs_xx.cod_azienda
order by anno_bolla_acq, num_bolla_acq, progressivo;  

open cu_testata;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in apertura cursore testata. ~n~r" + sqlca.sqlerrtext)
	return
end if	

do while 1 = 1
	fetch cu_testata into:ls_cod_azienda_tes,
								  :ll_anno_bolla_acq_tes,
								  :ls_num_bolla_acq_tes,
								  :ll_progressivo_tes,
								  :ls_cod_fornitore_tes,
								  :ls_cod_cliente_tes,
								  :ls_cod_tipo_bol_acq_tes,
								  :ldt_data_registrazione_tes,
								  :ls_cod_operatore_tes,
								  :ls_cod_fil_fornitore_tes,
								  :ls_cod_deposito_tes,
								  :ls_cod_ubicazione_tes,
								  :ls_cod_valuta_tes,
								  :ld_cambio_acq_tes,
								  :ls_cod_tipo_listino_prodotto_tes,
								  :ls_cod_pagamento_tes,
								  :ls_cod_banca_clien_for_tes,
								  :ld_totale_val_bolla_tes,
								  :ls_flag_movimenti_tes,
								  :ls_flag_riorganizzabile_tes,
								  :ls_cod_banca_tes,
								  :ldt_data_bolla_tes,
								  :ld_sconto_tes,
								  :ls_flag_blocco_tes,
								  :ls_flag_gen_fat_tes;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da testata. ~n~r" + sqlca.sqlerrtext)
		close cu_testata;
		return
	end if	
	
	if sqlca.sqlcode = 100 then exit
	
	if ll_anno_comodo <> ll_anno_bolla_acq_tes then
		ll_i = 1
		ll_anno_comodo = ll_anno_bolla_acq_tes
	end if	
	
	insert into tes_bol_acq (cod_azienda,
									anno_bolla_acq,
									num_bolla_acq,
									cod_fornitore,
									cod_cliente,
									cod_tipo_bol_acq,
									data_registrazione,
									cod_operatore,
									cod_fil_fornitore,
									cod_deposito,
									cod_ubicazione,
									cod_valuta,
									cambio_acq,
									cod_tipo_listino_prodotto,
									cod_pagamento,
									cod_banca_clien_for,
									totale_val_bolla,
									flag_movimenti,
									flag_riorganizzabile,
									cod_banca,
									data_bolla,
									sconto,
									flag_blocco,
									flag_gen_fat,
									flag_doc_suc_tes,
									flag_doc_suc_pie,
									flag_st_note_tes,
									flag_st_note_pie,
									tot_merci,
									tot_spese_trasporto,
									tot_spese_imballo,
									tot_spese_bolli,
									tot_spese_varie,
									tot_spese_cassa,
									tot_sconti_commerciali,
									tot_val_bol_acq,
									tot_valuta_bol_acq,
									importo_iva,
									imponibile_iva,
									importo_valuta_iva,
									imponibile_valuta_iva)
						  values (:s_cs_xx.cod_azienda,
									 :ll_anno_bolla_acq_tes,
									 :ll_i,
									 :ls_cod_fornitore_tes,
									 :ls_cod_cliente_tes,
									 :ls_cod_tipo_bol_acq_tes,
									 :ldt_data_registrazione_tes,
									 :ls_cod_operatore_tes,
									 :ls_cod_fil_fornitore_tes,
									 :ls_cod_deposito_tes,
									 :ls_cod_ubicazione_tes,
									 :ls_cod_valuta_tes,
									 :ld_cambio_acq_tes,
									 :ls_cod_tipo_listino_prodotto_tes,
									 :ls_cod_pagamento_tes,
									 :ls_cod_banca_clien_for_tes,
									 :ld_totale_val_bolla_tes,
									 :ls_flag_movimenti_tes,
									 :ls_flag_riorganizzabile_tes,
								 	 :ls_cod_banca_tes,
								 	 :ldt_data_bolla_tes,
									 :ld_sconto_tes,
									 :ls_flag_blocco_tes,
									 :ls_flag_gen_fat_tes,
									 'I',
									 'I',
									 'I',
									 'I',
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0,
									 0);						  

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in inserimento dati in testata. ~n~r" + sqlca.sqlerrtext)
		close cu_testata;
		return
	end if		
	
	declare cu_dettaglio cursor for
	 select cod_azienda,
           anno_bolla_acq,
           num_bolla_acq,
			  prog_riga_bolla_acq,
			  cod_tipo_det_acq,
			  cod_misura,
			  cod_prodotto,
			  des_prodotto,
			  quan_arrivata,
			  prezzo_acquisto,
			  fat_conversione,
			  sconto_1,
			  sconto_2,
			  sconto_3,
			  cod_iva,
			  quan_fatturata,
			  val_riga,
			  cod_deposito,
			  cod_ubicazione,
			  cod_lotto,
			  data_stock,
			  cod_tipo_movimento,
			  num_registrazione_mov_mag,
		 	  flag_saldo,
		 	  flag_accettazione,
		 	  flag_blocco,
			  nota_dettaglio,
			  anno_registrazione,
			  num_registrazione,
			  prog_riga_ordine_acq,
			  anno_commessa,
		 	  num_commessa,
		 	  cod_centro_costo,
			  sconto_4,
			  sconto_5,
			  sconto_6,
			  sconto_7,
			  sconto_8,
			  sconto_9,
			  sconto_10,
			  anno_registrazione_mov_mag,
			  anno_reg_bol_ven,
			  num_reg_bol_ven,
			  prog_riga_bol_ven,
			  anno_reg_des_mov,
			  num_reg_des_mov,
			  num_confezioni,
			  num_pezzi_confezione,
			  flag_doc_suc_det,
		 	  flag_st_note_det,
		 	  progr_stock
		from det_bol_acq_old
	  where cod_azienda = :s_cs_xx.cod_azienda
	    and anno_bolla_acq = :ll_anno_bolla_acq_tes
		 and num_bolla_acq = :ls_num_bolla_acq_tes
		 and progressivo = :ll_progressivo_tes
  order by anno_bolla_acq, num_bolla_acq, progressivo;		 

	open cu_dettaglio;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in apertura cursore dettaglio. ~n~r" + sqlca.sqlerrtext)
		close cu_testata;
		return
	end if			
	
	do while 1 = 1
		fetch cu_dettaglio into :ls_cod_azienda,
										:ll_anno_bolla_acq,
										:ls_num_bolla_acq,
										:ll_prog_riga_bolla_acq,
										:ls_cod_tipo_det_acq,
										:ls_cod_misura,
										:ls_cod_prodotto,
										:ls_des_prodotto,
										:ld_quan_arrivata,
										:ld_prezzo_acquisto,
										:ld_fat_conversione,
										:ld_sconto_1,
										:ld_sconto_2,
										:ld_sconto_3,
										:ls_cod_iva,
										:ld_quan_fatturata,
										:ld_val_riga,
										:ls_cod_deposito,
										:ls_cod_ubicazione,
										:ls_cod_lotto,
										:ldt_data_stock,
										:ls_cod_tipo_movimento,
										:ll_num_registrazione_mov_mag,
										:ls_flag_saldo,
										:ls_flag_accettazione,
										:ls_flag_blocco,
										:ls_nota_dettaglio,
										:ll_anno_registrazione,
										:ll_num_registrazione,
										:ll_prog_riga_ordine_acq,
										:ll_anno_commessa,
										:ll_num_commessa,
										:ls_cod_centro_costo,
										:ld_sconto_4,
										:ld_sconto_5,
										:ld_sconto_6,
										:ld_sconto_7,
										:ld_sconto_8,
										:ld_sconto_9,
										:ld_sconto_10,
										:ll_anno_registrazione_mov_mag,
										:ll_anno_reg_bol_ven,
										:ll_num_reg_bol_ven,
										:ll_prog_riga_bol_ven,
										:ll_anno_reg_des_mov,
										:ld_num_reg_des_mov,
										:ld_num_confezioni,
										:ld_num_pezzi_confezione,
										:ls_flag_doc_suc_det,
										:ls_flag_st_note_det,
										:ll_prog_stock;		
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati da dettaglio. ~n~r" + sqlca.sqlerrtext)
			close cu_testata;
			close cu_dettaglio;
			return
		end if				
		
		if sqlca.sqlcode = 100 then exit
		
		insert into det_bol_acq (cod_azienda,
									  anno_bolla_acq,
									  num_bolla_acq,
									  prog_riga_bolla_acq,
									  cod_tipo_det_acq,
									  cod_misura,
									  cod_prodotto,
									  des_prodotto,
									  quan_arrivata,
									  prezzo_acquisto,
									  fat_conversione,
									  sconto_1,
									  sconto_2,
									  sconto_3,
									  cod_iva,
									  quan_fatturata,
									  val_riga,
									  cod_deposito,
									  cod_ubicazione,
									  cod_lotto,
									  data_stock,
									  cod_tipo_movimento,
									  num_registrazione_mov_mag,
									  flag_saldo,
									  flag_accettazione,
									  flag_blocco,
									  nota_dettaglio,
									  anno_registrazione,
									  num_registrazione,
									  prog_riga_ordine_acq,
									  anno_commessa,
									  num_commessa,
									  cod_centro_costo,
									  sconto_4,
									  sconto_5,
									  sconto_6,
									  sconto_7,
									  sconto_8,
									  sconto_9,
									  sconto_10,
									  anno_registrazione_mov_mag,
									  anno_reg_bol_ven,
									  num_reg_bol_ven,
									  prog_riga_bol_ven,
									  anno_reg_des_mov,
									  num_reg_des_mov,
									  num_confezioni,
									  num_pezzi_confezione,
									  flag_doc_suc_det,
									  flag_st_note_det,
									  prog_stock,
									  importo_conai,
									  importo_conai_scon)
							values (:ls_cod_azienda,
										:ll_anno_bolla_acq_tes,
										:ll_i,
										:ll_prog_riga_bolla_acq,
										:ls_cod_tipo_det_acq,
										:ls_cod_misura,
										:ls_cod_prodotto,
										:ls_des_prodotto,
										:ld_quan_arrivata,
										:ld_prezzo_acquisto,
										:ld_fat_conversione,
										:ld_sconto_1,
										:ld_sconto_2,
										:ld_sconto_3,
										:ls_cod_iva,
										:ld_quan_fatturata,
										:ld_val_riga,
										:ls_cod_deposito,
										:ls_cod_ubicazione,
										:ls_cod_lotto,
										:ldt_data_stock,
										:ls_cod_tipo_movimento,
										:ll_num_registrazione_mov_mag,
										:ls_flag_saldo,
										:ls_flag_accettazione,
										:ls_flag_blocco,
										:ls_nota_dettaglio,
										:ll_anno_registrazione,
										:ll_num_registrazione,
										:ll_prog_riga_ordine_acq,
										:ll_anno_commessa,
										:ll_num_commessa,
										:ls_cod_centro_costo,
										:ld_sconto_4,
										:ld_sconto_5,
										:ld_sconto_6,
										:ld_sconto_7,
										:ld_sconto_8,
										:ld_sconto_9,
										:ld_sconto_10,
										:ll_anno_registrazione_mov_mag,
										:ll_anno_reg_bol_ven,
										:ll_num_reg_bol_ven,
										:ll_prog_riga_bol_ven,
										:ll_anno_reg_des_mov,
										:ld_num_reg_des_mov,
										:ld_num_confezioni,
										:ld_num_pezzi_confezione,
										:ls_flag_doc_suc_det,
										:ls_flag_st_note_det,
										:ll_prog_stock,
										0,
										0);
										
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in inserimento dati in dettaglio. ~n~r" + sqlca.sqlerrtext)
			close cu_testata;
			close cu_dettaglio;
			return
		end if											

	loop 
	close cu_dettaglio;
	ll_i ++ 
loop
close cu_testata;

commit;

g_mb.messagebox("Apice", "Aggiornamento eseguito!")
















end event

type st_1 from statictext within w_aggiorna_bolle
int X=92
int Y=41
int Width=1578
int Height=181
boolean Enabled=false
string Text="Finestra di aggiornamento dati per le tabelle: TES_BOL_ACQ e DET_BOL_ACQ"
boolean FocusRectangle=false
long TextColor=255
long BackColor=79741120
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

