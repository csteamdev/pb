﻿$PBExportHeader$w_aggiorna_bolle_nbf.srw
$PBExportComments$Procedura di aggiornamento database bolle
forward
global type w_aggiorna_bolle_nbf from window
end type
type cb_aggiornamento from commandbutton within w_aggiorna_bolle_nbf
end type
type st_1 from statictext within w_aggiorna_bolle_nbf
end type
end forward

global type w_aggiorna_bolle_nbf from window
integer x = 1074
integer y = 484
integer width = 2341
integer height = 484
boolean titlebar = true
string title = "Aggiornamento Tabelle"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 79741120
cb_aggiornamento cb_aggiornamento
st_1 st_1
end type
global w_aggiorna_bolle_nbf w_aggiorna_bolle_nbf

on w_aggiorna_bolle_nbf.create
this.cb_aggiornamento=create cb_aggiornamento
this.st_1=create st_1
this.Control[]={this.cb_aggiornamento,&
this.st_1}
end on

on w_aggiorna_bolle_nbf.destroy
destroy(this.cb_aggiornamento)
destroy(this.st_1)
end on

type cb_aggiornamento from commandbutton within w_aggiorna_bolle_nbf
integer x = 1787
integer y = 256
integer width = 434
integer height = 100
integer taborder = 1
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;string ls_num_bolla_acq_tes
long ll_anno_bolla_acq_tes, ll_i, ll_progressivo_tes
double ld_cambio_acq_tes, ld_totale_val_bolla_tes, ld_sconto_tes

long ll_anno_comodo
                     
ll_i = 1
ll_anno_comodo = 0


declare cu_testata cursor for 
 select anno_bolla_acq,
		  num_bolla_acq,
		  progressivo
	from tes_bol_acq_old
  where cod_azienda = :s_cs_xx.cod_azienda
order by anno_bolla_acq, num_bolla_acq, progressivo;  

open cu_testata;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in apertura cursore testata. ~n~r" + sqlca.sqlerrtext)
	return
end if	

do while 1 = 1
	fetch cu_testata into :ll_anno_bolla_acq_tes,
								 :ls_num_bolla_acq_tes,
								 :ll_progressivo_tes;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da testata old. ~n~r" + sqlca.sqlerrtext)
		close cu_testata;
		return
	end if	
	
	if sqlca.sqlcode = 100 then exit
	
	if ll_anno_comodo <> ll_anno_bolla_acq_tes then
		ll_i = 1
		ll_anno_comodo = ll_anno_bolla_acq_tes
	end if	
	
	update tes_bol_acq
		set num_bolla_fornitore = :ls_num_bolla_acq_tes
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and anno_bolla_acq = :ll_anno_bolla_acq_tes
		and num_bolla_acq = :ll_i;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in aggiornamento dati in testata. ~n~r" + sqlca.sqlerrtext)
		close cu_testata;
		return
	end if		
	
	ll_i ++ 
loop
close cu_testata;

commit;

g_mb.messagebox("Apice", "Aggiornamento eseguito!")
















end event

type st_1 from statictext within w_aggiorna_bolle_nbf
integer x = 91
integer y = 40
integer width = 2126
integer height = 180
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "Finestra di aggiornamento dati per le tabelle: TES_BOL_ACQ"
boolean focusrectangle = false
end type

