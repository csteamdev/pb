﻿$PBExportHeader$w_aggiorna_costo_standard.srw
forward
global type w_aggiorna_costo_standard from w_std_principale
end type
type st_progres from statictext within w_aggiorna_costo_standard
end type
type cbx_nuovo_campo_vuoto from checkbox within w_aggiorna_costo_standard
end type
type cbx_aggiorna_solo_se_campo_dest_vuoto from checkbox within w_aggiorna_costo_standard
end type
type sle_file from singlelineedit within w_aggiorna_costo_standard
end type
type mle_log from multilineedit within w_aggiorna_costo_standard
end type
type cb_1 from commandbutton within w_aggiorna_costo_standard
end type
end forward

global type w_aggiorna_costo_standard from w_std_principale
integer width = 2597
integer height = 2540
st_progres st_progres
cbx_nuovo_campo_vuoto cbx_nuovo_campo_vuoto
cbx_aggiorna_solo_se_campo_dest_vuoto cbx_aggiorna_solo_se_campo_dest_vuoto
sle_file sle_file
mle_log mle_log
cb_1 cb_1
end type
global w_aggiorna_costo_standard w_aggiorna_costo_standard

on w_aggiorna_costo_standard.create
int iCurrent
call super::create
this.st_progres=create st_progres
this.cbx_nuovo_campo_vuoto=create cbx_nuovo_campo_vuoto
this.cbx_aggiorna_solo_se_campo_dest_vuoto=create cbx_aggiorna_solo_se_campo_dest_vuoto
this.sle_file=create sle_file
this.mle_log=create mle_log
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_progres
this.Control[iCurrent+2]=this.cbx_nuovo_campo_vuoto
this.Control[iCurrent+3]=this.cbx_aggiorna_solo_se_campo_dest_vuoto
this.Control[iCurrent+4]=this.sle_file
this.Control[iCurrent+5]=this.mle_log
this.Control[iCurrent+6]=this.cb_1
end on

on w_aggiorna_costo_standard.destroy
call super::destroy
destroy(this.st_progres)
destroy(this.cbx_nuovo_campo_vuoto)
destroy(this.cbx_aggiorna_solo_se_campo_dest_vuoto)
destroy(this.sle_file)
destroy(this.mle_log)
destroy(this.cb_1)
end on

type st_progres from statictext within w_aggiorna_costo_standard
integer x = 2126
integer y = 440
integer width = 389
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = " ----"
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_nuovo_campo_vuoto from checkbox within w_aggiorna_costo_standard
integer x = 114
integer y = 240
integer width = 1445
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Aggiorna anche se il nuovo valore è VUOTO O ZERO"
end type

type cbx_aggiorna_solo_se_campo_dest_vuoto from checkbox within w_aggiorna_costo_standard
integer x = 114
integer y = 140
integer width = 2021
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Aggiorna SOLO se il campo di destinazione VUOTO O A ZERO"
boolean checked = true
end type

type sle_file from singlelineedit within w_aggiorna_costo_standard
integer x = 27
integer y = 4
integer width = 2501
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_aggiorna_costo_standard
integer x = 23
integer y = 560
integer width = 2491
integer height = 1840
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_aggiorna_costo_standard
integer x = 928
integer y = 368
integer width = 667
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Aggiorna Anagrafiche"
end type

event clicked;string			ls_cod_prodotto,ls_costo, ls_str, ls_full_path, ls_docname
long			ll_file, ll_ret, ll_pos
integer		li_count, ll_
dec{4}		ld_costo_standard, ld_costo_standard_new
boolean		lb_inserisci


li_count = GetFileOpenName("Select File", ls_full_path, ls_docname, "text","Text,*.txt;*.csv", "C:")
	
if li_count<0 then
	g_mb.error("Errore in selezione file!")
	return
elseif li_count=0 then
	//annullato dall'operatore
	return
end if

sle_file.text = ls_full_path


ll_file = fileopen(sle_file.text)
if ll_file < 0 then
	g_mb.error("Errore in apertura del file indicato")
	return
end if

//formato file di testo
//COD_PRODOTTO<tabulatore>COD_NOMENCLATURA

mle_log.text += "~r~n~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"
mle_log.text += "INIZIO ELABORAZIONE"+string(now(),"hh:mm:ss")+"~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"

li_count = 0

do while true
	ll_ret = fileread(ll_file, ls_str)
	if ll_ret < 0 then exit
	
	ll_pos = pos(ls_str, "~t")
	
	ls_cod_prodotto = left(ls_str, ll_pos -1)
	
	if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
		mle_log.text += "~r~nRiga saltata in quanto la colonna prodotto è vuota"
		continue
	end if
	
	li_count ++
	
	st_progres.text = string(li_count)
	Yield()
	
	select 	costo_standard
	into 		:ld_costo_standard
	from 		anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in controllo esistenza cod prodotto "+ls_cod_prodotto+" : "+sqlca.sqlerrtext
		exit
	elseif sqlca.sqlcode = 100 then
		mle_log.text += "~r~nRiga saltata in quanto il prodotto "+ls_cod_prodotto+" non è presente in anagrafica"
		continue
	end if
	
	if cbx_aggiorna_solo_se_campo_dest_vuoto.checked then
		if ld_costo_standard > 0 and not isnull(ld_costo_standard) then continue
	end if
	
	if not cbx_nuovo_campo_vuoto.checked then
		if (ld_costo_standard_new = 0 or isnull(ld_costo_standard_new)) then continue
	end if
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	
	if ll_pos > 0 then
		ls_costo = left(ls_str, ll_pos -1)
	else
		if len(ls_str) > 0 then
			ls_costo = ls_str
		end if
	end if
	
	ld_costo_standard_new = dec(ls_costo)
	ld_costo_standard_new = round(ld_costo_standard_new, 4)
	
	update anag_prodotti
	set costo_standard = :ld_costo_standard_new
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in aggiornamento costo standard per il prodotto "+ls_cod_prodotto+": "+sqlca.sqlerrtext
		rollback;
		exit
	else
		
		mle_log.text += "~r~nProdotto "+ls_cod_prodotto+" aggiornato costo standard " + ls_costo
		commit;
		
	end if
loop

commit;

fileclose(ll_file)

g_mb.warning("Fine")

end event

