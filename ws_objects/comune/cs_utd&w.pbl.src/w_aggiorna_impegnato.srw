﻿$PBExportHeader$w_aggiorna_impegnato.srw
forward
global type w_aggiorna_impegnato from window
end type
type st_in_produzione from statictext within w_aggiorna_impegnato
end type
type st_assegnato from statictext within w_aggiorna_impegnato
end type
type st_in_spedizione from statictext within w_aggiorna_impegnato
end type
type st_ordinato from statictext within w_aggiorna_impegnato
end type
type st_impegnato from statictext within w_aggiorna_impegnato
end type
type hpb_1 from hprogressbar within w_aggiorna_impegnato
end type
type cb_progressivi_e_impegnato from commandbutton within w_aggiorna_impegnato
end type
type cb_ricalcola_impegnato_bnew from commandbutton within w_aggiorna_impegnato
end type
type st_file from statictext within w_aggiorna_impegnato
end type
type cb_ordinato from commandbutton within w_aggiorna_impegnato
end type
type cb_ricalcolaprogressivianagprodotti from commandbutton within w_aggiorna_impegnato
end type
type cb_quan_in_produzione from commandbutton within w_aggiorna_impegnato
end type
type cb_spedito from commandbutton within w_aggiorna_impegnato
end type
type cb_assegnato from commandbutton within w_aggiorna_impegnato
end type
type st_3 from statictext within w_aggiorna_impegnato
end type
type st_2 from statictext within w_aggiorna_impegnato
end type
type st_1 from statictext within w_aggiorna_impegnato
end type
type em_1 from editmask within w_aggiorna_impegnato
end type
end forward

global type w_aggiorna_impegnato from window
integer x = 1074
integer y = 480
integer width = 2633
integer height = 1768
boolean titlebar = true
string title = "Ricalcolo Progressivi"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
boolean center = true
event ue_key pbm_keydown
st_in_produzione st_in_produzione
st_assegnato st_assegnato
st_in_spedizione st_in_spedizione
st_ordinato st_ordinato
st_impegnato st_impegnato
hpb_1 hpb_1
cb_progressivi_e_impegnato cb_progressivi_e_impegnato
cb_ricalcola_impegnato_bnew cb_ricalcola_impegnato_bnew
st_file st_file
cb_ordinato cb_ordinato
cb_ricalcolaprogressivianagprodotti cb_ricalcolaprogressivianagprodotti
cb_quan_in_produzione cb_quan_in_produzione
cb_spedito cb_spedito
cb_assegnato cb_assegnato
st_3 st_3
st_2 st_2
st_1 st_1
em_1 em_1
end type
global w_aggiorna_impegnato w_aggiorna_impegnato

type variables
string		is_path_origine
boolean	ib_log_errori_su_file = false
end variables

forward prototypes
public function integer wf_aggiorna_spedito (string fs_cod_prodotto, ref string fs_errore)
public function integer wf_quan_in_produzione (string fs_cod_prodotto, ref string fs_errore)
public function integer wf_scrivi_log (string fs_testo, string fs_file)
public function integer wf_ricalcola_progressivi (datetime fdt_data_chius, ref string fs_error)
public function integer wf_aggiorna_impegnato_prodotto (string fs_cod_prodotto, ref string fs_error)
public function integer wf_ricalcola_progressivi_prodotto (string fs_cod_prodotto, ref string fs_error)
public function integer wf_aggiorna_impegnato_e_progressivi (ref string fs_error)
public function integer wf_aggiorna_impegnato_totale (ref string fs_error)
end prototypes

event ue_key;if key = KeyEscape! then
	if g_mb.messagebox("APICE", "Interrompo Elaborazione?", Question!, YesNo!, 2) = 2 then
		rollback;
		return
	end if
end if
	
end event

public function integer wf_aggiorna_spedito (string fs_cod_prodotto, ref string fs_errore);uo_prodotto_ricalcolo_progressivi luo_progressivi

luo_progressivi = create uo_prodotto_ricalcolo_progressivi

if luo_progressivi.uof_aggiorna_spedito(fs_cod_prodotto, fs_errore) = 0 then
	st_in_spedizione.text = fs_errore
else
	g_mb.messagebox("Apice", "Si è verificato un errore durante il ricalcolo: verificare i files di LOG")
end if

destroy luo_progressivi

return 0



//string					ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto,ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
//						ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_sql, ls_sql2, ls_str, ls_cod_misura_mag
//						
//long					ll_prog_stock, ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, &
//						ll_index, ll_tot
//
//dec{4}				ld_quan_consegnata
//
//datetime				ldt_data_stock
//
//datastore			lds_data
//
//
//ls_str = st_2.text
//
//
//ls_sql = "SELECT		a.cod_tipo_det_ven,"+&
//				"a.cod_prodotto,"+&
//				"a.cod_deposito,"+&
//				"a.cod_ubicazione,"+&
//				"a.cod_lotto,"+&
//				"a.data_stock,"+&
//				"a.progr_stock,"+&
//				"a.quan_consegnata,"+&
//				"a.anno_registrazione_mov_mag,"+&
//				"a.num_registrazione_mov_mag "+&
// "FROM  det_bol_ven as a "+&
//" JOIN tes_bol_ven as b ON 	b.cod_azienda=a.cod_azienda and "+&
// 									"b.anno_registrazione=a.anno_registrazione and "+&
//									 "b.num_registrazione=a.num_registrazione "+&
//"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
//			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and "+&
//			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
//			"a.cod_prodotto = '"+fs_cod_prodotto+"' "
//
//ls_sql2 = "SELECT	a.cod_tipo_det_ven,"+&
//			"a.cod_prodotto,"+&
//			"a.cod_deposito,"+&
//			"a.cod_ubicazione,"+&
//			"a.cod_lotto,"+&
//			"a.data_stock,"+&
//			"a.progr_stock,"+&
//			"a.quan_fatturata,"+&
//			"a.anno_registrazione_mov_mag,"+&
//			"a.num_registrazione_mov_mag "+&
// "FROM  det_fat_ven as a "+&
// "JOIN	tes_fat_ven as b ON		b.cod_azienda=a.cod_azienda and "+&
// 								"b.anno_registrazione=a.anno_registrazione and "+&
//								"b.num_registrazione=a.num_registrazione "+&
//"JOIN tab_tipi_fat_ven as t ON 	t.cod_azienda=a.cod_azienda and "+&
//										"t.cod_tipo_fat_ven=b.cod_tipo_fat_ven "+&
//"WHERE	a.cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
//			"b.flag_movimenti = 'N' and b.flag_blocco = 'N' and t.flag_tipo_fat_ven='I' and "+&
//			"(a.anno_registrazione_mov_mag is null or a.anno_registrazione_mov_mag<=0) and " +&
//			"a.cod_prodotto = '"+fs_cod_prodotto+"' "
//
//
////azzero la quantità in spedizione in anagrafica del prodotto
//update anag_prodotti
//set    quan_in_spedizione = 0 
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_prodotto = :fs_cod_prodotto;
//		 
//if sqlca.sqlcode <> 0 then
//	fs_errore = "Errore in azzeramento quantita in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
//	return -1
//end if
//
////azzero la quantità in spedizione negli stock del prodotto
//update stock
//set    quan_in_spedizione = 0 
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_prodotto like :fs_cod_prodotto;
//		 
//if sqlca.sqlcode <> 0 then
//	fs_errore = "Errore in azzeramento quantità in spedizione negli stock: " + sqlca.sqlerrtext
//	return -1
//end if
//
//
//
//st_2.text = ls_str + " : preparazione query righe ddt uscita non confermate in corso ..."
//
//ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)
//
//st_2.text = ls_str + " : elaborazione righe ddt uscita non confermate in corso ..."
//
//if ll_tot<0 then
//	fs_errore = "Errore in creazione datastore ddt: " + fs_errore
//	return -1
//elseif ll_tot > 0 then
//	for ll_index=1 to ll_tot
//		
//		ls_cod_tipo_det_ven					= lds_data.getitemstring(ll_index, 1)
//		ls_cod_prodotto						= lds_data.getitemstring(ll_index, 2)
//		ls_cod_deposito						= lds_data.getitemstring(ll_index, 3)
//		ls_cod_ubicazione						= lds_data.getitemstring(ll_index, 4)
//		ls_cod_lotto								= lds_data.getitemstring(ll_index, 5)
//		ldt_data_stock							= lds_data.getitemdatetime(ll_index, 6)
//		ll_prog_stock							= lds_data.getitemnumber(ll_index, 7)
//		ld_quan_consegnata					= lds_data.getitemdecimal(ll_index, 8)
//		//ll_anno_registrazione_mov_mag	= lds_data.getitemnumber(ll_index, 9)
//		//ll_num_registrazione_mov_mag	= lds_data.getitemnumber(ll_index, 10)
//		
//		
//		select flag_tipo_det_ven
//		into   :ls_flag_tipo_det_ven
//		from   tab_tipi_det_ven
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//				 
//		if sqlca.sqlcode < 0 then
//			fs_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
//			destroy lds_data
//			return -1
//		end if
//		
//		if ls_flag_tipo_det_ven = "M" then
//		
//			update anag_prodotti
//			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto ;
//					 
//			if sqlca.sqlcode = -1 then
//				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
//				destroy lds_data
//				return -1
//			end if
//			
//			update stock
//			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto and
//					 cod_deposito = :ls_cod_deposito and
//					 cod_ubicazione = :ls_cod_ubicazione and
//					 cod_lotto = :ls_cod_lotto and
//					 data_stock = :ldt_data_stock and
//					 prog_stock = :ll_prog_stock;
//					 
//			if sqlca.sqlcode = -1 then
//				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
//				destroy lds_data
//				return -1
//			end if
//		end if
//		
//		
//	next
//end if
//
//destroy lds_data
//
//st_2.text = ls_str + " : preparazione query righe fatture vendita non confermate in corso ..."
//
//ll_tot =guo_functions.uof_crea_datastore(lds_data, ls_sql2, fs_errore)
//
//st_2.text = ls_str + " : elaborazione righe fatture vendita non confermate in corso ..."
//
//if ll_tot<0 then
//	fs_errore = "Errore in creazione datastore fatture: " + fs_errore
//	return -1
//elseif ll_tot > 0 then
//	
//	for ll_index=1 to ll_tot
//		ls_cod_tipo_det_ven							= lds_data.getitemstring(ll_index, 1)
//		ls_cod_prodotto								= lds_data.getitemstring(ll_index, 2)
//		ls_cod_deposito								= lds_data.getitemstring(ll_index, 3)
//		ls_cod_ubicazione								= lds_data.getitemstring(ll_index, 4)
//		ls_cod_lotto										= lds_data.getitemstring(ll_index, 5)
//		ldt_data_stock									= lds_data.getitemdatetime(ll_index, 6)
//		ll_prog_stock									= lds_data.getitemnumber(ll_index, 7)
//		ld_quan_consegnata							= lds_data.getitemdecimal(ll_index, 8)
//		//ll_anno_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 9)
//		//ll_num_registrazione_mov_mag			= lds_data.getitemnumber(ll_index, 10)
//		
//		////riga ddt già movimentata?
//		//if not isnull(ll_anno_registrazione_mov_mag) or not isnull(ll_anno_registrazione_mov_mag) then continue
//	
//		
//		select flag_tipo_det_ven
//		into   :ls_flag_tipo_det_ven
//		from   tab_tipi_det_ven
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//				 
//		if sqlca.sqlcode < 0 then
//			fs_errore = "Errore in ricerca tipo dettaglio "+ls_cod_tipo_det_ven+": " + sqlca.sqlerrtext
//			destroy lds_data
//			return -1
//		end if
//		
//		if ls_flag_tipo_det_ven = "M" then
//			
//			update anag_prodotti
//			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto;
//					 
//			if sqlca.sqlcode < 0 then
//				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
//				destroy lds_data
//				return -1
//			end if
//			
//			update stock
//			set    quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto and
//					 cod_deposito = :ls_cod_deposito and
//					 cod_ubicazione = :ls_cod_ubicazione and
//					 cod_lotto = :ls_cod_lotto and
//					 data_stock = :ldt_data_stock and
//					 prog_stock = :ll_prog_stock;
//					 
//			if sqlca.sqlcode < 0 then
//				fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica stock: " + sqlca.sqlerrtext
//				destroy lds_data
//				return -1
//			end if
//		end if
//	next
//end if
//
//
//select quan_in_spedizione, cod_misura_mag
//into :ld_quan_consegnata, :ls_cod_misura_mag
//from anag_prodotti
//where 	cod_azienda=:s_cs_xx.cod_azienda and
//			cod_prodotto=:fs_cod_prodotto;
//			
//if isnull(ld_quan_consegnata) then ld_quan_consegnata = 0
//		
//st_in_spedizione.text = string(ld_quan_consegnata, "###,###,##0.00##") + " " + ls_cod_misura_mag + " " + fs_cod_prodotto
//
//destroy lds_data
//
//return 0
//
end function

public function integer wf_quan_in_produzione (string fs_cod_prodotto, ref string fs_errore);uo_prodotto_ricalcolo_progressivi luo_progressivi

luo_progressivi = create uo_prodotto_ricalcolo_progressivi

if luo_progressivi.uof_aggiorna_quan_in_produzione(fs_cod_prodotto, fs_errore) = 0 then
	st_in_produzione.text = fs_errore
else
	g_mb.messagebox("Apice", "Si è verificato un errore durante il ricalcolo: verificare i files di LOG")
end if

destroy luo_progressivi

return 0

//string						ls_cod_prodotto_raggruppato, ls_sql, ls_cod_misura_mag
//		 
//dec{4}					ld_quan_in_produzione, ld_quan_raggruppo
//
//datastore				lds_data
//
//long						ll_tot, ll_index
//
//
////
////commesse del prodotto non bloccate e in avanzamento
//ls_sql = 	"SELECT quan_in_produzione "+&
//			"FROM  anag_commesse "+&
//			"WHERE  cod_azienda = '"+s_cs_xx.cod_azienda+"' AND "+&
//						"flag_blocco = 'N' and "+&
//						"cod_prodotto = '"+fs_cod_prodotto+"' and "+&
//						"(flag_tipo_avanzamento='2' or flag_tipo_avanzamento='3') and "+&
//						"quan_in_produzione>0 "
//
//
//if isnull(fs_cod_prodotto) or len(fs_cod_prodotto) < 1 then fs_cod_prodotto = "%"
//
//update anag_prodotti
//set    quan_in_produzione = 0 
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_prodotto = :fs_cod_prodotto;
//		 
//if sqlca.sqlcode <> 0 then
//	fs_errore = "Errore in azzeramento quantita in produzione in anagrafica prodotti: " + sqlca.sqlerrtext
//	return -1
//end if
//
//// enme 08/1/2006 gestione prodotto raggruppato
//update anag_prodotti  
//	set quan_in_produzione = 0
// where cod_azienda = :s_cs_xx.cod_azienda and  
//		 cod_prodotto in 
//							(select cod_prodotto_raggruppato 
//							 from  anag_prodotti 
//							 where cod_azienda = :s_cs_xx.cod_azienda and
//							       cod_prodotto like :fs_cod_prodotto and 
//							       cod_prodotto_raggruppato is not null);
//if sqlca.sqlcode = -1 then
//	fs_errore = "Errore in azzeramento quantita in produzione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
//	return -1
//end if
//
//ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)
//if ll_tot < 0 then
//	fs_errore = "Errore in creazione datastpre commesse: " + fs_errore
//	return -1
//	
//elseif ll_tot > 0 then
//	
//	for ll_index=1 to ll_tot
//		yield()
//		ld_quan_in_produzione = lds_data.getitemdecimal(ll_index, 1)
//	
//		//#######################################################################
//		//													q_prodotta			q_in_prod		q_ordine
//		//#######################################################################
//		//Quando creo la commessa:						0						0					8
//		//Quando attivo											0						8					8
//		//Mentre produco										3						5					8
//		//....														6						2					8
//		//alla chiusura											8						0					8
//		//in ogni caso la quota parte per ogni commessa di prodotto in produzione è nella colonna quan_in_produzione
//		
//		
//		update anag_prodotti
//		set    quan_in_produzione = quan_in_produzione + :ld_quan_in_produzione
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_prodotto = :fs_cod_prodotto;
//				 
//		if sqlca.sqlcode < 0 then
//			fs_errore = "Errore in aggiornamento quantità in spedizione in anagrafica prodotti: " + sqlca.sqlerrtext
//			return -1
//		end if
//		
//		// enme 08/1/2006 gestione prodotto raggruppato
//		setnull(ls_cod_prodotto_raggruppato)
//	
//		select cod_prodotto_raggruppato
//		into   :ls_cod_prodotto_raggruppato
//		from   anag_prodotti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_prodotto = :fs_cod_prodotto;
//				 
//		if not isnull(ls_cod_prodotto_raggruppato) then
//		
//			ld_quan_raggruppo = f_converti_qta_raggruppo(fs_cod_prodotto, ld_quan_in_produzione, "M")
//		
//			update anag_prodotti  
//				set quan_in_produzione = quan_in_produzione + :ld_quan_raggruppo 
//			 where cod_azienda = :s_cs_xx.cod_azienda and  
//					 cod_prodotto = :ls_cod_prodotto_raggruppato;
//			if sqlca.sqlcode < 0 then
//				fs_errore = "Errore in aggiornamento quantità in spedizione del raggruppato in anagrafica prodotti: " + sqlca.sqlerrtext
//				return -1
//			end if
//		end if
//		
//		//commit su ogni prodotto
//		
//	next
//end if
//
//select quan_in_produzione, cod_misura_mag
//into :ld_quan_in_produzione, :ls_cod_misura_mag
//from anag_prodotti
//where 	cod_azienda=:s_cs_xx.cod_azienda and
//			cod_prodotto=:fs_cod_prodotto;
//
//if isnull(ld_quan_in_produzione) then ld_quan_in_produzione = 0
//
//st_in_produzione.text = string(ld_quan_in_produzione, "###,###,##0.00##") + " " + ls_cod_misura_mag
//
//
//return 0

end function

public function integer wf_scrivi_log (string fs_testo, string fs_file);integer li_file

li_file = FileOpen(fs_file,LineMode!, Write!, LockWrite!, Append!)
filewrite(li_file, fs_testo)
fileclose(li_file)

return 0
end function

public function integer wf_ricalcola_progressivi (datetime fdt_data_chius, ref string fs_error);
string				ls_filtro_prodotto, ls_file, ls_cod_prodotto, ls_prod_in_chius[], ls_chiave[], ls_vuoto[], ls_error

long				ll_tot, ll_index

dec{4}			ld_quant_val_attuale[], ld_vuoto[], ld_giacenza[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]

integer			li_ret


hpb_1.position = 0
ls_filtro_prodotto = em_1.text
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

ls_file = is_path_origine + "ricalcola_progressivi.log"
filedelete(ls_file)


declare cur_prod cursor for 
select distinct cod_prodotto
from  mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda AND  
		cod_prodotto like :ls_filtro_prodotto and
		data_registrazione <= :fdt_data_chius //and
		//data_registrazione > :ldt_data_chiusura_annuale_prec and
      //flag_storico = 'S'
order by cod_prodotto;

ll_index = 0

OPEN cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext
	rollback;
	return -1
end if

DO while true
	FETCH cur_prod INTO :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		fs_error="Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	//sle_1.text = string(ll_i) + "  " + ls_cod_prodotto
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_index += 1
		ls_prod_in_chius[ll_index] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
LOOP

CLOSE cur_prod ;

ll_tot = upperbound(ls_prod_in_chius[])

hpb_1.maxposition = ll_tot
hpb_1.position = 0

for ll_index = 1 to  ll_tot
	yield()
	// ****************************** Ricalcolo Progressivi Prodotto *************************************
	// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
	// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
	// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
	
	st_2.text = "Elaborazione progressivi: percentuale di avanzamento " + string(long((ll_index / ll_tot) * 100)) + " %"
	
	ld_quant_val_attuale = ld_vuoto
	ls_chiave = ls_vuoto
	ld_giacenza = ld_vuoto
	ld_costo_medio_stock = ld_vuoto
	ld_quan_costo_medio_stock = ld_vuoto
	ls_cod_prodotto = ls_prod_in_chius[ll_index]
	
	
	li_ret = wf_ricalcola_progressivi_prodotto(ls_cod_prodotto, ls_error)
	
	hpb_1.stepit()
	
	if li_ret<0 then
		//errore critico
		rollback;
		return -1
		
	elseif li_ret=1 then
		//errore, segnala ma continua
		rollback;
		continue
	else
		//tutto OK: commit già fatto
	end if
	
next


return 0
end function

public function integer wf_aggiorna_impegnato_prodotto (string fs_cod_prodotto, ref string fs_error);uo_prodotto_ricalcolo_progressivi luo_progressivi

luo_progressivi = create uo_prodotto_ricalcolo_progressivi

if luo_progressivi.uof_aggiorna_impegnato(fs_cod_prodotto, fs_error) = 0 then
	st_impegnato.text = fs_error
else
	g_mb.messagebox("Apice", "Si è verificato un errore durante il ricalcolo: verificare i files di LOG")
end if

destroy luo_progressivi

return 0
//uo_magazzino					luo_magazzino
//long								ll_i, ll_tot
//string								ls_file, ls_str, ls_cod_misura_mag
//dec{4}							ld_quan_impegnata_old, ld_quan_impegnata_new
//
//ls_file = is_path_origine + "aggiorna_impegnato.log"
//
//luo_magazzino = CREATE uo_magazzino
//
////	
////leggo l'impegnato prima
//select quan_impegnata
//into :ld_quan_impegnata_old
//from anag_prodotti
//where  cod_azienda  = :s_cs_xx.cod_azienda and
//		 cod_prodotto = :fs_cod_prodotto;
//			 
//if sqlca.sqlcode<0 then
//	fs_error = "Errore lettura q.tà impegnata (prima) per il prodotto "+fs_cod_prodotto
//	rollback;
//	return -1
//elseif sqlca.sqlcode=100 then
//	fs_error = "Prodotto "+fs_cod_prodotto+" inesistente in anagrafica!"
//	
//	if ib_log_errori_su_file then
//		wf_scrivi_log(fs_error, ls_file)
//		fs_error = ""
//		return 1
//	else
//		rollback;
//		g_mb.messagebox("Ricalcolo Impegnato", fs_error)
//		//esci dopo errore
//		return -1
//	end if
//	
//	fs_error = ""
//	return 1
//end if
//	
//if luo_magazzino.uof_ricalcola_impegnato( fs_cod_prodotto, fs_error) < 0 then
//	st_2.text = "Errore!"
//	destroy luo_magazzino
//	rollback;
//	
//	if ib_log_errori_su_file then
//		wf_scrivi_log(fs_error, ls_file)
//		return 1
//	else
//		g_mb.messagebox("Ricalcolo Impegnato", fs_error)
//		//esci dopo errore
//		return -1
//	end if
//end if
//	
////commit del singolo prodotto
//commit;
//	
////leggo l'impegnato dopo
//select quan_impegnata, cod_misura_mag
//into :ld_quan_impegnata_new, :ls_cod_misura_mag
//from anag_prodotti
//where  cod_azienda  = :s_cs_xx.cod_azienda and
//		 cod_prodotto = :fs_cod_prodotto;
//
//if isnull(ld_quan_impegnata_new) then ld_quan_impegnata_new = 0
//	
//ls_str = 	"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------~r~n" + &
//			string(today(),"dd/mm/yyyy") + " " + string(now(),"hh:mm:ss") + " " + "Aggiornamento Impegnato Prodotto: " + fs_cod_prodotto + "~r~n" + &
//			"quan_impegnata = " + string(ld_quan_impegnata_new, "###,###,##0.00")			+ "  -> (era "+string(ld_quan_impegnata_old, "###,###,##0.00")+")" 
//wf_scrivi_log(ls_str, ls_file)
//
//
//st_impegnato.text = string(ld_quan_impegnata_new, "###,###,##0.00##") + " " + ls_cod_misura_mag + " " + fs_cod_prodotto
//
//destroy luo_magazzino
//
//return 0
end function

public function integer wf_ricalcola_progressivi_prodotto (string fs_cod_prodotto, ref string fs_error);string					ls_file, ls_chiave[], ls_vuoto[], ls_where, ls_error, ls_str
dec{4}   				ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
						ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
						ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
						ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
						ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[], ld_costo_medio_stock[], &
						ld_quan_costo_medio_stock[],ld_saldo_quan_anno_prec_1, ld_vuoto[]
uo_magazzino		luo_magazzino
datetime				ldt_max_data_mov
integer				li_ret_funz

dec{4}				ld_saldo_quan_inizio_anno_old, ld_saldo_quan_ultima_chiusura_old, ld_prog_quan_entrata_old, ld_val_quan_entrata_old, &
						ld_prog_quan_uscita_old, ld_val_quan_uscita_old, ld_prog_quan_acquistata_old, ld_val_quan_acquistata_old, &
						ld_prog_quan_venduta_old, ld_val_quan_venduta_old


ls_file = is_path_origine + "ricalcola_progressivi.log"


	// ****************************** Ricalcolo Progressivi Prodotto *************************************
	// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
	// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
	// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
	
		
ld_quant_val_attuale = ld_vuoto
ls_chiave = ls_vuoto
ld_giacenza = ld_vuoto
ld_costo_medio_stock = ld_vuoto
ld_quan_costo_medio_stock = ld_vuoto
	
ldt_max_data_mov = datetime(today())

luo_magazzino = create uo_magazzino
li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal( fs_cod_prodotto, &
																			ldt_max_data_mov, &
																			ls_where, &
																			ref ld_quant_val_attuale[], &
																			ref ls_error, &
																			'N', &
																			ref ls_chiave[], &
																			ref ld_giacenza[], &
																			ref ld_costo_medio_stock[], &
																			ref ld_quan_costo_medio_stock[])

destroy luo_magazzino

if li_ret_funz < 0 then
	fs_error = "Errore nel calcolo della situazione attuale del prodotto " + fs_cod_prodotto + ", " + ls_error
	
	if ib_log_errori_su_file then
		wf_scrivi_log(fs_error, ls_file)
		fs_error = ""
		return 1
	else
		//in fs_error il messaggio
		return -1
	end if
	
	//return -1
end if

//leggo prima il pre-esistente
select		saldo_quan_inizio_anno,
			 saldo_quan_ultima_chiusura,
			 prog_quan_entrata,
			 val_quan_entrata,
			 prog_quan_uscita,
			 val_quan_uscita,
			 prog_quan_acquistata,
			 val_quan_acquistata,
			 prog_quan_venduta,
			 val_quan_venduta
into			:ld_saldo_quan_inizio_anno_old,
				:ld_saldo_quan_ultima_chiusura_old,
				:ld_prog_quan_entrata_old,
				:ld_val_quan_entrata_old,
				:ld_prog_quan_uscita_old,
				:ld_val_quan_uscita_old,
				:ld_prog_quan_acquistata_old,
				:ld_val_quan_acquistata_old,
				:ld_prog_quan_venduta_old,
				:ld_val_quan_venduta_old
from anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
			cod_prodotto = :fs_cod_prodotto;
			

if isnull(ld_saldo_quan_inizio_anno_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_saldo_quan_ultima_chiusura_old) then ld_saldo_quan_ultima_chiusura_old=0
if isnull(ld_prog_quan_entrata_old) then ld_prog_quan_entrata_old=0
if isnull(ld_val_quan_entrata_old) then ld_val_quan_entrata_old=0
if isnull(ld_prog_quan_uscita_old) then ld_prog_quan_uscita_old=0
if isnull(ld_val_quan_uscita_old) then ld_val_quan_uscita_old=0
if isnull(ld_prog_quan_acquistata_old) then ld_prog_quan_acquistata_old=0
if isnull(ld_val_quan_acquistata_old) then ld_val_quan_acquistata_old=0
if isnull(ld_prog_quan_venduta_old) then ld_saldo_quan_inizio_anno_old=0
if isnull(ld_val_quan_venduta_old) then ld_val_quan_venduta_old=0

ld_saldo_quan_inizio_anno = ld_quant_val_attuale[1]
ld_prog_quan_entrata = ld_quant_val_attuale[4]
ld_val_quan_entrata = ld_quant_val_attuale[5]
ld_prog_quan_uscita = ld_quant_val_attuale[6]
ld_val_quan_uscita = ld_quant_val_attuale[7]
ld_prog_quan_acquistata = ld_quant_val_attuale[8]
ld_val_quan_acquistata = ld_quant_val_attuale[9]
ld_prog_quan_venduta = ld_quant_val_attuale[10]
ld_val_quan_venduta = ld_quant_val_attuale[11]

update anag_prodotti 
set	 saldo_quan_inizio_anno = :ld_saldo_quan_inizio_anno,
		 saldo_quan_ultima_chiusura = :ld_saldo_quan_inizio_anno,
		 prog_quan_entrata = :ld_prog_quan_entrata,
		 val_quan_entrata = :ld_val_quan_entrata,
		 prog_quan_uscita = :ld_prog_quan_uscita,
		 val_quan_uscita = :ld_val_quan_uscita,
		 prog_quan_acquistata = :ld_prog_quan_acquistata,
		 val_quan_acquistata = :ld_val_quan_acquistata,
		 prog_quan_venduta = :ld_prog_quan_venduta,
		 val_quan_venduta = :ld_val_quan_venduta
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :fs_cod_prodotto;	

if sqlca.sqlcode <> 0 then
	fs_error = "Errore durante aggiornamento progressivi prodotto " + fs_cod_prodotto + ", " + sqlca.sqlerrtext
	
	if ib_log_errori_su_file then
		wf_scrivi_log(fs_error, ls_file)
		fs_error = ""
		rollback;
		return 1
	else
		//in fs_error il messaggio
		rollback;
		return -1
	end if
	//return -1
	
end if

//fai il commit ad ogni prodotto elaborato e scrivi nel log il risultato
commit;

//#######################################################################################

ls_str = 	"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------~r~n" + &
			string(today(),"dd/mm/yyyy") + " " + string(now(),"hh:mm:ss") + " " + "Aggiornamento Progressivi Prodotto: " + fs_cod_prodotto + "~r~n" + &
			"saldo_quan_inizio_anno = " + string(ld_saldo_quan_inizio_anno, "###,###,##0.00")			+ "  -> (era "+string(ld_saldo_quan_inizio_anno_old, "###,###,##0.00")+")" + "~r~n" + &
			"saldo_quan_ultima_chiusura =  " + string(ld_saldo_quan_inizio_anno,"###,###,##0.00")	+ "  -> (era "+string(ld_saldo_quan_ultima_chiusura_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_entrata = " + string(ld_prog_quan_entrata,"###,###,##0.00")						+ "  -> (era "+string(ld_prog_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_entrata = " + string(ld_val_quan_entrata,"###,###,##0.00")							+ "  -> (era "+string(ld_val_quan_entrata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_uscita = " + string(ld_prog_quan_uscita,"###,###,##0.00")							+ "  -> (era "+string(ld_prog_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_uscita = " + string(ld_val_quan_uscita,"###,###,##0.00")								+ "  -> (era "+string(ld_val_quan_uscita_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_acquistata = " + string(ld_prog_quan_acquistata,"###,###,##0.00")				+ "  -> (era "+string(ld_prog_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_acquistata = " + string(ld_val_quan_acquistata,"###,###,##0.00")					+ "  -> (era "+string(ld_val_quan_acquistata_old, "###,###,##0.00")+")" +  "~r~n" + &
			"prog_quan_venduta = " + string(ld_prog_quan_venduta,"###,###,##0.00")						+ "  -> (era "+string(ld_prog_quan_venduta_old, "###,###,##0.00")+")" +  "~r~n" + &
			"val_quan_venduta = " + string(ld_val_quan_venduta,"###,###,##0.00")							+  "  -> (era "+string(ld_val_quan_venduta_old, "###,###,##0.00")+")"
wf_scrivi_log(ls_str, ls_file)


return 0
end function

public function integer wf_aggiorna_impegnato_e_progressivi (ref string fs_error);string				ls_filtro_prodotto, ls_cod_prodotto, ls_prod_in_chius[], ls_file, ls_str, ls_file2
long				ll_i, ll_tot, ll_count
dec{4}			ld_quan_impegnata_old, ld_quan_impegnata_new
integer			li_ret

hpb_1.position = 0
ls_filtro_prodotto = em_1.text
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

ls_file = is_path_origine + "aggiorna_impegnato.log"
ls_file2 = is_path_origine + "ricalcola_progressivi.log"
filedelete(ls_file)
filedelete(ls_file2)

declare cur_prod cursor for 
select distinct cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_prodotto like :ls_filtro_prodotto
order by cod_prodotto;

ll_i = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	fetch cur_prod into :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		fs_error="Errore durante lettura elenco prodotti~r~n "+sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_i += 1
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;

ll_tot = upperbound(ls_prod_in_chius[])

hpb_1.maxposition = ll_tot
hpb_1.position = 0

st_impegnato.text = ""

for ll_i = 1 to ll_tot
	Yield()
	ls_cod_prodotto = ls_prod_in_chius[ll_i]
	
	//st_2.text = "Rielaborazione impegnato per il prodotto "+ls_cod_prodotto+" in corso ...    ("+string(ll_i)+" di "+string(ll_tot)+")"
	st_2.text = "Elaborazione impegnato: percentuale di avanzamento " + string(long((ll_i / ll_tot) * 100)) + " %"
	
	st_impegnato.text = ""
	
	li_ret = wf_aggiorna_impegnato_prodotto(ls_cod_prodotto, fs_error)
	
	if li_ret<0 then
		//errore critico
		rollback;
		return -1
	elseif li_ret=1 then
		//errore, ma continua
		rollback;
		
		hpb_1.stepit()
		
		continue
	else
		//ok, commit già fatto dalla funzione
	end if
	
	//per i progressivi elabora solo se il prodotto è movimentato
	select count(*)
	into :ll_count
	from mov_magazzino
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
	
	if ll_count=0 or isnull(ll_count) then
		//salta
		wf_scrivi_log("Prodotto "+ls_cod_prodotto+" saltato dal calcolo progressivi perchè non risulta movimentato!", ls_file2)
	else
		
		//st_2.text = "Rielaborazione progressivi per il prodotto "+ls_cod_prodotto+" in corso ...       ("+string(ll_i)+" di "+string(ll_tot)+")"
		st_2.text = "Elaborazione progressivi: percentuale di avanzamento " + string(long((ll_i / ll_tot) * 100)) + " %"
		
		li_ret = wf_ricalcola_progressivi_prodotto(ls_cod_prodotto, fs_error)
		if li_ret<0 then
			//errore critico
			rollback;
			return -1
			
		elseif li_ret=1 then
			//errore, segnala ma continua
			rollback;
			
			hpb_1.stepit()
			
			continue
		else
			//tutto OK: commit già fatto
		end if
		
	end if
	
	hpb_1.stepit()
	
next

return 0
end function

public function integer wf_aggiorna_impegnato_totale (ref string fs_error);string				ls_filtro_prodotto, ls_cod_prodotto, ls_prod_in_chius[], ls_file, ls_prodotto, ls_str
long				ll_i, ll_tot
dec{4}			ld_quan_impegnata_old, ld_quan_impegnata_new
integer			li_ret
uo_prodotto_ricalcolo_progressivi luo_progressivi


hpb_1.position = 0
ls_filtro_prodotto = em_1.text
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

ls_file = is_path_origine + "aggiorna_impegnato.log"
filedelete(ls_file)

declare cur_prod cursor for 
select distinct cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_prodotto like :ls_filtro_prodotto
order by cod_prodotto;

ll_i = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

do while true
	fetch cur_prod into :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		fs_error="Errore durante lettura elenco prodotti~r~n "+sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	//sle_1.text = string(ll_i) + "  " + ls_cod_prodotto
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_i += 1
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;

ll_tot = upperbound(ls_prod_in_chius[])

hpb_1.maxposition = ll_tot
hpb_1.position = 0

st_impegnato.text = ""

luo_progressivi = create uo_prodotto_ricalcolo_progressivi

for ll_i = 1 to ll_tot
	Yield()
	ls_prodotto = ls_prod_in_chius[ll_i]
	
	st_2.text = "Elaborazione impegnato: percentuale di avanzamento " + string(long((ll_i / ll_tot) * 100)) + " %"
	st_impegnato.text = ""

//	li_ret = wf_aggiorna_impegnato_prodotto(ls_prodotto, fs_error)
	li_ret = luo_progressivi.uof_aggiorna_impegnato(ls_cod_prodotto, fs_error) 

	choose case li_ret
		case 0 
		st_impegnato.text = fs_error
	case is > 0 
		//errore, ma continua
		rollback;
		continue
	case else
		// errore critico
		rollback;
		destroy luo_progressivi
		return -1
	end choose

	
	hpb_1.stepit()
	
next
destroy luo_progressivi
return 0
end function

on w_aggiorna_impegnato.create
this.st_in_produzione=create st_in_produzione
this.st_assegnato=create st_assegnato
this.st_in_spedizione=create st_in_spedizione
this.st_ordinato=create st_ordinato
this.st_impegnato=create st_impegnato
this.hpb_1=create hpb_1
this.cb_progressivi_e_impegnato=create cb_progressivi_e_impegnato
this.cb_ricalcola_impegnato_bnew=create cb_ricalcola_impegnato_bnew
this.st_file=create st_file
this.cb_ordinato=create cb_ordinato
this.cb_ricalcolaprogressivianagprodotti=create cb_ricalcolaprogressivianagprodotti
this.cb_quan_in_produzione=create cb_quan_in_produzione
this.cb_spedito=create cb_spedito
this.cb_assegnato=create cb_assegnato
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_1=create em_1
this.Control[]={this.st_in_produzione,&
this.st_assegnato,&
this.st_in_spedizione,&
this.st_ordinato,&
this.st_impegnato,&
this.hpb_1,&
this.cb_progressivi_e_impegnato,&
this.cb_ricalcola_impegnato_bnew,&
this.st_file,&
this.cb_ordinato,&
this.cb_ricalcolaprogressivianagprodotti,&
this.cb_quan_in_produzione,&
this.cb_spedito,&
this.cb_assegnato,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_1}
end on

on w_aggiorna_impegnato.destroy
destroy(this.st_in_produzione)
destroy(this.st_assegnato)
destroy(this.st_in_spedizione)
destroy(this.st_ordinato)
destroy(this.st_impegnato)
destroy(this.hpb_1)
destroy(this.cb_progressivi_e_impegnato)
destroy(this.cb_ricalcola_impegnato_bnew)
destroy(this.st_file)
destroy(this.cb_ordinato)
destroy(this.cb_ricalcolaprogressivianagprodotti)
destroy(this.cb_quan_in_produzione)
destroy(this.cb_spedito)
destroy(this.cb_assegnato)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_1)
end on

event open;string				ls_cod_prodotto


try
	ls_cod_prodotto = message.stringparm
	
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then em_1.text = ls_cod_prodotto
	
catch (runtimeerror err)
end try


is_path_origine = guo_functions.uof_get_user_documents_folder( )

st_file.text = is_path_origine
end event

type st_in_produzione from statictext within w_aggiorna_impegnato
integer x = 1051
integer y = 1068
integer width = 1495
integer height = 96
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_assegnato from statictext within w_aggiorna_impegnato
integer x = 1051
integer y = 952
integer width = 1495
integer height = 96
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_in_spedizione from statictext within w_aggiorna_impegnato
integer x = 1051
integer y = 836
integer width = 1495
integer height = 96
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_ordinato from statictext within w_aggiorna_impegnato
integer x = 1051
integer y = 720
integer width = 1495
integer height = 96
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_impegnato from statictext within w_aggiorna_impegnato
integer x = 1051
integer y = 584
integer width = 1495
integer height = 96
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_aggiorna_impegnato
integer x = 23
integer y = 1464
integer width = 2546
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_progressivi_e_impegnato from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 332
integer width = 960
integer height = 96
integer taborder = 31
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PROGRESSIVI E IMPEGNATO"
end type

event clicked;string				ls_error


if g_mb.confirm("Procedo con la rigenerazione impegnato e dei progressivi del/dei prodotti indicati?") then
	
	if wf_aggiorna_impegnato_e_progressivi(ls_error) < 0 then 
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.messagebox("Errore", ls_error)
	else
		//g_mb.messagebox("APICE", "Ricalcolo Impegnato/progressivi eseguito con successo!")
		st_2.text = "Ricalcolo Impegnato/progressivi eseguito con successo!"
	end if
end if

end event

type cb_ricalcola_impegnato_bnew from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 584
integer width = 960
integer height = 96
integer taborder = 21
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "IMPEGNATO"
end type

event clicked;string				ls_error


if g_mb.confirm("Procedo con la rigenerazione impegnato del/dei prodotti indicati?") then
	
	if wf_aggiorna_impegnato_totale(ls_error) < 0 then
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.messagebox("Errore", ls_error)
	else
		//g_mb.messagebox("APICE", "Ricalcolo Impegnato eseguito con successo!")
		st_2.text = "Ricalcolo Impegnato eseguito con successo!"
	end if
end if

end event

type st_file from statictext within w_aggiorna_impegnato
integer x = 64
integer y = 1320
integer width = 2519
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_ordinato from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 720
integer width = 960
integer height = 96
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ORDINATO FORNITORE"
end type

event clicked;string					ls_prodotto, ls_null, ls_str, ls_cod_prodotto, ls_errore, ls_filtro_prodotto, ls_prod_in_chius[], ls_cod_misura_mag
uo_magazzino		luo_magazzino
long					ll_index, ll_tot
dec{4}				ld_qta


hpb_1.position = 0

if not g_mb.confirm("L'operazione ricalcolerà le quantità ordinate a fornitore del/dei Prodotto selezionato. Continuare?") then return

setnull(ls_null)
ls_filtro_prodotto = em_1.text

if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

declare cur_prod cursor for
select distinct cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto like :ls_filtro_prodotto
order by cod_prodotto;

ll_index = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	ls_errore = "Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext
	st_2.text = "Errore!"
	hpb_1.position = 0
	g_mb.error(ls_errore)
	
	rollback;
	return -1
end if

do while true
	fetch cur_prod into :ls_prodotto;
	
	if sqlca.sqlcode < 0 then
		ls_errore="Errore durante lettura elenco prodotti~r~n "+sqlca.sqlerrtext
		close cur_prod;
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.error(ls_errore)
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	if not isnull(ls_prodotto) and ls_prodotto<>"" then
		ll_index += 1
		ls_prod_in_chius[ll_index] = ls_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;


ll_tot =upperbound(ls_prod_in_chius[])

hpb_1.maxposition = ll_tot
hpb_1.position = 0

luo_magazzino = CREATE uo_magazzino

st_ordinato.text = ""

for ll_index=1 to ll_tot
	Yield()
	
	st_2.text = "Elaborazione ordinato fornitore: percentuale di avanzamento " + string(long((ll_index / ll_tot) * 100)) + " %"
	
	ls_prodotto = ls_prod_in_chius[ll_index]
	
	st_ordinato.text = ""
	
	if luo_magazzino.uof_ricalcola_ordinato_fornitore( ls_prodotto, ref ls_errore) < 0 then
		rollback;
		st_2.text = "Errore!"
		hpb_1.position = 0
		
		g_mb.error(ls_errore)
		destroy luo_magazzino
		return
		
	else
		//commit per ogni prodotto
		commit;
		
		select quan_ordinata, cod_misura_mag
		into :ld_qta, :ls_cod_misura_mag
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_prodotto;
		
		if isnull(ld_qta) then ld_qta = 0
		
		st_ordinato.text = string(ld_qta, "###,###,##0.00##") + " " + ls_cod_misura_mag + " " + ls_prodotto
		
	end if
	
	hpb_1.stepit()
	
next

destroy luo_magazzino

st_2.text = "Elaborazione ordinato fornitore terminata!"


end event

type cb_ricalcolaprogressivianagprodotti from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 468
integer width = 960
integer height = 96
integer taborder = 21
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PROGRESSIVI"
end type

event clicked;string ls_error
datetime ldt_oggi

ldt_oggi = datetime(today())

if g_mb.confirm("L'operazione ricalcolerà i progressivi nell'anagrafica del/dei Prodotto selezionato. Continuare?") then
	if wf_ricalcola_progressivi(ldt_oggi, ls_error) < 0 then
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.messagebox("Errore", ls_error)
		
	else
		st_2.text = "Ricalcolo Progressivi eseguito con successo!"
	end if
end if


end event

type cb_quan_in_produzione from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 1068
integer width = 960
integer height = 96
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "IN PRODUZIONE"
end type

event clicked;string					ls_prodotto, ls_null, ls_str, ls_cod_prodotto, ls_errore, ls_cod_prodotto_filtro, ls_prod_in_chius[]
long					ll_index, ll_tot
integer				li_ret


hpb_1.position = 0
setnull(ls_null)
ls_cod_prodotto_filtro = em_1.text

if not g_mb.confirm("L'operazione ricalcolerà le quantità in produzione del/dei Prodotto selezionato. Continuare?") then return

if isnull(ls_cod_prodotto_filtro) or len(ls_cod_prodotto_filtro) < 1 then ls_cod_prodotto_filtro = "%"


declare cur_prod cursor for 
select cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_prodotto like :ls_cod_prodotto_filtro
order by cod_prodotto;

ll_index = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	ls_errore = "Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext
	st_2.text = "Errore!"
	hpb_1.position = 0
	
	g_mb.error(ls_errore)
	return -1
end if

do while true
	fetch cur_prod into :ls_cod_prodotto;
	
	yield()
	
	if sqlca.sqlcode < 0 then
		ls_errore="Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext
		
		st_2.text = "Errore!"
		hpb_1.position = 0
		
		rollback;
		g_mb.error(ls_errore)
		
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_index += 1
		ls_prod_in_chius[ll_index] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;

ll_tot = upperbound(ls_prod_in_chius[])
hpb_1.position = 0
hpb_1.maxposition = ll_tot

st_in_produzione.text = ""

for ll_index=1 to ll_tot
	Yield()
	ls_prodotto = ls_prod_in_chius[ll_index]
	
	st_2.text = string(long((ll_index / ll_tot) * 100)) + " % "
	
	st_in_produzione.text = ""
	li_ret = wf_quan_in_produzione(ls_prodotto, ls_errore)
	
	hpb_1.stepit()
	
	if li_ret < 0 then
		rollback;
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.error(ls_errore)
		return
		
	else
		//su ogni articolo elaborato
		commit;
	end if
	
next

st_2.text = "Ricalcolo quantità in produzione eseguito con successo!"


















/*


string ls_prodotto, ls_null, ls_str, ls_cod_prodotto, ls_errore

setnull(ls_null)
ls_prodotto = em_1.text

if len(ls_prodotto) < 1 then setnull(ls_prodotto)

if isnull(ls_prodotto) or len(trim(ls_prodotto)) < 1 then
	
	if g_mb.messagebox("APICE","Riscostruisco Quantità in produzione dell'intero magazzino?", Question!, YesNo!, 2) = 2 then return
	
	st_2.text = "Attendere la fine elaborazione..."
	
	if wf_quan_in_produzione(ls_prodotto, ref ls_errore) < 0 then
		g_mb.messagebox("Quantità in Produzione", ls_errore)
		rollback;
		return
	end if
	
else
	
	if g_mb.messagebox("APICE","Procedo con la rigenerazione Quantità in Produzione dei prodotti indicati?", Question!, YesNo!, 2) = 2 then return
	
	declare cu_prodotti cursor for	
	select cod_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto like :ls_prodotto;
			 
	open cu_prodotti;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore prodotti:" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return -1
	end if
	
	do while true
		
		yield()
		
		fetch cu_prodotti into :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox( "APICE", "Errore durante il caricamento del cursore prodotti:" + sqlca.sqlerrtext, stopsign!)
			close cu_prodotti;
			rollback;
			return -1
		end if		
		
		if sqlca.sqlcode = 100 then exit
		
		st_2.text = ls_cod_prodotto
		
		if wf_quan_in_produzione(ls_prodotto, ref ls_errore) < 0 then
			g_mb.messagebox("Quantità in Produzione", ls_errore)
			rollback;
			return
		end if
		
	loop
	
	close cu_prodotti;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "APICE", "Errore durante la chiusura del cursore prodotti:" + sqlca.sqlerrtext, stopsign!)
		rollback;
		return -1
	end if		

end if
		
commit;		

st_2.text = ""

g_mb.messagebox("APICE", "Ricalcolo Quantità in Produzione con Successo.")

*/

end event

type cb_spedito from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 836
integer width = 960
integer height = 96
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "IN SPEDIZIONE"
end type

event clicked;string					ls_prodotto, ls_null, ls_str, ls_cod_prodotto, ls_errore, ls_cod_prodotto_filtro, ls_prod_in_chius[]
long					ll_index, ll_tot
integer				li_ret


hpb_1.position = 0
setnull(ls_null)
ls_cod_prodotto_filtro = em_1.text

if not g_mb.confirm("L'operazione ricalcolerà le quantità Spedizione del/dei Prodotto selezionato. Continuare?") then return

if isnull(ls_cod_prodotto_filtro) or len(ls_cod_prodotto_filtro) < 1 then ls_cod_prodotto_filtro = "%"


declare cur_prod cursor for 
select cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_prodotto like :ls_cod_prodotto_filtro
order by cod_prodotto;

ll_index = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	ls_errore = "Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext
	st_2.text = "Errore!"
	hpb_1.position = 0
	
	g_mb.error(ls_errore)
	return -1
end if

do while true
	fetch cur_prod into :ls_cod_prodotto;
	
	yield()
	
	if sqlca.sqlcode < 0 then
		ls_errore="Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext
		
		st_2.text = "Errore!"
		hpb_1.position = 0
		
		rollback;
		g_mb.error(ls_errore)
		
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_index += 1
		ls_prod_in_chius[ll_index] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;

ll_tot = upperbound(ls_prod_in_chius[])
hpb_1.position = 0
hpb_1.maxposition = ll_tot

st_in_spedizione.text = ""

for ll_index=1 to ll_tot
	Yield()
	ls_prodotto = ls_prod_in_chius[ll_index]
	
	st_2.text = string(long((ll_index / ll_tot) * 100)) + " % "
	
	st_in_spedizione.text = ""
	li_ret = wf_aggiorna_spedito(ls_prodotto, ls_errore)
	
	hpb_1.stepit()
	
	if li_ret < 0 then
		rollback;
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.error(ls_errore)
		return
		
	else
		//su ogni articolo elaborato
		commit;
	end if
	
next

st_2.text = "Ricalcolo quantità in Spedizione eseguito con successo!"


end event

type cb_assegnato from commandbutton within w_aggiorna_impegnato
integer x = 73
integer y = 952
integer width = 960
integer height = 96
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ASSEGNATO (IN EVASIONE)"
end type

event clicked;string					ls_prodotto, ls_null, ls_str, ls_cod_prodotto, ls_errore, ls_cod_prodotto_filtro, ls_prod_in_chius[]
long					ll_index, ll_tot
integer				li_ret

uo_prodotto_ricalcolo_progressivi luo_progressivi


hpb_1.position = 0
setnull(ls_null)
ls_cod_prodotto_filtro = em_1.text

if not g_mb.confirm("L'operazione ricalcolerà le quantità assegnate (in evasione) del/dei Prodotto selezionato. Continuare?") then return

if isnull(ls_cod_prodotto_filtro) or len(ls_cod_prodotto_filtro) < 1 then ls_cod_prodotto_filtro = "%"


declare cur_prod cursor for 
select cod_prodotto
from  anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_prodotto like :ls_cod_prodotto_filtro
order by cod_prodotto;

ll_index = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	ls_errore = "Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext
	st_2.text = "Errore!"
	hpb_1.position = 0
	
	g_mb.error(ls_errore)
	return -1
end if


do while true
	fetch cur_prod into :ls_cod_prodotto;
	
	yield()
	
	if sqlca.sqlcode < 0 then
		ls_errore="Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext
		
		st_2.text = "Errore!"
		hpb_1.position = 0
		
		rollback;
		g_mb.error(ls_errore)
		
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	
	
	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_index += 1
		ls_prod_in_chius[ll_index] = ls_cod_prodotto
		st_2.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array ..."
	end if
	
loop

close cur_prod;

ll_tot = upperbound(ls_prod_in_chius[])
hpb_1.position = 0
hpb_1.maxposition = ll_tot

st_assegnato.text = ""

luo_progressivi = create uo_prodotto_ricalcolo_progressivi


for ll_index=1 to ll_tot
	Yield()
	ls_prodotto = ls_prod_in_chius[ll_index]
	
	st_2.text = string(long((ll_index / ll_tot) * 100)) + " % "
	
	st_assegnato.text = ""
	luo_progressivi.uof_aggiorna_assegnato(ls_prodotto, ls_errore)
//	li_ret = wf_aggiorna_assegnato(ls_prodotto, ls_errore)
	
	hpb_1.stepit()
	
	if li_ret < 0 then
		rollback;
		st_2.text = "Errore!"
		hpb_1.position = 0
		g_mb.error(ls_errore)
		return
		
	else
		//su ogni articolo elaborato
		commit;
	end if
	
next
destroy luo_progressivi

st_2.text = "Ricalcolo quantità assegnate (in evasione) eseguito con successo!"


end event

type st_3 from statictext within w_aggiorna_impegnato
integer x = 23
integer y = 12
integer width = 2546
integer height = 92
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "RICALCOLO PROGRESSIVI ANAGRAFICA PRODOTTI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_aggiorna_impegnato
integer x = 119
integer y = 1560
integer width = 2450
integer height = 84
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_aggiorna_impegnato
integer x = 73
integer y = 144
integer width = 443
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Codice Prodotto:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_aggiorna_impegnato
integer x = 571
integer y = 132
integer width = 754
integer height = 80
integer taborder = 2
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
maskdatatype maskdatatype = stringmask!
string displaydata = "Ä"
end type

