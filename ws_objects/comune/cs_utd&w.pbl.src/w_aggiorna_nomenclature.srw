﻿$PBExportHeader$w_aggiorna_nomenclature.srw
forward
global type w_aggiorna_nomenclature from w_std_principale
end type
type sle_file from singlelineedit within w_aggiorna_nomenclature
end type
type mle_log from multilineedit within w_aggiorna_nomenclature
end type
type cb_1 from commandbutton within w_aggiorna_nomenclature
end type
end forward

global type w_aggiorna_nomenclature from w_std_principale
integer width = 2597
integer height = 2540
sle_file sle_file
mle_log mle_log
cb_1 cb_1
end type
global w_aggiorna_nomenclature w_aggiorna_nomenclature

on w_aggiorna_nomenclature.create
int iCurrent
call super::create
this.sle_file=create sle_file
this.mle_log=create mle_log
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_file
this.Control[iCurrent+2]=this.mle_log
this.Control[iCurrent+3]=this.cb_1
end on

on w_aggiorna_nomenclature.destroy
call super::destroy
destroy(this.sle_file)
destroy(this.mle_log)
destroy(this.cb_1)
end on

type sle_file from singlelineedit within w_aggiorna_nomenclature
integer x = 27
integer y = 44
integer width = 2501
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_aggiorna_nomenclature
integer x = 37
integer y = 400
integer width = 2491
integer height = 2012
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_aggiorna_nomenclature
integer x = 928
integer y = 248
integer width = 667
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Aggiorna Nomenclature"
end type

event clicked;string			ls_cod_prodotto,ls_cod_nomenclatura, ls_str, ls_full_path, ls_docname
long			ll_file, ll_ret, ll_pos
integer		li_count
boolean		lb_inserisci


li_count = GetFileOpenName("Select File", ls_full_path, ls_docname, "text","Text,*.txt;*.csv", "C:")
	
if li_count<0 then
	g_mb.error("Errore in selezione file!")
	return
elseif li_count=0 then
	//annullato dall'operatore
	return
end if

sle_file.text = ls_full_path


ll_file = fileopen(sle_file.text)
if ll_file < 0 then
	g_mb.error("Errore in apertura del file indicato")
	return
end if

//formato file di testo
//COD_PRODOTTO<tabulatore>COD_NOMENCLATURA

mle_log.text += "~r~n~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"
mle_log.text += "INIZIO ELABORAZIONE"+string(now(),"hh:mm:ss")+"~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"

do while true
	ll_ret = fileread(ll_file, ls_str)
	if ll_ret < 0 then exit
	
	ll_pos = pos(ls_str, "~t")
	
	ls_cod_prodotto = left(ls_str, ll_pos -1)
	
	if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
		mle_log.text += "~r~nRiga saltata in quanto la colonna prodotto è vuota"
		continue
	end if
	
	select count(*)
	into :li_count
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode<0 then
		mle_log.text += "~r~nErrore critico in controllo esistenza cod prodotto "+ls_cod_prodotto+" : "+sqlca.sqlerrtext
		exit
	elseif li_count > 0 then
		//il prodotto esiste, quindi prosegui con l'elaborazione
	else
		mle_log.text += "~r~nRiga saltata in quanto il prodotto "+ls_cod_prodotto+" non è presente in anagrafica"
		continue
	end if
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	
	if ll_pos > 0 then
		ls_cod_nomenclatura = left(ls_str, ll_pos -1)
	else
		if len(ls_str) > 0 then
			ls_cod_nomenclatura = ls_str
		end if
	end if
	
	if ls_cod_nomenclatura="" or isnull(ls_cod_nomenclatura) then
		mle_log.text += "~r~nRiga saltata per il prodotto "+ls_cod_prodotto+" in quanto la colonna codice nomenclatura è vuota"
		continue
	end if
	
	lb_inserisci = false
	
	select cod_nomenclatura
	into	:ls_str
	from tab_nomenclature
	where	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_nomenclatura = :ls_cod_nomenclatura;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in controllo esistenza cod nomenclatura "+ls_cod_nomenclatura+" : "+sqlca.sqlerrtext
		exit
	
	elseif sqlca.sqlcode = 100 then
		//la nomenclatura non esiste, quindi la inseriamo
		insert into tab_nomenclature 
			(	cod_azienda,
				cod_nomenclatura)
		values
			(	:s_cs_xx.cod_azienda,
				:ls_cod_nomenclatura);
		
		if sqlca.sqlcode < 0 then
			mle_log.text += "~r~nErrore critico in inserimento nomenclatura "+ls_cod_nomenclatura+" : "+sqlca.sqlerrtext
			rollback;
			exit
		end if
		
		lb_inserisci = true
	end if
	
	update anag_prodotti
	set cod_nomenclatura = :ls_cod_nomenclatura
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in aggiornamento nomenclatura "+ls_cod_nomenclatura+" per il prodotto "+ls_cod_prodotto+": "+sqlca.sqlerrtext
		rollback;
		exit
	else
		
		mle_log.text += "~r~nProdotto "+ls_cod_prodotto+" aggiornato con nomenclatura "+ls_cod_nomenclatura
		
		if lb_inserisci then
			mle_log.text += " -> La nomenclatura "+ls_cod_nomenclatura+" è stata anche inserita in quanto mancante..."
		end if
		
		commit;
		
	end if
loop

fileclose(ll_file)

g_mb.warning("Fine")

end event

