﻿$PBExportHeader$w_aggiorna_ordini_evasi.srw
forward
global type w_aggiorna_ordini_evasi from Window
end type
type cb_1 from commandbutton within w_aggiorna_ordini_evasi
end type
type mle_1 from multilineedit within w_aggiorna_ordini_evasi
end type
end forward

global type w_aggiorna_ordini_evasi from Window
int X=833
int Y=361
int Width=1249
int Height=505
boolean TitleBar=true
string Title="Untitled"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
cb_1 cb_1
mle_1 mle_1
end type
global w_aggiorna_ordini_evasi w_aggiorna_ordini_evasi

on w_aggiorna_ordini_evasi.create
this.cb_1=create cb_1
this.mle_1=create mle_1
this.Control[]={ this.cb_1,&
this.mle_1}
end on

on w_aggiorna_ordini_evasi.destroy
destroy(this.cb_1)
destroy(this.mle_1)
end on

type cb_1 from commandbutton within w_aggiorna_ordini_evasi
int X=10
int Y=281
int Width=1185
int Height=109
int TabOrder=10
boolean BringToTop=true
string Text="Aggiorna Ordini di Vendita"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_sql, ls_flag_evasione, ls_flag
double ld_quan_ordine, ld_quan_evasa, ld_anno_registrazione, ld_num_registrazione, &
ld_num_dettagli, ld_num_evasi, ld_num_aperti
long ll_evasione
integer li_net

//Messaggio di conferma operazione
li_net = g_mb.messagebox("ATTENZIONE", "Aggiornare gli ordini evasi?", Question!, YesNo!)
if li_net <> 1 then return -1

//Aggiornare il flag di evasione delle righe sbagliate del dettaglio ordini 
update det_ord_ven set det_ord_ven.flag_evasione= 'E'
where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			det_ord_ven.quan_ordine <= det_ord_ven.quan_evasa and 
			det_ord_ven.flag_evasione <> 'E';

//Ricercare gli ordini aperti oppure completamente/parzialmente evasi
declare cu_tes_ord_ven cursor for
	select tes_ord_ven.num_registrazione, tes_ord_ven.anno_registrazione
	from tes_ord_ven 
	where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
open cu_tes_ord_ven;

do while 0 = 0
	fetch cu_tes_ord_ven  into :ld_num_registrazione,
										:ld_anno_registrazione;
	
	if sqlca.sqlcode = 100 then 
		g_mb.messagebox("ATTENZIONE", "Fine aggiornamento")
		exit
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("ERRORE", "Errore durante la lettura Evasione Ordini Vendita Testata.")
		close cu_tes_ord_ven;
		return -1
	end if

	ls_flag = "P"
	
	//Trovare numero totale di righe di dettaglio
	select count(*)
	into :ld_num_dettagli
	from det_ord_ven
	where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda
	and det_ord_ven.num_registrazione = :ld_num_registrazione
	and det_ord_ven.anno_registrazione = :ld_anno_registrazione;

	//Controllare se tutte le righe di dettaglio sono aperte		
	select count(*)
	into :ld_num_aperti
	from det_ord_ven
	where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda
	and det_ord_ven.num_registrazione = :ld_num_registrazione
	and det_ord_ven.anno_registrazione = :ld_anno_registrazione
	and det_ord_ven.flag_evasione = 'A';
			
	if ld_num_aperti = ld_num_dettagli then
		ls_flag = "A"
	else

		//Controllare se tutte le righe di dettaglio sono evase
		select count(*)
		into :ld_num_evasi
		from det_ord_ven
		where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda
		and det_ord_ven.num_registrazione = :ld_num_registrazione
		and det_ord_ven.anno_registrazione = :ld_anno_registrazione
		and det_ord_ven.flag_evasione = 'E';
		
		if ld_num_evasi = ld_num_dettagli then
			ls_flag = "E"
		end if
	
	end if

	//Aggiornare il flag della testata dell'ordine 
	update tes_ord_ven 
	set tes_ord_ven.flag_evasione = :ls_flag
	where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda
	and tes_ord_ven.num_registrazione = :ld_num_registrazione
	and tes_ord_ven.anno_registrazione = :ld_anno_registrazione;
	
loop

close cu_tes_ord_ven;
end event

type mle_1 from multilineedit within w_aggiorna_ordini_evasi
int X=10
int Y=13
int Width=1185
int Height=253
int TabOrder=20
boolean BringToTop=true
string Text="Premere il pulsante sottostante per aggiornare il flag di evasione degli ordini di vendita (l'aggiornamento avverrà sia sulle testate degli ordini, sia sulle righe di detaglio)"
long TextColor=8388608
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

