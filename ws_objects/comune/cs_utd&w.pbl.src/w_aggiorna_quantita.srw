﻿$PBExportHeader$w_aggiorna_quantita.srw
forward
global type w_aggiorna_quantita from w_cs_xx_principale
end type
type st_4 from statictext within w_aggiorna_quantita
end type
type st_3 from statictext within w_aggiorna_quantita
end type
type st_1 from statictext within w_aggiorna_quantita
end type
type cb_esegui from commandbutton within w_aggiorna_quantita
end type
type dw_selezione from uo_dw_main within w_aggiorna_quantita
end type
end forward

global type w_aggiorna_quantita from w_cs_xx_principale
int Width=2547
int Height=541
boolean TitleBar=true
string Title="Aggiorna Quantita"
st_4 st_4
st_3 st_3
st_1 st_1
cb_esegui cb_esegui
dw_selezione dw_selezione
end type
global w_aggiorna_quantita w_aggiorna_quantita

on w_aggiorna_quantita.create
int iCurrent
call w_cs_xx_principale::create
this.st_4=create st_4
this.st_3=create st_3
this.st_1=create st_1
this.cb_esegui=create cb_esegui
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_4
this.Control[iCurrent+2]=st_3
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=cb_esegui
this.Control[iCurrent+5]=dw_selezione
end on

on w_aggiorna_quantita.destroy
call w_cs_xx_principale::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.cb_esegui)
destroy(this.dw_selezione)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "rs_cod_prodotto", &
                 sqlca, &
					  "anag_prodotti", &
                 "cod_prodotto", &
					  "des_prodotto", &	
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave )

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

end event

type st_4 from statictext within w_aggiorna_quantita
int X=46
int Y=281
int Width=1070
int Height=61
boolean Enabled=false
string Text="L' operazione puo' richiedere parecchio tempo"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_aggiorna_quantita
int X=46
int Y=361
int Width=1555
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_aggiorna_quantita
int X=46
int Y=201
int Width=2090
int Height=61
boolean Enabled=false
string Text="Premendo il tasto 'Esegui' verrano aggiornate le quantita della tabella ANAGRAFICA PRODOTTI."
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_esegui from commandbutton within w_aggiorna_quantita
int X=2081
int Y=341
int Width=366
int Height=81
int TabOrder=10
string Text="Esegui"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;decimal ld_quantita
string ls_cod_prodotto,ls_prova_errore, ls_prodotto_selezionato
long ll_contatore

SetPointer(HourGlass!)

ll_contatore = 0

 declare cu_anag_prodotti cursor for
  select cod_prodotto
	 from anag_prodotti
   where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_prodotto like :ls_prodotto_selezionato
order by cod_prodotto;

dw_selezione.accepttext()

ls_prodotto_selezionato = dw_selezione.getitemstring(1,"rs_cod_prodotto")
if isnull(ls_prodotto_selezionato) or ls_prodotto_selezionato = "" then
	ls_prodotto_selezionato = '%'
end if	

open cu_anag_prodotti;

do while 1 = 1
	fetch cu_anag_prodotti into :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox ("Errore", "Errore durante la lettura della tabella ANAG_PRODOTTI")
		exit
	end if		
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = 0 then
		st_3.text = ls_cod_prodotto
		select sum(quan_ordinata - quan_arrivata)
		  into :ld_quantita
		  from det_ord_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_prodotto = :ls_cod_prodotto
			and flag_saldo = 'N';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox ("Errore", "Errore durante la lettura della tabella DET_ORD_ACQ")
			exit
		end if
		if sqlca.sqlcode = 0 then 
			if sqlca.sqlcode = 100 or isnull(ld_quantita) or ld_quantita < 0 then
				ld_quantita = 0
			end if
			update anag_prodotti 
			   set quan_ordinata = :ld_quantita
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode < 0 then
				ls_prova_errore = sqlca.sqlerrtext + " " + string(sqlca.sqlcode)
				g_mb.messagebox ("Errore", "Errore durante aggiornamento tabella ANAG_PRODOTTI")
				exit
			end if
			if sqlca.sqlcode = 0 then
				ll_contatore = ll_contatore + 1
			end if	
		end if	
	end if		
loop

close cu_anag_prodotti;
commit;

st_3.text = "Sono stati aggiornarnati " + string(ll_contatore) + " record"
SetPointer(Arrow!)
	 
end event

type dw_selezione from uo_dw_main within w_aggiorna_quantita
int X=23
int Y=21
int Width=2469
int Height=117
string DataObject="d_aggiorna_quantita"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

