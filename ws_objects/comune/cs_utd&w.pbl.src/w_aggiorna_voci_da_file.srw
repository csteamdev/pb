﻿$PBExportHeader$w_aggiorna_voci_da_file.srw
$PBExportComments$Utilità allineamento voci
forward
global type w_aggiorna_voci_da_file from Window
end type
type sle_file_log from singlelineedit within w_aggiorna_voci_da_file
end type
type st_2 from statictext within w_aggiorna_voci_da_file
end type
type st_3 from statictext within w_aggiorna_voci_da_file
end type
type sle_nome from singlelineedit within w_aggiorna_voci_da_file
end type
type st_dir from statictext within w_aggiorna_voci_da_file
end type
type plb_selezione from picturelistbox within w_aggiorna_voci_da_file
end type
type st_1 from statictext within w_aggiorna_voci_da_file
end type
type lb_selezione from listbox within w_aggiorna_voci_da_file
end type
end forward

global type w_aggiorna_voci_da_file from Window
int X=828
int Y=613
int Width=2245
int Height=1305
boolean TitleBar=true
string Title="Allinea voci"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
sle_file_log sle_file_log
st_2 st_2
st_3 st_3
sle_nome sle_nome
st_dir st_dir
plb_selezione plb_selezione
st_1 st_1
lb_selezione lb_selezione
end type
global w_aggiorna_voci_da_file w_aggiorna_voci_da_file

forward prototypes
public function integer wf_converti_file (string f_nome_file)
public function integer wf_leggi_file_txt (integer f_file)
public function string wf_incrementa (string fs_codice)
public function integer wf_file_di_testo ()
end prototypes

public function integer wf_converti_file (string f_nome_file);//crea un file di testo partendo dalla tabella voci

string ls_cod_voce,ls_des_voce,ls_tipo_voce,ls_nome_window,ls_cod_padre,ls_num_liv,ls_num_ord
string ls_riga
integer li_file,li_risposta,cont

cont=1

f_nome_file="D:\Irene\"+f_nome_file

li_file=FileOpen(f_nome_file,LineMode!,Write!,LockWrite!,Replace!)

if li_file = -1 then
	g_mb.messagebox("Attenzione","Errore nell'apertura del file ")
end if

 DECLARE scorri_voci CURSOR FOR  
  SELECT "voci"."cod_voce",   
         "voci"."des_voce",   
         "voci"."flag_tipo_voce",   
         "voci"."nome_window",   
         "voci"."cod_voce_padre",   
         "voci"."num_livello",   
         "voci"."num_ordinamento"  
    FROM "voci"  ;



Open scorri_voci;

do 
	
	Fetch scorri_voci into :ls_cod_voce, :ls_des_voce, :ls_tipo_voce, :ls_nome_window, :ls_cod_padre, :ls_num_liv, :ls_num_ord;   
	
	if isnull(ls_nome_window)then
		ls_nome_window="$"
	end if
	
	ls_riga=ls_cod_voce+"*"+ls_des_voce+"*"+ls_tipo_voce+"*"+ls_nome_window+"*"+ls_cod_padre+"*"+ls_num_liv+"*"+ls_num_ord+"*"
   li_risposta = Filewrite(li_file,ls_riga)
	cont=cont+1

loop until sqlca.sqlcode = 100 

g_mb.messagebox("Controllo","Totale righe scritte: "+string(cont))
FileClose(li_file)
close scorri_voci;
return 0
end function

public function integer wf_leggi_file_txt (integer f_file);//legge un file di testo e memorizza i dati nelle variabili corrispondenti

string ls_riga,ls_cod_voce,ls_des_voce,ls_tipo,ls_nome_window,ls_cod_padre,ls_num_liv,ls_num_ord
integer li_risposta,li_file,li_pos,i,cont
string ls_linea


   li_risposta = Fileread(f_file,ls_riga)
	
	
	li_pos  = Pos(ls_riga,"*",1)
	
	ls_cod_voce = Mid(ls_riga,1,li_pos - 1)
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_des_voce = Mid(ls_riga,i,li_pos - i )
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_tipo = Mid(ls_riga,i,li_pos - i )
			
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_nome_window = Mid(ls_riga,i,li_pos - i )
	
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_cod_padre = Mid(ls_riga,i,li_pos - i )
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_num_liv = Mid(ls_riga,i,li_pos - i )
					
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"*",i+1)
	ls_num_ord = Mid(ls_riga,i,li_pos - i )
	
	ls_linea=ls_cod_voce+"*"+ls_des_voce+"*"+ls_tipo+"*"+ls_nome_window+"*"+ls_cod_padre+"*"+ls_num_liv+"*"+ls_num_ord
	//FileWrite(li_file,ls_linea)
	
return li_risposta


	
end function

public function string wf_incrementa (string fs_codice);//funzione che incrementa il codice predefinito I00

string ls_prima_cifra,ls_sec_cifra,ls_numeri
integer li_converti_1,li_converti_2

ls_prima_cifra=mid(fs_codice,3)
ls_sec_cifra=mid(fs_codice,2)

li_converti_1=asc(ls_prima_cifra)+1
li_converti_2=asc(ls_sec_cifra)

if li_converti_1=9 then
	li_converti_2=li_converti_2+1
	if li_converti_2=9 then
		g_mb.messagebox("Attenzione","La cartella temporanea è piena")
		return "-1"
	else
	 li_converti_1=0
	 ls_numeri="I"+char(li_converti_2)+char(li_converti_1)
   end if 
else
	ls_numeri="I"+char(li_converti_2)+char(li_converti_1)
end if

return ls_numeri
end function

public function integer wf_file_di_testo ();string ls_riga,ls_cod_voce_p,ls_des_voce_p,ls_tipo_p,ls_nome_window_p,ls_cod_padre_p,ls_num_liv_p,ls_num_ord_p
integer li_risposta,li_file,li_file_r,li_pos,i,cont,li_cartella,li_write,li_presente,li_presente2
string ls_linea,ls_max,ls_cod_pred,ls_nome_file

ls_nome_file=sle_file_log.text

li_file_r=FileOpen(sle_nome.text,LineMode!,Read!,LockRead!)

if li_file_r=-1 then
	g_mb.messagebox("Attenzione","Errore nell'apertura del file: "+sle_nome.text)
end if
	
li_file=FileOpen(ls_nome_file,LineMode!,Write!,LockWrite!,Replace!)

if li_file =-1 then
	g_mb.messagebox("Attenzione","Errore nell'apertura del file: "+ls_nome_file)
end if

li_risposta = Fileread(li_file_r,ls_riga)

//controllo se esiste già la cartella predefinita I00
select count(*) into :li_cartella from voci where (cod_voce='I00');
if li_cartella=0 then
	//la cartella non esiste bisogna crearla
	insert into voci(cod_voce,des_voce,flag_tipo_voce,num_livello,num_ordinamento)values('I00','Cartella temporanea','M','1','1');
	if sqlca.sqlcode<>0 then
	 g_mb.messagebox("Attenzione","Si è verificato un errore durante la creazione della cartella predefinita I00 "+sqlca.sqlerrtext)
	 li_write = filewrite(li_file,sqlca.sqlerrtext)
	end if
	g_mb.messagebox("Controllo","Cartella predefinita I00 creata ")
else
	g_mb.messagebox("Controllo","Cartella predefinita I00 già presente ")
end if

select max(cod_voce) into :ls_max from voci where (cod_voce like 'I%');
ls_cod_pred=ls_max
//messagebox("Controllo",ls_max)

do while li_risposta<>-100
	
	li_pos  = Pos(ls_riga,"	",1)
	
	ls_cod_voce_p = Mid(ls_riga,1,li_pos - 1)
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_des_voce_p = Mid(ls_riga,i,li_pos - i )
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_tipo_p = Mid(ls_riga,i,li_pos - i )
			
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_nome_window_p = Mid(ls_riga,i,li_pos - i )
	
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_cod_padre_p = Mid(ls_riga,i,li_pos - i )
		
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_num_liv_p = Mid(ls_riga,i,li_pos - i )
					
	i = li_pos + 1
	li_pos  = Pos(ls_riga,"	",i)
	ls_num_ord_p = Mid(ls_riga,i,len(ls_riga) - 1 )
	
	ls_linea=ls_cod_voce_p+"*"+ls_des_voce_p+"*"+ls_tipo_p+"*"+ls_nome_window_p+"*"+ls_cod_padre_p+"*"+ls_num_liv_p+"*"+ls_num_ord_p
	//FileWrite(li_write,ls_linea)
	
if mid(ls_cod_voce_p,1,1)<>"I" then
 if ls_tipo_p="W" then 
  select count(*) into :li_presente from voci where (nome_window = :ls_nome_window_p);
    if li_presente = 0 then		
		//la finestra non è presente nel database del cliente
		//controllare se il codice di partenza è libero nel database del cliente
		select count(*) into :li_presente2 from voci where (cod_voce = :ls_cod_voce_p);
		if (li_presente2 = 0) then 
			//il codice è libero inseriamo il nuovo record con questo codice ls_cod_voce_p
	      insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello,num_ordinamento)values(:ls_cod_voce_p,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p,:ls_cod_padre_p,:ls_num_liv_p,:ls_num_ord_p);
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
				li_write = filewrite(li_file,sqlca.sqlerrtext)
			end if
         li_write = filewrite(li_file,ls_cod_voce_p+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_nome_window_p+" "+ls_cod_padre_p)
         //messagebox("Controllo",ls_cod_voce_p+"  "+ls_nome_window_p+"~n tipo voce "+ls_tipo_p)
		elseif li_presente2>0 then
			//la finestra deve essere aggiunta solo che il codice è già occupato
			//inseriamo il nuovo record con il codice predefinito
			//wf_incrementa incrementa il codice predefinito I00
			ls_cod_pred=wf_incrementa(ls_cod_pred)
			if ls_cod_pred="-1" then
				exit
			end if
			//messagebox("Controllo","ls_cod_pred è :"+ls_cod_pred)
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello)values(:ls_cod_pred,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, 'I00','2');
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			   ls_des_voce_p=ls_des_voce_p+"***"
		   end if
         li_write = filewrite(li_file,ls_cod_pred+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_nome_window_p+" "+ls_cod_padre_p)
		   //messagebox("Controllo","Il codice "+ls_cod_voce_p+" è già in uso  ~n tipo voce "+ls_tipo_p)
		end if
    end if
  elseif ls_tipo_p='M' then
	 select count(*) into :li_presente from voci where (des_voce = :ls_des_voce_p);
	 if li_presente=0 then
		//controlla se il codice è libero
		select count(*) into :li_presente2 from voci where (cod_voce= :ls_cod_voce_p);
		 if li_presente2=0 then
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello,num_ordinamento)values(:ls_cod_voce_p,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, :ls_cod_padre_p, :ls_num_liv_p, :ls_num_ord_p);
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			  ls_des_voce_p=ls_des_voce_p+"**"
		   end if
			li_write = filewrite(li_file,ls_cod_voce_p+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_cod_padre_p)
		 elseif li_presente2>0 then	
		   //incrementare il codice predefinito
			ls_cod_pred=wf_incrementa(ls_cod_pred)
			if ls_cod_pred="-1" then
			 exit
			end if
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello)values(:ls_cod_pred,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, 'I00','2');
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			  ls_des_voce_p=ls_des_voce_p+"*****"
		   end if
			li_write = filewrite(li_file,ls_cod_pred+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_cod_padre_p)
		 end if	
	 end if
  end if
end if 
  li_risposta = Fileread(li_file_r,ls_riga)

loop


g_mb.messagebox("Confronto voci","Confronto terminato")		

fileclose(li_file_r)
fileclose(li_file)

plb_selezione.enabled=false
lb_selezione.enabled=true


return 0
end function

on w_aggiorna_voci_da_file.create
this.sle_file_log=create sle_file_log
this.st_2=create st_2
this.st_3=create st_3
this.sle_nome=create sle_nome
this.st_dir=create st_dir
this.plb_selezione=create plb_selezione
this.st_1=create st_1
this.lb_selezione=create lb_selezione
this.Control[]={ this.sle_file_log,&
this.st_2,&
this.st_3,&
this.sle_nome,&
this.st_dir,&
this.plb_selezione,&
this.st_1,&
this.lb_selezione}
end on

on w_aggiorna_voci_da_file.destroy
destroy(this.sle_file_log)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.sle_nome)
destroy(this.st_dir)
destroy(this.plb_selezione)
destroy(this.st_1)
destroy(this.lb_selezione)
end on

event open;integer fi, ind, ini, t, totale
string testo



lb_selezione.additem("Profilo_1")
lb_selezione.additem("Profilo_2")
lb_selezione.additem("Profilo_3")
lb_selezione.additem("Profilo_4")
lb_selezione.additem("File di testo")


plb_selezione.dirlist(".",16400,st_dir)
ind = 1
fi = plb_selezione.FindItem("[",ind)
ini = fi
	
DO
   testo = plb_selezione.text(fi)
	plb_selezione.Deleteitem(fi)
	plb_selezione.InsertItem(testo, 1,fi)
	ind = fi
	fi = plb_selezione.FindItem("[",ind)
	
LOOP UNTIL fi = ini

totale = plb_selezione.totalitems()

for t = 1 to totale
 testo = plb_selezione.text(t)
 if not Match(testo, "^[\[]") then
	plb_selezione.Deleteitem(t)
	plb_selezione.InsertItem(testo, 2,t)
 end if
next

lb_selezione.enabled=false
sle_file_log.setfocus()
 

end event

type sle_file_log from singlelineedit within w_aggiorna_voci_da_file
int X=275
int Y=101
int Width=892
int Height=81
int TabOrder=31
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;sle_file_log.enabled=false
lb_selezione.enabled=true

end event

type st_2 from statictext within w_aggiorna_voci_da_file
int X=46
int Y=101
int Width=677
int Height=77
boolean Enabled=false
string Text="File Log"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_aggiorna_voci_da_file
int X=1418
int Y=781
int Width=686
int Height=77
boolean Enabled=false
string Text="File da confrontare"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_nome from singlelineedit within w_aggiorna_voci_da_file
int X=1418
int Y=901
int Width=709
int Height=81
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
boolean DisplayOnly=true
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_dir from statictext within w_aggiorna_voci_da_file
int X=1418
int Y=81
int Width=709
int Height=61
boolean Enabled=false
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type plb_selezione from picturelistbox within w_aggiorna_voci_da_file
int X=1418
int Y=181
int Width=732
int Height=561
int TabOrder=30
boolean Enabled=false
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string PictureName[]={"Custom039!",&
"ScriptYes!"}
long PictureMaskColor=553648127
end type

event doubleclicked;string ls_file, ls_est, ls_riga
integer t, li_totale, li_find, ind, ini

ls_est = "*.*"

if plb_selezione.DirSelect(ls_file) THEN 
	ls_file = ls_file + ls_est
	plb_selezione.DirList(ls_file, 16400,st_dir)
	ind = 1
   li_find = plb_selezione.FindItem("[",ind)
	ini = li_find

 DO
   ls_riga = plb_selezione.text(li_find)
	plb_selezione.Deleteitem(li_find)
	plb_selezione.InsertItem(ls_riga, 1,li_find)
	ind = li_find
	li_find = plb_selezione.FindItem("[",ind)
	
 LOOP UNTIL li_find = ini

 li_totale = plb_selezione.totalitems()

 for t = 1 to li_totale
	ls_riga = plb_selezione.text(t)
   if not Match(ls_riga, "^[\[]") then
	 plb_selezione.Deleteitem(t)
	 plb_selezione.InsertItem(ls_riga, 2,t)
   end if
 next
 sle_nome.text = ""

ELSE
 sle_nome.text = plb_selezione.selecteditem()
 wf_file_di_testo()
END IF


end event

type st_1 from statictext within w_aggiorna_voci_da_file
int X=33
int Y=265
int Width=1093
int Height=77
boolean Enabled=false
string Text="Selezionare la fonte di dati di partenza"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type lb_selezione from listbox within w_aggiorna_voci_da_file
int X=46
int Y=361
int Width=494
int Height=309
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event doubleclicked;string ls_database,ls_key,ls_numero,ls_dbms,ls_dbparm,ls_servername,ls_log,ls_pass,ls_engine
string ls_cod_voce_p,ls_des_voce_p,ls_tipo_p,ls_nome_window_p,ls_cod_padre_p,ls_num_liv_p,ls_num_ord_p
string ls_riga,ls_win,ls_cod,ls_des,ls_cod_pred,ls_max,ls_nome_file
integer li_pos,li_file,li_write, li_presente, li_presente2,li_presente3,cont,i,li_cartella
transaction t_dati_partenza

lb_selezione.DirSelect(ls_riga)

cont=0



if ls_riga="File di testo." then
	
  plb_selezione.enabled=true
  lb_selezione.enabled=false
	
else
	
  ls_nome_file=sle_file_log.text
  li_file=FileOpen(ls_nome_file,LineMode!, Write!, LockWrite!, Replace!)
  
  if li_file=-1 then
	 g_mb.messagebox("Attenzione","Inserire il percorso completo")
	 sle_file_log.enabled=true
	 lb_selezione.enabled=false
  end if
   
	sle_file_log.enabled=false
	lb_selezione.enabled=true
  
  //creo transazione partendo dal profilo scelto
  //li_pos=pos(ls_riga,"_")
  ls_numero=mid(ls_riga,9,1)
  ls_key= "HKEY_LOCAL_MACHINE\Software\Consulting&Software\"
  Registryget(ls_key + "database_"+ls_numero,"database",ls_database)
  Registryget(ls_key + "database_"+ls_numero,"dbms",ls_dbms)
  Registryget(ls_key + "database_"+ls_numero,"dbparm",ls_dbparm)
  Registryget(ls_key + "database_"+ls_numero,"servername",ls_servername)
  Registryget(ls_key + "database_"+ls_numero,"logid",ls_log)
  Registryget(ls_key + "database_"+ls_numero,"logpass",ls_pass)
  Registryget(ls_key + "database_"+ls_numero,"enginetype",ls_engine)	
  
  t_dati_partenza = create transaction
  t_dati_partenza.Database =  ls_database
  t_dati_partenza.Dbms =      ls_dbms
  t_dati_partenza.DbParm =    ls_dbparm
  t_dati_partenza.servername= ls_servername
  t_dati_partenza.logid =     ls_log
  t_dati_partenza.logpass =   ls_pass
  connect using t_dati_partenza;
 
  if t_dati_partenza.sqlcode=-1 then
	g_mb.messagebox("Errore","Attenzione si è verificato un errore durante la connessione "+t_dati_partenza.sqlerrtext)
  else
	g_mb.messagebox("Confronto voci","Connessione al profilo_"+ls_numero+" riuscita")
  end if

 DECLARE scorri_voci_p CURSOR FOR  
  SELECT voci.cod_voce,   
         voci.des_voce,   
         voci.flag_tipo_voce,   
         voci.nome_window,   
         voci.cod_voce_padre,  
			voci.num_livello,
			voci.num_ordinamento
    FROM voci  using t_dati_partenza;
	 
open scorri_voci_p;

fetch scorri_voci_p into :ls_cod_voce_p, :ls_des_voce_p, :ls_tipo_p, :ls_nome_window_p, :ls_cod_padre_p, :ls_num_liv_p, :ls_num_ord_p;

//controllo se esiste già la cartella predefinita I00
select count(*) into :li_cartella from voci where (cod_voce='I00');
if li_cartella=0 then
	//la cartella non esiste bisogna crearla
	insert into voci(cod_voce,des_voce,flag_tipo_voce,num_livello,num_ordinamento)values('I00','Cartella temporanea','M','1','1');
	if sqlca.sqlcode<>0 then
	 g_mb.messagebox("Attenzione","Si è verificato un errore durante la creazione della cartella predefinita I00 "+sqlca.sqlerrtext)
	 li_write = filewrite(li_file,sqlca.sqlerrtext)
	end if
	g_mb.messagebox("Controllo","Cartella predefinita I00 creata ")
else
	g_mb.messagebox("Controllo","Cartella predefinita I00 già presente ")
end if

select max(cod_voce) into :ls_max from voci where (cod_voce like 'I%');
ls_cod_pred=ls_max


do 
if mid(ls_cod_voce_p,1,1)<>"I" then	
if ls_tipo_p="W" then 
  select count(*) into :li_presente from voci where (nome_window = :ls_nome_window_p);
    if li_presente = 0 then		
		//la finestra non è presente nel database del cliente
		//controllare se il codice di partenza è libero nel database del cliente
		select count(*) into :li_presente2 from voci where (cod_voce = :ls_cod_voce_p);
		if (li_presente2 = 0) then 
			//il codice è libero inseriamo il nuovo record con questo codice ls_cod_voce_p
	      insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello,num_ordinamento)values(:ls_cod_voce_p,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p,:ls_cod_padre_p,:ls_num_liv_p,:ls_num_ord_p);
			if sqlca.sqlcode<>0 then
				g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
				li_write = filewrite(li_file,sqlca.sqlerrtext)
			end if
         li_write = filewrite(li_file,ls_cod_voce_p+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_nome_window_p+" "+ls_cod_padre_p)
         //messagebox("Controllo",ls_cod_voce_p+"  "+ls_nome_window_p+"~n tipo voce "+ls_tipo_p)
		elseif li_presente2>0 then
			//la finestra deve essere aggiunta solo che il codice è già occupato
			//inseriamo il nuovo record con il codice predefinito
			//wf_incrementa incrementa il codice predefinito I00
			ls_cod_pred=wf_incrementa(ls_cod_pred)
			if ls_cod_pred="-1" then
				exit
			end if
			//messagebox("Controllo","ls_cod_pred è :"+ls_cod_pred)
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello)values(:ls_cod_pred,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, 'I00','2');
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			   ls_des_voce_p=ls_des_voce_p+"***"
		   end if
         li_write = filewrite(li_file,ls_cod_pred+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_nome_window_p+" "+ls_cod_padre_p)
		   //messagebox("Controllo","Il codice "+ls_cod_voce_p+" è già in uso  ~n tipo voce "+ls_tipo_p)
		end if
    end if
  elseif ls_tipo_p='M' then
	 select count(*) into :li_presente from voci where (des_voce = :ls_des_voce_p);
	 if li_presente=0 then
		//controlla se il codice è libero
		select count(*) into :li_presente2 from voci where (cod_voce= :ls_cod_voce_p);
		 if li_presente2=0 then
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello,num_ordinamento)values(:ls_cod_voce_p,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, :ls_cod_padre_p, :ls_num_liv_p, :ls_num_ord_p);
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			  ls_des_voce_p=ls_des_voce_p+"**"
		   end if
			li_write = filewrite(li_file,ls_cod_voce_p+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_cod_padre_p)
		 elseif li_presente2>0 then	
		   //incrementare il codice predefinito
			ls_cod_pred=wf_incrementa(ls_cod_pred)
			if ls_cod_pred="-1" then
			 exit
			end if
			insert into voci(cod_voce,des_voce,flag_tipo_voce,nome_window,cod_voce_padre,num_livello)values(:ls_cod_pred,:ls_des_voce_p,:ls_tipo_p,:ls_nome_window_p, 'I00','2');
			if sqlca.sqlcode<>0 then
			  g_mb.messagebox("Attenzione","Si è verificato un errore durante l'inserimento "+sqlca.sqlerrtext)
			  li_write = filewrite(li_file,sqlca.sqlerrtext)
			  ls_des_voce_p=ls_des_voce_p+"*****"
		   end if
			li_write = filewrite(li_file,ls_cod_pred+" "+ls_des_voce_p+" "+ls_tipo_p+" "+ls_cod_padre_p)
		 end if	
	 end if
  end if 
end if
					
fetch scorri_voci_p into :ls_cod_voce_p, :ls_des_voce_p, :ls_tipo_p, :ls_nome_window_p, :ls_cod_padre_p, :ls_num_liv_p, :ls_num_ord_p;

loop until t_dati_partenza.sqlcode <> 0 	
  
g_mb.messagebox("Confronto voci","Confronto terminato")
  
close scorri_voci_p;
  
fileclose(li_file)

disconnect using t_dati_partenza;

end if		



end event

