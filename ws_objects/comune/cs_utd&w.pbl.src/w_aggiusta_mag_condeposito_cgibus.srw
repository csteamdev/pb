﻿$PBExportHeader$w_aggiusta_mag_condeposito_cgibus.srw
forward
global type w_aggiusta_mag_condeposito_cgibus from w_cs_xx_principale
end type
type cb_crea_stock_corretti from commandbutton within w_aggiusta_mag_condeposito_cgibus
end type
type st_6 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type cb_cancella_stock from commandbutton within w_aggiusta_mag_condeposito_cgibus
end type
type st_5 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type cb_verifica from commandbutton within w_aggiusta_mag_condeposito_cgibus
end type
type st_4 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type st_3 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type st_1 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type st_12 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type st_11 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type em_data_ini from editmask within w_aggiusta_mag_condeposito_cgibus
end type
type cb_ini from commandbutton within w_aggiusta_mag_condeposito_cgibus
end type
type dw_selezione from uo_cs_xx_dw within w_aggiusta_mag_condeposito_cgibus
end type
type st_legenda from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type st_8 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type em_anno from editmask within w_aggiusta_mag_condeposito_cgibus
end type
type st_2 from statictext within w_aggiusta_mag_condeposito_cgibus
end type
type em_log from editmask within w_aggiusta_mag_condeposito_cgibus
end type
type em_file from editmask within w_aggiusta_mag_condeposito_cgibus
end type
type cb_apri from commandbutton within w_aggiusta_mag_condeposito_cgibus
end type
end forward

global type w_aggiusta_mag_condeposito_cgibus from w_cs_xx_principale
integer width = 2834
integer height = 1508
string title = "Aggiustamenti di Magazzino -:)"
cb_crea_stock_corretti cb_crea_stock_corretti
st_6 st_6
cb_cancella_stock cb_cancella_stock
st_5 st_5
cb_verifica cb_verifica
st_4 st_4
st_3 st_3
st_1 st_1
st_12 st_12
st_11 st_11
em_data_ini em_data_ini
cb_ini cb_ini
dw_selezione dw_selezione
st_legenda st_legenda
st_8 st_8
em_anno em_anno
st_2 st_2
em_log em_log
em_file em_file
cb_apri cb_apri
end type
global w_aggiusta_mag_condeposito_cgibus w_aggiusta_mag_condeposito_cgibus

forward prototypes
public function boolean wf_bisestile (long fl_anno)
public subroutine wf_remove_space (ref string as_mystring)
public function integer wf_aggiorna_doc (long fl_anno_mov, long fl_num_mov, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock)
end prototypes

public function boolean wf_bisestile (long fl_anno);if mod(fl_anno, 4) = 0 then
	//divisibile per 4: altri controlli da fare
	if mod(fl_anno, 100) = 0 then
		//divisibile per 100, inizio secolo: altri controlli da fare
		if mod(fl_anno, 400) = 0 then
			//divisibile per 400: BISESTILE
			return true
		else
			//non divisibile per 400: NON BISESTILE
		end if
	else
		//non divisibile per 100, non inizio secolo: BISESTILE
		return true
	end if	
else
	//non divisibile per 4: NON BISESTILE
	return false
end if
end function

public subroutine wf_remove_space (ref string as_mystring);long start_pos=1

//toglie tutti gli spazi presenti prima diìopo e all'interno

if not isnull(as_mystring) and as_mystring<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(as_mystring, " ", start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 as_mystring = Replace(as_mystring, start_pos, Len(" "), "")
		 // Find the next occurrence of old_str.
		 start_pos = Pos(as_mystring, " ", start_pos+Len(""))
	LOOP	
else
	as_mystring=""
end if
end subroutine

public function integer wf_aggiorna_doc (long fl_anno_mov, long fl_num_mov, string fs_cod_prodotto, string fs_cod_deposito, string fs_cod_ubicazione, string fs_cod_lotto, datetime fdt_data_stock, long fl_prog_stock);string ls_tabella[], ls_anno[], ls_numero[], ls_sql, ls_fileERR
long ll_i, ll_tot, ll_filelog

ls_fileERR = em_log.text+"LOG_aggiorna_doc.txt"
filedelete(ls_fileERR)

ls_tabella[1] = "det_bol_ven"
ls_anno[1] = "anno_registrazione_mov_mag"
ls_numero[1] = "num_registrazione_mov_mag"

ls_tabella[2] = "det_fat_ven"
ls_anno[2] = "anno_registrazione_mov_mag"
ls_numero[2] = "num_registrazione_mov_mag"

ls_tabella[3] = "det_bol_acq"
ls_anno[3] = "anno_registrazione_mov_mag"
ls_numero[3] = "num_registrazione_mov_mag"

ls_tabella[4] = "det_fat_acq"
ls_anno[4] = "anno_registrazione_mov_mag"
ls_numero[4] = "num_registrazione_mov_mag"


ll_tot = upperbound(ls_tabella)
for ll_i=1 to ll_tot
	
	ls_sql="update " + ls_tabella[ll_i] + &
       " set  cod_deposito='"+fs_cod_deposito+"', " + &
		 "cod_ubicazione='"+fs_cod_ubicazione+"', "+&
		  "cod_lotto='"+fs_cod_ubicazione+"', "+&
		   "data_stock='"+string(fdt_data_stock, s_cs_xx.db_funzioni.formato_data)+"', "
			
	if ls_tabella[ll_i] = "det_bol_ven" or ls_tabella[ll_i]="det_fat_ven" then
		ls_sql += " progr_stock="+string(fl_prog_stock)+" "
	elseif ls_tabella[ll_i] = "det_bol_acq" or ls_tabella[ll_i]="det_fat_acq" then
		ls_sql += " prog_stock="+string(fl_prog_stock)+" "
	else
		g_mb.messagebox("CSTEAM", "verificare sql per la tabella " + ls_tabella[ll_i]+" "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	ls_sql += " where cod_azienda ='" + s_cs_xx.cod_azienda + "' and " + &
             ls_anno[ll_i] + " = " + string(fl_anno_mov) + " and " +  &
             ls_numero[ll_i]  + " = " + string(fl_num_mov) + " " 

	EXECUTE IMMEDIATE :ls_sql USING sqlca;
	
	if sqlca.sqlcode < 0 then	
		g_mb.messagebox("CSTEAM", "Errore in update tabella " + ls_tabella[ll_i]+" "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
	filewrite(ll_filelog, "Aggiornata tabella "+ls_tabella[ll_i]+" per il prodotto "+fs_cod_prodotto)
	fileclose(ll_filelog)
next

return 1
end function

on w_aggiusta_mag_condeposito_cgibus.create
int iCurrent
call super::create
this.cb_crea_stock_corretti=create cb_crea_stock_corretti
this.st_6=create st_6
this.cb_cancella_stock=create cb_cancella_stock
this.st_5=create st_5
this.cb_verifica=create cb_verifica
this.st_4=create st_4
this.st_3=create st_3
this.st_1=create st_1
this.st_12=create st_12
this.st_11=create st_11
this.em_data_ini=create em_data_ini
this.cb_ini=create cb_ini
this.dw_selezione=create dw_selezione
this.st_legenda=create st_legenda
this.st_8=create st_8
this.em_anno=create em_anno
this.st_2=create st_2
this.em_log=create em_log
this.em_file=create em_file
this.cb_apri=create cb_apri
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_crea_stock_corretti
this.Control[iCurrent+2]=this.st_6
this.Control[iCurrent+3]=this.cb_cancella_stock
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.cb_verifica
this.Control[iCurrent+6]=this.st_4
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.st_12
this.Control[iCurrent+10]=this.st_11
this.Control[iCurrent+11]=this.em_data_ini
this.Control[iCurrent+12]=this.cb_ini
this.Control[iCurrent+13]=this.dw_selezione
this.Control[iCurrent+14]=this.st_legenda
this.Control[iCurrent+15]=this.st_8
this.Control[iCurrent+16]=this.em_anno
this.Control[iCurrent+17]=this.st_2
this.Control[iCurrent+18]=this.em_log
this.Control[iCurrent+19]=this.em_file
this.Control[iCurrent+20]=this.cb_apri
end on

on w_aggiusta_mag_condeposito_cgibus.destroy
call super::destroy
destroy(this.cb_crea_stock_corretti)
destroy(this.st_6)
destroy(this.cb_cancella_stock)
destroy(this.st_5)
destroy(this.cb_verifica)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.em_data_ini)
destroy(this.cb_ini)
destroy(this.dw_selezione)
destroy(this.st_legenda)
destroy(this.st_8)
destroy(this.em_anno)
destroy(this.st_2)
destroy(this.em_log)
destroy(this.em_file)
destroy(this.cb_apri)
end on

event pc_setwindow;call super::pc_setwindow;em_anno.text = string(f_anno_esercizio())

dw_selezione.insertrow(0)
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_deposito", &
//							sqlca, &
//							"anag_depositi", &
//							"cod_deposito", &
//                     "des_deposito", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type cb_crea_stock_corretti from commandbutton within w_aggiusta_mag_condeposito_cgibus
integer x = 1765
integer y = 992
integer width = 343
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Go"
end type

event clicked;string				ls_sql, ls_cod_prodotto_ds
string				ls_cod_deposito_OK, ls_cod_ubicazione_OK, ls_cod_lotto_OK, ls_fileERR
datetime			ldt_data_stock_ds, ldt_data_stock_OK
datastore			lds_prodotti
long					ll_i, ll_row, ll_prog_stock_ds, ll_prog_stock_OK, ll_count, ll_anno_mov, ll_num_mov, ll_filelog

ls_fileERR = em_log.text+"5_LOG_stock_mancanti.txt"
filedelete(ls_fileERR)

//stock corretto
ls_cod_deposito_OK 			= "001"
ls_cod_ubicazione_OK 		= "UB001"
ls_cod_lotto_OK 				= "LT001"
ldt_data_stock_OK 			= datetime(date(2000, 1, 1), time("00:00:00"))
ll_prog_stock_OK 				= 10

//movimenti con data nell'anno 2010 e stock diverso da quello ritenuto corretto
ls_sql = "select "+&
				"cod_prodotto "+&				
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' "
					
if f_crea_datastore(lds_prodotti, ls_sql) then
else
	g_mb.messagebox("CSTEAM","Errore durante la creazione del datastore",StopSign!)
	rollback;
	return
end if

ll_row = lds_prodotti.retrieve()

for ll_i=1 to ll_row
	ls_cod_prodotto_ds = lds_prodotti.getitemstring(ll_i, "cod_prodotto")
	
	st_legenda.text = "Elaborazione Prodotto: "+ls_cod_prodotto_ds
	
	//vedo se nella  tabella stock esiste tale prodotto (se esiste sicuramente lo stock è corretto!)
	setnull(ll_count)
	select count(*)
	into :ll_count
	from stock
	where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_ds and
			cod_deposito=:ls_cod_deposito_OK;
		
	if sqlca.sqlcode<0 then
		g_mb.messagebox("CSTEAM","Errore durante la lettura se esiste lo stock corretto!"+sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if
	
	if ll_count>0 then
		//lo stock corretto esiste
	elseif ll_count=0 then
		//stock inesistente: inserirlo
		insert into stock
			(cod_azienda,cod_prodotto,cod_deposito,cod_ubicazione,
			 cod_lotto,data_stock,prog_stock,giacenza_stock)
		 values
			(:s_cs_xx.cod_azienda,:ls_cod_prodotto_ds,:ls_cod_deposito_OK,:ls_cod_ubicazione_OK,
			 :ls_cod_lotto_OK,:ldt_data_stock_OK,:ll_prog_stock_OK,0);
			 
		if sqlca.sqlcode<0 then
			g_mb.messagebox("CSTEAM","Errore durante l'inserimento dello stock! "+&
										"Prodotto:"+ls_cod_prodotto_ds + &
										sqlca.sqlerrtext,StopSign!)
			rollback;
			return
		end if
		
		ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
		filewrite(ll_filelog, "Inserito stock mancante per il prodotto "+ls_cod_prodotto_ds)
		fileclose(ll_filelog)
		
	end if
next

//se arrivi fin qui fai la commit;
commit;
g_mb.messagebox("CSTEAM","Operazione terminata!",StopSign!)

return
end event

type st_6 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 64
integer y = 1012
integer width = 1650
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "5. creazione dello stock corretto per tutti i prodotti senza stock:"
boolean focusrectangle = false
end type

type cb_cancella_stock from commandbutton within w_aggiusta_mag_condeposito_cgibus
integer x = 1531
integer y = 864
integer width = 343
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Go"
end type

event clicked;string			ls_cod_deposito_OK, ls_cod_ubicazione_OK, ls_cod_lotto_OK
datetime		ldt_data_stock_OK
long				ll_prog_stock_OK

//stock corretto
ls_cod_deposito_OK 			= "001"
ls_cod_ubicazione_OK 		= "UB001"
ls_cod_lotto_OK 				= "LT001"
ldt_data_stock_OK 			= datetime(date(2000, 1, 1), time("00:00:00"))
ll_prog_stock_OK 				= 10

delete from stock
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_deposito=:ls_cod_deposito_OK and
	(cod_ubicazione<>:ls_cod_ubicazione_OK or
		cod_lotto<>:ls_cod_lotto_OK or data_stock<>:ldt_data_stock_OK or
		prog_stock<>:ll_prog_stock_OK);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("CSTEAM", "Errore durante la cancellazione degli stock!"+sqlca.sqlerrtext, stopsign!)
	rollback;
	return
else
	commit;
	g_mb.messagebox("CSTEAM", "Operazione terminata!", stopsign!)
end if
end event

type st_5 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 64
integer y = 876
integer width = 1403
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "4. Cancellazione degli stock diversi da quello corretto:"
boolean focusrectangle = false
end type

type cb_verifica from commandbutton within w_aggiusta_mag_condeposito_cgibus
integer x = 1865
integer y = 744
integer width = 343
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Go"
end type

event clicked;string				ls_sql, ls_cod_prodotto_ds, ls_cod_deposito_ds, ls_cod_ubicazione_ds, ls_cod_lotto_ds
string				ls_cod_deposito_OK, ls_cod_ubicazione_OK, ls_cod_lotto_OK, ls_fileERR
datetime			ldt_1gen2010, ldt_data_stock_ds, ldt_data_stock_OK
datastore			lds_mov
long					ll_i, ll_row, ll_prog_stock_ds, ll_prog_stock_OK, ll_count, ll_anno_mov, ll_num_mov, ll_filelog
string				ls_where_stock

st_legenda.text=""
ls_fileERR = em_log.text+"3_LOG_sist_mov_2010.txt"
filedelete(ls_fileERR)

ldt_1gen2010 					= datetime(date(2010, 1, 1), time("00:00:00"))

//stock corretto
ls_cod_deposito_OK 			= "001"
ls_cod_ubicazione_OK 		= "UB001"
ls_cod_lotto_OK 				= "LT001"
ldt_data_stock_OK 			= datetime(date(2000, 1, 1), time("00:00:00"))
ll_prog_stock_OK 				= 10

ls_where_stock = " cod_deposito='001' and "+&
							" (cod_ubicazione<>'UB001' or "+&
									"cod_lotto<>'LT001' or "+&
									"data_stock<>'"+string(ldt_data_stock_OK, s_cs_xx.db_funzioni.formato_data)+"' or "+&
									"prog_stock<>"+string(ll_prog_stock_OK)+" "+&
							")"

//movimenti con data nell'anno 2010 e stock diverso da quello ritenuto corretto
ls_sql = "select "+&
				"anno_registrazione,"+&
				"num_registrazione,"+&
				"cod_prodotto,"+&
				"cod_deposito,"+&
				"cod_ubicazione,"+&
				"cod_lotto,"+&
				"data_stock,"+&
				"prog_stock "+&
			"from mov_magazzino "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"data_registrazione>='"+string(ldt_1gen2010, s_cs_xx.db_funzioni.formato_data)+"' "+&
					"and "+ls_where_stock

ls_sql += " order by anno_registrazione, num_registrazione "
					
if f_crea_datastore(lds_mov, ls_sql) then
else
	g_mb.messagebox("CSTEAM","Errore durante la creazione del datastore",StopSign!)
	rollback;
	return
end if

ll_row = lds_mov.retrieve()

for ll_i=1 to ll_row		
	ll_anno_mov = lds_mov.getitemnumber(ll_i, "anno_registrazione")
	ll_num_mov = lds_mov.getitemnumber(ll_i, "num_registrazione")
	ls_cod_prodotto_ds = lds_mov.getitemstring(ll_i, "cod_prodotto")
	ls_cod_deposito_ds = lds_mov.getitemstring(ll_i, "cod_deposito")
	ls_cod_ubicazione_ds = lds_mov.getitemstring(ll_i, "cod_ubicazione")
	ls_cod_lotto_ds = lds_mov.getitemstring(ll_i, "cod_lotto")
	ldt_data_stock_ds = lds_mov.getitemdatetime(ll_i, "data_stock")
	ll_prog_stock_ds = lds_mov.getitemnumber(ll_i, "prog_stock")
	
	st_legenda.text = "Elaborazione Movimento: "+string(ll_anno_mov)+"/"+string(ll_num_mov)
	
	//vedo se nella stock tale prodotto ha lo stock corretto
	setnull(ll_count)
	select count(*)
	into :ll_count
	from stock
	where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_ds and
		cod_deposito=:ls_cod_deposito_OK and cod_ubicazione=:ls_cod_ubicazione_OK and
		cod_lotto=:ls_cod_lotto_OK and data_stock=:ldt_data_stock_OK and prog_stock=:ll_prog_stock_OK;
		
	if sqlca.sqlcode<0 then
		g_mb.messagebox("CSTEAM","Errore durante la lettura se esiste lo stock corretto!"+sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if
	
	if ll_count>0 then
		//lo stock corretto esiste
	else
		//stock inesistente: inserirlo
		insert into stock
			(cod_azienda,cod_prodotto,cod_deposito,cod_ubicazione,
			 cod_lotto,data_stock,prog_stock,giacenza_stock)
		 values
			(:s_cs_xx.cod_azienda,:ls_cod_prodotto_ds,:ls_cod_deposito_OK,:ls_cod_ubicazione_OK,
			 :ls_cod_lotto_OK,:ldt_data_stock_OK,:ll_prog_stock_OK,0);
			 
		if sqlca.sqlcode<0 then
			g_mb.messagebox("CSTEAM","Errore durante l'inserimento dello stock corretto! "+&
										"Prodotto:"+ls_cod_prodotto_ds+" Mov.:"+string(ll_anno_mov)+"/"+string(ll_num_mov)+&
										sqlca.sqlerrtext,StopSign!)
			rollback;
			return
		end if
		
		ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
		filewrite(ll_filelog, "Inserito stock per prodotto "+ls_cod_prodotto_ds+" - Movimento "+string(ll_anno_mov)+"/"+string(ll_num_mov))
		fileclose(ll_filelog)

	end if
	
	//aggiorno lo stock nel movimento
	update mov_magazzino
	set cod_deposito=:ls_cod_deposito_OK,
		cod_ubicazione=:ls_cod_ubicazione_OK,
		cod_lotto=:ls_cod_lotto_OK,
		data_stock=:ldt_data_stock_OK,
		prog_stock=:ll_prog_stock_OK
	where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:ll_anno_mov and num_registrazione=:ll_num_mov;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("CSTEAM","Errore durante l'aggiornamento dello stock corretto nel movimento! "+&
										"Prodotto:"+ls_cod_prodotto_ds+" Mov.:"+string(ll_anno_mov)+"/"+string(ll_num_mov)+&
										sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if
	
	ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
	filewrite(ll_filelog, "Aggiornato stock del prodotto "+ls_cod_prodotto_ds+" per il movimento "+string(ll_anno_mov)+"/"+string(ll_num_mov))
	fileclose(ll_filelog)
	
	//aggiorno le tabelle dei documenti (bolle e fatture di vendita e acquisto)
	if wf_aggiorna_doc(ll_anno_mov, ll_num_mov, &
								ls_cod_prodotto_ds, ls_cod_deposito_OK, &
								ls_cod_ubicazione_OK, ls_cod_lotto_OK, &
								ldt_data_stock_OK, ll_prog_stock_OK) = -1 then
	
		//msg di errore già dato
		rollback;
		
		return
	end if	
next

//se arrivi fin qui fai la commit;
commit;
g_mb.messagebox("CSTEAM","Operazione terminata!",StopSign!)

return
end event

type st_4 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 64
integer y = 752
integer width = 1769
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "3. Verifica gli stock per i movimenti del 2010 che erano pre-esistenti"
boolean focusrectangle = false
end type

type st_3 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 59
integer y = 628
integer width = 2615
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "2. Cancellazione movimenti precedenti al 01/01/2010: vedi w_elimina_reg_mov_mag (progetto tenda)"
boolean focusrectangle = false
end type

type st_1 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 59
integer y = 416
integer width = 942
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "1. creazione movimenti INI"
boolean focusrectangle = false
end type

type st_12 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 923
integer y = 488
integer width = 1358
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "<< esegue mov. INI su giacenze specificate da file"
boolean focusrectangle = false
end type

type st_11 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 421
integer y = 488
integer width = 91
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "al:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_ini from editmask within w_aggiusta_mag_condeposito_cgibus
integer x = 535
integer y = 488
integer width = 375
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "01/01/2010"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type cb_ini from commandbutton within w_aggiusta_mag_condeposito_cgibus
integer x = 55
integer y = 488
integer width = 343
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "INI"
end type

event clicked;string				ls_cod_prodotto,ls_cod_tipo_mov_scarico_pf,ls_referenza, ls_cod_deposito[], &
						ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[],ls_stringa, &
						ls_cod_tipo_movimento,ls_chiave[],ls_where, ls_errore, ls_des_prodotto
						
long					ll_i, ll_rows, ll_filelog, ll_prog_stock[], ll_anno_reg_mov[], ll_num_reg_mov[], &
						ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_file,ll_err,ll_pos,ll_y, ll_index, &
						ll_mese_in_esame, ll_t, ll_anno_reg_mov_rag[], ll_num_reg_mov_rag[]
						
dec{4}				ld_quantita,ld_giacenza[12], ld_quant_val[],ld_giacenza_stock[], ld_costo_medio_stock[],&
						ld_quan_costo_medio_stock[], ld_mingiacenza, ld_quan_mensile, ld_quan_cumulata,ld_quan_movimento, &
						ld_quantita_scarico,ld_quan_riporto, ld_quan_periodo, ld_fat_conversione_rag_mag
						
datetime			ldt_inizio, ldt_fine[],ldt_data_reg_mov,ldt_data_stock[]

uo_magazzino	luo_mag

long					ll_num_stock, ll_indice, ll_line

string				ls_cod_deposito_corrente, ls_cod_prodotto_old, ls_des_prodotto_old, ls_fileOK, ls_fileERR, &
					ls_cod_prod_raggruppo, ls_appo, ls_cod_prodotto_mov_mag

boolean			lb_elabora

st_legenda.text = ""

ls_fileERR = em_log.text+"log_err.txt"
filedelete(ls_fileERR)

//il file da leggere deve essere del tipo
//prodotto;descrizione;quantita

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ls_fileOK = em_log.text+"log_ok.txt"
filedelete(ls_fileOK)

ls_cod_tipo_movimento = "INI"

////Donato 05/01/2009 leggo il deposito da elaborare
//ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
//if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
//	g_mb.messagebox("APICE", "Specificare un Deposito!")
//	return
//end if
//
//select count(*)
//into :ll_indice
//from anag_depositi
//where cod_azienda=:s_cs_xx.cod_azienda and cod_deposito=:ls_cod_deposito_corrente;
//
//if isnull(ll_indice) then ll_indice = 0
//
//if sqlca.sqlcode <> 0 or ll_indice = 0 then
//	g_mb.messagebox("APICE", "Deposito non trovato oppure si è verificato un errore durante la lettura del deposito!!")
//	return
//end if

setpointer(HourGlass!)

ll_line = 0

ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
filewrite(ll_filelog, "LOG ERRORI")
filewrite(ll_filelog, "--------------------------------------------------------")
fileclose(ll_filelog)

ll_filelog = fileopen(ls_fileOK, LINEMODE!, WRITE!)
filewrite(ll_filelog, "LOG MOVIMENTI 'INI'")
filewrite(ll_filelog, "---------------------------------------------------------")
fileclose(ll_filelog)

do while true
	ll_line += 1
	
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	//leggi il codice -------------
	ll_pos = Pos( ls_stringa, ";", 1)
	ls_cod_prodotto = Left( ls_stringa, ll_pos - 1)		
	
	//leggi la descrizione --------------------
	ls_stringa = Mid( ls_stringa, ll_pos + 1)
	ll_pos = Pos( ls_stringa, ";", 1 )
	ls_des_prodotto = Left( ls_stringa, ll_pos - 1)
	
	if ls_cod_prodotto = "" then
		//codice VUOTO ############################################################################
		
		//controlla la descrizione
		if ls_des_prodotto = "" then
			//possibile prodotto precedente: elabora solo se il semaforo è ON ---------------------------
			
			if lb_elabora then
				//OK: elabora solo in presenza di qta valida e considera il codice salvato 				
				//leggi la quantità
				setnull(ld_quantita)
				ls_stringa = Mid( ls_stringa, ll_pos + 1)
				
				//in alcuni casi potrei trovare il carattere ";" alla fine della riga
				//rimuovere eventuale ";" alla fine della stringa
				ll_pos = Pos( ls_stringa, ";", 1 )
				if ll_pos > 0 then
					ls_stringa = left(ls_stringa, ll_pos - 1)			
				end if
				
				if ls_stringa <> "" and isnumber(ls_stringa) then
					ld_quantita = dec(ls_stringa)
					
					ls_cod_prodotto = ls_cod_prodotto_old	//devo elaborare l'ultimo codice valido
				else
					ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
					filewrite(ll_filelog, "linea "+string(ll_line)+": prodotto senza quantità o quantità invalida")
					fileclose(ll_filelog)
					continue
				end if				
			else
				//il semaforo è OFF: non posso elaborare perchè trattasi di prodotto senza codice
				ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
				filewrite(ll_filelog, "linea "+string(ll_line)+": prodotto senza codice")
				fileclose(ll_filelog)
				continue
			end if
			
		else
			//descizione non vuota: non posso elaborare perchè trattasi di prodotto senza codice -------------------------------
			
			//spengo il semaforo
			lb_elabora = false
			
			//non posso elaborare perchè trattasi di prodotto con descrizione ma senza codice
			ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
			filewrite(ll_filelog, "linea "+string(ll_line)+": prodotto '"+ls_des_prodotto+"' senza codice")
			fileclose(ll_filelog)
			continue
			//--------------------------------------------------------------------------------------------------------------------------------------------------
		end if
		
		//#########################################################################################
	else
		//codice non VUOTO: elabora se qta è numerico ###################################################
		
		//semaforo ON
		lb_elabora = true
		
		//salvo il codice
		ls_cod_prodotto_old = ls_cod_prodotto
		
		//leggi la quantità
		setnull(ld_quantita)
		ls_stringa = Mid( ls_stringa, ll_pos + 1)
		
		//in alcuni casi potrei trovare il carattere ";" alla fine della riga
		//rimuovere eventuale ";" alla fine della stringa
		ll_pos = Pos( ls_stringa, ";", 1 )
		if ll_pos > 0 then
			ls_stringa = left(ls_stringa, ll_pos - 1)			
		end if
		
		if ls_stringa <> "" and isnumber(ls_stringa) then
			ld_quantita = dec(ls_stringa)
		else
			ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
			filewrite(ll_filelog, "linea "+string(ll_line)+": prodotto senza quantità o quantità invalida")
			fileclose(ll_filelog)
			continue
		end if
		//#########################################################################################
	end if
	
	//controlla se il prodotto esiste ###################
	select count(*)
	into :ll_filelog
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and
		cod_prodotto=:ls_cod_prodotto;
		
	if sqlca.sqlcode = 0 and ll_filelog=1 then
	else
		//prima di scrivere che non esiste controlla se ci sono spazi all'interno, rimuovili e poi riprova
		ls_appo = ls_cod_prodotto
		wf_remove_space(ls_appo)
		
		select count(*)
		into :ll_filelog
		from anag_prodotti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_appo;
		
		if sqlca.sqlcode = 0 and ll_filelog=1 then						
			ll_filelog = fileopen(ls_fileOK, LINEMODE!, WRITE!)
			filewrite(ll_filelog, "-----------------------------------------------")
			filewrite(ll_filelog, "linea:"+string(ll_line) + "  E' stato elaborato il Prodotto: '"+ls_appo+"' al posto di '"+ls_cod_prodotto+"' (rimozione spazi)")
			filewrite(ll_filelog, "-----------------------------------------------")
			fileclose(ll_filelog)
			
			ls_cod_prodotto = ls_appo
		else
			ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
			filewrite(ll_filelog, "linea "+string(ll_line)+": prodotto '"+ls_cod_prodotto+"' inesistente")
			fileclose(ll_filelog)
			continue
		end if		
	end if
	
	//controlla se esiste il prodotto raggruppato
	//stefanop: spostato sotto pechè eseguo due movimenti
//	ls_cod_prod_raggruppo = ""
//	setnull(ls_cod_prodotto_mov_mag)
//	
//	select cod_prodotto_raggruppato, fat_conversione_rag_mag
//	into :ls_cod_prod_raggruppo, :ld_fat_conversione_rag_mag
//	from anag_prodotti
//	where cod_azienda=:s_cs_xx.cod_azienda and
//		cod_prodotto=:ls_cod_prodotto;
//		
//	if sqlca.sqlcode=0 and not isnull(ls_cod_prod_raggruppo) and ls_cod_prod_raggruppo<>"" then
//		//devo movimentare il cod prodotto raggruppato al posto del cod prodotto
//		ls_cod_prodotto = ls_cod_prod_raggruppo
//		ld_quantita = ld_quantita * ld_fat_conversione_rag_mag
//	else
//		ls_cod_prod_raggruppo = ""
//	end if
// ----------------------------------------------------------------------
	
	//log se tutto ok un altro file
//	ll_filelog = fileopen(ls_fileOK, LINEMODE!, WRITE!)
//	filewrite(ll_filelog, "linea:"+string(ll_line) + "  OK per Prodotto: "+ls_cod_prodotto+"  q.tà:"+string(ld_quantita) + "  Raggruppo:"+ls_cod_prod_raggruppo)
//	fileclose(ll_filelog)
	
	st_legenda.text = ls_cod_prodotto + " linea: "+string(ll_line)
	
	
	//in ld_quantita c'è la quantita letta da file
	ld_quan_movimento = ld_quantita
	
	ldt_data_reg_mov = datetime(date(em_data_ini.text), 00:00:00) 
	ls_referenza   = ""
	
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	//stock corretto
	ls_cod_deposito[1] = "001"
	ls_cod_ubicazione[1] = "UB001"
	ls_cod_lotto[1] = "LT001"
	ldt_data_stock[1] = datetime(date(string("01/01/2000")),00:00:00)
	ll_prog_stock[1] = 10
	
	luo_mag = create uo_magazzino
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
												
		g_mb.messagebox("APICE", "Errore: f_crea_dest_mov_magazzino")
		ROLLBACK;
		return
	end if
		
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
										 
		g_mb.messagebox("APICE", "Errore: f_verifica_dest_mov_mag")
		ROLLBACK;
		return
	end if
		
	if luo_mag.uof_movimenti_mag ( ldt_data_reg_mov, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quan_movimento, &
								0, &
								0, &
								ldt_data_reg_mov, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ref ll_anno_reg_mov[], &
								ref ll_num_reg_mov[] ) = 0 then
		//COMMIT;
			
//		ls_stringa = ls_cod_prodotto + " - INI - Reg. " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " Data Reg " + string(ldt_data_reg_mov,"dd/mm/yyyy")
//		ll_filelog = fileopen(ls_fileOK, LINEMODE!, WRITE!)
//		filewrite(ll_filelog, ls_stringa)
//		fileclose(ll_filelog)
		
		ll_filelog = fileopen(ls_fileOK, LINEMODE!, WRITE!)
		filewrite(ll_filelog, "Prodotto: "+ls_cod_prodotto+"  q.tà:"+string(ld_quan_movimento) + "  Mov:"+string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " Data Reg " + string(ldt_data_reg_mov,"dd/mm/yyyy"))
		fileclose(ll_filelog)
		
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock) = -1 then
			ROLLBACK; // rollback della sola eliminazione dest_mov_magazzino
		end if
		
		// stefanop 06/12/2011: movimento anche il prodotto raggruppato
		if luo_mag.uof_movimenti_mag_ragguppato(ldt_data_reg_mov, &
									ls_cod_tipo_movimento, &
									"S", &
									ls_cod_prodotto, &
									ld_quan_movimento, &
									0, &
									0, &
									ldt_data_reg_mov, &
									ls_referenza, &
									ll_anno_reg_dest_stock, &
									ll_num_reg_dest_stock, &
									ls_cod_deposito[], &
									ls_cod_ubicazione[], &
									ls_cod_lotto[], &
									ldt_data_stock[], &
									ll_prog_stock[], &
									ls_cod_fornitore[], &
									ls_cod_cliente[], &
									ll_anno_reg_mov_rag[], &
									ll_num_reg_mov_rag[],&
									long(ls_referenza),&
									ll_anno_reg_mov[1],&
									ll_num_reg_mov[1]) = -1 then
									
			ROLLBACK;
			destroy luo_mag
			return -1
		end if					
		// ----
		//COMMIT;
	else
		ROLLBACK;
			
		ls_stringa = "prodotto: "+ls_cod_prodotto+"  q.tà:"+string(ld_quan_movimento) + " - " + " uof_movimenti_mag-> movimento non eseguito a causa di un errore."
		ll_filelog = fileopen(ls_fileERR, LINEMODE!, WRITE!)
		filewrite(ll_filelog, ls_stringa)
		fileclose(ll_filelog)
		
		return
	end if
	
	destroy luo_mag
loop

FILECLOSE(ll_file)

//arrivi finqui fai COMMIT
COMMIT;

setpointer(Arrow!)

RUN("notepad.exe " + ls_fileERR)
RUN("notepad.exe " + ls_fileOK)
end event

type dw_selezione from uo_cs_xx_dw within w_aggiusta_mag_condeposito_cgibus
integer x = 114
integer y = 252
integer width = 1669
integer height = 120
integer taborder = 40
string dataobject = "d_ripara_mag_sel_deposito"
boolean border = false
end type

type st_legenda from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 37
integer y = 1288
integer width = 2706
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_8 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 2048
integer y = 256
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "ANNO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_anno from editmask within w_aggiusta_mag_condeposito_cgibus
integer x = 2272
integer y = 244
integer width = 251
integer height = 92
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
end type

type st_2 from statictext within w_aggiusta_mag_condeposito_cgibus
integer x = 114
integer y = 156
integer width = 279
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "LOG:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_log from editmask within w_aggiusta_mag_condeposito_cgibus
integer x = 402
integer y = 144
integer width = 2126
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "C:\cs_clien\centro_gibus\MAGAZZINO\IMPORT\"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_file from editmask within w_aggiusta_mag_condeposito_cgibus
integer x = 402
integer y = 28
integer width = 2126
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_apri from commandbutton within w_aggiusta_mag_condeposito_cgibus
integer x = 18
integer y = 28
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Apri"
end type

event clicked;string docpath, docname[]

integer i, li_cnt, li_rtn, li_filenum

 

li_rtn = GetFileOpenName( "Seleziona File Scarichi", docpath, docname[])

 

em_file.text = ""

IF li_rtn < 1 THEN return

li_cnt = Upperbound(docname)

if li_cnt = 1 then

   em_file.text = string(docpath)

end if


end event

