﻿$PBExportHeader$w_aggiusta_magazzino_con_deposito.srw
forward
global type w_aggiusta_magazzino_con_deposito from w_cs_xx_principale
end type
type cb_due_carichi from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type st_log_due_carichi from statictext within w_aggiusta_magazzino_con_deposito
end type
type sle_causale_carico from singlelineedit within w_aggiusta_magazzino_con_deposito
end type
type st_14 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_secondo_carico from editmask within w_aggiusta_magazzino_con_deposito
end type
type em_primo_carico from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_13 from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_12 from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_11 from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_10 from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_9 from statictext within w_aggiusta_magazzino_con_deposito
end type
type sle_cod_ubz_terzista from singlelineedit within w_aggiusta_magazzino_con_deposito
end type
type em_data_ini from editmask within w_aggiusta_magazzino_con_deposito
end type
type cb_6 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type dw_selezione from uo_cs_xx_dw within w_aggiusta_magazzino_con_deposito
end type
type st_legenda from statictext within w_aggiusta_magazzino_con_deposito
end type
type dw_percentuali from datawindow within w_aggiusta_magazzino_con_deposito
end type
type st_scarico from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_carico from statictext within w_aggiusta_magazzino_con_deposito
end type
type cb_5 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type st_8 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_anno from editmask within w_aggiusta_magazzino_con_deposito
end type
type em_data_fine from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_7 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_data_inizio from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_6 from statictext within w_aggiusta_magazzino_con_deposito
end type
type cb_4 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type cb_3 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type cbx_giacenza from checkbox within w_aggiusta_magazzino_con_deposito
end type
type st_5 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_mov_scarichi from editmask within w_aggiusta_magazzino_con_deposito
end type
type cb_2 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type em_fine from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_4 from statictext within w_aggiusta_magazzino_con_deposito
end type
type st_3 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_inizio from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_2 from statictext within w_aggiusta_magazzino_con_deposito
end type
type em_log from editmask within w_aggiusta_magazzino_con_deposito
end type
type st_1 from statictext within w_aggiusta_magazzino_con_deposito
end type
type cb_non_collegati from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type em_file from editmask within w_aggiusta_magazzino_con_deposito
end type
type cb_1 from commandbutton within w_aggiusta_magazzino_con_deposito
end type
type ln_1 from line within w_aggiusta_magazzino_con_deposito
end type
type ln_2 from line within w_aggiusta_magazzino_con_deposito
end type
type r_1 from rectangle within w_aggiusta_magazzino_con_deposito
end type
end forward

global type w_aggiusta_magazzino_con_deposito from w_cs_xx_principale
integer width = 2766
integer height = 2084
string title = "Aggiustamenti di Magazzino -:)"
event ue_salva ( )
cb_due_carichi cb_due_carichi
st_log_due_carichi st_log_due_carichi
sle_causale_carico sle_causale_carico
st_14 st_14
em_secondo_carico em_secondo_carico
em_primo_carico em_primo_carico
st_13 st_13
st_12 st_12
st_11 st_11
st_10 st_10
st_9 st_9
sle_cod_ubz_terzista sle_cod_ubz_terzista
em_data_ini em_data_ini
cb_6 cb_6
dw_selezione dw_selezione
st_legenda st_legenda
dw_percentuali dw_percentuali
st_scarico st_scarico
st_carico st_carico
cb_5 cb_5
st_8 st_8
em_anno em_anno
em_data_fine em_data_fine
st_7 st_7
em_data_inizio em_data_inizio
st_6 st_6
cb_4 cb_4
cb_3 cb_3
cbx_giacenza cbx_giacenza
st_5 st_5
em_mov_scarichi em_mov_scarichi
cb_2 cb_2
em_fine em_fine
st_4 st_4
st_3 st_3
em_inizio em_inizio
st_2 st_2
em_log em_log
st_1 st_1
cb_non_collegati cb_non_collegati
em_file em_file
cb_1 cb_1
ln_1 ln_1
ln_2 ln_2
r_1 r_1
end type
global w_aggiusta_magazzino_con_deposito w_aggiusta_magazzino_con_deposito

forward prototypes
public function boolean wf_bisestile (long fl_anno)
end prototypes

event ue_salva();integer li_FileNum

string  ls_data_inizio, ls_data_fine, ls_stringa, ls_path_file
dec{2}	  ld_1

ls_data_inizio = em_data_inizio.text
ls_data_fine = em_data_fine.text

dw_percentuali.accepttext()

if dw_percentuali.rowcount() > 0 then
	
	ls_stringa = ls_data_inizio + "~t" + ls_data_fine + "~t" 
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_1")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_2")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_3")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_4")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_5")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_6")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_7")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_8")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_9")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"

	ld_1 = dw_percentuali.getitemnumber( 1, "perc_10")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"

	ld_1 = dw_percentuali.getitemnumber( 1, "perc_11")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1) + "~t"
	
	ld_1 = dw_percentuali.getitemnumber( 1, "perc_12")
	if isnull(ld_1) then ld_1 = 0
	ls_stringa += string(ld_1)
	
end if

select stringa
into   :ls_path_file
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'AMP';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante la lettura del file delle percentuali:" + sqlca.sqlerrtext)
else
	if not isnull(ls_stringa) and ls_stringa <> "" then	
		li_filenum = fileopen( ls_path_file,LineMode!, Write!, LockWrite!, replace!)
		FileWrite(li_FileNum, ls_stringa)
		fileclose( li_filenum)
	end if
end if
end event

public function boolean wf_bisestile (long fl_anno);if mod(fl_anno, 4) = 0 then
	//divisibile per 4: altri controlli da fare
	if mod(fl_anno, 100) = 0 then
		//divisibile per 100, inizio secolo: altri controlli da fare
		if mod(fl_anno, 400) = 0 then
			//divisibile per 400: BISESTILE
			return true
		else
			//non divisibile per 400: NON BISESTILE
		end if
	else
		//non divisibile per 100, non inizio secolo: BISESTILE
		return true
	end if	
else
	//non divisibile per 4: NON BISESTILE
	return false
end if
end function

on w_aggiusta_magazzino_con_deposito.create
int iCurrent
call super::create
this.cb_due_carichi=create cb_due_carichi
this.st_log_due_carichi=create st_log_due_carichi
this.sle_causale_carico=create sle_causale_carico
this.st_14=create st_14
this.em_secondo_carico=create em_secondo_carico
this.em_primo_carico=create em_primo_carico
this.st_13=create st_13
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.sle_cod_ubz_terzista=create sle_cod_ubz_terzista
this.em_data_ini=create em_data_ini
this.cb_6=create cb_6
this.dw_selezione=create dw_selezione
this.st_legenda=create st_legenda
this.dw_percentuali=create dw_percentuali
this.st_scarico=create st_scarico
this.st_carico=create st_carico
this.cb_5=create cb_5
this.st_8=create st_8
this.em_anno=create em_anno
this.em_data_fine=create em_data_fine
this.st_7=create st_7
this.em_data_inizio=create em_data_inizio
this.st_6=create st_6
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cbx_giacenza=create cbx_giacenza
this.st_5=create st_5
this.em_mov_scarichi=create em_mov_scarichi
this.cb_2=create cb_2
this.em_fine=create em_fine
this.st_4=create st_4
this.st_3=create st_3
this.em_inizio=create em_inizio
this.st_2=create st_2
this.em_log=create em_log
this.st_1=create st_1
this.cb_non_collegati=create cb_non_collegati
this.em_file=create em_file
this.cb_1=create cb_1
this.ln_1=create ln_1
this.ln_2=create ln_2
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_due_carichi
this.Control[iCurrent+2]=this.st_log_due_carichi
this.Control[iCurrent+3]=this.sle_causale_carico
this.Control[iCurrent+4]=this.st_14
this.Control[iCurrent+5]=this.em_secondo_carico
this.Control[iCurrent+6]=this.em_primo_carico
this.Control[iCurrent+7]=this.st_13
this.Control[iCurrent+8]=this.st_12
this.Control[iCurrent+9]=this.st_11
this.Control[iCurrent+10]=this.st_10
this.Control[iCurrent+11]=this.st_9
this.Control[iCurrent+12]=this.sle_cod_ubz_terzista
this.Control[iCurrent+13]=this.em_data_ini
this.Control[iCurrent+14]=this.cb_6
this.Control[iCurrent+15]=this.dw_selezione
this.Control[iCurrent+16]=this.st_legenda
this.Control[iCurrent+17]=this.dw_percentuali
this.Control[iCurrent+18]=this.st_scarico
this.Control[iCurrent+19]=this.st_carico
this.Control[iCurrent+20]=this.cb_5
this.Control[iCurrent+21]=this.st_8
this.Control[iCurrent+22]=this.em_anno
this.Control[iCurrent+23]=this.em_data_fine
this.Control[iCurrent+24]=this.st_7
this.Control[iCurrent+25]=this.em_data_inizio
this.Control[iCurrent+26]=this.st_6
this.Control[iCurrent+27]=this.cb_4
this.Control[iCurrent+28]=this.cb_3
this.Control[iCurrent+29]=this.cbx_giacenza
this.Control[iCurrent+30]=this.st_5
this.Control[iCurrent+31]=this.em_mov_scarichi
this.Control[iCurrent+32]=this.cb_2
this.Control[iCurrent+33]=this.em_fine
this.Control[iCurrent+34]=this.st_4
this.Control[iCurrent+35]=this.st_3
this.Control[iCurrent+36]=this.em_inizio
this.Control[iCurrent+37]=this.st_2
this.Control[iCurrent+38]=this.em_log
this.Control[iCurrent+39]=this.st_1
this.Control[iCurrent+40]=this.cb_non_collegati
this.Control[iCurrent+41]=this.em_file
this.Control[iCurrent+42]=this.cb_1
this.Control[iCurrent+43]=this.ln_1
this.Control[iCurrent+44]=this.ln_2
this.Control[iCurrent+45]=this.r_1
end on

on w_aggiusta_magazzino_con_deposito.destroy
call super::destroy
destroy(this.cb_due_carichi)
destroy(this.st_log_due_carichi)
destroy(this.sle_causale_carico)
destroy(this.st_14)
destroy(this.em_secondo_carico)
destroy(this.em_primo_carico)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.sle_cod_ubz_terzista)
destroy(this.em_data_ini)
destroy(this.cb_6)
destroy(this.dw_selezione)
destroy(this.st_legenda)
destroy(this.dw_percentuali)
destroy(this.st_scarico)
destroy(this.st_carico)
destroy(this.cb_5)
destroy(this.st_8)
destroy(this.em_anno)
destroy(this.em_data_fine)
destroy(this.st_7)
destroy(this.em_data_inizio)
destroy(this.st_6)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cbx_giacenza)
destroy(this.st_5)
destroy(this.em_mov_scarichi)
destroy(this.cb_2)
destroy(this.em_fine)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.em_inizio)
destroy(this.st_2)
destroy(this.em_log)
destroy(this.st_1)
destroy(this.cb_non_collegati)
destroy(this.em_file)
destroy(this.cb_1)
destroy(this.ln_1)
destroy(this.ln_2)
destroy(this.r_1)
end on

event pc_setwindow;call super::pc_setwindow;em_anno.text = string(f_anno_esercizio())


dw_percentuali.settransobject( sqlca)

dw_percentuali.postevent("ue_new")

dw_selezione.insertrow(0)
end event

event close;call super::close;long	ll_ret

ll_ret = g_mb.messagebox( "APICE", "Vuoi salvare le date e le percentuali inserite?", Exclamation!, YesNo!, 2)

IF ll_ret = 1 THEN
	triggerevent("ue_salva")
END IF
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW( dw_selezione, &
//							"cod_deposito", &
//							sqlca, &
//							"anag_depositi", &
//							"cod_deposito", &
//                     "des_deposito", &
//							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW( dw_selezione, &
							"cod_deposito", &
							sqlca, &
							"anag_depositi", &
							"cod_deposito", &
                     "des_deposito", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type cb_due_carichi from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 2039
integer y = 1736
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PROCEDI"
end type

event clicked;



string					ls_cod_deposito_corrente, ls_cod_tipo_movimento, ls_stringa, ls_cod_deposito[], ls_cod_ubicazione[], &
						ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_cod_prodotto,ls_referenza
						
datetime				ldt_primo, ldt_secondo, ldt_data_stock[]

long					ll_file, ll_log, ll_indice, ll_pos, ll_pos2, ll_err, ll_prog_stock[],ll_anno_reg_dest_stock, ll_num_reg_dest_stock, &
						ll_anno_reg_mov[], ll_num_reg_mov[]

decimal				ld_quantita, ld_quantita_carico

uo_magazzino		luo_mag


//-------------------------------------------------------------------------------------
ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
	g_mb.error("APICE", "Specificare un Deposito!")
	return
end if

select count(*)
into :ll_indice
from anag_depositi
where cod_azienda=:s_cs_xx.cod_azienda and 
		 cod_deposito=:ls_cod_deposito_corrente;

if isnull(ll_indice) then ll_indice = 0

if sqlca.sqlcode <> 0 or ll_indice = 0 then
	g_mb.error("APICE", "Deposito non trovato oppure si è verificato un errore durante la lettura del deposito!")
	return
end if

//-------------------------------------------------------------------------------------
ldt_primo = datetime(date(em_primo_carico.text), 00:00:00) 
ldt_secondo   = datetime(date(em_secondo_carico.text), 00:00:00) 

if isnull(ldt_primo) or year(date(ldt_primo))<1950 then
	g_mb.error("APICE", "Specificare la data del primo carico!")
	return
end if
if isnull(ldt_secondo) or year(date(ldt_secondo))<1950 then
	g_mb.error("APICE", "Specificare la data del secondo carico!")
	return
end if

//-------------------------------------------------------------------------------------
ls_cod_tipo_movimento = sle_causale_carico.text

if isnull(ls_cod_tipo_movimento) or ls_cod_tipo_movimento="" then
	g_mb.error("APICE", "Specificare la causale di carico!")
	return
end if

st_log_due_carichi.text = "Inizio elaborazione ..."

//ripulisci il log
filedelete(em_LOG.text)

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.error("APICE", "Errore in OPEN del File da leggere: "+em_file.text)
	return
end if

setpointer(HourGlass!)

luo_mag = create uo_magazzino

do while true
	Yield()
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1, ll_pos - 1)
	
	st_log_due_carichi.text = "Elaborazione prodotto " + ls_cod_prodotto
	
	ll_pos += 1
	ll_pos2 = pos(ls_stringa, "~t", ll_pos)
	ls_stringa = mid(ls_stringa, ll_pos, ll_pos2 - ll_pos)
	
	//quantita da caricare
	ld_quantita = dec(ls_stringa)
	
	st_log_due_carichi.text = "Elaborazione prodotto " + ls_cod_prodotto + " e quantità " + string(ld_quantita, "########0.0000")
	
	//carico il 40% nella prima data ed il 60% nella seconda
	ld_quantita_carico = (ld_quantita / 100) * 40
	
	//########################################################################################
	// eseguo il primo movimento in ldt_primo ##############################################################
	ls_referenza   = "***"
	
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	ls_cod_deposito[1] = ls_cod_deposito_corrente
//	if sle_cod_ubz_terzista.text <> "" and not isnull(sle_cod_ubz_terzista.text) then
//		ls_cod_ubicazione[1] = sle_cod_ubz_terzista.text
//	end if
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		destroy luo_mag;
		g_mb.error("APICE", "Errore: f_crea_dest_mov_magazzino")
		rollback;
		return
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		destroy luo_mag;
		g_mb.error("APICE", "Errore: f_verifica_dest_mov_mag")
		rollback;
		return
	end if
	
	if luo_mag.uof_movimenti_mag ( ldt_primo, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quantita_carico, &
								0, &
								0, &
								ldt_primo, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ref ll_anno_reg_mov[], &
								ref ll_num_reg_mov[] ) = 0 then
		
		ls_stringa = "1. " + ls_cod_prodotto + " - " + ls_cod_tipo_movimento + " qta "+ string(ld_quantita_carico, "########0.0000") + &
						" - N° " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " del " + string(ldt_primo,"dd/mm/yyyy")
		ll_log = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_log, ls_stringa)
		fileclose(ll_log)
	
	else
		destroy luo_mag;
		rollback;
		g_mb.error(ls_cod_prodotto + ": movimento non eseguito a causa di un errore.")
		return
	end if
	
	//########################################################################################
	//carico il 60% nella seconda data
	ld_quantita_carico = (ld_quantita / 100) * 60
	
	ls_referenza   = "***"
	
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	ls_cod_deposito[1] = ls_cod_deposito_corrente
//	if sle_cod_ubz_terzista.text <> "" and not isnull(sle_cod_ubz_terzista.text) then
//		ls_cod_ubicazione[1] = sle_cod_ubz_terzista.text
//	end if
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		destroy luo_mag;
		g_mb.error("APICE", "Errore: f_crea_dest_mov_magazzino")
		rollback;
		return
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		destroy luo_mag;
		g_mb.error("APICE", "Errore: f_verifica_dest_mov_mag")
		rollback;
		return
	end if
	
	if luo_mag.uof_movimenti_mag ( ldt_secondo, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quantita_carico, &
								0, &
								0, &
								ldt_secondo, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ref ll_anno_reg_mov[], &
								ref ll_num_reg_mov[] ) = 0 then
		
		ls_stringa = "2. "+ls_cod_prodotto + " - " + ls_cod_tipo_movimento + " qta "+ string(ld_quantita_carico, "########0.0000") + &
						" - N° " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " del " + string(ldt_secondo,"dd/mm/yyyy")
		ll_log = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_log, ls_stringa)
		fileclose(ll_log)
	
	else
		destroy luo_mag;
		rollback;
		g_mb.error(ls_cod_prodotto + ": movimento non eseguito a causa di un errore.")
		return
	end if
	
loop

destroy luo_mag;

//arrivi finqui e fai commit
commit;

st_log_due_carichi.text = "Fine elaborazione ..."

fileclose(ll_file)
setpointer(Arrow!)

run("notepad.exe " + em_LOG.text)
end event

type st_log_due_carichi from statictext within w_aggiusta_magazzino_con_deposito
integer x = 101
integer y = 1856
integer width = 2565
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type sle_causale_carico from singlelineedit within w_aggiusta_magazzino_con_deposito
integer x = 1614
integer y = 1732
integer width = 343
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "CMA"
borderstyle borderstyle = stylelowered!
end type

type st_14 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 1303
integer y = 1744
integer width = 288
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOV:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_secondo_carico from editmask within w_aggiusta_magazzino_con_deposito
integer x = 699
integer y = 1732
integer width = 494
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "22/04/2010"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type em_primo_carico from editmask within w_aggiusta_magazzino_con_deposito
integer x = 101
integer y = 1740
integer width = 494
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "04/02/2010"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type st_13 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 82
integer y = 1656
integer width = 1339
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Creazione Movimenti Carico in due date diverse"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_12 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 914
integer y = 580
integer width = 1600
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "<< esegue mov. INI su giacenze specificate da file"
boolean focusrectangle = false
end type

type st_11 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 411
integer y = 580
integer width = 91
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "al:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_10 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 846
integer y = 480
integer width = 1600
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "<< da utilizzare solo in caso di errore e se diverso da UB0001"
boolean focusrectangle = false
end type

type st_9 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 69
integer y = 480
integer width = 325
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ubicazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_cod_ubz_terzista from singlelineedit within w_aggiusta_magazzino_con_deposito
integer x = 411
integer y = 460
integer width = 402
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type em_data_ini from editmask within w_aggiusta_magazzino_con_deposito
integer x = 526
integer y = 580
integer width = 375
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type cb_6 from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 46
integer y = 580
integer width = 343
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "INI"
end type

event clicked;string ls_cod_prodotto,ls_cod_tipo_mov_scarico_pf,ls_referenza, ls_cod_deposito[], &
       ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[],ls_stringa, &
		 ls_cod_tipo_movimento,ls_chiave[],ls_where, ls_errore
long	ll_i, ll_rows, ll_filelog, ll_prog_stock[], ll_anno_reg_mov[], ll_num_reg_mov[], &
      ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_file,ll_err,ll_pos,ll_y, ll_index, &
		ll_mese_in_esame, ll_t
dec{4} ld_quantita,ld_giacenza[12], ld_quant_val[],ld_giacenza_stock[], ld_costo_medio_stock[],&
		ld_quan_costo_medio_stock[], ld_mingiacenza, ld_quan_mensile, ld_quan_cumulata,ld_quan_movimento, &
		ld_quantita_scarico,ld_quan_riporto, ld_quan_periodo
datetime ldt_inizio, ldt_fine[],ldt_data_reg_mov,ldt_data_stock[]
uo_magazzino luo_mag
long ll_num_stock, ll_indice
string ls_cod_deposito_corrente

st_legenda.text = ""

g_mb.messagebox("APICE", "Procedo? Hai controllato la data?")

filedelete(em_LOG.text)

//il file da leggere deve essere del tipo
//prodotto<TAB>quantita

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ls_cod_tipo_movimento = "INI"

////Donato 05/01/2009 leggo il deposito da elaborare
//ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
//if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
//	g_mb.messagebox("APICE", "Specificare un Deposito!")
//	return
//end if
//
//select count(*)
//into :ll_indice
//from anag_depositi
//where cod_azienda=:s_cs_xx.cod_azienda and cod_deposito=:ls_cod_deposito_corrente;
//
//if isnull(ll_indice) then ll_indice = 0
//
//if sqlca.sqlcode <> 0 or ll_indice = 0 then
//	g_mb.messagebox("APICE", "Deposito non trovato oppure si è verificato un errore durante la lettura del deposito!!")
//	return
//end if

setpointer(HourGlass!)

do while true
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	
	st_legenda.text = ls_cod_prodotto
	
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	
	//quantita da caricare per l'allineamento delle giacenze
	ld_quantita = dec(ls_stringa)
	
	ll_index = 1		// è il mese in esame
	ld_mingiacenza = 0
	
	//in ld_quantita c'è la quantita letta da file
	ld_quan_movimento = ld_quantita
	
	ldt_data_reg_mov = datetime(date(em_data_ini.text), 00:00:00) 
	ls_referenza   = ""
	
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	ls_cod_deposito[1] = "01"
	ls_cod_ubicazione[1] = "UB0001"
	ls_cod_lotto[1] = "LT0001"
	ldt_data_stock[1] = datetime(date(string("01/01/2000")),00:00:00)
	ll_prog_stock[1] = 10
	
	
	luo_mag = create uo_magazzino
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
												
		g_mb.messagebox("APICE", "Errore: f_crea_dest_mov_magazzino")
		ROLLBACK;
		return
	end if
		
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
										 
		g_mb.messagebox("APICE", "Errore: f_verifica_dest_mov_mag")
		ROLLBACK;
		return
	end if
		
	if luo_mag.uof_movimenti_mag ( ldt_data_reg_mov, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quan_movimento, &
								0, &
								0, &
								ldt_data_reg_mov, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ref ll_anno_reg_mov[], &
								ref ll_num_reg_mov[] ) = 0 then
		COMMIT;
			
		ls_stringa = ls_cod_prodotto + " - INI - Reg. " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " Data Reg " + string(ldt_data_reg_mov,"dd/mm/yyyy")
		ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_filelog, ls_stringa)
		fileclose(ll_filelog)
		
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock) = -1 then
			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
		end if
		COMMIT;
	else
		ROLLBACK;
			
		ls_stringa = ls_cod_prodotto + " - " + em_mov_scarichi.text + " uof_movimenti_mag-> movimento non eseguito a causa di un errore."
		ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_filelog, ls_stringa)
		fileclose(ll_filelog)
	end if
	

	
	destroy luo_mag
	
loop

FILECLOSE(ll_file)
setpointer(Arrow!)

RUN("notepad.exe " + em_LOG.text)
end event

type dw_selezione from uo_cs_xx_dw within w_aggiusta_magazzino_con_deposito
integer x = 114
integer y = 360
integer width = 1669
integer height = 120
integer taborder = 40
string dataobject = "d_ripara_mag_sel_deposito"
boolean border = false
end type

type st_legenda from statictext within w_aggiusta_magazzino_con_deposito
integer x = 46
integer y = 680
integer width = 2217
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_percentuali from datawindow within w_aggiusta_magazzino_con_deposito
event ue_new ( )
integer x = 23
integer y = 1020
integer width = 2103
integer height = 320
integer taborder = 70
string title = "none"
string dataobject = "d_aggiusta_magazzino_perc"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_new();long	  ll_riga, ll_i, ll_ret, ll_pos
string  ls_path_file, ls_stringa, ls_data_inizio, ls_data_fine
dec{2}  ld_perc_1, ld_perc_2, ld_perc_3, ld_perc_4, ld_perc_5, ld_perc_6, ld_perc_7, ld_perc_8, ld_perc_9, &
        ld_perc_10, ld_perc_11, ld_perc_12
integer li_FileNum

// 

select stringa
into   :ls_path_file
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'AMP';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "APICE", "Errore durante la lettura del file delle percentuali:" + sqlca.sqlerrtext)
else
	if not isnull(ls_path_file) and ls_path_file <> "" then
		
		li_FileNum = FileOpen( ls_path_file, StreamMode!)

		do while true
			
			ll_ret = fileread(li_FileNum, ls_stringa)
			if ll_ret = -100 then exit		// file elaborazione
			
			if ll_ret < 0 then				// errore interrompo
				g_mb.messagebox("APICE", "Errore in lettura file di testo")
				return
			end if
			 
			ll_pos = pos(ls_stringa, "~t")
			ls_data_inizio = mid(ls_stringa, 1,ll_pos -1)
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ls_data_fine = mid(ls_stringa, 1,ll_pos -1)
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_1 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_2 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_3 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_4 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_5 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)	
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_6 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)		
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_7 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_8 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_9 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_10 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)
			
			ll_pos = pos(ls_stringa, "~t")
			ld_perc_11 = dec(mid(ls_stringa, 1,ll_pos -1))
			ls_stringa = mid(ls_stringa, ll_pos + 1)	
			
			ld_perc_12 = dec(mid(ls_stringa, 1,ll_pos -1))				
	
		loop
		
		fileclose( li_filenum)
		
	end if
end if

ll_riga = this.insertrow( 0)

for ll_i = 1 to 12
	
	setitem( ll_riga, "perc_1", ld_perc_1)
	setitem( ll_riga, "perc_2", ld_perc_2)
	setitem( ll_riga, "perc_3", ld_perc_3)
	setitem( ll_riga, "perc_4", ld_perc_4)
	setitem( ll_riga, "perc_5", ld_perc_5)
	setitem( ll_riga, "perc_6", ld_perc_6)
	setitem( ll_riga, "perc_7", ld_perc_7)
	setitem( ll_riga, "perc_8", ld_perc_8)
	setitem( ll_riga, "perc_9", ld_perc_9)
	setitem( ll_riga, "perc_10", ld_perc_10)
	setitem( ll_riga, "perc_11", ld_perc_11)
	setitem( ll_riga, "perc_12", ld_perc_12)
	
next

if not isnull(ls_data_inizio) and ls_data_inizio <> "" then
	
	em_data_inizio.text = ls_data_inizio
	
end if

if not isnull(ls_data_fine) and ls_data_fine <> "" then
	
	em_data_fine.text = ls_data_fine
	
end if
end event

type st_scarico from statictext within w_aggiusta_magazzino_con_deposito
integer x = 416
integer y = 928
integer width = 727
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_carico from statictext within w_aggiusta_magazzino_con_deposito
integer x = 416
integer y = 824
integer width = 727
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_5 from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 23
integer y = 928
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "SCARICO"
end type

event clicked;string ls_stringa, ls_cod_prodotto, ls_where, ls_errore, ls_chiave[], ls_null

long ll_rows, ll_file, ll_ret, ll_pos, ll_i, ll_anno_registrazione, ll_num_registrazione, &
     ll_y,ll_err, ll_tot_mesi_verifica,ll_row, ll_z,ll_t, ll_log, ll_null, ll_x, &
	  LL_LOG1
	  
dec{4} ld_quan_rettifica,ld_quan_rettifica_movimento, ld_quan_progressiva, &
       ld_quan_movimento, ld_null[12],ld_quant_val[],ld_giacenza_stock[], &
		 ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza[12], ld_quan_mese, &
		 ld_quan_rettifica_mese,ld_quan_recupero, ld_perc, ll_perc[12]
		 
datetime ldt_inizio,ldt_fine, ldt_data_fine_mese[12],ldt_data_registrazione,ldt_data_inzio_mese, ldt_null

datastore lds_movimenti, lds_giacenze
uo_magazzino luo_mag

setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)

//ll_perc[1] = 5
//ll_perc[2] = 8
//ll_perc[3] = 9
//ll_perc[4] = 13
//ll_perc[5] = 17
//ll_perc[6] = 29
//ll_perc[7] = 19
//ll_perc[8] = 0
//ll_perc[9] = 0
//ll_perc[10] = 0
//ll_perc[11] = 0
//ll_perc[12] = 0

dw_percentuali.accepttext()

ll_perc[1] = dw_percentuali.getitemnumber( 1, "perc_1")
ll_perc[2] = dw_percentuali.getitemnumber( 1, "perc_2")
ll_perc[3] = dw_percentuali.getitemnumber( 1, "perc_3")
ll_perc[4] = dw_percentuali.getitemnumber( 1, "perc_4")
ll_perc[5] = dw_percentuali.getitemnumber( 1, "perc_5")
ll_perc[6] = dw_percentuali.getitemnumber( 1, "perc_6")
ll_perc[7] = dw_percentuali.getitemnumber( 1, "perc_7")
ll_perc[8] = dw_percentuali.getitemnumber( 1, "perc_8")
ll_perc[9] = dw_percentuali.getitemnumber( 1, "perc_9")
ll_perc[10] = dw_percentuali.getitemnumber( 1, "perc_10")
ll_perc[11] = dw_percentuali.getitemnumber( 1, "perc_11")
ll_perc[12] = dw_percentuali.getitemnumber( 1, "perc_12")

ldt_inizio = datetime(date(em_data_inizio.text), 00:00:00) 
ldt_fine   = datetime(date(em_data_fine.text), 00:00:00) 

g_mb.messagebox("APICE", "Procedo? Hai controllato le date?")

filedelete(em_LOG.text)

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ll_log = fileopen(em_log.text, linemode!, write!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ll_log1 = fileopen("C:\SCARICHI_NONEXEC.TXT", linemode!, write!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if



lds_movimenti = create datastore
lds_movimenti.dataobject = 'd_ds_aggiusta_magazzino_2'

lds_movimenti.settransobject(sqlca)

lds_giacenze = create datastore
lds_giacenze.dataobject = 'd_ds_aggiusta_magazzino_3'

do while true
	
	ll_ret = fileread(ll_file, ls_stringa)
	if ll_ret = -100 then exit		// file elaborazione
	
	if ll_ret < 0 then				// errore interrompo
		g_mb.messagebox("APICE", "Errore in lettura file di testo")
		return
	end if
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	ld_quan_rettifica = dec(ls_stringa)
	
	// eseguo 12 inventari del prodotto
	
	lds_giacenze.reset()
	
	for ll_i = 1 to 12 
		st_scarico.text = "Inventario " + string(ll_i) + " " + ls_cod_prodotto
		yield()
		// creazione limiti date
		choose case ll_i
			case 4,6,9,11
				ldt_data_fine_mese[ll_i] = datetime(date(string("30/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
			case 2
				if mod(long(em_anno.text), 4 ) = 0 then		// mese bisesto, anno funesto !!!! kz
					ldt_data_fine_mese[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				else
					ldt_data_fine_mese[ll_i] = datetime(date(string("28/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				end if
			case else
				ldt_data_fine_mese[ll_i] = datetime(date(string("31/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
		end choose
		
		if ldt_data_fine_mese[ll_i] > ldt_fine then
			ldt_data_fine_mese[ll_i] = ldt_fine
		end if
			
		for ll_y = 1 to 14
			ld_quant_val[ll_y] = 0
		next
		
		luo_mag = CREATE uo_magazzino
		
		ll_err = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, &
																		ldt_data_fine_mese[ll_i], &
																		ls_where, &
																		ld_quant_val[], &
																		ls_errore, &
																		"N", &
																		ls_chiave[], &
																		ld_giacenza_stock[], &
																		ld_costo_medio_stock[],&
																		ld_quan_costo_medio_stock[])
	
		ld_giacenza[ll_i] = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
		ll_row = lds_giacenze.insertrow(0)
		lds_giacenze.setitem(ll_row, "mese", ll_i)
		lds_giacenze.setitem(ll_row, "giacenza", ld_giacenza[ll_i])
		
		destroy luo_mag
		
		if	ldt_data_fine_mese[ll_i] = ldt_fine then exit
		
	next
	

	ll_tot_mesi_verifica = ll_i
	
	// verifico se esistono movimenti SMP
	ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_inizio, ldt_fine, "SMP")

	ld_quan_progressiva = 0
	
	
	if ll_rows > 0 then
		
		filewrite(ll_log, "Il prodotto " + ls_cod_prodotto + " ha " + string(ll_rows) + " movimenti SMP")
		/* se la quantità devo rettificare mi porta il magazzino in negativo a fine mese,
			salto il prodotto, e lo segnalo nel log 
			Per sicurezza faccio un controllo preventivo.	
		*/
		
		lds_giacenze.setfilter("giacenza > 0")
		lds_giacenze.filter()
		lds_giacenze.setsort("giacenza A, mese A")
		lds_giacenze.sort()
		
		for ll_z = 1 to lds_giacenze.rowcount()
			
			ldt_data_inzio_mese = datetime(date(string("01/" + string(lds_giacenze.getitemnumber(ll_z, "mese")) + "/" + em_anno.text)),00:00:00)
		
			if ldt_data_inzio_mese > ldt_fine then continue
			if ldt_data_inzio_mese < ldt_inizio then continue
			
			if ldt_data_fine_mese[lds_giacenze.getitemnumber(ll_z, "mese")] > ldt_fine then
				ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_data_inzio_mese, ldt_fine, "SMP")
			else
				ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_data_inzio_mese, ldt_data_fine_mese[lds_giacenze.getitemnumber(ll_z, "mese")], "SMP")
			end if
			
			if ll_rows = 0 then
				// nel mese corrente non ci sono movimenti SMP, quandi passo al mese succ
				continue
			else
				if lds_giacenze.getitemnumber(ll_z, "giacenza") >= ld_quan_rettifica then 
					ld_quan_rettifica_mese = ld_quan_rettifica
				else
					ld_quan_rettifica_mese = lds_giacenze.getitemnumber(ll_z, "giacenza")
				end if
				
				// spalmo sui movimenti del mese gli aumenti
				ld_quan_movimento = ld_quan_rettifica_mese / ll_rows
				if int(ld_quan_movimento) <> ld_quan_movimento then
					ld_quan_movimento = int(ld_quan_movimento) + 1
				end if
				
			end if
			
			ld_quan_mese = 0
			
			for ll_i = 1 to ll_rows
				ll_anno_registrazione = lds_movimenti.getitemnumber(ll_i, "anno_registrazione")
				ll_num_registrazione = lds_movimenti.getitemnumber(ll_i, "num_registrazione")
				ldt_data_registrazione = lds_movimenti.getitemdatetime(ll_i, "data_registrazione")
				
				
				if (ld_quan_mese + ld_quan_movimento) > ld_quan_rettifica_mese then
					ld_quan_movimento = ld_quan_rettifica_mese - ld_quan_mese
				end if
				ld_quan_mese = ld_quan_mese + ld_quan_movimento
				
				//****
				if ld_quan_progressiva + ld_quan_movimento > ld_quan_rettifica then
					// eseguo movimento per la rimanenza
					ld_quan_movimento = ld_quan_rettifica - ld_quan_progressiva
				end if
				
				ld_quan_progressiva = ld_quan_progressiva + ld_quan_movimento
				
				st_scarico.text = "UPDATE " + ls_cod_prodotto
				yield()
				
				if ld_quan_movimento <= 0 then
					continue
				end if
			
				update mov_magazzino
				set    quan_movimento = quan_movimento + :ld_quan_movimento
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
				       num_registrazione = :ll_num_registrazione;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE", "Errore Database in UPDATE Mov_magazzino.~r~n" + sqlca.sqlerrtext)
					rollback;
					return
				end if
	
				filewrite(ll_log, "PRODOTTO " + ls_cod_prodotto + ": incrementato di " + string(ld_quan_movimento) + " REG:" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
				
				// raggiunta la quantità richiesta ... esco
				if ld_quan_progressiva >= ld_quan_rettifica then  exit
				
				// raggiunta la quantità prevista per il mese... passo al mese successivo
				if ld_quan_mese >= ld_quan_rettifica_mese   then exit
	
			next	
			
			// diminuisco le giacenze dei mesi successivi
			
			DEC{4} ld_temp1, ld_temp2
			long ll_temp1, ll_temp2
			
			
			lds_giacenze.SAVEAS("GIACENZE.TXT", TEXT!, true)
			
			for ll_t = ll_z to lds_giacenze.rowcount()
				ll_temp1 = lds_giacenze.getitemnumber(ll_t, "mese")	// mese in esame
				ll_temp2 = lds_giacenze.getitemnumber(ll_z, "mese")	// mese corrente di elaborazione
				
				ld_temp1 = lds_giacenze.getitemnumber(ll_t, "giacenza")
				
				if ll_temp1 >= ll_temp2 then
					ld_giacenza[ll_temp1] = lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_mese
					lds_giacenze.setitem(ll_t, "giacenza",lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_mese)
					// i mesi precedenti non possono avere una giacenza superiore
					
					for ll_x = 1 to lds_giacenze.rowcount()
						if lds_giacenze.getitemnumber(ll_x, "mese") < ll_temp1 then
							if lds_giacenze.getitemnumber(ll_x, "giacenza") > ld_giacenza[ll_temp1] then
								lds_giacenze.setitem(ll_x, "giacenza", ld_giacenza[ll_temp1])
								ld_giacenza[lds_giacenze.getitemnumber(ll_x, "mese")] = ld_giacenza[ll_temp1]
							end if
						end if
					next
					
				end if
			next

			// raggiunta la quantità richiesta ... passo ad altro prodotto
			if ld_quan_progressiva >= ld_quan_rettifica then 
				commit;
				exit
			end if
			
		next
		
	else		// non esiste alcun movimento SMP, allora procedo con la creazione del movimento.
		
		filewrite(ll_log, "Il prodotto " + ls_cod_prodotto + " NON ha movimenti SMP")

		lds_giacenze.setsort("giacenza A, mese A")
		lds_giacenze.sort()
		
		ld_quan_progressiva = 0
		luo_mag = create uo_magazzino
		
		ld_quan_recupero = 0
		for ll_z = 1 to lds_giacenze.rowcount()
			
			if ldt_data_inzio_mese > ldt_fine then continue
			
			if lds_giacenze.getitemnumber(ll_z, "giacenza") <= 0 then
				ld_perc = ld_quan_rettifica / 100 * ll_perc[lds_giacenze.getitemnumber(ll_z, "mese")]
				ld_quan_recupero = ld_quan_recupero + ld_perc
				continue
			end if
			
			if lds_giacenze.getitemnumber(ll_z, "giacenza") >= ld_quan_rettifica then 
				ld_quan_rettifica_mese = ld_quan_rettifica
			else
				ld_quan_rettifica_mese = lds_giacenze.getitemnumber(ll_z, "giacenza")
			end if
			
			// spalmo sui movimenti del mese gli aumenti
			
			ld_quan_movimento = ld_quan_rettifica / 100 * ll_perc[lds_giacenze.getitemnumber(ll_z, "mese")]
			if int(ld_quan_movimento) <> ld_quan_movimento then
				ld_quan_movimento = int(ld_quan_movimento + ld_quan_recupero) + 1
				ld_quan_recupero = 0
			end if
			
			if lds_giacenze.getitemnumber(ll_z, "giacenza") < ld_quan_movimento then 
				ld_quan_recupero = ld_quan_movimento - lds_giacenze.getitemnumber(ll_z, "giacenza")
				ld_quan_movimento = lds_giacenze.getitemnumber(ll_z, "giacenza")
			end if 
			
			// sono capitato in un mese a zero
			if ld_quan_movimento <= 0 then 
				continue
			end if
			
			ld_quan_progressiva = ld_quan_progressiva + ld_quan_movimento
			
			if ld_quan_progressiva > ld_quan_rettifica then
				ld_quan_movimento = ld_quan_movimento - (ld_quan_progressiva - ld_quan_rettifica)
			end if
			
//			ESEGUO movimento di magazzino

			st_scarico.text = "NUOVO MOV. " + ls_cod_prodotto
			yield()
			
			string ls_cod_tipo_movimento, ls_cod_deposito[],ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[]
			long ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_anno_reg[], ll_num_reg[]
			datetime ldt_data_stock[]
			
			ls_cod_tipo_movimento = "SMP"
			ls_cod_deposito[1] = "001"
			ls_cod_ubicazione[1] = ls_null
			ls_cod_lotto[1] = ls_null
			ldt_data_stock[1] = ldt_null
			ll_prog_stock[1] = ll_null
			ls_cod_cliente[1] = ls_null
			ls_cod_fornitore[1] = ls_null
			
			if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
													ls_cod_prodotto, &
													ls_cod_deposito[], &
													ls_cod_ubicazione[], &
													ls_cod_lotto[], &
													ldt_data_stock[], &
													ll_prog_stock[], &
													ls_cod_cliente[], &
													ls_cod_fornitore[], &
													ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock ) = -1 then
				ROLLBACK;
				// segnalare !!!!!
				return -1
			end if
			
			if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
											 ll_num_reg_dest_stock, &
											 ls_cod_tipo_movimento, &
											 ls_cod_prodotto) = -1 then
				ROLLBACK;
				return 0
				// segnalare !!!!!
			end if
			

			if luo_mag.uof_movimenti_mag ( ldt_data_fine_mese[lds_giacenze.getitemnumber(ll_z, "mese")], &
										ls_cod_tipo_movimento, &
										"S", &
										ls_cod_prodotto, &
										ld_quan_movimento, &
										0, &
										0, &
										ldt_data_fine_mese[lds_giacenze.getitemnumber(ll_z, "mese")], &
										'', &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_fornitore[], &
										ls_cod_cliente[], &
										ll_anno_reg[], &
										ll_num_reg[] ) = 0 then
			
				if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock) = -1 then
					ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
					// segnalare !!!!!
				end if
			end if

			filewrite(ll_log, "PRODOTTO " + ls_cod_prodotto + ": Aggiunto movimento qta=" + string(ld_quan_movimento) + " REG:" + string(ll_anno_reg[1]) + "/" + string(ll_num_reg[1]))
				
			lds_giacenze.SAVEAS("GIACENZE.TXT", TEXT!, true)

			// diminuisco le giacenze dei mesi successivi
			for ll_t = ll_z to lds_giacenze.rowcount()
				ll_temp1 = lds_giacenze.getitemnumber(ll_t, "mese")
				ll_temp2 = lds_giacenze.getitemnumber(ll_z, "mese")
				
				ld_temp1 = lds_giacenze.getitemnumber(ll_t, "giacenza")
				
				if ll_temp1 >= ll_temp2 then
					ld_giacenza[ll_temp1] = ld_temp1 - ld_quan_movimento        //lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_movimento
					lds_giacenze.setitem(ll_t, "giacenza", ld_giacenza[ll_temp1]) //lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_movimento )
					
					// i mesi precedenti non possono avere una giacenza superiore
					
//					for ll_x = 1 to lds_giacenze.rowcount()
//						if lds_giacenze.getitemnumber(ll_x, "mese") < ll_temp1 then
//							if lds_giacenze.getitemnumber(ll_x, "giacenza") > ld_giacenza[ll_temp1] then
//								lds_giacenze.setitem(ll_x, "giacenza", ld_giacenza[ll_temp1])
//								ld_giacenza[lds_giacenze.getitemnumber(ll_x, "mese")] = ld_giacenza[ll_temp1]
//							end if
//						end if
//					next
					
				end if
			next

			
			// diminuisco le giacenze dei mesi successivi
//			for ll_t = 1 to lds_giacenze.rowcount()
//				if lds_giacenze.getitemnumber(ll_t, "mese") >= lds_giacenze.getitemnumber(ll_z, "mese") then
//					ld_giacenza[lds_giacenze.getitemnumber(ll_t, "mese")] = lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_movimento
//					lds_giacenze.setitem(ll_t, "giacenza",lds_giacenze.getitemnumber(ll_t, "giacenza") - ld_quan_movimento)
//				end if
//			next

			// raggiunta la quantità richiesta ... passo ad altro prodotto
			if ld_quan_progressiva >= ld_quan_rettifica then 
				commit;
				exit
			end if
			
		next
		
		destroy luo_mag
		
		if ld_quan_progressiva < ld_quan_rettifica then 
			filewrite(ll_log1, "PRODOTTO " + ls_cod_prodotto + " non rettificabile per la quantità " + string(ld_quan_rettifica))
			rollback;
		end if
		

	end if
loop

fileclose(ll_file)
fileclose(ll_log)
fileclose(ll_log1)

end event

type st_8 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 2048
integer y = 256
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "ANNO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_anno from editmask within w_aggiusta_magazzino_con_deposito
integer x = 2272
integer y = 244
integer width = 251
integer height = 92
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
end type

type em_data_fine from editmask within w_aggiusta_magazzino_con_deposito
integer x = 2185
integer y = 812
integer width = 375
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_7 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 1925
integer y = 816
integer width = 238
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "FINE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_inizio from editmask within w_aggiusta_magazzino_con_deposito
integer x = 1440
integer y = 812
integer width = 375
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_6 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 1189
integer y = 828
integer width = 238
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "INIZIO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_aggiusta_magazzino_con_deposito
boolean visible = false
integer x = 2226
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "SCARICO"
end type

event clicked;string ls_stringa, ls_cod_prodotto, ls_where, ls_errore, ls_chiave[]

long ll_rows, ll_file, ll_ret, ll_pos, ll_i, ll_anno_registrazione, ll_num_registrazione, &
     ll_y,ll_err, ll_tot_mesi_verifica,ll_row
	  
dec{4} ld_quan_rettifica,ld_quan_rettifica_movimento, ld_quan_progressiva, ld_quan_differenza, &
       ld_quan_movimento, ld_quant_perc[12], ld_null[12],ld_quant_val[],ld_giacenza_stock[], &
		 ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza[12], ll_perc[12]
		 
datetime ldt_inizio,ldt_fine, ldt_data_fine_mese[12],ldt_data_registrazione

datastore lds_movimenti, lds_giacenze
uo_magazzino luo_mag

dw_percentuali.accepttext()

ll_perc[1] = dw_percentuali.getitemnumber( 1, "perc_1")
ll_perc[2] = dw_percentuali.getitemnumber( 1, "perc_2")
ll_perc[3] = dw_percentuali.getitemnumber( 1, "perc_3")
ll_perc[4] = dw_percentuali.getitemnumber( 1, "perc_4")
ll_perc[5] = dw_percentuali.getitemnumber( 1, "perc_5")
ll_perc[6] = dw_percentuali.getitemnumber( 1, "perc_6")
ll_perc[7] = dw_percentuali.getitemnumber( 1, "perc_7")
ll_perc[8] = dw_percentuali.getitemnumber( 1, "perc_8")
ll_perc[9] = dw_percentuali.getitemnumber( 1, "perc_9")
ll_perc[10] = dw_percentuali.getitemnumber( 1, "perc_10")
ll_perc[11] = dw_percentuali.getitemnumber( 1, "perc_11")
ll_perc[12] = dw_percentuali.getitemnumber( 1, "perc_12")

ldt_inizio = datetime(date(em_data_inizio.text), 00:00:00) 
ldt_fine   = datetime(date(em_data_fine.text), 00:00:00) 

g_mb.messagebox("APICE", "Procedo? Hai controllato le date?")

filedelete(em_LOG.text)

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if


lds_movimenti = create datastore
lds_movimenti.dataobject = 'd_ds_aggiusta_magazzino_2'

lds_movimenti.settransobject(sqlca)

do while true
	
	ll_ret = fileread(ll_file, ls_stringa)
	if ll_ret = -100 then exit		// file elaborazione
	
	if ll_ret < 0 then				// errore interrompo
		g_mb.messagebox("APICE", "Errore in lettura file di testo")
		return
	end if
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	ld_quan_rettifica = dec(ls_stringa)
	
	// eseguo 12 inventari del prodotto
	
	lds_giacenze = create datastore
	lds_giacenze.dataobject = 'd_ds_aggiusta_magazzino_3'
	
	for ll_i = 1 to 12 
		// creazione limiti date
		choose case ll_i
			case 4,6,9,11
				ldt_data_fine_mese[ll_i] = datetime(date(string("30/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
			case 2
				if mod(long(em_anno.text), 4 ) = 0 then		// mese bisesto, anno funesto !!!! kz
					ldt_data_fine_mese[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				else
					ldt_data_fine_mese[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				end if
			case else
				ldt_data_fine_mese[ll_i] = datetime(date(string("31/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
		end choose
		
		if ldt_data_fine_mese[ll_i] > ldt_fine then
			ldt_data_fine_mese[ll_i] = ldt_fine
		end if
			
		for ll_y = 1 to 14
			ld_quant_val[ll_y] = 0
		next
		
		luo_mag = CREATE uo_magazzino
		
		ll_err = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, &
																		ldt_data_fine_mese[ll_i], &
																		ls_where, &
																		ld_quant_val[], &
																		ls_errore, &
																		"N", &
																		ls_chiave[], &
																		ld_giacenza_stock[], &
																		ld_costo_medio_stock[],&
																		ld_quan_costo_medio_stock[])
	
		ld_giacenza[ll_i] = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
		ll_row = lds_giacenze.insertrow(0)
		lds_giacenze.setitem(ll_row, "mese", ll_i)
		lds_giacenze.setitem(ll_row, "giacenza", ld_giacenza[ll_i])
		
		destroy luo_mag
		
		if	ldt_data_fine_mese[ll_i] = ldt_fine then exit
		
	next

	ll_tot_mesi_verifica = ll_i
	
	// verifico se esistono movimenti SMP
	ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_inizio, ldt_fine, "SMP")

	ld_quan_progressiva = 0
	
	if ll_rows > 0 then
		ld_quan_rettifica_movimento = ld_quan_rettifica / ll_rows
		if int(ld_quan_rettifica_movimento) <> ld_quan_rettifica_movimento then 
			ld_quan_rettifica_movimento = int(ld_quan_rettifica_movimento) + 1
		end if
		
		/* se la quantità devo rettificare mi porta il magazzino in negativo a fine mese,
			salto il prodotto, e lo segnalo nel log 
			Per sicurezza faccio un controllo preventivo.	
		*/
		
		lds_giacenze.setsort("mese A")
		lds_giacenze.sort()
		
		
		
		
		for ll_i = 1 to ll_tot_mesi_verifica
			ld_quan_progressiva += ld_quan_rettifica_movimento
			if ld_giacenza[ll_i] < ld_quan_progressiva then
				// segnala nel log
				// salta il prodotto
			end if
		next	
		
		// se passo di qua vuol dire che non andrò mai in negativo
			

		for ll_i = 1 to ll_rows
			ll_anno_registrazione = lds_movimenti.getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = lds_movimenti.getitemnumber(ll_i, "num_registrazione")
			ld_quan_movimento = lds_movimenti.getitemnumber(ll_i, "quan_movimento")
			ldt_data_registrazione = lds_movimenti.getitemdatetime(ll_i, "data_registrazione")
			
			if ld_quan_movimento > ld_quan_rettifica_movimento then
				ld_quan_differenza = ld_quan_rettifica_movimento
			else
				ld_quan_differenza = ld_quan_movimento
			end if
			
			ld_quan_progressiva = ld_quan_progressiva + ld_quan_differenza
			
			if ld_quan_progressiva > ld_quan_rettifica then
				ld_quan_differenza = ld_quan_rettifica - ld_quan_progressiva
			end if
			
//			update mov_magazzino
//			set    quan_movimento = quan_movimento + :ld_quan_differenza
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 anno_registrazione = :ll_anno_registrazione and
//			       num_registrazione = :ll_num_registrazione;
//			if sqlca.sqlcode <> 0 then
//				messagebox("APICE", "Errore Database in UPDATE Mov_magazzino.~r~n" + sqlca.sqlerrtext)
//				rollback;
//				return
//			end if
			
			// raggiunta la quantità richiesta ... esco
			if ld_quan_progressiva >= ld_quan_rettifica then  exit

		next	
		
		// raggiunta la quantità richiesta ... passo ad altro prodotto
		if ld_quan_progressiva >= ld_quan_rettifica then 
			commit;
			continue
		end if
		
	else		// non esiste alcun movimento SMP, allora procedo con la creazione del movimento.
		
		// calcolo le quantità mensili
		ld_quant_perc[] = ld_null[]
		ld_quan_rettifica_movimento = ld_quan_rettifica
		for ll_i = 1 to ll_tot_mesi_verifica
			if ll_i = ll_tot_mesi_verifica then
				ld_quant_perc[ll_i] = ld_quan_rettifica_movimento
			else
				ld_quant_perc[ll_i] = round((ld_quan_rettifica / 100) * ll_perc[ll_i],0)
				ld_quan_rettifica_movimento = ld_quan_rettifica_movimento - ld_quant_perc[ll_i]
			end if
		next
		
		/* se la quantità devo rettificare mi porta il magazzino in negativo a fine mese,
			salto il prodotto, e lo segnalo nel log 
			Per sicurezza faccio un controllo preventivo.	
		*/
		for ll_i = 1 to ll_tot_mesi_verifica
			ld_quan_progressiva += ld_quan_rettifica_movimento
			if ld_giacenza[ll_i] < ld_quan_progressiva then
				// segnala nel log
				// salta il prodotto
			end if
		next	
		
		// se passo di qua vuol dire che non andrò mai in negativo

		//	a questo punto procedo con la creazione dei movimenti di magazzino richiesti.
		
		string ls_cod_tipo_movimento, ls_cod_deposito[],ls_cod_ubicazione[],ls_cod_lotto[],ls_cod_cliente[],ls_cod_fornitore[]
		long ll_prog_stock[],ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_anno_reg[], ll_num_reg[]
		datetime ldt_data_stock[]
		
		ls_cod_tipo_movimento = "SMP"
		ls_cod_deposito[1] = "001"
		luo_mag = create uo_magazzino
		
		for ll_i = 1 to ll_tot_mesi_verifica
			
			if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
													ls_cod_prodotto, &
													ls_cod_deposito[], &
													ls_cod_ubicazione[], &
													ls_cod_lotto[], &
													ldt_data_stock[], &
													ll_prog_stock[], &
													ls_cod_cliente[], &
													ls_cod_fornitore[], &
													ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock ) = -1 then
				ROLLBACK;
				return -1
			end if
			
			if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
											 ll_num_reg_dest_stock, &
											 ls_cod_tipo_movimento, &
											 ls_cod_prodotto) = -1 then
				ROLLBACK;
				return 0
				// segnalare !!!!!
			end if
			
			
			if luo_mag.uof_movimenti_mag ( ldt_data_fine_mese[ll_i], &
										ls_cod_tipo_movimento, &
										"S", &
										ls_cod_prodotto, &
										ld_quant_perc[ll_i], &
										0, &
										0, &
										ldt_data_fine_mese[ll_i], &
										'', &
										ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock, &
										ls_cod_deposito[], &
										ls_cod_ubicazione[], &
										ls_cod_lotto[], &
										ldt_data_stock[], &
										ll_prog_stock[], &
										ls_cod_fornitore[], &
										ls_cod_cliente[], &
										ll_anno_reg[], &
										ll_num_reg[] ) = 0 then
			
				if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
													ll_num_reg_dest_stock) = -1 then
					ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
				end if
			end if
					
		next
	end if
loop
end event

type cb_3 from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 23
integer y = 816
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CARICO"
end type

event clicked;string ls_stringa, ls_cod_prodotto
long ll_rows, ll_file, ll_ret, ll_pos, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_log, ll_log1
dec{4} ld_quan_rettifica,ld_quan_rettifica_movimento, ld_quan_progressiva, ld_quan_differenza, &
       ld_quan_movimento
datetime ldt_inizio,ldt_fine
datastore lds_movimenti


ldt_inizio = datetime(date(em_data_inizio.text), 00:00:00) 
ldt_fine   = datetime(date(em_data_fine.text), 00:00:00) 

g_mb.messagebox("APICE", "Procedo? Hai controllato le date?")

filedelete(em_LOG.text)

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ll_log = fileopen(em_log.text, linemode!, write!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ll_log1 = fileopen("C:\NEXEC_CAR.TXT", linemode!, write!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

lds_movimenti = create datastore
lds_movimenti.dataobject = 'd_ds_aggiusta_magazzino_2'

lds_movimenti.settransobject(sqlca)

do while true
	
	ll_ret = fileread(ll_file, ls_stringa)
	if ll_ret = -100 then exit		// fine elaborazione
	
	if ll_ret < 0 then				// errore interrompo
		g_mb.messagebox("APICE", "Errore in lettura file di testo")
		return
	end if
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	ld_quan_rettifica = dec(ls_stringa)
	
	st_carico.text = ls_cod_prodotto
	yield()
	
	// verifico se esistono movimenti SMP
	ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_inizio, ldt_fine, "SMP")

	ld_quan_progressiva = 0
	
	filewrite(ll_log, "Il prodotto " + ls_cod_prodotto + " ha " + string(ll_rows) + " movimenti SMP")

	if ll_rows > 0 then
		ld_quan_rettifica_movimento = ld_quan_rettifica / ll_rows
		if int(ld_quan_rettifica_movimento) <> ld_quan_rettifica_movimento then 
			ld_quan_rettifica_movimento = int(ld_quan_rettifica_movimento) + 1
		end if
		
		for ll_i = 1 to ll_rows
			ll_anno_registrazione = lds_movimenti.getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = lds_movimenti.getitemnumber(ll_i, "num_registrazione")
			ld_quan_movimento = lds_movimenti.getitemnumber(ll_i, "quan_movimento")
			
			//prendo come quan_differenza la più piccola tra "ld_quan_movimento" e "ld_quan_rettifica_movimento"
			if ld_quan_movimento > ld_quan_rettifica_movimento then
				ld_quan_differenza = ld_quan_rettifica_movimento
			else
				ld_quan_differenza = ld_quan_movimento
			end if
			
			//chiedere a Enrico che "minchia" fa questa cosa.....che io dal mio piccolo non ci arrivo...
			if (ld_quan_progressiva + ld_quan_differenza) > ld_quan_rettifica then
				if (ld_quan_rettifica - (ld_quan_progressiva + ld_quan_differenza)) > ld_quan_movimento then
					ld_quan_differenza = ld_quan_movimento
				end if
			end if
			//-----------------

			ld_quan_progressiva = ld_quan_progressiva + ld_quan_differenza
			
			st_carico.text = "UPDATE DATABASE"
			yield()
			
			update mov_magazzino
			set    quan_movimento = quan_movimento - :ld_quan_differenza
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
			       num_registrazione = :ll_num_registrazione;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Errore Database in UPDATE Mov_magazzino.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			filewrite(ll_log, "PRODOTTO " + ls_cod_prodotto + ": decrementato (SMP) di " + string(ld_quan_differenza) + " REG:" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			// raggiunta la quantità richiesta ... esco
			if ld_quan_progressiva >= ld_quan_rettifica then  exit

		next	
		
		// raggiunta la quantità richiesta ... passo ad altro prodotto
		if ld_quan_progressiva >= ld_quan_rettifica then 
			commit;  		// FACCIO IL COMMIT SOLO QUANDO HO COMPLETATO ANCHE GLI VE
			continue
		end if
		
		// non ancora raggiunta la quantità richiesta, provo con movimenti SVE
		
	end if
	
	
	// verifico se esistono movimenti SVE
	ll_rows = lds_movimenti.retrieve(ls_cod_prodotto, ldt_inizio, ldt_fine, "SVE")

	filewrite(ll_log, "Il prodotto " + ls_cod_prodotto + " ha " + string(ll_rows) + " movimenti SVE")

	if ll_rows > 0 then
		ld_quan_rettifica_movimento = (ld_quan_rettifica - ld_quan_progressiva) / ll_rows
		if int(ld_quan_rettifica_movimento) <> ld_quan_rettifica_movimento then 
			ld_quan_rettifica_movimento = int(ld_quan_rettifica_movimento) + 1
		end if
		
		for ll_i = 1 to ll_rows
			ll_anno_registrazione = lds_movimenti.getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = lds_movimenti.getitemnumber(ll_i, "num_registrazione")
			ld_quan_movimento = lds_movimenti.getitemnumber(ll_i, "quan_movimento")
			
			if ld_quan_movimento > ld_quan_rettifica_movimento then
				ld_quan_differenza = ld_quan_rettifica_movimento
			else
				ld_quan_differenza = ld_quan_movimento
			end if
			
			ld_quan_progressiva = ld_quan_progressiva + ld_quan_differenza
			
			if ld_quan_progressiva > ld_quan_rettifica then
				ld_quan_differenza = ld_quan_rettifica - ld_quan_progressiva
			end if
			
			st_carico.text = "UPDATE DATABASE"
			yield()
			
			update mov_magazzino
			set    quan_movimento = quan_movimento - :ld_quan_differenza
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
			       num_registrazione = :ll_num_registrazione;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Errore Database in UPDATE Mov_magazzino.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			filewrite(ll_log, "PRODOTTO " + ls_cod_prodotto + ": decrementato (SVE) di " + string(ld_quan_differenza) + " REG:" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			
			// raggiunta la quantità richiesta ... esco
			if ld_quan_progressiva >= ld_quan_rettifica then  exit

		next	
		
		// raggiunta la quantità richiesta ... passo ad altro prodotto
		if ld_quan_progressiva >= ld_quan_rettifica then 
			commit;
			continue
		end if
		
		// a questo punto se non sono riuscito a  rettificare tutto faccio rollback di tutto e si arrangiano a mano		
		// lo scrivo nel log
		
		filewrite(ll_log1, "PRODOTTO " + ls_cod_prodotto + ": QUANTITA' SMP+SVE NON SUFFICIENTE")
		rollback;
		
	end if
loop

fileclose(ll_file)
fileclose(ll_log)
fileclose(ll_log1)


end event

type cbx_giacenza from checkbox within w_aggiusta_magazzino_con_deposito
integer x = 823
integer y = 252
integer width = 567
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Controlla Giacenza"
boolean checked = true
boolean lefttext = true
end type

type st_5 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 105
integer y = 260
integer width = 288
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "TIPO MOV:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_mov_scarichi from editmask within w_aggiusta_magazzino_con_deposito
integer x = 411
integer y = 244
integer width = 370
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_2 from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 2171
integer y = 360
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "MOVIMENTI"
end type

event clicked;string ls_cod_prodotto,ls_cod_tipo_mov_scarico_pf,ls_referenza, ls_cod_deposito[], &
       ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[],ls_stringa, &
		 ls_cod_tipo_movimento,ls_chiave[],ls_where, ls_errore
long	ll_i, ll_rows, ll_filelog, ll_prog_stock[], ll_anno_reg_mov[], ll_num_reg_mov[], &
      ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_file,ll_err,ll_pos,ll_y, ll_index, &
		ll_mese_in_esame, ll_t
dec{4} ld_quantita,ld_giacenza[12], ld_quant_val[],ld_giacenza_stock[], ld_costo_medio_stock[],&
		ld_quan_costo_medio_stock[], ld_mingiacenza, ld_quan_mensile, ld_quan_cumulata,ld_quan_movimento, &
		ld_quantita_scarico,ld_quan_riporto, ld_quan_periodo
datetime ldt_inizio, ldt_fine[],ldt_data_reg_mov,ldt_data_stock[]
uo_magazzino luo_mag
long ll_num_stock, ll_indice
string ls_cod_deposito_corrente

st_legenda.text = ""

g_mb.messagebox("APICE", "Procedo? Hai controllato le date e l'anno ?")

filedelete(em_LOG.text)

//il file da leggere deve essere del tipo
//prodotto<TAB>quantita

ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ls_cod_tipo_movimento = em_mov_scarichi.text

//Donato 05/01/2009 leggo il deposito da elaborare
ls_cod_deposito_corrente = dw_selezione.getitemstring(1, "cod_deposito")
if ls_cod_deposito_corrente = "" or isnull(ls_cod_deposito_corrente) then
	g_mb.messagebox("APICE", "Specificare un Deposito!")
	return
end if

select count(*)
into :ll_indice
from anag_depositi
where cod_azienda=:s_cs_xx.cod_azienda and cod_deposito=:ls_cod_deposito_corrente;

if isnull(ll_indice) then ll_indice = 0

if sqlca.sqlcode <> 0 or ll_indice = 0 then
	g_mb.messagebox("APICE", "Deposito non trovato oppure si è verificato un errore durante la lettura del deposito!!")
	return
end if

setpointer(HourGlass!)

do while true
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	
	st_legenda.text = ls_cod_prodotto
	
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	
	//quantita da caricare o scaricare per l'allineamento delle giacenze
	ld_quantita = dec(ls_stringa)
	
	//	faccio 12 inventari per il prodotto, ciascuno in data fine
	for ll_i = 1 to 12 
		// creazione limiti date
		choose case ll_i
			case 4,6,9,11
				ldt_fine[ll_i] = datetime(date(string("30/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
			case 2 //febbraio
				//gestire anno bisesto, anno funesto
				if wf_bisestile(long(em_anno.text)) then
					ldt_fine[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				else
					ldt_fine[ll_i] = datetime(date(string("28/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				end if
				//ldt_fine[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
				
			case else
				ldt_fine[ll_i] = datetime(date(string("31/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
		end choose
			
		// verifico de devo fare il controllo della giacenza
		if cbx_giacenza.checked then
			for ll_y = 1 to 14
				ld_quant_val[ll_y] = 0
			next
			
			luo_mag = CREATE uo_magazzino
			
			//Donato 05/01/2009 le giacenze da leggere sono quelle di un singolo deposito interno
			//specificato in "em_cod_deposito.text"
			/*
			ll_err = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, &
																			ldt_fine[ll_i], &
																			ls_where, &
																			ld_quant_val[], &
																			ls_errore, &
																			"N", &
																			ls_chiave[], &
																			ld_giacenza_stock[], &
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])
					
			ld_giacenza[ll_i] = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			*/
			
			//attenzione all'argomento n°6, deve valere "D" se voglio la giacenza raggruppata per depositi
			//ll_err = luo_mag.uof_saldo_prod_date_decimal_comm(ls_cod_prodotto, &
			ll_err = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, &
																			ldt_fine[ll_i], &
																			ls_where, &
																			ld_quant_val[], &
																			ls_errore, &
																			"D", &
																			ls_chiave[], &
																			ld_giacenza_stock[], &
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])
			
			//in ls_chiave ci sono i vari depositi trovati
			ll_num_stock = upperbound(ls_chiave[])
			
			//annullo la giacenza
			ld_giacenza[ll_i] = 0
			
			//ciclo l'array dei depositi per beccare quello che mi interessa
			for ll_indice = 1 to ll_num_stock
				if ls_chiave[ll_indice] = ls_cod_deposito_corrente then
					//deposito trovato
					//metto la quantità letta dalla funzione
					ld_giacenza[ll_i] = ld_giacenza_stock[ll_indice]
					exit
				end if
			next
			//se il for precedente non trova il deposito voluto allora
			//in ld_giacenza[ll_i] rimarrà 0: OK
			
			//fine modifica ------------------------------------------------------------------------
			
			destroy luo_mag
			
		end if		
		
	next
	
	ll_index = 1		// è il mese in esame
	ld_mingiacenza = 0
	
	//in ld_quantita c'è la quantita letta da file
	ld_quantita_scarico = ld_quantita
	
	if mod(ld_quantita_scarico,12) <> 0 then
		ld_quan_mensile = int(ld_quantita_scarico / 12) + 1
	else
		ld_quan_mensile = int(ld_quantita_scarico / 12)
	end if
	
	ld_quan_cumulata = 0
	ld_quan_riporto = 0
	
	luo_mag = create uo_magazzino
	
	do while ll_index <= 12
		
		//verifico se devo fare il controllo della giacenza
		if cbx_giacenza.checked then
			
			ld_quan_periodo = 0
			//trovo la minor giacenza nei prossimi mesi
			for ll_i = ll_index to 12
				if ll_i = ll_index then 
					ld_mingiacenza = ld_giacenza[ll_i]
					ld_quan_cumulata = ld_quan_mensile + ld_quan_riporto
					ll_mese_in_esame = ll_i
					ld_quan_riporto = 0
				else
					if ld_giacenza[ll_i] <= ld_mingiacenza then 
						ll_mese_in_esame = ll_i
						ld_quan_cumulata = ld_quan_cumulata + ld_quan_mensile + ld_quan_periodo
						ld_mingiacenza = ld_giacenza[ll_i]
						ld_quan_periodo = 0
					else
						ld_quan_periodo = ld_quan_periodo + ld_quan_mensile
					end if
				end if
			next
			
			// una volta uscito so qual'è il mese che ha la minor giacenza
			// quindi se la minor giacenza è <= 0 riparto dal mese mese successivo, altrimenti
			// scarico la minor giacenza e riaprto dal mese successivo
			
			ll_index = ll_mese_in_esame
			
			if ld_mingiacenza <= 0 then
				ll_index ++
				ld_quan_riporto = ld_quan_cumulata
				continue
			end if
			
			if ld_quan_cumulata > ld_giacenza[ll_index] then
				ld_quan_movimento = ld_giacenza[ll_index]		// prendo la giacenza
				ld_quan_cumulata = ld_quan_cumulata - ld_quan_movimento
			else
				if ll_mese_in_esame = 12 then
					if ld_quan_cumulata >= ld_giacenza[12] then
						ld_quan_movimento = ld_giacenza[12]
					else
						ld_quan_movimento = ld_quan_cumulata
					end if
				else
					ld_quan_movimento = ld_quan_cumulata			// prendo tutta la quan cumulata
				end if
				ld_quan_cumulata = 0
			end if
			
		else // nessun controllo giacenza
			
			
			ll_mese_in_esame = ll_index  	// avanti di mese in mese
			ld_quan_movimento = ld_quan_mensile
			
		end if
		
		if ld_quantita_scarico >= ld_quan_movimento then
			ld_quantita_scarico = ld_quantita_scarico - ld_quan_movimento
		else
			ld_quan_movimento = ld_quantita_scarico
			ld_quantita_scarico = 0
		end if
		
		ld_quan_riporto = ld_quan_cumulata
		// eseguo il movimento alla fine del mese 
		
		ldt_data_reg_mov = ldt_fine[ll_mese_in_esame]
		ls_referenza   = ""
		
		setnull(ls_cod_deposito[1])
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
		
		//Donato 05/01/2009 inizializzo il vettore con il deposito da scaricare/caricare
		ls_cod_deposito[1] = ls_cod_deposito_corrente
		if sle_cod_ubz_terzista.text <> "" and not isnull(sle_cod_ubz_terzista.text) then
			ls_cod_ubicazione[1] = sle_cod_ubz_terzista.text
		end if
		
//		if if cbx_giacenza.checked then
//			//movimento su deposito di terzista: la ubicazione è il codice fornitore
//			ls_cod_ubicazione[1] = ""
//		else
//			//movimento su deposito interno
//			ls_cod_ubicazione[1] = "UB0001"
//		end if
//		ls_cod_lotto[1] = "LT0001"
		
		if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
												
			g_mb.messagebox("APICE", "Errore: f_crea_dest_mov_magazzino")
			ROLLBACK;
			return
		end if
		
		if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
										 ll_num_reg_dest_stock, &
										 ls_cod_tipo_movimento, &
										 ls_cod_prodotto) = -1 then
										 
			g_mb.messagebox("APICE", "Errore: f_verifica_dest_mov_mag")
			ROLLBACK;
			return
		end if
		
		if luo_mag.uof_movimenti_mag ( ldt_data_reg_mov, &
									ls_cod_tipo_movimento, &
									"S", &
									ls_cod_prodotto, &
									ld_quan_movimento, &
									0, &
									0, &
									ldt_data_reg_mov, &
									ls_referenza, &
									ll_anno_reg_dest_stock, &
									ll_num_reg_dest_stock, &
									ls_cod_deposito[], &
									ls_cod_ubicazione[], &
									ls_cod_lotto[], &
									ldt_data_stock[], &
									ll_prog_stock[], &
									ls_cod_fornitore[], &
									ls_cod_cliente[], &
									ref ll_anno_reg_mov[], &
									ref ll_num_reg_mov[] ) = 0 then
			COMMIT;
			
			ls_stringa = ls_cod_prodotto + " - " + em_mov_scarichi.text + " - Reg. " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " Data Reg " + string(ldt_data_reg_mov,"dd/mm/yyyy")
			ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
			filewrite(ll_filelog, ls_stringa)
			fileclose(ll_filelog)
		
			if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock) = -1 then
				ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
			end if
			COMMIT;
		else
			ROLLBACK;
			
			ls_stringa = ls_cod_prodotto + " - " + em_mov_scarichi.text + " movimento non eseseguito a causa di un errore."
			ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
			filewrite(ll_filelog, ls_stringa)
			fileclose(ll_filelog)
		end if
		
		if ld_quantita_scarico <= 0 then exit	/// finita la quantià da scaricare ... fuori!!!!

		
		// DOPO AVER FATTO IL MOVIMENTO METTO A POSTO LE GIACENZE
		
		for ll_t = ll_index to 12
			ld_giacenza[ll_t] = ld_giacenza[ll_t] - ld_quan_movimento
		next
		
		// passo al mese successivo
		
		commit;
		
		ll_index ++
		
	loop	
	
	destroy luo_mag
	
loop

FILECLOSE(ll_file)
setpointer(Arrow!)

RUN("notepad.exe " + em_LOG.text)


/*
string ls_cod_prodotto,ls_cod_tipo_mov_scarico_pf,ls_referenza, ls_cod_deposito[], &
       ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[],ls_stringa, &
		 ls_cod_tipo_movimento,ls_chiave[],ls_where, ls_errore
long	ll_i, ll_rows, ll_filelog, ll_prog_stock[], ll_anno_reg_mov[], ll_num_reg_mov[], &
      ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_file,ll_err,ll_pos,ll_y, ll_index, &
		ll_mese_in_esame, ll_t
dec{4} ld_quantita,ld_giacenza[12], ld_quant_val[],ld_giacenza_stock[], ld_costo_medio_stock[],&
		ld_quan_costo_medio_stock[], ld_mingiacenza, ld_quan_mensile, ld_quan_cumulata,ld_quan_movimento, &
		ld_quantita_scarico,ld_quan_riporto, ld_quan_periodo
datetime ldt_inizio, ldt_fine[],ldt_data_reg_mov,ldt_data_stock[]
uo_magazzino luo_mag
long ll_num_stock, ll_indice
string ls_file

st_legenda.text = ""

g_mb.messagebox("APICE", "Procedo? Hai controllato le date e l'anno ?")

filedelete(em_LOG.text)

ls_file = em_file.text
ll_file = fileopen(em_file.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File")
	return
end if

ls_cod_tipo_movimento = em_mov_scarichi.text

setpointer(HourGlass!)

do while true
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	
	st_legenda.text = ls_cod_prodotto
	
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	ld_quantita = dec(ls_stringa)
	
	//	faccio 12 inventari per il prodotto
	for ll_i = 1 to 12 
		// creazione limiti date
		choose case ll_i
			case 4,6,9,11
				ldt_fine[ll_i] = datetime(date(string("30/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
			case 2
				ldt_fine[ll_i] = datetime(date(string("29/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
			case else
				ldt_fine[ll_i] = datetime(date(string("31/" + string(ll_i) + "/" + em_anno.text)),00:00:00)
		end choose
			
		// verifico se devo fare il controllo della giacenza
		if cbx_giacenza.checked then
			for ll_y = 1 to 14
				ld_quant_val[ll_y] = 0
			next
			
			luo_mag = CREATE uo_magazzino
			
			ll_err = luo_mag.uof_saldo_prod_date_decimal(ls_cod_prodotto, &
																			ldt_fine[ll_i], &
																			ls_where, &
																			ld_quant_val[], &
																			ls_errore, &
																			"N", &
																			ls_chiave[], &
																			ld_giacenza_stock[], &
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])
					
			ld_giacenza[ll_i] = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			
			destroy luo_mag
			
		end if		
		
	next
	
	ll_index = 1		// è il mese in esame
	ld_mingiacenza = 0
	ld_quantita_scarico = ld_quantita
	
	if mod(ld_quantita_scarico,12) <> 0 then
		ld_quan_mensile = int(ld_quantita_scarico / 12) + 1
	else
		ld_quan_mensile = int(ld_quantita_scarico / 12)
	end if
	
	ld_quan_cumulata = 0
	ld_quan_riporto = 0
	
	luo_mag = create uo_magazzino
	
	do while ll_index <= 12
		
		// verifico de devo fare il controllo della giacenza
		if cbx_giacenza.checked then
			
			ld_quan_periodo = 0
			// trovo la minor giacenza nei prossimi mesi
			for ll_i = ll_index to 12
				if ll_i = ll_index then 
					ld_mingiacenza = ld_giacenza[ll_i]
					ld_quan_cumulata = ld_quan_mensile + ld_quan_riporto
					ll_mese_in_esame = ll_i
					ld_quan_riporto = 0
				else
					if ld_giacenza[ll_i] <= ld_mingiacenza then 
						ll_mese_in_esame = ll_i
						ld_quan_cumulata = ld_quan_cumulata + ld_quan_mensile + ld_quan_periodo
						ld_mingiacenza = ld_giacenza[ll_i]
						ld_quan_periodo = 0
					else
						ld_quan_periodo = ld_quan_periodo + ld_quan_mensile
					end if
				end if
			next
			
			// una volta uscito so qual'è il mese che ha la minor giacenza
			// quindi se la minor giacenza è <= 0 riparto dal mese mese successivo, altrimenti
			// scarico la minor giacenza e riaprto dal mese successivo
			
			ll_index = ll_mese_in_esame
			
			if ld_mingiacenza <= 0 then
				ll_index ++
				ld_quan_riporto = ld_quan_cumulata
				continue
			end if
			
			if ld_quan_cumulata > ld_giacenza[ll_index] then
				ld_quan_movimento = ld_giacenza[ll_index]		// prendo la giacenza
				ld_quan_cumulata = ld_quan_cumulata - ld_quan_movimento
			else
				if ll_mese_in_esame = 12 then
					if ld_quan_cumulata >= ld_giacenza[12] then
						ld_quan_movimento = ld_giacenza[12]
					else
						ld_quan_movimento = ld_quan_cumulata
					end if
				else
					ld_quan_movimento = ld_quan_cumulata			// prendo tutta la quan cumulata
				end if
				ld_quan_cumulata = 0
			end if
			
		else // nessun controllo giacenza
			
			
			ll_mese_in_esame = ll_index  	// avanti di mese in mese
			ld_quan_movimento = ld_quan_mensile
			
		end if
		
		if ld_quantita_scarico >= ld_quan_movimento then
			ld_quantita_scarico = ld_quantita_scarico - ld_quan_movimento
		else
			ld_quan_movimento = ld_quantita_scarico
			ld_quantita_scarico = 0
		end if
		
		ld_quan_riporto = ld_quan_cumulata
		// eseguo il movimento alla fine del mese 
		
		ldt_data_reg_mov = ldt_fine[ll_mese_in_esame]
		ls_referenza   = ""
		
		setnull(ls_cod_deposito[1])
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
		
		
		if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
			ROLLBACK;
			return -1
		end if
		
		if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
										 ll_num_reg_dest_stock, &
										 ls_cod_tipo_movimento, &
										 ls_cod_prodotto) = -1 then
			ROLLBACK;
			return 0
		end if
		
		
		if luo_mag.uof_movimenti_mag ( ldt_data_reg_mov, &
									ls_cod_tipo_movimento, &
									"S", &
									ls_cod_prodotto, &
									ld_quan_movimento, &
									0, &
									0, &
									ldt_data_reg_mov, &
									ls_referenza, &
									ll_anno_reg_dest_stock, &
									ll_num_reg_dest_stock, &
									ls_cod_deposito[], &
									ls_cod_ubicazione[], &
									ls_cod_lotto[], &
									ldt_data_stock[], &
									ll_prog_stock[], &
									ls_cod_fornitore[], &
									ls_cod_cliente[], &
									ref ll_anno_reg_mov[], &
									ref ll_num_reg_mov[] ) = 0 then
			COMMIT;
			
			ls_stringa = ls_cod_prodotto + " - " + em_mov_scarichi.text + " - Reg. " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " Data Reg " + string(ldt_data_reg_mov,"dd/mm/yyyy")
			ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
			filewrite(ll_filelog, ls_stringa)
			fileclose(ll_filelog)
		
			if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock) = -1 then
				ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
			end if
			COMMIT;
		else
			ROLLBACK;
			
			ls_stringa = ls_cod_prodotto + " - " + em_mov_scarichi.text + " movimento non eseseguito a causa di un errore."
			ll_filelog = fileopen(em_LOG.text, LINEMODE!, WRITE!)
			filewrite(ll_filelog, ls_stringa)
			fileclose(ll_filelog)
		end if
		
		if ld_quantita_scarico <= 0 then exit	/// finita la quantià da scaricare ... fuori!!!!

		
		// DOPO AVER FATTO IL MOVIMENTO METTO A POSTO LE GIACENZE
		
		for ll_t = ll_index to 12
			ld_giacenza[ll_t] = ld_giacenza[ll_t] - ld_quan_movimento
		next
		
		// passo al mese successivo
		
		commit;
		
		ll_index ++
		
	loop	
	
	destroy luo_mag
	
loop

FILECLOSE(ll_file)
setpointer(Arrow!)

RUN("notepad.exe " + em_LOG.text)
*/
end event

type em_fine from editmask within w_aggiusta_magazzino_con_deposito
integer x = 1568
integer y = 1504
integer width = 375
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_4 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 1307
integer y = 1508
integer width = 238
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "FINE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 571
integer y = 1520
integer width = 238
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "INIZIO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_inizio from editmask within w_aggiusta_magazzino_con_deposito
integer x = 823
integer y = 1504
integer width = 375
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_2 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 114
integer y = 156
integer width = 279
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "LOG:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_log from editmask within w_aggiusta_magazzino_con_deposito
integer x = 402
integer y = 144
integer width = 2126
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "c:\temp\aggiustamag.log"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_1 from statictext within w_aggiusta_magazzino_con_deposito
integer x = 23
integer y = 1428
integer width = 2318
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Esame dei prodotti inseriti in bolle, ma non collegati; creazione automatica dei movimenti."
boolean focusrectangle = false
end type

type cb_non_collegati from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 23
integer y = 1504
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Non Collegati"
end type

event clicked;string ls_cod_prodotto,ls_cod_tipo_movimento,ls_referenza, ls_cod_deposito[], &
       ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente[],ls_stringa
long	ll_i, ll_rows,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_bol_ven, ll_file, &
		ll_prog_stock[], ll_anno_reg_mov[], ll_num_reg_mov[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock
dec{4} ld_quan_consegnata
datetime ldt_inizio, ldt_fine,ldt_data_bolla,ldt_data_stock[]
datastore lds_bolle 
uo_magazzino luo_mag


ldt_inizio = datetime(date(em_inizio.text), 00:00:00) 
ldt_fine   = datetime(date(em_fine.text), 00:00:00) 

g_mb.messagebox("APICE", "Procedo? Hai controllato le date?")

filedelete(em_LOG.text)

lds_bolle = create datastore
lds_bolle.dataobject = 'd_ds_aggiunta_magazzino_1'

lds_bolle.settransobject(sqlca)

ll_rows = lds_bolle.retrieve(s_cs_xx.cod_azienda, ldt_inizio, ldt_fine)

for ll_i = 1 to ll_rows
	ll_anno_registrazione = lds_bolle.getitemnumber(ll_i, "det_bol_ven_anno_registrazione")
	ll_num_registrazione = lds_bolle.getitemnumber(ll_i, "det_bol_ven_num_registrazione")
	ll_prog_riga_bol_ven = lds_bolle.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")
	ls_cod_prodotto = lds_bolle.getitemstring(ll_i, "det_bol_ven_cod_prodotto")
	ld_quan_consegnata =lds_bolle.getitemnumber(ll_i, "det_bol_ven_quan_consegnata")
	ls_cod_tipo_movimento = lds_bolle.getitemstring(ll_i, "tab_tipi_det_ven_cod_tipo_movimento")
	ldt_data_bolla = lds_bolle.getitemdatetime(ll_i, "tes_bol_ven_data_bolla")
	ls_referenza = "BV" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_bol_ven)
	
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		ROLLBACK;
		return -1
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		ROLLBACK;
		return 0
	end if
	
	luo_mag = create uo_magazzino
	
	// FORZO LA POSSIBILITA' DI ANDARE IN NEGATIVO
	luo_mag.ib_negativo = TRUE
	
	if luo_mag.uof_movimenti_mag ( ldt_data_bolla, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quan_consegnata, &
								0, &
								ll_num_registrazione, &
								ldt_data_bolla, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ref ll_anno_reg_mov[], &
								ref ll_num_reg_mov[] ) = 0 then
		COMMIT;
		
		ls_stringa = "Movimento " + string(ll_anno_reg_mov[1]) + "/" + string(ll_num_reg_mov[1]) + " relativo alla bolla " + ls_referenza + " " + string(ldt_data_bolla,"dd/mm/yyyy")
		ll_file = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_file, ls_stringa)
		fileclose(ll_file)
	
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock) = -1 then
			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
		end if
		COMMIT;
	else
		ROLLBACK;
		
		ls_stringa = "Riga Bolla " + ls_referenza + " movimento non eseseguito a causa di un errore. DATA="+ string(ldt_data_bolla) +" PRODOTTO=" + ls_cod_prodotto + " QUAN=" + STRING(ld_quan_consegnata)
		ll_file = fileopen(em_LOG.text, LINEMODE!, WRITE!)
		filewrite(ll_file, ls_stringa)
		fileclose(ll_file)
	end if
	
	destroy luo_mag
next

RUN("notepad.exe " + em_LOG.text)
end event

type em_file from editmask within w_aggiusta_magazzino_con_deposito
integer x = 402
integer y = 28
integer width = 2126
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_1 from commandbutton within w_aggiusta_magazzino_con_deposito
integer x = 18
integer y = 28
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Apri"
end type

event clicked;string docpath, docname[]

integer i, li_cnt, li_rtn, li_filenum

 

li_rtn = GetFileOpenName( "Seleziona File Scarichi", docpath, docname[])

 

em_file.text = ""

IF li_rtn < 1 THEN return

li_cnt = Upperbound(docname)

if li_cnt = 1 then

   em_file.text = string(docpath)

end if


end event

type ln_1 from line within w_aggiusta_magazzino_con_deposito
long linecolor = 33554432
integer linethickness = 6
integer beginx = 37
integer beginy = 1388
integer endx = 2569
integer endy = 1388
end type

type ln_2 from line within w_aggiusta_magazzino_con_deposito
long linecolor = 33554432
integer linethickness = 6
integer beginx = 23
integer beginy = 792
integer endx = 2555
integer endy = 792
end type

type r_1 from rectangle within w_aggiusta_magazzino_con_deposito
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 1688
integer width = 2674
integer height = 264
end type

