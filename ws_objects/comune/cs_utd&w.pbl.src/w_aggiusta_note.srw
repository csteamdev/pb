﻿$PBExportHeader$w_aggiusta_note.srw
forward
global type w_aggiusta_note from w_cs_xx_principale
end type
type mle_1 from multilineedit within w_aggiusta_note
end type
type dw_1 from datawindow within w_aggiusta_note
end type
end forward

global type w_aggiusta_note from w_cs_xx_principale
integer width = 2066
integer height = 1648
mle_1 mle_1
dw_1 dw_1
end type
global w_aggiusta_note w_aggiusta_note

forward prototypes
public subroutine wf_log (string as_info, string as_message)
public function integer wf_note_off_ven (long al_anno_registrazione, long al_num_registrazione_da, long al_num_registrazione_a)
end prototypes

public subroutine wf_log (string as_info, string as_message);

mle_1.text += as_info + "~t" + as_message + "~r~n"

mle_1.scroll(mle_1.LineCount())
end subroutine

public function integer wf_note_off_ven (long al_anno_registrazione, long al_num_registrazione_da, long al_num_registrazione_a);string ls_nota_testata, ls_nota_piede
long ll_num_registrazione

wf_log("", "-------------")
wf_log("I", "Processo note offerte vendita da " + string(al_num_registrazione_da) + " al " + string(al_num_registrazione_a))

for ll_num_registrazione = al_num_registrazione_da to al_num_registrazione_a
	
	select nota_testata, nota_piede
	into :ls_nota_testata, :ls_nota_piede
	from tes_off_ven
	where 
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione =:ll_num_registrazione;
		
	if sqlca.sqlcode = 100 then
		continue
	end if
		
	wf_log("I", g_str.format("Processo offerte vendita: $1/$2", al_anno_registrazione,ll_num_registrazione))
		
	ls_nota_testata = guo_functions.uof_string_to_rte(ls_nota_testata)
	ls_nota_piede = guo_functions.uof_string_to_rte(ls_nota_piede)
	
	update tes_off_ven
	set 
		nota_testata = :ls_nota_testata,
		nota_piede = :ls_nota_piede
	where 
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione =:ll_num_registrazione;
		
	if sqlca.sqlcode < 0 then
		wf_log("E", g_str.format("Errore aggiornamento offerte vendita: $1/$2. $3", al_anno_registrazione,ll_num_registrazione, sqlca.sqlerrtext))
		rollback;
		return -1
	elseif sqlca.sqlcode = 0 then
		commit;
	end if
	
next

return 1	
end function

on w_aggiusta_note.create
int iCurrent
call super::create
this.mle_1=create mle_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mle_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_aggiusta_note.destroy
call super::destroy
destroy(this.mle_1)
destroy(this.dw_1)
end on

event resize;dw_1.move(0,0)
mle_1.move(0, dw_1.height + 20)
mle_1.resize(newwidth - 20, newheight - dw_1.height)
end event

event pc_setwindow;call super::pc_setwindow;dw_1.insertrow(0)
end event

type mle_1 from multilineedit within w_aggiusta_note
integer x = 41
integer y = 436
integer width = 1929
integer height = 1092
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_aggiusta_note
integer x = 37
integer y = 28
integer width = 1929
integer height = 344
integer taborder = 10
string title = "none"
string dataobject = "d_aggiusta_note"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;long ll_da, ll_a, ll_anno

choose case dwo.name
	case "b_off_ven"
		ll_anno = getitemnumber(1, "anno")
		ll_da = getitemnumber(1, "num_da_off_ven")
		ll_a = getitemnumber(1, "num_a_off_ven")
		
		if isnull(ll_da) or ll_da < 1 then
			select min(num_registrazione)
			into :ll_da
			from tes_off_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno;
					
			dw_1.setitem(1, "num_da_off_ven", ll_da)
		end if
		
		if isnull(ll_a) or ll_a < 1 then
			select max(num_registrazione)
			into :ll_a
			from tes_off_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno;
					
			dw_1.setitem(1, "num_a_off_ven", ll_a)
		end if
		
		if wf_note_off_ven(ll_anno, ll_da, ll_a) = 1 then
			g_mb.success("Procedura conclusa con successo")
		else
			g_mb.error("Errore, vedi log")
		end if
		
end choose
end event

