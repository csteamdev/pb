﻿$PBExportHeader$w_allinea_doc_passivi_con_magazzino.srw
forward
global type w_allinea_doc_passivi_con_magazzino from w_cs_xx_principale
end type
type st_2 from statictext within w_allinea_doc_passivi_con_magazzino
end type
type st_1 from statictext within w_allinea_doc_passivi_con_magazzino
end type
type cb_bol_acq from commandbutton within w_allinea_doc_passivi_con_magazzino
end type
type st_log from statictext within w_allinea_doc_passivi_con_magazzino
end type
type hpb_progress from hprogressbar within w_allinea_doc_passivi_con_magazzino
end type
type cb_fat_acq from commandbutton within w_allinea_doc_passivi_con_magazzino
end type
end forward

global type w_allinea_doc_passivi_con_magazzino from w_cs_xx_principale
integer width = 1531
integer height = 824
st_2 st_2
st_1 st_1
cb_bol_acq cb_bol_acq
st_log st_log
hpb_progress hpb_progress
cb_fat_acq cb_fat_acq
end type
global w_allinea_doc_passivi_con_magazzino w_allinea_doc_passivi_con_magazzino

forward prototypes
public subroutine wf_reset_log ()
public function boolean wf_allinea_fat_acq ()
public function boolean wf_allinea_bol_acq ()
end prototypes

public subroutine wf_reset_log ();hpb_progress.maxposition = 0
hpb_progress.position = 0
st_log.text = ""
end subroutine

public function boolean wf_allinea_fat_acq ();string  ls_cod_tipo_det_acq, ls_cod_prodotto, ls_flag_tipo_det_acq, &
		 ls_cod_prodotto_raggruppato
integer li_anno_bolla_acq
		 
long ll_prog_riga, ll_prog_bolla_acq, ll_prog_riga_bolla_acq, &
	  ll_num_bolla_acq, ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_mov_mag, &
	  ll_num_registrazione_mov_mag, ll_anno_reg_mov_rag, ll_num_reg_mov_rag
	  
dec{4} ld_sconto_testata, ld_cambio_acq, ld_quantita, ld_prezzo_acquisto, ld_sconto_1, &
       ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_pagamento, ld_val_sconto_1, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_riga_netto, ld_quan_ordinata, &
		 ld_num_pezzi_confezione, ld_quan_sconto_merce, ld_quan_raggruppo, ld_fat_conversione_rag_mag, &
		 ld_valore_unitario
		 
datetime ldt_data_documento

declare cu_det cursor for 

select anno_registrazione,
		num_registrazione,
		prog_riga_fat_acq ,
		cod_tipo_det_acq,
		cod_prodotto,
		quan_fatturata,
		prezzo_acquisto,
		sconto_1,
		sconto_2,
		sconto_3,
		sconto_4,
		sconto_5,
		sconto_6,
		sconto_7,
		sconto_8,
		sconto_9,
		sconto_10,
		anno_bolla_acq,
		num_bolla_acq,
		prog_bolla_acq,
		prog_riga_bolla_acq,
		num_pezzi_confezione,
		quan_sconto_merce,
		anno_registrazione_mov_mag,
		num_registrazione_mov_mag
from det_fat_acq  
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione_mov_mag is not null
order by prog_riga_fat_acq asc;

//and anno_registrazione = 2012 and
//num_registrazione = 326 and
//prog_riga_fat_acq = 20
		
open cu_det;

do while 0 = 0
	fetch cu_det into :ll_anno_registrazione, 
		:ll_num_registrazione,
		:ll_prog_riga,   
		:ls_cod_tipo_det_acq, 
		:ls_cod_prodotto,   
		:ld_quantita,   
		:ld_prezzo_acquisto,   
		:ld_sconto_1,   
		:ld_sconto_2,  
		:ld_sconto_3,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,
		:li_anno_bolla_acq,
		:ll_num_bolla_acq,
		:ll_prog_bolla_acq,
		:ll_prog_riga_bolla_acq,
		:ld_num_pezzi_confezione, 
		:ld_quan_sconto_merce,
		:ll_anno_registrazione_mov_mag,
		:ll_num_registrazione_mov_mag;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		g_mb.error("Errore durante la lettura Dettagli.", sqlca)
		close cu_det;
		return false
	end if
	
	hpb_progress.position ++
	st_log.text = "Analisi fattura " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga)
	yield()
	
	select cambio_acq, sconto
	into :ld_cambio_acq, :ld_sconto_testata
	from tes_fat_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	
	if ld_quan_sconto_merce > 0 and ld_num_pezzi_confezione > 0 then
		ld_quantita = ld_quantita + (ld_quan_sconto_merce * ld_num_pezzi_confezione)
	end if

	select tab_tipi_det_acq.flag_tipo_det_acq
	into :ls_flag_tipo_det_acq
	from tab_tipi_det_acq
	where tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		 g_mb.error("Si è verificato un errore in fase lettura tabella tipi dettaglio acquisto.",sqlca)
		close cu_det;
		return false
	end if

//	if ls_flag_tipo_det_acq = "M" and &
//		(isnull(li_anno_bolla_acq) or li_anno_bolla_acq = 0) and &
//		(isnull(ll_num_bolla_acq) or ll_num_bolla_acq = 0) and &
//		(isnull(ll_prog_bolla_acq) or ll_prog_bolla_acq = 0 ) and &
//		(isnull(ll_prog_riga_bolla_acq) or ll_prog_riga_bolla_acq = 0 ) then

	if ls_flag_tipo_det_acq = "M" then

		ld_val_sconto_1 = (ld_prezzo_acquisto * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_prezzo_acquisto - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
		ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
		ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
		ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		ld_val_riga_netto = round((ld_val_riga_netto * ld_cambio_acq), 4)

		update mov_magazzino
		set
			quan_movimento = :ld_quantita,
			val_movimento = :ld_val_riga_netto
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione_mov_mag and
			num_registrazione = :ll_num_registrazione_mov_mag;
			
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante l'aggiornamento del movimento di magazzino del prodotto finito", sqlca)
			close cu_det;
			return false
		end if

		// ************************************************************************
		// Controllo raggruppato		
		setnull(ls_cod_prodotto_raggruppato)
	
		select cod_prodotto_raggruppato, fat_conversione_rag_mag
		into :ls_cod_prodotto_raggruppato, :ld_fat_conversione_rag_mag
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;			 
				 
		if isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato = "" then continue
		
		ld_valore_unitario = f_converti_qta_raggruppo(ls_cod_prodotto, ld_val_riga_netto, "D")
		ld_quantita = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quantita, "M")
		
		select anno_registrazione, num_registrazione
		into :ll_anno_reg_mov_rag, :ll_num_reg_mov_rag
		from mov_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_mov_origine = :ll_anno_registrazione_mov_mag and
				 num_reg_mov_origine = :ll_num_registrazione_mov_mag;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante la ricerca del movimento di magazzino del raggruppato.", sqlca)
			close cu_det;
			return false
		elseif sqlca.sqlcode = 100 then
			continue
		end if
		
		update mov_magazzino
		set
			quan_movimento = :ld_quantita,
			val_movimento = :ld_valore_unitario
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg_mov_rag and
			num_registrazione = :ll_num_reg_mov_rag;
			
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante l'aggiornamento del movimento di magazzino del raggruppato.", sqlca)
			close cu_det;
			return false
		end if
	end if
	
	//commit;
loop

close cu_det;

return true
end function

public function boolean wf_allinea_bol_acq ();string ls_cod_pagamento, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, ls_cod_prodotto, &
		 ls_cod_cliente, ls_valuta, ls_messaggio, ls_cod_prodotto_raggruppato
long ll_prog_riga, ll_anno_registrazione, ll_num_registrazione, &
		ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag,ll_anno_reg_mov_rag, &
		ll_num_reg_mov_rag, ll_count

dec{4} ld_sconto_testata, ld_cambio_acq, ld_quantita, ld_prezzo_acquisto, ld_sconto_1, &
       ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_pagamento, ld_val_sconto_1, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_riga_netto, ld_quan_ordinata, &
		 ld_quan_raggruppo, ld_prezzo_raggruppo
		 
uo_condizioni_cliente luo_arrotonda

declare cu_det cursor for 
select anno_registrazione,
		num_registrazione,
		prog_riga_bolla_acq ,
		cod_tipo_det_acq,
		quan_arrivata,
		prezzo_acquisto,
		sconto_1,
		sconto_2,
		sconto_3,
		sconto_4,
		sconto_5,
		sconto_6,
		sconto_7,
		sconto_8,
		sconto_9,
		sconto_10,
		anno_registrazione_mov_mag,
		num_registrazione_mov_mag,
		cod_prodotto
from det_bol_acq  
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione_mov_mag is not null and
	num_registrazione_mov_mag is not null
order by prog_riga_bolla_acq asc;

open cu_det;

do while true
	fetch cu_det into :ll_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga,
		:ls_cod_tipo_det_acq,   
		:ld_quantita,   
		:ld_prezzo_acquisto,   
		:ld_sconto_1,   
		:ld_sconto_2,  
		:ld_sconto_3,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,
		:ll_anno_registrazione_mov_mag,
		:ll_num_registrazione_mov_mag,
		:ls_cod_prodotto;

	if sqlca.sqlcode = 100 then 
		exit
	elseif sqlca.sqlcode = -1 then
		g_mb.error("Errore durante la lettura Dettagli.", sqlca)
		close cu_det;
		return false
	end if
	
	hpb_progress.position ++
	st_log.text = "Analisi fattura " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga)
	yield()
	
	// Esiste una fattura collegata? se si allora salto il controllo
	select count(*)
	into :ll_count
	from det_fat_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_bolla_acq = :ll_anno_registrazione and
			 num_bolla_acq = :ll_num_registrazione;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo del riferimento con le fatture.", sqlca)
		close cu_det;
		return false
	elseif ll_count > 0 then
		// la bolla è referenziata in una riga di fattura.
		// non devo fare nulla.
		continue
	end if
	// -------
	
	select cambio_acq,
			cod_pagamento,
			sconto,
			cod_valuta
	into   	:ld_cambio_acq,
			:ls_cod_pagamento,
			:ld_sconto_testata,
			:ls_valuta
	from tes_bol_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
		anno_bolla_acq = :ll_anno_registrazione and
		num_bolla_acq = :ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante la lettura Testata.", sqlca)
		close cu_det;
		return false
	elseif sqlca.sqlcode = 100 then
		continue
	end if

	select sconto
	into :ld_sconto_pagamento
	from tab_pagamenti
	where cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_pagamento = :ls_cod_pagamento;
	
	if sqlca.sqlcode <> 0 then ld_sconto_pagamento = 0
		
	select tab_tipi_det_acq.flag_tipo_det_acq
	into   :ls_flag_tipo_det_acq
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		g_mb.error("Si è verificato un errore in fase lettura tabella tipi dettaglio acquisto.", sqlca)
		rollback;
		close cu_det;
		return false
	end if

	if ls_flag_tipo_det_acq = "M" then
		ld_val_sconto_1 = (ld_prezzo_acquisto * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_prezzo_acquisto - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
		ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
		ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
		ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		ld_val_riga_netto = ld_val_riga_netto * ld_cambio_acq
		
		luo_arrotonda = create uo_condizioni_cliente
		luo_arrotonda.uof_arrotonda_prezzo_decimal(ld_val_riga_netto,ls_valuta,ld_val_riga_netto,ls_messaggio)
		destroy luo_arrotonda	
		
		if ll_anno_registrazione_mov_mag > 0 then
			
			update mov_magazzino
			set
				quan_movimento = :ld_quantita,
				val_movimento = :ld_val_riga_netto
			where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione_mov_mag and
				num_registrazione = :ll_num_registrazione_mov_mag;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.error("Errore durante l'allineamento del movimento del prodotto finito.", sqlca)
				close cu_det;
				return false
			end if
			
		end if
		
		// ************************************************************************
		// Controllo raggruppato		
		setnull(ls_cod_prodotto_raggruppato)
	
		select cod_prodotto_raggruppato
		into :ls_cod_prodotto_raggruppato
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;			 
				 
		if isnull(ls_cod_prodotto_raggruppato) or ls_cod_prodotto_raggruppato = "" then continue
		
		ld_val_riga_netto = f_converti_qta_raggruppo(ls_cod_prodotto, ld_val_riga_netto, "D")
		ld_quantita = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quantita, "M")
		
		select anno_registrazione, num_registrazione
		into :ll_anno_reg_mov_rag, :ll_num_reg_mov_rag
		from mov_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_reg_mov_origine = :ll_anno_registrazione_mov_mag and
				 num_reg_mov_origine = :ll_num_registrazione_mov_mag;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante la ricerca del movimento di magazzino del raggruppato.", sqlca)
			close cu_det;
			return false
		elseif sqlca.sqlcode = 100 then
			continue
		end if
		
		update mov_magazzino
		set
			quan_movimento = :ld_quantita,
			val_movimento = :ld_val_riga_netto
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_reg_mov_rag and
			num_registrazione = :ll_num_reg_mov_rag;
			
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante l'aggiornamento del movimento di magazzino del raggruppato.", sqlca)
			close cu_det;
			return false
		end if

	end if
loop

close cu_det;

return true
end function

on w_allinea_doc_passivi_con_magazzino.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.cb_bol_acq=create cb_bol_acq
this.st_log=create st_log
this.hpb_progress=create hpb_progress
this.cb_fat_acq=create cb_fat_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_bol_acq
this.Control[iCurrent+4]=this.st_log
this.Control[iCurrent+5]=this.hpb_progress
this.Control[iCurrent+6]=this.cb_fat_acq
end on

on w_allinea_doc_passivi_con_magazzino.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_bol_acq)
destroy(this.st_log)
destroy(this.hpb_progress)
destroy(this.cb_fat_acq)
end on

type st_2 from statictext within w_allinea_doc_passivi_con_magazzino
integer x = 46
integer y = 280
integer width = 1394
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Le righe non devono superare il limite dei 50.000"
boolean focusrectangle = false
end type

type st_1 from statictext within w_allinea_doc_passivi_con_magazzino
integer x = 46
integer y = 20
integer width = 1394
integer height = 200
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Le procedure controllano tutte le righe dei documenti e ricalcolano la quantità ed il valore dei movimenti di magazzino"
boolean focusrectangle = false
end type

type cb_bol_acq from commandbutton within w_allinea_doc_passivi_con_magazzino
integer x = 777
integer y = 400
integer width = 686
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allinea Bolle Acquisto"
end type

event clicked;long ll_count

wf_reset_log()

select count(*)
into :ll_count
from det_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione_mov_mag is not null and
	num_registrazione_mov_mag is not null;

hpb_progress.maxposition = ll_count

if wf_allinea_bol_acq() then
	st_log.text = "Processo completato"
	//commit;
	rollback;
else
	rollback;
end if
end event

type st_log from statictext within w_allinea_doc_passivi_con_magazzino
integer x = 46
integer y = 620
integer width = 1394
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
boolean focusrectangle = false
end type

type hpb_progress from hprogressbar within w_allinea_doc_passivi_con_magazzino
integer x = 46
integer y = 540
integer width = 1417
integer height = 60
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_fat_acq from commandbutton within w_allinea_doc_passivi_con_magazzino
integer x = 46
integer y = 400
integer width = 686
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allinea Fatture Acquisto"
end type

event clicked;long ll_count

wf_reset_log()

select count(*)
into :ll_count
from det_fat_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione_mov_mag is not null;

hpb_progress.maxposition = ll_count

if wf_allinea_fat_acq() then
	st_log.text = "Processo completato"
	commit;
else
	rollback;
end if
end event

