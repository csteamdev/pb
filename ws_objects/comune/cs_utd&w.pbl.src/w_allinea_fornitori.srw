﻿$PBExportHeader$w_allinea_fornitori.srw
forward
global type w_allinea_fornitori from w_cs_xx_principale
end type
type cb_stop_insert from commandbutton within w_allinea_fornitori
end type
type cb_start_insert from commandbutton within w_allinea_fornitori
end type
type st_11 from statictext within w_allinea_fornitori
end type
type st_10 from statictext within w_allinea_fornitori
end type
type cb_stop_update from commandbutton within w_allinea_fornitori
end type
type st_7 from statictext within w_allinea_fornitori
end type
type st_log from statictext within w_allinea_fornitori
end type
type cbx_header from checkbox within w_allinea_fornitori
end type
type cb_start_update from commandbutton within w_allinea_fornitori
end type
type st_6 from statictext within w_allinea_fornitori
end type
type st_5 from statictext within w_allinea_fornitori
end type
type st_file from statictext within w_allinea_fornitori
end type
type cb_selezione from commandbutton within w_allinea_fornitori
end type
type st_2 from statictext within w_allinea_fornitori
end type
type st_1 from statictext within w_allinea_fornitori
end type
end forward

global type w_allinea_fornitori from w_cs_xx_principale
integer width = 2491
integer height = 1772
string title = "Allinea Fornitori"
cb_stop_insert cb_stop_insert
cb_start_insert cb_start_insert
st_11 st_11
st_10 st_10
cb_stop_update cb_stop_update
st_7 st_7
st_log st_log
cbx_header cbx_header
cb_start_update cb_start_update
st_6 st_6
st_5 st_5
st_file st_file
cb_selezione cb_selezione
st_2 st_2
st_1 st_1
end type
global w_allinea_fornitori w_allinea_fornitori

type variables
constant string LR = "~r~n"

private:
	string is_desktop
	string is_file_excel
	uo_excel iuo_excel
	uo_log	iuo_log
	boolean ib_stop = false
end variables

forward prototypes
public subroutine wf_abilita_pulsanti (boolean ab_enabled)
public function integer wf_insert_fornitori (ref string as_error)
public function integer wf_update_fornitori (ref string as_error)
end prototypes

public subroutine wf_abilita_pulsanti (boolean ab_enabled);cb_start_insert.enabled = ab_enabled
cb_start_update.enabled = ab_enabled


if ab_enabled = false then
	st_log.text = "Pronto"
	cb_stop_update.visible = false
	cb_stop_insert.visible = false
end if
end subroutine

public function integer wf_insert_fornitori (ref string as_error);/**
 * enme
 * 17/09/2015
 *
 * Inserimento fornitori da foglio Excel
 * 
 * Return: -1 errore, 1 ok
 **/
 
constant uint COL_LOG = 30

string ls_test, ls_log, ls_cod_fornitore, ls_rag_soc_1,ls_rag_soc_2, ls_partita_iva, ls_cod_fiscale, ls_indirizzo, ls_cap, ls_frazione, ls_localita, ls_provincia, &
		ls_telefono, ls_fax,ls_cellulare, ls_mail, ls_cod_pagamento,ls_cin,ls_paese, ls_cin_euro,ls_conto_corrente, ls_flag_tipo_fornitore, &
		ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, ls_mail_amministrazione, ls_iban, ls_stato_estero, ls_abi,ls_cab, ls_sito_web, ls_cod_nazione, &
		ls_rif_interno
int ll_i
ulong ll_row
long	ll_cont, ll_cont_banca

ll_row = 0

select 	count(*)
into		:ll_cont_banca
from		anag_banche_clien_for
where	 	cod_azienda = :s_cs_xx.cod_azienda ;


if isnull(ll_cont_banca) then ll_cont_banca = 0

if cbx_header.checked then ll_row++
 
do while true
	ll_row++
	st_log.text = "Elaboro riga: " + string(ll_row)
	
	ls_cod_fornitore = string(iuo_excel.uof_read(ll_row, "A"))
	if ib_stop or g_str.isempty(ls_cod_fornitore) then exit
	
	ls_log = "Riga " + string(ll_row) + " FORNITORE " + ls_cod_fornitore + " "
	yield()
	
	select	cod_fornitore
	into 	:ls_test
	from 	anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante il controllo esistenza fornitore " + g_str.safe(ls_cod_fornitore) + "." + LR + sqlca.sqlerrtext
		iuo_log.error(ls_log + as_error)
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		return -1
	elseif sqlca.sqlcode = 0 then
		iuo_log.info(ls_log + " già esistente")
		iuo_excel.uof_set(ll_row, COL_LOG, " Già esistente")
		continue
	end if	
	
	setnull(ls_cod_fiscale)
	setnull(ls_partita_iva)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_stato_estero)
	setnull(ls_localita)
	setnull(ls_frazione)	
	
	// Leggo variabili
	ls_rag_soc_1 = left(string(iuo_excel.uof_read(ll_row, "B")),40)
	ls_rag_soc_2 = left(trim(string(iuo_excel.uof_read(ll_row, "C"))),40)
	ls_partita_iva = trim(string(iuo_excel.uof_read(ll_row, "D")))
	ls_cod_fiscale = trim(string(iuo_excel.uof_read(ll_row, "E")))
	ls_indirizzo = left(string(iuo_excel.uof_read(ll_row, "F")),40)
	ls_cap = left(string(iuo_excel.uof_read(ll_row, "G")),6)
	ls_frazione = left(string(iuo_excel.uof_read(ll_row, "H")),40)
	ls_localita = left(string(iuo_excel.uof_read(ll_row, "I")),40)
	ls_provincia = left(string(iuo_excel.uof_read(ll_row, "J")),2)
	ls_telefono = left(string(iuo_excel.uof_read(ll_row, "K"))	,20)
	ls_fax = left(string(iuo_excel.uof_read(ll_row, "L")),20)
	ls_cellulare = left(string(iuo_excel.uof_read(ll_row, "M")),20)
	ls_mail = string(iuo_excel.uof_read(ll_row, "N"))
	ls_sito_web = string(iuo_excel.uof_read(ll_row, "O"))
	ls_cod_nazione = left(string(iuo_excel.uof_read(ll_row, "P")),40)
	ls_rif_interno = string(iuo_excel.uof_read(ll_row, "Q"))
	ls_cod_pagamento = left(string(iuo_excel.uof_read(ll_row, "R")),40)
	ls_abi = string(iuo_excel.uof_read(ll_row, "S"))
	ls_cab = string(iuo_excel.uof_read(ll_row, "T"))
	ls_cin = string(iuo_excel.uof_read(ll_row, "U"))
	ls_paese = string(iuo_excel.uof_read(ll_row, "V"))
	ls_cin_euro = string(iuo_excel.uof_read(ll_row, "W"))
	ls_conto_corrente = string(iuo_excel.uof_read(ll_row, "X"))
	ls_iban = string(iuo_excel.uof_read(ll_row, "Y"))
	
	
	
	if g_str.isempty(ls_cod_pagamento) or ls_cod_pagamento = " " then setnull(ls_cod_pagamento)
	if g_str.isempty(ls_cin) or ls_cin = " " then setnull(ls_cin)
	if g_str.isempty(ls_paese) or ls_paese = " " then setnull(ls_paese)
	if g_str.isempty(ls_cin_euro) or ls_cin_euro = " " then setnull(ls_cin_euro)
	if g_str.isempty(ls_abi) or ls_abi = " " then setnull(ls_abi)
	if g_str.isempty(ls_cab) or ls_cab = " " then setnull(ls_cab)
	if g_str.isempty(ls_conto_corrente) or ls_conto_corrente = " " then setnull(ls_conto_corrente)
	
	//
	if not isnull(ls_cod_nazione) then
		insert into tab_nazioni
			(	cod_azienda,
				cod_nazione,
				des_nazione,
				flag_blocco,
				data_blocco)
		values
			(	:s_cs_xx.cod_azienda,
				:ls_cod_nazione,
				'NUOVO',
				'N',
				null);
	end if			
	
	
	// sistema il tipo fornitore
	if not isnull(ls_partita_iva) then
		if left(upper(ls_partita_iva),1) > "A" and left(upper(ls_partita_iva),1) < "Z" then // è un estero perchè ha la partita iva che inizia per lettere
			ls_cod_fiscale = ls_partita_iva
			setnull(ls_partita_iva)
			ls_flag_tipo_fornitore = "C"
			ls_stato_estero = ls_localita
			ls_localita = ls_frazione
			setnull(ls_frazione)
		else
			if left(upper(ls_cod_fiscale),1) > "A" and left(upper(ls_cod_fiscale),1) < "Z" then	// trattasti di persona fisica
				ls_flag_tipo_fornitore = "F"
			else
				ls_flag_tipo_fornitore = "S"		// si tratta di una persona giurudica
			end if
		end if
	
	else
		// se la partita iva è nella e c'è un CF allora è un privato
		if not isnull(ls_cod_fiscale) then
			ls_flag_tipo_fornitore = 'P'
		else
			as_error = "Partita iva e codice fiscale non inseriti: il fornitore viene saltato " + g_str.safe(ls_cod_fornitore) + "." + LR + sqlca.sqlerrtext
			iuo_log.error(ls_log + as_error)
			iuo_excel.uof_set(ll_row, COL_LOG, as_error)
			continue
		end if
	end if
	
	ls_cod_tipo_listino_prodotto = '001'
	ls_mail_amministrazione = ls_mail
	setnull(ls_cod_banca_clien_for)
	
	if isnull(ls_cin) or isnull(ls_cab) or isnull(ls_abi)  or isnull(ls_cin_euro) or isnull(ls_paese) or isnull(ls_conto_corrente) then
      	setnull(ls_iban)
	end if

	// creazione banca
	if not isnull(ls_abi) and not isnull(ls_cab) then
		select tab_abi.des_abi
		into	:ls_test
		from 	tab_abi
				join tab_abicab on  tab_abi.cod_abi = tab_abicab.cod_abi
		where tab_abi.cod_abi = :ls_abi and
				 tab_abicab.cod_cab = :ls_cab;
				 
		if sqlca.sqlcode = 0 then
			
			select 	cod_banca_clien_for
			into		:ls_cod_banca_clien_for
			from		anag_banche_clien_for
			where	 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_abi = :ls_abi and
						cod_cab = :ls_cab;
			
			if sqlca.sqlcode = 100 then
				ll_cont_banca ++
				ls_cod_banca_clien_for = string(ll_cont_banca,"000")
			
				INSERT INTO dbo.anag_banche_clien_for  
					( cod_azienda,   
					  cod_banca_clien_for,   
					  des_banca,   
					  indirizzo,   
					  localita,   
					  cap,   
					  provincia,   
					  cod_abi,   
					  cod_cab,   
					  flag_blocco,   
					  data_blocco,   
					  cin,   
					  iban )  
					select :s_cs_xx.cod_azienda,
							:ls_cod_banca_clien_for,
							left(tab_abi.des_abi + ' ' + tab_abicab.agenzia, 80),
							tab_abicab.indirizzo,
							tab_abicab.localita,
							tab_abicab.cap,
							tab_abicab.provincia,
							tab_abi.cod_abi,
							tab_abicab.cod_cab,
							'N',
							null,
							:ls_cin,
							:ls_iban
					from 	tab_abi
							join tab_abicab on  tab_abi.cod_abi = tab_abicab.cod_abi
					where tab_abi.cod_abi = :ls_abi and
							 tab_abicab.cod_cab = :ls_cab;
							 
				if sqlca.sqlcode < 0 then
					as_error = "Inserire banca a mano: errore durante l'inserimento della banca fornitore ABI/CAB" + ls_abi + "/" + ls_cab + ". " + sqlca.sqlerrtext
					iuo_excel.uof_set(ll_row, COL_LOG, as_error)
					iuo_log.error(ls_log + as_error)
					setnull(ls_cod_banca_clien_for)
				end if
			end if
		else
			setnull(ls_cod_banca_clien_for)
		end if
	end if
	
	
	INSERT INTO anag_fornitori  
			( cod_azienda,   
			  cod_fornitore,   
			  rag_soc_1,   
			  rag_soc_2,   
			  indirizzo,   
			  localita,   
			  frazione,   
			  cap,   
			  provincia,   
			  telefono,   
			  fax,   
			  telex,   
			  cod_fiscale,   
			  partita_iva,   
			  rif_interno,   
			  flag_tipo_fornitore,   
			  cod_conto,   
			  cod_iva,   
			  num_prot_esenzione_iva,   
			  data_esenzione_iva,   
			  flag_sospensione_iva,   
			  cod_pagamento,   
			  giorno_fisso_scadenza,   
			  mese_esclusione_1,   
			  mese_esclusione_2,   
			  data_sostituzione_1,   
			  data_sostituzione_2,   
			  sconto,   
			  cod_tipo_listino_prodotto,   
			  fido,   
			  cod_banca_clien_for,   
			  conto_corrente,   
			  cod_lingua,   
			  cod_nazione,   
			  cod_area,   
			  cod_zona,   
			  cod_valuta,   
			  cod_categoria,   
			  cod_imballo,   
			  cod_porto,   
			  cod_resa,   
			  cod_mezzo,   
			  cod_vettore,   
			  cod_inoltro,   
			  cod_deposito,   
			  flag_certificato,   
			  flag_approvato,   
			  flag_strategico,   
			  flag_terzista,   
			  num_contratto,   
			  data_contratto,   
			  flag_ver_ispettiva,   
			  data_ver_ispettiva,   
			  note_ver_ispettiva,   
			  flag_omologato,   
			  flag_procedure_speciali,   
			  peso_val_servizio,   
			  peso_val_qualita,   
			  limite_tolleranza,   
			  flag_blocco,   
			  data_blocco,   
			  flag_tipo_certificazione,   
			  cod_piano_campionamento,   
			  casella_mail,   
			  nome_doc_qualificazione,   
			  sito_internet,   
			  cod_banca,   
			  data_creazione,   
			  data_modifica,   
			  cod_tipo_anagrafica,   
			  email_amministrazione,   
			  flag_accetta_email,   
			  giudizio_storico,   
			  data_giudizio_storico,   
			  giudizio_terzi,   
			  data_giudizio_terzi,   
			  riferimenti,   
			  cod_utente,   
			  cin,   
			  abi,   
			  cab,   
			  iban,   
			  cod_fornitore_adhoc,   
			  origine_anagrafica,   
			  origine_anagrafica_codice,   
			  ggmm_esclusione_1_da,   
			  ggmm_esclusione_1_a,   
			  ggmm_esclusione_2_da,   
			  ggmm_esclusione_2_a,   
			  id_doc_fin_voce_fin_impresa,   
			  id_doc_fin_rating_impresa,   
			  swift,   
			  stato )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_fornitore,   
			  :ls_rag_soc_1,   
			  :ls_rag_soc_2,   
			  :ls_indirizzo,   
			  :ls_localita,   
			  :ls_frazione,   
			  :ls_cap,   
			  :ls_provincia,   
			  :ls_telefono,   
			  :ls_fax,   
			  :ls_cellulare,   
			  :ls_cod_fiscale,   
			  :ls_partita_iva,   
			  :ls_rif_interno,   
			  :ls_flag_tipo_fornitore,   
			  null,   
			  null,   
			  null,   
			  null,   
			  'N',   
			  :ls_cod_pagamento,   
			  null,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  '001',   
			  0,   
			  :ls_cod_banca_clien_for,   
			  :ls_conto_corrente,   
			  null,   
			  :ls_cod_nazione,   
			  null,   
			  null,   
			  'EUR',   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  '100',   
			  'N',   
			  'N',   
			  'N',   
			  'N',   
			  null,   
			  null,   
			  'N',   
			  null,   
			  null,   
			  'N',   
			  'N',   
			  0,   
			  0,   
			  0,   
			  'N',   
			  null,   
			  'N',   
			  null,   
			  :ls_mail,   
			  null,   
			  :ls_sito_web,   
			  null,   
			  null,   
			  null,   
			  null,   
			  :ls_mail,   
			  'N',   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  :ls_cin,   
			  :ls_abi,   
			  :ls_cab,   
			  :ls_iban,   
			  null,   
			  null,   
			  null,   
			  0,   
			  0,   
			  0,   
			  0,   
			  null,   
			  null,   
			  null,   
			  :ls_stato_estero )  ;

	
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante l'inserimento del fornitore " + ls_cod_fornitore + ". " + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
	else
		iuo_excel.uof_set(ll_row, COL_LOG, "OK")
		iuo_log.info(ls_log + "inserito")
		//rollback;
		commit;
	end if
	
loop
 
return 1
end function

public function integer wf_update_fornitori (ref string as_error);/**
 * enme
 * 17/09/2015
 *
 * Inserimento fornitori da foglio Excel
 * 
 * Return: -1 errore, 1 ok
 **/
 
constant uint COL_LOG = 30

string ls_test, ls_log, ls_cod_fornitore, ls_rag_soc_1,ls_rag_soc_2, ls_partita_iva, ls_cod_fiscale, ls_indirizzo, ls_cap, ls_frazione, ls_localita, ls_provincia, &
		ls_telefono, ls_fax,ls_cellulare, ls_mail, ls_cod_pagamento,ls_cin,ls_paese, ls_cin_euro,ls_conto_corrente, ls_flag_tipo_fornitore, &
		ls_cod_tipo_listino_prodotto, ls_cod_banca_clien_for, ls_mail_amministrazione, ls_iban, ls_stato_estero, ls_abi,ls_cab, ls_sito_web, ls_cod_nazione, &
		ls_rif_interno
int ll_i
ulong ll_row
long	ll_cont, ll_cont_banca

ll_row = 0

select 	count(*)
into		:ll_cont_banca
from		anag_banche_clien_for
where	 	cod_azienda = :s_cs_xx.cod_azienda ;


if isnull(ll_cont_banca) then ll_cont_banca = 0

if cbx_header.checked then ll_row++
 
do while true
	ll_row++
	st_log.text = "Elaboro riga: " + string(ll_row)
	
	ls_cod_fornitore = string(iuo_excel.uof_read(ll_row, "A"))
	if ib_stop or g_str.isempty(ls_cod_fornitore) then exit
	
	ls_log = "Riga " + string(ll_row) + " FORNITORE " + ls_cod_fornitore + " "
	yield()
	
	select	cod_fornitore
	into 	:ls_test
	from 	anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante il controllo esistenza fornitore " + g_str.safe(ls_cod_fornitore) + "." + LR + sqlca.sqlerrtext
		iuo_log.error(ls_log + as_error)
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		return -1
	elseif sqlca.sqlcode = 100 then
		iuo_log.info(ls_log + " non presente in anagrafica")
		iuo_excel.uof_set(ll_row, COL_LOG, " non presente in anagrafica")
		continue
	end if	
	
	setnull(ls_cod_fiscale)
	setnull(ls_partita_iva)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_stato_estero)
	setnull(ls_localita)
	setnull(ls_frazione)	
	
	// Leggo variabili
	ls_rag_soc_1 = left(string(iuo_excel.uof_read(ll_row, "B")),40)
	ls_rag_soc_2 = left(trim(string(iuo_excel.uof_read(ll_row, "C"))),40)
	ls_partita_iva = trim(string(iuo_excel.uof_read(ll_row, "D")))
	ls_cod_fiscale = trim(string(iuo_excel.uof_read(ll_row, "E")))
	ls_indirizzo = left(string(iuo_excel.uof_read(ll_row, "F")),40)
	ls_cap = left(string(iuo_excel.uof_read(ll_row, "G")),6)
	ls_frazione = left(string(iuo_excel.uof_read(ll_row, "H")),40)
	ls_localita = left(string(iuo_excel.uof_read(ll_row, "I")),40)
	ls_provincia = left(string(iuo_excel.uof_read(ll_row, "J")),2)
	ls_telefono = left(string(iuo_excel.uof_read(ll_row, "K"))	,20)
	ls_fax = left(string(iuo_excel.uof_read(ll_row, "L")),20)
	ls_cellulare = left(string(iuo_excel.uof_read(ll_row, "M")),20)
	ls_mail = string(iuo_excel.uof_read(ll_row, "N"))
	ls_sito_web = string(iuo_excel.uof_read(ll_row, "O"))
	ls_cod_nazione = left(string(iuo_excel.uof_read(ll_row, "P")),40)
	ls_rif_interno = string(iuo_excel.uof_read(ll_row, "Q"))
	ls_cod_pagamento = left(string(iuo_excel.uof_read(ll_row, "R")),40)
	ls_abi = string(iuo_excel.uof_read(ll_row, "S"))
	ls_cab = string(iuo_excel.uof_read(ll_row, "T"))
	ls_cin = string(iuo_excel.uof_read(ll_row, "U"))
	ls_paese = string(iuo_excel.uof_read(ll_row, "V"))
	ls_cin_euro = string(iuo_excel.uof_read(ll_row, "W"))
	ls_conto_corrente = string(iuo_excel.uof_read(ll_row, "X"))
	ls_iban = string(iuo_excel.uof_read(ll_row, "Y"))
	
	
	
	if g_str.isempty(ls_cod_pagamento) or ls_cod_pagamento = " " then setnull(ls_cod_pagamento)
	if g_str.isempty(ls_cin) or ls_cin = " " then setnull(ls_cin)
	if g_str.isempty(ls_paese) or ls_paese = " " then setnull(ls_paese)
	if g_str.isempty(ls_cin_euro) or ls_cin_euro = " " then setnull(ls_cin_euro)
	if g_str.isempty(ls_abi) or ls_abi = " " then setnull(ls_abi)
	if g_str.isempty(ls_cab) or ls_cab = " " then setnull(ls_cab)
	if g_str.isempty(ls_conto_corrente) or ls_conto_corrente = " " then setnull(ls_conto_corrente)
	
	//
	if not isnull(ls_cod_nazione) then
		insert into tab_nazioni
			(	cod_azienda,
				cod_nazione,
				des_nazione,
				flag_blocco,
				data_blocco)
		values
			(	:s_cs_xx.cod_azienda,
				:ls_cod_nazione,
				'NUOVO',
				'N',
				null);
	end if			
	
	
	// sistema il tipo fornitore
	if not isnull(ls_partita_iva) then
		if left(upper(ls_partita_iva),1) > "A" and left(upper(ls_partita_iva),1) < "Z" then // è un estero perchè ha la partita iva che inizia per lettere
			ls_cod_fiscale = ls_partita_iva
			setnull(ls_partita_iva)
			ls_flag_tipo_fornitore = "C"
			ls_stato_estero = ls_localita
			ls_localita = ls_frazione
			setnull(ls_frazione)
		else
			if left(upper(ls_cod_fiscale),1) > "A" and left(upper(ls_cod_fiscale),1) < "Z" then	// trattasti di persona fisica
				ls_flag_tipo_fornitore = "F"
			else
				ls_flag_tipo_fornitore = "S"		// si tratta di una persona giurudica
			end if
		end if
	
	else
		// se la partita iva è nella e c'è un CF allora è un privato
		if not isnull(ls_cod_fiscale) then
			ls_flag_tipo_fornitore = 'P'
		else
			as_error = "Partita iva e codice fiscale non inseriti: il fornitore viene saltato " + g_str.safe(ls_cod_fornitore) + "." + LR + sqlca.sqlerrtext
			iuo_log.error(ls_log + as_error)
			iuo_excel.uof_set(ll_row, COL_LOG, as_error)
			continue
		end if
	end if
	
	ls_cod_tipo_listino_prodotto = '001'
	ls_mail_amministrazione = ls_mail
	setnull(ls_cod_banca_clien_for)
	
	if isnull(ls_cin) or isnull(ls_cab) or isnull(ls_abi)  or isnull(ls_cin_euro) or isnull(ls_paese) or isnull(ls_conto_corrente) then
      	setnull(ls_iban)
	end if

	// creazione banca
	if not isnull(ls_abi) and not isnull(ls_cab) then
		select tab_abi.des_abi
		into	:ls_test
		from 	tab_abi
				join tab_abicab on  tab_abi.cod_abi = tab_abicab.cod_abi
		where tab_abi.cod_abi = :ls_abi and
				 tab_abicab.cod_cab = :ls_cab;
				 
		if sqlca.sqlcode = 0 then
			
			select 	cod_banca_clien_for
			into		:ls_cod_banca_clien_for
			from		anag_banche_clien_for
			where	 	cod_azienda = :s_cs_xx.cod_azienda and
						cod_abi = :ls_abi and
						cod_cab = :ls_cab;
			
			if sqlca.sqlcode = 100 then
				ll_cont_banca ++
				ls_cod_banca_clien_for = string(ll_cont_banca,"000")
			
				INSERT INTO dbo.anag_banche_clien_for  
					( cod_azienda,   
					  cod_banca_clien_for,   
					  des_banca,   
					  indirizzo,   
					  localita,   
					  cap,   
					  provincia,   
					  cod_abi,   
					  cod_cab,   
					  flag_blocco,   
					  data_blocco,   
					  cin,   
					  iban )  
					select :s_cs_xx.cod_azienda,
							:ls_cod_banca_clien_for,
							left(tab_abi.des_abi + ' ' + tab_abicab.agenzia, 80),
							tab_abicab.indirizzo,
							tab_abicab.localita,
							tab_abicab.cap,
							tab_abicab.provincia,
							tab_abi.cod_abi,
							tab_abicab.cod_cab,
							'N',
							null,
							:ls_cin,
							:ls_iban
					from 	tab_abi
							join tab_abicab on  tab_abi.cod_abi = tab_abicab.cod_abi
					where tab_abi.cod_abi = :ls_abi and
							 tab_abicab.cod_cab = :ls_cab;
							 
				if sqlca.sqlcode < 0 then
					as_error = "Inserire banca a mano: errore durante l'inserimento della banca fornitore ABI/CAB" + ls_abi + "/" + ls_cab + ". " + sqlca.sqlerrtext
					iuo_excel.uof_set(ll_row, COL_LOG, as_error)
					iuo_log.error(ls_log + as_error)
					setnull(ls_cod_banca_clien_for)
				end if
			end if
		else
			setnull(ls_cod_banca_clien_for)
		end if
	end if
	
	
	update anag_fornitori
	set 	  rag_soc_1 =    :ls_rag_soc_1,
			  rag_soc_2 =  :ls_rag_soc_2  ,
			  indirizzo =    :ls_indirizzo,
			  localita =    :ls_localita,
			  frazione =    :ls_frazione,
			  cap =    :ls_cap ,
			  provincia =     :ls_provincia,
			  telefono =    :ls_telefono,
			  fax =    :ls_fax,
			  telex =    :ls_cellulare,
			  cod_fiscale = :ls_cod_fiscale    ,
			  partita_iva =    :ls_partita_iva,
			  rif_interno =    :ls_rif_interno,
			  flag_tipo_fornitore =  :ls_flag_tipo_fornitore  ,
			  cod_pagamento =    :ls_cod_pagamento,
			  cod_banca_clien_for =   :ls_cod_banca_clien_for,
			  conto_corrente =    :ls_conto_corrente,
			  casella_mail =    :ls_mail ,
			  sito_internet = :ls_sito_web,   
			  cin =    :ls_cin,
			  abi =    :ls_abi,
			  cab =  :ls_cab,
			  iban = :ls_iban, 
			  stato = :ls_stato_estero
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante l'inserimento del fornitore " + ls_cod_fornitore + ". " + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
	else
		iuo_excel.uof_set(ll_row, COL_LOG, " OK")
		iuo_log.info(ls_log + " aggiornato")
		//rollback;
		commit;
	end if
	
loop
 
return 1
end function

on w_allinea_fornitori.create
int iCurrent
call super::create
this.cb_stop_insert=create cb_stop_insert
this.cb_start_insert=create cb_start_insert
this.st_11=create st_11
this.st_10=create st_10
this.cb_stop_update=create cb_stop_update
this.st_7=create st_7
this.st_log=create st_log
this.cbx_header=create cbx_header
this.cb_start_update=create cb_start_update
this.st_6=create st_6
this.st_5=create st_5
this.st_file=create st_file
this.cb_selezione=create cb_selezione
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stop_insert
this.Control[iCurrent+2]=this.cb_start_insert
this.Control[iCurrent+3]=this.st_11
this.Control[iCurrent+4]=this.st_10
this.Control[iCurrent+5]=this.cb_stop_update
this.Control[iCurrent+6]=this.st_7
this.Control[iCurrent+7]=this.st_log
this.Control[iCurrent+8]=this.cbx_header
this.Control[iCurrent+9]=this.cb_start_update
this.Control[iCurrent+10]=this.st_6
this.Control[iCurrent+11]=this.st_5
this.Control[iCurrent+12]=this.st_file
this.Control[iCurrent+13]=this.cb_selezione
this.Control[iCurrent+14]=this.st_2
this.Control[iCurrent+15]=this.st_1
end on

on w_allinea_fornitori.destroy
call super::destroy
destroy(this.cb_stop_insert)
destroy(this.cb_start_insert)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.cb_stop_update)
destroy(this.st_7)
destroy(this.st_log)
destroy(this.cbx_header)
destroy(this.cb_start_update)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_file)
destroy(this.cb_selezione)
destroy(this.st_2)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;wf_abilita_pulsanti(false)
end event

type cb_stop_insert from commandbutton within w_allinea_fornitori
integer x = 754
integer y = 1240
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma"
end type

event clicked;ib_stop = true
visible = false
end event

type cb_start_insert from commandbutton within w_allinea_fornitori
integer x = 229
integer y = 1240
integer width = 503
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Avvia"
end type

event clicked;string ls_error

if isvalid(iuo_excel) then destroy iuo_excel
if isvalid(iuo_log) then destroy iuo_log

iuo_excel = create uo_excel
iuo_excel.uof_open(is_file_excel, false)
iuo_excel.uof_set_sheet(1)

iuo_log = create uo_log
iuo_log.open(is_desktop + "insert_fornitori.log", true)

cb_stop_insert.visible = true
ib_stop = false

if wf_insert_fornitori(ref ls_error) < 0 then
	g_mb.error(ls_error)
else
	g_mb.success("Inserimento completato")
end if

destroy iuo_excel

end event

type st_11 from statictext within w_allinea_fornitori
integer x = 229
integer y = 1100
integer width = 1920
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Legge il foglio numero 1 del file excel e INSERISCE in anagrafica fornitori. Il processo termina alla prima riga vuota della colonna A."
boolean focusrectangle = false
end type

type st_10 from statictext within w_allinea_fornitori
integer x = 46
integer y = 960
integer width = 1536
integer height = 140
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Inserimento nuovi Fornitori"
boolean focusrectangle = false
end type

type cb_stop_update from commandbutton within w_allinea_fornitori
integer x = 754
integer y = 700
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma"
end type

event clicked;ib_stop = true
visible = false
end event

type st_7 from statictext within w_allinea_fornitori
integer x = 23
integer y = 1580
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stato:"
boolean focusrectangle = false
end type

type st_log from statictext within w_allinea_fornitori
integer x = 274
integer y = 1580
integer width = 1897
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
boolean focusrectangle = false
end type

type cbx_header from checkbox within w_allinea_fornitori
integer x = 251
integer y = 220
integer width = 1074
integer height = 72
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Salta intestazione file (salta la prima riga)"
boolean checked = true
end type

type cb_start_update from commandbutton within w_allinea_fornitori
integer x = 229
integer y = 700
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Avvia"
end type

event clicked;string ls_error

if isvalid(iuo_excel) then destroy iuo_excel
if isvalid(iuo_log) then destroy iuo_log

iuo_excel = create uo_excel
iuo_excel.uof_open(is_file_excel, false)
iuo_excel.uof_set_sheet(1)

iuo_log = create uo_log
iuo_log.open(is_desktop + "update_fornitori.log", true)

cb_stop_update.visible = true
ib_stop = false

if wf_update_fornitori(ref ls_error) < 0 then
	g_mb.error(ls_error)
else
	g_mb.success("Aggiornamento completato")
end if

destroy iuo_excel
end event

type st_6 from statictext within w_allinea_fornitori
integer x = 251
integer y = 580
integer width = 1897
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Legge il foglio numero 1 del file excel e aggiorna i dati in anagrafica fornitori. Il processo termina alla prima riga vuota della colonna A."
boolean focusrectangle = false
end type

type st_5 from statictext within w_allinea_fornitori
integer x = 46
integer y = 440
integer width = 2075
integer height = 120
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiornamento Fornitori"
boolean focusrectangle = false
end type

type st_file from statictext within w_allinea_fornitori
integer x = 777
integer y = 140
integer width = 1646
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_selezione from commandbutton within w_allinea_fornitori
integer x = 229
integer y = 120
integer width = 503
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Seleziona File"
end type

event clicked;string ls_docname[]

wf_abilita_pulsanti(false)
st_file.text = ""
is_desktop = guo_functions.uof_get_user_desktop_folder( )

if GetFileOpenName("Select File", is_file_excel, ls_docname[], "Excel","Excel,*.xls;*.xlsx,", is_desktop) > 0 then
	wf_abilita_pulsanti(true)
	st_file.text = is_file_excel
end if
end event

type st_2 from statictext within w_allinea_fornitori
integer x = 229
integer y = 40
integer width = 1943
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Selezione File Excel"
boolean focusrectangle = false
end type

type st_1 from statictext within w_allinea_fornitori
integer x = 46
integer y = 60
integer width = 160
integer height = 160
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "*"
boolean focusrectangle = false
end type

