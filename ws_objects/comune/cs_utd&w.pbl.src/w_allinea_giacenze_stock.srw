﻿$PBExportHeader$w_allinea_giacenze_stock.srw
forward
global type w_allinea_giacenze_stock from window
end type
type st_filtro from statictext within w_allinea_giacenze_stock
end type
type em_prodotto from editmask within w_allinea_giacenze_stock
end type
type cb_5 from commandbutton within w_allinea_giacenze_stock
end type
type cb_4 from commandbutton within w_allinea_giacenze_stock
end type
type st_10 from statictext within w_allinea_giacenze_stock
end type
type em_esercizio from editmask within w_allinea_giacenze_stock
end type
type em_data_apertura from editmask within w_allinea_giacenze_stock
end type
type st_5 from statictext within w_allinea_giacenze_stock
end type
type st_note from statictext within w_allinea_giacenze_stock
end type
type cb_2 from commandbutton within w_allinea_giacenze_stock
end type
type st_1 from statictext within w_allinea_giacenze_stock
end type
end forward

global type w_allinea_giacenze_stock from window
integer width = 2853
integer height = 1260
boolean titlebar = true
string title = "Allineamento Stock"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
st_filtro st_filtro
em_prodotto em_prodotto
cb_5 cb_5
cb_4 cb_4
st_10 st_10
em_esercizio em_esercizio
em_data_apertura em_data_apertura
st_5 st_5
st_note st_note
cb_2 cb_2
st_1 st_1
end type
global w_allinea_giacenze_stock w_allinea_giacenze_stock

on w_allinea_giacenze_stock.create
this.st_filtro=create st_filtro
this.em_prodotto=create em_prodotto
this.cb_5=create cb_5
this.cb_4=create cb_4
this.st_10=create st_10
this.em_esercizio=create em_esercizio
this.em_data_apertura=create em_data_apertura
this.st_5=create st_5
this.st_note=create st_note
this.cb_2=create cb_2
this.st_1=create st_1
this.Control[]={this.st_filtro,&
this.em_prodotto,&
this.cb_5,&
this.cb_4,&
this.st_10,&
this.em_esercizio,&
this.em_data_apertura,&
this.st_5,&
this.st_note,&
this.cb_2,&
this.st_1}
end on

on w_allinea_giacenze_stock.destroy
destroy(this.st_filtro)
destroy(this.em_prodotto)
destroy(this.cb_5)
destroy(this.cb_4)
destroy(this.st_10)
destroy(this.em_esercizio)
destroy(this.em_data_apertura)
destroy(this.st_5)
destroy(this.st_note)
destroy(this.cb_2)
destroy(this.st_1)
end on

event open;datetime ldt_data_chiusura_periodica

select data_chiusura_periodica
into   :ldt_data_chiusura_periodica
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

em_esercizio.text = string(f_anno_esercizio())
em_data_apertura.text = string(relativedate(date(ldt_data_chiusura_periodica), 1))


end event

type st_filtro from statictext within w_allinea_giacenze_stock
integer x = 640
integer y = 584
integer width = 1339
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "FILTRO PRODOTTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_prodotto from editmask within w_allinea_giacenze_stock
integer x = 1998
integer y = 584
integer width = 782
integer height = 88
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_5 from commandbutton within w_allinea_giacenze_stock
integer x = 2043
integer y = 892
integer width = 631
integer height = 84
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Allineamento Reale"
end type

event clicked;string ls_cod_prodotto,ls_des_prodotto,ls_where,ls_error,ls_chiave[], ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str, ls_vuoto[] , ls_sql
integer li_return, li_i, li_file
long ll_anno_lifo, ll_prog_stock
dec{4} ld_giacenza_finale,ld_costo_medio_ponderato,ld_quant_val[],ld_giacenza_stock[], ld_giacenza,ld_quan_assegnata_stock, &
       ld_quan_in_spedizione_stock, ld_costo_medio_stock, ld_vuoto[], ld_costo_medio[], ld_quan_costo_medio_stock[]
datetime ldt_data_apertura, ldt_data, ldt_data_stock
uo_magazzino luo_magazzino

ldt_data_apertura = datetime(date(em_data_apertura.text), 00:00:00)
ldt_data = datetime(date(today()),00:00:00)
	
if g_mb.messagebox("APICE","Sei sicuro di voler procedere con l'elaborazione?~r~nLe operazioni eseguite non saranno reversibili",Question!,YesNo!,2) = 2 then
	g_mb.messagebox("","Operazione annullata dall'utente.")
	return
end if

ls_sql = "select  cod_prodotto, des_prodotto from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' "
if len(em_prodotto.text) > 0 then
	ls_sql = ls_sql + " and cod_prodotto like '" + em_prodotto.text + "' "
end if
ls_sql = ls_sql + " order by cod_prodotto "

declare cu_lifo dynamic cursor for sqlsa;
PREPARE SQLSA FROM :ls_sql;

open dynamic cu_lifo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
	rollback;
	return
end if

li_file = FileOpen("c:\temp\allineamento_lotti_reale.txt", LineMode!, Write!, LockWrite!, Replace!)
ls_str = "PROCEDURA DI ALLIENAMENTO REALE DEGLI STOCK. RISULTATI ELABORAZIONE"
filewrite(li_file, ls_str)
ls_str = "CODICE PRODOTTO~tSTOCK~tGIACENZA DA MOVIMENTI~tGIACENZA DA STOCK~tASSEGNATA~tIN SPEDIZIONE"
filewrite(li_file, ls_str)

do while true
	fetch cu_lifo into :ls_cod_prodotto,   :ls_des_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
		close cu_lifo;
		rollback;
		return
	end if
	st_note.text = ls_cod_prodotto + "  " + ls_des_prodotto
	ls_chiave = ls_vuoto
	ld_quant_val = ld_vuoto
	ld_giacenza_stock = ld_vuoto
	
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data, ls_where, ld_quant_val[], ls_error, "S", ls_chiave[], ld_giacenza_stock[], ld_costo_medio[], ld_quan_costo_medio_stock[])

	destroy luo_magazzino
	
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		close cu_lifo;
		rollback;
		return 
	end if

	for li_i = 1 to upperbound(ls_chiave)
		if len(ls_chiave[li_i]) < 1 then continue
		f_parse_stock(ls_chiave[li_i], ref ls_cod_deposito, ref ls_cod_ubicazione, ref ls_cod_lotto, ref ldt_data_stock, ref ll_prog_stock)
	
		SELECT giacenza_stock,   
				quan_assegnata,   
				quan_in_spedizione,   
				costo_medio  
		 INTO :ld_giacenza,   
				:ld_quan_assegnata_stock,   
				:ld_quan_in_spedizione_stock,   
				:ld_costo_medio_stock  
		 FROM stock  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :ls_cod_prodotto AND  
				cod_deposito = :ls_cod_deposito AND  
				cod_ubicazione = :ls_cod_ubicazione AND  
				cod_lotto = :ls_cod_lotto AND  
				data_stock = :ldt_data_stock AND  
				prog_stock = :ll_prog_stock ;
		if sqlca.sqlcode <> 0 then
			ls_str = "Errore~t"+ls_chiave[li_i]+"~t0~t0~t0~t0~t" + sqlca.sqlerrtext + "~t" + string(sqlca.sqlcode)
			filewrite(li_file, ls_str)
			continue
		end if
		
		update stock 
		set   giacenza_stock = :ld_giacenza_stock[li_i]
		where cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :ls_cod_prodotto AND  
				cod_deposito = :ls_cod_deposito AND  
				cod_ubicazione = :ls_cod_ubicazione AND  
				cod_lotto = :ls_cod_lotto AND  
				data_stock = :ldt_data_stock AND  
				prog_stock = :ll_prog_stock ;
		if sqlca.sqlcode <> 0 then
			ls_str = "Errore~t"+ls_chiave[li_i]+"~t0~t0~t0~t0~t" + sqlca.sqlerrtext + "~t" + string(sqlca.sqlcode)
			continue
		else
			ls_str = ls_cod_prodotto + "~t" + ls_chiave[li_i] + "~t" + string(ld_giacenza_stock[li_i]) + "~t" + string(ld_giacenza) + "~t" + string(ld_quan_assegnata_stock) + "~t" + string(ld_quan_in_spedizione_stock)
		end if
		filewrite(li_file, ls_str)
	next
loop
close cu_lifo;
fileclose(li_file)
commit;
run("notepad.exe c:\temp\allineamento_lotti_reale.txt")

end event

type cb_4 from commandbutton within w_allinea_giacenze_stock
integer x = 901
integer y = 892
integer width = 827
integer height = 84
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Simulazione Allineamento"
end type

event clicked;string ls_cod_prodotto,ls_des_prodotto,ls_where,ls_error,ls_chiave[], ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str, ls_vuoto[], ls_sql
integer li_return, li_i, li_file
long ll_anno_lifo, ll_prog_stock
dec{4} ld_giacenza_finale,ld_costo_medio_ponderato,ld_quant_val[],ld_giacenza_stock[], ld_giacenza,ld_quan_assegnata_stock, &
       ld_quan_in_spedizione_stock, ld_costo_medio_stock, ld_vuoto[], ld_costo_medio[], ld_quan_costo_medio_stock[]
datetime ldt_data_apertura, ldt_data, ldt_data_stock
uo_magazzino  luo_magazzino

ldt_data_apertura = datetime(date(em_data_apertura.text), 00:00:00)
ldt_data = datetime(date(today()),00:00:00)
	
if g_mb.messagebox("APICE","Tale elaborazione potrebbe durare molto tempo ( > 4 ore )?~r~nSei sicuro di voler procedere con l'elaborazione?~r~nLe operazioni eseguite non saranno reversibili",Question!,YesNo!,2) = 2 then
	g_mb.messagebox("","Operazione annullata dall'utente.")
	return
end if

ls_sql = "select  cod_prodotto, des_prodotto from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' "
if len(em_prodotto.text) > 0 then
	ls_sql = ls_sql + " and cod_prodotto like '" + em_prodotto.text + "' "
end if
ls_sql = ls_sql + " order by cod_prodotto "

declare cu_lifo dynamic cursor for sqlsa;
PREPARE SQLSA FROM :ls_sql;

open dynamic cu_lifo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
	rollback;
	return
end if

li_file = FileOpen("c:\temp\simulazione_allineamento_lotti.txt", LineMode!, Write!, LockWrite!, Replace!)
ls_str = "CODICE PRODOTTO~tSTOCK~tGIACENZA DA MOVIMENTI~tGIACENZA DA STOCK~tASSEGNATA~tIN SPEDIZIONE"
filewrite(li_file, ls_str)

do while true
	fetch cu_lifo into :ls_cod_prodotto,   :ls_des_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if
	st_note.text = ls_cod_prodotto + "  " + ls_des_prodotto
	ls_chiave = ls_vuoto
	ld_quant_val = ld_vuoto
	ld_giacenza_stock = ld_vuoto
	
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data, ls_where, ld_quant_val[], ls_error, "S", ls_chiave[], ld_giacenza_stock[],ld_costo_medio[],ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
	
	if li_return <0 then
		g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
		rollback;
		return 
	end if

	for li_i = 1 to upperbound(ls_chiave)
		if len(ls_chiave[li_i]) < 1 then continue
		f_parse_stock(ls_chiave[li_i], ref ls_cod_deposito, ref ls_cod_ubicazione, ref ls_cod_lotto, ref ldt_data_stock, ref ll_prog_stock)
	
		SELECT giacenza_stock,   
				quan_assegnata,   
				quan_in_spedizione,   
				costo_medio  
		 INTO :ld_giacenza,   
				:ld_quan_assegnata_stock,   
				:ld_quan_in_spedizione_stock,   
				:ld_costo_medio_stock  
		 FROM stock  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :ls_cod_prodotto AND  
				cod_deposito = :ls_cod_deposito AND  
				cod_ubicazione = :ls_cod_ubicazione AND  
				cod_lotto = :ls_cod_lotto AND  
				data_stock = :ldt_data_stock AND  
				prog_stock = :ll_prog_stock ;
		if sqlca.sqlcode <> 0 then
			ls_str = "Errore~t"+ls_chiave[li_i]+"~t0~t0~t0~t0~t" + sqlca.sqlerrtext + "~t" + string(sqlca.sqlcode)
		else
			ls_str = ls_cod_prodotto + "~t" + ls_chiave[li_i] + "~t" + string(ld_giacenza_stock[li_i]) + "~t" + string(ld_giacenza) + "~t" + string(ld_quan_assegnata_stock) + "~t" + string(ld_quan_in_spedizione_stock)
		end if
		filewrite(li_file, ls_str)
	
	next
loop
fileclose(li_file)
commit;

run("notepad.exe c:\temp\simulazione_allineamento_lotti.txt")

end event

type st_10 from statictext within w_allinea_giacenze_stock
integer x = 219
integer y = 356
integer width = 1760
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "ESERCIZIO CORRENTE IN CUI SARANNO REGISTRATE LE SCRITTURE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_esercizio from editmask within w_allinea_giacenze_stock
integer x = 1998
integer y = 344
integer width = 338
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
string mask = "0000"
string minmax = "1900~~2999"
end type

type em_data_apertura from editmask within w_allinea_giacenze_stock
integer x = 1998
integer y = 464
integer width = 457
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string minmax = "01/01/1900~~31/12/2099"
end type

type st_5 from statictext within w_allinea_giacenze_stock
integer x = 640
integer y = 476
integer width = 1339
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "DATA INIZIO ESERCIZIO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_note from statictext within w_allinea_giacenze_stock
integer x = 14
integer y = 1060
integer width = 2779
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_allinea_giacenze_stock
integer x = 32
integer y = 892
integer width = 553
integer height = 84
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type st_1 from statictext within w_allinea_giacenze_stock
integer x = 14
integer y = 12
integer width = 2743
integer height = 248
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Allineamento delle giacenze degli stock in base alle giacenze ricalcolate dai movimenti di magazzino"
alignment alignment = center!
boolean focusrectangle = false
end type

