﻿$PBExportHeader$w_allinea_mov_mag_raggruppati.srw
forward
global type w_allinea_mov_mag_raggruppati from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_allinea_mov_mag_raggruppati
end type
type dp_a from datepicker within w_allinea_mov_mag_raggruppati
end type
type dp_da from datepicker within w_allinea_mov_mag_raggruppati
end type
type dw_list from uo_std_dw within w_allinea_mov_mag_raggruppati
end type
type cb_ricerca from commandbutton within w_allinea_mov_mag_raggruppati
end type
end forward

global type w_allinea_mov_mag_raggruppati from w_cs_xx_principale
integer width = 3429
integer height = 2244
string title = "Allinea Movimenti Magazzino Raggruppati"
hpb_1 hpb_1
dp_a dp_a
dp_da dp_da
dw_list dw_list
cb_ricerca cb_ricerca
end type
global w_allinea_mov_mag_raggruppati w_allinea_mov_mag_raggruppati

forward prototypes
public function integer wf_ricerca_mov_mag ()
end prototypes

public function integer wf_ricerca_mov_mag ();/**
 * stefanop
 * 23/03/2012
 *
 * Cerco tutti i movimenti di magazzino che il codice prodotto ha un prodotto graggruppato impostato
 **/
 
string ls_sql, ls_cod_prodotto_raggruppato, ls_cod_prodotto_raggruppato_next, ls_flag_tipo
long ll_count, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_count_ragg, ll_row, ll_num_registrazione_next
datastore lds_store

ls_sql = "SELECT anno_registrazione, num_registrazione,  mov_magazzino.cod_prodotto, anag_prodotti.cod_prodotto_raggruppato " + &
			"FROM mov_magazzino " + &
			"JOIN anag_prodotti ON " + &
			"	 anag_prodotti.cod_prodotto = mov_magazzino.cod_prodotto " + &
			"WHERE mov_magazzino.cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
			"mov_magazzino.cod_prodotto IN ( " + &
			"	 select cod_prodotto " + &
			"	 from anag_prodotti " + &
			"	 where cod_prodotto_raggruppato is not null) " + &
			"AND cod_tipo_movimento <> 'INL' " + &
			"AND data_registrazione >= '" + string(dp_da.value, s_cs_xx.db_funzioni.formato_data) + "' and data_registrazione <='" + string(dp_a.value, s_cs_xx.db_funzioni.formato_data) + "'"

dw_list.reset()
dw_list.setredraw(false)

ll_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)
hpb_1.maxposition = ll_count
hpb_1.position = 0

for ll_i = 1 to ll_count
	
	ll_anno_registrazione = lds_store.getitemnumber(ll_i, 1)
	ll_num_registrazione = lds_store.getitemnumber(ll_i, 2)
	ls_cod_prodotto_raggruppato =  lds_store.getitemstring(ll_i, 4)
	
	hpb_1.position = ll_i
	pcca.mdi_frame.setmicrohelp(string(ll_i) + "/" + string(ll_count) + " - movimento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
	yield()
	
	// Verifico se esiste un movimento di magazzino collegato, quindi esiste il movimento dello sfuso ed è collegato
	select count(*)
	into :ll_count_ragg
	from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_mov_origine = :ll_anno_registrazione and
			 num_reg_mov_origine = :ll_num_registrazione;
			 
	if sqlca.sqlcode < 0 then 
		g_mb.error("Errore durante il controllo del movimento raggruppato", sqlca)
		destroy lds_store
		return -1
	elseif sqlca.sqlcode = 0 and ll_count_ragg > 0 then
		// esiste il movimento del prodotto raggruppato ed è correttamente collegato
		continue
	end if
	
	// Controllo che il movimento successivo sia del prodotto raggruppato
	ll_num_registrazione_next ++
	
	select cod_prodotto
	into :ls_cod_prodotto_raggruppato_next
	from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione_next;
			 
	if ls_cod_prodotto_raggruppato_next = ls_cod_prodotto_raggruppato then
		ls_flag_tipo = "S"
	else
		ls_flag_tipo = "N"
	end if
	
	ll_row = dw_list.insertrow(0)
	dw_list.setitem(ll_row, "anno_registrazione", ll_anno_registrazione)
	dw_list.setitem(ll_row, "num_registrazione", ll_num_registrazione)
	dw_list.setitem(ll_row, "flag_tipo", ls_flag_tipo)
	
next

hpb_1.position = 0
dw_list.setredraw(true)
return 1
end function

on w_allinea_mov_mag_raggruppati.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.dp_a=create dp_a
this.dp_da=create dp_da
this.dw_list=create dw_list
this.cb_ricerca=create cb_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.dp_a
this.Control[iCurrent+3]=this.dp_da
this.Control[iCurrent+4]=this.dw_list
this.Control[iCurrent+5]=this.cb_ricerca
end on

on w_allinea_mov_mag_raggruppati.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.dp_a)
destroy(this.dp_da)
destroy(this.dw_list)
destroy(this.cb_ricerca)
end on

type hpb_1 from hprogressbar within w_allinea_mov_mag_raggruppati
integer x = 2446
integer y = 40
integer width = 914
integer height = 60
unsignedinteger maxposition = 100
integer setstep = 10
end type

type dp_a from datepicker within w_allinea_mov_mag_raggruppati
integer x = 503
integer y = 20
integer width = 434
integer height = 100
integer taborder = 30
boolean border = true
borderstyle borderstyle = stylelowered!
string customformat = "dd/mm/yyyy"
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2012-01-31"), Time("12:04:46.000000"))
integer textsize = -9
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
boolean valueset = true
end type

type dp_da from datepicker within w_allinea_mov_mag_raggruppati
integer x = 23
integer y = 20
integer width = 434
integer height = 100
integer taborder = 20
boolean border = true
borderstyle borderstyle = stylelowered!
string customformat = "dd/mm/yyyy"
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2012-01-01"), Time("12:04:40.000000"))
integer textsize = -9
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
boolean valueset = true
end type

type dw_list from uo_std_dw within w_allinea_mov_mag_raggruppati
integer x = 23
integer y = 140
integer width = 3337
integer height = 1980
integer taborder = 20
string dataobject = "d_allinea_mov_mag_raggruppati"
boolean hscrollbar = true
boolean vscrollbar = true
boolean ib_colora = false
end type

type cb_ricerca from commandbutton within w_allinea_mov_mag_raggruppati
integer x = 960
integer y = 20
integer width = 389
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;wf_ricerca_mov_mag()
end event

