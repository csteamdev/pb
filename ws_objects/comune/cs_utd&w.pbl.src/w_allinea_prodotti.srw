﻿$PBExportHeader$w_allinea_prodotti.srw
forward
global type w_allinea_prodotti from w_cs_xx_principale
end type
type cb_stop_insert from commandbutton within w_allinea_prodotti
end type
type cb_3 from commandbutton within w_allinea_prodotti
end type
type st_11 from statictext within w_allinea_prodotti
end type
type st_10 from statictext within w_allinea_prodotti
end type
type cb_stop_update from commandbutton within w_allinea_prodotti
end type
type st_7 from statictext within w_allinea_prodotti
end type
type cb_stop_cancella from commandbutton within w_allinea_prodotti
end type
type st_log from statictext within w_allinea_prodotti
end type
type cbx_header from checkbox within w_allinea_prodotti
end type
type cb_2 from commandbutton within w_allinea_prodotti
end type
type st_6 from statictext within w_allinea_prodotti
end type
type st_5 from statictext within w_allinea_prodotti
end type
type cb_1 from commandbutton within w_allinea_prodotti
end type
type st_4 from statictext within w_allinea_prodotti
end type
type st_3 from statictext within w_allinea_prodotti
end type
type st_file from statictext within w_allinea_prodotti
end type
type cb_selezione from commandbutton within w_allinea_prodotti
end type
type st_2 from statictext within w_allinea_prodotti
end type
type st_1 from statictext within w_allinea_prodotti
end type
end forward

global type w_allinea_prodotti from w_cs_xx_principale
integer width = 2569
integer height = 2208
string title = "Allinea Prodotti"
cb_stop_insert cb_stop_insert
cb_3 cb_3
st_11 st_11
st_10 st_10
cb_stop_update cb_stop_update
st_7 st_7
cb_stop_cancella cb_stop_cancella
st_log st_log
cbx_header cbx_header
cb_2 cb_2
st_6 st_6
st_5 st_5
cb_1 cb_1
st_4 st_4
st_3 st_3
st_file st_file
cb_selezione cb_selezione
st_2 st_2
st_1 st_1
end type
global w_allinea_prodotti w_allinea_prodotti

type variables
constant string LR = "~r~n"

private:
	string is_desktop
	string is_file_excel
	uo_excel iuo_excel
	uo_log	iuo_log
	boolean ib_stop = false
end variables

forward prototypes
public subroutine wf_abilita_pulsanti (boolean ab_enabled)
public function integer wf_cancella_prodotti (ref string as_error)
public function integer wf_cancella_listini (string as_cod_prodotto, ref string as_error)
public function integer wf_update_prodotti (ref string as_error)
public function integer wf_insert_prodotti (ref string as_error)
end prototypes

public subroutine wf_abilita_pulsanti (boolean ab_enabled);cb_1.enabled = ab_enabled
cb_2.enabled = ab_enabled


if ab_enabled = false then
	st_log.text = "Pronto"
	cb_stop_update.visible = false
	cb_stop_cancella.visible = false
end if
end subroutine

public function integer wf_cancella_prodotti (ref string as_error);/**
 * stefanop
 * 16/07/2014
 *
 * Cancello i prodotti.
 * 
 * Return: -1 errore, 1 ok
 **/
 
string ls_cod_prodotto, ls_test, ls_log
ulong ll_row

ll_row = 0

if cbx_header.checked then ll_row++
 
do while true
	ll_row++
	st_log.text = "Elaboro riga: " + string(ll_row)
	yield()
	as_error = ""
	
	ls_cod_prodotto = string(iuo_excel.uof_read(ll_row, "A"))
	
	if ib_stop or g_str.isempty(ls_cod_prodotto) then exit
	
	ls_log = "Riga " + string(ll_row) + " prodotto " + ls_cod_prodotto + " "
	
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante il controllo esistenza prodotto " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_log.error(ls_log + as_error)
		iuo_excel.uof_set(ll_row, 2, as_error)
		return -1
	
	elseif sqlca.sqlcode = 100 then
		iuo_log.info(ls_log + " non trovato")
		iuo_excel.uof_set(ll_row, 2, "OK")
		continue
	end if	
	
	// CANCELLAZIONE -------------------------
	if wf_cancella_listini(ls_cod_prodotto, as_error) < 0 then
		iuo_excel.uof_set(ll_row, 2, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
		continue
	end if
	
	delete from anag_prodotti_reparti_depositi
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la cancellazione del deposito prodotto (anag_prodotti_reparti_depositi) " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, 2, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
		continue
	end if
			 	
	delete from stock
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la cancellazione del stock (stock) " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, 2, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
		continue
	end if
	
	delete from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la cancellazione del prodotto (anag_prodotti) " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, 2, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
	else
		iuo_excel.uof_set(ll_row, 2, "OK")
		iuo_log.info(ls_log + " cancellato")
		commit;
	end if
		
loop
 
return 1
end function

public function integer wf_cancella_listini (string as_cod_prodotto, ref string as_error);/**
 * Cancello i listini collegati al prodotto
 **/
 
string ls_sql, ls_cod_tipo_listino_prodotto, ls_cod_valuta
long ll_rows, ll_progressivo, ll_i
datetime ld_data_inizio_val
datastore lds_store

ls_sql = "SELECT cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo FROM listini_ven_comune " &
		+ "WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_prodotto='" + as_cod_prodotto + "'"
		
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)

if ll_rows > 0 then
	for ll_i = 1 to ll_rows
		
		ls_cod_tipo_listino_prodotto = lds_store.getitemstring(ll_rows, "cod_tipo_listino_prodotto")
		ls_cod_valuta = lds_store.getitemstring(ll_rows, "cod_valuta")
		ld_data_inizio_val = lds_store.getitemdatetime(ll_rows, "data_inizio_val")
		ll_progressivo = lds_store.getitemnumber(ll_rows, "progressivo")
		
		delete from listini_ven_dim_comune
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ld_data_inizio_val and
				 progressivo = :ll_progressivo;
				 
		if sqlca.sqlcode < 0 then
			as_error = "Errore durante la cancellazione del listino (listini_ven_dim_comune). " + sqlca.sqlerrtext
			destroy lds_store
			return -1
		end if
				 
	next
	
	// Cancello il listino dopo aver cancellato la referenza
	delete from listini_ven_comune
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la cancellazione del listino (listini_ven_comune). " + sqlca.sqlerrtext
		destroy lds_store
		return -1
	end if
	
	delete from listini_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :as_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante la cancellazione del listino (listini_fornitori). " + sqlca.sqlerrtext
		destroy lds_store
		return -1
	end if
			
end if

destroy lds_store
return ll_rows


end function

public function integer wf_update_prodotti (ref string as_error);/**
 * stefanop
 * 16/07/2014
 *
 * Aggiorno i prodotti
 * 
 * Return: -1 errore, 1 ok
 **/
 
constant uint COL_LOG = 14

string ls_cod_prodotto, ls_test, ls_log, ls_rif_dogana, ls_cod_livello_prod[], ls_cod_misura_peso, ls_empty_array[]
int ll_i
ulong ll_row
decimal{4} ld_peso, ld_costo_standard

ll_row = 0

if cbx_header.checked then ll_row++
 
do while true
	ll_row++
	st_log.text = "Elaboro riga: " + string(ll_row)
	yield()
	
	ls_cod_prodotto = string(iuo_excel.uof_read(ll_row, "A"))
	if ib_stop or g_str.isempty(ls_cod_prodotto) then exit
	
	ls_log = "Riga " + string(ll_row) + " prodotto " + ls_cod_prodotto + " "
	
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante il controllo esistenza prodotto " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_log.error(ls_log + as_error)
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		return -1
	elseif sqlca.sqlcode = 100 then
		iuo_log.info(ls_log + " non trovato")
		iuo_excel.uof_set(ll_row, COL_LOG, "Non trovato")
		continue
	end if	
	
	// Leggo variabili
	ls_cod_livello_prod = ls_empty_array
	ls_rif_dogana = string(iuo_excel.uof_read(ll_row, "B"))
	ld_peso = dec(iuo_excel.uof_read(ll_row, "E"))
	ls_cod_livello_prod[1] = string(iuo_excel.uof_read(ll_row, "G"))
	ls_cod_livello_prod[2] = string(iuo_excel.uof_read(ll_row, "H"))
	ls_cod_livello_prod[3] = string(iuo_excel.uof_read(ll_row, "I"))
	ls_cod_livello_prod[4] = string(iuo_excel.uof_read(ll_row, "J"))
	ls_cod_misura_peso = string(iuo_excel.uof_read(ll_row, "K"))
		
	try
		// ci sono dei casi che legge #N/D
		ld_costo_standard =  dec(iuo_excel.uof_read(ll_row, "L"))
		if ld_costo_standard < 0 then ld_costo_standard = 0
	catch(Exception e)
		ld_costo_standard = 0
	end try
	
	// fix
	for ll_i = 1 to 4
		if g_str.isempty(ls_cod_livello_prod[ll_i]) or ls_cod_livello_prod[ll_i] = " "  then
			setnull(ls_cod_livello_prod[ll_i])
		end if
	next
	
	if g_str.isempty(ls_cod_misura_peso) or ls_cod_misura_peso = " " then setnull(ls_cod_misura_peso)
	if g_str.isempty(ls_rif_dogana) or ls_rif_dogana = " " then setnull(ls_rif_dogana)
	
	update anag_prodotti
	set
		cod_livello_prod_1 = :ls_cod_livello_prod[1],
		cod_livello_prod_2 = :ls_cod_livello_prod[2],
		cod_livello_prod_3 = :ls_cod_livello_prod[3],
		cod_livello_prod_4 = :ls_cod_livello_prod[4],
		peso_netto = :ld_peso,
		cod_misura_peso = :ls_cod_misura_peso, 
		costo_standard = :ld_costo_standard,
		cod_nomenclatura = :ls_rif_dogana
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante l'aggiornamento del prodotto " + ls_cod_prodotto + ". " + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
	else
		iuo_excel.uof_set(ll_row, COL_LOG, "OK")
		iuo_log.info(ls_log + "aggiornato")
		commit;
	end if
loop
 
return 1
end function

public function integer wf_insert_prodotti (ref string as_error);/**
 * enme
 * 17/09/2015
 *
 * Aggiorno i prodotti
 * 
 * Return: -1 errore, 1 ok
 **/
 
constant uint COL_LOG = 14

string ls_cod_prodotto, ls_test, ls_log, ls_rif_dogana, ls_cod_livello_prod[], ls_cod_misura_peso, ls_empty_array[], &
		ls_des_prodotto, ls_cod_misura_mag, ls_cod_misura_acq, ls_cod_misura_ven, ls_cod_tipo_politica_riordino
int ll_i
ulong ll_row
decimal{4} ld_peso_netto, ld_costo_standard, ld_fat_conversione_ven, ld_fat_conversione_acq, ld_pezzi_collo

ll_row = 0

if cbx_header.checked then ll_row++
 
do while true
	ll_row++
	st_log.text = "Elaboro riga: " + string(ll_row)
	yield()
	
	ls_cod_prodotto = string(iuo_excel.uof_read(ll_row, "A"))
	if ib_stop or g_str.isempty(ls_cod_prodotto) then exit
	
	ls_log = "Riga " + string(ll_row) + " prodotto " + ls_cod_prodotto + " "
	
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante il controllo esistenza prodotto " + g_str.safe(ls_cod_prodotto) + "." + LR + sqlca.sqlerrtext
		iuo_log.error(ls_log + as_error)
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		return -1
	elseif sqlca.sqlcode = 0 then
		iuo_log.info(ls_log + " già esistente")
		iuo_excel.uof_set(ll_row, COL_LOG, " Già esistente")
		continue
	end if	
	
	// Leggo variabili
	ls_cod_livello_prod = ls_empty_array
	ls_rif_dogana = string(iuo_excel.uof_read(ll_row, "B"))
	ls_des_prodotto = trim(string(iuo_excel.uof_read(ll_row, "C")))
	ls_cod_misura_mag = trim(string(iuo_excel.uof_read(ll_row, "D")))
	ld_peso_netto = dec(iuo_excel.uof_read(ll_row, "E"))
	ld_pezzi_collo = dec(iuo_excel.uof_read(ll_row, "E"))
	ls_cod_livello_prod[1] = string(iuo_excel.uof_read(ll_row, "G"))
	ls_cod_livello_prod[2] = string(iuo_excel.uof_read(ll_row, "H"))
	ls_cod_livello_prod[3] = string(iuo_excel.uof_read(ll_row, "I"))
	ls_cod_livello_prod[4] = string(iuo_excel.uof_read(ll_row, "J"))
	ls_cod_misura_peso = string(iuo_excel.uof_read(ll_row, "K"))	
	ls_cod_misura_acq = string(iuo_excel.uof_read(ll_row, "L"))
	ld_fat_conversione_acq = dec(iuo_excel.uof_read(ll_row, "M"))
	ls_cod_misura_ven = string(iuo_excel.uof_read(ll_row, "N"))
	ld_fat_conversione_acq = dec(iuo_excel.uof_read(ll_row, "O"))
	ls_cod_tipo_politica_riordino = string(iuo_excel.uof_read(ll_row, "P"))

	
	// fix
	for ll_i = 1 to 4
		if g_str.isempty(ls_cod_livello_prod[ll_i]) or ls_cod_livello_prod[ll_i] = " "  then
			setnull(ls_cod_livello_prod[ll_i])
		end if
	next
	
	if g_str.isempty(ls_cod_misura_peso) or ls_cod_misura_peso = " " then setnull(ls_cod_misura_peso)
	if g_str.isempty(ls_rif_dogana) or ls_rif_dogana = " " then setnull(ls_rif_dogana)
	if g_str.isempty(ls_cod_tipo_politica_riordino) or ls_cod_tipo_politica_riordino = " " then setnull(ls_cod_tipo_politica_riordino)
	
	if g_str.isempty(ls_cod_misura_mag) or ls_cod_misura_mag = " " then setnull(ls_cod_misura_mag)
	if isnull(ld_peso_netto) then ld_peso_netto = 0
	if isnull(ld_pezzi_collo) or ld_pezzi_collo = 0 then ld_pezzi_collo = 1
	if g_str.isempty(ls_cod_misura_acq) or ls_cod_misura_acq = " " then setnull(ls_cod_misura_acq)
	if g_str.isempty(ls_cod_misura_ven) or ls_cod_misura_ven = " " then setnull(ls_cod_misura_ven)

	if isnull(ld_fat_conversione_acq) or ld_fat_conversione_acq = 0 then ld_fat_conversione_acq = 1
	if isnull(ld_fat_conversione_ven) or ld_fat_conversione_ven = 0 then ld_fat_conversione_ven = 1
	
	insert into anag_prodotti
		(	cod_azienda,
			cod_prodotto,
			des_prodotto,
			cod_misura_mag,
			cod_nomenclatura,
			cod_livello_prod_1,
			cod_livello_prod_2,
			cod_livello_prod_3,
			cod_livello_prod_4,
			cod_livello_prod_5,
			peso_netto,
			pezzi_collo,
			cod_misura_peso,
			cod_misura_acq,
			fat_conversione_acq,
			cod_misura_ven,
			fat_conversione_ven,
			cod_tipo_politica_riordino )
			values
		(	:s_cs_xx.cod_azienda,
			:ls_cod_prodotto,
			:ls_des_prodotto,
			:ls_cod_misura_mag,
			:ls_rif_dogana,
			:ls_cod_livello_prod[1],
			:ls_cod_livello_prod[2],
			:ls_cod_livello_prod[3],
			:ls_cod_livello_prod[4],
			null,
			:ld_peso_netto,
			:ld_pezzi_collo,
			:ls_cod_misura_peso,
			:ls_cod_misura_acq,
			:ld_fat_conversione_acq,
			:ls_cod_misura_ven,
			:ld_fat_conversione_ven,
			:ls_cod_tipo_politica_riordino);
	
	if sqlca.sqlcode < 0 then
		as_error = "Errore durante l'inserimento del prodotto " + ls_cod_prodotto + ". " + sqlca.sqlerrtext
		iuo_excel.uof_set(ll_row, COL_LOG, as_error)
		iuo_log.error(ls_log + as_error)
		rollback;
	else
		iuo_excel.uof_set(ll_row, COL_LOG, "OK")
		iuo_log.info(ls_log + "inserito")
		commit;
	end if
loop
 
return 1
end function

on w_allinea_prodotti.create
int iCurrent
call super::create
this.cb_stop_insert=create cb_stop_insert
this.cb_3=create cb_3
this.st_11=create st_11
this.st_10=create st_10
this.cb_stop_update=create cb_stop_update
this.st_7=create st_7
this.cb_stop_cancella=create cb_stop_cancella
this.st_log=create st_log
this.cbx_header=create cbx_header
this.cb_2=create cb_2
this.st_6=create st_6
this.st_5=create st_5
this.cb_1=create cb_1
this.st_4=create st_4
this.st_3=create st_3
this.st_file=create st_file
this.cb_selezione=create cb_selezione
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stop_insert
this.Control[iCurrent+2]=this.cb_3
this.Control[iCurrent+3]=this.st_11
this.Control[iCurrent+4]=this.st_10
this.Control[iCurrent+5]=this.cb_stop_update
this.Control[iCurrent+6]=this.st_7
this.Control[iCurrent+7]=this.cb_stop_cancella
this.Control[iCurrent+8]=this.st_log
this.Control[iCurrent+9]=this.cbx_header
this.Control[iCurrent+10]=this.cb_2
this.Control[iCurrent+11]=this.st_6
this.Control[iCurrent+12]=this.st_5
this.Control[iCurrent+13]=this.cb_1
this.Control[iCurrent+14]=this.st_4
this.Control[iCurrent+15]=this.st_3
this.Control[iCurrent+16]=this.st_file
this.Control[iCurrent+17]=this.cb_selezione
this.Control[iCurrent+18]=this.st_2
this.Control[iCurrent+19]=this.st_1
end on

on w_allinea_prodotti.destroy
call super::destroy
destroy(this.cb_stop_insert)
destroy(this.cb_3)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.cb_stop_update)
destroy(this.st_7)
destroy(this.cb_stop_cancella)
destroy(this.st_log)
destroy(this.cbx_header)
destroy(this.cb_2)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.cb_1)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_file)
destroy(this.cb_selezione)
destroy(this.st_2)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;wf_abilita_pulsanti(false)
end event

type cb_stop_insert from commandbutton within w_allinea_prodotti
integer x = 754
integer y = 1720
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma"
end type

event clicked;ib_stop = true
visible = false
end event

type cb_3 from commandbutton within w_allinea_prodotti
integer x = 229
integer y = 1720
integer width = 503
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Avvia"
end type

event clicked;string ls_error

if isvalid(iuo_excel) then destroy iuo_excel
if isvalid(iuo_log) then destroy iuo_log

iuo_excel = create uo_excel
iuo_excel.uof_open(is_file_excel, false)
iuo_excel.uof_set_sheet(3)

iuo_log = create uo_log
iuo_log.open(is_desktop + "insert_prodotti.log", true)

cb_stop_insert.visible = true
ib_stop = false

if wf_insert_prodotti(ref ls_error) < 0 then
	g_mb.error(ls_error)
else
	g_mb.success("Inserimento completato")
end if

destroy iuo_excel
end event

type st_11 from statictext within w_allinea_prodotti
integer x = 229
integer y = 1580
integer width = 2194
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Legge il foglio numero 3 del file excel e INSERISCE in anagrafica prodotti. Il processo termina alla prima riga vuota della colonna A."
boolean focusrectangle = false
end type

type st_10 from statictext within w_allinea_prodotti
integer x = 46
integer y = 1440
integer width = 1509
integer height = 140
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Inserimento nuovi prodotti"
boolean focusrectangle = false
end type

type cb_stop_update from commandbutton within w_allinea_prodotti
integer x = 754
integer y = 1180
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma"
end type

event clicked;ib_stop = true
visible = false
end event

type st_7 from statictext within w_allinea_prodotti
integer x = 23
integer y = 2020
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stato:"
boolean focusrectangle = false
end type

type cb_stop_cancella from commandbutton within w_allinea_prodotti
integer x = 754
integer y = 640
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma"
end type

event clicked;ib_stop = true
visible = false
end event

type st_log from statictext within w_allinea_prodotti
integer x = 274
integer y = 2020
integer width = 1897
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
boolean focusrectangle = false
end type

type cbx_header from checkbox within w_allinea_prodotti
integer x = 251
integer y = 220
integer width = 1074
integer height = 72
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Salta intestazione file (salta la prima riga)"
boolean checked = true
end type

type cb_2 from commandbutton within w_allinea_prodotti
integer x = 229
integer y = 1180
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Avvia"
end type

event clicked;string ls_error

if isvalid(iuo_excel) then destroy iuo_excel
if isvalid(iuo_log) then destroy iuo_log

iuo_excel = create uo_excel
iuo_excel.uof_open(is_file_excel, false)
iuo_excel.uof_set_sheet(1)

iuo_log = create uo_log
iuo_log.open(is_desktop + "update_prodotti.log", true)

cb_stop_update.visible = true
ib_stop = false

if wf_update_prodotti(ref ls_error) < 0 then
	g_mb.error(ls_error)
else
	g_mb.success("Aggiornamento completato")
end if

destroy iuo_excel
end event

type st_6 from statictext within w_allinea_prodotti
integer x = 251
integer y = 1060
integer width = 2171
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Legge il foglio numero 1 del file excel e aggiorna i dati in anagrafica prodotti. Il processo termina alla prima riga vuota della colonna A."
boolean focusrectangle = false
end type

type st_5 from statictext within w_allinea_prodotti
integer x = 46
integer y = 920
integer width = 2075
integer height = 120
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiornamento prodotti magazzino"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_allinea_prodotti
integer x = 229
integer y = 640
integer width = 503
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Avvia"
end type

event clicked;string ls_error
oleobject lole_range

if isvalid(iuo_excel) then destroy iuo_excel
if isvalid(iuo_log) then destroy iuo_log

iuo_excel = create uo_excel
iuo_excel.uof_open(is_file_excel, false)
iuo_excel.uof_set_sheet(2)

iuo_log = create uo_log
iuo_log.open(is_desktop + "cancellazione_prodotti.log", true)

cb_stop_cancella.visible = true
ib_stop = false

if wf_cancella_prodotti(ref ls_error) < 0 then
	g_mb.error(ls_error)
else
	g_mb.success("Cancellazione Completata")
end if

destroy iuo_excel
destroy iuo_log
end event

type st_4 from statictext within w_allinea_prodotti
integer x = 251
integer y = 500
integer width = 2171
integer height = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Legge il foglio numero 2 del file excel e cancella tutti i prodotti trovati nella colonna A. Il processo termina alla prima riga vuota"
boolean focusrectangle = false
end type

type st_3 from statictext within w_allinea_prodotti
integer x = 46
integer y = 380
integer width = 1874
integer height = 120
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Cancellazione prodotti magazzino"
boolean focusrectangle = false
end type

type st_file from statictext within w_allinea_prodotti
integer x = 777
integer y = 140
integer width = 1646
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_selezione from commandbutton within w_allinea_prodotti
integer x = 229
integer y = 120
integer width = 503
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Seleziona File"
end type

event clicked;string ls_docname[]

wf_abilita_pulsanti(false)
st_file.text = ""
is_desktop = guo_functions.uof_get_user_desktop_folder( )

if GetFileOpenName("Select File", is_file_excel, ls_docname[], "Excel","Excel,*.xls;*.xlsx,", is_desktop) > 0 then
	wf_abilita_pulsanti(true)
	st_file.text = is_file_excel
end if
end event

type st_2 from statictext within w_allinea_prodotti
integer x = 229
integer y = 40
integer width = 1943
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Selezione File Excel"
boolean focusrectangle = false
end type

type st_1 from statictext within w_allinea_prodotti
integer x = 46
integer y = 60
integer width = 160
integer height = 160
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "*"
boolean focusrectangle = false
end type

