﻿$PBExportHeader$w_allinea_trasferimenti.srw
forward
global type w_allinea_trasferimenti from window
end type
type cb_log from commandbutton within w_allinea_trasferimenti
end type
type cb_allinea from commandbutton within w_allinea_trasferimenti
end type
type st_log from statictext within w_allinea_trasferimenti
end type
type st_anno from statictext within w_allinea_trasferimenti
end type
type sle_anno from singlelineedit within w_allinea_trasferimenti
end type
type dw_lista from datawindow within w_allinea_trasferimenti
end type
type cb_cerca from commandbutton within w_allinea_trasferimenti
end type
end forward

global type w_allinea_trasferimenti from window
integer width = 4635
integer height = 1960
boolean titlebar = true
string title = "Allinea Trasferimenti"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_log cb_log
cb_allinea cb_allinea
st_log st_log
st_anno st_anno
sle_anno sle_anno
dw_lista dw_lista
cb_cerca cb_cerca
end type
global w_allinea_trasferimenti w_allinea_trasferimenti

type variables
datastore ids_store_bolle
uo_log iuo_log

string is_log_file
end variables

forward prototypes
public function integer wf_crea_datastore_bolle ()
public function integer wf_allinea ()
public function integer wf_aggiungi_bolla (long al_anno, long al_numero)
end prototypes

public function integer wf_crea_datastore_bolle ();/**
 * Crea il datastore delle bolle.
 * Il datastore contiene anno e numero della bolla da ricalcolare
 **/
 
 
string ls_sql

// Select finta per creare la dw di appoggio
ls_sql = "SELECT anno_registrazione, num_registrazione FROM tes_bol_ven WHERE cod_azienda='A01' and anno_registrazione=1900"

guo_functions.uof_crea_datastore(ref ids_store_bolle, ref ls_sql)
 
return 0
end function

public function integer wf_allinea ();string ls_error, ls_ord_ven
long ll_i, ll_row, ll_anno_ord_ven, ll_num_ord_ven, ll_prog_riga_ord_ven, ll_anno_bol_ven, ll_num_bol_ven, ll_prog_riga_bol_ven, &
		ll_anno_mov_mag, ll_num_mov_mag
decimal{4} ld_ord_ven_prezzo_vendita_add, ld_prezzo_vendita, ld_sconto_1, ld_sconto_sbt,ld_imponibile_iva, ld_quan_consegnata,ld_val_movimento
uo_calcola_documento_euro luo_calcolo

iuo_log.info("**********************************************")
iuo_log.info("Inizio allineamento bolle di trasferimento")
iuo_log.info("**********************************************")

ll_row = dw_lista.rowcount()

if ll_row < 1 then return 0

// Parametro SBT
guo_functions.uof_get_parametro_azienda("SBT", ld_sconto_sbt)
if isnull(ld_sconto_sbt) or ld_sconto_sbt < 0 then ld_sconto_sbt = 0

iuo_log.info("Parametro SBT: " + string(ld_sconto_sbt))
iuo_log.info("Anno di analisi: " + sle_anno.text)
// ----

iuo_log.info("")
iuo_log.info("**********************************************")
iuo_log.info("* PASSO 1")
iuo_log.info("* Ricalcolo i prezzi delle bolle di trasferimento")
iuo_log.info("**********************************************")

for ll_i = 1 to ll_row
	
	st_log.text = "Allineo " + string(ll_i) + "/" + string(ll_row)
	yield()
	
	ll_anno_bol_ven = dw_lista.getitemnumber(ll_i, "det_bol_ven_anno_registrazione")
	ll_num_bol_ven  = dw_lista.getitemnumber(ll_i, "det_bol_ven_num_registrazione")
	ll_prog_riga_bol_ven  = dw_lista.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")
	
	ll_anno_ord_ven = dw_lista.getitemnumber(ll_i, "det_ord_ven_anno_registrazione")
	ll_num_ord_ven  = dw_lista.getitemnumber(ll_i, "det_ord_ven_num_registrazione")
	ll_prog_riga_ord_ven = dw_lista.getitemnumber(ll_i, "det_ord_ven_prog_riga_ord_ven")
	ld_prezzo_vendita = dw_lista.getitemnumber(ll_i, "det_ord_ven_prezzo_vendita")
	ld_sconto_1 = dw_lista.getitemnumber(ll_i, "det_ord_ven_sconto_1")
	
	ls_ord_ven = string(ll_anno_ord_ven) + "/" + string(ll_num_ord_ven) + "/" + string(ll_prog_riga_ord_ven)
					
	// Recupero Addizionali
	select sum(prezzo_vendita - (prezzo_vendita * sconto_1 / 100))
	into		:ld_ord_ven_prezzo_vendita_add
	from		det_ord_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_ord_ven and  
				 num_registrazione = :ll_num_ord_ven and
				 num_riga_appartenenza = :ll_prog_riga_ord_ven and
				 cod_tipo_det_ven = 'ADD';
				 
	
	// Non ci sono addizionali, quindi il conto era già corretto
	if isnull(ld_ord_ven_prezzo_vendita_add) or ld_ord_ven_prezzo_vendita_add = 0 then
		iuo_log.info("Bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + " saltata in quanto l'ordine " + ls_ord_ven + " non ha addizionali e quindi corretto.")
		continue
	end if
	
	wf_aggiungi_bolla(ll_anno_bol_ven, ll_num_bol_ven)
	
	ld_ord_ven_prezzo_vendita_add = round(ld_ord_ven_prezzo_vendita_add, 2)

	if ld_sconto_1 > 0 then
		ld_prezzo_vendita = ld_prezzo_vendita * (1 - (ld_sconto_1 / 100))
	end if

	ld_prezzo_vendita += ld_ord_ven_prezzo_vendita_add
	
	if ld_sconto_sbt > 0 then
		ld_prezzo_vendita = ld_prezzo_vendita * (1 - (ld_sconto_sbt / 100))
	end if
	
	ld_prezzo_vendita = round(ld_prezzo_vendita, 2)
	// ----
	
	update det_bol_ven
	set prezzo_vendita = :ld_prezzo_vendita
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_bol_ven and
			 num_registrazione = :ll_num_bol_ven and
			 prog_riga_bol_ven = :ll_prog_riga_bol_ven;
			 
	if sqlca.sqlcode = 0 then
		iuo_log.info("Prezzo vendita aggiornato a " + string(ld_prezzo_vendita, "###,##0.00") + " per riga bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + " rif. ordine " + ls_ord_ven)
		commit;
	else
		iuo_log.error("Errore durante l'aggiornamento del prezzo vendita " + string(ld_prezzo_vendita, "###,##0.00") + " nella bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + " rif. ordine " + ls_ord_ven + ". " + sqlca.sqlerrtext)
		rollback;
	end if
next


// Passo 2: Calcolo i documenti
iuo_log.info("")
iuo_log.info("**********************************************")
iuo_log.info("* PASSO 2")
iuo_log.info("* Calcolo bolle modifiche")
iuo_log.info("**********************************************")
luo_calcolo = create uo_calcola_documento_euro

for ll_i = 1 to ids_store_bolle.rowcount()
	ll_anno_bol_ven = ids_store_bolle.getitemnumber(ll_i, "anno_registrazione")
	ll_num_bol_ven  = ids_store_bolle.getitemnumber(ll_i, "num_registrazione")
	
	st_log.text = "Calcolo bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven)
	yield()
	
	if luo_calcolo.uof_calcola_documento(ll_anno_bol_ven,ll_num_bol_ven,"bol_ven", ls_error) <> 0 then
		destroy luo_calcolo
		iuo_log.error("Errore durante il calcolo della bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + ". " + ls_error)
		rollback;
	else
		iuo_log.info("Calcolata bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven))
		commit;
	end if
next

destroy luo_calcolo

// Passo 3: Aggiorno i movimenti
iuo_log.info("")
iuo_log.info("**********************************************")
iuo_log.info("* PASSO 3")
iuo_log.info("* Aggiorno i movimenti")
iuo_log.info("**********************************************")
for ll_i = 1 to ll_row
	ll_anno_bol_ven = dw_lista.getitemnumber(ll_i, "det_bol_ven_anno_registrazione")
	ll_num_bol_ven  = dw_lista.getitemnumber(ll_i, "det_bol_ven_num_registrazione")
	ll_prog_riga_bol_ven  = dw_lista.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")
	
	ll_anno_mov_mag = dw_lista.getitemnumber(ll_i, "det_bol_ven_anno_registrazione_mov_mag")
	ll_num_mov_mag = dw_lista.getitemnumber(ll_i, "det_bol_ven_num_registrazione_mov_mag")
	
	st_log.text = "Aggiorno i movimenti bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven)
	yield()
	
	// Bolla non confermata salto.
	if isnull(ll_anno_mov_mag) or ll_anno_mov_mag = 0 then
		continue
	end if
	
	// recupero il prezzo vendita ricalcolato
	select imponibile_iva, quan_consegnata
	into :ld_imponibile_iva, :ld_quan_consegnata
	from det_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_bol_ven and
			 num_registrazione = :ll_num_bol_ven and
			 prog_riga_bol_ven = :ll_prog_riga_bol_ven;
			 
	if sqlca.sqlcode = 0 then
		
		if isnull(ld_quan_consegnata) or ld_quan_consegnata = 0 then
			ld_val_movimento = 0
		else
			ld_val_movimento = round( ld_imponibile_iva / ld_quan_consegnata, 4)
		end if
		
		update mov_magazzino
		set val_movimento = :ld_val_movimento
		where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_mov_mag and
			 num_registrazione = :ll_num_mov_mag;
			 
		if sqlca.sqlcode = 0 then
			iuo_log.info("Valore del movimento " + string(ll_anno_mov_mag) + "/" + string(ll_num_mov_mag) + " aggiornato a " + string(ld_val_movimento, "###,##0.00") &
							+ " riferito alla riga bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven))
			commit;
		else
			iuo_log.error("Errore durante l'aggiornamento del del movimento " + string(ll_anno_mov_mag) + "/" + string(ll_num_mov_mag) &
							+ " riferito alla riga bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + ". " + sqlca.sqlerrtext)
			rollback;
		end if
		
	else
		iuo_log.error("Errore durante la ricerca della bolla " + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + "/" + string(ll_prog_riga_bol_ven) + ". " + sqlca.sqlerrtext)
		rollback;
	end if
next

iuo_log.info("")
iuo_log.info("**********************************************")
iuo_log.info("Processo completato")
iuo_log.info("**********************************************")

return 0
end function

public function integer wf_aggiungi_bolla (long al_anno, long al_numero);/**
 * Aggiunge una bolla allo stack delle bolle da ricarcolare
 **/
 
long ll_row 

ll_row = ids_store_bolle.find("anno_registrazione = '" + string(al_anno) + "' and num_registrazione='" + string(al_numero) + "'", 0, ids_store_bolle.rowcount())

if ll_row = 0 then
	
	ll_row = ids_store_bolle.insertrow(0)
	ids_store_bolle.setitem(ll_row, "anno_registrazione", al_anno)
	ids_store_bolle.setitem(ll_row, "num_registrazione", al_numero)
	
end if

return ll_row
end function

on w_allinea_trasferimenti.create
this.cb_log=create cb_log
this.cb_allinea=create cb_allinea
this.st_log=create st_log
this.st_anno=create st_anno
this.sle_anno=create sle_anno
this.dw_lista=create dw_lista
this.cb_cerca=create cb_cerca
this.Control[]={this.cb_log,&
this.cb_allinea,&
this.st_log,&
this.st_anno,&
this.sle_anno,&
this.dw_lista,&
this.cb_cerca}
end on

on w_allinea_trasferimenti.destroy
destroy(this.cb_log)
destroy(this.cb_allinea)
destroy(this.st_log)
destroy(this.st_anno)
destroy(this.sle_anno)
destroy(this.dw_lista)
destroy(this.cb_cerca)
end on

event open;
dw_lista.settransobject(sqlca)

wf_crea_datastore_bolle()
end event

type cb_log from commandbutton within w_allinea_trasferimenti
integer x = 1074
integer y = 20
integer width = 389
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "File di Log"
end type

event clicked;string docpath, docname[], ls_desktop
integer i, li_cnt, li_rtn, li_filenum

cb_allinea.enabled = false
ls_desktop = guo_functions.uof_get_user_desktop_folder()

setnull(is_log_file)

li_rtn = GetFileOpenName("Select File", &
   docpath, docname[], "TXT", &
   + "Text Files (*.TXT),*.TXT,", &
   ls_desktop, 18)

IF li_rtn < 1 THEN return
li_cnt = Upperbound(docname)

// if only one file is picked, docpath contains the 
// path and file name
is_log_file = string(docpath)
st_log.text = "Log in " + is_log_file
cb_allinea.enabled = true

end event

type cb_allinea from commandbutton within w_allinea_trasferimenti
integer x = 1509
integer y = 20
integer width = 402
integer height = 92
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "Allinea"
end type

event clicked;
if g_mb.confirm("Procedo con l'allineamento?") then
	
	iuo_log = create uo_log
	iuo_log.open(is_log_file)
	
	wf_allinea()
	
	destroy iuo_log
	
end if
end event

type st_log from statictext within w_allinea_trasferimenti
integer x = 3131
integer y = 40
integer width = 1417
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "log"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_anno from statictext within w_allinea_trasferimenti
integer x = 46
integer y = 20
integer width = 183
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Anno"
boolean focusrectangle = false
end type

type sle_anno from singlelineedit within w_allinea_trasferimenti
integer x = 229
integer y = 20
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "2014"
borderstyle borderstyle = stylelowered!
end type

type dw_lista from datawindow within w_allinea_trasferimenti
integer x = 23
integer y = 140
integer width = 4549
integer height = 1680
integer taborder = 20
string title = "none"
string dataobject = "d_allinea_trasferimenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event retrievestart;st_log.text = "Caricamento..."
end event

type cb_cerca from commandbutton within w_allinea_trasferimenti
integer x = 640
integer y = 20
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cerca"
end type

event clicked;long ll_row

dw_lista.reset()
ll_row = dw_lista.retrieve(s_cs_xx.cod_azienda, integer(sle_anno.text))

st_log.text = "Trovate " + string(ll_row)
end event

