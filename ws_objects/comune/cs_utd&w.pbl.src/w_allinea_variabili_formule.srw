﻿$PBExportHeader$w_allinea_variabili_formule.srw
forward
global type w_allinea_variabili_formule from w_cs_xx_principale
end type
type hpb_status from hprogressbar within w_allinea_variabili_formule
end type
type mle_log from multilineedit within w_allinea_variabili_formule
end type
type st_help from statictext within w_allinea_variabili_formule
end type
type cb_1 from commandbutton within w_allinea_variabili_formule
end type
end forward

global type w_allinea_variabili_formule from w_cs_xx_principale
integer width = 2254
integer height = 1480
string title = "Allinea Variabili Formule"
hpb_status hpb_status
mle_log mle_log
st_help st_help
cb_1 cb_1
end type
global w_allinea_variabili_formule w_allinea_variabili_formule

forward prototypes
public function integer wf_avvia_procedura (ref string as_error)
end prototypes

public function integer wf_avvia_procedura (ref string as_error);string ls_cod_formula, ls_variabili[]
long ll_count_formule, ll_i, li_count_variabili, ll_j
uo_formule_calcolo luo_formule
datastore lds_store


hpb_status.visible = true
hpb_status.position = 0
mle_log.text = ""


// Datastore formule
ll_count_formule = guo_functions.uof_crea_datastore(lds_store, &
	"SELECT cod_formula FROM tab_formule_db WHERE cod_azienda='" + s_cs_xx.cod_azienda  +"' ", &
	as_error)
	
if ll_count_formule < 0 then
	return -1
end if

hpb_status.maxposition = ll_count_formule
luo_formule = create uo_formule_calcolo

for ll_i = 1 to ll_count_formule
	hpb_status.position = ll_i
	
	ls_cod_formula = lds_store.getitemstring(ll_i, 1)
	
	mle_log.text += "Processo formula: " + ls_cod_formula
	
	li_count_variabili = luo_formule.uof_allinea_variabili_formula(ls_cod_formula, as_error)
	if li_count_variabili <0 then return -1
	
	mle_log.text += " - trovate: " + string(li_count_variabili) + " variabili"
	mle_log.text += "~r~n"
next


return 1
end function

on w_allinea_variabili_formule.create
int iCurrent
call super::create
this.hpb_status=create hpb_status
this.mle_log=create mle_log
this.st_help=create st_help
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_status
this.Control[iCurrent+2]=this.mle_log
this.Control[iCurrent+3]=this.st_help
this.Control[iCurrent+4]=this.cb_1
end on

on w_allinea_variabili_formule.destroy
call super::destroy
destroy(this.hpb_status)
destroy(this.mle_log)
destroy(this.st_help)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;/**
 * stefanop
 * 10/07/2014
 *
 * La finestra consente di allineare la tabella tab_formule_db_varibili che contiene
 * le varibili usate all'interno di una formula
 **/
 

end event

type hpb_status from hprogressbar within w_allinea_variabili_formule
boolean visible = false
integer x = 1349
integer y = 220
integer width = 805
integer height = 60
unsignedinteger maxposition = 100
unsignedinteger position = 50
integer setstep = 10
end type

type mle_log from multilineedit within w_allinea_variabili_formule
integer x = 69
integer y = 340
integer width = 2080
integer height = 1000
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type st_help from statictext within w_allinea_variabili_formule
integer x = 46
integer y = 40
integer width = 2103
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 8388608
long backcolor = 12632256
string text = "Allinea la tabella tab_formule_db_varibili con le variabili usante all~'interno delle formule"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_allinea_variabili_formule
integer x = 46
integer y = 200
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Allinea"
end type

event clicked;string ls_error

hpb_status.visible = false
hpb_status.maxposition = 0

if g_mb.confirm("Confermi avvio procedura?") then
	
	if wf_avvia_procedura(ref ls_error) < 0 then
		mle_log.text += "~r~n---------------------------------"
		mle_log.text += ls_error
		g_mb.error(ls_error)
		rollback;
	else
		mle_log.text += "~r~n---------------------------------"
		mle_log.text += "SUCCESS"
		
		g_mb.success("Procedura completata con successo")
	end if
	
	hpb_status.visible = false
	
end if
end event

