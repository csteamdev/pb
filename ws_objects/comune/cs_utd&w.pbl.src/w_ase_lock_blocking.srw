﻿$PBExportHeader$w_ase_lock_blocking.srw
forward
global type w_ase_lock_blocking from w_cs_xx_principale
end type
type st_2 from statictext within w_ase_lock_blocking
end type
type cbx_1 from checkbox within w_ase_lock_blocking
end type
type cb_1 from commandbutton within w_ase_lock_blocking
end type
type em_timer from editmask within w_ase_lock_blocking
end type
type st_1 from statictext within w_ase_lock_blocking
end type
type dw_1 from datawindow within w_ase_lock_blocking
end type
end forward

global type w_ase_lock_blocking from w_cs_xx_principale
integer width = 3419
integer height = 1760
string title = "Locks Bloccanti ASE"
st_2 st_2
cbx_1 cbx_1
cb_1 cb_1
em_timer em_timer
st_1 st_1
dw_1 dw_1
end type
global w_ase_lock_blocking w_ase_lock_blocking

on w_ase_lock_blocking.create
int iCurrent
call super::create
this.st_2=create st_2
this.cbx_1=create cbx_1
this.cb_1=create cb_1
this.em_timer=create em_timer
this.st_1=create st_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.cbx_1
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.em_timer
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.dw_1
end on

on w_ase_lock_blocking.destroy
call super::destroy
destroy(this.st_2)
destroy(this.cbx_1)
destroy(this.cb_1)
destroy(this.em_timer)
destroy(this.st_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.settransobject(sqlca)
cb_1.postevent("clicked")


end event

event timer;call super::timer;dw_1.event ue_retrieve()


end event

type st_2 from statictext within w_ase_lock_blocking
integer x = 27
integer y = 1460
integer width = 3296
integer height = 176
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Attenzione: per terminare il processo, cliccare su KILL e poi digitare il comando KILL seguito dallo SPID, fare INVIO e poi digitare GO e dare INVIO; chiudere la finestra al termine"
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_ase_lock_blocking
integer x = 2203
integer y = 1268
integer width = 347
integer height = 96
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Alarm"
boolean checked = true
end type

type cb_1 from commandbutton within w_ase_lock_blocking
event ue_timer pbm_timer
integer x = 1915
integer y = 1272
integer width = 238
integer height = 92
integer taborder = 30
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "SET"
end type

event clicked;timer( long(em_timer.text))

end event

type em_timer from editmask within w_ase_lock_blocking
integer x = 1632
integer y = 1260
integer width = 247
integer height = 112
integer taborder = 20
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "5"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
end type

type st_1 from statictext within w_ase_lock_blocking
integer x = 1184
integer y = 1276
integer width = 416
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "REFRESH SEC:"
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_ase_lock_blocking
event ue_retrieve ( )
integer x = 27
integer y = 24
integer width = 3319
integer height = 1184
integer taborder = 10
string title = "none"
string dataobject = "d_ase_locks_blocking"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();long ll_ret

ll_ret = dw_1.retrieve()

if cbx_1.checked and ll_ret > 0 then
	
	beep( 3 )
	
end if
end event

event buttonclicked;//long ll_procid
string ls_sql
//
//if row > 0 then
//	choose case dwo.name
//		case "b_kill"
//			
//			ll_procid = getitemnumber(row, "process_id")
//			ls_sql = "begin transaction  " + char(13) + char(10) + " go "  + char(13) + char(10) + "kill " + string(ll_procid)  + char(13) + char(10) + " go " + char(13) + char(10) +"commit transaction go"
//			execute immediate :ls_sql;
//			
//			event ue_retrieve()
//	end choose
//end if

ls_sql = "isql -S" + sqlca.servername + " -Uadmin -PGIBadm30"

run(ls_sql)
end event

