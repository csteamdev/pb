﻿$PBExportHeader$w_calcolo_fattura.srw
forward
global type w_calcolo_fattura from window
end type
type st_1 from statictext within w_calcolo_fattura
end type
type cb_1 from commandbutton within w_calcolo_fattura
end type
type mle_1 from multilineedit within w_calcolo_fattura
end type
type dw_1 from datawindow within w_calcolo_fattura
end type
end forward

global type w_calcolo_fattura from window
integer width = 2875
integer height = 1684
boolean titlebar = true
string title = "Calcola Fattura"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
st_1 st_1
cb_1 cb_1
mle_1 mle_1
dw_1 dw_1
end type
global w_calcolo_fattura w_calcolo_fattura

on w_calcolo_fattura.create
this.st_1=create st_1
this.cb_1=create cb_1
this.mle_1=create mle_1
this.dw_1=create dw_1
this.Control[]={this.st_1,&
this.cb_1,&
this.mle_1,&
this.dw_1}
end on

on w_calcolo_fattura.destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.mle_1)
destroy(this.dw_1)
end on

event open;dw_1.settransobject(sqlca)
dw_1.retrieve()
end event

type st_1 from statictext within w_calcolo_fattura
integer x = 5
integer y = 864
integer width = 2194
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_calcolo_fattura
integer x = 2263
integer y = 860
integer width = 526
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CALCOLA"
end type

event clicked;string ls_messaggio, ls_MSC, ls_flag_contabilita
long   ll_anno_reg, ll_num_reg, ll_righe, ll_i
uo_calcola_documento_euro luo_calcola_documento_euro


ll_righe = dw_1.rowcount()

for ll_i = 1 to ll_righe

	ll_anno_reg = dw_1.getitemnumber(ll_i,"anno_registrazione")
	ll_num_reg = dw_1.getitemnumber(ll_i,"num_registrazione")
	
	st_1.text = "calcolo fattura " + string(ll_i) + "/"  + string(ll_righe) + " ... in corso"
	
	luo_calcola_documento_euro = create uo_calcola_documento_euro
	
	//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
	luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
	
	if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_reg,ll_num_reg,"fat_ven",ls_messaggio) <> 0 then
		//Donato 05-11-2008 dare msg solo se c'è
		if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
		rollback;
		exit
	else
		commit;
	end if
	destroy luo_calcola_documento_euro

next

commit; 
end event

type mle_1 from multilineedit within w_calcolo_fattura
integer x = 23
integer y = 980
integer width = 2789
integer height = 680
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from datawindow within w_calcolo_fattura
integer y = 20
integer width = 2811
integer height = 820
integer taborder = 10
string title = "none"
string dataobject = "d_lista_fatture_calcolo"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

