﻿$PBExportHeader$w_cambia_anno.srw
forward
global type w_cambia_anno from w_cs_xx_principale
end type
type dw_status from datawindow within w_cambia_anno
end type
type cb_imposta from commandbutton within w_cambia_anno
end type
type em_anno from editmask within w_cambia_anno
end type
type st_2 from statictext within w_cambia_anno
end type
type st_1 from statictext within w_cambia_anno
end type
end forward

global type w_cambia_anno from w_cs_xx_principale
integer width = 1637
integer height = 1480
string title = "Cambio Anno"
dw_status dw_status
cb_imposta cb_imposta
em_anno em_anno
st_2 st_2
st_1 st_1
end type
global w_cambia_anno w_cambia_anno

type variables
constant long SUCCESS = 8454016
constant long WARN = 8454143
constant long FAIL = 8421631

constant string LETTERE_NUMERATORI = "ABCDEFGHILMNOPQ"
end variables

forward prototypes
public function integer wf_aggiorna_anno (integer ai_anno)
public subroutine wf_aggiungi_stato (long al_color, string as_message)
public subroutine wf_aggiorna_esc_azienda (string as_cod_azienda, integer ai_anno)
public subroutine wf_aggiorna_numeratori_azienda (string as_cod_azienda, integer ai_anno)
public subroutine wf_aggiorna_con_mag (string as_cod_azienda, integer ai_anno)
end prototypes

public function integer wf_aggiorna_anno (integer ai_anno);/**
 * stefanop
 * 27/04/2016
 *
 * Aggiornamento anno ESC
 **/

string ls_cod_azienda
int ll_aziende, ll_i
datastore lds_store

ll_aziende = guo_functions.uof_crea_datastore(lds_store, "select cod_azienda from aziende")
for ll_i = 1 to ll_aziende
	ls_cod_azienda = lds_store.getitemstring(ll_i, "cod_azienda")
	wf_aggiorna_esc_azienda(ls_cod_azienda, ai_anno)
	
	wf_aggiorna_numeratori_azienda(ls_cod_azienda, ai_anno)
	
	wf_aggiorna_con_mag(ls_cod_azienda, ai_anno)
next
 


return 1
end function

public subroutine wf_aggiungi_stato (long al_color, string as_message);int ll_row

ll_row = dw_status.insertrow(0)
dw_status.setitem(ll_row, "box_color", al_color)
dw_status.setitem(ll_row, "descrizione", as_message)
end subroutine

public subroutine wf_aggiorna_esc_azienda (string as_cod_azienda, integer ai_anno);update parametri_azienda
set numero = :ai_anno
where cod_azienda = :as_cod_azienda and
		flag_parametro = 'N' and
		cod_parametro = 'ESC';
		
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante l'aggiornamento del parametro esc", sqlca)
	wf_aggiungi_stato(FAIL, "Parametro ESC azienda " + as_cod_azienda)
else
	wf_aggiungi_stato(SUCCESS, "Parametro ESC azienda " + as_cod_azienda)
end if
end subroutine

public subroutine wf_aggiorna_numeratori_azienda (string as_cod_azienda, integer ai_anno);/**
 * stefanop
 * 27/04/2016
 *
 * Imposta i numeratori per l'azienda passata
 **/

string ls_sql, ls_cod_documento, ls_numeratore_documento
int li_count, li_i, li_j, li_num_numeratori, li_exists
datetime ldt_data
datastore lds_store

ldt_data = datetime(today(), now())
ls_sql =  "select cod_documento, num_numeratori from tab_documenti where cod_azienda='" + as_cod_azienda + "'"
li_count = guo_functions.uof_crea_datastore(lds_store,ls_sql)

for li_i = 1 to li_count
	ls_cod_documento = lds_store.getitemstring(li_i, "cod_documento")
	li_num_numeratori = lds_store.getitemnumber(li_i, "num_numeratori")
	
	for li_j = 1 to li_num_numeratori
		ls_numeratore_documento = mid(LETTERE_NUMERATORI, li_j, 1)
		
		// Esiste già?
		select count(*)
		into :li_exists
		from numeratori
		where cod_azienda = :as_cod_azienda and
				cod_documento = :ls_cod_documento and
				numeratore_documento = :ls_numeratore_documento and
				anno_documento = :ai_anno;
				
		if sqlca.sqlcode = 100 or li_exists < 1 then
			INSERT INTO numeratori(
				cod_azienda, cod_documento, 
				numeratore_documento, anno_documento, 
				num_documento, data_ultima
			) values (
				:as_cod_azienda,
				:ls_cod_documento,
				:ls_numeratore_documento,
				:ai_anno,
				1,
				:ldt_data);
				
				if sqlca.sqlcode = 0 then
					wf_aggiungi_stato(SUCCESS, "Inserito numeratore " + as_cod_azienda + " - " + ls_cod_documento + " - " + ls_numeratore_documento)
				else
					wf_aggiungi_stato(FAIL, "Errore inserimento numeratore " + as_cod_azienda + " - " + ls_cod_documento + " - " + ls_numeratore_documento)
				end if
		else
			wf_aggiungi_stato(WARN, "Numeratore " + as_cod_azienda + " - " + ls_cod_documento + " - " + ls_numeratore_documento + " esistente, saltato")
		end if
	next
next
end subroutine

public subroutine wf_aggiorna_con_mag (string as_cod_azienda, integer ai_anno);update con_magazzino
set num_registrazione = 0, prog_mov = 0
where cod_azienda = :as_cod_azienda;

if sqlca.sqlcode = 0 then
	wf_aggiungi_stato(SUCCESS, "Reset numero registrazione con_magazzino")
else
	wf_aggiungi_stato(FAIL, "Reset numero registrazione con_magazzino")
end if
end subroutine

on w_cambia_anno.create
int iCurrent
call super::create
this.dw_status=create dw_status
this.cb_imposta=create cb_imposta
this.em_anno=create em_anno
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_status
this.Control[iCurrent+2]=this.cb_imposta
this.Control[iCurrent+3]=this.em_anno
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
end on

on w_cambia_anno.destroy
call super::destroy
destroy(this.dw_status)
destroy(this.cb_imposta)
destroy(this.em_anno)
destroy(this.st_2)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;em_anno.text = string(year(date(now())))
end event

type dw_status from datawindow within w_cambia_anno
integer x = 37
integer y = 256
integer width = 1518
integer height = 1072
integer taborder = 40
string title = "none"
string dataobject = "d_grid_status"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_imposta from commandbutton within w_cambia_anno
integer x = 969
integer y = 128
integer width = 585
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Anno"
end type

event clicked;wf_aggiorna_anno(integer(em_anno.text))
end event

type em_anno from editmask within w_cambia_anno
integer x = 421
integer y = 128
integer width = 311
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2015"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####"
end type

type st_2 from statictext within w_cambia_anno
integer x = 37
integer y = 144
integer width = 366
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nuovo Anno:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_cambia_anno
integer x = 37
integer y = 32
integer width = 1166
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Cambia anno e numeratori per l~'applicazione"
boolean focusrectangle = false
end type

