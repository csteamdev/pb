﻿$PBExportHeader$w_cambia_cod_prodotto.srw
$PBExportComments$Finestra Richiesta Modifica Cod Prod a 15 Car - Import Files
forward
global type w_cambia_cod_prodotto from w_cs_xx_risposta
end type
type st_1 from statictext within w_cambia_cod_prodotto
end type
type sle_1 from singlelineedit within w_cambia_cod_prodotto
end type
type st_2 from statictext within w_cambia_cod_prodotto
end type
type sle_2 from singlelineedit within w_cambia_cod_prodotto
end type
type st_3 from statictext within w_cambia_cod_prodotto
end type
type cb_1 from commandbutton within w_cambia_cod_prodotto
end type
type cb_2 from commandbutton within w_cambia_cod_prodotto
end type
end forward

global type w_cambia_cod_prodotto from w_cs_xx_risposta
int Width=1422
int Height=565
st_1 st_1
sle_1 sle_1
st_2 st_2
sle_2 sle_2
st_3 st_3
cb_1 cb_1
cb_2 cb_2
end type
global w_cambia_cod_prodotto w_cambia_cod_prodotto

on open;call w_cs_xx_risposta::open;sle_1.text = s_cs_xx.parametri.parametro_s_1
end on

on w_cambia_cod_prodotto.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.sle_1=create sle_1
this.st_2=create st_2
this.sle_2=create sle_2
this.st_3=create st_3
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=sle_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=sle_2
this.Control[iCurrent+5]=st_3
this.Control[iCurrent+6]=cb_1
this.Control[iCurrent+7]=cb_2
end on

on w_cambia_cod_prodotto.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.sle_1)
destroy(this.st_2)
destroy(this.sle_2)
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.cb_2)
end on

type st_1 from statictext within w_cambia_cod_prodotto
int X=23
int Y=21
int Width=1340
int Height=93
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Adattamento del Codice Prodotto ai 15 Caratteri"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_cambia_cod_prodotto
int X=412
int Y=141
int Width=915
int Height=81
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_cambia_cod_prodotto
int X=115
int Y=141
int Width=289
int Height=73
boolean Enabled=false
string Text="Vs Codice:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_2 from singlelineedit within w_cambia_cod_prodotto
int X=412
int Y=241
int Width=915
int Height=81
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_cambia_cod_prodotto
int X=23
int Y=241
int Width=380
int Height=73
boolean Enabled=false
string Text="Nuovo Codice:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_cambia_cod_prodotto
int X=1029
int Y=341
int Width=316
int Height=101
int TabOrder=30
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;string ls_prodotto
if isnull(sle_2.text) then
   g_mb.messagebox("Cambio Codice Prodotto","Inserire un nuovo codice prodotto")
   sle_2.setfocus()
   return
end if

if len(sle_2.text) > 15 then
   g_mb.messagebox("Cambio Codice Prodotto","Inserire un codice con memno di 15 caratteri")
   sle_2.setfocus()
   return
end if

SELECT anag_prodotti.cod_prodotto
INTO   :ls_prodotto
FROM   anag_prodotti
WHERE  anag_prodotti.cod_prodotto = :sle_2.text ;


if (SQLCA.SQLCode = -1) then   g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)

if SQLCA.SQLCode = 0 then
   g_mb.messagebox("Cambio Codice Prodotto","Il codice digitato è già esistente")
   sle_2.setfocus()
   return
end if

s_cs_xx.parametri.parametro_s_2 = sle_2.text
close(w_cambia_cod_prodotto)


end on

type cb_2 from commandbutton within w_cambia_cod_prodotto
int X=686
int Y=341
int Width=316
int Height=101
int TabOrder=20
string Text="&Salta"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;setnull(s_cs_xx.parametri.parametro_s_2)
close(w_cambia_cod_prodotto)
end on

