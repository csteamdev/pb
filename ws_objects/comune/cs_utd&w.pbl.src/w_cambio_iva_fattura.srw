﻿$PBExportHeader$w_cambio_iva_fattura.srw
$PBExportComments$Finestra di cambio iva sulle fatture
forward
global type w_cambio_iva_fattura from w_cs_xx_risposta
end type
type dw_cambio_iva from uo_cs_xx_dw within w_cambio_iva_fattura
end type
type cb_esegui from commandbutton within w_cambio_iva_fattura
end type
type cb_annulla from commandbutton within w_cambio_iva_fattura
end type
type st_1 from statictext within w_cambio_iva_fattura
end type
type em_anno_fattura from editmask within w_cambio_iva_fattura
end type
type st_2 from statictext within w_cambio_iva_fattura
end type
type em_num_fattura from editmask within w_cambio_iva_fattura
end type
type st_4 from statictext within w_cambio_iva_fattura
end type
type st_3 from statictext within w_cambio_iva_fattura
end type
type mle_1 from multilineedit within w_cambio_iva_fattura
end type
end forward

global type w_cambio_iva_fattura from w_cs_xx_risposta
int Width=2122
int Height=929
boolean TitleBar=true
string Title="Cambio IVA"
dw_cambio_iva dw_cambio_iva
cb_esegui cb_esegui
cb_annulla cb_annulla
st_1 st_1
em_anno_fattura em_anno_fattura
st_2 st_2
em_num_fattura em_num_fattura
st_4 st_4
st_3 st_3
mle_1 mle_1
end type
global w_cambio_iva_fattura w_cambio_iva_fattura

type variables
string is_tabella_det
long il_anno_registrazione, il_num_registrazione

end variables

forward prototypes
public function integer wf_aggiorna_iva (string as_tabella, long al_anno_registrazione, long al_num_registrazione, string as_cod_iva_da, string as_cod_iva_a, ref string as_messaggio)
end prototypes

public function integer wf_aggiorna_iva (string as_tabella, long al_anno_registrazione, long al_num_registrazione, string as_cod_iva_da, string as_cod_iva_a, ref string as_messaggio);integer li_risposta
string ls_sql


ls_sql = "update " + as_tabella + " set cod_iva ='" + as_cod_iva_a + "' where " + &
		 "anno_registrazione =" + string(al_anno_registrazione) + &
		 " and num_registrazione = " + string(al_num_registrazione)

if (not isnull(as_cod_iva_da)) then
	ls_sql = ls_sql + " and cod_iva ='" + as_cod_iva_da +"' "
end if

EXECUTE IMMEDIATE :ls_sql ;

li_risposta = sqlca.sqlcode
as_messaggio = sqlca.sqlerrtext

return li_risposta 
end function

on w_cambio_iva_fattura.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_cambio_iva=create dw_cambio_iva
this.cb_esegui=create cb_esegui
this.cb_annulla=create cb_annulla
this.st_1=create st_1
this.em_anno_fattura=create em_anno_fattura
this.st_2=create st_2
this.em_num_fattura=create em_num_fattura
this.st_4=create st_4
this.st_3=create st_3
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cambio_iva
this.Control[iCurrent+2]=cb_esegui
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=em_anno_fattura
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=em_num_fattura
this.Control[iCurrent+8]=st_4
this.Control[iCurrent+9]=st_3
this.Control[iCurrent+10]=mle_1
end on

on w_cambio_iva_fattura.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_cambio_iva)
destroy(this.cb_esegui)
destroy(this.cb_annulla)
destroy(this.st_1)
destroy(this.em_anno_fattura)
destroy(this.st_2)
destroy(this.em_num_fattura)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.mle_1)
end on

event pc_setwindow;call super::pc_setwindow;is_tabella_det = "det_fat_ven"
il_anno_registrazione = 0
il_num_registrazione = 0

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_cambio_iva.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)




end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_cambio_iva, &
                 "cod_iva_da", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
					  
f_po_loaddddw_dw(dw_cambio_iva, &
                 "cod_iva_a", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_cambio_iva from uo_cs_xx_dw within w_cambio_iva_fattura
int X=23
int Y=21
int Width=2058
int Height=201
int TabOrder=20
string DataObject="d_cambio_iva"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type cb_esegui from commandbutton within w_cambio_iva_fattura
int X=1692
int Y=721
int Width=375
int Height=81
int TabOrder=30
boolean BringToTop=true
string Text="Esegui"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_iva_da, ls_cod_iva_a, ls_messaggio
long ll_risp

dw_cambio_iva.accepttext()
il_anno_registrazione = long(em_anno_fattura.text)
il_num_registrazione  = long(em_num_fattura.text)

ls_cod_iva_da = dw_cambio_iva.getitemstring(1, "cod_iva_da")
ls_cod_iva_a = dw_cambio_iva.getitemstring(1, "cod_iva_a")

if isnull(ls_cod_iva_a) then
	g_mb.messagebox("Apice", "Inserire il nuovo codice iva")
	return
end if
g_mb.messagebox("Apice: Attenzione!", "Ricordarsi che le righe di riferimento con Aliquota iva non nulla possono causare problemi nel calcolo fattura.", Information!)
ll_risp = wf_aggiorna_iva(is_tabella_det, il_anno_registrazione, il_num_registrazione, ls_cod_iva_da, ls_cod_iva_a, ls_messaggio)
if ll_risp < 0 then
	g_mb.messagebox("Apice", "Errore in aggiornamento dettagli: " + ls_messaggio)
elseif ll_risp = 0 then	// ok
	g_mb.messagebox("Apice", "Aggiornamento iva eseguito correttamente")
else			// ll_risp = 100: eseguito su nessuna riga
	g_mb.messagebox("Apice", "Aggiornamento iva non eseguito")
end if
s_cs_xx.parametri.parametro_i_1 = ll_risp		// operazione andata a buon fine
close(parent)
end event

type cb_annulla from commandbutton within w_cambio_iva_fattura
int X=1281
int Y=721
int Width=375
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
s_cs_xx.parametri.parametro_i_1 = 100	//operazione interrotta da utente oppure nessuna riga aggiornata

close(parent)


end event

type st_1 from statictext within w_cambio_iva_fattura
int X=23
int Y=241
int Width=2035
int Height=141
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
string Text="Selezionare l'eventuale IVA da cambiare, indicare la nuova aliquota, quindi fare click su esegui"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_anno_fattura from editmask within w_cambio_iva_fattura
int X=435
int Y=561
int Width=389
int Height=81
int TabOrder=60
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="#####0"
string DisplayData=""
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_cambio_iva_fattura
int X=46
int Y=561
int Width=371
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Anno Fattura: "
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=80269524
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_num_fattura from editmask within w_cambio_iva_fattura
int X=1669
int Y=561
int Width=389
int Height=81
int TabOrder=40
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="#####0"
string DisplayData="4"
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_cambio_iva_fattura
int X=892
int Y=561
int Width=755
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Num. Registrazione  Fattura: "
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=80269524
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_cambio_iva_fattura
int X=46
int Y=461
int Width=2035
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="INDICARE IL NUMERO INTERNO DI REGISTRAZIONE DELLA FATTURA."
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_1 from multilineedit within w_cambio_iva_fattura
int X=23
int Y=661
int Width=1235
int Height=161
int TabOrder=50
boolean BringToTop=true
Alignment Alignment=Center!
string Text="Attenzione! La modfica avviene anche sulle fatture già emesse e confermate."
long TextColor=255
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

