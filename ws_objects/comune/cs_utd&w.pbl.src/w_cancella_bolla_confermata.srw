﻿$PBExportHeader$w_cancella_bolla_confermata.srw
forward
global type w_cancella_bolla_confermata from window
end type
type st_8 from statictext within w_cancella_bolla_confermata
end type
type st_7 from statictext within w_cancella_bolla_confermata
end type
type st_6 from statictext within w_cancella_bolla_confermata
end type
type st_5 from statictext within w_cancella_bolla_confermata
end type
type cb_esegui from commandbutton within w_cancella_bolla_confermata
end type
type st_4 from statictext within w_cancella_bolla_confermata
end type
type st_3 from statictext within w_cancella_bolla_confermata
end type
type st_2 from statictext within w_cancella_bolla_confermata
end type
type st_1 from statictext within w_cancella_bolla_confermata
end type
type em_num_bolla from editmask within w_cancella_bolla_confermata
end type
type em_anno_bolla from editmask within w_cancella_bolla_confermata
end type
type em_numeratore from editmask within w_cancella_bolla_confermata
end type
type em_cod_documento from editmask within w_cancella_bolla_confermata
end type
type gb_1 from groupbox within w_cancella_bolla_confermata
end type
end forward

global type w_cancella_bolla_confermata from window
integer width = 2405
integer height = 1288
boolean titlebar = true
string title = "Cancellazione Bolle Confermate"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
cb_esegui cb_esegui
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
em_num_bolla em_num_bolla
em_anno_bolla em_anno_bolla
em_numeratore em_numeratore
em_cod_documento em_cod_documento
gb_1 gb_1
end type
global w_cancella_bolla_confermata w_cancella_bolla_confermata

on w_cancella_bolla_confermata.create
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.cb_esegui=create cb_esegui
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_num_bolla=create em_num_bolla
this.em_anno_bolla=create em_anno_bolla
this.em_numeratore=create em_numeratore
this.em_cod_documento=create em_cod_documento
this.gb_1=create gb_1
this.Control[]={this.st_8,&
this.st_7,&
this.st_6,&
this.st_5,&
this.cb_esegui,&
this.st_4,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_num_bolla,&
this.em_anno_bolla,&
this.em_numeratore,&
this.em_cod_documento,&
this.gb_1}
end on

on w_cancella_bolla_confermata.destroy
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.cb_esegui)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_num_bolla)
destroy(this.em_anno_bolla)
destroy(this.em_numeratore)
destroy(this.em_cod_documento)
destroy(this.gb_1)
end on

event open;em_cod_documento.setfocus()
end event

type st_8 from statictext within w_cancella_bolla_confermata
integer x = 46
integer y = 340
integer width = 2299
integer height = 180
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "ATTENZIONE: Se la bolla è stata originata da un ordine, quest~'ultimo non sarà ripristinato."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_7 from statictext within w_cancella_bolla_confermata
integer x = 46
integer y = 240
integer width = 2286
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "di magazzino; il magazzino sarà ripristinato alla situazione inziale."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_cancella_bolla_confermata
integer x = 46
integer y = 140
integer width = 2286
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Verranno cancellate tutte le righe della bolla, la testata e gli eventuali movimenti"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_5 from statictext within w_cancella_bolla_confermata
integer x = 46
integer y = 40
integer width = 2286
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Questa procedura permette di cancellare una bolla confermata."
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_esegui from commandbutton within w_cancella_bolla_confermata
integer x = 983
integer y = 980
integer width = 434
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cancella Bolla"
end type

event clicked;string ls_cod_documento, ls_numeratore
long ll_anno_registrazione, ll_num_registrazione, ll_anno_bolla, ll_num_bolla, ll_anno_registrazione_mov_mag, &
     ll_num_registrazione_mov_mag, ll_prog_riga_bol_ven, ll_conta_fatture
	  uo_magazzino luo_mag

if g_mb.messagebox("APICE","Sei sicuro di voler procedere con la cancellazione della bolla?",Question!,YesNo!,2) = 2 then return
ls_cod_documento = em_cod_documento.text
ls_numeratore = em_numeratore.text
ll_anno_bolla = long(em_anno_bolla.text)
ll_num_bolla = long(em_num_bolla.text)

if isnull(ls_cod_documento) or len(ls_cod_documento) < 1 then
	g_mb.messagebox("APICE","Indicare un codice documento valido!",stopsign!)
	return
end if
if isnull(ls_numeratore) or len(ls_numeratore) < 1 then
	g_mb.messagebox("APICE","Indicare un numeratore di documento valido!",stopsign!)
	return
end if
if isnull(ll_anno_bolla) or ll_anno_bolla < 1 then
	g_mb.messagebox("APICE","Indicare un anno bolla valido!",stopsign!)
	return
end if
if isnull(ll_num_bolla) or ll_num_bolla < 1 then
	g_mb.messagebox("APICE","Indicare un numero bolla valido!",stopsign!)
	return
end if

select anno_registrazione,
       num_registrazione
into   :ll_anno_registrazione,
       :ll_num_registrazione
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_documento = :ls_cod_documento and
		 numeratore_documento = :ls_numeratore and
		 anno_documento = : ll_anno_bolla and
		 num_documento = :ll_num_bolla;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Attenzione! La bolla richiesta non esiste; verificare !!!")
	return
end if
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Attenzione! si è verificato il seguente errore nella ricerca della testata della bolla~r~n"+sqlca.sqlerrtext)
	return
end if

ll_conta_fatture = 0
select count(*)
into   :ll_conta_fatture
from   det_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione_bol_ven = :ll_anno_registrazione and
		 num_registrazione_bol_ven = :ll_num_registrazione;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Attenzione! si è verificato il seguente errore nella ricerca delle fatture collegate alla bolla selezionata~r~n"+sqlca.sqlerrtext)
	return
end if
if ll_conta_fatture > 0 then
	g_mb.messagebox("APICE","Ci sono ancora alcune fatture collegate a questa bolla; elaborazione interrotta !!", StopSign!)
	return
end if

declare cu_det_bol_ven cursor for  
  select prog_riga_bol_ven,
  			anno_registrazione_mov_mag,   
         num_registrazione_mov_mag  
    from det_bol_ven  
   where cod_azienda = :s_cs_xx.cod_azienda  and  
         anno_registrazione = :ll_anno_registrazione  and  
         num_registrazione = :ll_num_registrazione  
order by prog_riga_bol_ven   ;

open cu_det_bol_ven;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Attenzione! si è verificato il seguente errore nell'OPEN del cursore cu_det_bol_ven~r~n"+sqlca.sqlerrtext)
	return
end if

do while  true
	fetch cu_det_bol_ven into :ll_prog_riga_bol_ven, :ll_anno_registrazione_mov_mag, :ll_num_registrazione_mov_mag;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Attenzione! si è verificato il seguente errore in fetch del cursore cu_det_bol_ven~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	update det_bol_ven
	set   anno_registrazione_mov_mag = null,
	      num_registrazione_mov_mag = null
   where cod_azienda = :s_cs_xx.cod_azienda  and  
         anno_registrazione = :ll_anno_registrazione  and  
         num_registrazione = :ll_num_registrazione   and
			prog_riga_bol_ven = :ll_prog_riga_bol_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Attenzione! si è verificato il seguente errore in cancellazione del num movimento dalla riga "+string(ll_prog_riga_bol_ven)+"  della bolla~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if not isnull(ll_anno_registrazione_mov_mag) and ll_anno_registrazione_mov_mag > 0 then
		
		luo_mag = create uo_magazzino
		if luo_mag.uof_elimina_movimenti(ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, true) <> 0 then
			g_mb.messagebox("APICE","Si è verificato un errore in cancellazione movimento di magazzino " + string(ll_anno_registrazione_mov_mag) + "-" + string(ll_num_registrazione_mov_mag), Information!)
			rollback;
			destroy luo_mag
			return
		end if
		destroy luo_mag
		
	end if
loop

// tolgo varie FK con la bolla che possono essere in giro

update avan_produzione_com
set   anno_reg_bol_ven = null,
      num_reg_bol_ven = null,
	   prog_riga_bol_ven = null
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_reg_bol_ven = :ll_anno_registrazione  and  
		num_reg_bol_ven = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione riferimenti bolla da avan_produzione_com~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if
		
update det_anag_comm_anticipi
set   anno_reg_bol_ven = null,
      num_reg_bol_ven = null,
	   prog_riga_bol_ven = null
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_reg_bol_ven = :ll_anno_registrazione  and  
		num_reg_bol_ven = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione riferimenti bolla da det_anag_comm_anticipi~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

update det_fat_ven
set   anno_registrazione_bol_ven = null,
      num_registrazione_bol_ven = null,
	   prog_riga_bol_ven = null
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione_bol_ven = :ll_anno_registrazione  and  
		num_registrazione_bol_ven = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione riferimenti bolla da det_fat_ven~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if
		
update prod_bolle_out_com
set   anno_registrazione = null,
      num_registrazione = null,
	   prog_riga_bol_ven = null
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione riferimenti bolla da prod_bolle_aut_com~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if
		

// cancello dati referenziati

delete from det_bol_ven_corrispondenze
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

delete from det_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

delete from iva_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

delete from tes_bol_ven_note
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

delete from tes_bol_ven_corrispondenze
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

delete from tes_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda  and  
		anno_registrazione = :ll_anno_registrazione  and  
		num_registrazione = :ll_num_registrazione  ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Si è verificato il seguente errore in cancellazione corrispondenze della bolla~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

COMMIT;
g_mb.messagebox("APICE","Cancellazione della bolla e dei movimenti di magazzino collegati eseguito con successo.")
return
end event

type st_4 from statictext within w_cancella_bolla_confermata
integer x = 1143
integer y = 780
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Numero Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_cancella_bolla_confermata
integer x = 297
integer y = 780
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Anno Bolla:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_cancella_bolla_confermata
integer x = 1189
integer y = 680
integer width = 357
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Numeratore:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_cancella_bolla_confermata
integer x = 183
integer y = 680
integer width = 517
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Codice Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_bolla from editmask within w_cancella_bolla_confermata
integer x = 1577
integer y = 780
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type em_anno_bolla from editmask within w_cancella_bolla_confermata
integer x = 731
integer y = 780
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
end type

type em_numeratore from editmask within w_cancella_bolla_confermata
integer x = 1577
integer y = 680
integer width = 137
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "a"
end type

type em_cod_documento from editmask within w_cancella_bolla_confermata
integer x = 731
integer y = 680
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type gb_1 from groupbox within w_cancella_bolla_confermata
integer x = 23
integer y = 560
integer width = 2309
integer height = 360
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
end type

