﻿$PBExportHeader$w_cancella_distinte_taglio_calcolate.srw
forward
global type w_cancella_distinte_taglio_calcolate from window
end type
type em_numero from editmask within w_cancella_distinte_taglio_calcolate
end type
type em_anno from editmask within w_cancella_distinte_taglio_calcolate
end type
type st_2 from statictext within w_cancella_distinte_taglio_calcolate
end type
type st_1 from statictext within w_cancella_distinte_taglio_calcolate
end type
type cb_1 from commandbutton within w_cancella_distinte_taglio_calcolate
end type
end forward

global type w_cancella_distinte_taglio_calcolate from window
integer width = 1554
integer height = 504
boolean titlebar = true
string title = "Cancellazione Distinte di Taglio"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
em_numero em_numero
em_anno em_anno
st_2 st_2
st_1 st_1
cb_1 cb_1
end type
global w_cancella_distinte_taglio_calcolate w_cancella_distinte_taglio_calcolate

on w_cancella_distinte_taglio_calcolate.create
this.em_numero=create em_numero
this.em_anno=create em_anno
this.st_2=create st_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.em_numero,&
this.em_anno,&
this.st_2,&
this.st_1,&
this.cb_1}
end on

on w_cancella_distinte_taglio_calcolate.destroy
destroy(this.em_numero)
destroy(this.em_anno)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

event open;em_anno.text = string( f_anno_esercizio() )
em_numero.text = "0"
end event

type em_numero from editmask within w_cancella_distinte_taglio_calcolate
integer x = 1102
integer y = 12
integer width = 329
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

type em_anno from editmask within w_cancella_distinte_taglio_calcolate
integer x = 398
integer y = 12
integer width = 183
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
end type

type st_2 from statictext within w_cancella_distinte_taglio_calcolate
integer x = 631
integer y = 20
integer width = 443
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Numero Ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_cancella_distinte_taglio_calcolate
integer x = 18
integer y = 16
integer width = 361
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Anno Ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_cancella_distinte_taglio_calcolate
integer x = 576
integer y = 196
integer width = 366
integer height = 89
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;long ll_anno_ordine, ll_num_ordine

if g_mb.messagebox("APICE","Procedo con la cancellazione delle distinte di taglio calcolate ?",Question!, YesNo!,2) = 2 then return

ll_anno_ordine = long(em_anno.text)
ll_num_ordine  = long(em_numero.text)

if ll_anno_ordine = 0 or isnull(ll_anno_ordine) then
	g_mb.messagebox("APICE", "Anno ordine obbligatorio !!!")
	return
end if

if ll_num_ordine = 0 or isnull(ll_num_ordine) then
	g_mb.messagebox("APICE", "Numero ordine obbligatorio !!!")
	return
end if


delete from distinte_taglio_calcolate
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_ordine and
		num_registrazione = :ll_num_ordine;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione" + sqlca.sqlerrtext)
	rollback;
	return
end if

commit;

g_mb.messagebox("APICE","Cancellazione Eseguita con Successo !!!~r~n     PREMERE INVIO")



end event

