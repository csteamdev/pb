﻿$PBExportHeader$w_cancella_mov_mag.srw
forward
global type w_cancella_mov_mag from w_cs_xx_principale
end type
type cbx_1 from checkbox within w_cancella_mov_mag
end type
type cb_2 from commandbutton within w_cancella_mov_mag
end type
type st_2 from statictext within w_cancella_mov_mag
end type
type cb_1 from commandbutton within w_cancella_mov_mag
end type
type st_1 from statictext within w_cancella_mov_mag
end type
type em_file from editmask within w_cancella_mov_mag
end type
end forward

global type w_cancella_mov_mag from w_cs_xx_principale
integer width = 3045
integer height = 936
string title = "Cancella movimenti"
cbx_1 cbx_1
cb_2 cb_2
st_2 st_2
cb_1 cb_1
st_1 st_1
em_file em_file
end type
global w_cancella_mov_mag w_cancella_mov_mag

on w_cancella_mov_mag.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.cb_2=create cb_2
this.st_2=create st_2
this.cb_1=create cb_1
this.st_1=create st_1
this.em_file=create em_file
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.em_file
end on

on w_cancella_mov_mag.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.cb_2)
destroy(this.st_2)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.em_file)
end on

type cbx_1 from checkbox within w_cancella_mov_mag
integer x = 1714
integer y = 360
integer width = 923
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Commit ad ogni cancellazione."
end type

type cb_2 from commandbutton within w_cancella_mov_mag
integer x = 2880
integer y = 140
integer width = 101
integer height = 100
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "..."
end type

event clicked;string ls_dektop, ls_docpath, ls_docname

ls_dektop = guo_functions.uof_get_user_desktop_folder( )

GetFileOpenName("Select File", ls_docpath, ls_docname, "Excel","Text,*.txt,", ls_dektop)

em_file.text = ls_docpath

end event

type st_2 from statictext within w_cancella_mov_mag
integer x = 960
integer y = 560
integer width = 1051
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_cancella_mov_mag
integer x = 1257
integer y = 340
integer width = 411
integer height = 112
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cancellazione"
end type

event clicked;string ls_str
long ll_file, ll_ret, ll_pos, ll_anno_movimento, ll_num_movimento, ll_cont

ll_file = fileopen(em_file.text, LineMode!, Read! )
if ll_file <= 0 then return

ll_cont = 0
do while true
	
		ll_ret = fileread(ll_file, ls_str)
		ll_cont ++
		
		if ll_ret <= 0 then exit
		
		ll_pos = pos(ls_str, "~t")
		ls_str = mid(ls_str, ll_pos + 1)
		
		ll_pos = pos(ls_str, "~t")
		ll_anno_movimento = long(mid(ls_str, 1, ll_pos - 1))
		ls_str = mid(ls_str, ll_pos + 1)
		
		ll_pos = pos(ls_str, "~t")
		ll_num_movimento = long(mid(ls_str, 1, ll_pos - 1))
		
		st_2.text = string(ll_cont) + " -->>" + string(ll_anno_movimento) + "-" + string(ll_num_movimento)
		Yield()
		
		delete mov_magazzino
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_movimento and
				num_registrazione = :ll_num_movimento;
				
		if sqlca.sqlcode <> 0 then
			g_mb.error("Movimento " + string(ll_anno_movimento) + "-" + string(ll_num_movimento), sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		if cbx_1.checked then 
			commit;
		end if
				
loop
commit;
fileclose(ll_file)

end event

type st_1 from statictext within w_cancella_mov_mag
integer x = 46
integer y = 40
integer width = 571
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "FileName"
boolean focusrectangle = false
end type

type em_file from editmask within w_cancella_mov_mag
integer x = 46
integer y = 140
integer width = 2834
integer height = 100
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

