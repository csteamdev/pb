﻿$PBExportHeader$w_carica_magazzino.srw
$PBExportComments$Procedira Personalizzata di carico magazzino a q.ta definita.
forward
global type w_carica_magazzino from window
end type
type cbx_zero from checkbox within w_carica_magazzino
end type
type em_cod_prodotto_fine from editmask within w_carica_magazzino
end type
type st_7 from statictext within w_carica_magazzino
end type
type em_cod_prodotto_inizio from editmask within w_carica_magazzino
end type
type st_5 from statictext within w_carica_magazzino
end type
type st_6 from statictext within w_carica_magazzino
end type
type st_record from statictext within w_carica_magazzino
end type
type st_4 from statictext within w_carica_magazzino
end type
type cb_1 from commandbutton within w_carica_magazzino
end type
type em_cod_tipo_movimento from editmask within w_carica_magazzino
end type
type st_3 from statictext within w_carica_magazzino
end type
type st_1 from statictext within w_carica_magazzino
end type
type em_quan_movimento from editmask within w_carica_magazzino
end type
end forward

global type w_carica_magazzino from window
integer x = 1074
integer y = 484
integer width = 2331
integer height = 1304
boolean titlebar = true
string title = "UTILITA~' DI CARICO MAGAZZINO"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cbx_zero cbx_zero
em_cod_prodotto_fine em_cod_prodotto_fine
st_7 st_7
em_cod_prodotto_inizio em_cod_prodotto_inizio
st_5 st_5
st_6 st_6
st_record st_record
st_4 st_4
cb_1 cb_1
em_cod_tipo_movimento em_cod_tipo_movimento
st_3 st_3
st_1 st_1
em_quan_movimento em_quan_movimento
end type
global w_carica_magazzino w_carica_magazzino

forward prototypes
public function integer wf_scrivi_log (string fs_messaggio)
end prototypes

public function integer wf_scrivi_log (string fs_messaggio);integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio

ls_nome_file = "\movimenti.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

li_file = fileopen(ls_volume + ls_nome_file, linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if

ls_messaggio = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + fs_messaggio
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

on w_carica_magazzino.create
this.cbx_zero=create cbx_zero
this.em_cod_prodotto_fine=create em_cod_prodotto_fine
this.st_7=create st_7
this.em_cod_prodotto_inizio=create em_cod_prodotto_inizio
this.st_5=create st_5
this.st_6=create st_6
this.st_record=create st_record
this.st_4=create st_4
this.cb_1=create cb_1
this.em_cod_tipo_movimento=create em_cod_tipo_movimento
this.st_3=create st_3
this.st_1=create st_1
this.em_quan_movimento=create em_quan_movimento
this.Control[]={this.cbx_zero,&
this.em_cod_prodotto_fine,&
this.st_7,&
this.em_cod_prodotto_inizio,&
this.st_5,&
this.st_6,&
this.st_record,&
this.st_4,&
this.cb_1,&
this.em_cod_tipo_movimento,&
this.st_3,&
this.st_1,&
this.em_quan_movimento}
end on

on w_carica_magazzino.destroy
destroy(this.cbx_zero)
destroy(this.em_cod_prodotto_fine)
destroy(this.st_7)
destroy(this.em_cod_prodotto_inizio)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_record)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.em_cod_tipo_movimento)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.em_quan_movimento)
end on

type cbx_zero from checkbox within w_carica_magazzino
integer x = 238
integer y = 48
integer width = 1696
integer height = 164
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Controlla se il codice prodotto ha 0 nella seconda posizione"
end type

type em_cod_prodotto_fine from editmask within w_carica_magazzino
integer x = 1312
integer y = 568
integer width = 658
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_7 from statictext within w_carica_magazzino
integer x = 123
integer y = 576
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "A CODICE PRODOTTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_cod_prodotto_inizio from editmask within w_carica_magazzino
integer x = 1312
integer y = 468
integer width = 658
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_5 from statictext within w_carica_magazzino
integer x = 123
integer y = 476
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "DA CODICE PRODOTTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_carica_magazzino
integer x = 18
integer y = 1024
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "record corrente:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_record from statictext within w_carica_magazzino
integer x = 14
integer y = 1100
integer width = 2267
integer height = 88
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_carica_magazzino
integer x = 41
integer y = 904
integer width = 1943
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "I MOVIMENTI SARANNO MEMORIZZATI NEL FILE C:\MOVIMENTI.LOG"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_carica_magazzino
integer x = 507
integer y = 720
integer width = 1115
integer height = 108
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui Movimenti"
end type

event clicked;string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], &
       ls_cod_cliente[], ls_referenza, ls_cod_tipo_movimento, ls_cod_prodotto, ls_des_prodotto, ls_sql, ls_prodotto_inizio, ls_prodotto_fine
long   ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
		 ll_anno_reg_dest_stock, ll_num_reg_dest_stock
double ll_quantita, ll_valore
datetime ldt_data_stock[], ldt_data_documento, ldt_data_registrazione

uo_magazzino luo_mag

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

ls_cod_tipo_movimento = em_cod_tipo_movimento.text
ll_quantita = double(em_quan_movimento.text)
ll_valore   = 0
ll_num_documento = 0
ldt_data_documento = datetime(today(), 00:00:00)
ldt_data_registrazione = ldt_data_documento
ls_referenza = "movimento iniziale"
ls_prodotto_inizio = em_cod_prodotto_inizio.text
ls_prodotto_fine = em_cod_prodotto_fine.text

if ll_quantita = 0 then
	if g_mb.messagebox("APICE","Attenzione movimento con quantità ZERO: Proseguo?",Question!,YesNo!,2) = 2 then return
end if

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])

declare cu_prodotti dynamic cursor for sqlsa;
ls_sql = "select cod_prodotto, des_prodotto from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "'"
if ls_prodotto_inizio <> "" and not isnull(ls_prodotto_inizio) then ls_sql = ls_sql + " and cod_prodotto >= '" + ls_prodotto_inizio + "'"
if ls_prodotto_fine   <> "" and not isnull(ls_prodotto_fine)   then ls_sql = ls_sql + " and cod_prodotto <= '" + ls_prodotto_fine + "'"

prepare sqlsa from :ls_sql;
open dynamic cu_prodotti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("UTILITA' carico magazzino","Errore in apertura cursore cu_prodotti")
end if

do while TRUE
   fetch cu_prodotti into :ls_cod_prodotto, :ls_des_prodotto;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	yield()	
	if cbx_zero.checked then
		if mid(ls_cod_prodotto, 2, 1) = "0" then 
			st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " SALTATO MOVIMENTO"
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " SALTATO MOVIMENTO")
			continue
		end if
	end if
	
	st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto

	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV"
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV") 
		continue;
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV"
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV") 
		continue;
	end if
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
								em_cod_tipo_movimento.text, &
								"S", &
								ls_cod_prodotto, &
								ll_quantita, &
								ll_valore, &
								ll_num_documento, &
								ldt_data_documento, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[] ) =  0 then
								
//		commit;
		
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " movimentato con successo !!" + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) 
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " movimentato con successo !! " + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) )
		
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock) = -1 then
			goto fine
		end if
		
	else
		
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore."
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore.")
		goto fine		
	end if	
	
	destroy luo_mag
		
loop

close cu_prodotti;

commit;

st_record.text = "FINITO CREAZIONE MOVIMENTI COMMIT"
wf_scrivi_log("FINITO CREAZIONE MOVIMENTI COMMIT")

return			


FINE:
close cu_prodotti;
ROLLBACK;
g_mb.messagebox("APICE","Elaborazione interrota per un errore: verificare il file di log")
return
end event

type em_cod_tipo_movimento from editmask within w_carica_magazzino
integer x = 1312
integer y = 368
integer width = 658
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_3 from statictext within w_carica_magazzino
integer x = 123
integer y = 376
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "CODICE MOVIMENTO DA USARE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_carica_magazzino
integer x = 123
integer y = 276
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "QUANTITA~' DA MOVIMENTARE IN MAGAZZINO"
boolean focusrectangle = false
end type

type em_quan_movimento from editmask within w_carica_magazzino
integer x = 1312
integer y = 268
integer width = 658
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,###,###,###"
boolean spin = true
string displaydata = "~b"
double increment = 1
string minmax = "0~~99999999"
end type

