﻿$PBExportHeader$w_carica_prodotti.srw
forward
global type w_carica_prodotti from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_carica_prodotti
end type
type cb_2 from commandbutton within w_carica_prodotti
end type
type cb_1 from commandbutton within w_carica_prodotti
end type
type dw_1 from datawindow within w_carica_prodotti
end type
end forward

global type w_carica_prodotti from w_cs_xx_principale
integer width = 3438
integer height = 2320
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
end type
global w_carica_prodotti w_carica_prodotti

event open;call super::open;dw_1.settransobject( sqlca )
end event

on w_carica_prodotti.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_1
end on

on w_carica_prodotti.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

type cb_3 from commandbutton within w_carica_prodotti
integer x = 2949
integer y = 2080
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "RETRIEVE"
end type

event clicked;dw_1.retrieve(s_cs_xx.cod_azienda)
end event

type cb_2 from commandbutton within w_carica_prodotti
integer x = 457
integer y = 2080
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "SALVA"
end type

event clicked;dw_1.update( )

commit;
end event

type cb_1 from commandbutton within w_carica_prodotti
integer x = 23
integer y = 2080
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "NUOVO"
end type

event clicked;dw_1.insertrow(0)

end event

type dw_1 from datawindow within w_carica_prodotti
integer x = 23
integer y = 20
integer width = 3337
integer height = 2040
integer taborder = 10
string title = "none"
string dataobject = "d_carica_prodotti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

