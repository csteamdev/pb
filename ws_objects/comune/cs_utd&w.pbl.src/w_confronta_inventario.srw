﻿$PBExportHeader$w_confronta_inventario.srw
forward
global type w_confronta_inventario from w_std_principale
end type
type em_data from editmask within w_confronta_inventario
end type
type st_1 from statictext within w_confronta_inventario
end type
type cb_1 from commandbutton within w_confronta_inventario
end type
type em_deposito from editmask within w_confronta_inventario
end type
type em_file from editmask within w_confronta_inventario
end type
end forward

global type w_confronta_inventario from w_std_principale
em_data em_data
st_1 st_1
cb_1 cb_1
em_deposito em_deposito
em_file em_file
end type
global w_confronta_inventario w_confronta_inventario

on w_confronta_inventario.create
int iCurrent
call super::create
this.em_data=create em_data
this.st_1=create st_1
this.cb_1=create cb_1
this.em_deposito=create em_deposito
this.em_file=create em_file
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_data
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.em_deposito
this.Control[iCurrent+5]=this.em_file
end on

on w_confronta_inventario.destroy
call super::destroy
destroy(this.em_data)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.em_deposito)
destroy(this.em_file)
end on

type em_data from editmask within w_confronta_inventario
integer x = 1070
integer y = 312
integer width = 571
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type st_1 from statictext within w_confronta_inventario
integer x = 46
integer y = 1420
integer width = 3383
integer height = 104
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_confronta_inventario
integer x = 1486
integer y = 960
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;string ls_cod_prodotto,ls_chiave[],ls_vuoto[], ls_where, ls_error,ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
long ll_row, li_return, ll_i, ll_prog_stock
decimal ld_quant_val[], ld_vuoto[], ld_giacenza_stock[], ld_costo_medio[], ld_quan_costo_medio_stock[], ld_giacenza_tot
datetime ldt_data,ldt_data_stock
uo_excel luo_excel
uo_magazzino luo_magazzino

luo_excel = create uo_excel
luo_excel.uof_open( em_file.text, true)

ll_row=1		// inizia dalla seconda riga saltando intestazione
do while true
	ll_row ++
	ls_cod_prodotto = luo_excel.uof_read_string( ll_row, 1)
	st_1.text = ls_cod_prodotto + string(ll_row)
	Yield()
	if len(ls_cod_prodotto) < 1 or isnull(ls_cod_prodotto) then exit
	
	// se va tutto bene procedo con inventario
	luo_magazzino = create uo_magazzino
	ls_chiave = ls_vuoto
	ld_quant_val = ld_vuoto
	ld_giacenza_stock = ld_vuoto
	ldt_data = datetime( date(em_data.text), 00:00:00 )
	ld_giacenza_tot = 0
	
	luo_magazzino = CREATE uo_magazzino
	
	li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data, ls_where, ld_quant_val[], ls_error, "S", ls_chiave[], ld_giacenza_stock[], ld_costo_medio[], ld_quan_costo_medio_stock[])
	
	for ll_i = 1 to upperbound(ld_giacenza_stock)
		
		if len(ls_chiave[ll_i]) < 1 then continue
		f_parse_stock(ls_chiave[ll_i], ref ls_cod_deposito, ref ls_cod_ubicazione, ref ls_cod_lotto, ref ldt_data_stock, ref ll_prog_stock)
		
		if em_deposito.text = ls_cod_deposito then
			ld_giacenza_tot += ld_giacenza_stock[ll_i]
		end if
	next
	
	luo_excel.uof_set(ll_row, 5, ld_giacenza_tot)

	destroy luo_magazzino
	
loop

destroy luo_excel

g_mb.messagebox("Elaborazione terminata", string(ll_row))



end event

type em_deposito from editmask within w_confronta_inventario
integer x = 59
integer y = 308
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "deposito"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_file from editmask within w_confronta_inventario
integer x = 50
integer y = 100
integer width = 2638
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "Percorso del file"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

