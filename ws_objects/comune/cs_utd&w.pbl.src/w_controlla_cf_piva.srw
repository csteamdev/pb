﻿$PBExportHeader$w_controlla_cf_piva.srw
forward
global type w_controlla_cf_piva from window
end type
type st_cli_errori from statictext within w_controlla_cf_piva
end type
type st_3 from statictext within w_controlla_cf_piva
end type
type st_tot_cli from statictext within w_controlla_cf_piva
end type
type st_for_errori from statictext within w_controlla_cf_piva
end type
type st_tot_for from statictext within w_controlla_cf_piva
end type
type st_1 from statictext within w_controlla_cf_piva
end type
type cb_clienti from commandbutton within w_controlla_cf_piva
end type
type hpb_1 from hprogressbar within w_controlla_cf_piva
end type
type cb_fornitori from commandbutton within w_controlla_cf_piva
end type
type mle_cli from multilineedit within w_controlla_cf_piva
end type
type mle_for from multilineedit within w_controlla_cf_piva
end type
end forward

global type w_controlla_cf_piva from window
integer width = 4087
integer height = 2928
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_cli_errori st_cli_errori
st_3 st_3
st_tot_cli st_tot_cli
st_for_errori st_for_errori
st_tot_for st_tot_for
st_1 st_1
cb_clienti cb_clienti
hpb_1 hpb_1
cb_fornitori cb_fornitori
mle_cli mle_cli
mle_for mle_for
end type
global w_controlla_cf_piva w_controlla_cf_piva

forward prototypes
public subroutine wf_log (string as_text, string as_type)
end prototypes

public subroutine wf_log (string as_text, string as_type);
if as_type = "F" then
	//log fornitori
	mle_for.setredraw(false)
	//mle_for.text += string(today(),"dd/mm/yyyy hh:mm:ss") + " " + as_text + " ~r~n"
	mle_for.text += as_text + " ~r~n"

	mle_for.scroll(mle_for.linecount())
	mle_for.SetRedraw(TRUE)
	
else
	//log clienti
	mle_cli.setredraw(false)
	//mle_cli.text += string(today(),"dd/mm/yyyy hh:mm:ss") + " " + as_text + " ~r~n"
	mle_cli.text += as_text + " ~r~n"

	mle_cli.scroll(mle_cli.linecount())
	mle_cli.SetRedraw(TRUE)
	
end if




return
end subroutine

on w_controlla_cf_piva.create
this.st_cli_errori=create st_cli_errori
this.st_3=create st_3
this.st_tot_cli=create st_tot_cli
this.st_for_errori=create st_for_errori
this.st_tot_for=create st_tot_for
this.st_1=create st_1
this.cb_clienti=create cb_clienti
this.hpb_1=create hpb_1
this.cb_fornitori=create cb_fornitori
this.mle_cli=create mle_cli
this.mle_for=create mle_for
this.Control[]={this.st_cli_errori,&
this.st_3,&
this.st_tot_cli,&
this.st_for_errori,&
this.st_tot_for,&
this.st_1,&
this.cb_clienti,&
this.hpb_1,&
this.cb_fornitori,&
this.mle_cli,&
this.mle_for}
end on

on w_controlla_cf_piva.destroy
destroy(this.st_cli_errori)
destroy(this.st_3)
destroy(this.st_tot_cli)
destroy(this.st_for_errori)
destroy(this.st_tot_for)
destroy(this.st_1)
destroy(this.cb_clienti)
destroy(this.hpb_1)
destroy(this.cb_fornitori)
destroy(this.mle_cli)
destroy(this.mle_for)
end on

type st_cli_errori from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 1928
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_3 from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 1848
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
string text = "con errori:"
boolean focusrectangle = false
end type

type st_tot_cli from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 1628
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tot:"
boolean focusrectangle = false
end type

type st_for_errori from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 560
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_tot_for from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 260
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tot:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_controlla_cf_piva
integer x = 3643
integer y = 480
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 12632256
string text = "con errori:"
boolean focusrectangle = false
end type

type cb_clienti from commandbutton within w_controlla_cf_piva
integer x = 3648
integer y = 1496
integer width = 366
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Clienti"
end type

event clicked;string			ls_sql, ls_errore, ls_codice, ls_rag_soc, ls_piva, ls_cf, ls_flag_tipo

long			ll_index, ll_tot, ll_errori

datastore	lds_data


mle_cli.text = ""

hpb_1.position = 0
ll_errori = 0
st_cli_errori.text = ""

// flag_tipo_cliente = "C" or flag_tipo_cliente = "E" or f_cod_fiscale(gettext())

ls_sql = "select cod_cliente, rag_soc_1, cod_fiscale, partita_iva, flag_tipo_cliente from anag_clienti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+" '"+&
			"order by cod_cliente"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	g_mb.error("Errore preparazione query: "+ls_errore)
	return
end if

hpb_1.maxposition = ll_tot

st_tot_cli.text = "Tot: "+string(ll_tot)

for ll_index=1 to ll_tot
	ls_errore = ""
	
	ls_codice = lds_data.getitemstring(ll_index, 1)
	ls_rag_soc = lds_data.getitemstring(ll_index, 2)
	
	ls_flag_tipo = lds_data.getitemstring(ll_index, 5)
	
	if isnull(ls_rag_soc) then ls_rag_soc=""
	
	if ls_flag_tipo<>"E" and ls_flag_tipo<>"C" then
		ls_cf = lds_data.getitemstring(ll_index, 3)
		if ls_cf<>"" and not isnull(ls_cf) then
			if not f_cod_fiscale(ls_cf) then
				//log
				ll_errori += 1
				ls_errore = "Cliente "+ls_codice+ " "+ls_rag_soc + " (tipo "+ls_flag_tipo+") : CF '"+ls_cf+"' non valido!"
				wf_log(ls_errore, "C")
			end if
		end if
	end if
	
	ls_piva = lds_data.getitemstring(ll_index, 4)
	if ls_piva<>"" and not isnull(ls_cf) then
		if not f_partita_iva(ls_piva) then
			//log
			ll_errori += 1
			ls_errore = "Cliente "+ls_codice+ " "+ls_rag_soc + " : P.Iva '"+ls_piva+"' non valida!"
			wf_log(ls_errore, "C")
		end if
	end if
	
	hpb_1.stepit()
	
next

st_cli_errori.text = string(ll_errori)


end event

type hpb_1 from hprogressbar within w_controlla_cf_piva
integer x = 55
integer y = 32
integer width = 3945
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_fornitori from commandbutton within w_controlla_cf_piva
integer x = 3648
integer y = 144
integer width = 366
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fornitori"
end type

event clicked;string			ls_sql, ls_errore, ls_codice, ls_rag_soc, ls_piva, ls_cf, ls_flag_tipo

long			ll_index, ll_tot, ll_errori

datastore	lds_data


hpb_1.position = 0
mle_for.text = ""
ll_errori = 0
st_for_errori.text = ""

ls_sql = "select cod_fornitore, rag_soc_1, cod_fiscale, partita_iva, flag_tipo_fornitore from anag_fornitori "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+" '"+&
			"order by cod_fornitore"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	g_mb.error("Errore preparazione query: "+ls_errore)
	return
end if

hpb_1.maxposition = ll_tot

st_tot_for.text = "Tot: "+string(ll_tot)

for ll_index=1 to ll_tot
	ls_errore = ""
	
	ls_codice = lds_data.getitemstring(ll_index, 1)
	ls_rag_soc = lds_data.getitemstring(ll_index, 2)
	
	ls_flag_tipo = lds_data.getitemstring(ll_index, 5)
	
	if isnull(ls_rag_soc) then ls_rag_soc=""
	
	if ls_flag_tipo<>"E" and ls_flag_tipo<>"C" then
		ls_cf = lds_data.getitemstring(ll_index, 3)
		if ls_cf<>"" and not isnull(ls_cf) then
			if not f_cod_fiscale(ls_cf) then
				//log
				ll_errori += 1
				ls_errore = "Fornitore "+ls_codice+ " "+ls_rag_soc + " (tipo "+ls_flag_tipo+") : CF '"+ls_cf+"' non valido!"
				wf_log(ls_errore, "F")
			end if
		end if
	end if
	
	ls_piva = lds_data.getitemstring(ll_index, 4)
	if ls_piva<>"" and not isnull(ls_cf) then
		if not f_partita_iva(ls_piva) then
			//log
			ll_errori += 1
			ls_errore = "Fornitore "+ls_codice+ " "+ls_rag_soc + " : P.Iva '"+ls_piva+"' non valida!"
			wf_log(ls_errore, "F")
		end if
	end if
next

st_for_errori.text = string(ll_errori)

return
end event

type mle_cli from multilineedit within w_controlla_cf_piva
integer x = 37
integer y = 1496
integer width = 3589
integer height = 1280
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type mle_for from multilineedit within w_controlla_cf_piva
integer x = 37
integer y = 144
integer width = 3589
integer height = 1280
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

