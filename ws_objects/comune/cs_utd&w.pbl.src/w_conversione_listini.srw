﻿$PBExportHeader$w_conversione_listini.srw
$PBExportComments$Finestra Conversione Listini Prodotti
forward
global type w_conversione_listini from w_cs_xx_principale
end type
type dw_con_listino_prodotti from uo_cs_xx_dw within w_conversione_listini
end type
type cb_2 from commandbutton within w_conversione_listini
end type
end forward

global type w_conversione_listini from w_cs_xx_principale
int Width=3580
int Height=1205
dw_con_listino_prodotti dw_con_listino_prodotti
cb_2 cb_2
end type
global w_conversione_listini w_conversione_listini

event pc_setwindow;call super::pc_setwindow;dw_con_listino_prodotti.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_con_listino_prodotti
end event

on w_conversione_listini.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_listino_prodotti=create dw_con_listino_prodotti
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_listino_prodotti
this.Control[iCurrent+2]=cb_2
end on

on w_conversione_listini.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_listino_prodotti)
destroy(this.cb_2)
end on

type dw_con_listino_prodotti from uo_cs_xx_dw within w_conversione_listini
int X=23
int Y=21
int Width=3498
int Height=961
int TabOrder=20
string DataObject="dw_con_listino_prodotti"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;dw_con_listino_prodotti.retrieve(s_cs_xx.cod_azienda)
end event

type cb_2 from commandbutton within w_conversione_listini
int X=3155
int Y=1001
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Conversione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto, ls_des_listino
long ll_progressivo, ll_cont, ll_i, ll_num_righe
double ld_quan_1, ld_quan_2, ld_quan_3, ld_quan_4, ld_quan_5, ld_prezzo_1, ld_prezzo_2, ld_prezzo_3, &
       ld_prezzo_4, ld_prezzo_5, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5
datetime ldt_data_inizio_val
  
ll_num_righe = dw_con_listino_prodotti.rowcount()

for ll_i = 1 to ll_num_righe  
	ls_cod_tipo_listino_prodotto = dw_con_listino_prodotti.getitemstring(ll_i,"cod_tipo_listino_prodotto")
	ls_cod_valuta   = dw_con_listino_prodotti.getitemstring(ll_i,"cod_valuta")
	ls_cod_prodotto = dw_con_listino_prodotti.getitemstring(ll_i,"cod_prodotto")
	ls_des_listino  = dw_con_listino_prodotti.getitemstring(ll_i,"des_listino_prodotto")
	ldt_data_inizio_val  = dw_con_listino_prodotti.getitemdatetime(ll_i,"data_inizio_val")
	ld_quan_1   = dw_con_listino_prodotti.getitemnumber(ll_i,"quantita_1")
	ld_quan_2   = dw_con_listino_prodotti.getitemnumber(ll_i,"quantita_2")
	ld_quan_3   = dw_con_listino_prodotti.getitemnumber(ll_i,"quantita_3")
	ld_quan_4   = dw_con_listino_prodotti.getitemnumber(ll_i,"quantita_4")
	ld_quan_5   = dw_con_listino_prodotti.getitemnumber(ll_i,"quantita_5")
	ld_prezzo_1 = dw_con_listino_prodotti.getitemnumber(ll_i,"prezzo_1")
	ld_prezzo_2 = dw_con_listino_prodotti.getitemnumber(ll_i,"prezzo_2")
	ld_prezzo_3 = dw_con_listino_prodotti.getitemnumber(ll_i,"prezzo_3")
	ld_prezzo_4 = dw_con_listino_prodotti.getitemnumber(ll_i,"prezzo_4")
	ld_prezzo_5 = dw_con_listino_prodotti.getitemnumber(ll_i,"prezzo_5")
	ld_sconto_1 = dw_con_listino_prodotti.getitemnumber(ll_i,"sconto_1")
	ld_sconto_2 = dw_con_listino_prodotti.getitemnumber(ll_i,"sconto_2")
	ld_sconto_3 = dw_con_listino_prodotti.getitemnumber(ll_i,"sconto_3")
	ld_sconto_4 = dw_con_listino_prodotti.getitemnumber(ll_i,"sconto_4")
	ld_sconto_5 = dw_con_listino_prodotti.getitemnumber(ll_i,"sconto_5")
	
	ll_progressivo = 1
	ll_cont = 0
	select max(progressivo)
	into   :ll_cont
	from   listini_vendite
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
			 cod_valuta = :ls_cod_valuta and
			 data_inizio_val = :ldt_data_inizio_val;
	if ll_cont = 0 or isnull(ll_cont) then
		ll_progressivo = 1
	else
		ll_progressivo = ll_cont + 1
	end if
	  
	INSERT INTO listini_vendite  
			( cod_azienda,   
			  cod_tipo_listino_prodotto,   
			  cod_valuta,   
			  data_inizio_val,   
			  progressivo,   
			  cod_cat_mer,   
			  cod_prodotto,   
			  cod_categoria,   
			  cod_cliente,   
			  des_listino_vendite,   
			  minimo_fatt_altezza,   
			  minimo_fatt_larghezza,   
			  minimo_fatt_profondita,   
			  minimo_fatt_superficie,   
			  minimo_fatt_volume,   
			  flag_sconto_a_parte,   
			  flag_tipo_scaglioni,   
			  scaglione_1,   
			  scaglione_2,   
			  scaglione_3,   
			  scaglione_4,   
			  scaglione_5,   
			  flag_sconto_mag_prezzo_1,   
			  flag_sconto_mag_prezzo_2,   
			  flag_sconto_mag_prezzo_3,   
			  flag_sconto_mag_prezzo_4,   
			  flag_sconto_mag_prezzo_5,   
			  variazione_1,   
			  variazione_2,   
			  variazione_3,   
			  variazione_4,   
			  variazione_5,   
			  flag_origine_prezzo_1,   
			  flag_origine_prezzo_2,   
			  flag_origine_prezzo_3,   
			  flag_origine_prezzo_4,   
			  flag_origine_prezzo_5 )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_tipo_listino_prodotto,   
			  :ls_cod_valuta,   
			  :ldt_data_inizio_val,   
			  :ll_progressivo,   
			  null,   
			  :ls_cod_prodotto,   
			  null,   
			  null,   
			  :ls_des_listino,   
			  0,   
			  0,   
			  0,   
			  0,   
			  0,   
			  'S',   
			  'Q',   
			  :ld_quan_1,   
			  :ld_quan_2,   
			  :ld_quan_3,   
			  :ld_quan_4,   
			  :ld_quan_5,   
			  'P',   
			  'P',   
			  'P',   
			  'P',   
			  'P',   
			  :ld_prezzo_1,   
			  :ld_prezzo_2,   
			  :ld_prezzo_3,   
			  :ld_prezzo_4,   
			  :ld_prezzo_5,   
			  'N',   
			  'N',   
			  'N',   
			  'N',   
			  'N' )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Errore in insert prezzi", sqlca.sqlerrtext)
		return
	end if
	
	if ld_sconto_1 > 0 or ld_sconto_2 > 0 or ld_sconto_3 > 0 or ld_sconto_4 > 0 or ld_sconto_5 > 0 then
			ll_cont = 0
			select max(progressivo)
			into   :ll_cont
			from   listini_vendite
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
					 cod_valuta = :ls_cod_valuta and
					 data_inizio_val = :ldt_data_inizio_val;
			if ll_cont = 0 or isnull(ll_cont) then
				ll_progressivo = 1
			else
				ll_progressivo ++
			end if
			  
			INSERT INTO listini_vendite  
					( cod_azienda,   
					  cod_tipo_listino_prodotto,   
					  cod_valuta,   
					  data_inizio_val,   
					  progressivo,   
					  cod_cat_mer,   
					  cod_prodotto,   
					  cod_categoria,   
					  cod_cliente,   
					  des_listino_vendite,   
					  minimo_fatt_altezza,   
					  minimo_fatt_larghezza,   
					  minimo_fatt_profondita,   
					  minimo_fatt_superficie,   
					  minimo_fatt_volume,   
					  flag_sconto_a_parte,   
					  flag_tipo_scaglioni,   
					  scaglione_1,   
					  scaglione_2,   
					  scaglione_3,   
					  scaglione_4,   
					  scaglione_5,   
					  flag_sconto_mag_prezzo_1,   
					  flag_sconto_mag_prezzo_2,   
					  flag_sconto_mag_prezzo_3,   
					  flag_sconto_mag_prezzo_4,   
					  flag_sconto_mag_prezzo_5,   
					  variazione_1,   
					  variazione_2,   
					  variazione_3,   
					  variazione_4,   
					  variazione_5,   
					  flag_origine_prezzo_1,   
					  flag_origine_prezzo_2,   
					  flag_origine_prezzo_3,   
					  flag_origine_prezzo_4,   
					  flag_origine_prezzo_5 )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_tipo_listino_prodotto,   
					  :ls_cod_valuta,   
					  :ldt_data_inizio_val,   
					  :ll_progressivo,   
					  null,   
					  :ls_cod_prodotto,   
					  null,   
					  null,   
					  :ls_des_listino,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  'S',   
					  'Q',   
					  :ld_quan_1,   
					  :ld_quan_2,   
					  :ld_quan_3,   
					  :ld_quan_4,   
					  :ld_quan_5,   
					  'S',   
					  'S',   
					  'S',   
					  'S',   
					  'S',   
					  :ld_sconto_1,   
					  :ld_sconto_2,   
					  :ld_sconto_3,   
					  :ld_sconto_4,   
					  :ld_sconto_5,   
					  'N',   
					  'N',   
					  'N',   
					  'N',   
					  'N' )  ;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Errore in insert sconti", sqlca.sqlerrtext)
				return
			end if
	end if		
next
end event

