﻿$PBExportHeader$w_conversione_listini_clienti.srw
$PBExportComments$Finestra Conversione Listini Clienti
forward
global type w_conversione_listini_clienti from w_cs_xx_principale
end type
type dw_con_listino_clienti from uo_cs_xx_dw within w_conversione_listini_clienti
end type
type cb_2 from commandbutton within w_conversione_listini_clienti
end type
type mle_1 from multilineedit within w_conversione_listini_clienti
end type
end forward

global type w_conversione_listini_clienti from w_cs_xx_principale
int Width=3580
int Height=1205
dw_con_listino_clienti dw_con_listino_clienti
cb_2 cb_2
mle_1 mle_1
end type
global w_conversione_listini_clienti w_conversione_listini_clienti

event pc_setwindow;call super::pc_setwindow;dw_con_listino_clienti.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_con_listino_clienti
end event

on w_conversione_listini_clienti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_listino_clienti=create dw_con_listino_clienti
this.cb_2=create cb_2
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_listino_clienti
this.Control[iCurrent+2]=cb_2
this.Control[iCurrent+3]=mle_1
end on

on w_conversione_listini_clienti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_listino_clienti)
destroy(this.cb_2)
destroy(this.mle_1)
end on

type dw_con_listino_clienti from uo_cs_xx_dw within w_conversione_listini_clienti
int X=23
int Y=21
int Width=3498
int Height=961
int TabOrder=20
string DataObject="dw_con_listino_clienti"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;dw_con_listino_clienti.retrieve(s_cs_xx.cod_azienda)
end event

type cb_2 from commandbutton within w_conversione_listini_clienti
int X=3155
int Y=1001
int Width=366
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Conversione"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto, ls_des_listino, ls_cod_cliente
long ll_progressivo, ll_cont, ll_i, ll_num_righe
double ld_quan_1, ld_quan_2, ld_quan_3, ld_quan_4, ld_quan_5, ld_prezzo_1, ld_prezzo_2, ld_prezzo_3, &
       ld_prezzo_4, ld_prezzo_5, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5
datetime ldt_data_inizio_val
  
ll_num_righe = dw_con_listino_clienti.rowcount()

for ll_i = 1 to ll_num_righe  
	ls_cod_cliente = dw_con_listino_clienti.getitemstring(ll_i,"cod_cliente")
	ls_cod_valuta   = dw_con_listino_clienti.getitemstring(ll_i,"cod_valuta")
	ls_cod_prodotto = dw_con_listino_clienti.getitemstring(ll_i,"cod_prodotto")
	ls_des_listino  = dw_con_listino_clienti.getitemstring(ll_i,"des_listino_cliente")
	ldt_data_inizio_val  = dw_con_listino_clienti.getitemdatetime(ll_i,"data_inizio_val")
	ld_quan_1   = dw_con_listino_clienti.getitemnumber(ll_i,"quantita_1")
	ld_quan_2   = dw_con_listino_clienti.getitemnumber(ll_i,"quantita_2")
	ld_quan_3   = dw_con_listino_clienti.getitemnumber(ll_i,"quantita_3")
	ld_quan_4   = dw_con_listino_clienti.getitemnumber(ll_i,"quantita_4")
	ld_quan_5   = dw_con_listino_clienti.getitemnumber(ll_i,"quantita_5")
	ld_prezzo_1 = dw_con_listino_clienti.getitemnumber(ll_i,"prezzo_1")
	ld_prezzo_2 = dw_con_listino_clienti.getitemnumber(ll_i,"prezzo_2")
	ld_prezzo_3 = dw_con_listino_clienti.getitemnumber(ll_i,"prezzo_3")
	ld_prezzo_4 = dw_con_listino_clienti.getitemnumber(ll_i,"prezzo_4")
	ld_prezzo_5 = dw_con_listino_clienti.getitemnumber(ll_i,"prezzo_5")
	ld_sconto_1 = dw_con_listino_clienti.getitemnumber(ll_i,"sconto_1")
	ld_sconto_2 = dw_con_listino_clienti.getitemnumber(ll_i,"sconto_2")
	ld_sconto_3 = dw_con_listino_clienti.getitemnumber(ll_i,"sconto_3")
	ld_sconto_4 = dw_con_listino_clienti.getitemnumber(ll_i,"sconto_4")
	ld_sconto_5 = dw_con_listino_clienti.getitemnumber(ll_i,"sconto_5")
	
	setnull(ls_cod_tipo_listino_prodotto)
	select cod_tipo_listino_prodotto
	into   :ls_cod_tipo_listino_prodotto
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
	if isnull(ls_cod_tipo_listino_prodotto) then
		mle_1.text = mle_1.text + "Cliente " + ls_cod_cliente + " manca indicazione del tipo listino in anagrafica cliente~r~n"
	else	
		
		ll_progressivo = 1
		ll_cont = 0
		select max(progressivo)
		into   :ll_cont
		from   listini_vendite
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val;
		if ll_cont = 0 or isnull(ll_cont) then
			ll_progressivo = 1
		else
			ll_progressivo = ll_cont + 1
		end if
		  
		INSERT INTO listini_vendite  
				( cod_azienda,   
				  cod_tipo_listino_prodotto,   
				  cod_valuta,   
				  data_inizio_val,   
				  progressivo,   
				  cod_cat_mer,   
				  cod_prodotto,   
				  cod_categoria,   
				  cod_cliente,   
				  des_listino_vendite,   
				  minimo_fatt_altezza,   
				  minimo_fatt_larghezza,   
				  minimo_fatt_profondita,   
				  minimo_fatt_superficie,   
				  minimo_fatt_volume,   
				  flag_sconto_a_parte,   
				  flag_tipo_scaglioni,   
				  scaglione_1,   
				  scaglione_2,   
				  scaglione_3,   
				  scaglione_4,   
				  scaglione_5,   
				  flag_sconto_mag_prezzo_1,   
				  flag_sconto_mag_prezzo_2,   
				  flag_sconto_mag_prezzo_3,   
				  flag_sconto_mag_prezzo_4,   
				  flag_sconto_mag_prezzo_5,   
				  variazione_1,   
				  variazione_2,   
				  variazione_3,   
				  variazione_4,   
				  variazione_5,   
				  flag_origine_prezzo_1,   
				  flag_origine_prezzo_2,   
				  flag_origine_prezzo_3,   
				  flag_origine_prezzo_4,   
				  flag_origine_prezzo_5 )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_tipo_listino_prodotto,   
				  :ls_cod_valuta,   
				  :ldt_data_inizio_val,   
				  :ll_progressivo,   
				  null,   
				  :ls_cod_prodotto,   
				  null,   
				  :ls_cod_cliente,   
				  :ls_des_listino,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  'S',   
				  'Q',   
				  :ld_quan_1,   
				  :ld_quan_2,   
				  :ld_quan_3,   
				  :ld_quan_4,   
				  :ld_quan_5,   
				  'P',   
				  'P',   
				  'P',   
				  'P',   
				  'P',   
				  :ld_prezzo_1,   
				  :ld_prezzo_2,   
				  :ld_prezzo_3,   
				  :ld_prezzo_4,   
				  :ld_prezzo_5,   
				  'N',   
				  'N',   
				  'N',   
				  'N',   
				  'N' )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Errore in insert prezzi", sqlca.sqlerrtext)
			return
		end if
		
		if ld_sconto_1 > 0 or ld_sconto_2 > 0 or ld_sconto_3 > 0 or ld_sconto_4 > 0 or ld_sconto_5 > 0 then
				ll_cont = 0
				select max(progressivo)
				into   :ll_cont
				from   listini_vendite
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto and
						 cod_valuta = :ls_cod_valuta and
						 data_inizio_val = :ldt_data_inizio_val;
				if ll_cont = 0 or isnull(ll_cont) then
					ll_progressivo = 1
				else
					ll_progressivo ++
				end if
				  
				INSERT INTO listini_vendite  
						( cod_azienda,   
						  cod_tipo_listino_prodotto,   
						  cod_valuta,   
						  data_inizio_val,   
						  progressivo,   
						  cod_cat_mer,   
						  cod_prodotto,   
						  cod_categoria,   
						  cod_cliente,   
						  des_listino_vendite,   
						  minimo_fatt_altezza,   
						  minimo_fatt_larghezza,   
						  minimo_fatt_profondita,   
						  minimo_fatt_superficie,   
						  minimo_fatt_volume,   
						  flag_sconto_a_parte,   
						  flag_tipo_scaglioni,   
						  scaglione_1,   
						  scaglione_2,   
						  scaglione_3,   
						  scaglione_4,   
						  scaglione_5,   
						  flag_sconto_mag_prezzo_1,   
						  flag_sconto_mag_prezzo_2,   
						  flag_sconto_mag_prezzo_3,   
						  flag_sconto_mag_prezzo_4,   
						  flag_sconto_mag_prezzo_5,   
						  variazione_1,   
						  variazione_2,   
						  variazione_3,   
						  variazione_4,   
						  variazione_5,   
						  flag_origine_prezzo_1,   
						  flag_origine_prezzo_2,   
						  flag_origine_prezzo_3,   
						  flag_origine_prezzo_4,   
						  flag_origine_prezzo_5 )  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ls_cod_tipo_listino_prodotto,   
						  :ls_cod_valuta,   
						  :ldt_data_inizio_val,   
						  :ll_progressivo,   
						  null,   
						  :ls_cod_prodotto,   
						  null,   
						  :ls_cod_cliente,   
						  :ls_des_listino,   
						  0,   
						  0,   
						  0,   
						  0,   
						  0,   
						  'S',   
						  'Q',   
						  :ld_quan_1,   
						  :ld_quan_2,   
						  :ld_quan_3,   
						  :ld_quan_4,   
						  :ld_quan_5,   
						  'S',   
						  'S',   
						  'S',   
						  'S',   
						  'S',   
						  :ld_sconto_1,   
						  :ld_sconto_2,   
						  :ld_sconto_3,   
						  :ld_sconto_4,   
						  :ld_sconto_5,   
						  'N',   
						  'N',   
						  'N',   
						  'N',   
						  'N' )  ;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Errore in insert sconti", sqlca.sqlerrtext)
					return
				end if
		end if		
	end if
next
end event

type mle_1 from multilineedit within w_conversione_listini_clienti
int X=23
int Y=21
int Width=3498
int Height=961
int TabOrder=1
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

