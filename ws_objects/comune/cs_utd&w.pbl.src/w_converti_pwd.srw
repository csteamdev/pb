﻿$PBExportHeader$w_converti_pwd.srw
forward
global type w_converti_pwd from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_converti_pwd
end type
end forward

global type w_converti_pwd from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 1518
integer height = 244
string title = "Conversione PWD utenti"
boolean minbox = false
boolean maxbox = false
cb_1 cb_1
end type
global w_converti_pwd w_converti_pwd

on w_converti_pwd.create
int iCurrent
call super::create
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
end on

on w_converti_pwd.destroy
call super::destroy
destroy(this.cb_1)
end on

type cb_1 from commandbutton within w_converti_pwd
integer x = 23
integer y = 20
integer width = 1440
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "AVVIA CONVERSIONE"
end type

event clicked;n_cst_crypto luo_crypto

string	ls_cod_utente, ls_pwd_db, ls_pwd_decrypt, ls_pwd_encrypt


luo_crypto = create n_cst_crypto

declare utenti cursor for

select
	cod_utente,
	password
from
	utenti;

open utenti;

if sqlca.sqlcode <> 0 then
	messagebox("Conversione PWD utenti","Errore in apertura cursore utenti: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do
	
	fetch
		utenti
	into
		:ls_cod_utente,
		:ls_pwd_db;
	
	if sqlca.sqlcode < 0 then
		messagebox("Conversione PWD utenti","Errore in fetch cursore utenti: " + sqlca.sqlerrtext,stopsign!)
		close utenti;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		continue
	end if
	
	if isnull(ls_pwd_db) then
		continue
	end if
	
	ls_pwd_decrypt = luo_crypto.old_decrypt(ls_pwd_db)
	
	if luo_crypto.encryptdata(ls_pwd_decrypt,ls_pwd_encrypt) < 0 then
		messagebox("Conversione PWD utenti","Errore in encrypt password utente " + ls_cod_utente + " con nuovo algoritmo",stopsign!)
		continue
	end if
	
	update
		utenti
	set
		password = :ls_pwd_encrypt
	where
		cod_utente = :ls_cod_utente;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Conversione PWD utenti","Errore in aggiornamento password utente " + ls_cod_utente + ": " + sqlca.sqlerrtext,stopsign!)
		close utenti;
		rollback;
		return -1
	end if
	
loop while sqlca.sqlcode <> 100

close utenti;

commit;
end event

