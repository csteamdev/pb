﻿$PBExportHeader$w_correggi_mag.srw
$PBExportComments$Window per la correzzione della quantita assegnata del magazzino
forward
global type w_correggi_mag from w_cs_xx_principale
end type
type dw_quan_assegnate from uo_cs_xx_dw within w_correggi_mag
end type
type cb_verifica from commandbutton within w_correggi_mag
end type
type cb_correggi from commandbutton within w_correggi_mag
end type
type st_1 from statictext within w_correggi_mag
end type
type st_bianco from statictext within w_correggi_mag
end type
type st_rosso from statictext within w_correggi_mag
end type
type st_2 from statictext within w_correggi_mag
end type
type sle_cod_prodotto from singlelineedit within w_correggi_mag
end type
end forward

global type w_correggi_mag from w_cs_xx_principale
int Width=2958
int Height=1637
boolean TitleBar=true
string Title="Allineamento Magazzino"
dw_quan_assegnate dw_quan_assegnate
cb_verifica cb_verifica
cb_correggi cb_correggi
st_1 st_1
st_bianco st_bianco
st_rosso st_rosso
st_2 st_2
sle_cod_prodotto sle_cod_prodotto
end type
global w_correggi_mag w_correggi_mag

on w_correggi_mag.create
int iCurrent
call w_cs_xx_principale::create
this.dw_quan_assegnate=create dw_quan_assegnate
this.cb_verifica=create cb_verifica
this.cb_correggi=create cb_correggi
this.st_1=create st_1
this.st_bianco=create st_bianco
this.st_rosso=create st_rosso
this.st_2=create st_2
this.sle_cod_prodotto=create sle_cod_prodotto
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_quan_assegnate
this.Control[iCurrent+2]=cb_verifica
this.Control[iCurrent+3]=cb_correggi
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=st_bianco
this.Control[iCurrent+6]=st_rosso
this.Control[iCurrent+7]=st_2
this.Control[iCurrent+8]=sle_cod_prodotto
end on

on w_correggi_mag.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_quan_assegnate)
destroy(this.cb_verifica)
destroy(this.cb_correggi)
destroy(this.st_1)
destroy(this.st_bianco)
destroy(this.st_rosso)
destroy(this.st_2)
destroy(this.sle_cod_prodotto)
end on

event pc_setwindow;call super::pc_setwindow;dw_quan_assegnate.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_multiselect + &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodeblack)
end event

type dw_quan_assegnate from uo_cs_xx_dw within w_correggi_mag
int X=23
int Y=101
int Width=2881
int Height=1241
int TabOrder=20
string DataObject="d_quan_assegnate"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

type cb_verifica from commandbutton within w_correggi_mag
int X=2195
int Y=1441
int Width=366
int Height=81
int TabOrder=40
boolean BringToTop=true
string Text="&Verifica Mag."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_inizio_codice
double ldd_quan_assegnata_p,ldd_quan_assegnata_stock,ldd_quan_assegnata_stock_mp, &
		 ldd_quan_assegnata_stock_evord, ldd_somma_quan_mp_evord,ldd_lunghezza_totale,ldd_lunghezza, &
		 ldd_step
long   ll_num_righe,ll_num_prodotti,ll_conteggio

setpointer(hourglass!)

dw_quan_assegnate.reset()

ldd_lunghezza_totale=st_bianco.width
st_rosso.width=0
ls_inizio_codice = sle_cod_prodotto.text + "%"

select count(*)
into   :ll_num_prodotti
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto like :ls_inizio_codice;

ldd_step = ldd_lunghezza_totale/ll_num_prodotti

declare righe_p cursor for
select  cod_prodotto,
		  quan_assegnata
from    anag_prodotti
where   cod_azienda=:s_cs_xx.cod_azienda
and     cod_prodotto like :ls_inizio_codice;

open righe_p;

do while 1=1
	fetch righe_p
	into  :ls_cod_prodotto,
			:ldd_quan_assegnata_p;

	if sqlca.sqlcode=100 then exit

	ll_conteggio++
	st_rosso.width=ldd_step*ll_conteggio

	select sum(quan_assegnata)
	into   :ldd_quan_assegnata_stock
	from   stock
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	select sum(quan_assegnata)
	into   :ldd_quan_assegnata_stock_mp
	from   mat_prime_commessa_stock
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	select sum(quan_in_evasione)
	into   :ldd_quan_assegnata_stock_evord
	from   evas_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	if isnull(ldd_quan_assegnata_p) then ldd_quan_assegnata_p=0
	if isnull(ldd_quan_assegnata_stock) then ldd_quan_assegnata_stock=0
	if isnull(ldd_quan_assegnata_stock_mp) then ldd_quan_assegnata_stock_mp=0
	if isnull(ldd_quan_assegnata_stock_evord) then ldd_quan_assegnata_stock_evord=0

	ldd_somma_quan_mp_evord = ldd_quan_assegnata_stock_evord + ldd_quan_assegnata_stock_mp

	if (ldd_quan_assegnata_stock <> ldd_quan_assegnata_p) or &
		(ldd_quan_assegnata_p <> ldd_somma_quan_mp_evord) or & 
		(ldd_quan_assegnata_stock <> ldd_somma_quan_mp_evord) then
		
		ll_num_righe++

		dw_quan_assegnate.insertrow(ll_num_righe)
		dw_quan_assegnate.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto)
		dw_quan_assegnate.setitem(ll_num_righe,"quan_assegnata_p",ldd_quan_assegnata_p)
		dw_quan_assegnate.setitem(ll_num_righe,"quan_assegnata_stock",ldd_quan_assegnata_stock)
		dw_quan_assegnate.setitem(ll_num_righe,"quan_assegnata_stock_mp",ldd_quan_assegnata_stock_mp)
		dw_quan_assegnate.setitem(ll_num_righe,"quan_assegnata_stock_evord",ldd_quan_assegnata_stock_evord)
	end if

loop

close righe_p;

setpointer(arrow!)
		
dw_quan_assegnate.resetupdate()			
dw_quan_assegnate.TriggerEvent("pcd_search")
end event

type cb_correggi from commandbutton within w_correggi_mag
int X=2583
int Y=1441
int Width=321
int Height=81
int TabOrder=10
boolean BringToTop=true
string Text="&Correggi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_cod_deposito,ls_cod_ubicazione,ls_cod_lotto
double ldd_quan_assegnata_p,ldd_quan_assegnata_stock,ldd_quan_assegnata_stock_mp, &
		 ldd_quan_assegnata_stock_evord, ldd_somma_quan_mp_evord,ldd_quan_assegnata_stock_mp_1, & 
		 ldd_quan_assegnata_stock_evord_1,ldd_somma_quan_mp_evord_1
long   ll_selected[],ll_i,ll_num_righe,ll_prog_stock
datetime ldt_data_stock

setpointer(hourglass!)

dw_quan_assegnate.get_selected_rows(ll_selected[])

ll_num_righe = upperbound(ll_selected)

for ll_i = 1 to ll_num_righe
	ldd_somma_quan_mp_evord=0
	ls_cod_prodotto = dw_quan_assegnate.getitemstring(ll_selected[ll_i],"cod_prodotto")
	ldd_quan_assegnata_stock_mp = dw_quan_assegnate.getitemnumber(ll_selected[ll_i],"quan_assegnata_stock_mp")
	ldd_quan_assegnata_stock_evord = dw_quan_assegnate.getitemnumber(ll_selected[ll_i],"quan_assegnata_stock_evord")
	ldd_somma_quan_mp_evord = ldd_quan_assegnata_stock_mp + ldd_quan_assegnata_stock_evord

	update anag_prodotti
	set    quan_assegnata=:ldd_somma_quan_mp_evord
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	declare righe_s cursor for
	select  cod_deposito,
			  cod_ubicazione,
			  cod_lotto,
			  data_stock,
			  prog_stock
	from    stock
	where   cod_azienda=:s_cs_xx.cod_azienda
	and     cod_prodotto=:ls_cod_prodotto;

	open righe_s;

	do while 1=1
		fetch righe_s
		into  :ls_cod_deposito,
				:ls_cod_ubicazione,
				:ls_cod_lotto,
				:ldt_data_stock,
				:ll_prog_stock;

		if sqlca.sqlcode=100 then exit

		select sum(quan_assegnata)
		into   :ldd_quan_assegnata_stock_mp_1
		from   mat_prime_commessa_stock
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_deposito=:ls_cod_deposito
		and    cod_ubicazione=:ls_cod_ubicazione
		and    cod_lotto=:ls_cod_lotto
		and    data_stock=:ldt_data_stock
		and    prog_stock=:ll_prog_stock;

		if isnull(ldd_quan_assegnata_stock_mp_1) then ldd_quan_assegnata_stock_mp_1=0

		select sum(quan_in_evasione)
		into   :ldd_quan_assegnata_stock_evord_1
		from   evas_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_deposito=:ls_cod_deposito
		and    cod_ubicazione=:ls_cod_ubicazione
		and    cod_lotto=:ls_cod_lotto
		and    data_stock=:ldt_data_stock
		and    prog_stock=:ll_prog_stock;

		if isnull(ldd_quan_assegnata_stock_evord_1) then ldd_quan_assegnata_stock_evord_1=0

		ldd_somma_quan_mp_evord_1 = ldd_quan_assegnata_stock_evord_1 + ldd_quan_assegnata_stock_mp_1

		update stock
		set 	 quan_assegnata=:ldd_somma_quan_mp_evord_1
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_deposito=:ls_cod_deposito
		and    cod_ubicazione=:ls_cod_ubicazione
		and    cod_lotto=:ls_cod_lotto
		and    data_stock=:ldt_data_stock
		and    prog_stock=:ll_prog_stock;

		ldd_quan_assegnata_stock_mp_1=0
		ldd_quan_assegnata_stock_evord_1=0
		ldd_somma_quan_mp_evord_1=0
	loop

	close righe_s;	

next
end event

type st_1 from statictext within w_correggi_mag
int X=23
int Y=21
int Width=1441
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco prodotti con le quantità assegnate non corrette:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_bianco from statictext within w_correggi_mag
int X=23
int Y=1441
int Width=2126
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_rosso from statictext within w_correggi_mag
int X=23
int Y=1441
int Width=2126
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=255
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_correggi_mag
int X=23
int Y=1361
int Width=366
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Cod.Prodotto:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_cod_prodotto from singlelineedit within w_correggi_mag
int X=389
int Y=1361
int Width=526
int Height=61
int TabOrder=30
boolean BringToTop=true
boolean AutoHScroll=false
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

