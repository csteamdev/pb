﻿$PBExportHeader$w_correggi_spese_trasp.srw
forward
global type w_correggi_spese_trasp from w_cs_xx_principale
end type
type st_log from statictext within w_correggi_spese_trasp
end type
type cb_correggi from commandbutton within w_correggi_spese_trasp
end type
type dp_from from datepicker within w_correggi_spese_trasp
end type
type cb_analizza from commandbutton within w_correggi_spese_trasp
end type
type dw_list from datawindow within w_correggi_spese_trasp
end type
end forward

global type w_correggi_spese_trasp from w_cs_xx_principale
integer width = 5307
integer height = 2296
string title = "Correggi Spese Trasporto"
st_log st_log
cb_correggi cb_correggi
dp_from dp_from
cb_analizza cb_analizza
dw_list dw_list
end type
global w_correggi_spese_trasp w_correggi_spese_trasp

on w_correggi_spese_trasp.create
int iCurrent
call super::create
this.st_log=create st_log
this.cb_correggi=create cb_correggi
this.dp_from=create dp_from
this.cb_analizza=create cb_analizza
this.dw_list=create dw_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.cb_correggi
this.Control[iCurrent+3]=this.dp_from
this.Control[iCurrent+4]=this.cb_analizza
this.Control[iCurrent+5]=this.dw_list
end on

on w_correggi_spese_trasp.destroy
call super::destroy
destroy(this.st_log)
destroy(this.cb_correggi)
destroy(this.dp_from)
destroy(this.cb_analizza)
destroy(this.dw_list)
end on

event pc_setwindow;call super::pc_setwindow;
dw_list.settransobject(sqlca)
end event

type st_log from statictext within w_correggi_spese_trasp
integer x = 960
integer y = 40
integer width = 1349
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_correggi from commandbutton within w_correggi_spese_trasp
integer x = 4827
integer y = 20
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Correggi"
end type

event clicked;string ls_errore
int li_ret
long ll_i, ll_rows, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_num_riga_appartenenza
uo_spese_trasporto luo_spese

luo_spese = create uo_spese_trasporto

ll_rows = dw_list.rowcount()

for ll_i = 1 to ll_rows
	
	ll_anno_registrazione = dw_list.getitemnumber(ll_i, "tes_ord_ven_anno_registrazione")
	ll_num_registrazione = dw_list.getitemnumber(ll_i, "det_ord_ven_num_registrazione")
	ll_prog_riga_ord_ven = dw_list.getitemnumber(ll_i, "det_ord_ven_prog_riga_ord_ven")
	ll_num_riga_appartenenza = dw_list.getitemnumber(ll_i, "det_ord_ven_num_riga_appartenenza")
	
	st_log.text = "Elaborazione riga " + string(ll_i) + "/" + string(ll_rows)
	
	if isnull(ll_num_riga_appartenenza) or  ll_num_riga_appartenenza = 0 then
		li_ret = luo_spese.uof_imposta_spese_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, true, ref ls_errore)
	
		if li_ret < 0 then
			g_mb.error("Attenzione", ls_errore)
			rollback;
			destroy luo_spese
			return
		else
			commit;
		end if
	end if
	
next

destroy luo_spese
g_mb.success("Operazione completata")
cb_analizza.postevent(clicked!)

end event

type dp_from from datepicker within w_correggi_spese_trasp
integer x = 457
integer y = 20
integer width = 457
integer height = 100
integer taborder = 20
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2014-03-03"), Time("17:18:15.000000"))
integer textsize = -9
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
integer calendarfontweight = 400
boolean todaysection = true
boolean todaycircle = true
boolean valueset = true
end type

type cb_analizza from commandbutton within w_correggi_spese_trasp
integer x = 23
integer y = 20
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Analizza"
end type

event clicked;long ll_rows

//ll_rows = dw_list.retrieve(s_cs_xx.cod_azienda, dp_from.value)
ll_rows = dw_list.retrieve(dp_from.value)

st_log.text = "Trovate " + string(ll_rows) + " righe"
end event

type dw_list from datawindow within w_correggi_spese_trasp
integer x = 23
integer y = 140
integer width = 5207
integer height = 2020
integer taborder = 10
string title = "none"
string dataobject = "d_correggi_spese_trasp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

