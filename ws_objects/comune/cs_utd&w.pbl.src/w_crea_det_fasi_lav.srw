﻿$PBExportHeader$w_crea_det_fasi_lav.srw
$PBExportComments$Finestra Visione Dettaglio Fasi Dopo Generazione - Import Produzione
forward
global type w_crea_det_fasi_lav from w_cs_xx_principale
end type
type dw_det_fasi_lavorazione_lista from uo_cs_xx_dw within w_crea_det_fasi_lav
end type
type cb_ricerca_prod from cb_prod_ricerca within w_crea_det_fasi_lav
end type
type dw_det_fasi_lavorazione_det from uo_dw_main within w_crea_det_fasi_lav
end type
end forward

global type w_crea_det_fasi_lav from w_cs_xx_principale
int Width=2734
int Height=1329
boolean TitleBar=true
string Title="Dettaglio Lavorazioni"
dw_det_fasi_lavorazione_lista dw_det_fasi_lavorazione_lista
cb_ricerca_prod cb_ricerca_prod
dw_det_fasi_lavorazione_det dw_det_fasi_lavorazione_det
end type
global w_crea_det_fasi_lav w_crea_det_fasi_lav

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_fasi_lavorazione_det,"cod_prodotto_materia_prima",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto","anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_det_fasi_lavorazione_lista.set_dw_key("cod_azienda")
dw_det_fasi_lavorazione_lista.set_dw_key("cod_prodotto")
dw_det_fasi_lavorazione_lista.set_dw_key("cod_reparto")
dw_det_fasi_lavorazione_lista.set_dw_key("cod_lavorazione")

dw_det_fasi_lavorazione_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_det_fasi_lavorazione_det.set_dw_options(sqlca,dw_det_fasi_lavorazione_lista,c_sharedata+c_scrollparent,c_default)

end on

on w_crea_det_fasi_lav.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_fasi_lavorazione_lista=create dw_det_fasi_lavorazione_lista
this.cb_ricerca_prod=create cb_ricerca_prod
this.dw_det_fasi_lavorazione_det=create dw_det_fasi_lavorazione_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_fasi_lavorazione_lista
this.Control[iCurrent+2]=cb_ricerca_prod
this.Control[iCurrent+3]=dw_det_fasi_lavorazione_det
end on

on w_crea_det_fasi_lav.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_fasi_lavorazione_lista)
destroy(this.cb_ricerca_prod)
destroy(this.dw_det_fasi_lavorazione_det)
end on

type dw_det_fasi_lavorazione_lista from uo_cs_xx_dw within w_crea_det_fasi_lav
int X=23
int Y=21
int Width=2652
int Height=501
int TabOrder=30
string DataObject="d_det_fasi_lavorazione_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;cb_ricerca_prod.enabled=false

end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_ricerca_prod.enabled=true

end on

on pcd_new;call uo_cs_xx_dw::pcd_new;long ll_num_registrazione

string ls_prodotto, ls_reparto, ls_lavorazione

ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

select max(det_fasi_lavorazione.prog_riga_fasi_lavorazione)
  into :ll_num_registrazione
  from det_fasi_lavorazione
  where (det_fasi_lavorazione.cod_azienda = :s_cs_xx.cod_azienda) and (det_fasi_lavorazione.cod_prodotto = :ls_prodotto) and (det_fasi_lavorazione.cod_reparto = :ls_reparto) and (det_fasi_lavorazione.cod_lavorazione = :ls_lavorazione)  ;

if (sqlca.SQLCode = 100) or (sqlca.SQLCode = -1) then
    dw_det_fasi_lavorazione_lista.SetItem (dw_det_fasi_lavorazione_lista.GetRow ( ),&
                                 "prog_riga_fasi_lavorazione", 1 )
else
   ll_num_registrazione = ll_num_registrazione + 1
   dw_det_fasi_lavorazione_lista.SetItem (dw_det_fasi_lavorazione_lista.GetRow ( ),&
                                 "prog_riga_fasi_lavorazione", ll_num_registrazione)
end if
cb_ricerca_prod.enabled=true

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore
string ls_prodotto, ls_reparto, ls_lavorazione


ls_prodotto    = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_reparto     = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_prodotto, ls_reparto, ls_lavorazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

string ls_prodotto, ls_reparto, ls_lavorazione

ls_prodotto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_prodotto")
ls_reparto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_reparto")
ls_lavorazione = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lavorazione")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto")) THEN
      SetItem(l_Idx, "cod_prodotto", ls_prodotto)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_reparto")) THEN
      SetItem(l_Idx, "cod_reparto", ls_reparto)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_lavorazione")) THEN
      SetItem(l_Idx, "cod_lavorazione", ls_lavorazione)
   END IF
NEXT

end on

type cb_ricerca_prod from cb_prod_ricerca within w_crea_det_fasi_lav
int X=2561
int Y=661
int TabOrder=20
end type

type dw_det_fasi_lavorazione_det from uo_dw_main within w_crea_det_fasi_lav
int X=23
int Y=541
int Width=2652
int Height=661
int TabOrder=10
string DataObject="d_det_fasi_lavorazione_det"
end type

