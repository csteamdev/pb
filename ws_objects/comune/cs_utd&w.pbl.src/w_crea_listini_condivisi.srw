﻿$PBExportHeader$w_crea_listini_condivisi.srw
forward
global type w_crea_listini_condivisi from window
end type
type cb_2 from commandbutton within w_crea_listini_condivisi
end type
type cb_1 from commandbutton within w_crea_listini_condivisi
end type
type dw_1 from datawindow within w_crea_listini_condivisi
end type
end forward

global type w_crea_listini_condivisi from window
integer width = 4306
integer height = 1728
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
end type
global w_crea_listini_condivisi w_crea_listini_condivisi

on w_crea_listini_condivisi.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_1}
end on

on w_crea_listini_condivisi.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

type cb_2 from commandbutton within w_crea_listini_condivisi
integer x = 1984
integer y = 1248
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "retrieve"
end type

event clicked;dw_1.settransobject(sqlca)

dw_1.retrieve()
end event

type cb_1 from commandbutton within w_crea_listini_condivisi
integer x = 3442
integer y = 1248
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;long ll_cont, ll_i,ll_progressivo
string ls_cod_valuta,ls_cod_tipo_listino_prodotto,ls_cod_cliente,ls_cod_categoria,ls_cod_prodotto,ls_cod_cat_mer
datetime ldt_data_inizio_val

ll_cont = dw_1.rowcount()

for ll_i = 1 to ll_cont
	
	ls_cod_tipo_listino_prodotto = dw_1.getitemstring(ll_i, "cod_tipo_listino_prodotto")
	ls_cod_valuta = dw_1.getitemstring(ll_i, "cod_valuta")
	ldt_data_inizio_val = dw_1.getitemdatetime(ll_i, "data_inizio_val")
	ll_progressivo = dw_1.getitemnumber(ll_i, "progressivo")
	
	ls_cod_cliente = dw_1.getitemstring(ll_i, "cod_cliente")
	ls_cod_categoria = dw_1.getitemstring(ll_i, "cod_categoria")
	ls_cod_prodotto = dw_1.getitemstring(ll_i, "cod_prodotto")
	ls_cod_cat_mer = dw_1.getitemstring(ll_i, "cod_cat_mer")
	
	
	if isnull(ls_cod_cliente) and isnull(ls_cod_categoria) and isnull(ls_cod_cat_mer) then
	// listino standard
		  INSERT INTO listini_ven_comune  
					( cod_azienda,   
					  cod_tipo_listino_prodotto,   
					  cod_valuta,   
					  data_inizio_val,   
					  progressivo,   
					  cod_cat_mer,   
					  cod_prodotto,   
					  cod_categoria,   
					  cod_cliente,   
					  des_listino_vendite,   
					  minimo_fatt_altezza,   
					  minimo_fatt_larghezza,   
					  minimo_fatt_profondita,   
					  minimo_fatt_superficie,   
					  minimo_fatt_volume,   
					  flag_sconto_a_parte,   
					  flag_tipo_scaglioni,   
					  scaglione_1,   
					  scaglione_2,   
					  scaglione_3,   
					  scaglione_4,   
					  scaglione_5,   
					  flag_sconto_mag_prezzo_1,   
					  flag_sconto_mag_prezzo_2,   
					  flag_sconto_mag_prezzo_3,   
					  flag_sconto_mag_prezzo_4,   
					  flag_sconto_mag_prezzo_5,   
					  variazione_1,   
					  variazione_2,   
					  variazione_3,   
					  variazione_4,   
					  variazione_5,   
					  flag_origine_prezzo_1,   
					  flag_origine_prezzo_2,   
					  flag_origine_prezzo_3,   
					  flag_origine_prezzo_4,   
					  flag_origine_prezzo_5 )  
		  SELECT cod_azienda,   
					cod_tipo_listino_prodotto,   
					cod_valuta,   
					data_inizio_val,   
					progressivo,   
					cod_cat_mer,   
					cod_prodotto,   
					cod_categoria,   
					cod_cliente,   
					des_listino_vendite,   
					minimo_fatt_altezza,   
					minimo_fatt_larghezza,   
					minimo_fatt_profondita,   
					minimo_fatt_superficie,   
					minimo_fatt_volume,   
					flag_sconto_a_parte,   
					flag_tipo_scaglioni,   
					scaglione_1,   
					scaglione_2,   
					scaglione_3,   
					scaglione_4,   
					scaglione_5,   
					flag_sconto_mag_prezzo_1,   
					flag_sconto_mag_prezzo_2,   
					flag_sconto_mag_prezzo_3,   
					flag_sconto_mag_prezzo_4,   
					flag_sconto_mag_prezzo_5,   
					variazione_1,   
					variazione_2,   
					variazione_3,   
					variazione_4,   
					variazione_5,   
					flag_origine_prezzo_1,   
					flag_origine_prezzo_2,   
					flag_origine_prezzo_3,   
					flag_origine_prezzo_4,   
					flag_origine_prezzo_5  
			 FROM listini_vendite 
			 WHERE cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
					 cod_valuta = :ls_cod_valuta and
					 data_inizio_val = :ldt_data_inizio_val  and
					 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite COMUNE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	  INSERT INTO listini_ven_dim_comune  
				( cod_azienda,   
				  cod_tipo_listino_prodotto,   
				  cod_valuta,   
				  data_inizio_val,   
				  progressivo,   
				  num_scaglione,   
				  limite_dimensione_1,   
				  limite_dimensione_2,   
				  flag_sconto_mag_prezzo,   
				  variazione )  
	  SELECT listini_vendite_dimensioni.cod_azienda,   
				listini_vendite_dimensioni.cod_tipo_listino_prodotto,   
				listini_vendite_dimensioni.cod_valuta,   
				listini_vendite_dimensioni.data_inizio_val,   
				listini_vendite_dimensioni.progressivo,   
				listini_vendite_dimensioni.num_scaglione,   
				listini_vendite_dimensioni.limite_dimensione_1,   
				listini_vendite_dimensioni.limite_dimensione_2,   
				listini_vendite_dimensioni.flag_sconto_mag_prezzo,   
				listini_vendite_dimensioni.variazione  
		 FROM listini_vendite_dimensioni 
		 WHERE cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val  and
				 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite DIM COMUNE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	  INSERT INTO listini_prod_comune  
				( cod_azienda,   
				  cod_tipo_listino_prodotto,   
				  cod_valuta,   
				  data_inizio_val,   
				  progressivo,   
				  cod_prodotto_listino,   
				  cod_versione,   
				  prog_listino_produzione,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_gruppo_variante,   
				  progr,   
				  cod_prodotto,   
				  des_listino_produzione,   
				  minimo_fatt_altezza,   
				  minimo_fatt_larghezza,   
				  minimo_fatt_profondita,   
				  minimo_fatt_superficie,   
				  minimo_fatt_volume,   
				  flag_sconto_a_parte,   
				  flag_tipo_scaglioni,   
				  scaglione_1,   
				  scaglione_2,   
				  scaglione_3,   
				  scaglione_4,   
				  scaglione_5,   
				  flag_sconto_mag_prezzo_1,   
				  flag_sconto_mag_prezzo_2,   
				  flag_sconto_mag_prezzo_3,   
				  flag_sconto_mag_prezzo_4,   
				  flag_sconto_mag_prezzo_5,   
				  variazione_1,   
				  variazione_2,   
				  variazione_3,   
				  variazione_4,   
				  variazione_5,   
				  flag_origine_prezzo_1,   
				  flag_origine_prezzo_2,   
				  flag_origine_prezzo_3,   
				  flag_origine_prezzo_4,   
				  flag_origine_prezzo_5,   
				  provvigione_1,   
				  provvigione_2 )  
	  SELECT listini_produzione.cod_azienda,   
				listini_produzione.cod_tipo_listino_prodotto,   
				listini_produzione.cod_valuta,   
				listini_produzione.data_inizio_val,   
				listini_produzione.progressivo,   
				listini_produzione.cod_prodotto_listino,   
				listini_produzione.cod_versione,   
				listini_produzione.prog_listino_produzione,   
				listini_produzione.cod_prodotto_padre,   
				listini_produzione.num_sequenza,   
				listini_produzione.cod_prodotto_figlio,   
				listini_produzione.cod_gruppo_variante,   
				listini_produzione.progr,   
				listini_produzione.cod_prodotto,   
				listini_produzione.des_listino_produzione,   
				listini_produzione.minimo_fatt_altezza,   
				listini_produzione.minimo_fatt_larghezza,   
				listini_produzione.minimo_fatt_profondita,   
				listini_produzione.minimo_fatt_superficie,   
				listini_produzione.minimo_fatt_volume,   
				listini_produzione.flag_sconto_a_parte,   
				listini_produzione.flag_tipo_scaglioni,   
				listini_produzione.scaglione_1,   
				listini_produzione.scaglione_2,   
				listini_produzione.scaglione_3,   
				listini_produzione.scaglione_4,   
				listini_produzione.scaglione_5,   
				listini_produzione.flag_sconto_mag_prezzo_1,   
				listini_produzione.flag_sconto_mag_prezzo_2,   
				listini_produzione.flag_sconto_mag_prezzo_3,   
				listini_produzione.flag_sconto_mag_prezzo_4,   
				listini_produzione.flag_sconto_mag_prezzo_5,   
				listini_produzione.variazione_1,   
				listini_produzione.variazione_2,   
				listini_produzione.variazione_3,   
				listini_produzione.variazione_4,   
				listini_produzione.variazione_5,   
				listini_produzione.flag_origine_prezzo_1,   
				listini_produzione.flag_origine_prezzo_2,   
				listini_produzione.flag_origine_prezzo_3,   
				listini_produzione.flag_origine_prezzo_4,   
				listini_produzione.flag_origine_prezzo_5,   
				listini_produzione.provvigione_1,   
				listini_produzione.provvigione_2  
		 FROM listini_produzione 
		 WHERE cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val  and
				 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite DIM COMUNE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	else
	// listino personalizzato
		  INSERT INTO listini_ven_locale  
					( cod_azienda,   
					  cod_tipo_listino_prodotto,   
					  cod_valuta,   
					  data_inizio_val,   
					  progressivo,   
					  cod_cat_mer,   
					  cod_prodotto,   
					  cod_categoria,   
					  cod_cliente,   
					  des_listino_vendite,   
					  minimo_fatt_altezza,   
					  minimo_fatt_larghezza,   
					  minimo_fatt_profondita,   
					  minimo_fatt_superficie,   
					  minimo_fatt_volume,   
					  flag_sconto_a_parte,   
					  flag_tipo_scaglioni,   
					  scaglione_1,   
					  scaglione_2,   
					  scaglione_3,   
					  scaglione_4,   
					  scaglione_5,   
					  flag_sconto_mag_prezzo_1,   
					  flag_sconto_mag_prezzo_2,   
					  flag_sconto_mag_prezzo_3,   
					  flag_sconto_mag_prezzo_4,   
					  flag_sconto_mag_prezzo_5,   
					  variazione_1,   
					  variazione_2,   
					  variazione_3,   
					  variazione_4,   
					  variazione_5,   
					  flag_origine_prezzo_1,   
					  flag_origine_prezzo_2,   
					  flag_origine_prezzo_3,   
					  flag_origine_prezzo_4,   
					  flag_origine_prezzo_5 )  
		  SELECT cod_azienda,   
					cod_tipo_listino_prodotto,   
					cod_valuta,   
					data_inizio_val,   
					progressivo,   
					cod_cat_mer,   
					cod_prodotto,   
					cod_categoria,   
					cod_cliente,   
					des_listino_vendite,   
					minimo_fatt_altezza,   
					minimo_fatt_larghezza,   
					minimo_fatt_profondita,   
					minimo_fatt_superficie,   
					minimo_fatt_volume,   
					flag_sconto_a_parte,   
					flag_tipo_scaglioni,   
					scaglione_1,   
					scaglione_2,   
					scaglione_3,   
					scaglione_4,   
					scaglione_5,   
					flag_sconto_mag_prezzo_1,   
					flag_sconto_mag_prezzo_2,   
					flag_sconto_mag_prezzo_3,   
					flag_sconto_mag_prezzo_4,   
					flag_sconto_mag_prezzo_5,   
					variazione_1,   
					variazione_2,   
					variazione_3,   
					variazione_4,   
					variazione_5,   
					flag_origine_prezzo_1,   
					flag_origine_prezzo_2,   
					flag_origine_prezzo_3,   
					flag_origine_prezzo_4,   
					flag_origine_prezzo_5  
			 FROM listini_vendite  
			 WHERE cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
					 cod_valuta = :ls_cod_valuta and
					 data_inizio_val = :ldt_data_inizio_val  and
					 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite LOCALE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	  INSERT INTO listini_ven_dim_locale  
				( cod_azienda,   
				  cod_tipo_listino_prodotto,   
				  cod_valuta,   
				  data_inizio_val,   
				  progressivo,   
				  num_scaglione,   
				  limite_dimensione_1,   
				  limite_dimensione_2,   
				  flag_sconto_mag_prezzo,   
				  variazione )  
	  SELECT listini_vendite_dimensioni.cod_azienda,   
				listini_vendite_dimensioni.cod_tipo_listino_prodotto,   
				listini_vendite_dimensioni.cod_valuta,   
				listini_vendite_dimensioni.data_inizio_val,   
				listini_vendite_dimensioni.progressivo,   
				listini_vendite_dimensioni.num_scaglione,   
				listini_vendite_dimensioni.limite_dimensione_1,   
				listini_vendite_dimensioni.limite_dimensione_2,   
				listini_vendite_dimensioni.flag_sconto_mag_prezzo,   
				listini_vendite_dimensioni.variazione  
		 FROM listini_vendite_dimensioni 
		 WHERE cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val  and
				 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite DIM LOCALE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	  INSERT INTO listini_prod_locale  
				( cod_azienda,   
				  cod_tipo_listino_prodotto,   
				  cod_valuta,   
				  data_inizio_val,   
				  progressivo,   
				  cod_prodotto_listino,   
				  cod_versione,   
				  prog_listino_produzione,   
				  cod_prodotto_padre,   
				  num_sequenza,   
				  cod_prodotto_figlio,   
				  cod_gruppo_variante,   
				  progr,   
				  cod_prodotto,   
				  des_listino_produzione,   
				  minimo_fatt_altezza,   
				  minimo_fatt_larghezza,   
				  minimo_fatt_profondita,   
				  minimo_fatt_superficie,   
				  minimo_fatt_volume,   
				  flag_sconto_a_parte,   
				  flag_tipo_scaglioni,   
				  scaglione_1,   
				  scaglione_2,   
				  scaglione_3,   
				  scaglione_4,   
				  scaglione_5,   
				  flag_sconto_mag_prezzo_1,   
				  flag_sconto_mag_prezzo_2,   
				  flag_sconto_mag_prezzo_3,   
				  flag_sconto_mag_prezzo_4,   
				  flag_sconto_mag_prezzo_5,   
				  variazione_1,   
				  variazione_2,   
				  variazione_3,   
				  variazione_4,   
				  variazione_5,   
				  flag_origine_prezzo_1,   
				  flag_origine_prezzo_2,   
				  flag_origine_prezzo_3,   
				  flag_origine_prezzo_4,   
				  flag_origine_prezzo_5,   
				  provvigione_1,   
				  provvigione_2 )  
	  SELECT listini_produzione.cod_azienda,   
				listini_produzione.cod_tipo_listino_prodotto,   
				listini_produzione.cod_valuta,   
				listini_produzione.data_inizio_val,   
				listini_produzione.progressivo,   
				listini_produzione.cod_prodotto_listino,   
				listini_produzione.cod_versione,   
				listini_produzione.prog_listino_produzione,   
				listini_produzione.cod_prodotto_padre,   
				listini_produzione.num_sequenza,   
				listini_produzione.cod_prodotto_figlio,   
				listini_produzione.cod_gruppo_variante,   
				listini_produzione.progr,   
				listini_produzione.cod_prodotto,   
				listini_produzione.des_listino_produzione,   
				listini_produzione.minimo_fatt_altezza,   
				listini_produzione.minimo_fatt_larghezza,   
				listini_produzione.minimo_fatt_profondita,   
				listini_produzione.minimo_fatt_superficie,   
				listini_produzione.minimo_fatt_volume,   
				listini_produzione.flag_sconto_a_parte,   
				listini_produzione.flag_tipo_scaglioni,   
				listini_produzione.scaglione_1,   
				listini_produzione.scaglione_2,   
				listini_produzione.scaglione_3,   
				listini_produzione.scaglione_4,   
				listini_produzione.scaglione_5,   
				listini_produzione.flag_sconto_mag_prezzo_1,   
				listini_produzione.flag_sconto_mag_prezzo_2,   
				listini_produzione.flag_sconto_mag_prezzo_3,   
				listini_produzione.flag_sconto_mag_prezzo_4,   
				listini_produzione.flag_sconto_mag_prezzo_5,   
				listini_produzione.variazione_1,   
				listini_produzione.variazione_2,   
				listini_produzione.variazione_3,   
				listini_produzione.variazione_4,   
				listini_produzione.variazione_5,   
				listini_produzione.flag_origine_prezzo_1,   
				listini_produzione.flag_origine_prezzo_2,   
				listini_produzione.flag_origine_prezzo_3,   
				listini_produzione.flag_origine_prezzo_4,   
				listini_produzione.flag_origine_prezzo_5,   
				listini_produzione.provvigione_1,   
				listini_produzione.provvigione_2  
		 FROM listini_produzione 
		 WHERE cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  and
				 cod_valuta = :ls_cod_valuta and
				 data_inizio_val = :ldt_data_inizio_val  and
				 progressivo = :ll_progressivo	 ;
						 
		if sqlca.sqlcode = -1 then
			messagebox("APICE", "Errore in insert listino vendite DIM LOCALE~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	end if
	
next

COMMIT;
end event

type dw_1 from datawindow within w_crea_listini_condivisi
integer x = 14
integer y = 12
integer width = 4224
integer height = 1128
integer taborder = 10
string title = "none"
string dataobject = "d_crea_listini_comuni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

