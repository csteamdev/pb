﻿$PBExportHeader$w_crea_mov_magazzino_excel.srw
$PBExportComments$Procedira Personalizzata di carico magazzino a q.ta definita.
forward
global type w_crea_mov_magazzino_excel from window
end type
type st_18 from statictext within w_crea_mov_magazzino_excel
end type
type st_17 from statictext within w_crea_mov_magazzino_excel
end type
type em_referenza from editmask within w_crea_mov_magazzino_excel
end type
type st_16 from statictext within w_crea_mov_magazzino_excel
end type
type st_15 from statictext within w_crea_mov_magazzino_excel
end type
type st_14 from statictext within w_crea_mov_magazzino_excel
end type
type st_13 from statictext within w_crea_mov_magazzino_excel
end type
type st_12 from statictext within w_crea_mov_magazzino_excel
end type
type cbx_commit from checkbox within w_crea_mov_magazzino_excel
end type
type em_deposito from editmask within w_crea_mov_magazzino_excel
end type
type st_11 from statictext within w_crea_mov_magazzino_excel
end type
type cbx_1 from checkbox within w_crea_mov_magazzino_excel
end type
type st_10 from statictext within w_crea_mov_magazzino_excel
end type
type cb_2 from commandbutton within w_crea_mov_magazzino_excel
end type
type st_9 from statictext within w_crea_mov_magazzino_excel
end type
type st_8 from statictext within w_crea_mov_magazzino_excel
end type
type st_7 from statictext within w_crea_mov_magazzino_excel
end type
type st_5 from statictext within w_crea_mov_magazzino_excel
end type
type st_2 from statictext within w_crea_mov_magazzino_excel
end type
type em_file from editmask within w_crea_mov_magazzino_excel
end type
type st_1 from statictext within w_crea_mov_magazzino_excel
end type
type st_6 from statictext within w_crea_mov_magazzino_excel
end type
type st_record from statictext within w_crea_mov_magazzino_excel
end type
type st_4 from statictext within w_crea_mov_magazzino_excel
end type
type cb_1 from commandbutton within w_crea_mov_magazzino_excel
end type
type em_cod_tipo_movimento from editmask within w_crea_mov_magazzino_excel
end type
type st_3 from statictext within w_crea_mov_magazzino_excel
end type
end forward

global type w_crea_mov_magazzino_excel from window
integer x = 1074
integer y = 484
integer width = 3259
integer height = 2140
boolean titlebar = true
string title = "UTILITA~' GENERAZIONE MOVIMENTI DI MAGAZZINO"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
st_18 st_18
st_17 st_17
em_referenza em_referenza
st_16 st_16
st_15 st_15
st_14 st_14
st_13 st_13
st_12 st_12
cbx_commit cbx_commit
em_deposito em_deposito
st_11 st_11
cbx_1 cbx_1
st_10 st_10
cb_2 cb_2
st_9 st_9
st_8 st_8
st_7 st_7
st_5 st_5
st_2 st_2
em_file em_file
st_1 st_1
st_6 st_6
st_record st_record
st_4 st_4
cb_1 cb_1
em_cod_tipo_movimento em_cod_tipo_movimento
st_3 st_3
end type
global w_crea_mov_magazzino_excel w_crea_mov_magazzino_excel

forward prototypes
public function integer wf_scrivi_log (string fs_messaggio)
end prototypes

public function integer wf_scrivi_log (string fs_messaggio);integer li_file,li_risposta
string  ls_messaggio, ls_file

ls_file = guo_functions.uof_get_user_documents_folder( ) + "creamov" + string(year(today()),"0000") + string(month(today()),"00") + string(day(today()),"00") + ".log"

li_file = fileopen(ls_file, linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	//g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	//return -1
	return 0
end if

ls_messaggio = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + fs_messaggio
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

on w_crea_mov_magazzino_excel.create
this.st_18=create st_18
this.st_17=create st_17
this.em_referenza=create em_referenza
this.st_16=create st_16
this.st_15=create st_15
this.st_14=create st_14
this.st_13=create st_13
this.st_12=create st_12
this.cbx_commit=create cbx_commit
this.em_deposito=create em_deposito
this.st_11=create st_11
this.cbx_1=create cbx_1
this.st_10=create st_10
this.cb_2=create cb_2
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_5=create st_5
this.st_2=create st_2
this.em_file=create em_file
this.st_1=create st_1
this.st_6=create st_6
this.st_record=create st_record
this.st_4=create st_4
this.cb_1=create cb_1
this.em_cod_tipo_movimento=create em_cod_tipo_movimento
this.st_3=create st_3
this.Control[]={this.st_18,&
this.st_17,&
this.em_referenza,&
this.st_16,&
this.st_15,&
this.st_14,&
this.st_13,&
this.st_12,&
this.cbx_commit,&
this.em_deposito,&
this.st_11,&
this.cbx_1,&
this.st_10,&
this.cb_2,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_5,&
this.st_2,&
this.em_file,&
this.st_1,&
this.st_6,&
this.st_record,&
this.st_4,&
this.cb_1,&
this.em_cod_tipo_movimento,&
this.st_3}
end on

on w_crea_mov_magazzino_excel.destroy
destroy(this.st_18)
destroy(this.st_17)
destroy(this.em_referenza)
destroy(this.st_16)
destroy(this.st_15)
destroy(this.st_14)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.cbx_commit)
destroy(this.em_deposito)
destroy(this.st_11)
destroy(this.cbx_1)
destroy(this.st_10)
destroy(this.cb_2)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_5)
destroy(this.st_2)
destroy(this.em_file)
destroy(this.st_1)
destroy(this.st_6)
destroy(this.st_record)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.em_cod_tipo_movimento)
destroy(this.st_3)
end on

type st_18 from statictext within w_crea_mov_magazzino_excel
integer x = 1042
integer y = 1004
integer width = 1211
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Marcatore per identificare il movimento"
boolean focusrectangle = false
end type

type st_17 from statictext within w_crea_mov_magazzino_excel
integer x = 96
integer y = 1008
integer width = 571
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "REFERENZA"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_referenza from editmask within w_crea_mov_magazzino_excel
integer x = 681
integer y = 1000
integer width = 334
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_16 from statictext within w_crea_mov_magazzino_excel
integer x = 1042
integer y = 904
integer width = 1211
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Se impostato prevale su quello indicato nel file"
boolean focusrectangle = false
end type

type st_15 from statictext within w_crea_mov_magazzino_excel
integer x = 1042
integer y = 808
integer width = 1211
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Se impostato prevale su quello indicato nel file"
boolean focusrectangle = false
end type

type st_14 from statictext within w_crea_mov_magazzino_excel
integer x = 96
integer y = 808
integer width = 571
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "DEPOSITO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_13 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 196
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "1) Tipo Movimento"
boolean focusrectangle = false
end type

type st_12 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 272
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "2) Deposito"
boolean focusrectangle = false
end type

type cbx_commit from checkbox within w_crea_mov_magazzino_excel
integer x = 1701
integer y = 1108
integer width = 1362
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Una transazione per ogni movimento (Commit)"
boolean checked = true
end type

type em_deposito from editmask within w_crea_mov_magazzino_excel
integer x = 681
integer y = 800
integer width = 334
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_11 from statictext within w_crea_mov_magazzino_excel
integer x = 96
integer y = 808
integer width = 571
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "DEPOSITO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_crea_mov_magazzino_excel
integer x = 681
integer y = 1116
integer width = 754
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prima Riga di Intestazione"
boolean checked = true
end type

type st_10 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 576
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "6) Data Movimento"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_crea_mov_magazzino_excel
integer x = 3077
integer y = 1204
integer width = 82
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;long ll_ret
string docpath,docname

ll_ret = GetFileOpenName("Select File", docpath, docname,"TXT",  "Text Files (*.TXT),*.TXT,", "%userprofile%\documenti")


if ll_ret > 0 then
	em_file.text = docpath
end if
end event

type st_9 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 496
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "5) Valore Movimento"
boolean focusrectangle = false
end type

type st_8 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 420
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "4) Quantità Movimento"
boolean focusrectangle = false
end type

type st_7 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 348
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "3) Codice Prodotto"
boolean focusrectangle = false
end type

type st_5 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 112
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Il formato del file deve essere TXT-Tab Separated con le seguenti colonne:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_crea_mov_magazzino_excel
integer x = 32
integer y = 32
integer width = 2578
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Questa procedura genera movimenti di magazzino con codice, quantità, valore presi dal file specificato."
boolean focusrectangle = false
end type

type em_file from editmask within w_crea_mov_magazzino_excel
integer x = 677
integer y = 1208
integer width = 2395
integer height = 236
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_1 from statictext within w_crea_mov_magazzino_excel
integer x = 55
integer y = 1212
integer width = 608
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "NOME FILE PRODOTTI:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_crea_mov_magazzino_excel
integer x = 27
integer y = 1684
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "record corrente:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_record from statictext within w_crea_mov_magazzino_excel
integer x = 23
integer y = 1760
integer width = 3141
integer height = 88
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_crea_mov_magazzino_excel
integer x = 27
integer y = 1888
integer width = 3177
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "I MOVIMENTI GENERATI SARANNO MEMORIZZATO NEL FILE DI LOG NELLA CARTELLA DOCUMENTI DEL PROFILO UTENTE."
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_crea_mov_magazzino_excel
integer x = 1083
integer y = 1476
integer width = 1115
integer height = 108
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera Movimenti"
end type

event clicked;string 	ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_str, ls_stringa_origine, &
			ls_cod_cliente[], ls_referenza, ls_cod_tipo_movimento, ls_cod_prodotto, ls_des_prodotto, ls_sql, ls_cod_tipo_movimento_file, ls_cod_deposito_file
long   	ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
			ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_file, ll_ret, ll_pos, ll_num_riga
dec{4}	ld_quantita, ld_valore
datetime ldt_data_stock[], ldt_data_registrazione

uo_magazzino luo_mag

ls_cod_deposito[1] = em_deposito.text
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

ls_cod_tipo_movimento = em_cod_tipo_movimento.text
ll_num_documento = 0
ls_referenza = em_referenza.text

ll_num_riga = 0

// open del file di testo
ll_file = fileopen(em_file.text)
if ll_file < 0 then
	g_mb.messagebox("APICE","Attenzione: problemi in fase di apertura del file " + em_file.text)
	return
end if

do while TRUE
	ll_ret = fileread(ll_file, ls_str)

	// errore file
 	if ll_ret = -1 then
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV") 
		goto fine
	end if
	
	// file file: titto ok vado a committare il tutto.
	if ll_ret = -100 then exit
	
	// riga vuota
	if ll_ret = 0 then continue
	
	ll_num_riga ++
	
	// Prima riga di intestazione.
	if ll_num_riga = 1 and cbx_1.checked then continue

	ls_stringa_origine = ls_str

	ll_pos = pos(ls_str, "~t")
	ls_cod_tipo_movimento_file = left(ls_str, ll_pos -1)
	ls_str = mid(ls_str, ll_pos + 1)
	
	if len(em_cod_tipo_movimento.text) > 0 then
		ls_cod_tipo_movimento = em_cod_tipo_movimento.text
	else
		ls_cod_tipo_movimento = ls_cod_tipo_movimento_file
	end if
	
	ll_pos = pos(ls_str, "~t")
	ls_cod_deposito_file = left(ls_str, ll_pos -1)
	ls_str = mid(ls_str, ll_pos + 1)
	
	if len(em_deposito.text) > 0 then
		ls_cod_deposito[1] = em_deposito.text
	else
		ls_cod_deposito[1] = ls_cod_deposito_file
	end if
	
	ll_pos = pos(ls_str, "~t")
	ls_cod_prodotto = left(ls_str, ll_pos -1)
	ls_str = mid(ls_str, ll_pos + 1)
	
	ll_pos = pos(ls_str, "~t")
	ld_quantita = dec(left(ls_str, ll_pos -1))
	ls_str = mid(ls_str, ll_pos + 1)
	
	ll_pos = pos(ls_str, "~t")
	ld_valore = dec(left(ls_str, ll_pos -1))
	ls_str = mid(ls_str, ll_pos + 1)
	
	if pos(ls_str, "~t") > 0 then
		ll_pos = pos(ls_str, "~t")
		ldt_data_registrazione = datetime( date(left(ls_str, ll_pos -1)), 00:00:00)
	else
		ldt_data_registrazione = datetime( date(trim(ls_str)) ,00:00:00)
	end if
		
	st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto
	yield()	

	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV"
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV") 
		continue;
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV"
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV") 
		continue;
	end if
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
								ls_cod_tipo_movimento, &
								"S", &
								ls_cod_prodotto, &
								ld_quantita, &
								ld_valore, &
								ll_num_documento, &
								ldt_data_registrazione, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[] ) =  0 then
								
	
		st_record.text = 	"Movimento " + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) 	+ " - "  + ls_stringa_origine
		wf_scrivi_log(		"Movimento " + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) 	+ "~t" + ls_stringa_origine)
		
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock) = -1 then
			goto fine
		end if
		
	else
		
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore."
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore.")
		goto fine		
	end if	
	
	if cbx_commit.checked then
		commit;
	end if
	
	destroy luo_mag
		
loop

fileclose(ll_file)

commit;
fileclose(ll_file)
st_record.text = "processo terminato"
wf_scrivi_log("FINITO CREAZIONE MOVIMENTI")
return			


FINE:
fileclose(ll_file)
ROLLBACK;
g_mb.messagebox("APICE","Elaborazione annullata interrotta per un errore: verificare il file di log")
return
end event

type em_cod_tipo_movimento from editmask within w_crea_mov_magazzino_excel
integer x = 681
integer y = 896
integer width = 334
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_3 from statictext within w_crea_mov_magazzino_excel
integer x = 96
integer y = 904
integer width = 571
integer height = 68
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "CODICE MOVIMENTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

