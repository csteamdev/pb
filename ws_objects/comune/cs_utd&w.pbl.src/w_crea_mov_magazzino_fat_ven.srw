﻿$PBExportHeader$w_crea_mov_magazzino_fat_ven.srw
forward
global type w_crea_mov_magazzino_fat_ven from w_cs_xx_principale
end type
type st_3 from statictext within w_crea_mov_magazzino_fat_ven
end type
type cb_leggi_det from commandbutton within w_crea_mov_magazzino_fat_ven
end type
type dw_det from uo_std_dw within w_crea_mov_magazzino_fat_ven
end type
type cb_1 from commandbutton within w_crea_mov_magazzino_fat_ven
end type
type sle_num from singlelineedit within w_crea_mov_magazzino_fat_ven
end type
type st_2 from statictext within w_crea_mov_magazzino_fat_ven
end type
type sle_anno from singlelineedit within w_crea_mov_magazzino_fat_ven
end type
type st_1 from statictext within w_crea_mov_magazzino_fat_ven
end type
end forward

global type w_crea_mov_magazzino_fat_ven from w_cs_xx_principale
integer width = 4119
integer height = 1508
st_3 st_3
cb_leggi_det cb_leggi_det
dw_det dw_det
cb_1 cb_1
sle_num sle_num
st_2 st_2
sle_anno sle_anno
st_1 st_1
end type
global w_crea_mov_magazzino_fat_ven w_crea_mov_magazzino_fat_ven

type variables
private:
	string is_selezionato = "N"
end variables

forward prototypes
public function integer wf_crea_dest_mov_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
end prototypes

public function integer wf_crea_dest_mov_fat_ven (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_pagamento, ls_cod_tipo_det_acq, ls_cod_deposito[], ls_cod_ubicazione[], &
		 ls_cod_lotto[], ls_cod_prodotto, ls_cod_tipo_movimento, ls_flag_tipo_det_acq, &
		 ls_cod_cliente[], ls_cod_fornitore[], ls_cod_misura, &
		 ls_cod_for_acc_mat, ls_rif_fat_acq, ls_rif_ord_acq, ls_flag_agg_costo_ultimo,&
		 ls_cod_prodotto_raggruppato, ls_flag_acc_materiali
		 
long ll_prog_riga, ll_progr_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, &
	  ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], ll_num_reg_ord_acq, ll_prog_ord_acq, &
	  ll_prog_bolla_acq, ll_prog_riga_bolla_acq, ll_anno_esercizio, ll_num_acc_materiali,&
	  ll_num_bolla_acq
	  
integer li_anno_reg_ord_acq, li_anno_bolla_acq

dec{4} ld_sconto_testata, ld_cambio_ven, ld_quantita, ld_prezzo_acquisto, ld_sconto_1, &
       ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_pagamento, ld_val_sconto_1, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_riga_netto, ld_quan_ordinata, &
		 ld_num_pezzi_confezione, ld_quan_sconto_merce, ld_quan_raggruppo
		 
datetime ldt_data_stock[], ldt_data_documento

uo_magazzino luo_mag

ls_flag_acc_materiali = "N"


ll_anno_esercizio = f_anno_esercizio()

select cod_fornitore,
		 data_fattura,
		 cambio_ven,
		 cod_pagamento,
		 sconto
into   :ls_cod_for_acc_mat, 
			:ldt_data_documento, 
		 :ld_cambio_ven,
		 :ls_cod_pagamento,
		 :ld_sconto_testata
from 	 tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la lettura Testata: "+sqlca.sqlerrtext
	rollback;
	return -1
end if

select sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	ld_sconto_pagamento = 0
end if

declare cu_det cursor for 

select  prog_riga_fat_ven ,
		cod_tipo_det_ven,
		cod_deposito, 
		cod_ubicazione,
		cod_lotto,
		data_stock,
		progr_stock,
		cod_prodotto,
		quan_fatturata,
		prezzo_vendita,
		sconto_1,
		sconto_2,
		sconto_3,
		sconto_4,
		sconto_5,
		sconto_6,
		sconto_7,
		sconto_8,
		sconto_9,
		sconto_10,
		cod_tipo_movimento,
		anno_reg_des_mov,
		num_reg_des_mov
from det_fat_ven
where   cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione
order by prog_riga_fat_ven asc;

open cu_det;

do while 0 = 0
	fetch cu_det into :ll_prog_riga,   
						   :ls_cod_tipo_det_acq,   
						   :ls_cod_deposito[1],   
						   :ls_cod_ubicazione[1],   
						   :ls_cod_lotto[1],   
						   :ldt_data_stock[1],   
						   :ll_progr_stock[1],   
						   :ls_cod_prodotto,   
						   :ld_quantita,   
						   :ld_prezzo_acquisto,   
						   :ld_sconto_1,   
						   :ld_sconto_2,  
						   :ld_sconto_3,   
						   :ld_sconto_4,   
						   :ld_sconto_5,   
						   :ld_sconto_6,   
						   :ld_sconto_7,   
						   :ld_sconto_8,   
						   :ld_sconto_9,   
						   :ld_sconto_10,
						   :ls_cod_tipo_movimento,
						   :ll_anno_reg_des_mov,
							:ll_num_reg_des_mov;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore durante la lettura Dettagli: "+sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if
	
	if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then continue
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		ld_quantita = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quantita, "M")
		ld_prezzo_acquisto = f_converti_qta_raggruppo(ls_cod_prodotto, ld_prezzo_acquisto, "D")
		//ls_cod_prodotto = ls_cod_prodotto_raggruppato
	end if
	
	
	// --------------------------------------------------
	luo_mag = create uo_magazzino
	int li_result
	
	li_result = luo_mag.uof_verifica_dest_mov_magazzino("det_fat_ven",fl_anno_registrazione, fl_num_registrazione, ll_prog_riga, &
							ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ldt_data_stock, &
							ll_progr_stock, ls_cod_cliente, ls_cod_fornitore, ref ll_anno_reg_des_mov, ref ll_num_reg_des_mov, ref fs_messaggio)
			
	if li_result <> 0 then
		destroy luo_mag
		rollback;
		close cu_det;
		return li_result
	end if
	// --------------------------------------------------
			
	if luo_mag.uof_movimenti_mag(ldt_data_documento, &
							 ls_cod_tipo_movimento, &
							 'N', &
							 ls_cod_prodotto, &
							 ld_quantita, &
							 ld_val_riga_netto, &
							 fl_num_registrazione, &
							 ldt_data_documento, &
							 "", &
							 ll_anno_reg_des_mov, &
							 ll_num_reg_des_mov, &
							 ls_cod_deposito[], &
							 ls_cod_ubicazione[], &
							 ls_cod_lotto[], &
							 ldt_data_stock[], &
							 ll_progr_stock[], &
							 ls_cod_fornitore[], &
							 ls_cod_cliente[], &
							 ll_anno_reg_mov_mag[], &
							 ll_num_reg_mov_mag[]) = -1 then
		destroy luo_mag
		fs_messaggio = "Attenzione: Si è verificato un errore in fase di generazione movimenti.~r~nDocumento: det_fat_ven (" + string(fl_anno_registrazione) +"/"+ string(fl_num_registrazione) +"/"+  string(ll_prog_riga) + ")~r~n"  + sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if
	
	if f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		fs_messaggio = "Attenzione: Si è verificato un errore in fase di cancellazione destinazione movimenti.~r~nDocumento: det_fat_ven (" + string(fl_anno_registrazione) +"/"+ string(fl_num_registrazione) +"/"+  string(ll_prog_riga) + ")~r~n"  + sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if
	
	
	destroy luo_mag
	
	string ls_sql
	
	ls_sql = "update det_fat_ven" + &
				" set anno_registrazione_mov_mag = " + string(ll_anno_reg_mov_mag[1]) + ", " + &
					 "num_registrazione_mov_mag = " + string(ll_num_reg_mov_mag[1]) + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						 "anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
						 "num_registrazione = " + string(fl_num_registrazione) + " and " + &
						 "prog_riga_fat_ven=" + string(ll_prog_riga)

	execute immediate :ls_sql;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Attenzione: Si è verificato un errore in fase di aggiornamento dettaglio.~r~nDocumento: det_fat_ve (" + string(fl_anno_registrazione) +"/"+ string(fl_num_registrazione) +"/"+  string(ll_prog_riga) + ")~r~n"  + sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if
	
	
//	
//	
//
//	if ld_quan_sconto_merce > 0 and ld_num_pezzi_confezione > 0 then
//		ld_quantita = ld_quantita + (ld_quan_sconto_merce * ld_num_pezzi_confezione)
//	end if
//
//	select tab_tipi_det_acq.flag_tipo_det_acq
//	into   :ls_flag_tipo_det_acq
//	from   tab_tipi_det_acq
//	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
//			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
//
//	if sqlca.sqlcode = -1 then
//		fs_messaggio = "Si è verificato un errore in fase lettura tabella tipi dettaglio acquisto: "+sqlca.sqlerrtext
//		rollback;
//		close cu_det;
//		return -1
//	end if
//
//	if ls_flag_tipo_det_acq = "M" and &
//		(isnull(li_anno_bolla_acq) or li_anno_bolla_acq = 0) and &
//		(isnull(ll_num_bolla_acq) or ll_num_bolla_acq = 0) and &
//		(isnull(ll_prog_bolla_acq) or ll_prog_bolla_acq = 0 ) and &
//		(isnull(ll_prog_riga_bolla_acq) or ll_prog_riga_bolla_acq = 0 ) then
//
//		ld_val_sconto_1 = (ld_prezzo_acquisto * ld_sconto_1) / 100
//		ld_val_riga_sconto_1 = ld_prezzo_acquisto - ld_val_sconto_1
//		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
//		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
//		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
//		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
//		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
//		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
//		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
//		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
//		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
//		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
//		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
//		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
//		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
//		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
//		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
//		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
//		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
//		ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
//		ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
//		ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
//		ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
//		ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
//		ld_val_riga_netto = round((ld_val_riga_netto * ld_cambio_ven), 4)
//
//		ls_cod_fornitore[1] = ls_cod_for_acc_mat
//		setnull(ls_cod_cliente[1])
//
//		if ll_anno_reg_des_mov = 0 or isnull(ll_anno_reg_des_mov) or &
//			ll_num_reg_des_mov = 0 or isnull(ll_num_reg_des_mov) then
//			if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
//													ls_cod_prodotto, &
//													ls_cod_deposito[], &
//													ls_cod_ubicazione[], &
//													ls_cod_lotto[], &
//													ldt_data_stock[], &
//													ll_progr_stock[], &
//													ls_cod_cliente[], &
//													ls_cod_fornitore[], &
//													ll_anno_reg_des_mov, &
//													ll_num_reg_des_mov ) = -1 then
//				fs_messaggio = "Riga "+string(ll_prog_riga)+": Si è verificato un errore in fase di Creazione Destinazioni Movimenti."
//				rollback;
//				close cu_det;
//				return -1
//			end if
//	
//			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, &
//											 ll_num_reg_des_mov, &
//											 ls_cod_tipo_movimento, &
//											 ls_cod_prodotto) = -1 then
//				fs_messaggio =  "Riga "+string(ll_prog_riga)+": Si è verificato un errore in fase di Verifica Destinazioni Movimenti."
//				rollback;
//				close cu_det;
//				return -1
//			end if
//		end if
//		
//		// **** vedo se aggiornare o no il costo ultimo
//		select flag_agg_costo_ultimo
//		into   :ls_flag_agg_costo_ultimo
//		from   det_ord_acq
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :li_anno_reg_ord_acq and
//				 num_registrazione =	:ll_num_reg_ord_acq and
//				 prog_riga_ordine_acq = :ll_prog_ord_acq;
//		
//		if sqlca.sqlcode < 0 then
//			fs_messaggio = "Si è verificato un errore in fase di lettura Aggiorna Costo Ultimo. " + sqlca.sqlerrtext
//			rollback;
//			close cu_det;
//			return -1			
//		end if
//		
//		luo_mag = create uo_magazzino
//
//		if ls_flag_agg_costo_ultimo = "S" then
//			luo_mag.ib_flag_agg_costo_ultimo = true
//		else
//			luo_mag.ib_flag_agg_costo_ultimo = false
//		end if
//		
//		if luo_mag.uof_movimenti_mag(ldt_data_documento, &
//								 ls_cod_tipo_movimento, &
//								 'N', &
//								 ls_cod_prodotto, &
//								 ld_quantita, &
//								 ld_val_riga_netto, &
//								 ll_prog_ord_acq, &
//								 ldt_data_documento, &
//								 "", &
//								 ll_anno_reg_des_mov, &
//								 ll_num_reg_des_mov, &
//								 ls_cod_deposito[], &
//								 ls_cod_ubicazione[], &
//								 ls_cod_lotto[], &
//								 ldt_data_stock[], &
//								 ll_progr_stock[], &
//								 ls_cod_fornitore[], &
//								 ls_cod_cliente[], &
//								 ll_anno_reg_mov_mag[], &
//								 ll_num_reg_mov_mag[]) = -1 then
//			destroy luo_mag
//			fs_messaggio = "Si è verificato un errore in fase di generazione movimenti."
//			rollback;
//			close cu_det;
//			return -1
//		end if
//		
//
//		if f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
//			fs_messaggio = "Si è verificato un errore in fase di cancellazione destinazione movimenti."
//			rollback;
//			close cu_det;
//			return -1
//		end if
//		
//		
//		// stefanop: 15/11/2011: movimento prodotto raggruppato
//		long ll_anno_reg_mov_mag_rag[], ll_num_reg_mov_mag_rag[], ll_null
//		setnull(ll_null)
//		
//		if luo_mag.uof_movimenti_mag_ragguppato(ldt_data_documento, &
//								 ls_cod_tipo_movimento, &
//								 'N', &
//								 ls_cod_prodotto, &
//								 ld_quantita, &
//								 ld_val_riga_netto, &
//								 ll_prog_ord_acq, &
//								 ldt_data_documento, &
//								 "", &
//								 ll_anno_reg_des_mov, &
//								 ll_num_reg_des_mov, &
//								 ls_cod_deposito[], &
//								 ls_cod_ubicazione[], &
//								 ls_cod_lotto[], &
//								 ldt_data_stock[], &
//								 ll_progr_stock[], &
//								 ls_cod_fornitore[], &
//								 ls_cod_cliente[], &
//								 ll_anno_reg_mov_mag_rag[], &
//								 ll_num_reg_mov_mag_rag[],&
//								 ll_null,&
//								 ll_anno_reg_mov_mag[1],&
//								 ll_num_reg_mov_mag[1]) = -1 then
//								 
//			fs_messaggio = "Si è verificato nella gestione dei movimenti del prodotto raggruppato."
//			rollback;
//			close cu_det;
//			destroy luo_mag
//			return -1
//		end if	
//		// ----
		
		
		destroy luo_mag

//		update det_fat_acq
//		set 	anno_registrazione_mov_mag = :ll_anno_reg_mov_mag[1] ,
//			 	num_registrazione_mov_mag = :ll_num_reg_mov_mag[1],
//				cod_deposito = :ls_cod_deposito[1],
//				cod_ubicazione = :ls_cod_ubicazione[1],
//				cod_lotto = :ls_cod_lotto[1],
//				data_stock = :ldt_data_stock[1],
//				prog_stock = :ll_progr_stock[1],
//				flag_accettazione = :ls_flag_acc_materiali
//		where cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione = :fl_anno_registrazione and
//				num_registrazione = :fl_num_registrazione and 
//				prog_riga_fat_acq = :ll_prog_riga;
//
//		if sqlca.sqlcode = -1 then
//			fs_messaggio = "Si è verificato un errore in fase di aggiornamento dettaglio: "+sqlca.sqlerrtext
//			rollback;
//			close cu_det;
//			return -1
//		end if
		
//		if ls_flag_acc_materiali = "S" then
//			select max(num_acc_materiali)  
//				into :ll_num_acc_materiali  
//				from acc_materiali
//				where cod_azienda = :s_cs_xx.cod_azienda and  
//						anno_acc_materiali = :ll_anno_esercizio;
//
//			if sqlca.sqlcode <> 0 then
//				fs_messaggio = "Errore durante la lettura Accettazione Materiali."
//				rollback;
//				return -1
//			end if
//
//			if ll_num_acc_materiali = 0 or isnull(ll_num_acc_materiali) then
//				ll_num_acc_materiali = 1
//			else
//				ll_num_acc_materiali ++
//			end if
//	
//			ls_rif_fat_acq = "Riferimento Fattura: " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione)
//			ls_rif_ord_acq = "Riferimento Ordine: " + string(li_anno_reg_ord_acq) + "/" + string(ll_num_reg_ord_acq)+ &
//									"/" + string(ll_prog_ord_acq)
//			
//			insert into acc_materiali
//					(cod_azienda,
//					anno_acc_materiali,
//					num_acc_materiali,
//					cod_fornitore,
//					cod_prodotto,
//					cod_prod_fornitore,
//					cod_misura,
//					quan_arrivata,
//					prezzo_acquisto,
//					rif_ord_acq,
//					rif_bol_acq,
//					rif_fat_acq,
//					data_prev_consegna,
//					data_eff_consegna,
//					cod_deposito,
//					cod_ubicazione,
//					cod_lotto,
//					data_stock,
//					prog_stock,
//					nome_doc_compilato,
//					flag_cq,
//					nome_doc_cq,
//					anno_registrazione,
//					num_registrazione,
//					prog_riga_ordine_acq,
//					anno_bolla_acq,
//					num_bolla_acq,
//					progressivo,
//					prog_riga_bolla_acq,
//					anno_reg_fat_acq,
//					num_reg_fat_acq,
//					prog_riga_fat_acq,
//					anno_reg_campionamenti,
//					num_reg_campionamenti,
//					anno_non_conf,
//					num_non_conf,
//					flag_esito_campionamento)
//			values ( :s_cs_xx.cod_azienda,   
//					:ll_anno_esercizio,   
//					:ll_num_acc_materiali,   
//					:ls_cod_for_acc_mat,   
//					:ls_cod_prodotto,   
//					null,   
//					:ls_cod_misura,   
//					:ld_quantita,   
//					:ld_prezzo_acquisto,   
//					:ls_rif_ord_acq,   
//					null,   
//					:ls_rif_fat_acq,   
//					null,   
//					null,   
//					:ls_cod_deposito[1],   
//					:ls_cod_ubicazione[1],   
//					:ls_cod_lotto[1],   
//					:ldt_data_stock[1],   
//					:ll_progr_stock[1],   
//					null,   
//					null,   
//					null,   
//					:li_anno_reg_ord_acq,
//					:ll_num_reg_ord_acq,
//					:ll_prog_ord_acq,
//					null,
//					null,
//					null,
//					null,
//					:fl_anno_registrazione,
//					:fl_num_registrazione,
//					:ll_prog_riga,
//					null,
//					null,
//					null,
//					null,
//					'D');
//			if sqlca.sqlcode <> 0 then
//				fs_messaggio = "Errore durante la scrittura Accettazione Materiali."
//				rollback;
//				return -1
//			end if
//		end if

//	end if
loop

close cu_det;

//update tes_fat_acq
//set    flag_fat_confermata = 'S'
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_registrazione = :fl_anno_registrazione and
//		 num_registrazione = :fl_num_registrazione;
//
//if sqlca.sqlcode = -1 then
//	fs_messaggio = "Si è verificato un errore in fase di aggiornamento testata."
//	rollback;
//	return -1
//end if

return 1
end function

on w_crea_mov_magazzino_fat_ven.create
int iCurrent
call super::create
this.st_3=create st_3
this.cb_leggi_det=create cb_leggi_det
this.dw_det=create dw_det
this.cb_1=create cb_1
this.sle_num=create sle_num
this.st_2=create st_2
this.sle_anno=create sle_anno
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.cb_leggi_det
this.Control[iCurrent+3]=this.dw_det
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.sle_num
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.sle_anno
this.Control[iCurrent+8]=this.st_1
end on

on w_crea_mov_magazzino_fat_ven.destroy
call super::destroy
destroy(this.st_3)
destroy(this.cb_leggi_det)
destroy(this.dw_det)
destroy(this.cb_1)
destroy(this.sle_num)
destroy(this.st_2)
destroy(this.sle_anno)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;

dw_det.settransobject(sqlca)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type st_3 from statictext within w_crea_mov_magazzino_fat_ven
integer x = 46
integer y = 260
integer width = 430
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Righe Dettaglio:"
boolean focusrectangle = false
end type

type cb_leggi_det from commandbutton within w_crea_mov_magazzino_fat_ven
integer x = 1371
integer y = 180
integer width = 631
integer height = 92
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Leggi righe dettaglio"
end type

event clicked;long ll_anno, ll_num

ll_anno = long(sle_anno.text)
ll_num = long(sle_num.text)

dw_det.retrieve(s_cs_xx.cod_azienda, ll_anno, ll_num)
end event

type dw_det from uo_std_dw within w_crea_mov_magazzino_fat_ven
integer x = 46
integer y = 340
integer width = 4023
integer height = 880
integer taborder = 30
string dataobject = "d_crea_mov_magazzino_fat_ven"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;long ll_i

choose case dwo.name
	case "selezionato_t"
		if row = 0 then
			if is_selezionato = "S" then
				is_selezionato = "N"
			else
				is_selezionato = "S"
			end if
			
			for ll_i = 1 to rowcount()
				setitem(ll_i, "selezionato", is_selezionato)
			next
		end if
		
end choose
end event

type cb_1 from commandbutton within w_crea_mov_magazzino_fat_ven
integer x = 3543
integer y = 1260
integer width = 526
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Genera Movimenti"
end type

event clicked;string ls_mex

if wf_crea_dest_mov_fat_ven(long(sle_anno.text), long(sle_num.text), ls_mex) = 1 then
	commit;
	g_mb.success("Movimenti creati")
else
	g_mb.error(ls_mex)
end if
end event

type sle_num from singlelineedit within w_crea_mov_magazzino_fat_ven
integer x = 937
integer y = 180
integer width = 402
integer height = 72
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "1"
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_crea_mov_magazzino_fat_ven
integer x = 46
integer y = 180
integer width = 398
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno/Numero:"
boolean focusrectangle = false
end type

type sle_anno from singlelineedit within w_crea_mov_magazzino_fat_ven
integer x = 549
integer y = 180
integer width = 357
integer height = 72
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "2012"
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_crea_mov_magazzino_fat_ven
integer x = 23
integer y = 40
integer width = 1417
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Crea movimenti di magazzino a partire da una fattura"
boolean focusrectangle = false
end type

