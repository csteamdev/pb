﻿$PBExportHeader$w_crea_stock_mancanti.srw
forward
global type w_crea_stock_mancanti from w_cs_xx_principale
end type
type sle_prodotto from singlelineedit within w_crea_stock_mancanti
end type
type st_6 from statictext within w_crea_stock_mancanti
end type
type cb_default from commandbutton within w_crea_stock_mancanti
end type
type em_lotto from editmask within w_crea_stock_mancanti
end type
type em_deposito from editmask within w_crea_stock_mancanti
end type
type st_5 from statictext within w_crea_stock_mancanti
end type
type st_4 from statictext within w_crea_stock_mancanti
end type
type st_3 from statictext within w_crea_stock_mancanti
end type
type st_2 from statictext within w_crea_stock_mancanti
end type
type st_1 from statictext within w_crea_stock_mancanti
end type
type em_prog_stock from editmask within w_crea_stock_mancanti
end type
type em_data_stock from editmask within w_crea_stock_mancanti
end type
type st_stato from statictext within w_crea_stock_mancanti
end type
type cb_esegui from commandbutton within w_crea_stock_mancanti
end type
type gb_1 from groupbox within w_crea_stock_mancanti
end type
type em_ubicazione from editmask within w_crea_stock_mancanti
end type
type gb_2 from groupbox within w_crea_stock_mancanti
end type
end forward

global type w_crea_stock_mancanti from w_cs_xx_principale
integer width = 2414
integer height = 928
string title = "Creazione Stock Mancanti"
boolean maxbox = false
boolean resizable = false
sle_prodotto sle_prodotto
st_6 st_6
cb_default cb_default
em_lotto em_lotto
em_deposito em_deposito
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
em_prog_stock em_prog_stock
em_data_stock em_data_stock
st_stato st_stato
cb_esegui cb_esegui
gb_1 gb_1
em_ubicazione em_ubicazione
gb_2 gb_2
end type
global w_crea_stock_mancanti w_crea_stock_mancanti

forward prototypes
public function integer wf_stock ()
end prototypes

public function integer wf_stock ();date   ldd_data_stock

dec{4} ld_quan_inizio_anno, ld_giacenza_inventario, ld_giacenza_reale

long	 ll_prog_stock, ll_count

string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto_sel, ls_temp


ls_cod_prodotto_sel = sle_prodotto.text

if ls_cod_prodotto_sel<>"" and not isnull(ls_cod_prodotto_sel) then
	//attivazione singolo prodotto
	
	select cod_prodotto
	into :ls_temp
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto_sel;
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore inn controllo esistenza prodotto filtro: " + sqlca.sqlerrtext)
		return -1
	end if
	
else
	ls_cod_prodotto_sel = "%"
end if



ls_cod_deposito = em_deposito.text

if isnull(ls_cod_deposito) then
	ls_cod_deposito = "001"
end if

ls_cod_ubicazione = em_ubicazione.text

if isnull(ls_cod_ubicazione) then
	ls_cod_ubicazione = "UB0001"
end if

ls_cod_lotto = em_lotto.text

if isnull(ls_cod_lotto) then
	ls_cod_lotto = "LT0001"
end if

ldd_data_stock = date(em_data_stock.text)

if isnull(ldd_data_stock) then
	ldd_data_stock = date("01/01/2000")
end if

ll_prog_stock = long(em_prog_stock.text)

if isnull(ll_prog_stock) then
	ll_prog_stock = 10
end if

declare stock cursor for
select cod_prodotto
from 	 anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
 		 cod_prodotto not in (	select cod_prodotto
		  							 from   stock
									 where	cod_azienda=:s_cs_xx.cod_azienda and
									 		  	cod_deposito=:ls_cod_deposito and
												cod_ubicazione=:ls_cod_ubicazione and
												cod_lotto=:ls_cod_lotto and
												data_stock=:ldd_data_stock and
												prog_stock=:ll_prog_stock) and
		cod_prodotto like :ls_cod_prodotto_sel;

open stock;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore nella open del cursore stock: " + sqlca.sqlerrtext)
	return -1
end if

ll_count = 0

st_stato.text = ""

do while true
	fetch stock
	into  :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore nella fetch del cursore stock: " + sqlca.sqlerrtext)
		close stock;
		return -1
	elseif sqlca.sqlcode = 100 then
		close stock;
		exit
	end if
	
	//st_stato.text = "Prodotto " + ls_cod_prodotto + " - " + string(ld_quan_inizio_anno)
	st_stato.text = "Prodotto " + ls_cod_prodotto
	ld_quan_inizio_anno = 0
	
	//inserisci solo se lo stock non esiste
	insert into stock
				   (cod_azienda,
					cod_prodotto,
					cod_deposito,
					cod_ubicazione,
					cod_lotto,
					data_stock,
					prog_stock,
					giacenza_stock,
					quan_assegnata,
					quan_in_spedizione,
					costo_medio,
					cod_fornitore,
					cod_cliente,
					flag_stato_lotto)
	values 	   (:s_cs_xx.cod_azienda,
					:ls_cod_prodotto,
					:ls_cod_deposito,
					:ls_cod_ubicazione,
					:ls_cod_lotto,
					:ldd_data_stock,
					:ll_prog_stock,
					:ld_quan_inizio_anno,
					0,
					0,
					0,
					null,
					null,
					'D');
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante creazione nuovo stock: " + sqlca.sqlerrtext)
		close stock;
		rollback;
		return -1
	end if
	
	ll_count ++
	
	select giacenza_inventario,
			 giacenza_reale
	into	 :ld_giacenza_inventario,
			 :ld_giacenza_reale
	from	 inventario
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante controllo presenza prodotto in inventario: " + sqlca.sqlerrtext)
		close stock;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		continue
	end if
	
	insert
	into  inventario_stock
         (cod_azienda,
         cod_prodotto,
         cod_deposito,
         cod_ubicazione,
         cod_lotto,
         data_stock,
         prog_stock,
         anno_reg_mov_mag,
         num_reg_mov_mag,
         giacenza_inventario,
         giacenza_reale)
  values (:s_cs_xx.cod_azienda,
         :ls_cod_prodotto,
         :ls_cod_deposito,
         :ls_cod_ubicazione,
         :ls_cod_lotto,
         :ldd_data_stock,
         :ll_prog_stock,
         null,
         null,
         :ld_giacenza_inventario,
         :ld_giacenza_reale);
	
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante creazione stock inventario: " + sqlca.sqlerrtext)
		close stock;
		rollback;
		return -1
	end if
	
	//commit ad ogni elaborazione
	commit;
	
loop

st_stato.text = string(ll_count) + " prodotti processati"

commit;

g_mb.success("Operazione effettuata con successo!")


return 0
end function

on w_crea_stock_mancanti.create
int iCurrent
call super::create
this.sle_prodotto=create sle_prodotto
this.st_6=create st_6
this.cb_default=create cb_default
this.em_lotto=create em_lotto
this.em_deposito=create em_deposito
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_prog_stock=create em_prog_stock
this.em_data_stock=create em_data_stock
this.st_stato=create st_stato
this.cb_esegui=create cb_esegui
this.gb_1=create gb_1
this.em_ubicazione=create em_ubicazione
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_prodotto
this.Control[iCurrent+2]=this.st_6
this.Control[iCurrent+3]=this.cb_default
this.Control[iCurrent+4]=this.em_lotto
this.Control[iCurrent+5]=this.em_deposito
this.Control[iCurrent+6]=this.st_5
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.em_prog_stock
this.Control[iCurrent+12]=this.em_data_stock
this.Control[iCurrent+13]=this.st_stato
this.Control[iCurrent+14]=this.cb_esegui
this.Control[iCurrent+15]=this.gb_1
this.Control[iCurrent+16]=this.em_ubicazione
this.Control[iCurrent+17]=this.gb_2
end on

on w_crea_stock_mancanti.destroy
call super::destroy
destroy(this.sle_prodotto)
destroy(this.st_6)
destroy(this.cb_default)
destroy(this.em_lotto)
destroy(this.em_deposito)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_prog_stock)
destroy(this.em_data_stock)
destroy(this.st_stato)
destroy(this.cb_esegui)
destroy(this.gb_1)
destroy(this.em_ubicazione)
destroy(this.gb_2)
end on

type sle_prodotto from singlelineedit within w_crea_stock_mancanti
integer x = 1010
integer y = 312
integer width = 635
integer height = 112
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_6 from statictext within w_crea_stock_mancanti
integer x = 37
integer y = 340
integer width = 919
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Solo per il seguente prodotto:"
boolean focusrectangle = false
end type

type cb_default from commandbutton within w_crea_stock_mancanti
integer x = 1577
integer y = 160
integer width = 686
integer height = 80
integer taborder = 70
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reimposta Valori Default"
end type

event clicked;em_deposito.text = "001"

em_ubicazione.text = "UB0001"

em_lotto.text = "LT0001"

em_data_stock.text = "01/01/2000"

em_prog_stock.text = "10"
end event

type em_lotto from editmask within w_crea_stock_mancanti
integer x = 1691
integer y = 60
integer width = 571
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "LT0001"
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!!!!"
end type

event getfocus;selecttext(1,len(text))
end event

type em_deposito from editmask within w_crea_stock_mancanti
integer x = 343
integer y = 60
integer width = 206
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "001"
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!"
end type

event getfocus;selecttext(1,len(text))
end event

type st_5 from statictext within w_crea_stock_mancanti
integer x = 777
integer y = 172
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Progressivo Stock:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_crea_stock_mancanti
integer x = 69
integer y = 172
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Data Stock:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_crea_stock_mancanti
integer x = 1531
integer y = 72
integer width = 160
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Lotto:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_crea_stock_mancanti
integer x = 594
integer y = 72
integer width = 320
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ubicazione:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_crea_stock_mancanti
integer x = 69
integer y = 72
integer width = 251
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deposito:"
boolean focusrectangle = false
end type

type em_prog_stock from editmask within w_crea_stock_mancanti
integer x = 1280
integer y = 160
integer width = 251
integer height = 80
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "10"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####0"
end type

event getfocus;selecttext(1,len(text))
end event

type em_data_stock from editmask within w_crea_stock_mancanti
integer x = 389
integer y = 160
integer width = 343
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "01/01/2000"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

event getfocus;selecttext(1,len(text))
end event

type st_stato from statictext within w_crea_stock_mancanti
integer x = 46
integer y = 728
integer width = 2240
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_esegui from commandbutton within w_crea_stock_mancanti
integer x = 23
integer y = 476
integer width = 2286
integer height = 184
integer taborder = 10
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Stock Mancanti"
end type

event clicked;setpointer(hourglass!)

wf_stock()

setpointer(arrow!)
end event

type gb_1 from groupbox within w_crea_stock_mancanti
integer x = 23
integer y = 688
integer width = 2286
integer height = 120
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type em_ubicazione from editmask within w_crea_stock_mancanti
integer x = 914
integer y = 60
integer width = 571
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "UB0001"
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!!!!"
end type

event getfocus;selecttext(1,len(text))
end event

type gb_2 from groupbox within w_crea_stock_mancanti
integer x = 23
integer width = 2286
integer height = 280
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

