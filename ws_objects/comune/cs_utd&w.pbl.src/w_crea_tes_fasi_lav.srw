﻿$PBExportHeader$w_crea_tes_fasi_lav.srw
$PBExportComments$Finestra Visione Testata Fasi Dopo Generazione - Import Produzione
forward
global type w_crea_tes_fasi_lav from w_cs_xx_principale
end type
type dw_crea_tes_fasi_lav from uo_cs_xx_dw within w_crea_tes_fasi_lav
end type
type cb_tempi from commandbutton within w_crea_tes_fasi_lav
end type
type cb_dettagli from commandbutton within w_crea_tes_fasi_lav
end type
type cb_1 from uo_cb_close within w_crea_tes_fasi_lav
end type
type cb_2 from uo_cb_save within w_crea_tes_fasi_lav
end type
end forward

global type w_crea_tes_fasi_lav from w_cs_xx_principale
int Width=2273
int Height=1529
boolean TitleBar=true
string Title="Importazione Fasi Lavorazione"
dw_crea_tes_fasi_lav dw_crea_tes_fasi_lav
cb_tempi cb_tempi
cb_dettagli cb_dettagli
cb_1 cb_1
cb_2 cb_2
end type
global w_crea_tes_fasi_lav w_crea_tes_fasi_lav

type variables

end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_crea_tes_fasi_lav,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and &
                  anag_prodotti.cod_prodotto = '" + w_importa_produzione.is_cod_semilavorato + "'")

f_PO_LoadDDDW_DW(dw_crea_tes_fasi_lav,"cod_reparto",sqlca,&
                 "anag_reparti","cod_reparto","des_reparto", &
                 "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and &
                  anag_reparti.cod_reparto = '" + w_importa_produzione.is_cod_reparto + "'")


f_PO_LoadDDDW_DW(dw_crea_tes_fasi_lav,"cod_cat_attrezzature",sqlca,&
                 "tab_cat_attrezzature","cod_cat_attrezzature","des_cat_attrezzature", &
                 "tab_cat_attrezzature.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_crea_tes_fasi_lav.set_dw_key("cod_azienda")
dw_crea_tes_fasi_lav.set_dw_options(sqlca,pcca.null_object,c_retrieveonopen+c_modifyonopen,c_default)


end on

on w_crea_tes_fasi_lav.create
int iCurrent
call w_cs_xx_principale::create
this.dw_crea_tes_fasi_lav=create dw_crea_tes_fasi_lav
this.cb_tempi=create cb_tempi
this.cb_dettagli=create cb_dettagli
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_crea_tes_fasi_lav
this.Control[iCurrent+2]=cb_tempi
this.Control[iCurrent+3]=cb_dettagli
this.Control[iCurrent+4]=cb_1
this.Control[iCurrent+5]=cb_2
end on

on w_crea_tes_fasi_lav.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_crea_tes_fasi_lav)
destroy(this.cb_tempi)
destroy(this.cb_dettagli)
destroy(this.cb_1)
destroy(this.cb_2)
end on

type dw_crea_tes_fasi_lav from uo_cs_xx_dw within w_crea_tes_fasi_lav
int X=23
int Y=21
int Width=2195
int Height=1281
int TabOrder=10
string DataObject="d_crea_tes_fasi_lav"
BorderStyle BorderStyle=StyleLowered!
end type

on pcd_view;call uo_cs_xx_dw::pcd_view;cb_dettagli.enabled = true
cb_tempi.enabled = false
cb_1.enabled = true
cb_2.enabled = false
end on

on pcd_modify;call uo_cs_xx_dw::pcd_modify;cb_dettagli.enabled = false
cb_tempi.enabled = true
cb_1.enabled = false
cb_2.enabled = true
end on

on pcd_new;call uo_cs_xx_dw::pcd_new;this.setitem(this.getrow(), "cod_prodotto", w_importa_produzione.is_cod_semilavorato)
this.setitem(this.getrow(), "cod_reparto", w_importa_produzione.is_cod_reparto)
this.setitem(this.getrow(), "cod_lavorazione", w_importa_produzione.is_cod_lavorazione)

cb_dettagli.enabled = false
cb_tempi.enabled = true
cb_1.enabled = false
cb_2.enabled = true
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   w_importa_produzione.is_cod_semilavorato, &
                   w_importa_produzione.is_cod_reparto, &
                   w_importa_produzione.is_cod_lavorazione)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end on

type cb_tempi from commandbutton within w_crea_tes_fasi_lav
int X=1463
int Y=1321
int Width=366
int Height=81
int TabOrder=20
boolean Enabled=false
string Text="Tempi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;window_open(w_lista_tempi, 0)
s_cs_xx.parametri.parametro_r_1 = round(s_cs_xx.parametri.parametro_r_1, 2)
if s_cs_xx.parametri.parametro_r_1 <> 0 then
   dw_crea_tes_fasi_lav.setitem(dw_crea_tes_fasi_lav.getrow(), "tempo_lavorazione", s_cs_xx.parametri.parametro_r_1)
end if
end on

type cb_dettagli from commandbutton within w_crea_tes_fasi_lav
int X=1852
int Y=1321
int Width=366
int Height=81
int TabOrder=50
boolean Enabled=false
string Text="Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;if not isvalid(w_crea_det_fasi_lav) then
	window_open_parm(w_crea_det_fasi_lav, -1, dw_crea_tes_fasi_lav)
end if
end on

type cb_1 from uo_cb_close within w_crea_tes_fasi_lav
int X=1075
int Y=1321
int Width=366
int Height=81
int TabOrder=40
string Text="Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_2 from uo_cb_save within w_crea_tes_fasi_lav
int X=686
int Y=1321
int Width=366
int Height=81
int TabOrder=30
string Text="Salva"
end type

on clicked;//******************************************************************
//  PC Module     : uo_CB_Main
//  Event         : Clicked
//  Description   : Command button to trigger a PowerClass event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING  l_TrigEvent
STRING  ls_str
INTEGER li_risposta

//------------------------------------------------------------------
//  Controllo l'inserimento dei dati obbligatori
//------------------------------------------------------------------

ls_str = dw_crea_tes_fasi_lav.getitemstring(dw_crea_tes_fasi_lav.getrow(), &
                                            "cod_cat_attrezzature")
if (len(ls_str) = 0) or (isnull(ls_str)) then
   g_mb.messagebox("Testata Fasi Lavorazione", "Attenzione!! Codice categoria attrezzatura mancante", Exclamation!)
   return
end if

if dw_crea_tes_fasi_lav.getitemnumber(dw_crea_tes_fasi_lav.getrow(), &
                                            "tempo_lavorazione") = 0  then
   li_risposta = g_mb.messagebox("Testata Fasi Lavorazione", "Il tempo di lavorazione potrebbe &
                             non essere stato impostato: proseguo?", Exclamation!, YesNo!, 2)
   if li_risposta <> 1 then return
end if


//------------------------------------------------------------------
//  Trigger the event!
//------------------------------------------------------------------

IF IsNull(i_TrigObject) THEN
   IF IsValid(PCCA.Window_Current) THEN
      l_TrigEvent = "pc_" + i_TrigEvent
      PCCA.Window_Current.TriggerEvent(l_TrigEvent)
   END IF
ELSE
   IF IsValid(i_TrigObject) THEN
      l_TrigEvent = "pcd_" + i_TrigEvent
      i_TrigObject.TriggerEvent(l_TrigEvent)
   END IF
END IF
end on

