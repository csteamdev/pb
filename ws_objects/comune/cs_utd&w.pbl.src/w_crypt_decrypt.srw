﻿$PBExportHeader$w_crypt_decrypt.srw
forward
global type w_crypt_decrypt from w_cs_xx_principale
end type
type cb_decripta_db from commandbutton within w_crypt_decrypt
end type
type cb_crypt_db from commandbutton within w_crypt_decrypt
end type
type cb_copy2 from commandbutton within w_crypt_decrypt
end type
type sle_4 from singlelineedit within w_crypt_decrypt
end type
type cb_decripta from commandbutton within w_crypt_decrypt
end type
type sle_2 from singlelineedit within w_crypt_decrypt
end type
type st_4 from statictext within w_crypt_decrypt
end type
type cb_copy1 from commandbutton within w_crypt_decrypt
end type
type sle_3 from singlelineedit within w_crypt_decrypt
end type
type cb_cripta from commandbutton within w_crypt_decrypt
end type
type sle_1 from singlelineedit within w_crypt_decrypt
end type
type st_3 from statictext within w_crypt_decrypt
end type
type r_1 from rectangle within w_crypt_decrypt
end type
type r_2 from rectangle within w_crypt_decrypt
end type
end forward

global type w_crypt_decrypt from w_cs_xx_principale
integer width = 2080
integer height = 1000
string title = "Crypt/Decrypt"
boolean maxbox = false
boolean resizable = false
cb_decripta_db cb_decripta_db
cb_crypt_db cb_crypt_db
cb_copy2 cb_copy2
sle_4 sle_4
cb_decripta cb_decripta
sle_2 sle_2
st_4 st_4
cb_copy1 cb_copy1
sle_3 sle_3
cb_cripta cb_cripta
sle_1 sle_1
st_3 st_3
r_1 r_1
r_2 r_2
end type
global w_crypt_decrypt w_crypt_decrypt

type variables
n_cst_crypto inv_crypt
end variables

event open;call super::open;inv_crypt = create n_cst_crypto
end event

on w_crypt_decrypt.create
int iCurrent
call super::create
this.cb_decripta_db=create cb_decripta_db
this.cb_crypt_db=create cb_crypt_db
this.cb_copy2=create cb_copy2
this.sle_4=create sle_4
this.cb_decripta=create cb_decripta
this.sle_2=create sle_2
this.st_4=create st_4
this.cb_copy1=create cb_copy1
this.sle_3=create sle_3
this.cb_cripta=create cb_cripta
this.sle_1=create sle_1
this.st_3=create st_3
this.r_1=create r_1
this.r_2=create r_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_decripta_db
this.Control[iCurrent+2]=this.cb_crypt_db
this.Control[iCurrent+3]=this.cb_copy2
this.Control[iCurrent+4]=this.sle_4
this.Control[iCurrent+5]=this.cb_decripta
this.Control[iCurrent+6]=this.sle_2
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.cb_copy1
this.Control[iCurrent+9]=this.sle_3
this.Control[iCurrent+10]=this.cb_cripta
this.Control[iCurrent+11]=this.sle_1
this.Control[iCurrent+12]=this.st_3
this.Control[iCurrent+13]=this.r_1
this.Control[iCurrent+14]=this.r_2
end on

on w_crypt_decrypt.destroy
call super::destroy
destroy(this.cb_decripta_db)
destroy(this.cb_crypt_db)
destroy(this.cb_copy2)
destroy(this.sle_4)
destroy(this.cb_decripta)
destroy(this.sle_2)
destroy(this.st_4)
destroy(this.cb_copy1)
destroy(this.sle_3)
destroy(this.cb_cripta)
destroy(this.sle_1)
destroy(this.st_3)
destroy(this.r_1)
destroy(this.r_2)
end on

event close;call super::close;destroy inv_crypt
end event

event pc_setwindow;call super::pc_setwindow;if (Not (s_cs_xx.cod_utente="CS_SYSTEM")) Then
	g_mb.messagebox("Errore", "Non si dispone dei livelli di privilegio necessari per accedere alla finestra", stopsign!)
	postevent("pc_close")
	return
end if
end event

type cb_decripta_db from commandbutton within w_crypt_decrypt
integer x = 1280
integer y = 680
integer width = 379
integer height = 84
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "De-cripta DB"
end type

event clicked;string ls_valore

ls_valore = sle_2.text

if isnull(ls_valore) or ls_valore = "" then
	messagebox("Attenzione!", "Inserire un valore da de-criptare!", Exclamation!)
	sle_4.text = ""
	return
end if

guo_application.uof_set_crypt_params_db( )
if inv_crypt.decryptdata(ls_valore, ls_valore) < 0 then
	messagebox("Attenzione!","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	sle_4.text = ""
	guo_application.uof_set_crypt_params( )
	return
end if

guo_application.uof_set_crypt_params( )
sle_4.text = ls_valore
end event

type cb_crypt_db from commandbutton within w_crypt_decrypt
integer x = 1280
integer y = 220
integer width = 366
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cripta DB"
end type

event clicked;string ls_valore

ls_valore = sle_1.text

if isnull(ls_valore) or ls_valore = "" then
	messagebox("Attenzione!", "Inserire un valore da criptare!", Exclamation!)
	sle_3.text = ""
	return
end if


guo_application.uof_set_crypt_params_db( )
if inv_crypt.EncryptData(ls_valore, ls_valore) < 0 then
	messagebox("Attenzione!","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	sle_3.text = ""
	guo_application.uof_set_crypt_params( )
	return
end if

sle_3.text = ls_valore
guo_application.uof_set_crypt_params( )

end event

type cb_copy2 from commandbutton within w_crypt_decrypt
integer x = 1938
integer y = 776
integer width = 91
integer height = 84
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;sle_4.SelectText ( 1, len(sle_4.text) )
if sle_4.copy() > 0 then
	messagebox("Attenzione!", "Il testo è stato copiato con successo negli appunti!", Information!)
end if
end event

type sle_4 from singlelineedit within w_crypt_decrypt
integer x = 78
integer y = 772
integer width = 1861
integer height = 104
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type cb_decripta from commandbutton within w_crypt_decrypt
integer x = 594
integer y = 680
integer width = 366
integer height = 84
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "De-cripta"
end type

event clicked;string ls_valore

ls_valore = sle_2.text

if isnull(ls_valore) or ls_valore = "" then
	messagebox("Attenzione!", "Inserire un valore da de-criptare!", Exclamation!)
	sle_4.text = ""
	return
end if

if inv_crypt.decryptdata(ls_valore, ls_valore) < 0 then
	messagebox("Attenzione!","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	sle_4.text = ""
	return
end if

sle_4.text = ls_valore
end event

type sle_2 from singlelineedit within w_crypt_decrypt
integer x = 73
integer y = 564
integer width = 1943
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_crypt_decrypt
integer x = 78
integer y = 480
integer width = 1221
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "DECRYPT: inserisci la password da de-criptare"
boolean focusrectangle = false
end type

type cb_copy1 from commandbutton within w_crypt_decrypt
integer x = 1943
integer y = 324
integer width = 91
integer height = 84
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C"
end type

event clicked;sle_3.SelectText ( 1, len(sle_3.text) )
if sle_3.copy() > 0 then
	messagebox("Attenzione!", "Il testo è stato copiato con successo negli appunti!", Information!)
end if
end event

type sle_3 from singlelineedit within w_crypt_decrypt
integer x = 78
integer y = 320
integer width = 1861
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type cb_cripta from commandbutton within w_crypt_decrypt
integer x = 594
integer y = 220
integer width = 366
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cripta"
end type

event clicked;string ls_valore

ls_valore = sle_1.text

if isnull(ls_valore) or ls_valore = "" then
	messagebox("Attenzione!", "Inserire un valore da criptare!", Exclamation!)
	sle_3.text = ""
	return
end if


if inv_crypt.EncryptData(ls_valore, ls_valore) < 0 then
	messagebox("Attenzione!","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	sle_3.text = ""
	return
end if

sle_3.text = ls_valore
end event

type sle_1 from singlelineedit within w_crypt_decrypt
integer x = 69
integer y = 100
integer width = 1943
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_crypt_decrypt
integer x = 78
integer y = 32
integer width = 1070
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "CRYPT: inserisci la password da criptare"
boolean focusrectangle = false
end type

type r_1 from rectangle within w_crypt_decrypt
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 46
integer y = 60
integer width = 2007
integer height = 380
end type

type r_2 from rectangle within w_crypt_decrypt
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 46
integer y = 508
integer width = 2007
integer height = 388
end type

