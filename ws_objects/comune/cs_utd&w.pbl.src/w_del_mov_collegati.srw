﻿$PBExportHeader$w_del_mov_collegati.srw
forward
global type w_del_mov_collegati from window
end type
type em_anno_fine from editmask within w_del_mov_collegati
end type
type ddlb_mese_fine from dropdownlistbox within w_del_mov_collegati
end type
type ddlb_mese_inizio from dropdownlistbox within w_del_mov_collegati
end type
type st_1 from statictext within w_del_mov_collegati
end type
type em_anno_inizio from editmask within w_del_mov_collegati
end type
type st_2 from statictext within w_del_mov_collegati
end type
type sle_where_add from singlelineedit within w_del_mov_collegati
end type
type mle_log from multilineedit within w_del_mov_collegati
end type
type st_fine from statictext within w_del_mov_collegati
end type
type st_inizio from statictext within w_del_mov_collegati
end type
type st_log from statictext within w_del_mov_collegati
end type
type st_progress from statictext within w_del_mov_collegati
end type
type hpb_1 from hprogressbar within w_del_mov_collegati
end type
type cb_elabora from commandbutton within w_del_mov_collegati
end type
end forward

global type w_del_mov_collegati from window
integer width = 3931
integer height = 2660
boolean titlebar = true
string title = "Elimina Movimenti Collegati"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
event ue_scroll_mle ( )
em_anno_fine em_anno_fine
ddlb_mese_fine ddlb_mese_fine
ddlb_mese_inizio ddlb_mese_inizio
st_1 st_1
em_anno_inizio em_anno_inizio
st_2 st_2
sle_where_add sle_where_add
mle_log mle_log
st_fine st_fine
st_inizio st_inizio
st_log st_log
st_progress st_progress
hpb_1 hpb_1
cb_elabora cb_elabora
end type
global w_del_mov_collegati w_del_mov_collegati

forward prototypes
public subroutine wf_get_data_fine (integer ai_anno_reg, integer ai_mese, ref integer ai_giorno_fine)
public function integer wf_cancella_movimento (integer ai_anno_mov, long al_num_mov, ref string as_errore)
end prototypes

event ue_scroll_mle();
mle_log.scroll(mle_log.linecount())
mle_log.SetRedraw(TRUE)
end event

public subroutine wf_get_data_fine (integer ai_anno_reg, integer ai_mese, ref integer ai_giorno_fine);

choose case ai_mese
	case 11, 4, 6, 9
		ai_giorno_fine = 30
		
	case 2
		if ai_anno_reg=2012 then
			//bisestile
			ai_giorno_fine = 29
		else
			ai_giorno_fine = 28
		end if
		
	case else
		ai_giorno_fine = 31
		
end choose


return
end subroutine

public function integer wf_cancella_movimento (integer ai_anno_mov, long al_num_mov, ref string as_errore);


delete from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_mov and
			num_registrazione=:al_num_mov;

if sqlca.sqlcode<0 then
	//******************************
	as_errore = " -> Errore cancellazione mov."+string(ai_anno_mov)+"/"+string(al_num_mov)+" : "+sqlca.sqlerrtext
	return -1
	//*******************************
end if

return 0
end function

on w_del_mov_collegati.create
this.em_anno_fine=create em_anno_fine
this.ddlb_mese_fine=create ddlb_mese_fine
this.ddlb_mese_inizio=create ddlb_mese_inizio
this.st_1=create st_1
this.em_anno_inizio=create em_anno_inizio
this.st_2=create st_2
this.sle_where_add=create sle_where_add
this.mle_log=create mle_log
this.st_fine=create st_fine
this.st_inizio=create st_inizio
this.st_log=create st_log
this.st_progress=create st_progress
this.hpb_1=create hpb_1
this.cb_elabora=create cb_elabora
this.Control[]={this.em_anno_fine,&
this.ddlb_mese_fine,&
this.ddlb_mese_inizio,&
this.st_1,&
this.em_anno_inizio,&
this.st_2,&
this.sle_where_add,&
this.mle_log,&
this.st_fine,&
this.st_inizio,&
this.st_log,&
this.st_progress,&
this.hpb_1,&
this.cb_elabora}
end on

on w_del_mov_collegati.destroy
destroy(this.em_anno_fine)
destroy(this.ddlb_mese_fine)
destroy(this.ddlb_mese_inizio)
destroy(this.st_1)
destroy(this.em_anno_inizio)
destroy(this.st_2)
destroy(this.sle_where_add)
destroy(this.mle_log)
destroy(this.st_fine)
destroy(this.st_inizio)
destroy(this.st_log)
destroy(this.st_progress)
destroy(this.hpb_1)
destroy(this.cb_elabora)
end on

event open;string			ls_rag_soc_1

select rag_soc_1
into :ls_rag_soc_1
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

this.title = "Elimina Movimenti Collegati - " + ls_rag_soc_1

em_anno_fine.text = string(year(today()))
ddlb_mese_fine.text = "12"

em_anno_inizio.text = string(year(today()))
ddlb_mese_inizio.text = "1"
end event

type em_anno_fine from editmask within w_del_mov_collegati
integer x = 1632
integer y = 36
integer width = 247
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####"
end type

type ddlb_mese_fine from dropdownlistbox within w_del_mov_collegati
integer x = 1408
integer y = 36
integer width = 215
integer height = 1040
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean sorted = false
string item[] = {"1","2","3","4","5","6","7","8","9","10","11","12"}
borderstyle borderstyle = stylelowered!
end type

type ddlb_mese_inizio from dropdownlistbox within w_del_mov_collegati
integer x = 795
integer y = 36
integer width = 215
integer height = 1040
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean sorted = false
string item[] = {"1","2","3","4","5","6","7","8","9","10","11","12"}
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_del_mov_collegati
integer x = 41
integer y = 36
integer width = 709
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
string text = "Mese/Anno inizio - fine"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_anno_inizio from editmask within w_del_mov_collegati
integer x = 1019
integer y = 36
integer width = 247
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####"
end type

type st_2 from statictext within w_del_mov_collegati
integer x = 2450
integer y = 176
integer width = 1381
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 67108864
string text = "<- es. and cod_prodotto in (~'cod1~',~'cod2~',~'cod3~')"
boolean focusrectangle = false
end type

type sle_where_add from singlelineedit within w_del_mov_collegati
integer x = 101
integer y = 152
integer width = 2331
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_del_mov_collegati
integer x = 32
integer y = 1268
integer width = 3767
integer height = 1256
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type st_fine from statictext within w_del_mov_collegati
integer x = 1042
integer y = 472
integer width = 919
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_inizio from statictext within w_del_mov_collegati
integer x = 50
integer y = 472
integer width = 919
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
alignment alignment = right!
boolean focusrectangle = false
end type

type st_log from statictext within w_del_mov_collegati
integer x = 32
integer y = 680
integer width = 3749
integer height = 556
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Pronto!"
boolean focusrectangle = false
end type

type st_progress from statictext within w_del_mov_collegati
integer x = 466
integer y = 336
integer width = 2994
integer height = 76
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_del_mov_collegati
integer x = 37
integer y = 592
integer width = 3739
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_elabora from commandbutton within w_del_mov_collegati
integer x = 37
integer y = 312
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "inizio"
end type

event clicked;datastore			lds_data
longlong				ll_tot, ll_num_mov, ll_k
string					ls_sql, ls_errore, ls_where_temp, ls_data_inizio, ls_where_data, ls_cod_prodotto, ls_temp
integer				li_anno_fine, li_mese, li_giorno_fine, li_anno_mov, li_percentuale, li_anno_inizio, li_mese_inizio, li_mese_fine, &
						li_anno_corrente, li_mese_corrente, li_mese_uscita
datetime				ldt_inizio, ldt_fine



mle_log.text = ""

li_anno_inizio = integer(em_anno_inizio.text)
li_mese_inizio = integer(ddlb_mese_inizio.text)

if isnull(li_anno_inizio) or li_anno_inizio <= 1950 then
	g_mb.warning("Impostare l'anno inizio elaborazione!")
	return
end if

if isnull(li_mese_inizio) or li_mese_inizio <1 or li_mese_inizio>12 then
	g_mb.warning("Impostare il mese inizio elaborazione!")
	return
end if

li_anno_fine = integer(em_anno_fine.text)
li_mese_fine =  integer(ddlb_mese_fine.text)

if isnull(li_anno_fine) or li_anno_fine <= 1950 then
	g_mb.warning("Impostare l'anno fine elaborazione!")
	return
end if

if isnull(li_mese_fine) or li_mese_fine <1 or li_mese_fine>12 then
	g_mb.warning("Impostare il mese inizio elaborazione!")
	return
end if

ldt_inizio = datetime(date(li_anno_inizio, li_mese_inizio, 1))
wf_get_data_fine(li_anno_fine, li_mese_fine, li_giorno_fine)
ldt_fine = datetime(date(li_anno_fine, li_mese_fine, li_giorno_fine))
ldt_inizio = datetime(date(ldt_inizio), 00:00:00)
ldt_fine = datetime(date(ldt_fine), 00:00:00)

if ldt_inizio > ldt_fine then
	g_mb.warning("La data fine non può essere inferiore alla data inizio elaborazione!")
	return
end if

st_inizio.text = string(datetime(today(), now()), "dd/mm/yyyy hh:mm:ss")
st_fine.text = ""
this.enabled = false


//ls_where_temp = "and cod_prodotto like 'A1013_' "
if ls_where_temp<>"" and not isnull(ls_where_temp) then
	ls_where_temp = trim(sle_where_add.text)
	if upper(left(ls_where_temp, 3)) <> "AND" then ls_where_temp = "and " + ls_where_temp
end if


li_anno_corrente = li_anno_fine
li_mese_corrente = li_mese_fine

//1° ciclo su anno
do while li_anno_corrente >= li_anno_inizio
	
	if li_anno_corrente = li_anno_inizio then
		//uscirò dal ciclo nel mese stabilito
		li_mese_uscita = li_mese_inizio
	else
		//uscirò dal ciclo nel mese di gennaio
		li_mese_uscita = 1
	end if
	
	//2° ciclo sui mesi dell'anno
	do while li_mese_corrente >= li_mese_uscita
		
		st_log.text = ""
		st_progress.text = "Estrazione Mov. anno "+string(li_anno_corrente)+" mese "+string(li_mese_corrente)+" in corso ..."
		
		//recupero data fine del mese /anno corrente
		wf_get_data_fine(li_anno_corrente, li_mese_corrente, li_giorno_fine)
		
		ls_sql = "select anno_registrazione, num_registrazione, cod_prodotto "+&
				"from mov_magazzino "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"anno_reg_mov_origine>0 and num_reg_mov_origine>0 and "+&
					"data_registrazione>='"+string(li_anno_corrente)+string(li_mese_corrente,"00")+"01' and "+&
					"data_registrazione<='"+string(li_anno_corrente)+string(li_mese_corrente,"00")+string(li_giorno_fine,"00")+"' "
		
		//aggiungo eventuale where aggiuntiva
		ls_sql += " " + ls_where_temp + " "
		
		ls_temp = "Elaborazione Mov. dal 01/"+string(li_mese_corrente, "00")+"/"+string(li_anno_corrente)+" al " + string(li_giorno_fine,"00") + "/"+string(li_mese_corrente,"00")+"/"+string(li_anno_corrente)
		
		ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
		
		if ll_tot<0 then
			st_log.text = ls_errore
			this.enabled = true
			st_fine.text = string(datetime(today(), now()), "dd/mm/yyyy hh:mm:ss")
			return
		end if
		
		//INIZIO CICLO CANCELLAZIONE MOVIMENTI DEL MESE/ANNO
		hpb_1.position = 0
		for ll_k=1 to ll_tot
			Yield()
			
			li_percentuale = integer((ll_k / ll_tot)*100)
			hpb_1.position = li_percentuale
			
			li_anno_mov = lds_data.getitemnumber(ll_k, 1)
			ll_num_mov = lds_data.getitemnumber(ll_k, 2)
			ls_cod_prodotto = lds_data.getitemstring(ll_k, 3)
			st_log.text = ls_temp + "~r~nMov n° "+string(li_anno_mov)+"/"+string(ll_num_mov)+ " Prodotto '" + ls_cod_prodotto + "' ("+string(ll_k)+" di "+string(ll_tot)+") ..."
			
			if wf_cancella_movimento(li_anno_mov, ll_num_mov, ls_errore) < 0 then
				rollback;
				st_log.text = "Anno "+string(li_anno_corrente) +" / Mese " +string(li_mese_corrente)+" " + ls_errore
				this.enabled = true
				destroy lds_data
				return
			else
				//commit su ogni singola cancellazione
				commit;
			end if

		next
		
		mle_log.text += "~r~n Anno "+string(li_anno_corrente) +" / Mese " +string(li_mese_corrente) + " -> Totale Movimenti Collegati elaborati : " +string(ll_tot)
		parent.event trigger ue_scroll_mle()
		destroy lds_data
		//FINE CICLO FOR CANCELLAZIONE MOVIMENTI DEL MESE/ANNO
		
		//vado al mese precedente
		li_mese_corrente -= 1
		
	loop
	//FINE CICLO WHILE MENSILE
	
	//vado all'anno precedente
	li_anno_corrente -= 1
	
	//e riprendo da dicembre
	li_mese_corrente = 12
	
loop
//FINE CICLO WHILE ANNUALE


//*********************************
st_progress.text = "Operazione terminata!"
st_log.text = ""

st_fine.text = string(datetime(today(), now()), "dd/mm/yyyy hh:mm:ss")
this.enabled = true

return
//**********************************




end event

