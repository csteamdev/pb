﻿$PBExportHeader$w_duplica_parametri_azienda.srw
forward
global type w_duplica_parametri_azienda from w_cs_xx_principale
end type
type hpb_1 from hprogressbar within w_duplica_parametri_azienda
end type
type cb_esegui from commandbutton within w_duplica_parametri_azienda
end type
type dw_duplica_parametri_azienda_2 from datawindow within w_duplica_parametri_azienda
end type
type dw_duplica_parametri_azienda_1 from datawindow within w_duplica_parametri_azienda
end type
end forward

global type w_duplica_parametri_azienda from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3378
integer height = 528
string title = "Duplicazione Parametri Aziendali"
boolean maxbox = false
boolean resizable = false
hpb_1 hpb_1
cb_esegui cb_esegui
dw_duplica_parametri_azienda_2 dw_duplica_parametri_azienda_2
dw_duplica_parametri_azienda_1 dw_duplica_parametri_azienda_1
end type
global w_duplica_parametri_azienda w_duplica_parametri_azienda

event pc_setwindow;call super::pc_setwindow;dw_duplica_parametri_azienda_1.insertrow(0)

dw_duplica_parametri_azienda_2.insertrow(0)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_duplica_parametri_azienda_2,"cod_azienda",sqlca,"aziende","cod_azienda",&
					  "rag_soc_1","cod_azienda <> '" + s_cs_xx.cod_azienda + "'")
end event

on w_duplica_parametri_azienda.create
int iCurrent
call super::create
this.hpb_1=create hpb_1
this.cb_esegui=create cb_esegui
this.dw_duplica_parametri_azienda_2=create dw_duplica_parametri_azienda_2
this.dw_duplica_parametri_azienda_1=create dw_duplica_parametri_azienda_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.hpb_1
this.Control[iCurrent+2]=this.cb_esegui
this.Control[iCurrent+3]=this.dw_duplica_parametri_azienda_2
this.Control[iCurrent+4]=this.dw_duplica_parametri_azienda_1
end on

on w_duplica_parametri_azienda.destroy
call super::destroy
destroy(this.hpb_1)
destroy(this.cb_esegui)
destroy(this.dw_duplica_parametri_azienda_2)
destroy(this.dw_duplica_parametri_azienda_1)
end on

type hpb_1 from hprogressbar within w_duplica_parametri_azienda
integer x = 23
integer y = 240
integer width = 3314
integer height = 40
unsignedinteger maxposition = 100
integer setstep = 1
boolean smoothscroll = true
end type

type cb_esegui from commandbutton within w_duplica_parametri_azienda
integer x = 2926
integer y = 320
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;dec{4}   ld_val_num

datetime ldt_val_data

long 		ll_i, ll_count

string 	ls_azienda, ls_cod_parametro, ls_des_parametro, ls_flag_parametro, ls_val_flag, ls_val_str


ls_azienda = dw_duplica_parametri_azienda_2.getitemstring(1,"cod_azienda")

if isnull(ls_azienda) then
	g_mb.messagebox("Parametri Aziendali","Selezionare l'azienda di destinazione prima di continuare",exclamation!)
	return -1
end if

dw_duplica_parametri_azienda_1.reset()

dw_duplica_parametri_azienda_1.dataobject = "d_duplica_parametri_azienda_1"

dw_duplica_parametri_azienda_1.settransobject(sqlca)

if dw_duplica_parametri_azienda_1.retrieve(s_cs_xx.cod_azienda) = -1 then
	g_mb.messagebox("Parametri Aziendali","Errore in lettura elenco parametri azienda corrente",stopsign!)
	return -1
end if

ll_count = dw_duplica_parametri_azienda_1.rowcount()

if ll_count > 0 then
	if g_mb.messagebox("Parametri Aziendali","Azzerare tutti i parametri dell'azienda destinazione e reimpostarli uguali all'azienda corrente?",question!,yesno!,2) = 2 then
		return -1
	end if
else
	g_mb.messagebox("Parametri Aziendali","L'azienda corrente non ha alcun parametro: operazione interrotta",exclamation!)
	return -1
end if

setpointer(hourglass!)

delete from parametri_azienda
where  cod_azienda = :ls_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Parametri Aziendali","Errore in azzeramento parametri: " + sqlca.sqlerrtext,stopsign!)
	setpointer(arrow!)
	rollback;
	return -1
end if

for ll_i = 1 to ll_count
	
	dw_duplica_parametri_azienda_1.setrow(ll_i)
	
	dw_duplica_parametri_azienda_1.scrolltorow(ll_i)
	
	ls_cod_parametro = dw_duplica_parametri_azienda_1.getitemstring(ll_i,"cod_parametro")
	
	ls_des_parametro = dw_duplica_parametri_azienda_1.getitemstring(ll_i,"des_parametro")
	
	ls_flag_parametro = dw_duplica_parametri_azienda_1.getitemstring(ll_i,"flag_parametro")
	
	ls_val_flag = dw_duplica_parametri_azienda_1.getitemstring(ll_i,"flag")
	
	ls_val_str = dw_duplica_parametri_azienda_1.getitemstring(ll_i,"stringa")
	
	ld_val_num = dw_duplica_parametri_azienda_1.getitemnumber(ll_i,"numero")
	
	ldt_val_data = dw_duplica_parametri_azienda_1.getitemdatetime(ll_i,"data")
	
	insert
	into   parametri_azienda
			 (cod_azienda,
			 cod_parametro,
			 flag_parametro,
			 des_parametro,
			 stringa,
			 flag,
			 data,
			 numero)
	values (:ls_azienda,
			 :ls_cod_parametro,
			 :ls_flag_parametro,
			 :ls_des_parametro,
			 :ls_val_str,
			 :ls_val_flag,
			 :ldt_val_data,
			 :ld_val_num);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Parametri Aziendali","Errore in inserimento parametro " + ls_cod_parametro +": " + sqlca.sqlerrtext,stopsign!)
		setpointer(arrow!)
		rollback;
		return -1
	end if
	
	hpb_1.position = round( (ll_i * 100 / ll_count) , 0 )
	
next

setpointer(arrow!)

commit;
end event

type dw_duplica_parametri_azienda_2 from datawindow within w_duplica_parametri_azienda
integer x = 23
integer y = 300
integer width = 3314
integer height = 120
integer taborder = 10
string dataobject = "d_duplica_parametri_azienda_2"
boolean livescroll = true
end type

type dw_duplica_parametri_azienda_1 from datawindow within w_duplica_parametri_azienda
integer x = 23
integer y = 20
integer width = 3314
integer height = 200
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

