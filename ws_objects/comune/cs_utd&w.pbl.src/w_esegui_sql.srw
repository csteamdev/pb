﻿$PBExportHeader$w_esegui_sql.srw
forward
global type w_esegui_sql from w_cs_xx_principale
end type
type st_numrows from statictext within w_esegui_sql
end type
type cb_execute_select from commandbutton within w_esegui_sql
end type
type cb_clear_select from commandbutton within w_esegui_sql
end type
type mle_select from multilineedit within w_esegui_sql
end type
type dw_select from datawindow within w_esegui_sql
end type
type rb_file from radiobutton within w_esegui_sql
end type
type rb_testo from radiobutton within w_esegui_sql
end type
type cb_clear from commandbutton within w_esegui_sql
end type
type st_path from statictext within w_esegui_sql
end type
type cb_esegui from commandbutton within w_esegui_sql
end type
type cb_file from commandbutton within w_esegui_sql
end type
type mle_sql from multilineedit within w_esegui_sql
end type
type dw_folder from u_folder within w_esegui_sql
end type
end forward

global type w_esegui_sql from w_cs_xx_principale
integer width = 2683
integer height = 2068
string title = "Esegui SQL"
event ue_postopen ( )
st_numrows st_numrows
cb_execute_select cb_execute_select
cb_clear_select cb_clear_select
mle_select mle_select
dw_select dw_select
rb_file rb_file
rb_testo rb_testo
cb_clear cb_clear
st_path st_path
cb_esegui cb_esegui
cb_file cb_file
mle_sql mle_sql
dw_folder dw_folder
end type
global w_esegui_sql w_esegui_sql

type variables
string is_sql = ""
end variables

event ue_postopen();


string ls_pwd

if s_cs_xx.cod_utente <> "CS_SYSTEM" then 

	openwithparm(w_inserisci_altro_valore, ls_pwd)

	ls_pwd = message.Stringparm
	
	if ls_pwd <> "CSTEAM" then
		g_mb.warning("CS_TEAM", "Operazione non autorizzata!")
		postevent("pc_close")
	end if

end if

end event

on w_esegui_sql.create
int iCurrent
call super::create
this.st_numrows=create st_numrows
this.cb_execute_select=create cb_execute_select
this.cb_clear_select=create cb_clear_select
this.mle_select=create mle_select
this.dw_select=create dw_select
this.rb_file=create rb_file
this.rb_testo=create rb_testo
this.cb_clear=create cb_clear
this.st_path=create st_path
this.cb_esegui=create cb_esegui
this.cb_file=create cb_file
this.mle_sql=create mle_sql
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_numrows
this.Control[iCurrent+2]=this.cb_execute_select
this.Control[iCurrent+3]=this.cb_clear_select
this.Control[iCurrent+4]=this.mle_select
this.Control[iCurrent+5]=this.dw_select
this.Control[iCurrent+6]=this.rb_file
this.Control[iCurrent+7]=this.rb_testo
this.Control[iCurrent+8]=this.cb_clear
this.Control[iCurrent+9]=this.st_path
this.Control[iCurrent+10]=this.cb_esegui
this.Control[iCurrent+11]=this.cb_file
this.Control[iCurrent+12]=this.mle_sql
this.Control[iCurrent+13]=this.dw_folder
end on

on w_esegui_sql.destroy
call super::destroy
destroy(this.st_numrows)
destroy(this.cb_execute_select)
destroy(this.cb_clear_select)
destroy(this.mle_select)
destroy(this.dw_select)
destroy(this.rb_file)
destroy(this.rb_testo)
destroy(this.cb_clear)
destroy(this.st_path)
destroy(this.cb_esegui)
destroy(this.cb_file)
destroy(this.mle_sql)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


//------------------------------------------------------------------------------
lw_oggetti[1] = dw_select
lw_oggetti[2] = mle_select
lw_oggetti[3] = cb_clear_select
lw_oggetti[4] = cb_execute_select
lw_oggetti[5] = st_numrows
dw_folder.fu_assigntab(2, "'Query' Selezione", lw_oggetti[])

lw_oggetti[1] = rb_testo
lw_oggetti[2] = rb_file
lw_oggetti[3] = cb_clear
lw_oggetti[4] = cb_esegui
lw_oggetti[5] = st_path
lw_oggetti[6] = cb_file
lw_oggetti[7] = mle_sql
dw_folder.fu_assigntab(1, "'Query' Aggiornamento", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

this.postevent("ue_postopen")



end event

type st_numrows from statictext within w_esegui_sql
integer x = 37
integer y = 952
integer width = 914
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Righe Totali: 0"
boolean border = true
boolean focusrectangle = false
end type

type cb_execute_select from commandbutton within w_esegui_sql
integer x = 389
integer y = 164
integer width = 334
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Execute"
end type

event clicked;string ls_sql, ls_error, ls_syntax
long ll_rows

ls_sql = mle_select.text

if ls_sql = "" or isnull(ls_sql) then
	g_mb.messagebox("CS_TEAM", "Nessuno script SQL da eseguire!", Exclamation!)
	return
end if

if g_mb.messagebox("CS_TEAM", "Eseguire lo script SQL?", Question!, YesNo!, 1) = 2 then
	return
end if

if pos(upper(ls_sql), "DELETE") > 0 or pos(upper(ls_sql), "INSERT") > 0 or &
	pos(upper(ls_sql), "UPDATE") > 0 or pos(upper(ls_sql), "CREATE") > 0 or &
	pos(upper(ls_sql), "DROP") > 0 then
	
	g_mb.messagebox("CS_TEAM", "Lo script SQL deve essere di tipo selezione!", Exclamation!)
	return
end if


ls_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)

if Len(ls_error) > 0 then
	g_mb.messagebox("CS_TEAM", "Errore creazione sintassi SQL! " + ls_error , Exclamation!)
else
	dw_select.create(ls_syntax, ls_error)
	if Len(ls_error) > 0 then
		g_mb.messagebox("CS_TEAM", "Errore creazione datawindow! " + ls_error , Exclamation!)
	end if
end if

dw_select.SetTransObject(SQLCA)
dw_select.Retrieve()
dw_select.visible = true

ll_rows = dw_select.rowcount()

st_numrows.text = "Righe Totali: " + string(ll_rows)


end event

type cb_clear_select from commandbutton within w_esegui_sql
integer x = 46
integer y = 164
integer width = 334
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Clear"
end type

event clicked;mle_select.text = ""

dw_select.dataobject = ""
end event

type mle_select from multilineedit within w_esegui_sql
integer x = 46
integer y = 368
integer width = 2519
integer height = 576
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
end type

type dw_select from datawindow within w_esegui_sql
integer x = 32
integer y = 1044
integer width = 2537
integer height = 864
integer taborder = 20
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type rb_file from radiobutton within w_esegui_sql
integer x = 622
integer y = 168
integer width = 393
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "da file"
end type

event clicked;cb_file.postevent(clicked!)
end event

type rb_testo from radiobutton within w_esegui_sql
integer x = 59
integer y = 168
integer width = 503
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "testo libero"
boolean checked = true
end type

event clicked;mle_sql.text = ""
mle_sql.displayonly = false

is_sql = ""
end event

type cb_clear from commandbutton within w_esegui_sql
integer x = 1888
integer y = 164
integer width = 334
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Clear"
end type

event clicked;
mle_sql.text = ""

is_sql = ""


end event

type st_path from statictext within w_esegui_sql
integer x = 59
integer y = 268
integer width = 2400
integer height = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

type cb_esegui from commandbutton within w_esegui_sql
integer x = 2231
integer y = 164
integer width = 334
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Execute"
end type

event clicked;integer li_FileNum

if rb_testo.checked then
	is_sql = mle_sql.text
else
	li_FileNum = FileOpen(st_path.text, TextMode!)
	FileReadEx(li_FileNum, is_sql)
end if

if is_sql = "" or isnull(is_sql) then
	g_mb.messagebox("CS_TEAM", "Nessuno script SQL da eseguire!", Exclamation!)
	return
end if

if g_mb.messagebox("CS_TEAM", "Eseguire lo script SQL?", Question!, YesNo!, 1) = 2 then
	return
end if

execute immediate :is_sql;
	
if sqlca.sqlcode <> 0 then
	messagebox("Impostazioni Mobilink","Errore in esecuzione script SQL: " + "~r~n" + sqlca.sqlerrtext,stopsign!,ok!)
	rollback;
	return
	
elseif sqlca.sqlcode = 0 then
	commit;
	g_mb.messagebox("CS_TEAM", "Script SQL eseguito con successo!", Exclamation!)
end if	

is_sql = ""
end event

type cb_file from commandbutton within w_esegui_sql
integer x = 2459
integer y = 268
integer width = 105
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I, li_FileNum

is_sql = ""
mle_sql.displayonly = true
mle_sql.text = ""	

value = GetFileOpenName("Selezione", docname, named, "*.sql", "Files sql (*.sql),*.sql")
if value < 1 then
else
	st_path.text = docname
	
	mle_sql.text = ""	
	
	li_FileNum = FileOpen(st_path.text, TextMode!)
	FileReadEx(li_FileNum, is_sql)
	
	mle_sql.text = is_sql
end if
end event

type mle_sql from multilineedit within w_esegui_sql
integer x = 37
integer y = 368
integer width = 2514
integer height = 1540
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_esegui_sql
integer x = 18
integer y = 20
integer width = 2597
integer height = 1912
integer taborder = 20
end type

