﻿$PBExportHeader$w_esporta_utenti.srw
forward
global type w_esporta_utenti from window
end type
type sle_file from singlelineedit within w_esporta_utenti
end type
type cb_esporta from commandbutton within w_esporta_utenti
end type
type cb_decript from commandbutton within w_esporta_utenti
end type
type cb_carica from commandbutton within w_esporta_utenti
end type
type dw_1 from datawindow within w_esporta_utenti
end type
end forward

global type w_esporta_utenti from window
integer width = 4023
integer height = 2448
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
sle_file sle_file
cb_esporta cb_esporta
cb_decript cb_decript
cb_carica cb_carica
dw_1 dw_1
end type
global w_esporta_utenti w_esporta_utenti

forward prototypes
public function integer wf_elabora_file (datastore ads_input, ref string as_errore)
public function integer wf_leggi_excel (string as_file_path, ref string ls_errore)
end prototypes

public function integer wf_elabora_file (datastore ads_input, ref string as_errore);string							ls_input, ls_code[], ls_data[], ls_barcode, ls_tipo, ls_cod_operaio
long							ll_index, ll_ret, ll_count, ll_riga, ll_progressivo, ll_barcode, ll_numero
integer						li_return, li_anno
datetime						ldt_data
s_cs_xx_parametri			lstr_return


ll_ret = ads_input.rowcount()
as_errore = ""

dw_1.reset()

//FORMATO DEL FILE TXT
//			nomecognome,tipo,username,password

for ll_index = 1 to ll_ret
	
	ls_input = ads_input.getitemstring(ll_index,"input")
	
	if isnull(ls_input) or ls_input = "" then
		//salta la riga
		continue
	end if
	
	f_split(ls_input, ";", ls_code[])

	//quindi as_input[] è fatto cosi:
	//		as_input[1]      nomecognome
	//		as_input[2]      tipo
	//		as_input[3]		username
	//		as_input[4]		password
	
	if upperbound(ls_code[]) <4 then
		as_errore = "Numero colonne ("+string(upperbound(ls_code[]))+") inferiori a quelle previste (4)!"
		return -1
	end if
	
	ll_count = dw_1.insertrow(0)
	dw_1.setitem(ll_count, "nominativo", ls_code[1])
	dw_1.setitem(ll_count, "tipo", ls_code[2])
	dw_1.setitem(ll_count, "username", ls_code[3])
	dw_1.setitem(ll_count, "password", ls_code[4])
	//pwd_decrypt
	
next

destroy ads_input

if dw_1.rowcount() = 0 then
	as_errore = "Alla fine dell'elaborazione del file, nessuna riga è stata caricata!"
	return -1
end if

return 0



end function

public function integer wf_leggi_excel (string as_file_path, ref string ls_errore);string ls_value
long ll_row, ll_dw_row
uo_excel luo_excel

luo_excel = create uo_excel
luo_excel.uof_open(as_file_path, false)

ll_row = 2
dw_1.setredraw(false)

do
	ls_value = luo_excel.uof_read_string(ll_row, "A")
	
	if g_str.isempty(ls_value) then
		exit
	end if
	
	ll_dw_row = dw_1.insertrow(0)
	dw_1.setitem(ll_dw_row, 1, ls_value)
	dw_1.setitem(ll_dw_row, 2, luo_excel.uof_read_string(ll_row, "B"))
	dw_1.setitem(ll_dw_row, 3, luo_excel.uof_read_string(ll_row, "C"))
	dw_1.setitem(ll_dw_row, 4, luo_excel.uof_read_string(ll_row, "D"))

	ll_row++
loop while true


ls_errore= "Importazione completata"
dw_1.setredraw(true)
destroy luo_excel
return 0
end function

on w_esporta_utenti.create
this.sle_file=create sle_file
this.cb_esporta=create cb_esporta
this.cb_decript=create cb_decript
this.cb_carica=create cb_carica
this.dw_1=create dw_1
this.Control[]={this.sle_file,&
this.cb_esporta,&
this.cb_decript,&
this.cb_carica,&
this.dw_1}
end on

on w_esporta_utenti.destroy
destroy(this.sle_file)
destroy(this.cb_esporta)
destroy(this.cb_decript)
destroy(this.cb_carica)
destroy(this.dw_1)
end on

type sle_file from singlelineedit within w_esporta_utenti
integer x = 46
integer y = 92
integer width = 2405
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_esporta from commandbutton within w_esporta_utenti
integer x = 3360
integer y = 88
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "esporta"
end type

event clicked;dw_1.saveas("", Excel!, true)
end event

type cb_decript from commandbutton within w_esporta_utenti
integer x = 2907
integer y = 84
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "decripta"
end type

event clicked;string				ls_password, ls_password_decrypt

n_cst_crypto	lnv_crypt

long				ll_index


lnv_crypt = create n_cst_crypto

for ll_index=1 to dw_1.rowcount()
	
	ls_password = dw_1.getitemstring(ll_index, "password")
	
	if ls_password<>"" and not isnull(ls_password) then
		
	end if
	
	if lnv_crypt.decryptdata(ls_password, ls_password_decrypt) < 0 then
		//ls_password_decrypt = "Caratteri non consentiti (usare 0..9, lettere oppure ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { })"
		ls_password_decrypt = "ERRORE!!!!!"
	end if
	
	dw_1.setitem(ll_index, "pwd_decrypt", ls_password_decrypt)
	
next

destroy lnv_crypt
end event

type cb_carica from commandbutton within w_esporta_utenti
integer x = 2464
integer y = 88
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "carica"
end type

event clicked;string			ls_file, ls_docname, ls_named, ls_sql, ls_errore, ls_FMP
long			ll_ret
datastore	lds_input

integer		li_ret


//FMP
guo_functions.uof_get_parametro_azienda("FMP", ls_FMP)
if ls_FMP<>"" and not isnull(ls_FMP) then ChangeDirectory(ls_FMP)

ll_ret = GetFileOpenName("Seleziona File", ls_docname, ls_named, 	&
															"XLSX", &
															"Files Excel (*.XLSX),*.XLSX,"+&
															"Files di testo (*.TXT),*.TXT,"+&
															"Files CSV (*.CSV),*.CSV,")

if ll_ret = 1 then
	sle_file.text = ls_docname
else
	sle_file.text = ""
	return 0
end if



if ls_docname<>"" and not isnull(ls_docname) then
	//aggiorna la lista leggendola dal file
	
	cb_carica.enabled = false
	cb_carica.text = "Caricamento...."
	
	if g_str.end_with(ls_docname, ".xlsx") then
		li_ret = wf_leggi_excel(ls_docname, ls_errore)
	else

		ls_sql = "select '                                                                                                                                                      "+&
									"                                                                                                                                                      ' as input "+&
					"from aziende "+&
					"where cod_azienda=''"
		guo_functions.uof_crea_datastore(lds_input, ls_sql, ls_errore)
	
		ll_ret = lds_input.importfile(CSV!, ls_docname)
	
		if ll_ret<0 then
			g_mb.error("Errore in lettura file selezionato: ("+string(ll_ret)+")")
			return
	
		elseif ll_ret=0 then
			g_mb.error("Il file selezionato sembra vuoto!")
			return
		end if
	
		ls_errore = ""
		
		li_ret = wf_elabora_file(lds_input, ls_errore)
	end if
	
	if li_ret < 0 then
		destroy lds_input
		g_mb.error(ls_errore)
		
	elseif li_ret = 0 then
		if g_str.isnotempty(ls_errore) then g_mb.success( ls_errore)
		
	elseif li_ret>0 and g_str.isnotempty(ls_errore) then
		g_mb.warning( ls_errore)
		
	end if
	
end if

cb_carica.enabled = true
cb_carica.text = "Carica"
return 0
end event

type dw_1 from datawindow within w_esporta_utenti
integer x = 46
integer y = 296
integer width = 3899
integer height = 2004
integer taborder = 10
string title = "none"
string dataobject = "d_esporta_utenti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

