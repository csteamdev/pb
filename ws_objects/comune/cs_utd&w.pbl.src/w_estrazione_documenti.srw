﻿$PBExportHeader$w_estrazione_documenti.srw
forward
global type w_estrazione_documenti from w_cs_xx_principale
end type
type st_1 from statictext within w_estrazione_documenti
end type
type hpb_1 from hprogressbar within w_estrazione_documenti
end type
type cb_start from commandbutton within w_estrazione_documenti
end type
end forward

global type w_estrazione_documenti from w_cs_xx_principale
integer width = 1659
integer height = 464
string title = "Estrazione Documenti"
boolean center = true
st_1 st_1
hpb_1 hpb_1
cb_start cb_start
end type
global w_estrazione_documenti w_estrazione_documenti

type variables
string is_folder_root
end variables

forward prototypes
public function integer wf_leggi_tipo_manutenzione (ref string as_error)
public function integer wf_leggi_anno_numero (string as_cod_tipo_manutenzione, ref string as_error)
public function integer wf_leggi_documenti (string as_cod_tipo_manutenzione, long al_anno_registrazione, long al_num_registrazione, ref string as_error)
public function boolean wf_crea_directory (string as_path)
public subroutine wf_log (string as_write)
public subroutine wf_pulisci (ref string as_stringa)
end prototypes

public function integer wf_leggi_tipo_manutenzione (ref string as_error);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che legge e salva tutti i cod_tipo_manutenzione per poi ciclare per ricavare anno_reg e num_reg di ciascuna manutenzione.
 **/


string ls_sql, ls_cod_tipo_manutenzione
datastore lds_store
long ll_righe, ll_i

ls_sql  = "SELECT distinct cod_tipo_manutenzione from manutenzioni where cod_azienda = '"+ s_cs_xx.cod_azienda +"' order by cod_tipo_manutenzione"
ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)
hpb_1.maxposition = ll_righe
hpb_1.position = 0
Yield()
if ll_righe < 0  then
	return -1
end if

for ll_i = 1 to ll_righe
	Yield()
	hpb_1.position = ll_i
	ls_cod_tipo_manutenzione = lds_store.getitemstring(ll_i, "cod_tipo_manutenzione")
	st_1.text = "Elaborazione in corso di: " + ls_cod_tipo_manutenzione
	wf_log("Tipo manutenzione: " + ls_cod_tipo_manutenzione)
	if wf_leggi_anno_numero(ls_cod_tipo_manutenzione, as_error) < 0 then
		return -1
	end if

next

return 0
end function

public function integer wf_leggi_anno_numero (string as_cod_tipo_manutenzione, ref string as_error);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che ricevendo il cod_tipo_manutenzione ricava anno_reg e num_reg necessari per poi poter estrarre il/i documento/i.
 **/
 
string ls_sql
datastore lds_store
long ll_righe, ll_i, ll_anno_registrazione, ll_num_registrazione

ls_sql = "SELECT anno_registrazione, num_registrazione FROM manutenzioni WHERE cod_azienda ='"+ s_cs_xx.cod_azienda+"' "

if isnull(as_cod_tipo_manutenzione) or as_cod_tipo_manutenzione = "" then
	ls_sql +=  'AND cod_tipo_manutenzione is null'
else 
	ls_sql += "AND cod_tipo_manutenzione= '"+ as_cod_tipo_manutenzione +"' "
end if

ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)

if ll_righe < 0 then return -1 

for ll_i = 1 to ll_righe
	ll_anno_registrazione = lds_store.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = lds_store.getitemnumber(ll_i, "num_registrazione")
	
	if wf_leggi_documenti(as_cod_tipo_manutenzione, ll_anno_registrazione, ll_num_registrazione, as_error) < 0 then 
		return -1
	end if
		
next

return 0
end function

public function integer wf_leggi_documenti (string as_cod_tipo_manutenzione, long al_anno_registrazione, long al_num_registrazione, ref string as_error);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che legge e salva tutti i documenti associati ad un determinato cod_tipo_manutenzione, anno_reg, num_reg.
 * Viene creata la struttura delle directory per poter salvare correttamente i documenti.
 * Nel caso un documento non avesse estensione, viene scritto sul log che tale documento non ha estensione e quindi non è possibile salvarlo.
 **/

string ls_sql, ls_descrizione, ls_estensione, ls_path
datastore lds_store
long ll_righe, ll_file, ll_i, ll_prog_manutenzioni
blob lb_blob
int li_progressivo

ls_sql ="select det_manutenzioni.prog_manutenzioni, det_manutenzioni.descrizione, tab_mimetype.estensione "+& 
"from det_manutenzioni "+&
	"left outer join tab_mimetype "+&
		"on det_manutenzioni.cod_azienda = tab_mimetype.cod_azienda "+&
		"and det_manutenzioni.prog_mimetype = tab_mimetype.prog_mimetype "+&
"where det_manutenzioni.cod_azienda ='"+ s_cs_xx.cod_azienda + "' " + &
	"and det_manutenzioni.anno_registrazione =" + string(al_anno_registrazione) + " " + &
	"and det_manutenzioni.num_registrazione ="+ string(al_num_registrazione)

ll_righe = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_error)

if ll_righe < 0 then return -1

for ll_i =1 to ll_righe
	
	ll_prog_manutenzioni = lds_store.getitemnumber(ll_i, 1) //prog_manutenzioni
	ls_descrizione = lds_store.getitemstring(ll_i, 2) //descrizione
	ls_estensione = lds_store.getitemstring(ll_i, 3) //estensione
	
	// Verifico descrizione
	if not isnull(ls_descrizione) and ls_descrizione <> "" then
		ls_descrizione = left(ls_descrizione,100)
		wf_pulisci(ls_descrizione)
	else
		ls_descrizione = string(al_anno_registrazione) + "_" + string(al_num_registrazione) + "_" + string(ll_prog_manutenzioni)
	end if
	
	//Verifico estensione
	if not isnull(ls_estensione) and ls_estensione <> "" then
		ls_estensione = lower(ls_estensione)
	else
		wf_log("Attenzione il documento " + string(al_anno_registrazione) + "_" + string(al_num_registrazione) + "_" + string(ll_prog_manutenzioni) + " non ha estensione")
	end if

	selectblob blob
	into :lb_blob
	from det_manutenzioni
	where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione = :al_num_registrazione and
		prog_manutenzioni = :ll_prog_manutenzioni ;
		
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante l'estrazione del blob:"+ string(al_anno_registrazione) + " " + string(al_num_registrazione) + " progressivo: " + string(ll_prog_manutenzioni) 
		return -1
	end if
	
	ls_path = is_folder_root + "\"
	
	if isnull(as_cod_tipo_manutenzione) or as_cod_tipo_manutenzione = "" then
		ls_path += "vuoto"
	else
		 wf_pulisci(as_cod_tipo_manutenzione)
		ls_path += as_cod_tipo_manutenzione
	end if
	wf_crea_directory(ls_path)
	
	ls_path += "\" + string(al_anno_registrazione) + "_" + string(al_num_registrazione) 
	wf_crea_directory(ls_path)
	
	ls_path += "\" + ls_descrizione + "." + ls_estensione
	if guo_functions.uof_blob_to_file(lb_blob, ls_path) < 0 then
		//errore
		as_error = "Errore durante il salvataggio del file " + string(al_anno_registrazione) + "-" + string(al_num_registrazione) + "-" + string(ll_prog_manutenzioni) 
		wf_log(as_error)
		return -1
	else
		wf_log("File Salvato correttamente "+ string(al_anno_registrazione) + "-" + string(al_num_registrazione) + "-" + string(ll_prog_manutenzioni) )
	end if
	 
next

return 0
end function

public function boolean wf_crea_directory (string as_path);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che attraverso il percorso crea la struttura di cartelle.
 **/

if not DirectoryExists(as_path) then
	return CreateDirectory(as_path) = 1
end if

return true
end function

public subroutine wf_log (string as_write);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che genera un log sulla root della cartella selezionata in partenza.
 * Il log contiene: cod_utente, data-ora, file salvato/non ha estensione, num_documento
 **/

long ll_file

ll_file = fileopen(is_folder_root + "\log.txt", linemode!, Write!, LockWrite!, Append!)
if ll_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
end if

if isnull(as_write) then as_write = ""

as_write = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + as_write
filewrite(ll_file, as_write)
fileclose(ll_file)

end subroutine

public subroutine wf_pulisci (ref string as_stringa);/**
 * Giulio
 * 14/02/2012
 *
 * Funzione che sostituisce i caratteri della stringa non validi per Windows con l'underscore.
 **/

guo_functions.uof_replace_string(as_stringa, " ", "_")
guo_functions.uof_replace_string(as_stringa, "\", "_")
guo_functions.uof_replace_string(as_stringa, "/", "_")
guo_functions.uof_replace_string(as_stringa, ":", "_")
guo_functions.uof_replace_string(as_stringa, "?", "_")
guo_functions.uof_replace_string(as_stringa, '"', "_")
guo_functions.uof_replace_string(as_stringa, "<", "_")
guo_functions.uof_replace_string(as_stringa, ">", "_")
guo_functions.uof_replace_string(as_stringa, "|", "_")

end subroutine

on w_estrazione_documenti.create
int iCurrent
call super::create
this.st_1=create st_1
this.hpb_1=create hpb_1
this.cb_start=create cb_start
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.cb_start
end on

on w_estrazione_documenti.destroy
call super::destroy
destroy(this.st_1)
destroy(this.hpb_1)
destroy(this.cb_start)
end on

event open;call super::open;/**
 * Giulio
 * 14/02/2012
 *
 * Funzione realizzata per Eng2K.
 * Scorre tutti i documenti presenti nelle manutenzioni e li salva nel percorso indicato dall'utente.
 * Dopo che l'utente ha scelto il percorso, vengono create le seguenti cartelle: 
 * "cod_tipo_manutenzione"/"anno_manutenzione"_"numero_manutenzione"/"num_documento"
 **/
end event

type st_1 from statictext within w_estrazione_documenti
integer x = 46
integer y = 240
integer width = 1531
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 553648127
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_estrazione_documenti
integer x = 663
integer y = 20
integer width = 914
integer height = 140
unsignedinteger maxposition = 100
integer setstep = 10
end type

type cb_start from commandbutton within w_estrazione_documenti
integer x = 23
integer y = 20
integer width = 549
integer height = 140
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Start"
end type

event clicked;integer li_result
string ls_error

li_result = GetFolder( "Selezionare cartella di destinazione", is_folder_root )

if li_result > 0 then
	
	if g_mb.confirm("Attenzione! I file presenti nella cartella verranno sovrascritti!") then
	
		cb_start.enabled = false
		if wf_leggi_tipo_manutenzione(ls_error) = 0 then
			commit ;
			g_mb.success("Elaborazione completata")
		else
			rollback;
			g_mb.error(ls_error)
		end if
	
	end if
else
	cb_start.enabled = true
	return
end if
end event

