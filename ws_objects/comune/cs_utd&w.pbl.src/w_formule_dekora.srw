﻿$PBExportHeader$w_formule_dekora.srw
forward
global type w_formule_dekora from window
end type
type em_num_righe from editmask within w_formule_dekora
end type
type cb_excel from commandbutton within w_formule_dekora
end type
type cbx_sovrascrivi from checkbox within w_formule_dekora
end type
type sle_filename from singlelineedit within w_formule_dekora
end type
type cb_salva from commandbutton within w_formule_dekora
end type
type cb_log from commandbutton within w_formule_dekora
end type
type sle_log from singlelineedit within w_formule_dekora
end type
type st_counter from statictext within w_formule_dekora
end type
type st_log from statictext within w_formule_dekora
end type
type cb_leggi from commandbutton within w_formule_dekora
end type
type st_tot from statictext within w_formule_dekora
end type
type sle_sfoglia from singlelineedit within w_formule_dekora
end type
type cb_sfoglia from commandbutton within w_formule_dekora
end type
type dw_distinta from datawindow within w_formule_dekora
end type
end forward

global type w_formule_dekora from window
integer width = 4690
integer height = 2892
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
em_num_righe em_num_righe
cb_excel cb_excel
cbx_sovrascrivi cbx_sovrascrivi
sle_filename sle_filename
cb_salva cb_salva
cb_log cb_log
sle_log sle_log
st_counter st_counter
st_log st_log
cb_leggi cb_leggi
st_tot st_tot
sle_sfoglia sle_sfoglia
cb_sfoglia cb_sfoglia
dw_distinta dw_distinta
end type
global w_formule_dekora w_formule_dekora

type variables
uo_excel_listini iuo_excel

long			il_max_row_excel = 300

string			is_cod_versione = "001"
string			is_des_versione = "VERSIONE CORRENTE"
string			is_flag_predefinita = "S"
string			is_cod_prodotto_finale = "TESSUTO"
string			is_path_log = ""
end variables

forward prototypes
public function integer wf_leggi_fogli (string as_path, ref string as_errore)
public subroutine wf_log (string as_valore)
public function integer wf_leggi_lista (string as_foglio, ref string as_errore)
public function integer wf_controlla_prodotto (string as_componente, string as_pf, ref string as_errore)
public function integer wf_assegna_formula (long al_riga, ref string as_errore)
end prototypes

public function integer wf_leggi_fogli (string as_path, ref string as_errore);string				ls_sheets[], ls_foglio, ls_cod_prodotto, ls_err, ls_ok, ls_temp[]
long				ll_tot, ll_i, ll_new, ll_pos
integer			li_ret


dw_distinta.reset()

iuo_excel = create uo_excel_listini

iuo_excel.is_path_file = as_path
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_temp[])
ll_tot = upperbound(ls_temp[])

//inverti l'ordine dei fogli
ll_new = 0
for ll_i= ll_tot to 1 step -1
	ll_new += 1
	ls_sheets[ll_new] = ls_temp[ll_i]
next


if isnull(ll_tot) or ll_tot <= 0 then
	as_errore = "Non esistono fogli di lavoro nel file excel specificato!"
	st_tot.text = "Fogli Totali: 0"
	iuo_excel.uof_close( )
	destroy iuo_excel
	return -1
end if

st_tot.text = "Fogli Totali: " +string(ll_tot)

//leggo solo il primo
for ll_i = 1 to 1
	ls_foglio = ls_sheets[ll_i]
	st_counter.text = string(ll_i)
	
	st_tot.text = "Fogli Totali: " +string(ll_tot) + " leggo foglio " + ls_foglio
	
	
	li_ret = wf_leggi_lista(ls_foglio, as_errore)
	
	if li_ret < 0 then
		g_mb.error(as_errore)
	end if
	

next

st_log.text = "Finito!"

if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if


return 0
end function

public subroutine wf_log (string as_valore);long			ll_file

if is_path_log="" or isnull(is_path_log) then return

ll_file = fileopen(is_path_log, LINEMODE!, WRITE!)
filewrite(ll_file, as_valore)
fileclose(ll_file)

return

end subroutine

public function integer wf_leggi_lista (string as_foglio, ref string as_errore);long				ll_index, ll_new, ll_max
string				ls_valore, ls_cod_componente, ls_B, ls_des_prodotto, ls_flag_ok, ls_cod_prodotto_finito, ls_G, ls_N, ls_MA, ls_SA, ls_SP
dec{4}			ld_valore, ld_qta_ini, ld_costo_std
datetime			ldt_valore
integer			li_ret


ll_max = long(em_num_righe.text)
if isnull(ll_max) then ll_max = 0

ll_index = 2

//A (1)		Prodotto Finito
//B (2)		Componente
//C (3)		Des. Componente
//D (4)		Bianco					B
//E (5)		Grigio						A o G
//F (6)		Nero						N
//G (7)		Met. Argento			MA
//H (8)		Sat. Argento			SA
//I  (9)		Speciale					SP
do while true

	if ll_max <> 0 then
		if ll_index > ll_max then exit
	end if
	
	st_log.text = "Elaborazione riga " + string(ll_index)
	
	ls_flag_ok = "N"
	
	//cerca la cella con PRODOTTO FINITO sulla colonna n° 1 (A)
	iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 1, ls_cod_prodotto_finito, ld_valore, ldt_valore, "S")
	
	if ls_cod_prodotto_finito<>"" and not isnull(ls_cod_prodotto_finito) then
		
		//inserisci riga
		ll_new = dw_distinta.insertrow(0)
		
		//controlla codice prodotto
		li_ret = wf_controlla_prodotto("", ls_cod_prodotto_finito, as_errore)
		if li_ret<0 then
			return -1
			
		elseif li_ret>0 then
			dw_distinta.setitem(ll_new, "errore", as_errore)
			as_errore = ""
			
			dw_distinta.setitem(ll_new, "flag_elaborazione", "X")
		else
			//tutto OK
		end if
		
		dw_distinta.setitem(ll_new, "pf", ls_cod_prodotto_finito)
		
		
		//leggi Componente
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 2, ls_cod_componente, ld_valore, ldt_valore, "S")
		
		//controlla codice componente
		li_ret = wf_controlla_prodotto(ls_cod_componente, ls_cod_prodotto_finito, as_errore)
		if li_ret<0 then
			return -1
			
		elseif li_ret>0 then
			//scrivi nel log e salta la riga
			//***************************************
			dw_distinta.setitem(ll_new, "errore", as_errore)
			as_errore = ""
			
			dw_distinta.setitem(ll_new, "flag_elaborazione", "X")
			
		else
			//tutto OK
		end if
		
		dw_distinta.setitem(ll_new, "mp", ls_cod_componente)
		
		
		
		//leggi Des Componente
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 3, ls_des_prodotto, ld_valore, ldt_valore, "S")
		
		//leggi BIANCO
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 4, ls_B, ld_valore, ldt_valore, "S")
		
		//leggi GRIGIO
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 5, ls_G, ld_valore, ldt_valore, "S")
		
		//leggi NERO
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 6, ls_N, ld_valore, ldt_valore, "S")
		
		//leggi MET.ARGENTO
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 7, ls_MA, ld_valore, ldt_valore, "S")
		
		//leggi SAT.ARGENTO
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 8, ls_SA, ld_valore, ldt_valore, "S")
		
		//leggi SPECIALE
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 9, ls_SP, ld_valore, ldt_valore, "S")
		
		dw_distinta.setitem(ll_new, "des", ls_des_prodotto)
		dw_distinta.setitem(ll_new, "bianco", ls_B)
		dw_distinta.setitem(ll_new, "grigio", ls_G)
		dw_distinta.setitem(ll_new, "nero", ls_N)
		dw_distinta.setitem(ll_new, "met_ag", ls_MA)
		dw_distinta.setitem(ll_new, "sat_ag", ls_SA)
		dw_distinta.setitem(ll_new, "spec", ls_SP)
		
		
		if dw_distinta.getitemstring(ll_new, "flag_elaborazione") = "N" then
			//prova ad assegnare la formula
			li_ret = wf_assegna_formula(ll_new, as_errore)
			
			if li_ret < 0 then
				dw_distinta.setitem(ll_new, "flag_elaborazione", "X")
				dw_distinta.setitem(ll_new, "errore", as_errore)
				as_errore = ""
			end if
			
		end if
		
	else
		//giunto alla fine del file
		return 0
	end if
			
	//passa a cercare su riga successiva
	ll_index = ll_index + 1
		
loop

return 0
end function

public function integer wf_controlla_prodotto (string as_componente, string as_pf, ref string as_errore);long				ll_new


if isnull(as_componente) or as_componente="" then
	//controlla solo il prodotto finito
	select count(*)
	into :ll_new
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_pf;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore controllo PF ("+as_pf+"): "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode = 100 or ll_new = 0 then
		//prodotto finito non presente in tabella anag prodotti
		as_errore = "PF "+as_pf+" non presente in anagrafica prodotti!"
		return 1
	end if
	
else
	//controlla se il componente esiste in anagrafica prodotti
	select count(*)
	into :ll_new
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_componente;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore controllo Componente ("+as_componente+"): "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode = 100 or ll_new = 0 then
		//prodotto finito non presente in tabella anag prodotti
		as_errore = "Prodotto "+as_componente+" non presente in anagrafica prodotti!"
		return 1
	end if
	
	
	//controlla se presente in distinta base del pf
	select count(*)
	into :ll_new
	from distinta
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto_padre=:as_pf and
				cod_prodotto_figlio=:as_componente;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore controllo presenza Componente ("+as_componente+") in distinta base: "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode = 100 or ll_new = 0 then
		//prodotto finito non presente in tabella anag prodotti
		as_errore = "Prodotto "+as_componente+" non presente in distinta base del PF "+as_pf+"!"
		return 1
	end if
end if

return 0

end function

public function integer wf_assegna_formula (long al_riga, ref string as_errore);string				ls_componente, ls_cod_formula
long				ll_pos

//formato codice formula figlio
/*
		NN-XX				dove
		
		NN vale 06 oppure 07 oppure 08 oppure 09 in base alla posizione della lettera colore nel codice componente
		XX vale A oppure G oppure MA a seconda di cosa c'è nella colonna grigio (assumi G se vuota)
*/

ls_componente = dw_distinta.getitemstring(al_riga, "mp")

//0. verifica se c'è almeno una lettera da contemplare nelle colonne colore di excel
if dw_distinta.getitemstring(al_riga, "bianco") <>"" or dw_distinta.getitemstring(al_riga, "grigio")<>"" or &
		dw_distinta.getitemstring(al_riga, "nero") <>"" or dw_distinta.getitemstring(al_riga, "met_ag") <>"" or &
		dw_distinta.getitemstring(al_riga, "sat_ag") <> "" or dw_distinta.getitemstring(al_riga, "spec") <> "" then
		
else
	//lettera colore non trovata in nessuna posizione 6-7-8-9
	as_errore = "Nessuna casisistica colore impostata per il componente " + ls_componente
	return -1
end if

//1. cerco di capire la posizione della lettera del colore nel componente
ll_pos = pos(ls_componente, "B", 6)

//se non hai trovato niente con B prova con N
if ll_pos >= 6 then
else
	ll_pos = pos(ls_componente, "N", 6)
	
	//se non hai trovato niente con B prova con G
	if ll_pos >= 6 then
	else
		ll_pos = pos(ls_componente, "G", 6)
		
		if ll_pos >= 6 then
			//trovato con G
		else
			//lettera colore non trovata in nessuna posizione 6-7-8-9
			as_errore = "Posizione lettera colore non trovata nel componente " + ls_componente
			return -1
		end if
	end if
end if

//Se arrivi fin in ll_pos c'è la posizione della lettera del colore
ls_cod_formula = right("00" + string(ll_pos), 2)

//2. vedo che lettera è usata per il caso GRIGIO  e compongo il codice della formula per il figlio
choose case dw_distinta.getitemstring(al_riga, "grigio")
	case "A"
		ls_cod_formula += "-A"
	case "MA"
		ls_cod_formula += "MA"
	case else
		//G
		ls_cod_formula += "-G"
end choose

dw_distinta.setitem(al_riga, "cod_formula_figlio", ls_cod_formula)

return 0
end function

on w_formule_dekora.create
this.em_num_righe=create em_num_righe
this.cb_excel=create cb_excel
this.cbx_sovrascrivi=create cbx_sovrascrivi
this.sle_filename=create sle_filename
this.cb_salva=create cb_salva
this.cb_log=create cb_log
this.sle_log=create sle_log
this.st_counter=create st_counter
this.st_log=create st_log
this.cb_leggi=create cb_leggi
this.st_tot=create st_tot
this.sle_sfoglia=create sle_sfoglia
this.cb_sfoglia=create cb_sfoglia
this.dw_distinta=create dw_distinta
this.Control[]={this.em_num_righe,&
this.cb_excel,&
this.cbx_sovrascrivi,&
this.sle_filename,&
this.cb_salva,&
this.cb_log,&
this.sle_log,&
this.st_counter,&
this.st_log,&
this.cb_leggi,&
this.st_tot,&
this.sle_sfoglia,&
this.cb_sfoglia,&
this.dw_distinta}
end on

on w_formule_dekora.destroy
destroy(this.em_num_righe)
destroy(this.cb_excel)
destroy(this.cbx_sovrascrivi)
destroy(this.sle_filename)
destroy(this.cb_salva)
destroy(this.cb_log)
destroy(this.sle_log)
destroy(this.st_counter)
destroy(this.st_log)
destroy(this.cb_leggi)
destroy(this.st_tot)
destroy(this.sle_sfoglia)
destroy(this.cb_sfoglia)
destroy(this.dw_distinta)
end on

event close;
if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if
end event

type em_num_righe from editmask within w_formule_dekora
integer x = 4082
integer y = 632
integer width = 343
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####0"
boolean spin = true
double increment = 1
string minmax = "2~~5000"
end type

type cb_excel from commandbutton within w_formule_dekora
integer x = 4059
integer y = 444
integer width = 343
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Excel"
end type

event clicked;if dw_distinta.saveas("", Excel!, true) < 0 then
	g_mb.error("Errore in esportazione excel")
	
	return
end if

g_mb.success("Esportazione effettuata con successo!")
end event

type cbx_sovrascrivi from checkbox within w_formule_dekora
integer x = 3552
integer y = 304
integer width = 654
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sovrascrivi esistenti:"
boolean lefttext = true
end type

type sle_filename from singlelineedit within w_formule_dekora
integer x = 1449
integer y = 284
integer width = 2048
integer height = 100
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_salva from commandbutton within w_formule_dekora
integer x = 4329
integer y = 292
integer width = 279
integer height = 92
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long				ll_tot, ll_index
string				ls_pf, ls_mp, ls_errore, ls_cod_formula_figlio
integer			li_ret

st_log.text = ""


dw_distinta.accepttext()

ll_tot = dw_distinta.rowcount()


if not g_mb.confirm("Procedo con il salvataggio delle formule?") then return

dw_distinta.setredraw(false)

for ll_index=1 to ll_tot	
	if dw_distinta.getitemstring(ll_index, "flag_elaborazione") = "N" then
		
		ls_pf = dw_distinta.getitemstring(ll_index, "pf")
		ls_mp = dw_distinta.getitemstring(ll_index, "mp")
		ls_cod_formula_figlio = dw_distinta.getitemstring(ll_index, "cod_formula_figlio")
		
		st_log.text = "Riga " + string(ll_index) + ": Aggiornamento PF "+ls_pf+"  COMPONENTE "+ls_mp + " FORMULA "+ls_cod_formula_figlio
		
		update distinta
		set cod_formula_prod_figlio = :ls_cod_formula_figlio
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_pf and
					cod_prodotto_figlio = :ls_mp;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore update :" + sqlca.sqlerrtext)
			g_mb.error("PF "+ls_pf+"  COMPONENTE "+ls_mp+" FORMULA "+ls_cod_formula_figlio+" (riga "+string(ll_index) + ")")
			rollback;
			dw_distinta.setredraw(true)
			return
			
		else
			dw_distinta.setitem(ll_index, "flag_elaborazione", "O")
		end if

	end if
next

dw_distinta.setredraw(true)

commit;

g_mb.success("Operazione terminata!")
st_log.text = "Fine!"

return
end event

type cb_log from commandbutton within w_formule_dekora
integer x = 4096
integer y = 160
integer width = 402
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Log"
end type

event clicked;



is_path_log = sle_log.text


//wf_log("~r~n~r~n"+string(today(), "dd-mm-yyyy")+" "+string(now(), "hh:mm_ss")+" INIZIO PROCEDURA IMPORTAZIONE GIACENZE INIZIALI")

g_mb.success("FILE DI LOG IMPOSTATO CORRETTAMENTE!")
end event

type sle_log from singlelineedit within w_formule_dekora
integer x = 69
integer y = 156
integer width = 4000
integer height = 112
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_counter from statictext within w_formule_dekora
integer x = 1097
integer y = 296
integer width = 320
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_log from statictext within w_formule_dekora
integer x = 73
integer y = 2428
integer width = 3954
integer height = 324
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_leggi from commandbutton within w_formule_dekora
integer x = 4229
integer y = 44
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi"
end type

event clicked;string			ls_file, ls_errore
long			ll_pos



ls_file = sle_sfoglia.text

ll_pos = lastpos(sle_sfoglia.text, "\")
sle_filename.text = right(ls_file, len(sle_sfoglia.text) - ll_pos)

if wf_leggi_fogli(ls_file, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return
end if

g_mb.success("Lettura effettuata!")
end event

type st_tot from statictext within w_formule_dekora
integer x = 37
integer y = 296
integer width = 1019
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Fogli Totali:"
boolean focusrectangle = false
end type

type sle_sfoglia from singlelineedit within w_formule_dekora
integer x = 69
integer y = 36
integer width = 4000
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "E:\Clienti\DEKORA\FORMULE_DISTINTA\Dekora Formule_per_distinta_cambio_colore.xls"
borderstyle borderstyle = stylelowered!
end type

type cb_sfoglia from commandbutton within w_formule_dekora
integer x = 4078
integer y = 36
integer width = 133
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[], ls_errore
integer	li_count, li_ret

ls_path = s_cs_xx.volume + "\DEKORA\"
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	sle_sfoglia.text = docpath
end if
end event

type dw_distinta from datawindow within w_formule_dekora
integer x = 64
integer y = 412
integer width = 3954
integer height = 2000
integer taborder = 10
string title = "none"
string dataobject = "d_formule_dekora"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

