﻿$PBExportHeader$w_gestione_importazioni.srw
$PBExportComments$Maschera impostazione parametri importazione
forward
global type w_gestione_importazioni from window
end type
type st_11 from statictext within w_gestione_importazioni
end type
type st_10 from statictext within w_gestione_importazioni
end type
type st_9 from statictext within w_gestione_importazioni
end type
type st_8 from statictext within w_gestione_importazioni
end type
type st_7 from statictext within w_gestione_importazioni
end type
type st_6 from statictext within w_gestione_importazioni
end type
type st_volume from statictext within w_gestione_importazioni
end type
type st_4 from statictext within w_gestione_importazioni
end type
type cbx_ordini_ven from checkbox within w_gestione_importazioni
end type
type sle_ordini_ven from singlelineedit within w_gestione_importazioni
end type
type sle_bolle_acq from singlelineedit within w_gestione_importazioni
end type
type sle_prodotti from singlelineedit within w_gestione_importazioni
end type
type sle_clienti from singlelineedit within w_gestione_importazioni
end type
type sle_fornitori from singlelineedit within w_gestione_importazioni
end type
type cb_chiudi from commandbutton within w_gestione_importazioni
end type
type cb_esegui from commandbutton within w_gestione_importazioni
end type
type cb_ok from commandbutton within w_gestione_importazioni
end type
type cb_annulla from commandbutton within w_gestione_importazioni
end type
type cbx_bolle_acq from checkbox within w_gestione_importazioni
end type
type cbx_prodotti from checkbox within w_gestione_importazioni
end type
type cbx_clienti from checkbox within w_gestione_importazioni
end type
type cbx_fornitori from checkbox within w_gestione_importazioni
end type
type st_3 from statictext within w_gestione_importazioni
end type
type rb_manuale from radiobutton within w_gestione_importazioni
end type
type rb_automatica from radiobutton within w_gestione_importazioni
end type
type st_2 from statictext within w_gestione_importazioni
end type
type sle_path from singlelineedit within w_gestione_importazioni
end type
type st_1 from statictext within w_gestione_importazioni
end type
type r_1 from rectangle within w_gestione_importazioni
end type
type r_2 from rectangle within w_gestione_importazioni
end type
type r_3 from rectangle within w_gestione_importazioni
end type
end forward

global type w_gestione_importazioni from window
integer width = 1490
integer height = 2016
boolean titlebar = true
string title = "Gestione importazione dati"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
long backcolor = 67108864
boolean clientedge = true
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_volume st_volume
st_4 st_4
cbx_ordini_ven cbx_ordini_ven
sle_ordini_ven sle_ordini_ven
sle_bolle_acq sle_bolle_acq
sle_prodotti sle_prodotti
sle_clienti sle_clienti
sle_fornitori sle_fornitori
cb_chiudi cb_chiudi
cb_esegui cb_esegui
cb_ok cb_ok
cb_annulla cb_annulla
cbx_bolle_acq cbx_bolle_acq
cbx_prodotti cbx_prodotti
cbx_clienti cbx_clienti
cbx_fornitori cbx_fornitori
st_3 st_3
rb_manuale rb_manuale
rb_automatica rb_automatica
st_2 st_2
sle_path sle_path
st_1 st_1
r_1 r_1
r_2 r_2
r_3 r_3
end type
global w_gestione_importazioni w_gestione_importazioni

type variables
boolean ib_mod, ib_salva

string  is_path, is_modalita, is_file_fornitori, is_file_clienti, is_file_prodotti, &
		  is_file_bolle_acq, is_file_ordini_ven, is_flag_fornitori, is_flag_clienti, is_flag_prodotti, &
		  is_flag_bolle_acq, is_flag_ordini_ven
end variables

on w_gestione_importazioni.create
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_volume=create st_volume
this.st_4=create st_4
this.cbx_ordini_ven=create cbx_ordini_ven
this.sle_ordini_ven=create sle_ordini_ven
this.sle_bolle_acq=create sle_bolle_acq
this.sle_prodotti=create sle_prodotti
this.sle_clienti=create sle_clienti
this.sle_fornitori=create sle_fornitori
this.cb_chiudi=create cb_chiudi
this.cb_esegui=create cb_esegui
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.cbx_bolle_acq=create cbx_bolle_acq
this.cbx_prodotti=create cbx_prodotti
this.cbx_clienti=create cbx_clienti
this.cbx_fornitori=create cbx_fornitori
this.st_3=create st_3
this.rb_manuale=create rb_manuale
this.rb_automatica=create rb_automatica
this.st_2=create st_2
this.sle_path=create sle_path
this.st_1=create st_1
this.r_1=create r_1
this.r_2=create r_2
this.r_3=create r_3
this.Control[]={this.st_11,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_6,&
this.st_volume,&
this.st_4,&
this.cbx_ordini_ven,&
this.sle_ordini_ven,&
this.sle_bolle_acq,&
this.sle_prodotti,&
this.sle_clienti,&
this.sle_fornitori,&
this.cb_chiudi,&
this.cb_esegui,&
this.cb_ok,&
this.cb_annulla,&
this.cbx_bolle_acq,&
this.cbx_prodotti,&
this.cbx_clienti,&
this.cbx_fornitori,&
this.st_3,&
this.rb_manuale,&
this.rb_automatica,&
this.st_2,&
this.sle_path,&
this.st_1,&
this.r_1,&
this.r_2,&
this.r_3}
end on

on w_gestione_importazioni.destroy
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_volume)
destroy(this.st_4)
destroy(this.cbx_ordini_ven)
destroy(this.sle_ordini_ven)
destroy(this.sle_bolle_acq)
destroy(this.sle_prodotti)
destroy(this.sle_clienti)
destroy(this.sle_fornitori)
destroy(this.cb_chiudi)
destroy(this.cb_esegui)
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.cbx_bolle_acq)
destroy(this.cbx_prodotti)
destroy(this.cbx_clienti)
destroy(this.cbx_fornitori)
destroy(this.st_3)
destroy(this.rb_manuale)
destroy(this.rb_automatica)
destroy(this.st_2)
destroy(this.sle_path)
destroy(this.st_1)
destroy(this.r_1)
destroy(this.r_2)
destroy(this.r_3)
end on

event closequery;if ib_mod then
	if g_mb.messagebox("Conferma chiusura","Chiudere ignorando le modifiche?",question!,yesno!,2) = 2 then
		return 1
	end if
end if
end event

event open;string ls_volume


ib_mod = false

if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume) = -1 then
	g_mb.messagebox("Gestione importazioni","Errore in lettura parametro volume dal registro di sistema")
	close(this)
	return -1
end if

st_volume.text = ls_volume

if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "pfi", is_path) = -1 then
	is_path = ""
end if

sle_path.text = is_path

if registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "fmi", is_modalita) = -1 then
	is_modalita = ""
end if

choose case is_modalita
	case "A"
		rb_automatica.checked = true
		rb_manuale.checked = false
		cb_esegui.enabled = false
	case "M"
		rb_automatica.checked = false
		rb_manuale.checked = true
		cb_esegui.enabled = true
end choose

select file_fornitori,
       file_clienti,
		 file_prodotti,
		 file_bolle_acq,
		 file_ordini_vendita,
		 flag_fornitori,
		 flag_clienti,
		 flag_prodotti,
		 flag_bolle_acq,
		 flag_ordini_vendita
into   :is_file_fornitori,
       :is_file_clienti,
		 :is_file_prodotti,
		 :is_file_bolle_acq,
		 :is_file_ordini_ven,
		 :is_flag_fornitori,
		 :is_flag_clienti,
		 :is_flag_prodotti,
		 :is_flag_bolle_acq,
		 :is_flag_ordini_ven
from   import_omnia
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Gestione importazioni","Errore nella select di import_omnia: " + sqlca.sqlerrtext)
	close(this)
	return -1
elseif sqlca.sqlcode = 100 then
	is_file_fornitori = ""
   is_file_clienti = ""
	is_file_prodotti = ""
	is_file_bolle_acq = ""
	is_file_ordini_ven = ""
	is_flag_fornitori = "N"
	is_flag_clienti = "N"
	is_flag_prodotti = "N"
	is_flag_bolle_acq = "N"
	is_flag_ordini_ven = "N"
end if

sle_fornitori.text = is_file_fornitori
sle_clienti.text = is_file_clienti
sle_prodotti.text = is_file_prodotti
sle_bolle_acq.text = is_file_bolle_acq
sle_ordini_ven.text = is_file_ordini_ven

if is_flag_fornitori = 'S' then
	cbx_fornitori.checked = true
end if

if is_flag_clienti = 'S' then
	cbx_clienti.checked = true
end if

if is_flag_prodotti = 'S' then
	cbx_prodotti.checked = true
end if

if is_flag_bolle_acq = 'S' then
	cbx_bolle_acq.checked = true
end if

if is_flag_ordini_ven = 'S' then
	cbx_ordini_ven.checked = true
end if
end event

type st_11 from statictext within w_gestione_importazioni
integer x = 69
integer y = 712
integer width = 366
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Ordini vendita:"
boolean focusrectangle = false
end type

type st_10 from statictext within w_gestione_importazioni
integer x = 69
integer y = 612
integer width = 389
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Bolle acquisto:"
boolean focusrectangle = false
end type

type st_9 from statictext within w_gestione_importazioni
integer x = 69
integer y = 512
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Prodotti:"
boolean focusrectangle = false
end type

type st_8 from statictext within w_gestione_importazioni
integer x = 69
integer y = 412
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Clienti:"
boolean focusrectangle = false
end type

type st_7 from statictext within w_gestione_importazioni
integer x = 69
integer y = 312
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Fornitori:"
boolean focusrectangle = false
end type

type st_6 from statictext within w_gestione_importazioni
integer x = 69
integer y = 212
integer width = 338
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Percorso file:"
boolean focusrectangle = false
end type

type st_volume from statictext within w_gestione_importazioni
integer x = 457
integer y = 100
integer width = 914
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_gestione_importazioni
integer x = 69
integer y = 112
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Volume:"
boolean focusrectangle = false
end type

type cbx_ordini_ven from checkbox within w_gestione_importazioni
integer x = 69
integer y = 1340
integer width = 1298
integer height = 76
integer taborder = 110
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Importazione dei dati dagli ordini di vendita"
end type

event clicked;ib_mod = true

if checked then
	is_flag_ordini_ven = "S"
else
	is_flag_ordini_ven = "N"
end if
end event

type sle_ordini_ven from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 700
integer width = 914
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_file_ordini_ven = text
end event

type sle_bolle_acq from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 600
integer width = 914
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_file_bolle_acq = text
end event

type sle_prodotti from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 500
integer width = 914
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_file_prodotti = text
end event

type sle_clienti from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 400
integer width = 914
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_file_clienti = text
end event

type sle_fornitori from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 300
integer width = 914
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_file_fornitori = text
end event

type cb_chiudi from commandbutton within w_gestione_importazioni
integer x = 229
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type cb_esegui from commandbutton within w_gestione_importazioni
integer x = 1006
integer y = 1648
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_messaggio

uo_importazione luo_importa


ib_salva = true

cb_ok.triggerevent("clicked")

luo_importa = create uo_importazione

setpointer(hourglass!)

if luo_importa.uof_importa_dati(ls_messaggio) = -1 then
	g_mb.messagebox("Importazione dati",ls_messaggio,stopsign!)
else
	g_mb.messagebox("Importazione dati","Importazione completata con successo")
end if

setpointer(arrow!)

destroy luo_importa

ib_salva = false
end event

type cb_ok from commandbutton within w_gestione_importazioni
integer x = 1051
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;if registryset(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "pfi", regstring!, is_path) = -1 then
	g_mb.messagebox("Gestione importazioni","Errore in aggiornamento parametro PFI nel registro di sistema")
	return -1
end if

if registryset(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "fmi", regstring!, is_modalita) = -1 then
	g_mb.messagebox("Gestione importazioni","Errore in aggiornamento parametro FMI nel registro di sistema")
	return -1
end if

delete from import_omnia;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Gestione importazioni","Errore nella delete di import_omnia: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

insert
into   import_omnia
		 (cod_azienda,
		 file_fornitori,
		 file_clienti,
		 file_prodotti,
		 file_bolle_acq,
		 file_ordini_vendita,
		 flag_fornitori,
		 flag_clienti,
		 flag_prodotti,
		 flag_bolle_acq,
		 flag_ordini_vendita)
values (:s_cs_xx.cod_azienda,
		 :is_file_fornitori,
		 :is_file_clienti,
		 :is_file_prodotti,
		 :is_file_bolle_acq,
		 :is_file_ordini_ven,
		 :is_flag_fornitori,
		 :is_flag_clienti,
		 :is_flag_prodotti,
		 :is_flag_bolle_acq,
		 :is_flag_ordini_ven);
			 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Gestione importazioni","Errore nella insert di import_omnia: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;

ib_mod = false

if ib_salva then
	return 0
end if	

close(parent)
end event

type cb_annulla from commandbutton within w_gestione_importazioni
integer x = 640
integer y = 1800
integer width = 366
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;if g_mb.messagebox("Conferma annullamento","Tornare alle impostazioni di default?",question!,yesno!,2) = 2 then
	return -1
end if

sle_path.text = ""
sle_fornitori.text = ""
sle_clienti.text = ""
sle_prodotti.text = ""
sle_bolle_acq.text = ""
sle_ordini_ven.text = ""

is_file_fornitori = ""
is_file_clienti = ""
is_file_prodotti = ""
is_file_bolle_acq = ""
is_file_ordini_ven = ""

cbx_fornitori.checked = false
cbx_clienti.checked = false
cbx_prodotti.checked = false
cbx_bolle_acq.checked = false
cbx_ordini_ven.checked = false

is_flag_fornitori = "N"
is_flag_clienti = "N"
is_flag_prodotti = "N"
is_flag_bolle_acq = "N"
is_flag_ordini_ven = "N"

rb_automatica.checked = false
rb_manuale.checked = true

is_modalita = "M"

cb_esegui.enabled = true
end event

type cbx_bolle_acq from checkbox within w_gestione_importazioni
integer x = 69
integer y = 1240
integer width = 1298
integer height = 76
integer taborder = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Importazione dei dati dalle bolle di acquisto"
end type

event clicked;ib_mod = true

if checked then
	is_flag_bolle_acq = "S"
else
	is_flag_bolle_acq = "N"
end if
end event

type cbx_prodotti from checkbox within w_gestione_importazioni
integer x = 69
integer y = 1140
integer width = 1298
integer height = 76
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Importazione dei dati dalle anagrafiche prodotti"
end type

event clicked;ib_mod = true

if checked then
	is_flag_prodotti = "S"
else
	is_flag_prodotti = "N"
end if
end event

type cbx_clienti from checkbox within w_gestione_importazioni
integer x = 69
integer y = 1040
integer width = 1298
integer height = 76
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Importazione dei dati dalle anagrafiche clienti"
end type

event clicked;ib_mod = true

if checked then
	is_flag_clienti = "S"
else
	is_flag_clienti = "N"
end if
end event

type cbx_fornitori from checkbox within w_gestione_importazioni
integer x = 69
integer y = 940
integer width = 1298
integer height = 76
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Importazione dei dati dalle anagrafiche fornitori"
end type

event clicked;ib_mod = true

if checked then
	is_flag_fornitori = "S"
else
	is_flag_fornitori = "N"
end if
end event

type st_3 from statictext within w_gestione_importazioni
integer x = 46
integer y = 860
integer width = 617
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Operazioni da eseguire"
boolean focusrectangle = false
end type

type rb_manuale from radiobutton within w_gestione_importazioni
integer x = 69
integer y = 1660
integer width = 827
integer height = 60
integer taborder = 130
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Manuale da parte dell~'utente:"
boolean checked = true
end type

event clicked;ib_mod = true

cb_esegui.enabled = true

is_modalita = "M"
end event

type rb_automatica from radiobutton within w_gestione_importazioni
integer x = 69
integer y = 1560
integer width = 1303
integer height = 60
integer taborder = 120
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Automatica all~'avvio del programma"
end type

event clicked;ib_mod = true

cb_esegui.enabled = false

is_modalita = "A"

end event

type st_2 from statictext within w_gestione_importazioni
integer x = 46
integer y = 1480
integer width = 603
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Modalità di esecuzione"
boolean focusrectangle = false
end type

type sle_path from singlelineedit within w_gestione_importazioni
integer x = 457
integer y = 200
integer width = 914
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
end type

event modified;ib_mod = true

is_path = text
end event

type st_1 from statictext within w_gestione_importazioni
integer x = 46
integer y = 20
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "File origine"
boolean focusrectangle = false
end type

type r_1 from rectangle within w_gestione_importazioni
integer linethickness = 1
long fillcolor = 79741120
integer x = 23
integer y = 60
integer width = 1394
integer height = 760
end type

type r_2 from rectangle within w_gestione_importazioni
integer linethickness = 1
long fillcolor = 79741120
integer x = 23
integer y = 1520
integer width = 1394
integer height = 240
end type

type r_3 from rectangle within w_gestione_importazioni
integer linethickness = 1
long fillcolor = 79741120
integer x = 23
integer y = 900
integer width = 1394
integer height = 540
end type

