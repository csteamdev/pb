﻿$PBExportHeader$w_giacenza_errata.srw
$PBExportComments$Window elenco prodotti con giacenza errata
forward
global type w_giacenza_errata from w_cs_xx_principale
end type
type dw_elenco_prodotti_giacenza_errore from uo_cs_xx_dw within w_giacenza_errata
end type
type cb_verifica from commandbutton within w_giacenza_errata
end type
type st_1 from statictext within w_giacenza_errata
end type
type st_2 from statictext within w_giacenza_errata
end type
end forward

global type w_giacenza_errata from w_cs_xx_principale
int Width=3329
int Height=1669
boolean TitleBar=true
string Title="Allineamento Magazzino"
dw_elenco_prodotti_giacenza_errore dw_elenco_prodotti_giacenza_errore
cb_verifica cb_verifica
st_1 st_1
st_2 st_2
end type
global w_giacenza_errata w_giacenza_errata

on w_giacenza_errata.create
int iCurrent
call w_cs_xx_principale::create
this.dw_elenco_prodotti_giacenza_errore=create dw_elenco_prodotti_giacenza_errore
this.cb_verifica=create cb_verifica
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_elenco_prodotti_giacenza_errore
this.Control[iCurrent+2]=cb_verifica
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=st_2
end on

on w_giacenza_errata.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_elenco_prodotti_giacenza_errore)
destroy(this.cb_verifica)
destroy(this.st_1)
destroy(this.st_2)
end on

event pc_setwindow;call super::pc_setwindow;dw_elenco_prodotti_giacenza_errore.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodeblack)
end event

type dw_elenco_prodotti_giacenza_errore from uo_cs_xx_dw within w_giacenza_errata
int X=23
int Y=101
int Width=3246
int Height=1361
int TabOrder=10
string DataObject="d_elenco_prodotti_giacenza_errore"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event doubleclicked;call super::doubleclicked;if i_extendmode then
	s_cs_xx.parametri.parametro_s_1 = dw_elenco_prodotti_giacenza_errore.getitemstring(dw_elenco_prodotti_giacenza_errore.getrow(),"cod_prodotto")
	s_cs_xx.parametri.parametro_s_2 = dw_elenco_prodotti_giacenza_errore.getitemstring(dw_elenco_prodotti_giacenza_errore.getrow(),"des_prodotto")
	s_cs_xx.parametri.parametro_d_1 = dw_elenco_prodotti_giacenza_errore.getitemnumber(dw_elenco_prodotti_giacenza_errore.getrow(),"giacenza")
	s_cs_xx.parametri.parametro_d_2 = dw_elenco_prodotti_giacenza_errore.getitemnumber(dw_elenco_prodotti_giacenza_errore.getrow(),"quan_assegnata")
	s_cs_xx.parametri.parametro_d_3 = dw_elenco_prodotti_giacenza_errore.getitemnumber(dw_elenco_prodotti_giacenza_errore.getrow(),"quan_in_spedizione")
	window_open(w_stock_giacenza_errata,-1)
end if
end event

type cb_verifica from commandbutton within w_giacenza_errata
int X=2743
int Y=1481
int Width=526
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&Verifica Giacenza"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto,ls_inizio_codice,ls_des_prodotto
double ldd_quan_assegnata,ldd_quan_in_spedizione, ldd_saldo_quan_inizio_anno,&
		 ldd_prog_quan_entrata, ldd_prog_quan_uscita,ldd_giacenza
long   ll_num_righe,ll_num_prodotti

setpointer(hourglass!)

dw_elenco_prodotti_giacenza_errore.reset()

declare righe_p cursor for
select  cod_prodotto,
		  des_prodotto,
		  saldo_quan_inizio_anno,	
		  prog_quan_entrata,
		  prog_quan_uscita,
		  quan_assegnata,
		  quan_in_spedizione
from    anag_prodotti
where   cod_azienda=:s_cs_xx.cod_azienda
and     saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita < quan_assegnata + quan_in_spedizione;

open righe_p;

do while 1=1
	fetch righe_p
	into  :ls_cod_prodotto,
			:ls_des_prodotto,
			:ldd_saldo_quan_inizio_anno,
			:ldd_prog_quan_entrata,
			:ldd_prog_quan_uscita,
			:ldd_quan_assegnata,
			:ldd_quan_in_spedizione;
			
	if sqlca.sqlcode=100 then exit
		
	ll_num_righe++

	ldd_giacenza = ldd_saldo_quan_inizio_anno + ldd_prog_quan_entrata - ldd_prog_quan_uscita 
	dw_elenco_prodotti_giacenza_errore.insertrow(ll_num_righe)
	dw_elenco_prodotti_giacenza_errore.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto)
	dw_elenco_prodotti_giacenza_errore.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
	dw_elenco_prodotti_giacenza_errore.setitem(ll_num_righe,"giacenza",ldd_giacenza)
	dw_elenco_prodotti_giacenza_errore.setitem(ll_num_righe,"quan_assegnata",ldd_quan_assegnata)
	dw_elenco_prodotti_giacenza_errore.setitem(ll_num_righe,"quan_in_spedizione",ldd_quan_in_spedizione)

loop

close righe_p;

setpointer(arrow!)
		
dw_elenco_prodotti_giacenza_errore.resetupdate()			
dw_elenco_prodotti_giacenza_errore.TriggerEvent("pcd_search")
end event

type st_1 from statictext within w_giacenza_errata
int X=23
int Y=21
int Width=961
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Elenco prodotti con giacenza errata:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_giacenza_errata
int X=23
int Y=1481
int Width=1555
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Per visualizzare gli stock: doppio click sul prodotto desiderato"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

