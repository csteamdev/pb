﻿$PBExportHeader$w_gruppo_gibus.srw
forward
global type w_gruppo_gibus from w_std_principale
end type
type st_9 from statictext within w_gruppo_gibus
end type
type cb_linee_pd from commandbutton within w_gruppo_gibus
end type
type st_rep_sfuso from statictext within w_gruppo_gibus
end type
type cb_rep_sfuso from commandbutton within w_gruppo_gibus
end type
type st_giri from statictext within w_gruppo_gibus
end type
type cb_giri from commandbutton within w_gruppo_gibus
end type
type st_prodotti_linee from statictext within w_gruppo_gibus
end type
type cb_prodotti_linee from commandbutton within w_gruppo_gibus
end type
type st_8 from statictext within w_gruppo_gibus
end type
type cb_clienti_viropa from commandbutton within w_gruppo_gibus
end type
type st_7 from statictext within w_gruppo_gibus
end type
type cb_clienti_centro from commandbutton within w_gruppo_gibus
end type
type st_6 from statictext within w_gruppo_gibus
end type
type st_5 from statictext within w_gruppo_gibus
end type
type cb_9 from commandbutton within w_gruppo_gibus
end type
type st_4 from statictext within w_gruppo_gibus
end type
type cb_8 from commandbutton within w_gruppo_gibus
end type
type st_3 from statictext within w_gruppo_gibus
end type
type cb_7 from commandbutton within w_gruppo_gibus
end type
type st_2 from statictext within w_gruppo_gibus
end type
type cb_6 from commandbutton within w_gruppo_gibus
end type
type cb_5 from commandbutton within w_gruppo_gibus
end type
type st_mc from statictext within w_gruppo_gibus
end type
type st_vr from statictext within w_gruppo_gibus
end type
type st_cg from statictext within w_gruppo_gibus
end type
type st_pt from statictext within w_gruppo_gibus
end type
type cb_4 from commandbutton within w_gruppo_gibus
end type
type st_1 from statictext within w_gruppo_gibus
end type
type cb_3 from commandbutton within w_gruppo_gibus
end type
type cb_2 from commandbutton within w_gruppo_gibus
end type
type st_file from statictext within w_gruppo_gibus
end type
type cb_clienti_pd from commandbutton within w_gruppo_gibus
end type
end forward

global type w_gruppo_gibus from w_std_principale
integer width = 3214
integer height = 2964
st_9 st_9
cb_linee_pd cb_linee_pd
st_rep_sfuso st_rep_sfuso
cb_rep_sfuso cb_rep_sfuso
st_giri st_giri
cb_giri cb_giri
st_prodotti_linee st_prodotti_linee
cb_prodotti_linee cb_prodotti_linee
st_8 st_8
cb_clienti_viropa cb_clienti_viropa
st_7 st_7
cb_clienti_centro cb_clienti_centro
st_6 st_6
st_5 st_5
cb_9 cb_9
st_4 st_4
cb_8 cb_8
st_3 st_3
cb_7 cb_7
st_2 st_2
cb_6 cb_6
cb_5 cb_5
st_mc st_mc
st_vr st_vr
st_cg st_cg
st_pt st_pt
cb_4 cb_4
st_1 st_1
cb_3 cb_3
cb_2 cb_2
st_file st_file
cb_clienti_pd cb_clienti_pd
end type
global w_gruppo_gibus w_gruppo_gibus

type variables
OLEObject iole_excel
string is_path_file

transaction it_sqlc_pt, it_sqlc_cg, it_sqlc_vr, it_sqlc_mc
end variables

forward prototypes
public function string wf_nome_cella (long fl_riga, long fl_colonna)
public subroutine wf_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo)
public function integer wf_get_banca_appoggio (string fs_cod_abi, string fs_cod_cab, string fs_cod_cin, string fs_cod_iban, string fs_cod_banca_clien_for, ref string fs_cod_abi_new, ref string fs_cod_cab_new, ref string fs_cod_cin_new, ref string fs_cod_banca_clien_for_new, ref string fs_errore)
end prototypes

public function string wf_nome_cella (long fl_riga, long fl_colonna);string ls_char, ls_caratteri[]
long ll_resto, ll_i

ls_char = ""

ls_caratteri[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}

if fl_colonna > 26 then
	ll_resto = mod(fl_colonna, 26)
	ll_i = long(fl_colonna / 26)
	if ll_resto = 0 then
		ll_resto = 26
		ll_i = ll_i - 1
	end if
	ls_char = ls_char + ls_caratteri[ll_i]
	fl_colonna = ll_resto 
end if

if fl_colonna <= 26 then
 ls_char = ls_char + ls_caratteri[fl_colonna] + string(fl_riga)
end if	

return ls_char
end function

public subroutine wf_get_value (string fs_foglio, long fl_riga, long fl_colonna, ref string fs_valore, ref decimal fd_valore, ref string fs_tipo);OLEObject		lole_foglio
any				lany_ret
string ls_1

//legge dalla cella (fl_riga, fl_colonna) del foglio di lavoro fs_foglio
/*
prova prima con valore decimale: 	se ok torna il valore in fd_valore e mette fs_tipo="D"
poi prova con valore string:				se ok torna il valore in fs_valore e mette fs_tipo="S"

se proprio non riesce a leggere mette a fs_tipo=NULL e ritorna
*/

fs_tipo = ""
setnull(fd_valore)
setnull(fs_valore)

lole_foglio = iole_excel.Application.ActiveWorkbook.Worksheets(fs_foglio)
ls_1 = wf_nome_cella(fl_riga, fl_colonna)

//lany_ret = lole_foglio.range(wf_nome_cella(fl_riga, fl_colonna) + ":" + wf_nome_cella(fl_riga , fl_colonna) ).value
lany_ret = lole_foglio.cells[fl_riga,fl_colonna].value

if isnull(lany_ret) then 
	fs_tipo = "S"
	setnull(fs_valore)
	return
end if

try
	//prova con valore stringa
	fs_valore = lany_ret
	fs_tipo = "S"
catch (RuntimeError rte2)
	setnull(fs_valore)
	setnull(fs_tipo)
end try


//try
//	//prova con valore decimale
//	fd_valore = lany_ret
//	fs_tipo = "D"
//catch (RuntimeError rte1)
//	
//	setnull(fd_valore)
//	setnull(fs_tipo)
//	try
//		//prova con valore stringa
//		fs_valore = lany_ret
//		fs_tipo = "S"
//	catch (RuntimeError rte2)
//		setnull(fs_valore)
//		setnull(fs_tipo)
//	end try
//	
//end try
end subroutine

public function integer wf_get_banca_appoggio (string fs_cod_abi, string fs_cod_cab, string fs_cod_cin, string fs_cod_iban, string fs_cod_banca_clien_for, ref string fs_cod_abi_new, ref string fs_cod_cab_new, ref string fs_cod_cin_new, ref string fs_cod_banca_clien_for_new, ref string fs_errore);//restitiusce il cod_banca_clien_for corrispondnete ad un abi e ad un cab

//TORNA 			-1 		se errore (in fs_msg)
//						 1		se OK e in fs_cod_banca_clien_for il relativo valore con cui fare il setitem nella dw



//verifica se abi e cab sono presenti in anag_banche_clien_for
//se esiste torna il cod_banca_clien_for
//se non esiste inserisci creando un nuovo codice e torna tale codice

integer ll_i, ll_len
string ls_char, ls_appo[], ls_cod_bancaclienfor, ls_max,  ls_null, ls_sql, ls_des_abi, ls_agenzia, ls_indirizzo, ls_des_banca, ls_localita, ls_cap, ls_provincia, ls_telefono, ls_fax
datastore lds_data
long ll_tot, ll_max

setnull(ls_null)

if fs_cod_abi="" or fs_cod_cab="" or isnull(fs_cod_abi) or isnull(fs_cod_cab) then
	setnull(fs_cod_abi_new)
	setnull(fs_cod_cab_new)
	setnull(fs_cod_cin_new)
	
	if isnull(fs_cod_banca_clien_for) then
		setnull(fs_cod_banca_clien_for_new)
		return 0
	else
		// non c'è abi e cab, ma solo la banca_clien_for; vado in cerca di abi e cab
		setnull(fs_cod_cin_new)
		
		select cod_abi, 
				cod_cab
		into 	:fs_cod_abi_new,
				:fs_cod_cab_new
		from 	anag_banche_clien_for
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_banca_clien_for = :fs_cod_banca_clien_for
		using it_sqlc_pt;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca banca cliente fornitore " + string(sqlca.sqlcode) + "  " + sqlca.sqlerrtext
			return -1
		end if
	end if		
	
else
	
	if len(fs_cod_abi) = 4 then  fs_cod_abi =  "0" + fs_cod_abi
	if len(fs_cod_cab) = 4 then fs_cod_cab = "0" + fs_cod_cab
	
	fs_cod_abi_new = fs_cod_abi
	fs_cod_cab_new = fs_cod_cab
	fs_cod_cin_new = fs_cod_cin
	
end if
// in ogni caso se arrivo qui ho ABI-CAB caricati

// controllo se per caso esiste già la stessa banca di appoggio
ll_tot = 0

select count(*) 
into :ll_tot
from 	anag_banche_clien_for
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_abi = :fs_cod_abi_new and
		cod_cab = :fs_cod_cab_new;

if ll_tot > 0 then
	// trovato almeno uno, prendi il primo
	ls_sql = "select cod_banca_clien_for from anag_banche_clien_for where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_abi = '" + fs_cod_abi_new + "' and cod_cab = '" + fs_cod_cab_new + "' "
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, fs_errore)
	// a questo punto sicuramente ll_to è > zero
	ls_cod_bancaclienfor = lds_data.getitemstring(1, 1)
else
	ls_cod_bancaclienfor = ""
end if

if ls_cod_bancaclienfor="" then
	//non trovato, proseguirai con il calcolo del nuovo codice ed inserimento in anag_banche_clien_for
else
	//trovato
	//fdw_dw.setitem(fl_row, fs_col_codbancaclienfor, ls_cod_bancaclienfor)
	
	fs_cod_banca_clien_for_new = ls_cod_bancaclienfor
	
//	select cod_banca_clien_for
//	into	:fs_cod_banca_clien_for_new
//	from 	anag_banche_clien_for
//	where cod_azienda = :s_cs_xx.cod_azienda and
//			cod_abi = :fs_cod_abi_new and
//			cod_cab = :fs_cod_cab_new;
	
	return 0
	
end if

//se sei arrivato fin qui crea il codice e inseriscilo
ll_max = 3

//leggi il max
setnull(ls_cod_bancaclienfor)


select max(substring(cod_banca_clien_for, 1, :ll_max))
into :ls_max
from anag_banche_clien_for
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	fs_errore = "Errore durante il lettura MAX codice da anag_banche_clien_for: "+sqlca.sqlerrtext	
	return -1
end if

if isnull(ls_max) or ls_max="" or sqlca.sqlcode=100 then
	ls_cod_bancaclienfor = "001"

else	
	//carica un array con i caratteri della stringa
	for ll_i = 1 to ll_max
		ls_appo[ll_i] = mid(ls_max, ll_i, 1)
	next
	
	for ll_i = ll_max to 1 step -1
		//preleva i caratteri a partire da destra
		ls_char = ls_appo[ll_i]
		
		choose case ls_char
			case "9"
				//fallo diventare "A"
				ls_appo[ll_i] = "A"
				exit
				
			case "Z"
				//metti il carattere corrente a "0"
				ls_appo[ll_i] = "0"
				
				//vai al successivo carattere spostandoti verso sinistra			
				
			case else
				//incrementa il suo codice ascii
				ls_appo[ll_i] = char(asc(ls_char) + 1)
				exit
				
		end choose
	next
	
	ls_cod_bancaclienfor = ""
	for ll_i = 1 to ll_max
		ls_cod_bancaclienfor +=  ls_appo[ll_i]
	next
	
end if

select   des_abi
into     :ls_des_abi
from     tab_abi
where    cod_abi = :fs_cod_abi_new;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante ricerca del codice ABI nella tabella tab_abi~r~n "+sqlca.sqlerrtext	
	return -1
end if


select 	agenzia,
			indirizzo,
			localita,
			cap,
			provincia,
			telefono,
			fax
into     	:ls_agenzia,
         	:ls_indirizzo,
			:ls_localita, 
			:ls_cap,
			:ls_provincia,
			:ls_telefono,
			:ls_fax
from    	 tab_abicab
where    	cod_abi = :fs_cod_abi_new and 
         	cod_cab = :fs_cod_cab_new;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante ricerca del codice CAB nella tabella tab_abicab / ABI=" + fs_cod_abi_new + " CAB="+  fs_cod_cab_new + "   ERRORE= " + sqlca.sqlerrtext	
	return -1
end if


if not isnull(ls_agenzia) and len(trim(ls_agenzia)) > 0 then
	// l'agenzia è presente
	
	if not isnull(ls_localita) and len(trim(ls_localita)) > 0 then
		// presente anche la località
			ls_des_banca = ls_des_abi + "-" + ls_agenzia + "-" + ls_localita
	else
		// NON presente la località
			ls_des_banca = ls_des_abi + "-" + ls_agenzia
	end if
	
else
	// l'agenzia NON è presente

	if not isnull(ls_localita) and len(trim(ls_localita)) > 0 then
		// presente almeno la località
			ls_des_banca = ls_des_abi + "-" + ls_localita
	else
		// NON presente la località
			ls_des_banca = ls_des_abi
	end if
end if

ls_des_banca = left(ls_des_banca, 40)

fs_cod_abi_new = fs_cod_abi
fs_cod_cab_new = fs_cod_cab
fs_cod_cin_new = ""
fs_cod_banca_clien_for_new = ls_cod_bancaclienfor


//fai l'inserimento nella anag_banche_clien_for
INSERT INTO anag_banche_clien_for  
		( cod_azienda,   
		  cod_banca_clien_for,   
		  des_banca,   
		  indirizzo,   
		  localita,   
		  cap,   
		  provincia,   
		  telefono,   
		  fax,   
		  cod_abi,   
		  cod_cab,   
		  flag_blocco,   
		  data_blocco,   
		  cin,   
		  iban )  
VALUES ( :s_cs_xx.cod_azienda,   
		  :fs_cod_banca_clien_for_new,   
		  :ls_des_banca,   
		  :ls_indirizzo,   
		  :ls_localita,   
		  :ls_cap,   
		  :ls_provincia,   
		  :ls_telefono,   
		  :ls_fax,   
		  :fs_cod_abi_new,   
		  :fs_cod_cab_new,   
		  'N',   
		  null,   
		  :fs_cod_cin_new,   
		  null)  ;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante l'inserimento della banca del cliente in anag_banche_clien_for~r~n "+sqlca.sqlerrtext	
	return -1
end if


return 1
end function

on w_gruppo_gibus.create
int iCurrent
call super::create
this.st_9=create st_9
this.cb_linee_pd=create cb_linee_pd
this.st_rep_sfuso=create st_rep_sfuso
this.cb_rep_sfuso=create cb_rep_sfuso
this.st_giri=create st_giri
this.cb_giri=create cb_giri
this.st_prodotti_linee=create st_prodotti_linee
this.cb_prodotti_linee=create cb_prodotti_linee
this.st_8=create st_8
this.cb_clienti_viropa=create cb_clienti_viropa
this.st_7=create st_7
this.cb_clienti_centro=create cb_clienti_centro
this.st_6=create st_6
this.st_5=create st_5
this.cb_9=create cb_9
this.st_4=create st_4
this.cb_8=create cb_8
this.st_3=create st_3
this.cb_7=create cb_7
this.st_2=create st_2
this.cb_6=create cb_6
this.cb_5=create cb_5
this.st_mc=create st_mc
this.st_vr=create st_vr
this.st_cg=create st_cg
this.st_pt=create st_pt
this.cb_4=create cb_4
this.st_1=create st_1
this.cb_3=create cb_3
this.cb_2=create cb_2
this.st_file=create st_file
this.cb_clienti_pd=create cb_clienti_pd
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_9
this.Control[iCurrent+2]=this.cb_linee_pd
this.Control[iCurrent+3]=this.st_rep_sfuso
this.Control[iCurrent+4]=this.cb_rep_sfuso
this.Control[iCurrent+5]=this.st_giri
this.Control[iCurrent+6]=this.cb_giri
this.Control[iCurrent+7]=this.st_prodotti_linee
this.Control[iCurrent+8]=this.cb_prodotti_linee
this.Control[iCurrent+9]=this.st_8
this.Control[iCurrent+10]=this.cb_clienti_viropa
this.Control[iCurrent+11]=this.st_7
this.Control[iCurrent+12]=this.cb_clienti_centro
this.Control[iCurrent+13]=this.st_6
this.Control[iCurrent+14]=this.st_5
this.Control[iCurrent+15]=this.cb_9
this.Control[iCurrent+16]=this.st_4
this.Control[iCurrent+17]=this.cb_8
this.Control[iCurrent+18]=this.st_3
this.Control[iCurrent+19]=this.cb_7
this.Control[iCurrent+20]=this.st_2
this.Control[iCurrent+21]=this.cb_6
this.Control[iCurrent+22]=this.cb_5
this.Control[iCurrent+23]=this.st_mc
this.Control[iCurrent+24]=this.st_vr
this.Control[iCurrent+25]=this.st_cg
this.Control[iCurrent+26]=this.st_pt
this.Control[iCurrent+27]=this.cb_4
this.Control[iCurrent+28]=this.st_1
this.Control[iCurrent+29]=this.cb_3
this.Control[iCurrent+30]=this.cb_2
this.Control[iCurrent+31]=this.st_file
this.Control[iCurrent+32]=this.cb_clienti_pd
end on

on w_gruppo_gibus.destroy
call super::destroy
destroy(this.st_9)
destroy(this.cb_linee_pd)
destroy(this.st_rep_sfuso)
destroy(this.cb_rep_sfuso)
destroy(this.st_giri)
destroy(this.cb_giri)
destroy(this.st_prodotti_linee)
destroy(this.cb_prodotti_linee)
destroy(this.st_8)
destroy(this.cb_clienti_viropa)
destroy(this.st_7)
destroy(this.cb_clienti_centro)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.cb_9)
destroy(this.st_4)
destroy(this.cb_8)
destroy(this.st_3)
destroy(this.cb_7)
destroy(this.st_2)
destroy(this.cb_6)
destroy(this.cb_5)
destroy(this.st_mc)
destroy(this.st_vr)
destroy(this.st_cg)
destroy(this.st_pt)
destroy(this.cb_4)
destroy(this.st_1)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.st_file)
destroy(this.cb_clienti_pd)
end on

event open;call super::open;// connessione al DB PTENDA

st_pt.visible = false
st_cg.visible = false
st_vr.visible = false
st_mc.visible = false

it_sqlc_pt = create transaction
if f_po_getconnectinfo("", "database_22", it_sqlc_pt) <> 0 then
   return
end if

if f_po_connect(it_sqlc_pt, true) <> 0 then
   return
end if
st_pt.visible = true


// connessione al DB CGIBUS
it_sqlc_cg = create transaction
if f_po_getconnectinfo("", "database_23", it_sqlc_cg) <> 0 then
   return
end if

if f_po_connect(it_sqlc_cg, true) <> 0 then
    return
end if
st_cg.visible = true


// connessione al DB VIROPA
it_sqlc_vr = create transaction
if f_po_getconnectinfo("", "database_24", it_sqlc_vr) <> 0 then
    return
end if

if f_po_connect(it_sqlc_vr, true) <> 0 then
    return
end if
st_vr.visible = true


// connessione al DB MOCELLINI
it_sqlc_mc = create transaction
if f_po_getconnectinfo("", "database_21", it_sqlc_mc) <> 0 then
    return
end if

if f_po_connect(it_sqlc_mc, true) <> 0 then
    return
end if
st_mc.visible = true

end event

type st_9 from statictext within w_gruppo_gibus
integer x = 946
integer y = 1600
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_linee_pd from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1596
integer width = 535
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
string text = "5.1 - Linee Sc. [PD]"
end type

event clicked;string ls_cod_prodotto,ls_valore,ls_tipo,ls_des_prodotto,ls_cod_livello_prod_1,ls_cod_livello_prod_2,ls_cod_livello_prod_3,ls_cod_livello_prod_4, ls_cod_reparto, &
		ls_cod_agente, ls_cod_cliente, ls_flag_sconto_particolare, ls_cod_gruppo_sconto, ls_cod_tipo_anagrafica

long	ll_riga, ll_progressivo, ll_cont

decimal ld_valore, ld_sconto_1, ld_sconto_2, ld_provvigione_1




ll_riga = 1

do while true	
	ll_riga ++
	
	wf_get_value( "clienti_linee", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_agente = upper(ls_valore)
	if len(ls_cod_agente) < 1 then setnull(ls_cod_agente)
//	setnull(ls_cod_agente)
	
	wf_get_value( "clienti_linee", ll_riga, 2, ls_valore, ld_valore, ls_tipo)
	ls_cod_cliente = upper(ls_valore)
	
	if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then exit
	
	select cod_gruppo_sconto, 
			cod_tipo_anagrafica
	into   	:ls_cod_gruppo_sconto, 
			:ls_cod_tipo_anagrafica
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;
	
	if sqlca.sqlcode <> 0 then continue
	
	
	// --- solo VIROPA ----
	ls_cod_gruppo_sconto = ls_cod_agente
	setnull(ls_cod_agente)
	// -------------------------------
	
	
	wf_get_value( "clienti_linee", ll_riga, 4, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_1 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_1) ) < 1 then  setnull(ls_cod_livello_prod_1)
	
	wf_get_value( "clienti_linee", ll_riga, 5, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_2 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_2) ) < 1 then  setnull(ls_cod_livello_prod_2)
	
	wf_get_value( "clienti_linee", ll_riga, 6, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_3 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_3) ) < 1 then  setnull(ls_cod_livello_prod_3)
	
	wf_get_value( "clienti_linee", ll_riga, 7, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_4 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_4) ) < 1 then  setnull(ls_cod_livello_prod_4)

	
	wf_get_value( "clienti_linee", ll_riga, 8, ls_valore, ld_valore, ls_tipo)
	if isnull(ls_valore) or len(ls_valore) < 1 then
		ld_sconto_1 = 0
	else
		ld_sconto_1 = dec(ls_valore)
	end if
	
	wf_get_value( "clienti_linee", ll_riga, 9, ls_valore, ld_valore, ls_tipo)
	if isnull(ls_valore) or len(ls_valore) < 1 then
		ld_sconto_2 = 0
	else
		ld_sconto_2 = dec(ls_valore)
	end if

	wf_get_value( "clienti_linee", ll_riga, 11, ls_valore, ld_valore, ls_tipo)
	if isnull(ls_valore) or len(ls_valore) < 1 then
		ld_provvigione_1 = 0
	else
		ld_provvigione_1 = dec(ls_valore)
	end if

	wf_get_value( "clienti_linee", ll_riga, 13, ls_valore, ld_valore, ls_tipo)
	if isnull(ls_valore) then ls_valore = 'N'
	ls_flag_sconto_particolare = ls_valore
	
	select count(*)
	into :ll_cont
	from gruppi_sconto
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente and
			cod_livello_prod_1 = :ls_cod_livello_prod_1  and
			cod_livello_prod_2 = :ls_cod_livello_prod_2  and
			cod_livello_prod_3 = :ls_cod_livello_prod_3  and
			cod_livello_prod_4 = :ls_cod_livello_prod_4  and
			sconto_1 = :ld_sconto_1 and
			sconto_2 = :ld_sconto_2 ;
	
	if ll_cont > 0 then 	
		ll_cont = 0
		continue
	end if
		
	
	select max(progressivo)
	into :ll_progressivo
	from gruppi_sconto
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_gruppo_sconto = :ls_cod_gruppo_sconto and
			cod_valuta = 'EUR' and
			data_inizio_val = '20120101';
			
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if


	INSERT INTO gruppi_sconto  
			( cod_azienda,   
			  cod_gruppo_sconto,   
			  cod_valuta,   
			  data_inizio_val,   
			  progressivo,   
			  cod_agente,   
			  cod_cliente,   
			  cod_livello_prod_1,   
			  cod_livello_prod_2,   
			  cod_livello_prod_3,   
			  cod_livello_prod_4,   
			  sconto_1,   
			  sconto_2,   
			  provvigione_1,   
			  flag_sconto_particolare )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_gruppo_sconto,   
			  'EUR',   
			  '20120101',   
			  :ll_progressivo,   
			  :ls_cod_agente,   
			  :ls_cod_cliente,   
			  :ls_cod_livello_prod_1,   
			  :ls_cod_livello_prod_2,   
			  :ls_cod_livello_prod_3,   
			  :ls_cod_livello_prod_4,   
			  :ld_sconto_1,   
			  :ld_sconto_2,   
			  :ld_provvigione_1,   
			  :ls_flag_sconto_particolare )  ;


	
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore insert linee sconto " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Linee di sconto " + ls_cod_cliente + "  " +string(ll_progressivo)+ " ... Eseguita !!! - ")
	Yield()
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata: elaborato righe nr " + string(ll_riga))



end event

type st_rep_sfuso from statictext within w_gruppo_gibus
integer x = 946
integer y = 1880
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "FATTO, ma forse è necessario eseguire gli insert per gli stabilimenti"
boolean focusrectangle = false
end type

type cb_rep_sfuso from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1880
integer width = 535
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
string text = "7 - Reparti SFUSO"
end type

event clicked;string ls_cod_prodotto, ls_cod_reparto,  ls_valore, ls_tipo, ls_cod_deposito

long	ll_riga,ll_prog_giro, ll_cont

decimal ld_valore


ll_riga = 1

do while true	
	ll_riga ++
	ll_prog_giro = ll_riga - 1
	
	// leggo sede origine
	wf_get_value( "sfuso_reparti_pd", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_prodotto = upper(ls_valore)
	
	if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then exit
	
	// verifica se  il codice giro esiste in tes_giri_consegne
	select 	count(*)
	into 		:ll_cont
	from 		anag_prodotti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	if ll_cont < 1 then
		g_mb.messagebox("IMPORT REPARTI","Il prodotto " + ls_cod_prodotto + " non esiste!")
		rollback;
		return
	end if
	
//	wf_get_value( "GIRI", ll_riga, 3, ls_valore, ld_valore, ls_tipo)
//	ls_cod_deposito = upper(ls_valore)
//	
//	// verifoc se esiste il deposito
//	select 	count(*)
//	into 		:ll_cont
//	from 		anag_depositi
//	where 	cod_azienda = :s_cs_xx.cod_azienda and
//				cod_deposito = :ls_cod_deposito;
//	
//	if ll_cont < 1 then
//		g_mb.messagebox("IMPORT REPARTI","Il deposito " + ls_cod_deposito + " non esiste!")
//		rollback;
//		return
//	end if
	
	// veriico se il reparto esiste

	
	wf_get_value( "sfuso_reparti_pd", ll_riga, 4, ls_valore, ld_valore, ls_tipo)
	ls_cod_reparto = upper(ls_valore)
	
	if isnull(ls_cod_reparto) or len(ls_cod_reparto) < 1 then continue
	
	// verifica se  il codice giro esiste in tes_giri_consegne
	select 	count(*)
	into 		:ll_cont
	from 		anag_reparti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_reparto = :ls_cod_reparto;
	
	if ll_cont < 1 then
		g_mb.messagebox("IMPORT REPARTI","Il reparto " + ls_cod_reparto + " non esiste!")
		rollback;
		return
	end if
	
	
	
	// update anag_prodotti
	update 	anag_prodotti
	set 		cod_reparto = :ls_cod_reparto 
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
			  
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore update anag_prodotti " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Aggiornamento reparto del prodotto " + ls_cod_prodotto + " ... Eseguita !!! - ")
	Yield()
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata: elaborato righe nr " + string(ll_riga))



end event

type st_giri from statictext within w_gruppo_gibus
integer x = 946
integer y = 1772
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_giri from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1768
integer width = 535
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
boolean enabled = false
string text = "6 - Giri"
end type

event clicked;string ls_cod_giro, ls_cod_cliente, ls_valore, ls_tipo

long	ll_riga,ll_prog_giro, ll_cont

decimal ld_valore


ll_riga = 1

do while true	
	ll_riga ++
	ll_prog_giro = ll_riga - 1
	
	// leggo sede origine
	wf_get_value( "GIRI", ll_riga, 2, ls_valore, ld_valore, ls_tipo)
	ls_cod_giro = upper(ls_valore)
	
	if isnull(ls_cod_giro) or len(ls_cod_giro) < 1 then exit
	
	// verifica se  il codice giro esiste in tes_giri_consegne
	select 	count(*)
	into 		:ll_cont
	from 		tes_giri_consegne
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_giro_consegna = :ls_cod_giro;
	
	if ll_cont < 1 then
		g_mb.messagebox("IMPORT GIRI","Il giro " + ls_cod_giro + " non esiste!")
		rollback;
		return
	end if
	
	wf_get_value( "GIRI", ll_riga, 3, ls_valore, ld_valore, ls_tipo)
	ls_cod_cliente = upper(ls_valore)
	
	// verifica se  il codice giro esiste in tes_giri_consegne
	select 	count(*)
	into 		:ll_cont
	from 		anag_clienti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_cod_cliente;
	
	if ll_cont < 1 then  continue
	
	// veriico se il cliente ESISTE
	
	// insert
	INSERT INTO det_giri_consegne  
         ( cod_azienda,   
           cod_giro_consegna,   
           cod_cliente,   
           prog_giro_consegna,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia )  
	VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_giro,   
           :ls_cod_cliente,   
           :ll_prog_giro,   
           null,   
           null,   
           null,   
           null,   
           null )  ;
			  
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore insert det_giri_consegne " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione giro cliente  " + ls_cod_cliente + " ... Eseguita !!! - ")
	Yield()
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata: elaborato righe nr " + string(ll_riga))



end event

type st_prodotti_linee from statictext within w_gruppo_gibus
integer x = 946
integer y = 1496
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Fatto, non serve più"
boolean focusrectangle = false
end type

type cb_prodotti_linee from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1492
integer width = 535
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
boolean enabled = false
string text = "5 - Prodotti Linee"
end type

event clicked;string ls_cod_prodotto,ls_valore,ls_tipo,ls_des_prodotto,ls_cod_livello_prod_1,ls_cod_livello_prod_2,ls_cod_livello_prod_3,ls_cod_livello_prod_4, ls_cod_reparto

long	ll_riga

decimal ld_valore




ll_riga = 1

do while true	
	ll_riga ++
	
	// leggo sede origine
	wf_get_value( "Prodotti_linee", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_prodotto = upper(ls_valore)
	
	if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then exit
	
	wf_get_value( "Prodotti_linee", ll_riga, 2, ls_valore, ld_valore, ls_tipo)
	ls_des_prodotto = upper(ls_valore)
	
	wf_get_value( "Prodotti_linee", ll_riga, 4, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_1 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_1) ) < 1 then  setnull(ls_cod_livello_prod_1)
	
	wf_get_value( "Prodotti_linee", ll_riga, 5, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_2 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_2) ) < 1 then  setnull(ls_cod_livello_prod_2)
	
	wf_get_value( "Prodotti_linee", ll_riga, 6, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_3 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_3) ) < 1 then  setnull(ls_cod_livello_prod_3)
	
	wf_get_value( "Prodotti_linee", ll_riga, 7, ls_valore, ld_valore, ls_tipo)
	ls_cod_livello_prod_4 = upper(ls_valore)
	if len( trim(ls_cod_livello_prod_4) ) < 1 then  setnull(ls_cod_livello_prod_4)

	wf_get_value( "Prodotti_linee", ll_riga, 8, ls_valore, ld_valore, ls_tipo)
	ls_cod_reparto = upper(ls_valore)
	if len( trim(ls_cod_reparto) ) < 1 then  setnull(ls_cod_reparto)

	update anag_prodotti
	set		cod_livello_prod_1 = :ls_cod_livello_prod_1 ,
			cod_livello_prod_2 = :ls_cod_livello_prod_2 ,
			cod_livello_prod_3 = :ls_cod_livello_prod_3 ,
			cod_livello_prod_4 = :ls_cod_livello_prod_4 ,
			cod_reparto = :ls_cod_reparto
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore update anag_prodotti " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Linee di sconto " + ls_cod_prodotto + "  " +ls_des_prodotto+ " ... Eseguita !!! - ")
	Yield()
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata: elaborato righe nr " + string(ll_riga))



end event

type st_8 from statictext within w_gruppo_gibus
integer x = 946
integer y = 1332
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_clienti_viropa from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1332
integer width = 535
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
boolean enabled = false
string text = "2.3 - Clienti VIROPA"
end type

event clicked;string	ls_cod_cliente_new, ls_valore, ls_tipo, ls_cod_cliente_old, &
ls_rag_soc_1, ls_rag_soc_2,ls_indirizzo, ls_localita,ls_frazione,ls_cap,ls_provincia,ls_telefono,ls_fax,ls_telex,ls_cod_fiscale,ls_partita_iva,ls_rif_interno,ls_flag_tipo_cliente,ls_cod_iva,ls_num_prot_esenzione, &
ls_flag_sospensione_iva,ls_cod_pagamento_old, ls_cod_pagamento_new,ls_cod_tipo_listino_cliente,&
ls_flag_fuori_fido,ls_banca_clien_for,ls_conto_corrente,ls_casella_mail,ls_sito_internet,ls_cod_banca_nostra,ls_rag_soc_abbreviata,ls_flag_raggruppo_scadenze,ls_cod_tipo_anagrafica,&
ls_flag_accetta_mail,ls_email_amministrazione,ls_cin,ls_abi,ls_cab,ls_iban,ls_flag_invia_aut_ddt,ls_flag_invia_aut_fat, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, &
ls_cod_banca_clien_for_new, ls_errore, ls_cod_capoconto, ls_cod_deposito, ls_cod_valuta, ls_cod_gruppo_sconto, ls_cod_porto, ls_cod_mezzo, ls_cod_agente_1

long	ll_riga,ll_nuovo_codice,ll_giorno_fisso_scadenza,ll_mese_escl_1,ll_mese_escl_2,ll_data_sost_1,ll_data_sost_2, ll_ret, ll_y

dec{4} ld_fido ,ld_sconto

decimal ld_valore

datetime ldt_data_esenzione_iva

datastore lds_data1, lds_data2,lds_data3



ll_riga = 1
ll_nuovo_codice = 0

do while true	
	ll_riga ++
	ll_nuovo_codice ++
	
	// leggo sede origine
	wf_get_value( "CLIENTI", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_cliente_old = upper(ls_valore)
	
	if isnull(ls_cod_cliente_old) or ls_cod_cliente_old="" then exit
	
	// ========= DATI DA IMPOSTARE PER OGNI IMPERTAZIONE =============
	// Creo il codice fornitore con il pre-codice corretto; 1=PD  2=VEGG  3=PT  4=CU
	ls_cod_cliente_new = "30" + ls_cod_cliente_old
	// Capoconto 01=PD   02=Vegg  03=PT  04=CU   05=Estero  10=Privati
	ls_cod_capoconto = "03"
	// Deposito appartenenza
	ls_cod_deposito = "300"
	// Valuta
	ls_cod_valuta ='EUR'
	// Linea di sconto
	ls_cod_gruppo_sconto = "001"
	//=======================================================

	SELECT rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			telefono,   
			fax,   
			telex,   
			cod_fiscale,   
			partita_iva,   
			rif_interno,   
			flag_tipo_cliente,   
			cod_iva,   
			num_prot_esenzione_iva,   
			data_esenzione_iva,   
			flag_sospensione_iva,   
			cod_pagamento,   
			giorno_fisso_scadenza,   
			mese_esclusione_1,   
			mese_esclusione_2,   
			data_sostituzione_1,   
			data_sostituzione_2,   
			sconto,   
			cod_tipo_listino_prodotto,   
			fido,   
			flag_fuori_fido,   
			cod_banca_clien_for,   
			conto_corrente,   
			casella_mail,   
			sito_internet,   
			cod_banca,   
			rag_soc_abbreviata,   
			flag_raggruppo_scadenze,   
			cod_tipo_anagrafica,   
			flag_accetta_mail,   
			email_amministrazione,   
			cin,   
			abi,   
			cab,   
			iban,   
			flag_invia_aut_ddt,   
			flag_invia_aut_fat,
			cod_porto,
			cod_mezzo,
			cod_agente_1
	INTO :ls_rag_soc_1,   
			:ls_rag_soc_2,   
			:ls_indirizzo,   
			:ls_localita,   
			:ls_frazione,   
			:ls_cap,   
			:ls_provincia,   
			:ls_telefono,   
			:ls_fax,   
			:ls_telex,   
			:ls_cod_fiscale,   
			:ls_partita_iva,   
			:ls_rif_interno,   
			:ls_flag_tipo_cliente,   
			:ls_cod_iva,   
			:ls_num_prot_esenzione,   
			:ldt_data_esenzione_iva,   
			:ls_flag_sospensione_iva,   
			:ls_cod_pagamento_old,   
			:ll_giorno_fisso_scadenza,   
			:ll_mese_escl_1,   
			:ll_mese_escl_2,   
			:ll_data_sost_1,   
			:ll_data_sost_2,   
			:ld_sconto,   
			:ls_cod_tipo_listino_cliente,   
			:ld_fido,   
			:ls_flag_fuori_fido,   
			:ls_banca_clien_for,   
			:ls_conto_corrente,   
			:ls_casella_mail,   
			:ls_sito_internet,   
			:ls_cod_banca_nostra,   
			:ls_rag_soc_abbreviata,   
			:ls_flag_raggruppo_scadenze,   
			:ls_cod_tipo_anagrafica,   
			:ls_flag_accetta_mail,   
			:ls_email_amministrazione,   
			:ls_cin,   
			:ls_abi,   
			:ls_cab,   
			:ls_iban,   
			:ls_flag_invia_aut_ddt,   
			:ls_flag_invia_aut_fat,
			:ls_cod_porto,
			:ls_cod_mezzo,
			:ls_cod_agente_1
	FROM anag_clienti 
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente_old
	using it_sqlc_vr ;

	if it_sqlc_vr.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in ricerca cliente vecchio " + ls_cod_cliente_old + "~r~n" + it_sqlc_vr.sqlerrtext)
		rollback using sqlca;
		return 
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1)
	Yield()

	
	/* elaborazioni   */
	// pagamenti
	setnull(ls_cod_pagamento_new)
	
	if not isnull(ls_cod_pagamento_old) then
		select	codice
		into 	:ls_cod_pagamento_new
		FROM temp_pagamenti
		where cli_viropa = :ls_cod_pagamento_old;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Il pagamento " + ls_cod_pagamento_old + " non ha trovato alcuna corrispondenza nel DB Gibus~r~nPRENDERE NOTA DEL CLIENTE!!!~r~n" + sqlca.sqlerrtext)
			setnull(ls_cod_pagamento_new)
		end if
	end if
	// banche di appoggio
	
	setnull(ls_cod_banca_clien_for_new)
	setnull(ls_cod_abi_new)
	setnull(ls_cod_cab_new)
	setnull(ls_cod_cin_new)
	
	if len(ls_abi) >= 4 then
	
		ll_ret = wf_get_banca_appoggio(ls_abi, ls_cab, ls_cin, ls_iban, ls_banca_clien_for, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, ls_cod_banca_clien_for_new, ls_errore)
	
		if ll_ret < 0 then
			g_mb.messagebox("APICE", "Errore in ricerca/creazione banca appoggio~r~n" + ls_errore)
			//rollback using sqlca;
			//return 
			setnull(ls_cod_banca_clien_for_new)
		end if
	end if
	// banca NOSTRA lasciamo stare.
	
	// Impostazione del codice capoconto
	choose case upper(ls_flag_tipo_cliente)
		case "C", "E"		// ESTERO
			ls_cod_capoconto = "05"
		case "P"				// PRIVATI   (di cosa ?)
			ls_cod_capoconto = "10"
	end choose
	

	ls_rag_soc_1 = upper(ls_rag_soc_1)
	ls_indirizzo = upper(ls_indirizzo)
	ls_cap = upper(ls_cap)
	
	
	INSERT INTO anag_clienti
			(	cod_azienda,
				cod_cliente,
				rag_soc_1,   
				rag_soc_2,   
				indirizzo,   
				localita,   
				frazione,   
				cap,   
				provincia,   
				telefono,   
				fax,   
				telex,   
				cod_fiscale,   
				partita_iva,   
				rif_interno,   
				flag_tipo_cliente,   
				cod_iva,   
				num_prot_esenzione_iva,   
				data_esenzione_iva,   
				flag_sospensione_iva,   
				cod_pagamento,   
				giorno_fisso_scadenza,   
				mese_esclusione_1,   
				mese_esclusione_2,   
				data_sostituzione_1,   
				data_sostituzione_2,   
				sconto,   
				cod_tipo_listino_prodotto,   
				fido,   
				flag_fuori_fido,   
				cod_banca_clien_for,   
				conto_corrente,   
				casella_mail,   
				sito_internet,   
				cod_banca,   
				rag_soc_abbreviata,   
				flag_raggruppo_scadenze,   
				cod_tipo_anagrafica,   
				flag_accetta_mail,   
				email_amministrazione,   
				cin,   
				abi,   
				cab,   
				iban,   
				flag_invia_aut_ddt,   
				flag_invia_aut_fat,
				origine_anagrafica_codice,
				cod_capoconto,
				cod_deposito,
				cod_gruppo_sconto,
				cod_valuta,
				cod_porto,
				cod_mezzo,
				cod_agente_1)
			VALUES
	(			:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_rag_soc_1,   
				:ls_rag_soc_2,   
				:ls_indirizzo,   
				:ls_localita,   
				:ls_frazione,   
				:ls_cap,   
				:ls_provincia,   
				:ls_telefono,   
				:ls_fax,   
				:ls_telex,   
				:ls_cod_fiscale,   
				:ls_partita_iva,   
				:ls_rif_interno,   
				:ls_flag_tipo_cliente,   
				:ls_cod_iva,   
				:ls_num_prot_esenzione,   
				:ldt_data_esenzione_iva,   
				:ls_flag_sospensione_iva,   
				:ls_cod_pagamento_new,   
				:ll_giorno_fisso_scadenza,   
				:ll_mese_escl_1,   
				:ll_mese_escl_2,   
				:ll_data_sost_1,   
				:ll_data_sost_2,   
				:ld_sconto,   
				:ls_cod_tipo_listino_cliente,   
				:ld_fido,   
				:ls_flag_fuori_fido,   
				:ls_cod_banca_clien_for_new,   
				:ls_conto_corrente,   
				:ls_casella_mail,   
				:ls_sito_internet,   
				null,   
				:ls_rag_soc_abbreviata,   
				:ls_flag_raggruppo_scadenze,   
				:ls_cod_tipo_anagrafica,   
				:ls_flag_accetta_mail,   
				:ls_email_amministrazione,   
				:ls_cod_cin_new,   
				:ls_cod_abi_new,   
				:ls_cod_cab_new,   
				:ls_iban,   
				:ls_flag_invia_aut_ddt,   
				:ls_flag_invia_aut_fat,
				:ls_cod_cliente_old,
				:ls_cod_capoconto,
				:ls_cod_deposito,
				:ls_cod_gruppo_sconto,
				:ls_cod_valuta,
				:ls_cod_porto,
				:ls_cod_mezzo,
				:ls_cod_agente_1);
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in inserimento cliente nuovo " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
		rollback using sqlca;
		return 
	end if

	// Procedo con la duplicazione delle tabelle collegate
	// ====>>> ADDEBITI
	string ls_cod_addebito, ls_des_addebito,ls_cod_tipo_det_ven,  ls_flag_tipo_documento,ls_flag_mod_valore_bolle,ls_flag_mod_valore_fatture, ls_sql
	dec{4} ld_importo_addebito
	datetime ldt_data_inizio, ldt_data_fine
	
	ls_sql = " SELECT cod_addebito, des_addebito,   cod_tipo_det_ven,   cod_valuta,   data_inizio,   data_fine,   importo_addebito,   flag_tipo_documento,   flag_mod_valore_bolle,   	flag_mod_valore_fatture  FROM anag_clienti_addebiti WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql, it_sqlc_vr  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Addebiti clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret
		ls_cod_addebito = lds_data1.getitemstring(ll_y, 1)
		ls_des_addebito = lds_data1.getitemstring(ll_y, 2)   
		ls_cod_tipo_det_ven = lds_data1.getitemstring(ll_y, 3)
		ls_cod_valuta = lds_data1.getitemstring(ll_y, 4)
		ldt_data_inizio    = lds_data1.getitemdatetime(ll_y, 5)
		ldt_data_fine   = lds_data1.getitemdatetime(ll_y, 6)
		ld_importo_addebito = lds_data1.getitemdecimal(ll_y, 7)
		ls_flag_tipo_documento = lds_data1.getitemstring(ll_y, 8)
		ls_flag_mod_valore_bolle = lds_data1.getitemstring(ll_y, 9)
		ls_flag_mod_valore_fatture  = lds_data1.getitemstring(ll_y, 10)
	
		INSERT INTO anag_clienti_addebiti  
			( cod_azienda,   
			  cod_cliente,   
			  cod_addebito,   
			  des_addebito,   
			  cod_tipo_det_ven,   
			  cod_valuta,   
			  data_inizio,   
			  data_fine,   
			  importo_addebito,   
			  flag_tipo_documento,   
			  flag_mod_valore_bolle,   
			  flag_mod_valore_fatture )  
		values (:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_cod_addebito,   
				:ls_des_addebito,   
				:ls_cod_tipo_det_ven,   
				:ls_cod_valuta,   
				:ldt_data_inizio,   
				:ldt_data_fine,   
				:ld_importo_addebito,   
				:ls_flag_tipo_documento,   
				:ls_flag_mod_valore_bolle,   
				:ls_flag_mod_valore_fatture )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento addebiti cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data1
	
	
	
	// ===>>>>DESTINAZIONI CLIENTI
	
	string ls_cod_des_cliente, ls_flag_dest_merce_fat
	
	ls_sql = "SELECT cod_des_cliente,   rag_soc_1,  rag_soc_2,   indirizzo,   localita,   frazione,   cap,   provincia,   telefono,   fax,   telex,   cod_deposito,   flag_blocco,   data_blocco,   flag_dest_merce_fat  FROM anag_des_clienti    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data2, ls_sql, it_sqlc_vr  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Destinazioni clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_des_cliente = lds_data2.getitemstring(ll_y, 1)
		ls_rag_soc_1 = lds_data2.getitemstring(ll_y, 2)
		ls_rag_soc_2 = lds_data2.getitemstring(ll_y, 3)
		ls_indirizzo = lds_data2.getitemstring(ll_y, 4)
		ls_localita = lds_data2.getitemstring(ll_y, 5)
		ls_frazione = lds_data2.getitemstring(ll_y, 6)
		ls_cap = lds_data2.getitemstring(ll_y, 7)
		ls_provincia = lds_data2.getitemstring(ll_y, 8)
		ls_telefono = lds_data2.getitemstring(ll_y, 9)
		ls_fax = lds_data2.getitemstring(ll_y, 10)
		ls_telex = lds_data2.getitemstring(ll_y, 11)
		ls_flag_dest_merce_fat = lds_data2.getitemstring(ll_y, 12)
	
		INSERT INTO anag_des_clienti  
         ( cod_azienda,   
           cod_cliente,   
           cod_des_cliente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_deposito,   
           flag_blocco,   
           data_blocco,   
           flag_dest_merce_fat )  
		VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_cliente_new,   
           :ls_cod_des_cliente,   
           :ls_rag_soc_1,   
           :ls_rag_soc_2,   
           :ls_indirizzo,   
           :ls_localita,   
           :ls_frazione,   
           :ls_cap,   
           :ls_provincia,   
           :ls_telefono,   
           :ls_fax,   
           :ls_telex,   
           null,   
           'N',   
           null,   
           :ls_flag_dest_merce_fat )  ;
	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento destinazioni cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data2
	
	
	// =========>>> NOTE FISSE
	
	string ls_cod_nota_fissa, ls_des_nota_fissa,ls_flag_trattativa_ven, ls_flag_offerta_ven,  ls_flag_ordine_ven, ls_nota_fissa, &
			ls_flag_bolla_ven, ls_flag_fattura_ven, ls_flag_offerta_acq, ls_flag_ordine_acq, ls_flag_piede_testata, ls_flag_blocco, ls_flag_uso_gen_doc, &
			ls_flag_bolla_acq,ls_flag_fattura_acq,ls_flag_fattura_proforma,ls_cod_tipo_fat_ven,ls_flag_usa_stampa_doc, ls_flag_listino_vendita
	long ll_prog_mimetype
	
	ls_sql = " SELECT cod_nota_fissa,   des_nota_fissa,   nota_fissa,   data_inizio,   data_fine,   flag_trattativa_ven,   flag_offerta_ven,   flag_ordine_ven,   flag_bolla_ven,   flag_fattura_ven,   flag_offerta_acq,   flag_ordine_acq,   flag_piede_testata,   flag_blocco,   flag_uso_gen_doc,   flag_bolla_acq,   flag_fattura_acq,   flag_fattura_proforma,   cod_tipo_fat_ven,   flag_usa_stampa_doc,   flag_listino_vendita,   prog_mimetype  FROM tab_note_fisse    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data3, ls_sql, it_sqlc_vr  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE note fisse clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_nota_fissa = lds_data3.getitemstring(ll_y, 1)
		ls_des_nota_fissa = lds_data3.getitemstring(ll_y, 2)
		ls_nota_fissa = lds_data3.getitemstring(ll_y, 3)
		ldt_data_inizio = lds_data3.getitemdatetime(ll_y, 4)
		ldt_data_fine = lds_data3.getitemdatetime(ll_y, 5)
		ls_flag_trattativa_ven = lds_data3.getitemstring(ll_y,6)
		ls_flag_offerta_ven = lds_data3.getitemstring(ll_y,7)
		ls_flag_ordine_ven = lds_data3.getitemstring(ll_y, 8)
		ls_flag_bolla_ven = lds_data3.getitemstring(ll_y, 9)
		ls_flag_fattura_ven = lds_data3.getitemstring(ll_y, 10)
		ls_flag_offerta_acq = lds_data3.getitemstring(ll_y, 11)
		ls_flag_ordine_acq = lds_data3.getitemstring(ll_y, 12)
		ls_flag_piede_testata = lds_data3.getitemstring(ll_y, 13)
		ls_flag_blocco = lds_data3.getitemstring(ll_y, 14)
		ls_flag_uso_gen_doc = lds_data3.getitemstring(ll_y, 15)
		ls_flag_bolla_acq = lds_data3.getitemstring(ll_y, 16)
		ls_flag_fattura_acq = lds_data3.getitemstring(ll_y, 17)
		ls_flag_fattura_proforma = lds_data3.getitemstring(ll_y, 18)
		ls_cod_tipo_fat_ven = lds_data3.getitemstring(ll_y, 19)
		ls_flag_usa_stampa_doc = lds_data3.getitemstring(ll_y, 20)
		ls_flag_listino_vendita = lds_data3.getitemstring(ll_y, 21)
		ll_prog_mimetype	 = lds_data3.getitemnumber(ll_y, 22)
		
		ls_cod_nota_fissa = "3" + fill("0", 5 - len(ls_cod_nota_fissa)) + ls_cod_nota_fissa

	  INSERT INTO tab_note_fisse  
				( cod_azienda,   
				  cod_nota_fissa,   
				  des_nota_fissa,   
				  nota_fissa,   
				  cod_cliente,   
				  cod_fornitore,   
				  data_inizio,   
				  data_fine,   
				  flag_trattativa_ven,   
				  flag_offerta_ven,   
				  flag_ordine_ven,   
				  flag_bolla_ven,   
				  flag_fattura_ven,   
				  flag_offerta_acq,   
				  flag_ordine_acq,   
				  flag_piede_testata,   
				  flag_blocco,   
				  flag_uso_gen_doc,   
				  flag_bolla_acq,   
				  flag_fattura_acq,   
				  flag_fattura_proforma,   
				  cod_tipo_fat_ven,   
				  flag_usa_stampa_doc,   
				  flag_listino_vendita,   
				  prog_mimetype )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_nota_fissa,   
				  :ls_des_nota_fissa,   
				  :ls_nota_fissa,   
				  :ls_cod_cliente_new,   
				  null,   
				  :ldt_data_inizio,   
				  :ldt_data_fine,   
				  :ls_flag_trattativa_ven,   
				  :ls_flag_offerta_ven,   
				  :ls_flag_ordine_ven,   
				  :ls_flag_Bolla_ven,   
				  :ls_flag_fattura_ven,   
				  :ls_flag_offerta_acq,   
				  :ls_flag_ordine_acq,   
				  :ls_flag_piede_testata,   
				  :ls_flag_blocco,   
				  :ls_flag_uso_gen_doc,   
				  :ls_flag_bolla_acq,   
				  :ls_flag_fattura_acq,   
				  :ls_flag_fattura_proforma,   
				  :ls_cod_tipo_fat_Ven,   
				  :ls_flag_usa_stampa_doc,   
				  :ls_flag_listino_vendita,   
				  :ll_prog_mimetype )  ;

	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento note fisse cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data3
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1 + " ... Eseguita !!! - "+ ls_cod_cliente_new)
	Yield()
	
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata")



end event

type st_7 from statictext within w_gruppo_gibus
integer x = 946
integer y = 1220
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_clienti_centro from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1220
integer width = 535
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
boolean enabled = false
string text = "2.2 - Clienti CENTRO"
end type

event clicked;string	ls_cod_cliente_new, ls_valore, ls_tipo, ls_cod_cliente_old, ls_error_sql,&
ls_rag_soc_1, ls_rag_soc_2,ls_indirizzo, ls_localita,ls_frazione,ls_cap,ls_provincia,ls_telefono,ls_fax,ls_telex,ls_cod_fiscale,ls_partita_iva,ls_rif_interno,ls_flag_tipo_cliente,ls_cod_iva,ls_num_prot_esenzione, &
ls_flag_sospensione_iva,ls_cod_pagamento_old, ls_cod_pagamento_new,ls_cod_tipo_listino_cliente,&
ls_flag_fuori_fido,ls_banca_clien_for,ls_conto_corrente,ls_casella_mail,ls_sito_internet,ls_cod_banca_nostra,ls_rag_soc_abbreviata,ls_flag_raggruppo_scadenze,ls_cod_tipo_anagrafica,&
ls_flag_accetta_mail,ls_email_amministrazione,ls_cin,ls_abi,ls_cab,ls_iban,ls_flag_invia_aut_ddt,ls_flag_invia_aut_fat, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, &
ls_cod_banca_clien_for_new, ls_errore, ls_cod_capoconto, ls_cod_deposito, ls_cod_valuta, ls_cod_gruppo_sconto, ls_cod_porto, ls_cod_mezzo, ls_cod_agente_1

long	ll_riga,ll_nuovo_codice,ll_giorno_fisso_scadenza,ll_mese_escl_1,ll_mese_escl_2,ll_data_sost_1,ll_data_sost_2, ll_ret, ll_y

dec{4} ld_fido ,ld_sconto

decimal ld_valore

datetime ldt_data_esenzione_iva

datastore lds_data1, lds_data2,lds_data3



ll_riga = 1
ll_nuovo_codice = 0

do while true	
	ll_riga ++
	ll_nuovo_codice ++
	
	// leggo sede origine
	wf_get_value( "CLIENTI", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_cliente_old = upper(ls_valore)
	
	if isnull(ls_cod_cliente_old) or ls_cod_cliente_old="" then exit
	
	// ========= DATI DA IMPOSTARE PER OGNI IMPERTAZIONE =============
	// Creo il codice fornitore con il pre-codice corretto; 1=PD  2=VEGG  3=PT  4=CU
	ls_cod_cliente_new = "20" + ls_cod_cliente_old
	// Capoconto 01=PD   02=Vegg  03=PT  04=CU   05=Estero  10=Privati
	ls_cod_capoconto = "02"
	// Deposito appartenenza
	ls_cod_deposito = "200"
	// Valuta
	ls_cod_valuta ='EUR'
	// Linea di sconto
	ls_cod_gruppo_sconto = "001"
	//=======================================================

	SELECT rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			telefono,   
			fax,   
			telex,   
			cod_fiscale,   
			partita_iva,   
			rif_interno,   
			flag_tipo_cliente,   
			cod_iva,   
			num_prot_esenzione_iva,   
			data_esenzione_iva,   
			flag_sospensione_iva,   
			cod_pagamento,   
			giorno_fisso_scadenza,   
			mese_esclusione_1,   
			mese_esclusione_2,   
			data_sostituzione_1,   
			data_sostituzione_2,   
			sconto,   
			cod_tipo_listino_prodotto,   
			fido,   
			flag_fuori_fido,   
			cod_banca_clien_for,   
			conto_corrente,   
			casella_mail,   
			sito_internet,   
			cod_banca,   
			rag_soc_abbreviata,   
			flag_raggruppo_scadenze,   
			cod_tipo_anagrafica,   
			flag_accetta_mail,   
			email_amministrazione,   
			cin,   
			abi,   
			cab,   
			iban,   
			flag_invia_aut_ddt,   
			flag_invia_aut_fat,
			cod_porto,
			cod_mezzo,
			cod_agente_1
	INTO :ls_rag_soc_1,   
			:ls_rag_soc_2,   
			:ls_indirizzo,   
			:ls_localita,   
			:ls_frazione,   
			:ls_cap,   
			:ls_provincia,   
			:ls_telefono,   
			:ls_fax,   
			:ls_telex,   
			:ls_cod_fiscale,   
			:ls_partita_iva,   
			:ls_rif_interno,   
			:ls_flag_tipo_cliente,   
			:ls_cod_iva,   
			:ls_num_prot_esenzione,   
			:ldt_data_esenzione_iva,   
			:ls_flag_sospensione_iva,   
			:ls_cod_pagamento_old,   
			:ll_giorno_fisso_scadenza,   
			:ll_mese_escl_1,   
			:ll_mese_escl_2,   
			:ll_data_sost_1,   
			:ll_data_sost_2,   
			:ld_sconto,   
			:ls_cod_tipo_listino_cliente,   
			:ld_fido,   
			:ls_flag_fuori_fido,   
			:ls_banca_clien_for,   
			:ls_conto_corrente,   
			:ls_casella_mail,   
			:ls_sito_internet,   
			:ls_cod_banca_nostra,   
			:ls_rag_soc_abbreviata,   
			:ls_flag_raggruppo_scadenze,   
			:ls_cod_tipo_anagrafica,   
			:ls_flag_accetta_mail,   
			:ls_email_amministrazione,   
			:ls_cin,   
			:ls_abi,   
			:ls_cab,   
			:ls_iban,   
			:ls_flag_invia_aut_ddt,   
			:ls_flag_invia_aut_fat,
			:ls_cod_porto,
			:ls_cod_mezzo,
			:ls_cod_agente_1
	FROM anag_clienti 
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente_old
	using it_sqlc_cg ;

	if it_sqlc_cg.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in ricerca cliente vecchio " + ls_cod_cliente_old + "~r~n" + it_sqlc_cg.sqlerrtext)
		rollback using sqlca;
		return 
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1)
	Yield()

	
	/* elaborazioni   */
	// pagamenti
	setnull(ls_cod_pagamento_new)
	
	if not isnull(ls_cod_pagamento_old) then
		select	codice
		into 	:ls_cod_pagamento_new
		FROM temp_pagamenti
		where cli_cgibus = :ls_cod_pagamento_old;
	
		if sqlca.sqlcode <> 0 then
			// g_mb.messagebox("IMPORT", "Il pagamento " + ls_cod_pagamento_old + " non ha trovato alcuna corrispondenza nel DB Gibus~r~nPRENDERE NOTA DEL CLIENTE!!!~r~n" + sqlca.sqlerrtext)
			setnull(ls_cod_pagamento_new)
		end if
	end if
	// banche di appoggio
	
	setnull(ls_cod_banca_clien_for_new)
	setnull(ls_cod_abi_new)
	setnull(ls_cod_cab_new)
	setnull(ls_cod_cin_new)
	
	ll_ret = wf_get_banca_appoggio(ls_abi, ls_cab, ls_cin, ls_iban, ls_banca_clien_for, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, ls_cod_banca_clien_for_new, ls_errore)
	
	if ll_ret < 0 then
		if g_mb.messagebox("APICE", "Errore in ricerca/creazione banca appoggio~r~n" + ls_errore, Question!, YesNo!, 1) = 1 then
			rollback using sqlca;
			return 
		end if
	end if
		
	// banca NOSTRA lasciamo stare.
	
	// Impostazione del codice capoconto
	choose case upper(ls_flag_tipo_cliente)
		case "C", "E"		// ESTERO
			ls_cod_capoconto = "05"
		case "P"				// PRIVATI   (di cosa ?)
			ls_cod_capoconto = "10"
	end choose
	
	if  not isnull(ls_cod_agente_1) then
		ls_cod_agente_1 = "2" + right(ls_cod_agente_1,2)
	end if
	
	ls_rag_soc_1 = upper(ls_rag_soc_1)
	ls_indirizzo = upper(ls_indirizzo)
	ls_cap = upper(ls_cap)
	
	if ls_cod_iva = '1' then ls_cod_iva = 'N41'
	if ls_cod_iva = '2' then ls_cod_iva = 'N8C'
	if ls_cod_iva = 'F02' then ls_cod_iva = 'F2'
	
	// inserimento del posto
	
	insert into tab_porti
	(cod_azienda,
	cod_porto,
	des_porto)
	select cod_azienda,
	cod_porto,
	des_porto
	from cs_db_cgibus.dbo.tab_porti
	where cod_porto = :ls_cod_porto;
	
	ls_error_sql = sqlca.sqlerrtext
	
	
	
	INSERT INTO anag_clienti
			(	cod_azienda,
				cod_cliente,
				rag_soc_1,   
				rag_soc_2,   
				indirizzo,   
				localita,   
				frazione,   
				cap,   
				provincia,   
				telefono,   
				fax,   
				telex,   
				cod_fiscale,   
				partita_iva,   
				rif_interno,   
				flag_tipo_cliente,   
				cod_iva,   
				num_prot_esenzione_iva,   
				data_esenzione_iva,   
				flag_sospensione_iva,   
				cod_pagamento,   
				giorno_fisso_scadenza,   
				mese_esclusione_1,   
				mese_esclusione_2,   
				data_sostituzione_1,   
				data_sostituzione_2,   
				sconto,   
				cod_tipo_listino_prodotto,   
				fido,   
				flag_fuori_fido,   
				cod_banca_clien_for,   
				conto_corrente,   
				casella_mail,   
				sito_internet,   
				cod_banca,   
				rag_soc_abbreviata,   
				flag_raggruppo_scadenze,   
				cod_tipo_anagrafica,   
				flag_accetta_mail,   
				email_amministrazione,   
				cin,   
				abi,   
				cab,   
				iban,   
				flag_invia_aut_ddt,   
				flag_invia_aut_fat,
				origine_anagrafica_codice,
				cod_capoconto,
				cod_deposito,
				cod_gruppo_sconto,
				cod_valuta,
				cod_porto,
				cod_mezzo,
				cod_agente_1)
			VALUES
	(			:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_rag_soc_1,   
				:ls_rag_soc_2,   
				:ls_indirizzo,   
				:ls_localita,   
				:ls_frazione,   
				:ls_cap,   
				:ls_provincia,   
				:ls_telefono,   
				:ls_fax,   
				:ls_telex,   
				:ls_cod_fiscale,   
				:ls_partita_iva,   
				:ls_rif_interno,   
				:ls_flag_tipo_cliente,   
				:ls_cod_iva,   
				:ls_num_prot_esenzione,   
				:ldt_data_esenzione_iva,   
				:ls_flag_sospensione_iva,   
				:ls_cod_pagamento_new,   
				:ll_giorno_fisso_scadenza,   
				:ll_mese_escl_1,   
				:ll_mese_escl_2,   
				:ll_data_sost_1,   
				:ll_data_sost_2,   
				:ld_sconto,   
				:ls_cod_tipo_listino_cliente,   
				:ld_fido,   
				:ls_flag_fuori_fido,   
				:ls_cod_banca_clien_for_new,   
				:ls_conto_corrente,   
				:ls_casella_mail,   
				:ls_sito_internet,   
				null,   
				:ls_rag_soc_abbreviata,   
				:ls_flag_raggruppo_scadenze,   
				:ls_cod_tipo_anagrafica,   
				:ls_flag_accetta_mail,   
				:ls_email_amministrazione,   
				:ls_cod_cin_new,   
				:ls_cod_abi_new,   
				:ls_cod_cab_new,   
				:ls_iban,   
				:ls_flag_invia_aut_ddt,   
				:ls_flag_invia_aut_fat,
				:ls_cod_cliente_old,
				:ls_cod_capoconto,
				:ls_cod_deposito,
				:ls_cod_gruppo_sconto,
				:ls_cod_valuta,
				:ls_cod_porto,
				:ls_cod_mezzo,
				:ls_cod_agente_1);
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in inserimento cliente nuovo " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
		rollback using sqlca;
		return 
	end if

	// Procedo con la duplicazione delle tabelle collegate
	// ====>>> ADDEBITI
	string ls_cod_addebito, ls_des_addebito,ls_cod_tipo_det_ven,  ls_flag_tipo_documento,ls_flag_mod_valore_bolle,ls_flag_mod_valore_fatture, ls_sql
	dec{4} ld_importo_addebito
	datetime ldt_data_inizio, ldt_data_fine
	
	ls_sql = " SELECT cod_addebito, des_addebito,   cod_tipo_det_ven,   cod_valuta,   data_inizio,   data_fine,   importo_addebito,   flag_tipo_documento,   flag_mod_valore_bolle,   	flag_mod_valore_fatture  FROM anag_clienti_addebiti WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql, it_sqlc_cg  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Addebiti clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret
		ls_cod_addebito = lds_data1.getitemstring(ll_y, 1)
		ls_des_addebito = lds_data1.getitemstring(ll_y, 2)   
		ls_cod_tipo_det_ven = lds_data1.getitemstring(ll_y, 3)
		ls_cod_valuta = lds_data1.getitemstring(ll_y, 4)
		ldt_data_inizio    = lds_data1.getitemdatetime(ll_y, 5)
		ldt_data_fine   = lds_data1.getitemdatetime(ll_y, 6)
		ld_importo_addebito = lds_data1.getitemdecimal(ll_y, 7)
		ls_flag_tipo_documento = lds_data1.getitemstring(ll_y, 8)
		ls_flag_mod_valore_bolle = lds_data1.getitemstring(ll_y, 9)
		ls_flag_mod_valore_fatture  = lds_data1.getitemstring(ll_y, 10)
	
		INSERT INTO anag_clienti_addebiti  
			( cod_azienda,   
			  cod_cliente,   
			  cod_addebito,   
			  des_addebito,   
			  cod_tipo_det_ven,   
			  cod_valuta,   
			  data_inizio,   
			  data_fine,   
			  importo_addebito,   
			  flag_tipo_documento,   
			  flag_mod_valore_bolle,   
			  flag_mod_valore_fatture )  
		values (:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_cod_addebito,   
				:ls_des_addebito,   
				:ls_cod_tipo_det_ven,   
				:ls_cod_valuta,   
				:ldt_data_inizio,   
				:ldt_data_fine,   
				:ld_importo_addebito,   
				:ls_flag_tipo_documento,   
				:ls_flag_mod_valore_bolle,   
				:ls_flag_mod_valore_fatture )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento addebiti cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data1
	
	
	
	// ===>>>>DESTINAZIONI CLIENTI
	
	string ls_cod_des_cliente, ls_flag_dest_merce_fat
	
	ls_sql = "SELECT cod_des_cliente,   rag_soc_1,  rag_soc_2,   indirizzo,   localita,   frazione,   cap,   provincia,   telefono,   fax,   telex,   cod_deposito,   flag_blocco,   data_blocco,   flag_dest_merce_fat  FROM anag_des_clienti    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data2, ls_sql, it_sqlc_cg  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Destinazioni clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_des_cliente = lds_data2.getitemstring(ll_y, 1)
		ls_rag_soc_1 = lds_data2.getitemstring(ll_y, 2)
		ls_rag_soc_2 = lds_data2.getitemstring(ll_y, 3)
		ls_indirizzo = lds_data2.getitemstring(ll_y, 4)
		ls_localita = lds_data2.getitemstring(ll_y, 5)
		ls_frazione = lds_data2.getitemstring(ll_y, 6)
		ls_cap = lds_data2.getitemstring(ll_y, 7)
		ls_provincia = lds_data2.getitemstring(ll_y, 8)
		ls_telefono = lds_data2.getitemstring(ll_y, 9)
		ls_fax = lds_data2.getitemstring(ll_y, 10)
		ls_telex = lds_data2.getitemstring(ll_y, 11)
		ls_flag_dest_merce_fat = lds_data2.getitemstring(ll_y, 12)
	
		INSERT INTO anag_des_clienti  
         ( cod_azienda,   
           cod_cliente,   
           cod_des_cliente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_deposito,   
           flag_blocco,   
           data_blocco,   
           flag_dest_merce_fat )  
		VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_cliente_new,   
           :ls_cod_des_cliente,   
           :ls_rag_soc_1,   
           :ls_rag_soc_2,   
           :ls_indirizzo,   
           :ls_localita,   
           :ls_frazione,   
           :ls_cap,   
           :ls_provincia,   
           :ls_telefono,   
           :ls_fax,   
           :ls_telex,   
           null,   
           'N',   
           null,   
           :ls_flag_dest_merce_fat )  ;
	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento destinazioni cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data2
	
	
	// =========>>> NOTE FISSE
	
	string ls_cod_nota_fissa, ls_des_nota_fissa,ls_flag_trattativa_ven, ls_flag_offerta_ven,  ls_flag_ordine_ven, ls_nota_fissa, &
			ls_flag_bolla_ven, ls_flag_fattura_ven, ls_flag_offerta_acq, ls_flag_ordine_acq, ls_flag_piede_testata, ls_flag_blocco, ls_flag_uso_gen_doc, &
			ls_flag_bolla_acq,ls_flag_fattura_acq,ls_flag_fattura_proforma,ls_cod_tipo_fat_ven,ls_flag_usa_stampa_doc, ls_flag_listino_vendita
	long ll_prog_mimetype
	
	ls_sql = " SELECT cod_nota_fissa,   des_nota_fissa,   nota_fissa,   data_inizio,   data_fine,   flag_trattativa_ven,   flag_offerta_ven,   flag_ordine_ven,   flag_bolla_ven,   flag_fattura_ven,   flag_offerta_acq,   flag_ordine_acq,   flag_piede_testata,   flag_blocco,   flag_uso_gen_doc,   flag_bolla_acq,   flag_fattura_acq,   flag_fattura_proforma,   cod_tipo_fat_ven,   flag_usa_stampa_doc,   flag_listino_vendita,   prog_mimetype  FROM tab_note_fisse    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data3, ls_sql, it_sqlc_cg  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE note fisse clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_nota_fissa = lds_data3.getitemstring(ll_y, 1)
		ls_des_nota_fissa = lds_data3.getitemstring(ll_y, 2)
		ls_nota_fissa = lds_data3.getitemstring(ll_y, 3)
		ldt_data_inizio = lds_data3.getitemdatetime(ll_y, 4)
		ldt_data_fine = lds_data3.getitemdatetime(ll_y, 5)
		ls_flag_trattativa_ven = lds_data3.getitemstring(ll_y,6)
		ls_flag_offerta_ven = lds_data3.getitemstring(ll_y,7)
		ls_flag_ordine_ven = lds_data3.getitemstring(ll_y, 8)
		ls_flag_bolla_ven = lds_data3.getitemstring(ll_y, 9)
		ls_flag_fattura_ven = lds_data3.getitemstring(ll_y, 10)
		ls_flag_offerta_acq = lds_data3.getitemstring(ll_y, 11)
		ls_flag_ordine_acq = lds_data3.getitemstring(ll_y, 12)
		ls_flag_piede_testata = lds_data3.getitemstring(ll_y, 13)
		ls_flag_blocco = lds_data3.getitemstring(ll_y, 14)
		ls_flag_uso_gen_doc = lds_data3.getitemstring(ll_y, 15)
		ls_flag_bolla_acq = lds_data3.getitemstring(ll_y, 16)
		ls_flag_fattura_acq = lds_data3.getitemstring(ll_y, 17)
		ls_flag_fattura_proforma = lds_data3.getitemstring(ll_y, 18)
		ls_cod_tipo_fat_ven = lds_data3.getitemstring(ll_y, 19)
		ls_flag_usa_stampa_doc = lds_data3.getitemstring(ll_y, 20)
		ls_flag_listino_vendita = lds_data3.getitemstring(ll_y, 21)
		ll_prog_mimetype	 = lds_data3.getitemnumber(ll_y, 22)
		
		ls_cod_nota_fissa = "2" + fill("0", 5 - len(ls_cod_nota_fissa)) + ls_cod_nota_fissa

	  INSERT INTO tab_note_fisse  
				( cod_azienda,   
				  cod_nota_fissa,   
				  des_nota_fissa,   
				  nota_fissa,   
				  cod_cliente,   
				  cod_fornitore,   
				  data_inizio,   
				  data_fine,   
				  flag_trattativa_ven,   
				  flag_offerta_ven,   
				  flag_ordine_ven,   
				  flag_bolla_ven,   
				  flag_fattura_ven,   
				  flag_offerta_acq,   
				  flag_ordine_acq,   
				  flag_piede_testata,   
				  flag_blocco,   
				  flag_uso_gen_doc,   
				  flag_bolla_acq,   
				  flag_fattura_acq,   
				  flag_fattura_proforma,   
				  cod_tipo_fat_ven,   
				  flag_usa_stampa_doc,   
				  flag_listino_vendita,   
				  prog_mimetype )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_nota_fissa,   
				  :ls_des_nota_fissa,   
				  :ls_nota_fissa,   
				  :ls_cod_cliente_new,   
				  null,   
				  :ldt_data_inizio,   
				  :ldt_data_fine,   
				  :ls_flag_trattativa_ven,   
				  :ls_flag_offerta_ven,   
				  :ls_flag_ordine_ven,   
				  :ls_flag_Bolla_ven,   
				  :ls_flag_fattura_ven,   
				  :ls_flag_offerta_acq,   
				  :ls_flag_ordine_acq,   
				  :ls_flag_piede_testata,   
				  :ls_flag_blocco,   
				  :ls_flag_uso_gen_doc,   
				  :ls_flag_bolla_acq,   
				  :ls_flag_fattura_acq,   
				  :ls_flag_fattura_proforma,   
				  :ls_cod_tipo_fat_Ven,   
				  :ls_flag_usa_stampa_doc,   
				  :ls_flag_listino_vendita,   
				  NULL )  ;

	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento note fisse cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data3
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1 + " ... Eseguita !!! - "+ ls_cod_cliente_new)
	Yield()
	
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata")



end event

type st_6 from statictext within w_gruppo_gibus
integer x = 946
integer y = 1112
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type st_5 from statictext within w_gruppo_gibus
integer x = 942
integer y = 900
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_9 from commandbutton within w_gruppo_gibus
integer x = 393
integer y = 900
integer width = 530
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "Prod-Fornitori"
end type

event clicked;string ls_sql,ls_cod_prodotto,ls_cod_fornitore,ls_cod_valuta, ls_des_listino_for,ls_flag_for_pref,ls_cod_misura, ls_cod_fornitore_new, &
		ls_cod_prod_fornitore, ls_des_prod_fornitore
long ll_rows, ll_i
decimal ld_quantita_1, ld_prezzo_ult_acquisto,ld_fat_conversione,ld_prezzo_1,ld_sconto_1,ld_quantita_2,ld_prezzo_2,ld_sconto_2,ld_quantita_3,ld_prezzo_3,ld_sconto_3,ld_quantita_4,ld_prezzo_4,ld_sconto_4,ld_quantita_5,ld_prezzo_5,ld_sconto_5
datetime ldt_data_inizio_val, ldt_data_ult_acquisto
datastore lds_prod_for

ls_sql = " SELECT cod_prodotto, cod_fornitore, cod_prod_fornitore, des_prod_fornitore from tab_prod_fornitori "

f_crea_datastore(lds_prod_for, ls_sql)
lds_prod_for.settransobject(it_sqlc_pt)

ll_rows = lds_prod_for.retrieve()

for ll_i = 1 to ll_rows

	ls_cod_prodotto = lds_prod_for.getitemstring(ll_i,1)
	ls_cod_fornitore = lds_prod_for.getitemstring(ll_i,2)
	ls_cod_prod_fornitore = lds_prod_for.getitemstring(ll_i,3)
	ls_des_prod_fornitore = lds_prod_for.getitemstring(ll_i,4)
	
	
	select cod_fornitore
	into :ls_cod_fornitore_new
	from anag_fornitori
	where cod_azienda =:s_cs_xx.cod_azienda and
			origine_anagrafica='PT' and origine_anagrafica_codice = :ls_cod_fornitore ;
			
	if sqlca.sqlcode = 100 then
		//messagebox("Import Listini", "Errore in ricerca fornitore progettotenda vecchio codice=" + ls_cod_fornitore )
		continue
	end if
	
	if sqlca.sqlcode <> 0 then
		messagebox("Import Listini", "Errore SCONOSCIUTO in ricerca fornitore progettotenda vecchio codice=" + ls_cod_fornitore  + "~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	st_5.text = ls_cod_prodotto + " - " + ls_cod_fornitore 
	
	
	INSERT INTO tab_prod_fornitori
         ( cod_azienda,   
           cod_prodotto,   
           cod_fornitore,
		cod_prod_fornitore,
		des_prod_fornitore)
	VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto,   
           :ls_cod_fornitore_new,   
		  :ls_cod_prod_fornitore,
		  :ls_des_prod_fornitore)  ;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Import Listini", "Errore SCONOSCIUTO in insert listino fornitore progettotenda vecchio codice=" + ls_cod_fornitore  + "~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
next

commit;

end event

type st_4 from statictext within w_gruppo_gibus
integer x = 942
integer y = 784
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_8 from commandbutton within w_gruppo_gibus
integer x = 393
integer y = 780
integer width = 530
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "Listini Acquisto"
end type

event clicked;string ls_sql,ls_cod_prodotto,ls_cod_fornitore,ls_cod_valuta, ls_des_listino_for,ls_flag_for_pref,ls_cod_misura, ls_cod_fornitore_new
long ll_rows, ll_i
decimal ld_quantita_1, ld_prezzo_ult_acquisto,ld_fat_conversione,ld_prezzo_1,ld_sconto_1,ld_quantita_2,ld_prezzo_2,ld_sconto_2,ld_quantita_3,ld_prezzo_3,ld_sconto_3,ld_quantita_4,ld_prezzo_4,ld_sconto_4,ld_quantita_5,ld_prezzo_5,ld_sconto_5
datetime ldt_data_inizio_val, ldt_data_ult_acquisto
datastore lds_listini_for

ls_sql = " SELECT cod_prodotto, cod_fornitore, cod_valuta, data_inizio_val, des_listino_for, quantita_1, prezzo_1, sconto_1, quantita_2, prezzo_2, sconto_2, quantita_3,  prezzo_3, sconto_3,  quantita_4, prezzo_4, sconto_4, quantita_5, prezzo_5,  sconto_5,  flag_for_pref,   prezzo_ult_acquisto,   data_ult_acquisto,   cod_misura,   fat_conversione  FROM listini_fornitori   "

f_crea_datastore(lds_listini_for, ls_sql)
lds_listini_for.settransobject(it_sqlc_pt)

ll_rows = lds_listini_for.retrieve()

for ll_i = 1 to ll_rows

	ls_cod_prodotto = lds_listini_for.getitemstring(ll_i,1)
	ls_cod_fornitore = lds_listini_for.getitemstring(ll_i,2)
	ls_cod_valuta = lds_listini_for.getitemstring(ll_i,3)
	ldt_data_inizio_val = lds_listini_for.getitemdatetime(ll_i,4)
	ls_des_listino_for = lds_listini_for.getitemstring(ll_i,5)
	ld_quantita_1 = lds_listini_for.getitemdecimal(ll_i,6)
	ld_prezzo_1 = lds_listini_for.getitemdecimal(ll_i,7)
	ld_sconto_1 = lds_listini_for.getitemdecimal(ll_i,8)
	ld_quantita_2 = lds_listini_for.getitemdecimal(ll_i,9)
	ld_prezzo_2 = lds_listini_for.getitemdecimal(ll_i,10)
	ld_sconto_2 = lds_listini_for.getitemdecimal(ll_i,11)
	ld_quantita_3 = lds_listini_for.getitemdecimal(ll_i,12)
	ld_prezzo_3 = lds_listini_for.getitemdecimal(ll_i,13)
	ld_sconto_3 = lds_listini_for.getitemdecimal(ll_i,14)
	ld_quantita_4 = lds_listini_for.getitemdecimal(ll_i,15)
	ld_prezzo_4 = lds_listini_for.getitemdecimal(ll_i,16)
	ld_sconto_4 = lds_listini_for.getitemdecimal(ll_i,17)
	ld_quantita_5 = lds_listini_for.getitemdecimal(ll_i,18)
	ld_prezzo_5 = lds_listini_for.getitemdecimal(ll_i,19)
	ld_sconto_5 = lds_listini_for.getitemdecimal(ll_i,20)
	ls_flag_for_pref = lds_listini_for.getitemstring(ll_i,21)
	ld_prezzo_ult_acquisto = lds_listini_for.getitemdecimal(ll_i,22)
	ldt_data_ult_acquisto = lds_listini_for.getitemdatetime(ll_i,23)
	ls_cod_misura = lds_listini_for.getitemstring(ll_i,24)
	ld_fat_conversione = lds_listini_for.getitemdecimal(ll_i,25)
	
	if ld_quantita_1 > 900000 then
		ld_quantita_1 = 9999999
	end if

	if ld_quantita_2 > 900000 then
		ld_quantita_2 = 9999999
	end if

	if ld_quantita_3 > 900000 then
		ld_quantita_3 = 9999999
	end if
	
	if ld_quantita_4 > 900000 then
		ld_quantita_4 = 9999999
	end if

	if ld_quantita_5 > 900000 then
		ld_quantita_5 = 9999999
	end if
	
	select cod_fornitore
	into :ls_cod_fornitore_new
	from anag_fornitori
	where cod_azienda =:s_cs_xx.cod_azienda and
			origine_anagrafica='PT' and origine_anagrafica_codice = :ls_cod_fornitore ;
			
	if sqlca.sqlcode = 100 then
		//messagebox("Import Listini", "Errore in ricerca fornitore progettotenda vecchio codice=" + ls_cod_fornitore )
		continue
	end if
	
	if sqlca.sqlcode <> 0 then
		messagebox("Import Listini", "Errore SCONOSCIUTO in ricerca fornitore progettotenda vecchio codice=" + ls_cod_fornitore  + "~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	st_4.text = ls_cod_prodotto + " - " + ls_cod_fornitore + " - " + string(ldt_data_inizio_val, "dd/mm/yyyy")
	
	
	INSERT INTO listini_fornitori  
         ( cod_azienda,   
           cod_prodotto,   
           cod_fornitore,   
           cod_valuta,   
           data_inizio_val,   
           des_listino_for,   
           quantita_1,   
           prezzo_1,   
           sconto_1,   
           quantita_2,   
           prezzo_2,   
           sconto_2,   
           quantita_3,   
           prezzo_3,   
           sconto_3,   
           quantita_4,   
           prezzo_4,   
           sconto_4,   
           quantita_5,   
           prezzo_5,   
           sconto_5,   
           flag_for_pref,   
           prezzo_ult_acquisto,   
           data_ult_acquisto,   
           cod_misura,   
           fat_conversione )  
	VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto,   
           :ls_cod_fornitore_new,   
           :ls_cod_valuta,   
           :ldt_data_inizio_val,   
           :ls_des_listino_for,   
           :ld_quantita_1,   
           :ld_prezzo_1,   
           :ld_sconto_1,   
           :ld_quantita_2,   
           :ld_prezzo_2,   
           :ld_sconto_2,   
           :ld_quantita_3,   
           :ld_prezzo_3,   
           :ld_sconto_3,   
           :ld_quantita_4,   
           :ld_prezzo_4,   
           :ld_sconto_4,   
           :ld_quantita_5,   
           :ld_prezzo_5,   
           :ld_sconto_5,   
           :ls_flag_for_pref,   
           :ld_prezzo_ult_acquisto,   
           :ldt_data_ult_acquisto,   
           :ls_cod_misura,   
           :ld_fat_conversione )  ;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Import Listini", "Errore SCONOSCIUTO in insert listino fornitore progettotenda vecchio codice=" + ls_cod_fornitore  + "~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
next

commit;

end event

type st_3 from statictext within w_gruppo_gibus
integer x = 942
integer y = 664
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' possibile eseguirlo senza problemi per un aggiornamento"
boolean focusrectangle = false
end type

type cb_7 from commandbutton within w_gruppo_gibus
integer x = 393
integer y = 660
integer width = 530
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "4 - Imp.Prodotti"
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto, ls_cod_cat_mer, ls_cod_misura_mag, ls_flag_blocco, ls_flag_articolo_fiscale, ls_cod_prodotto_alt, &
		ls_cod_misura_acq, ls_cod_fornitore, ls_cod_misura_ven, ls_cod_comodo, ls_cod_rifiuto, ls_flag_stato_rifiuto, ls_cod_prodotto_raggruppato, &
		ls_cod_fornitore_new, ls_sql, ls_errore, ls_cod_reparto, ls_cod_comodo_2, ls_cod_politica_riordino, ls_flag_materia_prima, ls_flag_classe_abc
long	ll_FileNum, ll_ret, ll_i, ll_cont,ll_cont_reparto
dec{5} ld_fat_conversione_ven, ld_fat_conversione_acq, ld_fat_conversione_rag_mag
datetime ldt_data_creazione, ldt_data_blocco
datastore lds_prodotti

lds_prodotti = create datastore
ls_sql = "select	 cod_prodotto, des_prodotto, data_creazione, cod_cat_mer, cod_misura_mag,	flag_blocco,	data_blocco, flag_articolo_fiscale,	cod_prodotto_alt,	cod_misura_acq,	fat_conversione_acq,	cod_fornitore,	cod_misura_ven,	fat_conversione_ven,	cod_comodo,	cod_rifiuto,	flag_stato_rifiuto,	cod_prodotto_raggruppato, fat_conversione_rag_mag, cod_reparto, cod_comodo_2, cod_tipo_politica_riordino, flag_materia_prima, flag_classe_abc from anag_prodotti where 	cod_azienda = 'A01' "

ls_sql = it_sqlc_pt.syntaxfromsql(ls_sql, "style(type=grid)", ls_errore)
if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	messagebox("f_crea_datastore", "Impossibile convertire stringa SQL per il datastore.~r~n" + ls_errore, StopSign!)
	return
end if

lds_prodotti.create(ls_sql, ls_errore)
if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_prodotti
	messagebox("f_crea_datastore", "Impossibile creare datastore.~r~n" + ls_errore, StopSign!)
	return
end if
lds_prodotti.settransobject(it_sqlc_pt)

ll_ret = lds_prodotti.retrieve()



ll_FileNum = FileOpen("C:\temp\prodotti.txt", LineMode!, Write!, LockWrite!, Replace!)


for ll_i = 1 to ll_ret
	
	Yield()
	
	ls_cod_prodotto = lds_prodotti.getitemstring(ll_i, 1)
	ls_des_prodotto = lds_prodotti.getitemstring(ll_i, 2)
	ldt_data_creazione = lds_prodotti.getitemdatetime(ll_i, 3)
	ls_cod_cat_mer = lds_prodotti.getitemstring(ll_i, 4) 
	ls_cod_misura_mag = lds_prodotti.getitemstring(ll_i, 5) 
	ls_flag_blocco = lds_prodotti.getitemstring(ll_i, 6)
	ldt_data_blocco = lds_prodotti.getitemdatetime(ll_i, 7)
	ls_flag_articolo_fiscale = lds_prodotti.getitemstring(ll_i, 8)
	ls_cod_prodotto_alt = lds_prodotti.getitemstring(ll_i, 9)
	ls_cod_misura_acq = lds_prodotti.getitemstring(ll_i, 10)
	ld_fat_conversione_acq = lds_prodotti.getitemnumber(ll_i, 11) 
	ls_cod_fornitore = lds_prodotti.getitemstring(ll_i, 12)
	ls_cod_misura_ven = lds_prodotti.getitemstring(ll_i, 13)
	ld_fat_conversione_ven = lds_prodotti.getitemnumber(ll_i, 14)
	ls_cod_comodo = lds_prodotti.getitemstring(ll_i, 15)
	ls_cod_rifiuto = lds_prodotti.getitemstring(ll_i, 16)
	ls_flag_stato_rifiuto = lds_prodotti.getitemstring(ll_i, 17)
	ls_cod_prodotto_raggruppato = lds_prodotti.getitemstring(ll_i, 18)
	ld_fat_conversione_rag_mag = lds_prodotti.getitemnumber(ll_i, 19)
	ls_cod_reparto = lds_prodotti.getitemstring(ll_i, 20)
	ls_cod_comodo_2 = lds_prodotti.getitemstring(ll_i, 21)
	ls_cod_politica_riordino = lds_prodotti.getitemstring(ll_i, 22)
	ls_flag_materia_prima = lds_prodotti.getitemstring(ll_i, 23)
	ls_flag_classe_abc = lds_prodotti.getitemstring(ll_i, 24)

	if not isnull(ls_cod_reparto) then
		select count(*)
		into :ll_cont_reparto
		from anag_reparti
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_reparto = :ls_cod_reparto;
		
		if ll_cont_reparto = 0 then
			setnull(ls_cod_reparto)
		end if
	end if
	
	st_3.text = ls_cod_prodotto + " - " + ls_des_prodotto
	
	setnull(ls_cod_fornitore_new)
	
	// conversione codice fornitore
	if not isnull(ls_cod_fornitore) then
		select cod_fornitore
		into   	:ls_cod_fornitore_new
		from	anag_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and
				 origine_anagrafica ='PT' and
				 origine_anagrafica_codice = :ls_cod_fornitore;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore fetch prodotti:~r~n" + it_sqlc_pt.sqlerrtext)
			rollback using sqlca;
			exit
		end if
	end if		 
	
	filewrite(	ll_FileNum , ls_cod_prodotto + "-" + ls_des_prodotto )
	
	select count(*)
	into :ll_cont
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore COUNT prodotti:~r~n" + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
		rollback using sqlca;
		exit
	end if
			
	if ll_cont = 0 then
		
		insert into anag_prodotti
			(cod_azienda,
			 cod_prodotto,
			 des_prodotto,
			 data_creazione,
			 cod_cat_mer,
			 cod_misura_mag,
			 flag_blocco,
			 data_blocco,
			 flag_articolo_fiscale,
			 cod_prodotto_alt,
			 cod_misura_acq,
			 fat_conversione_acq,
			 cod_fornitore,
			 cod_misura_ven,
			 fat_conversione_ven,
			 cod_comodo,
			 cod_rifiuto,
			 flag_stato_rifiuto,
			 cod_prodotto_raggruppato,
			 fat_conversione_rag_mag,
			cod_reparto,
			cod_comodo_2,
			cod_tipo_politica_riordino ,
			flag_materia_prima,
			flag_classe_abc )
			values (
			:s_cs_xx.cod_azienda,
			:ls_cod_prodotto, 
			:ls_des_prodotto, 
			:ldt_data_creazione, 
			:ls_cod_cat_mer, 
			:ls_cod_misura_mag, 
			:ls_flag_blocco, 
			:ldt_data_blocco, 
			:ls_flag_articolo_fiscale, 
			:ls_cod_prodotto_alt,
			:ls_cod_misura_acq, 
			:ld_fat_conversione_acq, 
			:ls_cod_fornitore_new,	 
			:ls_cod_misura_ven, 
			:ld_fat_conversione_ven, 
			:ls_cod_comodo, 
			:ls_cod_rifiuto, 
			:ls_flag_stato_rifiuto, 
			:ls_cod_prodotto_raggruppato,
			:ld_fat_conversione_rag_mag,
			:ls_cod_reparto,
			:ls_cod_comodo_2,
			:ls_cod_politica_riordino ,
			:ls_flag_materia_prima,
			:ls_flag_classe_abc	) ;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore insert prodotti:~r~n" + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			exit
		end if
		
	else
		// ATTENZIONE: la descrizione non va aggiornata.
		update anag_prodotti
		set data_creazione = :ldt_data_creazione, 
			 cod_cat_mer = :ls_cod_cat_mer,
			 cod_misura_mag = :ls_cod_misura_mag,
			 flag_blocco = :ls_flag_blocco,
			 data_blocco = :ldt_data_blocco, 
			 flag_articolo_fiscale = :ls_flag_articolo_fiscale,
			 cod_prodotto_alt = :ls_cod_prodotto_alt,
			 cod_misura_acq = :ls_cod_misura_acq,
			 fat_conversione_acq = :ld_fat_conversione_acq, 
			 cod_fornitore = :ls_cod_fornitore_new,
			 cod_misura_ven = :ls_cod_misura_ven,
			 fat_conversione_ven = :ld_fat_conversione_ven,
			 cod_comodo = :ls_cod_comodo,
			 cod_rifiuto = :ls_cod_rifiuto,
			 flag_stato_rifiuto = :ls_flag_stato_rifiuto, 
			 cod_prodotto_raggruppato = :ls_cod_prodotto_raggruppato,
			 fat_conversione_rag_mag = :ld_fat_conversione_rag_mag,
			 cod_reparto = :ls_cod_reparto,
			 cod_comodo_2 = :ls_cod_comodo_2,
			 cod_tipo_politica_riordino = :ls_cod_politica_riordino ,
			 flag_materia_prima = :ls_flag_materia_prima,
			 flag_classe_abc = :ls_flag_classe_abc			 
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore update prodotti:~r~n" + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
				rollback using sqlca;
				exit
			end if
			
		end if		
	
next

fileclose(ll_filenum)

commit;
rollback using it_sqlc_pt ;

end event

type st_2 from statictext within w_gruppo_gibus
integer x = 942
integer y = 544
integer width = 2094
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "E~' già stata eseguita"
boolean focusrectangle = false
end type

type cb_6 from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 540
integer width = 530
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "3 - Imp.Fornitori"
end type

event clicked;string ls_valore, ls_tipo, ls_origine_anagrafica,ls_origine_anagrafica_codice, ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
		ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_cod_fiscale, ls_partita_iva, ls_rif_interno, ls_flag_tipo_fornitore, &
		ls_cod_pagamento, ls_cod_pagamento_nuovo, ls_flag_certificato, ls_flag_tipo_certificazione, ls_flag_approvato, ls_flag_terzista, &
		ls_casella_mail, ls_sito_internet, ls_abi, ls_cab, ls_iban
decimal ld_valore
long ll_riga, ll_nuovo_codice
datetime ldt_data_creazione

ll_riga = 1
ll_nuovo_codice = 0

do while true
	
	ll_riga ++
	ll_nuovo_codice ++
	ls_cod_fornitore = string(ll_nuovo_codice, "000000")
	
	// leggo sede origine
	wf_get_value( "GENERALE", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_origine_anagrafica = upper(ls_valore)
	
	if isnull(ls_origine_anagrafica) or len(ls_origine_anagrafica) < 1 then exit
	
	// codice originale
	wf_get_value( "GENERALE", ll_riga, 2, ls_valore, ld_valore, ls_tipo)
	ls_origine_anagrafica_codice = upper(ls_valore)
	
	wf_get_value( "GENERALE", ll_riga, 3, ls_valore, ld_valore, ls_tipo)
	ls_rag_soc_1 = left(upper(ls_valore), 40)

	wf_get_value( "GENERALE", ll_riga, 4, ls_valore, ld_valore, ls_tipo)
	ls_rag_soc_2 = left(upper(ls_valore), 40)
	
	st_2.text = ls_origine_anagrafica + "-" + ls_origine_anagrafica_codice + "-" + ls_rag_soc_1
	
	Yield()

	wf_get_value( "GENERALE", ll_riga, 5, ls_valore, ld_valore, ls_tipo)
	ls_indirizzo = left(upper(ls_valore), 40)

	wf_get_value( "GENERALE", ll_riga, 6, ls_valore, ld_valore, ls_tipo)
	ls_frazione = left(upper(ls_valore), 40)

	wf_get_value( "GENERALE", ll_riga, 7, ls_valore, ld_valore, ls_tipo)
	ls_cap = left(upper(ls_valore), 5)

	wf_get_value( "GENERALE", ll_riga, 8, ls_valore, ld_valore, ls_tipo)
	ls_localita = left(upper(ls_valore), 40)

	wf_get_value( "GENERALE", ll_riga, 9, ls_valore, ld_valore, ls_tipo)
	ls_provincia = left(upper(ls_valore), 2)

	wf_get_value( "GENERALE", ll_riga, 10, ls_valore, ld_valore, ls_tipo)
	ls_telefono = left(upper(ls_valore), 20)

	wf_get_value( "GENERALE", ll_riga, 11, ls_valore, ld_valore, ls_tipo)
	ls_fax = left(upper(ls_valore), 20)

	choose case upper(ls_origine_anagrafica)
		case "VI"
			wf_get_value( "GENERALE", ll_riga, 12, ls_valore, ld_valore, ls_tipo)
			ls_telex = left(upper(ls_valore), 40)
		case "PT"
			wf_get_value( "GENERALE", ll_riga, 34, ls_valore, ld_valore, ls_tipo)
			ls_telex = left(upper(ls_valore), 40)
		case else
			setnull(ls_telex)
	end choose
			
	wf_get_value( "GENERALE", ll_riga, 13, ls_valore, ld_valore, ls_tipo)
	ls_cod_fiscale = left(upper(ls_valore), 16)

	wf_get_value( "GENERALE", ll_riga, 14, ls_valore, ld_valore, ls_tipo)
	ls_partita_iva = left(upper(ls_valore), 11)

	wf_get_value( "GENERALE", ll_riga, 16, ls_valore, ld_valore, ls_tipo)
	ls_rif_interno = left(upper(ls_valore), 30)

	wf_get_value( "GENERALE", ll_riga, 17, ls_valore, ld_valore, ls_tipo)
	ls_flag_tipo_fornitore = left(upper(ls_valore), 1)
	
	if isnull(ls_flag_tipo_fornitore) or len(trim(ls_flag_tipo_fornitore)) < 1 then
		if 		not isnull(ls_partita_iva) and len(trim(ls_partita_iva)) > 0 and not isnull(ls_cod_fiscale) and len(trim(ls_cod_fiscale)) > 0 and ls_partita_iva = ls_cod_fiscale then
			ls_flag_tipo_fornitore = "S" 
			
		elseif	not isnull(ls_partita_iva) and len(trim(ls_partita_iva)) > 0 and not isnull(ls_cod_fiscale) and len(trim(ls_cod_fiscale)) > 0 and ls_partita_iva <> ls_cod_fiscale then
			ls_flag_tipo_fornitore = "F" 

		elseif	not isnull(ls_partita_iva) and len(trim(ls_partita_iva)) > 0 and  (isnull(ls_cod_fiscale) or len(trim(ls_cod_fiscale)) < 1 ) then
			ls_flag_tipo_fornitore = "S" 
			
		elseif	not isnull(ls_cod_fiscale) and len(trim(ls_cod_fiscale)) > 0 and (isnull(ls_partita_iva) or len(trim(ls_partita_iva)) < 1 ) then
			ls_flag_tipo_fornitore = "C" 
		else
			ls_flag_tipo_fornitore = "S" 
		end if
	end if

	wf_get_value( "GENERALE", ll_riga, 18, ls_valore, ld_valore, ls_tipo)
	ls_cod_pagamento = left(upper(ls_valore), 3)
	
	if not isnull(ls_cod_pagamento) and len(ls_cod_pagamento) > 0 then

		choose case upper(ls_origine_anagrafica)
			case "PT"
				select codice
				into :ls_cod_pagamento_nuovo
				from  temp_pagamenti
				where for_ptenda = :ls_cod_pagamento;
				
				if sqlca.sqlcode <> 0 then
					f_scrivi_log(ls_origine_anagrafica + "-" + ls_origine_anagrafica_codice + "-" + ls_cod_fornitore + " -->> Pagamento " + ls_cod_pagamento + " non trovata la corrispondenza " )
				end if
				
			case "VI"
				select codice
				into :ls_cod_pagamento_nuovo
				from  temp_pagamenti
				where for_viropa = :ls_cod_pagamento;

				if sqlca.sqlcode <> 0 then
					f_scrivi_log(ls_origine_anagrafica + "-" + ls_origine_anagrafica_codice + "-" + ls_cod_fornitore + " -->> Pagamento " + ls_cod_pagamento + " non trovata la corrispondenza " )
				end if
				
			case "CG", "CG2"
				select codice
				into :ls_cod_pagamento_nuovo
				from  temp_pagamenti
				where for_cgibus = :ls_cod_pagamento;
				
				if sqlca.sqlcode <> 0 then
					f_scrivi_log(ls_origine_anagrafica + "-" + ls_origine_anagrafica_codice + "-" + ls_cod_fornitore + " -->> Pagamento " + ls_cod_pagamento + " non trovata la corrispondenza " )
				end if

			case "MO"
				
				
		end choose
		
	end if

	wf_get_value( "GENERALE", ll_riga, 22, ls_valore, ld_valore, ls_tipo)
	ls_flag_certificato = left(upper(ls_valore), 1)
	if ls_flag_certificato <> "S" or isnull(ls_flag_certificato) then ls_flag_certificato = "N"

	wf_get_value( "GENERALE", ll_riga, 23, ls_valore, ld_valore, ls_tipo)
	ls_flag_tipo_certificazione = left(upper(ls_valore), 1)
	if len(ls_flag_tipo_certificazione) < 1 or isnull(ls_flag_tipo_certificazione) then ls_flag_tipo_certificazione = "N"

	wf_get_value( "GENERALE", ll_riga, 24, ls_valore, ld_valore, ls_tipo)
	ls_flag_approvato = left(upper(ls_valore), 1)
	if ls_flag_approvato <> "S" or isnull(ls_flag_approvato) then ls_flag_approvato = "N"

	wf_get_value( "GENERALE", ll_riga, 25, ls_valore, ld_valore, ls_tipo)
	ls_flag_terzista = left(upper(ls_valore),1 )
	if ls_flag_terzista <> "S" or isnull(ls_flag_terzista) then ls_flag_terzista = "N"

	ldt_data_creazione = datetime( date( today() ) )
	
	wf_get_value( "GENERALE", ll_riga,30 , ls_valore, ld_valore, ls_tipo)
	ls_casella_mail = lower( left(upper(ls_valore), 40) )

	wf_get_value( "GENERALE", ll_riga,31 , ls_valore, ld_valore, ls_tipo)
	ls_sito_internet = lower( left(upper(ls_valore), 100) )

	if isnull(ls_rag_soc_2) or len(ls_rag_soc_2) < 1 then
		wf_get_value( "GENERALE", ll_riga,33 , ls_valore, ld_valore, ls_tipo)
		ls_rag_soc_2 = left(upper(ls_valore), 40)
	end if

	choose case upper(ls_origine_anagrafica)
			
		case "PT", "VI","CG", "CG2"
			wf_get_value( "GENERALE", ll_riga, 86 , ls_valore, ld_valore, ls_tipo)
			ls_abi = string( left(upper(ls_valore), 5))
			if len( trim(ls_abi) ) < 1 or isnull(ls_abi) then
				setnull(ls_abi)
			else
				ls_abi = fill("0", 5 - len( trim(ls_abi) ) ) + ls_abi
			end if
			
			wf_get_value( "GENERALE", ll_riga, 87 , ls_valore, ld_valore, ls_tipo)
			ls_cab = string( left(upper(ls_valore), 5), "00000")
			if len( trim(ls_cab) ) < 1 or isnull(ls_cab) then
				setnull(ls_cab)
			else
				ls_cab = fill("0", 5 - len( trim(ls_cab) ) ) + ls_cab
			end if

			wf_get_value( "GENERALE", ll_riga, 88 , ls_valore, ld_valore, ls_tipo)
			ls_iban = left(upper(ls_valore), 50)
			if len( trim(ls_iban) ) < 1 then setnull(ls_iban)
			
			if ls_abi = "00000" then setnull(ls_abi)
			if ls_cab = "00000" then setnull(ls_cab)
			

		case "MO"
			wf_get_value( "GENERALE", ll_riga, 90 , ls_valore, ld_valore, ls_tipo)
			ls_abi = string( left(upper(ls_valore), 5), "00000")
			if len( trim(ls_abi) ) < 1 or ls_abi = "0" or isnull(ls_abi) then 
				setnull(ls_abi)
			else
				ls_abi = fill("0", 5 - len( trim(ls_abi) ) ) + ls_abi
			end if
			
			wf_get_value( "GENERALE", ll_riga, 91 , ls_valore, ld_valore, ls_tipo)
			ls_cab = string( left(upper(ls_valore), 5), "00000")
			if len( trim(ls_cab) ) < 1 or ls_cab = "0" or isnull(ls_cab) then
				setnull(ls_cab)
			else
				ls_cab = fill("0", 5 - len( trim(ls_cab) ) ) + ls_cab
			end if
			
			if ls_abi = "00000" then setnull(ls_abi)
			if ls_cab = "00000" then setnull(ls_cab)
			
	end choose
	
	
	
	INSERT INTO anag_fornitori  
				( cod_azienda,   
				  cod_fornitore,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  cod_fiscale,   
				  partita_iva,   
				  rif_interno,   
				  flag_tipo_fornitore,   
				  cod_conto,   
				  cod_iva,   
				  num_prot_esenzione_iva,   
				  data_esenzione_iva,   
				  flag_sospensione_iva,   
				  cod_pagamento,   
				  giorno_fisso_scadenza,   
				  mese_esclusione_1,   
				  mese_esclusione_2,   
				  data_sostituzione_1,   
				  data_sostituzione_2,   
				  sconto,   
				  cod_tipo_listino_prodotto,   
				  fido,   
				  cod_banca_clien_for,   
				  conto_corrente,   
				  cod_lingua,   
				  cod_nazione,   
				  cod_area,   
				  cod_zona,   
				  cod_valuta,   
				  cod_categoria,   
				  cod_imballo,   
				  cod_porto,   
				  cod_resa,   
				  cod_mezzo,   
				  cod_vettore,   
				  cod_inoltro,   
				  cod_deposito,   
				  flag_certificato,   
				  flag_approvato,   
				  flag_strategico,   
				  flag_terzista,   
				  num_contratto,   
				  data_contratto,   
				  flag_ver_ispettiva,   
				  data_ver_ispettiva,   
				  note_ver_ispettiva,   
				  flag_omologato,   
				  flag_procedure_speciali,   
				  peso_val_servizio,   
				  peso_val_qualita,   
				  limite_tolleranza,   
				  flag_blocco,   
				  data_blocco,   
				  flag_tipo_certificazione,   
				  cod_piano_campionamento,   
				  casella_mail,   
				  nome_doc_qualificazione,   
				  sito_internet,   
				  cod_banca,   
				  data_creazione,   
				  data_modifica,   
				  blob_doc_qualif,   
				  cod_tipo_anagrafica,   
				  email_amministrazione,   
				  flag_accetta_email,   
				  giudizio_storico,   
				  data_giudizio_storico,   
				  giudizio_terzi,   
				  data_giudizio_terzi,   
				  riferimenti,   
				  cod_utente,   
				  cin,   
				  abi,   
				  cab,   
				  iban,   
				  cod_fornitore_adhoc,   
				  origine_anagrafica,   
				  origine_anagrafica_codice )  
	VALUES (:s_cs_xx.cod_azienda,   
				:ls_cod_fornitore,   
				:ls_rag_soc_1,   
				:ls_rag_soc_2,   
				:ls_indirizzo,   
				:ls_localita,   
				:ls_frazione,   
				:ls_cap,   
				:ls_provincia,   
				:ls_telefono,   
				:ls_fax,   
				:ls_telex,   
				:ls_cod_fiscale,   
				:ls_partita_iva,   
				:ls_rif_interno,   
				:ls_flag_tipo_fornitore,   
				null,   
				null,   
				null,   
				null,   
				'N',   
				:ls_cod_pagamento_nuovo ,   
				null,   
				null,   
				null,   
				null,   
				null,   
				0,   
				null,   
				0,   
				null,   
				null,   
				null,   
				null,   
				null,   
				null,   
				'EUR',   
				null,   
				null,   
				null,   
				null,   
				null,   
				null,   
				null,   
				'100',   
				:ls_flag_certificato,   
				:ls_flag_approvato,   
				'N',   
				:ls_flag_terzista,   
				null,   
				null,   
				'N',   
				null,   
				null,   
				'N',   
				'N',   
				0,   
				0,   
				0,   
				'N',   
				null,   
				:ls_flag_tipo_certificazione,   
				null,   
				:ls_casella_mail,   
				null,   
				:ls_sito_internet,   
				null,   
				:ldt_data_creazione,   
				null,   
				null,   
				null,   
				null,   
				'N',   
				null,   
				null,   
				null,   
				null,   
				null,   
				null,   
				null,   
				:ls_abi,   
				:ls_cab,   
				:ls_iban,   
				null,   
				:ls_origine_anagrafica,   
				:ls_origine_anagrafica_codice )  ;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in insert anag_fornitori.~r~n" + sqlca.sqlerrtext)
		rollback;
	end if
	
	setnull(ls_cod_fornitore)   
	setnull(ls_rag_soc_1)   
	setnull(ls_rag_soc_2)   
	setnull(ls_indirizzo)   
	setnull(ls_localita)   
	setnull(ls_frazione)   
	setnull(ls_cap)   
	setnull(ls_provincia)   
	setnull(ls_telefono)   
	setnull(ls_fax)   
	setnull(ls_telex)   
	setnull(ls_cod_fiscale)   
	setnull(ls_partita_iva)   
	setnull(ls_rif_interno)   
	setnull(ls_flag_tipo_fornitore)   
	setnull(ls_cod_pagamento_nuovo )   
	setnull(ls_flag_certificato)   
	setnull(ls_flag_approvato)   
	setnull(ls_flag_terzista)   
	setnull(ls_flag_tipo_certificazione)   
	setnull(ls_casella_mail)   
	setnull(ldt_data_creazione)   
	setnull(ls_abi)   
	setnull(ls_cab)   
	setnull(ls_iban)   
	setnull(ls_origine_anagrafica)   
	setnull(ls_origine_anagrafica_codice )
	setnull(ls_sito_internet)
	
	
loop

messagebox("IMPORT", "Elaborato " + string(ll_riga - 1) + " Righe del foglio EXCEL")
commit;

st_2.text = "Elaborazione terminata."
end event

type cb_5 from commandbutton within w_gruppo_gibus
integer x = 46
integer y = 2564
integer width = 640
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "8 - Close Transactions"
end type

event clicked;commit using it_sqlc_pt;
destroy it_sqlc_pt;

commit using it_sqlc_cg;
destroy it_sqlc_cg;

commit using it_sqlc_vr;
destroy it_sqlc_vr;

commit using it_sqlc_mc;
destroy it_sqlc_mc;


end event

type st_mc from statictext within w_gruppo_gibus
integer x = 1335
integer y = 160
integer width = 398
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
long textcolor = 33554432
long backcolor = 65280
string text = "MC -on line"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_vr from statictext within w_gruppo_gibus
integer x = 905
integer y = 156
integer width = 398
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
long textcolor = 33554432
long backcolor = 65280
string text = "VR-on line"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_cg from statictext within w_gruppo_gibus
integer x = 466
integer y = 156
integer width = 398
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
long textcolor = 33554432
long backcolor = 65280
string text = "CG-on line"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_pt from statictext within w_gruppo_gibus
integer x = 27
integer y = 156
integer width = 398
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean underline = true
long textcolor = 33554432
long backcolor = 65280
string text = "PT-on line"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_gruppo_gibus
integer x = 46
integer y = 2696
integer width = 640
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "9 - Chiudi Istanza File"
end type

event clicked;iole_excel = CREATE OLEObject
end event

type st_1 from statictext within w_gruppo_gibus
integer x = 27
integer y = 20
integer width = 3003
integer height = 124
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16777215
long backcolor = 0
string text = "Utility di gestione dati globali per fusione tabelle e anagrafiche gruppo Gibus"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_gruppo_gibus
integer x = 18
integer y = 388
integer width = 535
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 - Istanza File"
end type

event clicked;long ll_errore, ll_count, ll_index

//inizio
iole_excel = CREATE OLEObject
ll_errore = iole_excel.ConnectToNewObject("excel.application")

if ll_errore < 0 then
	messagebox("OMNIA","Si è verificato un errore durante la chiamata a MsExcel.~r~nContattare il servizio di assistenza")
	return -1
end if

iole_excel.Application.Workbooks.Open(is_path_file)

iole_excel.application.visible = true


return 0
end event

type cb_2 from commandbutton within w_gruppo_gibus
integer x = 2816
integer y = 268
integer width = 233
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "File"
end type

event clicked;string		ls_path, docpath, docname[]
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", 	+ "Files Excel (*.XLS),*.XLS,", 	ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	st_file.text = docpath
	is_path_file = docpath
end if
end event

type st_file from statictext within w_gruppo_gibus
integer x = 18
integer y = 276
integer width = 2770
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Selezionare il file da elaborare"
boolean focusrectangle = false
end type

type cb_clienti_pd from commandbutton within w_gruppo_gibus
integer x = 398
integer y = 1104
integer width = 535
integer height = 88
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
boolean enabled = false
string text = "2 - Clienti PTENDA"
end type

event clicked;string	ls_cod_cliente_new, ls_valore, ls_tipo, ls_cod_cliente_old, &
ls_rag_soc_1, ls_rag_soc_2,ls_indirizzo, ls_localita,ls_frazione,ls_cap,ls_provincia,ls_telefono,ls_fax,ls_telex,ls_cod_fiscale,ls_partita_iva,ls_rif_interno,ls_flag_tipo_cliente,ls_cod_iva,ls_num_prot_esenzione, &
ls_flag_sospensione_iva,ls_cod_pagamento_old, ls_cod_pagamento_new,ls_cod_tipo_listino_cliente,&
ls_flag_fuori_fido,ls_banca_clien_for,ls_conto_corrente,ls_casella_mail,ls_sito_internet,ls_cod_banca_nostra,ls_rag_soc_abbreviata,ls_flag_raggruppo_scadenze,ls_cod_tipo_anagrafica,&
ls_flag_accetta_mail,ls_email_amministrazione,ls_cin,ls_abi,ls_cab,ls_iban,ls_flag_invia_aut_ddt,ls_flag_invia_aut_fat, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, &
ls_cod_banca_clien_for_new, ls_errore, ls_cod_capoconto, ls_cod_deposito, ls_cod_valuta, ls_cod_gruppo_sconto, ls_cod_porto, ls_cod_mezzo, ls_cod_agente_1

long	ll_riga,ll_nuovo_codice,ll_giorno_fisso_scadenza,ll_mese_escl_1,ll_mese_escl_2,ll_data_sost_1,ll_data_sost_2, ll_ret, ll_y

dec{4} ld_fido ,ld_sconto

decimal ld_valore

datetime ldt_data_esenzione_iva

datastore lds_data1, lds_data2,lds_data3



ll_riga = 1
ll_nuovo_codice = 0

do while true	
	ll_riga ++
	ll_nuovo_codice ++
	
	// leggo sede origine
	wf_get_value( "CLIENTI", ll_riga, 1, ls_valore, ld_valore, ls_tipo)
	ls_cod_cliente_old = upper(ls_valore)
	
	if isnull(ls_cod_cliente_old) or ls_cod_cliente_old="" then exit
	
	// ========= DATI DA IMPOSTARE PER OGNI IMPERTAZIONE =============
	// Creo il codice fornitore con il pre-codice corretto; 1=PD  2=VEGG  3=PT  4=CU
	ls_cod_cliente_new = "10" + ls_cod_cliente_old
	// Capoconto 01=PD   02=Vegg  03=PT  04=CU   05=Estero  10=Privati
	ls_cod_capoconto = "01"
	// Deposito appartenenza
	ls_cod_deposito = "100"
	// Valuta
	ls_cod_valuta ='EUR'
	// Linea di sconto
	ls_cod_gruppo_sconto = "001"
	//=======================================================

	SELECT rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			telefono,   
			fax,   
			telex,   
			cod_fiscale,   
			partita_iva,   
			rif_interno,   
			flag_tipo_cliente,   
			cod_iva,   
			num_prot_esenzione_iva,   
			data_esenzione_iva,   
			flag_sospensione_iva,   
			cod_pagamento,   
			giorno_fisso_scadenza,   
			mese_esclusione_1,   
			mese_esclusione_2,   
			data_sostituzione_1,   
			data_sostituzione_2,   
			sconto,   
			cod_tipo_listino_prodotto,   
			fido,   
			flag_fuori_fido,   
			cod_banca_clien_for,   
			conto_corrente,   
			casella_mail,   
			sito_internet,   
			cod_banca,   
			rag_soc_abbreviata,   
			flag_raggruppo_scadenze,   
			cod_tipo_anagrafica,   
			flag_accetta_mail,   
			email_amministrazione,   
			cin,   
			abi,   
			cab,   
			iban,   
			flag_invia_aut_ddt,   
			flag_invia_aut_fat,
			cod_porto,
			cod_mezzo,
			cod_agente_1
	INTO :ls_rag_soc_1,   
			:ls_rag_soc_2,   
			:ls_indirizzo,   
			:ls_localita,   
			:ls_frazione,   
			:ls_cap,   
			:ls_provincia,   
			:ls_telefono,   
			:ls_fax,   
			:ls_telex,   
			:ls_cod_fiscale,   
			:ls_partita_iva,   
			:ls_rif_interno,   
			:ls_flag_tipo_cliente,   
			:ls_cod_iva,   
			:ls_num_prot_esenzione,   
			:ldt_data_esenzione_iva,   
			:ls_flag_sospensione_iva,   
			:ls_cod_pagamento_old,   
			:ll_giorno_fisso_scadenza,   
			:ll_mese_escl_1,   
			:ll_mese_escl_2,   
			:ll_data_sost_1,   
			:ll_data_sost_2,   
			:ld_sconto,   
			:ls_cod_tipo_listino_cliente,   
			:ld_fido,   
			:ls_flag_fuori_fido,   
			:ls_banca_clien_for,   
			:ls_conto_corrente,   
			:ls_casella_mail,   
			:ls_sito_internet,   
			:ls_cod_banca_nostra,   
			:ls_rag_soc_abbreviata,   
			:ls_flag_raggruppo_scadenze,   
			:ls_cod_tipo_anagrafica,   
			:ls_flag_accetta_mail,   
			:ls_email_amministrazione,   
			:ls_cin,   
			:ls_abi,   
			:ls_cab,   
			:ls_iban,   
			:ls_flag_invia_aut_ddt,   
			:ls_flag_invia_aut_fat,
			:ls_cod_porto,
			:ls_cod_mezzo,
			:ls_cod_agente_1
	FROM anag_clienti 
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente_old
	using it_sqlc_pt ;

	if it_sqlc_pt.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in ricerca cliente vecchio " + ls_cod_cliente_old + "~r~n" + it_sqlc_pt.sqlerrtext)
		rollback using sqlca;
		return 
	end if
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1)
	Yield()

	
	/* elaborazioni   */
	// pagamenti
	setnull(ls_cod_pagamento_new)
	
	if not isnull(ls_cod_pagamento_old) then
		select	codice
		into 	:ls_cod_pagamento_new
		FROM temp_pagamenti
		where cli_ptenda = :ls_cod_pagamento_old;
	
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Il pagamento " + ls_cod_pagamento_old + " non ha trovato alcuna corrispondenza nel DB Gibus~r~nPRENDERE NOTA DEL CLIENTE!!!~r~n" + sqlca.sqlerrtext)
			setnull(ls_cod_pagamento_new)
		end if
	end if
	// banche di appoggio
	
	setnull(ls_cod_banca_clien_for_new)
	setnull(ls_cod_abi_new)
	setnull(ls_cod_cab_new)
	setnull(ls_cod_cin_new)
	
	ll_ret = wf_get_banca_appoggio(ls_abi, ls_cab, ls_cin, ls_iban, ls_banca_clien_for, ls_cod_abi_new, ls_cod_cab_new, ls_cod_cin_new, ls_cod_banca_clien_for_new, ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("APICE", "Errore in ricerca/creazione banca appoggio~r~n" + ls_errore)
		rollback using sqlca;
		return 
	end if
		
	// banca NOSTRA lasciamo stare.
	
	// Impostazione del codice capoconto
	choose case upper(ls_flag_tipo_cliente)
		case "C", "E"		// ESTERO
			ls_cod_capoconto = "05"
		case "P"				// PRIVATI   (di cosa ?)
			ls_cod_capoconto = "10"
	end choose
	
	
	INSERT INTO anag_clienti
			(	cod_azienda,
				cod_cliente,
				rag_soc_1,   
				rag_soc_2,   
				indirizzo,   
				localita,   
				frazione,   
				cap,   
				provincia,   
				telefono,   
				fax,   
				telex,   
				cod_fiscale,   
				partita_iva,   
				rif_interno,   
				flag_tipo_cliente,   
				cod_iva,   
				num_prot_esenzione_iva,   
				data_esenzione_iva,   
				flag_sospensione_iva,   
				cod_pagamento,   
				giorno_fisso_scadenza,   
				mese_esclusione_1,   
				mese_esclusione_2,   
				data_sostituzione_1,   
				data_sostituzione_2,   
				sconto,   
				cod_tipo_listino_prodotto,   
				fido,   
				flag_fuori_fido,   
				cod_banca_clien_for,   
				conto_corrente,   
				casella_mail,   
				sito_internet,   
				cod_banca,   
				rag_soc_abbreviata,   
				flag_raggruppo_scadenze,   
				cod_tipo_anagrafica,   
				flag_accetta_mail,   
				email_amministrazione,   
				cin,   
				abi,   
				cab,   
				iban,   
				flag_invia_aut_ddt,   
				flag_invia_aut_fat,
				origine_anagrafica_codice,
				cod_capoconto,
				cod_deposito,
				cod_gruppo_sconto,
				cod_valuta,
				cod_porto,
				cod_mezzo,
				cod_agente_1)
			VALUES
	(			:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_rag_soc_1,   
				:ls_rag_soc_2,   
				:ls_indirizzo,   
				:ls_localita,   
				:ls_frazione,   
				:ls_cap,   
				:ls_provincia,   
				:ls_telefono,   
				:ls_fax,   
				:ls_telex,   
				:ls_cod_fiscale,   
				:ls_partita_iva,   
				:ls_rif_interno,   
				:ls_flag_tipo_cliente,   
				:ls_cod_iva,   
				:ls_num_prot_esenzione,   
				:ldt_data_esenzione_iva,   
				:ls_flag_sospensione_iva,   
				:ls_cod_pagamento_new,   
				:ll_giorno_fisso_scadenza,   
				:ll_mese_escl_1,   
				:ll_mese_escl_2,   
				:ll_data_sost_1,   
				:ll_data_sost_2,   
				:ld_sconto,   
				:ls_cod_tipo_listino_cliente,   
				:ld_fido,   
				:ls_flag_fuori_fido,   
				:ls_cod_banca_clien_for_new,   
				:ls_conto_corrente,   
				:ls_casella_mail,   
				:ls_sito_internet,   
				null,   
				:ls_rag_soc_abbreviata,   
				:ls_flag_raggruppo_scadenze,   
				:ls_cod_tipo_anagrafica,   
				:ls_flag_accetta_mail,   
				:ls_email_amministrazione,   
				:ls_cod_cin_new,   
				:ls_cod_abi_new,   
				:ls_cod_cab_new,   
				:ls_iban,   
				:ls_flag_invia_aut_ddt,   
				:ls_flag_invia_aut_fat,
				:ls_cod_cliente_old,
				:ls_cod_capoconto,
				:ls_cod_deposito,
				:ls_cod_gruppo_sconto,
				:ls_cod_valuta,
				:ls_cod_porto,
				:ls_cod_mezzo,
				:ls_cod_agente_1);
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("IMPORT", "Errore in inserimento cliente nuovo " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
		rollback using sqlca;
		return 
	end if

	// Procedo con la duplicazione delle tabelle collegate
	// ====>>> ADDEBITI
	string ls_cod_addebito, ls_des_addebito,ls_cod_tipo_det_ven,  ls_flag_tipo_documento,ls_flag_mod_valore_bolle,ls_flag_mod_valore_fatture, ls_sql
	dec{4} ld_importo_addebito
	datetime ldt_data_inizio, ldt_data_fine
	
	ls_sql = " SELECT cod_addebito, des_addebito,   cod_tipo_det_ven,   cod_valuta,   data_inizio,   data_fine,   importo_addebito,   flag_tipo_documento,   flag_mod_valore_bolle,   	flag_mod_valore_fatture  FROM anag_clienti_addebiti WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql, it_sqlc_pt  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Addebiti clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret
		ls_cod_addebito = lds_data1.getitemstring(ll_y, 1)
		ls_des_addebito = lds_data1.getitemstring(ll_y, 2)   
		ls_cod_tipo_det_ven = lds_data1.getitemstring(ll_y, 3)
		ls_cod_valuta = lds_data1.getitemstring(ll_y, 4)
		ldt_data_inizio    = lds_data1.getitemdatetime(ll_y, 5)
		ldt_data_fine   = lds_data1.getitemdatetime(ll_y, 6)
		ld_importo_addebito = lds_data1.getitemdecimal(ll_y, 7)
		ls_flag_tipo_documento = lds_data1.getitemstring(ll_y, 8)
		ls_flag_mod_valore_bolle = lds_data1.getitemstring(ll_y, 9)
		ls_flag_mod_valore_fatture  = lds_data1.getitemstring(ll_y, 10)
	
		INSERT INTO anag_clienti_addebiti  
			( cod_azienda,   
			  cod_cliente,   
			  cod_addebito,   
			  des_addebito,   
			  cod_tipo_det_ven,   
			  cod_valuta,   
			  data_inizio,   
			  data_fine,   
			  importo_addebito,   
			  flag_tipo_documento,   
			  flag_mod_valore_bolle,   
			  flag_mod_valore_fatture )  
		values (:s_cs_xx.cod_azienda,
				:ls_cod_cliente_new,
				:ls_cod_addebito,   
				:ls_des_addebito,   
				:ls_cod_tipo_det_ven,   
				:ls_cod_valuta,   
				:ldt_data_inizio,   
				:ldt_data_fine,   
				:ld_importo_addebito,   
				:ls_flag_tipo_documento,   
				:ls_flag_mod_valore_bolle,   
				:ls_flag_mod_valore_fatture )  ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento addebiti cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data1
	
	
	
	// ===>>>>DESTINAZIONI CLIENTI
	
	string ls_cod_des_cliente, ls_flag_dest_merce_fat
	
	ls_sql = "SELECT cod_des_cliente,   rag_soc_1,  rag_soc_2,   indirizzo,   localita,   frazione,   cap,   provincia,   telefono,   fax,   telex,   cod_deposito,   flag_blocco,   data_blocco,   flag_dest_merce_fat  FROM anag_des_clienti    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data2, ls_sql, it_sqlc_pt  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE Destinazioni clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_des_cliente = lds_data2.getitemstring(ll_y, 1)
		ls_rag_soc_1 = lds_data2.getitemstring(ll_y, 2)
		ls_rag_soc_2 = lds_data2.getitemstring(ll_y, 3)
		ls_indirizzo = lds_data2.getitemstring(ll_y, 4)
		ls_localita = lds_data2.getitemstring(ll_y, 5)
		ls_frazione = lds_data2.getitemstring(ll_y, 6)
		ls_cap = lds_data2.getitemstring(ll_y, 7)
		ls_provincia = lds_data2.getitemstring(ll_y, 8)
		ls_telefono = lds_data2.getitemstring(ll_y, 9)
		ls_fax = lds_data2.getitemstring(ll_y, 10)
		ls_telex = lds_data2.getitemstring(ll_y, 11)
		ls_flag_dest_merce_fat = lds_data2.getitemstring(ll_y, 12)
	
		INSERT INTO anag_des_clienti  
         ( cod_azienda,   
           cod_cliente,   
           cod_des_cliente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_deposito,   
           flag_blocco,   
           data_blocco,   
           flag_dest_merce_fat )  
		VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_cliente_new,   
           :ls_cod_des_cliente,   
           :ls_rag_soc_1,   
           :ls_rag_soc_2,   
           :ls_indirizzo,   
           :ls_localita,   
           :ls_frazione,   
           :ls_cap,   
           :ls_provincia,   
           :ls_telefono,   
           :ls_fax,   
           :ls_telex,   
           null,   
           'N',   
           null,   
           :ls_flag_dest_merce_fat )  ;
	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento destinazioni cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data2
	
	
	// =========>>> NOTE FISSE
	
	string ls_cod_nota_fissa, ls_des_nota_fissa,ls_flag_trattativa_ven, ls_flag_offerta_ven,  ls_flag_ordine_ven, ls_nota_fissa, &
			ls_flag_bolla_ven, ls_flag_fattura_ven, ls_flag_offerta_acq, ls_flag_ordine_acq, ls_flag_piede_testata, ls_flag_blocco, ls_flag_uso_gen_doc, &
			ls_flag_bolla_acq,ls_flag_fattura_acq,ls_flag_fattura_proforma,ls_cod_tipo_fat_ven,ls_flag_usa_stampa_doc, ls_flag_listino_vendita
	long ll_prog_mimetype
	
	ls_sql = " SELECT cod_nota_fissa,   des_nota_fissa,   nota_fissa,   data_inizio,   data_fine,   flag_trattativa_ven,   flag_offerta_ven,   flag_ordine_ven,   flag_bolla_ven,   flag_fattura_ven,   flag_offerta_acq,   flag_ordine_acq,   flag_piede_testata,   flag_blocco,   flag_uso_gen_doc,   flag_bolla_acq,   flag_fattura_acq,   flag_fattura_proforma,   cod_tipo_fat_ven,   flag_usa_stampa_doc,   flag_listino_vendita,   prog_mimetype  FROM tab_note_fisse    WHERE	cod_azienda = '" + s_cs_xx.cod_azienda + "' and	 cod_cliente = '" + ls_cod_cliente_old + "' "
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data3, ls_sql, it_sqlc_pt  , ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("IMPORT", "Errore in creazione DATASTORE note fisse clienti " + ls_errore)
		rollback using sqlca;
		return 
	end if
	
	for ll_y = 1 to ll_ret

		ls_cod_nota_fissa = lds_data3.getitemstring(ll_y, 1)
		ls_des_nota_fissa = lds_data3.getitemstring(ll_y, 2)
		ls_nota_fissa = lds_data3.getitemstring(ll_y, 3)
		ldt_data_inizio = lds_data3.getitemdatetime(ll_y, 4)
		ldt_data_fine = lds_data3.getitemdatetime(ll_y, 5)
		ls_flag_trattativa_ven = lds_data3.getitemstring(ll_y,6)
		ls_flag_offerta_ven = lds_data3.getitemstring(ll_y,7)
		ls_flag_ordine_ven = lds_data3.getitemstring(ll_y, 8)
		ls_flag_bolla_ven = lds_data3.getitemstring(ll_y, 9)
		ls_flag_fattura_ven = lds_data3.getitemstring(ll_y, 10)
		ls_flag_offerta_acq = lds_data3.getitemstring(ll_y, 11)
		ls_flag_ordine_acq = lds_data3.getitemstring(ll_y, 12)
		ls_flag_piede_testata = lds_data3.getitemstring(ll_y, 13)
		ls_flag_blocco = lds_data3.getitemstring(ll_y, 14)
		ls_flag_uso_gen_doc = lds_data3.getitemstring(ll_y, 15)
		ls_flag_bolla_acq = lds_data3.getitemstring(ll_y, 16)
		ls_flag_fattura_acq = lds_data3.getitemstring(ll_y, 17)
		ls_flag_fattura_proforma = lds_data3.getitemstring(ll_y, 18)
		ls_cod_tipo_fat_ven = lds_data3.getitemstring(ll_y, 19)
		ls_flag_usa_stampa_doc = lds_data3.getitemstring(ll_y, 20)
		ls_flag_listino_vendita = lds_data3.getitemstring(ll_y, 21)
		ll_prog_mimetype	 = lds_data3.getitemnumber(ll_y, 22)

	  INSERT INTO tab_note_fisse  
				( cod_azienda,   
				  cod_nota_fissa,   
				  des_nota_fissa,   
				  nota_fissa,   
				  cod_cliente,   
				  cod_fornitore,   
				  data_inizio,   
				  data_fine,   
				  flag_trattativa_ven,   
				  flag_offerta_ven,   
				  flag_ordine_ven,   
				  flag_bolla_ven,   
				  flag_fattura_ven,   
				  flag_offerta_acq,   
				  flag_ordine_acq,   
				  flag_piede_testata,   
				  flag_blocco,   
				  flag_uso_gen_doc,   
				  flag_bolla_acq,   
				  flag_fattura_acq,   
				  flag_fattura_proforma,   
				  cod_tipo_fat_ven,   
				  flag_usa_stampa_doc,   
				  flag_listino_vendita,   
				  prog_mimetype )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_nota_fissa,   
				  :ls_des_nota_fissa,   
				  :ls_nota_fissa,   
				  :ls_cod_cliente_new,   
				  null,   
				  :ldt_data_inizio,   
				  :ldt_data_fine,   
				  :ls_flag_trattativa_ven,   
				  :ls_flag_offerta_ven,   
				  :ls_flag_ordine_ven,   
				  :ls_flag_Bolla_ven,   
				  :ls_flag_fattura_ven,   
				  :ls_flag_offerta_acq,   
				  :ls_flag_ordine_acq,   
				  :ls_flag_piede_testata,   
				  :ls_flag_blocco,   
				  :ls_flag_uso_gen_doc,   
				  :ls_flag_bolla_acq,   
				  :ls_flag_fattura_acq,   
				  :ls_flag_fattura_proforma,   
				  :ls_cod_tipo_fat_Ven,   
				  :ls_flag_usa_stampa_doc,   
				  :ls_flag_listino_vendita,   
				  :ll_prog_mimetype )  ;

	

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("IMPORT", "Errore in inserimento note fisse cliente " + ls_cod_cliente_new + "~r~n" + sqlca.sqlerrtext)
			rollback using sqlca;
			return 
		end if
	next
	
	destroy lds_data3
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + "...Importazione Cliente " + ls_cod_cliente_old + " " + ls_rag_soc_1 + " ... Eseguita !!! - "+ ls_cod_cliente_new)
	Yield()
	
	
loop

commit;

g_mb.messagebox("IMPORT", "Importazione Terminata")



end event

