﻿$PBExportHeader$w_import_clienti_mast.srw
forward
global type w_import_clienti_mast from w_std_principale
end type
type st_progres from statictext within w_import_clienti_mast
end type
type sle_file from singlelineedit within w_import_clienti_mast
end type
type mle_log from multilineedit within w_import_clienti_mast
end type
type cb_1 from commandbutton within w_import_clienti_mast
end type
end forward

global type w_import_clienti_mast from w_std_principale
integer width = 2597
integer height = 2540
st_progres st_progres
sle_file sle_file
mle_log mle_log
cb_1 cb_1
end type
global w_import_clienti_mast w_import_clienti_mast

on w_import_clienti_mast.create
int iCurrent
call super::create
this.st_progres=create st_progres
this.sle_file=create sle_file
this.mle_log=create mle_log
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_progres
this.Control[iCurrent+2]=this.sle_file
this.Control[iCurrent+3]=this.mle_log
this.Control[iCurrent+4]=this.cb_1
end on

on w_import_clienti_mast.destroy
call super::destroy
destroy(this.st_progres)
destroy(this.sle_file)
destroy(this.mle_log)
destroy(this.cb_1)
end on

type st_progres from statictext within w_import_clienti_mast
integer x = 2126
integer y = 440
integer width = 389
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = " ----"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_file from singlelineedit within w_import_clienti_mast
integer x = 27
integer y = 4
integer width = 2501
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_import_clienti_mast
integer x = 23
integer y = 560
integer width = 2491
integer height = 1840
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_import_clienti_mast
integer x = 983
integer y = 140
integer width = 667
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Aggiorna Anagrafiche"
end type

event clicked;string		ls_str, ls_full_path, ls_docname, ls_cod_cliente, ls_rag_soc_1, ls_indirizzo, ls_cap,ls_localiita,ls_provincia,ls_telefono,ls_fax,ls_email,ls_tipo_cliente,ls_zonal,ls_cod_pagamento, ls_des_pagamento
long			ll_file, ll_ret, ll_pos
integer		li_count, ll_
//dec{4}		
boolean		lb_inserisci


li_count = GetFileOpenName("Select File", ls_full_path, ls_docname, "text","Text,*.txt;*.csv", "C:")
	
if li_count<0 then
	g_mb.error("Errore in selezione file!")
	return
elseif li_count=0 then
	//annullato dall'operatore
	return
end if

sle_file.text = ls_full_path


ll_file = fileopen(sle_file.text)
if ll_file < 0 then
	g_mb.error("Errore in apertura del file indicato")
	return
end if

//formato file di testo
//COD_PRODOTTO<tabulatore>COD_NOMENCLATURA

mle_log.text += "~r~n~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"
mle_log.text += "INIZIO ELABORAZIONE"+string(now(),"hh:mm:ss")+"~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"

li_count = 0

do while true
	ll_ret = fileread(ll_file, ls_str)
	if ll_ret < 0 then exit
	
	
	ll_pos = pos(ls_str, "~t")
	ls_cod_cliente = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_rag_soc_1 = left(ls_str, ll_pos -1)
	ls_rag_soc_1 = left(ls_rag_soc_1,40)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_indirizzo = left(ls_str, ll_pos -1)
	ls_indirizzo = left(ls_indirizzo, 40)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_cap = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_localiita = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_provincia = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_telefono = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_fax = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_email = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_tipo_cliente = left(ls_str, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_zonal = left(ls_str, ll_pos -1)
	ls_zonal = right(ls_zonal,3)
	
	insert into tab_zone
	(cod_azienda, 	cod_zona, des_zona)
	values
	(:s_cs_xx.cod_azienda, :ls_zonal, 'NUOVA ZONA');
	
	ls_str = mid(ls_str, ll_pos + 1)
	ll_pos = pos(ls_str, "~t")
	ls_cod_pagamento = left(ls_str, ll_pos -1)
	
	if len(ls_cod_pagamento) > 3 then
		ls_des_pagamento = "Pagamento codice " + ls_cod_pagamento
		ls_cod_pagamento = left(ls_cod_pagamento,1) + right(ls_cod_pagamento,2)
	else
		ls_des_pagamento = "Pagamento codice " + ls_cod_pagamento
	end if
	
	insert into tab_pagamenti
	(cod_azienda,
		cod_pagamento,
		des_pagamento)
		values
		(:s_cs_xx.cod_azienda,
		:ls_cod_pagamento,
		'NUOVO PAGAMENTO');
	
	li_count ++
	
	st_progres.text = string(li_count)
	Yield()
	

	insert into anag_clienti
	(	cod_azienda,
		cod_cliente,
		rag_soc_1,
		indirizzo,
		cap,
		localita,
		provincia,
		telefono,
		fax,
		casella_mail ,
		cod_tipo_anagrafica,
		cod_zona,
		cod_pagamento,
		flag_tipo_cliente)
		values(
		:s_cs_xx.cod_azienda,
		:ls_cod_cliente,
		:ls_rag_soc_1,
		:ls_indirizzo,
		:ls_cap,
		:ls_localiita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_email,
		:ls_tipo_cliente,
		:ls_zonal,
		:ls_cod_pagamento,
		'S') ;
		
	
	
	// -----------------  INSERT
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in inserimento cliente  "+ls_cod_cliente+": "+sqlca.sqlerrtext
		rollback;
	else
		commit;
	end if

	
loop

commit;

fileclose(ll_file)

g_mb.warning("Fine")

end event

