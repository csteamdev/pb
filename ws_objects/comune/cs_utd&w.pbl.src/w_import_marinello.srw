﻿$PBExportHeader$w_import_marinello.srw
forward
global type w_import_marinello from window
end type
type em_fornitori from editmask within w_import_marinello
end type
type st_1 from statictext within w_import_marinello
end type
type cb_1 from commandbutton within w_import_marinello
end type
end forward

global type w_import_marinello from window
integer width = 4091
integer height = 2244
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
em_fornitori em_fornitori
st_1 st_1
cb_1 cb_1
end type
global w_import_marinello w_import_marinello

on w_import_marinello.create
this.em_fornitori=create em_fornitori
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.em_fornitori,&
this.st_1,&
this.cb_1}
end on

on w_import_marinello.destroy
destroy(this.em_fornitori)
destroy(this.st_1)
destroy(this.cb_1)
end on

type em_fornitori from editmask within w_import_marinello
integer x = 503
integer y = 20
integer width = 3086
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_1 from statictext within w_import_marinello
integer x = 23
integer y = 40
integer width = 457
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
string text = "Fornitori:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_import_marinello
integer x = 3611
integer y = 20
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Importa"
end type

event clicked;uo_excel luo_excel


luo_excel = CREATE uo_excel

if luo_excel.uof_open(em_fornitori.text, false, false) then
	
end if

end event

