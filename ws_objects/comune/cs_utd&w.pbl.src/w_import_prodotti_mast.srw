﻿$PBExportHeader$w_import_prodotti_mast.srw
forward
global type w_import_prodotti_mast from w_std_principale
end type
type cb_2 from commandbutton within w_import_prodotti_mast
end type
type st_progres from statictext within w_import_prodotti_mast
end type
type sle_file from singlelineedit within w_import_prodotti_mast
end type
type mle_log from multilineedit within w_import_prodotti_mast
end type
type cb_1 from commandbutton within w_import_prodotti_mast
end type
end forward

global type w_import_prodotti_mast from w_std_principale
integer width = 2597
integer height = 2540
cb_2 cb_2
st_progres st_progres
sle_file sle_file
mle_log mle_log
cb_1 cb_1
end type
global w_import_prodotti_mast w_import_prodotti_mast

on w_import_prodotti_mast.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.st_progres=create st_progres
this.sle_file=create sle_file
this.mle_log=create mle_log
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.st_progres
this.Control[iCurrent+3]=this.sle_file
this.Control[iCurrent+4]=this.mle_log
this.Control[iCurrent+5]=this.cb_1
end on

on w_import_prodotti_mast.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.st_progres)
destroy(this.sle_file)
destroy(this.mle_log)
destroy(this.cb_1)
end on

type cb_2 from commandbutton within w_import_prodotti_mast
integer x = 983
integer y = 280
integer width = 722
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Allinea Prodotti-Fornitori"
end type

event clicked;string		ls_str, ls_full_path, ls_docname, ls_cod_prodotto, ls_cod_fornitore,ls_cod_fornitore_prec
long			ll_file, ll_ret, ll_pos, ll_riga
long			ll_count
dec{4}		ld_costo_standard
boolean		lb_inserisci


ll_count = GetFileOpenName("Select File", ls_full_path, ls_docname, "text","Text,*.txt;*.csv", "C:")
	
if ll_count<0 then
	g_mb.error("Errore in selezione file!")
	return
elseif ll_count=0 then
	//annullato dall'operatore
	return
end if

sle_file.text = ls_full_path


ll_file = fileopen(sle_file.text)
if ll_file < 0 then
	g_mb.error("Errore in apertura del file indicato")
	return
end if

mle_log.text += "~r~n~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"
mle_log.text += "INIZIO ELABORAZIONE"+string(now(),"hh:mm:ss")+"~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"

ll_count = 0
ll_riga = 0

do while true
	// leggo riga 1
	ll_ret = fileread(ll_file, ls_str)
	if ll_ret <= 0 then exit

	ll_count ++
	

	st_progres.text = string(ll_count)
	Yield()
	
	ll_pos = pos(ls_str,"~t")
	if ll_pos <= 0 then continue
	
	ls_cod_prodotto = mid(ls_str, 1, ll_pos - 1)
	
	ls_str = mid(ls_str,ll_pos + 1)
	
	ll_pos = pos(ls_str,"~t")
	if ll_pos <= 0 then continue
	
	ls_cod_fornitore = mid(ls_str, 1, ll_pos - 1)

	
	// -----------------  UPDATE ----------------------------- //
	
	select cod_fornitore
	into    :ls_cod_fornitore_prec
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then
		mle_log.text += "~r~nProdotto  "+ls_cod_prodotto+" non trovato "
		continue
	end if
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in ricerca prodotto  "+ls_cod_prodotto+": "+sqlca.sqlerrtext
		continue
	end if

	if isnull(ls_cod_fornitore_prec) then
	
		update anag_prodotti
		set cod_fornitore = :ls_cod_fornitore
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			mle_log.text += "~r~nErrore critico in allineamento prodotto-fornitore  "+ls_cod_prodotto+": "+sqlca.sqlerrtext
			rollback;
		else
			commit;
		end if
		
	end if
loop

commit;

fileclose(ll_file)

g_mb.warning("Fine")

end event

type st_progres from statictext within w_import_prodotti_mast
integer x = 2126
integer y = 440
integer width = 389
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = " ----"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_file from singlelineedit within w_import_prodotti_mast
integer x = 27
integer y = 4
integer width = 2501
integer height = 112
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type mle_log from multilineedit within w_import_prodotti_mast
integer x = 23
integer y = 560
integer width = 2491
integer height = 1840
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_import_prodotti_mast
integer x = 983
integer y = 140
integer width = 667
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica Prodotti"
end type

event clicked;string		ls_str, ls_full_path, ls_docname, ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_costo_standard, ls_cod_comodo
long			ll_file, ll_ret, ll_pos, ll_riga
long			ll_count
dec{4}		ld_costo_standard
boolean		lb_inserisci


ll_count = GetFileOpenName("Select File", ls_full_path, ls_docname, "text","Text,*.txt;*.csv", "C:")
	
if ll_count<0 then
	g_mb.error("Errore in selezione file!")
	return
elseif ll_count=0 then
	//annullato dall'operatore
	return
end if

sle_file.text = ls_full_path


ll_file = fileopen(sle_file.text)
if ll_file < 0 then
	g_mb.error("Errore in apertura del file indicato")
	return
end if

mle_log.text += "~r~n~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"
mle_log.text += "INIZIO ELABORAZIONE"+string(now(),"hh:mm:ss")+"~r~n"
mle_log.text += "------------------------------------------------------------------------------------------~r~n"

ll_count = 0
ll_riga = 0

do while true
	// leggo riga 1
	ll_ret = fileread(ll_file, ls_str)
	if ll_ret < 0 then exit

	ll_count ++
	
	if len(ls_str) < 5 then
		ll_riga = 0
		continue
	end if

	st_progres.text = string(ll_count)
	Yield()

	if left(ls_str,1) = "|" then
		if ll_riga = 0 then
			ll_riga = 1 		// riga articolo
			ls_cod_prodotto = trim(mid(ls_str,2,15))
			ls_cod_misura = trim(mid(ls_str,50,2))
			ls_costo_standard = trim(mid(ls_str,80,8))
			if left(ls_cod_prodotto,2) <> "30" and left(ls_cod_prodotto,2) <> "40" and left(ls_cod_prodotto,2) <> "60" then
				ls_cod_prodotto = ls_cod_prodotto
			end if
			continue
		elseif ll_riga = 1 then
			ll_riga = 2
			ls_des_prodotto = trim(mid(ls_str,2,15))
			ls_cod_comodo = trim(mid(ls_str,43,10))
		end if
	else
		continue
	end if
	
	ll_riga = 0
	ld_costo_standard = dec(ls_costo_standard)
	
	insert into tab_misure
	(cod_azienda, 	cod_misura, des_misura)
	values
	(:s_cs_xx.cod_azienda, :ls_cod_misura, 'NUOVA UM');
	

	insert into anag_prodotti
	(	cod_azienda,
		cod_prodotto,
		des_prodotto,
		cod_misura_mag,
		cod_misura_acq,
		cod_misura_ven,
		cod_comodo,
		costo_standard)
		values(
		:s_cs_xx.cod_azienda,
		:ls_cod_prodotto,
		:ls_des_prodotto,
		:ls_cod_misura,
		:ls_cod_misura,
		:ls_cod_misura,
		:ls_cod_comodo,
		:ld_costo_standard) ;
		
	
	
	// -----------------  INSERT
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "~r~nErrore critico in inserimento cliente  "+ls_cod_prodotto+": "+sqlca.sqlerrtext
		rollback;
	else
		commit;
	end if

	
loop

commit;

fileclose(ll_file)

g_mb.warning("Fine")

end event

