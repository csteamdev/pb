﻿$PBExportHeader$w_import_solari.srw
forward
global type w_import_solari from w_cs_xx_principale
end type
type st_3 from statictext within w_import_solari
end type
type cb_censimento from commandbutton within w_import_solari
end type
type dw_selezione from uo_cs_xx_dw within w_import_solari
end type
type st_2 from statictext within w_import_solari
end type
type st_1 from statictext within w_import_solari
end type
type cb_schede from commandbutton within w_import_solari
end type
type cb_2 from commandbutton within w_import_solari
end type
type cb_1 from commandbutton within w_import_solari
end type
end forward

global type w_import_solari from w_cs_xx_principale
integer width = 3232
integer height = 1384
st_3 st_3
cb_censimento cb_censimento
dw_selezione dw_selezione
st_2 st_2
st_1 st_1
cb_schede cb_schede
cb_2 cb_2
cb_1 cb_1
end type
global w_import_solari w_import_solari

type variables
OLEObject myoleobject

end variables

forward prototypes
public function string wf_des_attrezzatura (string as_descrizione)
end prototypes

public function string wf_des_attrezzatura (string as_descrizione);string ls_codice, ls_descrizione
long ll_pos1, ll_pos2

ll_pos1 = 0
ll_pos2 = 1

ls_descrizione = as_descrizione

do while true
	
	ll_pos1 = pos(ls_descrizione, "(", ll_pos2)
	
	//trovato niente
	if ll_pos1 = 0 and ll_pos2 = 1 then exit
	
	// ora POS2 contiene la posizione dell'ultima parentesi aperta
	if ll_pos1 = 0 and ll_pos2 > 1 then
		
		ll_pos2 = ll_pos2 - 1
		
		// cerco la posizione della parentesi chiusa
		ll_pos1 = pos(ls_descrizione, ")", ll_pos2)
		
		ls_codice = trim( mid(ls_descrizione, ll_pos2, ll_pos1 - ll_pos2 + 1) )
		
		ls_descrizione = trim( left(ls_descrizione, ll_pos2 - 1) )
		
		ls_descrizione = ls_codice + " " + ls_descrizione
		
		ls_descrizione = left(ls_descrizione, 40)
		
		exit
	
	end if
	
	ll_pos2 = ll_pos1 + 1
	
loop


return trim(ls_descrizione)
end function

on w_import_solari.create
int iCurrent
call super::create
this.st_3=create st_3
this.cb_censimento=create cb_censimento
this.dw_selezione=create dw_selezione
this.st_2=create st_2
this.st_1=create st_1
this.cb_schede=create cb_schede
this.cb_2=create cb_2
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.cb_censimento
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_schede
this.Control[iCurrent+7]=this.cb_2
this.Control[iCurrent+8]=this.cb_1
end on

on w_import_solari.destroy
call super::destroy
destroy(this.st_3)
destroy(this.cb_censimento)
destroy(this.dw_selezione)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_schede)
destroy(this.cb_2)
destroy(this.cb_1)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(	dw_selezione, &
						"cod_divisione",&
						sqlca,&
						"anag_divisioni",&
						"cod_divisione", &
						"des_divisione",&
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_selezione

end event

type st_3 from statictext within w_import_solari
integer x = 23
integer y = 388
integer width = 2683
integer height = 368
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Verdana"
long textcolor = 255
long backcolor = 12632256
string text = "PER IL CORRETTO FUNZIONAMENTO DEL SISTEEMA E~' NECESSARIO CHE IL WORKSHEET SIA RINOMINATO IN ~"SCHEDE~" PER L~'IMPORTAZIONE DELLE SCHEDE E IN ~"CENSIMENTO~" PER L~'IMPORTAZIONE DEL CENSIMENTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_censimento from commandbutton within w_import_solari
integer x = 2766
integer y = 692
integer width = 402
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "CENSIMENTO"
boolean flatstyle = true
end type

event clicked;any  		la_value
long 		ll_i, ll_riga_corrente, ll_prog_attrezzatura, ll_cont
string	ls_cod_commessa, ls_cod_divisione, ls_cod_area_aziendale, ls_tipo_impianto, ls_componente, &
			ls_marca, ls_modello, ls_matricola, ls_potenza, ls_alimentazione, ls_inventario, ls_etichetta_poste, &
			ls_cod_attrezzatura, ls_des_attrezzatura, ls_ubicazione, ls_des_cat_attrezzatura, ls_fabbricante


ll_riga_corrente=1
ll_prog_attrezzatura=0

dw_selezione.accepttext() 

ls_cod_commessa = dw_selezione.getitemstring(1, "cod_commessa")
ls_cod_divisione = dw_selezione.getitemstring(1, "cod_divisione")
ls_cod_area_aziendale = dw_selezione.getitemstring(1, "cod_area_aziendale")

do while true
	
	ll_riga_corrente ++
	
	st_2.text = string(ll_riga_corrente)
	
	Yield()
	
	if ll_riga_corrente > 30000 then exit
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,1).value
	ls_tipo_impianto = trim( string(la_value) )

	if len(ls_tipo_impianto) < 1 or isnull(ls_tipo_impianto) then continue
	
	insert into anag_reparti
		(cod_azienda,
		 cod_reparto,
		 des_reparto,
		 cod_area_aziendale,
		 cod_divisione)
	values
		(:s_cs_xx.cod_azienda,
		 :ls_tipo_impianto,
		 'DA CODIFICARE',
		 :ls_cod_area_aziendale,
		 :ls_cod_divisione);
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,2).value
	ls_ubicazione = trim( string(la_value)	)
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,3).value
	ls_componente = string(la_value)	
	
	ls_componente = upper( left( ls_componente, 2) )
//	if pos(ls_componente, " ") > 0 then
//		ls_componente = mid(ls_componente, 1, pos(ls_componente, " ") - 1)
//	end if
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,4).value
	ls_marca = string(la_value)	
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,5).value
	ls_modello = trim(string(la_value)	)
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,7).value
	ls_matricola = trim(string(la_value)	)
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,8).value
	ls_potenza = string(la_value)	
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,9).value
	ls_alimentazione = string(la_value)	
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,10).value
	ls_inventario = left( trim( string(la_value)), 40)
	
	la_value = myoleobject.sheets("CENSIMENTO").cells(ll_riga_corrente,11).value
	ls_etichetta_poste = left( trim( string(la_value) ), 40)
	
	
	do 
		ll_prog_attrezzatura ++
		ls_cod_attrezzatura = ls_cod_commessa + string(ll_prog_attrezzatura,"0000")
		ll_cont = 0
		
		select count(cod_attrezzatura)
		into   :ll_cont
		from   anag_attrezzature
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_attrezzatura = :ls_cod_attrezzatura;
				 
	loop until ll_cont = 0
	
	select des_cat_attrezzature
	into   :ls_des_cat_attrezzatura
	from	 tab_cat_attrezzature
	where  cod_azienda = :s_cs_xx.cod_azienda  and
			 cod_cat_attrezzature = :ls_componente;
	
	if sqlca.sqlcode <> 0 then
		messagebox("IMPORTAZIONE","RIGA:" + STRING(ll_riga_corrente) + "Impossibile trovare la categoria attrezzatura " + ls_componente + "~r~n" + sqlca.sqlerrtext + "~r~n" + "TUTTO ANNULLATO")
		rollback;
		return
	end if
	
	if isnull(ls_des_cat_attrezzatura) then ls_des_cat_attrezzatura = ""
	
	ls_des_attrezzatura = trim( left(ls_des_cat_attrezzatura + " " + ls_ubicazione, 40) )
	
	ls_fabbricante = left(trim(ls_marca) + " " + trim(ls_potenza) + " " + trim(ls_alimentazione), 40)
	
	ls_modello = left(ls_modello + " Matr." + ls_matricola, 40)
	
	insert into anag_attrezzature
			(cod_azienda,
			 cod_attrezzatura,
			 descrizione,
			 cod_cat_attrezzature,
			 cod_reparto,
			 cod_area_aziendale,
			 cod_divisione,
			 fabbricante,
			 num_matricola,
			 cod_inventario,
			 modello)
	values
			(:s_cs_xx.cod_azienda,
			 :ls_cod_attrezzatura,
			 :ls_des_attrezzatura,
			 :ls_componente,
			 :ls_tipo_impianto,
			 :ls_cod_area_aziendale,
			 :ls_cod_divisione,
			 :ls_fabbricante,
			 :ls_etichetta_poste,
			 :ls_inventario,
			 :ls_modello);
			 
	if sqlca.sqlcode <> 0 then
		messagebox("IMPORTAZIONE","RIGA:" + STRING(ll_riga_corrente) + "ERRORE IN INSERIMENTO ATTREZZATURA " + ls_cod_attrezzatura + "~r~n" + sqlca.sqlerrtext + "~r~n" + "TUTTO ANNULLATO")
		rollback;
		return
	end if
	
	
loop

commit;

myoleobject.DisconnectObject()



end event

type dw_selezione from uo_cs_xx_dw within w_import_solari
integer x = 23
integer y = 16
integer width = 3163
integer height = 320
integer taborder = 10
string dataobject = "d_selezione_import_solari"
boolean border = false
end type

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	
	string ls_null
	
	choose case dwo.name
			
		case "cod_divisione"

			f_po_loaddddw_dw(	dw_selezione, &
									"cod_area_aziendale",&
									sqlca,&
									"tab_aree_aziendali",&
									"cod_area_aziendale", &
									"des_area",&
									"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_divisione ='" + data + "'")
	
			setnull( ls_null )
			
			setitem(row, "cod_area_aziendale", ls_null)
			
	end choose
	
end if
end event

type st_2 from statictext within w_import_solari
integer x = 2766
integer y = 800
integer width = 402
integer height = 96
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 255
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_1 from statictext within w_import_solari
integer x = 18
integer y = 800
integer width = 2711
integer height = 448
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_schede from commandbutton within w_import_solari
integer x = 2766
integer y = 580
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "SCHEDE"
boolean flatstyle = true
end type

event clicked;any  la_value
long ll_i, ll_riga_corrente, ll_colonna_corrente, ll_prog_attrezzatura
string ls_string, ls_str, ls_des_manutenzione, ls_num_scheda_manutenzione, ls_des_tipo_manutenzione, ls_controllo, &
		 ls_cod_commessa, ls_cod_divisione, ls_cod_area_aziendale, ls_cod_attrezzatura, ls_des_attrezzatura, &
		 ls_des_operative, ls_cod_tipo_manutenzione


ll_riga_corrente=0
ll_colonna_corrente=0
ll_prog_attrezzatura=0

dw_selezione.accepttext() 

ls_cod_commessa = dw_selezione.getitemstring(1, "cod_commessa")
ls_cod_divisione = dw_selezione.getitemstring(1, "cod_divisione")
ls_cod_area_aziendale = dw_selezione.getitemstring(1, "cod_area_aziendale")

do while true
	
	ll_riga_corrente ++
	
	st_2.text = string(ll_riga_corrente)
	
	Yield()
	
	if ll_riga_corrente > 30000 then exit

	la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,1).value
	ls_string = string(la_value)	
	
	// leggo la descrizione attrezzatura
	if left(upper(ls_string), 16) = "DESCRIZIONE DELL" then
		
		la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,2).value
		ls_des_manutenzione = string( la_value )
		
		st_1.text = ls_des_manutenzione
		
		// sistemo la descrizione
		ls_des_attrezzatura = wf_des_attrezzatura(ls_des_manutenzione)
		
		// procedo con insert attrezzatura
		ll_prog_attrezzatura ++
		ls_cod_attrezzatura = ls_cod_commessa + string(ll_prog_attrezzatura,"0000")
		
		insert into anag_attrezzature
				(cod_azienda,
				 cod_attrezzatura,
				 descrizione,
				 cod_area_aziendale,
				 cod_divisione)
		values
				(:s_cs_xx.cod_azienda,
				 :ls_cod_attrezzatura,
				 :ls_des_attrezzatura,
				 :ls_cod_area_aziendale,
				 :ls_cod_divisione);
				 
		if sqlca.sqlcode <> 0 then
			messagebox("IMPORTAZIONE INTEROTTA","RIGA:" + string(ll_riga_corrente) + " - Errore in insert anag_attrezzature~r~n" + sqlca.sqlerrtext)
			rollback;
			exit
		end if
	end if
	
	// leggo il numero della scheda di manutenzione
	if left(upper(ls_string), 22) = "SCHEDA DI MANUTENZIONE" then
		la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,2).value
		ls_num_scheda_manutenzione = string( la_value )
		
		st_1.text = ls_num_scheda_manutenzione
		
	end if
	
	// leggo il numero della scheda di manutenzione
	if left(upper(ls_string), 24) = "OPERAZIONI DA EFFETTUARE" then
		
		ls_cod_tipo_manutenzione = upper(ls_cod_commessa) + "_" + ls_num_scheda_manutenzione
		la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,2).value
		ls_des_tipo_manutenzione = trim( string( la_value ))
		
		ls_des_operative = ""
		
		do while true
			la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,2).value
			ls_str = string( la_value )
			
			la_value = myoleobject.sheets("SCHEDE").cells(ll_riga_corrente,1).value
			ls_controllo = string( la_value )
			
			if left(ls_controllo, 20) = "MATERIALI SOSTITUITI" then exit

			if len(trim(ls_des_tipo_manutenzione)) < 1 then exit
			
			ls_str = trim(ls_str)
			
			if len(ls_des_operative) > 0 then
				ls_des_operative += "~r~n" + ls_str
			else
				ls_des_operative = ls_str
			end if
			
			st_1.text = ls_des_operative
			ll_riga_corrente ++
			
		loop
		
		insert into tab_tipi_manutenzione
				(cod_azienda,
				 cod_attrezzatura,
				 cod_tipo_manutenzione,
				 des_tipo_manutenzione,
				 flag_manutenzione,
				 modalita_esecuzione)
			values
				(:s_cs_xx.cod_azienda,
				 :ls_cod_attrezzatura,
				 :ls_cod_tipo_manutenzione,
				 :ls_des_tipo_manutenzione,
				 'M',
				 :ls_des_operative);
				 
		if sqlca.sqlcode <> 0 then
			messagebox("IMPORTAZIONE INTEROTTA","RIGA:" + string(ll_riga_corrente) + " - Errore in insert tipi manutenzioni~r~n" + sqlca.sqlerrtext)
			rollback;
			exit
		end if
		
	end if
	
loop

myoleobject.DisconnectObject()


commit;

end event

type cb_2 from commandbutton within w_import_solari
integer x = 2766
integer y = 468
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "VEDI"
boolean flatstyle = true
end type

type cb_1 from commandbutton within w_import_solari
integer x = 2766
integer y = 356
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "APRI"
boolean flatstyle = true
end type

event clicked;BOOLEAN lb_find
long ll_ret

myoleobject = CREATE OLEObject
ll_ret = myoleobject.ConnectToNewObject("excel.application")

lb_find = myoleobject.findfile()

myoleobject.Visible = True
end event

