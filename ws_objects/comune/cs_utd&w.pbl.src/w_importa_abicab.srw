﻿$PBExportHeader$w_importa_abicab.srw
forward
global type w_importa_abicab from w_cs_xx_principale
end type
type dw_lista_abi from uo_cs_xx_dw within w_importa_abicab
end type
type dw_lista_abicab from uo_cs_xx_dw within w_importa_abicab
end type
type cb_1 from commandbutton within w_importa_abicab
end type
type st_1 from statictext within w_importa_abicab
end type
type st_2 from statictext within w_importa_abicab
end type
type st_3 from statictext within w_importa_abicab
end type
type st_4 from statictext within w_importa_abicab
end type
end forward

global type w_importa_abicab from w_cs_xx_principale
int Width=2835
int Height=1513
boolean TitleBar=true
string Title="Aggiornamento ABI CAB"
dw_lista_abi dw_lista_abi
dw_lista_abicab dw_lista_abicab
cb_1 cb_1
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
end type
global w_importa_abicab w_importa_abicab

type variables
transaction i_sqlorig
end variables

event pc_setwindow;call super::pc_setwindow;string ls_dsn

ls_dsn = "ABICAB"

i_sqlorig = CREATE transaction

i_sqlorig.servername = ""
i_sqlorig.logid = ""
i_sqlorig.logpass = ""
i_sqlorig.dbms = "Odbc"
i_sqlorig.database = ""
i_sqlorig.userid = ""
i_sqlorig.dbpass = ""
i_sqlorig.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(i_sqlorig, TRUE)

set_w_options(c_noenablepopup)

windowobject lw_oggetti[]

dw_lista_abi.set_dw_options(i_sqlorig, &
                                        pcca.null_object, &
                                        c_multiselect + &
												    c_nonew + &
												    c_nomodify + &
													 c_nodelete + &
													 c_disablecc + &
													 c_disableccinsert, &
                                        c_default)
dw_lista_abicab.set_dw_options(i_sqlorig, &
                                        pcca.null_object, &
                                        c_multiselect + &
												    c_nonew + &
												    c_nomodify + &
													 c_nodelete + &
													 c_disablecc + &
													 c_disableccinsert, &
                                        c_default)

save_on_close(c_socnosave)

end event

on w_importa_abicab.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_abi=create dw_lista_abi
this.dw_lista_abicab=create dw_lista_abicab
this.cb_1=create cb_1
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_abi
this.Control[iCurrent+2]=dw_lista_abicab
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=st_2
this.Control[iCurrent+6]=st_3
this.Control[iCurrent+7]=st_4
end on

on w_importa_abicab.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_abi)
destroy(this.dw_lista_abicab)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
end on

type dw_lista_abi from uo_cs_xx_dw within w_importa_abicab
int X=23
int Y=21
int Width=2743
int Height=621
int TabOrder=10
string DataObject="d_lista_abi"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;this.retrieve()
end event

type dw_lista_abicab from uo_cs_xx_dw within w_importa_abicab
int X=23
int Y=661
int Width=2743
int Height=621
int TabOrder=20
boolean BringToTop=true
string DataObject="d_lista_abicab"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;this.retrieve()
end event

type cb_1 from commandbutton within w_importa_abicab
int X=2378
int Y=1301
int Width=366
int Height=81
int TabOrder=2
boolean BringToTop=true
string Text="Aggiorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_abi, ls_cod_abi, ls_des_banca, ls_cab, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_agenzia
long ll_i, ll_num_righe
double ld_abi

ll_num_righe = dw_lista_abi.rowcount()

for ll_i = 1 to ll_num_righe
	ld_abi = dw_lista_abi.getitemnumber(ll_i, "abi")
	ls_abi = string(ld_abi)
	ls_des_banca = dw_lista_abi.getitemstring(ll_i, "banca")
	ls_des_banca = left(ls_des_banca, 40)
	st_2.text = ls_abi
	select cod_abi
	into   :ls_cod_abi
	from   tab_abi
	where  cod_abi = :ls_abi;
	if sqlca.sqlcode = 0 then
		update tab_abi set des_abi = :ls_des_banca where cod_abi = :ls_abi;
	else
		insert into tab_abi (cod_abi, des_abi) values (:ls_abi, :ls_des_banca);
	end if
next

ll_num_righe = dw_lista_abicab.rowcount()

for ll_i = 1 to ll_num_righe
	ld_abi = dw_lista_abicab.getitemnumber(ll_i, "abi")
	ls_cab = dw_lista_abicab.getitemstring(ll_i, "cab")
	ls_abi = string(ld_abi)
	ls_indirizzo = dw_lista_abicab.getitemstring(ll_i, "indirizzo")
	ls_indirizzo = left(ls_indirizzo, 40)
	ls_agenzia = dw_lista_abicab.getitemstring(ll_i, "sportello")
	ls_agenzia = left(ls_agenzia, 40)
	ls_localita = dw_lista_abicab.getitemstring(ll_i, "citta")
	ls_localita = left(ls_localita, 40)
	ls_frazione = dw_lista_abicab.getitemstring(ll_i, "comune")
	ls_frazione = left(ls_frazione, 40)
	ls_cap = dw_lista_abicab.getitemstring(ll_i, "cap")
	ls_provincia = dw_lista_abicab.getitemstring(ll_i, "pr")
	st_4.text = ls_cab
	select cod_abi
	into   :ls_cod_abi
	from   tab_abicab
	where  cod_abi = :ls_abi and
	       cod_cab = :ls_cab;
	if sqlca.sqlcode = 0 then
		  update tab_abicab
		  set indirizzo = :ls_indirizzo,
		      agenzia = :ls_agenzia, 
				localita = :ls_localita, 
				frazione = :ls_frazione, 
				cap = :ls_cap, 
				provincia = :ls_provincia 
		where cod_abi = :ls_abi and 
		      cod_cab = :ls_cab;
	else
		  INSERT INTO tab_abicab  
					( cod_abi,   
					  cod_cab,   
					  indirizzo,   
					  localita,   
					  frazione,   
					  cap,   
					  provincia,   
					  telefono,   
					  fax,   
					  telex,   
					  agenzia )  
		  VALUES ( :ls_abi,   
					  :ls_cab,   
					  :ls_indirizzo,   
					  :ls_localita,   
					  :ls_frazione,   
					  :ls_cap,   
					  :ls_provincia,   
					  null,   
					  null,   
					  null,   
					  :ls_agenzia )  ;
	end if
	commit;
next

g_mb.messagebox("Importazione ABI-CAB","Elaborazione Terminata")
cb_1.enabled = false

end event

type st_1 from statictext within w_importa_abicab
int X=23
int Y=1301
int Width=138
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="ABI:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_importa_abicab
int X=161
int Y=1301
int Width=366
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_importa_abicab
int X=572
int Y=1301
int Width=138
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="CAB:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_importa_abicab
int X=732
int Y=1301
int Width=458
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

