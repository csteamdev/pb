﻿$PBExportHeader$w_importa_clienti.srw
$PBExportComments$Importazione Clienti Progetto Tenda
forward
global type w_importa_clienti from w_cs_xx_principale
end type
type cb_importa from uo_cb_ok within w_importa_clienti
end type
type cb_1 from uo_cb_close within w_importa_clienti
end type
type cbx_aggiorna from checkbox within w_importa_clienti
end type
type st_1 from statictext within w_importa_clienti
end type
type gb_1 from groupbox within w_importa_clienti
end type
type rb_totale from radiobutton within w_importa_clienti
end type
type rb_selezionati from radiobutton within w_importa_clienti
end type
type dw_errore_imp from uo_cs_xx_dw within w_importa_clienti
end type
type dw_importa_clienti_orig from uo_cs_xx_dw within w_importa_clienti
end type
type dw_folder from u_folder within w_importa_clienti
end type
type dw_importa_clienti_dest from uo_cs_xx_dw within w_importa_clienti
end type
end forward

global type w_importa_clienti from w_cs_xx_principale
int Width=2972
int Height=1629
boolean TitleBar=true
string Title="Importazione Fornitori"
cb_importa cb_importa
cb_1 cb_1
cbx_aggiorna cbx_aggiorna
st_1 st_1
gb_1 gb_1
rb_totale rb_totale
rb_selezionati rb_selezionati
dw_errore_imp dw_errore_imp
dw_importa_clienti_orig dw_importa_clienti_orig
dw_folder dw_folder
dw_importa_clienti_dest dw_importa_clienti_dest
end type
global w_importa_clienti w_importa_clienti

type variables
transaction i_sqlorig
end variables

forward prototypes
public function long wf_importa (long al_riga, ref long al_riga_sel)
end prototypes

public function long wf_importa (long al_riga, ref long al_riga_sel);long   ll_conta, ll_i, ll_lungo, ll_sconto
string ls_cod_cliente, ls_rag_soc_1, ls_provincia, ls_partita_iva, &
       ls_cap, ls_indirizzo, ls_localita, ls_cod_pagamento, ls_cod_iva, ls_telefono, ls_telex, &
		 ls_fax, ls_rif_interno, ls_cod_fiscale, ls_controllo,&
		 ls_cod_valuta, ls_flag_tipo_cliente, ls_cod_zona, ls_cod_agente, ls_sconto, &
		 ls_flag_spese_incasso, ls_flag_iva_sospesa, ls_banca_cliente,  &
		 ls_des_banca_appoggio, ls_abi, ls_cab, ls_piazza, ls_conto_corrente, ls_cod_porto, &
		 ls_cod_spedizione, ls_cod_vettore, ls_cod_giro, ls_str, &
		 ls_resp_acquisti, ls_resp_vendite, ls_dest_1, ls_dest_2, ls_note_1, ls_note_2
		 

select parametri_azienda.stringa
into   :ls_cod_valuta
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Parametro LIR non trovato.", &
              exclamation!, ok!)
   return ll_conta
end if

ls_cod_cliente = dw_importa_clienti_orig.getitemstring(al_riga, "cod_cliente")
ls_rag_soc_1   = dw_importa_clienti_orig.getitemstring(al_riga, "rag_soc_1")
ls_indirizzo   = dw_importa_clienti_orig.getitemstring(al_riga, "rag_soc_2")
ls_cap         = dw_importa_clienti_orig.getitemstring(al_riga, "cap")
ls_provincia   = dw_importa_clienti_orig.getitemstring(al_riga, "provincia")
ls_partita_iva = dw_importa_clienti_orig.getitemstring(al_riga, "partita_iva")
if not isnull(ls_partita_iva) and len(ls_partita_iva) > 11 then
	ls_flag_tipo_cliente = "F"
	ls_cod_fiscale = ls_partita_iva
	setnull(ls_partita_iva)
else
	ls_flag_tipo_cliente = "S"
end if
ls_localita      = dw_importa_clienti_orig.getitemstring(al_riga, "citta")
ls_cod_pagamento = dw_importa_clienti_orig.getitemstring(al_riga, "cod_pagamento")
ls_cod_iva       = dw_importa_clienti_orig.getitemstring(al_riga, "cod_iva")
if ls_cod_iva = "0" or ls_cod_iva = "1" then setnull(ls_cod_iva)
ls_telefono = dw_importa_clienti_orig.getitemstring(al_riga, "telefono")
ls_telex    = dw_importa_clienti_orig.getitemstring(al_riga, "telex")
ls_fax      = dw_importa_clienti_orig.getitemstring(al_riga, "fax")
ls_rif_interno = dw_importa_clienti_orig.getitemstring(al_riga, "resp_acquisti")
ls_cod_zona    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_zona")
ls_cod_agente  = dw_importa_clienti_orig.getitemstring(al_riga, "cod_agente")
ls_sconto      = dw_importa_clienti_orig.getitemstring(al_riga, "cod_cat_sconto_cliente")
if isnull(ls_sconto) or len(ls_sconto) < 1 then
	ll_sconto  = 0
else
	ll_sconto = long(ll_sconto)
end if
ls_flag_spese_incasso = dw_importa_clienti_orig.getitemstring(al_riga, "flag_spese_incasso")
ls_flag_iva_sospesa   = dw_importa_clienti_orig.getitemstring(al_riga, "flag_iva_sospesa")
ls_banca_cliente      = dw_importa_clienti_orig.getitemstring(al_riga, "cod_banca_cliente")
ls_des_banca_appoggio = dw_importa_clienti_orig.getitemstring(al_riga, "des_banca_appoggio")
ls_abi    = dw_importa_clienti_orig.getitemstring(al_riga, "abi_banca_appoggio")
ls_abi    = right(ls_abi,4)
ls_cab    = dw_importa_clienti_orig.getitemstring(al_riga, "cab_banca_appoggio")
ls_piazza = dw_importa_clienti_orig.getitemstring(al_riga, "piazza_banca_appoggio")
ls_conto_corrente = dw_importa_clienti_orig.getitemstring(al_riga, "conto_corrente_banca_appoggio")
ls_cod_porto      = dw_importa_clienti_orig.getitemstring(al_riga, "cod_porto")
ls_cod_spedizione = dw_importa_clienti_orig.getitemstring(al_riga, "cod_spedizione")
ls_cod_vettore    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_vettore")
ls_cod_giro = dw_importa_clienti_orig.getitemstring(al_riga, "cod_giro")
ls_telefono = dw_importa_clienti_orig.getitemstring(al_riga, "telefono")
ls_fax      = dw_importa_clienti_orig.getitemstring(al_riga, "fax")
ls_telex    = dw_importa_clienti_orig.getitemstring(al_riga, "telex")
ls_resp_acquisti = dw_importa_clienti_orig.getitemstring(al_riga, "resp_acquisti")
ls_resp_vendite  = dw_importa_clienti_orig.getitemstring(al_riga, "resp_vendite")
ls_dest_1 = dw_importa_clienti_orig.getitemstring(al_riga, "destinazione_1")
ls_dest_2 = dw_importa_clienti_orig.getitemstring(al_riga, "destinazione_2")
ls_note_1 = dw_importa_clienti_orig.getitemstring(al_riga, "note_1")
ls_note_2 = dw_importa_clienti_orig.getitemstring(al_riga, "note_2")

ll_conta = 0

insert into anag_depositi  
            (cod_azienda,   
             cod_deposito,   
             des_deposito,   
             indirizzo,   
             localita,   
             frazione,   
             cap,   
             provincia,   
             telefono,   
             fax,   
             telex,   
             cod_documento,   
             flag_blocco,   
             data_blocco)  
values      (:s_cs_xx.cod_azienda,   
             '001',   
             'Deposito generico utilizzato per importazione',   
             null,   
             null,   
             null,   
             null,   
             null,   
             null,   
             null,   
             null,   
             null,   
             'N',   
             null);

if not isnull(ls_cod_iva) or len(ls_cod_iva) > 0 then
  INSERT INTO tab_ive  
         ( cod_azienda,   
           cod_iva,   
           des_iva,   
           aliquota,   
           flag_calcolo,   
           flag_blocco,   
           data_blocco )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_iva,   
           'NUOVA ALIQUOTA IMPORTATA',   
           0,   
           'S',   
           'N',   
           null )  ;
end if

if not isnull(ls_cod_zona) or len(ls_cod_zona) > 0 then
	INSERT INTO tab_zone  
         ( cod_azienda,   
           cod_zona,   
           des_zona,   
           flag_blocco,   
           data_blocco )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_zona,   
           'NUOVA ZONA IMPORTATA',   
           'N',   
           null )  ;
end if

if not isnull(ls_cod_agente) or len(ls_cod_agente) > 0 then
  INSERT INTO anag_agenti  
         ( cod_azienda,   
           cod_agente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           prov_agente,   
           flag_blocco,   
           data_blocco )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_agente,   
           'NUOVO AGENTE IMPORTATO',   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           'N',   
           null )  ;
end if

if not isnull(ls_banca_cliente) or len(ls_banca_cliente) > 0 then

	if not isnull(ls_abi) or len(ls_abi) > 0 then
		INSERT INTO tab_abi  
				( cod_abi,   
				  des_abi )  
		VALUES ( :ls_abi,   
				  'NUOVO ABI IMPORTATO' )  ;
	end if

	if not isnull(ls_cab) or len(ls_cab) > 0 then
		INSERT INTO tab_abicab  
				( cod_abi,   
				  cod_cab,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  agenzia )  
		VALUES ( :ls_abi,   
				  :ls_cab,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null )  ;
	end if 

	select cod_banca_clien_for
	into   :ls_str
	from   anag_banche_clien_for
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_banca_clien_for = :ls_banca_cliente;
	if sqlca.sqlcode = 100 then
	  INSERT INTO anag_banche_clien_for  
				( cod_azienda,   
				  cod_banca_clien_for,   
				  des_banca,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  cod_fiscale,   
				  partita_iva,   
				  cod_abi,   
				  cod_cab,   
				  flag_blocco,   
				  data_blocco )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_banca_cliente,   
				  :ls_des_banca_appoggio,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_abi,   
				  :ls_cab,   
				  'N',   
				  null )  ;
	elseif sqlca.sqlcode = 0 then
		update anag_banche_clien_for
		set    des_banca_clien_for = :ls_des_banca_appoggio,
				 cod_abi = :ls_abi,
				 cod_cab = :ls_cab
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_banca_clien_for = :ls_banca_cliente;
	end if
end if


if not isnull(ls_cod_porto) or len(ls_cod_porto) > 0 then  
	INSERT INTO tab_porti  
			( cod_azienda,   
			  cod_porto,   
			  des_porto,   
			  flag_blocco,   
			  data_blocco )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_porto,   
			  'NUOVO PORTO IMPORTATO',   
			  'N',   
			  null )  ;
end if	
	
if not isnull(ls_cod_spedizione) or len(ls_cod_spedizione) > 0 then
	INSERT INTO tab_mezzi  
			( cod_azienda,   
			  cod_mezzo,   
			  des_mezzo,   
			  flag_blocco,   
			  data_blocco )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_spedizione,   
			  'NUOVO MEZZO SPEDIZIONE',   
			  'N',   
			  null )  ;
end if
	
if not isnull(ls_cod_vettore) or len(ls_cod_vettore) > 0 then
	INSERT INTO anag_vettori  
			( cod_azienda,   
			  cod_vettore,   
			  rag_soc_1,   
			  rag_soc_2,   
			  indirizzo,   
			  localita,   
			  frazione,   
			  cap,   
			  provincia,   
			  telefono,   
			  fax,   
			  telex,   
			  flag_blocco,   
			  data_blocco )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_vettore,   
			  'NUOVO VETTORE IMPORTATO',   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  'N',   
			  null )  ;
end if


st_1.text = ls_cod_cliente + " | " + ls_rag_soc_1

insert into tab_pagamenti  
            (cod_azienda,   
             cod_pagamento,   
             des_pagamento,   
             flag_tipo_pagamento,   
             sconto,   
             flag_bolli,   
             flag_partenza,   
             int_partenza,   
             num_rate_com,   
             cod_doc_prima_rata_cli,   
             cod_doc_prima_rata_for,   
             perc_prima_rata,   
             flag_iva_prima_rata,   
             num_gior_prima_rata,   
             cod_doc_int_rata_cli,   
             cod_doc_int_rata_for,   
             num_gior_int_rata,   
             perc_ult_rata,   
             cod_doc_ult_rata_cli,   
             cod_doc_ult_rata_for,   
             num_gior_ult_rata,   
             flag_blocco,   
             data_blocco,
				 flag_cal_commerciale)  
values      (:s_cs_xx.cod_azienda,   
             :ls_cod_pagamento,   
             'NUOVO PAGAMENTO',   
             'D',   
             0,   
             'N',   
             'D',   
             0,   
             1,   
             null,   
             null,   
             100,   
             'S',   
             0,   
             null,   
             null,   
             0,   
             0,   
             null,   
             null,   
             0,   
             'N',   
             null,
				 'S');

select anag_clienti.cod_cliente
into   :ls_controllo  
from   anag_clienti
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode = 0 then
   if cbx_aggiorna.checked then 
		ls_partita_iva = trim(ls_partita_iva)
		ls_cod_fiscale = trim(ls_cod_fiscale)
      
		update anag_clienti
      set    rag_soc_1 = :ls_rag_soc_1,   
             indirizzo = :ls_indirizzo,   
             localita  = :ls_localita,   
				 cap       = :ls_cap,
				 provincia = :ls_provincia,
             partita_iva = :ls_partita_iva,   
				 cod_fiscale = :ls_cod_fiscale,
				 flag_tipo_cliente = :ls_flag_tipo_cliente,
             cod_pagamento     = :ls_cod_pagamento,   
             telefono = :ls_telefono,   
             telex    = :ls_telex,   
             fax      = :ls_fax,   
             rif_interno   = :ls_resp_vendite,
				 cod_zona      = :ls_cod_zona,
				 cod_agente_1  = :ls_cod_agente,
				 sconto        = :ll_sconto,
				 flag_sospensione_iva = :ls_flag_iva_sospesa,
				 cod_iva              = :ls_cod_iva, 
				 cod_banca_clien_for  = :ls_banca_cliente,
				 conto_corrente       = :ls_conto_corrente,
				 cod_porto   = :ls_cod_porto,
				 cod_mezzo   = :ls_cod_spedizione,
				 cod_vettore = :ls_cod_vettore
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
             anag_clienti.cod_cliente = :ls_cod_cliente;

      if sqlca.sqlcode = 0 then
			ll_conta = 1
         for ll_i = 1 to dw_importa_clienti_dest.rowcount()
            if dw_importa_clienti_dest.getitemstring(ll_i, "cod_cliente") = ls_cod_cliente then
               dw_importa_clienti_dest.setitem(ll_i, "rag_soc_1", ls_rag_soc_1)
               dw_importa_clienti_dest.setitem(ll_i, "indirizzo", ls_indirizzo)
               dw_importa_clienti_dest.setitem(ll_i, "localita", ls_localita)
               dw_importa_clienti_dest.setitem(ll_i, "partita_iva", ls_partita_iva)
               dw_importa_clienti_dest.setitem(ll_i, "cod_pagamento", ls_cod_pagamento)
               dw_importa_clienti_dest.setitem(ll_i, "cod_iva", ls_cod_iva)
               dw_importa_clienti_dest.setitem(ll_i, "telefono", ls_telefono)
               dw_importa_clienti_dest.setitem(ll_i, "telex", ls_telex)
               dw_importa_clienti_dest.setitem(ll_i, "fax", ls_fax)
               dw_importa_clienti_dest.setitem(ll_i, "rif_interno", ls_resp_vendite)
         	   al_riga_sel = ll_i
               exit
            end if
			next
      end if
		
      if sqlca.sqlcode = -1 then
         dw_errore_imp.insertrow(0)
    	   dw_errore_imp.setrow(dw_errore_imp.rowcount())
    		dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_cliente + " | " + sqlca.sqlerrtext)
   	end if
   end if
elseif sqlca.sqlcode = 100 then
	
	ls_partita_iva = trim(ls_partita_iva)
	ls_cod_fiscale = trim(ls_cod_fiscale)
	
  INSERT INTO anag_clienti  
         ( cod_azienda,   
           cod_cliente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_fiscale,   
           partita_iva,   
           rif_interno,   
           flag_tipo_cliente,   
           cod_capoconto,   
           cod_conto,   
           cod_iva,   
           num_prot_esenzione_iva,   
           data_esenzione_iva,   
           flag_sospensione_iva,   
           cod_pagamento,   
           giorno_fisso_scadenza,   
           mese_esclusione_1,   
           mese_esclusione_2,   
           data_sostituzione_1,   
           data_sostituzione_2,   
           sconto,   
           cod_tipo_listino_prodotto,   
           fido,   
           flag_fuori_fido,   
           cod_banca_clien_for,   
           conto_corrente,   
           cod_lingua,   
           cod_nazione,   
           cod_area,   
           cod_zona,   
           cod_valuta,   
           cod_categoria,   
           cod_agente_1,   
           cod_agente_2,   
           cod_imballo,   
           cod_porto,   
           cod_resa,   
           cod_mezzo,   
           cod_vettore,   
           cod_inoltro,   
           cod_deposito,   
           flag_riep_boll,   
           flag_riep_fatt,   
           flag_blocco,   
           data_blocco,   
           casella_mail,   
           flag_raggruppo_iva_doc )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_cliente,   
           :ls_rag_soc_1,   
           null,   
           :ls_indirizzo,   
           :ls_localita,   
           null,   
           :ls_cap,   
           :ls_provincia,   
           :ls_telefono,   
           :ls_fax,   
           :ls_telex,   
           :ls_cod_fiscale,   
           :ls_partita_iva,   
           :ls_resp_acquisti,   
           :ls_flag_tipo_cliente,   
           null,   
           null,   
           :ls_cod_iva,   
           null,   
           null,   
           :ls_flag_iva_sospesa,   
           :ls_cod_pagamento,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :ll_sconto,   
           'N',   
           :ls_banca_cliente,   
           :ls_conto_corrente,   
           null,   
           null,   
           null,   
           :ls_cod_zona,   
           :ls_cod_valuta,   
           null,   
           :ls_cod_agente,   
           null,   
           null,   
           :ls_cod_porto,   
           null,   
           :ls_cod_spedizione,   
           :ls_cod_vettore,   
           null,   
           '001',   
           'S',   
           'S',   
           'N',   
           null,   
           null,   
           'N' )  ;

						
						
   if sqlca.sqlcode = 0 then
	   ll_conta = 1
      dw_importa_clienti_dest.insertrow(0)
      dw_importa_clienti_dest.setrow(dw_importa_clienti_dest.rowcount())
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "cod_cliente", ls_cod_cliente)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "indirizzo", ls_indirizzo)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "localita", ls_localita)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "partita_iva", ls_partita_iva)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "cod_pagamento", ls_cod_pagamento)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "cod_iva", ls_cod_iva)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "telefono", ls_telefono)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "telex", ls_telex)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "fax", ls_fax)
      dw_importa_clienti_dest.setitem(dw_importa_clienti_dest.getrow(), "rif_interno", ls_resp_acquisti)
		al_riga_sel = dw_importa_clienti_dest.getrow()	
   end if

   if sqlca.sqlcode = -1 then
      dw_errore_imp.insertrow(0)
 	   dw_errore_imp.setrow(dw_errore_imp.rowcount())
  		dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_cliente + " | " + sqlca.sqlerrtext)
	end if
elseif sqlca.sqlcode = -1 then
   dw_errore_imp.insertrow(0)
   dw_errore_imp.setrow(dw_errore_imp.rowcount())
   dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_cliente + " | " + sqlca.sqlerrtext)
end if

return ll_conta
end function

event pc_setwindow;call super::pc_setwindow;string ls_dsn

ls_dsn = "IMPORT"

i_sqlorig = CREATE transaction

i_sqlorig.servername = ""
i_sqlorig.logid = ""
i_sqlorig.logpass = ""
i_sqlorig.dbms = "Odbc"
i_sqlorig.database = ""
i_sqlorig.userid = ""
i_sqlorig.dbpass = ""
i_sqlorig.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(i_sqlorig, TRUE)

set_w_options(c_noenablepopup)

windowobject lw_oggetti[]

lw_oggetti[1] = dw_importa_clienti_orig
dw_folder.fu_assigntab(1, "Origine Dati", lw_oggetti[])
lw_oggetti[1] = dw_importa_clienti_dest
dw_folder.fu_assigntab(2, "Destinazione Dati", lw_oggetti[])
lw_oggetti[1] = dw_errore_imp
dw_folder.fu_assigntab(3, "Errori", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_importa_clienti_orig.set_dw_options(i_sqlorig, &
                                         pcca.null_object, &
                                         c_multiselect + &
													  c_nonew + &
													  c_nomodify + &
													  c_nodelete + &
													  c_disablecc + &
													  c_disableccinsert, &
                                         c_default)

dw_importa_clienti_dest.set_dw_options(sqlca, &
                                         pcca.null_object, &
                                         c_multiselect + &
													  c_nonew + &
													  c_nomodify + &
													  c_nodelete + &
												  	  c_disablecc + &
													  c_disableccinsert, &
                                         c_viewmodeblack)
													 
dw_errore_imp.set_dw_options(sqlca, &
                             pcca.null_object, &
									  c_nonew + &
									  c_nomodify + &
									  c_nodelete + &
									  c_disablecc + &
									  c_disableccinsert, &
                             c_viewmodeblack)
													 
save_on_close(c_socnosave)

end event

on w_importa_clienti.create
int iCurrent
call w_cs_xx_principale::create
this.cb_importa=create cb_importa
this.cb_1=create cb_1
this.cbx_aggiorna=create cbx_aggiorna
this.st_1=create st_1
this.gb_1=create gb_1
this.rb_totale=create rb_totale
this.rb_selezionati=create rb_selezionati
this.dw_errore_imp=create dw_errore_imp
this.dw_importa_clienti_orig=create dw_importa_clienti_orig
this.dw_folder=create dw_folder
this.dw_importa_clienti_dest=create dw_importa_clienti_dest
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_importa
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cbx_aggiorna
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=gb_1
this.Control[iCurrent+6]=rb_totale
this.Control[iCurrent+7]=rb_selezionati
this.Control[iCurrent+8]=dw_errore_imp
this.Control[iCurrent+9]=dw_importa_clienti_orig
this.Control[iCurrent+10]=dw_folder
this.Control[iCurrent+11]=dw_importa_clienti_dest
end on

on w_importa_clienti.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_importa)
destroy(this.cb_1)
destroy(this.cbx_aggiorna)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.rb_totale)
destroy(this.rb_selezionati)
destroy(this.dw_errore_imp)
destroy(this.dw_importa_clienti_orig)
destroy(this.dw_folder)
destroy(this.dw_importa_clienti_dest)
end on

event close;call super::close;commit;
end event

type cb_importa from uo_cb_ok within w_importa_clienti
event clicked pbm_bnclicked
int X=2149
int Y=1421
int Width=366
int Height=81
int TabOrder=60
string Text="&Importa"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long ll_riga, ll_errore, ll_selected[], ll_i, ll_conta, ll_totale, ll_riga_sel[], al_riga_sel


setpointer(hourglass!)

ll_totale = 0
if rb_totale.checked then
   for ll_riga = 1 to dw_importa_clienti_orig.rowcount()
      ll_conta = wf_importa(ll_riga, al_riga_sel)
      ll_totale = ll_totale + ll_conta
      if ll_totale > 0 then ll_riga_sel[ll_totale] = al_riga_sel
      if mod(ll_riga,10) = 0 then 
         commit;
      end if
   next
   st_1.text = "Importazione Terminata. Record Letti: " + string(dw_importa_clienti_orig.rowcount()) + &
               " Record Scritti: " + string(ll_totale)
else
   dw_importa_clienti_orig.get_selected_rows(ll_selected[])
   ll_i = 1
	for ll_riga = 1 to upperbound(ll_selected)
      ll_conta = wf_importa(ll_selected[ll_riga], al_riga_sel)
      ll_totale = ll_totale + ll_conta
      if ll_totale > 0 then ll_riga_sel[ll_totale] = al_riga_sel
      if mod(ll_riga,10) = 0 then 
         commit;
      end if
   next
   st_1.text = "Importazione Terminata. Record Letti: " + string(upperbound(ll_selected)) + &
               " Record Scritti: " + string(ll_totale)
end if

if ll_errore < 0 then
   pcca.error = c_fatal
end if

dw_importa_clienti_dest.set_selected_rows(ll_totale, &
                                            ll_riga_sel[], &
                                            c_ignorechanges, &
                                            c_refreshchildren, &
                                            c_refreshview)

dw_importa_clienti_dest.setsort("cod_fornitore A")
dw_importa_clienti_dest.sort() 

setpointer(arrow!)
end event

type cb_1 from uo_cb_close within w_importa_clienti
int X=2538
int Y=1421
int Width=366
int Height=81
int TabOrder=50
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_aggiorna from checkbox within w_importa_clienti
int X=23
int Y=1441
int Width=823
int Height=61
boolean BringToTop=true
string Text="Aggiornare Dati già Importati"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_importa_clienti
int X=23
int Y=1121
int Width=2218
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_importa_clienti
int X=23
int Y=1201
int Width=846
int Height=221
int TabOrder=70
string Text="Tipo Importazione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_totale from radiobutton within w_importa_clienti
int X=46
int Y=1261
int Width=801
int Height=61
boolean BringToTop=true
string Text="Totale"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_selezionati from radiobutton within w_importa_clienti
int X=46
int Y=1341
int Width=801
int Height=61
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_errore_imp from uo_cs_xx_dw within w_importa_clienti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2835
int Height=941
int TabOrder=10
string DataObject="d_errore_imp"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_importa_clienti_orig from uo_cs_xx_dw within w_importa_clienti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2835
int Height=941
int TabOrder=30
string DataObject="d_importa_clienti_orig"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_importa_clienti
int X=1
int Y=21
int Width=2903
int Height=1081
int TabOrder=20
end type

type dw_importa_clienti_dest from uo_cs_xx_dw within w_importa_clienti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2835
int Height=941
int TabOrder=40
string DataObject="d_importa_clienti_dest"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

