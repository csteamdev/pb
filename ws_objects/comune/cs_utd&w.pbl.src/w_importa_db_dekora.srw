﻿$PBExportHeader$w_importa_db_dekora.srw
forward
global type w_importa_db_dekora from window
end type
type cb_salva_dtaglio from commandbutton within w_importa_db_dekora
end type
type dw_dtaglio from datawindow within w_importa_db_dekora
end type
type cb_log from commandbutton within w_importa_db_dekora
end type
type sle_log from singlelineedit within w_importa_db_dekora
end type
type st_counter from statictext within w_importa_db_dekora
end type
type st_log from statictext within w_importa_db_dekora
end type
type cb_leggi from commandbutton within w_importa_db_dekora
end type
type st_tot from statictext within w_importa_db_dekora
end type
type sle_sfoglia from singlelineedit within w_importa_db_dekora
end type
type cb_sfoglia from commandbutton within w_importa_db_dekora
end type
type dw_lista from datawindow within w_importa_db_dekora
end type
type dw_distinta from datawindow within w_importa_db_dekora
end type
end forward

global type w_importa_db_dekora from window
integer width = 5120
integer height = 3236
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_salva_dtaglio cb_salva_dtaglio
dw_dtaglio dw_dtaglio
cb_log cb_log
sle_log sle_log
st_counter st_counter
st_log st_log
cb_leggi cb_leggi
st_tot st_tot
sle_sfoglia sle_sfoglia
cb_sfoglia cb_sfoglia
dw_lista dw_lista
dw_distinta dw_distinta
end type
global w_importa_db_dekora w_importa_db_dekora

type variables
uo_excel_listini iuo_excel

long			il_max_row_excel = 300

string			is_cod_versione = "001"
string			is_des_versione = "VERSIONE CORRENTE"
string			is_flag_predefinita = "S"
string			is_cod_prodotto_finale = "TESSUTO"
string			is_path_log = ""
end variables

forward prototypes
public function integer wf_leggi_fogli (string as_path, ref string as_errore)
public function integer wf_leggi_distinta (long al_row, ref string as_errore)
public function integer wf_estrai_prodotto (string as_input, ref string as_output, ref string as_errore)
public function integer wf_salva_distinta (long al_row, ref string as_errore)
public subroutine wf_log (string as_valore)
public subroutine wf_des_prodotto (long al_row, string as_cod_prodotto, ref string as_des_prodotto)
public subroutine wf_insert_row_dw (string as_padre, string as_figlio, string as_um, decimal ad_qta, string as_ok, string as_cella_originale, string as_var_taglio, long ad_taglio)
end prototypes

public function integer wf_leggi_fogli (string as_path, ref string as_errore);string				ls_sheets[], ls_foglio, ls_cod_prodotto, ls_err, ls_ok, ls_temp[]
long				ll_tot, ll_i, ll_new, ll_pos
integer			li_ret


dw_lista.reset()
dw_distinta.reset()

iuo_excel = create uo_excel_listini

iuo_excel.is_path_file = as_path
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_temp[])
ll_tot = upperbound(ls_temp[])

//inverti l'ordine dei fogli
ll_new = 0
for ll_i= ll_tot to 1 step -1
	ll_new += 1
	ls_sheets[ll_new] = ls_temp[ll_i]
next


if isnull(ll_tot) or ll_tot <= 0 then
	as_errore = "Non esistono fogli di lavoro nel file excel specificato!"
	st_tot.text = "Fogli Totali: 0"
	iuo_excel.uof_close( )
	destroy iuo_excel
	return -1
end if

st_tot.text = "Fogli Totali: " +string(ll_tot)

for ll_i = 1 to ll_tot
	ls_foglio = ls_sheets[ll_i]
	st_counter.text = string(ll_i)
	
	ls_cod_prodotto = ls_foglio
	
	//se trovi uno spazio o un carattere - tronca
	ll_pos = pos(ls_cod_prodotto, "-", 1)
	if ll_pos > 0 then
		ls_cod_prodotto = left(ls_cod_prodotto, ll_pos - 1)
	else
		ll_pos = pos(ls_cod_prodotto, " ", 1)
		if ll_pos > 0 then
			ls_cod_prodotto = left(ls_cod_prodotto, ll_pos - 1)
		end if
	end if
	//----------------------------------------------
	
	dw_lista.insertrow(0)
	dw_lista.setitem(ll_i, "foglio", ls_foglio)
	dw_lista.setitem(ll_i, "cod_prodotto_originale", ls_cod_prodotto)
	
	//cerca per cod_comodo_2, poi per cod_comodo, poi per cod_prodotto
	li_ret = wf_estrai_prodotto(ls_cod_prodotto, ls_cod_prodotto, ls_err)
	if li_ret = -1 then
		//errore critico!
		iuo_excel.uof_close( )
		destroy iuo_excel
		return -1

	elseif li_ret = -2 then
		//non trovato
		ls_ok = "N"

	else
		//OK
		ls_ok = "S"
	end if
	
	dw_lista.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	
	dw_lista.setitem(ll_i, "flag_ok", ls_ok)
	
next


return 0
end function

public function integer wf_leggi_distinta (long al_row, ref string as_errore);string				ls_foglio, ls_cod_prodotto_finito, ls_flag_ok, ls_valore, ls_cod_prodotto_figlio, ls_um, ls_temp, ls_variabile

long				ll_index, ll_count, ll_taglio

decimal			ld_valore, ld_qta_utilizzo

datetime			ldt_valore

integer			li_stato_lettura, li_ret



ls_foglio = dw_lista.getitemstring(al_row, "foglio")
ls_cod_prodotto_finito = dw_lista.getitemstring(al_row, "cod_prodotto")
ls_flag_ok = dw_lista.getitemstring(al_row, "flag_ok")

dw_distinta.reset()

dw_dtaglio.reset()

st_log.text = ""
li_stato_lettura = 0

if ls_cod_prodotto_finito="" or isnull(ls_cod_prodotto_finito) or ls_cod_prodotto_finito = "NOT FOUND" or ls_flag_ok<>"S" then
	st_log.text = "Prodotto Finito NON IMPOSTATO!"
	dw_lista.setitem(al_row, "flag_ok", "N")
	//return 1
end if

//inserisci la riga in distinta padri, ma solo se non c'è
//se già c'è salta il foglio
select count(*)
into :ll_count
from distinta_padri
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto_finito and
			cod_versione=:is_cod_versione;

if sqlca.sqlcode<0 then
	as_errore= "Errore controllo esistenza distinta base: "+sqlca.sqlerrtext
	dw_lista.setitem(al_row, "flag_ok", "N")
	st_log.text = as_errore
	return -1
	
elseif sqlca.sqlcode=0 and (ll_count=0 or isnull(ll_count)) then
	//continua nell'inserimento
	
elseif sqlca.sqlcode=0 and ll_count>0 then
	//già esiste
	st_log.text = "Questa Distinta Base già esiste!"
	dw_lista.setitem(al_row, "flag_ok", "N")
	//return 1
end if

ll_index = 1

do while true
	
	//cerca la cella con CODICE sulla colonna n° 1 (A)
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 1, ls_valore, ld_valore, ldt_valore, "S")
	if upper(ls_valore) = "CODICE" then
			
		//hai trovato la PRIMA parola CODICE
		li_stato_lettura = 1
		
		//inizia a leggere dalla riga successiva
		ll_index = ll_index + 1
		
		LETTURA:

		//nella colonna n°1 (A) ci sono i codici prodotto figlio (SE VUOTO SALTA ALLA RIGA SUCCESSIVA!!!!!)
		//SE UPPER(TRIM()) = is_cod_prodotto_finale, cioè TESSUTO, allora quella corrente è l'ultima riga da leggere
		//nella riga corrente alla colonna 3 (C) c'è la unità di misura (se TESSUTO metti ML)
		//nella riga corrente alla colonna 4 (D) c'è la quantità utilizzo
		
		do while true
			
			//lettura cella su prima colonna dove ci dovrebbe essere un codice prodotto figlio
			iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 1, ls_valore, ld_valore, ldt_valore, "S")
			
			if upper(ls_valore) = "CODICE" then
				//questo segnala che il primo gruppo di codici è terminato e sta per iniziare il secondo gruppo
				if li_stato_lettura = 2 then
					//stato lettura era già 2, qualcosa non va ....
					as_errore = "Parola CODICE trovata più di due volte!!"
					dw_lista.setitem(al_row, "flag_ok", "N")
					st_log.text = "VERIFICARE il FILE: " + as_errore
					return -1
				end if
				
				li_stato_lettura = 2
				//passa a cercare su riga successiva
				ll_index = ll_index + 1
				goto LETTURA
				
			elseif ls_valore = "" or isnull(ls_valore) then
				//vai a riga successiva
				
			elseif upper(ls_valore) = is_cod_prodotto_finale then
				//questa è l'ultima riga da leggere
				ls_cod_prodotto_figlio = "TESSUTI"
				ls_um = "ML"
				ld_qta_utilizzo = 1
				
				//verifica se c'è una distinta di taglio: colonna n°5 (E) vale L o H
				//												colonna n°6 (F) valore del taglio, esempio -15
				iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 5, ls_variabile, ld_valore, ldt_valore, "S")
				//verifica variabile
				iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 6, ls_valore, ld_valore, ldt_valore, "D")
				ll_taglio = long(ld_valore)
				
				wf_insert_row_dw(ls_cod_prodotto_finito, ls_cod_prodotto_figlio, ls_um, ld_qta_utilizzo, "N", ls_valore, ls_variabile, ll_taglio)
				
				//finito!
				dw_lista.setitem(al_row, "flag_ok", "S")
				st_log.text += " Lettura terminata con SUCCESSO!"
				return 0
				
			else
				//trovato quello che sembra un codice prodotto figlio (colonna n°1 (A))
				ls_temp = ls_valore
				li_ret = wf_estrai_prodotto(ls_valore, ls_cod_prodotto_figlio, as_errore)
				if li_ret = -2 then
					//NOT FOUND
					ls_um = ""
					ld_qta_utilizzo = -1
					
					//verifica se c'è una distinta di taglio: colonna n°5 (E) vale L o H
					//												colonna n°6 (F) valore del taglio, esempio -15
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 5, ls_variabile, ld_valore, ldt_valore, "S")
					//verifica variabile
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 6, ls_valore, ld_valore, ldt_valore, "D")
					ll_taglio = long(ld_valore)
					
					wf_insert_row_dw(ls_cod_prodotto_finito, ls_cod_prodotto_figlio, ls_um, ld_qta_utilizzo, "N", ls_temp, ls_variabile, ll_taglio)
					
				elseif li_ret = -1 then
					//errore critico
					dw_lista.setitem(al_row, "flag_ok", "N")
					st_log.text += "(2nda lettura): " + as_errore
					return -1
				else
					//OK
					//UM, colonna n° 3 (C)
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 3, ls_um, ld_valore, ldt_valore, "S")
					//qta utilizzo, colonna n° 4 (D)
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 4, ls_valore, ld_qta_utilizzo, ldt_valore, "D")
					
					
					//verifica se c'è una distinta di taglio: colonna n°5 (E) vale L o H
					//												colonna n°6 (F) valore del taglio, esempio -15
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 5, ls_variabile, ld_valore, ldt_valore, "S")
					//verifica variabile
					iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_index, 6, ls_valore, ld_valore, ldt_valore, "D")
					ll_taglio = long(ld_valore)
					
					wf_insert_row_dw(ls_cod_prodotto_finito, ls_cod_prodotto_figlio, ls_um, ld_qta_utilizzo, "S", ls_temp, ls_variabile, ll_taglio)
				end if
			end if
			
			
			//passa a cercare su riga successiva
			ll_index = ll_index + 1
			
			//se hai superato il max esci e dai errore
			if ll_index > il_max_row_excel then
				as_errore = "Parola CODICE o Tessuto non trovata in seconda lettura e superato MAX RIGHE!"
				dw_lista.setitem(al_row, "flag_ok", "N")
				st_log.text += "VERIFICARE il FILE: " + as_errore
				return -1
			else
				continue
			end if
		loop			
			
	else
		//passa a cercare su riga successiva
		ll_index = ll_index + 1
		
		//se hai superato il max esci e dai errore
		if ll_index > il_max_row_excel then
			as_errore = "Parola CODICE non trovata in prima lettura e superato MAX RIGHE!"
			dw_lista.setitem(al_row, "flag_ok", "N")
			st_log.text += "VERIFICARE il FILE: " + as_errore
			return -1
		else
			continue
		end if
		
	end if
	
loop

return 0
end function

public function integer wf_estrai_prodotto (string as_input, ref string as_output, ref string as_errore);datastore		lds_data
string				ls_sql, ls_temp
long				ll_tot


as_output = ""

//prova per cod_prodotto ---------------------------------------------------
//ma prima tolgo il carattere "." eventuale

ls_temp = as_input
guo_functions.uof_replace_string(ls_temp, ".", "")

ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_prodotto='"+ls_temp+"' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if



//prova per cod_comodo_2 ----------------------------------------------------------------
ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_comodo_2 like '"+as_input+"%' "+&
			"order by cod_prodotto asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if


//prova per cod_comodo ------------------------------------------------------------------
ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_comodo like '"+as_input+"%' "+&
			"order by cod_prodotto asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if


//se sei arrivato fin qui non hai trovato nulla
as_output = "NOT FOUND"
return -2
end function

public function integer wf_salva_distinta (long al_row, ref string as_errore);string				ls_foglio, ls_cod_prodotto_finito, ls_flag_ok, ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_versione_figlio,ls_cod_misura, ls_valore, ls_temp

long				ll_tot, ll_index, ll_count, ll_num_sequenza

dec{4}			ld_quan_utilizzo

boolean			lb_almeno_una = false


ls_foglio = dw_lista.getitemstring(al_row, "foglio")
ls_cod_prodotto_finito = dw_lista.getitemstring(al_row, "cod_prodotto")
ls_flag_ok = dw_lista.getitemstring(al_row, "flag_ok")

dw_distinta.accepttext()
ll_tot = dw_distinta.rowcount()
if ll_tot>0 then
else
	as_errore = "Nessuna riga sembra presente per il salvataggio in distinta base!"
	st_log.text = as_errore
	dw_lista.setitem(al_row, "flag_ok", "N")
	return -1
end if


if ls_cod_prodotto_finito="" or isnull(ls_cod_prodotto_finito) or ls_cod_prodotto_finito = "NOT FOUND" or ls_flag_ok<>"S" then
	as_errore = "Prodotto Finito NON IMPOSTATO!"
	st_log.text = as_errore
	dw_lista.setitem(al_row, "flag_ok", "N")
	return -1
end if

//inserisci la riga in distinta padri, ma solo se non c'è
//se già c'è salta il foglio
select count(*)
into :ll_count
from distinta_padri
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto_finito and
			cod_versione=:is_cod_versione;

if sqlca.sqlcode<0 then
	as_errore= "Errore controllo esistenza distinta base: "+sqlca.sqlerrtext
	dw_lista.setitem(al_row, "flag_ok", "N")
	st_log.text = as_errore
	return -1
	
elseif sqlca.sqlcode=0 and (ll_count=0 or isnull(ll_count)) then
	//continua nell'inserimento
	
elseif sqlca.sqlcode=0 and ll_count>0 then
	//già esiste
	as_errore = "Questa Distinta Base già esiste!"
	st_log.text = as_errore
	dw_lista.setitem(al_row, "flag_ok", "N")
	return 1
end if

for ll_index=1 to ll_tot
	if dw_distinta.getitemstring(ll_index, "flag_ok") = "S" then
		lb_almeno_una = true
		exit
	end if
next

if not lb_almeno_una then
	as_errore = "Nessuna riga è stata selezionata per il salvataggio in distinta base!"
	st_log.text = as_errore
	dw_lista.setitem(al_row, "flag_ok", "N")
	return -1
end if

//salva distinta padri
INSERT INTO distinta_padri  
		(	cod_azienda,   
			cod_prodotto,   
			cod_versione,   
			des_versione,   
			flag_predefinita,   
			flag_blocco)  
VALUES (	:s_cs_xx.cod_azienda,
				:ls_cod_prodotto_finito,
				:is_cod_versione,
				:is_des_versione,
				'S',   
				'N');

if sqlca.sqlcode<0 then
	as_errore = "Errore inserimento distinta padri : "+sqlca.sqlerrtext
	return -1
end if

ls_valore = "~r~n"+"Creazione distinta base del prodotto finito "+ls_cod_prodotto_finito
ls_valore += "~r~n"+"--------------------------------------------------------------------------------------"

//salva distinta
for ll_index=1 to ll_tot
	if dw_distinta.getitemstring(ll_index, "flag_ok") = "S" then
		
		ll_num_sequenza			= dw_distinta.getitemnumber(ll_index, "num_sequenza")
		ls_cod_prodotto_figlio		= dw_distinta.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione			= dw_distinta.getitemstring(ll_index, "cod_versione")
		ls_cod_versione_figlio		= dw_distinta.getitemstring(ll_index, "cod_versione_figlio")
		ls_cod_misura				= dw_distinta.getitemstring(ll_index, "cod_misura")
		ld_quan_utilizzo			= dw_distinta.getitemnumber(ll_index, "quan_utilizzo")
		
		INSERT INTO distinta  
         ( cod_azienda,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_versione,   
           cod_versione_figlio,   
           cod_misura,   
           quan_tecnica,   
           quan_utilizzo)  
	  VALUES (	:s_cs_xx.cod_azienda,
					:ls_cod_prodotto_finito,
					:ll_num_sequenza,
					:ls_cod_prodotto_figlio,
					:ls_cod_versione,
					:ls_cod_versione_figlio,
					:ls_cod_misura,
					0,
					:ld_quan_utilizzo	);
		if sqlca.sqlcode<0 then
			as_errore = "Errore inserimento riga n° "+string(ll_index) + " : "+sqlca.sqlerrtext
			return -1
		end if
		
		ls_valore += "~r~n"+"Assegnato correttamente in distinta articolo "+ls_cod_prodotto_figlio+" (Qta: "+string(ld_quan_utilizzo, "###,###.0000")+" - UM: "+ls_cod_misura+")"

	else
		//riga saltata ...
		ls_temp = dw_distinta.getitemstring(ll_index, "cella_originale")
		if isnull(ls_temp) or ls_temp="" then ls_temp = "[VALORENULLO]"
		ls_valore += "~r~n>>>>>>>>>"+ ls_temp + " NON IMPORTATO perchè non trovato in anagrafica ..."
		
	end if
next

ls_valore += "~r~n"+"--------------------------------------------------------------------------------------"
wf_log(ls_valore)

return 0
end function

public subroutine wf_log (string as_valore);long			ll_file

if is_path_log="" or isnull(is_path_log) then return

ll_file = fileopen(is_path_log, LINEMODE!, WRITE!)
filewrite(ll_file, as_valore)
fileclose(ll_file)

return

end subroutine

public subroutine wf_des_prodotto (long al_row, string as_cod_prodotto, ref string as_des_prodotto);string				ls_des_prodotto,ls_um

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then
			
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto;
	
	if sqlca.sqlcode = 0 then
		as_des_prodotto = ls_des_prodotto
		dw_distinta.setitem(al_row, "des_prodotto", ls_des_prodotto)
		dw_distinta.setitem(al_row, "flag_ok", "S")
		ls_um = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  as_cod_prodotto + "'", "cod_misura_mag" )
		dw_distinta.setitem(al_row, "cod_misura", ls_um)
	else
		as_des_prodotto = "ERROR or NOT FOUND"
		dw_distinta.setitem(al_row, "des_prodotto", "ERROR or NOT FOUND")
		dw_distinta.setitem(al_row, "cod_misura", "NOTFOUND")
		dw_distinta.setitem(al_row, "flag_ok", "N")
	end if
	
else
	as_des_prodotto = ""
	dw_distinta.setitem(al_row, "flag_ok", "N")
	dw_distinta.setitem(al_row, "des_prodotto", "")
end if

return
end subroutine

public subroutine wf_insert_row_dw (string as_padre, string as_figlio, string as_um, decimal ad_qta, string as_ok, string as_cella_originale, string as_var_taglio, long ad_taglio);long			ll_new, ll_num_sequenza
string			ls_um, ls_formula_misura
string			ls_des_prodotto


ll_new = dw_distinta.insertrow(0)

ll_num_sequenza = ll_new * 10

dw_distinta.setitem(ll_new, "cod_prodotto_padre", as_padre)
dw_distinta.setitem(ll_new, "num_sequenza", ll_num_sequenza)
dw_distinta.setitem(ll_new, "cod_prodotto_figlio", as_figlio)
dw_distinta.setitem(ll_new, "cod_versione", is_cod_versione)
dw_distinta.setitem(ll_new, "cod_versione_figlio", is_cod_versione)

if as_figlio <> "NOT FOUND" then
	ls_um = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  as_figlio + "'", "cod_misura_mag" )
	dw_distinta.setitem(ll_new, "cod_misura", ls_um)
else
	dw_distinta.setitem(ll_new, "cod_misura", "NOTFOUND")
end if


dw_distinta.setitem(ll_new, "quan_utilizzo", ad_qta)
dw_distinta.setitem(ll_new, "flag_ok", as_ok)
dw_distinta.setitem(ll_new, "cella_originale", as_cella_originale)

wf_des_prodotto(ll_new, as_figlio, ls_des_prodotto)

as_var_taglio = trim(as_var_taglio)

if as_var_taglio <>"" and not isnull(as_var_taglio) and (upper(as_var_taglio)="L" or upper(as_var_taglio)="H") and as_figlio<>"NOT FOUND"then
	if as_var_taglio="L" then
		as_var_taglio = "DIM_1"
	else
		as_var_taglio = "DIM_2"
	end if
	
	if ad_taglio<=0 and not isnull(ad_taglio) then
		ad_taglio = abs(ad_taglio)
		
		ls_formula_misura = as_var_taglio + " - " + string(ad_taglio)
		
		//inserisci formula DT taglio
		ll_new = dw_dtaglio.insertrow(0)
	
		dw_dtaglio.setitem(ll_new, "cod_azienda", "A01")
		dw_dtaglio.setitem(ll_new, "cod_prodotto_padre", as_padre)
		dw_dtaglio.setitem(ll_new, "num_sequenza", ll_num_sequenza)
		dw_dtaglio.setitem(ll_new, "cod_prodotto_figlio", as_figlio)
		dw_dtaglio.setitem(ll_new, "cod_versione", is_cod_versione)
		dw_dtaglio.setitem(ll_new, "cod_versione_figlio", is_cod_versione)
		dw_dtaglio.setitem(ll_new, "progressivo", 1)
		dw_dtaglio.setitem(ll_new, "des_distinta_taglio", left(ls_des_prodotto, 40))
		dw_dtaglio.setitem(ll_new, "um_misura", "MM")
		dw_dtaglio.setitem(ll_new, "formula_misura", ls_formula_misura)
		dw_dtaglio.setitem(ll_new, "tutti_comandi", "N")
		
	end if
end if



return

end subroutine

on w_importa_db_dekora.create
this.cb_salva_dtaglio=create cb_salva_dtaglio
this.dw_dtaglio=create dw_dtaglio
this.cb_log=create cb_log
this.sle_log=create sle_log
this.st_counter=create st_counter
this.st_log=create st_log
this.cb_leggi=create cb_leggi
this.st_tot=create st_tot
this.sle_sfoglia=create sle_sfoglia
this.cb_sfoglia=create cb_sfoglia
this.dw_lista=create dw_lista
this.dw_distinta=create dw_distinta
this.Control[]={this.cb_salva_dtaglio,&
this.dw_dtaglio,&
this.cb_log,&
this.sle_log,&
this.st_counter,&
this.st_log,&
this.cb_leggi,&
this.st_tot,&
this.sle_sfoglia,&
this.cb_sfoglia,&
this.dw_lista,&
this.dw_distinta}
end on

on w_importa_db_dekora.destroy
destroy(this.cb_salva_dtaglio)
destroy(this.dw_dtaglio)
destroy(this.cb_log)
destroy(this.sle_log)
destroy(this.st_counter)
destroy(this.st_log)
destroy(this.cb_leggi)
destroy(this.st_tot)
destroy(this.sle_sfoglia)
destroy(this.cb_sfoglia)
destroy(this.dw_lista)
destroy(this.dw_distinta)
end on

event close;
if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if
end event

event open;


dw_dtaglio.settransobject(sqlca)
end event

type cb_salva_dtaglio from commandbutton within w_importa_db_dekora
integer x = 4850
integer y = 1596
integer width = 187
integer height = 92
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long				ll_tot, ll_index


ll_tot = dw_dtaglio.rowcount()
if ll_tot<=0 then
	g_mb.error("Nessuna riga da salvare!")
	return
end if

if not g_mb.confirm("Salvo la distinta di taglio?") then return

if dw_dtaglio.update() < 0 then
	g_mb.error("Errore in UPDATE DW!")
	rollback;
	return
end if

commit;
g_mb.success("Distinte Taglio salvate con successo!")

return
end event

type dw_dtaglio from datawindow within w_importa_db_dekora
integer x = 1522
integer y = 1596
integer width = 3273
integer height = 1132
integer taborder = 30
string title = "none"
string dataobject = "d_importa_db_dekora_tagli"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_log from commandbutton within w_importa_db_dekora
integer x = 4096
integer y = 160
integer width = 402
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Log"
end type

event clicked;



is_path_log = sle_log.text


wf_log("~r~n~r~n"+string(today(), "dd-mm-yyyy")+" "+string(now(), "hh:mm_ss")+" INIZIO PROCEDURA IMPORTAZIONE DISTINTE BASI")

g_mb.success("FILE DI LOG IMPOSTATO CORRETTAMENTE!")
end event

type sle_log from singlelineedit within w_importa_db_dekora
integer x = 69
integer y = 156
integer width = 4000
integer height = 112
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_counter from statictext within w_importa_db_dekora
integer x = 1015
integer y = 296
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_log from statictext within w_importa_db_dekora
integer x = 32
integer y = 2764
integer width = 4997
integer height = 324
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_leggi from commandbutton within w_importa_db_dekora
integer x = 4270
integer y = 44
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi"
end type

event clicked;string			ls_file, ls_errore

ls_file = sle_sfoglia.text

if wf_leggi_fogli(ls_file, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return
end if

g_mb.success("Lettura effettuata!")
end event

type st_tot from statictext within w_importa_db_dekora
integer x = 37
integer y = 296
integer width = 928
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Fogli Totali:"
boolean focusrectangle = false
end type

type sle_sfoglia from singlelineedit within w_importa_db_dekora
integer x = 69
integer y = 36
integer width = 4000
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_sfoglia from commandbutton within w_importa_db_dekora
integer x = 4078
integer y = 36
integer width = 133
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[], ls_errore
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	sle_sfoglia.text = docpath
end if
end event

type dw_lista from datawindow within w_importa_db_dekora
integer x = 37
integer y = 396
integer width = 1440
integer height = 2328
integer taborder = 20
string title = "none"
string dataobject = "d_importa_db_dekora"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;string				ls_errore

integer			li_ret


if row > 0 then
	setpointer(Hourglass!)
	li_ret = wf_leggi_distinta(row, ls_errore)
	if li_ret < 0 then
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
	end if
	setpointer(Arrow!)
end if

end event

event buttonclicked;string				ls_errore

integer			li_ret


if row > 0 then
	choose case dwo.name
		case "b_importa"
			li_ret = wf_salva_distinta(row, ls_errore)
			if li_ret<0 then
				g_mb.error(ls_errore)
				rollback;
				
			else
				commit;
				g_mb.success("Salvataggio effettuato con successo!")
				
			end if
			
			return
			
	end choose
end if

end event

type dw_distinta from datawindow within w_importa_db_dekora
integer x = 1504
integer y = 288
integer width = 3534
integer height = 1284
integer taborder = 10
string title = "none"
string dataobject = "d_importa_db_dekora_det"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;string				ls_des_prodotto


if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_prodotto_figlio"
		
		wf_des_prodotto(row, data, ls_des_prodotto)
		
end choose
end event

