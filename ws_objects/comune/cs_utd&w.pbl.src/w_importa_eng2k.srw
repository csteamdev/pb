﻿$PBExportHeader$w_importa_eng2k.srw
forward
global type w_importa_eng2k from w_cs_xx_principale
end type
type em_1 from editmask within w_importa_eng2k
end type
type cb_importa from commandbutton within w_importa_eng2k
end type
type mle_log from multilineedit within w_importa_eng2k
end type
type st_path from statictext within w_importa_eng2k
end type
type cb_path from commandbutton within w_importa_eng2k
end type
end forward

global type w_importa_eng2k from w_cs_xx_principale
integer width = 2816
integer height = 1984
string title = "Importazione ENG2K"
em_1 em_1
cb_importa cb_importa
mle_log mle_log
st_path st_path
cb_path cb_path
end type
global w_importa_eng2k w_importa_eng2k

type variables
string is_path
uo_excel_listini iuo_excel
end variables

forward prototypes
public subroutine wf_scrivi_mle (string fs_valore)
public function integer wf_importa (string fs_foglio)
public function integer wf_carica_riga (string fs_foglio, long fl_riga, ref s_importa_eng2k f_struttura)
public function integer wf_inserisci_riga (s_importa_eng2k f_struttura, long fl_row)
end prototypes

public subroutine wf_scrivi_mle (string fs_valore);mle_log.SetRedraw(FALSE)

mle_log.text += fs_valore + "~r~n"

mle_log.scroll(mle_log.linecount())

mle_log.SetRedraw(TRUE)
end subroutine

public function integer wf_importa (string fs_foglio);long							ll_riga_offset, li_ret,ll_colonna, ll_riga, ll_num_intervento, ll_anno_reg, ll_ret
string						ls_valore, ls_cliente, ls_des_intervento
decimal						ld_valore
datetime					ldt_valore
s_importa_eng2k 		l_struttura, l_vuota

ll_riga_offset = long(em_1.text)
ls_cliente = "600216"//"COMIFAR"
ll_num_intervento = -1
ll_anno_reg = 2010
/*
 1.		DES. INTERVENTO
 2.		NUM INTERVENTO
 4.		DATA REGISTRAZIONE
 7.		CLIENTE

*/

ll_riga = ll_riga_offset

do while 1=1	
	Yield()
	
	l_struttura = l_vuota
	
	//colonna cliente
	ll_colonna = 7
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if li_ret = 1 then
		ls_valore = upper(ls_valore)
		
		if isnull(ls_valore) or ls_valore="" then
			//probabilmente sei alla fine del foglio
			wf_scrivi_mle("-------------------------------------------------------------")
			wf_scrivi_mle("FINE LETTURA DATI: "+string(ll_riga - 1))
			exit
		elseif ls_valore <> ls_cliente then
			//salta perchè non è COMIFAR
			wf_scrivi_mle("Riga "+string(ll_riga) + ": ignorata perchè non è il cliente "+ls_cliente)
			wf_scrivi_mle("---------------------------------------------")
			wf_scrivi_mle("")
			
			//prima però incrementa la riga altrimenti vai in loop
			ll_riga += 1
			continue
		end if
	else
		//probabilmente sei alla fine del foglio
		wf_scrivi_mle("-------------------------------------------------------------")
		wf_scrivi_mle("FINE LETTURA DATI: "+string(ll_riga - 1))
		exit
	end if
	//continua ad elaborare le colonne della riga
	wf_scrivi_mle("Riga "+string(ll_riga)+": Elaborazione in corso -----------------------------------------")
	
	l_struttura.s_cliente = ls_valore
	
	//elabora num. intervento ---------------------------------------------------------------------------------------------------
	ll_colonna = 2
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then		
		//valore letto
		
		//verifica se per caso è lo stesso precedente, se si allora, dato che hai ordinato i dati in excel
		//si tratta di una revisione più bassa: SALTA
		if ld_valore=ll_num_intervento then
			wf_scrivi_mle("Riga "+string(ll_riga) + ": IGNORATA perchè è una REVISIONE PRECEDENTE")
			wf_scrivi_mle("----------------------------------------------------------------------------------------")
			wf_scrivi_mle("")
			
			//prima però incrementa la riga altrimenti vai in loop
			ll_riga += 1
			continue
		else
			//conserva il numero
			ll_num_intervento = long(ld_valore)
			wf_scrivi_mle("Riga "+string(ll_riga) +": Num. intervento: "+string(ll_num_intervento))
		end if
		
	else
		//errore in lettura
		wf_scrivi_mle("Riga "+string(ll_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))		
		return -1
	end if
	//------------------------------------------------------------------------------------------------------------------------------------
	
	l_struttura.l_num_chiamata = ll_num_intervento
	
	//elabora colonna data ---------------------------------------------------------------------------------------------------
	ll_colonna = 4
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "T")
	if li_ret = 1 then

		if ll_anno_reg <> year(date(ldt_valore)) then
			//non elaborare perchè non è dell'anno 2010
			wf_scrivi_mle("Riga "+string(ll_riga) + ": IGNORATA perchè non riguarda l'ANNO 2010: "+string(ldt_valore, "dd-mm-yyyy"))
			wf_scrivi_mle("------------------------------------------------------------------------------------------")
			wf_scrivi_mle("")
			
			//prima però incrementa la riga altrimenti vai in loop
			ll_riga += 1
			continue
		else
			wf_scrivi_mle("Riga "+string(ll_riga) +": Data reg.: "+string(ldt_valore, "dd-mm-yyyy"))
		end if		
	else
		//errore in lettura
		wf_scrivi_mle("Riga "+string(ll_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))		
		return -1
	end if
	//------------------------------------------------------------------------------------------------------------------------------------
	
	//se arrivi fin qui puoi caricare tutto nella struttura
	l_struttura.dt_data_registrazione = datetime(date(ldt_valore), Time("08:00:00"))
	
	//carica qui il resto
	ll_ret = wf_carica_riga(fs_foglio, ll_riga, l_struttura)
	if ll_ret = -1 then
		//log già scritto
		return -1
	elseif ll_ret = 100 then
		//salta la riga
		//prima però incrementa la riga altrimenti vai in loop
		ll_riga += 1
		continue
	end if
	
	//CHIAMA QUI LA FUNZIONE CHE SI ELABORA LA STRUTTURA E FA GLI INSERIMENTI ##############
	if wf_inserisci_riga(l_struttura, ll_riga) <> 1 then
		//log già scritto
		return -1
	end if
	//##################################################################################
	
	wf_scrivi_mle("Riga "+string(ll_riga) +": Fine Elaborazione Excel-----------------------------------------")
	wf_scrivi_mle("")
	
	ll_riga += 1
loop

return 1
end function

public function integer wf_carica_riga (string fs_foglio, long fl_riga, ref s_importa_eng2k f_struttura);long				ll_colonna, li_ret
string			ls_valore, ls_stato
decimal			ld_valore
datetime		ldt_valore

/*MAPPING COLONNE
le colonne numero chiamata (2), data registraz. (4) e cliente (7)
sono state già inserite nella struttura

1. descrizione intervento
6. nostra competenza (MPO)

8. numero offerta cliente
9. data offerta cliente
10. GG
11. numero registro chiamata
12. data inizio intervento  (SE ANNULLATA non è importata)

13. data offerta fornitore
14. tipo offerta fornitore
15. num. offerta fornitore
16. attivo off. forn
17. passivo off. forn
18. fornitore

20. impianto (colonna ubicazione delle richieste)
21. tipo (colonna guasto/tipologia delle richieste)
22. priorita (colonna classe richiesta/urgenza delle richieste)
23. famiglia (colonna tipolog. richiesta/famiglia delle richieste)

24.cod. (fatt.cliente: conto contabile, da tabella conti contabili)
25. periodo fatturazione
26 num.fattura attiva (cliente) -> chiave PK
27. imp. fattura cliente

29. data fine intervento

35. num. ordine (di acquisto fornitore)
36. fornitore
37. num.fattura acquisto fornitore (campo di testo)-> macchè è prorio la chiave  PK
38. data fattura di acquisto fornitore
39. importo fattura acquisto fornitore
*/

//elabora des intervento ----------------------------------------------------------------------------------------------------
ll_colonna = 1
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": Descriz. intervento: "+ls_valore)
	f_struttura.s_des_intervento = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora flag nostra competenza ----------------------------------------------------------------------------------------
ll_colonna = 6
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	if upper(ls_valore) = "X" then
		f_struttura.s_nostra_competenza = "S"
	else
		f_struttura.s_nostra_competenza = "N"
	end if	
else
	f_struttura.s_nostra_competenza = "N"
end if	
wf_scrivi_mle("Riga "+string(fl_riga) +": MPO: "+f_struttura.s_nostra_competenza)	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora numero offerta cliente ----------------------------------------------------------------------------------------------------
ll_colonna = 8
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.s_num_off_cli = ls_valore
	else
		f_struttura.s_num_off_cli = ""
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": N° offerta cliente: "+string(f_struttura.s_num_off_cli))	
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora data offerta cliente ----------------------------------------------------------------------------------------------------
ll_colonna = 9
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then	
	if isdate(string(date(ls_valore),"dd/mm/yyyy")) and year(date(ls_valore))>1990 then
		f_struttura.dt_data_off_cli = datetime(date(ls_valore),time("00:00:00"))
	else
		setnull(f_struttura.dt_data_off_cli)
	end if
	if isnull(f_struttura.dt_data_off_cli) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": data offerta cliente: non specificata")
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": data offerta cliente: "+string(f_struttura.dt_data_off_cli,"dd-mm-yyyy"))
	end if
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora GG (pagamento offerta cliente) ------------------------------------------------------------------------------
ll_colonna = 10
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if trim(ls_valore)="-" then
		setnull(f_struttura.s_gg_pag_off_cli)
		wf_scrivi_mle("Riga "+string(fl_riga) +": GG pag. offerta cliente: (-) non specificata: considero vuoto")
	else
		f_struttura.s_gg_pag_off_cli = ls_valore	
		wf_scrivi_mle("Riga "+string(fl_riga) +": GG pag. offerta cliente: "+f_struttura.s_gg_pag_off_cli)
	end if
else
//	//errore in lettura
//	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
//	return -1
	
	setnull(f_struttura.s_gg_pag_off_cli)
	wf_scrivi_mle("Riga "+string(fl_riga) +": GG pag. offerta cliente: non specificata: considero vuoto")
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora numero registro chiamata cliente -------------------------------------------------------------------------------
ll_colonna = 11
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.l_num_chiamata_registro = long(ls_valore)
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
		return -1
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": N° offerta cliente: "+string(f_struttura.s_num_off_cli))	
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora data inizio intervento----------------------------------------------------------------------------------------------
ll_colonna = 12
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then	
	if isdate(string(date(ls_valore),"dd/mm/yyyy")) and year(date(ls_valore))>1990 then
		f_struttura.dt_data_inizio = datetime(date(ls_valore),time("00:00:00"))
		ls_stato = "ACCETTATA"
	else
		setnull(f_struttura.dt_data_inizio)	
		ls_stato = upper(ls_valore)
	end if
	if isnull(f_struttura.dt_data_inizio) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": data inizio intervento: "+ls_valore+"    stato: "+ls_stato)
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": data inizio intervento: "+string(f_struttura.dt_data_inizio,"dd-mm-yyyy"))
	end if
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if

choose case ls_stato
	case "SOSPESA"
		f_struttura.s_flag_stato_off_cliente = "S"
		
	case "ACCETTATA"
		f_struttura.s_flag_stato_off_cliente = "A"
		
	case "ANNULLATA"
		f_struttura.s_flag_stato_off_cliente = "N"
		wf_scrivi_mle("Riga "+string(fl_riga) +": Richiesta Annullata NON sarà importata!")	
		return 100
		
	case else
		f_struttura.s_flag_stato_off_cliente = "A"
		
end choose

//------------------------------------------------------------------------------------------------------------------------------------

//elabora data fine intervento----------------------------------------------------------------------------------------------
ll_colonna = 29
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then	
	if isdate(string(date(ls_valore),"dd/mm/yyyy")) and year(date(ls_valore))>1990 then
		f_struttura.dt_data_fine = datetime(date(ls_valore),time("00:00:00"))
	else
		setnull(f_struttura.dt_data_fine)
	end if
	if isnull(f_struttura.dt_data_fine) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": data fine intervento: "+ls_valore)
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": data fine intervento: "+string(f_struttura.dt_data_fine,"dd-mm-yyyy"))
	end if
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora data offerta fornitore-----------------------------------------------------------------------------------------------
ll_colonna = 13
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then	
	if isdate(string(date(ls_valore),"dd/mm/yyyy")) and year(date(ls_valore))>1990 then
		f_struttura.dt_data_off_forn = datetime(date(ls_valore),time("00:00:00"))
	else
		setnull(f_struttura.dt_data_off_forn)
	end if
	if isnull(f_struttura.dt_data_off_forn) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": data offerta fornitore: non specificata")
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": data offerta fornitore: "+string(f_struttura.dt_data_off_forn,"dd-mm-yyyy"))
	end if
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora tipo offerta fornitore ----------------------------------------------------------------------------------------------------
ll_colonna = 14
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 and ls_valore<>"" and not isnull(ls_valore) then	
	f_struttura.s_tipo_off_forn = ls_valore
	wf_scrivi_mle("Riga "+string(fl_riga) +": tipo offerta fornitore: "+ls_valore)
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora num. offerta fornitore --------------------------------------------------------------------------------------------
ll_colonna = 15
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	f_struttura.s_num_off_forn = ls_valore
	if isnumber(ls_valore) then
		f_struttura.s_num_off_forn = ls_valore
	else
		f_struttura.s_num_off_forn = ""
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": Num. offerta fornitore: "+string(f_struttura.s_num_off_forn))	
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora imp. attivo offerta fornitore ----------------------------------------------------------------------------------------------------
ll_colonna = 16
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.d_imponibile_attivo = dec(ls_valore)
	else
		f_struttura.d_imponibile_attivo = 0
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": imp.attivo off.forn.: "+string(f_struttura.d_imponibile_attivo, "######0.00"))
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " considero ZERO")	
	f_struttura.d_imponibile_attivo = 0
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora imp. passivo offerta fornitore -----------------------------------------------------------------------------------------------
ll_colonna = 17
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.d_imponibile_passivo = dec(ls_valore)
	else
		f_struttura.d_imponibile_passivo = 0
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": imp.passivo off.forn.: "+string(f_struttura.d_imponibile_passivo, "######0.00"))
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " considero ZERO")	
	f_struttura.d_imponibile_passivo = 0
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora fornitore (offerta)----------------------------------------------------------------------------------------------------
ll_colonna = 18
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": Fornitore (offerta): "+ls_valore)
	f_struttura.s_fornitore = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati fornitore offerta (colonna: "+string(ll_colonna)+") CONSIDERO NESSUN FORNITORE")
	f_struttura.s_fornitore = ""	
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora impianto ----------------------------------------------------------------------------------------------------
ll_colonna = 20
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": Impianto: "+upper(ls_valore))
	f_struttura.s_impianto = upper(ls_valore)
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati impianto (colonna: "+string(ll_colonna)+") considero nessun impianto")	
	setnull(f_struttura.s_impianto)
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora tipo (guasto/tipologia) -----------------------------------------------------------------------------------------------
ll_colonna = 21
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	wf_scrivi_mle("Riga "+string(fl_riga) +": tipo: "+upper(ls_valore))
	f_struttura.s_tipo_chiamata = upper(ls_valore)
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati tipo (colonna: "+string(ll_colonna)+") considero nessun tipo")	
	setnull(f_struttura.s_tipo_chiamata)
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora priorità (classe richiesta/urgenza) ------------------------------------------------------------------------------
ll_colonna = 22
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": priorità: "+upper(ls_valore))
	f_struttura.s_priorita = upper(ls_valore)
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati priorità (colonna: "+string(ll_colonna)+") considero nessuna priorità")	
	setnull(f_struttura.s_priorita)
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora famiglia (tip. richiesta/famiglia) ----------------------------------------------------------------------------------------------------
ll_colonna = 23
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": Famiglia: "+upper(ls_valore))
	f_struttura.s_famiglia = upper(ls_valore)
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati famiglia (colonna: "+string(ll_colonna)+") considero nessuna famiglia")	
	setnull(f_struttura.s_famiglia)
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora conto contabile  ----------------------------------------------------------------------------------------------------
ll_colonna = 24
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": Conto Contabile: "+ls_valore)
	f_struttura.s_conto_contabile_cli = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati conto contabile (colonna: "+string(ll_colonna)+") considero nessun c.c.")	
	f_struttura.s_conto_contabile_cli = ""
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora ? periodo fatturazione (cliente)  ----------------------------------------------------------------------------------------------------
ll_colonna = 25
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": col.? (per. fatt. cli.): "+ls_valore)
	f_struttura.s_interrogativo_cli = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati ?per.fatt.cli. (colonna: "+string(ll_colonna)+") considero nessun ?per.fatt.cli.")	
	f_struttura.s_interrogativo_cli = ""
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora num fattura attiva (cliente)  ----------------------------------------------------------------------------------------------------
ll_colonna = 26
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	if isnumber(ls_valore) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": num.fattura attiva (cliente): "+ls_valore)
		f_struttura.s_num_fatt_attiva_cli = ls_valore
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " non creerò la fatt.ven.")	
		f_struttura.s_num_fatt_attiva_cli = ""
	end if	
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " non creerò la fatt.ven.")	
	f_struttura.s_num_fatt_attiva_cli = ""
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora imp. fattura cliente ----------------------------------------------------------------------------------------------------
ll_colonna = 27
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.d_importo_fattura_cli = dec(ls_valore)
	else
		f_struttura.d_importo_fattura_cli = 0
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": imp.fattura cliente.: "+string(f_struttura.d_importo_fattura_cli, "######0.00"))
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati imp.fattura cliente: colonna: "+string(ll_colonna)+ " considero ZERO")	
	f_struttura.d_importo_fattura_cli = 0
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora num. ordine (di acquisto fornitore)  ----------------------------------------------------------------------------------------------------
ll_colonna = 35
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": col.num.ord. acq. (fornitore): "+ls_valore)
	f_struttura.s_num_ord_acq = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati num.ord. acq. (fornitore) (colonna: "+string(ll_colonna)+") considero nessun num.ord. acq. (fornitore)")	
	f_struttura.s_num_ord_acq = ""
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora fornitore fattura acq ----------------------------------------------------------------------------------------------------
ll_colonna = 36
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then		
	wf_scrivi_mle("Riga "+string(fl_riga) +": col. fornitore fattura acq: "+ls_valore)
	f_struttura.s_fornitore_fatt = ls_valore
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati fornitore fattura acq (colonna: "+string(ll_colonna)+")")	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora num.fattura acquisto fornitore (campo di testo) NO è la PK  ----------------------------------------------------------------------------------------------------
ll_colonna = 37
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	if isnumber(ls_valore) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": col.num.fattura acquisto fornitore: "+ls_valore)
		f_struttura.s_num_fatt_forn = ls_valore
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " non creerò la fatt.acq.")	
		f_struttura.s_num_fatt_forn = ""
	end if	
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna)+ " non creerò la fatt.acq.")	
	f_struttura.s_num_fatt_forn = ""
end if	

//------------------------------------------------------------------------------------------------------------------------------------

//elabora data fattura di acquisto fornitore-----------------------------------------------------------------------------------------------
ll_colonna = 38
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then	
	if isdate(string(date(ls_valore),"dd/mm/yyyy")) and year(date(ls_valore))>1990 then
		f_struttura.d_data_fattura_forn = datetime(date(ls_valore),time("00:00:00"))
	else
		setnull(f_struttura.d_data_fattura_forn)
	end if
	if isnull(f_struttura.d_data_fattura_forn) then
		wf_scrivi_mle("Riga "+string(fl_riga) +": data fattura di acquisto fornitore: non specificata")
	else
		wf_scrivi_mle("Riga "+string(fl_riga) +": data fattura di acquisto fornitore: "+string(f_struttura.d_data_fattura_forn,"dd-mm-yyyy"))
	end if
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati: colonna: "+string(ll_colonna))	
	return -1
end if	
//------------------------------------------------------------------------------------------------------------------------------------

//elabora importo fattura acquisto fornitore ----------------------------------------------------------------------------------------------------
ll_colonna = 39
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, fl_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")

if li_ret = 1 then
	
	if isnumber(ls_valore) then
		f_struttura.d_importo_fattura_forn = dec(ls_valore)
	else
		f_struttura.d_importo_fattura_forn = 0
	end if
	
	wf_scrivi_mle("Riga "+string(fl_riga) +": imp.fattura fornitore.: "+string(f_struttura.d_importo_fattura_forn, "######0.00"))
else
	//errore in lettura
	wf_scrivi_mle("Riga "+string(fl_riga) +": Errore in lettura dati imp.fattura fornitore: colonna: "+string(ll_colonna)+ " considero ZERO")	
	f_struttura.d_importo_fattura_forn = 0
end if	
//------------------------------------------------------------------------------------------------------------------------------------

return 1
end function

public function integer wf_inserisci_riga (s_importa_eng2k f_struttura, long fl_row);//N.B. le anagrafiche devono essere già presenti
/*
fornitore
risorsa esterna e categoria risorsa esterna
cliente
tipo pagamento
tipologia offerta
impianto
guasto
class. richiesta
tip. richiesta
conti contabili
*/

long ll_anno_richiesta, ll_num_reg_intervento, ll_num_off_acq, ll_num_off_ven
long ll_num_reg_fatt_ven, ll_num_reg_fatt_acq
datetime ldt_data_reg, ldt_ora_reg, ldt_data_inizio_intervento, ldt_data_fine_intervento, ldt_data_temp
string ls_cod_cat_risorse_esterne, ls_cod_risorsa_esterna, ls_flag_chiusura, ls_msg
uo_calcola_documento_euro luo_doc

//off.ven.
string ls_cod_tipo_off_ven, ls_cod_tipo_off_ven_det
//off.acq.
string  ls_cod_tipo_off_acq_det
//fat.acq.
string ls_cod_tipo_fat_acq, ls_cod_tipo_fat_acq_det
//fat.ven.
string ls_cod_tipo_fat_ven, ls_cod_tipo_fat_ven_det

ll_anno_richiesta = year(date(f_struttura.dt_data_registrazione))
ldt_data_reg = datetime(date(f_struttura.dt_data_registrazione), time("00:00:00"))
ldt_ora_reg = datetime(date(1900, 1, 1), time(f_struttura.dt_data_registrazione))
ldt_data_inizio_intervento=datetime(date(f_struttura.dt_data_inizio),time("00:00:00"))
ldt_data_fine_intervento=datetime(date(f_struttura.dt_data_fine),time("00:00:00"))

ls_flag_chiusura = "S"

//dati iniziali statici ---------------------------------------------------
ls_cod_tipo_off_ven= "001"
ls_cod_tipo_off_ven_det= "001"

ls_cod_tipo_off_acq_det= "001"

ls_cod_tipo_fat_acq= "001"
ls_cod_tipo_fat_acq_det= "001"

ls_cod_tipo_fat_ven= "001"
ls_cod_tipo_fat_ven_det= "001"

//ls_cod_centro_costo = ""
//----------------------------------------------------------------------------

//se c'è la data fine intervento chiudi la chiamata e l'intervento
if isnull(ldt_data_fine_intervento) then ls_flag_chiusura="N"

//recupera la risorsa esterna
select 	cod_cat_risorse_esterne,
			cod_risorsa_esterna
into		:ls_cod_cat_risorse_esterne,
			:ls_cod_risorsa_esterna
from anag_risorse_esterne
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore=:f_struttura.s_fornitore;
			
if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore","Errore in lettura risorsa esterna (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
	return -1
elseif sqlca.sqlcode=100 then
	g_mb.messagebox("Errore","Risorsa Esterna non trovata (riga "+string(fl_row)+")", StopSign!)
	return -1
end if

//inserisci richiesta
insert into tab_richieste
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	data_registrazione,
	ora_registrazione,
	flag_competenza,
	cod_area_aziendale,
	cod_guasto,					//tipo
	cod_classe_richiesta,	//priorità
	cod_tipologia_richiesta, //famiglia
	cod_documento,
	anno_documento,
	numeratore_documento,
	num_documento,
	flag_richiesta_straordinaria,
	cod_cat_risorse_esterne,
	cod_risorsa_esterna,
	des_richiesta,
	flag_richiesta_chiusa,
	cod_divisione)	
values
(	:s_cs_xx.cod_azienda,
	:ll_anno_richiesta,
	:f_struttura.l_num_chiamata,
	:ldt_data_reg,
	:ldt_ora_reg,
	:f_struttura.s_nostra_competenza,
	:f_struttura.s_impianto,
	:f_struttura.s_tipo_chiamata,
	:f_struttura.s_priorita,
	:f_struttura.s_famiglia,
	'001',
	2010,
	'A',
	:f_struttura.l_num_chiamata_registro,
	'S',
	:ls_cod_cat_risorse_esterne,
	:ls_cod_risorsa_esterna,
	:f_struttura.s_des_intervento,
	:ls_flag_chiusura,
	'001');

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore","Errore in inserimento richiesta (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
	return -1
end if

select max(num_registrazione)
into :ll_num_reg_intervento
from manutenzioni
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_richiesta;
			
if isnull(ll_num_reg_intervento) then ll_num_reg_intervento = 0
ll_num_reg_intervento += 1

//--------------------------------------------------------------------------------------------------------------

//inserisci l'intervento
insert into manutenzioni
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	anno_reg_richiesta,
	num_reg_richiesta,
	data_registrazione,
	data_inizio_intervento,
	data_fine_intervento,
	flag_straordinario,
	flag_ordinario,
	flag_eseguito)
values
(	:s_cs_xx.cod_azienda,
	:ll_anno_richiesta,
	:ll_num_reg_intervento,
	:ll_anno_richiesta,
	:f_struttura.l_num_chiamata,
	:ldt_data_reg,
	:ldt_data_inizio_intervento,
	:ldt_data_fine_intervento,
	'S',
	'N',
	:ls_flag_chiusura
);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore","Errore in inserimento intervento (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
	return -1
end if

//inserisci la fase
insert into manutenzioni_fasi
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_fase,
	des_fase,
	data_inizio,
	data_fine)
values
(	:s_cs_xx.cod_azienda,
	:ll_anno_richiesta,
	:ll_num_reg_intervento,
	1,
	"Fase 1",
	:ldt_data_inizio_intervento,
	:ldt_data_fine_intervento);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore","Errore in inserimento fase intervento (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
	return -1
end if

//inserisci la fase risorsa esterna
insert into manutenzioni_fasi_risorse
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_fase,
	cod_cat_risorse_esterne,
	cod_risorsa_esterna,
	data_inizio,
	data_fine)
values
(	:s_cs_xx.cod_azienda,
	:ll_anno_richiesta,
	:ll_num_reg_intervento,
	1,
	:ls_cod_cat_risorse_esterne,
	:ls_cod_risorsa_esterna,
	:ldt_data_inizio_intervento,
	:ldt_data_fine_intervento);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Errore","Errore in inserimento fase risorsa esterna intervento (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
	return -1
end if
//--------------------------------------------------------------------------------------------------------------

//if not isnull(f_struttura.s_num_off_cli) and f_struttura.s_num_off_cli<>"" then
if f_struttura.d_imponibile_attivo > 0 then
	//crea la offerta cliente accettata
	//ll_num_off_ven = long(f_struttura.s_num_off_cli)
	
	//creo la pk
	select max(num_registrazione)
	into :ll_num_off_ven
	from tes_off_ven
	where cod_azienda=:s_cs_xx.cod_azienda and anno_registrazione=:ll_anno_richiesta;
	
	if isnull(ll_num_off_ven) then ll_num_off_ven = 0
	ll_num_off_ven += 1
		
	if isnull(f_struttura.dt_data_off_cli) then
		ldt_data_temp = ldt_data_reg
	else
		ldt_data_temp = f_struttura.dt_data_off_cli
	end if
	
	insert into tes_off_ven
		(cod_azienda,
		anno_registrazione,
		num_registrazione,
		cod_cliente,
		data_registrazione,
		cod_tipo_off_ven,
		cod_pagamento,
		data_acc_off,
		flag_stato_off,
		num_ric_cliente,
		cod_deposito,
		cod_valuta)
	values
		(:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_off_ven,
		:f_struttura.s_cliente,
		:ldt_data_temp,
		:ls_cod_tipo_off_ven,
		:f_struttura.s_gg_pag_off_cli,
		:f_struttura.dt_data_off_cli,
		'A',
		:f_struttura.s_num_off_cli,
		'001',
		'EUR');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata offerta cliente (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	//inserisci il dettaglio dell'offerta cliente
	insert into det_off_ven
		(cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_off_ven,
		cod_tipo_det_ven,
		imponibile_iva,
		prezzo_um,
		prezzo_vendita,
		quantita_um,
		quan_offerta,
		anno_reg_richiesta,
		num_reg_richiesta,
		des_prodotto,
		cod_iva)
	values
		(:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_off_ven,
		1,
		:ls_cod_tipo_off_ven_det,
		:f_struttura.d_imponibile_attivo,
		:f_struttura.d_imponibile_attivo,
		:f_struttura.d_imponibile_attivo,
		1,
		1,
		:ll_anno_richiesta,
		:f_struttura.l_num_chiamata,
		'IMPORTO',
		'20');
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento dettaglio offerta cliente (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	luo_doc = create uo_calcola_documento_euro
	if luo_doc.uof_calcola_documento(ll_anno_richiesta,ll_num_off_ven,"off_ven", ls_msg) = -1 then
		g_mb.messagebox("Errore","Errore in calcolo offerta cliente (riga "+string(fl_row)+"): "+ls_msg, StopSign!)
		destroy luo_doc
		return -1
	end if
	destroy luo_doc
	
	wf_scrivi_mle("Riga "+string(fl_row) +": inserita offerta n°"+string(ll_num_off_ven))	
else
	wf_scrivi_mle("Riga "+string(fl_row) +": offerta NON creata per assenza di importo")	
end if

if not isnull(f_struttura.s_num_off_forn) and f_struttura.s_num_off_forn<>"" then
	//crea la offerta fornitore
	ll_num_off_acq = long(f_struttura.s_num_off_forn)
	
	insert into tes_off_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		cod_fornitore,
		cod_tipo_off_acq,
		data_registrazione,
		anno_richiesta,
		num_richiesta,
		cod_deposito,
		cod_valuta)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_off_acq,
		:f_struttura.s_fornitore,
		:f_struttura.s_tipo_off_forn,
		:f_struttura.dt_data_off_forn,
		:ll_anno_richiesta,
		:f_struttura.l_num_chiamata,
		'001',
		'EUR');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata offerta fornitore (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	//inserisci il dettaglio dell'offerta fornitore
	insert into det_off_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_off_acq,
		cod_tipo_det_acq,
		imponibile_iva,
		quan_ordinata,
		prezzo_acquisto,
		cod_iva,
		des_prodotto)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_off_acq,
		1,
		:ls_cod_tipo_off_acq_det,
		:f_struttura.d_imponibile_passivo,
		1,
		:f_struttura.d_imponibile_passivo,
		'20',
		'IMPORTO');
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento dettaglio offerta fornitore (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	luo_doc = create uo_calcola_documento_euro	
	if luo_doc.uof_calcola_documento(ll_anno_richiesta,ll_num_off_acq,"off_acq", ls_msg) = -1 then
		g_mb.messagebox("Errore", "Errore in calcolo offerta fornitore (riga "+string(fl_row)+") "+ls_msg, StopSign!)
		destroy luo_doc
		return -1
	end if
	destroy luo_doc
	
end if

//dati fatturazione cliente
//1 testata e un dettaglio
//per le fatture calcolo la chiave
if not isnull(f_struttura.s_num_fatt_attiva_cli) and f_struttura.s_num_fatt_attiva_cli<>"" then
	ll_num_reg_fatt_ven = long(f_struttura.s_num_fatt_attiva_cli)
	
	//conto contabile  det_fat_ven
	//periodo fatturazione
	//numero fattura
	//importo fattura
	
	datetime ldt_data_reg_fatt_ven
	string ls_temp1, ls_temp2
	long ll_pos, ll_temp1, ll_temp2, ll_gg
	
	setnull(ldt_data_reg_fatt_ven)

	//periodo fatturazione va in data registrazione
	if not isnull(f_struttura.s_interrogativo_cli) and f_struttura.s_interrogativo_cli<>"" then
		if upper(left(f_struttura.s_interrogativo_cli, 2)) = "SI" then			
			ll_pos = pos(f_struttura.s_interrogativo_cli, "-")
			if ll_pos>0 then
				//anno
				ls_temp1 = right(f_struttura.s_interrogativo_cli, 2)
				
				//mese
				ls_temp2 = mid(f_struttura.s_interrogativo_cli, 3, 2)
				
				if isnumber(ls_temp1) and isnumber(ls_temp2) then
					
					ll_temp1 = long(ls_temp1)	//anno
					ll_temp2 = long(ls_temp2)	//mese
					
					ll_temp1 += 2000
					
					if ll_temp2 = 2 then
						ll_gg = 28
					elseif ll_temp2=4 or ll_temp2=6 or ll_temp2=9 or ll_temp2=11 then
						ll_gg = 30
					else
						ll_gg = 31
					end if
					
					ldt_data_reg_fatt_ven = datetime(date(ll_temp1, ll_temp2, ll_gg), time("00:00:00"))
				end if
			end if
		end if
	end if
	
	
	insert into tes_fat_ven
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		data_registrazione,
		cod_tipo_fat_ven,
		cod_cliente,
		anno_reg_richiesta,
		num_reg_richiesta,
		cod_deposito,
		cod_valuta,
		cod_pagamento)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_reg_fatt_ven,
		:ldt_data_reg_fatt_ven,
		:ls_cod_tipo_fat_ven,
		:f_struttura.s_cliente,
		:ll_anno_richiesta,
		:f_struttura.l_num_chiamata,
		'001',
		'EUR',
		:f_struttura.s_gg_pag_off_cli);
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata fattura vendita (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	//inserisci dettaglio fattura vendita
	insert into det_fat_ven
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_fat_ven,
		cod_tipo_det_ven,
		quan_fatturata,
		prezzo_vendita,
		cod_iva,
		des_prodotto)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_reg_fatt_ven,
		1,
		:ls_cod_tipo_fat_ven_det,
		1,
		:f_struttura.d_importo_fattura_cli,
		'20',
		'IMPORTO');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento dettaglio fattura vendita (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	//inserisci dettaglio conti contabili fatt.ven.
	insert into det_fat_ven_cc
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_fat_ven,
		cod_centro_costo,
		cod_conto,
		percentuale,
		importo)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_reg_fatt_ven,
		1,
		:f_struttura.s_conto_contabile_cli,
		:f_struttura.s_conto_contabile_cli,
		100,
		:f_struttura.d_importo_fattura_cli);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento dettaglio fattura vendita cc (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
//	luo_doc = create uo_calcola_documento_euro	
//	if luo_doc.uof_calcola_documento(ll_anno_richiesta,ll_num_reg_fatt_ven,"fat_ven", ls_msg) = -1 then
//		g_mb.messagebox("Errore", "Errore in calcolo fattura vendita (riga "+string(fl_row)+") "+ls_msg, StopSign!)
//		destroy luo_doc
//		return -1
//	end if
//	destroy luo_doc
	
end if
//-----------------------

//dati fatturazione fornitore
//1 testata e un dettaglio
if not isnull(f_struttura.s_num_fatt_forn) and f_struttura.s_num_fatt_forn<>"" then
	//ll_num_reg_fatt_acq = long(f_struttura.s_num_fatt_forn)
	
	//calcolo la pk
	select max(num_registrazione)
	into :ll_num_reg_fatt_acq
	from tes_fat_acq
	where cod_azienda=:s_cs_xx.cod_azienda and anno_registrazione = :ll_anno_richiesta ;
	
	if isnull(ll_num_reg_fatt_acq) then ll_num_reg_fatt_acq = 0
	ll_num_reg_fatt_acq += 1
	
	//conto contabile
	//periodo fatturazione
	//numero fattura
	//importo fattura
	
	long ll_anno_doc_origine, ll_num_doc_origine	
	
	setnull(ll_anno_doc_origine)
	setnull(ll_num_doc_origine)
	
	if not isnull(f_struttura.s_num_ord_acq) and f_struttura.s_num_ord_acq<>"" then		
		ll_pos = pos(f_struttura.s_num_ord_acq, "-")
		if ll_pos>0 then
			ls_temp1 = left(f_struttura.s_num_ord_acq, ll_pos -1)
			ls_temp2 = right(f_struttura.s_num_ord_acq, len(f_struttura.s_num_ord_acq) - ll_pos)
			
			if isnumber(ls_temp1) and isnumber(ls_temp2) then
				ll_num_doc_origine = long(ls_temp1)
				ll_anno_doc_origine = long(ls_temp2)
				
				ll_anno_doc_origine += 2000
			end if
		end if
	end if
	
	
	insert into tes_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		data_registrazione,
		cod_tipo_fat_acq,
		cod_fornitore,
		des_num_fat_acq,
		anno_reg_richiesta,
		num_reg_richiesta,		
		cod_valuta,
		protocollo,
		anno_doc_origine,
		num_doc_origine)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_reg_fatt_acq,
		:f_struttura.d_data_fattura_forn,
		:ls_cod_tipo_fat_acq,
		:f_struttura.s_fornitore_fatt,
		:f_struttura.s_num_fatt_forn,
		:ll_anno_richiesta,
		:f_struttura.l_num_chiamata,
		'EUR',
		:f_struttura.s_num_fatt_forn,
		:ll_anno_doc_origine,
		:ll_num_doc_origine);
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento testata fattura vendita (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	//inserisci dettaglio fattura acquisto
	insert into det_fat_acq
	(	cod_azienda,
		anno_registrazione,
		num_registrazione,
		prog_riga_fat_acq,
		cod_tipo_det_acq,
		imponibile_iva,
		flag_imponibile_forzato,
		quan_fatturata,
		prezzo_acquisto,
		des_prodotto)
	values
	(	:s_cs_xx.cod_azienda,
		:ll_anno_richiesta,
		:ll_num_reg_fatt_acq,
		1,
		:ls_cod_tipo_fat_ven_det,
		:f_struttura.d_importo_fattura_forn,
		'N',
		1,
		:f_struttura.d_importo_fattura_forn,
		'IMPORTO');
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore","Errore in inserimento dettaglio fattura vendita (riga "+string(fl_row)+") "+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
//	luo_doc = create uo_calcola_documento_euro	
//	if luo_doc.uof_calcola_documento(ll_anno_richiesta,ll_num_reg_fatt_ven,"fat_acq", ls_msg) = -1 then
//		g_mb.messagebox("Errore", "Errore in calcolo fattura vendita (riga "+string(fl_row)+") "+ls_msg, StopSign!)
//		destroy luo_doc
//		return -1
//	end if
//	destroy luo_doc
	
end if

return 1
end function

on w_importa_eng2k.create
int iCurrent
call super::create
this.em_1=create em_1
this.cb_importa=create cb_importa
this.mle_log=create mle_log
this.st_path=create st_path
this.cb_path=create cb_path
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_1
this.Control[iCurrent+2]=this.cb_importa
this.Control[iCurrent+3]=this.mle_log
this.Control[iCurrent+4]=this.st_path
this.Control[iCurrent+5]=this.cb_path
end on

on w_importa_eng2k.destroy
call super::destroy
destroy(this.em_1)
destroy(this.cb_importa)
destroy(this.mle_log)
destroy(this.st_path)
destroy(this.cb_path)
end on

type em_1 from editmask within w_importa_eng2k
integer x = 2021
integer y = 208
integer width = 402
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type cb_importa from commandbutton within w_importa_eng2k
integer x = 1170
integer y = 204
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "inizio"
end type

event clicked;string		ls_sheets[], ls_nome_foglio
integer		li_count, li_ret
long			ll_tot

wf_scrivi_mle("")

if not FileExists(is_path) then
	wf_scrivi_mle("Il file non esiste oppure è stato spostato!")
	return
end if

if g_mb.messagebox("APICE","Importare il contenuto del file '"+&
							is_path+"' ?",Question!,YesNo!,2)=1 then	
else
	return
end if

iuo_excel = create uo_excel_listini

iuo_excel.is_path_file = is_path
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_sheets)

ll_tot = upperbound(ls_sheets)

if isnull(ll_tot) or ll_tot <= 0 then
	wf_scrivi_mle("Non esistono fogli di lavoro nel file excel specificato!")
	iuo_excel.uof_close( )
	destroy iuo_excel
	return
end if

ls_nome_foglio = ls_sheets[1]
ll_tot = wf_importa(ls_nome_foglio)

if ll_tot = 1 then
	commit;
	wf_scrivi_mle("Importazione terminata con successo!")
else
	rollback;
	wf_scrivi_mle("Importazione terminata con ERRORI!")
end if

iuo_excel.uof_close()
destroy iuo_excel
end event

type mle_log from multilineedit within w_importa_eng2k
integer x = 37
integer y = 596
integer width = 2706
integer height = 1240
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_path from statictext within w_importa_eng2k
integer x = 37
integer y = 40
integer width = 2574
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_path from commandbutton within w_importa_eng2k
integer x = 2619
integer y = 40
integer width = 142
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[], ls_sheets[], ls_nome_foglio
integer		li_count, li_ret
long			ll_tot

is_path = ""
mle_log.text = ""

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)
												
if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	st_path.text = docpath
	wf_scrivi_mle("OK apertura file excel!")
	is_path = docpath
else
	wf_scrivi_mle("Errore in apertura file excel!")
	return
end if
end event

