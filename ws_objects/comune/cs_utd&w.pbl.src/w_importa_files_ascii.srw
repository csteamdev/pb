﻿$PBExportHeader$w_importa_files_ascii.srw
$PBExportComments$Gestione Importazione File ASCII
forward
global type w_importa_files_ascii from w_cs_xx_principale
end type
type cb_chiudi_log from commandbutton within w_importa_files_ascii
end type
type cbx_cat_com from checkbox within w_importa_files_ascii
end type
type st_5 from statictext within w_importa_files_ascii
end type
type cb_cerca_cat_com from commandbutton within w_importa_files_ascii
end type
type cb_cat_commerciali from commandbutton within w_importa_files_ascii
end type
type sle_file_cat_commerciali from singlelineedit within w_importa_files_ascii
end type
type sle_file_origine_commesse from singlelineedit within w_importa_files_ascii
end type
type sle_file_origine_clienti from singlelineedit within w_importa_files_ascii
end type
type st_2 from statictext within w_importa_files_ascii
end type
type sle_file_origine from singlelineedit within w_importa_files_ascii
end type
type cb_carica_clienti from commandbutton within w_importa_files_ascii
end type
type cb_carica_prodotti from commandbutton within w_importa_files_ascii
end type
type cb_carica_commesse from commandbutton within w_importa_files_ascii
end type
type st_1 from statictext within w_importa_files_ascii
end type
type sle_file_origine_fornitori from singlelineedit within w_importa_files_ascii
end type
type cb_carica_fornitori from commandbutton within w_importa_files_ascii
end type
type sle_file_origine_tes_fasi_lavoro from singlelineedit within w_importa_files_ascii
end type
type cb_carica_tes_fasi from commandbutton within w_importa_files_ascii
end type
type sle_file_origine_distinta from singlelineedit within w_importa_files_ascii
end type
type cb_carica_distinta from commandbutton within w_importa_files_ascii
end type
type sle_file_origine_descriz_1 from singlelineedit within w_importa_files_ascii
end type
type cb_carica_descrizioni_1 from commandbutton within w_importa_files_ascii
end type
type st_10 from statictext within w_importa_files_ascii
end type
type st_stringa from statictext within w_importa_files_ascii
end type
type cb_cerca_prodotti from commandbutton within w_importa_files_ascii
end type
type cb_cerca_clienti from commandbutton within w_importa_files_ascii
end type
type cb_cerca_fornitori from commandbutton within w_importa_files_ascii
end type
type cb_cerca_commesse from commandbutton within w_importa_files_ascii
end type
type cb_cerca_tes_fasi from commandbutton within w_importa_files_ascii
end type
type cb_cerca_distinta from commandbutton within w_importa_files_ascii
end type
type cb_cerca_descrizioni_1 from commandbutton within w_importa_files_ascii
end type
type st_3 from statictext within w_importa_files_ascii
end type
type st_21 from statictext within w_importa_files_ascii
end type
type st_22 from statictext within w_importa_files_ascii
end type
type st_23 from statictext within w_importa_files_ascii
end type
type st_24 from statictext within w_importa_files_ascii
end type
type st_25 from statictext within w_importa_files_ascii
end type
type sle_file_origine_det_fasi_lavoro from singlelineedit within w_importa_files_ascii
end type
type cb_carica_det_fasi from commandbutton within w_importa_files_ascii
end type
type cb_cerca_det_fasi from commandbutton within w_importa_files_ascii
end type
type st_62 from statictext within w_importa_files_ascii
end type
type sle_file_origine_descriz_2 from singlelineedit within w_importa_files_ascii
end type
type sle_file_origine_descriz_3 from singlelineedit within w_importa_files_ascii
end type
type sle_file_origine_descriz_4 from singlelineedit within w_importa_files_ascii
end type
type cb_cerca_descrizioni_2 from commandbutton within w_importa_files_ascii
end type
type cb_cerca_descrizioni_3 from commandbutton within w_importa_files_ascii
end type
type cb_cerca_descrizioni_4 from commandbutton within w_importa_files_ascii
end type
type st_32 from statictext within w_importa_files_ascii
end type
type st_33 from statictext within w_importa_files_ascii
end type
type st_34 from statictext within w_importa_files_ascii
end type
type ln_1 from line within w_importa_files_ascii
end type
type st_commit from statictext within w_importa_files_ascii
end type
type sle_da_anno_comm from singlelineedit within w_importa_files_ascii
end type
type cbx_anag_prodotti from checkbox within w_importa_files_ascii
end type
type cbx_anag_clienti from checkbox within w_importa_files_ascii
end type
type cbx_anag_fornitori from checkbox within w_importa_files_ascii
end type
type st_4 from statictext within w_importa_files_ascii
end type
type ln_2 from line within w_importa_files_ascii
end type
type sle_carica_tutti from singlelineedit within w_importa_files_ascii
end type
type cb_carica_tutti from commandbutton within w_importa_files_ascii
end type
type st_29 from statictext within w_importa_files_ascii
end type
type cb_1 from commandbutton within w_importa_files_ascii
end type
type lb_1 from listbox within w_importa_files_ascii
end type
end forward

shared variables
string ls_cod_articolo, ls_des_articolo, ls_misura, ls_cod_iva
end variables

global type w_importa_files_ascii from w_cs_xx_principale
integer width = 2848
integer height = 1744
string title = "Importazione Files Ascii"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_chiudi_log cb_chiudi_log
cbx_cat_com cbx_cat_com
st_5 st_5
cb_cerca_cat_com cb_cerca_cat_com
cb_cat_commerciali cb_cat_commerciali
sle_file_cat_commerciali sle_file_cat_commerciali
sle_file_origine_commesse sle_file_origine_commesse
sle_file_origine_clienti sle_file_origine_clienti
st_2 st_2
sle_file_origine sle_file_origine
cb_carica_clienti cb_carica_clienti
cb_carica_prodotti cb_carica_prodotti
cb_carica_commesse cb_carica_commesse
st_1 st_1
sle_file_origine_fornitori sle_file_origine_fornitori
cb_carica_fornitori cb_carica_fornitori
sle_file_origine_tes_fasi_lavoro sle_file_origine_tes_fasi_lavoro
cb_carica_tes_fasi cb_carica_tes_fasi
sle_file_origine_distinta sle_file_origine_distinta
cb_carica_distinta cb_carica_distinta
sle_file_origine_descriz_1 sle_file_origine_descriz_1
cb_carica_descrizioni_1 cb_carica_descrizioni_1
st_10 st_10
st_stringa st_stringa
cb_cerca_prodotti cb_cerca_prodotti
cb_cerca_clienti cb_cerca_clienti
cb_cerca_fornitori cb_cerca_fornitori
cb_cerca_commesse cb_cerca_commesse
cb_cerca_tes_fasi cb_cerca_tes_fasi
cb_cerca_distinta cb_cerca_distinta
cb_cerca_descrizioni_1 cb_cerca_descrizioni_1
st_3 st_3
st_21 st_21
st_22 st_22
st_23 st_23
st_24 st_24
st_25 st_25
sle_file_origine_det_fasi_lavoro sle_file_origine_det_fasi_lavoro
cb_carica_det_fasi cb_carica_det_fasi
cb_cerca_det_fasi cb_cerca_det_fasi
st_62 st_62
sle_file_origine_descriz_2 sle_file_origine_descriz_2
sle_file_origine_descriz_3 sle_file_origine_descriz_3
sle_file_origine_descriz_4 sle_file_origine_descriz_4
cb_cerca_descrizioni_2 cb_cerca_descrizioni_2
cb_cerca_descrizioni_3 cb_cerca_descrizioni_3
cb_cerca_descrizioni_4 cb_cerca_descrizioni_4
st_32 st_32
st_33 st_33
st_34 st_34
ln_1 ln_1
st_commit st_commit
sle_da_anno_comm sle_da_anno_comm
cbx_anag_prodotti cbx_anag_prodotti
cbx_anag_clienti cbx_anag_clienti
cbx_anag_fornitori cbx_anag_fornitori
st_4 st_4
ln_2 ln_2
sle_carica_tutti sle_carica_tutti
cb_carica_tutti cb_carica_tutti
st_29 st_29
cb_1 cb_1
lb_1 lb_1
end type
global w_importa_files_ascii w_importa_files_ascii

type variables
boolean ib_carica_tutti = FALSE
string is_valuta
end variables

forward prototypes
public function integer wf_ricerca_misura (string ws_cod_misura)
public function integer wf_ricerca_iva (string ws_cod_iva)
public function integer wf_insert_descrizioni (string ws_cod_prodotto, string ws_descrizione, string ws_cod_nota, boolean flag_aggiungi)
public function integer wf_insert_distinta (string cod_prodotto_padre, string cod_prodotto_figlio, integer riga, double quantita, long num_sequenza)
public function integer wf_scrivi_log (string ws_errore)
public function integer w_insert_anag_prodotti (string wl_cod_prodotto, string wl_des_prodotto, string wl_cod_misura, string wl_cod_iva, double wl_ult_prz_acq, string ws_update)
public function integer wf_insert_anag_commesse (string ws_cod_cliente, long wl_anno_commessa, long wl_num_commessa, double wl_quan_comm, datetime wl_data_comm, datetime wl_data_evasione, long wl_anno_ordine, long wl_num_ordine, long wl_riga_ordine, long wl_quan_ord, string ws_cod_prodotto)
public function integer wf_insert_anag_fornitori (string wl_cod_fornitore, string wl_rag_sociale, string wl_cap, string wl_indirizzo, string wl_provincia, string wl_partita_iva, string wl_cod_banca, string wl_cod_pagamento, string wl_citta, string wl_telefono, string wl_cellulare, string wl_fax, string wl_email, string wl_rif_interno, string wl_web)
public function integer wf_inserisci_prodotti (string wl_cod_prodotto, string wl_des_prodotto, string wl_cod_misura, string wl_cod_iva, decimal wl_ult_prz_acq, string wl_cod_fornitore, string wl_cod_deposito, string wl_cod_cat_commerciale, string ws_update)
protected function integer wf_inserisci_cat_commerciali (string wl_cod_categoria, string wl_des_categoria, string ws_update)
public subroutine wf_mostra_log (boolean ab_show)
protected function integer w_insert_anag_clienti (string wl_cod_cliente, string wl_rag_sociale, string wl_rag_sociale_2, string wl_indirizzo, string wl_cap, string wl_citta, string wl_provincia, string wl_partita_iva, string wl_cod_valuta, string wl_cod_banca, string ws_update, string wl_tipo_cliente)
public function integer w_inserisci_fornitore_2 (string wl_cod_fornitore, string wl_rag_sociale, string wl_rag_sociale_2, string wl_cap, string wl_indirizzo, string wl_citta, string wl_provincia, string wl_partita_iva, string wl_cod_banca, string wl_cod_valuta, string wl_tipo_fornitore)
end prototypes

public function integer wf_ricerca_misura (string ws_cod_misura);string ls_cod_misura

if len(ws_cod_misura) = 0 then
   ws_cod_misura = "NR."
end if
SELECT tab_misure.cod_misura
INTO   :ls_cod_misura
FROM   tab_misure
WHERE  (:s_cs_xx.cod_azienda = tab_misure.cod_azienda) and (tab_misure.cod_misura = :ws_cod_misura) ;

if sqlca.sqlcode = 100 then 

   INSERT INTO tab_misure  
           ( cod_azienda,   
           cod_misura,   
           des_misura,   
           fat_conversione,   
           flag_blocco,   
           data_blocco )  
   VALUES ( :s_cs_xx.cod_azienda,   
           :ws_cod_misura,   
           :ws_cod_misura,   
           1,   
           'N',   
           null)    ;
   return 1

else

  return 0

end if

end function

public function integer wf_ricerca_iva (string ws_cod_iva);string ls_codice_iva, ls_des_iva
long   ll_cod_iva

if len(ws_cod_iva) = 0 then
   ws_cod_iva = "19"
end if
select tab_ive.cod_iva
into   :ls_codice_iva
from   tab_ive
where  (:s_cs_xx.cod_azienda = tab_ive.cod_azienda) and (tab_ive.cod_iva = :ws_cod_iva) ;
 
if sqlca.sqlcode  <> 0 then

  ll_cod_iva = long(ws_cod_iva)
  ls_des_iva = "Iva al " + ls_codice_iva + "%"
  INSERT INTO tab_ive  
              (cod_azienda,   
              cod_iva,   
              des_iva,   
              aliquota,   
              flag_calcolo,   
              flag_blocco,   
              data_blocco )  
   VALUES    (:s_cs_xx.cod_azienda,   
              :ls_codice_iva,   
              :ls_des_iva,   
              :ll_cod_iva,   
              'S',   
              'N',   
              null )    ;
   return 1
else
   return 0
end if
end function

public function integer wf_insert_descrizioni (string ws_cod_prodotto, string ws_descrizione, string ws_cod_nota, boolean flag_aggiungi);// -------------------------------------------------------------------------------------------
//	Funzione di inserimento in tabella anag_prodotti_note
//
// nome: wf_insert_descrizioni
// tipo: integer
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//							 ws_codice prodotto      string 			valore
//							 ws_descrizione          string			valore
//		Creata il 02-07-96 
//		Autore EnMe
//
// -------------------------------------------------------------------------------------------

string ls_cod_misura, ls_prodotto, ls_nota

// -------------------------------------------------------------------------------------------


   //  --------------  verifico esistenza prodotto  in magazzino ---------------------
   if len(trim(ws_cod_prodotto)) < 16 then
      SELECT anag_prodotti.cod_prodotto  
      INTO   :ls_prodotto
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_prodotto = :ws_cod_prodotto )   ;
   else
      SELECT anag_prodotti.cod_prodotto
      INTO   :ls_prodotto
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_comodo = :ws_cod_prodotto )   ;
   end if
   choose case sqlca.sqlcode
      case 100   
         g_mb.messagebox("Caricamento Distinta", "Il prodotto non esiste in anagrafica prodotti")
         return -1
      case 0
   
      case else 
         g_mb.messagebox("Caricamento Distinta", sqlca.sqlerrtext)
         return -1
   end choose
 

   //  --------------  verifico esistenza delle descrizioni ------------------------------
     SELECT anag_prodotti_note.nota_prodotto  
     INTO   :ls_nota  
     FROM   anag_prodotti_note  
     WHERE  ( anag_prodotti_note.cod_azienda       = :s_cs_xx.cod_azienda ) AND  
            ( anag_prodotti_note.cod_prodotto      = :ws_cod_prodotto )     AND  
            ( anag_prodotti_note.cod_nota_prodotto = :ws_cod_nota )  ;


   //  --------------- insert oppure update nuove righe descrizione --------------------
   if sqlca.sqlcode = 0 then
      if flag_aggiungi then
         ls_nota = ls_nota + char(13) + char(10) + ws_descrizione
      else
         ls_nota = ws_descrizione
      end if
      UPDATE anag_prodotti_note  
      SET    nota_prodotto = :ws_descrizione  
      WHERE  ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti_note.cod_prodotto = :ws_cod_prodotto )   and
             ( anag_prodotti_note.cod_nota_prodotto = :ws_cod_nota )   ;
   else
      if sqlca.sqlcode = 100 then
        INSERT INTO anag_prodotti_note  
               ( cod_azienda,   
               cod_prodotto,   
               cod_nota_prodotto,   
               nota_prodotto )  
        VALUES ( :s_cs_xx.cod_azienda,   
               :ws_cod_prodotto,   
               :ws_cod_nota,   
               :ws_descrizione )  ;
      end if
   end if

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("INSERT DB Distinta",SQLCA.SQLErrText)
      return -1
   end if

return 0


end function

public function integer wf_insert_distinta (string cod_prodotto_padre, string cod_prodotto_figlio, integer riga, double quantita, long num_sequenza);
// -------------------------------------------------------------------------------------------
//	Funzione di inserimento in tabella distinta 
//
// nome: wf_insert_distinta
// tipo: integer
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//							 codice prodotto padre   string 			valore
//							 cod_prodotto_figlio     string			valore
//							 riga                    integer			valore
//						    quantita                double		   valore
//							 num_sequenza            long          valore
//		Creata il 27-06-96 
//		Autore EnMe
//
// -------------------------------------------------------------------------------------------

string ls_cod_misura, ls_prodotto_padre, ls_prodotto_figlio
integer li_risposta

// -------------------------------------------------------------------------------------------


   //  --------------  verifico esistenza prodotto figlio in magazzino ---------------------
   if len(trim(cod_prodotto_figlio)) < 16 then
      SELECT anag_prodotti.cod_prodotto  
      INTO   :ls_prodotto_figlio
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_prodotto = :cod_prodotto_figlio )   ;
   else
      SELECT anag_prodotti.cod_prodotto
      INTO   :ls_prodotto_figlio
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_comodo = :cod_prodotto_figlio )   ;
   end if
   choose case sqlca.sqlcode
      case 100   
//         g_mb.messagebox("Caricamento Distinta", "Il prodotto figlio non esiste in anagrafica prodotti")
           return 1
      case 0
   
      case else 
         if ib_carica_tutti then
            wf_scrivi_log("Errore durante la verifica esistenza prodotto figlio; dettaglio SQL --> "+SQLCA.SQLErrText)
            return 1
         else
            li_risposta = g_mb.messagebox("Verifica esistenza prodotto figlio",SQLCA.SQLErrText+" PROSEGUO?",question!, YESNO!,1)
            if li_risposta = 1 then return 1
            return -1
         end if
   end choose
 

   //  --------------  verifico esistenza prodotto padre in magazzino ---------------------
   if len(trim(cod_prodotto_padre)) < 16 then
      SELECT anag_prodotti.cod_prodotto  
      INTO   :ls_prodotto_padre
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_prodotto = :cod_prodotto_padre )   ;
   else
      SELECT anag_prodotti.cod_prodotto
      INTO   :ls_prodotto_padre
      FROM   anag_prodotti  
      WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
             ( anag_prodotti.cod_comodo = :cod_prodotto_padre )   ;
   end if
   choose case sqlca.sqlcode
      case 100   
//         g_mb.messagebox("Verifica Esistenza prodotto padre", "Il prodotto padre "+cod_prodotto_padre+" non esiste in anagrafica prodotti")
         return 1
      case 0
   
      case else 
         if ib_carica_tutti then
            wf_scrivi_log("Errore durante la verifica esistenza prodotto padre; dettaglio SQL --> "+SQLCA.SQLErrText)
            return 1
         else
            li_risposta = g_mb.messagebox("Verifica Esistenza prodotto padre",SQLCA.SQLErrText+" PROSEGUO?",question!, YESNO!,1)
            if li_risposta = 1 then return 1
            return -1
         end if
   end choose


   //  --------------  ricerco unità di misura da anagrafica prodotti magazzino  --------------
   SELECT anag_prodotti.cod_misura_mag  
   INTO   :ls_cod_misura  
   FROM   anag_prodotti  
   WHERE  (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
          (anag_prodotti.cod_prodotto = :ls_prodotto_figlio ) ;

   if sqlca.sqlcode <> 0 then
      if sqlca.sqlcode = 100 then
//         g_mb.messagebox("Ricerca Prodotto","Il prodotto "+ls_prodotto_figlio+" non esiste in anagrafica prodotti")
         return 1
      else
         if ib_carica_tutti then
            wf_scrivi_log("Errore durante la verifica esistenza unità di misura; dettaglio SQL --> "+SQLCA.SQLErrText)
            return 1
         else
            li_risposta = g_mb.messagebox("Verifica esistenza codice misura",SQLCA.SQLErrText+" PROSEGUO?",question!, YESNO!,1)
            if li_risposta = 1 then return 1
            return -1
         end if
      end if
   end if
   //  --------------  annullo vecchie righe distinta base ------------------------------

//   DELETE FROM import_distinta  
//   WHERE  ( distinta.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//          ( distinta.cod_prodotto_padre = :ls_prodotto_padre ) AND  
//          ( distinta.cod_prodotto_figlio = :ls_prodotto_figlio )   ;

   //  --------------  insert nuove righe distinta base ---------------------------------
   INSERT INTO import_distinta  
          (cod_azienda,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_misura,   
           quan_tecnica,   
           quan_utilizzo,   
           fase_ciclo )  
   VALUES  (:s_cs_xx.cod_azienda,   
            :ls_prodotto_padre,   
            :num_sequenza,   
            :ls_prodotto_figlio,   
            :ls_cod_misura,   
            :quantita,   
            :quantita,   
            1)  ;

   if sqlca.sqlcode <> 0 then
      if ib_carica_tutti then
         wf_scrivi_log("Errore durante inserimento record distinta; dettaglio SQL --> "+SQLCA.SQLErrText)
         return 1
      else
         li_risposta = g_mb.messagebox("INSERT DB Distinta",SQLCA.SQLErrText+" PROSEGUO?",question!, YESNO!,1)
         if li_risposta = 1 then return 1
         return -1
      end if
   end if

return sqlca.sqlcode


end function

public function integer wf_scrivi_log (string ws_errore);integer li_num_file
string ls_str


//li_num_file = FileOpen(sle_carica_tutti.text, StreamMode!, Write!, LockWrite!, Append!)
//FileWrite(li_num_file, ws_errore+char(13)+char(10) )
//fileclose(li_num_file)

//mle_1.text = mle_1.text + ws_errore + char(13) + char(10)
ls_str = ws_errore
LB_1.AddItem(ls_str)

// stefano 12/04/2010
st_stringa.text = ws_errore
return 1
end function

public function integer w_insert_anag_prodotti (string wl_cod_prodotto, string wl_des_prodotto, string wl_cod_misura, string wl_cod_iva, double wl_ult_prz_acq, string ws_update);//	Funzione di insert articolo nella tabella anag_prodotti
//
// nome: wf_insert_anag_prodotti
// tipo: string 
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//							 codice prodotto    	   string 			valore
//							 descrizione 			   string			valore
//							 unità di misura		   string			valore
//						    codice aliquota iva	   string			valore
//								
//		Creata il 24-04-96 
//		Autore EnMe
//
string ls_prodotto, ls_cod_comodo, ls_str
datetime ldt_oggi



setnull(ls_cod_comodo)
if len( trim(wl_cod_prodotto) ) > 15 then

   SELECT anag_prodotti.cod_comodo
   INTO   :ls_prodotto
   FROM   anag_prodotti
   WHERE  (anag_prodotti.cod_comodo = :wl_cod_prodotto) and
          (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda) ;

   if SQLCA.SQLCode = 0  then
      if ws_update = "S" then
         UPDATE anag_prodotti  
            SET des_prodotto = :wl_des_prodotto,   
                cod_misura_mag = :wl_cod_misura,   
                cod_misura_acq = :wl_cod_misura,   
                prezzo_acquisto = :wl_ult_prz_acq,   
                cod_misura_ven = :wl_cod_misura,   
                cod_iva = :wl_cod_iva  
         WHERE  (anag_prodotti.cod_comodo = :wl_cod_prodotto) and
                (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda) ;
         if SQLCA.SQLCode = -1 then
            ls_str = "Caricamento Prodotti   "
            if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
            ls_str = "Errore UPDATE SQL --> "+SQLCA.SQLErrText
            wf_scrivi_log(ls_str)
//            g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
//            g_mb.messagebox("Errore SQL",wl_cod_prodotto+" - "+ls_cod_comodo)
         end if
      end if
      goto uscita   
   end if

   if SQLCA.SQLCode = -1 then
      ls_str = "Caricamento Prodotti   "
      if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
      ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
      wf_scrivi_log(ls_str)
//      g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
      goto uscita   
   else
      if SQLCA.SQLCode = 100 then
         ls_cod_comodo = wl_cod_prodotto
         wl_cod_prodotto = left(ls_cod_comodo,2) + mid(ls_cod_comodo, 4, 13)
         wf_ricerca_misura(wl_cod_misura)
         wf_ricerca_iva(wl_cod_iva)  
      end if
   end if
else
    SELECT anag_prodotti.cod_prodotto
    INTO   :ls_prodotto
    FROM   anag_prodotti
    WHERE (anag_prodotti.cod_prodotto = :wl_cod_prodotto) and
          (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda) ;
    if sqlca.sqlcode = 0 then    
       if ws_update = "S" then
          UPDATE anag_prodotti  
             SET des_prodotto = :wl_des_prodotto,   
                 cod_misura_mag = :wl_cod_misura,   
                 cod_misura_acq = :wl_cod_misura,   
                 prezzo_acquisto = :wl_ult_prz_acq,   
                 cod_misura_ven = :wl_cod_misura,   
                 cod_iva = :wl_cod_iva  
          WHERE  (anag_prodotti.cod_prodotto = :wl_cod_prodotto) and
                 (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda) ;
          if SQLCA.SQLCode = -1 then
             ls_str = "Caricamento Prodotti   "
             if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
             ls_str = "Errore UPDATE SQL --> "+SQLCA.SQLErrText
             wf_scrivi_log(ls_str)
//              g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
//              g_mb.messagebox("Errore SQL",wl_cod_prodotto+" - "+ls_cod_comodo)
          end if
       end if
   end if


    if (SQLCA.SQLCode = -1) then 
       ls_str = "Caricamento Prodotti"
       if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
       ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
       wf_scrivi_log(ls_str)
//     g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
    end if
    if SQLCA.SQLCode = 0 then goto uscita
    setnull(ls_cod_comodo)
    wf_ricerca_misura(wl_cod_misura)
    wf_ricerca_iva(wl_cod_iva)  
end if

ldt_oggi = datetime(today())

INSERT INTO anag_prodotti  
         ( cod_azienda,   
           cod_prodotto,   
           cod_comodo,   
           cod_barre,   
           des_prodotto,   
           data_creazione,   
           cod_deposito,   
           cod_ubicazione,   
           flag_decimali,   
           cod_cat_mer,   
           cod_responsabile,   
           cod_misura_mag,   
           cod_misura_peso,   
           peso_lordo,   
           peso_netto,   
           cod_misura_vol,   
           volume,   
           pezzi_collo,   
           flag_classe_abc,   
           flag_sotto_scorta,   
           flag_lifo,   
           saldo_quan_anno_prec,   
           saldo_quan_inizio_anno,   
           val_inizio_anno,   
           saldo_quan_ultima_chiusura,   
           prog_quan_entrata,   
           val_quan_entrata,   
           prog_quan_uscita,   
           val_quan_uscita,   
           prog_quan_acquistata,   
           val_quan_acquistata,   
           prog_quan_venduta,   
           val_quan_venduta,   
           quan_ordinata,   
           quan_impegnata,   
           quan_assegnata,   
           quan_in_spedizione,   
           quan_anticipi,   
           costo_standard,   
           costo_ultimo,   
           lotto_economico,   
           livello_riordino,   
           scorta_minima,   
           scorta_massima,   
           indice_rotazione,   
           consumo_medio,   
           cod_prodotto_alt,   
           cod_misura_acq,   
           fat_conversione_acq,   
           cod_fornitore,   
           cod_gruppo_acq,   
           prezzo_acquisto,   
           sconto_acquisto,   
           quan_minima,   
           inc_ordine,   
           quan_massima,   
           tempo_app,   
           cod_misura_ven,   
           fat_conversione_ven,   
           cod_gruppo_ven,   
           cod_iva,   
           prezzo_vendita,   
           sconto,   
           provvigione,   
           flag_blocco,   
           data_blocco,
			  cod_livello_prod_1,
			  cod_livello_prod_2,
			  cod_livello_prod_3,
			  cod_livello_prod_4,
			  cod_livello_prod_5,
			  cod_nomenclatura)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_cod_prodotto,   
           :ls_cod_comodo,   
           0,   
           :wl_des_prodotto,   
           :ldt_oggi,   
           null,   
           null,   
           'S',   
           null,   
           null,   
           :wl_cod_misura,   
           null,   
           0,   
           0,   
           null,   
           0,   
           1,   
           'A',   
           'N',   
           'S',   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           :wl_cod_misura,   
           1,   
           null,   
           null,   
           :wl_ult_prz_acq,   
           0,   
           0,   
           0,   
           0,   
           0,   
           :wl_cod_misura,   
           1,   
           null,   
           :wl_cod_iva,   
           0,   
           0,   
           0,   
           'N',   
           null,
			  null,
			  null,
			  null,
			  null,
			  null,
			  null)  ;
if SQLCA.SQLCode = -1 then
//   g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
//   g_mb.messagebox("Errore SQL",wl_cod_prodotto+" - "+ls_cod_comodo)
     ls_str = "Caricamento Prodotti"
     if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
     ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
     wf_scrivi_log(ls_str)
end if

uscita:
return SQLCA.SQLCode


end function

public function integer wf_insert_anag_commesse (string ws_cod_cliente, long wl_anno_commessa, long wl_num_commessa, double wl_quan_comm, datetime wl_data_comm, datetime wl_data_evasione, long wl_anno_ordine, long wl_num_ordine, long wl_riga_ordine, long wl_quan_ord, string ws_cod_prodotto);//	Funzione di insert articolo nella tabella anag_prodotti
//
// nome: wf_insert_anag_prodotti
// tipo: string 
//  
//								
//		Creata il 26-06-96 
//		Autore EnMe
//
long ll_anno, ll_num, ll_anno_ordine
string ls_des_prodotto, ls_cod_cli, ls_des_cli, ls_cod_prodotto, ls_cod_prod


ll_anno_ordine = wl_anno_commessa
if mid(ws_cod_prodotto,1,1) = "-" then  return 1


SELECT anag_clienti.rag_soc_1  
INTO   :ls_des_cli 
FROM   anag_clienti  
WHERE  (anag_clienti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
       (anag_clienti.cod_cliente = :ws_cod_cliente );
if sqlca.sqlcode  = 100 then
     wf_scrivi_log("Importazione commesse: Cliente non presente in archivio")
//   g_mb.messagebox("Importazione Commesse", "Cliente "+ws_cod_cliente+" non presente in archivio")
   return sqlca.sqlcode
end if

if len(ws_cod_prodotto) > 15 then
   SELECT anag_prodotti.cod_comodo
   INTO :ls_cod_prodotto
   FROM anag_prodotti  
   WHERE ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( anag_prodotti.cod_prodotto = :ws_cod_prodotto )   ;
   if sqlca.sqlcode <> 0 then
      wf_scrivi_log("Prodotto Inesistente salto inserimento commessa "+string(wl_num_commessa))
//      g_mb.messagebox("Prodotto Inesistente ","Salto inserimento commessa "+string(wl_num_commessa),Information!)
      return sqlca.sqlcode
   end if
else
   SELECT anag_prodotti.cod_prodotto
   INTO :ls_cod_prodotto
   FROM anag_prodotti  
   WHERE ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( anag_prodotti.cod_prodotto = :ws_cod_prodotto )   ;
   if sqlca.sqlcode <> 0 then
      wf_scrivi_log("Prodotto Inesistente salto inserimento commessa "+string(wl_num_commessa))
//      g_mb.messagebox("Prodotto Inesistente ","Salto inserimento commessa "+string(wl_num_commessa),Information!)
      return sqlca.sqlcode
   end if
end if


SELECT anag_commesse.anno_commessa, anag_commesse.num_commessa
INTO   :ll_anno, :ll_num
FROM   anag_commesse
WHERE  (anag_commesse.cod_azienda = :s_cs_xx.cod_azienda) and 
		 (anag_commesse.anno_commessa = :wl_anno_commessa) and 
		 (anag_commesse.num_commessa = :wl_num_commessa) ;


choose case sqlca.sqlcode
case 100
   INSERT INTO anag_commesse  
         ( cod_azienda,   
           anno_commessa,   
           num_commessa,   
           cod_tipo_commessa,   
           data_registrazione,   
           cod_operatore,   
           nota_testata,   
           nota_piede,   
           flag_blocco,   
           quan_ordine,   
           quan_prodotta,   
           data_chiusura,   
           data_consegna,
			  cod_prodotto,
			  quan_assegnata,
			  quan_in_produzione,
			  cod_deposito_versamento,
			  cod_deposito_prelievo)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_anno_commessa,   
           :wl_num_commessa,   
           null,   
           :wl_data_comm,   
           null,   
           null,   
           null,   
           'N',   
           :wl_quan_comm,   
			  0,
           null,
           :wl_data_evasione,
			  :ls_cod_prodotto,
			  0,
			  0,
			  null,
			  null);
     if sqlca.sqlcode <> 0 then
        wf_scrivi_log("Errore SQL INSERT COMMESSE; dettaglio errore SQL -->" +SQLCA.SQLErrText)
     end if
     
case 0
     UPDATE anag_commesse  
     SET quan_ordine = :wl_quan_comm,   
         quan_prodotta = 0,   
         data_chiusura = :wl_data_comm,   
         data_consegna = :wl_data_evasione, 
			cod_prodotto = :ls_cod_prodotto,
			quan_assegnata = 0,
			quan_in_produzione = 0,
			cod_deposito_versamento = null,
			cod_deposito_prelievo = null
   WHERE ( anag_commesse.cod_azienda = :s_cs_xx.cod_azienda) AND  
         ( anag_commesse.anno_commessa = :wl_anno_commessa ) AND  
         ( anag_commesse.num_commessa = :wl_num_commessa )   ;
     if sqlca.sqlcode <> 0 then
        wf_scrivi_log("Errore SQL UPDATE COMMESSE; dettaglio errore SQL -->" +SQLCA.SQLErrText)
     end if

case else
   wf_scrivi_log("Errore SQL SELECT COMMESSE; dettaglio errore SQL -->" +SQLCA.SQLErrText)
   return 1
end choose
return SQLCA.SQLCode

end function

public function integer wf_insert_anag_fornitori (string wl_cod_fornitore, string wl_rag_sociale, string wl_cap, string wl_indirizzo, string wl_provincia, string wl_partita_iva, string wl_cod_banca, string wl_cod_pagamento, string wl_citta, string wl_telefono, string wl_cellulare, string wl_fax, string wl_email, string wl_rif_interno, string wl_web);//	Funzione di insert articolo nella tabella anag_prodotti
//
// nome: wf_insert_anag_prodotti
// tipo: string 
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//									cod cliente			string			valore
// 								ragione sociale     "			      "   
//									cap					  "			      "
//									indirizzo			  "   			   "
//									citta               "	   		   "
//									provincia           "		   	   "
//									partita iva         "			      "
// 								pagamento           "			      "
//									banca               "			      "
//									telefono            "			      "
//									cellulare           "			      "
//									fax                 "			      "
//									email               "			      "
//									rif. interno        "			      "
//									indirizzo web       "			      "
//								
//		Creata il 24-04-96 
//		Autore EnMe
//
//    Modificata il 03/10/2002 da MiMa
//
string ls_fornitore, ls_cod_pagamento


if len(wl_cod_pagamento) > 0 then
   SELECT tab_pagamenti.cod_pagamento  
   INTO  :ls_cod_pagamento  
   FROM  tab_pagamenti  
   WHERE ( tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         ( tab_pagamenti.cod_pagamento = :wl_cod_pagamento )   ;
   if sqlca.sqlcode = 100 then
      INSERT INTO tab_pagamenti  
         ( cod_azienda,   
           cod_pagamento,   
           des_pagamento,   
           flag_tipo_pagamento,   
           sconto,   
           flag_bolli,   
           flag_partenza,   
           int_partenza,   
           num_rate_com,   
           cod_doc_prima_rata_cli,   
           cod_doc_prima_rata_for,   
           perc_prima_rata,   
           flag_iva_prima_rata,   
           num_gior_prima_rata,   
           cod_doc_int_rata_cli,   
           cod_doc_int_rata_for,   
           num_gior_int_rata,   
           perc_ult_rata,   
           cod_doc_ult_rata_cli,   
           cod_doc_ult_rata_for,   
           num_gior_ult_rata,   
           flag_blocco,   
           data_blocco,
			  flag_cal_commerciale)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_cod_pagamento,   
           'Pagamento ' + :wl_cod_pagamento,   
           'D',   
           0,   
           'N',   
           'F',   
           0,   
           1,   
           null,   
           null,   
           100,   
           'S',   
           0,   
           null,   
           null,   
           0,   
           0,   
           null,   
           null,   
           0,   
           'N',   
           null,
			  'S')  ;
   end if
else
   setnull(wl_cod_pagamento)
end if

SELECT anag_fornitori.cod_fornitore
INTO   :ls_fornitore
FROM   anag_fornitori
WHERE  (anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and (anag_fornitori.cod_fornitore = :wl_cod_fornitore) ;

if SQLCA.SQLCode = 0 then
   UPDATE anag_fornitori  
      SET rag_soc_1 = :wl_rag_sociale,   
          indirizzo = :wl_indirizzo,   
          localita = :wl_citta,   
          cap = :wl_cap,   
          provincia = :wl_provincia,   
          partita_iva = :wl_partita_iva,
          cod_pagamento =:wl_cod_pagamento,
			 telefono = :wl_telefono,
			 telex = :wl_cellulare,
			 fax = :wl_fax,
			 casella_mail = :wl_email,
			 rif_interno = :wl_rif_interno,
			 sito_internet = :wl_web
   WHERE  (anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (anag_fornitori.cod_fornitore = :wl_cod_fornitore) ;

  if sqlca.sqlcode <> 0 then
     wf_scrivi_log("Errore durante update del fornitore   ERRORE--> "+SQLCA.SQLErrText)
  end if
  return sqlca.sqlcode

end if

if SQLCA.SQLCode = 100 then
   INSERT INTO anag_fornitori  
         ( cod_azienda,   
           cod_fornitore,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_fiscale,   
           partita_iva,   
           rif_interno,   
           flag_tipo_fornitore,   
           cod_conto,   
           cod_iva,   
           num_prot_esenzione_iva,   
           data_esenzione_iva,   
           flag_sospensione_iva,   
           cod_pagamento,   
           giorno_fisso_scadenza,   
           mese_esclusione_1,   
           mese_esclusione_2,   
           data_sostituzione_1,   
           data_sostituzione_2,   
           sconto,   
           cod_tipo_listino_prodotto,   
           fido,   
           cod_banca_clien_for,   
           conto_corrente,   
           cod_lingua,   
           cod_nazione,   
           cod_area,   
           cod_zona,   
           cod_valuta,   
           cod_categoria,   
           cod_imballo,   
           cod_porto,   
           cod_resa,   
           cod_mezzo,   
           cod_vettore,   
           cod_inoltro,   
           cod_deposito,   
           flag_certificato,   
           flag_approvato,   
           flag_strategico,   
           flag_terzista,   
           num_contratto,   
           data_contratto,   
           flag_ver_ispettiva,   
           data_ver_ispettiva,   
           note_ver_ispettiva,   
           flag_omologato,   
           flag_procedure_speciali,   
           peso_val_servizio,   
           peso_val_qualita,   
           limite_tolleranza,   
           flag_blocco,   
           data_blocco,
           flag_tipo_certificazione,
           cod_piano_campionamento,
			  casella_mail,
			  nome_doc_qualificazione,
			  sito_internet)
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_cod_fornitore,   
           :wl_rag_sociale,   
           null,   
           :wl_indirizzo,   
           :wl_citta,   
           null,   
           :wl_cap,   
           :wl_provincia,   
           :wl_telefono,   
           :wl_fax,   
           :wl_cellulare,   
           null,   
           :wl_partita_iva,   
           :wl_rif_interno,   
           'S',   
           null,   
           null,   
           null,   
           null,   
           'N',   
           :wl_cod_pagamento,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :is_valuta,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           '001',   
           'N',   
           'N',   
           'C',   
           'N',   
           null,   
           null,   
           'N',   
           null,   
           null,   
           'N',   
           'N',   
           0,   
           0,   
           0,   
           'N',   
           null,
           'N',
           null,
			  :wl_email,
			  null,
			  :wl_web);
   if sqlca.sqlcode <> 0 then
      wf_scrivi_log("Errore durante inserimento del fornitore   ERRORE--> "+SQLCA.SQLErrText)
   end if
else
   if sqlca.sqlcode <> 0 then wf_scrivi_log("Errore durante la creazione del fornitore   ERRORE--> "+SQLCA.SQLErrText)
//   g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
end if
return SQLCA.SQLCode

end function

public function integer wf_inserisci_prodotti (string wl_cod_prodotto, string wl_des_prodotto, string wl_cod_misura, string wl_cod_iva, decimal wl_ult_prz_acq, string wl_cod_fornitore, string wl_cod_deposito, string wl_cod_cat_commerciale, string ws_update);string ls_prodotto, ls_cod_comodo, ls_str
datetime ldt_oggi

setnull(ls_cod_comodo)
if len(trim(wl_cod_prodotto) ) > 15 then

	SELECT anag_prodotti.cod_comodo
	INTO   :ls_prodotto
	FROM   anag_prodotti
	WHERE 
		anag_prodotti.cod_comodo = :wl_cod_prodotto and
		anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda;

	if SQLCA.SQLCode = 0  then
		if ws_update = "S" then
			UPDATE anag_prodotti  
			SET 
				des_prodotto = :wl_des_prodotto,   
				cod_misura_mag = :wl_cod_misura,   
				cod_misura_acq = :wl_cod_misura,   
				prezzo_acquisto = :wl_ult_prz_acq,   
				cod_misura_ven = :wl_cod_misura,   
				cod_iva = :wl_cod_iva,
				cod_fornitore = :wl_cod_fornitore,
				cod_deposito = :wl_cod_deposito,
				cod_cat_mer = :wl_cod_cat_commerciale
			WHERE  
				anag_prodotti.cod_comodo = :wl_cod_prodotto and
				anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda;
				
			if SQLCA.SQLCode = -1 then
				ls_str = "Caricamento Prodotti   "
				if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
					ls_str = "Errore UPDATE SQL --> "+SQLCA.SQLErrText
					wf_scrivi_log(ls_str)
				end if
			end if
			
		goto uscita   
   end if

   if SQLCA.SQLCode = -1 then
      ls_str = "Caricamento Prodotti   "
      if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto: "+wl_cod_prodotto
      ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
      wf_scrivi_log(ls_str)
      goto uscita   
   else
      if SQLCA.SQLCode = 100 then
         ls_cod_comodo = wl_cod_prodotto
         wl_cod_prodotto = left(ls_cod_comodo,2) + mid(ls_cod_comodo, 4, 13)
         wf_ricerca_misura(wl_cod_misura)
         wf_ricerca_iva(wl_cod_iva)  
      end if
   end if
else
	
    SELECT anag_prodotti.cod_prodotto
    INTO   :ls_prodotto
    FROM   anag_prodotti
    WHERE
	 	anag_prodotti.cod_prodotto = :wl_cod_prodotto and
      	anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda;
			
    if sqlca.sqlcode = 0 then
       if ws_update = "S" then
		UPDATE anag_prodotti  
		SET
			des_prodotto = :wl_des_prodotto,   
			cod_misura_mag = :wl_cod_misura,   
			cod_misura_acq = :wl_cod_misura,   
			prezzo_acquisto = :wl_ult_prz_acq,   
			cod_misura_ven = :wl_cod_misura, 
			cod_iva = :wl_cod_iva,
			cod_fornitore = :wl_cod_fornitore,
			cod_deposito = :wl_cod_deposito,
			cod_cat_mer = :wl_cod_cat_commerciale
          WHERE 
			 anag_prodotti.cod_prodotto = :wl_cod_prodotto and
          	anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda;
				 
          if SQLCA.SQLCode = -1 then
             ls_str = "Caricamento Prodotti   "
             if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
             ls_str = "Errore UPDATE SQL --> "+SQLCA.SQLErrText
             wf_scrivi_log(ls_str)
          end if
       end if
   end if


if (SQLCA.SQLCode = -1) then 
	ls_str = "Caricamento Prodotti"
	if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
	ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
	wf_scrivi_log(ls_str)
	//     g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
end if

if SQLCA.SQLCode = 0 then goto uscita
	setnull(ls_cod_comodo)
	wf_ricerca_misura(wl_cod_misura)
	wf_ricerca_iva(wl_cod_iva)  
end if

ldt_oggi = datetime(today())

INSERT INTO anag_prodotti  
		( cod_azienda,   
		cod_prodotto,   
		cod_comodo,   
		cod_barre,   
		des_prodotto,   
		data_creazione,   
		cod_deposito,   
		cod_ubicazione,   
		flag_decimali,   
		cod_cat_mer,   
		cod_responsabile,   
		cod_misura_mag,   
		cod_misura_peso,   
		peso_lordo,   
		peso_netto,   
		cod_misura_vol,   
		volume,   
		pezzi_collo,   
		flag_classe_abc,   
		flag_sotto_scorta,   
		flag_lifo,   
		saldo_quan_anno_prec,   
		saldo_quan_inizio_anno,   
		val_inizio_anno,   
		saldo_quan_ultima_chiusura,   
		prog_quan_entrata,   
		val_quan_entrata,   
		prog_quan_uscita,   
		val_quan_uscita,   
		prog_quan_acquistata,   
		val_quan_acquistata,   
		prog_quan_venduta,   
		val_quan_venduta,   
		quan_ordinata,   
		quan_impegnata,   
		quan_assegnata,   
		quan_in_spedizione,   
		quan_anticipi,   
		costo_standard,   
		costo_ultimo,   
		lotto_economico,   
		livello_riordino,   
		scorta_minima,   
		scorta_massima,   
		indice_rotazione,   
		consumo_medio,   
		cod_prodotto_alt,   
		cod_misura_acq,   
		fat_conversione_acq,   
		cod_fornitore,   
		cod_gruppo_acq,   
		prezzo_acquisto,   
		sconto_acquisto,   
		quan_minima,   
		inc_ordine,   
		quan_massima,   
		tempo_app,   
		cod_misura_ven,   
		fat_conversione_ven,   
		cod_gruppo_ven,   
		cod_iva,   
		prezzo_vendita,   
		sconto,   
		provvigione,   
		flag_blocco,   
		data_blocco,
		cod_livello_prod_1,
		cod_livello_prod_2,
		cod_livello_prod_3,
		cod_livello_prod_4,
		cod_livello_prod_5,
		cod_nomenclatura)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_cod_prodotto,   
           :ls_cod_comodo,   
           0,   
           :wl_des_prodotto,   
           :ldt_oggi,   
           :wl_cod_deposito,   
           null,   
           'S',   
           :wl_cod_cat_commerciale,   
           null,   
           :wl_cod_misura,   
           null,   
           0,   
           0,   
           null,   
           0,   
           1,   
           'A',   
           'N',   
           'S',   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           :wl_cod_misura,   
           1,   
           :wl_cod_fornitore,   
           null,   
           :wl_ult_prz_acq,   
           0,   
           0,   
           0,   
           0,   
           0,   
           :wl_cod_misura,   
           1,   
           null,   
           :wl_cod_iva,   
           0,   
           0,   
           0,   
           'N',   
           null,
			  null,
			  null,
			  null,
			  null,
			  null,

			  null)  ;
if SQLCA.SQLCode = -1 then
     ls_str = "Caricamento Prodotti"
     if not isnull(wl_cod_prodotto) then ls_str = ls_str + "Prodotto:"+wl_cod_prodotto
     ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
     wf_scrivi_log(ls_str)
end if

uscita:
return SQLCA.SQLCode


end function

protected function integer wf_inserisci_cat_commerciali (string wl_cod_categoria, string wl_des_categoria, string ws_update);string ls_test, ls_cod_comodo, ls_str
datetime ldt_oggi

	
SELECT cod_azienda
INTO :ls_test
FROM tab_cat_mer
WHERE
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cat_mer = :wl_cod_categoria;

if sqlca.sqlcode = 0 then
	if ws_update = "S" then
		UPDATE tab_cat_mer  
		SET
			des_cat_mer = :wl_des_categoria
		WHERE 
			cod_cat_mer = :wl_cod_categoria and
			cod_azienda = :s_cs_xx.cod_azienda;
		
		if SQLCA.SQLCode = -1 then
			ls_str = "Caricamento Prodotti   "
			
			if not isnull(wl_cod_categoria) then ls_str = ls_str + "Codice: "+wl_cod_categoria
			ls_str = "Errore UPDATE SQL --> "+SQLCA.SQLErrText
			wf_scrivi_log(ls_str)
		end if
	end if
	
elseif sqlca.sqlcode = -1 then 
	ls_str = "Caricamento Prodotti"
	if not isnull(wl_cod_categoria) then ls_str = ls_str + "Prodotto:"+wl_cod_categoria
	ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
	wf_scrivi_log(ls_str)
end if


INSERT INTO tab_cat_mer  
	(cod_azienda,
	cod_cat_mer,
	des_cat_mer,
	flag_blocco)  
VALUES (
	:s_cs_xx.cod_azienda,
	:wl_cod_categoria,
	:wl_des_categoria,
	'N'
);

if sqlca.sqlcode = -1 then
     ls_str = "Caricamento Prodotti"
     if not isnull(wl_cod_categoria) then ls_str = ls_str + "Codice: "+wl_cod_categoria
     ls_str = "Errore SELECT SQL --> "+SQLCA.SQLErrText
     wf_scrivi_log(ls_str)
end if

return sqlca.sqlcode


end function

public subroutine wf_mostra_log (boolean ab_show);cb_chiudi_log.visible = ab_show
lb_1.visible = ab_show
end subroutine

protected function integer w_insert_anag_clienti (string wl_cod_cliente, string wl_rag_sociale, string wl_rag_sociale_2, string wl_indirizzo, string wl_cap, string wl_citta, string wl_provincia, string wl_partita_iva, string wl_cod_valuta, string wl_cod_banca, string ws_update, string wl_tipo_cliente);//	Funzione di insert articolo nella tabella anag_prodotti
//
// nome: wf_insert_anag_prodotti
// tipo: string 
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//									cod cliente			string			valore
// 								ragione sociale     "			      "   
//									cap					  "			      "
//									indirizzo			  "   			   "
//									citta               "	   		   "
//									provincia           "		   	   "
//									partita iva         "			      "
// 								pagamento           "			      "
//									banca               "			      "
//								
//		Creata il 24-04-96 
//		Autore EnMe
//
string ls_cliente, ls_cod_pagamento, ls_cod_valuta

//if len(wl_cod_pagamento) > 0 then
////   SELECT tab_pagamenti.cod_pagamento  
////   INTO  :ls_cod_pagamento  
////   FROM  tab_pagamenti  
////   WHERE ( tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
////         ( tab_pagamenti.cod_pagamento = :wl_cod_pagamento )   ;
////   if sqlca.sqlcode = 100 then
////      INSERT INTO tab_pagamenti  
////         ( cod_azienda,   
////           cod_pagamento,   
////           des_pagamento,   
////           flag_tipo_pagamento,   
////           sconto,   
////           flag_bolli,   
////           flag_partenza,   
////           int_partenza,   
////           num_rate_com,   
////           cod_doc_prima_rata_cli,   
////           cod_doc_prima_rata_for,   
////           perc_prima_rata,   
////           flag_iva_prima_rata,   
////           num_gior_prima_rata,   
////           cod_doc_int_rata_cli,   
////           cod_doc_int_rata_for,   
////           num_gior_int_rata,   
////           perc_ult_rata,   
////           cod_doc_ult_rata_cli,   
////           cod_doc_ult_rata_for,   
////           num_gior_ult_rata,   
////           flag_blocco,   
////           data_blocco,
////			  flag_cal_commerciale)  
////  VALUES ( :s_cs_xx.cod_azienda,   
////           :wl_cod_pagamento,   
////           'NUOVO RECORD',   
////           'D',   
////           0,   
////           'N',   
////           'F',   
////           0,   
////           1,   
////           null,   
////           null,   
////           100,   
////           'S',   
////           0,   
////           null,   
////           null,   
////           0,   
////           0,   
////           null,   
////           null,   
////           0,   
////           'N',   
////           null,
////			  'S')  ;
////     if sqlca.sqlcode <> 0 then
////        wf_scrivi_log("Errore durante inserimento pagamento cliente   ERRORE-->"+SQLCA.SQLErrText)
////     end if
//
//	SELECT cod_valuta
//   INTO  :ls_cod_valuta  
//   FROM  tab_valute  
//   WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
//         ( cod_valuta = :wl_cod_pagamento )   ;
//   if sqlca.sqlcode = 100 then
//      INSERT INTO tab_pagamenti  
//         ( cod_azienda,   
//           cod_pagamento,   
//           des_pagamento,   
//           flag_tipo_pagamento,   
//           sconto,   
//           flag_bolli,   
//           flag_partenza,   
//           int_partenza,   
//           num_rate_com,   
//           cod_doc_prima_rata_cli,   
//           cod_doc_prima_rata_for,   
//           perc_prima_rata,   
//           flag_iva_prima_rata,   
//           num_gior_prima_rata,   
//           cod_doc_int_rata_cli,   
//           cod_doc_int_rata_for,   
//           num_gior_int_rata,   
//           perc_ult_rata,   
//           cod_doc_ult_rata_cli,   
//           cod_doc_ult_rata_for,   
//           num_gior_ult_rata,   
//           flag_blocco,   
//           data_blocco,
//			  flag_cal_commerciale)  
//  VALUES ( :s_cs_xx.cod_azienda,   
//           :wl_cod_pagamento,   
//           'NUOVO RECORD',   
//           'D',   
//           0,   
//           'N',   
//           'F',   
//           0,   
//           1,   
//           null,   
//           null,   
//           100,   
//           'S',   
//           0,   
//           null,   
//           null,   
//           0,   
//           0,   
//           null,   
//           null,   
//           0,   
//           'N',   
//           null,
//			  'S')  ;
//     if sqlca.sqlcode <> 0 then
//        wf_scrivi_log("Errore durante inserimento pagamento cliente   ERRORE-->"+SQLCA.SQLErrText)
//     end if
//  else
//     if sqlca.sqlcode <> 0 then wf_scrivi_log("Errore durante select pagamento cliente   ERRORE-->"+SQLCA.SQLErrText)
//  end if
//else
//  setnull(wl_cod_pagamento)
//end if


SELECT anag_clienti.cod_cliente
INTO :ls_cliente
FROM anag_clienti
WHERE (anag_clienti.cod_azienda = :s_cs_xx.cod_azienda) and (anag_clienti.cod_cliente = :wl_cod_cliente) ;

if SQLCA.SQLCode = 0 then
	if ws_update = "N" then
	  return 1
	else
		UPDATE anag_clienti  
		SET
			rag_soc_1 = :wl_rag_sociale,
			rag_soc_2 = :wl_rag_sociale_2,
			indirizzo = :wl_indirizzo,   
			localita = :wl_citta,   
			cap = :wl_cap,   
			provincia = :wl_provincia,   
			partita_iva = :wl_partita_iva,
			cod_valuta = :wl_cod_valuta,
			flag_tipo_cliente = :wl_tipo_cliente
		WHERE
			anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
			anag_clienti.cod_cliente = :wl_cod_cliente;
			
		if SQLCA.SQLCode = -1 then
			wf_scrivi_log("Errore durante l'aggiornamento del cliente " + wl_cod_cliente + "  ERRORE-->"+SQLCA.SQLErrText)
		end if
     end if
else
	INSERT INTO anag_clienti  
		(cod_azienda,   
		cod_cliente,   
		rag_soc_1,   
		rag_soc_2,   
		indirizzo,   
		localita,   
		frazione,   
		cap,   
		provincia,   
		telefono,   
		fax,   
		telex,   
		cod_fiscale,   
		partita_iva,   
		rif_interno,   
		flag_tipo_cliente,   
		cod_capoconto,   
		cod_conto,   
		cod_iva,   
		num_prot_esenzione_iva,   
		data_esenzione_iva,   
		flag_sospensione_iva,   
		cod_pagamento,   
		giorno_fisso_scadenza,   
		mese_esclusione_1,   
		mese_esclusione_2,   
		data_sostituzione_1,   
		data_sostituzione_2,   
		sconto,   
		cod_tipo_listino_prodotto,   
		fido,   
		flag_fuori_fido,
		cod_banca_clien_for,   
		conto_corrente,   
		cod_lingua,   
		cod_nazione,   
		cod_area,   
		cod_zona,   
		cod_valuta,   
		cod_categoria,   
		cod_agente_1,   
		cod_agente_2,   
		cod_imballo,   
		cod_porto,   
		cod_resa,   
		cod_mezzo,   
		cod_vettore,   
		cod_inoltro,   
		cod_deposito,   
		flag_riep_boll,   
		flag_riep_fatt,   
		flag_blocco,   
		data_blocco,
		casella_mail)  
	VALUES (:s_cs_xx.cod_azienda,   
		:wl_cod_cliente,   
		:wl_rag_sociale,   
		:wl_rag_sociale_2,   
		:wl_indirizzo,   
		:wl_citta,   
		null,   
		:wl_cap,   
		:wl_provincia,   
		null,   
		null,   
		null,   
		null,   
		:wl_partita_iva,  
		null,   
		'S',   
		null,   
		null,   
		null,   
		null,   
		null,   
		'N',   
		null,   
		0,   
		0,   
		0,   
		0,   
		0,   
		0,   
		null,   
		0,   
		'N',   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		:wl_cod_valuta, //:is_valuta,   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		null,   
		'001',
		'N',   
		'N',   
		'N',   
		null,
		null); 
end if

if SQLCA.SQLCode = -1 then
   wf_scrivi_log("Errore durante l'inserimento cliente   ERRORE-->"+SQLCA.SQLErrText)
end if

return SQLCA.SQLCode


end function

public function integer w_inserisci_fornitore_2 (string wl_cod_fornitore, string wl_rag_sociale, string wl_rag_sociale_2, string wl_cap, string wl_indirizzo, string wl_citta, string wl_provincia, string wl_partita_iva, string wl_cod_banca, string wl_cod_valuta, string wl_tipo_fornitore);//	Funzione di insert articolo nella tabella anag_prodotti
//
// nome: wf_insert_anag_prodotti
// tipo: string 
//  
//	Variabili passate: 		nome					 tipo	   		passaggio per
//							
//									cod cliente			string			valore
// 								ragione sociale     "			      "   
//									cap					  "			      "
//									indirizzo			  "   			   "
//									citta               "	   		   "
//									provincia           "		   	   "
//									partita iva         "			      "
// 								pagamento           "			      "
//									banca               "			      "
//									telefono            "			      "
//									cellulare           "			      "
//									fax                 "			      "
//									email               "			      "
//									rif. interno        "			      "
//									indirizzo web       "			      "
//								
//		Creata il 24-04-96 
//		Autore EnMe
//
//    Modificata il 03/10/2002 da MiMa
//
string ls_fornitore, ls_cod_pagamento


//if len(wl_cod_pagamento) > 0 then
//   SELECT tab_pagamenti.cod_pagamento  
//   INTO  :ls_cod_pagamento  
//   FROM  tab_pagamenti  
//   WHERE ( tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
//         ( tab_pagamenti.cod_pagamento = :wl_cod_pagamento )   ;
//   if sqlca.sqlcode = 100 then
//      INSERT INTO tab_pagamenti  
//         ( cod_azienda,   
//           cod_pagamento,   
//           des_pagamento,   
//           flag_tipo_pagamento,   
//           sconto,   
//           flag_bolli,   
//           flag_partenza,   
//           int_partenza,   
//           num_rate_com,   
//           cod_doc_prima_rata_cli,   
//           cod_doc_prima_rata_for,   
//           perc_prima_rata,   
//           flag_iva_prima_rata,   
//           num_gior_prima_rata,   
//           cod_doc_int_rata_cli,   
//           cod_doc_int_rata_for,   
//           num_gior_int_rata,   
//           perc_ult_rata,   
//           cod_doc_ult_rata_cli,   
//           cod_doc_ult_rata_for,   
//           num_gior_ult_rata,   
//           flag_blocco,   
//           data_blocco,
//			  flag_cal_commerciale)  
//  VALUES ( :s_cs_xx.cod_azienda,   
//           :wl_cod_pagamento,   
//           'Pagamento ' + :wl_cod_pagamento,   
//           'D',   
//           0,   
//           'N',   
//           'F',   
//           0,   
//           1,   
//           null,   
//           null,   
//           100,   
//           'S',   
//           0,   
//           null,   
//           null,   
//           0,   
//           0,   
//           null,   
//           null,   
//           0,   
//           'N',   
//           null,
//			  'S')  ;
//   end if
//else
//   setnull(wl_cod_pagamento)
//end if

SELECT anag_fornitori.cod_fornitore
INTO   :ls_fornitore
FROM   anag_fornitori
WHERE  (anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and (anag_fornitori.cod_fornitore = :wl_cod_fornitore) ;

if SQLCA.SQLCode = 0 then
   UPDATE anag_fornitori  
      SET rag_soc_1 = :wl_rag_sociale,   
			rag_soc_2 = :wl_rag_sociale_2,
          indirizzo = :wl_indirizzo,   
          localita = :wl_citta,   
          cap = :wl_cap,   
          provincia = :wl_provincia,   
          partita_iva = :wl_partita_iva,
          cod_valuta =:wl_cod_valuta,
		flag_tipo_fornitore = :wl_tipo_fornitore
   WHERE  (anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda) and
          (anag_fornitori.cod_fornitore = :wl_cod_fornitore) ;

  if sqlca.sqlcode <> 0 then
     wf_scrivi_log("Errore durante update del fornitore   ERRORE--> "+SQLCA.SQLErrText)
  end if
  return sqlca.sqlcode

end if

if SQLCA.SQLCode = 100 then
   INSERT INTO anag_fornitori  
         ( cod_azienda,   
           cod_fornitore,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           telefono,   
           fax,   
           telex,   
           cod_fiscale,   
           partita_iva,   
           rif_interno,   
           flag_tipo_fornitore,   
           cod_conto,   
           cod_iva,   
           num_prot_esenzione_iva,   
           data_esenzione_iva,   
           flag_sospensione_iva,   
           cod_pagamento,   
           giorno_fisso_scadenza,   
           mese_esclusione_1,   
           mese_esclusione_2,   
           data_sostituzione_1,   
           data_sostituzione_2,   
           sconto,   
           cod_tipo_listino_prodotto,   
           fido,   
           cod_banca_clien_for,   
           conto_corrente,   
           cod_lingua,   
           cod_nazione,   
           cod_area,   
           cod_zona,   
           cod_valuta,   
           cod_categoria,   
           cod_imballo,   
           cod_porto,   
           cod_resa,   
           cod_mezzo,   
           cod_vettore,   
           cod_inoltro,   
           cod_deposito,   
           flag_certificato,   
           flag_approvato,   
           flag_strategico,   
           flag_terzista,   
           num_contratto,   
           data_contratto,   
           flag_ver_ispettiva,   
           data_ver_ispettiva,   
           note_ver_ispettiva,   
           flag_omologato,   
           flag_procedure_speciali,   
           peso_val_servizio,   
           peso_val_qualita,   
           limite_tolleranza,   
           flag_blocco,   
           data_blocco,
           flag_tipo_certificazione,
           cod_piano_campionamento,
			  casella_mail,
			  nome_doc_qualificazione,
			  sito_internet)
  VALUES ( :s_cs_xx.cod_azienda,   
           :wl_cod_fornitore,   
           :wl_rag_sociale,   
           :wl_rag_sociale_2,   
           :wl_indirizzo,   
           :wl_citta,   
           null,   
           :wl_cap,   
           :wl_provincia,   
           '',   
           '',   
           '',   
           null,   
           :wl_partita_iva,   
           '',   
           'S',   
           null,   
           null,   
           null,   
           null,   
           'N',   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           0,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :wl_cod_valuta,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           '001',   
           'N',   
           'N',   
           'C',   
           'N',   
           null,   
           null,   
           'N',   
           null,   
           null,   
           'N',   
           'N',   
           0,   
           0,   
           0,   
           'N',   
           null,
           'N',
           null,
			'',
			  null,
			  '');
   if sqlca.sqlcode <> 0 then
      wf_scrivi_log("Errore durante inserimento del fornitore   ERRORE--> "+SQLCA.SQLErrText)
   end if
else
   if sqlca.sqlcode <> 0 then wf_scrivi_log("Errore durante la creazione del fornitore   ERRORE--> "+SQLCA.SQLErrText)
//   g_mb.messagebox("Errore SQL",SQLCA.SQLErrText,Information!)
end if
return SQLCA.SQLCode

end function

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;string pathfile

setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA1' )   ;
sle_file_origine.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA2' )   ;
sle_file_origine_clienti.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA3' )   ;
sle_file_origine_fornitori.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA4' )   ;
sle_file_origine_commesse.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA5' )   ;
sle_file_origine_tes_fasi_lavoro.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA6' )   ;
sle_file_origine_distinta.text = pathfile


setnull(pathfile)
SELECT parametri.stringa  
INTO :pathfile
FROM parametri  
WHERE ( parametri.flag_parametro = 'S' ) AND  
      ( parametri.cod_parametro = 'FA7' )   ;
sle_carica_tutti.text = pathfile

SELECT parametri_azienda.stringa  
INTO :is_valuta
FROM parametri_azienda  
WHERE ( parametri_azienda.flag_parametro = 'S' ) AND  
      ( parametri_azienda.cod_parametro = 'LIR' ) and
      ( parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda )   ;
if isnull(is_valuta) then
   g_mb.messagebox("Importazione Files ASCII","Impostare parametro LIR in parametri azienda", information!)
end if

end on

on w_importa_files_ascii.create
int iCurrent
call super::create
this.cb_chiudi_log=create cb_chiudi_log
this.cbx_cat_com=create cbx_cat_com
this.st_5=create st_5
this.cb_cerca_cat_com=create cb_cerca_cat_com
this.cb_cat_commerciali=create cb_cat_commerciali
this.sle_file_cat_commerciali=create sle_file_cat_commerciali
this.sle_file_origine_commesse=create sle_file_origine_commesse
this.sle_file_origine_clienti=create sle_file_origine_clienti
this.st_2=create st_2
this.sle_file_origine=create sle_file_origine
this.cb_carica_clienti=create cb_carica_clienti
this.cb_carica_prodotti=create cb_carica_prodotti
this.cb_carica_commesse=create cb_carica_commesse
this.st_1=create st_1
this.sle_file_origine_fornitori=create sle_file_origine_fornitori
this.cb_carica_fornitori=create cb_carica_fornitori
this.sle_file_origine_tes_fasi_lavoro=create sle_file_origine_tes_fasi_lavoro
this.cb_carica_tes_fasi=create cb_carica_tes_fasi
this.sle_file_origine_distinta=create sle_file_origine_distinta
this.cb_carica_distinta=create cb_carica_distinta
this.sle_file_origine_descriz_1=create sle_file_origine_descriz_1
this.cb_carica_descrizioni_1=create cb_carica_descrizioni_1
this.st_10=create st_10
this.st_stringa=create st_stringa
this.cb_cerca_prodotti=create cb_cerca_prodotti
this.cb_cerca_clienti=create cb_cerca_clienti
this.cb_cerca_fornitori=create cb_cerca_fornitori
this.cb_cerca_commesse=create cb_cerca_commesse
this.cb_cerca_tes_fasi=create cb_cerca_tes_fasi
this.cb_cerca_distinta=create cb_cerca_distinta
this.cb_cerca_descrizioni_1=create cb_cerca_descrizioni_1
this.st_3=create st_3
this.st_21=create st_21
this.st_22=create st_22
this.st_23=create st_23
this.st_24=create st_24
this.st_25=create st_25
this.sle_file_origine_det_fasi_lavoro=create sle_file_origine_det_fasi_lavoro
this.cb_carica_det_fasi=create cb_carica_det_fasi
this.cb_cerca_det_fasi=create cb_cerca_det_fasi
this.st_62=create st_62
this.sle_file_origine_descriz_2=create sle_file_origine_descriz_2
this.sle_file_origine_descriz_3=create sle_file_origine_descriz_3
this.sle_file_origine_descriz_4=create sle_file_origine_descriz_4
this.cb_cerca_descrizioni_2=create cb_cerca_descrizioni_2
this.cb_cerca_descrizioni_3=create cb_cerca_descrizioni_3
this.cb_cerca_descrizioni_4=create cb_cerca_descrizioni_4
this.st_32=create st_32
this.st_33=create st_33
this.st_34=create st_34
this.ln_1=create ln_1
this.st_commit=create st_commit
this.sle_da_anno_comm=create sle_da_anno_comm
this.cbx_anag_prodotti=create cbx_anag_prodotti
this.cbx_anag_clienti=create cbx_anag_clienti
this.cbx_anag_fornitori=create cbx_anag_fornitori
this.st_4=create st_4
this.ln_2=create ln_2
this.sle_carica_tutti=create sle_carica_tutti
this.cb_carica_tutti=create cb_carica_tutti
this.st_29=create st_29
this.cb_1=create cb_1
this.lb_1=create lb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_chiudi_log
this.Control[iCurrent+2]=this.cbx_cat_com
this.Control[iCurrent+3]=this.st_5
this.Control[iCurrent+4]=this.cb_cerca_cat_com
this.Control[iCurrent+5]=this.cb_cat_commerciali
this.Control[iCurrent+6]=this.sle_file_cat_commerciali
this.Control[iCurrent+7]=this.sle_file_origine_commesse
this.Control[iCurrent+8]=this.sle_file_origine_clienti
this.Control[iCurrent+9]=this.st_2
this.Control[iCurrent+10]=this.sle_file_origine
this.Control[iCurrent+11]=this.cb_carica_clienti
this.Control[iCurrent+12]=this.cb_carica_prodotti
this.Control[iCurrent+13]=this.cb_carica_commesse
this.Control[iCurrent+14]=this.st_1
this.Control[iCurrent+15]=this.sle_file_origine_fornitori
this.Control[iCurrent+16]=this.cb_carica_fornitori
this.Control[iCurrent+17]=this.sle_file_origine_tes_fasi_lavoro
this.Control[iCurrent+18]=this.cb_carica_tes_fasi
this.Control[iCurrent+19]=this.sle_file_origine_distinta
this.Control[iCurrent+20]=this.cb_carica_distinta
this.Control[iCurrent+21]=this.sle_file_origine_descriz_1
this.Control[iCurrent+22]=this.cb_carica_descrizioni_1
this.Control[iCurrent+23]=this.st_10
this.Control[iCurrent+24]=this.st_stringa
this.Control[iCurrent+25]=this.cb_cerca_prodotti
this.Control[iCurrent+26]=this.cb_cerca_clienti
this.Control[iCurrent+27]=this.cb_cerca_fornitori
this.Control[iCurrent+28]=this.cb_cerca_commesse
this.Control[iCurrent+29]=this.cb_cerca_tes_fasi
this.Control[iCurrent+30]=this.cb_cerca_distinta
this.Control[iCurrent+31]=this.cb_cerca_descrizioni_1
this.Control[iCurrent+32]=this.st_3
this.Control[iCurrent+33]=this.st_21
this.Control[iCurrent+34]=this.st_22
this.Control[iCurrent+35]=this.st_23
this.Control[iCurrent+36]=this.st_24
this.Control[iCurrent+37]=this.st_25
this.Control[iCurrent+38]=this.sle_file_origine_det_fasi_lavoro
this.Control[iCurrent+39]=this.cb_carica_det_fasi
this.Control[iCurrent+40]=this.cb_cerca_det_fasi
this.Control[iCurrent+41]=this.st_62
this.Control[iCurrent+42]=this.sle_file_origine_descriz_2
this.Control[iCurrent+43]=this.sle_file_origine_descriz_3
this.Control[iCurrent+44]=this.sle_file_origine_descriz_4
this.Control[iCurrent+45]=this.cb_cerca_descrizioni_2
this.Control[iCurrent+46]=this.cb_cerca_descrizioni_3
this.Control[iCurrent+47]=this.cb_cerca_descrizioni_4
this.Control[iCurrent+48]=this.st_32
this.Control[iCurrent+49]=this.st_33
this.Control[iCurrent+50]=this.st_34
this.Control[iCurrent+51]=this.ln_1
this.Control[iCurrent+52]=this.st_commit
this.Control[iCurrent+53]=this.sle_da_anno_comm
this.Control[iCurrent+54]=this.cbx_anag_prodotti
this.Control[iCurrent+55]=this.cbx_anag_clienti
this.Control[iCurrent+56]=this.cbx_anag_fornitori
this.Control[iCurrent+57]=this.st_4
this.Control[iCurrent+58]=this.ln_2
this.Control[iCurrent+59]=this.sle_carica_tutti
this.Control[iCurrent+60]=this.cb_carica_tutti
this.Control[iCurrent+61]=this.st_29
this.Control[iCurrent+62]=this.cb_1
this.Control[iCurrent+63]=this.lb_1
end on

on w_importa_files_ascii.destroy
call super::destroy
destroy(this.cb_chiudi_log)
destroy(this.cbx_cat_com)
destroy(this.st_5)
destroy(this.cb_cerca_cat_com)
destroy(this.cb_cat_commerciali)
destroy(this.sle_file_cat_commerciali)
destroy(this.sle_file_origine_commesse)
destroy(this.sle_file_origine_clienti)
destroy(this.st_2)
destroy(this.sle_file_origine)
destroy(this.cb_carica_clienti)
destroy(this.cb_carica_prodotti)
destroy(this.cb_carica_commesse)
destroy(this.st_1)
destroy(this.sle_file_origine_fornitori)
destroy(this.cb_carica_fornitori)
destroy(this.sle_file_origine_tes_fasi_lavoro)
destroy(this.cb_carica_tes_fasi)
destroy(this.sle_file_origine_distinta)
destroy(this.cb_carica_distinta)
destroy(this.sle_file_origine_descriz_1)
destroy(this.cb_carica_descrizioni_1)
destroy(this.st_10)
destroy(this.st_stringa)
destroy(this.cb_cerca_prodotti)
destroy(this.cb_cerca_clienti)
destroy(this.cb_cerca_fornitori)
destroy(this.cb_cerca_commesse)
destroy(this.cb_cerca_tes_fasi)
destroy(this.cb_cerca_distinta)
destroy(this.cb_cerca_descrizioni_1)
destroy(this.st_3)
destroy(this.st_21)
destroy(this.st_22)
destroy(this.st_23)
destroy(this.st_24)
destroy(this.st_25)
destroy(this.sle_file_origine_det_fasi_lavoro)
destroy(this.cb_carica_det_fasi)
destroy(this.cb_cerca_det_fasi)
destroy(this.st_62)
destroy(this.sle_file_origine_descriz_2)
destroy(this.sle_file_origine_descriz_3)
destroy(this.sle_file_origine_descriz_4)
destroy(this.cb_cerca_descrizioni_2)
destroy(this.cb_cerca_descrizioni_3)
destroy(this.cb_cerca_descrizioni_4)
destroy(this.st_32)
destroy(this.st_33)
destroy(this.st_34)
destroy(this.ln_1)
destroy(this.st_commit)
destroy(this.sle_da_anno_comm)
destroy(this.cbx_anag_prodotti)
destroy(this.cbx_anag_clienti)
destroy(this.cbx_anag_fornitori)
destroy(this.st_4)
destroy(this.ln_2)
destroy(this.sle_carica_tutti)
destroy(this.cb_carica_tutti)
destroy(this.st_29)
destroy(this.cb_1)
destroy(this.lb_1)
end on

type cb_chiudi_log from commandbutton within w_importa_files_ascii
boolean visible = false
integer x = 2377
integer y = 1460
integer width = 434
integer height = 80
integer taborder = 340
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi Log"
end type

event clicked;cb_chiudi_log.visible = false
lb_1.visible = false
end event

type cbx_cat_com from checkbox within w_importa_files_ascii
integer x = 2331
integer y = 840
integer width = 494
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Aggiorna Campi"
end type

type st_5 from statictext within w_importa_files_ascii
integer x = 366
integer y = 840
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_cerca_cat_com from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 840
integer width = 69
integer height = 80
integer taborder = 270
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;integer value
string docname, named


value = GetFileOpenName("Select File", docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_cat_commerciali.text = docname
end event

type cb_cat_commerciali from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 840
integer width = 343
integer height = 80
integer taborder = 260
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cat. Com."
end type

event clicked;integer li_filenum, li_ritorno, li_exit, li_riga, li_risposta
long dimensione, ll_quantita, riga, ll_num_sequenza, ll_commit
string ls_buffer, ls_cod_categoria, ls_des_categoria, ls_update
double ldd_quantita 


if not(ib_carica_tutti) then
   if len(sle_file_cat_commerciali.text) < 1 or isnull(sle_file_cat_commerciali.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Distinta ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

riga = 0
li_filenum = fileopen(sle_file_cat_commerciali.text, linemode! )
pcca.mdi_frame.setmicrohelp("Caricamento categorie commerciali in corso...")

if li_filenum = -1 then return   

ll_commit = 0
li_ritorno = 0

if cbx_cat_com.checked then
	ls_update = 'S'
else
	ls_update = 'N'
end if


do
	if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
		
		st_stringa.text = "Processo riga " + string(riga)
		
		ls_cod_categoria = trim(mid(ls_buffer, 1, 3))
		ls_des_categoria = trim(mid(ls_buffer, 5, 40))
		
		li_exit = wf_inserisci_cat_commerciali(ls_cod_categoria, ls_des_categoria, ls_update) 
					
		if li_exit = -1 then
			if ib_carica_tutti then
				wf_scrivi_log("Procedura di caricamento categorie commerciali interrotta dall'utente alla codice " + ls_cod_categoria)
				return
			else
				li_risposta = g_mb.messagebox("Importazione Categorie Commerciali", "Procedura interrotta dall'utente: proseguo?", Question!, YesNo!, 2)
				if li_risposta = 2 then return
			end if
		end if
	end if
	
	li_ritorno = fileread(li_filenum, ls_buffer)
	riga = riga + 1
	
	if li_ritorno = 0 then
		if ib_carica_tutti then
			wf_scrivi_log("Nessun carattere letto dal Files ASCII Distinta")
			return
		else
			li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
			if li_exit = 2 then return
		end if
	end if
loop until (li_ritorno = -100) or (li_ritorno = -1)

if li_ritorno = -100 then
	wf_scrivi_log("Importazione dati distinta terminata correttamente")
end if

if li_ritorno = -1 then
	wf_scrivi_log("Errore generale nella lettura del file ASCII distinta")
	rollback;
	fileclose(li_filenum)
	return -1
end if

pcca.mdi_frame.setmicrohelp("Caricamento categorie commerciali terminata ...")
commit;
fileclose(li_filenum)
   
end event

type sle_file_cat_commerciali from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 840
integer width = 1669
integer height = 80
integer taborder = 260
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_file_origine_commesse from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 440
integer width = 1669
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_file_origine_clienti from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 240
integer width = 1669
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_importa_files_ascii
integer x = 366
integer y = 140
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_file_origine from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 140
integer width = 1669
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_carica_clienti from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 240
integer width = 343
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Clienti"
end type

event clicked;long 	  riga, ll_commit

integer li_filenum, li_ritorno, li_exit, li_risposta

string  ls_buffer, ls_cod_cliente, ls_rag_soc, ls_cap, ls_indirizzo, ls_localita, ls_provincia, &
		  ls_partita_iva, ls_cod_banca, ls_pagamento, ls_update, ls_rag_soc_2, ls_tipo_cliente, &
		  ls_cod_valuta


if not(ib_carica_tutti) then
   if len(sle_file_origine_clienti.text) < 1 or isnull(sle_file_origine_clienti.text) then 
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Clienti ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

wf_mostra_log(true)
pcca.mdi_frame.setmicrohelp("Caricamento clienti in corso...")

riga = 0
li_filenum = fileopen(sle_file_origine_clienti.text, linemode! )

if li_filenum = -1 then return

if cbx_anag_clienti.checked then
	ls_update = "S"
else
	ls_update = "N"
end if

ll_commit = 0
li_ritorno = 0
do
	if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
		st_stringa.text = ls_buffer + ">>>"+string(riga)
		
		ls_cod_cliente	= trim(mid(ls_buffer,   6,  6))
		ls_rag_soc		= trim(mid(ls_buffer,  13,  40))
		ls_rag_soc_2	= trim(mid(ls_buffer,  54,  40))
		ls_cap			= trim(mid(ls_buffer,  95,  5))
		ls_indirizzo		= trim(mid(ls_buffer,  101,  40))
		ls_localita		= trim(mid(ls_buffer,  142, 40))
		ls_provincia		= trim(mid(ls_buffer, 183, 2))
		ls_partita_iva	= trim(mid(ls_buffer, 203, 16))
		ls_tipo_cliente	= trim(mid(ls_buffer, 220, 1))
		ls_cod_valuta   = trim(mid(ls_buffer, 222, 4))
		
		//         ls_cod_cliente = trim(mid(ls_buffer,   6,  6))
		//         ls_rag_soc     = trim(mid(ls_buffer,  13,  35))
		//         ls_cap         = trim(mid(ls_buffer,  49,  5))
		//         ls_indirizzo   = trim(mid(ls_buffer,  55,  35))
		//         ls_localita    = trim(mid(ls_buffer,  91, 25))
		//         ls_provincia   = trim(mid(ls_buffer, 117, 2))
		//         ls_partita_iva = trim(mid(ls_buffer, 120, 16))
		//         ls_pagamento   = trim(mid(ls_buffer, 144, 3))
		
		setnull(ls_cod_banca)
		
		li_exit = w_insert_anag_clienti(ls_cod_cliente, ls_rag_soc, ls_rag_soc_2, ls_indirizzo, ls_cap, ls_localita, ls_provincia, ls_partita_iva, ls_cod_valuta, ls_cod_banca, ls_update, ls_tipo_cliente)
		if li_exit = -1 then exit

		li_ritorno = fileread(li_filenum, ls_buffer)
		riga = riga + 1
		if li_ritorno = 0 then
			wf_scrivi_log("Nessun carattere letto da file ASCII clienti")
			//li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
		end if
	end if
loop until (li_ritorno = -100) or (li_ritorno = -1)

if li_ritorno = -100 then
	wf_scrivi_log("Importazione File ASCII clienti terminata correttamente")
	wf_mostra_log(false)
end if

if li_ritorno = -1 then
	rollback;
	wf_scrivi_log("Errore generale durante la lettura del file ASCII cliente")
	fileclose(li_filenum)
	return -1
end if

pcca.mdi_frame.setmicrohelp("Caricamento clienti terminata ...")
commit;
fileclose(li_filenum)
end event

type cb_carica_prodotti from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 140
integer width = 343
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prodotti"
end type

event clicked;integer li_filenum, li_ritorno, li_exit, li_exit_1, value, li_risposta
long  riga, ll_commit
double ll_ult_prz_acq
string ls_buffer, ls_update, ls_cod_prodotto, ls_des_prodotto, ls_um, ls_cod_fornitore, &
			ls_cod_deposito, ls_cod_cat_commerciale
string docname, named
decimal ld_ultimo_prezzo
 
if not(ib_carica_tutti) then
   if len(sle_file_origine.text) < 1 or isnull(sle_file_origine.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if
   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Prodotti ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

wf_mostra_log(true)
riga = 0
li_filenum = fileopen(sle_file_origine.text, linemode!)   

if li_filenum = -1 then return

pcca.mdi_frame.setmicrohelp("Caricamento prodotti in corso ...")

if cbx_anag_prodotti.checked then
	ls_update = "S"
else
	ls_update = "N"
end if

ll_commit = 0
li_ritorno = 0
do
	if (li_ritorno <> 0) and (li_ritorno <> -100) and (li_ritorno <> -1) then
			
		// stefano 12/04/2010: addattamento specifica "Importazione da file di testo ed excel"
//		st_stringa.text = ls_buffer + ">>>"+string(riga)
//		ls_cod_art = trim(mid(ls_buffer,1,16) )
//		ls_des_art = mid(ls_buffer,18,35)	
//		
//		ls_cod_misura = trim(mid(ls_buffer,54,3))
//		if isnull(ls_cod_misura) or len(ls_cod_misura) < 1 then ls_cod_misura = "NR."
//		ls_codice_iva = trim(mid(ls_buffer,58,2))
//		if isnull(ls_codice_iva) or len(ls_codice_iva) < 1 then ls_codice_iva = "19"
//		ll_ult_prz_acq = double(mid(ls_buffer,74,11))
//		li_exit = w_insert_anag_prodotti(ls_cod_art, ls_des_art, ls_cod_misura, ls_codice_iva, ll_ult_prz_acq, ls_update)

		st_stringa.text = "Processo riga " + string(riga)
		
		ls_cod_prodotto = trim(mid(ls_buffer, 1, 15))
		ls_des_prodotto = trim(mid(ls_buffer, 17, 40))
		ls_um = trim(mid(ls_buffer, 58, 3))
		ls_cod_iva = trim(mid(ls_buffer, 62, 3))
		ld_ultimo_prezzo = dec(trim(mid(ls_buffer, 77,11)))
		ls_cod_fornitore = trim(mid(ls_buffer, 94, 6))
		ls_cod_deposito = trim(mid(ls_buffer, 101, 3))
		ls_cod_cat_commerciale = trim(mid(ls_buffer, 105, 3))
				
		li_exit = wf_inserisci_prodotti(ls_cod_prodotto, ls_des_prodotto, ls_um, ls_cod_iva, ld_ultimo_prezzo, ls_cod_fornitore, ls_cod_deposito, ls_cod_cat_commerciale, ls_update)
		
		if li_exit = 0 then ll_commit = ll_commit + 1
//		if ll_commit > 10 then
//			st_commit.text = "COMMIT in corso ..."
//			COMMIT;
//			ll_commit = 0
//		else
//			st_commit.text = ""
//		end if
	end if
	
	li_ritorno = fileread(li_filenum, ls_buffer)
	riga = riga + 1
	if li_ritorno = 0 then
		wf_scrivi_log("Caricamento Prodotti: nessun carattere letto")

		if li_exit = 2 then return
	end if
loop until (li_ritorno = -100) or (li_ritorno = -1)

if li_ritorno = -100 then
	wf_scrivi_log("Caricamento Prodotti terminata correttamente")
	wf_mostra_log(false)
end if


if li_ritorno = -1 then
	wf_scrivi_log("Caricamento Prodotti: errore generale di lettura sul file ASCII prodotti")
	fileclose(li_filenum)
	rollback;
	return -1
end if

commit;
pcca.mdi_frame.setmicrohelp("Caricamento prodotti terminata ...")
fileclose(li_filenum)
end event

type cb_carica_commesse from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 440
integer width = 343
integer height = 80
integer taborder = 160
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Commesse"
end type

event clicked;integer li_filenum, li_ritorno, li_exit, li_riga_ord, li_risposta, li_gg, li_mm, li_aa
long dimensione, riga, ll_anno_comm, ll_da_anno, ll_num_comm, ll_anno_ord, ll_num_ord, ll_riga_ord, ll_commit
string ls_buffer, ls_cod_cliente, ls_cod_prodotto
double ld_quan_ordine, ld_quan_evasa, ld_quan_comm
datetime ldt_data_comm, ldt_data_evasione
 

if not(ib_carica_tutti) then
   if len(sle_file_origine_commesse.text) < 1 or isnull(sle_file_origine_commesse.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if
   if len(sle_da_anno_comm.text) < 1 or isnull(sle_da_anno_comm.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Anno di partenza mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Commesse ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

	riga = 0
	li_filenum = fileopen(sle_file_origine_commesse.text, linemode! )

   if li_filenum = -1 then return   

   ll_commit = 0
   li_ritorno = 0
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
			ls_cod_cliente = trim(mid(ls_buffer,7,6))
			ll_anno_comm = 1990 + long(mid(ls_buffer,1,1))
         if ll_anno_comm >= long(sle_da_anno_comm.text) then
   			ll_num_comm = long(mid(ls_buffer,2,4))
 	   		ld_quan_comm = double(mid(ls_buffer,94,9))

   			li_aa = integer(mid(ls_buffer,31,2))
   	      if li_aa = 0 then
     	         ldt_data_comm = datetime(date(1900-01-01))
       	   else
     	         ldt_data_comm = datetime(date("19"+mid(ls_buffer,31,2)+"/"+mid(ls_buffer,33,2)+"/"+mid(ls_buffer,35,2)))
            end if 

   			li_aa = integer(mid(ls_buffer,39,2))
            if li_aa = 0 then
               ldt_data_evasione = datetime(date(1900-01-01))
            else
               ldt_data_evasione = datetime(date("19"+mid(ls_buffer,39,2)+"/"+mid(ls_buffer,41,2)+"/"+mid(ls_buffer,43,2)))
            end if 

   			ll_anno_ord = ll_anno_comm
	   		ll_num_ord = long(mid(ls_buffer,15,6))
 				ll_riga_ord = long(mid(ls_buffer,53,4)) 
				ld_quan_ordine = double(mid(ls_buffer,82,4))
      	   ls_cod_prodotto = trim(mid(ls_buffer,65,16))

	         li_exit = wf_insert_anag_commesse(ls_cod_cliente, ll_anno_comm, ll_num_comm, ld_quan_comm, ldt_data_comm, ldt_data_evasione, ll_anno_ord, ll_num_ord, ll_riga_ord, ld_quan_ordine, ls_cod_prodotto)
            if li_exit = 0 then ll_commit = ll_commit + 1
  	         if ll_commit > 10 then
               st_commit.text = "COMMIT in corso ..."
  	            COMMIT;
               ll_commit = 0
            else
               st_commit.text = ""
            end if
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         if ib_carica_tutti then
            wf_scrivi_log("Nessun carattere letto dal file ASCII commesse")
            return
         else
            li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
            if li_exit = 2 then return
         end if
  	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      wf_scrivi_log("Importazione Commesse terminata correttamente")
//      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      wf_scrivi_log("Errore generale durante la lettura del file ASCII")
//      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)
   
end event

type st_1 from statictext within w_importa_files_ascii
integer x = 23
integer y = 20
integer width = 2789
integer height = 100
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Importazione/Esportazione    Dati    Tramite    Files  A S C I I "
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type sle_file_origine_fornitori from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 340
integer width = 1669
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_carica_fornitori from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 340
integer width = 343
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fornitori"
end type

event clicked;long 	  dimensione, riga, ll_commit

integer li_filenum, li_ritorno, li_exit, li_risposta

string  ls_buffer, ls_cod_for, ls_rag_soc, ls_cap, ls_indirizzo, ls_localita, ls_provincia, ls_partita_iva, &
		  ls_cod_banca, ls_pagamento, ls_telefono, ls_cellulare, ls_fax, ls_email, ls_rif_interno, ls_web, &
		  ls_rag_soc_2, ls_tipo_fornitore


if not(ib_carica_tutti) then
   if len(sle_file_origine_fornitori.text) < 1 or isnull(sle_file_origine_fornitori.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Fornitori ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

wf_mostra_log(true)

riga = 0
li_filenum = fileopen(sle_file_origine_fornitori.text, linemode! )
pcca.mdi_frame.setmicrohelp("Caricamento fornitori in corso ...")

if li_filenum = -1 then return

ll_commit = 0
li_ritorno = 0
do
	if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
		// stefano 12/04/2010
		// st_stringa.text = ls_buffer + ">>>"+string(riga)
		// ls_cod_for     = trim(mid(ls_buffer,   6,  6))
		// ls_rag_soc     = trim(mid(ls_buffer,  13,  35))
		// ls_cap         = trim(mid(ls_buffer,  49,  5))
		// ls_indirizzo   = trim(mid(ls_buffer,  55,  35))
		// ls_localita    = trim(mid(ls_buffer,  91, 25))
		// ls_provincia   = trim(mid(ls_buffer, 117, 2))
		// ls_partita_iva = trim(mid(ls_buffer, 120, 16))
		// ls_pagamento   = trim(mid(ls_buffer, 144, 3))
		//		
		// ls_telefono    = trim(mid(ls_buffer, 150, 20))
		// ls_cellulare   = trim(mid(ls_buffer, 170, 20))
		// ls_fax         = trim(mid(ls_buffer, 190, 20))
		// ls_email       = trim(mid(ls_buffer, 210, 40))
		// ls_rif_interno = trim(mid(ls_buffer, 250, 30))
		// ls_web         = trim(mid(ls_buffer, 280, 100))
	
		ls_cod_for     = trim(mid(ls_buffer,   6,  6))
		ls_rag_soc     = trim(mid(ls_buffer,  13,  40))
		ls_rag_soc_2     = trim(mid(ls_buffer,  54,  40))
		ls_cap         = trim(mid(ls_buffer,  95,  5))
		ls_indirizzo   = trim(mid(ls_buffer,  101,  40))
		ls_localita    = trim(mid(ls_buffer,  142, 40))
		ls_provincia   = trim(mid(ls_buffer, 183, 2))
		ls_partita_iva = trim(mid(ls_buffer, 203, 16))
		ls_tipo_fornitore = trim(mid(ls_buffer, 220, 1))
		ls_pagamento   = trim(mid(ls_buffer, 222, 4))		
		
		setnull(ls_cod_banca)
	
		// li_exit = wf_insert_anag_fornitori(ls_cod_for, ls_rag_soc, ls_cap, ls_indirizzo, ls_provincia, ls_partita_iva, &
		//						 								  ls_cod_banca, ls_pagamento, ls_localita, ls_telefono, ls_cellulare, ls_fax, &
		//														  ls_email, ls_rif_interno, ls_web)
		li_exit = w_inserisci_fornitore_2(ls_cod_for, ls_rag_soc, ls_rag_soc_2, ls_cap, ls_indirizzo,ls_localita, ls_provincia, ls_partita_iva, &
		ls_cod_banca, ls_pagamento, ls_tipo_fornitore)
		
		
		if li_exit = 0 then ll_commit = ll_commit + 1
		if li_exit = -1 then	exit
		
		li_ritorno = fileread(li_filenum, ls_buffer)
		riga = riga + 1
		if li_ritorno = 0 then
			if ib_carica_tutti then
				wf_scrivi_log("Nessun carattere letto dal file ASCII fornitori")
				return
			else
				li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
				if li_exit = 2 then return
			end if
		end if
	end if
loop until (li_ritorno = -100) or (li_ritorno = -1)

if li_ritorno = -100 then
	wf_scrivi_log("Importazione file ASCII fornitori terminata correttamente")
	wf_mostra_log(false)
end if

if li_ritorno = -1 then
	rollback;
	wf_scrivi_log("Errore generale nella lettura file ASCII fornitori")
	fileclose(li_filenum)
	return -1
end if

commit;
pcca.mdi_frame.setmicrohelp("Caricamento fornitori terminata ...")
fileclose(li_filenum)
   




end event

type sle_file_origine_tes_fasi_lavoro from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 540
integer width = 1669
integer height = 80
integer taborder = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_carica_tes_fasi from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 540
integer width = 343
integer height = 80
integer taborder = 170
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tes.Fasi"
end type

event clicked;integer li_filenum, li_ritorno, li_exit, li_exit_1, value, li_risposta, li_riga, li_fase
long  riga, ll_commit
string ls_cod_prodotto, ls_descrizione, ls_buffer, docname, named, ls_cod_materia_prima


if not(ib_carica_tutti) then
   if len(sle_file_origine_tes_fasi_lavoro.text) < 1 or isnull(sle_file_origine_tes_fasi_lavoro.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Attenzione, bisogna aver inserito la conversione tra lavorazione e reparti in parametri_aziende",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if
   
	riga = 0

	li_filenum = fileopen(sle_file_origine_tes_fasi_lavoro.text, linemode! )   
   if li_filenum = -1 then return

   DELETE FROM import_fasi_lavorazione;

   ll_commit = 0
   li_ritorno = 0
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))                  // codice prodotto finito
         li_riga = integer(trim(mid(ls_buffer,19,4)))                 // progressivo
         li_fase = integer(trim(mid(ls_buffer,25,4)))                 // progressivo
         ls_descrizione = trim(mid(ls_buffer,31,40))                  // descrizione fase
         ls_cod_materia_prima = trim(mid(ls_buffer, 73, 16))          // materia prima usata

         INSERT INTO import_fasi_lavorazione  
                     ( cod_azienda,   
                       cod_prodotto,   
                       progressivo,   
                       num_fase,   
                       des_fase,   
                       cod_materia_prima )  
         VALUES      ( :s_cs_xx.cod_azienda,   
                       :ls_cod_prodotto,   
                       :li_riga,   
                       :li_fase,   
                       :ls_descrizione,   
                       :ls_cod_materia_prima )  ;

         if sqlca.sqlcode <> 0 then
//            g_mb.messagebox("ERRORE DB",sqlca.sqlerrtext)
            wf_scrivi_log("Errore in INSERT import_tes_fasi_lavorazione; dettaglio errore SQL-->"+sqlca.sqlerrtext)
         end if

         if sqlca.sqlcode = 0 then ll_commit = ll_commit + 1
         if ll_commit > 10 then
            st_commit.text = "COMMIT in corso ..."
            COMMIT;
            ll_commit = 0
         else
            st_commit.text = ""
         end if

      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         if ib_carica_tutti then
            wf_scrivi_log("Nessun carattere letto dal file ASCII Fasi di Lavoro")
            return
         else
            li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
            if li_exit = 2 then return
         end if
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      wf_scrivi_log("Importazione files ASCII Import_tes_fasi_lavorazione eseguita con successo")
//      g_mb.messagebox("IMPORTAZIONE FILES ASCII","Importazione Terminata")
	end if
   if li_ritorno = -1 then
      wf_scrivi_log("Errore generale durante la lettura del file ASCII import_tes_fasi_lavorazione")
//      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)

end event

type sle_file_origine_distinta from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 740
integer width = 1669
integer height = 80
integer taborder = 250
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_carica_distinta from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 740
integer width = 343
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Distinta"
end type

on clicked;integer li_filenum, li_ritorno, li_exit, li_riga, li_risposta
long dimensione, ll_quantita, riga, ll_num_sequenza, ll_commit
string ls_buffer, ls_cod_prodotto, ls_cod_materia_prima, old_prodotto
double ldd_quantita 


if not(ib_carica_tutti) then
   if len(sle_file_origine_distinta.text) < 1 or isnull(sle_file_origine_distinta.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII origine mancante",Information! )
      return
   end if

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Distinta ?",Question!	,OKCancel! )
   if li_risposta <> 1 then return
end if

	riga = 0
	li_filenum = fileopen(sle_file_origine_distinta.text, linemode! )

   if li_filenum = -1 then return   

   DELETE FROM import_distinta;
   COMMIT;

   ll_commit = 0
   li_ritorno = 0
   ll_num_sequenza = 10
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))
         ls_cod_materia_prima = trim(mid(ls_buffer,21,16))
         li_riga = integer(trim(mid(ls_buffer,18,2)))
         ldd_quantita = double(mid(ls_buffer,74,7))

         if (li_riga > 0) and (mid(ls_cod_materia_prima,1,1) <> "-") and (len(ls_cod_materia_prima) > 0) and (len(ls_cod_prodotto) > 0) then
            if old_prodotto <> ls_cod_prodotto then
               ll_num_sequenza = 10
               old_prodotto = ls_cod_prodotto
            else
               ll_num_sequenza = ll_num_sequenza + 10
            end if

            li_exit = wf_insert_distinta(ls_cod_prodotto, ls_cod_materia_prima, li_riga, ldd_quantita, ll_num_sequenza)

            if li_exit = 0 then ll_commit = ll_commit + 1
               if ll_commit > 10 then
               st_commit.text = "COMMIT in corso ..."
               COMMIT;
               ll_commit = 0
            else
               st_commit.text = ""
            end if

            if li_exit = -1 then
               if ib_carica_tutti then
                  wf_scrivi_log("Procedura di caricamento distinta interrotta dall'utente al prodotto "+ls_cod_prodotto)
                  return
               else
                  li_risposta = g_mb.messagebox("Importazione Distinta","Procedura interrotta dall'utente: proseguo?",Question!, YesNo!, 2)
                  if li_risposta = 2 then return
               end if
            end if
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         if ib_carica_tutti then
            wf_scrivi_log("Nessun carattere letto dal Files ASCII Distinta")
            return
         else
            li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
            if li_exit = 2 then return
         end if
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      wf_scrivi_log("Importazione dati distinta terminata correttamente")
//      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      wf_scrivi_log("Errore generale nella lettura del file ASCII distinta")
//      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)
   
end on

type sle_file_origine_descriz_1 from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 1080
integer width = 1669
integer height = 80
integer taborder = 280
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type cb_carica_descrizioni_1 from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 1080
integer width = 343
integer height = 80
integer taborder = 190
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Des.Art"
end type

event clicked;integer li_filenum, li_ritorno, li_exit, li_riga, li_risposta
long dimensione, ll_quantita, riga, ll_num_sequenza
string ls_buffer, ls_cod_prodotto, ls_descrizione
 

   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Descrizioni Aggiuntive?",Question!	,OKCancel! )

   if li_risposta <> 1 then return
   if len(trim(sle_file_origine_descriz_1.text)) = 0 then
      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file 1 di origine")
      return
   end if
//   if len(trim(sle_file_origine_descriz_2.text)) = 0 then
//      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file 2 di origine")
//      return
//   end if
//   if len(trim(sle_file_origine_descriz_3.text)) = 0 then
//      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file 3 di origine")
//      return
//   end if
//   if len(trim(sle_file_origine_descriz_4.text)) = 0 then
//      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file 4 di origine")
//      return
//   end if

	riga = 0
	li_filenum = fileopen(sle_file_origine_descriz_1.text, linemode! )

   if li_filenum = -1 then return   

   li_ritorno = 0
   ll_num_sequenza = 10
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))
         if len(trim(mid(ls_buffer,18,35))) > 0 then ls_descrizione  = trim(mid(ls_buffer,18,35)) + char(13) + char(10)
         if len(trim(mid(ls_buffer,55,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,55,35)) + char(13) + char(10)

         if len(trim(mid(ls_buffer,92,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,92,35)) + char(13) + char(10)
         if len(trim(mid(ls_buffer,129,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,129,35)) + char(13) + char(10)
         if len(trim(mid(ls_buffer,166,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,166,35))

         if (len(ls_cod_prodotto) > 0) and (len(ls_descrizione) > 0) then
            li_exit = wf_insert_descrizioni(ls_cod_prodotto, ls_descrizione, "001", false)
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
         if li_exit = 2 then return
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)
   
   return

// -------------------  caricamento seconda parte descrizioni da file nr 2 ------------------


   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Descrizioni Aggiuntive?",Question!	,OKCancel! )

   if li_risposta <> 1 then return
   if len(trim(sle_file_origine_descriz_2.text)) = 0 then
      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file di origine")
      return
   end if

	riga = 0
	li_filenum = fileopen(sle_file_origine_descriz_2.text, linemode! )

   if li_filenum = -1 then return   

   li_ritorno = 0
   ll_num_sequenza = 10
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))
         if len( trim(mid(ls_buffer,55,35)) ) > 0 then ls_descrizione  = trim(mid(ls_buffer,18,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,55,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,55,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,92,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,92,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,129,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,129,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,166,35))) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,166,35))

         if (li_riga > 0) and  (len(ls_cod_prodotto) > 0) then
            li_exit = wf_insert_descrizioni(ls_cod_prodotto, ls_descrizione, "001", true)
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
         if li_exit = 2 then return
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)


// ------------------------------------------------------------------------------------------
// ------------------ inserisco descrizioni produzione in nota002 ---------------------------
// ------------------------------------------------------------------------------------------
   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Descrizioni Aggiuntive?",Question!	,OKCancel! )

   if li_risposta <> 1 then return
   if len(trim(sle_file_origine_descriz_3.text)) = 0 then
      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file di origine")
      return
   end if


	riga = 0
	li_filenum = fileopen(sle_file_origine_descriz_3.text, linemode! )

   if li_filenum = -1 then return   

   li_ritorno = 0
   ll_num_sequenza = 10
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))
         if len( trim(mid(ls_buffer,18,35)) ) > 0 then ls_descrizione  = trim(mid(ls_buffer,18,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,55,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,55,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,92,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,92,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,129,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,129,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,166,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,166,35))

         if (len(ls_cod_prodotto) > 0) and (len(ls_descrizione) > 0) then
            li_exit = wf_insert_descrizioni(ls_cod_prodotto, ls_descrizione, "002", false)
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
         if li_exit = 2 then return
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)
   


// -------------------  caricamento seconda parte descrizioni da file nr 2 ------------------


   li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Eseguo Elaborazione Caricamento Descrizioni Aggiuntive?",Question!	,OKCancel! )

   if li_risposta <> 1 then return
   if len(trim(sle_file_origine_descriz_4.text)) = 0 then
      li_risposta = g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Manca il file di origine")
      return
   end if

	riga = 0
	li_filenum = fileopen(sle_file_origine_descriz_4.text, linemode! )

   if li_filenum = -1 then return   

   li_ritorno = 0
   ll_num_sequenza = 10
	do
      if (li_ritorno <> 0) and(li_ritorno <> -100)and (li_ritorno <> -1) then
         st_stringa.text = ls_buffer + ">>>"+string(riga)
        
         ls_cod_prodotto = trim(mid(ls_buffer,1,16))
         if len( trim(mid(ls_buffer,18,35)) ) > 0 then ls_descrizione  = trim(mid(ls_buffer,18,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,55,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,55,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,92,35)) ) > 0 then ls_descrizione  = ls_descrizione + trim(mid(ls_buffer,92,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,129,35)) ) > 0 then ls_descrizione = ls_descrizione + trim(mid(ls_buffer,129,35)) + char(13) + char(10)
         if len( trim(mid(ls_buffer,166,35)) ) > 0 then ls_descrizione = ls_descrizione + trim(mid(ls_buffer,166,35))

         if (li_riga > 0) and  (len(ls_cod_prodotto) > 0) then
            li_exit = wf_insert_descrizioni(ls_cod_prodotto, ls_descrizione, "002", true)
         end if
      end if
      li_ritorno = fileread(li_filenum, ls_buffer)
      riga = riga + 1
      if li_ritorno = 0 then
         li_exit = g_mb.messagebox("ASCII", "Nessun Carattere Letto"+string(riga), Question!	,YesNo!  )
         if li_exit = 2 then return
	   end if
	loop until (li_ritorno = -100) or (li_ritorno = -1)
	
   if li_ritorno = -100 then
      g_mb.messagebox("ASCII", "Trovato EOF  >>> "+string(riga) )
	end if
   if li_ritorno = -1 then
      g_mb.messagebox("ASCII", "Errore" )
	end if

	fileclose(li_filenum)


end event

type st_10 from statictext within w_importa_files_ascii
integer x = 23
integer y = 1480
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Record in Lettura:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_stringa from statictext within w_importa_files_ascii
integer x = 23
integer y = 1560
integer width = 2789
integer height = 80
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_cerca_prodotti from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 140
integer width = 69
integer height = 80
integer taborder = 90
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine.text = docname
end on

type cb_cerca_clienti from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 240
integer width = 69
integer height = 80
integer taborder = 70
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")


if value <> 1 then return

sle_file_origine_clienti.text = docname
end on

type cb_cerca_fornitori from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 340
integer width = 69
integer height = 80
integer taborder = 60
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_fornitori.text = docname
end on

type cb_cerca_commesse from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 440
integer width = 69
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_commesse.text = docname
end on

type cb_cerca_tes_fasi from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 540
integer width = 69
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_tes_fasi_lavoro.text = docname
end on

type cb_cerca_distinta from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 740
integer width = 69
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_distinta.text = docname
end on

type cb_cerca_descrizioni_1 from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 1080
integer width = 69
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_descriz_1.text = docname
end on

type st_3 from statictext within w_importa_files_ascii
integer x = 366
integer y = 240
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_21 from statictext within w_importa_files_ascii
integer x = 366
integer y = 740
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_22 from statictext within w_importa_files_ascii
integer x = 366
integer y = 1080
integer width = 165
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE1:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_23 from statictext within w_importa_files_ascii
integer x = 366
integer y = 540
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_24 from statictext within w_importa_files_ascii
integer x = 366
integer y = 440
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_25 from statictext within w_importa_files_ascii
integer x = 366
integer y = 340
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_file_origine_det_fasi_lavoro from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 640
integer width = 1669
integer height = 80
integer taborder = 240
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type cb_carica_det_fasi from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 640
integer width = 343
integer height = 80
integer taborder = 230
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Det.Fasi"
end type

type cb_cerca_det_fasi from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 640
integer width = 69
integer height = 80
integer taborder = 220
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_det_fasi_lavoro.text = docname
end on

type st_62 from statictext within w_importa_files_ascii
integer x = 366
integer y = 640
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_file_origine_descriz_2 from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 1180
integer width = 1669
integer height = 80
integer taborder = 350
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type sle_file_origine_descriz_3 from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 1280
integer width = 1669
integer height = 80
integer taborder = 340
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type sle_file_origine_descriz_4 from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 1380
integer width = 1669
integer height = 80
integer taborder = 330
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type cb_cerca_descrizioni_2 from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 1180
integer width = 69
integer height = 80
integer taborder = 320
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_descriz_2.text = docname
end on

type cb_cerca_descrizioni_3 from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 1280
integer width = 69
integer height = 80
integer taborder = 310
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_descriz_3.text = docname
end on

type cb_cerca_descrizioni_4 from commandbutton within w_importa_files_ascii
integer x = 2217
integer y = 1380
integer width = 69
integer height = 80
integer taborder = 290
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

on clicked;integer value
string docname, named


value = GetFileOpenName("Select File",  &
docname, named,"TXT","Text Files (*.TXT), *.TXT")

if value <> 1 then return

sle_file_origine_descriz_4.text = docname
end on

type st_32 from statictext within w_importa_files_ascii
integer x = 366
integer y = 1180
integer width = 165
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE2:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_33 from statictext within w_importa_files_ascii
integer x = 366
integer y = 1280
integer width = 165
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE3:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_34 from statictext within w_importa_files_ascii
integer x = 366
integer y = 1380
integer width = 165
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE4:"
alignment alignment = center!
boolean focusrectangle = false
end type

type ln_1 from line within w_importa_files_ascii
long linecolor = 33554432
integer linethickness = 13
integer beginx = 23
integer beginy = 1060
integer endx = 2286
integer endy = 1060
end type

type st_commit from statictext within w_importa_files_ascii
integer x = 503
integer y = 1480
integer width = 594
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_da_anno_comm from singlelineedit within w_importa_files_ascii
integer x = 2606
integer y = 440
integer width = 206
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type cbx_anag_prodotti from checkbox within w_importa_files_ascii
integer x = 2309
integer y = 140
integer width = 494
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Aggiorna Campi"
end type

type cbx_anag_clienti from checkbox within w_importa_files_ascii
integer x = 2309
integer y = 240
integer width = 494
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Aggiorna Campi"
end type

type cbx_anag_fornitori from checkbox within w_importa_files_ascii
integer x = 2309
integer y = 340
integer width = 494
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Aggiorna Campi"
end type

type st_4 from statictext within w_importa_files_ascii
integer x = 2309
integer y = 440
integer width = 288
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Solo Anno:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ln_2 from line within w_importa_files_ascii
long linecolor = 33554432
integer linethickness = 13
integer beginx = 23
integer beginy = 940
integer endx = 2286
integer endy = 940
end type

type sle_carica_tutti from singlelineedit within w_importa_files_ascii
integer x = 526
integer y = 960
integer width = 1669
integer height = 80
integer taborder = 270
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_carica_tutti from commandbutton within w_importa_files_ascii
integer x = 23
integer y = 960
integer width = 343
integer height = 80
integer taborder = 260
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

on clicked;   ib_carica_tutti = true

   if len(sle_carica_tutti.text) < 1 or isnull(sle_carica_tutti.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file LOG origine mancante",Information! )
      return
   end if

   if len(sle_file_origine.text) < 1 or isnull(sle_file_origine.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII prodotti origine mancante",Information! )
      return
   end if

   if len(sle_file_origine_clienti.text) < 1 or isnull(sle_file_origine_clienti.text) then 
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII clienti origine mancante",Information! )
      return
   end if

   if len(sle_file_origine_fornitori.text) < 1 or isnull(sle_file_origine_fornitori.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII fornitori origine mancante",Information! )
      return
   end if

   if len(sle_file_origine_commesse.text) < 1 or isnull(sle_file_origine_commesse.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII commesse origine mancante",Information! )
      return
   end if
   if len(sle_da_anno_comm.text) < 1 or isnull(sle_da_anno_comm.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Anno di partenza mancante",Information! )
      return
   end if

   if len(sle_file_origine_tes_fasi_lavoro.text) < 1 or isnull(sle_file_origine_tes_fasi_lavoro.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII fasi lavoro origine mancante",Information! )
      return
   end if

   if len(sle_file_origine_distinta.text) < 1 or isnull(sle_file_origine_distinta.text) then
      g_mb.messagebox("AGGIORNAMENTO ARCHIVI", "Percorso file ASCII distinta origine mancante",Information! )
      return
   end if

lb_1.show()
cb_1.show()   
cb_carica_prodotti.triggerevent("Clicked")
cb_carica_clienti.triggerevent("Clicked")
cb_carica_fornitori.triggerevent("Clicked")
cb_carica_commesse.triggerevent("Clicked")
cb_carica_tes_fasi.triggerevent("Clicked")
cb_carica_distinta.triggerevent("Clicked")
g_mb.messagebox("Importazione Files ASCII","Importazione Terminata",Information!)
ib_carica_tutti = false

end on

type st_29 from statictext within w_importa_files_ascii
integer x = 366
integer y = 960
integer width = 146
integer height = 72
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "FILE:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_importa_files_ascii
boolean visible = false
integer x = 2331
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 300
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

on clicked;LB_1.hide()
cb_1.hide()
end on

type lb_1 from listbox within w_importa_files_ascii
boolean visible = false
integer x = 23
integer y = 140
integer width = 2789
integer height = 1320
integer taborder = 210
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean hscrollbar = true
boolean vscrollbar = true
end type

