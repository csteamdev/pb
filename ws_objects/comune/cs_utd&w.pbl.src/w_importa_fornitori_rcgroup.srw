﻿$PBExportHeader$w_importa_fornitori_rcgroup.srw
forward
global type w_importa_fornitori_rcgroup from window
end type
type cb_2 from commandbutton within w_importa_fornitori_rcgroup
end type
type st_2 from statictext within w_importa_fornitori_rcgroup
end type
type st_1 from statictext within w_importa_fornitori_rcgroup
end type
type cbx_1 from checkbox within w_importa_fornitori_rcgroup
end type
type cb_1 from commandbutton within w_importa_fornitori_rcgroup
end type
end forward

global type w_importa_fornitori_rcgroup from window
integer width = 1851
integer height = 912
boolean titlebar = true
string title = "RCGROUP - Importazione Fornitori"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_2 cb_2
st_2 st_2
st_1 st_1
cbx_1 cbx_1
cb_1 cb_1
end type
global w_importa_fornitori_rcgroup w_importa_fornitori_rcgroup

on w_importa_fornitori_rcgroup.create
this.cb_2=create cb_2
this.st_2=create st_2
this.st_1=create st_1
this.cbx_1=create cbx_1
this.cb_1=create cb_1
this.Control[]={this.cb_2,&
this.st_2,&
this.st_1,&
this.cbx_1,&
this.cb_1}
end on

on w_importa_fornitori_rcgroup.destroy
destroy(this.cb_2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cbx_1)
destroy(this.cb_1)
end on

type cb_2 from commandbutton within w_importa_fornitori_rcgroup
integer x = 1664
integer y = 436
integer width = 128
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;long ll_ret
string docname, named

ll_ret = GetFileOpenName("Select File", docname, named, "TXT", "Text Files (*.TXT),*.TXT,")

if ll_ret = 1 then
	st_2.text = docname
else
	st_2.text = ""
end if
end event

type st_2 from statictext within w_importa_fornitori_rcgroup
integer x = 27
integer y = 436
integer width = 1623
integer height = 96
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

type st_1 from statictext within w_importa_fornitori_rcgroup
integer x = 23
integer y = 676
integer width = 1769
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_importa_fornitori_rcgroup
integer x = 407
integer y = 256
integer width = 937
integer height = 96
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiorna Fornitori Esistenti"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_importa_fornitori_rcgroup
integer x = 709
integer y = 116
integer width = 379
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;boolean lb_exit
integer li_File, li_test
long ll_ret, ll_pos, ll_campo,ll_cont, ll_inseriti, ll_aggiornati
string ls_str, ls_separatore,ls_cod_fornitore,ls_rag_soc_1,ls_cap,ls_indirizzo,ls_localita, ls_provincia, ls_partita_iva, &
       ls_flag_tipo_fornitore, ls_valore, ls_cod_fiscale,ls_flag_certiticato,ls_flag_approvato, ls_flag_strategico, ls_flag_terzista, &
		 ls_flag_omologato
datetime ldt_data

if g_mb.messagebox("OMNIA","Procedo con l'importazione ?",Question!,YesNo!,2) = 2 then return

li_File = FileOpen(st_2.text, LineMode!) 
if li_file < 1 then
	g_mb.messagebox("OMNIA","Errore in lettura del file selezionato")
	return
end if

ls_separatore = '","'
ll_cont = 0
ll_inseriti = 0
ll_aggiornati = 0
ldt_data = datetime(today(),00:00:00)
ls_flag_certiticato = "N"
ls_flag_approvato = "N"
ls_flag_strategico = "N"
ls_flag_terzista = "N"
ls_flag_omologato = "N"

do while true
	ll_ret = fileread(li_file, ls_str)
	if ll_ret = -100 then exit
	if ll_ret < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura file di importazione; elaborazione interrotta",stopsign!)
		fileclose(li_file)
		return
	end if
	ll_cont ++
	
	st_1.text = string(ll_cont) + " - " + ls_str
	
	lb_exit = true
	ll_campo = 0
	setnull(ls_cod_fornitore)
	setnull(ls_rag_soc_1)
	setnull(ls_cap)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_provincia)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_valore)
	
	do
		ll_pos = pos(ls_str, ls_separatore)
		if ll_pos > 0 then
			ll_campo ++
			
			ls_valore = mid(ls_str, 2, ll_pos - 2)
			
			choose case ll_campo
				case 1		//codice fornitore
					ls_cod_fornitore = ls_valore
				case 2		// rag sociale
					ls_rag_soc_1 = ls_valore
				case 3		// cap
					ls_cap = ls_valore
				case 4		// indirizzo
					ls_indirizzo = ls_valore
				case 5		// localita
					ls_localita = ls_valore
				case 6		// provincia
					ls_provincia = ls_valore
				case 7		// partita iva
					ls_partita_iva = ls_valore
				case 8		// tipo cliente (I=Italia  E=Estero  P=Persona fisica  C=Cee)
					ls_flag_tipo_fornitore = ls_valore
					if ls_flag_tipo_fornitore = "I" then ls_flag_tipo_fornitore = "S"
					if ls_flag_tipo_fornitore = "P" then ls_flag_tipo_fornitore = "F"
					if len(trim(ls_flag_tipo_fornitore)) < 1 or isnull(ls_flag_tipo_fornitore) then ls_flag_tipo_fornitore = "P"
			end choose
			
			ls_str = mid(ls_str, ll_pos + 2)
			
		else
			lb_exit = false
		end if
		
	loop while lb_exit
	
	// insert oppure update
	if not isnull(ls_cod_fornitore) and len(trim(ls_cod_fornitore)) > 0 then
		// decido se carica partita iva o codcie fiscale
		if ls_flag_tipo_fornitore = "S" or ls_flag_tipo_fornitore = "F" then
			setnull(ls_cod_fiscale)
		else
			ls_cod_fiscale = ls_partita_iva
			setnull(ls_partita_iva)
		end if
		
		ls_cap = left(ls_cap, 5)
		
		li_test = 0
		
		select count(cod_fornitore)
		into   :li_test
		from anag_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_fornitore = :ls_cod_fornitore;
				
		if li_test > 0 then		// eseguo update sui dati
			ll_aggiornati ++
			update anag_fornitori
			set    rag_soc_1 = :ls_rag_soc_1,
					 indirizzo = :ls_indirizzo,
					 cap = :ls_cap,
					 localita = :ls_localita,
					 provincia = :ls_provincia,
					 partita_iva = :ls_partita_iva,
					 cod_fiscale = :ls_cod_fiscale,
					 flag_tipo_fornitore = :ls_flag_tipo_fornitore,
					 data_modifica = :ldt_data
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_fornitore = :ls_cod_fornitore;
			
			if sqlca.sqlcode <> 0 then
				if g_mb.messagebox("OMNIA","Errore in aggiornamento fornitore " + ls_cod_fornitore + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
					rollback;
					fileclose(li_file)
					return
				end if
			else
				commit;
			end if
			
		else							// eseguo insert fornitore
			ll_inseriti ++
			
			INSERT INTO "ANAG_FORNITORI"  
					( "COD_AZIENDA",   
					  "COD_FORNITORE",   
					  "RAG_SOC_1",   
					  "RAG_SOC_2",   
					  "INDIRIZZO",   
					  "LOCALITA",   
					  "FRAZIONE",   
					  "CAP",   
					  "PROVINCIA",   
					  "TELEFONO",   
					  "FAX",   
					  "TELEX",   
					  "COD_FISCALE",   
					  "PARTITA_IVA",   
					  "RIF_INTERNO",   
					  "FLAG_TIPO_FORNITORE",   
					  "COD_CONTO",   
					  "COD_IVA",   
					  "NUM_PROT_ESENZIONE_IVA",   
					  "DATA_ESENZIONE_IVA",   
					  "FLAG_SOSPENSIONE_IVA",   
					  "COD_PAGAMENTO",   
					  "GIORNO_FISSO_SCADENZA",   
					  "MESE_ESCLUSIONE_1",   
					  "MESE_ESCLUSIONE_2",   
					  "DATA_SOSTITUZIONE_1",   
					  "DATA_SOSTITUZIONE_2",   
					  "SCONTO",   
					  "COD_TIPO_LISTINO_PRODOTTO",   
					  "FIDO",   
					  "COD_BANCA_CLIEN_FOR",   
					  "CONTO_CORRENTE",   
					  "COD_LINGUA",   
					  "COD_NAZIONE",   
					  "COD_AREA",   
					  "COD_ZONA",   
					  "COD_VALUTA",   
					  "COD_CATEGORIA",   
					  "COD_IMBALLO",   
					  "COD_PORTO",   
					  "COD_RESA",   
					  "COD_MEZZO",   
					  "COD_VETTORE",   
					  "COD_INOLTRO",   
					  "COD_DEPOSITO",   
					  "FLAG_CERTIFICATO",   
					  "FLAG_APPROVATO",   
					  "FLAG_STRATEGICO",   
					  "FLAG_TERZISTA",   
					  "NUM_CONTRATTO",   
					  "DATA_CONTRATTO",   
					  "FLAG_VER_ISPETTIVA",   
					  "DATA_VER_ISPETTIVA",   
					  "NOTE_VER_ISPETTIVA",   
					  "FLAG_OMOLOGATO",   
					  "FLAG_PROCEDURE_SPECIALI",   
					  "PESO_VAL_SERVIZIO",   
					  "PESO_VAL_QUALITA",   
					  "LIMITE_TOLLERANZA",   
					  "FLAG_BLOCCO",   
					  "DATA_BLOCCO",   
					  "FLAG_TIPO_CERTIFICAZIONE",   
					  "COD_PIANO_CAMPIONAMENTO",   
					  "CASELLA_MAIL",   
					  "NOME_DOC_QUALIFICAZIONE",   
					  "SITO_INTERNET",   
					  "COD_CLIENTE2",   
					  "COD_FORNITORE2",   
					  "COD_BANCA",   
					  "DATA_CREAZIONE",   
					  "DATA_MODIFICA" )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_fornitore,   
					  :ls_rag_soc_1,   
					  null,   
					  :ls_indirizzo,   
					  :ls_localita,   
					  null,   
					  :ls_cap,   
					  :ls_provincia,   
					  null,   
					  null,   
					  null,   
					  :ls_cod_fiscale,   
					  :ls_partita_iva,   
					  null,   
					  :ls_flag_tipo_fornitore,   
					  null,   
					  null,   
					  null,   
					  null,   
					  'N',   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  0,   
					  null,   
					  0,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  '01',   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  '001',   
					  :ls_flag_certiticato,   
					  :ls_flag_approvato,   
					  :ls_flag_strategico,   
					  :ls_flag_terzista,   
					  null,   
					  null,   
					  'N',   
					  null,   
					  null,   
					  :ls_flag_omologato,   
					  'N',   
					  0,   
					  0,   
					  0,   
					  'N',   
					  null,   
					  'N',   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  :ldt_data,   
					  :ldt_data )  ;
			
			if sqlca.sqlcode <> 0 then
				if g_mb.messagebox("OMNIA","Errore in inserimento nuovo fornitore " + ls_cod_fornitore + " " + ls_rag_soc_1 + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
					rollback;
					fileclose(li_file)
					return
				end if
			else
				commit;
			end if
		end if
		
	end if	
	
loop
fileclose(li_file)

commit;

g_mb.messagebox("OMNIA", "Importazione eseguita con successo !!!~r~nImportati " +string(ll_cont) + " fornitori ~r~nInseriti " +string(ll_inseriti) + " fornitori nuovi~r~nAggiornati " +string(ll_aggiornati) + " fornitori ~r~nPREMERE INVIO.")


end event

