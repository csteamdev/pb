﻿$PBExportHeader$w_importa_nazioni.srw
forward
global type w_importa_nazioni from w_cs_xx_principale
end type
type cbx_1 from checkbox within w_importa_nazioni
end type
type mle_log from multilineedit within w_importa_nazioni
end type
type cb_1 from commandbutton within w_importa_nazioni
end type
type st_1 from statictext within w_importa_nazioni
end type
end forward

global type w_importa_nazioni from w_cs_xx_principale
integer width = 2121
integer height = 1540
string title = "Importa Nazioni"
cbx_1 cbx_1
mle_log mle_log
cb_1 cb_1
st_1 st_1
end type
global w_importa_nazioni w_importa_nazioni

forward prototypes
public subroutine wf_log (string as_message)
public subroutine wf_help ()
end prototypes

public subroutine wf_log (string as_message);
end subroutine

public subroutine wf_help ();// HELP
mle_log.text = "Formato del file excel"
mle_log.text += "~r~nColonna A = codice nazione"
mle_log.text += "~r~nColonna B = descrizione nazione"
end subroutine

on w_importa_nazioni.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.mle_log=create mle_log
this.cb_1=create cb_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.mle_log
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.st_1
end on

on w_importa_nazioni.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.mle_log)
destroy(this.cb_1)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;

wf_help()
end event

type cbx_1 from checkbox within w_importa_nazioni
integer x = 69
integer y = 120
integer width = 613
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "File con intestazione"
boolean checked = true
end type

type mle_log from multilineedit within w_importa_nazioni
integer x = 23
integer y = 380
integer width = 2034
integer height = 1020
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_importa_nazioni
integer x = 69
integer y = 220
integer width = 1006
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Seleziona File"
end type

event clicked;string ls_docpath, ls_docname[], ls_dektop, ls_cod_nazione, ls_des_nazione
int li_row, li_test
uo_excel luo_excel

mle_log.clear()
ls_dektop = guo_functions.uof_get_user_desktop_folder( )

if GetFileOpenName("Select File", ls_docpath, ls_docname[], "Excel","Excel,*.xls;*.xlsx,", ls_dektop) > 0 then
	
	luo_excel = create uo_excel
	luo_excel.uof_open(ls_docpath, false, true)
	
	if(cbx_1.checked) then
		li_row = 2
	else
		li_row = 1
	end if
	
	mle_log.text = "Inzio importazione dalla riga " + string(li_row)
	
	do while true
		
		ls_cod_nazione = luo_excel.uof_read_string(li_row, 1)
		ls_des_nazione = luo_excel.uof_read_string(li_row, 2)
		
		if isnull(ls_cod_nazione) or ls_cod_nazione = "" then
			exit
		end if
		
		// esiste?
		select count(*)
		into :li_test
		from tab_nazioni
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_nazione = :ls_cod_nazione;
				 
		if sqlca.sqlcode = 100 or ( sqlca.sqlcode = 0 and li_test = 0)  then
			// INSERISCO
			INSERT INTO tab_nazioni 
			VALUES (:s_cs_xx.cod_azienda, :ls_cod_nazione, :ls_des_nazione, 'N', NULL);
			
			mle_log.text += "~r~nInserita nuova nazione  " + ls_cod_nazione + " - " + ls_des_nazione

		elseif sqlca.sqlcode = 0 and li_test = 1 then
			UPDATE tab_nazioni
			SET des_nazione = :ls_des_nazione
			WHERE cod_azienda = :s_cs_xx.cod_azienda and 
					  cod_nazione = :ls_cod_nazione;
					  
			mle_log.text += "~r~nAggiornata la nazione nazione  " + ls_cod_nazione + " - " + ls_des_nazione
		else
			mle_log.text += "~r~nErrore in riga " + string(li_row) + " - " + sqlca.sqlerrtext
		end if
		
		li_row++
		
	loop

	//luo_excel.uof_close( )
	destroy luo_excel
	mle_log.text += "~r~nImportazione completata."
	
else
	wf_help()
	
end if
end event

type st_1 from statictext within w_importa_nazioni
integer x = 23
integer y = 20
integer width = 1001
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Inserisce o aggiorna la tabella nazioni"
boolean focusrectangle = false
end type

