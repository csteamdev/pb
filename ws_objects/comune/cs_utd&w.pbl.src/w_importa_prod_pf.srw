﻿$PBExportHeader$w_importa_prod_pf.srw
$PBExportComments$Selezione Prodotto Finito di cui Elaborare Produzione
forward
global type w_importa_prod_pf from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_importa_prod_pf
end type
type dw_lista_prodotti_finiti from uo_cs_xx_dw within w_importa_prod_pf
end type
end forward

global type w_importa_prod_pf from w_cs_xx_principale
int Width=2419
int Height=745
boolean TitleBar=true
string Title="Importazione Dati Produzione"
cb_2 cb_2
dw_lista_prodotti_finiti dw_lista_prodotti_finiti
end type
global w_importa_prod_pf w_importa_prod_pf

type variables
string is_cod_prodotto_finito
end variables

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_PO_LoadDDDW_DW(dw_lista_prodotti_finiti,"cod_prodotto_padre",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_lista_prodotti_finiti.set_dw_options(sqlca,pcca.null_object,c_nonew+c_nodelete+c_nomodify+c_disableCC+c_disableCCinsert,c_default)

iuo_dw_main = dw_lista_prodotti_finiti



end on

on w_importa_prod_pf.create
int iCurrent
call w_cs_xx_principale::create
this.cb_2=create cb_2
this.dw_lista_prodotti_finiti=create dw_lista_prodotti_finiti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_2
this.Control[iCurrent+2]=dw_lista_prodotti_finiti
end on

on w_importa_prod_pf.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_2)
destroy(this.dw_lista_prodotti_finiti)
end on

type cb_2 from commandbutton within w_importa_prod_pf
int X=2012
int Y=541
int Width=343
int Height=81
int TabOrder=20
string Text="Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;s_cs_xx.parametri.parametro_s_1 = dw_lista_prodotti_finiti.getitemstring(dw_lista_prodotti_finiti.getrow(), "cod_prodotto_padre")
if (not isvalid(w_importa_produzione)) and ( len(s_cs_xx.parametri.parametro_s_1) > 0) then
	window_open(w_importa_produzione, 1)
end if
end on

type dw_lista_prodotti_finiti from uo_cs_xx_dw within w_importa_prod_pf
int X=23
int Y=21
int Width=2332
int Height=501
int TabOrder=10
string DataObject="d_lista_prod_finiti_import"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

