﻿$PBExportHeader$w_importa_prodotti_rcgroup.srw
forward
global type w_importa_prodotti_rcgroup from window
end type
type cb_2 from commandbutton within w_importa_prodotti_rcgroup
end type
type st_2 from statictext within w_importa_prodotti_rcgroup
end type
type st_1 from statictext within w_importa_prodotti_rcgroup
end type
type cbx_1 from checkbox within w_importa_prodotti_rcgroup
end type
type cb_1 from commandbutton within w_importa_prodotti_rcgroup
end type
end forward

global type w_importa_prodotti_rcgroup from window
integer width = 1851
integer height = 912
boolean titlebar = true
string title = "RCGROUP - Importazione Prodotti"
boolean controlmenu = true
long backcolor = 12632256
cb_2 cb_2
st_2 st_2
st_1 st_1
cbx_1 cbx_1
cb_1 cb_1
end type
global w_importa_prodotti_rcgroup w_importa_prodotti_rcgroup

on w_importa_prodotti_rcgroup.create
this.cb_2=create cb_2
this.st_2=create st_2
this.st_1=create st_1
this.cbx_1=create cbx_1
this.cb_1=create cb_1
this.Control[]={this.cb_2,&
this.st_2,&
this.st_1,&
this.cbx_1,&
this.cb_1}
end on

on w_importa_prodotti_rcgroup.destroy
destroy(this.cb_2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cbx_1)
destroy(this.cb_1)
end on

type cb_2 from commandbutton within w_importa_prodotti_rcgroup
integer x = 1664
integer y = 436
integer width = 128
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;long ll_ret
string docname, named

ll_ret = GetFileOpenName("Select File", docname, named, "TXT", "Text Files (*.TXT),*.TXT,")

if ll_ret = 1 then
	st_2.text = docname
else
	st_2.text = ""
end if
end event

type st_2 from statictext within w_importa_prodotti_rcgroup
integer x = 27
integer y = 436
integer width = 1623
integer height = 96
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

type st_1 from statictext within w_importa_prodotti_rcgroup
integer x = 23
integer y = 676
integer width = 1769
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_importa_prodotti_rcgroup
integer x = 407
integer y = 256
integer width = 937
integer height = 96
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Aggiorna Prodotti Esistenti"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_importa_prodotti_rcgroup
integer x = 709
integer y = 116
integer width = 379
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;boolean lb_exit,lb_codice_lungo
integer li_File, li_fill
long ll_ret, ll_pos, ll_campo,ll_cont, ll_inseriti, ll_aggiornati, ll_errore, ll_saltati
string ls_str, ls_separatore, ls_valore, ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_iva,ls_des_prodotto_2,&
       ls_des_prodotto_3,ls_nota, ls_test,ls_cod_comodo
datetime ldt_data

if g_mb.messagebox("OMNIA","Procedo con l'importazione ?",Question!,YesNo!,2) = 2 then return

li_File = FileOpen(st_2.text, LineMode!) 
if li_file < 1 then
	g_mb.messagebox("OMNIA","Errore in lettura del file selezionato")
	return
end if

ls_separatore = '","'
ll_cont = 0
ll_inseriti = 0
ll_aggiornati = 0
ldt_data = datetime(today(),00:00:00)

do while true
	ll_ret = fileread(li_file, ls_str)
	if ll_ret = -100 then exit
	if ll_ret < 0 then
		g_mb.messagebox("OMNIA","Errore in lettura file di importazione; elaborazione interrotta",stopsign!)
		fileclose(li_file)
		return
	end if
	ll_cont ++
	
	st_1.text = string(ll_cont) + " - " + ls_str
	
	lb_exit = true
	ll_campo = 0
	setnull(ls_cod_prodotto)
	setnull(ls_des_prodotto)
	setnull(ls_cod_misura)
	setnull(ls_cod_iva)
	setnull(ls_valore)
	
	do
		
		ll_campo ++
		
		ll_pos = pos(ls_str, ls_separatore)
		
		if ll_pos < 1 or isnull(ll_pos) then
			ll_pos = len(ls_str)
			lb_exit = false
		end if
		
		ls_valore = mid(ls_str, 2, ll_pos - 2)
		
		choose case ll_campo
			case 1		//codice prodotto
				ls_cod_prodotto = ls_valore
			case 2		// des prodotto
				ls_des_prodotto = ls_valore
			case 3		// des_prodotto_2 (cod_comodo_2)
				ls_des_prodotto_2 = ls_valore
			case 4		// des_prodotto_3
				ls_des_prodotto_3 = ls_valore
			case 5		// cod_misura
				ls_cod_misura = ls_valore
			case 6		// cod iva
				ls_cod_iva = ls_valore
		end choose
		
		ls_str = mid(ls_str, ll_pos + 2)
		
	loop while lb_exit

	ls_cod_comodo = ls_cod_prodotto
	
	if len(ls_cod_prodotto) > 15 then		// prodotti con codice maggiore di 15 ????
		lb_codice_lungo = TRUE
	else
		lb_codice_lungo = FALSE
	end if
	
	if isnull(ls_cod_iva) then ls_cod_iva = '20'
	
	if isnull(ls_cod_misura) then ls_cod_misura = 'NR'

	INSERT INTO tab_misure  
			( cod_azienda,   
			  cod_misura,   
			  des_misura,   
			  fat_conversione,   
			  flag_blocco,   
			  flag_prezzo )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_misura,   
			  :ls_cod_misura,   
			  1,   
			  'N',   
			  'N' )  ;


	INSERT INTO tab_ive  
			( cod_azienda,   
			  cod_iva,   
			  des_iva,   
			  aliquota,   
			  flag_calcolo,   
			  flag_blocco,   
			  perc_imponibile_soggetto,   
			  cod_iva_differenziato )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_iva,   
			  :ls_cod_iva,   
			  0,   
			  'S',   
			  'N',   
			  0,   
			  null )  ;
	
	// insert oppure update
	if not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
		
		if lb_codice_lungo then
			select cod_prodotto
			into   :ls_cod_prodotto
			from  anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comodo = :ls_cod_comodo;
		else
			select cod_prodotto
			into   :ls_cod_prodotto
			from  anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
		end if			
					
		if sqlca.sqlcode = 0 then		// il prodotto esiste, procedo con updateeseguo update sui dati
			
			if cbx_1.checked = false then
				ll_saltati ++
				continue
			end if
			
			ll_aggiornati ++
			update anag_prodotti
			set    des_prodotto = :ls_des_prodotto,
			       cod_misura_mag = :ls_cod_misura,
			       cod_misura_acq = :ls_cod_misura,
			       cod_misura_ven = :ls_cod_misura,
					 cod_deposito = '001',
					 cod_comodo_2 = :ls_des_prodotto_2,
					 cod_comodo = :ls_cod_comodo
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				if g_mb.messagebox("OMNIA","Errore in aggiornamento prodotto " + ls_cod_prodotto + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
					rollback;
					fileclose(li_file)
					return
				end if
			end if
			
			if isnull(ls_des_prodotto_2) then ls_des_prodotto_2 = ""
			if isnull(ls_des_prodotto_3) then ls_des_prodotto_3 = ""
			
			ls_nota = ""
			if len(trim(ls_des_prodotto_2)) > 0  then
				ls_nota = ls_des_prodotto_2
			end if
			
			if len(ls_nota) > 0 and len(trim(ls_des_prodotto_3)) > 0 then
				ls_nota += "~r~n"
			end if
			
			if len(trim(ls_des_prodotto_3)) > 0  then
				ls_nota += ls_des_prodotto_3
			end if
			
			select nota_prodotto
			into   :ls_test
			from anag_prodotti_note
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_prodotto = :ls_cod_prodotto and
					cod_nota_prodotto = 'DES';
					
			if sqlca.sqlcode <> 0 then		// la nota non esiste faccio insert
				
				INSERT INTO anag_prodotti_note  
						( cod_azienda,   
						  cod_prodotto,   
						  cod_nota_prodotto,   
						  nota_prodotto )  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ls_cod_prodotto,   
						  'DES',   
						  :ls_nota )  ;
				
				if sqlca.sqlcode <> 0 then
					ll_errore ++
					if g_mb.messagebox("OMNIA","Errore in inserimento nota nel nuovo prodotto " + ls_cod_prodotto + " " + ls_des_prodotto + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
						rollback;
						fileclose(li_file)
						return
					end if
				else
					commit;
				end if
			else			// la nota esiste già, eseguo update
				update anag_prodotti_note  
				set nota_prodotto = :ls_nota
				where cod_azienda = :s_cs_xx.cod_azienda and
				      cod_prodotto = :ls_cod_prodotto and
						cod_nota_prodotto = 'DES';
				
				if sqlca.sqlcode <> 0 then
					ll_errore ++
					if g_mb.messagebox("OMNIA","Errore in inserimento nota nel nuovo prodotto " + ls_cod_prodotto + " " + ls_des_prodotto + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
						rollback;
						fileclose(li_file)
						return
					end if
				else
					commit;
				end if
				
			end if
			
			
		else							// eseguo insert prodotto
			
			// se avevo un codice > 15 caratteri, devo produrre un cod_prodotto univoco
			if lb_codice_lungo then
				li_fill = 1
				do
					// produco un codice alternativo
					ls_cod_prodotto = left(ls_cod_prodotto, 15 - li_fill) + fill("_", li_fill)
					// vedo se esiste
					select cod_prodotto
					into :ls_cod_prodotto
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto;
					
					li_fill ++
				loop until sqlca.sqlcode <> 0
			end if
			
			
			ll_inseriti ++
			
			INSERT INTO anag_prodotti  
					( cod_azienda,   
					  cod_prodotto,   
					  cod_comodo,   
					  cod_barre,   
					  des_prodotto,   
					  data_creazione,   
					  cod_deposito,   
					  cod_ubicazione,   
					  flag_decimali,   
					  cod_cat_mer,   
					  cod_responsabile,   
					  cod_misura_mag,   
					  cod_misura_peso,   
					  peso_lordo,   
					  peso_netto,   
					  cod_misura_vol,   
					  volume,   
					  pezzi_collo,   
					  flag_classe_abc,   
					  flag_sotto_scorta,   
					  flag_lifo,   
					  saldo_quan_anno_prec,   
					  saldo_quan_inizio_anno,   
					  val_inizio_anno,   
					  saldo_quan_ultima_chiusura,   
					  prog_quan_entrata,   
					  val_quan_entrata,   
					  prog_quan_uscita,   
					  val_quan_uscita,   
					  prog_quan_acquistata,   
					  val_quan_acquistata,   
					  prog_quan_venduta,   
					  val_quan_venduta,   
					  quan_ordinata,   
					  quan_impegnata,   
					  quan_assegnata,   
					  quan_in_spedizione,   
					  quan_anticipi,   
					  costo_standard,   
					  costo_ultimo,   
					  lotto_economico,   
					  livello_riordino,   
					  scorta_minima,   
					  scorta_massima,   
					  indice_rotazione,   
					  consumo_medio,   
					  cod_prodotto_alt,   
					  cod_misura_acq,   
					  fat_conversione_acq,   
					  cod_fornitore,   
					  cod_gruppo_acq,   
					  prezzo_acquisto,   
					  sconto_acquisto,   
					  quan_minima,   
					  inc_ordine,   
					  quan_massima,   
					  tempo_app,   
					  cod_misura_ven,   
					  fat_conversione_ven,   
					  cod_gruppo_ven,   
					  cod_iva,   
					  prezzo_vendita,   
					  sconto,   
					  provvigione,   
					  flag_blocco,   
					  data_blocco,   
					  cod_livello_prod_1,   
					  cod_livello_prod_2,   
					  cod_livello_prod_3,   
					  cod_livello_prod_4,   
					  cod_livello_prod_5,   
					  cod_nomenclatura,   
					  cod_imballo,   
					  flag_articolo_fiscale,   
					  flag_cauzione,   
					  cod_cauzione,   
					  cod_tipo_politica_riordino,   
					  cod_misura_lead_time,   
					  lead_time,   
					  cod_formula,   
					  cod_reparto,   
					  flag_materia_prima,   
					  flag_escludibile,   
					  cod_rifiuto,   
					  flag_stato_rifiuto,   
					  cod_comodo_2,   
					  cod_tipo_costo_mp )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_prodotto,   
					  :ls_cod_comodo,   
					  null,   
					  :ls_des_prodotto,   
					  :ldt_data,   
					  '001',   
					  null,   
					  'S',   
					  null,   
					  null,   
					  :ls_cod_misura,   
					  null,   
					  0,   
					  0,   
					  null,   
					  0,   
					  0,   
					  'A',   
					  'S',   
					  'S',   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  null,   
					  :ls_cod_misura,   
					  1,   
					  null,   
					  null,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  :ls_cod_misura,   
					  1,   
					  null,   
					  :ls_cod_iva,   
					  0,   
					  0,   
					  0,   
					  'N',   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  'S',   
					  'N',   
					  null,   
					  null,   
					  null,   
					  0,   
					  null,   
					  null,   
					  'N',   
					  'N',   
					  null,   
					  'S',   
					  :ls_des_prodotto_2,   
					  null )  ;
			
			if sqlca.sqlcode <> 0 then
				ll_errore ++
				if g_mb.messagebox("OMNIA","Errore in inserimento nuovo prodotto " + ls_cod_prodotto + " " + ls_des_prodotto + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
					rollback;
					fileclose(li_file)
					return
				end if
			end if
			
			if isnull(ls_des_prodotto_2) then ls_des_prodotto_2 = ""
			if isnull(ls_des_prodotto_3) then ls_des_prodotto_3 = ""
			
			ls_nota = ""
			if len(trim(ls_des_prodotto_2)) > 0  then
				ls_nota = ls_des_prodotto_2
			end if
			
			if len(ls_nota) > 0 and len(trim(ls_des_prodotto_3)) > 0 then
				ls_nota += "~r~n"
			end if
			
			if len(trim(ls_des_prodotto_3)) > 0  then
				ls_nota += ls_des_prodotto_3
			end if
				
			
			INSERT INTO anag_prodotti_note  
					( cod_azienda,   
					  cod_prodotto,   
					  cod_nota_prodotto,   
					  nota_prodotto )  
			VALUES ( :s_cs_xx.cod_azienda,   
					  :ls_cod_prodotto,   
					  'DES',   
					  :ls_nota )  ;
			
			if sqlca.sqlcode <> 0 then
				ll_errore ++
				if g_mb.messagebox("OMNIA","Errore in inserimento nota nel nuovo prodotto " + ls_cod_prodotto + " " + ls_des_prodotto + ". Dettaglio Errore:~r~n" + sqlca.sqlerrtext + "~r~nPROSEGUO ?",Question!,YesNo!,1) = 2 then
					rollback;
					fileclose(li_file)
					return
				end if
			else
				commit;
			end if
			
		end if
		
	end if	
	
loop
fileclose(li_file)

commit;

g_mb.messagebox("OMNIA", "Importazione eseguita con successo !!!~r~nImportati " +string(ll_cont) + " prodotti ~r~nInseriti " +string(ll_inseriti) + " prodotti nuovi~r~nAggiornati " +string(ll_aggiornati) + " prodotti~r~nIgnorati " +string(ll_saltati) + " prodotti~r~nPREMERE INVIO.")
end event

