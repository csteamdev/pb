﻿$PBExportHeader$w_importa_produzione.srw
$PBExportComments$Impostazione Dati Produzione (Semilav Rep + Reparto + Mat Prime ecc.. )
forward
global type w_importa_produzione from w_cs_xx_principale
end type
type dw_lista_materie_prima from uo_cs_xx_dw within w_importa_produzione
end type
type dw_lista_fasi_lavorazione from uo_cs_xx_dw within w_importa_produzione
end type
type cb_genera from commandbutton within w_importa_produzione
end type
type cb_2 from commandbutton within w_importa_produzione
end type
type cb_1 from commandbutton within w_importa_produzione
end type
type st_1 from statictext within w_importa_produzione
end type
type sle_2 from singlelineedit within w_importa_produzione
end type
type st_2 from statictext within w_importa_produzione
end type
type sle_4 from singlelineedit within w_importa_produzione
end type
type r_1 from rectangle within w_importa_produzione
end type
type st_3 from statictext within w_importa_produzione
end type
type sle_1 from singlelineedit within w_importa_produzione
end type
type ddlb_1 from dropdownlistbox within w_importa_produzione
end type
type ddlb_2 from dropdownlistbox within w_importa_produzione
end type
type ddlb_lavorazione from dropdownlistbox within w_importa_produzione
end type
type gb_1 from groupbox within w_importa_produzione
end type
type st_4 from statictext within w_importa_produzione
end type
type st_50 from statictext within w_importa_produzione
end type
type st_51 from statictext within w_importa_produzione
end type
type em_prodotto_semilavorato from editmask within w_importa_produzione
end type
type cb_3 from commandbutton within w_importa_produzione
end type
type cb_carica_lavorazioni from commandbutton within w_importa_produzione
end type
end forward

global type w_importa_produzione from w_cs_xx_principale
int Width=2775
int Height=1865
boolean TitleBar=true
string Title="Importazione Dati Produzione"
dw_lista_materie_prima dw_lista_materie_prima
dw_lista_fasi_lavorazione dw_lista_fasi_lavorazione
cb_genera cb_genera
cb_2 cb_2
cb_1 cb_1
st_1 st_1
sle_2 sle_2
st_2 st_2
sle_4 sle_4
r_1 r_1
st_3 st_3
sle_1 sle_1
ddlb_1 ddlb_1
ddlb_2 ddlb_2
ddlb_lavorazione ddlb_lavorazione
gb_1 gb_1
st_4 st_4
st_50 st_50
st_51 st_51
em_prodotto_semilavorato em_prodotto_semilavorato
cb_3 cb_3
cb_carica_lavorazioni cb_carica_lavorazioni
end type
global w_importa_produzione w_importa_produzione

type variables
string is_cod_prodotto_finito, is_cod_semilavorato
string is_cod_reparto, is_cod_lavorazione
end variables

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_lista_fasi_lavorazione,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_lista_materie_prima,"cod_prodotto_figlio",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_PO_LoadDDDW_DW(dw_lista_materie_prima,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "tab_misure.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDLB_sort(ddlb_1, &
              SQLCA,"anag_reparti", "cod_reparto", "des_reparto", &
              "anag_reparti.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","des_reparto")

//f_PO_LoadDDLB_sort(ddlb_2, &
//              SQLCA,"anag_prodotti", "cod_prodotto", "des_prodotto", &
//              "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","cod_prodotto")

//f_PO_LoadDDLB_sort(ddlb_lavorazione, &
//              SQLCA,"tes_fasi_lavorazione", "cod_lavorazione", "des_estesa_prodotto", &
//              "tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","cod_lavorazione")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_lista_materie_prima.set_dw_key("cod_azienda")
dw_lista_materie_prima.set_dw_key("cod_prodotto_padre")
dw_lista_materie_prima.set_dw_options(sqlca,pcca.null_object,c_multiselect,c_default)

dw_lista_fasi_lavorazione.set_dw_key("cod_azienda")
dw_lista_fasi_lavorazione.set_dw_key("cod_prodotto")
dw_lista_fasi_lavorazione.set_dw_options(sqlca,pcca.null_object,c_multiselect,c_default)
//+c_disableCC+c_disableCCinsert
iuo_dw_main = pcca.null_object
is_cod_prodotto_finito = s_cs_xx.parametri.parametro_s_1


end on

on w_importa_produzione.create
int iCurrent
call w_cs_xx_principale::create
this.dw_lista_materie_prima=create dw_lista_materie_prima
this.dw_lista_fasi_lavorazione=create dw_lista_fasi_lavorazione
this.cb_genera=create cb_genera
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_1=create st_1
this.sle_2=create sle_2
this.st_2=create st_2
this.sle_4=create sle_4
this.r_1=create r_1
this.st_3=create st_3
this.sle_1=create sle_1
this.ddlb_1=create ddlb_1
this.ddlb_2=create ddlb_2
this.ddlb_lavorazione=create ddlb_lavorazione
this.gb_1=create gb_1
this.st_4=create st_4
this.st_50=create st_50
this.st_51=create st_51
this.em_prodotto_semilavorato=create em_prodotto_semilavorato
this.cb_3=create cb_3
this.cb_carica_lavorazioni=create cb_carica_lavorazioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_materie_prima
this.Control[iCurrent+2]=dw_lista_fasi_lavorazione
this.Control[iCurrent+3]=cb_genera
this.Control[iCurrent+4]=cb_2
this.Control[iCurrent+5]=cb_1
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=sle_2
this.Control[iCurrent+8]=st_2
this.Control[iCurrent+9]=sle_4
this.Control[iCurrent+10]=r_1
this.Control[iCurrent+11]=st_3
this.Control[iCurrent+12]=sle_1
this.Control[iCurrent+13]=ddlb_1
this.Control[iCurrent+14]=ddlb_2
this.Control[iCurrent+15]=ddlb_lavorazione
this.Control[iCurrent+16]=gb_1
this.Control[iCurrent+17]=st_4
this.Control[iCurrent+18]=st_50
this.Control[iCurrent+19]=st_51
this.Control[iCurrent+20]=em_prodotto_semilavorato
this.Control[iCurrent+21]=cb_3
this.Control[iCurrent+22]=cb_carica_lavorazioni
end on

on w_importa_produzione.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_lista_materie_prima)
destroy(this.dw_lista_fasi_lavorazione)
destroy(this.cb_genera)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.sle_2)
destroy(this.st_2)
destroy(this.sle_4)
destroy(this.r_1)
destroy(this.st_3)
destroy(this.sle_1)
destroy(this.ddlb_1)
destroy(this.ddlb_2)
destroy(this.ddlb_lavorazione)
destroy(this.gb_1)
destroy(this.st_4)
destroy(this.st_50)
destroy(this.st_51)
destroy(this.em_prodotto_semilavorato)
destroy(this.cb_3)
destroy(this.cb_carica_lavorazioni)
end on

type dw_lista_materie_prima from uo_cs_xx_dw within w_importa_produzione
int X=23
int Y=421
int Width=2675
int Height=501
int TabOrder=80
string DataObject="d_lista_materie_prima"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto_padre")) THEN
      SetItem(l_Idx, "cod_prodotto_padre", is_cod_prodotto_finito)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type dw_lista_fasi_lavorazione from uo_cs_xx_dw within w_importa_produzione
int X=23
int Y=941
int Width=2675
int Height=501
int TabOrder=90
string DataObject="d_lista_fasi_lavorazione"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_prodotto")) THEN
      SetItem(l_Idx, "cod_prodotto", is_cod_prodotto_finito)
   END IF
NEXT

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_prodotto_finito)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF




end on

type cb_genera from commandbutton within w_importa_produzione
int X=2332
int Y=1461
int Width=366
int Height=81
int TabOrder=120
string Text="Genera "
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long li_num_1, li_num_2, li_selected_1[], li_selected_2[], li_risposta, ll_num_sequenza
string ls_descrizione_fase, ls_des_fasi[], ls_mat_prime[], ls_str, ls_cod_mp, ls_mis_mp, ls_cp
integer li_i, li_cont, li_i2, li_num_mp, li_cont_mp
double ld_qta_mp

is_cod_lavorazione = trim(sle_1.text)
if len(is_cod_semilavorato) = 0 then
   g_mb.messagebox("Generazione Produzione","Il codice del semilavorato di reparto è mancante",StopSign!)
   return
end if

if len(is_cod_reparto) = 0 then
   g_mb.messagebox("Generazione Produzione","Il codice del reparto è mancante",StopSign!)
   return
end if

if len(is_cod_lavorazione) = 0 then
   g_mb.messagebox("Generazione Produzione","Il codice lavorazione è mancante",StopSign!)
   return
end if

if len(is_cod_semilavorato) = 0 then
   g_mb.messagebox("Generazione Produzione","Il codice semilavorato è mancante",StopSign!)
   return
else
     SELECT anag_prodotti.cod_prodotto  
     INTO   :ls_str
     FROM   anag_prodotti  
     WHERE  ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( anag_prodotti.cod_prodotto = :is_cod_semilavorato )   ;
     if sqlca.sqlcode = 100 then
        g_mb.messagebox("Generazione Produzione","Il codice semilavorato non esiste &
                    in anagrafica prodotti",StopSign!)
        return
     end if
     if sqlca.sqlcode = -1 then
        g_mb.messagebox("Generazione Produzione - Errore DB",sqlca.sqlerrtext,StopSign!)
        return
     end if
end if



// -------------------  generazione righe su tabella det_fasi_lavorazione ------------------
parent.SetMicroHelp("Generazione records su tabella dettaglio fasi lavorazione")

   //------------------ inizio con il leggere le righe selezionate sulle dw ----------------
   li_Num_1 = dw_lista_fasi_lavorazione.Get_Selected_Rows(li_Selected_1[])
   li_Num_2 = dw_lista_materie_prima.Get_Selected_Rows(li_Selected_2[])
   
   //------------------ leggo descrizione completa fase ------------------------------------
   ls_descrizione_fase = ""
   for li_i = 1 to li_Num_1
      ls_descrizione_fase = ls_descrizione_fase + " " + trim(dw_lista_fasi_lavorazione.getitemstring(li_Selected_1[li_i], "des_fase"))
   next

   //------------------ carico materie prime presenti in righe fasi -------------------------
   li_cont = 1
   for li_i = 1 to li_Num_1
      if len(trim(dw_lista_fasi_lavorazione.getitemstring(li_Selected_1[li_i], "cod_materia_prima"))) > 0 then
         ls_mat_prime[li_cont] = trim(dw_lista_fasi_lavorazione.getitemstring(li_Selected_1[li_i], "cod_materia_prima"))
         SELECT anag_prodotti.cod_prodotto  
         INTO   :ls_cp
         FROM   anag_prodotti  
         WHERE ( anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
               ( anag_prodotti.cod_prodotto = :ls_mat_prime[li_cont] )   ;
         if sqlca.sqlcode <> 0 then
            g_mb.messagebox("Fasi Lavorazione", "Verificare che i codici materie prime in fasi lav. siano corretti", information!)
            ROLLBACK;
            return
         end if
         li_cont = li_cont + 1
      end if
   next 

   //------------------ carico materie prime selezionate in distinta ------------------------
   li_cont = upperbound(ls_mat_prime) + 1
   li_num_mp = upperbound(ls_mat_prime)
   for li_i2 = 1 to li_Num_2
       ls_str = trim(dw_lista_materie_prima.getitemstring(li_Selected_2[li_i2], "cod_prodotto_figlio"))
       for li_i = 1 to li_num_mp
          if ls_mat_prime[li_i] = ls_str then goto salta
       next
       if (len(ls_str) > 0) and not( (mid(ls_str,1,1) = "L") and  (len(ls_str)=3) ) then
          ls_mat_prime[li_cont] = trim(dw_lista_materie_prima.getitemstring(li_Selected_2[li_i2], "cod_prodotto_figlio"))
          li_cont = li_cont + 1
       end if
salta:
   next 

   // -------------------  generazione testata fasi lavorazione -------------------------------
   parent.SetMicroHelp("Generazione records su tabella testata fasi lavorazione")
     INSERT INTO tes_fasi_lavorazione  
            ( cod_azienda,   
              cod_prodotto,   
              cod_reparto,   
              cod_lavorazione,   
              cod_cat_attrezzature,   
              des_estesa_prodotto,   
              num_priorita,   
              cod_centro_costo,   
              quan_prodotta,   
              quan_non_conforme,   
              tempo_attrezzaggio,   
              tempo_attrezzaggio_commessa,   
              tempo_lavorazione,
				  cod_cat_attrezzature_2,
				  tempo_risorsa_umana)  
     VALUES ( :s_cs_xx.cod_azienda,   
              :is_cod_semilavorato,   
              :is_cod_reparto,   
              :is_cod_lavorazione,   
              null,   
              :ls_descrizione_fase,   
              1,   
              null,   
              0,   
              0,   
              0,   
              0,   
              1,
				  null,
				  1)  ;

    if sqlca.sqlcode <> 0 then
       g_mb.messagebox("TES_FASI Errore nel DB", sqlca.sqlerrtext, stopsign!)
       g_mb.messagebox("TES_FASI Errore nel DB", "La creazione del dettaglio fasi potrebbe non essere completo: verificare", information!)
       ROLLBACK;
       return
    end if
   //------------------- genero righe in det_fasi_lavorazione -------------------------------

   for li_i = 1 to li_num_1
       ls_des_fasi[li_i] = ""
   next 

   li_cont_mp = 1
   for li_i = 1 to li_Num_1
      ls_des_fasi[li_cont_mp] = ls_des_fasi[li_cont_mp] + " " + trim(dw_lista_fasi_lavorazione. &
                                getitemstring(li_Selected_1[li_i], "des_fase"))
      if li_i >= li_num_1 then exit
      if len(trim(dw_lista_fasi_lavorazione.getitemstring( &
             li_Selected_1[li_i] + 1, "cod_materia_prima"))) > 0 then
         li_cont_mp = li_cont_mp + 1
      end if
   next

   li_cont = 10
   li_i2 = 1
   if upperbound(ls_mat_prime) > 0 then li_i2 = upperbound(ls_mat_prime)
   
   for li_i = 1 to li_i2
      INSERT INTO det_fasi_lavorazione  
                  (cod_azienda,   
                  cod_prodotto,   
                  cod_reparto,   
                  cod_lavorazione,   
                  prog_riga_fasi_lavorazione,   
                  cod_prodotto_materia_prima,   
                  descrizione,   
                  disegno,   
                  progressivo_documento )  
      VALUES      (:s_cs_xx.cod_azienda,   
                  :is_cod_semilavorato,   
                  :is_cod_reparto,   
                  :is_cod_lavorazione,   
                  :li_cont,   
                  :ls_mat_prime[li_i],   
                  :ls_des_fasi[li_i],   
                  null,   
                  null )  ;
      if sqlca.sqlcode <> 0 then
         g_mb.messagebox("Errore nel DB", sqlca.sqlerrtext, stopsign!)
         g_mb.messagebox("Errore nel DB", "La creazione del dettaglio fasi potrebbe non essere completo: verificare", information!)
         ROLLBACK;
         return
      else
         li_cont = li_cont + 10
         ls_descrizione_fase = ""
      end if
   next


//  --------------------- generazione prodotti in distinta base -------------------------------

SELECT MAX(distinta.num_sequenza)
INTO   :ll_num_sequenza
FROM   distinta
WHERE  (distinta.cod_azienda = :s_cs_xx.cod_azienda) and
       (distinta.cod_prodotto_padre = :is_cod_prodotto_finito) ;  // era cod_semilavorato
if sqlca.sqlcode <> 0 then
   ll_num_sequenza = 10
else
   if isnull(ll_num_sequenza) then
      ll_num_sequenza = 10 
   else
      ll_num_sequenza = ll_num_sequenza + 10
   end if
end if


INSERT INTO distinta  
          ( cod_azienda, cod_prodotto_padre, num_sequenza, cod_prodotto_figlio, cod_misura,   
            quan_tecnica, quan_utilizzo, fase_ciclo )  
   VALUES ( :s_cs_xx.cod_azienda, :is_cod_prodotto_finito, :ll_num_sequenza, :is_cod_semilavorato,   
            null, 1, 1, 1)  ;
if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Errore nel DB", sqlca.sqlerrtext, stopsign!)
   g_mb.messagebox("Errore nel DB", "Potrebbe non essere stato generato il semilavorato in distinta", information!)
   ROLLBACK;
   return
end if

for li_i2 = 1 to li_Num_2
   ls_cod_mp = trim(dw_lista_materie_prima.getitemstring(li_Selected_2[li_i2], "cod_prodotto_figlio"))
   ls_mis_mp = trim(dw_lista_materie_prima.getitemstring(li_Selected_2[li_i2], "cod_misura"))
   ld_qta_mp = dw_lista_materie_prima.getitemnumber(li_Selected_2[li_i2], "quan_utilizzo")
      
   SELECT MAX(distinta.num_sequenza)
   INTO   :ll_num_sequenza
   FROM   distinta
   WHERE  (distinta.cod_azienda = :s_cs_xx.cod_azienda) and
          (distinta.cod_prodotto_padre = :is_cod_semilavorato) ;  // era cod_semilavorato
   if sqlca.sqlcode <> 0 then ll_num_sequenza = 10
   if isnull(ll_num_sequenza) then
      ll_num_sequenza = 10 
   else
      ll_num_sequenza = ll_num_sequenza + 10
   end if

   INSERT INTO distinta  
          ( cod_azienda, cod_prodotto_padre, num_sequenza, cod_prodotto_figlio, cod_misura,   
           quan_tecnica, quan_utilizzo, fase_ciclo )  
   VALUES ( :s_cs_xx.cod_azienda, :is_cod_semilavorato, :ll_num_sequenza, :ls_cod_mp, :ls_mis_mp,   
            :ld_qta_mp, :ld_qta_mp, 1 )  ;
   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Errore nel DB", sqlca.sqlerrtext, stopsign!)
      g_mb.messagebox("Errore nel DB", "Materia prima "+ls_cod_mp+" non inserita in distinta", information!)
      ROLLBACK;
      return
   end if
next

COMMIT;    // consolido tutto quanto inciso nel database

setnull(ddlb_1.text)
setnull(ddlb_2.text)
setnull(sle_1.text)

if not isvalid(w_crea_tes_fasi_lav) then
	window_open(w_crea_tes_fasi_lav, 1)
end if
end event

type cb_2 from commandbutton within w_importa_produzione
int X=1281
int Y=1541
int Width=366
int Height=81
int TabOrder=110
string Text="Nuovo Semil."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;//if not(isvalid(w_prodotti)) then
//   window_open(w_prodotti, 1)
//else
//   w_prodotti.setfocus()
//end if

string ls_cod_prodotto, ls_des_prodotto, ls_str
datetime ldt_oggi


if len(sle_2.text) < 1 then
   g_mb.messagebox("Nuovo Semilavorato","Codice prodotto semilavorato mancante")
   return
end if
if len(sle_4.text) < 1 then
   g_mb.messagebox("Nuovo Semilavorato","Descrizione prodotto semilavorato mancante")
   return
end if
ls_cod_prodotto = trim(sle_2.text)
ls_des_prodotto = trim(sle_4.text)
ldt_oggi = datetime(today())

setpointer(hourglass!)

SELECT anag_prodotti.cod_prodotto  
INTO   :ls_str  
FROM   anag_prodotti  
WHERE  ( anag_prodotti.cod_azienda  = :s_cs_xx.cod_azienda ) AND  
       ( anag_prodotti.cod_prodotto = :ls_cod_prodotto ) ;

if sqlca.sqlcode = 0 then
   g_mb.messagebox("Nuovo Semilavorato", "Codice semilavorato già esistente in anagrafica prodotti")
   return
end if

INSERT INTO anag_prodotti  
         ( cod_azienda,   
           cod_prodotto,   
           cod_comodo,   
           cod_barre,   
           des_prodotto,   
           data_creazione,   
           cod_deposito,   
           cod_ubicazione,   
           flag_decimali,   
           cod_cat_mer,   
           cod_responsabile,   
           cod_misura_mag,   
           cod_misura_peso,   
           peso_lordo,   
           peso_netto,   
           cod_misura_vol,   
           volume,   
           pezzi_collo,   
           flag_classe_abc,   
           flag_sotto_scorta,   
           flag_lifo,   
           saldo_quan_anno_prec,   
           saldo_quan_inizio_anno,   
           val_inizio_anno,   
           saldo_quan_ultima_chiusura,   
           prog_quan_entrata,   
           val_quan_entrata,   
           prog_quan_uscita,   
           val_quan_uscita,   
           prog_quan_acquistata,   
           val_quan_acquistata,   
           prog_quan_venduta,   
           val_quan_venduta,   
           quan_ordinata,   
           quan_impegnata,   
           quan_assegnata,   
           quan_in_spedizione,   
           quan_anticipi,   
           costo_standard,   
           costo_ultimo,   
           lotto_economico,   
           livello_riordino,   
           scorta_minima,   
           scorta_massima,   
           indice_rotazione,   
           consumo_medio,   
           cod_prodotto_alt,   
           cod_misura_acq,   
           fat_conversione_acq,   
           cod_fornitore,   
           cod_gruppo_acq,   
           prezzo_acquisto,   
           sconto_acquisto,   
           quan_minima,   
           inc_ordine,   
           quan_massima,   
           tempo_app,   
           cod_misura_ven,   
           fat_conversione_ven,   
           cod_gruppo_ven,   
           cod_iva,   
           prezzo_vendita,   
           sconto,   
           provvigione,   
           flag_blocco,   
           data_blocco,
			  cod_livello_prod_1,
			  cod_livello_prod_2,
			  cod_livello_prod_3,
			  cod_livello_prod_4,
			  cod_livello_prod_5 )  
VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_prodotto,   
           null,   
           null,   
           :ls_des_prodotto,   
           :ldt_oggi,   
           null,   
           null,   
           'N',   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           null,   
           0,   
           1,   
           'A',   
           'N',   
           'N',   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           1,   
           1,   
           0,   
           0,   
           0,   
           0,   
           null,   
           null,   
           1,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           1,   
           null,   
           null,   
           0,   
           0,   
           0,   
           'N',   
           :ldt_oggi,
			  null,
			  null,
			  null,
			  null,
			  null )  ;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Nuovo Semilavorato",sqlca.sqlerrtext)
   ROLLBACK ;
   return
end if


COMMIT;
//ddlb_2.reset()
//f_PO_LoadDDLB_sort(ddlb_2, &
//              SQLCA,"anag_prodotti", "cod_prodotto", "des_prodotto", &
//              "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","cod_prodotto")
//ddlb_2.selectitem(ls_cod_prodotto, 1)
//
setpointer(arrow!)
end event

type cb_1 from commandbutton within w_importa_produzione
int X=2332
int Y=1561
int Width=366
int Height=81
int TabOrder=100
string Text="Elimina"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;long l_selected[]

if dw_lista_fasi_lavorazione.get_selected_rows(l_selected[]) > 0 then
        dw_lista_fasi_lavorazione.triggerevent("pcd_delete")
end if         
if dw_lista_materie_prima.get_selected_rows(l_selected[]) > 0 then
        dw_lista_materie_prima.triggerevent("pcd_delete")
end if         
w_importa_produzione.triggerevent("pcd_save")
end on

type st_1 from statictext within w_importa_produzione
int X=138
int Y=1541
int Width=247
int Height=73
boolean Enabled=false
string Text="Codice:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_2 from singlelineedit within w_importa_produzione
int X=389
int Y=1541
int Width=709
int Height=81
int TabOrder=130
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_importa_produzione
int X=46
int Y=1641
int Width=334
int Height=73
boolean Enabled=false
string Text="Descrizione:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_4 from singlelineedit within w_importa_produzione
int X=389
int Y=1641
int Width=1258
int Height=81
int TabOrder=140
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type r_1 from rectangle within w_importa_produzione
int X=23
int Y=1501
int Width=1646
int Height=241
boolean Enabled=false
int LineThickness=5
long FillColor=12632256
end type

type st_3 from statictext within w_importa_produzione
int X=46
int Y=1461
int Width=531
int Height=73
boolean Enabled=false
string Text="Nuovo Semilavorato"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_importa_produzione
int X=46
int Y=281
int Width=298
int Height=81
int TabOrder=70
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
int Limit=6
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_1 from dropdownlistbox within w_importa_produzione
int X=343
int Y=81
int Width=823
int Height=1001
int TabOrder=20
boolean Sorted=false
boolean VScrollBar=true
boolean AllowEdit=true
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;integer li_len, li_i
string  ls_str, ls_cod_reparto

ls_cod_reparto = ""
li_len = len(ddlb_1.text)
if li_len > 0 then
   ls_str = ddlb_1.text
   for li_i = 1 to li_len
      if mid(ls_str, li_i, 1) = "-" then  exit
      ls_cod_reparto = ls_cod_reparto + mid(ls_str, li_i, 1)
   next
end if
is_cod_reparto = trim(ls_cod_reparto)
end on

type ddlb_2 from dropdownlistbox within w_importa_produzione
int X=709
int Y=181
int Width=1898
int Height=1001
int TabOrder=60
boolean Sorted=false
boolean AutoHScroll=true
boolean VScrollBar=true
boolean AllowEdit=true
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;integer li_len, li_i
string  ls_str, ls_cod_prodotto

ls_cod_prodotto = ""
li_len = len(ddlb_2.text)
if li_len > 0 then
   ls_str = ddlb_2.text
   for li_i = 1 to li_len
      if mid(ls_str, li_i, 1) = "-" then  exit
      ls_cod_prodotto = ls_cod_prodotto + mid(ls_str, li_i, 1)
   next
end if
is_cod_semilavorato = trim(ls_cod_prodotto)
em_prodotto_semilavorato.text = trim(is_cod_semilavorato)
end event

type ddlb_lavorazione from dropdownlistbox within w_importa_produzione
int X=366
int Y=281
int Width=2241
int Height=1001
int TabOrder=40
boolean Sorted=false
boolean AutoHScroll=true
boolean VScrollBar=true
boolean AllowEdit=true
long TextColor=33554432
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on selectionchanged;integer li_len, li_i
string  ls_str, ls_cod_lavorazione

ls_cod_lavorazione = ""
li_len = len(ddlb_lavorazione.text)
if li_len > 0 then
   ls_str = ddlb_lavorazione.text
   for li_i = 1 to li_len
      if mid(ls_str, li_i, 1) = "-" then  exit
      ls_cod_lavorazione = ls_cod_lavorazione + mid(ls_str, li_i, 1)
   next
end if
sle_1.text = trim(ls_cod_lavorazione)
end on

type gb_1 from groupbox within w_importa_produzione
int X=23
int Y=21
int Width=2693
int Height=381
int TabOrder=10
string Text="Reparto - Prodotto Semilavorato - Lavorazione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_importa_produzione
int X=69
int Y=181
int Width=609
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Prodotto Semilavorato:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_50 from statictext within w_importa_produzione
int X=69
int Y=81
int Width=243
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Reparto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_51 from statictext within w_importa_produzione
int X=1189
int Y=81
int Width=801
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Codice Prodotto Semilavorato:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_prodotto_semilavorato from editmask within w_importa_produzione
int X=2012
int Y=81
int Width=663
int Height=81
int TabOrder=30
boolean BringToTop=true
Alignment Alignment=Center!
BorderStyle BorderStyle=StyleLowered!
string Mask="xxxxxxxxxxxxxxx"
MaskDataType MaskDataType=StringMask!
string DisplayData=""
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;is_cod_semilavorato = trim(em_prodotto_semilavorato.text)
end event

type cb_3 from commandbutton within w_importa_produzione
int X=2606
int Y=181
int Width=69
int Height=89
int TabOrder=50
boolean BringToTop=true
string Text="C"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ddlb_2.reset()
f_PO_LoadDDLB_sort(ddlb_2, &
              SQLCA,"anag_prodotti", "cod_prodotto", "des_prodotto", &
              "anag_prodotti.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","cod_prodotto")
ddlb_2.selectitem(trim(sle_2.text), 1)

end event

type cb_carica_lavorazioni from commandbutton within w_importa_produzione
event clicked pbm_bnclicked
int X=2606
int Y=281
int Width=69
int Height=89
int TabOrder=51
boolean BringToTop=true
string Text="C"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;ddlb_lavorazione.reset()
f_PO_LoadDDLB_sort(ddlb_lavorazione, &
              SQLCA,"tes_fasi_lavorazione", "cod_lavorazione", "des_estesa_prodotto", &
              "tes_fasi_lavorazione.cod_azienda = '" + s_cs_xx.cod_azienda + "'", "","cod_lavorazione")


end event

