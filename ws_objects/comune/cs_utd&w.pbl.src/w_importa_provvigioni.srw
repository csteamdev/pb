﻿$PBExportHeader$w_importa_provvigioni.srw
forward
global type w_importa_provvigioni from w_cs_xx_principale
end type
type dw from datawindow within w_importa_provvigioni
end type
type cb_1 from commandbutton within w_importa_provvigioni
end type
end forward

global type w_importa_provvigioni from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3122
integer height = 480
string title = "Importazione provvigioni"
boolean maxbox = false
boolean resizable = false
dw dw
cb_1 cb_1
end type
global w_importa_provvigioni w_importa_provvigioni

event pc_setwindow;call super::pc_setwindow;dw.settransobject(sqlca)

dw.retrieve(s_cs_xx.cod_azienda)
end event

on w_importa_provvigioni.create
int iCurrent
call super::create
this.dw=create dw
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw
this.Control[iCurrent+2]=this.cb_1
end on

on w_importa_provvigioni.destroy
call super::destroy
destroy(this.dw)
destroy(this.cb_1)
end on

type dw from datawindow within w_importa_provvigioni
integer x = 23
integer y = 20
integer width = 3045
integer height = 72
integer taborder = 10
string dataobject = "d_importa_provvigioni"
boolean livescroll = true
end type

type cb_1 from commandbutton within w_importa_provvigioni
integer x = 23
integer y = 180
integer width = 3040
integer height = 180
integer taborder = 20
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa dati dalla tabella LISTINI_PRODOTTI alla tabella PROVVIGIONI"
end type

event clicked;long     ll_i, ll_prog

string   ls_prodotto, ls_listino, ls_valuta

decimal  ld_q1, ld_p1, ld_q2, ld_p2, ld_q3, ld_p3, ld_q4, ld_p4, ld_q5, ld_p5

datetime ldt_data


enabled = false

setpointer(hourglass!)

dw.height = 132

for ll_i = 1 to dw.rowcount()
	
	dw.scrolltorow(ll_i)
	
	ls_prodotto = dw.getitemstring(ll_i,"cod_prodotto")
	
	ls_listino = dw.getitemstring(ll_i,"cod_tipo_listino_prodotto")
	
	ls_valuta = dw.getitemstring(ll_i,"cod_valuta")
	
	ldt_data = dw.getitemdatetime(ll_i,"data_inizio_val")
	
	ld_q1 = dw.getitemnumber(ll_i,"quantita_1")
	
	ld_p1 = dw.getitemnumber(ll_i,"provvigione_1")
	
	ld_q2 = dw.getitemnumber(ll_i,"quantita_2")
	
	ld_p2 = dw.getitemnumber(ll_i,"provvigione_2")
	
	ld_q3 = dw.getitemnumber(ll_i,"quantita_3")
	
	ld_p3 = dw.getitemnumber(ll_i,"provvigione_3")
	
	ld_q4 = dw.getitemnumber(ll_i,"quantita_4")
	
	ld_p4 = dw.getitemnumber(ll_i,"provvigione_4")
	
	ld_q5 = dw.getitemnumber(ll_i,"quantita_5")
	
	ld_p5 = dw.getitemnumber(ll_i,"provvigione_5")
	
	select max(progressivo)
	into   :ll_prog
	from   provvigioni
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_listino_prodotto = :ls_listino and
			 cod_valuta = :ls_valuta and
			 data_inizio_val = :ldt_data;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura massimo progressivo da provvigioni: " + sqlca.sqlerrtext)
		exit
	end if
	
	if isnull(ll_prog) then
		ll_prog = 0
	end if
	
	ll_prog ++
	
	insert
	into   provvigioni
	       (cod_azienda,
			 cod_tipo_listino_prodotto,
			 cod_valuta,
			 data_inizio_val,
			 progressivo,
			 cod_prodotto,
			 flag_tipo_scaglioni,
			 scaglione_1,
			 scaglione_2,
			 scaglione_3,
			 scaglione_4,
			 scaglione_5,
			 variazione_1,
			 variazione_2,
			 variazione_3,
			 variazione_4,
			 variazione_5)
	values (:s_cs_xx.cod_azienda,
	       :ls_listino,
			 :ls_valuta,
			 :ldt_data,
			 :ll_prog,
			 :ls_prodotto,
			 'Q',
			 :ld_q1,
			 :ld_q2,
			 :ld_q3,
			 :ld_q4,
			 :ld_q5,
			 :ld_p1,
			 :ld_p2,
			 :ld_p3,
			 :ld_p4,
			 :ld_p5);
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella insert di provvigioni: " + sqlca.sqlerrtext)
		rollback;
		exit
	end if
	
	commit;
	
next

dw.height = 72

setpointer(arrow!)

enabled = true
end event

