﻿$PBExportHeader$w_importa_reparti_viropa.srw
$PBExportComments$Importazione Clienti Progetto Tenda
forward
global type w_importa_reparti_viropa from w_cs_xx_principale
end type
type st_1 from statictext within w_importa_reparti_viropa
end type
type cb_importa from uo_cb_ok within w_importa_reparti_viropa
end type
type cb_1 from uo_cb_close within w_importa_reparti_viropa
end type
type dw_importa_clienti_orig from uo_cs_xx_dw within w_importa_reparti_viropa
end type
end forward

global type w_importa_reparti_viropa from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3685
integer height = 1968
string title = "Importazione clienti"
st_1 st_1
cb_importa cb_importa
cb_1 cb_1
dw_importa_clienti_orig dw_importa_clienti_orig
end type
global w_importa_reparti_viropa w_importa_reparti_viropa

type variables
transaction i_sqlorig
end variables

forward prototypes
public function long wf_importa ()
end prototypes

public function long wf_importa ();long   ll_conta, ll_i, ll_lungo, ll_sconto, ll_car, ll_cli, al_riga
string ls_cod_cliente, ls_rag_soc_1, ls_provincia, ls_partita_iva, ls_rag_soc_2, ls_cellulare,&
       ls_cap, ls_indirizzo, ls_localita, ls_cod_pagamento, ls_cod_iva, ls_telefono, ls_telex, &
		 ls_fax, ls_rif_interno, ls_cod_fiscale, ls_controllo,ls_cod_cin,&
		 ls_cod_valuta, ls_flag_tipo_cliente, ls_cod_zona, ls_cod_agente, ls_sconto, &
		 ls_flag_spese_incasso, ls_flag_iva_sospesa, ls_cod_banca_cli_for,  &
		 ls_des_banca_appoggio, ls_piazza, ls_conto_corrente, ls_cod_porto, &
		 ls_cod_spedizione, ls_cod_vettore, ls_cod_giro, ls_str, &
		 ls_resp_acquisti, ls_resp_vendite, ls_dest_1, ls_dest_2, ls_note_1, ls_note_2, ls_cin,&
		 ls_note_fisse_video,ls_cod_giro_inv, ls_cod_abi,ls_cod_cab, ls_des_abi, &
		 ls_email, ls_sito_internet, ls_cod_nazione,ls_flag_blocco, ls_cod_area,ls_note_fisse,ls_cod_categoria,ls_spese_trasporto,ls_cod_gruppo_sconto
dec{4} ld_spese_trasporto

		 

select parametri_azienda.stringa
into   :ls_cod_valuta
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Parametro LIR non trovato.", &
              exclamation!, ok!)
   return ll_conta
end if

for al_riga = 1 to dw_importa_clienti_orig.rowcount()

	ls_cod_cliente = dw_importa_clienti_orig.getitemstring(al_riga, "cod_cliente")
	ls_rag_soc_1   = dw_importa_clienti_orig.getitemstring(al_riga, "rag_soc_1")
	ls_rag_soc_2   = dw_importa_clienti_orig.getitemstring(al_riga, "rag_soc_2")
	ls_indirizzo   = dw_importa_clienti_orig.getitemstring(al_riga, "indirizzo")
	ls_localita   = dw_importa_clienti_orig.getitemstring(al_riga, "localita")
	ls_cap         = dw_importa_clienti_orig.getitemstring(al_riga, "cap")
	ls_provincia   = dw_importa_clienti_orig.getitemstring(al_riga, "provincia")
	ls_partita_iva = dw_importa_clienti_orig.getitemstring(al_riga, "partita_iva")
	ls_flag_tipo_cliente = dw_importa_clienti_orig.getitemstring(al_riga, "flag_tipo_cliente")
	ls_localita      = dw_importa_clienti_orig.getitemstring(al_riga, "localita")
	ls_cod_pagamento = dw_importa_clienti_orig.getitemstring(al_riga, "cod_pagamento")
	setnull(ls_cod_iva)
	ls_telefono = dw_importa_clienti_orig.getitemstring(al_riga, "telefono")
	ls_cellulare = dw_importa_clienti_orig.getitemstring(al_riga, "cellulare")
	setnull(ls_telex)
	ls_fax      = dw_importa_clienti_orig.getitemstring(al_riga, "fax")
	setnull(ls_rif_interno)
	ls_cod_area    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_area")
	ls_cod_zona    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_zona")
	ls_cod_agente  = dw_importa_clienti_orig.getitemstring(al_riga, "cod_agente")
	setnull(ls_sconto)
	ls_des_banca_appoggio = dw_importa_clienti_orig.getitemstring(al_riga, "des_banca_appoggio")
	ls_cod_abi    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_abi")
	ls_cod_cab    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_cab")
	ls_cod_cin    = dw_importa_clienti_orig.getitemstring(al_riga, "cod_cin")
	ls_note_fisse_video = dw_importa_clienti_orig.getitemstring(al_riga, "note_fisse_video")
	ls_conto_corrente = dw_importa_clienti_orig.getitemstring(al_riga, "conto_corrente")
	setnull(ls_cod_porto)
	setnull(ls_cod_spedizione)
	setnull(ls_cod_vettore)
	ls_cod_giro = dw_importa_clienti_orig.getitemstring(al_riga, "cod_giro")
	ls_cod_giro_inv = dw_importa_clienti_orig.getitemstring(al_riga, "cod_giro_inv")
	ls_telefono = dw_importa_clienti_orig.getitemstring(al_riga, "telefono")
	ls_fax      = dw_importa_clienti_orig.getitemstring(al_riga, "fax")
	ls_telex    = dw_importa_clienti_orig.getitemstring(al_riga, "email")
	ls_flag_iva_sospesa = "N"
	setnull(ls_cod_banca_cli_for)
	ls_cod_fiscale = dw_importa_clienti_orig.getitemstring(al_riga, "cod_fiscale")
	ls_email = dw_importa_clienti_orig.getitemstring(al_riga, "email")
	ls_sito_internet = dw_importa_clienti_orig.getitemstring(al_riga, "sito_internet")
	ls_cod_nazione = dw_importa_clienti_orig.getitemstring(al_riga, "cod_nazione")
	ls_flag_blocco = dw_importa_clienti_orig.getitemstring(al_riga, "flag_blocco")
	ls_cod_categoria = dw_importa_clienti_orig.getitemstring(al_riga, "cod_categoria")
	ls_note_fisse = dw_importa_clienti_orig.getitemstring(al_riga, "note_fisse_video")
	ls_spese_trasporto = dw_importa_clienti_orig.getitemstring(al_riga, "spese_trasporto")
	ld_spese_trasporto = dec(ls_spese_trasporto)
	ls_cod_gruppo_sconto = dw_importa_clienti_orig.getitemstring(al_riga, "cod_gruppo")
	ll_conta = 0
	
	if len(ls_cod_gruppo_sconto) > 1 then setnull(ls_cod_gruppo_sconto)
	
	
	if not isnull(ls_cod_zona) or len(ls_cod_zona) > 0 then
		INSERT INTO tab_zone  
				( cod_azienda,   
				  cod_zona,   
				  des_zona,   
				  flag_blocco,   
				  data_blocco )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_zona,   
				  'NUOVA ZONA IMPORTATA',   
				  'N',   
				  null )  ;
	end if
	
	if not isnull(ls_cod_agente) or len(ls_cod_agente) > 0 then
	  INSERT INTO anag_agenti  
				( cod_azienda,   
				  cod_agente,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  prov_agente,   
				  flag_blocco,   
				  data_blocco )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_agente,   
				  'NUOVO AGENTE IMPORTATO',   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  'N',   
				  null )  ;
	end if
	
	
	if not isnull(ls_cod_porto) or len(ls_cod_porto) > 0 then  
		INSERT INTO tab_porti  
				( cod_azienda,   
				  cod_porto,   
				  des_porto,   
				  flag_blocco,   
				  data_blocco )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_porto,   
				  'NUOVO PORTO IMPORTATO',   
				  'N',   
				  null )  ;
	end if	
		
	if not isnull(ls_cod_spedizione) or len(ls_cod_spedizione) > 0 then
		INSERT INTO tab_mezzi  
				( cod_azienda,   
				  cod_mezzo,   
				  des_mezzo,   
				  flag_blocco,   
				  data_blocco )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_spedizione,   
				  'NUOVO MEZZO SPEDIZIONE',   
				  'N',   
				  null )  ;
	end if
		
	if not isnull(ls_cod_vettore) or len(ls_cod_vettore) > 0 then
		INSERT INTO anag_vettori  
				( cod_azienda,   
				  cod_vettore,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  telefono,   
				  fax,   
				  telex,   
				  flag_blocco,   
				  data_blocco )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_vettore,   
				  'NUOVO VETTORE IMPORTATO',   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  'N',   
				  null )  ;
	end if
	
	
	st_1.text = ls_cod_cliente + " - " + ls_rag_soc_1
	

	if (not isnull(ls_cod_abi) and len(ls_cod_abi) > 0 ) and ( not isnull(ls_cod_cab) and len(ls_cod_cab) > 0 ) then
		
		if not isnull(ls_cod_abi) or len(ls_cod_abi) > 0 then
			INSERT INTO tab_abi  
					( cod_abi,   
					  des_abi )  
			VALUES ( :ls_cod_abi,   
					  'NUOVO ABI IMPORTATO' )  ;
		end if
	
		if not isnull(ls_cod_cab) or len(ls_cod_cab) > 0 then
			INSERT INTO tab_abicab  
					( cod_abi,   
					  cod_cab,   
					  indirizzo,   
					  localita,   
					  frazione,   
					  cap,   
					  provincia,   
					  telefono,   
					  fax,   
					  telex,   
					  agenzia )  
			VALUES ( :ls_cod_abi,   
					  :ls_cod_cab,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null,   
					  null )  ;
		end if 
	
		select des_banca
		into   :ls_str
		from   anag_banche_clien_for
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_abi = :ls_cod_abi and
				 cod_cab = :ls_cod_cab;
				 
		if sqlca.sqlcode = 100 then
			
			ll_car = asc("A")
			ll_cli = long(left(ls_cod_cliente, 2))
			ll_car = ll_car + ll_cli
			
			ls_cod_banca_cli_for = char(ll_car) + right(ls_cod_cliente, 2)
			
			select des_abi
			into   :ls_des_abi
			from   tab_abi
			where  cod_abi = :ls_cod_abi;
			
			
			INSERT INTO anag_banche_clien_for  
					( cod_azienda,   
					  cod_banca_clien_for,   
					  des_banca,   
					  cod_abi,   
					  cod_cab )  
			values (:s_cs_xx.cod_azienda,
					  :ls_cod_banca_cli_for,
					  :ls_des_abi,
					  :ls_cod_abi,
					  :ls_cod_cab);
	
		end if
	end if
	
	
	
	ls_partita_iva = trim(ls_partita_iva)
	ls_cod_fiscale = trim(ls_cod_fiscale)
	
	INSERT INTO anag_clienti  
			( cod_azienda,   
			  cod_cliente,   
			  rag_soc_1,   
			  rag_soc_2,   
			  indirizzo,   
			  localita,   
			  frazione,   
			  cap,   
			  provincia,   
			  telefono,   
			  fax,   
			  telex,   
			  cod_fiscale,   
			  partita_iva,   
			  rif_interno,   
			  flag_tipo_cliente,   
			  cod_capoconto,   
			  cod_conto,   
			  cod_iva,   
			  num_prot_esenzione_iva,   
			  data_esenzione_iva,   
			  flag_sospensione_iva,   
			  cod_pagamento,   
			  giorno_fisso_scadenza,   
			  mese_esclusione_1,   
			  mese_esclusione_2,   
			  data_sostituzione_1,   
			  data_sostituzione_2,   
			  sconto,   
			  cod_tipo_listino_prodotto,   
			  fido,   
			  flag_fuori_fido,   
			  cod_banca_clien_for,   
			  conto_corrente,   
			  cod_lingua,   
			  cod_nazione,   
			  cod_area,   
			  cod_zona,   
			  cod_valuta,   
			  cod_categoria,   
			  cod_agente_1,   
			  cod_agente_2,   
			  cod_imballo,   
			  cod_porto,   
			  cod_resa,   
			  cod_mezzo,   
			  cod_vettore,   
			  cod_inoltro,   
			  cod_deposito,   
			  flag_riep_boll,   
			  flag_riep_fatt,   
			  flag_blocco,   
			  data_blocco,   
			  casella_mail,   
			  flag_raggruppo_iva_doc,
			  sito_internet,
			  cod_gruppo_sconto)  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_cliente,   
			  :ls_rag_soc_1,   
			  :ls_rag_soc_2,   
			  :ls_indirizzo,   
			  :ls_localita,   
			  null,   
			  :ls_cap,   
			  :ls_provincia,   
			  :ls_telefono,   
			  :ls_fax,   
			  :ls_cellulare,   
			  :ls_cod_fiscale,   
			  :ls_partita_iva,   
			  :ls_resp_acquisti,   
			  :ls_flag_tipo_cliente,   
			  null,   
			  null,   
			  :ls_cod_iva,   
			  null,   
			  null,   
			  :ls_flag_iva_sospesa,   
			  :ls_cod_pagamento,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  null,   
			  :ll_sconto,   
			  'N',   
			  :ls_cod_banca_cli_for,   
			  :ls_conto_corrente,   
			  null,   
			  :ls_cod_nazione,   
			  :ls_cod_area,   
			  :ls_cod_zona,   
			  :ls_cod_valuta,   
			  :ls_cod_categoria,   
			  :ls_cod_agente,   
			  null,   
			  null,   
			  :ls_cod_porto,   
			  null,   
			  :ls_cod_spedizione,   
			  :ls_cod_vettore,   
			  null,   
			  '001',   
			  'S',   
			  'S',   
			  :ls_flag_blocco,   
			  null,   
			  :ls_email,   
			  'N',
			  :ls_sito_internet,
			  :ls_cod_gruppo_sconto)  ;
			  
	if sqlca.sqlcode <> 0 then
		messagebox("","ERRORE INSERT CLIENTI")
		rollback;
		return -1
	end if

	if not isnull(ls_cod_giro) and len(ls_cod_giro) > 0 then
	  INSERT INTO tes_giri_consegne  
				( cod_azienda,   
				  cod_giro_consegna,   
				  des_giro_consegna,   
				  note,   
				  flag_blocco,   
				  data_blocco )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_giro,   
				  null,   
				  null,   
				  'N',   
				  null )  ;
				  
	  INSERT INTO det_giri_consegne  
				( cod_azienda,   
				  cod_giro_consegna,   
				  cod_cliente,   
				  prog_giro_consegna,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_giro,   
				  :ls_cod_cliente,   
				  1,   
				  :ls_indirizzo,   
				  :ls_localita,   
				  null,   
				  :ls_cap,   
				  :ls_provincia )  ;
		if sqlca.sqlcode <> 0 then
			messagebox("","ERRORE INSERT det_giri_consegne")
			rollback;
			return -1
		end if
	end if
	
	
	if not isnull(ls_cod_giro_inv) and len(ls_cod_giro_inv) > 0 then
	  INSERT INTO tes_giri_consegne  
				( cod_azienda,   
				  cod_giro_consegna,   
				  des_giro_consegna,   
				  note,   
				  flag_blocco,   
				  data_blocco )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_giro,   
				  null,   
				  null,   
				  'N',   
				  null )  ;
				  
	  INSERT INTO det_giri_consegne  
				( cod_azienda,   
				  cod_giro_consegna,   
				  cod_cliente,   
				  prog_giro_consegna,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :ls_cod_giro_inv,   
				  :ls_cod_cliente,   
				  1,   
				  :ls_indirizzo,   
				  :ls_localita,   
				  null,   
				  :ls_cap,   
				  :ls_provincia )  ;
		if sqlca.sqlcode <> 0 then
			messagebox("","ERRORE INSERT det_giri_consegne")
			rollback;
			return -1
		end if
	end if
	
	if len(ls_note_fisse) > 0 and not isnull(ls_note_fisse) then
		  INSERT INTO tab_note_fisse  
         ( cod_azienda,   
           cod_nota_fissa,   
           des_nota_fissa,   
           nota_fissa,   
           note_esterne,   
           cod_cliente,   
           cod_fornitore,   
           data_inizio,   
           data_fine,   
           flag_trattativa_ven,   
           flag_offerta_ven,   
           flag_ordine_ven,   
           flag_bolla_ven,   
           flag_fattura_ven,   
           flag_offerta_acq,   
           flag_ordine_acq,   
           flag_piede_testata,   
           flag_blocco,   
           flag_uso_gen_doc,   
           flag_bolla_acq,   
           flag_fattura_acq,   
           flag_fattura_proforma,   
           cod_tipo_fat_ven,   
           flag_usa_stampa_doc,   
           flag_listino_vendita )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ls_cod_cliente,   
           'NOTA CLIENTE' + :ls_cod_cliente,   
           :ls_note_fisse,   
           null,   
           :ls_cod_cliente,   
           null,   
           '20070101',   
           '20991231',   
           'S',   
           'S',   
           'S',   
           'S',   
           'S',   
           'N',   
           'N',   
           'V',   
           'N',   
           'N',   
           'N',   
           'N',   
           'N',   
           null,   
           'N',   
           'N' )  ;

		if sqlca.sqlcode <> 0 then
			messagebox("","ERRORE INSERT tab_note_fisse")
			rollback;
			return -1
		end if
	end if

next						

return ll_conta

commit; 

return 0
end function

event pc_setwindow;call super::pc_setwindow;string ls_dsn

ls_dsn = "clienti"

i_sqlorig = CREATE transaction

i_sqlorig.servername = ""
i_sqlorig.logid = ""
i_sqlorig.logpass = ""
i_sqlorig.dbms = "Odbc"
i_sqlorig.database = ""
i_sqlorig.userid = ""
i_sqlorig.dbpass = ""
i_sqlorig.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(i_sqlorig, TRUE)

set_w_options(c_noenablepopup)

dw_importa_clienti_orig.settransobject(i_sqlorig)

dw_importa_clienti_orig.retrieve()


end event

on w_importa_reparti_viropa.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_importa=create cb_importa
this.cb_1=create cb_1
this.dw_importa_clienti_orig=create dw_importa_clienti_orig
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_importa
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_importa_clienti_orig
end on

on w_importa_reparti_viropa.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_importa)
destroy(this.cb_1)
destroy(this.dw_importa_clienti_orig)
end on

event close;call super::close;commit;
end event

type st_1 from statictext within w_importa_reparti_viropa
integer x = 23
integer y = 1732
integer width = 2702
integer height = 100
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_importa from uo_cb_ok within w_importa_reparti_viropa
event clicked pbm_bnclicked
integer x = 2862
integer y = 1736
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Importa"
end type

event clicked;call super::clicked;//wf_importa()

long ll_i
string ls_cod_prodotto, ls_cod_reparto

for ll_i = 1 to dw_importa_clienti_orig.rowcount()
	ls_cod_prodotto = dw_importa_clienti_orig.getitemstring(ll_i, "cod_prodotto")
	ls_cod_reparto = dw_importa_clienti_orig.getitemstring(ll_i, "cod_reparto")
	
	update anag_prodotti
	set cod_reparto = :ls_cod_reparto
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_prodotto = :ls_cod_prodotto;
			
	if sqlca.sqlcode <> 0 then
		messagebox("",sqlca.sqlcode)
		rollback;
		return
	end if
next

commit;
	
end event

type cb_1 from uo_cb_close within w_importa_reparti_viropa
integer x = 3250
integer y = 1736
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type dw_importa_clienti_orig from uo_cs_xx_dw within w_importa_reparti_viropa
event pcd_retrieve pbm_custom60
integer x = 27
integer y = 16
integer width = 3584
integer height = 1688
integer taborder = 30
string dataobject = "d_importa_reparti_viropa"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

