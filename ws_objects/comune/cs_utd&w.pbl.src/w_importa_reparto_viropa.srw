﻿$PBExportHeader$w_importa_reparto_viropa.srw
forward
global type w_importa_reparto_viropa from window
end type
type cb_1 from commandbutton within w_importa_reparto_viropa
end type
end forward

global type w_importa_reparto_viropa from window
integer width = 3566
integer height = 1648
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
end type
global w_importa_reparto_viropa w_importa_reparto_viropa

on w_importa_reparto_viropa.create
this.cb_1=create cb_1
this.Control[]={this.cb_1}
end on

on w_importa_reparto_viropa.destroy
destroy(this.cb_1)
end on

type cb_1 from commandbutton within w_importa_reparto_viropa
integer x = 978
integer y = 328
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;long ll_file,ll_err
string ls_cod_prodotto,ls_cod_reparto,ls_str


ll_file = fileopen("C:\Documents and Settings\enrico\Desktop\VIROPA\sfuso.txt",linemode!)
if ll_file < 1 then return

do while true
	ll_err = fileread(ll_file, ls_str)
	
	if ll_err = -100 then exit // fine
	
	if ll_err < 0 then 
		messagebox("","Errore Read file")
		rollback;
		return
	end if
	
	if ll_err = 0 then continue
	
	ls_cod_prodotto = left(ls_str, pos(ls_str, "~t")-1 )
	
	ls_cod_reparto  = mid(ls_str, pos(ls_str, "~t")+1) 
	
	update anag_prodotti
	set cod_reparto = :ls_cod_reparto
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode = -1 then
		messagebox("SEP", "Errore in update~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

commit;
end event

