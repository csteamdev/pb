﻿$PBExportHeader$w_imposta_impegnato.srw
$PBExportComments$Finestra per impostare quantità impegnata
forward
global type w_imposta_impegnato from window
end type
type cb_1 from commandbutton within w_imposta_impegnato
end type
type em_2 from editmask within w_imposta_impegnato
end type
type em_1 from editmask within w_imposta_impegnato
end type
type shl_cs from statichyperlink within w_imposta_impegnato
end type
type st_2 from statictext within w_imposta_impegnato
end type
type st_1 from statictext within w_imposta_impegnato
end type
end forward

global type w_imposta_impegnato from window
integer width = 1271
integer height = 632
boolean titlebar = true
string title = "Imposta Impegno Prodotto"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
cb_1 cb_1
em_2 em_2
em_1 em_1
shl_cs shl_cs
st_2 st_2
st_1 st_1
end type
global w_imposta_impegnato w_imposta_impegnato

on w_imposta_impegnato.create
this.cb_1=create cb_1
this.em_2=create em_2
this.em_1=create em_1
this.shl_cs=create shl_cs
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.cb_1,&
this.em_2,&
this.em_1,&
this.shl_cs,&
this.st_2,&
this.st_1}
end on

on w_imposta_impegnato.destroy
destroy(this.cb_1)
destroy(this.em_2)
destroy(this.em_1)
destroy(this.shl_cs)
destroy(this.st_2)
destroy(this.st_1)
end on

type cb_1 from commandbutton within w_imposta_impegnato
integer x = 402
integer y = 308
integer width = 466
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_cod_prodotto
decimal ld_quan_prodotto

ls_cod_prodotto = em_2.text
ld_quan_prodotto = dec(em_1.text)

update anag_prodotti
set    quan_impegnata = :ld_quan_prodotto
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento quantità impegnata. " + sqlca.sqlerrtext)
	return
else
	g_mb.messagebox("APICE","Operazione eseguita correttamente")
end if
end event

type em_2 from editmask within w_imposta_impegnato
integer x = 649
integer y = 16
integer width = 581
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_1 from editmask within w_imposta_impegnato
integer x = 649
integer y = 112
integer width = 581
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###,##0.0000"
end type

type shl_cs from statichyperlink within w_imposta_impegnato
integer x = 23
integer y = 532
integer width = 1202
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
boolean underline = true
string pointer = "HyperLink!"
long textcolor = 16711680
long backcolor = 67108864
string text = "Consulting&Software"
alignment alignment = center!
boolean focusrectangle = false
string url = "www.csteam.com"
end type

type st_2 from statictext within w_imposta_impegnato
integer x = 27
integer y = 120
integer width = 603
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Quantità da impostare:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_imposta_impegnato
integer x = 27
integer y = 24
integer width = 453
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Codice Prodotto:"
boolean focusrectangle = false
end type

