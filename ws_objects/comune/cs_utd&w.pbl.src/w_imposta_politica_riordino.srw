﻿$PBExportHeader$w_imposta_politica_riordino.srw
forward
global type w_imposta_politica_riordino from w_std_principale
end type
type st_1 from statictext within w_imposta_politica_riordino
end type
type cb_2 from commandbutton within w_imposta_politica_riordino
end type
type dw_1 from uo_std_dw within w_imposta_politica_riordino
end type
type ddlb_1 from dropdownlistbox within w_imposta_politica_riordino
end type
type cb_1 from commandbutton within w_imposta_politica_riordino
end type
type em_1 from editmask within w_imposta_politica_riordino
end type
type dw_imposta_politica_riordino from uo_std_dw within w_imposta_politica_riordino
end type
end forward

global type w_imposta_politica_riordino from w_std_principale
integer height = 1552
st_1 st_1
cb_2 cb_2
dw_1 dw_1
ddlb_1 ddlb_1
cb_1 cb_1
em_1 em_1
dw_imposta_politica_riordino dw_imposta_politica_riordino
end type
global w_imposta_politica_riordino w_imposta_politica_riordino

on w_imposta_politica_riordino.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_2=create cb_2
this.dw_1=create dw_1
this.ddlb_1=create ddlb_1
this.cb_1=create cb_1
this.em_1=create em_1
this.dw_imposta_politica_riordino=create dw_imposta_politica_riordino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_1
this.Control[iCurrent+4]=this.ddlb_1
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.em_1
this.Control[iCurrent+7]=this.dw_imposta_politica_riordino
end on

on w_imposta_politica_riordino.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.dw_1)
destroy(this.ddlb_1)
destroy(this.cb_1)
destroy(this.em_1)
destroy(this.dw_imposta_politica_riordino)
end on

event open;call super::open;dw_imposta_politica_riordino.settransobject(sqlca)

dw_imposta_politica_riordino.retrieve(s_cs_xx.cod_azienda)

dw_1.insertrow(0)

f_po_loaddddw_dw(dw_1, &
                 "cod_tipo_politica_riordino", &
                 sqlca, &
                 "tab_tipi_politiche_riordino", &
                 "cod_tipo_politica_riordino", &
                 "des_tipo_politica_riordino", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type st_1 from statictext within w_imposta_politica_riordino
integer x = 3159
integer y = 64
integer width = 343
integer height = 56
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_imposta_politica_riordino
integer x = 3163
integer y = 1344
integer width = 343
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Applica"
end type

event clicked;string ls_cod_prodotto, ls_cod_tipo_politica_riordino
long ll_rows, ll_i

ll_rows = dw_imposta_politica_riordino.rowcount()

for ll_i = 1 to ll_rows
	
	if dw_imposta_politica_riordino.getitemstring(ll_i, "N") = "S" then
		
		ls_cod_prodotto = dw_imposta_politica_riordino.getitemstring(ll_i, "cod_prodotto")
		ls_cod_tipo_politica_riordino = dw_1.getitemstring(1, "cod_tipo_politica_riordino")
		
		update anag_prodotti
		set    cod_tipo_politica_riordino = :ls_cod_tipo_politica_riordino
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("UTILITY", "Errore in update del prodotto " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	end if
	
next

commit;

end event

type dw_1 from uo_std_dw within w_imposta_politica_riordino
integer x = 18
integer y = 1316
integer width = 2235
integer height = 112
integer taborder = 20
string dataobject = "d_imposta_politica_riordino_dddw"
boolean border = false
borderstyle borderstyle = stylebox!
end type

type ddlb_1 from dropdownlistbox within w_imposta_politica_riordino
integer x = 1394
integer y = 20
integer width = 654
integer height = 324
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string item[] = {"Codice Prodotto","Descrizione Prodotto","Entrambi"}
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_imposta_politica_riordino
integer x = 2075
integer y = 16
integer width = 343
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Filtro"
end type

event clicked;string ls_stringa_ricerca, ls_str

long ll_i, ll_y

ls_stringa_ricerca = em_1.text

if not isnull(ls_stringa_ricerca) and len(ls_stringa_ricerca) > 1 then

	// ****************** aggiungo in automatico un asterisco alla fine della stringa *******
	if right(trim(ls_stringa_ricerca), 1) <> "*" and pos(ls_stringa_ricerca, "&") < 1 then
		ls_stringa_ricerca += "*"
	end if
	// ***************************************************************************************
	
	ll_i = len(ls_stringa_ricerca)
	for ll_y = 1 to ll_i
		if mid(ls_stringa_ricerca, ll_y, 1) = "*" then
			ls_stringa_ricerca = replace(ls_stringa_ricerca, ll_y, 1, "%")
		end if
	next
	
	choose case lower( left(ddlb_1.text, 3) )
			
		case "cod"
			ls_str = "cod_prodotto like '" + ls_stringa_ricerca + "' "
			
		case "des"
			ls_str = "des_prodotto like '" + ls_stringa_ricerca + "' "
			
		case "ent"
			ls_str = "(cod_prodotto like '" + ls_stringa_ricerca + "') OR (des_prodotto like '" + ls_stringa_ricerca + "') "
			
		case "else"
			ls_str = ""
	end choose
	
	ll_i = dw_imposta_politica_riordino.setfilter( ls_str )
	ll_i = dw_imposta_politica_riordino.filter()
	
end if

st_1.text = string( dw_imposta_politica_riordino.rowcount() )
end event

type em_1 from editmask within w_imposta_politica_riordino
integer x = 18
integer y = 20
integer width = 1353
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type dw_imposta_politica_riordino from uo_std_dw within w_imposta_politica_riordino
integer x = 18
integer y = 136
integer width = 3488
integer height = 1160
integer taborder = 10
string dataobject = "d_imposta_politica_riordino"
boolean vscrollbar = true
end type

