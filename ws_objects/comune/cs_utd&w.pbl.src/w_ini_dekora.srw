﻿$PBExportHeader$w_ini_dekora.srw
forward
global type w_ini_dekora from window
end type
type sle_referenza from singlelineedit within w_ini_dekora
end type
type st_1 from statictext within w_ini_dekora
end type
type cbx_commit_ogni_riga from checkbox within w_ini_dekora
end type
type cb_excel from commandbutton within w_ini_dekora
end type
type sle_filename from singlelineedit within w_ini_dekora
end type
type sle_path_inizio from singlelineedit within w_ini_dekora
end type
type sle_cod_tipo_mov from singlelineedit within w_ini_dekora
end type
type sle_deposito from singlelineedit within w_ini_dekora
end type
type em_data_movimento from editmask within w_ini_dekora
end type
type cb_salva from commandbutton within w_ini_dekora
end type
type cb_log from commandbutton within w_ini_dekora
end type
type sle_log from singlelineedit within w_ini_dekora
end type
type st_counter from statictext within w_ini_dekora
end type
type st_log from statictext within w_ini_dekora
end type
type cb_leggi from commandbutton within w_ini_dekora
end type
type st_tot from statictext within w_ini_dekora
end type
type sle_sfoglia from singlelineedit within w_ini_dekora
end type
type cb_sfoglia from commandbutton within w_ini_dekora
end type
type dw_distinta from datawindow within w_ini_dekora
end type
end forward

global type w_ini_dekora from window
integer width = 4690
integer height = 3236
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
sle_referenza sle_referenza
st_1 st_1
cbx_commit_ogni_riga cbx_commit_ogni_riga
cb_excel cb_excel
sle_filename sle_filename
sle_path_inizio sle_path_inizio
sle_cod_tipo_mov sle_cod_tipo_mov
sle_deposito sle_deposito
em_data_movimento em_data_movimento
cb_salva cb_salva
cb_log cb_log
sle_log sle_log
st_counter st_counter
st_log st_log
cb_leggi cb_leggi
st_tot st_tot
sle_sfoglia sle_sfoglia
cb_sfoglia cb_sfoglia
dw_distinta dw_distinta
end type
global w_ini_dekora w_ini_dekora

type variables
uo_excel_listini iuo_excel

long			il_max_row_excel = 300

string			is_cod_versione = "001"
string			is_des_versione = "VERSIONE CORRENTE"
string			is_flag_predefinita = "S"
string			is_cod_prodotto_finale = "TESSUTO"
string			is_path_log = ""
end variables

forward prototypes
public function integer wf_leggi_fogli (string as_path, ref string as_errore)
public function integer wf_estrai_prodotto (string as_input, ref string as_output, ref string as_errore)
public subroutine wf_log (string as_valore)
public subroutine wf_des_prodotto (long al_row, string as_cod_prodotto, ref string as_des_prodotto)
public function integer wf_leggi_lista (string as_foglio, ref string as_errore)
public function integer wf_movimento (string as_cod_tipo_mov, datetime adt_data_mov, string as_cod_prodotto, string as_cod_deposito, string as_referenza, decimal ad_quantita, decimal ad_valore_unitario, ref long al_anno_mov[], ref long al_num_mov[], ref string as_errore)
end prototypes

public function integer wf_leggi_fogli (string as_path, ref string as_errore);string				ls_sheets[], ls_foglio, ls_cod_prodotto, ls_err, ls_ok, ls_temp[]
long				ll_tot, ll_i, ll_new, ll_pos
integer			li_ret


dw_distinta.reset()

iuo_excel = create uo_excel_listini

iuo_excel.is_path_file = as_path
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_temp[])
ll_tot = upperbound(ls_temp[])

//inverti l'ordine dei fogli
ll_new = 0
for ll_i= ll_tot to 1 step -1
	ll_new += 1
	ls_sheets[ll_new] = ls_temp[ll_i]
next


if isnull(ll_tot) or ll_tot <= 0 then
	as_errore = "Non esistono fogli di lavoro nel file excel specificato!"
	st_tot.text = "Fogli Totali: 0"
	iuo_excel.uof_close( )
	destroy iuo_excel
	return -1
end if

st_tot.text = "Fogli Totali: " +string(ll_tot)

//leggo solo il primo
for ll_i = 1 to 1
	ls_foglio = ls_sheets[ll_i]
	st_counter.text = string(ll_i)
	
	
	wf_leggi_lista(ls_foglio, as_errore)
	

next

if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if


return 0
end function

public function integer wf_estrai_prodotto (string as_input, ref string as_output, ref string as_errore);datastore		lds_data
string				ls_sql, ls_temp
long				ll_tot


as_output = ""

//prova per cod_prodotto ---------------------------------------------------
//ma prima tolgo il carattere "." eventuale

ls_temp = as_input
guo_functions.uof_replace_string(ls_temp, ".", "")

ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_prodotto='"+ls_temp+"' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if



//prova per cod_comodo_2 ----------------------------------------------------------------
ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_comodo_2 like '"+as_input+"%' "+&
			"order by cod_prodotto asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if


//prova per cod_comodo ------------------------------------------------------------------
ls_sql = "select cod_prodotto "+&
			"from anag_prodotti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					  "cod_comodo like '"+as_input+"%' "+&
			"order by cod_prodotto asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot < 0 then
	return -1
end if

if ll_tot > 0 then
	//leggi il primo della lista
	as_output = lds_data.getitemstring(1, 1)
	destroy lds_data
	return 0
end if


//se sei arrivato fin qui non hai trovato nulla
as_output = "NOT FOUND"
return -2
end function

public subroutine wf_log (string as_valore);long			ll_file

if is_path_log="" or isnull(is_path_log) then return

ll_file = fileopen(is_path_log, LINEMODE!, WRITE!)
filewrite(ll_file, as_valore)
fileclose(ll_file)

return

end subroutine

public subroutine wf_des_prodotto (long al_row, string as_cod_prodotto, ref string as_des_prodotto);string				ls_des_prodotto,ls_um

if as_cod_prodotto<>"" and not isnull(as_cod_prodotto) then
			
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto;
	
	if sqlca.sqlcode = 0 then
		as_des_prodotto = ls_des_prodotto
		dw_distinta.setitem(al_row, "des_prodotto", ls_des_prodotto)
		dw_distinta.setitem(al_row, "flag_ok", "S")
		ls_um = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  as_cod_prodotto + "'", "cod_misura_mag" )
		dw_distinta.setitem(al_row, "cod_misura", ls_um)
	else
		as_des_prodotto = "ERROR or NOT FOUND"
		dw_distinta.setitem(al_row, "des_prodotto", "ERROR or NOT FOUND")
		dw_distinta.setitem(al_row, "cod_misura", "NOTFOUND")
		dw_distinta.setitem(al_row, "flag_ok", "N")
	end if
	
else
	as_des_prodotto = ""
	dw_distinta.setitem(al_row, "flag_ok", "N")
	dw_distinta.setitem(al_row, "des_prodotto", "")
end if

return
end subroutine

public function integer wf_leggi_lista (string as_foglio, ref string as_errore);long				ll_index, ll_new, ll_skip
string				ls_valore, ls_codice, ls_cod_deposito, ls_flag_ok, ls_des_prodotto, ls_tipologia, ls_referenza
dec{4}			ld_valore, ld_qta, ld_val_unitario
datetime			ldt_valore, ldt_data_mov
integer			li_ret

st_counter.text = ""
ls_tipologia = sle_cod_tipo_mov.text
ll_skip = 0

choose case ls_tipologia
	case "INL","CMA","SVE"
		
	case else
		as_errore = "Tipologia movimento non selezionata oppure non tra quelle previste (INL, CMA, SVE)"
		return -1
		
end choose


//inizia dalla riga 2 ...
ll_index = 2

//PER INL ----------------------------
//A (1)		deposito
//B (2)		articolo
//C (3)		quantità
//D (4)		valore unitario

//PER CAQ o SVE ------------------
//A (1)		data movimento
//B (2)		referenza
//C (3)		articolo
//D (4)		descrizione
//E (5)		um
//F (6)		quantita


ls_referenza = sle_referenza.text

if ls_referenza="" or isnull(ls_referenza) then
	if not g_mb.confirm("Non hai ihdicato la referenza per i movimenti. Continuare?") then
		as_errore = "Annullato dall'operatore!"
		return -1
	end if
end if


do while true
	
	ls_flag_ok = "N"
	
	//articolo ################################################################################
	if ls_tipologia="INL" then
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 2, ls_codice, ld_valore, ldt_valore, "S")
	else
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 3, ls_codice, ld_valore, ldt_valore, "S")
	end if
	
	if ls_codice<>"" and not isnull(ls_codice) then
		//controlla codice prodotto
		select des_prodotto
		into :ls_des_prodotto
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_codice;
		
		if sqlca.sqlcode=0 then
			ls_flag_ok = "S"
		else
			ls_des_prodotto = "!!!! <INESISTENTE> !!!!"
			ll_skip += 1
		end if

		//deposito ############################################################################
		if ls_tipologia="INL" then
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 1, ls_cod_deposito, ld_valore, ldt_valore, "S")
			if isnull(ls_cod_deposito) or ls_cod_deposito="" then
				if not isnull(sle_deposito.text) and sle_deposito.text<>"" then
					ls_cod_deposito=sle_deposito.text
				else
					ls_cod_deposito="100"
				end if
			end if
		else
			if not isnull(sle_deposito.text) and sle_deposito.text<>"" then
				ls_cod_deposito=sle_deposito.text
			else
				ls_cod_deposito="100"
			end if
		end if
			

		//quantita ##################################################################################
		if ls_tipologia="INL" then
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 3, ls_valore, ld_qta, ldt_valore, "D")
		else
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 6, ls_valore, ld_qta, ldt_valore, "D")
		end if
		if isnull(ld_qta) or ld_qta<0 then ld_qta = 0


		//valore unitario ############################################################################
		ld_val_unitario = 0
		if ls_tipologia="INL" then
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 4, ls_valore, ld_val_unitario, ldt_valore, "D")
			
		elseif ls_tipologia="CMA" then
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 7, ls_valore, ld_val_unitario, ldt_valore, "D")
			
		else
			//cerco di prendere il valore unitario da un movimento INL
			select val_movimento
			into :ld_val_unitario
			from mov_magazzino
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_codice and
						cod_tipo_movimento='INL';
		end if
		if isnull(ld_val_unitario) or ld_val_unitario<0 then ld_val_unitario=0
		
		
		//data movimento ############################################################################
		if ls_tipologia="INL" then
			ldt_data_mov = datetime(date(em_data_movimento.text), 00:00:00)
		else
			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 1, ls_valore, ld_valore, ldt_data_mov, "T")
		end if

		//referenza ############################################################################
//		if ls_tipologia="INL" then
//			ls_referenza = "(*) INV.XLS 1-12-14"
//		else
//			iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 2, ls_referenza, ld_valore, ldt_valore, "S")
//			if isnull(ls_referenza) or ls_referenza="" then
//				if ls_tipologia="CMA" then
//					ls_referenza ="(*) CMA"
//				else
//					ls_referenza ="(*) SVE"
//				end if
//			else
//				ls_referenza ="(*)"+ls_referenza
//			end if
//		end if
		//-------------------------------------------------------------------------------------------------------------
		//insert in datawindow
		ll_new = dw_distinta.insertrow(0)
		dw_distinta.setitem(ll_new, "cod_deposito", ls_cod_deposito)
		dw_distinta.setitem(ll_new, "cod_prodotto", ls_codice)
		dw_distinta.setitem(ll_new, "quantita", ld_qta)
		dw_distinta.setitem(ll_new, "flag_ok", ls_flag_ok)
		dw_distinta.setitem(ll_new, "des_prodotto", ls_des_prodotto)
		dw_distinta.setitem(ll_new, "costo_std", ld_val_unitario)
		dw_distinta.setitem(ll_new, "data_mov", ldt_data_mov)
		dw_distinta.setitem(ll_new, "referenza", ls_referenza)
		//-------------------------------------------------------------------------------------------------------------
		
	else
		//giunto alla fine del file
		return 0
	end if
			
	//passa a cercare su riga successiva
	ll_index = ll_index + 1
	st_counter.text = "OK: " + string(ll_new) + " -  Skipped:" + string(ll_skip)
		
loop

return 0
end function

public function integer wf_movimento (string as_cod_tipo_mov, datetime adt_data_mov, string as_cod_prodotto, string as_cod_deposito, string as_referenza, decimal ad_quantita, decimal ad_valore_unitario, ref long al_anno_mov[], ref long al_num_mov[], ref string as_errore);

datetime				ldt_data_stock[]
string					ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[]
long					ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock
uo_magazzino		luo_mag

as_referenza = left(as_referenza, 20)

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
	
ls_cod_deposito[1] = as_cod_deposito
ls_cod_ubicazione[1] = "UB0001"
ls_cod_lotto[1] = "LT0001"
ldt_data_stock[1] = datetime(date(string("01/01/2000")),00:00:00)
ll_prog_stock[1] = 10

	
luo_mag = create uo_magazzino
	
if f_crea_dest_mov_magazzino (	as_cod_tipo_mov, &
											as_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
											
	as_errore =  "Errore: f_crea_dest_mov_magazzino"
	destroy luo_mag
	return -1
end if
		
if f_verifica_dest_mov_mag (	ll_anno_reg_dest_stock, &
										ll_num_reg_dest_stock, &
										as_cod_tipo_mov, &
										as_cod_prodotto) = -1 then
									 
	as_errore = "Errore: f_verifica_dest_mov_mag"
	destroy luo_mag
	return -1
end if

if luo_mag.uof_movimenti_mag (	adt_data_mov, &
											as_cod_tipo_mov, &
											"S", &
											as_cod_prodotto, &
											ad_quantita, &
											ad_valore_unitario, &
											0, &
											adt_data_mov, &
											as_referenza, &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_fornitore[], &
											ls_cod_cliente[], &
											ref al_anno_mov[], &
											ref al_num_mov[] ) = 0 then
	
	destroy luo_mag
	return 0
	
else
	destroy luo_mag
	as_errore = "Errore: uof_movimenti_mag"
	return -1

end if
end function

on w_ini_dekora.create
this.sle_referenza=create sle_referenza
this.st_1=create st_1
this.cbx_commit_ogni_riga=create cbx_commit_ogni_riga
this.cb_excel=create cb_excel
this.sle_filename=create sle_filename
this.sle_path_inizio=create sle_path_inizio
this.sle_cod_tipo_mov=create sle_cod_tipo_mov
this.sle_deposito=create sle_deposito
this.em_data_movimento=create em_data_movimento
this.cb_salva=create cb_salva
this.cb_log=create cb_log
this.sle_log=create sle_log
this.st_counter=create st_counter
this.st_log=create st_log
this.cb_leggi=create cb_leggi
this.st_tot=create st_tot
this.sle_sfoglia=create sle_sfoglia
this.cb_sfoglia=create cb_sfoglia
this.dw_distinta=create dw_distinta
this.Control[]={this.sle_referenza,&
this.st_1,&
this.cbx_commit_ogni_riga,&
this.cb_excel,&
this.sle_filename,&
this.sle_path_inizio,&
this.sle_cod_tipo_mov,&
this.sle_deposito,&
this.em_data_movimento,&
this.cb_salva,&
this.cb_log,&
this.sle_log,&
this.st_counter,&
this.st_log,&
this.cb_leggi,&
this.st_tot,&
this.sle_sfoglia,&
this.cb_sfoglia,&
this.dw_distinta}
end on

on w_ini_dekora.destroy
destroy(this.sle_referenza)
destroy(this.st_1)
destroy(this.cbx_commit_ogni_riga)
destroy(this.cb_excel)
destroy(this.sle_filename)
destroy(this.sle_path_inizio)
destroy(this.sle_cod_tipo_mov)
destroy(this.sle_deposito)
destroy(this.em_data_movimento)
destroy(this.cb_salva)
destroy(this.cb_log)
destroy(this.sle_log)
destroy(this.st_counter)
destroy(this.st_log)
destroy(this.cb_leggi)
destroy(this.st_tot)
destroy(this.sle_sfoglia)
destroy(this.cb_sfoglia)
destroy(this.dw_distinta)
end on

event close;
if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if
end event

type sle_referenza from singlelineedit within w_ini_dekora
integer x = 3013
integer y = 288
integer width = 1056
integer height = 100
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "(*) INV.XLS 1-12-14"
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_ini_dekora
integer x = 2619
integer y = 304
integer width = 343
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "referenza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_commit_ogni_riga from checkbox within w_ini_dekora
integer x = 3026
integer y = 400
integer width = 530
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
string text = "Commit ogni riga"
end type

type cb_excel from commandbutton within w_ini_dekora
integer x = 4210
integer y = 580
integer width = 402
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "XLS"
end type

event clicked;


dw_distinta.saveas("",  Excel!, true)
end event

type sle_filename from singlelineedit within w_ini_dekora
integer x = 69
integer y = 280
integer width = 1111
integer height = 104
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_path_inizio from singlelineedit within w_ini_dekora
integer x = 78
integer y = 2716
integer width = 2185
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "H:\CS_CLIEN\Dekora\Mov_da_excel"
borderstyle borderstyle = stylelowered!
end type

type sle_cod_tipo_mov from singlelineedit within w_ini_dekora
integer x = 2350
integer y = 284
integer width = 229
integer height = 88
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "INL"
borderstyle borderstyle = stylelowered!
end type

type sle_deposito from singlelineedit within w_ini_dekora
integer x = 1563
integer y = 288
integer width = 183
integer height = 88
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "100"
borderstyle borderstyle = stylelowered!
end type

type em_data_movimento from editmask within w_ini_dekora
integer x = 1211
integer y = 288
integer width = 343
integer height = 88
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "01/03/2014"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dddd/mm/yyyy"
end type

type cb_salva from commandbutton within w_ini_dekora
integer x = 4096
integer y = 292
integer width = 402
integer height = 104
integer taborder = 40
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long				ll_tot, ll_index, ll_anno_mov[], ll_num_mov[], ll_count, ll_count2
string				ls_cod_prodotto, ls_valore, ls_errore, ls_cod_tipo_movimento, ls_cod_deposito, ls_file_name, ls_referenza
dec{4}			ld_qta, ls_costo_std
integer			li_ret
datetime			ldt_data_mov


ls_valore = ""
st_log.text = ""
ls_cod_tipo_movimento = sle_cod_tipo_mov.text
ll_count2 = 0

ls_file_name = sle_filename.text

dw_distinta.accepttext()

ll_tot = dw_distinta.rowcount()
if ll_tot<=0 then
	g_mb.error("Nessuna riga da salvare!")
	st_log.text = "Fine!"
	return
end if

if not g_mb.confirm("Procedo con i movimenti?") then return

for ll_index=1 to ll_tot
	Yield()
	
	ls_valore = ""
//	if ll_index=1 then
//	else
//		ls_valore += "~r~n"
//	end if
	
	if dw_distinta.getitemstring(ll_index, "flag_ok") = "S" then
		ll_count2 += 1
		
		ls_cod_prodotto = dw_distinta.getitemstring(ll_index, "cod_prodotto")
		ld_qta = dw_distinta.getitemdecimal(ll_index, "quantita")
		ls_costo_std = dw_distinta.getitemdecimal(ll_index, "costo_std")
		ls_cod_deposito = dw_distinta.getitemstring(ll_index, "cod_deposito")
		ldt_data_mov = dw_distinta.getitemdatetime(ll_index, "data_mov")
		ls_referenza = dw_distinta.getitemstring(ll_index, "referenza")
		
		if ls_cod_tipo_movimento="INL" then
			//controllo se esiste già un movimento INL con questo codice nel deposito
			select count(*)
			into :ll_count
			from mov_magazzino
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_prodotto=:ls_cod_prodotto and
						cod_tipo_movimento=:ls_cod_tipo_movimento and
						cod_deposito=:ls_cod_deposito;
		else
			ll_count = 0
		end if
		
		
		if ll_count>0 then
			//c'è già un movimento INL
			ls_valore += ls_file_name + "~t" + ls_cod_prodotto + "~t" + "ESISTE GIA UN MOV."+ls_cod_tipo_movimento + "~t" + string(ld_qta, "#####0.00") + &
							"~t" + string(ls_costo_std, "#####0.0000") + "~t"+string(0)+"~t"+string(0)
			st_log.text = "Codice " + ls_cod_prodotto + " già caricato con "+ls_cod_tipo_movimento+"!"
		else
			ls_valore +=   ls_file_name + "~t" + ls_cod_prodotto + "~t" + ls_cod_tipo_movimento + "~t" + string(ld_qta, "#####0.00") + "~t" + string(ls_costo_std, "#####0.0000")
		
			setpointer(HourGlass!)
			st_log.text = string(ll_index) + " di " + string(ll_tot) +" - Esecuzione movimento "+ls_cod_tipo_movimento+" per codice " + ls_cod_prodotto
			li_ret = wf_movimento(	ls_cod_tipo_movimento,&
											ldt_data_mov,&
											ls_cod_prodotto,&
											ls_cod_deposito,&
											ls_referenza,&
											ld_qta,&
											ls_costo_std,&
											ll_anno_mov[],&
											ll_num_mov[],&
											ls_errore)
			setpointer(Arrow!)
			
			if li_ret<0 then
				g_mb.error("Prodotto " + ls_cod_prodotto + " : " + ls_errore)
				st_log.text = "Fine con errori!"
				rollback;
				return
			else
				ls_valore += "~t"+string(ll_anno_mov[1])+"~t"+string(ll_num_mov[1])
				
				if cbx_commit_ogni_riga.checked then
					commit;
				end if
				
			end if
		
		end if
		
	else
		//riga saltata, probabilmente il codice prodotto non è stato trovato! Metto nel log.
		ls_cod_prodotto = dw_distinta.getitemstring(ll_index, "cod_prodotto")
		ld_qta = dw_distinta.getitemdecimal(ll_index, "quantita")
		ls_costo_std = dw_distinta.getitemdecimal(ll_index, "costo_std")
		
		st_log.text = "Codice " + ls_cod_prodotto + " saltato!"
		
		ls_valore +=   ls_file_name + "~t" + "NOT FOUND" + "~t" + ls_cod_prodotto + "~t" + "NON IN ANAGRAFICA O ESCLUSO" + "~t" + string(ld_qta, "#####0.00") + &
						"~t" + string(ls_costo_std, "#####0.0000") + "~tMOV."+ls_cod_tipo_movimento+"~t"+string(0)+"~t"+string(0)
		
	end if
	
	wf_log(ls_valore)
	
next

commit;

//wf_log(ls_valore)

g_mb.success("Operazione terminata!")
st_log.text = "Fine!"

return
end event

type cb_log from commandbutton within w_ini_dekora
integer x = 4096
integer y = 160
integer width = 402
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta Log"
end type

event clicked;



is_path_log = sle_log.text


//wf_log("~r~n~r~n"+string(today(), "dd-mm-yyyy")+" "+string(now(), "hh:mm_ss")+" INIZIO PROCEDURA IMPORTAZIONE GIACENZE INIZIALI")

g_mb.success("FILE DI LOG IMPOSTATO CORRETTAMENTE!")
end event

type sle_log from singlelineedit within w_ini_dekora
integer x = 69
integer y = 156
integer width = 4000
integer height = 112
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_counter from statictext within w_ini_dekora
integer x = 1015
integer y = 404
integer width = 1349
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_log from statictext within w_ini_dekora
integer x = 64
integer y = 2828
integer width = 3954
integer height = 260
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_leggi from commandbutton within w_ini_dekora
integer x = 4229
integer y = 44
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi"
end type

event clicked;string			ls_file, ls_errore
long			ll_pos



ls_file = sle_sfoglia.text

ll_pos = lastpos(sle_sfoglia.text, "\")
sle_filename.text = right(ls_file, len(sle_sfoglia.text) - ll_pos)

if wf_leggi_fogli(ls_file, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return
end if

g_mb.success("Lettura effettuata!")
end event

type st_tot from statictext within w_ini_dekora
integer x = 37
integer y = 404
integer width = 928
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Fogli Totali:"
boolean focusrectangle = false
end type

type sle_sfoglia from singlelineedit within w_ini_dekora
integer x = 69
integer y = 36
integer width = 4000
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_sfoglia from commandbutton within w_ini_dekora
integer x = 4078
integer y = 36
integer width = 133
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[], ls_errore
integer	li_count, li_ret

//ls_path = s_cs_xx.volume + "\DEKORA\"
//ls_path = "H:\CS_CLIEN\Dekora\Mov_da_excel\"
ls_path = sle_path_inizio.text + "\"

li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)
if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	sle_sfoglia.text = docpath
end if
end event

type dw_distinta from datawindow within w_ini_dekora
integer x = 64
integer y = 500
integer width = 3954
integer height = 2188
integer taborder = 10
string title = "none"
string dataobject = "d_ini_dekora"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;string				ls_des_prodotto


if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_prodotto_figlio"
		
		wf_des_prodotto(row, data, ls_des_prodotto)
		
end choose
end event

