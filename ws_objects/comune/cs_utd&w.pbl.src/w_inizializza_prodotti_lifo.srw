﻿$PBExportHeader$w_inizializza_prodotti_lifo.srw
forward
global type w_inizializza_prodotti_lifo from window
end type
type em_prodotto from editmask within w_inizializza_prodotti_lifo
end type
type st_filtro from statictext within w_inizializza_prodotti_lifo
end type
type st_10 from statictext within w_inizializza_prodotti_lifo
end type
type em_esercizio from editmask within w_inizializza_prodotti_lifo
end type
type st_6 from statictext within w_inizializza_prodotti_lifo
end type
type em_tipo_movimento from editmask within w_inizializza_prodotti_lifo
end type
type cb_3 from commandbutton within w_inizializza_prodotti_lifo
end type
type em_data_chiusura from editmask within w_inizializza_prodotti_lifo
end type
type st_5 from statictext within w_inizializza_prodotti_lifo
end type
type st_4 from statictext within w_inizializza_prodotti_lifo
end type
type em_data_apertura from editmask within w_inizializza_prodotti_lifo
end type
type st_note from statictext within w_inizializza_prodotti_lifo
end type
type cb_2 from commandbutton within w_inizializza_prodotti_lifo
end type
type cb_1 from commandbutton within w_inizializza_prodotti_lifo
end type
type em_anno from editmask within w_inizializza_prodotti_lifo
end type
type st_2 from statictext within w_inizializza_prodotti_lifo
end type
type st_1 from statictext within w_inizializza_prodotti_lifo
end type
end forward

global type w_inizializza_prodotti_lifo from window
integer width = 2853
integer height = 1624
boolean titlebar = true
string title = "Inizializzazione Prodotti"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
em_prodotto em_prodotto
st_filtro st_filtro
st_10 st_10
em_esercizio em_esercizio
st_6 st_6
em_tipo_movimento em_tipo_movimento
cb_3 cb_3
em_data_chiusura em_data_chiusura
st_5 st_5
st_4 st_4
em_data_apertura em_data_apertura
st_note st_note
cb_2 cb_2
cb_1 cb_1
em_anno em_anno
st_2 st_2
st_1 st_1
end type
global w_inizializza_prodotti_lifo w_inizializza_prodotti_lifo

on w_inizializza_prodotti_lifo.create
this.em_prodotto=create em_prodotto
this.st_filtro=create st_filtro
this.st_10=create st_10
this.em_esercizio=create em_esercizio
this.st_6=create st_6
this.em_tipo_movimento=create em_tipo_movimento
this.cb_3=create cb_3
this.em_data_chiusura=create em_data_chiusura
this.st_5=create st_5
this.st_4=create st_4
this.em_data_apertura=create em_data_apertura
this.st_note=create st_note
this.cb_2=create cb_2
this.cb_1=create cb_1
this.em_anno=create em_anno
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.em_prodotto,&
this.st_filtro,&
this.st_10,&
this.em_esercizio,&
this.st_6,&
this.em_tipo_movimento,&
this.cb_3,&
this.em_data_chiusura,&
this.st_5,&
this.st_4,&
this.em_data_apertura,&
this.st_note,&
this.cb_2,&
this.cb_1,&
this.em_anno,&
this.st_2,&
this.st_1}
end on

on w_inizializza_prodotti_lifo.destroy
destroy(this.em_prodotto)
destroy(this.st_filtro)
destroy(this.st_10)
destroy(this.em_esercizio)
destroy(this.st_6)
destroy(this.em_tipo_movimento)
destroy(this.cb_3)
destroy(this.em_data_chiusura)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.em_data_apertura)
destroy(this.st_note)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.em_anno)
destroy(this.st_2)
destroy(this.st_1)
end on

event open;em_esercizio.text = string(f_anno_esercizio())
end event

type em_prodotto from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 816
integer width = 782
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_filtro from statictext within w_inizializza_prodotti_lifo
integer x = 562
integer y = 828
integer width = 1435
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "FILTRO PRODOTTO (SOLO PER PRODOTTI CON L.I.F.O.):"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_10 from statictext within w_inizializza_prodotti_lifo
integer x = 238
integer y = 708
integer width = 1760
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "ESERCIZIO CORRENTE IN CUI SARANNO REGISTRATE LE SCRITTURE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_esercizio from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 696
integer width = 338
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
string mask = "0000"
string minmax = "1900~~2999"
end type

type st_6 from statictext within w_inizializza_prodotti_lifo
integer x = 658
integer y = 228
integer width = 1339
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "TIPO MOVIMENTO DI MAGAZZINO DA USARE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_tipo_movimento from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 216
integer width = 338
integer height = 88
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "INZ"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string minmax = "1900~~2999"
end type

event modified;em_data_apertura.text = "01/01/" + em_anno.text
em_data_chiusura.text = "31/12/" + em_anno.text
end event

type cb_3 from commandbutton within w_inizializza_prodotti_lifo
integer x = 2222
integer y = 1256
integer width = 553
integer height = 84
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prod. senza LIFO"
end type

event clicked;string ls_cod_prodotto,ls_des_prodotto
long ll_anno_lifo
dec{4} ld_giacenza_finale,ld_costo_medio_ponderato
datetime ldt_data_apertura, ldt_data_chiusura


ll_anno_lifo = long(em_anno.text)
if ll_anno_lifo = 0 then
	g_mb.messagebox("APICE","Anno LIFO non impostato correttamente! Verificare.")
	return
end if

ldt_data_apertura = datetime(date(em_data_apertura.text), 00:00:00)
ldt_data_chiusura = datetime(date(em_data_chiusura.text), 00:00:00)
if ldt_data_chiusura > ldt_data_apertura then
	g_mb.messagebox("APICE","Data apertura / chiusura incoerenti.")
	return
end if
	
if g_mb.messagebox("APICE","Sei sicuro di voler procedere con l'elaborazione?~r~nLe operazioni eseguite non saranno reversibili",Question!,YesNo!,2) = 2 then
	g_mb.messagebox("","Operazione annullata dall'utente.")
	return
end if


declare cu_lifo cursor for  
 select cod_prodotto, des_prodotto
 from   anag_prodotti
 where  cod_azienda = :s_cs_xx.cod_azienda and
        cod_prodotto not in (	 select cod_prodotto 
											from lifo  
										  where cod_azienda = :s_cs_xx.cod_azienda and
												  anno_lifo = :ll_anno_lifo )
	order by cod_prodotto;

open cu_lifo;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
	rollback;
	return
end if

do while true
	fetch cu_lifo into :ls_cod_prodotto,   :ls_des_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if

	st_note.text = ls_cod_prodotto + " - " + ls_des_prodotto

	update anag_prodotti  
	  set  saldo_quan_anno_prec = 0,   
			 saldo_quan_inizio_anno = 0,   
			 saldo_quan_ultima_chiusura = 0 , 
			 val_inizio_anno = 0  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento anagrafica prodotti del prodotto "+ ls_cod_prodotto +"~r~n" + sqlca.sqlerrtext,StopSign!)
		rollback;
		return
	end if

loop

update con_magazzino  
  set data_chiusura_annuale_prec = :ldt_data_chiusura,   
		data_chiusura_annuale = :ldt_data_chiusura,   
		data_chiusura_periodica = :ldt_data_chiusura  
where cod_azienda = :s_cs_xx.cod_azienda ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento parametri magazzino. ~r~n" + sqlca.sqlerrtext,StopSign!)
	rollback;
	return
end if

commit;
end event

type em_data_chiusura from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 576
integer width = 457
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string minmax = "01/01/1900~~31/12/2099"
end type

type st_5 from statictext within w_inizializza_prodotti_lifo
integer x = 658
integer y = 588
integer width = 1339
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "DATA DI CHIUSURA ESERCIZIO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_inizializza_prodotti_lifo
integer x = 69
integer y = 468
integer width = 1929
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "DATA IN CUI SARANNO GENERATE LE SCRITTURE DI  INVENTARIO INIZIALE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_apertura from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 456
integer width = 457
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string minmax = "01/01/1900~~31/12/2099"
end type

type st_note from statictext within w_inizializza_prodotti_lifo
integer x = 14
integer y = 1420
integer width = 2779
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_inizializza_prodotti_lifo
integer x = 27
integer y = 1256
integer width = 553
integer height = 84
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_1 from commandbutton within w_inizializza_prodotti_lifo
integer x = 1125
integer y = 1256
integer width = 553
integer height = 84
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prod. LIFO"
end type

event clicked;string ls_cod_prodotto,ls_des_prodotto,ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], &
       ls_cod_cliente[], ls_cod_tipo_movimento, ls_sql
long ll_anno_lifo,ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
		 ll_anno_reg_dest_stock, ll_num_reg_dest_stock
dec{4} ld_giacenza_finale,ld_costo_medio_ponderato,ld_valore_fine_anno
datetime ldt_data_apertura, ldt_data_chiusura,ldt_data_stock[]
uo_magazzino luo_mag
TRANSACTION sqlcb
DynamicStagingArea sqlsb

sqlcb = create transaction
sqlcb.ServerName = sqlca.ServerName
sqlcb.logid = sqlca.logid
sqlcb.LogPass = sqlca.LogPass
sqlcb.DBMS = sqlca.DBMS
sqlcb.Database = sqlca.Database
sqlcb.UserId = sqlca.UserId
sqlcb.DBPass = sqlca.DBPass
sqlcb.Lock = sqlca.Lock
connect using sqlcb;

ll_anno_lifo = long(em_anno.text)
if ll_anno_lifo = 0 then
	g_mb.messagebox("APICE","Anno LIFO non impostato correttamente! Verificare.")
	return
end if

ldt_data_apertura = datetime(date(em_data_apertura.text), 00:00:00)

ldt_data_chiusura = datetime(date(em_data_chiusura.text), 00:00:00)

if ldt_data_chiusura > ldt_data_apertura then
	g_mb.messagebox("APICE","Data apertura / chiusura incoerenti.")
	return
end if

ls_cod_tipo_movimento = em_tipo_movimento.text
	
if g_mb.messagebox("APICE","Sei sicuro di voler procedere con l'elaborazione?~r~nLe operazioni eseguite non saranno reversibili",Question!,YesNo!,2) = 2 then
	g_mb.messagebox("","Operazione annullata dall'utente.")
	return
end if

ls_sql = "select cod_prodotto,giacenza_finale,costo_medio_ponderato,valore_fine_anno from lifo where cod_azienda = '"+s_cs_xx.cod_azienda +"' and anno_lifo = " + string(ll_anno_lifo)
if len(em_prodotto.text) > 0 then
	ls_sql = ls_sql + " and cod_prodotto like '" + em_prodotto.text + "' "
end if
ls_sql = ls_sql + " order by cod_prodotto"
sqlsb = create DynamicStagingArea
declare cu_lifo dynamic cursor for sqlsb;
prepare sqlsb from :ls_sql using sqlcb;

open dynamic cu_lifo;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open del cursore cu_lifo~r~n" + sqlcb.sqlerrtext,StopSign!)
	rollback;
	return
end if

do while true
	fetch cu_lifo into :ls_cod_prodotto,   :ld_giacenza_finale,   :ld_costo_medio_ponderato, :ld_valore_fine_anno ;
	if sqlcb.sqlcode = 100 then exit
	if sqlcb.sqlcode <> 0 then
		goto fine_causa_errore
		g_mb.messagebox("APICE","Errore in fetch del cursore cu_lifo~r~n" + sqlcb.sqlerrtext,StopSign!)
	end if

	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto  using sqlcb;
	
	st_note.text = ls_cod_prodotto + " - " + ls_des_prodotto
	if sqlcb.sqlcode <> 0 then
		goto fine_causa_errore
		g_mb.messagebox("APICE","Errore in ricerca del prodotto "+ls_cod_prodotto+"~r~n" + sqlcb.sqlerrtext,StopSign!)
	end if
	
/////////////////////////////////////////////////////////////////////////////////////////////////

	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		goto fine_causa_errore
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		goto fine_causa_errore
	end if
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( ldt_data_apertura, &
								ls_cod_tipo_movimento, &
								'N', &
								ls_cod_prodotto, &
								ld_giacenza_finale, &
								ld_costo_medio_ponderato, &
								0, &
								ldt_data_apertura, &
								'', &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[] ) <> 0 then
	
		g_mb.messagebox("APICE","Movimento di magazzino non eseguito a causa di un errore~r~n" +sqlca.sqlerrtext)
		goto fine_causa_errore
		return
	end if	
	
	destroy luo_mag
	
/////////////////////////////////////////////////////////////////////////////////////////////////

	update anag_prodotti  
	  set  saldo_quan_anno_prec = :ld_giacenza_finale,   
			 saldo_quan_inizio_anno = :ld_giacenza_finale,   
			 saldo_quan_ultima_chiusura = :ld_giacenza_finale  ,
			 val_inizio_anno = :ld_valore_fine_anno  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto using sqlca;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento anagrafica prodotti del prodotto "+ls_cod_prodotto+"~r~n" + sqlca.sqlerrtext,StopSign!)
		goto fine_causa_errore
	end if
	commit using sqlca;

loop

update con_magazzino  
  set data_chiusura_annuale_prec = :ldt_data_chiusura,   
		data_chiusura_annuale = :ldt_data_chiusura,   
		data_chiusura_periodica = :ldt_data_chiusura  
where cod_azienda = :s_cs_xx.cod_azienda using sqlcb ;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento parametri magazzino~r~n" + sqlcb.sqlerrtext,StopSign!)
	goto fine_causa_errore
end if

close cu_lifo;
commit using sqlcb;
disconnect using sqlcb;
destroy sqlcb;
g_mb.messagebox("APICE","Elaborazione Terminata con Successo !",information!)
return

fine_causa_errore:
close cu_lifo;
rollback using sqlca;
rollback using sqlcb;
disconnect using sqlcb;
destroy sqlcb;
return



end event

type em_anno from editmask within w_inizializza_prodotti_lifo
integer x = 2007
integer y = 336
integer width = 338
integer height = 88
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
string minmax = "1900~~2999"
end type

event modified;em_data_apertura.text = "01/01/" + string(long(em_anno.text) + 1)
em_data_chiusura.text = "31/12/" + em_anno.text

end event

type st_2 from statictext within w_inizializza_prodotti_lifo
integer x = 658
integer y = 348
integer width = 1339
integer height = 96
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "ANNO L.I.F.O. RIFERIMENTO PER INIZIALIZZAZIONE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_inizializza_prodotti_lifo
integer x = 14
integer y = 12
integer width = 2789
integer height = 124
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Inizializzazione Prodotti di Magazzino con valore L.I.F.O."
alignment alignment = center!
boolean focusrectangle = false
end type

