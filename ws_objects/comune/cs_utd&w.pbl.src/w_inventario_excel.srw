﻿$PBExportHeader$w_inventario_excel.srw
$PBExportComments$Finestra di inventario per esportare dati in Excel
forward
global type w_inventario_excel from window
end type
type st_2 from statictext within w_inventario_excel
end type
type em_password from editmask within w_inventario_excel
end type
type st_1 from statictext within w_inventario_excel
end type
type cb_importa from commandbutton within w_inventario_excel
end type
type cb_esporta from commandbutton within w_inventario_excel
end type
type dw_report from datawindow within w_inventario_excel
end type
end forward

global type w_inventario_excel from window
integer x = 1074
integer y = 484
integer width = 3223
integer height = 1256
boolean titlebar = true
string title = "Utilità Inventario"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 79741120
st_2 st_2
em_password em_password
st_1 st_1
cb_importa cb_importa
cb_esporta cb_esporta
dw_report dw_report
end type
global w_inventario_excel w_inventario_excel

on w_inventario_excel.create
this.st_2=create st_2
this.em_password=create em_password
this.st_1=create st_1
this.cb_importa=create cb_importa
this.cb_esporta=create cb_esporta
this.dw_report=create dw_report
this.Control[]={this.st_2,&
this.em_password,&
this.st_1,&
this.cb_importa,&
this.cb_esporta,&
this.dw_report}
end on

on w_inventario_excel.destroy
destroy(this.st_2)
destroy(this.em_password)
destroy(this.st_1)
destroy(this.cb_importa)
destroy(this.cb_esporta)
destroy(this.dw_report)
end on

type st_2 from statictext within w_inventario_excel
integer x = 23
integer y = 980
integer width = 1074
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
boolean focusrectangle = false
end type

type em_password from editmask within w_inventario_excel
integer x = 320
integer y = 1060
integer width = 777
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~t/"
end type

type st_1 from statictext within w_inventario_excel
integer x = 23
integer y = 1060
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Password:"
boolean focusrectangle = false
end type

type cb_importa from commandbutton within w_inventario_excel
event ue_ultimo ( )
integer x = 2811
integer y = 980
integer width = 357
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Importa"
end type

event ue_ultimo;//string docname, named, ls_cod_prodotto, ls_unita_misura, ls_array_null[], ls_cod_tipo_movimento, &
//		 ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_referenza
//integer value
//long ll_esito_importazione, ll_i, ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_num_documento, &
//	  ll_anno_registrazione[], ll_num_registrazione[]
//double ld_giacenza, ld_costo_ultimo
//datetime ldt_data_stock[], ldt_data_documento
//
//SetPointer(HourGlass!)
//
//for ll_i = 1 to dw_report.rowcount()
//	ls_cod_prodotto = dw_report.getitemstring(ll_i, "cod_prodotto")
//	ld_giacenza = dw_report.getitemnumber(ll_i, "giacenza")
//	ld_costo_ultimo = dw_report.getitemnumber(ll_i, "costo_ultimo")
//
//	select cod_deposito, 
//			 cod_ubicazione, 
//			 cod_lotto, 
//			 data_stock, 
//			 prog_stock
//	  into :ls_cod_deposito[1],
//	  		 :ls_cod_ubicazione[1],
//			 :ls_cod_lotto[1],	
//			 :ldt_data_stock[1],
//			 :ll_prog_stock[1]
//	  from stock
//	 where cod_azienda = :s_cs_xx.cod_azienda
//	   and cod_prodotto = :ls_cod_prodotto;
//		
//	if sqlca.sqlcode <> 0 then
//		messagebox("Apice", "Errore in lettura dati da tabella stock " + sqlca.sqlerrtext)
//		return
//	end if
//	
//	ls_cod_cliente[] = ls_array_null[]
//	ls_cod_fornitore[] = ls_array_null[]
//	
//	if f_crea_dest_mov_magazzino (s_cs_xx.parametri.parametro_s_1, &
//											ls_cod_prodotto, &
//											ls_cod_deposito[], &
//											ls_cod_ubicazione[], &
//											ls_cod_lotto[], &
//											ldt_data_stock[], &
//											ll_prog_stock[], &
//											ls_cod_cliente[], &
//											ls_cod_fornitore[], &
//											ll_anno_reg_dest_stock, &
//											ll_num_reg_dest_stock ) = -1 then
//		ROLLBACK;
//		return -1
//	end if	
//	
//	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
//									 ll_num_reg_dest_stock, &
//									 s_cs_xx.parametri.parametro_s_1, &
//									 ls_cod_prodotto) = -1 then
//		ROLLBACK;
//		return 0
//	end if	
//	
//	ldt_data_stock[1] = s_cs_xx.parametri.parametro_data_1
//	setnull(ll_num_documento)
//	setnull(ldt_data_documento)
//	setnull(ls_referenza)
//	setnull(ls_cod_deposito)
//	setnull(ls_cod_ubicazione)
//	setnull(ls_cod_lotto)
//	setnull(ldt_data_stock)
//	setnull(ll_prog_stock)
//	
//	if f_movimenti_mag ( ldt_data_stock[1], &
//							s_cs_xx.parametri.parametro_s_1, &
//							"N", &
//							ls_cod_prodotto, &
//							ld_giacenza, &
//							ld_costo_ultimo, &
//							ll_num_documento, &
//							ldt_data_documento, &
//							ls_referenza, &
//							ll_anno_reg_dest_stock, &
//							ll_num_reg_dest_stock, &
//							ls_cod_deposito[], &
//							ls_cod_ubicazione[], &
//							ls_cod_lotto[], &
//							ldt_data_stock[], &
//							ll_prog_stock[], &
//							ls_cod_fornitore[], &
//							ls_cod_cliente[], &
//							ll_anno_registrazione[], &
//							ll_num_registrazione[] ) = 0 then
//		COMMIT;
//	
//		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
//											ll_num_reg_dest_stock) = -1 then
//			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
//		end if
//		COMMIT;
////		messagebox("APICE","Movimento eseguito con successo")
////		parent.postevent("pc_close")
//	else
//		ROLLBACK;
////		messagebox("APICE","Movimento non eseguito a causa di un errore")
////		parent.postevent("pc_close")
//	end if		
//	
//	st_2.text = string(ll_i)
//	
//next	
//
//st_2.text = string(ll_i - 1) + " record importati"
//
//SetPointer(Arrow!)
//
//messagebox("Apice", "importazione eseguita con successo!")
//
//
end event

event clicked;string docname, named, ls_cod_prodotto, ls_unita_misura, ls_array_null[], ls_cod_tipo_movimento, &
		 ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_referenza
integer value
long ll_esito_importazione, ll_i, ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_num_documento, &
	  ll_anno_registrazione[], ll_num_registrazione[], ll_num_righe, ll_array_null[]
double ld_giacenza, ld_costo_ultimo
datetime ldt_data_stock[], ldt_data_documento, ldt_array_null[]

uo_magazzino luo_mag

if em_password.text <> "INVENTARIO" then 
	g_mb.messagebox("Apice", "La Password inserita non è corretta!")
	return
end if	

dw_report.reset()

//setnull(ls_array_null)

value = GetFileOpenName("Select File",  &
	+ docname, named, "TXT",  &
	+ "Text Files (*.TXT),*.TXT")

value =1
if value = 1 then
	dw_report.ImportFile(docname, 2) 	
	choose case ll_esito_importazione
		case -1	
			g_mb.messagebox("Apice", "No rows")
			return
		case -2	
			g_mb.messagebox("Apice", "Empty file")
			return			
		case -3	
			g_mb.messagebox("Apice", "Invalid argument")
			return			
		case -4	
			g_mb.messagebox("Apice", "Invalid input")
			return			
		case -5	
			g_mb.messagebox("Apice", "Could not open the file")
			return			
		case -6	
			g_mb.messagebox("Apice", "Could not close the file")
			return			
		case -7
			g_mb.messagebox("Apice", "Error reading the text")
			return				
		case -8	
			g_mb.messagebox("Apice", "Not a TXT file")
			return				
		case -9	
			g_mb.messagebox("Apice", "The user canceled the import")
			return				
		case -10	
			g_mb.messagebox("Apice", "Unsupported dBase file format (not version 2 or 3)")
			return							
	end choose			
end if	

window_open(w_sel_inv_excel, 0)


SetPointer(HourGlass!)

ll_num_righe = dw_report.rowcount()

for ll_i = 1 to dw_report.rowcount()
	ls_cod_prodotto = dw_report.getitemstring(ll_i, "cod_prodotto")
	ld_giacenza = dw_report.getitemnumber(ll_i, "giacenza")
	ld_costo_ultimo = dw_report.getitemnumber(ll_i, "costo_ultimo")

//	select cod_deposito, 
//			 cod_ubicazione, 
//			 cod_lotto, 
//			 data_stock, 
//			 prog_stock
//	  into :ls_cod_deposito[1],
//	  		 :ls_cod_ubicazione[1],
//			 :ls_cod_lotto[1],	
//			 :ldt_data_stock[1],
//			 :ll_prog_stock[1]
//	  from stock
//	 where cod_azienda = :s_cs_xx.cod_azienda
//	   and cod_prodotto = :ls_cod_prodotto;
//		
//	if sqlca.sqlcode < 0 then
//		messagebox("Apice", "Errore in lettura dati da tabella stock " + sqlca.sqlerrtext)
//		return
//	end if
	
	ls_cod_cliente[] = ls_array_null[]
	ls_cod_fornitore[] = ls_array_null[]
	setnull(ls_cod_deposito[1])
	setnull(ls_cod_ubicazione[1])
	setnull(ls_cod_lotto[1])
	setnull(ldt_data_stock[1])
	setnull(ll_prog_stock[1])
	setnull(ls_cod_cliente[1])
	setnull(ls_cod_fornitore[1])
	
	if f_crea_dest_mov_magazzino (s_cs_xx.parametri.parametro_s_1, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		ROLLBACK;
		return -1
	end if	
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 s_cs_xx.parametri.parametro_s_1, &
									 ls_cod_prodotto) = -1 then
		ROLLBACK;
		return 0
	end if	
	
	ldt_data_stock[1] = s_cs_xx.parametri.parametro_data_1
	setnull(ll_num_documento)
	setnull(ldt_data_documento)
	setnull(ls_referenza)
	ls_cod_deposito[] = ls_array_null[]
	ls_cod_ubicazione[] = ls_array_null[]
	ls_cod_lotto[] = ls_array_null[]
	ldt_data_stock[] = ldt_array_null[]
	ll_prog_stock[] = ll_array_null[]
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( s_cs_xx.parametri.parametro_data_1, &
							s_cs_xx.parametri.parametro_s_1, &
							"N", &
							ls_cod_prodotto, &
							ld_giacenza, &
							ld_costo_ultimo, &
							ll_num_documento, &
							ldt_data_documento, &
							ls_referenza, &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ll_anno_registrazione[], &
							ll_num_registrazione[] ) = 0 then
		COMMIT;
	
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock) = -1 then
			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
		end if
		COMMIT;

	else
		ROLLBACK;

	end if
	
	destroy luo_mag
	
	st_2.text = string(ll_i)
	
next	

st_2.text = string(ll_i - 1) + " record importati"

SetPointer(Arrow!)

g_mb.messagebox("Apice", "importazione eseguita con successo!")


end event

type cb_esporta from commandbutton within w_inventario_excel
integer x = 2423
integer y = 980
integer width = 357
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta"
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto, ls_unita_misura, ls_cod_misura_mag, docname, named
double ld_giacenza, ld_costo_ultimo, ld_pezzi_collo, ld_costo_medio, ld_val_inizio_anno, ld_val_quan_acquistata, &
		 ld_saldo_quan_inizio_anno, ld_prog_quan_acquistata, ld_prog_quan_entrata, ld_prog_quan_uscita
long ll_i, value, ll_continua

if em_password.text <> "INVENTARIO" then 
	g_mb.messagebox("Apice", "La Password inserita non è corretta!")
	return
end if	


ll_continua = g_mb.messagebox("Apice", "Attenzione questa operazione comporta l'eliminazione di tutti i dati contenuti nelle tabelle delle~n~rBOLLE di vendita, FATTURE di vendita e MOVIMENTI MAGAZZINO.",  &
		Exclamation!, OKCancel!, 2)
if ll_continua = 2 then
	return
end if

ll_i = 1
dw_report.reset()

declare cu_anag_prodotti cursor for
 select cod_prodotto,
 		  des_prodotto,
		  cod_misura_mag,	
		  val_inizio_anno,
		  val_quan_acquistata,
		  saldo_quan_inizio_anno,
		  prog_quan_acquistata,
		  prog_quan_entrata, 
		  prog_quan_uscita,
		  costo_ultimo,
		  pezzi_collo
	from anag_prodotti
  where cod_azienda = :s_cs_xx.cod_azienda
 order by cod_prodotto;	
		  
open cu_anag_prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in apertura cursore per tabella ANAGRAFICA PRODOTTI" + sqlca.sqlerrtext)
	return
end if	

SetPointer(HourGlass!)

do while 1 = 1
	fetch cu_anag_prodotti into :ls_cod_prodotto,
										 :ls_des_prodotto,
										 :ls_cod_misura_mag,
										 :ld_val_inizio_anno,
										 :ld_val_quan_acquistata,
										 :ld_saldo_quan_inizio_anno,
										 :ld_prog_quan_acquistata,
										 :ld_prog_quan_entrata, 
										 :ld_prog_quan_uscita,
										 :ld_costo_ultimo,
										 :ld_pezzi_collo;
										 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella ANAGRAFICA PRODOTTI" + sqlca.sqlerrtext)
		return
	end if											 

	if sqlca.sqlcode = 100 then exit
	
	dw_report.insertrow(0)
	dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
	dw_report.setitem(ll_i, "unita_misura", ls_cod_misura_mag)
	ld_giacenza = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita
	dw_report.setitem(ll_i, "giacenza", ld_giacenza)	
	dw_report.setitem(ll_i, "costo_ultimo", ld_costo_ultimo)		
	dw_report.setitem(ll_i, "pezzi_collo", ld_pezzi_collo)	
	if (ld_saldo_quan_inizio_anno + ld_prog_quan_acquistata) <> 0 then
		ld_costo_medio = (ld_val_inizio_anno + ld_val_quan_acquistata) / (ld_saldo_quan_inizio_anno + ld_prog_quan_acquistata)
	else
		ld_costo_medio = (ld_val_inizio_anno + ld_val_quan_acquistata)
	end if	
	dw_report.setitem(ll_i, "costo_medio", ld_costo_medio)		
	
	st_2.text = string(ll_i)
	
	ll_i ++ 
	
	update anag_prodotti
	   set saldo_quan_anno_prec = 0,
			 saldo_quan_inizio_anno = 0,
			 saldo_quan_ultima_chiusura = 0,
			 val_inizio_anno = 0,
			 prog_quan_entrata = 0,
			 prog_quan_acquistata = 0,
			 prog_quan_uscita = 0,
			 prog_quan_venduta = 0,
			 quan_ordinata = 0,
			 quan_impegnata = 0,
			 quan_assegnata = 0,
		 	 quan_in_spedizione = 0,
		 	 quan_anticipi = 0,
		  	 val_quan_entrata = 0,
		  	 val_quan_acquistata = 0,
		 	 val_quan_uscita = 0,
			 val_quan_venduta = 0,
			 costo_standard = 0,
			 costo_ultimo = 0
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in aggiornamento dati da tabella ANAGRAFICA PRODOTTI" + sqlca.sqlerrtext)
		rollback;
		return
	end if											 	
	
loop 
close cu_anag_prodotti;

SetPointer(Arrow!)

st_2.text = string(ll_i - 1) + " record letti"

value = GetFileSaveName("Select File",  & 
	docname, named, "XLS",  &
	"XLS Files (*.XLS),*.XLS")
IF value = 1 THEN 
	dw_report.SaveAs(docname, Excel!, true)
//end if	
	
	SetPointer(HourGlass!)
	
	//--------------- Cancellazione Fatture
	delete from iva_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella IVA_FAT_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 	
	
	delete from cont_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella CONT_FAT_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from scad_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella SCAD_FAT_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_fat_ven_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_VEN_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_fat_ven_stat;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_VEN_STAT " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from det_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	//delete from tes_fat_ven_cauzioni;
	//if sqlca.sqlcode <> 0 then
	//	messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_VEN_CAUZIONI " + sqlca.sqlerrtext)
	//	rollback;
	//	return
	//end if	
	
	delete from tes_fat_ven_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_VEN_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_fat_ven_note;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_VEN_NOTE " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	//--------------- Cancellazione Bolle
	delete from det_anag_comm_anticipi;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_ANAG_COMM_ANTICIPI " + sqlca.sqlerrtext)
		rollback;
		return
	end if												
	
	//delete from prod_bolle_out_com;
	//if sqlca.sqlcode <> 0 then
	//	messagebox("Apice", "Errore in cancellazione dati dalla tabella PROD_BOLLE_OUT_COM " + sqlca.sqlerrtext)
	//	rollback;
	//	return
	//end if												
	
	//delete from iva_bol_ven;
	//if sqlca.sqlcode <> 0 then
	//	messagebox("Apice", "Errore in cancellazione dati dalla tabella IVA_BOL_VEN " + sqlca.sqlerrtext)
	//	rollback;
	//	return
	//end if											 	
	
	delete from det_bol_ven_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_VEN_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_bol_ven_stat;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_VEN_STAT " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from det_bol_ven_note;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_VEN_NOTE " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from det_bol_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_bol_ven_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_VEN_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from tes_bol_ven_note;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_VEN_NOTE " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	//delete from tes_bol_ven_cauzioni;
	//if sqlca.sqlcode <> 0 then
	//	messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_VEN_CAUZIONI " + sqlca.sqlerrtext)
	//	rollback;
	//	return
	//end if
	
	delete from tes_bol_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	//--------------- Cancellazione bolle acquisto
	delete from det_bol_acq_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_ACQ_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_bol_acq_stat;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_BOL_ACQ_STAT " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from det_bol_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_ACQ_VEN " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_bol_acq_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_ACQ_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from tes_bol_acq_note;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_ACQ_NOTE " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from tes_bol_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_BOL_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	//--------------- Cancellazione Fatture Acquisto
	delete from iva_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella IVA_FAT_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 	
	
	delete from cont_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella CONT_FAT_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from scad_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella SCAD_FAT_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_fat_acq_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_ACQ_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if											 		
	
	delete from det_fat_acq_stat;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_ACQ_STAT " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from det_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella DET_FAT_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	//delete from tes_fat_ven_cauzioni;
	//if sqlca.sqlcode <> 0 then
	//	messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_VEN_CAUZIONI " + sqlca.sqlerrtext)
	//	rollback;
	//	return
	//end if	
	
	delete from tes_fat_acq_corrispondenze;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_ACQ_CORRISPONDENZE " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_fat_acq_note;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_ACQ_NOTE " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	delete from tes_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella TES_FAT_ACQ " + sqlca.sqlerrtext)
		rollback;
		return
	end if	
	
	//--------------- Cancellazione Movimenti Magazzino
	delete from mat_prime_commessa_stock;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella MAT_PRIME_COMMESSA_STOCK " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from mat_prime_commessa;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella MAT_PRIME_COMMESSA " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	delete from mov_magazzino;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in cancellazione dati dalla tabella MOV_MAGAZZINO " + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
	
	SetPointer(Arrow!)
	
	g_mb.messagebox("Apice", "Esportazione avvenuta con successo!")

end if
end event

type dw_report from datawindow within w_inventario_excel
integer x = 23
integer y = 20
integer width = 3131
integer height = 940
string dataobject = "d_appoggio_inv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

