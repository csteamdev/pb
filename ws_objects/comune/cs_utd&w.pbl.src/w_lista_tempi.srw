﻿$PBExportHeader$w_lista_tempi.srw
$PBExportComments$Finestra Visualizzazione Tempi Produzione
forward
global type w_lista_tempi from w_cs_xx_risposta
end type
type dw_lista_tempi from uo_cs_xx_dw within w_lista_tempi
end type
type cb_2 from commandbutton within w_lista_tempi
end type
type cb_1 from uo_cb_close within w_lista_tempi
end type
type sle_tempo_min from singlelineedit within w_lista_tempi
end type
type sle_tempo_sec from singlelineedit within w_lista_tempi
end type
type st_1 from statictext within w_lista_tempi
end type
type st_2 from statictext within w_lista_tempi
end type
end forward

global type w_lista_tempi from w_cs_xx_risposta
int Width=1546
int Height=845
boolean TitleBar=true
string Title="Lista Tempi"
dw_lista_tempi dw_lista_tempi
cb_2 cb_2
cb_1 cb_1
sle_tempo_min sle_tempo_min
sle_tempo_sec sle_tempo_sec
st_1 st_1
st_2 st_2
end type
global w_lista_tempi w_lista_tempi

on pc_setwindow;call w_cs_xx_risposta::pc_setwindow;dw_lista_tempi.set_dw_options(sqlca,pcca.null_object,c_multiselect+ c_nonew+ &
                                  c_nodelete + c_nomodify, c_default)

end on

on w_lista_tempi.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_tempi=create dw_lista_tempi
this.cb_2=create cb_2
this.cb_1=create cb_1
this.sle_tempo_min=create sle_tempo_min
this.sle_tempo_sec=create sle_tempo_sec
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_tempi
this.Control[iCurrent+2]=cb_2
this.Control[iCurrent+3]=cb_1
this.Control[iCurrent+4]=sle_tempo_min
this.Control[iCurrent+5]=sle_tempo_sec
this.Control[iCurrent+6]=st_1
this.Control[iCurrent+7]=st_2
end on

on w_lista_tempi.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_tempi)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.sle_tempo_min)
destroy(this.sle_tempo_sec)
destroy(this.st_1)
destroy(this.st_2)
end on

type dw_lista_tempi from uo_cs_xx_dw within w_lista_tempi
int X=23
int Y=21
int Width=1463
int Height=501
int TabOrder=30
string DataObject="d_lista_tempi"
BorderStyle BorderStyle=StyleLowered!
end type

on clicked;call uo_cs_xx_dw::clicked;//LONG  l_Num, l_Selected[ ], tempo_min, tempo_sec
//integer li_i
//
//l_Num = dw_lista_tempi.Get_Selected_Rows(l_Selected[])
//
//if l_num > 0 then
//   tempo_min = 0
//   tempo_sec = 0
//   for li_i = 1 to l_num
//       tempo_min = tempo_min + this.getitemnumber(l_Selected[li_i], "quan_utilizzo")
//       tempo_sec = round(tempo_min / 60, 2)
//   next
//end if
//
//sle_tempo_min.text = string(sle_tempo_min)
//sle_tempo_sec.text = string(sle_tempo_sec)


end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, w_importa_produzione.is_cod_prodotto_finito)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end on

type cb_2 from commandbutton within w_lista_tempi
int X=1121
int Y=641
int Width=366
int Height=81
int TabOrder=50
string Text="Ritorna"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;LONG  l_Num, l_Selected[ ]
integer li_i
real tempo_sec, tempo_min

l_Num = dw_lista_tempi.Get_Selected_Rows(l_Selected[])
tempo_sec = 0
if l_num > 0 then
   tempo_min = 0
   tempo_sec = 0
   for li_i = 1 to l_num
       tempo_sec = tempo_sec + dw_lista_tempi.getitemnumber(l_Selected[li_i], "quan_utilizzo")
   next
end if
 
tempo_min = tempo_sec / 60

sle_tempo_min.text = string(tempo_min)
sle_tempo_sec.text = string(tempo_sec)

s_cs_xx.parametri.parametro_r_1 = tempo_min
parent.triggerevent("pc_close")
end on

type cb_1 from uo_cb_close within w_lista_tempi
int X=1121
int Y=541
int Width=366
int Height=81
int TabOrder=10
string Text="Abbandona"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_tempo_min from singlelineedit within w_lista_tempi
int X=366
int Y=541
int Width=298
int Height=81
int TabOrder=40
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_tempo_sec from singlelineedit within w_lista_tempi
int X=366
int Y=641
int Width=298
int Height=81
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_lista_tempi
int X=37
int Y=541
int Width=311
int Height=73
boolean Enabled=false
string Text="Tempo Min:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_lista_tempi
int X=23
int Y=641
int Width=325
int Height=73
boolean Enabled=false
string Text="Tempo Sec:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

