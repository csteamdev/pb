﻿$PBExportHeader$w_log_sistema.srw
forward
global type w_log_sistema from window
end type
type st_2 from statictext within w_log_sistema
end type
type st_1 from statictext within w_log_sistema
end type
type mle_extended_2 from multilineedit within w_log_sistema
end type
type st_tot from statictext within w_log_sistema
end type
type dw_ricerca from datawindow within w_log_sistema
end type
type mle_extended from multilineedit within w_log_sistema
end type
type dw_log_sistema from datawindow within w_log_sistema
end type
end forward

global type w_log_sistema from window
integer width = 3570
integer height = 2920
boolean titlebar = true
string title = "LOG SISTEMA"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
boolean center = true
event ue_postopen ( )
st_2 st_2
st_1 st_1
mle_extended_2 mle_extended_2
st_tot st_tot
dw_ricerca dw_ricerca
mle_extended mle_extended
dw_log_sistema dw_log_sistema
end type
global w_log_sistema w_log_sistema

type variables
string			is_sql_base = ""
end variables

forward prototypes
public subroutine wf_get_extended (long al_row)
public function integer wf_retrieve ()
public subroutine wf_imposta_dddw ()
end prototypes

event ue_postopen();

datetime				ldt_oggi


ldt_oggi = datetime(today(), 00:00:00)

dw_ricerca.setitem(1, "data_registrazione_inizio", ldt_oggi)
dw_ricerca.setitem(1, "data_registrazione_fine", ldt_oggi)


wf_imposta_dddw()
end event

public subroutine wf_get_extended (long al_row);

//mle_extended.text = "(ID:" + string(dw_log_sistema.getitemnumber(al_row, "id_log_sistema")) + ")     " + dw_log_sistema.getitemstring(al_row, "messaggio")
//mle_extended_2.text = "(ID:" + string(dw_log_sistema.getitemnumber(al_row, "id_log_sistema")) + ")     " + dw_log_sistema.getitemstring(al_row, "db_sqlerrtext")

mle_extended.text = dw_log_sistema.getitemstring(al_row, "messaggio")
mle_extended_2.text = dw_log_sistema.getitemstring(al_row, "db_sqlerrtext")

end subroutine

public function integer wf_retrieve ();long			ll_tot

datetime		ldt_data

string			ls_sql, ls_valore

ls_sql = is_sql_base
ls_sql += " where 1=1 "

dw_ricerca.accepttext()

ldt_data = dw_ricerca.getitemdatetime(1, "data_registrazione_inizio")
if year(date(ldt_data)) > 1950 then
	ls_sql += " and data_registrazione>='" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) + "'"
end if

ldt_data = dw_ricerca.getitemdatetime(1, "data_registrazione_fine")
if year(date(ldt_data)) > 1950 then
	ls_sql += " and data_registrazione<='" + string(ldt_data, s_cs_xx.db_funzioni.formato_data) + "'"
end if

ls_valore = dw_ricerca.getitemstring(1, "utente")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and utente='" + ls_valore + "'"
end if

ls_valore = dw_ricerca.getitemstring(1, "flag_tipo_log")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and flag_tipo_log='" + ls_valore + "'"
end if

ls_valore = dw_ricerca.getitemstring(1, "messaggio")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and messaggio like '%" + ls_valore + "%'"
end if

ls_valore = dw_ricerca.getitemstring(1, "sqlerrtext")
if ls_valore<>"" and not isnull(ls_valore) then
	ls_sql += " and db_sqlerrtext like '%" + ls_valore + "%'"
end if

ls_sql += " order by data_registrazione desc, ora_registrazione desc "

dw_log_sistema.setsqlselect(ls_sql)

ll_tot = dw_log_sistema.retrieve()

st_tot.text = "Tot.Righe: " + string(ll_tot)

return 0
end function

public subroutine wf_imposta_dddw ();DataWindowChild 			ldw_child, ldw_child_2


//f_po_loaddddw_dw(dw_ricerca, &
//                 "utente", &
//                 sqlca, &
//                 "utenti", &
//                 "cod_utente", &
//                 "nome_cognome", &
//                 "1=1")




dw_ricerca.GetChild("flag_tipo_log", ldw_child)
ldw_child.SetTransObject(SQLCA)
ldw_child.Retrieve("")


dw_ricerca.GetChild("utente", ldw_child_2)
ldw_child_2.SetTransObject(SQLCA)
ldw_child_2.Retrieve("")

return
end subroutine

on w_log_sistema.create
this.st_2=create st_2
this.st_1=create st_1
this.mle_extended_2=create mle_extended_2
this.st_tot=create st_tot
this.dw_ricerca=create dw_ricerca
this.mle_extended=create mle_extended
this.dw_log_sistema=create dw_log_sistema
this.Control[]={this.st_2,&
this.st_1,&
this.mle_extended_2,&
this.st_tot,&
this.dw_ricerca,&
this.mle_extended,&
this.dw_log_sistema}
end on

on w_log_sistema.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.mle_extended_2)
destroy(this.st_tot)
destroy(this.dw_ricerca)
destroy(this.mle_extended)
destroy(this.dw_log_sistema)
end on

event open;

dw_log_sistema.settransobject(sqlca)
is_sql_base = dw_log_sistema.getsqlselect()

dw_ricerca.insertrow(0)

postevent("ue_postopen")

end event

type st_2 from statictext within w_log_sistema
integer x = 23
integer y = 1496
integer width = 1755
integer height = 64
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "MESSAGGIO"
boolean focusrectangle = false
end type

type st_1 from statictext within w_log_sistema
integer x = 1819
integer y = 1496
integer width = 1682
integer height = 64
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "SQLERRTEXT"
boolean focusrectangle = false
end type

type mle_extended_2 from multilineedit within w_log_sistema
integer x = 1819
integer y = 1560
integer width = 1682
integer height = 1232
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 128
long backcolor = 12632256
boolean vscrollbar = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_tot from statictext within w_log_sistema
integer x = 2907
integer y = 380
integer width = 594
integer height = 56
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot.Righe: 0"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_ricerca from datawindow within w_log_sistema
integer x = 23
integer y = 20
integer width = 2747
integer height = 416
integer taborder = 10
string title = "none"
string dataobject = "d_log_sistema_sel"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string			ls_null

choose case dwo.name
	case "b_cerca"
		wf_retrieve()
		
	case "b_del"
		dw_ricerca.setitem(1, "flag_tipo_log", ls_null)
		
	case "b_del_2"
		dw_ricerca.setitem(1, "utente", ls_null)
		
end choose
end event

type mle_extended from multilineedit within w_log_sistema
integer x = 23
integer y = 1560
integer width = 1755
integer height = 1232
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 128
long backcolor = 12632256
boolean vscrollbar = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type dw_log_sistema from datawindow within w_log_sistema
integer x = 23
integer y = 464
integer width = 3479
integer height = 984
integer taborder = 20
string title = "none"
string dataobject = "d_log_sistema"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;

if currentrow > 0 then
	selectrow(0, false)
	selectrow(currentrow, true)
	
	wf_get_extended(currentrow)
	
else
	mle_extended.text = ""
	mle_extended_2.text = ""
end if
end event

