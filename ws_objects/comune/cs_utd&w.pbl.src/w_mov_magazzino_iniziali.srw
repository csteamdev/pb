﻿$PBExportHeader$w_mov_magazzino_iniziali.srw
$PBExportComments$Finestra Movimenti di Magazzino Iniziali
forward
global type w_mov_magazzino_iniziali from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_mov_magazzino_iniziali
end type
type cb_annulla from commandbutton within w_mov_magazzino_iniziali
end type
type dw_dati_mov_magazzino from uo_cs_xx_dw within w_mov_magazzino_iniziali
end type
type lb_conferme from listbox within w_mov_magazzino_iniziali
end type
end forward

global type w_mov_magazzino_iniziali from w_cs_xx_risposta
integer width = 3419
integer height = 1384
string title = "Dati Movimento Magazzino"
event ue_postopen ( )
cb_ok cb_ok
cb_annulla cb_annulla
dw_dati_mov_magazzino dw_dati_mov_magazzino
lb_conferme lb_conferme
end type
global w_mov_magazzino_iniziali w_mov_magazzino_iniziali

type variables
boolean ib_new = false
end variables

forward prototypes
public function integer wf_cli_for (string ws_cod_tipo_movimento)
end prototypes

event ue_postopen;call super::ue_postopen;dw_dati_mov_magazzino.triggerevent("pcd_new")
ib_new = true
end event

public function integer wf_cli_for (string ws_cod_tipo_movimento);string ls_flag_cliente, ls_cod_cliente, ls_flag_fornitore, ls_cod_fornitore, ls_sql, ls_null

setnull(ls_null)

declare cu_det_movimenti dynamic cursor for sqlsa;
ls_sql = "SELECT det_tipi_movimenti.flag_cliente, det_tipi_movimenti.cod_cliente, det_tipi_movimenti.flag_fornitore, det_tipi_movimenti.cod_fornitore FROM det_tipi_movimenti WHERE (det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (det_tipi_movimenti.cod_tipo_movimento = '" + ws_cod_tipo_movimento + "')"
prepare sqlsa from :ls_sql;
open dynamic cu_det_movimenti;

fetch cu_det_movimenti into :ls_flag_cliente, :ls_cod_cliente, :ls_flag_fornitore, :ls_cod_fornitore;
if sqlca.sqlcode = 0 then
	if ls_flag_cliente = "S" then
		dw_dati_mov_magazzino.Object.cod_cliente.border = 5
		dw_dati_mov_magazzino.Object.cod_cliente.background.mode = "0"
		dw_dati_mov_magazzino.Object.cod_cliente.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_cliente.tabsequence = 90
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_cliente",ls_cod_cliente)
		f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_cliente",sqlca,&
							  "anag_clienti","cod_cliente","rag_soc_1",&
							  "(anag_clienti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
							  "((anag_clienti.flag_blocco <> 'S') or (anag_clienti.flag_blocco = 'S' and anag_clienti.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	else
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_cliente",ls_null)
		dw_dati_mov_magazzino.Object.cod_cliente.border = 6
		dw_dati_mov_magazzino.Object.cod_cliente.background.mode = "1"
		dw_dati_mov_magazzino.Object.cod_cliente.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_cliente.tabsequence = 0
	end if
	if ls_flag_fornitore = "S" then
		dw_dati_mov_magazzino.Object.cod_fornitore.border = 5
		dw_dati_mov_magazzino.Object.cod_fornitore.background.mode = "0"
		dw_dati_mov_magazzino.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_fornitore.tabsequence = 90
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_fornitore",ls_cod_fornitore)
		f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_fornitore",sqlca,&
							  "anag_fornitori","cod_fornitore","rag_soc_1",&
							  "(anag_fornitori.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
							  "(anag_fornitori.flag_terzista = 'S') and " + &
							  "((anag_fornitori.flag_blocco <> 'S') or (anag_fornitori.flag_blocco = 'S' and anag_fornitori.data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	else
		dw_dati_mov_magazzino.setitem(dw_dati_mov_magazzino.getrow(),"cod_fornitore",ls_null)
		dw_dati_mov_magazzino.Object.cod_fornitore.border = 6
		dw_dati_mov_magazzino.Object.cod_fornitore.background.mode = "1"
		dw_dati_mov_magazzino.Object.cod_fornitore.background.color = rgb(255,255,255)
		dw_dati_mov_magazzino.Object.cod_fornitore.tabsequence = 0
	end if
end if
close cu_det_movimenti;
					  
					  

return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_dati_mov_magazzino.set_dw_options(sqlca, &
												 i_openparm, &
												 c_noretrieveonopen, &
												 c_default)
postevent("ue_postopen")
save_on_close(c_socnosave)

end event

on w_mov_magazzino_iniziali.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_dati_mov_magazzino=create dw_dati_mov_magazzino
this.lb_conferme=create lb_conferme
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.dw_dati_mov_magazzino
this.Control[iCurrent+4]=this.lb_conferme
end on

on w_mov_magazzino_iniziali.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_dati_mov_magazzino)
destroy(this.lb_conferme)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_mov_magazzino",sqlca,&
                 "tab_tipi_movimenti","cod_tipo_movimento","des_tipo_movimento",&
                 "tab_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_tipo_mov_det",sqlca,&
//                 "det_tipi_movimenti","cod_tipo_mov_det","des_tipo_movimento",&
//                 "det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_1",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_2",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_3",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_4",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_5",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_6",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_7",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
					  "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_8",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_9",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  

f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_deposito_10",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_ok from commandbutton within w_mov_magazzino_iniziali
integer x = 1993
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string ls_cod_deposito[], ls_cod_deposito_comodo[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], &
       ls_cod_cliente[], ls_referenza, ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_tipo_mov_det, &
		 ls_null
long   ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
		 ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_i_depositi, ll_null
double ll_quantita, ll_valore
datetime ldt_data_stock[], ldt_data_documento, ldt_null

uo_magazzino luo_mag

// ------------  faccio in modo che sulla DW venga scatenato itemchanged -----------------
//           altrimento il valore impostato nell'ultimo campo non viene accettato
//dw_dati_mov_magazzino.setcolumn(4)
//dw_dati_mov_magazzino.setcolumn(14)
// ----------------------------------------------------------------------------------------

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
setnull(ldt_null)
setnull(ll_null)
setnull(ls_null)
lb_conferme.Sorted = false

ls_cod_tipo_movimento = dw_dati_mov_magazzino.object.cod_mov_magazzino[1]
//ls_cod_tipo_mov_det = dw_dati_mov_magazzino.object.cod_tipo_mov_det[1]
ls_cod_prodotto = dw_dati_mov_magazzino.object.cod_prodotto[1]
ll_quantita = dw_dati_mov_magazzino.object.quantita[1]
ll_valore   = dw_dati_mov_magazzino.object.prezzo[1]
ll_num_documento = ll_null
ldt_data_documento = ldt_null
ls_referenza = "Carico Iniziale"


if not isnull(ls_cod_tipo_movimento) and isnull(ls_cod_prodotto) then
	g_mb.messagebox("Apice", "Campo obbligatorio [Cod_prodotto]")
	return -1
end if		
	
if ll_quantita = 0 or isnull(ll_quantita) and not isnull(ls_cod_prodotto) then
	if g_mb.messagebox("APICE","Attenzione movimento con quantità ZERO: Proseguo?",Question!,YesNo!,2) = 2 then return
end if

if ll_quantita = 0 then
	if g_mb.messagebox("APICE","Attenzione movimento a valore ZERO: Proseguo?",Question!,YesNo!,2) = 2 then return
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_1[1]) then 
	ls_cod_deposito_comodo[1] = dw_dati_mov_magazzino.object.cod_deposito_1[1]
else 
	ls_cod_deposito_comodo[1] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_2[1]) then 
	ls_cod_deposito_comodo[2] = dw_dati_mov_magazzino.object.cod_deposito_2[1]
else 
	ls_cod_deposito_comodo[2] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_3[1]) then 
	ls_cod_deposito_comodo[3] = dw_dati_mov_magazzino.object.cod_deposito_3[1]
else 
	ls_cod_deposito_comodo[3] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_4[1]) then 
	ls_cod_deposito_comodo[4] = dw_dati_mov_magazzino.object.cod_deposito_4[1]
else 
	ls_cod_deposito_comodo[4] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_5[1]) then 
	ls_cod_deposito_comodo[5] = dw_dati_mov_magazzino.object.cod_deposito_5[1]
else 
	ls_cod_deposito_comodo[5] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_6[1]) then 
	ls_cod_deposito_comodo[6] = dw_dati_mov_magazzino.object.cod_deposito_6[1]
else 
	ls_cod_deposito_comodo[6] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_7[1]) then 
	ls_cod_deposito_comodo[7] = dw_dati_mov_magazzino.object.cod_deposito_7[1]
else 
	ls_cod_deposito_comodo[7] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_8[1]) then 
	ls_cod_deposito_comodo[8] = dw_dati_mov_magazzino.object.cod_deposito_8[1]
else 
	ls_cod_deposito_comodo[8] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_9[1]) then 
	ls_cod_deposito_comodo[9] = dw_dati_mov_magazzino.object.cod_deposito_9[1]
else 
	ls_cod_deposito_comodo[9] = ls_null
end if

if not isnull(dw_dati_mov_magazzino.object.cod_deposito_10[1]) then 
	ls_cod_deposito_comodo[10] = dw_dati_mov_magazzino.object.cod_deposito_10[1]
else 
	ls_cod_deposito_comodo[10] = ls_null
end if

ls_cod_deposito_comodo[11] = ls_null

//select cod_ubicazione,
//		 cod_lotto,
//		 data_stock,
//		 prog_stock
//  into :ls_cod_ubicazione[1],		 
//		 :ls_cod_lotto[1],
//		 :ldt_data_stock[1],
//		 :ll_prog_stock[1]
//  from det_tipi_movimenti
// where cod_azienda = :s_cs_xx.cod_azienda
//   and cod_tipo_movimento = :ls_cod_tipo_movimento
//	and cod_tipo_mov_det = :ls_cod_tipo_mov_det;
//	
//if sqlca.sqlcode <> 0 and not isnull(ls_cod_tipo_movimento) then
//	messagebox("Utilità", "Attenzione non esiste nessuno stock per il movimento di magazzino inserito")
//	return -1
//end if	

ls_cod_lotto[1] = ls_null
ldt_data_stock[1] = ldt_null
ll_prog_stock[1] = ll_null


ll_i_depositi = 1

do while 1 = 1
	
	ls_cod_deposito[1] = ls_cod_deposito_comodo[ll_i_depositi]
	
	if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
											ls_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
		ROLLBACK;
		return -1
	end if
	
	if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
									 ll_num_reg_dest_stock, &
									 ls_cod_tipo_movimento, &
									 ls_cod_prodotto) = -1 then
		ROLLBACK;
		return 0
	end if
	
	luo_mag = create uo_magazzino
	
	if luo_mag.uof_movimenti_mag ( dw_dati_mov_magazzino.object.data_registrazione[1], &
								dw_dati_mov_magazzino.object.cod_mov_magazzino[1], &
								"S", &
								dw_dati_mov_magazzino.object.cod_prodotto[1], &
								ll_quantita, &
								ll_valore, &
								ll_num_documento, &
								ldt_data_documento, &
								ls_referenza, &
								ll_anno_reg_dest_stock, &
								ll_num_reg_dest_stock, &
								ls_cod_deposito[], &
								ls_cod_ubicazione[], &
								ls_cod_lotto[], &
								ldt_data_stock[], &
								ll_prog_stock[], &
								ls_cod_fornitore[], &
								ls_cod_cliente[], &
								ll_anno_registrazione[], &
								ll_num_registrazione[] ) = 0 then
		COMMIT;
	
		if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock) = -1 then
			ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
		end if
		COMMIT;
//		messagebox("APICE","Caricamento nel deposito " + ls_cod_deposito_comodo[ll_i_depositi] + "  eseguito con successo")
		lb_conferme.InsertItem("Deposito " + string(ll_i_depositi) + " OK!" , ll_i_depositi)
	else
		ROLLBACK;
//		messagebox("APICE","Caricamento nel deposito " + ls_cod_deposito_comodo[ll_i_depositi] + " non eseguito a causa di un errore")
		lb_conferme.InsertItem("Deposito " + string(ll_i_depositi) + " Errore!" , ll_i_depositi)		
	end if
	
	destroy luo_mag
	
	ll_i_depositi = ll_i_depositi + 1
	if ll_i_depositi > 10 or isnull(ls_cod_deposito_comodo[ll_i_depositi]) then exit
loop

//parent.postevent("pc_close")
end event

type cb_annulla from commandbutton within w_mov_magazzino_iniziali
integer x = 2386
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;parent.triggerevent("pc_close")
end event

type dw_dati_mov_magazzino from uo_cs_xx_dw within w_mov_magazzino_iniziali
integer x = 18
integer y = 20
integer width = 2720
integer height = 1140
integer taborder = 40
string dataobject = "d_mov_magazzino_dati_iniz"
borderstyle borderstyle = stylelowered!
end type

event pcd_modify;call super::pcd_modify;//string ls_cod_tipo_movimento
//
//ls_cod_tipo_movimento = getitemstring(getrow(),"cod_mov_magazzino")
//
//if not isnull(ls_cod_tipo_movimento) and (i_extendmode) then
//	f_PO_LoadDDDW_DW(dw_dati_mov_magazzino,"cod_tipo_mov_det",sqlca,&
//                 "tab_tipi_movimenti_det","cod_tipo_mov_det","des_tipo_movimento",&
//                 "(tab_tipi_movimenti_det.cod_azienda = '" + s_cs_xx.cod_azienda + "') and" + &
//					  "(tab_tipi_movimenti_det.cod_tipo_mov_det in " + & 
//					       "(select det_tipi_movimenti.cod_tipo_mov_det " + &
//							 "from det_tipi_movimenti " + &
//							 "where det_tipi_movimenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_tipi_movimenti.cod_tipo_movimento = '" + ls_cod_tipo_movimento + "'))" )
//end if
end event

event itemchanged;call super::itemchanged;//if i_extendmode then
//	string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str, &
//	       ls_cod_misura, ls_des_misura
//	long   ll_prog_stock
//	double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
//	datetime ldt_data_stock
//	
//	choose case i_colname
//		case "cod_tipo_movimento"
//			if not isnull(i_coltext) then
//				wf_cli_for(i_coltext)
//			end if
//		case "cod_prodotto"
//			ls_cod_prodotto = i_coltext
//			ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
//			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
//			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
//			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
//			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
//			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
//				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
//				not(ll_prog_stock < 1) then
//				  select stock.giacenza_stock,
//							stock.quan_assegnata,
//							stock.quan_in_spedizione
//				  into   :ld_giacenza,
//							:ld_quan_assegnata,
//							:ld_quan_in_spedizione
//				  from   stock  
//				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
//							(stock.cod_prodotto   = :ls_cod_prodotto ) and
//							(stock.cod_deposito   = :ls_cod_deposito ) and
//							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
//							(stock.cod_lotto      = :ls_cod_lotto ) and
//							(stock.data_stock     = :ldt_data_stock ) and
//							(stock.prog_stock     = :ll_prog_stock )   ;
//				  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione),"###,###,##0.0###")
//				  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
//			end if
//			select anag_prodotti.cod_misura_mag,   
//                tab_misure.des_misura
//         into   :ls_cod_misura,   
//                :ls_des_misura  
//         from   anag_prodotti,   
//                tab_misure  
//         where (tab_misure.cod_azienda = anag_prodotti.cod_azienda ) and  
//               (tab_misure.cod_misura = anag_prodotti.cod_misura_mag ) and  
//               ( (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) and
//                 (anag_prodotti.cod_prodotto = :ls_cod_prodotto ) );
//			if sqlca.sqlcode = 0 then
//				dw_dati_mov_magazzino.object.st_quantita.text = "Quantità in " + ls_cod_misura + " - " + ls_des_misura
//			end if
//
//	case "cod_deposito"
//			ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
//			ls_cod_deposito = i_coltext
//			ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
//			ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
//			ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
//			ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
//			if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
//				not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime(date("01/01/1900"))) and &
//				not(ll_prog_stock < 1) then
//				  select stock.giacenza_stock  
//				  into   :ld_giacenza
//				  from   stock  
//				  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
//							(stock.cod_prodotto   = :ls_cod_prodotto ) and
//							(stock.cod_deposito   = :ls_cod_deposito ) and
//							(stock.cod_ubicazione = :ls_cod_ubicazione ) and
//							(stock.cod_lotto      = :ls_cod_lotto ) and
//							(stock.data_stock     = :ldt_data_stock ) and
//							(stock.prog_stock     = :ll_prog_stock )   ;
//				  ls_str = string(ld_giacenza)
//				  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
//			end if
//	end choose
//end if
//
end event

event editchanged;call super::editchanged;//string ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_str
//long   ll_prog_stock
//double ld_giacenza, ld_quan_assegnata, ld_quan_in_spedizione
//datetime ldt_data_stock
//
//if ((dw_dati_mov_magazzino.getcolumnname() = "cod_prodotto") or &
//   (dw_dati_mov_magazzino.getcolumnname() = "cod_deposito") or &
//   (dw_dati_mov_magazzino.getcolumnname() = "cod_ubicazione") or &
//   (dw_dati_mov_magazzino.getcolumnname() = "cod_lotto") or &
//   (dw_dati_mov_magazzino.getcolumnname() = "data_stock") or &
//   (dw_dati_mov_magazzino.getcolumnname() = "prog_stock")) and &
//	(i_extendmode) then
//	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
//	ls_cod_deposito = this.getitemstring(this.getrow(),"cod_deposito")
//	ls_cod_ubicazione = this.getitemstring(this.getrow(),"cod_ubicazione")
//	ls_cod_lotto = this.getitemstring(this.getrow(),"cod_lotto")
//	ldt_data_stock = this.getitemdatetime(this.getrow(),"data_stock")
//	ll_prog_stock  = this.getitemnumber(this.getrow(),"prog_stock")
//
//	choose case dw_dati_mov_magazzino.getcolumnname()
//		case "cod_prodotto"
//			ls_cod_prodotto = data
//		case "cod_deposito"
//			ls_cod_deposito = data
//		case "cod_ubicazione"
//			ls_cod_ubicazione = data
//		case "cod_lotto"
//			ls_cod_lotto = data
//		case "data_stock"
//			ldt_data_stock = datetime(data)
//		case "prog_stock"
//			ll_prog_stock  = long(data)
//	end choose
//
//	if not(isnull(ls_cod_prodotto)) and not(isnull(ls_cod_deposito)) and not(isnull(ls_cod_ubicazione)) and &
//		not(isnull(ls_cod_lotto)) and not(isnull(ldt_data_stock)) and not(ldt_data_stock <= datetime("01/01/1900")) and &
//		not(ll_prog_stock < 1) then
//		  select stock.giacenza_stock,
//					stock.quan_assegnata,
//					stock.quan_in_spedizione
//		  into   :ld_giacenza,
//					:ld_quan_assegnata,
//					:ld_quan_in_spedizione
//		  from   stock  
//		  where  (stock.cod_azienda    = :s_cs_xx.cod_azienda ) and
//					(stock.cod_prodotto   = :ls_cod_prodotto ) and
//					(stock.cod_deposito   = :ls_cod_deposito ) and
//					(stock.cod_ubicazione = :ls_cod_ubicazione ) and
//					(stock.cod_lotto      = :ls_cod_lotto ) and
//					(stock.data_stock     = :ldt_data_stock ) and
//					(stock.prog_stock     = :ll_prog_stock )   ;
//		  ls_str = string(ld_giacenza - ( ld_quan_assegnata + ld_quan_in_spedizione) ,"###,###,##0.0###")
//		  dw_dati_mov_magazzino.object.st_giacenza.text = ls_str
//	end if
//end if
//
end event

event losefocus;call super::losefocus;//if ib_new then
//	this.triggerevent("itemchanged")
//end if
end event

event pcd_new;call super::pcd_new;this.setitem(this.getrow(), "data_registrazione", datetime(today()))
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_dati_mov_magazzino,"cod_prodotto")
end choose
end event

type lb_conferme from listbox within w_mov_magazzino_iniziali
integer x = 2761
integer y = 20
integer width = 599
integer height = 1096
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean vscrollbar = true
borderstyle borderstyle = styleraised!
end type

