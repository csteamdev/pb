﻿$PBExportHeader$w_mrboxer.srw
forward
global type w_mrboxer from window
end type
type st_2 from statictext within w_mrboxer
end type
type em_data_inizio from editmask within w_mrboxer
end type
type st_1 from statictext within w_mrboxer
end type
type pgr_1 from hprogressbar within w_mrboxer
end type
type mle_log from multilineedit within w_mrboxer
end type
type cb_1 from commandbutton within w_mrboxer
end type
end forward

global type w_mrboxer from window
integer width = 1413
integer height = 1296
boolean titlebar = true
string title = "Importa Dati"
boolean controlmenu = true
boolean minbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_2 st_2
em_data_inizio em_data_inizio
st_1 st_1
pgr_1 pgr_1
mle_log mle_log
cb_1 cb_1
end type
global w_mrboxer w_mrboxer

type variables
uo_mrboxer io_mrboxer
end variables

forward prototypes
public subroutine fwf_resize (integer ai_height, string as_modal)
end prototypes

public subroutine fwf_resize (integer ai_height, string as_modal);
long ll_incremento = 20
long ll_incremento_height = 0

if ai_height < 10 then return

choose case as_modal
	case "Down"
		do while true
			if ll_incremento_height >= ai_height then exit
			ll_incremento_height += ll_incremento
			this.height +=  ll_incremento
			sleep(.350)
			Yield()
		loop
		
	case "Up"
		do while true
			if ll_incremento_height >= ai_height then exit
			ll_incremento_height += ll_incremento
			this.height -=  ll_incremento
			sleep(.350)
			Yield()
		loop
		
end choose
end subroutine

on w_mrboxer.create
this.st_2=create st_2
this.em_data_inizio=create em_data_inizio
this.st_1=create st_1
this.pgr_1=create pgr_1
this.mle_log=create mle_log
this.cb_1=create cb_1
this.Control[]={this.st_2,&
this.em_data_inizio,&
this.st_1,&
this.pgr_1,&
this.mle_log,&
this.cb_1}
end on

on w_mrboxer.destroy
destroy(this.st_2)
destroy(this.em_data_inizio)
destroy(this.st_1)
destroy(this.pgr_1)
destroy(this.mle_log)
destroy(this.cb_1)
end on

event open;io_mrboxer = create uo_mrboxer
io_mrboxer.uof_setlog( mle_log )
io_mrboxer.uof_setprogressbar(pgr_1)
io_mrboxer.uof_setbutton(cb_1)

// *** stefanop 29/01/2008
// imposto il campo data importazione ad una settimana prima della data attuale
// funzione che dovrebbe rendere più piacevole l'uso del programma
// {
date ld_now

ld_now = relativeDate(Today(), -7);
em_data_inizio.Text = string(ld_now)
// }
end event

event close;destroy io_mrboxer
end event

type st_2 from statictext within w_mrboxer
integer x = 91
integer y = 1032
integer width = 667
integer height = 72
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "DATA INIZIO IMPORT:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_inizio from editmask within w_mrboxer
integer x = 777
integer y = 1020
integer width = 503
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "01/01/2008"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_1 from statictext within w_mrboxer
boolean visible = false
integer x = 69
integer y = 1020
integer width = 1234
integer height = 140
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Attenzione: premendo ferma i dati non saranno salvati nel database"
boolean focusrectangle = false
end type

type pgr_1 from hprogressbar within w_mrboxer
integer x = 69
integer y = 900
integer width = 1234
integer height = 80
unsignedinteger maxposition = 100
integer setstep = 10
end type

type mle_log from multilineedit within w_mrboxer
integer x = 69
integer y = 280
integer width = 1234
integer height = 600
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
integer tabstop[] = {0}
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_mrboxer
integer x = 69
integer y = 40
integer width = 1234
integer height = 200
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Importa dati"
boolean default = true
end type

event clicked;string ls_data

if this.text = "Importa dati" then
	this.text = "Ferma importazione"
	//fwf_resize(172, "Down")
	st_1.visible = true
	st_2.visible = false;
	em_data_inizio.visible = false;
	Yield()
	
	ls_data = em_data_inizio.text
	if isnull(ls_data) or len(ls_data) < 1 then
		io_mrboxer.id_data_controllo = date("01/01/1990")
	else
		io_mrboxer.id_data_controllo = date(em_data_inizio.text)
	end if
	
	messagebox("IMPORT", "Importazione Ordini dalla data" + string(io_mrboxer.id_data_controllo,"dd/mm/yyyy"))
	
	io_mrboxer.uof_importa( )
else
	io_mrboxer.uof_stop( )
	st_1.visible = false
	st_2.visible = true;
	em_data_inizio.visible = true;
	this.text = "Importa dati"
	//fwf_resize(172, "Up")
end if
end event

