﻿$PBExportHeader$w_pulisci_db_menu.srw
forward
global type w_pulisci_db_menu from w_cs_xx_principale
end type
type cb_elimina from commandbutton within w_pulisci_db_menu
end type
type dw_elementi from datawindow within w_pulisci_db_menu
end type
end forward

global type w_pulisci_db_menu from w_cs_xx_principale
integer width = 2519
integer height = 1596
string title = "Elementi incongruenti"
cb_elimina cb_elimina
dw_elementi dw_elementi
end type
global w_pulisci_db_menu w_pulisci_db_menu

on w_pulisci_db_menu.create
int iCurrent
call super::create
this.cb_elimina=create cb_elimina
this.dw_elementi=create dw_elementi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elimina
this.Control[iCurrent+2]=this.dw_elementi
end on

on w_pulisci_db_menu.destroy
call super::destroy
destroy(this.cb_elimina)
destroy(this.dw_elementi)
end on

event open;call super::open;if dw_elementi.settransobject(sqlca) < 0 then
	g_mb.messagebox("Elementi senza padre","Errore in impostazione transazione.",stopsign!)
	destroy dw_elementi
	return -1
end if

if (dw_elementi.retrieve()<0) then
	g_mb.messagebox("Elementi senza padre","Errore in lettura voci.",stopsign!)
	destroy dw_elementi
	return -1
end if
end event

type cb_elimina from commandbutton within w_pulisci_db_menu
integer x = 151
integer y = 1332
integer width = 2194
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Elimina righe"
end type

event clicked;integer li_elimina, li_update;

Do While (dw_elementi.retrieve()>0)
	Do While (dw_elementi.Rowcount() > 0)
		//Controllo che non ci siano errori...
		if(dw_elementi.deleterow(1)=-1) Then
			MessageBox("Errore", "Si è verificato un errore durante l'eliminazione della riga!", StopSign!)
			return -1
		end if
	LOOP
	
	//Controllo che non ci siano errori...
	if(dw_elementi.update( )<>1) Then
		MessageBox("Errore", "Si è verificato un errore durante l'eliminazione della riga!", StopSign!)
	else
		//Riporto tutto sul DB
		COMMIT ;
	end if
Loop
end event

type dw_elementi from datawindow within w_pulisci_db_menu
integer x = 32
integer y = 64
integer width = 2432
integer height = 1220
integer taborder = 10
string title = "none"
string dataobject = "ds_voci_da_cancellare_menu"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

