﻿$PBExportHeader$w_ricalcola_anag_prodotti.srw
$PBExportComments$Chiusura Periodica ed Annuale del Magazzino
forward
global type w_ricalcola_anag_prodotti from window
end type
type cb_1 from commandbutton within w_ricalcola_anag_prodotti
end type
type em_1 from editmask within w_ricalcola_anag_prodotti
end type
type st_4 from statictext within w_ricalcola_anag_prodotti
end type
type st_3 from statictext within w_ricalcola_anag_prodotti
end type
type sle_1 from singlelineedit within w_ricalcola_anag_prodotti
end type
type cb_2 from commandbutton within w_ricalcola_anag_prodotti
end type
end forward

global type w_ricalcola_anag_prodotti from window
integer x = 832
integer y = 360
integer width = 2587
integer height = 1184
boolean titlebar = true
string title = "Ricalcolo Scheda Prodotto"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
cb_1 cb_1
em_1 em_1
st_4 st_4
st_3 st_3
sle_1 sle_1
cb_2 cb_2
end type
global w_ricalcola_anag_prodotti w_ricalcola_anag_prodotti

forward prototypes
public function integer wf_controlla_progressivi (ref string fs_error)
public function integer wf_ricalcola_progressivi (ref string fs_error)
end prototypes

public function integer wf_controlla_progressivi (ref string fs_error);// ------------------------------------------------------------------------------------------
// wf_chiusura_annuale( fdt_data_chius, fs_error)
//
//                       Funzione che esegue la chiusura annuale
//
// Calcola le quantità inizio anno, azzera i progressivi e le rispettive valorizzazioni.
// La procedura calcola inoltre il costo medio .
// Tali valori vengono calcolati come differenza fra quanto letto in anag_prodotti 
// e quanto calcolato per i movimenti dopo la chiusura annuale.
// Inoltre la procedura esegue un movimento di apertuta per ogni lotto verificando che la
// giacenza dei lotto corrisponda con la giacenza del prodotto a magazzino.
// Prima di tutto deve essere fatta la la chiusura periodica
// ritorna -1 in caso di errore
//
// ------------------------------------------------------------------------------------------
string ls_cod_prodotto, ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_prod_in_chius[], ls_where, &
       ls_filtro_prodotto, ls_chiave[], ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_data_stock, ls_prog_stock, &
		 ls_messaggio,ls_cod_tipo_mov_inv_iniz,ls_cod_tipo_mov_chius_annuale, ls_str, ls_tipo_valorizzazione
integer li_ret_funz
long ll_num_righe, ll_cont,ll_i, ll_y, ll_num_prod, ll_pos, ll_pos_old, ll_prog_stock,ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], &
     ll_file,ll_anno_reg_mov_mag_chius[], ll_num_reg_mov_mag_chius[]
dec{4}   ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
			ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
			ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
			ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
			ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[],ld_giacenza_inventario, &
			ld_giacenza_anagrafica, ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_chiusura_annuale_prec, ldt_data_registrazione, ldt_oggi, ldt_data_chiusura_periodica, ldt_max_data_mov, &
         ldt_data_stock,ldt_data_apertura
datastore lds_mov_magazzino
uo_magazzino luo_magazzino
// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven


ls_filtro_prodotto = em_1.text
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

select data_chiusura_annuale, 
       data_chiusura_periodica,
		 cod_tipo_mov_inv_iniz,
		 cod_tipo_mov_chius_annuale
into   :ldt_data_chiusura_annuale_prec,
	    :ldt_data_chiusura_periodica,
		 :ls_cod_tipo_mov_inv_iniz,
		 :ls_cod_tipo_mov_chius_annuale
from   con_magazzino
where  cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale_prec) then
	ldt_data_chiusura_annuale_prec=datetime(date("01/01/1900"))
end if

ll_num_righe=0

sle_1.text = "Elaborazione in corso. Attendere ......"

select max(data_registrazione)
into   :ldt_max_data_mov
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

ldt_oggi=datetime(today())

// seleziono i prodotti da chiudere
sle_1.text = "Ricerca dei prodotti da elaborare ....."

DECLARE cur_prod CURSOR FOR 
select distinct cod_prodotto
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda AND  
		cod_prodotto like :ls_filtro_prodotto;

ll_i=1
OPEN cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if
DO while 0=0
	FETCH cur_prod INTO :ls_cod_prodotto;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode <> 0 then
		fs_error="Errore nel DB "+SQLCA.SQLErrText
		rollback;
		return -1
	end if
	if not isnull(ls_cod_prodotto) then 
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		ll_i = ll_i + 1
		sle_1.text = "Ricerca dei prodotti da elaborare ....."+ string(ll_i) + "(" + ls_cod_prodotto + ")"
	end if
	
LOOP
CLOSE cur_prod ;

ls_where = ""

// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

ll_File = FileOpen("C:\controllo_coerenza.txt", LineMode!, Write!, LockWrite!, Replace!)
ls_str = ls_cod_prodotto + "~t giacenza anagrafica ~t giacenza inventario"

for ll_num_righe = 1 to  ll_i - 1
	ls_cod_prodotto = ls_prod_in_chius[ll_num_righe]
	sle_1.text = "Elaborazione Prodotto: " + ls_cod_prodotto
	
	luo_magazzino = CREATE uo_magazzino
	
	li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto,ldt_max_data_mov,ls_where,ref ld_quant_val_attuale[],ref ls_error,'N',ref ls_chiave[],ref ld_giacenza[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[])
	
	destroy luo_magazzino
	
	if li_ret_funz < 0 then
		fs_error = "Errore nel calcolo della situazione attuale del prodotto " + ls_cod_prodotto + ", " + ls_error
		return -1
	end if
	
	select saldo_quan_inizio_anno + prog_quan_entrata - prog_quan_uscita
	into   :ld_giacenza_anagrafica
	from   anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		fs_error = "Errore durante ricerca progressivi prodotto " + ls_cod_prodotto + ", " + sqlca.sqlerrtext
		rollback;
		return -1
	end if

	ld_giacenza_inventario = ld_quant_val_attuale[1] + ld_quant_val_attuale[4] - ld_quant_val_attuale[6]
	
	if ld_giacenza_anagrafica - ld_giacenza_inventario <> 0 then
		ls_str = ls_cod_prodotto + "~t" + string(ld_giacenza_anagrafica,"###,###,###,##0.0000")+ "~t" + string(ld_giacenza_inventario,"###,###,###,##0.0000")
		filewrite(ll_file,ls_str)
	end if
	
next

rollback;
FileClose(ll_file)
destroy lds_mov_magazzino
sle_1.text = "Elaborazione terminata con successo !  "
return 0
end function

public function integer wf_ricalcola_progressivi (ref string fs_error);// ------------------------------------------------------------------------------------------
// wf_chiusura_annuale( fdt_data_chius, fs_error)
//
//                       Funzione che esegue la chiusura annuale
//
// Calcola le quantità inizio anno, azzera i progressivi e le rispettive valorizzazioni.
// La procedura calcola inoltre il costo medio .
// Tali valori vengono calcolati come differenza fra quanto letto in anag_prodotti 
// e quanto calcolato per i movimenti dopo la chiusura annuale.
// Inoltre la procedura esegue un movimento di apertuta per ogni lotto verificando che la
// giacenza dei lotto corrisponda con la giacenza del prodotto a magazzino.
// Prima di tutto deve essere fatta la la chiusura periodica
// ritorna -1 in caso di errore
//
// ------------------------------------------------------------------------------------------
string ls_cod_prodotto, ls_cod_tipo_movimento_det, ls_error, ls_cod_tipo_movimento, ls_prod_in_chius[], ls_where, &
       ls_filtro_prodotto, ls_chiave[], ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_data_stock, ls_prog_stock, &
		 ls_messaggio,ls_cod_tipo_mov_inv_iniz,ls_cod_tipo_mov_chius_annuale, ls_str, ls_tipo_valorizzazione
integer li_ret_funz
long ll_num_righe, ll_cont,ll_i, ll_y, ll_num_prod, ll_pos, ll_pos_old, ll_prog_stock,ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], &
     ll_file,ll_anno_reg_mov_mag_chius[], ll_num_reg_mov_mag_chius[]
dec{4}   ld_saldo_quan_inizio_anno, ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_val_acq, &
			ld_prog_quan_entrata, ld_val_quan_entrata, ld_prog_quan_uscita, ld_val_quan_uscita, &
			ld_prog_quan_acquistata, ld_val_quan_acquistata, ld_prog_quan_venduta, &
			ld_val_quan_venduta , ld_saldo_quan_anno_prec, ld_quan_movimento, ld_val_movimento, &
			ld_valore_prodotto, ld_quan_acq,ld_quant_val[], ld_quant_val_attuale[], ld_giacenza[], &
			ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
datetime ldt_data_chiusura_annuale_prec, ldt_data_registrazione, ldt_oggi, ldt_data_chiusura_periodica, ldt_max_data_mov, &
         ldt_data_stock,ldt_data_apertura
datastore lds_mov_magazzino
uo_magazzino luo_magazzino
// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven


ls_filtro_prodotto = em_1.text
if isnull(ls_filtro_prodotto) or len(ls_filtro_prodotto) < 1 then ls_filtro_prodotto = "%"

select data_chiusura_annuale, 
       data_chiusura_periodica,
		 cod_tipo_mov_inv_iniz,
		 cod_tipo_mov_chius_annuale
into   :ldt_data_chiusura_annuale_prec,
	    :ldt_data_chiusura_periodica,
		 :ls_cod_tipo_mov_inv_iniz,
		 :ls_cod_tipo_mov_chius_annuale
from   con_magazzino
where  cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale_prec) then
	ldt_data_chiusura_annuale_prec=datetime(date("01/01/1900"))
end if

ll_num_righe=0

sle_1.text = "Elaborazione in corso. Attendere ......"

select max(data_registrazione)
into   :ldt_max_data_mov
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;

ldt_oggi=datetime(today())

// seleziono i prodotti da chiudere
sle_1.text = "Ricerca dei prodotti da elaborare ....."

DECLARE cur_prod CURSOR FOR 
select distinct cod_prodotto
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda AND  
		cod_prodotto like :ls_filtro_prodotto;

ll_i=1
OPEN cur_prod;
if sqlca.sqlcode <> 0 then
	fs_error = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if
DO while 0=0
	FETCH cur_prod INTO :ls_cod_prodotto;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode <> 0 then
		fs_error="Errore nel DB "+SQLCA.SQLErrText
		rollback;
		return -1
	end if
	if not isnull(ls_cod_prodotto) then 
		ls_prod_in_chius[ll_i] = ls_cod_prodotto
		ll_i = ll_i + 1
		sle_1.text = "Ricerca dei prodotti da elaborare ....."+ string(ll_i) + "(" + ls_cod_prodotto + ")"
	end if
	
LOOP
CLOSE cur_prod ;

ls_where = ""

// ****************************** Ricalcolo Progressivi Prodotto *************************************
// ld_quant_val: [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven

for ll_num_righe = 1 to  ll_i - 1
	ls_cod_prodotto = ls_prod_in_chius[ll_num_righe]
	sle_1.text = "Elaborazione Prodotto: " + ls_cod_prodotto
	
	luo_magazzino = CREATE uo_magazzino

	li_ret_funz = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto,ldt_max_data_mov,ls_where,ref ld_quant_val_attuale[],ref ls_error,'N',ref ls_chiave[],ref ld_giacenza[], ref ld_costo_medio_stock[], ref ld_quan_costo_medio_stock[])
	
	destroy 	luo_magazzino
	
	if li_ret_funz < 0 then
		fs_error = "Errore nel calcolo della situazione attuale del prodotto " + ls_cod_prodotto + ", " + ls_error
		return -1
	end if
	
//	ld_saldo_quan_anno_prec = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
//	ld_saldo_quan_ultima_chiusura = ld_saldo_quan_anno_prec
	ld_saldo_quan_inizio_anno = ld_quant_val_attuale[1]
//	ld_val_inizio_anno = ld_valore_prodotto
	
	ld_prog_quan_entrata = ld_quant_val_attuale[4]
	ld_val_quan_entrata = ld_quant_val_attuale[5]
	ld_prog_quan_uscita = ld_quant_val_attuale[6]
	ld_val_quan_uscita = ld_quant_val_attuale[7]
	ld_prog_quan_acquistata = ld_quant_val_attuale[8]
	ld_val_quan_acquistata = ld_quant_val_attuale[9]
	ld_prog_quan_venduta = ld_quant_val_attuale[10]
	ld_val_quan_venduta = ld_quant_val_attuale[11]
	
	update anag_prodotti 
	set	 saldo_quan_inizio_anno = :ld_saldo_quan_inizio_anno,
			 prog_quan_entrata = :ld_prog_quan_entrata,
			 val_quan_entrata = :ld_val_quan_entrata,
			 prog_quan_uscita = :ld_prog_quan_uscita,
			 val_quan_uscita = :ld_val_quan_uscita,
			 prog_quan_acquistata = :ld_prog_quan_acquistata,
			 val_quan_acquistata = :ld_val_quan_acquistata,
			 prog_quan_venduta = :ld_prog_quan_venduta,
			 val_quan_venduta = :ld_val_quan_venduta
	WHERE  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode <> 0 then
		fs_error = "Errore durante aggiornamento progressivi prodotto " + ls_cod_prodotto + ", " + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	// ************************** Fine Ricalcolo Progressivi Prodotto *************************************
	
	commit;
next

destroy lds_mov_magazzino
sle_1.text = "Elaborazione terminata con successo !  "
return 0
end function

on w_ricalcola_anag_prodotti.create
this.cb_1=create cb_1
this.em_1=create em_1
this.st_4=create st_4
this.st_3=create st_3
this.sle_1=create sle_1
this.cb_2=create cb_2
this.Control[]={this.cb_1,&
this.em_1,&
this.st_4,&
this.st_3,&
this.sle_1,&
this.cb_2}
end on

on w_ricalcola_anag_prodotti.destroy
destroy(this.cb_1)
destroy(this.em_1)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.sle_1)
destroy(this.cb_2)
end on

type cb_1 from commandbutton within w_ricalcola_anag_prodotti
integer x = 914
integer y = 520
integer width = 663
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CONTROLLO"
end type

event clicked;string ls_error
integer li_ret_fun

SetPointer(HourGlass!)

li_ret_fun = wf_controlla_progressivi(ls_error)

if li_ret_fun<0 then 
	g_mb.messagebox("Errore in Chiusura Annuale", ls_error)
else
	g_mb.messagebox("Ricalcolo Progressivi", "Ricalcolo eseguito con successo.")
end if

SetPointer(Arrow!)

RUN("notepad.exe C:\controllo_coerenza.txt")



end event

type em_1 from editmask within w_ricalcola_anag_prodotti
integer x = 1074
integer y = 340
integer width = 731
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 16777215
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type st_4 from statictext within w_ricalcola_anag_prodotti
integer x = 709
integer y = 340
integer width = 343
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "PRODOTTO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_ricalcola_anag_prodotti
integer x = 23
integer y = 40
integer width = 2473
integer height = 140
integer textsize = -20
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "AGGIORNAMENTO SCHEDA PRODOTTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_ricalcola_anag_prodotti
integer x = 23
integer y = 980
integer width = 2514
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
boolean border = false
boolean autohscroll = false
end type

type cb_2 from commandbutton within w_ricalcola_anag_prodotti
integer x = 914
integer y = 700
integer width = 663
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESEGUI RICALCOLO"
end type

event clicked;string ls_error
integer li_ret_fun

SetPointer(HourGlass!)

li_ret_fun = wf_ricalcola_progressivi(ls_error)

if li_ret_fun<0 then 
	g_mb.messagebox("Errore in Chiusura Annuale", ls_error)
else
	g_mb.messagebox("Ricalcolo Progressivi", "Ricalcolo eseguito con successo.")
end if

SetPointer(Arrow!)


end event

