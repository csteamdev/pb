﻿$PBExportHeader$w_ricalcolo_stato_ordini.srw
forward
global type w_ricalcolo_stato_ordini from window
end type
type st_3 from statictext within w_ricalcolo_stato_ordini
end type
type cb_1 from commandbutton within w_ricalcolo_stato_ordini
end type
type st_2 from statictext within w_ricalcolo_stato_ordini
end type
type st_1 from statictext within w_ricalcolo_stato_ordini
end type
type em_1 from editmask within w_ricalcolo_stato_ordini
end type
end forward

global type w_ricalcolo_stato_ordini from window
integer width = 2002
integer height = 724
boolean titlebar = true
string title = "Utilità di Ricalcolo Stato Ordini"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 80269524
st_3 st_3
cb_1 cb_1
st_2 st_2
st_1 st_1
em_1 em_1
end type
global w_ricalcolo_stato_ordini w_ricalcolo_stato_ordini

on w_ricalcolo_stato_ordini.create
this.st_3=create st_3
this.cb_1=create cb_1
this.st_2=create st_2
this.st_1=create st_1
this.em_1=create em_1
this.Control[]={this.st_3,&
this.cb_1,&
this.st_2,&
this.st_1,&
this.em_1}
end on

on w_ricalcolo_stato_ordini.destroy
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_1)
end on

type st_3 from statictext within w_ricalcolo_stato_ordini
integer x = 23
integer y = 520
integer width = 1897
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_ricalcolo_stato_ordini
integer x = 571
integer y = 340
integer width = 823
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESEGUI ELABORAZIONE"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_righe, ll_i, ll_ret
datetime ldt_data_registrazione
datastore lds_ordini
uo_generazione_documenti luo_gen_doc

ll_anno_registrazione = long(em_1.text)
if isnull(ll_anno_registrazione) or ll_anno_registrazione < 1 then
	g_mb.messagebox("APICE","E' obbligatorio selezionare un anno")
	return
end if

luo_gen_doc = create uo_generazione_documenti 

lds_ordini = create datastore
lds_ordini.dataobject = 'd_ricalcola_stato_ordine'
lds_ordini.settransobject(sqlca)
ll_righe = lds_ordini.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione)
if ll_righe < 0 then
	g_mb.messagebox("APICE","errore in retrieve datastore")
	return
end if

for ll_i = 1 to ll_righe
	ll_num_registrazione = lds_ordini.getitemnumber(ll_i,"num_registrazione")
	ldt_data_registrazione  = lds_ordini.getitemdatetime(ll_i,"data_registrazione")
	
	st_3.text = "In elaborazione ordine " + string(ll_num_registrazione)
	
	ll_ret = luo_gen_doc.uof_calcola_stato_ordine(ll_anno_registrazione, ll_num_registrazione)
	if ll_ret = 0 then
		commit;
	else
		g_mb.messagebox("APICE","Errore un calcolo stato ordine " + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
		rollback;
	end if
next

g_mb.messagebox("APICE","Elaborazione finita")

commit;

end event

type st_2 from statictext within w_ricalcolo_stato_ordini
integer x = 320
integer y = 220
integer width = 571
integer height = 88
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "ANNO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_ricalcolo_stato_ordini
integer x = 46
integer y = 20
integer width = 1874
integer height = 120
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ricalcolo dello Stato degli ordini di Vendita"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_1 from editmask within w_ricalcolo_stato_ordini
integer x = 914
integer y = 220
integer width = 320
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
boolean spin = true
end type

