﻿$PBExportHeader$w_rigenera_quan_impegnata.srw
$PBExportComments$Finestra rigenerazione quantità impegnata
forward
global type w_rigenera_quan_impegnata from w_cs_xx_risposta
end type
type st_1 from statictext within w_rigenera_quan_impegnata
end type
type cb_1 from commandbutton within w_rigenera_quan_impegnata
end type
type st_2 from statictext within w_rigenera_quan_impegnata
end type
type mle_1 from multilineedit within w_rigenera_quan_impegnata
end type
end forward

global type w_rigenera_quan_impegnata from w_cs_xx_risposta
int Width=2460
int Height=1465
boolean TitleBar=true
string Title="Rig. Quantità Impegnata"
st_1 st_1
cb_1 cb_1
st_2 st_2
mle_1 mle_1
end type
global w_rigenera_quan_impegnata w_rigenera_quan_impegnata

forward prototypes
public subroutine wf_rigenera_quan_impegnata ()
end prototypes

public subroutine wf_rigenera_quan_impegnata ();string ls_cod_prodotto, ls_cod_tipo_det_ven, ls_sql, ls_flag_tipo_det_ven, ls_flag_blocco, ls_str
long ll_quan_impegnata, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
double ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa


st_2.text = "Azzeramento quantità impegnata dalle schede"
mle_1.text = space(200)
mle_1.text = "PRODOTTO        ANNO   REGISTRAZIONE   RIGA   NOTA" + CHAR(13) + CHAR(10)
update anag_prodotti
set    quan_impegnata = 0
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then return

declare cu_det_ord_ven dynamic cursor for sqlsa;

ls_sql = "SELECT anno_registrazione, num_registrazione, prog_riga_ord_ven, cod_tipo_det_ven, cod_prodotto, quan_ordine, quan_in_evasione, quan_evasa, flag_blocco FROM det_ord_ven WHERE det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' ORDER BY cod_prodotto, anno_registrazione, num_registrazione, prog_riga_ord_ven"

prepare sqlsa from :ls_sql;

open dynamic cu_det_ord_ven;

do while 1=1
   fetch cu_det_ord_ven
	into  :ll_anno_registrazione,
	      :ll_num_registrazione,
			:ll_prog_riga_ord_ven,
			:ls_cod_tipo_det_ven,   
         :ls_cod_prodotto,   
         :ld_quan_ordine,   
         :ld_quan_in_evasione,   
         :ld_quan_evasa,   
         :ls_flag_blocco  ;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

		st_2.text = ls_cod_prodotto
	   select tab_tipi_det_ven.flag_tipo_det_ven 
	   into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
	   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
	          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	   if sqlca.sqlcode = -1 then
	      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
	                 exclamation!, ok!)
			rollback;
			return
	   end if

   	if ls_flag_tipo_det_ven = "M" and ls_flag_blocco = "N" then
			update anag_prodotti  
			   set quan_impegnata = quan_impegnata + ( :ld_quan_ordine - :ld_quan_in_evasione - :ld_quan_evasa )
		    where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
		          anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
		                 exclamation!, ok!)
				ls_str = space(200)
				ls_str = replace(ls_str,1,16,ls_cod_prodotto)
				ls_str = replace(ls_str,17,4,string(ll_anno_registrazione))
				ls_str = replace(ls_str,25,6,string(ll_num_registrazione))
				ls_str = replace(ls_str,40,6,string(ll_prog_riga_ord_ven))
				ls_str = replace(ls_str,47,100,"In questo caso la quantità impegnata in scheda prodotto risulta < 0")
				mle_1.text = ls_str + char(13) + char(10)
				
			end if
		end if
loop
close cu_det_ord_ven;
st_2.text = "Elaborazione eseguita correttamente"
commit;
st_2.text = "Elaborazione eseguita correttamente; esguito COMMIT"
return

end subroutine

on w_rigenera_quan_impegnata.create
int iCurrent
call w_cs_xx_risposta::create
this.st_1=create st_1
this.cb_1=create cb_1
this.st_2=create st_2
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=mle_1
end on

on w_rigenera_quan_impegnata.destroy
call w_cs_xx_risposta::destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.mle_1)
end on

type st_1 from statictext within w_rigenera_quan_impegnata
int X=23
int Y=21
int Width=2378
int Height=181
boolean Enabled=false
boolean BringToTop=true
string Text="Rigenerazione Quantità Impeganata in Schede Prodotto Magazzino"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=67108864
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from commandbutton within w_rigenera_quan_impegnata
int X=1052
int Y=1161
int Width=366
int Height=81
int TabOrder=1
boolean BringToTop=true
string Text="&Rigenera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;g_mb.messagebox("APICE","Attenzione!! Per il buon fine dell'operazione di rigenerazione ~r~n" + &
                   "si consiglia di verificare che nessun client stia utilizzando APICE",Information!)

if g_mb.messagebox("APICE","Procedo con la rigenerazione ?",Question!,YesNoCancel!,2) = 1 then
	wf_rigenera_quan_impegnata()
end if
end event

type st_2 from statictext within w_rigenera_quan_impegnata
int X=23
int Y=1261
int Width=2378
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="Prodotto in elaborazione"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_1 from multilineedit within w_rigenera_quan_impegnata
int X=23
int Y=221
int Width=2378
int Height=921
int TabOrder=1
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

