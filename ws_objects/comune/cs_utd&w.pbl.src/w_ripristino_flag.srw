﻿$PBExportHeader$w_ripristino_flag.srw
forward
global type w_ripristino_flag from window
end type
type tab_1 from tab within w_ripristino_flag
end type
type tabpage_3 from userobject within tab_1
end type
type cb_3 from commandbutton within tabpage_3
end type
type mle_1 from multilineedit within tabpage_3
end type
type tabpage_3 from userobject within tab_1
cb_3 cb_3
mle_1 mle_1
end type
type tabpage_1 from userobject within tab_1
end type
type cbx_movimenti from checkbox within tabpage_1
end type
type cb_1 from commandbutton within tabpage_1
end type
type st_3 from statictext within tabpage_1
end type
type cbx_flag_gen_fat from checkbox within tabpage_1
end type
type st_2 from statictext within tabpage_1
end type
type em_num_documento from editmask within tabpage_1
end type
type em_anno_documento from editmask within tabpage_1
end type
type em_numeratore from editmask within tabpage_1
end type
type em_cod_documento from editmask within tabpage_1
end type
type st_1 from statictext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cbx_movimenti cbx_movimenti
cb_1 cb_1
st_3 st_3
cbx_flag_gen_fat cbx_flag_gen_fat
st_2 st_2
em_num_documento em_num_documento
em_anno_documento em_anno_documento
em_numeratore em_numeratore
em_cod_documento em_cod_documento
st_1 st_1
end type
type tabpage_2 from userobject within tab_1
end type
type cb_2 from commandbutton within tabpage_2
end type
type cbx_fat_movimenti from checkbox within tabpage_2
end type
type cbx_flag_contabilita from checkbox within tabpage_2
end type
type st_6 from statictext within tabpage_2
end type
type em_fat_num_documento from editmask within tabpage_2
end type
type em_fat_numeratore from editmask within tabpage_2
end type
type em_fat_anno_documento from editmask within tabpage_2
end type
type em_fat_cod_documento from editmask within tabpage_2
end type
type st_5 from statictext within tabpage_2
end type
type st_4 from statictext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_2 cb_2
cbx_fat_movimenti cbx_fat_movimenti
cbx_flag_contabilita cbx_flag_contabilita
st_6 st_6
em_fat_num_documento em_fat_num_documento
em_fat_numeratore em_fat_numeratore
em_fat_anno_documento em_fat_anno_documento
em_fat_cod_documento em_fat_cod_documento
st_5 st_5
st_4 st_4
end type
type tabpage_4 from userobject within tab_1
end type
type cb_baq_esegui from commandbutton within tabpage_4
end type
type cbx_baq_flag_gen_movimenti from checkbox within tabpage_4
end type
type cbx_baq_flag_gen_faq from checkbox within tabpage_4
end type
type st_baq_1 from statictext within tabpage_4
end type
type em_baq_num_documento from editmask within tabpage_4
end type
type st_baq_3 from statictext within tabpage_4
end type
type em_baq_anno_documento from editmask within tabpage_4
end type
type st_baq_2 from statictext within tabpage_4
end type
type tabpage_4 from userobject within tab_1
cb_baq_esegui cb_baq_esegui
cbx_baq_flag_gen_movimenti cbx_baq_flag_gen_movimenti
cbx_baq_flag_gen_faq cbx_baq_flag_gen_faq
st_baq_1 st_baq_1
em_baq_num_documento em_baq_num_documento
st_baq_3 st_baq_3
em_baq_anno_documento em_baq_anno_documento
st_baq_2 st_baq_2
end type
type tabpage_5 from userobject within tab_1
end type
type cb_4 from commandbutton within tabpage_5
end type
type cbx_faq_flag_movimenti from checkbox within tabpage_5
end type
type cbx_faq_flag_contabilita from checkbox within tabpage_5
end type
type st_faq_2 from statictext within tabpage_5
end type
type em_faq_num_documento from editmask within tabpage_5
end type
type em_faq_numeratore_documento from editmask within tabpage_5
end type
type st_faq_3 from statictext within tabpage_5
end type
type em_faq_anno_documento from editmask within tabpage_5
end type
type em_faq_cod_documento from editmask within tabpage_5
end type
type st_faq_1 from statictext within tabpage_5
end type
type tabpage_5 from userobject within tab_1
cb_4 cb_4
cbx_faq_flag_movimenti cbx_faq_flag_movimenti
cbx_faq_flag_contabilita cbx_faq_flag_contabilita
st_faq_2 st_faq_2
em_faq_num_documento em_faq_num_documento
em_faq_numeratore_documento em_faq_numeratore_documento
st_faq_3 st_faq_3
em_faq_anno_documento em_faq_anno_documento
em_faq_cod_documento em_faq_cod_documento
st_faq_1 st_faq_1
end type
type tab_1 from tab within w_ripristino_flag
tabpage_3 tabpage_3
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_ripristino_flag from window
integer width = 2661
integer height = 988
boolean titlebar = true
string title = "Ripristino Indicativi in Bolle Fatture"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
tab_1 tab_1
end type
global w_ripristino_flag w_ripristino_flag

on w_ripristino_flag.create
this.tab_1=create tab_1
this.Control[]={this.tab_1}
end on

on w_ripristino_flag.destroy
destroy(this.tab_1)
end on

event open;tab_1.tabpage_1.enabled = false
tab_1.tabpage_2.enabled = false
tab_1.tabpage_4.enabled = false
tab_1.tabpage_5.enabled = false
end event

type tab_1 from tab within w_ripristino_flag
integer y = 20
integer width = 2601
integer height = 860
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_3 tabpage_3
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

on tab_1.create
this.tabpage_3=create tabpage_3
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_3,&
this.tabpage_1,&
this.tabpage_2,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_3)
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2565
integer height = 736
long backcolor = 12632256
string text = "Avviso"
long tabtextcolor = 33554432
long tabbackcolor = 255
long picturemaskcolor = 536870912
cb_3 cb_3
mle_1 mle_1
end type

on tabpage_3.create
this.cb_3=create cb_3
this.mle_1=create mle_1
this.Control[]={this.cb_3,&
this.mle_1}
end on

on tabpage_3.destroy
destroy(this.cb_3)
destroy(this.mle_1)
end on

type cb_3 from commandbutton within tabpage_3
integer x = 2190
integer y = 652
integer width = 366
integer height = 81
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Accetto"
end type

event clicked;tab_1.tabpage_1.enabled = true
tab_1.tabpage_2.enabled = true
tab_1.tabpage_4.enabled = true
tab_1.tabpage_5.enabled = true
end event

type mle_1 from multilineedit within tabpage_3
integer x = 50
integer y = 52
integer width = 2505
integer height = 640
integer taborder = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "ATTENZIONE !!! SI INFORMANO GLI UTENTI CHE  L~'USO DELLE FUNZIONI PRESENTI IN QUESTA MASCHERA POTREBBE CAUSARE PROBLEMI DI DOPPIE FATTURAZIONI, DOPPI MOVIMENTI DI MAGAZZINO. LA CONSULTING&SOFTWARE DECLINA OGNI RESPONSABILITA~' DALL~'USO CHE GLI UTENTI FARANNO DELLE FUNZIONI CONTENUTE IN QUESTA MASCHERA."
boolean border = false
alignment alignment = center!
end type

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2565
integer height = 736
long backcolor = 12632256
string text = "Bolle Uscita"
long tabtextcolor = 33554432
long tabbackcolor = 16776960
long picturemaskcolor = 536870912
cbx_movimenti cbx_movimenti
cb_1 cb_1
st_3 st_3
cbx_flag_gen_fat cbx_flag_gen_fat
st_2 st_2
em_num_documento em_num_documento
em_anno_documento em_anno_documento
em_numeratore em_numeratore
em_cod_documento em_cod_documento
st_1 st_1
end type

on tabpage_1.create
this.cbx_movimenti=create cbx_movimenti
this.cb_1=create cb_1
this.st_3=create st_3
this.cbx_flag_gen_fat=create cbx_flag_gen_fat
this.st_2=create st_2
this.em_num_documento=create em_num_documento
this.em_anno_documento=create em_anno_documento
this.em_numeratore=create em_numeratore
this.em_cod_documento=create em_cod_documento
this.st_1=create st_1
this.Control[]={this.cbx_movimenti,&
this.cb_1,&
this.st_3,&
this.cbx_flag_gen_fat,&
this.st_2,&
this.em_num_documento,&
this.em_anno_documento,&
this.em_numeratore,&
this.em_cod_documento,&
this.st_1}
end on

on tabpage_1.destroy
destroy(this.cbx_movimenti)
destroy(this.cb_1)
destroy(this.st_3)
destroy(this.cbx_flag_gen_fat)
destroy(this.st_2)
destroy(this.em_num_documento)
destroy(this.em_anno_documento)
destroy(this.em_numeratore)
destroy(this.em_cod_documento)
destroy(this.st_1)
end on

type cbx_movimenti from checkbox within tabpage_1
integer x = 27
integer y = 472
integer width = 2002
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG MOVIMENTI~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type cb_1 from commandbutton within tabpage_1
integer x = 2171
integer y = 652
integer width = 389
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_flag_gen_fat, ls_flag_movimenti, ls_sql

setnull(ls_flag_gen_fat)
setnull(ls_flag_movimenti)

if tab_1.tabpage_1.em_cod_documento.text = "" or isnull(tab_1.tabpage_1.em_cod_documento.text) or &
   tab_1.tabpage_1.em_anno_documento.text = "" or isnull(tab_1.tabpage_1.em_anno_documento.text) or &
   tab_1.tabpage_1.em_numeratore.text = "" or isnull(tab_1.tabpage_1.em_numeratore.text) or &
   tab_1.tabpage_1.em_num_documento.text = "" or isnull(tab_1.tabpage_1.em_num_documento.text) then
	g_mb.messagebox("APICE","Inserire il numero fiscale del documento COMPLETO !!!!!! ")
	return
end if


if tab_1.tabpage_1.cbx_flag_gen_fat.thirdstate = true and tab_1.tabpage_1.cbx_movimenti.thirdstate = true then return
ls_sql = "update tes_bol_ven set "
if tab_1.tabpage_1.cbx_flag_gen_fat.thirdstate = false then
	if tab_1.tabpage_1.cbx_flag_gen_fat.checked then
		ls_sql = ls_sql + "flag_gen_fat = 'S'"
	else
		ls_sql = ls_sql + "flag_gen_fat = 'N'"
	end if
	if tab_1.tabpage_1.cbx_movimenti.thirdstate = false then ls_sql = ls_sql + ", "
end if

if tab_1.tabpage_1.cbx_movimenti.thirdstate = false then
	if tab_1.tabpage_1.cbx_movimenti.checked then
		ls_sql = ls_sql + "flag_movimenti = 'S'"
	else
		ls_sql = ls_sql + "flag_movimenti = 'N'"
	end if
end if

ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_documento = '" + tab_1.tabpage_1.em_cod_documento.text + "' " + &
         " and numeratore_documento = '" + tab_1.tabpage_1.em_numeratore.text + "' " + &
			" and anno_documento = " + tab_1.tabpage_1.em_anno_documento.text + &
         " and num_documento = " + tab_1.tabpage_1.em_num_documento.text

execute immediate :ls_sql;
if sqlca.sqlcode = 0 then
	commit;
	g_mb.messagebox("APICE","Operazione eseguita con successo!")
	f_scrivi_log("Ripristino flags = " + ls_sql)
else
	if sqlca.sqlcode = 100  then
		g_mb.messagebox("APICE", "Documento non trovato")
	else
		g_mb.messagebox("APICE", "Errore in ripristino flag_documento.~r~n"+sqlca.sqlerrtext)
	end if
	rollback;
end if
return
end event

type st_3 from statictext within tabpage_1
integer x = 599
integer y = 172
integer width = 1106
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ESEMPIO      BV -      2005      /     A    -     532"
boolean focusrectangle = false
end type

type cbx_flag_gen_fat from checkbox within tabpage_1
integer x = 27
integer y = 352
integer width = 2002
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG GENERATO FATTURA~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type st_2 from statictext within tabpage_1
integer x = 1298
integer y = 52
integer width = 46
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_num_documento from editmask within tabpage_1
integer x = 1490
integer y = 44
integer width = 274
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "######"
end type

type em_anno_documento from editmask within tabpage_1
integer x = 1065
integer y = 44
integer width = 229
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "####"
end type

type em_numeratore from editmask within tabpage_1
integer x = 1344
integer y = 44
integer width = 137
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "A"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type em_cod_documento from editmask within tabpage_1
integer x = 873
integer y = 44
integer width = 183
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type st_1 from statictext within tabpage_1
integer x = 27
integer y = 52
integer width = 832
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero Fiscale del Documento:"
boolean focusrectangle = false
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2565
integer height = 736
long backcolor = 12632256
string text = "Fatture Vendita"
long tabtextcolor = 33554432
long tabbackcolor = 16776960
long picturemaskcolor = 536870912
cb_2 cb_2
cbx_fat_movimenti cbx_fat_movimenti
cbx_flag_contabilita cbx_flag_contabilita
st_6 st_6
em_fat_num_documento em_fat_num_documento
em_fat_numeratore em_fat_numeratore
em_fat_anno_documento em_fat_anno_documento
em_fat_cod_documento em_fat_cod_documento
st_5 st_5
st_4 st_4
end type

on tabpage_2.create
this.cb_2=create cb_2
this.cbx_fat_movimenti=create cbx_fat_movimenti
this.cbx_flag_contabilita=create cbx_flag_contabilita
this.st_6=create st_6
this.em_fat_num_documento=create em_fat_num_documento
this.em_fat_numeratore=create em_fat_numeratore
this.em_fat_anno_documento=create em_fat_anno_documento
this.em_fat_cod_documento=create em_fat_cod_documento
this.st_5=create st_5
this.st_4=create st_4
this.Control[]={this.cb_2,&
this.cbx_fat_movimenti,&
this.cbx_flag_contabilita,&
this.st_6,&
this.em_fat_num_documento,&
this.em_fat_numeratore,&
this.em_fat_anno_documento,&
this.em_fat_cod_documento,&
this.st_5,&
this.st_4}
end on

on tabpage_2.destroy
destroy(this.cb_2)
destroy(this.cbx_fat_movimenti)
destroy(this.cbx_flag_contabilita)
destroy(this.st_6)
destroy(this.em_fat_num_documento)
destroy(this.em_fat_numeratore)
destroy(this.em_fat_anno_documento)
destroy(this.em_fat_cod_documento)
destroy(this.st_5)
destroy(this.st_4)
end on

type cb_2 from commandbutton within tabpage_2
integer x = 2171
integer y = 648
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_flag_contabilita, ls_flag_movimenti, ls_sql

setnull(ls_flag_contabilita)
setnull(ls_flag_movimenti)

if tab_1.tabpage_2.em_fat_cod_documento.text = "" or isnull(tab_1.tabpage_2.em_fat_cod_documento.text) or &
   tab_1.tabpage_2.em_fat_anno_documento.text = "" or isnull(tab_1.tabpage_2.em_fat_anno_documento.text) or &
   tab_1.tabpage_2.em_fat_numeratore.text = "" or isnull(tab_1.tabpage_2.em_fat_numeratore.text) or &
   tab_1.tabpage_2.em_fat_num_documento.text = "" or isnull(tab_1.tabpage_2.em_fat_num_documento.text) then
	g_mb.messagebox("APICE","Inserire il numero fiscale del documento COMPLETO !!!!!! ")
	return
end if


if tab_1.tabpage_2.cbx_flag_contabilita.thirdstate = true and tab_1.tabpage_2.cbx_fat_movimenti.thirdstate = true then return
ls_sql = "update tes_fat_ven set "
if tab_1.tabpage_2.cbx_flag_contabilita.thirdstate = false then
	if tab_1.tabpage_2.cbx_flag_contabilita.checked then
		ls_sql = ls_sql + "flag_contabilita = 'S'"
	else
		ls_sql = ls_sql + "flag_contabilita = 'N'"
	end if
	if tab_1.tabpage_2.cbx_fat_movimenti.thirdstate = false then ls_sql = ls_sql + ", "
end if

if tab_1.tabpage_2.cbx_fat_movimenti.thirdstate = false then
	if tab_1.tabpage_2.cbx_fat_movimenti.checked then
		ls_sql = ls_sql + "flag_movimenti = 'S'"
	else
		ls_sql = ls_sql + "flag_movimenti = 'N'"
	end if
end if

ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_documento = '" + tab_1.tabpage_2.em_fat_cod_documento.text + "' " + &
         " and numeratore_documento = '" + tab_1.tabpage_2.em_fat_numeratore.text + "' " + &
			" and anno_documento = " + tab_1.tabpage_2.em_fat_anno_documento.text + &
         " and num_documento = " + tab_1.tabpage_2.em_fat_num_documento.text

execute immediate :ls_sql;
if sqlca.sqlcode = 0 then
	commit;
	g_mb.messagebox("APICE","Operazione eseguita con successo!")
	f_scrivi_log("Ripristino flags = " + ls_sql)
else
	if sqlca.sqlcode = 100  then
		g_mb.messagebox("APICE", "Documento non trovato")
	else
		g_mb.messagebox("APICE", "Errore in ripristino flag_documento.~r~n"+sqlca.sqlerrtext)
	end if
	rollback;
end if
return
end event

type cbx_fat_movimenti from checkbox within tabpage_2
integer x = 50
integer y = 452
integer width = 2002
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG MOVIMENTI~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type cbx_flag_contabilita from checkbox within tabpage_2
integer x = 50
integer y = 332
integer width = 2002
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG CONTABILIZZATA~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type st_6 from statictext within tabpage_2
integer x = 1298
integer y = 52
integer width = 46
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_fat_num_documento from editmask within tabpage_2
integer x = 1499
integer y = 44
integer width = 274
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "######"
end type

type em_fat_numeratore from editmask within tabpage_2
integer x = 1353
integer y = 44
integer width = 137
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "A"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type em_fat_anno_documento from editmask within tabpage_2
integer x = 1088
integer y = 44
integer width = 206
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "####"
end type

type em_fat_cod_documento from editmask within tabpage_2
integer x = 896
integer y = 44
integer width = 183
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type st_5 from statictext within tabpage_2
integer x = 599
integer y = 172
integer width = 1079
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ESEMPIO      FV -      2005      /     A    -     532"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_4 from statictext within tabpage_2
integer x = 50
integer y = 52
integer width = 832
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero Fiscale del Documento:"
alignment alignment = center!
boolean focusrectangle = false
end type

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2565
integer height = 736
long backcolor = 12632256
string text = "Bolle Entrata"
long tabtextcolor = 33554432
long tabbackcolor = 65535
long picturemaskcolor = 536870912
string powertiptext = "Bolle Entrata"
cb_baq_esegui cb_baq_esegui
cbx_baq_flag_gen_movimenti cbx_baq_flag_gen_movimenti
cbx_baq_flag_gen_faq cbx_baq_flag_gen_faq
st_baq_1 st_baq_1
em_baq_num_documento em_baq_num_documento
st_baq_3 st_baq_3
em_baq_anno_documento em_baq_anno_documento
st_baq_2 st_baq_2
end type

on tabpage_4.create
this.cb_baq_esegui=create cb_baq_esegui
this.cbx_baq_flag_gen_movimenti=create cbx_baq_flag_gen_movimenti
this.cbx_baq_flag_gen_faq=create cbx_baq_flag_gen_faq
this.st_baq_1=create st_baq_1
this.em_baq_num_documento=create em_baq_num_documento
this.st_baq_3=create st_baq_3
this.em_baq_anno_documento=create em_baq_anno_documento
this.st_baq_2=create st_baq_2
this.Control[]={this.cb_baq_esegui,&
this.cbx_baq_flag_gen_movimenti,&
this.cbx_baq_flag_gen_faq,&
this.st_baq_1,&
this.em_baq_num_documento,&
this.st_baq_3,&
this.em_baq_anno_documento,&
this.st_baq_2}
end on

on tabpage_4.destroy
destroy(this.cb_baq_esegui)
destroy(this.cbx_baq_flag_gen_movimenti)
destroy(this.cbx_baq_flag_gen_faq)
destroy(this.st_baq_1)
destroy(this.em_baq_num_documento)
destroy(this.st_baq_3)
destroy(this.em_baq_anno_documento)
destroy(this.st_baq_2)
end on

type cb_baq_esegui from commandbutton within tabpage_4
integer x = 2171
integer y = 652
integer width = 389
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_flag_gen_fat, ls_flag_movimenti, ls_sql

setnull(ls_flag_gen_fat)
setnull(ls_flag_movimenti)

if tab_1.tabpage_4.em_baq_anno_documento.text = "" or isnull(tab_1.tabpage_4.em_baq_anno_documento.text) or &
   tab_1.tabpage_4.em_baq_num_documento.text = "" or isnull(tab_1.tabpage_4.em_baq_num_documento.text) then
	g_mb.messagebox("APICE","Inserire il numero fiscale del documento COMPLETO !!!!!! ")
	return
end if


if tab_1.tabpage_4.cbx_baq_flag_gen_faq.thirdstate = true and tab_1.tabpage_4.cbx_baq_flag_gen_movimenti.thirdstate = true then return
ls_sql = "update tes_bol_acq set "
if tab_1.tabpage_4.cbx_baq_flag_gen_faq.thirdstate = false then
	if tab_1.tabpage_4.cbx_baq_flag_gen_faq.checked then
		ls_sql = ls_sql + "flag_gen_fat = 'S'"
	else
		ls_sql = ls_sql + "flag_gen_fat = 'N'"
	end if
	if tab_1.tabpage_4.cbx_baq_flag_gen_movimenti.thirdstate = false then ls_sql = ls_sql + ", "
end if

if tab_1.tabpage_4.cbx_baq_flag_gen_movimenti.thirdstate = false then
	if tab_1.tabpage_4.cbx_baq_flag_gen_movimenti.checked then
		ls_sql = ls_sql + "flag_movimenti = 'S'"
	else
		ls_sql = ls_sql + "flag_movimenti = 'N'"
	end if
end if

ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' " + &
			" and anno_bolla_acq = " + tab_1.tabpage_4.em_baq_anno_documento.text + &
         " and num_bolla_acq = " + tab_1.tabpage_4.em_baq_num_documento.text

execute immediate :ls_sql;
if sqlca.sqlcode = 0 then
	commit;
	g_mb.messagebox("APICE","Operazione eseguita con successo!")
	f_scrivi_log("Ripristino flags = " + ls_sql)
else
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE", "Documento non trovato")
	else
		g_mb.messagebox("APICE", "Errore in ripristino flag_documento.~r~n"+sqlca.sqlerrtext)
	end if
	
	rollback;
end if
return
end event

type cbx_baq_flag_gen_movimenti from checkbox within tabpage_4
integer x = 23
integer y = 468
integer width = 2002
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG MOVIMENTI~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type cbx_baq_flag_gen_faq from checkbox within tabpage_4
integer x = 23
integer y = 348
integer width = 2002
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG GENERATO FATTURA~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type st_baq_1 from statictext within tabpage_4
integer x = 535
integer y = 168
integer width = 759
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ESEMPIO   2005      /      532"
boolean focusrectangle = false
end type

type em_baq_num_documento from editmask within tabpage_4
integer x = 1056
integer y = 36
integer width = 274
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "######"
end type

type st_baq_3 from statictext within tabpage_4
integer x = 1001
integer y = 48
integer width = 46
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_baq_anno_documento from editmask within tabpage_4
integer x = 759
integer y = 40
integer width = 229
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "####"
end type

type st_baq_2 from statictext within tabpage_4
integer x = 23
integer y = 48
integer width = 914
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero Registrazione Doc:"
boolean focusrectangle = false
end type

type tabpage_5 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 2565
integer height = 736
long backcolor = 12632256
string text = "Fatture Acquisto"
long tabtextcolor = 33554432
long tabbackcolor = 65535
long picturemaskcolor = 536870912
string powertiptext = "Fatture Acquisto"
cb_4 cb_4
cbx_faq_flag_movimenti cbx_faq_flag_movimenti
cbx_faq_flag_contabilita cbx_faq_flag_contabilita
st_faq_2 st_faq_2
em_faq_num_documento em_faq_num_documento
em_faq_numeratore_documento em_faq_numeratore_documento
st_faq_3 st_faq_3
em_faq_anno_documento em_faq_anno_documento
em_faq_cod_documento em_faq_cod_documento
st_faq_1 st_faq_1
end type

on tabpage_5.create
this.cb_4=create cb_4
this.cbx_faq_flag_movimenti=create cbx_faq_flag_movimenti
this.cbx_faq_flag_contabilita=create cbx_faq_flag_contabilita
this.st_faq_2=create st_faq_2
this.em_faq_num_documento=create em_faq_num_documento
this.em_faq_numeratore_documento=create em_faq_numeratore_documento
this.st_faq_3=create st_faq_3
this.em_faq_anno_documento=create em_faq_anno_documento
this.em_faq_cod_documento=create em_faq_cod_documento
this.st_faq_1=create st_faq_1
this.Control[]={this.cb_4,&
this.cbx_faq_flag_movimenti,&
this.cbx_faq_flag_contabilita,&
this.st_faq_2,&
this.em_faq_num_documento,&
this.em_faq_numeratore_documento,&
this.st_faq_3,&
this.em_faq_anno_documento,&
this.em_faq_cod_documento,&
this.st_faq_1}
end on

on tabpage_5.destroy
destroy(this.cb_4)
destroy(this.cbx_faq_flag_movimenti)
destroy(this.cbx_faq_flag_contabilita)
destroy(this.st_faq_2)
destroy(this.em_faq_num_documento)
destroy(this.em_faq_numeratore_documento)
destroy(this.st_faq_3)
destroy(this.em_faq_anno_documento)
destroy(this.em_faq_cod_documento)
destroy(this.st_faq_1)
end on

type cb_4 from commandbutton within tabpage_5
integer x = 2171
integer y = 656
integer width = 389
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_flag_contabilita, ls_flag_movimenti, ls_sql

setnull(ls_flag_contabilita)
setnull(ls_flag_movimenti)

if tab_1.tabpage_5.em_faq_cod_documento.text = "" or isnull(tab_1.tabpage_5.em_faq_cod_documento.text) or &
   tab_1.tabpage_5.em_faq_anno_documento.text = "" or isnull(tab_1.tabpage_5.em_faq_anno_documento.text) or &
   tab_1.tabpage_5.em_faq_numeratore_documento.text = "" or isnull(tab_1.tabpage_5.em_faq_numeratore_documento.text) or &
   tab_1.tabpage_5.em_faq_num_documento.text = "" or isnull(tab_1.tabpage_5.em_faq_num_documento.text) then
	g_mb.messagebox("APICE","Inserire il numero fiscale del documento COMPLETO !!!!!! ")
	return
end if


if tab_1.tabpage_5.cbx_faq_flag_contabilita.thirdstate = true and tab_1.tabpage_5.cbx_faq_flag_movimenti.thirdstate = true then return
ls_sql = "update tes_fat_acq set "
if tab_1.tabpage_5.cbx_faq_flag_contabilita.thirdstate = false then
	if tab_1.tabpage_5.cbx_faq_flag_contabilita.checked then
		ls_sql = ls_sql + "flag_contabilita = 'S'"
	else
		ls_sql = ls_sql + "flag_contabilita = 'N'"
	end if
	if tab_1.tabpage_5.cbx_faq_flag_movimenti.thirdstate = false then ls_sql = ls_sql + ", "
end if

if tab_1.tabpage_5.cbx_faq_flag_movimenti.thirdstate = false then
	if tab_1.tabpage_5.cbx_faq_flag_movimenti.checked then
		ls_sql = ls_sql + "flag_fat_confermata = 'S'"
	else
		ls_sql = ls_sql + "flag_fat_confermata = 'N'"
	end if
end if

ls_sql = ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_doc_origine = '" + tab_1.tabpage_5.em_faq_cod_documento.text + "' " + &
         " and numeratore_doc_origine = '" + tab_1.tabpage_5.em_faq_numeratore_documento.text + "' " + &
			" and anno_doc_origine = " + tab_1.tabpage_5.em_faq_anno_documento.text + &
         " and num_doc_origine = " + tab_1.tabpage_5.em_faq_num_documento.text

execute immediate :ls_sql;
if sqlca.sqlcode = 0 then
	commit;
	g_mb.messagebox("APICE","Operazione eseguita con successo!")
	f_scrivi_log("Ripristino flags = " + ls_sql)
else
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE", "Documento non trovato")
	else
		g_mb.messagebox("APICE", "Errore in ripristino flag_documento.~r~n"+sqlca.sqlerrtext)
	end if
	rollback;
end if
return
end event

type cbx_faq_flag_movimenti from checkbox within tabpage_5
integer x = 50
integer y = 460
integer width = 2002
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG MOVIMENTI~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type cbx_faq_flag_contabilita from checkbox within tabpage_5
integer x = 50
integer y = 340
integer width = 2002
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Impostare lo stato al quale si vuole mettere il  ~"FLAG CONTABILIZZATA~""
boolean lefttext = true
boolean threestate = true
boolean thirdstate = true
end type

type st_faq_2 from statictext within tabpage_5
integer x = 599
integer y = 180
integer width = 1079
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ESEMPIO      FA -      2005      /     A    -     532"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_faq_num_documento from editmask within tabpage_5
integer x = 1499
integer y = 52
integer width = 274
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "######"
end type

type em_faq_numeratore_documento from editmask within tabpage_5
integer x = 1353
integer y = 52
integer width = 137
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type st_faq_3 from statictext within tabpage_5
integer x = 1298
integer y = 60
integer width = 46
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_faq_anno_documento from editmask within tabpage_5
integer x = 1088
integer y = 52
integer width = 206
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
string mask = "####"
end type

type em_faq_cod_documento from editmask within tabpage_5
integer x = 896
integer y = 52
integer width = 183
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = stringmask!
end type

type st_faq_1 from statictext within tabpage_5
integer x = 50
integer y = 60
integer width = 832
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero Fiscale del Documento:"
alignment alignment = center!
boolean focusrectangle = false
end type

