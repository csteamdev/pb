﻿$PBExportHeader$w_sblocca_mov_magazzino.srw
forward
global type w_sblocca_mov_magazzino from window
end type
type cb_sblocca from commandbutton within w_sblocca_mov_magazzino
end type
type cb_blocca from commandbutton within w_sblocca_mov_magazzino
end type
type st_3 from statictext within w_sblocca_mov_magazzino
end type
type st_2 from statictext within w_sblocca_mov_magazzino
end type
type st_1 from statictext within w_sblocca_mov_magazzino
end type
type em_movimento from editmask within w_sblocca_mov_magazzino
end type
type em_prodotto from editmask within w_sblocca_mov_magazzino
end type
type em_anno from editmask within w_sblocca_mov_magazzino
end type
end forward

global type w_sblocca_mov_magazzino from window
integer width = 1605
integer height = 748
boolean titlebar = true
string title = "Utility per Blocco/Sblocco Movimento Magazzino."
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_sblocca cb_sblocca
cb_blocca cb_blocca
st_3 st_3
st_2 st_2
st_1 st_1
em_movimento em_movimento
em_prodotto em_prodotto
em_anno em_anno
end type
global w_sblocca_mov_magazzino w_sblocca_mov_magazzino

on w_sblocca_mov_magazzino.create
this.cb_sblocca=create cb_sblocca
this.cb_blocca=create cb_blocca
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_movimento=create em_movimento
this.em_prodotto=create em_prodotto
this.em_anno=create em_anno
this.Control[]={this.cb_sblocca,&
this.cb_blocca,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_movimento,&
this.em_prodotto,&
this.em_anno}
end on

on w_sblocca_mov_magazzino.destroy
destroy(this.cb_sblocca)
destroy(this.cb_blocca)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_movimento)
destroy(this.em_prodotto)
destroy(this.em_anno)
end on

event open;/*

questa utility consente lo sblocco dei movimenti di magazzino automatici.

in questo modo il movimento puà essere modificato (anche se referenziato 

dato che mantiene lo stesso numero)o cancellato (se non referenziato).

realizzata per progettotenda da EnMe 27/9/2006

*/
end event

type cb_sblocca from commandbutton within w_sblocca_mov_magazzino
integer x = 873
integer y = 460
integer width = 343
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "SBLOCCA"
end type

event clicked;string ls_cod_prodotto, ls_cod_tipo_movimento
long   ll_anno_registrazione,ll_cont

ll_anno_registrazione = long(em_anno.text)

ls_cod_prodotto = em_prodotto.text
ls_cod_tipo_movimento = em_movimento.text

select count(*)
into   :ll_cont
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione and
		 cod_tipo_movimento = :ls_cod_tipo_movimento and
		 cod_prodotto = :ls_cod_prodotto and
		 flag_manuale = 'N';

if g_mb.messagebox("APICE", "Trovati " + string(ll_cont) + " movimenti: procedo con SBLOCCO? ", question!,yesno!,2 ) = 2 then return

g_mb.messagebox("APICE", "Attendere fino alla fine: terminare l'applicazione potrebbe bloccare tutta l'azienda")

update mov_magazzino
set    flag_manuale = 'S'
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione and
		 cod_tipo_movimento = :ls_cod_tipo_movimento and
		 cod_prodotto = :ls_cod_prodotto and
		 flag_manuale = 'N';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", sqlca.sqlerrtext)
	rollback;
else
	commit;
	g_mb.messagebox("APICE", "Elaborazione eseguita con successo")
end if

return
end event

type cb_blocca from commandbutton within w_sblocca_mov_magazzino
integer x = 137
integer y = 460
integer width = 343
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "BLOCCA"
end type

event clicked;string ls_cod_prodotto, ls_cod_tipo_movimento
long   ll_anno_registrazione,ll_cont

ll_anno_registrazione = long(em_anno.text)
ls_cod_prodotto = em_prodotto.text
ls_cod_tipo_movimento = em_movimento.text

select count(*)
into   :ll_cont
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione and
		 cod_tipo_movimento = :ls_cod_tipo_movimento and
		 cod_prodotto = :ls_cod_prodotto and
		 flag_manuale = 'S';

if g_mb.messagebox("APICE", "Trovati " + string(ll_cont) + " movimenti: procedo con BLOCCO ? ", question!,yesno!,2 ) = 2 then return

g_mb.messagebox("APICE", "Attendere fino alla fine: terminare l'applicazione potrebbe bloccare tutta l'azienda")

update mov_magazzino
set    flag_manuale = 'N'
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione and
		 cod_tipo_movimento = :ls_cod_tipo_movimento and
		 cod_prodotto = :ls_cod_prodotto and
		 flag_manuale = 'S';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", sqlca.sqlerrtext)
	rollback;
else
	commit;
	g_mb.messagebox("APICE", "Elaborazione eseguita con successo")
end if



return
end event

type st_3 from statictext within w_sblocca_mov_magazzino
integer x = 91
integer y = 180
integer width = 590
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Codice Movimento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_sblocca_mov_magazzino
integer x = 91
integer y = 308
integer width = 590
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Codice Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_sblocca_mov_magazzino
integer x = 169
integer y = 60
integer width = 512
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Anno Registrazione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_movimento from editmask within w_sblocca_mov_magazzino
integer x = 709
integer y = 172
integer width = 311
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_prodotto from editmask within w_sblocca_mov_magazzino
integer x = 709
integer y = 296
integer width = 759
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type em_anno from editmask within w_sblocca_mov_magazzino
integer x = 704
integer y = 48
integer width = 297
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2006"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
end type

