﻿$PBExportHeader$w_secure_id.srw
forward
global type w_secure_id from w_cs_xx_principale
end type
type st_1 from statictext within w_secure_id
end type
type cb_1 from commandbutton within w_secure_id
end type
end forward

global type w_secure_id from w_cs_xx_principale
integer width = 1449
integer height = 616
st_1 st_1
cb_1 cb_1
end type
global w_secure_id w_secure_id

on w_secure_id.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_1
end on

on w_secure_id.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_1)
end on

type st_1 from statictext within w_secure_id
integer x = 55
integer y = 80
integer width = 1262
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Imposta in tutti i processi il secure_id"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_secure_id
integer x = 55
integer y = 224
integer width = 1298
integer height = 176
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Processa"
end type

event clicked;uo_secure_id luo_secure

luo_secure = create uo_secure_id

try 
	luo_secure.uof_secure_clienti()
	
	g_mb.success("Processo terminato con successo")
	
catch(SqlException e)
	g_mb.error(e.getMessage())
finally
	destroy luo_secure
end try



end event

