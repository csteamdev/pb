﻿$PBExportHeader$w_sel_inv_excel.srw
$PBExportComments$Finestra selezione inventario per esportare dati in Excel
forward
global type w_sel_inv_excel from w_cs_xx_risposta
end type
type dw_selezione from uo_cs_xx_dw within w_sel_inv_excel
end type
type cb_conferma from commandbutton within w_sel_inv_excel
end type
end forward

global type w_sel_inv_excel from w_cs_xx_risposta
int Width=2286
int Height=445
boolean TitleBar=true
string Title="Inserimento Dati"
dw_selezione dw_selezione
cb_conferma cb_conferma
end type
global w_sel_inv_excel w_sel_inv_excel

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 							 
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_mov_mag", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_sel_inv_excel.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_selezione=create dw_selezione
this.cb_conferma=create cb_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_selezione
this.Control[iCurrent+2]=cb_conferma
end on

on w_sel_inv_excel.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_selezione)
destroy(this.cb_conferma)
end on

type dw_selezione from uo_cs_xx_dw within w_sel_inv_excel
int X=23
int Y=21
int Width=2218
int Height=221
string DataObject="d_selezione_campi"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_new;call super::pcd_new;dw_selezione.setitem(1, "data_inizio", date("01/01/" + string(year(today()) + 1)))
end event

type cb_conferma from commandbutton within w_sel_inv_excel
int X=1875
int Y=241
int Width=357
int Height=81
int TabOrder=2
string Text="&Conferma"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_selezione.getitemstring(1, "cod_tipo_mov_mag")
s_cs_xx.parametri.parametro_data_1 = datetime(dw_selezione.getitemdate(1, "data_inizio"))

close(w_sel_inv_excel)


end event

