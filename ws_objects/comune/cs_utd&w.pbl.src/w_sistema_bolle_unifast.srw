﻿$PBExportHeader$w_sistema_bolle_unifast.srw
forward
global type w_sistema_bolle_unifast from window
end type
type cb_2 from commandbutton within w_sistema_bolle_unifast
end type
type cb_1 from commandbutton within w_sistema_bolle_unifast
end type
type st_2 from statictext within w_sistema_bolle_unifast
end type
type st_1 from statictext within w_sistema_bolle_unifast
end type
type dw_1 from datawindow within w_sistema_bolle_unifast
end type
end forward

global type w_sistema_bolle_unifast from window
integer width = 3689
integer height = 1572
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_2 cb_2
cb_1 cb_1
st_2 st_2
st_1 st_1
dw_1 dw_1
end type
global w_sistema_bolle_unifast w_sistema_bolle_unifast

on w_sistema_bolle_unifast.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_2=create st_2
this.st_1=create st_1
this.dw_1=create dw_1
this.Control[]={this.cb_2,&
this.cb_1,&
this.st_2,&
this.st_1,&
this.dw_1}
end on

on w_sistema_bolle_unifast.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_1)
end on

type cb_2 from commandbutton within w_sistema_bolle_unifast
integer x = 2697
integer y = 1280
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "carica dati"
end type

event clicked;dw_1.settransobject(sqlca)
dw_1.retrieve()
end event

type cb_1 from commandbutton within w_sistema_bolle_unifast
integer x = 3136
integer y = 1276
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ESEGUI"
end type

event clicked;string ls_cod_tipo_det_ven, ls_flag_tipo_det_ven
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven

for ll_i = 1 to dw_1.rowcount()
	if dw_1.getitemstring(ll_i, "tes_bol_ven_flag_movimenti") = "N" then continue
	
	if not isnull(dw_1.getitemnumber(ll_i, "det_bol_ven_anno_registrazione_mov_mag")) then continue 
	
	ls_cod_tipo_det_ven = dw_1.getitemstring(ll_i, "det_bol_ven_cod_tipo_det_ven")
	
	if isnull(ls_cod_tipo_det_ven) then continue
	
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	// continuo solo se prodotto a magazzino
	if ls_flag_tipo_det_ven <> "M" then continue 
	
	// quindi a questi punto ho trovato una riga di una bolla confermata, ma che ha una riga di prodotto a magazzino senza movimento.
	
	update det_bol_ven
	set    anno_registrazione_mov_mag = 2002, num_registrazione_mov_mag = 2
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_bol_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("update mov_magazzino", sqlca.sqlerrtext)
		rollback;
	end if
	
	commit;
	
next
end event

type st_2 from statictext within w_sistema_bolle_unifast
integer x = 14
integer y = 124
integer width = 3566
integer height = 108
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Questo causa molti problemi nel sistema di MRP; viene impostato il movimento di magazzino 2002/2 su tutti"
boolean focusrectangle = false
end type

type st_1 from statictext within w_sistema_bolle_unifast
integer x = 18
integer width = 3566
integer height = 108
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "questa procedura sistema  i DDT di Unifast per i quali esiste il flag_movimenti=S, ma le cui righe non hanno il movimento di magazzino."
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_sistema_bolle_unifast
integer x = 23
integer y = 256
integer width = 3561
integer height = 1008
integer taborder = 10
string title = "none"
string dataobject = "d_sistema_bolle_unifast"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

