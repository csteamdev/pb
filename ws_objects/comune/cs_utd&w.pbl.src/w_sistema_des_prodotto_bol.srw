﻿$PBExportHeader$w_sistema_des_prodotto_bol.srw
forward
global type w_sistema_des_prodotto_bol from w_cs_xx_principale
end type
type dw_categorie from uo_cs_xx_dw within w_sistema_des_prodotto_bol
end type
type cb_1 from commandbutton within w_sistema_des_prodotto_bol
end type
end forward

global type w_sistema_des_prodotto_bol from w_cs_xx_principale
int Width=3255
int Height=1237
boolean TitleBar=true
string Title="SISTEMA DESCRIZIONI PRODOTTO IN FATTURA"
dw_categorie dw_categorie
cb_1 cb_1
end type
global w_sistema_des_prodotto_bol w_sistema_des_prodotto_bol

type variables

end variables

event pc_setwindow;call super::pc_setwindow;dw_categorie.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nonew + &
									 c_nodelete+ &
									 c_nomodify, &
                            c_default)

end event

on w_sistema_des_prodotto_bol.create
int iCurrent
call w_cs_xx_principale::create
this.dw_categorie=create dw_categorie
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_categorie
this.Control[iCurrent+2]=cb_1
end on

on w_sistema_des_prodotto_bol.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_categorie)
destroy(this.cb_1)
end on

type dw_categorie from uo_cs_xx_dw within w_sistema_des_prodotto_bol
int X=23
int Y=21
int Width=3178
int Height=1001
int TabOrder=10
string DataObject="d_sistema_des_prodotto_fat"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type cb_1 from commandbutton within w_sistema_des_prodotto_bol
int X=2835
int Y=1041
int Width=366
int Height=81
int TabOrder=11
boolean BringToTop=true
string Text="&Esegui"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto
long ll_i, ll_rows, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven


ll_rows = dw_categorie.rowcount()
for ll_i = 1 to ll_rows
	ls_cod_prodotto = dw_categorie.getitemstring(ll_i, "cod_prodotto")	
	ls_des_prodotto = dw_categorie.getitemstring(ll_i, "des_prodotto")	
	if not isnull(ls_cod_prodotto) and ( ls_des_prodotto = "" or isnull(ls_des_prodotto) ) then
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Errore DB","Errore in select prodotto " + ls_cod_prodotto)
		else
			ll_anno_registrazione = dw_categorie.getitemnumber(ll_i, "anno_registrazione")	
			ll_num_registrazione = dw_categorie.getitemnumber(ll_i, "num_registrazione")	
			ll_prog_riga_bol_ven = dw_categorie.getitemnumber(ll_i, "prog_riga_bol_ven")	
			
			update det_bol_ven
			set    des_prodotto = :ls_des_prodotto
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_bol_ven = :ll_prog_riga_bol_ven;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Errore DB","Errore in update bolla " + string(ll_anno_registrazione) + "-" + string(ll_num_registrazione) + "-" + string(ll_prog_riga_bol_ven))
			else
				commit;
			end if
		end if
	end if
next

commit;

g_mb.messagebox("Conversione righe bolla","Elaborazione eseguita correttamente!", information!)
end event

