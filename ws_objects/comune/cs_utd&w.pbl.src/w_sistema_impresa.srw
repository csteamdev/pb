﻿$PBExportHeader$w_sistema_impresa.srw
forward
global type w_sistema_impresa from window
end type
type st_1 from statictext within w_sistema_impresa
end type
type cb_1 from commandbutton within w_sistema_impresa
end type
type dw_1 from datawindow within w_sistema_impresa
end type
end forward

global type w_sistema_impresa from window
integer width = 3566
integer height = 1648
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_1 st_1
cb_1 cb_1
dw_1 dw_1
end type
global w_sistema_impresa w_sistema_impresa

on w_sistema_impresa.create
this.st_1=create st_1
this.cb_1=create cb_1
this.dw_1=create dw_1
this.Control[]={this.st_1,&
this.cb_1,&
this.dw_1}
end on

on w_sistema_impresa.destroy
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.dw_1)
end on

event open;DW_1.settransobject(sqlci)
dw_1.retrieve()
end event

type st_1 from statictext within w_sistema_impresa
integer x = 2610
integer y = 360
integer width = 814
integer height = 164
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_sistema_impresa
integer x = 2473
integer y = 52
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;long ll_id_mastro,ll_id_sottoconto,ll_id_conto, ll_i
string ls_codice_conto,ls_nuovo_codice,ls_codice_mastro,ls_codice_sottoconto


for ll_i = 1 to dw_1.rowcount()

//	ll_id_mastro = dw_1.getitemnumber(ll_i, "id_mastro")
	ll_id_sottoconto = dw_1.getitemnumber(ll_i, "id_sottomastro")
	ll_id_conto  = dw_1.getitemnumber(ll_i, "id_conto")
	ls_codice_conto = dw_1.getitemstring(ll_i, "codice")
	
	if len(ls_codice_conto) < 5 then
	
		select codice, id_mastro
		into :ls_codice_sottoconto, :ll_id_mastro
		from sottomastro
		where id_sottomastro = :ll_id_sottoconto
		using sqlci;
		
		select codice
		into :ls_codice_mastro
		from mastro
		where id_mastro = :ll_id_mastro
		using sqlci;
		
		
		ls_nuovo_codice = ls_codice_mastro + ls_codice_sottoconto + ls_codice_conto
		
		update conto
		set codice = :ls_nuovo_codice
		where id_conto = :ll_id_conto
		using sqlci;
		
		if sqlci.sqlcode = -1 then
			messagebox("",sqlci.sqlerrtext)
			rollback;
		end if
		st_1.text = ls_nuovo_codice
		Yield()
	end if
next


commit;
end event

type dw_1 from datawindow within w_sistema_impresa
integer x = 23
integer y = 8
integer width = 2299
integer height = 1484
integer taborder = 10
string title = "none"
string dataobject = "d_conto_impresa"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

