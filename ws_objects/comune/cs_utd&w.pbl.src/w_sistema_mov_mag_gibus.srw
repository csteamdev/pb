﻿$PBExportHeader$w_sistema_mov_mag_gibus.srw
forward
global type w_sistema_mov_mag_gibus from window
end type
type st_2 from statictext within w_sistema_mov_mag_gibus
end type
type cb_3 from commandbutton within w_sistema_mov_mag_gibus
end type
type em_1 from editmask within w_sistema_mov_mag_gibus
end type
type cb_2 from commandbutton within w_sistema_mov_mag_gibus
end type
type st_1 from statictext within w_sistema_mov_mag_gibus
end type
type cb_1 from commandbutton within w_sistema_mov_mag_gibus
end type
end forward

global type w_sistema_mov_mag_gibus from window
integer width = 3022
integer height = 1168
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
st_2 st_2
cb_3 cb_3
em_1 em_1
cb_2 cb_2
st_1 st_1
cb_1 cb_1
end type
global w_sistema_mov_mag_gibus w_sistema_mov_mag_gibus

on w_sistema_mov_mag_gibus.create
this.st_2=create st_2
this.cb_3=create cb_3
this.em_1=create em_1
this.cb_2=create cb_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.st_2,&
this.cb_3,&
this.em_1,&
this.cb_2,&
this.st_1,&
this.cb_1}
end on

on w_sistema_mov_mag_gibus.destroy
destroy(this.st_2)
destroy(this.cb_3)
destroy(this.em_1)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

type st_2 from statictext within w_sistema_mov_mag_gibus
integer x = 791
integer y = 488
integer width = 2034
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 16777215
string text = "Pronto"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_sistema_mov_mag_gibus
integer x = 46
integer y = 464
integer width = 695
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "2 Gen-Chiudi Commessa"
end type

event clicked;long ll_riga,ll_anno_registrazione,ll_anno_reg_ord_ven,ll_num_reg_ord_ven,ll_prog_riga_ord_ven, ll_num_commessa,li_risposta,ll_null,ll_test_commessa
integer ll_anno_commessa
string ls_cod_deposito_forzato, ls_str, ls_cod_deposito,ls_cod_prodotto,ls_cod_tipo_ord_ven,ls_errore,ls_null
dec{4} ld_quantita
uo_excel luo_excel
uo_funzioni_1 luo_funzioni_commesse

luo_excel = create uo_excel
luo_funzioni_commesse = create uo_funzioni_1
setnull(ls_null)
ll_riga = 1
if luo_excel.uof_open(st_1.text, false, false) then
	
	do while true
		ll_riga ++
		
		st_2.text = string(ll_riga)
		Yield()
		
		
		ls_str = string( luo_excel.uof_read(ll_riga, "B"))
		
		if isnull(ls_str) or len(ls_str) < 1 then exit		// fine del foglio
		
		ls_cod_deposito_forzato = string( luo_excel.uof_read(ll_riga, "A"))
		
		ll_anno_reg_ord_ven = long(ls_str)
		ll_num_reg_ord_ven = long( luo_excel.uof_read(ll_riga, "C"))
		ll_prog_riga_ord_ven = long( luo_excel.uof_read(ll_riga, "D"))
		
		ls_cod_tipo_ord_ven = string( luo_excel.uof_read(ll_riga, "E"))
		ls_cod_deposito = string( luo_excel.uof_read(ll_riga, "F"))
		ls_cod_prodotto = string( luo_excel.uof_read(ll_riga, "G"))
		ld_quantita = dec(luo_excel.uof_read(ll_riga, "H"))
		setnull(ll_null)
		
		select cod_tipo_ord_ven
		into :ls_cod_tipo_ord_ven
		from tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_reg_ord_ven and
					num_registrazione = :ll_num_reg_ord_ven;
		if sqlca.sqlcode <> 0 then
			messagebox("ERRORE IN RICERCA ORDINE "+string(ll_num_reg_ord_ven), sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		setnull(ll_test_commessa)
		
		select anno_commessa
		into :ll_test_commessa
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_reg_ord_ven and
					num_registrazione = :ll_num_reg_ord_ven and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven;
					
		if ll_test_commessa > 0 or not isnull(ll_test_commessa) then
			st_2.text = string(ll_riga) + "Ord. " + string(ll_num_reg_ord_ven) + " SALTO ...."
			continue
		end if
	
		
		// generazione commessa
		
		st_2.text = string(ll_riga) + "Ord. " + string(ll_num_reg_ord_ven) + " GEN COMM in corso ...."
		Yield()
		
		if not isnull(ls_cod_deposito_forzato) and len(ls_cod_deposito_forzato) > 0 then
			luo_funzioni_commesse.ib_forza_deposito_scarico_mp = true
			luo_funzioni_commesse.is_cod_deposito_scarico_mp = ls_cod_deposito_forzato
		end if
		
		setnull(ll_anno_commessa)
		setnull(ll_num_commessa)
										
		li_risposta = luo_funzioni_commesse.uof_genera_commesse(	ls_cod_tipo_ord_ven, &
																		ll_anno_reg_ord_ven,  &
																		ll_num_reg_ord_ven,&
																		ll_prog_riga_ord_ven, &
																		ll_null, &
																		ll_anno_commessa, &
																		ll_num_commessa, &
																		ls_errore)
																		
		if li_risposta = 0 then
			// sistemo la quantità nella commessa.
			update 	anag_commesse
			set 		quan_ordine = :ld_quantita
			where 	cod_azienda =:s_cs_xx.cod_azienda and
						anno_commessa = :ll_anno_commessa and
						num_commessa = :ll_num_commessa;
			if sqlca.sqlcode <> 0 then
				messagebox("ERRORE IN AGGIORNAMENTO QUANTITA' COMMESSA " + string(ll_num_commessa), sqlca.sqlerrtext)
				rollback;
				return
			end if

			
			st_2.text = string(ll_riga) + "Comm. " + string(ll_num_commessa) + " AVANZ COMM in corso ...."
			Yield()

			li_risposta = luo_funzioni_commesse.uof_avanza_commessa( ll_anno_commessa, ll_num_commessa, ls_null, ls_errore)
			
			if li_risposta <> 0 then
				messagebox("ERRORE IN CHIUSURA COMMESSA " + string(ll_num_commessa), sqlca.sqlerrtext)
				rollback;
				return
			end if
				
			luo_funzioni_commesse.ib_forza_deposito_scarico_mp = false
			setnull(luo_funzioni_commesse.is_cod_deposito_scarico_mp)
			
		else
			st_2.text = "ERRORE " + string(ll_riga) + "Ord. " + string(ll_num_reg_ord_ven) + " GEN COMM in corso ...."
			messagebox("ERRORE IN GENERAZIONE COMMESSA "+string(ll_num_reg_ord_ven), sqlca.sqlerrtext)
			rollback;
			return
		end if
	loop
	
end if

messagebox("Fine", "Elaborazione eseguita correttamente")

destroy luo_excel
destroy luo_funzioni_commesse

commit;


end event

type em_1 from editmask within w_sistema_mov_mag_gibus
integer x = 718
integer y = 244
integer width = 2094
integer height = 100
integer taborder = 20
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,##0"
end type

type cb_2 from commandbutton within w_sistema_mov_mag_gibus
integer x = 2469
integer y = 64
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "path"
end type

event clicked;string docpath, docname[]

integer i, li_cnt, li_rtn, li_filenum

 

li_rtn = GetFileOpenName("Select File", docpath, docname[], "XLS","Excel (*.xls),*.xls,Excel 2012 (*.xlsx),*.xlsx,All Files (*.*), *.*")

if li_rtn > 0 then
	st_1.text = docpath
end if
end event

type st_1 from statictext within w_sistema_mov_mag_gibus
integer x = 27
integer y = 64
integer width = 2405
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_sistema_mov_mag_gibus
integer x = 50
integer y = 236
integer width = 544
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 Cambia Deposito"
end type

event clicked;long ll_riga,ll_elaboro, ll_anno_movimento, ll_num_movimento
string ls_cod_deposito, ls_cod_deposito_giusto
uo_excel luo_excel

luo_excel = create uo_excel
ll_riga = long(em_1.text)
if luo_excel.uof_open(st_1.text, false, false) then
	
	do while true
		ll_riga ++
		
		em_1.text = string(ll_riga)
		Yield()
		
		ls_cod_deposito = luo_excel.uof_read(ll_riga, "A")

		if isnull(ls_cod_deposito) or len(ls_cod_deposito) < 1 then exit

		ll_anno_movimento  = luo_excel.uof_read(ll_riga, "N")
		ll_num_movimento  = luo_excel.uof_read(ll_riga, "O")
		ls_cod_deposito_giusto  = luo_excel.uof_read(ll_riga, "P")
		
		update mov_magazzino
		set cod_deposito = :ls_cod_deposito_giusto
		where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_movimento and
					num_registrazione = :ll_num_movimento;
		if sqlca.sqlcode = -1 then
			messagebox("ERRORE UPDATE", sqlca.sqlerrtext)
			rollback;
		else
			commit;
		end if
			
	loop
	
end if

destroy luo_excel

commit;


end event

