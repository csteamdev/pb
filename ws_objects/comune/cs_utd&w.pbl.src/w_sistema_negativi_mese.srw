﻿$PBExportHeader$w_sistema_negativi_mese.srw
forward
global type w_sistema_negativi_mese from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_sistema_negativi_mese
end type
type dw_giacenze_sistema from datawindow within w_sistema_negativi_mese
end type
type cb_esporta_2 from commandbutton within w_sistema_negativi_mese
end type
type cb_esporta from commandbutton within w_sistema_negativi_mese
end type
type dw_giacenze_2 from datawindow within w_sistema_negativi_mese
end type
type cb_giacenze_mensili_2 from commandbutton within w_sistema_negativi_mese
end type
type st_1 from statictext within w_sistema_negativi_mese
end type
type st_file from statictext within w_sistema_negativi_mese
end type
type em_data_rif from editmask within w_sistema_negativi_mese
end type
type sle_cod_deposito from singlelineedit within w_sistema_negativi_mese
end type
type sle_cod_prodotto_test from singlelineedit within w_sistema_negativi_mese
end type
type cb_giacenze_mensili from commandbutton within w_sistema_negativi_mese
end type
type cb_leggi_e_sistema from commandbutton within w_sistema_negativi_mese
end type
type st_log from statictext within w_sistema_negativi_mese
end type
type sle_cod_prodotto_like from singlelineedit within w_sistema_negativi_mese
end type
type st_count from statictext within w_sistema_negativi_mese
end type
type cb_carica_prodotti from commandbutton within w_sistema_negativi_mese
end type
type dw_giacenze from datawindow within w_sistema_negativi_mese
end type
end forward

global type w_sistema_negativi_mese from w_cs_xx_principale
integer width = 4306
integer height = 2732
string title = "Sistemazione negativi mensili"
cb_1 cb_1
dw_giacenze_sistema dw_giacenze_sistema
cb_esporta_2 cb_esporta_2
cb_esporta cb_esporta
dw_giacenze_2 dw_giacenze_2
cb_giacenze_mensili_2 cb_giacenze_mensili_2
st_1 st_1
st_file st_file
em_data_rif em_data_rif
sle_cod_deposito sle_cod_deposito
sle_cod_prodotto_test sle_cod_prodotto_test
cb_giacenze_mensili cb_giacenze_mensili
cb_leggi_e_sistema cb_leggi_e_sistema
st_log st_log
sle_cod_prodotto_like sle_cod_prodotto_like
st_count st_count
cb_carica_prodotti cb_carica_prodotti
dw_giacenze dw_giacenze
end type
global w_sistema_negativi_mese w_sistema_negativi_mese

type variables
string		is_prodotti[], is_vuoto[]
string		is_cod_tipo_mov_scarico = "SMP"
string		is_cod_tipo_mov_vendita = "SVE"
string		is_path_origine

//questa variabile va resettata ad ogni inizio elaborazione prodotto
//conserva il mese più alto in cui ho trovato giacenza negativa (durante la prima elaborazione) e che ho sistemato a ZERO
//mi servirà nella seconda passata, quando dovrò compensare le qta che ho tolto dagli SMP (infatti la giacenza a fine anno non deve cambiare)
//se ad es. ho portato a zero una qta mensile negativa a giugno e a settembre, per compensare (aumentare gli SMP, ove possibile) devo partire da ottobre ...
integer	ii_mese_succ_zero = 0

integer	ii_mese_minima = 1
decimal	id_giacenza_minima = 999999999999

string is_mesi[]
end variables

forward prototypes
public function integer wf_leggi_e_sistema (ref string fs_errore)
public function integer wf_carica_prodotti (ref string fs_errore)
public function integer wf_giacenza_mese (uo_magazzino fuo_mag, integer fi_anno, integer fi_mese, string fs_cod_prodotto, string fs_cod_deposito, ref decimal fd_giacenza, ref string fs_errore)
public function integer wf_scrivi_log (string fs_testo, string fs_file)
public function integer wf_inventari_mensili (boolean fb_live, integer fi_anno, string fs_cod_prodotto, string fs_cod_deposito, ref decimal fd_giacenza[], ref string fs_errore)
public subroutine wf_giacenza_minima (decimal fd_giacenze[], integer fi_mese_inizio)
public function integer wf_compensa_qta_prodotto (string fs_file, string fs_cod_prodotto, string fs_cod_deposito, integer fi_anno_rif, integer fi_mese, ref decimal fd_quan_da_compensare, ref decimal fd_giacenze_mese[], ref boolean fb_movimento_fatto, ref string fs_errore)
public function integer wf_correggi_negativi_mese (string fs_file, string fs_cod_prodotto, string fs_cod_deposito, integer fi_anno_rif, integer fi_mese, ref decimal fd_quan_da_correggere, ref decimal fd_quan_da_compensare, ref decimal fd_giacenze_mese[], ref string fs_errore)
public function integer wf_esamina_giacenze_dopo (string fs_cod_prodotto, decimal fd_giacenza[], ref string fs_errore)
public subroutine wf_leggi_e_sistema_prodotto (uo_magazzino fuo_mag, string fs_file, string fs_cod_prodotto, string fs_cod_deposito, datetime fdt_data_rif, ref integer fi_return, ref string fs_errore)
end prototypes

public function integer wf_leggi_e_sistema (ref string fs_errore);string				ls_filtro_prodotto, ls_errore, ls_cod_prodotto_cu, ls_cod_deposito, ls_file, ls_file_saltati
datetime			ldt_data_chiusura_annuale, ldt_data_riferimento
long				ll_tot, ll_index
integer			li_mese
uo_magazzino	luo_mag
integer			li_ret

ll_tot = upperbound(is_prodotti[])

if ll_tot<=0 then
	fs_errore = "Array prodotti da elaborare vuoto!"
	return -1
end if

ls_cod_deposito = sle_cod_deposito.text

if ls_cod_deposito="" or isnull(ls_cod_deposito) then
	fs_errore = "Selezionare il deposito!"
	return -1
end if

ldt_data_riferimento = datetime(date(em_data_rif.text), 00:00:00)

if isnull(ldt_data_riferimento) or year(date(ldt_data_riferimento)) <=2000 then
	fs_errore = "Inserire una data di riferimento!"
	return -1
end if

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if ldt_data_riferimento < ldt_data_chiusura_annuale then
	fs_errore = "Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale!"
	return -1
end if

if not g_mb.confirm("Procedere alla sistemazione dei prodotti caricati?") then return 2

ls_file = is_path_origine + "sistema_negativi_mese.log"
ls_file_saltati = is_path_origine + "saltati_negativi_mese.log"
filedelete(ls_file)
filedelete(ls_file_saltati)
dw_giacenze_sistema.reset()

wf_scrivi_log("QUI SARANNO ELENCATI I PRODOTTI CHE LA PROCEDURA NON RIESCE A SISTEMARE!!!", ls_file_saltati)

luo_mag = create uo_magazzino

for ll_index=1 to ll_tot
	Yield()
	ls_cod_prodotto_cu = is_prodotti[ll_index]
	
	st_log.text = "Elaborazione prodotto "+ls_cod_prodotto_cu+" in corso ...      ("+string(ll_index)+" di "+string(ll_tot)+")"
	
	wf_scrivi_log("Inizio elaborazione Prodotto "+ls_cod_prodotto_cu+ "----------------------------------------------", ls_file)
	
	fs_errore = ""
	wf_leggi_e_sistema_prodotto(luo_mag, ls_file, ls_cod_prodotto_cu, ls_cod_deposito, ldt_data_riferimento, li_ret, fs_errore)
	
	if li_ret<0 then
		//in fs_errore il messaggio di errore
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" elaborato con errori:"+fs_errore, ls_file)
		rollback;
		return -1
	
	
	elseif li_ret=500 then
		//valore imprevisto per la variabile ii_mese_succ_zero
		rollback;
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+ ": valore imprevisto per la variabile ii_mese_succ_zero; valore = "+string(ii_mese_succ_zero), ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+ ": valore imprevisto per la variabile ii_mese_succ_zero; valore = "+string(ii_mese_succ_zero), ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
	
	
	elseif li_ret=401 then
		//prodotto elaborato ma in maniera errata, infatti dopo un ricontrollo alcune giacenze sono risultate negative
		//comunque faccio commit, lo controllerò dopo
		commit;
		//wf_scrivi_log(fs_errore, ls_file_saltati)
		
		wf_scrivi_log(fs_errore, ls_file)
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" elaborato con MA CON PROBLEMI DI GIACENZA NEGATIVA, faccio comunque COMMIT, ma dopo vai a controllare!", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------", ls_file)
		continue
	
	
	elseif li_ret=400 then
		//prodotto presenta a dicembre, prima dell'elaborazione una giacenza negativa, salto subito
		rollback;
		wf_scrivi_log(fs_errore, ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log(fs_errore, ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
	
	
	elseif li_ret=300 then
		//prodotto con giacenza negativa a dicembre sin da prima della procedura
		rollback;
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+ ": presenta una giacenza negativa a dicembre sin dall'inizio, quindi neanche lo elaboro ...", ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+ ": presenta una giacenza negativa a dicembre sin dall'inizio, quindi lo salto.", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
		

	elseif li_ret=200 then
		//messaggio personalizzato giacenza restata negativa
		rollback;
		wf_scrivi_log(fs_errore, ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log(fs_errore, ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
		
		
	elseif li_ret=100 then
		//il prodotto non aveva giacenze mensili NEGATIVE, scrivi nel log questo
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" non necessita di elaborazione. Non presenta giacenze negative in nessun mese!", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
		
	
	elseif li_ret=2 then
		//le giacenze negative mensili sono state sistemate, ma non sono riuscito a compensare per non variare la giacenza a fine anno
		//annullo questo prodotto e proseguo, scrivendo nel log che è stato saltato
		rollback;
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" saltato. Gli scarichi (SMP+SVE) erano stati sistemati ma non si è riusciti a compensare (SMP+SVE) per non variare la giacenza a fine anno!", ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" saltato e fatto ROLLBACK. Gli scarichi (SMP+SVE) erano stati sistemati ma non si è riusciti a compensare (SMP+SVE) per non variare la giacenza a fine anno!", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
		
	
	elseif li_ret=1 then
		//non sono riuscito a sistemare le giacenze negative mese per mese
		//annullo questo prodotto e proseguo, scrivendo nel log che è stato saltato
		rollback;
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" saltato. Gli scarichi (SMP+SVE) non sono stati sufficienti per sistemare le giacenze negative mensili!", ls_file_saltati)
		
		//lo scrivo anche nella procedura
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" saltato e fatto ROLLBACK. Gli scarichi (SMP+SVE) non sono stati sufficienti per sistemare le giacenze negative mensili!", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------------------------------------------", ls_file)
		continue
	
	
	else
		//tutto OK, c'è da fare un Commit
		commit;
		wf_scrivi_log("Prodotto "+ls_cod_prodotto_cu+" elaborato con successo e fatto COMMIT.", ls_file)
		wf_scrivi_log("------------------------------------------------------------------------------------------", ls_file)
		
	end if
	
next

destroy luo_mag

return 0


end function

public function integer wf_carica_prodotti (ref string fs_errore);string				ls_filtro_prodotto, ls_cod_prodotto
datetime			ldt_data_chiusura_annuale, ldt_data_riferimento
long				ll_i

is_prodotti[] = is_vuoto[]
ldt_data_riferimento = datetime(date(em_data_rif.text), 00:00:00)

if isnull(ldt_data_riferimento) or year(date(ldt_data_riferimento)) <=2000 then
	fs_errore = "Inserire una data di riferimento!"
	return -1
end if

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if ldt_data_riferimento < ldt_data_chiusura_annuale then
	fs_errore = "Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale!"
	return -1
end if

ls_filtro_prodotto = sle_cod_prodotto_like.text
if isnull(ls_filtro_prodotto) or ls_filtro_prodotto="" then ls_filtro_prodotto="%"

declare cur_prod cursor for 
select distinct m.cod_prodotto
from mov_magazzino as m
join anag_prodotti as p on 	p.cod_azienda=m.cod_azienda and
									p.cod_prodotto=m.cod_prodotto
where	m.cod_azienda = :s_cs_xx.cod_azienda and
         	m.cod_prodotto like :ls_filtro_prodotto and
         	m.data_registrazione <= :ldt_data_riferimento and
         	m.data_registrazione > :ldt_data_chiusura_annuale and
			p.flag_articolo_fiscale='S' and p.flag_lifo='S' and p.flag_classe_abc='A'
order by m.cod_prodotto;

ll_i = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in OPEN cursore cur_prod~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cur_prod into :ls_cod_prodotto;

	if sqlca.sqlcode < 0 then
		fs_errore="Errore durante lettura elenco prodotti~r~n "+sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 100 then exit

	yield()

	if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		ll_i += 1
		is_prodotti[ll_i] = ls_cod_prodotto
		st_log.text = "Caricamento prodotto "+ls_cod_prodotto+" nell'array da rielaborare ... "
	end if

loop

close cur_prod;

ll_i = upperbound(is_prodotti[])
st_count.text = "Tot. Prodotti caricati: "+string(ll_i)
st_log.text = "Pronto!"

return 0


end function

public function integer wf_giacenza_mese (uo_magazzino fuo_mag, integer fi_anno, integer fi_mese, string fs_cod_prodotto, string fs_cod_deposito, ref decimal fd_giacenza, ref string fs_errore);integer			li_ret, li_gg
datetime			ldt_fine
string				ls_where, ls_chiave[]
decimal			ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
long				ll_num_stock, ll_indice, ll_new



choose case fi_mese
	case 4,6,9,11
		ldt_fine = datetime(date(fi_anno, fi_mese, 30), 00:00:00)

	case 2 //febbraio
		//gestire anno bisesto, anno funesto
		li_gg = 28
		
		if guo_functions.uof_anno_bisestile(fi_anno) then
			li_gg = 29
		end if
		ldt_fine = datetime(date(fi_anno, fi_mese, li_gg), 00:00:00)
		
	case else
		ldt_fine = datetime(date(fi_anno, fi_mese, 31), 00:00:00)
end choose

//st_log.text = "Elaborazione giacenza mensile al "+string(ldt_fine, "dd/mm/yyyy")+ " per il prodotto "+fs_cod_prodotto+" nel deposito "+fs_cod_deposito

//l'argomento n°6, deve valere "D" se voglio la giacenza raggruppata per depositi
//se voglio invece la giacenza complessi metto "N" e poi per ottenere la giacenza faccio la seguente operazione
//								ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]

//li_ret = fuo_mag.uof_saldo_prod_date_decimal(	fs_cod_prodotto, &
//																ldt_fine, &
//																ls_where, &
//																ld_quant_val[], &
//																fs_errore, &
//																"D", &
//																ls_chiave[], &
//																ld_giacenza_stock[], &
//																ld_costo_medio_stock[],&
//																ld_quan_costo_medio_stock[])

fuo_mag.is_considera_depositi_fornitori = "I"
fuo_mag.is_cod_depositi_in = "'"+fs_cod_deposito+"'"
li_ret = fuo_mag.uof_saldo_prod_date_decimal(		fs_cod_prodotto,&
																	ldt_fine, &
																	ls_where, &
																	ld_quant_val, &
																	fs_errore, &
																	"N", &
																	ls_chiave[], &
																	ld_giacenza_stock[], &
																	ld_costo_medio_stock[], &
																	ld_quan_costo_medio_stock[])


////in ls_chiave ci sono i vari depositi trovati
//ll_num_stock = upperbound(ls_chiave[])
//
//fd_giacenza = 0
//
////ciclo l'array dei depositi per beccare quello che mi interessa
//for ll_indice = 1 to ll_num_stock
//	if ls_chiave[ll_indice] = fs_cod_deposito then
//		//deposito trovato
//		//metto la quantità letta dalla funzione
//		fd_giacenza = ld_giacenza_stock[ll_indice]
//		exit
//	end if
//next
////se il for precedente non trova il deposito voluto allora
////in fd_giacenza rimarrà 0: OK

fd_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
if isnull(fd_giacenza) then fd_giacenza = 0




return 0
end function

public function integer wf_scrivi_log (string fs_testo, string fs_file);integer li_file

li_file = FileOpen(fs_file,LineMode!, Write!, LockWrite!, Append!)
filewrite(li_file, string(datetime(today(),now()), "dd/mm/yy hh:mm:ss          ")+fs_testo)
fileclose(li_file)

return 0
end function

public function integer wf_inventari_mensili (boolean fb_live, integer fi_anno, string fs_cod_prodotto, string fs_cod_deposito, ref decimal fd_giacenza[], ref string fs_errore);integer			li_i, li_ret, li_gg
datetime			ldt_fine[]
uo_magazzino	luo_mag
string				ls_where, ls_chiave[]
decimal			ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
long				ll_num_stock, ll_indice, ll_new



if fb_live then
	ll_new = dw_giacenze_sistema.insertrow(0)
	dw_giacenze_sistema.setitem(ll_new, "cod_prodotto", fs_cod_prodotto)
	dw_giacenze_sistema.setitem(ll_new, "cod_deposito", fs_cod_deposito)
	dw_giacenze_sistema.setitem(ll_new, "tipo_riga", "G")
	
	dw_giacenze_sistema.scrolltorow(ll_new)
end if


//fd_giacenza[1] = -5
//fd_giacenza[2] = -5
//fd_giacenza[3] = -9
//fd_giacenza[4] = -7
//fd_giacenza[5] = -1
//fd_giacenza[6] = -2
//fd_giacenza[7] = -3
//fd_giacenza[8] = -2
//fd_giacenza[9] = 6
//fd_giacenza[10] = -0
//fd_giacenza[11] = 0
//fd_giacenza[12] = 2
//
//for li_i = 1 to 12 
//	if fb_live then dw_giacenze_sistema.setitem(ll_new, "gz_"+string(li_i), fd_giacenza[li_i])
//next



luo_mag = CREATE uo_magazzino

//	faccio 12 inventari per il prodotto, ciascuno in data fine
for li_i = 1 to 12 
	fd_giacenza[][li_i] = 0
	wf_giacenza_mese(luo_mag, fi_anno, li_i, fs_cod_prodotto, fs_cod_deposito, fd_giacenza[][li_i], fs_errore)
	
	if fb_live then dw_giacenze_sistema.setitem(ll_new, "gz_"+string(li_i), fd_giacenza[li_i])
next

destroy luo_mag

return 0
end function

public subroutine wf_giacenza_minima (decimal fd_giacenze[], integer fi_mese_inizio);integer			li_index

ii_mese_minima = 0
id_giacenza_minima = 999999999999

for li_index=fi_mese_inizio to 12
	if fd_giacenze[li_index] < id_giacenza_minima then
		ii_mese_minima = li_index
		id_giacenza_minima = fd_giacenze[li_index]
	end if
next

end subroutine

public function integer wf_compensa_qta_prodotto (string fs_file, string fs_cod_prodotto, string fs_cod_deposito, integer fi_anno_rif, integer fi_mese, ref decimal fd_quan_da_compensare, ref decimal fd_giacenze_mese[], ref boolean fb_movimento_fatto, ref string fs_errore);integer			li_anno_registrazione_cu
long				ll_num_registrazione_cu
decimal			ld_quan_movimento_cu, ld_quan_movimento_new, ld_quan_gz_new, ls_qta_usaegetta
integer			li_index
string				ls_cod_tipo_movimento_cu
datetime			ldt_data_reg_cu

fb_movimento_fatto = false

//SMP o SVE, dai la priorità alla modifica del movimento SMP, se possibile
declare cur_scarichi cursor for 
select 	anno_registrazione, num_registrazione, cod_tipo_movimento, quan_movimento, data_registrazione
from mov_magazzino  
where	cod_azienda=:s_cs_xx.cod_azienda and
         	cod_prodotto=:fs_cod_prodotto and
			cod_deposito=:fs_cod_deposito and
         	year(data_registrazione)=:fi_anno_rif and
         	month(data_registrazione)=:fi_mese and
			(cod_tipo_movimento=:is_cod_tipo_mov_scarico or cod_tipo_movimento=:is_cod_tipo_mov_vendita)
order by cod_tipo_movimento asc, data_registrazione desc;


open cur_scarichi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in OPEN cursore cur_scarichi: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cur_scarichi into :li_anno_registrazione_cu, :ll_num_registrazione_cu, :ls_cod_tipo_movimento_cu, :ld_quan_movimento_cu, :ldt_data_reg_cu;
	
	if sqlca.sqlcode < 0 then
		fs_errore="Errore durante lettura scarichi del prodotto "+fs_cod_prodotto+": "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	//prosegui
	if fd_giacenze_mese[fi_mese] < fd_quan_da_compensare then
		//la giacenza del mese, da sola, NON basta ad azzerare LA COMPENSAZIONE DA EFFETTUARE
		//aumento la quantità dell'ultimo scarico del mese fino a quanto permesso, cioè pari alla giacenza E POI ESCO SUBITO DAL CURSORE !!!!!!!
		//(in tal caso la giacenza di questo mese andrà a ZERO) e cercherò di annullare la compensazione residua, provando con scarichi
		//in un mese successivo, ove la giacenza lo permetta
		ls_qta_usaegetta = fd_giacenze_mese[fi_mese]

	else
		//la giacenza del mese, da sola, già basta ad azzerare LA COMPENSAZIONE DA EFFETTUARE
		//aumento la quantità dell'ultimo scarico del mese di una quantità pari alla compensazione  E POI ESCO SUBITO DAL CURSORE !!!!!!!
		//(in tal caso la giacenza di questo mese non andrà in ogni caso a ZERO) e per questo prodotto avrò finito
		ls_qta_usaegetta = fd_quan_da_compensare
		
	end if
	
	ld_quan_movimento_new = ld_quan_movimento_cu + ls_qta_usaegetta
	ld_quan_gz_new = - ls_qta_usaegetta
	fd_quan_da_compensare = fd_quan_da_compensare - ls_qta_usaegetta
	
	
	//salvo la quantità movimento prima di modificarla, per eventuali ripristini successivi
	update mov_magazzino
	set _quan_movimento_old_2=quan_movimento
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione_cu and
				num_registrazione=:ll_num_registrazione_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in pre-salvataggio 2 di qta_mov per prodotto "+fs_cod_prodotto+ " : "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	//aggiorno la quantità movimento
	update mov_magazzino
	set quan_movimento=:ld_quan_movimento_new
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione_cu and
				num_registrazione=:ll_num_registrazione_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in aggiornamento qta_mov per prodotto "+fs_cod_prodotto+ " : "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	wf_scrivi_log(	"Aggiornato movimento in seconda passata "+string(li_anno_registrazione_cu)+"/"+string(ll_num_registrazione_cu)+&
						":  quan_movimento="+string(ld_quan_movimento_new, "###,###,##0.0000") +&
						"        (era "+string(ld_quan_movimento_cu, "###,###,##0.0000")+") "+&
						" cod_tipo_movimento: "+ls_cod_tipo_movimento_cu+" data_registrazione: "+string(ldt_data_reg_cu, "dd/mm/yyyy"), &
						fs_file)
	
	fb_movimento_fatto = true
	
//	//riallinea giacenze mensili successive (somma algebrica), avendo aumentato un SMP le giacenze mensili successive (e del mese corrente) subiranno una diminuzione
//	//in ogni caso la diminuzione non sarà tale da avere mesi che ritornano in giacenza negativa
//	for li_index=fi_mese to upperbound(fd_giacenze_mese[])
//		fd_giacenze_mese[li_index] += ld_quan_gz_new		//(somma algebrica, infatti ld_quan_gz_new è un valore negativo)
//	next
	
	//riallinea giacenze mensili precedenti (somma algebrica)
	for li_index=12 to fi_mese step -1
		fd_giacenze_mese[li_index] += ld_quan_gz_new		//(somma algebrica, infatti ld_quan_gz_new è un valore negativo)
	next
	
	//ora in ogni caso esci dal cursore ...
	//se ho annullato la quantità da compensare già in questo mese, all'uscita di questa funzione troverò fd_quan_da_compensare a ZERO
	//e quindi non avrò necessità di cercare altre giacenze positive nei mesi successivi
	//altrimenti provo con il movimento di scarico successivo nel mese
	exit
	
loop

close cur_scarichi;


return 0
end function

public function integer wf_correggi_negativi_mese (string fs_file, string fs_cod_prodotto, string fs_cod_deposito, integer fi_anno_rif, integer fi_mese, ref decimal fd_quan_da_correggere, ref decimal fd_quan_da_compensare, ref decimal fd_giacenze_mese[], ref string fs_errore);integer			li_anno_registrazione_cu, li_mese_mov
long				ll_num_registrazione_cu
decimal			ld_quan_movimento_cu, ld_quan_movimento_new, ld_quan_gz_new, ls_qta_usaegetta
integer			li_index
datetime			ldt_data_reg_cu
string				ls_cod_tipo_movimento_cu

//ordino prima per  cod tipo movimento, poi data registrazione
//quindi prima elaborerò gli SMP, da fine mese in su e poi gli SVE, sempre da fine mese in su)
declare cur_scarichi cursor for 
select 	anno_registrazione, num_registrazione, cod_tipo_movimento, quan_movimento, data_registrazione
from mov_magazzino  
where	cod_azienda=:s_cs_xx.cod_azienda and
         	cod_prodotto=:fs_cod_prodotto and
			cod_deposito=:fs_cod_deposito and
         	year(data_registrazione)=:fi_anno_rif and
         	month(data_registrazione)<=:fi_mese and
			(cod_tipo_movimento=:is_cod_tipo_mov_scarico or cod_tipo_movimento=:is_cod_tipo_mov_vendita) and
			quan_movimento>0
order by cod_tipo_movimento asc, data_registrazione desc;


open cur_scarichi;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in OPEN cursore cur_scarichi: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cur_scarichi into :li_anno_registrazione_cu, :ll_num_registrazione_cu, :ls_cod_tipo_movimento_cu, :ld_quan_movimento_cu, :ldt_data_reg_cu;
	
	if sqlca.sqlcode < 0 then
		fs_errore="Errore durante lettura scarichi del prodotto "+fs_cod_prodotto+": "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	//prosegui
	if ld_quan_movimento_cu < fd_quan_da_correggere then
		//la quantità di questo movimento di scarico, da sola non basta ad azzerare il saldo negativo maturato fino a questo momento
		//metto a ZERO la quantità di questo movimento, e proseguo con il successivo scarico del mese, se ce ne sono
		ls_qta_usaegetta = ld_quan_movimento_cu
		
	else
		//la quantità di questo movimento di scarico, da sola già basta ad azzerare il saldo negativo maturato fino a questo momento
		//diminuisco la quantità di questo movimento di una quantità pari al saldo negativo, in modo da annullarlo
		//e posso quindi poi uscire da questo ciclo e passare a controllare i mesi successivi
		ls_qta_usaegetta = fd_quan_da_correggere
	
	end if
	
	ld_quan_movimento_new = ld_quan_movimento_cu - ls_qta_usaegetta
	ld_quan_gz_new = ls_qta_usaegetta
	fd_quan_da_compensare = fd_quan_da_compensare + ls_qta_usaegetta
	fd_quan_da_correggere = fd_quan_da_correggere - ls_qta_usaegetta
	
	
	//salvo la quantità movimento prima di modificarla
	update mov_magazzino
	set _quan_movimento_old=quan_movimento
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione_cu and
				num_registrazione=:ll_num_registrazione_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in pre-salvataggio qta_mov per prodotto "+fs_cod_prodotto+ " : "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	//aggiorno la quantità movimento, salvandomi quella precedente, per eventuali ripristini successivi
	update mov_magazzino
	set quan_movimento=:ld_quan_movimento_new
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione_cu and
				num_registrazione=:ll_num_registrazione_cu;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in aggiornamento qta_mov per prodotto "+fs_cod_prodotto+ " : "+sqlca.sqlerrtext
		close cur_scarichi;
		return -1
	end if
	
	wf_scrivi_log(	"Aggiornato movimento "+string(li_anno_registrazione_cu)+"/"+string(ll_num_registrazione_cu)+&
						":  quan_movimento="+string(ld_quan_movimento_new, "###,###,##0.0000") +&
						"        (era "+string(ld_quan_movimento_cu, "###,###,##0.0000")+")  "+&
						" cod_tipo_movimento: "+ls_cod_tipo_movimento_cu+" data_registrazione: "+string(ldt_data_reg_cu, "dd/mm/yyyy"), &
						fs_file)
	
	//devo verificare quale è il mese del movimento che è stato aggiornato,
	//perchè poi devo ri-aggiornare le giacenze a partire da questo mese i poi
	
	li_mese_mov = month(date(ldt_data_reg_cu))
	
	//riallinea giacenze mensili successive (ma anche quella del mese corrente)
	for li_index=li_mese_mov to upperbound(fd_giacenze_mese[])
	//for li_index=fi_mese to upperbound(fd_giacenze_mese[])
		fd_giacenze_mese[li_index] += ld_quan_gz_new
	next
	
	//se ho annullato il saldo negativo già in questo mese, esco, 
	//altrimenti provo con il movimento di scarico successivo nel mese
	if fd_quan_da_correggere=0 then 
		exit
	end if
	
loop

close cur_scarichi;

//verifica se la giacenza nel mese è rimasta negativa
//se si allora salta il prodotto e segnala nel log
if fd_giacenze_mese[fi_mese] < 0 then
	return 1
end if


return 0
end function

public function integer wf_esamina_giacenze_dopo (string fs_cod_prodotto, decimal fd_giacenza[], ref string fs_errore);integer li_i
boolean lb_negativo = false
string ls_mese[]

fs_errore = ""

for li_i = 1 to 12 
	if fd_giacenza[li_i] < 0 then
		
		if not lb_negativo then fs_errore = "Il prodotto "+fs_cod_prodotto+" dopo la procedura presenta quantità mensili negative: "
		
		lb_negativo = true
		
		fs_errore += " mese "+is_mesi[li_i] + ": " + string(fd_giacenza[li_i], "###,###,##0.0000")
		
	end if
next

if fs_errore<>"" then return -1

return 0
end function

public subroutine wf_leggi_e_sistema_prodotto (uo_magazzino fuo_mag, string fs_file, string fs_cod_prodotto, string fs_cod_deposito, datetime fdt_data_rif, ref integer fi_return, ref string fs_errore);integer			li_mese, li_anno_rif, li_ret, li_mese_minima
decimal			ld_giacenze_mese[], ld_quan_da_correggere, ld_quan_da_compensare, ld_giacenza_minima
boolean			lb_giacenza_negativa_mensile
boolean			lb_SMP_OK, lb_Compensa_OK, lb_movimento_fatto
string				ls_messaggio

li_mese = 1
li_anno_rif = year(date(fdt_data_rif))
ld_quan_da_correggere = 0
ld_quan_da_compensare = 0
ii_mese_succ_zero = 1
lb_giacenza_negativa_mensile = false
lb_SMP_OK = true
lb_Compensa_OK = true
lb_movimento_fatto = false

//preparo l'array degli inventari mensili nel deposito per il prodotto
wf_inventari_mensili(true, li_anno_rif, fs_cod_prodotto, fs_cod_deposito, ld_giacenze_mese[], fs_errore)

wf_scrivi_log("Giacenza al 31/12:  "+string(ld_giacenze_mese[12], "###,###,##0.0000"), fs_file)

//se la giacenza del prodotto a dicembre risulta negativa salta il prodotto e segnala
if ld_giacenze_mese[12] < 0 then
	fi_return = 400
	fs_errore = "Prodotto "+fs_cod_prodotto+" prima della procedura presenta già una giacenza al 31/12 negativa, pertanto lo salto dall'inizio!"
	return
	//return 400
end if

//##################################################################################
// provo con i movimenti SMP, poi con gli SVE
do while li_mese <= 12
	//leggo la giacenza del prodotto alla fine del mese li_mese: ld_giacenze_mese[li_mese]
	
	//in ld_giacenza la giacenza mensile
	if ld_giacenze_mese[li_mese]<0 then
		//metto anche questa quantità da correggere, oltre all'eventualmente ancora presente come residui negativi mesi precedenti
		//ovviamente, essendo negativa la prendo co segno positivo
		ld_quan_da_correggere += abs(ld_giacenze_mese[li_mese])
		lb_giacenza_negativa_mensile = true
		
		ii_mese_succ_zero = li_mese + 1
		
	end if

	if ld_quan_da_correggere>0 then
		//chiamo una funzione apposita per cercare di azzerare/dimininuire il + possibile questo saldo negativo
		//la funzione esamina gli scarichi presenti nel mese e cerca di diminuirne/azzerarne la quantità
		//a partire dai movimenti di fine mese fino ad inizio mese
		//in caso di variazioni di quantità scarichi, occoore anche riallineare le giacenze mensili a partire dal mese corrente (ecco perchè passo byref ld_giacenze_mese[] )
		//poi mi serve una variabile ld_quan_da_compensare per cercare di riequilibare le quantità annuali e fare in modo che la giacenza a fine anno resti invariata
		
		li_ret = wf_correggi_negativi_mese(	fs_file, fs_cod_prodotto, fs_cod_deposito, li_anno_rif, li_mese, &
														ref ld_quan_da_correggere, ref ld_quan_da_compensare, ref ld_giacenze_mese[], ref fs_errore)
		
		if li_ret<0 then
			//errore critico
			fi_return = -1
			return
			//return -1
			
		elseif li_ret=1 then
			//non sono riuscito ad azzerare la giacenza nel mese, pertanto è rimasta negativa
			fs_errore = 	"Il prodotto "+fs_cod_prodotto+" è restato con giacenza negativa in anno/mese "+string(li_anno_rif)+"/"+right("00"+string(li_mese),2)
			
//			lb_SMP_OK = false
//			
//			//ritolgo la quota parte che non sono riuscito a correggere, per risommarla tra un attimo
//			ld_quan_da_correggere -= abs(ld_giacenze_mese[li_mese])
//			wf_scrivi_log("Prodotto "+fs_cod_prodotto+": Non sono riuscito a sistemare i negativi mensili  con gli SMP. Provo con gli SVE", fs_file)
//			exit
			fi_return = 200
			return
			//return 200
		end if
		
	else
		//no quantità da correggere per ora, arrivati a questo mese
		//presegui con i mesi successivi, se ce ne sono
	end if
	
	//vai al mese successivo, se ce ne sono ...
	li_mese += 1
loop
//##################################################################################

//if lb_SMP_OK then
//	//sono riuscito ad azzerare la giacenza nel mese (li_mese), utilizzando i soli SMP. Non è quindi necessario provare con gli SVE
//	goto COMPENSA
//end if


////##################################################################################
//// provo con i movimenti SVE
//do while li_mese <= 12
//	//leggo la giacenza del prodotto alla fine del mese li_mese: ld_giacenze_mese[li_mese]
//	
//	//in ld_giacenza la giacenza mensile
//	if ld_giacenze_mese[li_mese]<0 then
//		//metto anche questa quantità da correggere, oltre all'eventualmente ancora presente come residui negativi mesi precedenti
//		//ovviamente, essendo negativa la prendo co segno positivo
//		ld_quan_da_correggere += abs(ld_giacenze_mese[li_mese])
//		lb_giacenza_negativa_mensile = true
//		
//		ii_mese_succ_zero = li_mese + 1
//		
//	end if
//
//	if ld_quan_da_correggere>0 then
//		//chiamo una funzione apposita per cercare di azzerrare/dimininuire il + possibile questo saldo negativo
//		//la funzione esamina gli scarichi presenti nel mese e cerca di diminuirne/azzerarne la quantità
//		//a partire dai movimenti di fine mese fino ad inizio mese
//		//in caso di variazioni di quantità scarichi, occoore anche riallineare le giacenze mensili a partire dal mese corrente (ecco perchè passo byref ld_giacenze_mese[] )
//		//poi mi serve una variabile ld_quan_da_compensare per cercare di riequilibare le quantità annuali e fare in modo che la giacenza a fine anno resti invariata
//		
//		li_ret = wf_correggi_negativi_mese(	fs_file, fs_cod_prodotto, fs_cod_deposito, is_cod_tipo_mov_vendita, li_anno_rif, li_mese, &
//														ref ld_quan_da_correggere, ref ld_quan_da_compensare, ref ld_giacenze_mese[], ref fs_errore)
//		
//		if li_ret<0 then
//			//errore critico
//			return -1
//			
//		elseif li_ret=1 then
//			//non sono riuscito ad azzerare la giacenza nel mese, pertanto è rimasta negativa
//			fs_errore = 	"Il prodotto "+fs_cod_prodotto+" è restato con giacenza negativa in anno/mese "+string(li_anno_rif)+"/"+right("00"+string(li_mese),2)+&
//							" probabilmente gli SMP e gli SVE del mese non hanno quantità sufficienti ..."
//			return 200
//		end if
//		
//	else
//		//no quantità da correggere per ora, arrivati a questo mese
//		//presegui con i mesi successivi, se ce ne sono
//	end if
//	
//	//vai al mese successivo, se ce ne sono ...
//	li_mese += 1
//loop
////##################################################################################

//wf_giacenza_minima(ld_giacenze_mese[], ii_mese_succ_zero, li_mese_minima, ld_giacenza_minima)

//COMPENSA:
//se uscito da questo ciclo trovo "fd_quan_da_correggere" > 0 vuol dire che non sono riuscito
//per questo prodotto a sistemare le giacenze mensili negative
//farò un rollback di quanto fatto per questo prodotto
if ld_quan_da_correggere > 0 then
	fi_return = 1
	return
	//return 1
end if


//adesso, sistemati i negativi che si presentavano a fine mese devo compensare le variazioni eventuali fatte (uso la variabile ld_quan_da_compensare)
//queste compensazioni devono partire dal mese indicato dalla variabile ii_mese_succ_zero, mese successivo + alto a partire dall'ultimo mese 
//la cui giacenza è stata reimpostata a zero (correzione dei negativi)
if ld_quan_da_compensare > 0 then
	
	if ii_mese_succ_zero >12 or ii_mese_succ_zero < 1 then
		//la variabile ii_mese_succ_zero ha assunto un valore imprevisto
		fi_return = 500
		return
		//return 500
	end if
	
else
	if lb_giacenza_negativa_mensile then
		//finito, ok questo prodotto
		wf_scrivi_log("Prodotto: "+fs_cod_prodotto+"   Giacenza al 31/12 dopo procedura:  "+string(ld_giacenze_mese[12], "###,###,##0.0000"), fs_file)
		fi_return = 0
		return
		//return 0
	else
		//il prodotto non aveva giacenze mensili NEGATIVE, scrivi nel log questo
		fi_return = 100
		return
		//return 100
	end if
end if

//se giungi qui, devi compensare
//nei mesi in cui ho giacenza positiva aumento gli scarichi, fino ad annullare ld_quan_da_compensare
wf_scrivi_log("Prodotto "+fs_cod_prodotto+": Ho annullato le giacenze negative. cerco di compensare la quantità "+string(ld_quan_da_compensare, "###,###,##0.0000"), fs_file)

//li_mese = ii_mese_succ_zero
li_mese = 12
lb_movimento_fatto = false

//#########################################################################################################
//provo con gli SMP/SVE
//do while li_mese <= 12
do while li_mese >= ii_mese_succ_zero
	if ld_giacenze_mese[li_mese]>0 then
		
		li_ret = wf_compensa_qta_prodotto(	fs_file, fs_cod_prodotto, fs_cod_deposito, li_anno_rif, li_mese, &
														ref ld_quan_da_compensare, ref ld_giacenze_mese[], ref lb_movimento_fatto, ref fs_errore)
		
		if ld_quan_da_compensare=0 then
			//finito
			exit
		end if
		
		//se arrivi fin qui vuol dire che hai ancora ld_quan_da_compensare > 0
		
		//************************************************************************************************
		//puoi fare un solo movimento di compensazione sfruttando is_cod_tipo_mov_vendita=SMP/SVE a partire da dicembre a ritroso, fino a quando hai giacenza positiva
		//quindi se ne hai fatto uno (lb_movimento_fatto=TRUE) e la quantità da compensare è ancora maggiore di zero, esci dal ciclo annullando tutto
		if lb_movimento_fatto then
			wf_scrivi_log("Prodotto: "+fs_cod_prodotto+"   compensazione non riuscita ...", fs_file)
			fi_return=1
			return
			//return 1
		end if
	end if
	
	if li_ret<0 then
		//errore critico
		fi_return = -1
		return
		//return -1
	end if
	
	//vai al mese successivo, se ce ne sono ...
	//li_mese += 1
	li_mese -= 1
loop

//se arrivati fin qui risulta ancora qualcosa da compensare esci annullando tutto
if ld_quan_da_compensare > 0 then
	lb_Compensa_OK = false
	wf_scrivi_log("Prodotto "+fs_cod_prodotto+": Non sono riuscito a compensare tutta la quantità con gli SMP/SVE", fs_file)
	fi_return=1
	return
	//return 1
else
	//goto FINE
end if
////#########################################################################################################
//
////#########################################################################################################
////ovviamente devo riparitre dall'ultimo mese che risulta con giacenza positiva
//
////li_mese = ii_mese_succ_zero
//li_mese = 12
//lb_movimento_fatto = false
//
////provo con gli SVE
////do while li_mese <= 12
//do while li_mese >= ii_mese_succ_zero
//	if ld_giacenze_mese[li_mese]>0 then
//		
//		li_ret = wf_compensa_qta_prodotto(	fs_file, fs_cod_prodotto, fs_cod_deposito, is_cod_tipo_mov_vendita, li_anno_rif, li_mese, &
//														ref ld_quan_da_compensare, ref ld_giacenze_mese[], ref lb_movimento_fatto, ref fs_errore)
//		
//		if ld_quan_da_compensare=0 then
//			//finito
//			exit
//		end if
//		
//		//************************************************************************************************
//		//puoi fare un solo movimento di compensazione sfruttando is_cod_tipo_mov_vendita=SVE a partire da dicembre a ritroso (fino a quando hai giacenza positiva)
//		//quindi se ne hai fatto uno (lb_movimento_fatto=TRUE) e la quantità da compensare è ancora maggiore di zero, esci, annullando tutto
//		if lb_movimento_fatto then
//			goto _ROLLBACK
//		end if
//		
//	end if
//	
//	if li_ret<0 then
//		//errore critico
//		return -1
//	end if
//	
//	//vai al mese successivo, se ce ne sono ...
//	//li_mese += 1
//	li_mese -= 1
//loop
//
////se arrivati fin qui risulta ancora qualcosa da compensare allora segnala e annulla tutto per questo prodotto
//if ld_quan_da_compensare > 0 then
//	return 1
//end if
////#########################################################################################################

if wf_esamina_giacenze_dopo(fs_cod_prodotto, ld_giacenze_mese[], fs_errore) < 0 then
	//hai elaborato ma hai messo a negativo qualche giacenza, segnala, ma non effettuare il rollback
	fi_return = 401
	wf_scrivi_log("Prodotto: "+fs_cod_prodotto+"   Giacenza al 31/12 dopo procedura:  "+string(ld_giacenze_mese[12], "###,###,##0.0000"), fs_file)
	return
	//return 400
end if

//FINE:
wf_scrivi_log("Prodotto: "+fs_cod_prodotto+"   Giacenza al 31/12 dopo procedura:  "+string(ld_giacenze_mese[12], "###,###,##0.0000"), fs_file)
fi_return = 0
return
//return 0



end subroutine

on w_sistema_negativi_mese.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_giacenze_sistema=create dw_giacenze_sistema
this.cb_esporta_2=create cb_esporta_2
this.cb_esporta=create cb_esporta
this.dw_giacenze_2=create dw_giacenze_2
this.cb_giacenze_mensili_2=create cb_giacenze_mensili_2
this.st_1=create st_1
this.st_file=create st_file
this.em_data_rif=create em_data_rif
this.sle_cod_deposito=create sle_cod_deposito
this.sle_cod_prodotto_test=create sle_cod_prodotto_test
this.cb_giacenze_mensili=create cb_giacenze_mensili
this.cb_leggi_e_sistema=create cb_leggi_e_sistema
this.st_log=create st_log
this.sle_cod_prodotto_like=create sle_cod_prodotto_like
this.st_count=create st_count
this.cb_carica_prodotti=create cb_carica_prodotti
this.dw_giacenze=create dw_giacenze
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_giacenze_sistema
this.Control[iCurrent+3]=this.cb_esporta_2
this.Control[iCurrent+4]=this.cb_esporta
this.Control[iCurrent+5]=this.dw_giacenze_2
this.Control[iCurrent+6]=this.cb_giacenze_mensili_2
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.st_file
this.Control[iCurrent+9]=this.em_data_rif
this.Control[iCurrent+10]=this.sle_cod_deposito
this.Control[iCurrent+11]=this.sle_cod_prodotto_test
this.Control[iCurrent+12]=this.cb_giacenze_mensili
this.Control[iCurrent+13]=this.cb_leggi_e_sistema
this.Control[iCurrent+14]=this.st_log
this.Control[iCurrent+15]=this.sle_cod_prodotto_like
this.Control[iCurrent+16]=this.st_count
this.Control[iCurrent+17]=this.cb_carica_prodotti
this.Control[iCurrent+18]=this.dw_giacenze
end on

on w_sistema_negativi_mese.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_giacenze_sistema)
destroy(this.cb_esporta_2)
destroy(this.cb_esporta)
destroy(this.dw_giacenze_2)
destroy(this.cb_giacenze_mensili_2)
destroy(this.st_1)
destroy(this.st_file)
destroy(this.em_data_rif)
destroy(this.sle_cod_deposito)
destroy(this.sle_cod_prodotto_test)
destroy(this.cb_giacenze_mensili)
destroy(this.cb_leggi_e_sistema)
destroy(this.st_log)
destroy(this.sle_cod_prodotto_like)
destroy(this.st_count)
destroy(this.cb_carica_prodotti)
destroy(this.dw_giacenze)
end on

event open;call super::open;is_path_origine = guo_functions.uof_get_user_documents_folder( )

st_file.text = is_path_origine

is_mesi[1] = "Gen"
is_mesi[2] = "Feb"
is_mesi[3] = "Mar"
is_mesi[4] = "Apr"
is_mesi[5] = "Mag"
is_mesi[6] = "Giu"
is_mesi[7] = "Lug"
is_mesi[8] = "Ago"
is_mesi[9] = "Set"
is_mesi[10] = "Ott"
is_mesi[11] = "Nov"
is_mesi[12] = "Dic"
end event

type cb_1 from commandbutton within w_sistema_negativi_mese
integer x = 2587
integer y = 380
integer width = 402
integer height = 76
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "XLS"
end type

event clicked;


if dw_giacenze_sistema.saveas("", Excel!, true) = 1 then
	g_mb.show( "Esportazione avvenuta con successo!" )
else
	g_mb.error( "Esportazione NON avvenuta per un errore!" )
end if

return
end event

type dw_giacenze_sistema from datawindow within w_sistema_negativi_mese
integer x = 37
integer y = 480
integer width = 4197
integer height = 680
integer taborder = 20
string title = "none"
string dataobject = "d_sistema_negativi_mese"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_esporta_2 from commandbutton within w_sistema_negativi_mese
integer x = 2587
integer y = 1912
integer width = 402
integer height = 76
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "XLS"
end type

event clicked;


if dw_giacenze_2.saveas("", Excel!, true) = 1 then
	g_mb.show( "Esportazione avvenuta con successo!" )
else
	g_mb.error( "Esportazione NON avvenuta per un errore!" )
end if

return
end event

type cb_esporta from commandbutton within w_sistema_negativi_mese
integer x = 2587
integer y = 1204
integer width = 402
integer height = 76
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "XLS"
end type

event clicked;


if dw_giacenze.saveas("", Excel!, true) = 1 then
	g_mb.show( "Esportazione avvenuta con successo!" )
else
	g_mb.error( "Esportazione NON avvenuta per un errore!" )
end if

return
end event

type dw_giacenze_2 from datawindow within w_sistema_negativi_mese
integer x = 37
integer y = 2000
integer width = 4197
integer height = 608
integer taborder = 20
string title = "none"
string dataobject = "d_sistema_negativi_mese"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_giacenze_mensili_2 from commandbutton within w_sistema_negativi_mese
integer x = 2999
integer y = 1912
integer width = 759
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Test Giacenze Mensili dopo"
end type

event clicked;string				ls_cod_prodotto_like, ls_cod_deposito, ls_errore, ls_cod_prodotto_cu, ls_prod_in_chius[]
long				ll_count, ll_index, ll_new
integer			li_anno, li_i
datetime			ldt_data_chiusura_annuale, ldt_data_riferimento
decimal			ld_giacenza[]


ls_cod_prodotto_like = sle_cod_prodotto_test.text
ldt_data_riferimento = datetime(date(em_data_rif.text), 00:00:00)

if ls_cod_prodotto_like="" or isnull(ls_cod_prodotto_like) then
	g_mb.error("Inserire almeno un prodotto da testare!")
	return
end if

if isnull(ldt_data_riferimento) or year(date(ldt_data_riferimento)) <=2000 then
	g_mb.error("Inserire una data di riferimento!")
	return
end if

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if ldt_data_riferimento < ldt_data_chiusura_annuale then
	g_mb.error("Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale!")
	return
end if

if isnull(ls_cod_prodotto_like) or ls_cod_prodotto_like="" then ls_cod_prodotto_like="%"

st_log.text = ""

dw_giacenze_2.reset()

li_anno = year(date(ldt_data_riferimento))
ls_cod_deposito = sle_cod_deposito.text

declare cur_prod cursor for 
select distinct m.cod_prodotto
from mov_magazzino as m
join anag_prodotti as p on	p.cod_azienda=m.cod_azienda and
									p.cod_prodotto=m.cod_prodotto
where	m.cod_azienda = :s_cs_xx.cod_azienda and
         	m.cod_prodotto like :ls_cod_prodotto_like and
         	m.data_registrazione <= :ldt_data_riferimento and
         	m.data_registrazione > :ldt_data_chiusura_annuale and
			p.flag_articolo_fiscale='S' and p.flag_lifo='S' and p.flag_classe_abc='A'
order by m.cod_prodotto;

ll_count = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext)
	return
end if

do while true
	fetch cur_prod into :ls_cod_prodotto_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext)
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	if not isnull(ls_cod_prodotto_cu) and ls_cod_prodotto_cu<>"" then
		ll_count += 1
		ls_prod_in_chius[ll_count] = ls_cod_prodotto_cu
		st_log.text = "Caricamento prodotto "+ls_cod_prodotto_cu+" nell'array ..."
	end if
	
loop

close cur_prod ;

ll_count = upperbound(ls_prod_in_chius[])

if ll_count>0 then
else
	g_mb.error("Nessun prodotto trovato (prodotto inesistente oppure non lifo o non fiscale o non di classe A)!")
	return
end if

for ll_index = 1 to  ll_count
	yield()
	
	ls_cod_prodotto_cu = ls_prod_in_chius[ll_index]
	st_log.text = "Elaborazione prodotto "+ls_cod_prodotto_cu+" ("+string(ll_index)+" di "+string(ll_count) +") "
	
	wf_inventari_mensili(false, li_anno, ls_cod_prodotto_cu, ls_cod_deposito, ld_giacenza[], ls_errore)
	
	//----------------------------------------------------------------------------------
	ll_new = dw_giacenze_2.insertrow(0)

	dw_giacenze_2.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_cu)
	dw_giacenze_2.setitem(ll_new, "cod_deposito", ls_cod_deposito)
	dw_giacenze_2.setitem(ll_new, "tipo_riga", "G")
	
	dw_giacenze.scrolltorow(ll_new)
	
	for li_i = 1 to 12
		dw_giacenze_2.setitem(ll_new, "gz_"+string(li_i), ld_giacenza[li_i])
	next
	//---------------------------------------------------------------------------------

next

st_log.text = "Fine!"
end event

type st_1 from statictext within w_sistema_negativi_mese
integer x = 1947
integer y = 252
integer width = 2208
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "solo quelli movimentati, e con flag_fiscale, flag_lifo posti a SI e flag_classe=~'A~'"
boolean focusrectangle = false
end type

type st_file from statictext within w_sistema_negativi_mese
integer x = 1179
integer y = 160
integer width = 2560
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type em_data_rif from editmask within w_sistema_negativi_mese
integer x = 471
integer y = 12
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "31/12/2011"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type sle_cod_deposito from singlelineedit within w_sistema_negativi_mese
integer x = 23
integer y = 12
integer width = 402
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "001"
end type

type sle_cod_prodotto_test from singlelineedit within w_sistema_negativi_mese
integer x = 3689
integer y = 1204
integer width = 549
integer height = 76
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

type cb_giacenze_mensili from commandbutton within w_sistema_negativi_mese
integer x = 2999
integer y = 1204
integer width = 681
integer height = 76
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Test Giacenze Mensili"
end type

event clicked;string				ls_cod_prodotto_like, ls_cod_deposito, ls_errore, ls_cod_prodotto_cu, ls_prod_in_chius[]
long				ll_count, ll_index, ll_new
integer			li_anno, li_i
datetime			ldt_data_chiusura_annuale, ldt_data_riferimento
decimal			ld_giacenza[]


ls_cod_prodotto_like = sle_cod_prodotto_test.text
ldt_data_riferimento = datetime(date(em_data_rif.text), 00:00:00)

if ls_cod_prodotto_like="" or isnull(ls_cod_prodotto_like) then
	g_mb.error("Inserire almeno un prodotto da testare!")
	return
end if

if isnull(ldt_data_riferimento) or year(date(ldt_data_riferimento)) <=2000 then
	g_mb.error("Inserire una data di riferimento!")
	return
end if

select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if ldt_data_riferimento < ldt_data_chiusura_annuale then
	g_mb.error("Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale!")
	return
end if

if isnull(ls_cod_prodotto_like) or ls_cod_prodotto_like="" then ls_cod_prodotto_like="%"

st_log.text = ""

dw_giacenze.reset()

li_anno = year(date(ldt_data_riferimento))
ls_cod_deposito = sle_cod_deposito.text

declare cur_prod cursor for 
select distinct m.cod_prodotto
from mov_magazzino as m
join anag_prodotti as p on	p.cod_azienda=m.cod_azienda and
									p.cod_prodotto=m.cod_prodotto
where	m.cod_azienda = :s_cs_xx.cod_azienda and
         	m.cod_prodotto like :ls_cod_prodotto_like and
         	m.data_registrazione <= :ldt_data_riferimento and
         	m.data_registrazione > :ldt_data_chiusura_annuale and
			p.flag_articolo_fiscale='S' and p.flag_lifo='S' and p.flag_classe_abc='A'
order by m.cod_prodotto;

ll_count = 0

open cur_prod;
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in OPEN cursore cur_prod: " + sqlca.sqlerrtext)
	return
end if

do while true
	fetch cur_prod into :ls_cod_prodotto_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante lettura elenco prodotti: "+sqlca.sqlerrtext)
		return
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	yield()
	
	if not isnull(ls_cod_prodotto_cu) and ls_cod_prodotto_cu<>"" then
		ll_count += 1
		ls_prod_in_chius[ll_count] = ls_cod_prodotto_cu
		st_log.text = "Caricamento prodotto "+ls_cod_prodotto_cu+" nell'array ..."
	end if
	
loop

close cur_prod ;

ll_count = upperbound(ls_prod_in_chius[])

if ll_count>0 then
else
	g_mb.error("Nessun prodotto trovato (prodotto inesistente oppure non lifo o non fiscale o non di classe A)!")
	return
end if

for ll_index = 1 to  ll_count
	yield()
	
	ls_cod_prodotto_cu = ls_prod_in_chius[ll_index]
	st_log.text = "Elaborazione prodotto "+ls_cod_prodotto_cu+" ("+string(ll_index)+" di "+string(ll_count) +") "
	
	wf_inventari_mensili(false, li_anno, ls_cod_prodotto_cu, ls_cod_deposito, ld_giacenza[], ls_errore)
	
	//----------------------------------------------------------------------------------
	ll_new = dw_giacenze.insertrow(0)

	dw_giacenze.setitem(ll_new, "cod_prodotto", ls_cod_prodotto_cu)
	dw_giacenze.setitem(ll_new, "cod_deposito", ls_cod_deposito)
	dw_giacenze.setitem(ll_new, "tipo_riga", "G")
	
	dw_giacenze.scrolltorow(ll_new)
	
	for li_i = 1 to 12
		dw_giacenze.setitem(ll_new, "gz_"+string(li_i), ld_giacenza[li_i])
	next
	//---------------------------------------------------------------------------------
	
	
next

st_log.text = "Fine!"
end event

type cb_leggi_e_sistema from commandbutton within w_sistema_negativi_mese
integer x = 37
integer y = 360
integer width = 923
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "2. leggi e sistema negativi mensili"
end type

event clicked;string			ls_errore
integer		li_ret


li_ret = wf_leggi_e_sistema(ls_errore)

choose case li_ret
	case is < 0
		st_log.text = ls_errore
		return
		
	case 2
		st_log.text = "Annullato dall'utente!"
		return
		
end choose

st_log.text = "Elaborazione completata!"
return
end event

type st_log from statictext within w_sistema_negativi_mese
integer x = 882
integer y = 28
integer width = 3351
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type sle_cod_prodotto_like from singlelineedit within w_sistema_negativi_mese
integer x = 539
integer y = 136
integer width = 603
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "%"
borderstyle borderstyle = stylelowered!
end type

type st_count from statictext within w_sistema_negativi_mese
integer x = 544
integer y = 248
integer width = 1330
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Tot. Prodotti caricati:"
boolean focusrectangle = false
end type

type cb_carica_prodotti from commandbutton within w_sistema_negativi_mese
integer x = 27
integer y = 124
integer width = 498
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "1. carica prodotti"
end type

event clicked;string			ls_errore
integer		li_ret

li_ret = wf_carica_prodotti(ls_errore)

if li_ret<0 then
	g_mb.error(ls_errore)
	rollback;
	return
end if
end event

type dw_giacenze from datawindow within w_sistema_negativi_mese
integer x = 37
integer y = 1292
integer width = 4197
integer height = 608
integer taborder = 10
string title = "none"
string dataobject = "d_sistema_negativi_mese"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

