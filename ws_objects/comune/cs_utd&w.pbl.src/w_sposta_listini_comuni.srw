﻿$PBExportHeader$w_sposta_listini_comuni.srw
forward
global type w_sposta_listini_comuni from window
end type
type em_fine from editmask within w_sposta_listini_comuni
end type
type em_inizio from editmask within w_sposta_listini_comuni
end type
type st_1 from statictext within w_sposta_listini_comuni
end type
type cb_1 from commandbutton within w_sposta_listini_comuni
end type
end forward

global type w_sposta_listini_comuni from window
integer width = 2112
integer height = 1300
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
em_fine em_fine
em_inizio em_inizio
st_1 st_1
cb_1 cb_1
end type
global w_sposta_listini_comuni w_sposta_listini_comuni

on w_sposta_listini_comuni.create
this.em_fine=create em_fine
this.em_inizio=create em_inizio
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.em_fine,&
this.em_inizio,&
this.st_1,&
this.cb_1}
end on

on w_sposta_listini_comuni.destroy
destroy(this.em_fine)
destroy(this.em_inizio)
destroy(this.st_1)
destroy(this.cb_1)
end on

type em_fine from editmask within w_sposta_listini_comuni
integer x = 1047
integer y = 448
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type em_inizio from editmask within w_sposta_listini_comuni
integer x = 105
integer y = 448
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_1 from statictext within w_sposta_listini_comuni
integer x = 50
integer y = 304
integer width = 1111
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sposta i dati comuni nelle tabelle COMUNI"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_sposta_listini_comuni
integer x = 50
integer y = 68
integer width = 1413
integer height = 204
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Sposta dati comuni"
end type

event clicked;string ls_cod_tipo_listino_prodotto,ls_cod_valuta
long ll_progressivo
datetime ldt_data_inizio_val, ldt_inizio, ldt_fine

ldt_inizio = datetime(date(em_inizio.text),00:00:00)
ldt_fine = datetime(date(em_fine.text),00:00:00)

 DECLARE cu_listini CURSOR FOR  
  SELECT  listini_vendite.cod_tipo_listino_prodotto,   
         listini_vendite.cod_valuta,   
         listini_vendite.data_inizio_val,   
         listini_vendite.progressivo  
    FROM listini_vendite  
   WHERE ( listini_vendite.cod_azienda = :s_cs_xx.cod_azienda) AND  
         ( listini_vendite.cod_prodotto is not null ) AND  
         ( listini_vendite.cod_cat_mer is null ) AND  
         ( listini_vendite.cod_categoria is null ) AND  
         ( listini_vendite.cod_cliente is null )  and 
			data_inizio_val >= :ldt_inizio and data_inizio_val <= :ldt_fine    ;

Open cu_listini;

do while true
	
	fetch cu_listini into :ls_cod_tipo_listino_prodotto, :ls_cod_valuta, :ldt_data_inizio_val, :ll_progressivo;

	if sqlca.sqlcode = -1 then
		messagebox("ERRORE fetch", sqlca.sqlerrtext)
		close cu_listini;
		rollback;
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	st_1.text = ls_cod_tipo_listino_prodotto + "-" + string(ldt_data_inizio_val,"dd/mm/yyyy") + "-" + string(ll_progressivo)
	Yield()
	
	
	  INSERT INTO listini_ven_comune  
         ( cod_azienda,   
           cod_tipo_listino_prodotto,   
           cod_valuta,   
           data_inizio_val,   
           progressivo,   
           cod_cat_mer,   
           cod_prodotto,   
           cod_categoria,   
           cod_cliente,   
           des_listino_vendite,   
           minimo_fatt_altezza,   
           minimo_fatt_larghezza,   
           minimo_fatt_profondita,   
           minimo_fatt_superficie,   
           minimo_fatt_volume,   
           flag_sconto_a_parte,   
           flag_tipo_scaglioni,   
           scaglione_1,   
           scaglione_2,   
           scaglione_3,   
           scaglione_4,   
           scaglione_5,   
           flag_sconto_mag_prezzo_1,   
           flag_sconto_mag_prezzo_2,   
           flag_sconto_mag_prezzo_3,   
           flag_sconto_mag_prezzo_4,   
           flag_sconto_mag_prezzo_5,   
           variazione_1,   
           variazione_2,   
           variazione_3,   
           variazione_4,   
           variazione_5,   
           flag_origine_prezzo_1,   
           flag_origine_prezzo_2,   
           flag_origine_prezzo_3,   
           flag_origine_prezzo_4,   
           flag_origine_prezzo_5 )  
     SELECT listini_vendite.cod_azienda,   
            listini_vendite.cod_tipo_listino_prodotto,   
            listini_vendite.cod_valuta,   
            listini_vendite.data_inizio_val,   
            listini_vendite.progressivo,   
            listini_vendite.cod_cat_mer,   
            listini_vendite.cod_prodotto,   
            listini_vendite.cod_categoria,   
            listini_vendite.cod_cliente,   
            listini_vendite.des_listino_vendite,   
            listini_vendite.minimo_fatt_altezza,   
            listini_vendite.minimo_fatt_larghezza,   
            listini_vendite.minimo_fatt_profondita,   
            listini_vendite.minimo_fatt_superficie,   
            listini_vendite.minimo_fatt_volume,   
            listini_vendite.flag_sconto_a_parte,   
            listini_vendite.flag_tipo_scaglioni,   
            listini_vendite.scaglione_1,   
            listini_vendite.scaglione_2,   
            listini_vendite.scaglione_3,   
            listini_vendite.scaglione_4,   
            listini_vendite.scaglione_5,   
            listini_vendite.flag_sconto_mag_prezzo_1,   
            listini_vendite.flag_sconto_mag_prezzo_2,   
            listini_vendite.flag_sconto_mag_prezzo_3,   
            listini_vendite.flag_sconto_mag_prezzo_4,   
            listini_vendite.flag_sconto_mag_prezzo_5,   
            listini_vendite.variazione_1,   
            listini_vendite.variazione_2,   
            listini_vendite.variazione_3,   
            listini_vendite.variazione_4,   
            listini_vendite.variazione_5,   
            listini_vendite.flag_origine_prezzo_1,   
            listini_vendite.flag_origine_prezzo_2,   
            listini_vendite.flag_origine_prezzo_3,   
            listini_vendite.flag_origine_prezzo_4,   
            listini_vendite.flag_origine_prezzo_5  
       FROM listini_vendite  
      WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  ) AND  
            ( cod_valuta = :ls_cod_valuta ) AND  
            ( data_inizio_val = :ldt_data_inizio_val ) AND  
            ( progressivo = :ll_progressivo )   ;

		if sqlca.sqlcode <> 0  then
			messagebox("ERRORE insert listini_vendite", sqlca.sqlerrtext)
			close cu_listini;
			rollback;
			return
		end if

	  INSERT INTO listini_ven_dim_comune  
         ( cod_azienda,   
           cod_tipo_listino_prodotto,   
           cod_valuta,   
           data_inizio_val,   
           progressivo,   
           num_scaglione,   
           limite_dimensione_1,   
           limite_dimensione_2,   
           flag_sconto_mag_prezzo,   
           variazione )  
     SELECT listini_vendite_dimensioni.cod_azienda,   
            listini_vendite_dimensioni.cod_tipo_listino_prodotto,   
            listini_vendite_dimensioni.cod_valuta,   
            listini_vendite_dimensioni.data_inizio_val,   
            listini_vendite_dimensioni.progressivo,   
            listini_vendite_dimensioni.num_scaglione,   
            listini_vendite_dimensioni.limite_dimensione_1,   
            listini_vendite_dimensioni.limite_dimensione_2,   
            listini_vendite_dimensioni.flag_sconto_mag_prezzo,   
            listini_vendite_dimensioni.variazione  
       FROM listini_vendite_dimensioni  
       WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  ) AND  
            ( cod_valuta = :ls_cod_valuta ) AND  
            ( data_inizio_val = :ldt_data_inizio_val ) AND  
            ( progressivo = :ll_progressivo )   ;

	
		if sqlca.sqlcode <> 0  then
			messagebox("ERRORE insert listini_vendite_dimensioni", sqlca.sqlerrtext)
			close cu_listini;
			rollback;
			return
		end if

	  
	  INSERT INTO listini_prod_comune  
         ( cod_azienda,   
           cod_tipo_listino_prodotto,   
           cod_valuta,   
           data_inizio_val,   
           progressivo,   
           cod_prodotto_listino,   
           cod_versione,   
           prog_listino_produzione,   
           cod_prodotto_padre,   
           num_sequenza,   
           cod_prodotto_figlio,   
           cod_gruppo_variante,   
           progr,   
           cod_prodotto,   
           des_listino_produzione,   
           minimo_fatt_altezza,   
           minimo_fatt_larghezza,   
           minimo_fatt_profondita,   
           minimo_fatt_superficie,   
           minimo_fatt_volume,   
           flag_sconto_a_parte,   
           flag_tipo_scaglioni,   
           scaglione_1,   
           scaglione_2,   
           scaglione_3,   
           scaglione_4,   
           scaglione_5,   
           flag_sconto_mag_prezzo_1,   
           flag_sconto_mag_prezzo_2,   
           flag_sconto_mag_prezzo_3,   
           flag_sconto_mag_prezzo_4,   
           flag_sconto_mag_prezzo_5,   
           variazione_1,   
           variazione_2,   
           variazione_3,   
           variazione_4,   
           variazione_5,   
           flag_origine_prezzo_1,   
           flag_origine_prezzo_2,   
           flag_origine_prezzo_3,   
           flag_origine_prezzo_4,   
           flag_origine_prezzo_5,   
           provvigione_1,   
           provvigione_2 )  
     SELECT listini_produzione.cod_azienda,   
            listini_produzione.cod_tipo_listino_prodotto,   
            listini_produzione.cod_valuta,   
            listini_produzione.data_inizio_val,   
            listini_produzione.progressivo,   
            listini_produzione.cod_prodotto_listino,   
            listini_produzione.cod_versione,   
            listini_produzione.prog_listino_produzione,   
            listini_produzione.cod_prodotto_padre,   
            listini_produzione.num_sequenza,   
            listini_produzione.cod_prodotto_figlio,   
            listini_produzione.cod_gruppo_variante,   
            listini_produzione.progr,   
            listini_produzione.cod_prodotto,   
            listini_produzione.des_listino_produzione,   
            listini_produzione.minimo_fatt_altezza,   
            listini_produzione.minimo_fatt_larghezza,   
            listini_produzione.minimo_fatt_profondita,   
            listini_produzione.minimo_fatt_superficie,   
            listini_produzione.minimo_fatt_volume,   
            listini_produzione.flag_sconto_a_parte,   
            listini_produzione.flag_tipo_scaglioni,   
            listini_produzione.scaglione_1,   
            listini_produzione.scaglione_2,   
            listini_produzione.scaglione_3,   
            listini_produzione.scaglione_4,   
            listini_produzione.scaglione_5,   
            listini_produzione.flag_sconto_mag_prezzo_1,   
            listini_produzione.flag_sconto_mag_prezzo_2,   
            listini_produzione.flag_sconto_mag_prezzo_3,   
            listini_produzione.flag_sconto_mag_prezzo_4,   
            listini_produzione.flag_sconto_mag_prezzo_5,   
            listini_produzione.variazione_1,   
            listini_produzione.variazione_2,   
            listini_produzione.variazione_3,   
            listini_produzione.variazione_4,   
            listini_produzione.variazione_5,   
            listini_produzione.flag_origine_prezzo_1,   
            listini_produzione.flag_origine_prezzo_2,   
            listini_produzione.flag_origine_prezzo_3,   
            listini_produzione.flag_origine_prezzo_4,   
            listini_produzione.flag_origine_prezzo_5,   
            listini_produzione.provvigione_1,   
            listini_produzione.provvigione_2  
       FROM listini_produzione 
		 WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  ) AND  
            ( cod_valuta = :ls_cod_valuta ) AND  
            ( data_inizio_val = :ldt_data_inizio_val ) AND  
            ( progressivo = :ll_progressivo ) ;

		if sqlca.sqlcode <> 0  then
			messagebox("ERRORE insert listini_produzione", sqlca.sqlerrtext)
			close cu_listini;
			rollback;
			return
		end if

	// ----------------------------------------  cancello dimensioni e produzione dopo averli passati ------------------ù
	
	delete listini_produzione
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  ) AND  
            ( cod_valuta = :ls_cod_valuta ) AND  
            ( data_inizio_val = :ldt_data_inizio_val ) AND  
            ( progressivo = :ll_progressivo );

		if sqlca.sqlcode <> 0  then
			messagebox("ERRORE delete listini_produzione", sqlca.sqlerrtext)
			close cu_listini;
			rollback;
			return
		end if

	delete listini_vendite_dimensioni
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
            ( cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto  ) AND  
            ( cod_valuta = :ls_cod_valuta ) AND  
            ( data_inizio_val = :ldt_data_inizio_val ) AND  
            ( progressivo = :ll_progressivo );

		if sqlca.sqlcode <> 0  then
			messagebox("ERRORE delete listini_vendite_dimension", sqlca.sqlerrtext)
			close cu_listini;
			rollback;
			return
		end if
		
loop

commit;
end event

