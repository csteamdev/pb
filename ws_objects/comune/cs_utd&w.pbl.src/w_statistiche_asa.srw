﻿$PBExportHeader$w_statistiche_asa.srw
$PBExportComments$Window di statistiche DB per ASA
forward
global type w_statistiche_asa from w_cs_xx_principale
end type
type dw_statistics from uo_cs_xx_dw within w_statistiche_asa
end type
end forward

global type w_statistiche_asa from w_cs_xx_principale
int Width=2620
int Height=2245
boolean TitleBar=true
string Title="Statistiche ASA"
dw_statistics dw_statistics
end type
global w_statistiche_asa w_statistiche_asa

type prototypes

end prototypes

on w_statistiche_asa.create
int iCurrent
call w_cs_xx_principale::create
this.dw_statistics=create dw_statistics
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_statistics
end on

on w_statistiche_asa.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_statistics)
end on

event pc_setwindow;call super::pc_setwindow;dw_statistics.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

end event

type dw_statistics from uo_cs_xx_dw within w_statistiche_asa
int X=23
int Y=21
int Width=2538
int Height=2101
int TabOrder=10
string DataObject="d_statistics_asa"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

