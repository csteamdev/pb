﻿$PBExportHeader$w_statistiche_ase.srw
$PBExportComments$Window di statistiche DB per ASE
forward
global type w_statistiche_ase from w_cs_xx_principale
end type
type cb_exp from commandbutton within w_statistiche_ase
end type
type st_2 from statictext within w_statistiche_ase
end type
type st_1 from statictext within w_statistiche_ase
end type
type cb_statisiche from commandbutton within w_statistiche_ase
end type
type dw_statistics from uo_cs_xx_dw within w_statistiche_ase
end type
type cb_check from commandbutton within w_statistiche_ase
end type
end forward

global type w_statistiche_ase from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2619
integer height = 2684
string title = "Statistiche ASE"
cb_exp cb_exp
st_2 st_2
st_1 st_1
cb_statisiche cb_statisiche
dw_statistics dw_statistics
cb_check cb_check
end type
global w_statistiche_ase w_statistiche_ase

type prototypes

end prototypes

on w_statistiche_ase.create
int iCurrent
call super::create
this.cb_exp=create cb_exp
this.st_2=create st_2
this.st_1=create st_1
this.cb_statisiche=create cb_statisiche
this.dw_statistics=create dw_statistics
this.cb_check=create cb_check
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_exp
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_statisiche
this.Control[iCurrent+5]=this.dw_statistics
this.Control[iCurrent+6]=this.cb_check
end on

on w_statistiche_ase.destroy
call super::destroy
destroy(this.cb_exp)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_statisiche)
destroy(this.dw_statistics)
destroy(this.cb_check)
end on

event pc_setwindow;call super::pc_setwindow;	dw_statistics.set_dw_options(sqlca,pcca.null_object,c_multiselect,c_default)

end event

type cb_exp from commandbutton within w_statistiche_ase
integer x = 1170
integer y = 2140
integer width = 585
integer height = 84
integer taborder = 21
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Export"
end type

event clicked;long ll_file, ll_i
string ls_str

ll_file = fileopen("c:\temp\update_stat.TXT", linemode!, write!)

for ll_i = 1 to dw_statistics.rowcount()
	ls_str = dw_statistics.getitemstring(ll_i, "sysobjects_name")
	
//	ls_str = "update all statistics " + ls_str + ""
//	ls_str = "dbcc checktable(" + ls_str + ")"
	ls_str = "dbcc checkalloc(" + ls_str + ", full)"
	
	filewrite(ll_file, ls_str)
	
	filewrite(ll_file, "go")
next

fileclose(ll_file)

end event

type st_2 from statictext within w_statistiche_ase
integer x = 14
integer y = 2252
integer width = 2546
integer height = 296
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

type st_1 from statictext within w_statistiche_ase
integer x = 2053
integer y = 2140
integer width = 503
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_statisiche from commandbutton within w_statistiche_ase
integer x = 18
integer y = 2140
integer width = 480
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Update Statistics"
end type

event clicked;string ls_nome_tabella,ls_sql,ls_dbparm,ls_logpass
long ll_selected[],ll_riga,ll_stato
integer li_risposta
transaction ltran_1

setpointer (hourglass!)

ltran_1 = create transaction

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "servername", ltran_1.ServerName)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "logid", ltran_1.LogId)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "logpass", ls_logpass)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if


li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbms", ltran_1.DBMS)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "database", ltran_1.Database)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "userid", ltran_1.UserId)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbpass", ltran_1.DBPass)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbparm", ls_dbparm)

if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

if ltran_1.DBMS <> "ODBC" then
	n_cst_crypto lnv_crypt
	lnv_crypt = CREATE n_cst_crypto
	
	if isnull(ls_logpass) or ls_logpass="" then
		g_mb.messagebox("Attenzione", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", Stopsign!)
		DESTROY lnv_crypt		
		return -1
	end if
	
	if lnv_crypt.DecryptData(ls_logpass, ls_logpass) > 0 then
		ls_logpass = ""
	end if
	
	ltran_1.LogPass = ls_logpass
	
	DESTROY lnv_crypt

end if

ltran_1.LogId = "sa"
ltran_1.LogPass = ""
ltran_1.DBParm = ls_dbparm

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "lock", ltran_1.Lock)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

ltran_1.autocommit=true

connect using ltran_1;

execute immediate "set chained off " using ltran_1;

//execute immediate "commit" using ltran_1;

dw_statistics.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[]) //scorre le righe selezionate dall'utente
	
	yield()
	
	st_1.text = string(ll_riga) + "/" + string(upperbound(ll_selected[]))
	
//	execute immediate "commit" using ltran_1;

	ls_nome_tabella = dw_statistics.getitemstring(ll_selected[ll_riga],"sysobjects_name")
	ls_sql = "update all statistics " + ls_nome_tabella

	execute immediate :ls_sql using ltran_1;
	
	if ltran_1.sqlcode < 0 then
		g_mb.messagebox("Frame","Errore sul database durante l'esecuzione del comando update statistics sulla tabella " + ls_nome_tabella + ". Errore DB:" + ltran_1.sqlerrtext,stopsign!)
		return
	end if
	
//	execute immediate "commit" ;
	
next

commit using ltran_1;

setpointer (arrow!)

destroy ltran_1

g_mb.messagebox("Frame","Esecuzione del comando update statistics eseguita con successo.",information!)

end event

type dw_statistics from uo_cs_xx_dw within w_statistiche_ase
integer x = 23
integer y = 20
integer width = 2537
integer height = 2100
integer taborder = 10
string dataobject = "d_statistics_ase"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_check from commandbutton within w_statistiche_ase
integer x = 530
integer y = 2144
integer width = 562
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "checktable"
end type

event clicked;string ls_nome_tabella,ls_sql,ls_dbparm,ls_logpass
long ll_selected[],ll_riga,ll_stato
integer li_risposta
transaction ltran_1

setpointer (hourglass!)

ltran_1 = create transaction

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "servername", ltran_1.ServerName)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "logid", ltran_1.LogId)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "logpass", ls_logpass)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if


li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbms", ltran_1.DBMS)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "database", ltran_1.Database)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "userid", ltran_1.UserId)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbpass", ltran_1.DBPass)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "dbparm", ls_dbparm)

if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

if ltran_1.DBMS <> "ODBC" then
	n_cst_crypto lnv_crypt
	lnv_crypt = CREATE n_cst_crypto
	
	if isnull(ls_logpass) or ls_logpass="" then
		g_mb.messagebox("Attenzione", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", Stopsign!)
		DESTROY lnv_crypt		
		return -1
	end if
	
	if lnv_crypt.DecryptData(ls_logpass, ls_logpass) < 0 then
		ls_logpass = ""
	end if

	ltran_1.LogPass = ls_logpass
	
	DESTROY lnv_crypt

end if


ltran_1.DBParm = ls_dbparm

ltran_1.Autocommit= true

li_risposta = registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "lock", ltran_1.Lock)
if li_risposta = -1 then
	g_mb.messagebox("frame","Mancano le impostazione del database sul registro.",stopsign!)
	return -1
end if

connect using ltran_1;

execute immediate "set chained off" ;

//execute immediate "commit" ;

dw_statistics.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[]) //scorre le righe selezionate dall'utente
	
	yield()
	
	st_1.text = string(ll_riga) + "/" + string(upperbound(ll_selected[]))
	
//	execute immediate "commit" ;
	
	ls_nome_tabella = dw_statistics.getitemstring(ll_selected[ll_riga],"sysobjects_name")
	ls_sql = "dbcc checktable(" + ls_nome_tabella +")"
	yield()

	execute immediate :ls_sql using ltran_1;
	
	if ltran_1.sqlcode < 0 then
		
		st_2.text =  ltran_1.sqlerrtext
		
		yield()
		
//		g_mb.messagebox("Frame","Errore sul database durante l'esecuzione del comando update statistics sulla tabella " + ls_nome_tabella + ". Errore DB:" + ltran_1.sqlerrtext,stopsign!)
//		return
	end if
	
//	execute immediate "commit" ;

next


setpointer (arrow!)

destroy ltran_1

g_mb.messagebox("Frame","Esecuzione del comando update statistics eseguita con successo.",information!)

end event

