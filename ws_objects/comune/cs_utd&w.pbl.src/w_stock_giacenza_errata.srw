﻿$PBExportHeader$w_stock_giacenza_errata.srw
$PBExportComments$Window stock con giacenza errata
forward
global type w_stock_giacenza_errata from w_cs_xx_principale
end type
type dw_stock_giacenza_errata from uo_cs_xx_dw within w_stock_giacenza_errata
end type
type st_titolo from statictext within w_stock_giacenza_errata
end type
type st_1 from statictext within w_stock_giacenza_errata
end type
type st_giacenza from statictext within w_stock_giacenza_errata
end type
type st_assegnata from statictext within w_stock_giacenza_errata
end type
type st_2 from statictext within w_stock_giacenza_errata
end type
type st_spedizione from statictext within w_stock_giacenza_errata
end type
type st_3 from statictext within w_stock_giacenza_errata
end type
type st_4 from statictext within w_stock_giacenza_errata
end type
type st_5 from statictext within w_stock_giacenza_errata
end type
end forward

global type w_stock_giacenza_errata from w_cs_xx_principale
int Width=3438
int Height=1701
boolean TitleBar=true
string Title="Stock prodotti"
dw_stock_giacenza_errata dw_stock_giacenza_errata
st_titolo st_titolo
st_1 st_1
st_giacenza st_giacenza
st_assegnata st_assegnata
st_2 st_2
st_spedizione st_spedizione
st_3 st_3
st_4 st_4
st_5 st_5
end type
global w_stock_giacenza_errata w_stock_giacenza_errata

on w_stock_giacenza_errata.create
int iCurrent
call w_cs_xx_principale::create
this.dw_stock_giacenza_errata=create dw_stock_giacenza_errata
this.st_titolo=create st_titolo
this.st_1=create st_1
this.st_giacenza=create st_giacenza
this.st_assegnata=create st_assegnata
this.st_2=create st_2
this.st_spedizione=create st_spedizione
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_stock_giacenza_errata
this.Control[iCurrent+2]=st_titolo
this.Control[iCurrent+3]=st_1
this.Control[iCurrent+4]=st_giacenza
this.Control[iCurrent+5]=st_assegnata
this.Control[iCurrent+6]=st_2
this.Control[iCurrent+7]=st_spedizione
this.Control[iCurrent+8]=st_3
this.Control[iCurrent+9]=st_4
this.Control[iCurrent+10]=st_5
end on

on w_stock_giacenza_errata.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_stock_giacenza_errata)
destroy(this.st_titolo)
destroy(this.st_1)
destroy(this.st_giacenza)
destroy(this.st_assegnata)
destroy(this.st_2)
destroy(this.st_spedizione)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
end on

event pc_setwindow;call super::pc_setwindow;dw_stock_giacenza_errata.set_dw_options(sqlca, &
                                       pcca.null_object, &
												   c_nonew + &
													c_nodelete, &
                                       c_default)
													
													
st_titolo.text = "Stock del prodotto " + s_cs_xx.parametri.parametro_s_1 + " " + s_cs_xx.parametri.parametro_s_2

st_giacenza.text = string(s_cs_xx.parametri.parametro_d_1)
st_assegnata.text = string(s_cs_xx.parametri.parametro_d_2)
st_spedizione.text = string(s_cs_xx.parametri.parametro_d_3)


end event

type dw_stock_giacenza_errata from uo_cs_xx_dw within w_stock_giacenza_errata
int X=23
int Y=281
int Width=3361
int Height=1221
int TabOrder=10
string DataObject="d_stock_giacenza_errata"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda,s_cs_xx.parametri.parametro_s_1)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then

	string ls_cod_prodotto
	double ldd_somma_giacenza,ldd_somma_quan_assegnata,ldd_somma_quan_in_spedizione
	
	ls_cod_prodotto = s_cs_xx.parametri.parametro_s_1
//	ldd_somma_giacenza = getitemnumber(1,"somma_giacenza")
	ldd_somma_quan_assegnata = getitemnumber(1,"somma_assegnata")
	ldd_somma_quan_in_spedizione = getitemnumber(1,"somma_spedizione")
	
	
	update anag_prodotti
	set    quan_assegnata =:ldd_somma_quan_assegnata,
			 quan_in_spedizione=:ldd_somma_quan_in_spedizione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
end if
end event

type st_titolo from statictext within w_stock_giacenza_errata
int X=23
int Y=21
int Width=3361
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Stock del prodotto"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_stock_giacenza_errata
int X=23
int Y=181
int Width=503
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Giacenza Prodotto:"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_giacenza from statictext within w_stock_giacenza_errata
int X=526
int Y=181
int Width=503
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_assegnata from statictext within w_stock_giacenza_errata
int X=1532
int Y=181
int Width=503
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_stock_giacenza_errata
int X=1121
int Y=181
int Width=412
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Qtà assegnata:"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_spedizione from statictext within w_stock_giacenza_errata
int X=2606
int Y=181
int Width=503
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_stock_giacenza_errata
int X=2126
int Y=181
int Width=471
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Qtà in spedizione:"
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_stock_giacenza_errata
int X=23
int Y=101
int Width=3361
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Quantità riferite ad anagrafica prodotti:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_stock_giacenza_errata
int X=23
int Y=1521
int Width=3361
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Attenzione! la modifica può compromettere l'integrita dei dati del magazzino. Modificare solo in caso di necessità assoluta."
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=255
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

