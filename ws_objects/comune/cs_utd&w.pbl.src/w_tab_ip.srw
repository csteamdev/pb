﻿$PBExportHeader$w_tab_ip.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_tab_ip from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_ip
end type
end forward

global type w_tab_ip from w_cs_xx_principale
integer width = 2423
integer height = 1644
string title = "Tabella IP"
dw_lista dw_lista
end type
global w_tab_ip w_tab_ip

event open;call super::open;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)

end event

on w_tab_ip.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_ip.destroy
call super::destroy
destroy(this.dw_lista)
end on

type dw_lista from uo_cs_xx_dw within w_tab_ip
integer x = 23
integer y = 20
integer width = 2336
integer height = 1500
integer taborder = 10
string dataobject = "d_tab_ip_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

