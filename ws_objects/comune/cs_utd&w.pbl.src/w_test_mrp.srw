﻿$PBExportHeader$w_test_mrp.srw
forward
global type w_test_mrp from window
end type
type st_4 from statictext within w_test_mrp
end type
type st_3 from statictext within w_test_mrp
end type
type st_2 from statictext within w_test_mrp
end type
type cb_nessuno from commandbutton within w_test_mrp
end type
type cb_sel_tutti from commandbutton within w_test_mrp
end type
type mle_log from multilineedit within w_test_mrp
end type
type dw_test_mrp from datawindow within w_test_mrp
end type
type cb_ordinato from commandbutton within w_test_mrp
end type
type cb_schedula from commandbutton within w_test_mrp
end type
type cb_impegnato from commandbutton within w_test_mrp
end type
end forward

global type w_test_mrp from window
integer width = 4018
integer height = 2792
boolean titlebar = true
string title = "mrp"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event ue_scroll_mle ( )
st_4 st_4
st_3 st_3
st_2 st_2
cb_nessuno cb_nessuno
cb_sel_tutti cb_sel_tutti
mle_log mle_log
dw_test_mrp dw_test_mrp
cb_ordinato cb_ordinato
cb_schedula cb_schedula
cb_impegnato cb_impegnato
end type
global w_test_mrp w_test_mrp

event ue_scroll_mle();mle_log.scroll(mle_log.linecount())

mle_log.SetRedraw(TRUE)

return
end event

on w_test_mrp.create
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.cb_nessuno=create cb_nessuno
this.cb_sel_tutti=create cb_sel_tutti
this.mle_log=create mle_log
this.dw_test_mrp=create dw_test_mrp
this.cb_ordinato=create cb_ordinato
this.cb_schedula=create cb_schedula
this.cb_impegnato=create cb_impegnato
this.Control[]={this.st_4,&
this.st_3,&
this.st_2,&
this.cb_nessuno,&
this.cb_sel_tutti,&
this.mle_log,&
this.dw_test_mrp,&
this.cb_ordinato,&
this.cb_schedula,&
this.cb_impegnato}
end on

on w_test_mrp.destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_nessuno)
destroy(this.cb_sel_tutti)
destroy(this.mle_log)
destroy(this.dw_test_mrp)
destroy(this.cb_ordinato)
destroy(this.cb_schedula)
destroy(this.cb_impegnato)
end on

event open;dw_test_mrp.settransobject(sqlca)
dw_test_mrp.retrieve(s_cs_xx.cod_azienda)
end event

type st_4 from statictext within w_test_mrp
integer x = 494
integer y = 2552
integer width = 1723
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 553648127
string text = "Schedulazione sulla base dei movimenti MRP aggiunti al punto 1 e 2"
boolean focusrectangle = false
end type

type st_3 from statictext within w_test_mrp
integer x = 494
integer y = 2404
integer width = 1605
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 553648127
string text = "Vengono elaborate le righe ordine fornitore con flag_mrp=N"
boolean focusrectangle = false
end type

type st_2 from statictext within w_test_mrp
integer x = 494
integer y = 2264
integer width = 1605
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 553648127
string text = "Vengono elaborate le righe ordine cliente con flag_mrp=N"
boolean focusrectangle = false
end type

type cb_nessuno from commandbutton within w_test_mrp
integer x = 1691
integer y = 40
integer width = 402
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "NESSUNO"
end type

event clicked;long 			ll_i


for ll_i=1 to dw_test_mrp.rowcount()
	dw_test_mrp.setitem(ll_i, "flag_selezione", "N")
next
end event

type cb_sel_tutti from commandbutton within w_test_mrp
integer x = 1275
integer y = 40
integer width = 402
integer height = 96
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "SEL. TUTTI"
end type

event clicked;long 			ll_i


for ll_i=1 to dw_test_mrp.rowcount()
	dw_test_mrp.setitem(ll_i, "flag_selezione", "S")
next
end event

type mle_log from multilineedit within w_test_mrp
integer x = 2121
integer y = 36
integer width = 1810
integer height = 2196
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type dw_test_mrp from datawindow within w_test_mrp
integer x = 27
integer y = 164
integer width = 2066
integer height = 2064
integer taborder = 30
string title = "none"
string dataobject = "d_test_mrp"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;long ll_i
if isvalid(dwo) then
	if dwo.name = "flag_selezione_t" then
		setredraw(false)
		for ll_i = 1 to rowcount()
			if getitemstring(ll_i, "flag_selezione") = "S" then
				this.setitem(ll_i, "flag_selezione","N")
			else
				this.setitem(ll_i, "flag_selezione","S")
			end if
		next
		setredraw(true)
	end if
end if
				
Yield()
end event

type cb_ordinato from commandbutton within w_test_mrp
integer x = 23
integer y = 2388
integer width = 453
integer height = 112
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "2. ORDINATO"
end type

event clicked;string ls_errore, ls_cod_prodotto
uo_service_mov_mrp luo_mrp
long ll_tot, ll_i, ll_index


mle_log.setredraw(false)
mle_log.textcolor = rgb(0,0,255)
mle_log.text = "Elaborazione ORDINATO in corso ...~r~n"
parent.event trigger ue_scroll_mle()

ll_tot = 0
for ll_i=1 to dw_test_mrp.rowcount()
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ll_tot += 1
	end if
next

ll_index = 0
luo_mrp = create uo_service_mov_mrp
luo_mrp.uof_init("MRP_Test",5)

for ll_i = 1 to dw_test_mrp.rowcount()
	Yield()
	
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ls_cod_prodotto = dw_test_mrp.getitemstring(ll_i, 1)
		
		ll_index += 1
		
		mle_log.setredraw(false)
		mle_log.text += "Elaborazione ORDINATO prodotto "+ls_cod_prodotto+ " (" + string(ll_index) + " di " + string(ll_tot) + ") in corso ...~r~n"
		parent.event trigger ue_scroll_mle()
		
		if luo_mrp.uof_insert_ordinato_prodotto(ls_cod_prodotto, "ORD", ls_errore) < 0 then
			rollback;
			
			mle_log.setredraw(false)
			mle_log.textcolor = rgb(255,0,0)
			mle_log.text = ls_errore + "~r~n"
			parent.event trigger ue_scroll_mle()
			
		else
			commit;
		end if
	end if
next

destroy luo_mrp

mle_log.setredraw(false)
mle_log.text = "Elaborazione ORDINATO terminata con successo!~r~n"
parent.event trigger ue_scroll_mle()




end event

type cb_schedula from commandbutton within w_test_mrp
integer x = 23
integer y = 2532
integer width = 453
integer height = 112
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "3. SCHEDULA"
end type

event clicked;long					ll_i, ll_tot, ll_index
string				ls_errore, ls_cod_prodotto
uo_service_mov_mrp luo_mrp

mle_log.setredraw(false)
mle_log.textcolor = rgb(0,0,255)
mle_log.text = "SCHEDULAZIONE in corso ...~r~n"
parent.event trigger ue_scroll_mle()


ll_tot = 0
for ll_i=1 to dw_test_mrp.rowcount()
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ll_tot += 1
	end if
next

ll_index = 0

luo_mrp = create uo_service_mov_mrp
luo_mrp.uof_init("MRP_Scheduler",5)

for ll_i = 1 to dw_test_mrp.rowcount()
	
	Yield()
	
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ls_cod_prodotto = dw_test_mrp.getitemstring(ll_i, 1)
		
		ll_index += 1
		
		mle_log.setredraw(false)
		mle_log.text += "SCHEDULAZIONE prodotto "+ls_cod_prodotto+ " (" + string(ll_index) + " di " + string(ll_tot) + ") in corso ...~r~n"
		parent.event trigger ue_scroll_mle()
		
		if luo_mrp.uof_mrp_scheduler( dw_test_mrp.getitemstring(ll_i, 1)  , ls_errore) < 0 then
			
			mle_log.setredraw(false)
			mle_log.textcolor = rgb(255,0,0)
			mle_log.text += "ERRORE prodotto "+ls_cod_prodotto+" : " + ls_errore + "~r~n"
			parent.event trigger ue_scroll_mle()
			rollback;
			
		else
			commit;
		end if
	end if
next

destroy luo_mrp

mle_log.setredraw(false)
mle_log.text += "SCHEDULAZIONE terminata!~r~n"
parent.event trigger ue_scroll_mle()

end event

type cb_impegnato from commandbutton within w_test_mrp
integer x = 23
integer y = 2248
integer width = 453
integer height = 112
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "1. IMPEGNATO"
end type

event clicked;string ls_errore, ls_cod_prodotto
uo_service_mov_mrp luo_mrp
long	ll_tot, ll_i, ll_index


mle_log.setredraw(false)
mle_log.textcolor = rgb(0,0,255)
mle_log.text = "Elaborazione IMPEGNATO in corso ...~r~n"
parent.event trigger ue_scroll_mle()

ll_tot = 0
for ll_i=1 to dw_test_mrp.rowcount()
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ll_tot += 1
	end if
next

ll_index = 0
luo_mrp = create uo_service_mov_mrp
luo_mrp.uof_init("MRP_Test",5)

for ll_i = 1 to dw_test_mrp.rowcount()
	Yield()
	
	if dw_test_mrp.getitemstring(ll_i, "flag_selezione") = "S" then
		ls_cod_prodotto = dw_test_mrp.getitemstring(ll_i, 1)
		
		ll_index += 1
		
		mle_log.setredraw(false)
		mle_log.text += "Elaborazione IMPEGNATO prodotto "+ls_cod_prodotto+ " (" + string(ll_index) + " di " + string(ll_tot) + ") in corso ...~r~n"
		parent.event trigger ue_scroll_mle()

		if luo_mrp.uof_insert_impegnato_prodotto(ls_cod_prodotto, "IMP", ls_errore) < 0 then
			rollback;
			
			mle_log.setredraw(false)
			mle_log.textcolor = rgb(255,0,0)
			mle_log.text = ls_errore + "~r~n"
			parent.event trigger ue_scroll_mle()
			
		else
			commit;
		end if
	end if
next

destroy luo_mrp

mle_log.setredraw(false)
mle_log.text = "Elaborazione IMPEGNATO terminata!~r~n"
parent.event trigger ue_scroll_mle()



end event

