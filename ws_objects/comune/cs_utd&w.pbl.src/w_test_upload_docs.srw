﻿$PBExportHeader$w_test_upload_docs.srw
forward
global type w_test_upload_docs from window
end type
type em_anno from editmask within w_test_upload_docs
end type
type cb_archivia_fattura from commandbutton within w_test_upload_docs
end type
type st_10 from statictext within w_test_upload_docs
end type
type st_9 from statictext within w_test_upload_docs
end type
type st_8 from statictext within w_test_upload_docs
end type
type st_7 from statictext within w_test_upload_docs
end type
type sle_download_folder from singlelineedit within w_test_upload_docs
end type
type st_6 from statictext within w_test_upload_docs
end type
type cb_4 from commandbutton within w_test_upload_docs
end type
type st_5 from statictext within w_test_upload_docs
end type
type cb_3 from commandbutton within w_test_upload_docs
end type
type sle_file_hash from singlelineedit within w_test_upload_docs
end type
type cb_2 from commandbutton within w_test_upload_docs
end type
type cb_1 from commandbutton within w_test_upload_docs
end type
type dw_1 from datawindow within w_test_upload_docs
end type
type sle_endpoint from singlelineedit within w_test_upload_docs
end type
type st_4 from statictext within w_test_upload_docs
end type
type cb_up_m from commandbutton within w_test_upload_docs
end type
type cb_clear_log from commandbutton within w_test_upload_docs
end type
type cb_up_sf from commandbutton within w_test_upload_docs
end type
type mle_log from multilineedit within w_test_upload_docs
end type
type sle_token_type from singlelineedit within w_test_upload_docs
end type
type sle_password from singlelineedit within w_test_upload_docs
end type
type sle_username from singlelineedit within w_test_upload_docs
end type
type st_3 from statictext within w_test_upload_docs
end type
type st_2 from statictext within w_test_upload_docs
end type
type st_1 from statictext within w_test_upload_docs
end type
type cb_login from commandbutton within w_test_upload_docs
end type
type em_numero from editmask within w_test_upload_docs
end type
type st_11 from statictext within w_test_upload_docs
end type
end forward

global type w_test_upload_docs from window
integer width = 2990
integer height = 3036
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
em_anno em_anno
cb_archivia_fattura cb_archivia_fattura
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
sle_download_folder sle_download_folder
st_6 st_6
cb_4 cb_4
st_5 st_5
cb_3 cb_3
sle_file_hash sle_file_hash
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
sle_endpoint sle_endpoint
st_4 st_4
cb_up_m cb_up_m
cb_clear_log cb_clear_log
cb_up_sf cb_up_sf
mle_log mle_log
sle_token_type sle_token_type
sle_password sle_password
sle_username sle_username
st_3 st_3
st_2 st_2
st_1 st_1
cb_login cb_login
em_numero em_numero
st_11 st_11
end type
global w_test_upload_docs w_test_upload_docs

type variables
uo_upload iuo_upload
end variables

forward prototypes
public subroutine wf_log (string as_message)
end prototypes

public subroutine wf_log (string as_message);mle_log.text += as_message + "~r~n"
end subroutine

on w_test_upload_docs.create
this.em_anno=create em_anno
this.cb_archivia_fattura=create cb_archivia_fattura
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.sle_download_folder=create sle_download_folder
this.st_6=create st_6
this.cb_4=create cb_4
this.st_5=create st_5
this.cb_3=create cb_3
this.sle_file_hash=create sle_file_hash
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
this.sle_endpoint=create sle_endpoint
this.st_4=create st_4
this.cb_up_m=create cb_up_m
this.cb_clear_log=create cb_clear_log
this.cb_up_sf=create cb_up_sf
this.mle_log=create mle_log
this.sle_token_type=create sle_token_type
this.sle_password=create sle_password
this.sle_username=create sle_username
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.cb_login=create cb_login
this.em_numero=create em_numero
this.st_11=create st_11
this.Control[]={this.em_anno,&
this.cb_archivia_fattura,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.sle_download_folder,&
this.st_6,&
this.cb_4,&
this.st_5,&
this.cb_3,&
this.sle_file_hash,&
this.cb_2,&
this.cb_1,&
this.dw_1,&
this.sle_endpoint,&
this.st_4,&
this.cb_up_m,&
this.cb_clear_log,&
this.cb_up_sf,&
this.mle_log,&
this.sle_token_type,&
this.sle_password,&
this.sle_username,&
this.st_3,&
this.st_2,&
this.st_1,&
this.cb_login,&
this.em_numero,&
this.st_11}
end on

on w_test_upload_docs.destroy
destroy(this.em_anno)
destroy(this.cb_archivia_fattura)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.sle_download_folder)
destroy(this.st_6)
destroy(this.cb_4)
destroy(this.st_5)
destroy(this.cb_3)
destroy(this.sle_file_hash)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
destroy(this.sle_endpoint)
destroy(this.st_4)
destroy(this.cb_up_m)
destroy(this.cb_clear_log)
destroy(this.cb_up_sf)
destroy(this.mle_log)
destroy(this.sle_token_type)
destroy(this.sle_password)
destroy(this.sle_username)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_login)
destroy(this.em_numero)
destroy(this.st_11)
end on

event open;iuo_upload = create uo_upload
end event

type em_anno from editmask within w_test_upload_docs
integer x = 1934
integer y = 984
integer width = 325
integer height = 96
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2015"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
end type

type cb_archivia_fattura from commandbutton within w_test_upload_docs
integer x = 1280
integer y = 972
integer width = 635
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "archivia fattura"
end type

type st_10 from statictext within w_test_upload_docs
integer x = 23
integer y = 1720
integer width = 2903
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 15780518
string text = "LOG"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_9 from statictext within w_test_upload_docs
integer y = 1280
integer width = 2903
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 15780518
string text = "3. DOWNLOAD"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_8 from statictext within w_test_upload_docs
integer x = 23
integer y = 480
integer width = 2903
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 15780518
string text = "2. UPLOAD"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_7 from statictext within w_test_upload_docs
integer x = 23
integer y = 20
integer width = 2903
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 15780518
string text = "1. LOGIN"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_download_folder from singlelineedit within w_test_upload_docs
integer x = 526
integer y = 1460
integer width = 2080
integer height = 80
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "C:\temp\"
borderstyle borderstyle = stylelowered!
end type

type st_6 from statictext within w_test_upload_docs
integer x = 23
integer y = 1460
integer width = 489
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cartella Download"
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_test_upload_docs
integer x = 1577
integer y = 1560
integer width = 1029
integer height = 100
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Download su cartella"
end type

event clicked;
try
	iuo_upload.download(sle_file_hash.text, sle_download_folder.text)
	wf_log("Download completato")
catch(Exception e)
	wf_log("Errore: " + e.getMessage())
end try
end event

type st_5 from statictext within w_test_upload_docs
integer x = 23
integer y = 1380
integer width = 434
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Hash Documento"
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_test_upload_docs
integer x = 23
integer y = 1560
integer width = 1029
integer height = 100
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Download manuale"
end type

event clicked;blob lbob_file
str_download lstr_file

try
	iuo_upload.download(sle_file_hash.text, ref lbob_file, lstr_file)
	
	wf_log("Download completato")
	wf_log("~tNome: " + lstr_file.filename)
	wf_log("~tMimetype: " + lstr_file.mimetype)
	wf_log("~tDimensione: " + string(lstr_file.size))
	
	guo_functions.uof_blob_to_file(lbob_file, "C:\temp\" + lstr_file.filename)
	
	wf_log("Salvato con successo")
catch(Exception e)
	wf_log("Errore: " + e.getMessage())
end try
end event

type sle_file_hash from singlelineedit within w_test_upload_docs
integer x = 526
integer y = 1380
integer width = 2080
integer height = 80
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "22845d1776f70bcdbe443c8bc8c14d7e9034494840007281ca4b616dc94ce62e"
borderstyle borderstyle = stylelowered!
end type

type cb_2 from commandbutton within w_test_upload_docs
integer x = 846
integer y = 580
integer width = 389
integer height = 100
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cancella"
end type

event clicked;dw_1.reset()
end event

type cb_1 from commandbutton within w_test_upload_docs
integer x = 46
integer y = 580
integer width = 206
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "+"
end type

event clicked;dw_1.insertrow(0)
end event

type dw_1 from datawindow within w_test_upload_docs
integer x = 46
integer y = 700
integer width = 1189
integer height = 540
integer taborder = 30
string title = "none"
string dataobject = "d_metadata"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_endpoint from singlelineedit within w_test_upload_docs
string tag = "http://127.0.0.1:8080/cs-docs"
integer x = 457
integer y = 120
integer width = 1486
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "http://test.csteam.com/docs"
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_test_upload_docs
integer x = 46
integer y = 120
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "EndPoint:"
boolean focusrectangle = false
end type

type cb_up_m from commandbutton within w_test_upload_docs
integer x = 1280
integer y = 820
integer width = 635
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "3. Upload multiplo"
end type

event clicked;string ls_docpath, ls_docname[], ls_hash
int li_rtn, li_i, li_j
str_file_upload_request lstr_request
str_file_upload_response lstr_response

li_rtn = GetFileOpenName("Select File", ls_docpath, ls_docname[], "Tutti i file", "All Files (*.*), *.*")

if li_rtn = 1 then
	wf_log(ls_docpath)
	
	for li_i = 1 to upperbound(ls_docname)
		wf_log("Selezionato il file " + ls_docname[li_i])
		
		try
			lstr_request.name = ls_docpath + "\" + ls_docname[li_i]
			
			dw_1.accepttext()
			for li_j = 1 to dw_1.rowcount()
				lstr_request.metadati[li_i].meta_key = dw_1.getitemstring(li_i, 1)
				lstr_request.metadati[li_i].meta_value = dw_1.getitemstring(li_i, 2)
			next

			iuo_upload.upload(lstr_request, lstr_response)
			
			wf_log("File: " + ls_hash)
			wf_log("~tname: " + lstr_response.name)
			wf_log("~textension: " + lstr_response.extension)
			wf_log("~tmimetype: " + lstr_response.mimetype)
			wf_log("~thash: " + lstr_response.hash)
		catch (Exception e)
			wf_log("Error: " + e.getmessage())
		end try
	next
end if
end event

type cb_clear_log from commandbutton within w_test_upload_docs
integer x = 2149
integer y = 120
integer width = 754
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Clear Log"
end type

event clicked;mle_log.text = ""
end event

type cb_up_sf from commandbutton within w_test_upload_docs
integer x = 1280
integer y = 700
integer width = 635
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "2. Upload singolo file"
end type

event clicked;string ls_docpath, ls_docname[], ls_hash
int li_rtn, li_i
str_file_upload_response lstr_response
str_file_upload_request lstr_request

li_rtn = GetFileOpenName("Select File", ls_docpath, ls_docname[], "Tutti i file", "All Files (*.*), *.*")

if li_rtn = 1 then
	wf_log("Selezionato il file " + ls_docpath)
	
	try
		lstr_request.name = ls_docpath
		
		for li_i = 1 to dw_1.rowcount()
			lstr_request.metadati[li_i].meta_key = dw_1.getitemstring(li_i, 1)
			lstr_request.metadati[li_i].meta_value = dw_1.getitemstring(li_i, 2)
		next
		
		
		iuo_upload.upload(lstr_request, lstr_response)
	
		wf_log("File caricato")
		wf_log("~tname: " + lstr_response.name)
		wf_log("~textension: " + lstr_response.extension)
		wf_log("~tmimetype: " + lstr_response.mimetype)
		wf_log("~thash: " + lstr_response.hash)
		
	catch (Exception e)
		wf_log("Error: " + e.getmessage())
	end try
end if
end event

type mle_log from multilineedit within w_test_upload_docs
integer x = 23
integer y = 1820
integer width = 2903
integer height = 1080
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type sle_token_type from singlelineedit within w_test_upload_docs
integer x = 457
integer y = 360
integer width = 1486
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_password from singlelineedit within w_test_upload_docs
integer x = 457
integer y = 280
integer width = 526
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "admin"
borderstyle borderstyle = stylelowered!
end type

type sle_username from singlelineedit within w_test_upload_docs
integer x = 457
integer y = 200
integer width = 526
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "admin"
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_test_upload_docs
integer x = 46
integer y = 360
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Token Type:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_test_upload_docs
integer x = 46
integer y = 280
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Password:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_test_upload_docs
integer x = 46
integer y = 200
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Username:"
boolean focusrectangle = false
end type

type cb_login from commandbutton within w_test_upload_docs
integer x = 2149
integer y = 260
integer width = 754
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1. Login"
end type

event clicked;iuo_upload.setEndpointUrl(sle_endpoint.text)
	
wf_log("Eseguo login")
wf_log("~tusername:" + sle_username.text)
wf_log("~tpassword: " +  sle_password.text)

try 
	iuo_upload.login(sle_username.text, sle_password.text, sle_token_type.text)
		
	wf_log("Token: ")
	wf_log("~ttoken: " +  iuo_upload.token().token)
	wf_log("~tlifetime: " +  string(iuo_upload.token().lifetime))
	wf_log("~tuserId: " +  string(iuo_upload.token().userId))
	
	cb_up_sf.enabled = true
	cb_up_m.enabled = true
	cb_archivia_fattura.enabled = true
	
catch(Exception ex)
	cb_up_sf.enabled = false
	cb_up_m.enabled = false
	cb_archivia_fattura.enabled = false
	
	wf_log(ex.getmessage())
end try
end event

type em_numero from editmask within w_test_upload_docs
integer x = 2345
integer y = 984
integer width = 507
integer height = 96
integer taborder = 40
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####0"
boolean spin = true
end type

type st_11 from statictext within w_test_upload_docs
integer x = 2263
integer y = 988
integer width = 96
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

