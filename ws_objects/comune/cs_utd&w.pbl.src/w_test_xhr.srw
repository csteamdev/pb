﻿$PBExportHeader$w_test_xhr.srw
forward
global type w_test_xhr from window
end type
type mle_response from multilineedit within w_test_xhr
end type
type sle_url from singlelineedit within w_test_xhr
end type
type cb_send from commandbutton within w_test_xhr
end type
end forward

global type w_test_xhr from window
integer width = 1970
integer height = 1480
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
mle_response mle_response
sle_url sle_url
cb_send cb_send
end type
global w_test_xhr w_test_xhr

on w_test_xhr.create
this.mle_response=create mle_response
this.sle_url=create sle_url
this.cb_send=create cb_send
this.Control[]={this.mle_response,&
this.sle_url,&
this.cb_send}
end on

on w_test_xhr.destroy
destroy(this.mle_response)
destroy(this.sle_url)
destroy(this.cb_send)
end on

type mle_response from multilineedit within w_test_xhr
integer x = 37
integer y = 352
integer width = 1847
integer height = 976
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
end type

type sle_url from singlelineedit within w_test_xhr
integer x = 37
integer y = 176
integer width = 1847
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "http://test.csteam.com/api/v1/server/ping"
borderstyle borderstyle = stylelowered!
end type

type cb_send from commandbutton within w_test_xhr
integer x = 37
integer y = 48
integer width = 430
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia Richiesta"
end type

event clicked;uo_xhr2 luo_xhr
oleobject lole_json

luo_xhr = create uo_xhr2

//luo_xhr.getJson(sle_url.text)
//
//if luo_xhr.send() = 200 then
//	mle_response.text = luo_xhr.responsetext()
//end if

luo_xhr.getJson(sle_url.text)
if luo_xhr.send() = 200 then
	try 
		luo_xhr.responsejson(lole_json)
		
		if lole_json.live then
		end if
		
		mle_response.text = string(lole_json.live)
	catch (XhrException e)
	end try
end if

destroy luo_xhr
end event

