﻿$PBExportHeader$w_utenti_divisioni.srw
forward
global type w_utenti_divisioni from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_utenti_divisioni
end type
type st_help from statictext within w_utenti_divisioni
end type
type dw_utenti from datawindow within w_utenti_divisioni
end type
type tv_1 from treeview within w_utenti_divisioni
end type
end forward

global type w_utenti_divisioni from w_cs_xx_principale
integer width = 3689
integer height = 2592
string title = "Utenti Divisoni"
cb_1 cb_1
st_help st_help
dw_utenti dw_utenti
tv_1 tv_1
end type
global w_utenti_divisioni w_utenti_divisioni

type variables
private:

	string is_cod_utente
	string is_des_utente
	
	long il_handle
	
end variables

forward prototypes
public function integer wf_inserisci_nodo_root ()
public function integer wf_inserisci_nodi_divisioni ()
public function integer wf_inserisci_utente (integer al_handle, string as_cod_divisione, string as_cod_utente, string as_des_utente)
public function integer wf_inserisci_utenti (integer al_handle, string as_cod_divisione)
public function integer wf_inserisci_singolo_utente (integer al_handle, string as_cod_divisione, string as_cod_utente, string as_des_utente)
end prototypes

public function integer wf_inserisci_nodo_root ();/**
 * stefanop
 * 24/08/2015
  **/


treeviewitem ltvi_item
ltvi_item.expanded = true
ltvi_item.selected = false
ltvi_item.children = true

ltvi_item.label = "Divisioni"

return tv_1.insertitemlast(0, ltvi_item)

end function

public function integer wf_inserisci_nodi_divisioni ();/**
 * stefanop
 * 24/08/2015
 *
 * Carico le divisioni all'interno della TV
 **/

string ls_sql, ls_cod_divisione, ls_des_divisione
int li_i, ll_handle, li_count
datastore lds_store

ll_handle = wf_inserisci_nodo_root()
li_count = guo_functions.uof_crea_datastore(lds_store, "SELECT cod_divisione, des_divisione  FROM anag_divisioni WHERE cod_azienda='" + s_cs_xx.cod_azienda + "'")

for li_i = 1 to li_count
	str_treeview lstr_data
	treeviewitem ltvi_item
	
	ls_cod_divisione = lds_store.getitemstring(li_i, "cod_divisione")
	ls_des_divisione = lds_store.getitemstring(li_i, "des_divisione")
	
	lstr_data.livello = 1
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_divisione
	
	ltvi_item.expanded = false
	ltvi_item.selected = false
	ltvi_item.children = true
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_cod_divisione + " - " + ls_des_divisione
	
	tv_1.insertitemlast(ll_handle, ltvi_item)
	
next

return li_count
end function

public function integer wf_inserisci_utente (integer al_handle, string as_cod_divisione, string as_cod_utente, string as_des_utente);/**
 * stefanop
 * 24/08/2015
 *
 **/
 
int ll_count
str_treeview lstr_data
treeviewitem ltvi_item

select count(*)
into :ll_count
from utenti_divisioni
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente =  :as_cod_utente and
		cod_divisione = :as_cod_divisione;
		
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la verifica dell'utente per la divisione selezionata", sqlca)
	return -1
elseif ll_count > 0 then
	g_mb.warning("L'utente è già associato alla divisione selezionata")
	return 0
end if
 
insert into utenti_divisioni (cod_azienda, cod_utente, cod_divisione)
values(:s_cs_xx.cod_azienda, :as_cod_utente, :as_cod_divisione);

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante l'inserimento dell'utente nella divisione selezionata", sqlca)
	rollback;	
	return -1
end if



return wf_inserisci_singolo_utente(al_handle, as_cod_divisione, as_cod_utente, as_des_utente)

end function

public function integer wf_inserisci_utenti (integer al_handle, string as_cod_divisione);/**
 * stefanop
 * 24/08/2015
 *
 **/
 
string ls_cod_utente, ls_des_utente
int li_count, li_i
datastore lds_store
str_treeview lstr_data
treeviewitem ltvi_item


li_count = guo_functions.uof_crea_datastore(lds_store, "select utenti.cod_utente, utenti.nome_cognome " + &
	"from utenti_divisioni join utenti on " + &
	"utenti.cod_utente = utenti_divisioni.cod_utente and utenti.cod_azienda = utenti_divisioni.cod_azienda " + &
	"where utenti_divisioni.cod_azienda='" + s_cs_xx.cod_azienda + "' and utenti_divisioni.cod_divisione='" + as_cod_divisione + "' ")
	
for li_i = 1 to li_count
	
	ls_cod_utente = lds_store.getitemstring(li_i,  1)
	ls_des_utente = lds_store.getitemstring(li_i,  2)
	
	wf_inserisci_singolo_utente(al_handle, as_cod_divisione, ls_cod_utente, ls_des_utente)
	
next

return li_count

end function

public function integer wf_inserisci_singolo_utente (integer al_handle, string as_cod_divisione, string as_cod_utente, string as_des_utente);/**
 * stefanop
 * 24/08/2015
 *
 **/
 
int ll_count
str_treeview lstr_data
treeviewitem ltvi_item

lstr_data.livello = 1
lstr_data.tipo_livello = "U"
lstr_data.codice = as_cod_utente
lstr_data.stringa[1] = as_cod_divisione

ltvi_item.expanded = false
ltvi_item.selected = false
ltvi_item.children = false

ltvi_item.data = lstr_data
ltvi_item.label = as_des_utente

return tv_1.insertitemlast(al_handle, ltvi_item)

end function

on w_utenti_divisioni.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.st_help=create st_help
this.dw_utenti=create dw_utenti
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.st_help
this.Control[iCurrent+3]=this.dw_utenti
this.Control[iCurrent+4]=this.tv_1
end on

on w_utenti_divisioni.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.st_help)
destroy(this.dw_utenti)
destroy(this.tv_1)
end on

event pc_setwindow;call super::pc_setwindow;

wf_inserisci_nodi_divisioni()

dw_utenti.settransobject(sqlca)
dw_utenti.retrieve(s_cs_xx.cod_azienda)


end event

type cb_1 from commandbutton within w_utenti_divisioni
integer x = 1714
integer y = 2328
integer width = 1888
integer height = 112
integer taborder = 30
string dragicon = "UserObject5!"
boolean dragauto = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

type st_help from statictext within w_utenti_divisioni
integer x = 1719
integer y = 36
integer width = 1861
integer height = 176
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 8421504
long backcolor = 12632256
string text = "Trasciara il codice dell~'utente nella divisione per aggiungerlo.Seleziona l~'utente dalla lista di sinistra e premi CANC per eliminarlo"
boolean focusrectangle = false
end type

type dw_utenti from datawindow within w_utenti_divisioni
integer x = 1714
integer y = 228
integer width = 1888
integer height = 2020
integer taborder = 20
string title = "none"
string dataobject = "d_utenti_divisioni_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;
if currentrow > 0 then
	
	is_cod_utente = getitemstring(currentrow, "cod_utente")
	is_des_utente = getitemstring(currentrow, "nome_cognome")
	
	cb_1.text = "Trascina " + is_cod_utente + " - " + is_des_utente
	
end if
end event

type tv_1 from treeview within w_utenti_divisioni
event ue_keydown pbm_keydown
integer x = 37
integer y = 28
integer width = 1632
integer height = 2432
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event dragwithin;TreeViewItem ltvi_item

if GetItem(handle, ltvi_item) = -1 then
	SetDropHighlight(0)
	il_handle = -1
else
	SetDropHighlight(handle)
	il_handle = handle
end if


end event

event dragdrop;long ll_drop, ll_drag

treeviewitem ltv_item
str_treeview lstr_data

if handle = 0 or isnull(handle) then
	return -1
end if

tv_1.getitem(handle, ltv_item)

lstr_data = ltv_item.data

if lstr_data.tipo_livello = "D" then
	
	wf_inserisci_utente(handle, lstr_data.codice, is_cod_utente, is_des_utente)
	
	if ltv_item.expanded = false then
		tv_1.ExpandItem(handle)
	end if
	
end if
	
SetDropHighlight(0)

return -1





end event

event itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if handle > 0 then

	getitem(handle, ltvi_item)
	
	lstr_data = ltvi_item.data
	
	wf_inserisci_utenti(handle, lstr_data.codice)

end if
end event

event key;treeviewitem ltv_item
str_treeview lstr_data

IF KeyDown(KeyDelete!) = TRUE THEN
	il_handle = FindItem(CurrentTreeItem!, 0)
   	GetItem(il_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	if lstr_data.tipo_livello = "U" then
		
		if g_mb.confirm("Eliminare l'utente " + ltv_item.label + " dalla divisione selezionata?") then
			
			delete from utenti_divisioni
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_utente = :lstr_data.codice and
					cod_divisione = :lstr_data.stringa[1]
					;
		
			deleteitem(il_handle)
			return 0
		else
			 return -1
		end if
	end if  
end if

return 0
end event

