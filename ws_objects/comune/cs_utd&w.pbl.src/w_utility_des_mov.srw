﻿$PBExportHeader$w_utility_des_mov.srw
forward
global type w_utility_des_mov from window
end type
type dw_dettagli from datawindow within w_utility_des_mov
end type
type st_documento from statictext within w_utility_des_mov
end type
type dw_dest_mov from datawindow within w_utility_des_mov
end type
type cb_salva from commandbutton within w_utility_des_mov
end type
type cb_cerca from commandbutton within w_utility_des_mov
end type
type st_1 from statictext within w_utility_des_mov
end type
type em_numero from editmask within w_utility_des_mov
end type
type em_anno from editmask within w_utility_des_mov
end type
type rb_fat_ven from radiobutton within w_utility_des_mov
end type
type rb_ddt_ven from radiobutton within w_utility_des_mov
end type
type rb_ddt_acq from radiobutton within w_utility_des_mov
end type
end forward

global type w_utility_des_mov from window
integer width = 4640
integer height = 2140
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_dettagli dw_dettagli
st_documento st_documento
dw_dest_mov dw_dest_mov
cb_salva cb_salva
cb_cerca cb_cerca
st_1 st_1
em_numero em_numero
em_anno em_anno
rb_fat_ven rb_fat_ven
rb_ddt_ven rb_ddt_ven
rb_ddt_acq rb_ddt_acq
end type
global w_utility_des_mov w_utility_des_mov

forward prototypes
public function integer wf_ricerca ()
public function integer wf_get_des_mov (long al_riga)
end prototypes

public function integer wf_ricerca ();string			ls_tabella, ls_sql_righe, ls_syntax, ls_errore
integer		li_anno
long			ll_numero, ll_rows

li_anno = long(em_anno.text)
ll_numero = long(em_numero.text)

if rb_ddt_acq.checked then
	
	ls_tabella = "det_bol_acq"
	
	ls_sql_righe = "select anno_bolla_acq, num_bolla_acq, prog_riga_bolla_acq, cod_prodotto, anno_reg_des_mov, num_reg_des_mov "+&
						"from det_bol_acq " +  &
						" where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						 		"anno_bolla_acq="+string(li_anno)+" and num_bolla_acq="+string(ll_numero) + " " +&
						"order by prog_riga_bolla_acq"
	
//-----------------------------------------------------

elseif rb_ddt_ven.checked then
	//ls_tabella = "det_bol_ven"
	
	ls_sql_righe = "select anno_registrazione, num_registrazione, prog_riga_bol_ven, cod_prodotto, anno_reg_des_mov, num_reg_des_mov "+&
						"from det_bol_ven " + &
						" where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"anno_registrazione="+string(li_anno)+" and num_registrazione="+string(ll_numero) + " " +&
						"order by prog_riga_bol_ven"
//-----------------------------------------------------
	
elseif rb_fat_ven.checked then
	//ls_tabella = "det_fat_ven"
	
	ls_sql_righe = "select anno_registrazione, num_registrazione, prog_riga_fat_ven, cod_prodotto, anno_reg_des_mov, num_reg_des_mov "+&
						"from det_fat_ven " + &
						" where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"anno_registrazione="+string(li_anno)+" and num_registrazione="+string(ll_numero) + " " +&
						"order by prog_riga_fat_ven"
	
else
	return 1
end if

dw_dettagli.reset()

ls_syntax = SQLCA.SyntaxFromSQL(ls_sql_righe, 'Style(Type=Grid)', ls_errore)

if Len(ls_errore) > 0 then
	g_mb.error("Errore creazione sintassi SQL! " + ls_errore)
	return -1
else
	dw_dettagli.create(ls_syntax, ls_errore)
	if Len(ls_errore) > 0 then
		g_mb.error("Errore creazione datawindow! " + ls_errore)
		return -1
	end if
end if

dw_dettagli.SetTransObject(SQLCA)
dw_dettagli.Retrieve()

ll_rows = dw_dettagli.rowcount()

if ll_rows > 0 then
	wf_get_des_mov(1)
else
	dw_dest_mov.reset()
end if

return 0





end function

public function integer wf_get_des_mov (long al_riga);long				ll_anno_des_reg_mov, ll_num_reg_des_mov

if al_riga > 0 then
	ll_anno_des_reg_mov = dw_dettagli.getitemnumber(al_riga, "anno_reg_des_mov")
	ll_num_reg_des_mov = dw_dettagli.getitemnumber(al_riga, "num_reg_des_mov")
	
	st_documento.text = "Dati riga " + string(dw_dettagli.getitemnumber(al_riga, 1)) + "/" + string(dw_dettagli.getitemnumber(al_riga, 2)) + "/" + string(dw_dettagli.getitemnumber(al_riga, 3))
	
	dw_dest_mov.retrieve(s_cs_xx.cod_azienda, ll_anno_des_reg_mov, ll_num_reg_des_mov)
	
else
	st_documento.text = "Nessuna Riga selezionata!"
	dw_dest_mov.reset()
end if


return 0
end function

on w_utility_des_mov.create
this.dw_dettagli=create dw_dettagli
this.st_documento=create st_documento
this.dw_dest_mov=create dw_dest_mov
this.cb_salva=create cb_salva
this.cb_cerca=create cb_cerca
this.st_1=create st_1
this.em_numero=create em_numero
this.em_anno=create em_anno
this.rb_fat_ven=create rb_fat_ven
this.rb_ddt_ven=create rb_ddt_ven
this.rb_ddt_acq=create rb_ddt_acq
this.Control[]={this.dw_dettagli,&
this.st_documento,&
this.dw_dest_mov,&
this.cb_salva,&
this.cb_cerca,&
this.st_1,&
this.em_numero,&
this.em_anno,&
this.rb_fat_ven,&
this.rb_ddt_ven,&
this.rb_ddt_acq}
end on

on w_utility_des_mov.destroy
destroy(this.dw_dettagli)
destroy(this.st_documento)
destroy(this.dw_dest_mov)
destroy(this.cb_salva)
destroy(this.cb_cerca)
destroy(this.st_1)
destroy(this.em_numero)
destroy(this.em_anno)
destroy(this.rb_fat_ven)
destroy(this.rb_ddt_ven)
destroy(this.rb_ddt_acq)
end on

event open;
dw_dest_mov.SetTransObject(SQLCA)


f_po_loaddddw_dw(dw_dest_mov, &
                 "cod_tipo_mov_det", &
                 sqlca, &
                 "tab_tipi_movimenti_det", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_dettagli from datawindow within w_utility_des_mov
integer x = 23
integer y = 352
integer width = 4553
integer height = 856
integer taborder = 30
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;

if currentrow>0 then
	wf_get_des_mov(currentrow)
else
	dw_dest_mov.reset()
end if
end event

type st_documento from statictext within w_utility_des_mov
integer x = 55
integer y = 184
integer width = 4512
integer height = 124
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_dest_mov from datawindow within w_utility_des_mov
integer x = 23
integer y = 1232
integer width = 4553
integer height = 776
integer taborder = 30
string title = "none"
string dataobject = "d_utility_dest_mov"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_salva from commandbutton within w_utility_des_mov
integer x = 3287
integer y = 52
integer width = 325
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

type cb_cerca from commandbutton within w_utility_des_mov
integer x = 2921
integer y = 52
integer width = 325
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;

wf_ricerca()
end event

type st_1 from statictext within w_utility_des_mov
integer x = 2336
integer y = 64
integer width = 96
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_numero from editmask within w_utility_des_mov
integer x = 2450
integer y = 40
integer width = 402
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######0"
boolean spin = true
double increment = 1
string minmax = "1~~99999999"
end type

type em_anno from editmask within w_utility_des_mov
integer x = 1934
integer y = 40
integer width = 402
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "1~~9999"
end type

type rb_fat_ven from radiobutton within w_utility_des_mov
integer x = 1257
integer y = 52
integer width = 489
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "FAT vendita"
boolean lefttext = true
end type

type rb_ddt_ven from radiobutton within w_utility_des_mov
integer x = 690
integer y = 52
integer width = 489
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "DDT vendita"
boolean lefttext = true
end type

type rb_ddt_acq from radiobutton within w_utility_des_mov
integer x = 128
integer y = 52
integer width = 489
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "DDT acquisto"
boolean checked = true
boolean lefttext = true
end type

