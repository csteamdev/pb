﻿$PBExportHeader$w_valorizza_test.srw
forward
global type w_valorizza_test from window
end type
type mle_log from multilineedit within w_valorizza_test
end type
type em_data_movimento from editmask within w_valorizza_test
end type
type cb_salva from commandbutton within w_valorizza_test
end type
type st_log from statictext within w_valorizza_test
end type
type cb_leggi from commandbutton within w_valorizza_test
end type
type sle_sfoglia from singlelineedit within w_valorizza_test
end type
type cb_sfoglia from commandbutton within w_valorizza_test
end type
type dw_lista from datawindow within w_valorizza_test
end type
end forward

global type w_valorizza_test from window
integer width = 4690
integer height = 2948
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
mle_log mle_log
em_data_movimento em_data_movimento
cb_salva cb_salva
st_log st_log
cb_leggi cb_leggi
sle_sfoglia sle_sfoglia
cb_sfoglia cb_sfoglia
dw_lista dw_lista
end type
global w_valorizza_test w_valorizza_test

type variables
uo_excel_listini iuo_excel
end variables

forward prototypes
public function integer wf_leggi_fogli (string as_path, ref string as_errore)
public subroutine wf_log (string as_valore)
public function integer wf_leggi_lista (string as_foglio, ref string as_errore)
public function integer wf_movimenti (string as_cod_tipo_movimento, string as_cod_deposito, string as_cod_prodotto, decimal ad_quantita, decimal ad_valore_unitario, ref long al_anno_mov[], ref long al_num_mov[], ref string as_errore)
end prototypes

public function integer wf_leggi_fogli (string as_path, ref string as_errore);string				ls_sheets[], ls_foglio, ls_cod_prodotto, ls_err, ls_ok, ls_temp[]
long				ll_tot, ll_i, ll_new, ll_pos
integer			li_ret


st_log.text = ""

dw_lista.reset()

iuo_excel = create uo_excel_listini

iuo_excel.is_path_file = as_path
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_temp[])
ll_tot = upperbound(ls_temp[])

//inverti l'ordine dei fogli
ll_new = 0
for ll_i= ll_tot to 1 step -1
	ll_new += 1
	ls_sheets[ll_new] = ls_temp[ll_i]
next


if isnull(ll_tot) or ll_tot <= 0 then
	as_errore = "Non esistono fogli di lavoro nel file excel specificato!"
	iuo_excel.uof_close( )
	destroy iuo_excel
	return -1
end if


//leggo solo il primo
for ll_i = 1 to 1
	ls_foglio = ls_sheets[ll_i]
	
	wf_leggi_lista(ls_foglio, as_errore)
next

if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if


return 0
end function

public subroutine wf_log (string as_valore);

mle_log.text += "~r~n" + as_valore
mle_log.scroll(mle_log.linecount())
mle_log.SetRedraw(true)

return

end subroutine

public function integer wf_leggi_lista (string as_foglio, ref string as_errore);long				ll_index, ll_new
string				ls_valore, ls_codice, ls_um_mag, ls_des_prodotto, ls_flag_ok, ls_cod_originale
dec{4}			ld_valore, ld_giacenza, ld_costo_medio
dec{5}			ld_qta_mag[]
datetime			ldt_valore
integer			li_ret


ll_index = 1

//A (1)		codice prodotto
//B	 (2)		descrizione prodotto
//C (3)		UM
//D (4)		qta SACCOLONGO		(100)
//E (5)		qta VEGGIANO			(200)
//F (6)		qta PISTOIA				(300)
//G (7)		qta CUNEO				(400)
//H (8)		qta ANODALL			(513)
//I (9)			giacenza
//L (10)		costo medio
//M (11)		valore totale


do while true
	Yield()
	ll_index += 1
	
	st_log.text = "Elaborazione riga excel n° "+string(ll_index)
	
	//A (1)		codice prodotto
	iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 1, ls_codice, ld_valore, ldt_valore, "S")
	if ls_codice<>"" and not isnull(ls_codice) then
	
		//B	 (2)		descrizione prodotto
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 2, ls_des_prodotto, ld_valore, ldt_valore, "S")
		
		//C (3)		UM
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 3, ls_um_mag, ld_valore, ldt_valore, "S")
		
		//D (4)		qta SACCOLONGO		(100)
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 4, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore = 0
		ld_qta_mag[1] = ld_valore
		
		//E (5)		qta VEGGIANO			(200)
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 5, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore = 0
		ld_qta_mag[2] = ld_valore
		
		//F (6)		qta PISTOIA				(300)
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 6, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore = 0
		ld_qta_mag[3] = ld_valore
		
		//G (7)		qta CUNEO				(400)
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 7, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore = 0
		ld_qta_mag[4] = ld_valore
		
		//H (8)		qta ANODALL			(513)
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 8, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore = 0
		ld_qta_mag[5] = ld_valore
		
		//I (9)			giacenza
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 9, ls_valore, ld_giacenza, ldt_valore, "D")
		if isnull(ld_giacenza) or ld_giacenza<0 then ld_giacenza = 0
		
		//L (10)		costo medio
		//leggi costo STD
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 10, ls_valore, ld_costo_medio, ldt_valore, "D")
		if isnull(ld_costo_medio) or ld_costo_medio<0 then ld_costo_medio=0
		
		//M (11)		valore totale
		//leggi costo STD
		iuo_excel.uof_leggi_ottimistico(as_foglio, ll_index, 11, ls_valore, ld_valore, ldt_valore, "D")
		if isnull(ld_valore) or ld_valore<0 then ld_valore=0
	
		ll_new = dw_lista.insertrow(0)
		dw_lista.setitem(ll_new, "cod_prodotto", ls_codice)
		dw_lista.setitem(ll_new, "des_prodotto", ls_des_prodotto)
		dw_lista.setitem(ll_new, "cod_misura", ls_um_mag)
		
		dw_lista.setitem(ll_new, "d100", ld_qta_mag[1])
		dw_lista.setitem(ll_new, "d200", ld_qta_mag[2])
		dw_lista.setitem(ll_new, "d300", ld_qta_mag[3])
		dw_lista.setitem(ll_new, "d400", ld_qta_mag[4])
		dw_lista.setitem(ll_new, "d513", ld_qta_mag[5])
		
		dw_lista.setitem(ll_new, "giacenza", ld_giacenza)
		dw_lista.setitem(ll_new, "costo_medio", ld_costo_medio)
		dw_lista.setitem(ll_new, "valore_totale", ld_valore)
		
	else
		//giunto alla fine del file
		st_log.text = "Fine Elaborazione!"
		return 0
	end if

loop

return 0
end function

public function integer wf_movimenti (string as_cod_tipo_movimento, string as_cod_deposito, string as_cod_prodotto, decimal ad_quantita, decimal ad_valore_unitario, ref long al_anno_mov[], ref long al_num_mov[], ref string as_errore);

datetime				ldt_data_reg_mov, ldt_data_stock[]
string					ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], ls_referenza
long					ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock
uo_magazzino		luo_mag


ldt_data_reg_mov = datetime(date(em_data_movimento.text), 00:00:00) 
ls_referenza   = "**"+as_cod_prodotto+"**"
	
setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])
	
ls_cod_deposito[1] = as_cod_deposito
ls_cod_ubicazione[1] = "UB0001"
ls_cod_lotto[1] = "LT0001"
ldt_data_stock[1] = datetime(date(string("01/01/2000")),00:00:00)
ll_prog_stock[1] = 10
	
	
luo_mag = create uo_magazzino
	
if f_crea_dest_mov_magazzino (as_cod_tipo_movimento, &
											as_cod_prodotto, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_cliente[], &
											ls_cod_fornitore[], &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock ) = -1 then
											
	as_errore =  "Errore: f_crea_dest_mov_magazzino"
	destroy luo_mag
	return -1
end if
		
if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
								 ll_num_reg_dest_stock, &
								 as_cod_tipo_movimento, &
								 as_cod_prodotto) = -1 then
									 
	as_errore = "Errore: f_verifica_dest_mov_mag"
	destroy luo_mag
	return -1
end if

if luo_mag.uof_movimenti_mag ( ldt_data_reg_mov, &
							as_cod_tipo_movimento, &
							"S", &
							as_cod_prodotto, &
							ad_quantita, &
							ad_valore_unitario, &
							0, &
							ldt_data_reg_mov, &
							ls_referenza, &
							ll_anno_reg_dest_stock, &
							ll_num_reg_dest_stock, &
							ls_cod_deposito[], &
							ls_cod_ubicazione[], &
							ls_cod_lotto[], &
							ldt_data_stock[], &
							ll_prog_stock[], &
							ls_cod_fornitore[], &
							ls_cod_cliente[], &
							ref al_anno_mov[], &
							ref al_num_mov[] ) = 0 then
	
	destroy luo_mag
	return 0
	
else
	destroy luo_mag
	as_errore = "Errore: uof_movimenti_mag"
	return -1

end if
end function

on w_valorizza_test.create
this.mle_log=create mle_log
this.em_data_movimento=create em_data_movimento
this.cb_salva=create cb_salva
this.st_log=create st_log
this.cb_leggi=create cb_leggi
this.sle_sfoglia=create sle_sfoglia
this.cb_sfoglia=create cb_sfoglia
this.dw_lista=create dw_lista
this.Control[]={this.mle_log,&
this.em_data_movimento,&
this.cb_salva,&
this.st_log,&
this.cb_leggi,&
this.sle_sfoglia,&
this.cb_sfoglia,&
this.dw_lista}
end on

on w_valorizza_test.destroy
destroy(this.mle_log)
destroy(this.em_data_movimento)
destroy(this.cb_salva)
destroy(this.st_log)
destroy(this.cb_leggi)
destroy(this.sle_sfoglia)
destroy(this.cb_sfoglia)
destroy(this.dw_lista)
end on

event close;
if isvalid(iuo_excel) then
	try
		iuo_excel.uof_close( )
		destroy iuo_excel
	catch (throwable err)
		
	end try
end if
end event

type mle_log from multilineedit within w_valorizza_test
integer x = 64
integer y = 1632
integer width = 4553
integer height = 1184
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
boolean hideselection = false
end type

type em_data_movimento from editmask within w_valorizza_test
integer x = 4247
integer y = 36
integer width = 370
integer height = 92
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "31/12/2012"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type cb_salva from commandbutton within w_valorizza_test
integer x = 3831
integer y = 36
integer width = 402
integer height = 92
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva"
end type

event clicked;long				ll_tot, ll_index, ll_anno_mov[], ll_num_mov[], ll_index2, ll_vuoto[]
string				ls_cod_prodotto, ls_des_prodotto, ls_um, ls_valore, ls_errore, ls_cod_deposito[], ls_log
dec{4}			ld_qta[], ld_giacenza, ld_valore_totale
dec{5}			ld_costo_medio
integer			li_ret

//bisogna fare un movimento RV+ con quantità e valore unitario letto dal file
//+ un movimento RV- con quantità letta dal file e valore unitario pari a ZERO

ls_valore = ""
st_log.text = ""
ls_cod_deposito[1] = "100"
ls_cod_deposito[2] = "200"
ls_cod_deposito[3] = "300"
ls_cod_deposito[4] = "400"
ls_cod_deposito[5] = "513"

ll_tot = dw_lista.rowcount()
if ll_tot<=0 then
	g_mb.error("Nessuna riga da elaborare!")
	wf_log("Nessuna riga da elaborare!")
	st_log.text = "Fine!"
	return
end if

if not g_mb.confirm("Procedo con i movimenti?") then return

setpointer(Hourglass!)

for ll_index=1 to ll_tot
	Yield()
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	ls_des_prodotto = dw_lista.getitemstring(ll_index, "des_prodotto")
	ls_um = dw_lista.getitemstring(ll_index, "cod_misura")
	
	ld_qta[1] = dw_lista.getitemdecimal(ll_index, "d100")
	ld_qta[2] = dw_lista.getitemdecimal(ll_index, "d200")
	ld_qta[3] = dw_lista.getitemdecimal(ll_index, "d300")
	ld_qta[4] = dw_lista.getitemdecimal(ll_index, "d400")
	ld_qta[5] = dw_lista.getitemdecimal(ll_index, "d513")
	
	ld_giacenza = dw_lista.getitemdecimal(ll_index, "giacenza")
	ld_costo_medio = dw_lista.getitemdecimal(ll_index, "costo_medio")
	ld_valore_totale = dw_lista.getitemdecimal(ll_index, "valore_totale")
	
	st_log.text = "(" + string(ll_index) + " di " + string(ll_tot) + ") -> Elaborazione prodotto " + ls_cod_prodotto
	wf_log("----------------------------------------------------------------------------------------------------------------")
	
	for ll_index2=1 to 5
		if ld_qta[ll_index2]>0 then
			
			//MOVIMENTO RV+ ----------------------------------------------------------------------------
			ll_anno_mov[] = ll_vuoto[]
			ll_num_mov[] = ll_vuoto[]
			li_ret = wf_movimenti("RV+", ls_cod_deposito[ll_index2], ls_cod_prodotto, ld_qta[ll_index2], ld_costo_medio, ll_anno_mov[], ll_num_mov[], ls_errore)
			
			if li_ret<0 then
				setpointer(Arrow!)
				g_mb.error("(RV+) Prodotto " + ls_cod_prodotto + " : " + ls_errore)
				st_log.text = "Fine con errori!"
				rollback;
				return
			else
				//movimento eseguito
				ls_log = "Mov. "+string(ll_anno_mov[1])+" / "+string(ll_num_mov[1])+" RV+ su Dep. '"+ls_cod_deposito[ll_index2]+"' per codice '" + ls_cod_prodotto + &
							"' - Q.tà "+string(ld_qta[ll_index2], "###,###,##0.0000") + " - Val.Unit. "+string(ld_costo_medio, "###,###,##0.00000")
				wf_log(ls_log)
			end if
			
			//MOVIMENTO RV- --------------------------------------------------------
			ll_anno_mov[] = ll_vuoto[]
			ll_num_mov[] = ll_vuoto[]
			li_ret = wf_movimenti("RV-", ls_cod_deposito[ll_index2], ls_cod_prodotto, ld_qta[ll_index2], 0, ll_anno_mov[], ll_num_mov[], ls_errore)
			
			if li_ret<0 then
				setpointer(Arrow!)
				g_mb.error("(RV-) Prodotto " + ls_cod_prodotto + " : " + ls_errore)
				st_log.text = "Fine con errori!"
				rollback;
				return
			else
				//movimento eseguito
				ls_log = "Mov. "+string(ll_anno_mov[1])+" / "+string(ll_num_mov[1])+" RV- su Dep. '"+ls_cod_deposito[ll_index2]+"' per codice '" + ls_cod_prodotto + &
							"' - Q.tà "+string(ld_qta[ll_index2], "###,###,##0.0000") + " -  Val.Unit. 0.00000"
				wf_log(ls_log)
			end if
			
		end if
	next
	
	//commit ad ogni prodotto
	commit;
	
next

wf_log("FINE ELABORAZIONE")

setpointer(Arrow!)

g_mb.success("Operazione terminata!")
st_log.text = "Fine!"

return
end event

type st_log from statictext within w_valorizza_test
integer x = 64
integer y = 1496
integer width = 4553
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_leggi from commandbutton within w_valorizza_test
integer x = 3269
integer y = 36
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi"
end type

event clicked;string			ls_file, ls_errore
long			ll_pos



ls_file = sle_sfoglia.text

ll_pos = lastpos(sle_sfoglia.text, "\")

mle_log.text = ""

if wf_leggi_fogli(ls_file, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return
end if

g_mb.success("Lettura effettuata!")
end event

type sle_sfoglia from singlelineedit within w_valorizza_test
integer x = 69
integer y = 36
integer width = 3017
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_sfoglia from commandbutton within w_valorizza_test
integer x = 3118
integer y = 28
integer width = 133
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[], ls_errore
integer	li_count, li_ret

ls_path = s_cs_xx.volume + "\"
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	sle_sfoglia.text = docpath
end if
end event

type dw_lista from datawindow within w_valorizza_test
integer x = 64
integer y = 168
integer width = 4553
integer height = 1300
integer taborder = 10
string title = "none"
string dataobject = "d_valorizza_test"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

