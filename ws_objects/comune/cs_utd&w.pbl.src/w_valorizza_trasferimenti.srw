﻿$PBExportHeader$w_valorizza_trasferimenti.srw
forward
global type w_valorizza_trasferimenti from w_std_principale
end type
type tab_1 from tab within w_valorizza_trasferimenti
end type
type tabpage_1 from userobject within tab_1
end type
type cb_valorizza from commandbutton within tabpage_1
end type
type cb_retrieve from commandbutton within tabpage_1
end type
type dw_1 from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
cb_valorizza cb_valorizza
cb_retrieve cb_retrieve
dw_1 dw_1
end type
type tabpage_2 from userobject within tab_1
end type
type cb_excel_2 from commandbutton within tabpage_2
end type
type cb_valorizza_2 from commandbutton within tabpage_2
end type
type cb_retrieve_2 from commandbutton within tabpage_2
end type
type dw_2 from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
cb_excel_2 cb_excel_2
cb_valorizza_2 cb_valorizza_2
cb_retrieve_2 cb_retrieve_2
dw_2 dw_2
end type
type tabpage_3 from userobject within tab_1
end type
type cbx_consolida from checkbox within tabpage_3
end type
type cbx_pre_consolida from checkbox within tabpage_3
end type
type cb_carica_3 from commandbutton within tabpage_3
end type
type cb_valorizza_3 from commandbutton within tabpage_3
end type
type cb_excel_3 from commandbutton within tabpage_3
end type
type dw_3 from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
cbx_consolida cbx_consolida
cbx_pre_consolida cbx_pre_consolida
cb_carica_3 cb_carica_3
cb_valorizza_3 cb_valorizza_3
cb_excel_3 cb_excel_3
dw_3 dw_3
end type
type tab_1 from tab within w_valorizza_trasferimenti
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
end forward

global type w_valorizza_trasferimenti from w_std_principale
integer width = 4192
integer height = 2160
string title = "Valorizza Trasferimenti"
tab_1 tab_1
end type
global w_valorizza_trasferimenti w_valorizza_trasferimenti

type variables
private:
	string is_filepath, is_desktop
	
	boolean ib_annulla = false
	
	datetime idt_date_limit = datetime(date("01/04/2012"), 00:00:00)
end variables

forward prototypes
public subroutine wf_select_and_scroll (ref datawindow adw_dw, long al_row)
public function integer wf_log (string as_message)
public function integer wf_update_val_mov_mag (integer ai_anno_registrazione, long al_num_registrazione, decimal ad_val_movimento, ref string as_error)
public function integer wf_update_det_bol_ven (long al_row, decimal ad_prezzo, ref string as_error)
end prototypes

public subroutine wf_select_and_scroll (ref datawindow adw_dw, long al_row);adw_dw.scrolltorow(al_row)
adw_dw.selectrow(0, false)
adw_dw.selectrow(al_row, true)
end subroutine

public function integer wf_log (string as_message); 
int li_handle
 
li_handle = fileopen(is_filepath, linemode!, Write!, LockWrite!, Append!)

if li_handle = -1 then
	return -1
end if

setmicrohelp(as_message)

as_message = string(now(), "hh:mm:ss") + "~t" + as_message

filewrite(li_handle, as_message)
fileclose(li_handle)


return 0
end function

public function integer wf_update_val_mov_mag (integer ai_anno_registrazione, long al_num_registrazione, decimal ad_val_movimento, ref string as_error);

update mov_magazzino
set val_movimento = :ad_val_movimento
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ai_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	as_error = g_str.format("Errore durante l'aggiornamento del val_movimenti ($1) nel movimentoi $2/$3. $4", ad_val_movimento, ai_anno_registrazione, al_num_registrazione, sqlca.sqlcode)
	return -1
end if

return 0
	

end function

public function integer wf_update_det_bol_ven (long al_row, decimal ad_prezzo, ref string as_error);int li_anno
long ll_num, ll_prog_riga


li_anno = tab_1.tabpage_3.dw_3.getitemnumber(al_row, "det_bol_ven_anno_registrazione")
ll_num = tab_1.tabpage_3.dw_3.getitemnumber(al_row, "det_bol_ven_num_registrazione")
ll_prog_riga = tab_1.tabpage_3.dw_3.getitemnumber(al_row, "det_bol_ven_prog_riga_bol_ven")

if isnull(li_anno) or isnull(ll_num) or isnull(ll_prog_riga) or li_anno < 1900 or ll_num < 1 or ll_prog_riga < 1 then
	return 0
end if

update det_bol_ven
set prezzo_vendita = :ad_prezzo
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :li_anno and
		 num_registrazione = :ll_num and
		 prog_riga_bol_ven = :ll_prog_riga;
		 

if sqlca.sqlcode <> 0 then
	as_error = g_str.format("Errore durante l'aggiornamento del prezzo_vendita della bolla $1/$2/$3", li_anno, ll_num, ll_prog_riga)
else
	as_error = g_str.format("Aggiornato prezzo_vendita nella bolla $1/$2/$3; prezzo: $4", li_anno, ll_num, ll_prog_riga, ad_prezzo)
end if

wf_log(as_error)
return sqlca.sqlcode
end function

on w_valorizza_trasferimenti.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_valorizza_trasferimenti.destroy
call super::destroy
destroy(this.tab_1)
end on

event resize;call super::resize;tab_1.move(20,20)
tab_1.resize(newwidth - 40, newheight - 40)

tab_1.event ue_resize()
end event

event pc_setwindow;call super::pc_setwindow;

tab_1.tabpage_1.dw_1.settransobject(sqlca)
tab_1.tabpage_2.dw_2.settransobject(sqlca)
tab_1.tabpage_3.dw_3.settransobject(sqlca)


is_desktop = guo_functions.uof_get_user_desktop_folder()
end event

type tab_1 from tab within w_valorizza_trasferimenti
event ue_resize ( )
integer x = 23
integer y = 20
integer width = 4114
integer height = 2020
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

event ue_resize();
// TAB 1
tabpage_1.cb_retrieve.move(20, tabpage_1.height - tabpage_1.cb_retrieve.height - 20)
tabpage_1.cb_valorizza.move(tabpage_1.width - tabpage_1.cb_valorizza.width - 20, tabpage_1.cb_retrieve.y)
tabpage_1.dw_1.move(0,0)
tabpage_1.dw_1.resize(tabpage_1.width, tabpage_1.cb_retrieve.y - 20)

// TAB 2
tabpage_2.cb_retrieve_2.move(20, tabpage_2.height - tabpage_2.cb_retrieve_2.height - 20)
tabpage_2.cb_valorizza_2.move(tabpage_2.width - tabpage_2.cb_valorizza_2.width - 20, tabpage_2.cb_retrieve_2.y)
tabpage_2.cb_excel_2.move(tabpage_2.cb_valorizza_2.x -tabpage_2.cb_excel_2.width - 20 , tabpage_2.cb_retrieve_2.y)
tabpage_2.dw_2.move(0,0)
tabpage_2.dw_2.resize(tabpage_2.width, tabpage_2.cb_retrieve_2.y - 20)

// TAB 3
tabpage_3.cb_carica_3.move(20,  tabpage_3.height - tabpage_3.cb_carica_3.height - 20)
tabpage_3.cb_valorizza_3.move(tabpage_3.width - tabpage_3.cb_valorizza_3.width - 20, tabpage_3.cb_carica_3.y)
tabpage_3.cb_excel_3.move(tabpage_3.cb_valorizza_3.x - tabpage_3.cb_excel_3.width - 20, tabpage_3.cb_carica_3.y)
tabpage_3.cbx_pre_consolida.move(tabpage_3.cb_carica_3.x + tabpage_3.cb_carica_3.width + 20, tabpage_3.cb_carica_3.y)
tabpage_3.cbx_consolida.move(tabpage_3.cbx_pre_consolida.x + tabpage_3.cbx_pre_consolida.width + 20, tabpage_3.cb_carica_3.y)
tabpage_3.dw_3.move(0,0)
tabpage_3.dw_3.resize(tabpage_3.width, tabpage_3.cb_carica_3.y - 20)
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4078
integer height = 1892
long backcolor = 12632256
string text = "1. Valorizza Prodotti Finiti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_valorizza cb_valorizza
cb_retrieve cb_retrieve
dw_1 dw_1
end type

on tabpage_1.create
this.cb_valorizza=create cb_valorizza
this.cb_retrieve=create cb_retrieve
this.dw_1=create dw_1
this.Control[]={this.cb_valorizza,&
this.cb_retrieve,&
this.dw_1}
end on

on tabpage_1.destroy
destroy(this.cb_valorizza)
destroy(this.cb_retrieve)
destroy(this.dw_1)
end on

type cb_valorizza from commandbutton within tabpage_1
integer x = 3639
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Valorizza"
end type

event clicked;string ls_log, ls_cod_prodotto, ls_ord_ven_cod_tipo_det_ven
int li_anno_bol_ven, li_ord_ven_anno_registrazione, li_anno_mov_magazzino, li_anno_old
long ll_rows, ll_i, ll_num_bol_ven, ll_prog_bol_ven, ll_ord_ven_num_registrazione, ll_prog_ord_ven, ll_num_mov_magazzino, ll_prog_mov, ll_num_old
decimal ld_ord_ven_prezzo_vendita, ld_sconto_1, ld_bol_ven_prezzo_vendita, ld_sconto, ld_ord_ven_prezzo_vendita_add
uo_calcola_documento_euro luo_calcola_documento_euro
uo_log luo_log

if not g_mb.confirm("Valorizzo le righe trovate?") then
	return 0
end if

// Imposto percorso LOG
is_filepath = guo_functions.uof_get_user_desktop_folder() + "prodotti_finiti_log.txt"

luo_log = create uo_log
luo_log.open(is_filepath)


ll_rows = dw_1.rowcount()
luo_log.log(g_str.format("Inizio valorizzazione di $1 righe ------------------------------------", ll_rows))
//wf_log(g_str.format("Inizio valorizzazione di $1 righe ------------------------------------", ll_rows))

for ll_i = 1 to ll_rows
	
	wf_select_and_scroll(dw_1, ll_i)
	yield()
	
	ls_cod_prodotto =  dw_1.getitemstring(ll_i, "det_bol_ven_cod_prodotto")
	li_anno_bol_ven = dw_1.getitemnumber(ll_i, "det_bol_ven_anno_registrazione")
	ll_num_bol_ven = dw_1.getitemnumber(ll_i, "det_bol_ven_num_registrazione")
	ll_prog_bol_ven = dw_1.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")
	li_anno_mov_magazzino = dw_1.getitemnumber(ll_i, "det_bol_ven_anno_registrazione_mov_mag")
	ll_num_mov_magazzino = dw_1.getitemnumber(ll_i, "det_bol_ven_num_registrazione_mov_mag")
	ll_prog_mov = dw_1.getitemnumber(ll_i, "mov_magazzino_prog_mov")
	
	li_ord_ven_anno_registrazione = dw_1.getitemnumber(ll_i, "det_ord_ven_anno_registrazione")
	ll_ord_ven_num_registrazione = dw_1.getitemnumber(ll_i, "det_ord_ven_num_registrazione")
	ll_prog_ord_ven = dw_1.getitemnumber(ll_i, "det_ord_ven_prog_riga_ord_ven")
	ls_ord_ven_cod_tipo_det_ven = dw_1.getitemstring(ll_i, "det_ord_ven_cod_tipo_det_ven")
	
	ld_ord_ven_prezzo_vendita = dw_1.getitemdecimal(ll_i, "det_ord_ven_prezzo_vendita")
	ld_sconto_1 = dw_1.getitemdecimal(ll_i, "det_ord_ven_sconto_1")
	
	ls_log = g_str.format("DDT: $1/$2/$3", li_anno_bol_ven, ll_num_bol_ven, ll_prog_bol_ven)
	luo_log.log(ls_log)
	//wf_log(ls_log)
	
	if ls_ord_ven_cod_tipo_det_ven = "ADD" then
		luo_log.log(g_str.format("Salto riga del DDT in quanto la riga d'ordine collegata $1/$2/$3 è un ADD e viene sommato dalla riga di appartenenza.",li_ord_ven_anno_registrazione,ll_ord_ven_num_registrazione, ll_prog_ord_ven))
		continue
	end if
	
	if ld_ord_ven_prezzo_vendita = 0 then
		luo_log.log(g_str.format("L'ordine di vendita $1/$2/$3 ha prezzo vendita uguale a ZERO", li_ord_ven_anno_registrazione, ll_ord_ven_num_registrazione, ll_prog_ord_ven))
		continue
	end if
	
	// SCONTO
	// -------------------------------------------
	if ld_sconto_1 = 0 then
		ld_sconto = 0
	else
		ld_sconto = (ld_ord_ven_prezzo_vendita * ld_sconto_1) / 100
	end if
	// -------------------------------------------
	
	// # Rige Ordine ADD
	// -------------------------------------------
	select sum( (prezzo_vendita * quan_ordine) - ((prezzo_vendita * quan_ordine) * (sconto_1 / 100) ))
	into :ld_ord_ven_prezzo_vendita_add
	from det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_ord_ven_anno_registrazione and
			 num_registrazione = :ll_ord_ven_num_registrazione and
			 num_riga_appartenenza = :ll_prog_ord_ven and
			 cod_tipo_det_ven = 'ADD';
		
	if sqlca.sqlcode < 0 then
		ls_log = g_str.format("Errore durante la somma delle righe ADD per l'ordine $1/$2/$3", li_ord_ven_anno_registrazione, ll_ord_ven_num_registrazione, ll_prog_ord_ven)
		luo_log.error(ls_log)
		g_mb.error(ls_log, sqlca)
		rollback;
		continue
	elseif sqlca.sqlcode = 100 or isnull(ld_ord_ven_prezzo_vendita_add) then
		ld_ord_ven_prezzo_vendita_add = 0
	end if
	
	ld_ord_ven_prezzo_vendita_add = round(ld_ord_ven_prezzo_vendita_add, 2)
	// -------------------------------------------
	
	// (Prezzo vendita dell'ordine - lo sconto_1) - il 20%
	ld_bol_ven_prezzo_vendita = ld_ord_ven_prezzo_vendita - ld_sconto // prezzo - sconto_1
	
	 // aggiungo le ADD
	 ld_bol_ven_prezzo_vendita += ld_ord_ven_prezzo_vendita_add
	
	// Ulteriore sconto del 20%
	ld_bol_ven_prezzo_vendita = round( ld_bol_ven_prezzo_vendita - (ld_bol_ven_prezzo_vendita * 0.2) , 2)
	
	luo_log.log("Somma Addizionali: " + string(ld_ord_ven_prezzo_vendita_add))
	luo_log.log(g_str.format("Prezzo $1 per la riga $2/$3/$4", ld_bol_ven_prezzo_vendita, li_anno_bol_ven, ll_num_bol_ven, ll_prog_bol_ven))
	
	update det_bol_ven
	set prezzo_vendita = :ld_bol_ven_prezzo_vendita,
		sconto_1 = 0,
		sconto_2 = 0,
		sconto_3 = 0,
		sconto_4 = 0,
		sconto_5 = 0,
		sconto_6 = 0,
		sconto_7 = 0,
		sconto_8 = 0,
		sconto_9 = 0,
		sconto_10 = 0
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_bol_ven and
			 num_registrazione = :ll_num_bol_ven and
			 prog_riga_bol_ven = :ll_prog_bol_ven;
			 
	if sqlca.sqlcode <> 0 then
		ls_log = g_str.format("Errore durante l'aggiornamento del prezzo di vendita nel DDT $1/$2/$3; prezzo $4", li_anno_bol_ven, ll_num_bol_ven, ll_prog_bol_ven, ld_bol_ven_prezzo_vendita)
		luo_log.error(ls_log)
		g_mb.error(ls_log, sqlca)
		rollback;
		return -1
	end if
	
	
	// aggiorno movimenti, se necessario
	if isnull(li_anno_mov_magazzino) or isnull(ll_num_mov_magazzino) or li_anno_mov_magazzino < 1900 or ll_num_mov_magazzino < 1 then
		luo_log.log(g_str.format("Il DDT $1/$2/$3 non ha nessun movimento di magazzino collegato", li_anno_bol_ven, ll_num_bol_ven, ll_prog_bol_ven))
		continue
	end if
	
	update mov_magazzino
	set val_movimento = :ld_bol_ven_prezzo_vendita
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_bol_ven and
			 prog_mov = :ll_prog_mov and
			 cod_prodotto = :ls_cod_prodotto;
		
	if sqlca.sqlcode <> 0 then
		ls_log = g_str.format("Errore durante l'aggiornamento dei movimenti di magazzino collegati al DDT $1/$2/$3", li_anno_bol_ven, ll_num_bol_ven, ll_prog_bol_ven)
		luo_log.error(ls_log)
		g_mb.error(ls_log, sqlca)
		rollback;
		return -1
	end if
	
	// # Calcolo il documento
	if li_anno_old <> li_anno_bol_ven or ll_num_old <> ll_num_bol_ven then
		
		if isnull(li_anno_old) or li_anno_old = 0 or isnull(ll_num_old) or ll_num_old = 0 then
			li_anno_old = li_anno_bol_ven
			ll_num_old = ll_num_bol_ven
			continue
		end if
		
		luo_calcola_documento_euro = create uo_calcola_documento_euro
	
		luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
		
		if luo_calcola_documento_euro.uof_calcola_documento(li_anno_old,ll_num_old,"bol_ven",ls_log) <> 0 then
			
			if not isnull(ls_log) and ls_log<> "" then
				luo_log.error(ls_log)
				g_mb.error(ls_log)
			end if
			
			destroy luo_calcola_documento_euro
			rollback;
			return -1
		end if	
	end if

	// Faccio subito il commit altrimenti ASE esplode
	commit;
	
next

g_mb.success("Valorizzazione completata con successo")
luo_log.log("Valorizzazione completata con successo")

destroy luo_log
end event

type cb_retrieve from commandbutton within tabpage_1
integer x = 27
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica"
end type

event clicked;long ll_rows

ll_rows = dw_1.retrieve()

setmicrohelp(g_str.format("Trovate $1 righe", ll_rows))
end event

type dw_1 from datawindow within tabpage_1
integer x = 27
integer y = 28
integer width = 4023
integer height = 1680
integer taborder = 20
string title = "none"
string dataobject = "d_valorizza_trasferimenti_prod_finiti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4078
integer height = 1892
long backcolor = 12632256
string text = "2. Valorizza Semilavorati"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_excel_2 cb_excel_2
cb_valorizza_2 cb_valorizza_2
cb_retrieve_2 cb_retrieve_2
dw_2 dw_2
end type

on tabpage_2.create
this.cb_excel_2=create cb_excel_2
this.cb_valorizza_2=create cb_valorizza_2
this.cb_retrieve_2=create cb_retrieve_2
this.dw_2=create dw_2
this.Control[]={this.cb_excel_2,&
this.cb_valorizza_2,&
this.cb_retrieve_2,&
this.dw_2}
end on

on tabpage_2.destroy
destroy(this.cb_excel_2)
destroy(this.cb_valorizza_2)
destroy(this.cb_retrieve_2)
destroy(this.dw_2)
end on

type cb_excel_2 from commandbutton within tabpage_2
integer x = 3205
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Excel"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Esporto la DW in excel
 **/
 
string ls_path, ls_file
int li_rc
 
if g_mb.confirm("Esportare in Excel la lista?") then
	
	if GetFileSaveName ( "Salva", ls_path, ls_file, "Excel", "Excel (*.xls),*.xls" , is_desktop) = 1 then
		
		dw_2.SaveAs(ls_path, Excel8!, true)
		
	end if
end if
end event

type cb_valorizza_2 from commandbutton within tabpage_2
integer x = 3639
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Valorizza"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Consolida i prezzi nel databsae
 **/
 
string ls_cod_prodotto_mov_mag, ls_cod_dep_tras, ls_errore, ls_cod_deposito, ls_cod_sl, ls_log, ls_cod_versione
int li_anno_ddt, li_anno_mov_mag, li_anno_commessa, li_distinta
long ll_rows, ll_i, ll_num_ddt, ll_prog_riga_ddt, ll_num_mov_mag, ll_num_commessa
uo_funzioni_1 luo_funzioni_1

if not g_mb.confirm("Avviare la procedura?") then
	return 1
end if

// Imposto percorso LOG
is_filepath = guo_functions.uof_get_user_desktop_folder() + "semilavorati_log.txt"

luo_funzioni_1 = create uo_funzioni_1
ll_rows = dw_2.rowcount()

for ll_i = 1 to ll_rows
	
	if ib_annulla then return -1
	
	li_anno_ddt = dw_2.getitemnumber(ll_i, "det_bol_ven_anno_registrazione")
	ll_num_ddt = dw_2.getitemnumber(ll_i, "det_bol_ven_num_registrazione")
	ll_prog_riga_ddt = dw_2.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")
	
	li_anno_mov_mag = dw_2.getitemnumber(ll_i, "mov_magazzino_anno_registrazione")
	ll_num_mov_mag = dw_2.getitemnumber(ll_i, "mov_magazzino_num_registrazione")
	ls_cod_prodotto_mov_mag =  dw_2.getitemstring(ll_i, "mov_magazzino_cod_prodotto")
	ls_cod_deposito = dw_2.getitemstring(ll_i, "tes_bol_ven_cod_deposito")
	ls_cod_dep_tras = dw_2.getitemstring(ll_i, "tes_bol_ven_cod_deposito_tras")
	
	ls_cod_versione = dw_2.getitemstring(ll_i, "det_bol_ven_cod_versione")
		
	wf_log(g_str.format("DDT $1/$2/$3", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt))
	wf_select_and_scroll(dw_2, ll_i)
	yield()
	
	if isnull(ls_cod_versione) or ls_cod_versione = "" then
		
		// Assegno la versione predefinita se esiste
		select cod_versione
		into :ls_cod_versione
		from distinta_padri
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_mov_mag and
				 flag_predefinita = 'S';
				 
		if sqlca.sqlcode < 0 then
		elseif sqlca.sqlcode = 100 or isnull(ls_cod_versione) or ls_cod_versione = "" then
			wf_log(g_str.format("La riga DDT $1/$2/$3 viene saltata in quanto il prodotto NON ha una distinta base ", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt))
			continue
		end if
		
		update det_bol_ven
		set cod_versione = :ls_cod_versione
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :li_anno_ddt and
				 num_registrazione = :ll_num_ddt and
				 prog_riga_bol_ven = :ll_prog_riga_ddt;
				 
		wf_log(g_str.format("La riga DDT $1/$2/$3 era senza versione è stata assegna quella predefinita ($4) automaticamente ", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt, ls_cod_versione))

	else
		// controllo che la versione esista in distinta
		select count(*)
		into :li_distinta
		from distinta_padri
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_mov_mag and
				 cod_versione = :ls_cod_versione;
				 
		if sqlca.sqlcode < 0 then
			wf_log(g_str.format("Errore durante il controllo della versione del prodotto $4 del DDT $1/$2/$3", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt, ls_cod_prodotto_mov_mag))
			continue
		elseif sqlca.sqlcode = 100 or isnull(li_distinta) or li_distinta < 1 then
			wf_log(g_str.format("La riga DDT $1/$2/$3 viene saltata in quanto la versione ($5) del prodotto $4 NON è valida", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt, ls_cod_prodotto_mov_mag, ls_cod_versione))
			continue
		end if
	end if
	
	update det_bol_ven
	set
		anno_registrazione_mov_mag = null,
		num_registrazione_mov_mag = null,
		sconto_1 = 0,
		sconto_2 = 0,
		sconto_3 = 0,
		sconto_4 = 0,
		sconto_5 = 0,
		sconto_6 = 0,
		sconto_7 = 0,
		sconto_8 = 0,
		sconto_9 = 0,
		sconto_10 = 0
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_ddt and
			 num_registrazione = :ll_num_ddt and
			 prog_riga_bol_ven = :ll_prog_riga_ddt;
			 
	if sqlca.sqlcode < 0 then
		ls_errore = g_str.format("Errore durante l'azzeramento dei movimento di magazzino della bolla $1/$2/$3", li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt)
		wf_log(ls_errore + " - " + sqlca.sqlerrtext)
		g_mb.error(ls_errore, sqlca)
		rollback;
		return -1
	end if
	
	// 1. CANCELLO MOVIMENTI MAGAZZINO
	// 1.1 cancello movimenti di magazzino collegati
	delete from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_reg_mov_origine = :li_anno_mov_mag and
			 num_reg_mov_origine = :ll_num_mov_mag;
			 
	if sqlca.sqlcode < 0 then
		ls_errore = "Errore durante la cancellazione dei movimenti di magazzino collegati al movimento: " + string(li_anno_mov_mag) + "/" + string(ll_num_mov_mag)
		wf_log(ls_errore + " - " + sqlca.sqlerrtext)
		g_mb.error(ls_errore, sqlca)
		rollback;
		return -1
	end if
	
	// 1.2 cancello movimenti di magazzino referenziati
	delete from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 num_documento = :ll_num_mov_mag and
			 cod_prodotto = :ls_cod_prodotto_mov_mag and
			 cod_deposito = :ls_cod_dep_tras;
			 
	if sqlca.sqlcode < 0 then
		ls_errore = "Errore durante la cancellazione dei movimenti di magazzino referenziati al movimento: " + string(li_anno_mov_mag) + "/" + string(ll_num_mov_mag)
		wf_log(ls_errore + " - " + sqlca.sqlerrtext)
		g_mb.error(ls_errore, sqlca)
		rollback;
		return -1
	end if
	
	// 1.3 cancello il movimento  vero e proprio
	delete from mov_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_mov_mag and
			 num_registrazione = :ll_num_mov_mag;
	
	if sqlca.sqlcode < 0 then
		ls_errore = "Errore durante la cancellazione del movimenti di magazzino: " + string(li_anno_mov_mag) + "/" + string(ll_num_mov_mag) + &
						" della bolla " + string(li_anno_ddt) + "/" + string(ll_num_ddt) + "/" + string(ll_prog_riga_ddt)
		wf_log(ls_errore + " - " + sqlca.sqlerrtext)
		g_mb.error(ls_errore, sqlca)
		rollback;
		return -1
	end if
	
	// CREO COMMESSA
	//esegui l'avanzamento della commessa della riga del ddt *************************************************************************
	if luo_funzioni_1.uof_genera_commesse_ddt(li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt, li_anno_commessa, ll_num_commessa, ls_errore) < 0 then
		wf_log(ls_errore)
		g_mb.error(ls_errore)
		rollback;
		continue
	end if
	
	wf_log(g_str.format("Assegnata nuova Commessa $1/$2 al DDT $3/$4/$5", li_anno_commessa, ll_num_commessa, li_anno_ddt, ll_num_ddt, ll_prog_riga_ddt))
	
	//procedo alla chiusura della commessa su ddt
	luo_funzioni_1.ib_forza_deposito_scarico_mp=true
	luo_funzioni_1.is_cod_deposito_scarico_mp = ls_cod_deposito
	setnull(ls_cod_sl)
	
	//per chiudere la commessa di trasferimento interno da riga ddt vendita
	luo_funzioni_1.ib_trasf_interno = true
	luo_funzioni_1.is_cod_deposito_carico_mp = ls_cod_dep_tras
	//--------------------------------------------------------------------------------
		
	if  luo_funzioni_1.uof_avanza_commessa(li_anno_commessa, ll_num_commessa, ls_cod_sl, ls_errore) < 0 then
		wf_log(ls_errore)
		g_mb.error(ls_errore)
		rollback;
		//return -1
		continue
	end if
	//*********************************************************************************************************************************************

	update det_bol_ven
	set _flag_elaborata = 'S'
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_ddt and
			 num_registrazione = :ll_num_ddt and
			 prog_riga_bol_ven = :ll_prog_riga_ddt;
			 
	commit;
next

destroy luo_funzioni_1

g_mb.success("Procedura completat con successo")

end event

type cb_retrieve_2 from commandbutton within tabpage_2
integer x = 27
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica"
end type

event clicked;long ll_rows

ll_rows = dw_2.retrieve()

setmicrohelp(g_str.format("Trovate $1 righe", ll_rows))
end event

type dw_2 from datawindow within tabpage_2
integer x = 27
integer y = 28
integer width = 4023
integer height = 1680
integer taborder = 30
string title = "none"
string dataobject = "d_valorizza_trasferimenti_sl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tabpage_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4078
integer height = 1892
long backcolor = 12632256
string text = "3. Valorizza Materie Prime"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cbx_consolida cbx_consolida
cbx_pre_consolida cbx_pre_consolida
cb_carica_3 cb_carica_3
cb_valorizza_3 cb_valorizza_3
cb_excel_3 cb_excel_3
dw_3 dw_3
end type

on tabpage_3.create
this.cbx_consolida=create cbx_consolida
this.cbx_pre_consolida=create cbx_pre_consolida
this.cb_carica_3=create cb_carica_3
this.cb_valorizza_3=create cb_valorizza_3
this.cb_excel_3=create cb_excel_3
this.dw_3=create dw_3
this.Control[]={this.cbx_consolida,&
this.cbx_pre_consolida,&
this.cb_carica_3,&
this.cb_valorizza_3,&
this.cb_excel_3,&
this.dw_3}
end on

on tabpage_3.destroy
destroy(this.cbx_consolida)
destroy(this.cbx_pre_consolida)
destroy(this.cb_carica_3)
destroy(this.cb_valorizza_3)
destroy(this.cb_excel_3)
destroy(this.dw_3)
end on

type cbx_consolida from checkbox within tabpage_3
integer x = 987
integer y = 1768
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Consolida"
end type

event clicked;if checked then
	g_mb.warning("Se il flag è attivo verrà eseguito l'update nel database")
end if
end event

type cbx_pre_consolida from checkbox within tabpage_3
integer x = 485
integer y = 1768
integer width = 443
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Pre-Consolida"
end type

type cb_carica_3 from commandbutton within tabpage_3
integer x = 27
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Carica"
end type

event clicked;long ll_rows

ll_rows = dw_3.retrieve()

setmicrohelp(g_str.format("Trovate $1 righe", ll_rows))
end event

type cb_valorizza_3 from commandbutton within tabpage_3
integer x = 3639
integer y = 1728
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Valorizza"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Processo di test del valore di prezzo
 *
 * Casi:
 * 1: data inferiore al limite
 **/
 
string ls_cod_prodotto, ls_error, ls_cod_livello_prod_1, ls_flag_caso
int li_anno_registrazione_mov_mag
long ll_rows, ll_i, ll_num_registrazione_mov_mag
decimal{4} ld_mov_magazzino_val_movimento, ld_det_bol_ven_prezzo_vendita, ld_prezzo_calcolato, ld_prezzo_listino
datetime ldt_tes_bol_ven_data_bolla, ldt_limite, ldt_today
uo_prodotti luo_prodotti
uo_condizioni_cliente luo_condizioni_cliente

ll_rows = dw_3.rowcount()
luo_prodotti = create uo_prodotti
luo_condizioni_cliente = create uo_condizioni_cliente

ldt_today = datetime(today(), now())

long DEBUG_LI_SPOT

DEBUG_LI_SPOT = 824177

if cbx_consolida.checked then
	if not g_mb.confirm("Avviare la procedura?~r~nVerranno consolidati i dati nel database") then
		return 1
	end if
end if

// Imposto percorso LOG
is_filepath = guo_functions.uof_get_user_desktop_folder() + "materie_prime.txt"

for ll_i = 1 to ll_rows
		
	ls_error = ""
	ldt_tes_bol_ven_data_bolla = dw_3.getitemdatetime(ll_i, "tes_bol_ven_data_bolla")
	ls_cod_prodotto = dw_3.getitemstring(ll_i, "mov_magazzino_cod_prodotto")
	ls_cod_livello_prod_1 = dw_3.getitemstring(ll_i, "anag_prodotti_cod_livello_prod_1")
	li_anno_registrazione_mov_mag = dw_3.getitemnumber(ll_i, "mov_magazzino_anno_registrazione")
	ll_num_registrazione_mov_mag = dw_3.getitemnumber(ll_i, "mov_magazzino_num_registrazione")
	
	// CASO 1: Non processo le bolle con data inferiore al limite impostato
//	if ldt_tes_bol_ven_data_bolla < idt_date_limit then
//		dw_3.setitem(ll_i, "flag_caso", "1")
//		dw_3.setitem(ll_i, "prezzo_calcolato", 0)
//		continue
//	end if
	
	//setmicrohelp("Processate " + string(ll_i) + " di " + string(ll_rows))
	//wf_select_and_scroll(dw_3, ll_i)
	//yield()
	
	wf_log(g_str.format("Mov: $1/$2; Riga: $3", li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, ll_i))
	
	//# Prezzo listino
	// --------------------------------------------------
	luo_condizioni_cliente.ib_setitem = false
	luo_condizioni_cliente.ib_setitem_provvigioni = false
	luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = dw_3.getitemstring(ll_i, "tes_bol_ven_cod_tipo_listino_prodotto")
	luo_condizioni_cliente.str_parametri.cod_valuta = dw_3.getitemstring(ll_i, "tes_bol_ven_cod_valuta")
	luo_condizioni_cliente.str_parametri.cambio_ven = dw_3.getitemnumber(ll_i, "tes_bol_ven_cambio_ven")
	luo_condizioni_cliente.str_parametri.data_riferimento = ldt_tes_bol_ven_data_bolla
	setnull(luo_condizioni_cliente.str_parametri.cod_cliente)
	luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
	luo_condizioni_cliente.str_parametri.dim_1 = 0
	luo_condizioni_cliente.str_parametri.dim_2 = 0
	luo_condizioni_cliente.str_parametri.quantita = dw_3.getitemdecimal(ll_i, "det_bol_ven_quan_consegnata")
	luo_condizioni_cliente.str_parametri.valore = 0
	setnull(luo_condizioni_cliente.str_parametri.cod_agente_1)
	luo_condizioni_cliente.str_parametri.fat_conversione_ven = dw_3.getitemdecimal(ll_i, "det_bol_ven_fat_conversione_ven")
	luo_condizioni_cliente.wf_condizioni_cliente()
	
	if upperbound(luo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(luo_condizioni_cliente.str_output.variazioni)) then
		ld_prezzo_listino = 0
	else
		ld_prezzo_listino = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
	end if
	
	dw_3.setitem(ll_i, "prezzo_listino", ld_prezzo_listino)
	// --------------------------------------------------
		
	ld_mov_magazzino_val_movimento = dw_3.getitemdecimal(ll_i, "mov_magazzino_val_movimento")
	ld_det_bol_ven_prezzo_vendita = dw_3.getitemdecimal(ll_i, "det_bol_ven_prezzo_vendita")
	
//	if ll_num_registrazione_mov_mag = DEBUG_LI_SPOT then
//		ll_i = ll_i
//	end if
	
	
	// CASO 5:
	/*if ld_mov_magazzino_val_movimento = ld_prezzo_listino and ls_cod_livello_prod_1 = "10" then
		dw_3.setitem(ll_i, "flag_caso", "5")
		dw_3.setitem(ll_i, "prezzo_calcolato", 0)
		
		if cbx_pre_consolida.checked then
			if wf_update_val_mov_mag(li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, 0, ls_error) < 0 then
				rollback;
				return -1
			end if
			
			wf_log(g_str.format("AZZERO prezzo movimento: $1/$2", li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag))
		end if 
		
	// CASO 6:	
	elseif ls_cod_livello_prod_1 = "01" and round(ld_prezzo_listino / 2, 2) = round(ld_mov_magazzino_val_movimento, 2) then
		dw_3.setitem(ll_i, "flag_caso", "6")
		dw_3.setitem(ll_i, "prezzo_calcolato", 0)
		
		if cbx_pre_consolida.checked then
			if wf_update_val_mov_mag(li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, 0, ls_error) < 0 then
				rollback;
				return -1
			end if
			
			wf_log(g_str.format("AZZERO prezzo movimento: $1/$2", li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag))
		end if
		*/
	// QUESTO VA LANCIATO SOLO DOPO AVER AZZERATO I DDT DEI CASI 6 e 7
	// quando si lancia questo codice, commentare i casi 6 e 7
	// ---------------------------------------------------------------------------------------------------------
	if (ld_mov_magazzino_val_movimento = ld_prezzo_listino and ls_cod_livello_prod_1 = "10" ) or &
	   (ls_cod_livello_prod_1 = "01" and round(ld_prezzo_listino / 2, 2) = round(ld_mov_magazzino_val_movimento, 2)) then
		
		ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_today, ls_error)
		
		if ld_prezzo_calcolato > 0 then
			ls_flag_caso = "5"
			
		else
			
			ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_today, ls_error)
			
			if ld_prezzo_calcolato > 0 then
				ls_flag_caso = "9"
			else
				ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_today, "N", ls_error)
				
				if ld_prezzo_calcolato > 0 then
					ls_flag_caso = "10"
				else
					ls_flag_caso = "4"
				end if
				
			end if
		end if
	// ---------------------------------------------------------------------------------------------------------
		
	// CASO 2: il valore del movimento è > 0 e <> dal valore del DDT
	elseif ld_mov_magazzino_val_movimento > 0 and ld_mov_magazzino_val_movimento <> ld_det_bol_ven_prezzo_vendita then
		ls_flag_caso = "2"
	
	// CASO 3: il valore del movimento e del DDT sono a 0; prevelo il prezzo dall'ultima fattura
	elseif ld_mov_magazzino_val_movimento = 0 then// and ld_det_bol_ven_prezzo_vendita = 0 then

		ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_tes_bol_ven_data_bolla, ls_error)
		
		if ld_prezzo_calcolato > 0 then
			ls_flag_caso = "3"
		else
			
			ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_today, ls_error)
			
			if ld_prezzo_calcolato > 0 then
				ls_flag_caso = "7"
			else
				ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_today, "N", ls_error)
				
				if ld_prezzo_calcolato > 0 then
					ls_flag_caso = "8"
				else
					ls_flag_caso = "4"
				end if
				
			end if
		end if
	else
		// CASO SCONOSCIUTO
		continue
	end if
	
	dw_3.setitem(ll_i, "flag_caso", ls_flag_caso)
	dw_3.setitem(ll_i, "prezzo_calcolato", ld_prezzo_calcolato)
	
	wf_log(g_str.format("Caso $1, prezzo calcolato: $2", ls_flag_caso, ld_prezzo_calcolato))
	
	if cbx_consolida.checked then
		wf_log(g_str.format("Aggiorno prezzo mov $1/$2 con prezzo: $3",li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, ld_prezzo_calcolato))
		
		// Aggiorno DOCUMENTI
		// 1.1 bolla vendita
		if wf_update_det_bol_ven(ll_i, ld_prezzo_calcolato, ls_error) <> 0 then
			rollback;
			continue
		end if
		
		update mov_magazzino
		set val_movimento = :ld_prezzo_calcolato
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :li_anno_registrazione_mov_mag and
				 num_registrazione = :ll_num_registrazione_mov_mag;
				 
		if sqlca.sqlcode <> 0 then
			wf_log(g_str.format("Errore durante l'aggiornamento del valore del movimento $1/$2. $3", li_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, sqlca.sqlerrtext))
			rollback;
		else
			commit;
		end if
	end if

next

destroy luo_condizioni_cliente

if cbx_pre_consolida.checked then
	commit;
end if

g_mb.success("Processo terminato")
return 0
end event

type cb_excel_3 from commandbutton within tabpage_3
integer x = 3182
integer y = 1728
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Excel"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Esporto la DW in excel
 **/
 
string ls_path, ls_file
int li_rc
 
if g_mb.confirm("Esportare in Excel la lista?") then
	
	if GetFileSaveName ( "Salva", ls_path, ls_file, "Excel", "Excel (*.xls),*.xls" , is_desktop) = 1 then
		
		dw_3.SaveAs(ls_path, Excel8!, true)
		
	end if
end if
end event

type dw_3 from datawindow within tabpage_3
integer x = 27
integer y = 8
integer width = 4046
integer height = 1680
integer taborder = 30
string title = "none"
string dataobject = "d_valorizza_trasferimenti_mp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

