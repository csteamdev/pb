﻿$PBExportHeader$w_valorizza_trasferimenti_mp.srw
forward
global type w_valorizza_trasferimenti_mp from w_std_principale
end type
type st_log from statictext within w_valorizza_trasferimenti_mp
end type
type cb_valorizza from commandbutton within w_valorizza_trasferimenti_mp
end type
type cb_excel from commandbutton within w_valorizza_trasferimenti_mp
end type
type cb_test_valorizza from commandbutton within w_valorizza_trasferimenti_mp
end type
type dw_1 from uo_std_dw within w_valorizza_trasferimenti_mp
end type
end forward

global type w_valorizza_trasferimenti_mp from w_std_principale
integer width = 3913
integer height = 1840
string title = "Valorizzazione Acquisti"
st_log st_log
cb_valorizza cb_valorizza
cb_excel cb_excel
cb_test_valorizza cb_test_valorizza
dw_1 dw_1
end type
global w_valorizza_trasferimenti_mp w_valorizza_trasferimenti_mp

type variables
private:
	string is_desktop
	
	datetime idt_date_limit = datetime(date("01/04/2012"), 00:00:00)
end variables

forward prototypes
public function integer wf_valorizza ()
public subroutine wf_set_caso (long al_row, string as_caso)
public subroutine wf_set_caso (long al_row, string as_caso, decimal ad_prezzo)
public function integer wf_convalida ()
end prototypes

public function integer wf_valorizza ();/**
 * stefanop
 * 24/07/2012
 *
 * Processo di test del valore di prezzo
 *
 * Casi:
 * 1: data inferiore al limite
 **/
 
string ls_cod_prodotto, ls_error
long ll_rows, ll_i
decimal{4} ld_mov_magazzino_val_movimento, ld_det_bol_ven_prezzo_vendita, ld_prezzo_calcolato
datetime ldt_tes_bol_ven_data_bolla, ldt_limite
uo_prodotti luo_prodotti

ll_rows = dw_1.rowcount()
luo_prodotti = create uo_prodotti

for ll_i = 1 to ll_rows
	
	setmicrohelp("Processate " + string(ll_i) + " di " + string(ll_rows))
	yield()
	
	ldt_tes_bol_ven_data_bolla = dw_1.getitemdatetime(ll_i, "tes_bol_ven_data_bolla")
	ls_cod_prodotto = dw_1.getitemstring(ll_i, "mov_magazzino_cod_prodotto")
	
	// Non processo le bolle con data inferiore al limite impostato
	if ldt_tes_bol_ven_data_bolla < idt_date_limit then 
		wf_set_caso(ll_i, "1")
		continue
	end if
	
	ld_mov_magazzino_val_movimento = dw_1.getitemdecimal(ll_i, "mov_magazzino_val_movimento")
	ld_det_bol_ven_prezzo_vendita = dw_1.getitemdecimal(ll_i, "det_bol_ven_prezzo_vendita")
	
	// CASO 2: il valore del movimento è > 0 e <> dal valore del DDT
	if ld_mov_magazzino_val_movimento > 0 and ld_mov_magazzino_val_movimento <> ld_det_bol_ven_prezzo_vendita then
		wf_set_caso(ll_i, "2", ld_mov_magazzino_val_movimento)
		continue
	
	// CASO 3: il valore del movimento e del DDT sono a 0; prevelo il prezzo dall'ultima fattura
	elseif ld_mov_magazzino_val_movimento = 0 and ld_det_bol_ven_prezzo_vendita = 0 then

		ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_tes_bol_ven_data_bolla, ls_error)
		
		if ld_prezzo_calcolato > 0 then
			wf_set_caso(ll_i, "3", ld_prezzo_calcolato)
		else
			wf_set_caso(ll_i, "4", ld_prezzo_calcolato)
		end if
	end if

next

return 0
end function

public subroutine wf_set_caso (long al_row, string as_caso);wf_set_caso(al_row, as_caso, 0)
end subroutine

public subroutine wf_set_caso (long al_row, string as_caso, decimal ad_prezzo);dw_1.setitem(al_row, "flag_caso", as_caso)
dw_1.setitem(al_row, "prezzo_calcolato", ad_prezzo)
end subroutine

public function integer wf_convalida ();/**
 * stefanop
 * 24/07/2012
 *
 * Consolida i prezzi nel databsae
 **/
 
long ll_rows, ll_i

if not g_mb.confirm("Salvare i nuovi prezzi? L'operazione non può essere annullata!") then
	return 1
end if


ll_rows = dw_1.rowcount()

for ll_i = 1 to ll_rows
	
next
end function

on w_valorizza_trasferimenti_mp.create
int iCurrent
call super::create
this.st_log=create st_log
this.cb_valorizza=create cb_valorizza
this.cb_excel=create cb_excel
this.cb_test_valorizza=create cb_test_valorizza
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.cb_valorizza
this.Control[iCurrent+3]=this.cb_excel
this.Control[iCurrent+4]=this.cb_test_valorizza
this.Control[iCurrent+5]=this.dw_1
end on

on w_valorizza_trasferimenti_mp.destroy
call super::destroy
destroy(this.st_log)
destroy(this.cb_valorizza)
destroy(this.cb_excel)
destroy(this.cb_test_valorizza)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.settransobject(sqlca)
dw_1.retrieve()

is_desktop = guo_functions.uof_get_user_desktop_folder()
end event

event resize;call super::resize;
// PULSANTI
cb_valorizza.move(newwidth - cb_valorizza.width - 20, newheight - cb_valorizza.height - 20)
cb_excel.move(cb_valorizza.x - cb_excel.width - 20, cb_valorizza.y)
cb_test_valorizza.move(cb_excel.x - cb_test_valorizza.width - 20, cb_valorizza.y)

st_log.move(20, cb_valorizza.y + 10)
st_log.width = cb_test_valorizza.x - 100


dw_1.move(20,20)
dw_1.resize(newwidth - 40, cb_test_valorizza.y - 40)

end event

type st_log from statictext within w_valorizza_trasferimenti_mp
integer x = 23
integer y = 1620
integer width = 1463
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_valorizza from commandbutton within w_valorizza_trasferimenti_mp
integer x = 3451
integer y = 1580
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Consolida"
end type

event clicked;int li_ret

li_ret = wf_convalida()


if li_ret = 0 then
	
	// commit;
	
elseif li_ret = -1 then
	
	rollback;
	
end if
end event

type cb_excel from commandbutton within w_valorizza_trasferimenti_mp
integer x = 2971
integer y = 1580
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Excel"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Esporto la DW in excel
 **/
 
string ls_path, ls_file
int li_rc
 
if g_mb.confirm("Esportare in Excel la lista?") then
	
	if GetFileSaveName ( "Salva", ls_path, ls_file, "Excel", "Excel (*.xls),*.xls" , is_desktop) = 1 then
		
		dw_1.SaveAs(ls_path, Excel8!, true)
		
	end if
end if
end event

type cb_test_valorizza from commandbutton within w_valorizza_trasferimenti_mp
integer x = 2514
integer y = 1580
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Valorizza"
end type

event clicked;
g_mb.confirm(g_str.format("Sicuri di volere confermare la bolla $1/$2?", 2012, 123))

//wf_valorizza()
end event

type dw_1 from uo_std_dw within w_valorizza_trasferimenti_mp
integer x = 46
integer y = 20
integer width = 3817
integer height = 1540
integer taborder = 10
string dataobject = "d_valorizza_trasferimenti_mp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_colora = false
end type

