﻿$PBExportHeader$w_valorizza_trasferimenti_sl.srw
forward
global type w_valorizza_trasferimenti_sl from w_std_principale
end type
type cb_1 from commandbutton within w_valorizza_trasferimenti_sl
end type
type dw_1 from uo_std_dw within w_valorizza_trasferimenti_sl
end type
end forward

global type w_valorizza_trasferimenti_sl from w_std_principale
integer width = 3913
integer height = 1840
string title = "Valorizzazione Acquisti"
cb_1 cb_1
dw_1 dw_1
end type
global w_valorizza_trasferimenti_sl w_valorizza_trasferimenti_sl

on w_valorizza_trasferimenti_sl.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_1
end on

on w_valorizza_trasferimenti_sl.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.settransobject(sqlca)
dw_1.retrieve()
end event

type cb_1 from commandbutton within w_valorizza_trasferimenti_sl
integer x = 3429
integer y = 1580
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Valorizza"
end type

type dw_1 from uo_std_dw within w_valorizza_trasferimenti_sl
integer x = 23
integer y = 20
integer width = 3817
integer height = 1540
integer taborder = 10
string dataobject = "d_valorizza_trasferimenti_sl"
boolean hscrollbar = true
boolean vscrollbar = true
end type

