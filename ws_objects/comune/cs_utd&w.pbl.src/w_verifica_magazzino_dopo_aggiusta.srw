﻿$PBExportHeader$w_verifica_magazzino_dopo_aggiusta.srw
forward
global type w_verifica_magazzino_dopo_aggiusta from w_cs_xx_principale
end type
type cb_da_scaricare from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type cb_da_caricare from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type cb_esporta from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type cb_tutti from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type cb_diversi from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type em_file_cliente from editmask within w_verifica_magazzino_dopo_aggiusta
end type
type cb_cliente from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type dw_1 from datawindow within w_verifica_magazzino_dopo_aggiusta
end type
type cb_verifica from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
type em_file_inventario from editmask within w_verifica_magazzino_dopo_aggiusta
end type
type cb_inventario from commandbutton within w_verifica_magazzino_dopo_aggiusta
end type
end forward

global type w_verifica_magazzino_dopo_aggiusta from w_cs_xx_principale
integer width = 2971
integer height = 1996
string title = "Aggiustamenti di Magazzino -:)"
event ue_salva ( )
cb_da_scaricare cb_da_scaricare
cb_da_caricare cb_da_caricare
cb_esporta cb_esporta
cb_tutti cb_tutti
cb_diversi cb_diversi
em_file_cliente em_file_cliente
cb_cliente cb_cliente
dw_1 dw_1
cb_verifica cb_verifica
em_file_inventario em_file_inventario
cb_inventario cb_inventario
end type
global w_verifica_magazzino_dopo_aggiusta w_verifica_magazzino_dopo_aggiusta

forward prototypes
public function boolean wf_bisestile (long fl_anno)
end prototypes

public function boolean wf_bisestile (long fl_anno);if mod(fl_anno, 4) = 0 then
	//divisibile per 4: altri controlli da fare
	if mod(fl_anno, 100) = 0 then
		//divisibile per 100, inizio secolo: altri controlli da fare
		if mod(fl_anno, 400) = 0 then
			//divisibile per 400: BISESTILE
			return true
		else
			//non divisibile per 400: NON BISESTILE
		end if
	else
		//non divisibile per 100, non inizio secolo: BISESTILE
		return true
	end if	
else
	//non divisibile per 4: NON BISESTILE
	return false
end if
end function

on w_verifica_magazzino_dopo_aggiusta.create
int iCurrent
call super::create
this.cb_da_scaricare=create cb_da_scaricare
this.cb_da_caricare=create cb_da_caricare
this.cb_esporta=create cb_esporta
this.cb_tutti=create cb_tutti
this.cb_diversi=create cb_diversi
this.em_file_cliente=create em_file_cliente
this.cb_cliente=create cb_cliente
this.dw_1=create dw_1
this.cb_verifica=create cb_verifica
this.em_file_inventario=create em_file_inventario
this.cb_inventario=create cb_inventario
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_da_scaricare
this.Control[iCurrent+2]=this.cb_da_caricare
this.Control[iCurrent+3]=this.cb_esporta
this.Control[iCurrent+4]=this.cb_tutti
this.Control[iCurrent+5]=this.cb_diversi
this.Control[iCurrent+6]=this.em_file_cliente
this.Control[iCurrent+7]=this.cb_cliente
this.Control[iCurrent+8]=this.dw_1
this.Control[iCurrent+9]=this.cb_verifica
this.Control[iCurrent+10]=this.em_file_inventario
this.Control[iCurrent+11]=this.cb_inventario
end on

on w_verifica_magazzino_dopo_aggiusta.destroy
call super::destroy
destroy(this.cb_da_scaricare)
destroy(this.cb_da_caricare)
destroy(this.cb_esporta)
destroy(this.cb_tutti)
destroy(this.cb_diversi)
destroy(this.em_file_cliente)
destroy(this.cb_cliente)
destroy(this.dw_1)
destroy(this.cb_verifica)
destroy(this.em_file_inventario)
destroy(this.cb_inventario)
end on

type cb_da_scaricare from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 1714
integer y = 240
integer width = 402
integer height = 84
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "da scaricare"
end type

event clicked;dw_1.setfilter("qta_i > qta_c")
dw_1.filter()
end event

type cb_da_caricare from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 1280
integer y = 240
integer width = 402
integer height = 84
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "da caricare"
end type

event clicked;dw_1.setfilter("qta_i < qta_c")
dw_1.filter()
end event

type cb_esporta from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 2587
integer y = 32
integer width = 325
integer height = 180
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "XLS"
end type

event clicked;integer li_ret

li_ret = dw_1.saveas("", Excel!, true)

if li_ret=1 then
	g_mb.show("Esportazione completata!")
end if
end event

type cb_tutti from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 411
integer y = 240
integer width = 402
integer height = 84
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;dw_1.setfilter("")
dw_1.filter()
end event

type cb_diversi from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 823
integer y = 240
integer width = 402
integer height = 84
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<>"
end type

event clicked;dw_1.setfilter("qta_i <> qta_c")
dw_1.filter()
end event

type em_file_cliente from editmask within w_verifica_magazzino_dopo_aggiusta
integer x = 402
integer y = 128
integer width = 2126
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_cliente from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 18
integer y = 128
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cliente"
end type

event clicked;string docpath, docname[]

integer i, li_cnt, li_rtn, li_filenum

 

li_rtn = GetFileOpenName( "Seleziona FileCliente", docpath, docname[])

em_file_cliente.text = ""

IF li_rtn < 1 THEN return

li_cnt = Upperbound(docname)

if li_cnt = 1 then

   em_file_cliente.text = string(docpath)

end if


end event

type dw_1 from datawindow within w_verifica_magazzino_dopo_aggiusta
integer x = 23
integer y = 340
integer width = 2720
integer height = 1480
integer taborder = 30
string title = "none"
string dataobject = "d_verifica_magazzino_dopo_aggiusta"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_verifica from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 2377
integer y = 240
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "VERIFICA"
end type

event clicked;long ll_rows, ll_index, ll_riga, ll_err, ll_file, ll_pos
string ls_file, ls_prodotto[], ls_stringa
string ls_cod_prodotto
datastore lds_datastore
long ll_tot, ll_i
decimal ld_quantita[], ld_qta_i, ld_qta_c

//Dati forniti dal cliente
setpointer(HourGlass!)

ll_file = fileopen(em_file_cliente.text, linemode!)
if ll_file < 0 then 
	g_mb.messagebox("APICE", "Errore in OPEN del File Cliente")
	return
end if

ll_index = 0
do while true
	ll_err = fileread(ll_file, ls_stringa)
	if ll_err < 0 then exit
	
	ll_pos = pos(ls_stringa, "~t")
	ls_cod_prodotto = mid(ls_stringa, 1,ll_pos -1)
	
	ll_index += 1
	ls_prodotto[ll_index] = ls_cod_prodotto
	ls_stringa = mid(ls_stringa, ll_pos + 1)
	
	//quantita da caricare per l'allineamento delle giacenze
	ld_qta_c = dec(ls_stringa)
	ld_quantita[ll_index] = ld_qta_c	
loop

//---------------

ll_tot = upperbound(ls_prodotto)

lds_datastore = create datastore
lds_datastore.dataobject = "d_ds_prodotti_aggiusta_appoggio"

//carica neldatastore dal file di giacenze apice
ll_rows = lds_datastore.importfile(Text!,em_file_inventario.text)
//----------------

//dal datastore metti nella dw e affianco accoppia il prodotto/giacenza del cliente
for ll_index = 1 to ll_rows
	ll_riga = dw_1.insertrow(0)
	ls_cod_prodotto = lds_datastore.getitemstring(ll_index, "cod_prodotto")
	
	dw_1.setitem(ll_riga, "cod_prodotto_i",ls_cod_prodotto)
	dw_1.setitem(ll_riga, "qta_i", dec(lds_datastore.getitemstring(ll_index, "qta")))
	
	for ll_i = 1 to ll_tot
		if ls_cod_prodotto = ls_prodotto[ll_i] then
			dw_1.setitem(ll_riga, "cod_prodotto_c",ls_prodotto[ll_i])
			dw_1.setitem(ll_riga, "qta_c", ld_quantita[ll_i])
			
			exit
		end if
	next
next

dw_1.sort()

setpointer(Arrow!)
end event

type em_file_inventario from editmask within w_verifica_magazzino_dopo_aggiusta
integer x = 402
integer y = 28
integer width = 2126
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
end type

type cb_inventario from commandbutton within w_verifica_magazzino_dopo_aggiusta
integer x = 18
integer y = 28
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Inventario"
end type

event clicked;string docpath, docname[]

integer i, li_cnt, li_rtn, li_filenum

 

li_rtn = GetFileOpenName( "Seleziona File Inventario", docpath, docname[])

em_file_inventario.text = ""

IF li_rtn < 1 THEN return

li_cnt = Upperbound(docname)

if li_cnt = 1 then

   em_file_inventario.text = string(docpath)

end if


end event

