﻿$PBExportHeader$w_gruppi_varianti.srw
$PBExportComments$Window Gruppi Varianti
forward
global type w_gruppi_varianti from w_cs_xx_principale
end type
type dw_gruppi_varianti from uo_cs_xx_dw within w_gruppi_varianti
end type
type cb_varianti from commandbutton within w_gruppi_varianti
end type
end forward

global type w_gruppi_varianti from w_cs_xx_principale
integer width = 2121
integer height = 1320
string title = "Gruppi Varianti"
dw_gruppi_varianti dw_gruppi_varianti
cb_varianti cb_varianti
end type
global w_gruppi_varianti w_gruppi_varianti

on w_gruppi_varianti.create
int iCurrent
call super::create
this.dw_gruppi_varianti=create dw_gruppi_varianti
this.cb_varianti=create cb_varianti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_gruppi_varianti
this.Control[iCurrent+2]=this.cb_varianti
end on

on w_gruppi_varianti.destroy
call super::destroy
destroy(this.dw_gruppi_varianti)
destroy(this.cb_varianti)
end on

event pc_setwindow;call super::pc_setwindow;dw_gruppi_varianti.set_dw_key("cod_azienda")
dw_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main =dw_gruppi_varianti
end event

type dw_gruppi_varianti from uo_cs_xx_dw within w_gruppi_varianti
integer x = 23
integer y = 20
integer width = 2039
integer height = 1080
string dataobject = "d_gruppi_varianti"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type cb_varianti from commandbutton within w_gruppi_varianti
integer x = 1696
integer y = 1120
integer width = 366
integer height = 80
integer taborder = 2
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Varianti"
end type

event clicked;window_open_parm(w_varianti,-1,dw_gruppi_varianti)
end event

