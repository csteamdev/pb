﻿$PBExportHeader$w_menu_varianti_nota.srw
forward
global type w_menu_varianti_nota from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_menu_varianti_nota
end type
type dw_1 from u_dw_search within w_menu_varianti_nota
end type
end forward

global type w_menu_varianti_nota from w_cs_xx_risposta
integer width = 2491
integer height = 1732
string title = "Imposta quantità"
boolean controlmenu = false
event ue_imposta_valori ( string fs_nota,  string fs_nota_precedente )
cb_ok cb_ok
dw_1 dw_1
end type
global w_menu_varianti_nota w_menu_varianti_nota

type variables
long il_max = 255
long  il_anno_registrazione,il_num_registrazione,il_prog_riga
end variables

event ue_imposta_valori(string fs_nota, string fs_nota_precedente);

dw_1.setitem(dw_1.getrow(), "nota", fs_nota)
dw_1.setitem(dw_1.getrow(), "nota_precedente", fs_nota_precedente)

return
end event

on w_menu_varianti_nota.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.dw_1
end on

on w_menu_varianti_nota.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;s_cs_xx_parametri lstr_parametri
string ls_nota, ls_nota_precedente


lstr_parametri = message.powerobjectparm

il_anno_registrazione = lstr_parametri.parametro_d_1_a[1]
il_num_registrazione = lstr_parametri.parametro_d_1_a[2]
il_prog_riga = lstr_parametri.parametro_d_1_a[3]
ls_nota = lstr_parametri.parametro_s_1_a[1]
ls_nota_precedente = lstr_parametri.parametro_s_1_a[2]


if isnull(ls_nota) then ls_nota = ""
if isnull(ls_nota_precedente) then ls_nota_precedente = ""

this.event ue_imposta_valori(ls_nota, ls_nota_precedente)


end event

type cb_ok from commandbutton within w_menu_varianti_nota
integer x = 987
integer y = 1524
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
boolean default = true
end type

event clicked;s_cs_xx_parametri		lstr_parametri
string						ls_nota


dw_1.accepttext()
ls_nota = dw_1.getitemstring(dw_1.getrow(), "nota")

if isnull(ls_nota) then ls_nota=""

//if len(ls_nota)=0 then
//	if not g_mb.confirm("", "Hai impostato la nota di dettaglio vuota. Confermi?", 2) then return
//	
//elseif len(ls_nota)>il_max then
//	g_mb.error("Lunghezza nota oltre il valore massimo ("+string(il_max)+") !")
//	return
//	
//end if

lstr_parametri.parametro_s_1_a[1] = ls_nota

closewithreturn(parent, lstr_parametri)
end event

type dw_1 from u_dw_search within w_menu_varianti_nota
integer x = 23
integer y = 28
integer width = 2368
integer height = 1452
integer taborder = 10
string dataobject = "d_nota_dettaglio_log"
boolean border = false
end type

