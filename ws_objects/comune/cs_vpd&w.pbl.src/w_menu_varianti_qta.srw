﻿$PBExportHeader$w_menu_varianti_qta.srw
forward
global type w_menu_varianti_qta from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_menu_varianti_qta
end type
type cb_ok from commandbutton within w_menu_varianti_qta
end type
type dw_1 from u_dw_search within w_menu_varianti_qta
end type
end forward

global type w_menu_varianti_qta from w_cs_xx_risposta
integer width = 1632
integer height = 536
string title = "Imposta quantità"
boolean controlmenu = false
event ue_imposta_valori ( decimal fd_qta_tecnica,  decimal fd_qta_utilizzo )
cb_annulla cb_annulla
cb_ok cb_ok
dw_1 dw_1
end type
global w_menu_varianti_qta w_menu_varianti_qta

event ue_imposta_valori(decimal fd_qta_tecnica, decimal fd_qta_utilizzo);

dw_1.setitem(dw_1.getrow(), "qta_tecnica", fd_qta_tecnica)
dw_1.setitem(dw_1.getrow(), "qta_utilizzo", fd_qta_utilizzo)

dw_1.setcolumn("qta_utilizzo")
dw_1.setfocus()

return
end event

on w_menu_varianti_qta.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.dw_1
end on

on w_menu_varianti_qta.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;s_cs_xx_parametri lstr_parametri
decimal ld_qta_utilizzo, ld_qta_tecnica


lstr_parametri = message.powerobjectparm

ld_qta_utilizzo = lstr_parametri.parametro_d_1_a[1]
ld_qta_tecnica = lstr_parametri.parametro_d_1_a[2]

if isnull(ld_qta_tecnica) then ld_qta_tecnica = 0
if isnull(ld_qta_utilizzo) then ld_qta_utilizzo = 0

//dw_1.object.qta_tecnica.visible = false
//dw_1.object.qta_tecnica_t.visible = false

this.event ue_imposta_valori(ld_qta_tecnica, ld_qta_utilizzo)


end event

type cb_annulla from commandbutton within w_menu_varianti_qta
integer x = 832
integer y = 328
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_cs_xx_parametri lstr_parametri

if g_mb.confirm("", "Annullare l'operazione?", 1) then return

lstr_parametri.parametro_b_1 = false

closewithreturn(parent, lstr_parametri)

end event

type cb_ok from commandbutton within w_menu_varianti_qta
integer x = 334
integer y = 328
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;s_cs_xx_parametri		lstr_parametri
decimal					ld_qta_tecnica, ld_qta_utilizzo


dw_1.accepttext()
ld_qta_tecnica = dw_1.getitemdecimal(dw_1.getrow(), "qta_tecnica")
ld_qta_utilizzo = dw_1.getitemdecimal(dw_1.getrow(), "qta_utilizzo")

if isnull(ld_qta_utilizzo) then ld_qta_utilizzo=0
if isnull(ld_qta_tecnica) then ld_qta_tecnica=0

if ld_qta_utilizzo=0 then
	if not g_mb.confirm("", "Q.tà utilizzo pari a ZERO! Continuare?", 2) then return
end if

lstr_parametri.parametro_b_1 = true
lstr_parametri.parametro_d_1_a[1] = ld_qta_utilizzo
lstr_parametri.parametro_d_1_a[2] = ld_qta_tecnica

closewithreturn(parent, lstr_parametri)
end event

type dw_1 from u_dw_search within w_menu_varianti_qta
integer x = 59
integer y = 48
integer width = 1504
integer height = 228
integer taborder = 10
string dataobject = "d_varianti_commesse_quantita"
boolean border = false
end type

