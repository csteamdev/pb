﻿$PBExportHeader$w_menu_varianti_versione.srw
forward
global type w_menu_varianti_versione from w_cs_xx_risposta
end type
type cb_ok from commandbutton within w_menu_varianti_versione
end type
type dw_1 from u_dw_search within w_menu_varianti_versione
end type
end forward

global type w_menu_varianti_versione from w_cs_xx_risposta
integer width = 2610
integer height = 572
string title = "Imposta quantità"
boolean controlmenu = false
event ue_imposta_valori ( string fs_cod_prodotto )
cb_ok cb_ok
dw_1 dw_1
end type
global w_menu_varianti_versione w_menu_varianti_versione

type variables
string is_cod_versione
end variables

event ue_imposta_valori(string fs_cod_prodotto);

dw_1.setitem(dw_1.getrow(), "rs_cod_prodotto", fs_cod_prodotto)
dw_1.object.rs_cod_prodotto.protect = true
dw_1.object.b_ricerca_prodotto.visible = false

//popola la versione prodotto
f_PO_LoadDDDW_DW( dw_1, 	"cod_versione", &
									sqlca, &
						        		"distinta", &
									" cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  			" cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + fs_cod_prodotto + "' ")

dw_1.setcolumn("cod_versione")
dw_1.setfocus()

return
end event

on w_menu_varianti_versione.create
int iCurrent
call super::create
this.cb_ok=create cb_ok
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ok
this.Control[iCurrent+2]=this.dw_1
end on

on w_menu_varianti_versione.destroy
call super::destroy
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;s_cs_xx_parametri lstr_parametri
string ls_cod_versione, ls_cod_prodotto


lstr_parametri = message.powerobjectparm

ls_cod_prodotto = lstr_parametri.parametro_s_1_a[1]
is_cod_versione = lstr_parametri.parametro_s_1_a[2]

this.event ue_imposta_valori(ls_cod_prodotto)


end event

type cb_ok from commandbutton within w_menu_varianti_versione
integer x = 1001
integer y = 336
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
boolean default = true
end type

event clicked;s_cs_xx_parametri		lstr_parametri
string						ls_cod_versione


dw_1.accepttext()
ls_cod_versione = dw_1.getitemstring(dw_1.getrow(), "cod_versione")

if isnull(ls_cod_versione) or ls_cod_versione="" then
	if not g_mb.confirm("", "Versione prodotto non selezionata! Continuare? (Verrà utilizzata la versione '"+is_cod_versione+"')", 2) then return
	
	ls_cod_versione = is_cod_versione
	
end if

lstr_parametri.parametro_s_1_a[1] = ls_cod_versione

closewithreturn(parent, lstr_parametri)
end event

type dw_1 from u_dw_search within w_menu_varianti_versione
integer x = 59
integer y = 32
integer width = 2432
integer height = 244
integer taborder = 10
string dataobject = "d_varianti_commessa_ricerca"
boolean border = false
end type

