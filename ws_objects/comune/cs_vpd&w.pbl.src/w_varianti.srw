﻿$PBExportHeader$w_varianti.srw
$PBExportComments$Window Varianti
forward
global type w_varianti from w_cs_xx_principale
end type
type dw_varianti_lista from uo_cs_xx_dw within w_varianti
end type
type cb_ric_prod from cb_prod_ricerca within w_varianti
end type
type dw_varianti_dettaglio from uo_cs_xx_dw within w_varianti
end type
type lb_elenco_campi from listbox within w_varianti
end type
type cb_inserisci from commandbutton within w_varianti
end type
type cb_verifica from commandbutton within w_varianti
end type
type st_1 from statictext within w_varianti
end type
type st_tempo from statictext within w_varianti
end type
end forward

global type w_varianti from w_cs_xx_principale
integer width = 3342
integer height = 1600
string title = "Varianti"
dw_varianti_lista dw_varianti_lista
cb_ric_prod cb_ric_prod
dw_varianti_dettaglio dw_varianti_dettaglio
lb_elenco_campi lb_elenco_campi
cb_inserisci cb_inserisci
cb_verifica cb_verifica
st_1 st_1
st_tempo st_tempo
end type
global w_varianti w_varianti

on w_varianti.create
int iCurrent
call super::create
this.dw_varianti_lista=create dw_varianti_lista
this.cb_ric_prod=create cb_ric_prod
this.dw_varianti_dettaglio=create dw_varianti_dettaglio
this.lb_elenco_campi=create lb_elenco_campi
this.cb_inserisci=create cb_inserisci
this.cb_verifica=create cb_verifica
this.st_1=create st_1
this.st_tempo=create st_tempo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_varianti_lista
this.Control[iCurrent+2]=this.cb_ric_prod
this.Control[iCurrent+3]=this.dw_varianti_dettaglio
this.Control[iCurrent+4]=this.lb_elenco_campi
this.Control[iCurrent+5]=this.cb_inserisci
this.Control[iCurrent+6]=this.cb_verifica
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.st_tempo
end on

on w_varianti.destroy
call super::destroy
destroy(this.dw_varianti_lista)
destroy(this.cb_ric_prod)
destroy(this.dw_varianti_dettaglio)
destroy(this.lb_elenco_campi)
destroy(this.cb_inserisci)
destroy(this.cb_verifica)
destroy(this.st_1)
destroy(this.st_tempo)
end on

event pc_setwindow;call super::pc_setwindow;dw_varianti_lista.set_dw_key("cod_azienda")
dw_varianti_lista.set_dw_key("cod_gruppo_variante")
dw_varianti_lista.set_dw_key("progr")

dw_varianti_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_varianti_dettaglio.set_dw_options(sqlca,dw_varianti_lista,c_sharedata + c_scrollparent,c_default)

iuo_dw_main =dw_varianti_lista
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_varianti_dettaglio,"cod_prodotto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco='N'")
//
end event

type dw_varianti_lista from uo_cs_xx_dw within w_varianti
integer x = 23
integer y = 20
integer width = 2537
integer height = 500
integer taborder = 40
string dataobject = "d_varianti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_gruppo_variante

ls_cod_gruppo_variante = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_gruppo_variante")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_variante)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG   l_Idx,ll_progr
string ls_cod_gruppo_variante,ls_cod_prodotto,ls_cod_misura

ls_cod_gruppo_variante = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_gruppo_variante")
ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")

select cod_misura_mag
into   :ls_cod_misura
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

select max(progr)
into   :ll_progr
from   varianti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_gruppo_variante=:ls_cod_gruppo_variante;

if isnull(ll_progr) then 
	ll_progr = 1
else
	ll_progr++
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
		SetItem(l_Idx, "cod_gruppo_variante", ls_cod_gruppo_variante)
		SetItem(l_Idx, "progr", ll_progr)
		SetItem(l_Idx, "cod_misura", ls_cod_misura)
   END IF
NEXT


end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_ric_prod.enabled=true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	cb_ric_prod.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_ric_prod.enabled=false
end if
end event

type cb_ric_prod from cb_prod_ricerca within w_varianti
event getfocus pbm_bnsetfocus
integer x = 2400
integer y = 644
integer width = 73
integer height = 80
integer taborder = 10
boolean bringtotop = true
boolean enabled = false
end type

event getfocus;call super::getfocus;dw_varianti_dettaglio.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type dw_varianti_dettaglio from uo_cs_xx_dw within w_varianti
integer x = 23
integer y = 540
integer width = 2537
integer height = 940
integer taborder = 50
string dataobject = "d_varianti_dettaglio"
borderstyle borderstyle = styleraised!
end type

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_inserisci.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_inserisci.enabled=true
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_inserisci.enabled=false
end if
end event

type lb_elenco_campi from listbox within w_varianti
integer x = 2583
integer y = 20
integer width = 709
integer height = 500
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string item[] = {"QUAN_TECNICA","QUAN_UTILIZZO","DIM_X","DIM_Y","DIM_Z","DIM_T","COEF_CALCOLO"}
borderstyle borderstyle = stylelowered!
end type

type cb_inserisci from commandbutton within w_varianti
event clicked pbm_bnclicked
integer x = 2583
integer y = 540
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Inserisci"
end type

event clicked;string ls_formula

dw_varianti_dettaglio.setcolumn("formula_tempo")

ls_formula = dw_varianti_dettaglio.gettext()

if isnull(ls_formula) then ls_formula=""

ls_formula = ls_formula + " " + lb_elenco_campi.selecteditem() + " "

dw_varianti_dettaglio.settext(ls_formula)


end event

type cb_verifica from commandbutton within w_varianti
event clicked pbm_bnclicked
integer x = 2583
integer y = 640
integer width = 366
integer height = 80
integer taborder = 12
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Verifica"
end type

event clicked;string ls_campo,ls_formula,ls_risultato
double ldd_valore_campo
integer li_i,li_risposta

ls_formula = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"formula_tempo")

for li_i = 1 to 7
	lb_elenco_campi.selectitem(li_i)
	ls_campo = lb_elenco_campi.selecteditem()
	if pos(ls_formula,ls_campo) <> 0 then
		ldd_valore_campo  = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),ls_campo)
		if isnull(ldd_valore_campo) then ldd_valore_campo=0
		li_risposta = f_sostituzione(ls_formula, ls_campo, string(ldd_valore_campo))
	end if
next

li_risposta = f_if(ls_formula)

if li_risposta < 0 then
	g_mb.messagebox("Sep",ls_formula,stopsign!)
	return
end if

ls_risultato = f_parser_parentesi(ls_formula)
if ls_risultato = "Errore" then
	g_mb.messagebox("Sep","Attenzione le librerie per il calcolo delle formule trigonometriche non sono installate correttamente. Verificare.",stopsign!)
	return -1
end if
st_tempo.text = ls_risultato
end event

type st_1 from statictext within w_varianti
integer x = 2583
integer y = 760
integer width = 695
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Tempo Calcolato in minuti:"
boolean focusrectangle = false
end type

type st_tempo from statictext within w_varianti
integer x = 2583
integer y = 860
integer width = 526
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

