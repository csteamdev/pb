﻿$PBExportHeader$w_varianti_commesse.srw
$PBExportComments$Gestione Varianti per Commesse
forward
global type w_varianti_commesse from w_cs_xx_principale
end type
type cb_genera_var_depositi from commandbutton within w_varianti_commesse
end type
type tab_1 from tab within w_varianti_commesse
end type
type tabpage_1 from userobject within tab_1
end type
type dw_varianti_commesse_quantita from datawindow within tabpage_1
end type
type dw_azioni from u_dw_search within tabpage_1
end type
type dw_ricerca from u_dw_search within tabpage_1
end type
type tv_db from treeview within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
tv_db tv_db
end type
type tabpage_2 from userobject within tab_1
end type
type dw_distinta_det from uo_cs_xx_dw within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_distinta_det dw_distinta_det
end type
type tabpage_3 from userobject within tab_1
end type
type cb_crea from commandbutton within tabpage_3
end type
type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
end type
type st_prodotti_varianti from statictext within tabpage_3
end type
type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
end type
type st_dettaglio_variante from statictext within tabpage_3
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
end type
type st_gruppi_varianti from statictext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
cb_crea cb_crea
dw_varianti_lista dw_varianti_lista
st_prodotti_varianti st_prodotti_varianti
dw_varianti_dettaglio dw_varianti_dettaglio
st_dettaglio_variante st_dettaglio_variante
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
st_gruppi_varianti st_gruppi_varianti
end type
type tabpage_4 from userobject within tab_1
end type
type cb_rimuovi_reparto from commandbutton within tabpage_4
end type
type cb_aggiungi_reparto from commandbutton within tabpage_4
end type
type dw_reparti_variante from datawindow within tabpage_4
end type
type dw_lista_reparti from datawindow within tabpage_4
end type
type dw_varianti_commessa from uo_cs_xx_dw within tabpage_4
end type
type tabpage_4 from userobject within tab_1
cb_rimuovi_reparto cb_rimuovi_reparto
cb_aggiungi_reparto cb_aggiungi_reparto
dw_reparti_variante dw_reparti_variante
dw_lista_reparti dw_lista_reparti
dw_varianti_commessa dw_varianti_commessa
end type
type tabpage_5 from userobject within tab_1
end type
type dw_integrazioni_commessa from uo_cs_xx_dw within tabpage_5
end type
type tabpage_5 from userobject within tab_1
dw_integrazioni_commessa dw_integrazioni_commessa
end type
type tab_1 from tab within w_varianti_commesse
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_varianti_commesse from w_cs_xx_principale
integer width = 3616
integer height = 2304
string title = "Varianti Commessa"
event pc_scarico_parziale ( )
event pc_chiudi_commessa ( )
cb_genera_var_depositi cb_genera_var_depositi
tab_1 tab_1
end type
global w_varianti_commesse w_varianti_commesse

type variables
long il_handle
string is_cod_prodotto_finito, is_cod_versione, is_cod_versione_figlio
long  il_anno_commessa, il_num_commessa, il_cicli

end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public function integer wf_filtra_reparti (string fs_cod_deposito)
public subroutine wf_elimina_varianti ()
public subroutine wf_elimina_integrazioni ()
public subroutine wf_report_distinta ()
public function long wf_comprimi (long al_handle)
public function integer wf_espandi (long al_handle, long al_livello, long al_cicli)
public function integer wf_scarico_parziale (string as_cod_prodotto_padre, long al_num_sequenza, string as_cod_prodotto_figlio, string as_cod_versione, string as_cod_versione_figlio, ref string as_errore)
public function integer wf_parametri_commessa (ref string as_deposito_prelievo_mp, ref string as_cod_tipo_mov_mp, ref string as_deposito_versamento_sl, ref string as_cod_tipo_mov_sl, ref string as_errore)
public subroutine wf_scarico_parziale (treeviewitem ftvi_campo, ref string fs_errore, ref integer fi_return)
public function integer wf_azione (string fs_modalita)
public function integer wf_chiudi_commessa (ref string as_errore)
public function integer wf_trova (string fs_cod_prodotto, long al_handle)
end prototypes

event pc_scarico_parziale();
wf_azione("SP")

return
end event

event pc_chiudi_commessa();integer			li_ret
string				ls_errore


li_ret = wf_chiudi_commessa(ls_errore)

choose case li_ret
	case is < 0
		rollback;
		g_mb.error(ls_errore)
		
	case 1
		rollback;
		g_mb.warning(ls_errore)
		
	case else
		//OK
		commit;
		g_mb.success("Operazione effettuata con successo!")
		
end choose



return
end event

public function integer wf_inizio ();string ls_errore,ls_des_prodotto
long ll_risposta
treeviewitem tvi_campo
uo_imposta_tv_varianti luo_varianti

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
l_chiave_distinta.cod_versione_padre = is_cod_versione

tab_1.tabpage_1.tv_db.setredraw(false)
tab_1.tabpage_1.tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto +  ", ver. " + is_cod_versione
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta = tab_1.tabpage_1.tv_db.insertitemlast(0, tvi_campo)

luo_varianti = CREATE uo_imposta_tv_varianti
luo_varianti.is_tipo_gestione = "varianti_commesse"
luo_varianti.is_cod_versione = is_cod_versione
luo_varianti.il_anno_registrazione = il_anno_commessa
luo_varianti.il_num_registrazione = il_num_commessa
luo_varianti.uof_imposta_tv(ref tab_1.tabpage_1.tv_db, l_chiave_distinta.cod_prodotto_padre, is_cod_versione, 1, 1, "S", ref ls_errore)
destroy luo_varianti
tab_1.tabpage_1.tv_db.expandall(1)
tab_1.tabpage_1.tv_db.setredraw(true)
tab_1.tabpage_1.tv_db.triggerevent("pcd_clicked")

return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

tab_1.tabpage_2.dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public function integer wf_filtra_reparti (string fs_cod_deposito);datawindowchild  		ldwc_child
integer					li_child
string						ls_filter

li_child = tab_1.tabpage_4.dw_varianti_commessa.GetChild("cod_reparto_produzione", ldwc_child)


if fs_cod_deposito="" or isnull(fs_cod_deposito) then
	//fai vedere tutti i reparti
	ls_filter = ""
else
	//filtra i reparti per deposito
	ls_filter = "cod_deposito='"+fs_cod_deposito+"'"
end if

ldwc_child.setfilter(ls_filter)
ldwc_child.filter()

return 1
end function

public subroutine wf_elimina_varianti ();if g_mb.messagebox("Sep", "Confermi l'eliminazione di TUTTE le varianti?",Question!, YesNo!, 2) = 2 then return

string ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione_variante, &
       ls_cod_versione_figlio, ls_errore

dec{4} ldd_quan_utilizzo, ldd_quan_utilizzo_distinta
long ll_null, ll_num_sequenza

//uo_magazzino luo_magazzino

declare cur_varianti cursor for
select cod_prodotto,
		 cod_versione_variante,
		 quan_utilizzo,
		 cod_prodotto_padre,
 		 cod_versione,
		 num_sequenza,
		 cod_prodotto_figlio,
		 cod_versione_figlio
from   varianti_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :il_anno_commessa and
		 num_commessa = :il_num_commessa;

open cur_varianti;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Errore nella OPEN del cursore cur_varianti~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

setnull(ll_null)

//luo_magazzino = create uo_magazzino


do while true

	fetch cur_varianti 
	into :ls_cod_prodotto,
		  :ls_cod_versione_variante,
		  :ldd_quan_utilizzo,
		  :ls_cod_prodotto_padre,
  		  :ls_cod_versione,
		  :ll_num_sequenza,
		  :ls_cod_prodotto_figlio,
		  :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore nella FETCH del cursore cur_varianti~r~n"+sqlca.sqlerrtext)
		close cur_varianti;
		rollback;
//		destroy luo_magazzino
		return
	end if

	if sqlca.sqlcode = 100 then
		close cur_varianti;
		exit
	end if


//	if luo_magazzino.uof_impegna_mp_commessa(	false, &
//															true, &
//															ls_cod_prodotto, &
//															ls_cod_versione, &
//															ldd_quan_utilizzo, &
//															il_anno_commessa, &
//															il_num_commessa, &
//															ls_errore) = -1 then
//		messagebox("Apice","Errore durante il disimpegno",stopsign!)
//		destroy luo_magazzino
//		rollback;
//		return
//	end if
	
//	if f_impegna_mp( ls_cod_prodotto, ls_cod_versione_variante, ldd_quan_utilizzo, il_anno_commessa, il_num_commessa, &
//						ll_null, "varianti_commesse", False ) = -1 then //Disimpegno la quantita
//		messagebox("Apice","Errore durante l'aggiornamento prodotti",stopsign!)
//		return -1
//	end if

	select quan_utilizzo
	into   :ldd_quan_utilizzo_distinta
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and    
	       cod_prodotto_padre = :ls_cod_prodotto_padre and    
			 num_sequenza = :ll_num_sequenza and    
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and    
			 cod_versione = :ls_cod_versione and
			 cod_versione_figlio = :ls_cod_versione_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Attenzione! Errore in Lettura Distinta:" + sqlca.sqlerrtext ,stopsign!)
//		destroy luo_magazzino
		rollback;
		return
	end if
	
//	if luo_magazzino.uof_impegna_mp_commessa(	true, &
//															true, &
//															ls_cod_prodotto_figlio, &
//															ls_cod_versione_figlio, &
//															ldd_quan_utilizzo_distinta, &
//															il_anno_commessa, &
//															il_num_commessa, &
//															ls_errore) = -1 then
//		messagebox("Apice","Errore durante il disimpegno",stopsign!)
//		destroy luo_magazzino
//		rollback;
//		return
//	end if
	
//	if f_impegna_mp( ls_cod_prodotto, ls_cod_versione_variante, ldd_quan_utilizzo_distinta, il_anno_commessa, il_num_commessa, &
//						ll_null, "varianti_commesse", True ) = -1 then //Impegno la quantita
//		messagebox("Apice","Errore durante l'aggiornamento prodotti",stopsign!)
//		return -1
//	end if

loop


delete from varianti_commesse
where cod_azienda = :s_cs_xx.cod_azienda
and   anno_commessa = :il_anno_commessa
and   num_commessa = :il_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//	destroy luo_magazzino
	rollback;
	return
end if

wf_inizio()

commit;	

//destroy luo_magazzino

end subroutine

public subroutine wf_elimina_integrazioni ();if g_mb.messagebox("Sep", "Confermi l'eliminazione di TUTTE le integrazioni ?",Question!, YesNo!, 2) = 2 then return

string ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione_padre, ls_cod_versione_figlio, &
       ls_errore
dec{4} ldd_quan_utilizzo, ldd_quan_utilizzo_distinta
long ll_null, ll_progressivo
//uo_magazzino luo_magazzino

declare cur_integrazioni cursor for
select cod_prodotto_figlio,
		 cod_versione_figlio,
		 quan_utilizzo,
		 cod_prodotto_padre,
		 cod_versione_padre,
		 progressivo,
		 cod_prodotto_figlio,
		 cod_versione_figlio
from   integrazioni_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :il_anno_commessa and
		 num_commessa = :il_num_commessa;

open cur_integrazioni;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Errore nella OPEN del cursore cur_integrazioni~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

setnull(ll_null)

select cod_versione
into   :ls_cod_versione
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :il_anno_commessa and
		 num_commessa = :il_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Errore in ricerca versione del prodotto finito~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

//luo_magazzino = create uo_magazzino
	
do while true

	fetch cur_integrazioni 
	into :ls_cod_prodotto,
		  :ls_cod_versione,
		  :ldd_quan_utilizzo,
		  :ls_cod_prodotto_padre,
		  :ls_cod_versione_padre,
		  :ll_progressivo,
		  :ls_cod_prodotto_figlio,
		  :ls_cod_versione_figlio;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore nella FETCH del cursore cur_integrazioni~r~n"+sqlca.sqlerrtext)
		close cur_integrazioni;
		rollback;
//		destroy luo_magazzino
		return
	end if

	if sqlca.sqlcode = 100 then
		close cur_integrazioni;
		exit
	end if
	
//	if luo_magazzino.uof_impegna_mp_commessa(	false, &
//															false, &
//															ls_cod_prodotto, &
//															ls_cod_versione, &
//															ldd_quan_utilizzo, &
//															il_anno_commessa, &
//															il_num_commessa, &
//															ls_errore) = -1 then
//		messagebox("Apice","Errore durante il disimpegno",stopsign!)
//		destroy luo_magazzino
//		rollback;
//		return -1
//	end if
	
//	if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_utilizzo, il_anno_commessa, il_num_commessa, &
//						ll_null, "varianti_commesse", False ) = -1 then //Disimpegno la quantita
//		messagebox("Apice","Errore durante l'aggiornamento prodotti",stopsign!)
//		return -1
//	end if

loop

delete from integrazioni_commessa
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :il_anno_commessa and
		num_commessa = :il_num_commessa;
		
if sqlca.sqlcode < 0 then
//	destroy luo_magazzino
	g_mb.messagebox("Sep","Errore in fase di cancellazione dell'integrazione:~r~n" + sqlca.sqlerrtext,stopsign!)
	rollback;
end if

wf_inizio()

commit;	

//destroy luo_magazzino;



end subroutine

public subroutine wf_report_distinta ();s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
s_cs_xx.parametri.parametro_s_2 = is_cod_versione
s_cs_xx.parametri.parametro_s_3 = "varianti_commesse"

s_cs_xx.parametri.parametro_i_1 = il_anno_commessa
s_cs_xx.parametri.parametro_d_1 = il_num_commessa
setnull(s_cs_xx.parametri.parametro_d_2)

window_open(w_report_distinta_varianti, -1) 
end subroutine

public function long wf_comprimi (long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)

end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)
	
end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

tab_1.tabpage_1.tv_db.collapseitem( al_handle)

return 100
end function

public function integer wf_espandi (long al_handle, long al_livello, long al_cicli);long ll_handle, ll_padre, ll_i

treeviewitem ltv_item

il_cicli ++

if il_cicli > al_livello then
	return 100
end if

tab_1.tabpage_1.tv_db.getitem( al_handle, ltv_item)
tab_1.tabpage_1.tv_db.ExpandItem ( al_handle )	

ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	wf_espandi(ll_handle, al_livello, il_cicli)
	il_cicli --
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	il_cicli --
	wf_espandi(ll_handle, al_livello, il_cicli)
//	il_cicli --
	
end if

return 100
end function

public function integer wf_scarico_parziale (string as_cod_prodotto_padre, long al_num_sequenza, string as_cod_prodotto_figlio, string as_cod_versione, string as_cod_versione_figlio, ref string as_errore);//effettua i movimenti di scarico MP e carico SL/PF del ramo commessa
//imposta il flag di scarico parziale della variante commessa a SI
//NOTE
//		Le MP da scaricare sono i sotto-rami legati al ramo della variante commessa
//		Il deposito di scarico materie prime coincide con il deposito produzione della variante (o tipo commessa)
//
//		il SL da caricare è il cod_prodotto della variante commessa
//		Il deposito di carico del PF/SL coincide con il deposito versamento PF (SL) della commessa (o tipo commessa)


uo_funzioni_1		luo_funz
integer				li_ret

string					ls_cod_semilavorato, ls_cod_versione_semilavorato, ls_cod_deposito_produzione


//leggo codice del semilavorato da caricare e relativa versione
select		cod_prodotto,
			cod_versione_variante,
			cod_deposito_produzione
into	:ls_cod_semilavorato,
		:ls_cod_versione_semilavorato,
		:ls_cod_deposito_produzione
from	varianti_commesse
where	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:il_anno_commessa and
			 num_commessa=:il_num_commessa and
			 cod_prodotto_padre=:as_cod_prodotto_padre and
			 num_sequenza=:al_num_sequenza and
			 cod_prodotto_figlio=:as_cod_prodotto_figlio and
			 cod_versione=:as_cod_versione and
			 cod_versione_figlio=:as_cod_versione_figlio;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura prodotto e versione della variante da caricare: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Variante non trovata!"
	return -1
	
elseif ls_cod_semilavorato="" or isnull(ls_cod_semilavorato) then
	as_errore = "Prodotto variante non impostato in varianti_commesse!"
	return -1
	
elseif ls_cod_versione_semilavorato="" or isnull(ls_cod_versione_semilavorato) then
	as_errore = "Versione prodotto variante non impostato in varianti_commesse!"
	return -1
	
end if


//---------------------------------------------------------------------------------------------------
//avanzamento parziale sul ramo commessa
//NOTA IMPORTANTE:	il flag a SI sulla variante commessa scaricata viene messo all'interno della chiamata
//								uof_avanza_commessa (in realtà dentro la funzione trova_mat_prime_varianti ... per la precisione)
luo_funz = create uo_funzioni_1

luo_funz.ib_forza_deposito_scarico_mp = false
setnull(luo_funz.is_cod_deposito_scarico_mp)
li_ret = luo_funz.uof_avanza_commessa(il_anno_commessa, il_num_commessa, ls_cod_semilavorato, as_errore)

destroy uo_funzioni_1

if li_ret < 0 then
	return -1
end if



return 0
end function

public function integer wf_parametri_commessa (ref string as_deposito_prelievo_mp, ref string as_cod_tipo_mov_mp, ref string as_deposito_versamento_sl, ref string as_cod_tipo_mov_sl, ref string as_errore);string				ls_cod_tipo_commessa, ls_des_tipo_commessa

//per l'avanzamento parziale di un ramo di commessa 
//		- il deposito per il SL prodotto viene letto dalla variante (che se non esiste viene creata): caso chiusura parziale commessa
//			siccome la creo io la faccio coincidere con il deposito versamento semilavorati del tipo commessa
//		- il deposito per le MP prelevate è quello impostato in anag_commessa (cod_deposito_prelievo), ma nel caso di produzione semilavorato coinciderà sempre
//			con quello della variante (quindi inutile controllare)
//		- il tipo movimento per il carico SL viene letto dalla tab_tipi_commessa  (cod_tipo_mov_ver_prod_finiti) -> già codificato nella funzione di avanzamento commessa
//		- il tipo movimento per gli scarichi MP viene letto dalla tab_tipi_commessa  (cod_tipo_mov_prel_mat_prime) -> già codificato nella funzione di avanzamento commessa


//quindi 
//as_deposito_prelievo_mp					--> tab_tipi_commessa.cod_deposito_sl		|
//as_deposito_versamento_sl				--> tab_tipi_commessa.cod_deposito_sl		|  questo deposito sarà impostato anche nella variante del SL

//as_cod_tipo_mov_mp						--> tab_tipi_commessa.cod_tipo_mov_prel_mat_prime
//as_cod_tipo_mov_sl				 			--> tab_tipi_commessa.cod_tipo_mov_ver_sl


//leggo tipo commessa e deposito prelievo mp
select		cod_tipo_commessa
into		:ls_cod_tipo_commessa
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:il_anno_commessa and
			num_commessa=:il_num_commessa;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura tipo della commessa: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Commessa "+string(il_anno_commessa)+"/"+string(il_num_commessa)+" non trovata!"
	return -1
else
	//lettura da anag_commesse OK
	
	if isnull(ls_cod_tipo_commessa) or ls_cod_tipo_commessa="" then
		as_errore = "Impostare il tipo nella Commessa "+string(il_anno_commessa)+"/"+string(il_num_commessa)+"!"
		return -1
	end if

	//--------------------------------------------------------------------------------------------
	//leggo i tipi movimento da utilizzare dal tipo commessa e il deposito versamento dei semilavorati (che utilizzerò anche per le MP) e come deposito della variante del SL
	select 	cod_tipo_mov_ver_sl, cod_tipo_mov_prel_mat_prime,
				cod_deposito_sl, des_tipo_commessa
	into 	:as_cod_tipo_mov_sl, :as_cod_tipo_mov_mp,
			:as_deposito_versamento_sl, :ls_des_tipo_commessa
	from tab_tipi_commessa
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_commessa=:ls_cod_tipo_commessa;
	
	if isnull(ls_des_tipo_commessa) then ls_des_tipo_commessa = ""
	ls_des_tipo_commessa = ls_cod_tipo_commessa + " " + ls_des_tipo_commessa
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore lettura tipi movimento MP/SL dal tipo commessa: "+sqlca.sqlerrtext
		return -1
		
	elseif as_cod_tipo_mov_sl="" or isnull(as_cod_tipo_mov_sl) then
		as_errore = "Sul tipo commessa "+ls_des_tipo_commessa+" non è stato impostato il tipo movimento versamento semilavorati! "+&
						"Serve per il carico del SemiLavorato!"
		return -1
		
	elseif as_cod_tipo_mov_mp="" or isnull(as_cod_tipo_mov_mp) then
		as_errore = "Sul tipo commessa "+ls_des_tipo_commessa+" non è stato impostato il tipo movimento prelievo materie prime! "+&
						"Serve per lo scarico delle MP del ramo!"
		return -1
		
	elseif as_deposito_versamento_sl="" or isnull(as_deposito_versamento_sl) then
		as_errore = "Sul tipo commessa "+ls_des_tipo_commessa+" non è stato impostato il deposito versamento semilavorati! "+&
						"Serve per lo scarico delle MP del ramo ed il carico del Semilavorato!"
		return -1
		
	else
		//OK
		
		//deposito versamento SL e prelievo MP coincidono
		as_deposito_prelievo_mp = as_deposito_versamento_sl
	end if
	
end if


return 0
end function

public subroutine wf_scarico_parziale (treeviewitem ftvi_campo, ref string fs_errore, ref integer fi_return);string						ls_cod_formula_quan_utilizzo, ls_cod_prodotto_figlio, ls_cod_misura, ls_flag_materia_prima,  & 
							ls_cod_prodotto_padre, ls_des_estesa, ls_formula_tempo, ls_cod_prodotto_variante, ls_flag_scarico_parziale, &
							ls_flag_ramo_descrittivo, ls_cod_prodotto, ls_cod_figlio, ls_cod_versione_padre, ls_prodotto_nota[], &
							ls_cod_versione_variante_padre, ls_cod_versione_variante, ls_rb_integraz_variante, ls_modalita, &
							ls_cod_deposito_prelievo, ls_cod_deposito_versamento, ls_cod_tipo_mov_mp, ls_cod_tipo_mov_sl, ls_dep_variante

boolean					lb_insert = true

long						ll_lead_time,ll_i, ll_handle

dec{4}					ldd_quan_utilizzo, ldd_quan_tecnica, ldd_coef_calcolo, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t

s_chiave_distinta		l_chiave_distinta, l_chiave_distinta_2
treeviewitem			ltvi_campo_2
s_cs_xx_parametri		lstr_parm


//se l'elemento selezionato nel treeview non ha figli non permettere di andare avanti
if tab_1.tabpage_1.tv_db.FindItem (ChildTreeItem!, il_handle) < 0 then
	//questo elemento non ha figli
	fs_errore = "Questo ramo di commessa non ha materie prime!"
	fi_return = 1
	return
end if



//as_deposito_prelievo_mp					--> tab_tipi_commessa.cod_deposito_sl		|
//as_deposito_versamento_sl				--> tab_tipi_commessa.cod_deposito_sl		|  questo deposito sarà impostato anche nella variante del SL
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//as_cod_tipo_mov_mp						--> tab_tipi_commessa.cod_tipo_mov_prel_mat_prime
//as_cod_tipo_mov_sl				 			--> tab_tipi_commessa.cod_tipo_mov_ver_sl
if wf_parametri_commessa(ls_cod_deposito_prelievo, ls_cod_tipo_mov_mp, ls_cod_deposito_versamento, ls_cod_tipo_mov_sl, fs_errore) < 0 then
	fi_return = -1
	return
end if

//per eventuali allineamenti fatti in anag_commessa dalla funzione precedente
commit;



l_chiave_distinta = ftvi_campo.data
tab_1.tabpage_1.dw_ricerca.accepttext()

ls_cod_prodotto_figlio 		= l_chiave_distinta.cod_prodotto_figlio
ls_cod_versione_variante 	= l_chiave_distinta.cod_versione_figlio
ldd_quan_utilizzo       		= l_chiave_distinta.quan_utilizzo
ldd_quan_tecnica        		= 0

if ls_cod_prodotto_figlio = "" or isnull(ls_cod_prodotto_figlio) then
	fs_errore = "E' necessario specificare il prodotto figlio!"
	fi_return = -1
	return
end if
//------------------------------------------------

ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle

do while 1 = ll_i 
	//carico i dati del nodo numero ll_handle nel nodo ltvi_campo_2
	tab_1.tabpage_1.tv_db.GetItem (ll_handle, ltvi_campo_2 )
	l_chiave_distinta_2 = ltvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
		//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
	else
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	end if
	
	//cerca il padre del nodo numero ll_handle
	ll_handle = tab_1.tabpage_1.tv_db.FindItem ( ParentTreeItem!, ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice

loop

//if l_chiave_distinta.flag_tipo_record = "I" then
//	fs_errore = "Attenzione !!!~r~Non è logico fare la variante su una integrazione. Al più modificare l'integrazione."
//	return "-1"
//end if

select	flag_scarico_parziale, cod_deposito_produzione
into	:ls_flag_scarico_parziale, :ls_dep_variante
from   varianti_commesse
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :il_anno_commessa and
			num_commessa = :il_num_commessa and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo variante esistente: "+sqlca.sqlerrtext
	fi_return = -1
	return
end if

lb_insert = true

if sqlca.sqlcode=0 then
	//variante già esistente
	if ls_flag_scarico_parziale="S" then
		//dai messaggio che già esiste
		fs_errore = "Questo ramo di commessa ha già subito il Carico Parziale!"
		fi_return = 1
		return
	else
		//variante già esistente quindi non devi crearla ...
		lb_insert = false
	end if
end if

if lb_insert then
	select cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 formula_tempo,	
			 des_estesa,
			 flag_materia_prima,
			 flag_ramo_descrittivo,
			 lead_time
	into     :ls_cod_misura,
			 :ldd_quan_tecnica,
			 :ldd_quan_utilizzo,
			 :ldd_dim_x,
			 :ldd_dim_y,
			 :ldd_dim_z,
			 :ldd_dim_t,
			 :ldd_coef_calcolo,
			 :ls_formula_tempo,
			 :ls_des_estesa,
			 :ls_flag_materia_prima,
			 :ls_flag_ramo_descrittivo,
			 :ll_lead_time
	from   distinta
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
				num_sequenza = :l_chiave_distinta.num_sequenza and
				cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
				cod_versione=:l_chiave_distinta.cod_versione_padre;

	if sqlca.sqlcode < 0 then
		fs_errore = "Attenzione! Errore in lettura dati da distinta:" + sqlca.sqlerrtext
		fi_return = -1
		return
	end if

	if ls_flag_ramo_descrittivo = "S" then
		fs_errore = "Impossibile fare varianti o integrazioni su rami descrittivi"
		fi_return = -1
		return
	end if

	if isnull(ll_lead_time) or ll_lead_time = 0 then
		select lead_time
		into   :ll_lead_time
		from   anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;
		if sqlca.sqlcode <> 0 then ll_lead_time = 0

	end if

	//creo variante uguale a ramo distinta
	insert into varianti_commesse
			(cod_azienda,
			 anno_commessa,
			 num_commessa,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione,
			 cod_versione_figlio,
			 cod_prodotto,
			 cod_versione_variante,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 formula_tempo,
			 flag_esclusione,
			 des_estesa,
			 flag_materia_prima,
			 lead_time,
			 cod_deposito_produzione,
			 cod_reparto_produzione,
			 flag_scarico_parziale)
	values (	:s_cs_xx.cod_azienda,
				:il_anno_commessa,
				:il_num_commessa,
				:l_chiave_distinta.cod_prodotto_padre,
				:l_chiave_distinta.num_sequenza,
				:l_chiave_distinta.cod_prodotto_figlio,
				:l_chiave_distinta.cod_versione_padre,
				:l_chiave_distinta.cod_versione_figlio,
				:ls_cod_prodotto_figlio,
				:ls_cod_versione_variante,
				:ls_cod_misura,
				:ldd_quan_tecnica,
				:ldd_quan_utilizzo,
				:ldd_dim_x,
				:ldd_dim_y,
				:ldd_dim_z,
				:ldd_dim_t,
				:ldd_coef_calcolo,
				:ls_formula_tempo,
				'N',
				:ls_des_estesa,
				:ls_flag_materia_prima,
				:ll_lead_time,
				:ls_cod_deposito_versamento,
				null,
				'N');

	if sqlca.sqlcode < 0 then
		fs_errore = "Attenzione! Errore in inserimento Variante Commessa:" + sqlca.sqlerrtext
		fi_return = -1
		return
	end if
else
	//se la variante già esiste e non ha deposito produzione, lo imposto con il deposito versamento SL del tipo commessa
	if isnull(ls_dep_variante) or ls_dep_variante="" then

		update varianti_commesse
		set cod_deposito_produzione=:ls_cod_deposito_versamento
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_commessa = :il_anno_commessa and
					num_commessa = :il_num_commessa and
					cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
					num_sequenza = :l_chiave_distinta.num_sequenza and
					cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
					cod_versione = :l_chiave_distinta.cod_versione_padre and
					cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
				 
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in impostazione deposito produzione variante: "+sqlca.sqlerrtext
			fi_return = -1
			return
		end if
		
	end if
	
end if

//commit necessario perchè la funzione di estrazione mat prime varianti esegue il rollback dopo aver estratto le materie prime
//dentro la finestra "w_varianti_commesse_avanzamento"
//al massimo mi ritroverò una variante su stesso codice prodotto semilavorato
commit;

//apri finestrella riepilogativa
lstr_parm.parametro_ul_1 = il_anno_commessa
lstr_parm.parametro_ul_2 = il_num_commessa
lstr_parm.parametro_s_1_a[1] = ls_cod_prodotto_figlio
lstr_parm.parametro_s_1_a[2] = ls_cod_deposito_versamento
lstr_parm.parametro_s_1_a[3] = ls_cod_tipo_mov_sl

lstr_parm.parametro_s_1_a[4] = ls_cod_deposito_prelievo
lstr_parm.parametro_s_1_a[5] = ls_cod_tipo_mov_mp

openwithparm(w_varianti_commesse_avanzamento, lstr_parm)

lstr_parm = message.powerobjectparm
if not lstr_parm.parametro_b_1 then
	fs_errore = "Operazione annullata dall'utente!"
	fi_return = 1
	return
end if


//se arrivi fin esegui lo scarico parziale sul ramo commessa
if wf_scarico_parziale(	l_chiave_distinta.cod_prodotto_padre, l_chiave_distinta.num_sequenza, l_chiave_distinta.cod_prodotto_figlio, &
								l_chiave_distinta.cod_versione_padre, l_chiave_distinta.cod_versione_figlio, fs_errore)<0 then
	fi_return = -1
	return
end if

fi_return = 0
return


end subroutine

public function integer wf_azione (string fs_modalita);//fs_modalita
//"CV="			modalità crea variante (scegliere prodotto e relativa quantità)					pop-up menu
//"SP"				scarico parziale ramo commessa														pop-up menu


treeviewitem				ltvi_campo
string							ls_ret, ls_errore
integer						li_ret

//if not wf_controlla_stampati() then
//	return 0
//end if

if il_handle>0 then
else
	return -1
end if

if il_handle = 1 and (fs_modalita="SP" or fs_modalita="CV=") then
	g_mb.warning("Attenzione! Il prodotto finito non può essere associato ad una variante!")
	return -1
end if

//leggo il nodo del tree corrispondente a il_handle
tab_1.tabpage_1.tv_db.GetItem (il_handle, ltvi_campo )

choose case fs_modalita
	case "SP"
		wf_scarico_parziale(ltvi_campo, ls_errore, li_ret)
		
	case "CV="
		//li_ret = wf_dragdrop_variante(ltvi_campo, "Z", ls_errore)
		
	case else
		g_mb.warning("Attenzione! Azione non tra quelle previste ('SP', 'CV=')!")
		return -1
		
end choose


choose case li_ret
	case is < 0
		rollback;
		 g_mb.error(ls_errore)
		 return -1
		 
	case 0
		commit;
		g_mb.success("Operazione effettuata con successo!")
		
		if  (fs_modalita="SP" or fs_modalita="CV")then
			tab_1.tabpage_3.dw_varianti_dettaglio.Change_DW_Current( )
			this.triggerevent("pc_retrieve")
		end if

		li_ret = wf_inizio()
		return 0
		
	case else
		//probabilmente hai risposto no a qualche richiesta di conferma oppure c'è qualche impedimento non grave
		//se c'è un messaggio comunque mostralo all'utente
		rollback;
		if ls_errore<>"" and not isnull(ls_errore) then g_mb.warning(ls_errore)
		
		return 0
		
end choose

end function

public function integer wf_chiudi_commessa (ref string as_errore);string					ls_flag_tipo_avanzamento, ls_null, ls_errore
uo_funzioni_1		luo_funz
integer				li_ret


select flag_tipo_avanzamento
into   :ls_flag_tipo_avanzamento
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:il_anno_commessa
and    num_commessa=:il_num_commessa;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in la lettura  flag tipo avanzamento della commessa: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
	ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
	as_errore = "La commessa " + string(il_anno_commessa) + "/" + string(il_num_commessa) +&
					" si trova in uno stato per il quale non è più possibile effettuare un avanzamento automatico!"
	return 1
else
	
	if not g_mb.confirm("Chiudere la commessa " + string(il_anno_commessa) + "/" + string(il_num_commessa) + " ?") then
		as_errore ="Operazione annullata dall'operatore!"
		return 1
	end if
	
	setnull(ls_null)
	
	luo_funz = create uo_funzioni_1
	li_ret = luo_funz.uof_avanza_commessa(il_anno_commessa, il_num_commessa, ls_null, ls_errore)
	destroy luo_funz
	
	if li_ret < 0 then 
		as_errore ="Errore in chiusura commessa. " + ls_errore
		return -1
	end if
	
	return 0
end if 
end function

public function integer wf_trova (string fs_cod_prodotto, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tab_1.tabpage_1.tv_db.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
				tab_1.tabpage_1.tv_db.setfocus()
				tab_1.tabpage_1.tv_db.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

on w_varianti_commesse.create
int iCurrent
call super::create
this.cb_genera_var_depositi=create cb_genera_var_depositi
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_genera_var_depositi
this.Control[iCurrent+2]=this.tab_1
end on

on w_varianti_commesse.destroy
call super::destroy
destroy(this.cb_genera_var_depositi)
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;long 	ll_null
integer ll_num_dettagli, li_anno_ordine
string ls_errore
dec{4} ldd_quan_in_produzione,ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine,  ldd_quan_impegnata
uo_magazzino luo_magazzino


tab_1.tabpage_2.dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew +c_nodelete + c_nomodify,c_default)

tab_1.tabpage_3.dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)

//tab_1.tabpage_4.dw_varianti_commessa.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)
tab_1.tabpage_4.dw_varianti_commessa.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen, c_default)

tab_1.tabpage_5.dw_integrazioni_commessa.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)

tab_1.tabpage_3.dw_varianti_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)

tab_1.tabpage_3.dw_varianti_dettaglio.set_dw_options(sqlca,tab_1.tabpage_3.dw_varianti_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)

tab_1.tabpage_1.dw_varianti_commesse_quantita.insertrow(0)
iuo_dw_main = tab_1.tabpage_2.dw_distinta_det


is_cod_prodotto_finito = s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_prodotto")
is_cod_versione = s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_versione")
il_anno_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"anno_commessa")
il_num_commessa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"num_commessa")

// all'apertura delle varianti x commessa devo disimpegnare le mp della configurazione di 
// partenza, poi alla chiusura devo impegnare le materie prime della nuova configurazione
// questo perchè l'utente ha la possibilità di cambiare le MP e anche di far diventare gli
// SL delle MP

setnull(ll_null)
ldd_quan_in_produzione = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_in_produzione")
ldd_quan_assegnata = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_assegnata")
ldd_quan_prodotta = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_prodotta")
ldd_quan_in_ordine = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_ordine")

ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta

luo_magazzino = create uo_magazzino

if luo_magazzino.uof_impegna_mp_commessa(false, &
															false, &
															is_cod_prodotto_finito, &
															is_cod_versione, &
															ldd_quan_impegnata, &
															il_anno_commessa, &
															il_num_commessa, &
															ls_errore) = -1 then
	g_mb.messagebox("Apice","Errore durante il disimpegno",stopsign!)
	destroy luo_magazzino
	rollback;
	return -1
end if

this.height = w_cs_xx_mdi.mdi_1.height - 50

wf_inizio()


//se la commessa non è relativa a nessuna riga ordine disabilito il pulsante in alto che genera le varianti per deposito commessa
//basato su tiga ordine vendita
select 	anno_registrazione
into 		:li_anno_ordine 
from		det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :il_anno_commessa and
			num_commessa = :il_num_commessa;

if sqlca.sqlcode <> 0 then
	cb_genera_var_depositi.enabled = false
end if

commit;

destroy luo_magazzino
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.tabpage_2.dw_distinta_det,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.tabpage_3.dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_1.tabpage_4.dw_varianti_commessa, &
				  "cod_deposito_produzione", &
				  sqlca, &
				  "anag_depositi", &
				  "cod_deposito", &
				  "des_deposito", &
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event close;call super::close;string ls_errore

dec{4} ldd_quan_in_produzione,ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
		 ldd_quan_impegnata
		 
uo_magazzino  luo_magazzino


ldd_quan_in_produzione = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_in_produzione")
ldd_quan_assegnata = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_assegnata")
ldd_quan_prodotta = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_prodotta")
ldd_quan_in_ordine = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"quan_ordine")

ldd_quan_impegnata = ldd_quan_in_ordine - ldd_quan_assegnata - ldd_quan_in_produzione - ldd_quan_prodotta

luo_magazzino = create uo_magazzino

if luo_magazzino.uof_impegna_mp_commessa(	true, &
														false, &
														is_cod_prodotto_finito, &
														is_cod_versione, &
														ldd_quan_impegnata, &
														il_anno_commessa, &
														il_num_commessa, &
														ls_errore) = -1 then
	g_mb.messagebox("Apice","Errore durante il disimpegno",stopsign!)
	destroy luo_magazzino
	rollback;
	return -1
end if

commit;

destroy luo_magazzino

end event

event resize;call super::resize;
setredraw(false)

cb_genera_var_depositi.x = 466
cb_genera_var_depositi.y = 12
cb_genera_var_depositi.width = 338
cb_genera_var_depositi.height = 76

tab_1.tabpage_1.dw_ricerca.x = 30
tab_1.tabpage_1.dw_ricerca.y = 50
tab_1.tabpage_1.dw_ricerca.width = 2400
tab_1.tabpage_1.dw_ricerca.height = 180

tab_1.tabpage_1.dw_azioni.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.dw_azioni.y = 260
tab_1.tabpage_1.dw_azioni.width = 3415
tab_1.tabpage_1.dw_azioni.height = 212

tab_1.tabpage_1.dw_varianti_commesse_quantita.x = tab_1.tabpage_1.dw_ricerca.x + tab_1.tabpage_1.dw_ricerca.width + 20 //2441
tab_1.tabpage_1.dw_varianti_commesse_quantita.y = tab_1.tabpage_1.dw_ricerca.y  //224
tab_1.tabpage_1.dw_varianti_commesse_quantita.width = 750
tab_1.tabpage_1.dw_varianti_commesse_quantita.height = 200

tab_1.tabpage_1.tv_db.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.tv_db.y = 474
tab_1.tabpage_1.tv_db.width = tab_1.width - 60
tab_1.tabpage_1.tv_db.height = tab_1.height - tab_1.tabpage_1.dw_ricerca.height - tab_1.tabpage_1.dw_azioni.height - 250

tab_1.tabpage_2.dw_distinta_det.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_2.dw_distinta_det.y = 50
tab_1.tabpage_2.dw_distinta_det.width = 2587
tab_1.tabpage_2.dw_distinta_det.height = 1432

tab_1.tabpage_3.st_gruppi_varianti.x = 10
tab_1.tabpage_3.st_gruppi_varianti.y = 12
tab_1.tabpage_3.st_gruppi_varianti.width = 1897
tab_1.tabpage_3.st_gruppi_varianti.height = 84

tab_1.tabpage_3.st_prodotti_varianti.x = 2190
tab_1.tabpage_3.st_prodotti_varianti.y = 12
tab_1.tabpage_3.st_prodotti_varianti.width = 1129
tab_1.tabpage_3.st_prodotti_varianti.height = 84

tab_1.tabpage_3.st_dettaglio_variante.x = 18
tab_1.tabpage_3.st_dettaglio_variante.y = 512
tab_1.tabpage_3.st_dettaglio_variante.width = 2514
tab_1.tabpage_3.st_dettaglio_variante.height = 84

tab_1.tabpage_3.dw_distinta_gruppi_varianti.x = 14
tab_1.tabpage_3.dw_distinta_gruppi_varianti.y = 100
tab_1.tabpage_3.dw_distinta_gruppi_varianti.width = 1957
tab_1.tabpage_3.dw_distinta_gruppi_varianti.height = 400

tab_1.tabpage_3.dw_varianti_lista.x = 1970
tab_1.tabpage_3.dw_varianti_lista.y = tab_1.tabpage_3.dw_distinta_gruppi_varianti.y
tab_1.tabpage_3.dw_varianti_lista.width = 1138
tab_1.tabpage_3.dw_varianti_lista.height = 632

tab_1.tabpage_3.cb_crea.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.cb_crea.y = 692
tab_1.tabpage_3.cb_crea.width = 361
tab_1.tabpage_3.cb_crea.height = 80

tab_1.tabpage_3.dw_varianti_dettaglio.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.dw_varianti_dettaglio.y = 788
tab_1.tabpage_3.dw_varianti_dettaglio.width = 2537
tab_1.tabpage_3.dw_varianti_dettaglio.height = 1020

tab_1.tabpage_4.dw_varianti_commessa.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_4.dw_varianti_commessa.y = 50
tab_1.tabpage_4.dw_varianti_commessa.width = 2519
tab_1.tabpage_4.dw_varianti_commessa.height = 1476

tab_1.tabpage_4.dw_lista_reparti.x = tab_1.tabpage_4.dw_varianti_commessa.x
tab_1.tabpage_4.dw_lista_reparti.y = tab_1.tabpage_4.dw_varianti_commessa.y + tab_1.tabpage_4.dw_varianti_commessa.height + 10
tab_1.tabpage_4.dw_lista_reparti.width = int( ( tab_1.tabpage_4.width / 2 ) - 200)
tab_1.tabpage_4.dw_lista_reparti.height =  tab_1.tabpage_4.height - tab_1.tabpage_4.dw_varianti_commessa.height - 100

tab_1.tabpage_4.dw_reparti_variante.y = tab_1.tabpage_4.dw_varianti_commessa.y + tab_1.tabpage_4.dw_varianti_commessa.height + 10
tab_1.tabpage_4.dw_reparti_variante.width = int( ( tab_1.tabpage_4.width / 2 ) - 200)
tab_1.tabpage_4.dw_reparti_variante.height =  tab_1.tabpage_4.height - tab_1.tabpage_4.dw_varianti_commessa.height - 100
tab_1.tabpage_4.dw_reparti_variante.x = tab_1.tabpage_4.width - tab_1.tabpage_4.dw_reparti_variante.width - 10

tab_1.tabpage_4.cb_aggiungi_reparto.x = int(  tab_1.tabpage_4.width / 2 ) - int( tab_1.tabpage_4.cb_aggiungi_reparto.width / 2 )
tab_1.tabpage_4.cb_aggiungi_reparto.y = tab_1.tabpage_4.dw_lista_reparti.y + int ( tab_1.tabpage_4.dw_lista_reparti.height / 3 )

tab_1.tabpage_4.cb_rimuovi_reparto.x = int(  tab_1.tabpage_4.width / 2 ) - int( tab_1.tabpage_4.cb_rimuovi_reparto.width / 2 )
tab_1.tabpage_4.cb_rimuovi_reparto.y = tab_1.tabpage_4.dw_lista_reparti.y + int ( ( tab_1.tabpage_4.dw_lista_reparti.height / 3)  * 2)

tab_1.tabpage_5.dw_integrazioni_commessa.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_5.dw_integrazioni_commessa.y = 50
tab_1.tabpage_5.dw_integrazioni_commessa.width = 2400
tab_1.tabpage_5.dw_integrazioni_commessa.height = 1316

setredraw(true)



end event

type cb_genera_var_depositi from commandbutton within w_varianti_commesse
integer x = 18
integer y = 12
integer width = 361
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen.Var.Fasi"
end type

event clicked;string ls_errore, ls_flag_supervisore
long	ll_ret
uo_funzioni_1  luo_funzioni_1

// verifico che l'utente sia un supervisore
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	select flag_supervisore
	into 	:ls_flag_supervisore
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
else
	ls_flag_supervisore = "S"
end if

if ls_flag_supervisore = "S" then

	luo_funzioni_1 = create uo_funzioni_1
	ll_ret = luo_funzioni_1.uof_gen_varianti_deposito_commesse ( il_anno_commessa, il_num_commessa, is_cod_prodotto_finito, is_cod_versione, ref ls_errore )
	if ll_ret < 0 then
		g_mb.error(ls_errore)
		rollback;
	else
		commit;
		
		wf_inizio()
		
	end if
	destroy luo_funzioni_1

else
	g_mb.warning("Per usare questa funzione l'utente deve avere il privilegio di Supervisore !")
end if


end event

type tab_1 from tab within w_varianti_commesse
event create ( )
event destroy ( )
integer y = 112
integer width = 3552
integer height = 2060
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

event selectionchanged;choose case newindex
	case 1
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.triggerevent("clicked")
	case 2
		iuo_dw_main = tab_1.tabpage_2.dw_distinta_det
		wf_visual_testo_formula(tab_1.tabpage_2.dw_distinta_det.getitemstring(tab_1.tabpage_2.dw_distinta_det.getrow(), "cod_formula_quan_utilizzo"))
	case 3
		iuo_dw_main = tab_1.tabpage_3.dw_distinta_gruppi_varianti
	case 4
		iuo_dw_main = tab_1.tabpage_4.dw_varianti_commessa
	case 5
		iuo_dw_main = tab_1.tabpage_5.dw_integrazioni_commessa
		
end choose
end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3515
integer height = 1932
long backcolor = 12632256
string text = "Struttura"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
tv_db tv_db
end type

on tabpage_1.create
this.dw_varianti_commesse_quantita=create dw_varianti_commesse_quantita
this.dw_azioni=create dw_azioni
this.dw_ricerca=create dw_ricerca
this.tv_db=create tv_db
this.Control[]={this.dw_varianti_commesse_quantita,&
this.dw_azioni,&
this.dw_ricerca,&
this.tv_db}
end on

on tabpage_1.destroy
destroy(this.dw_varianti_commesse_quantita)
destroy(this.dw_azioni)
destroy(this.dw_ricerca)
destroy(this.tv_db)
end on

type dw_varianti_commesse_quantita from datawindow within tabpage_1
integer x = 2651
integer y = 192
integer width = 750
integer height = 200
integer taborder = 160
boolean bringtotop = true
string title = "none"
string dataobject = "d_varianti_commesse_quantita"
boolean border = false
boolean livescroll = true
end type

type dw_azioni from u_dw_search within tabpage_1
event ue_aggiorna_distinta ( )
integer x = 14
integer y = 16
integer width = 3008
integer height = 208
integer taborder = 170
boolean bringtotop = true
string dataobject = "d_azioni_varianti_commessa"
boolean border = false
end type

event ue_aggiorna_distinta();wf_inizio()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_elimina_var"
		wf_elimina_varianti()
		
	case "b_elimina_integraz"
		wf_elimina_integrazioni()
		
//	case "b_prezzo_variante"
//		wf_prezzo()
		
	case "b_stampa"
		wf_report_distinta()
		
	case "b_comprimi"
		long ll_tvi
		
		dw_azioni.accepttext()
		tv_db.setredraw( false)
		ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
		wf_comprimi(ll_tvi)
		tv_db.setredraw( true)

	case "b_espandi"
		long ll_handle, ll_livelli
		
		dw_azioni.accepttext()
		ll_livelli = dw_azioni.getitemnumber(dw_azioni.getrow(),"flag_livello")
		il_cicli = 0
		
		tv_db.setredraw( false)
		
		il_handle = tv_db.finditem(roottreeitem!,0)
		wf_comprimi(il_handle)
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			wf_espandi(il_handle, ll_livelli, il_cicli)
		end if
		
		tv_db.setredraw( true)		
end choose



end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_visione_dati"
		
		postevent("ue_aggiorna_distinta")
		
end choose
end event

type dw_ricerca from u_dw_search within tabpage_1
event ue_key pbm_dwnkey
integer x = 27
integer y = 236
integer width = 2450
integer height = 156
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_varianti_commessa_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		this.setcolumn("cod_versione")
		
	case "b_trova"
		string ls_cod_prodotto
		
		ls_cod_prodotto = dw_ricerca.getitemstring( 1, "rs_cod_prodotto")
		
		if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
			g_mb.messagebox( "SEP", "Attenzione:selezionare un prodotto per eseguire la ricerca!")
			return -1
		end if
		
		if isnull(il_handle) or il_handle = 0 then
			il_handle = tv_db.finditem(roottreeitem!,0)
		end if
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			if wf_trova(ls_cod_prodotto,il_handle) = 100 then
				g_mb.messagebox("Menu Principale","Prodotto non trovato",information!)
				tab_1.tabpage_1.tv_db.setfocus()
			end if
		end if		
		
	case else
		drag(Begin!)
		
end choose
end event

event constructor;call super::constructor;this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "cs_sep.ico"
end event

event itemchanged;call super::itemchanged;long ll_test

if not isnull(data) then
	dw_varianti_commesse_quantita.setitem(dw_varianti_commesse_quantita.getrow(),"versione",is_cod_versione)
end if

choose case dwo.name
	case "rs_cod_prodotto"
		
		if isnull(data) or data = "" then
		else
			
			f_PO_LoadDDDW_DW( this, &
			                  "cod_versione", &
									sqlca, &
						         "distinta", &
									" cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  			" cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")						  
		end if
		
end choose
end event

event clicked;call super::clicked;
choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		this.setcolumn("cod_versione")
	
	case else
		drag(Begin!)
		
end choose
end event

type tv_db from treeview within tabpage_1
integer x = 18
integer y = 424
integer width = 3479
integer height = 1468
integer taborder = 40
string dragicon = "Exclamation!"
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event clicked;if handle<>0 then
	il_handle = handle
	iuo_dw_main = tab_1.tabpage_2.dw_distinta_det
	tab_1.tabpage_2.dw_distinta_det.Change_DW_Current( )
	w_varianti_commesse.triggerevent("pc_retrieve")
	
	iuo_dw_main = tab_1.tabpage_3.dw_distinta_gruppi_varianti
	tab_1.tabpage_3.dw_distinta_gruppi_varianti.Change_DW_Current( )
	w_varianti_commesse.triggerevent("pc_retrieve")
	
	iuo_dw_main = tab_1.tabpage_4.dw_varianti_commessa
	tab_1.tabpage_4.dw_varianti_commessa.Change_DW_Current( )
	w_varianti_commesse.triggerevent("pc_retrieve")
	
	iuo_dw_main = tab_1.tabpage_5.dw_integrazioni_commessa
	tab_1.tabpage_5.dw_integrazioni_commessa.Change_DW_Current( )
	w_varianti_commesse.triggerevent("pc_retrieve")
end if
end event

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")								//	1
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")							//	2
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")								//	3
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")					//	4
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")						//	5
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")					//	6
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")				//	7
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")					//	8
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")				//	9
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")			//	10
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante_vincol.bmp")				//	11

end event

event dragdrop;string ls_cod_formula_quan_utilizzo,ls_cod_prodotto_figlio,ls_cod_misura,ls_flag_materia_prima,ls_flag_escludibile, & 
		 ls_cod_prodotto_padre,ls_messaggio,ls_errore,ls_des_estesa,ls_formula_tempo,ls_cod_prodotto_variante,ls_test, &
		 ls_flag_ramo_descrittivo, ls_cod_versione_variante,ls_cod_versione_variante_padre,ls_cod_versione_padre, &
		 ls_flag_integraz_variante
integer li_risposta
long ll_risposta, ll_progressivo, ll_num_sequenza, ld_cont,ll_lead_time
dec{4} ldd_quan_utilizzo,ldd_quan_tecnica,ldd_coef_calcolo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo


ls_flag_integraz_variante = dw_azioni.getitemstring(1,"flag_integraz_variante")

if ls_flag_integraz_variante="I" then

	GetItem (handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
	
	ls_cod_prodotto_figlio = upper(dw_ricerca.getitemstring(dw_ricerca.getrow(), "rs_cod_prodotto"))
	
	if isnull(ls_cod_prodotto_figlio) or len(ls_cod_prodotto_figlio) < 1 then
		 g_mb.messagebox("Sep","Selezionare un prodotto valido !",Information!)
		 return
	end if
		
	if l_chiave_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio   then
		 g_mb.messagebox("Sep","Attenzione: padre e figlio non possono coincidere!")
		 return
	end if
	
		// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
	long ll_i, ll_handle
	string  ls_cod_prodotto, ls_cod_figlio
	string prova
	treeviewitem  tvi_campo_2
	s_chiave_distinta l_chiave_distinta_2
	ll_i = 1
	//numero del nodo da cui parto il_handle
	ll_handle = il_handle
	
	do while 1 = ll_i 
		//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
		GetItem (ll_handle, tvi_campo_2 )
		l_chiave_distinta_2 = tvi_campo_2.data
		if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
			//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
			ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
		else
			ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
		end if
		
		if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
			g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata", stopsign!)
			return
		end if
		//cerca il padre del nodo numero ll_handle
		ll_handle = FindItem ( ParentTreeItem!	,  ll_handle)
		if ll_handle = -1 then exit   // allora sono alla radice
	
	loop
	
	//*********fine*** 
	
	SELECT cod_misura_mag,
			 flag_escludibile,
			 flag_materia_prima,
			 lead_time
	INTO   :ls_cod_misura,
			 :ls_flag_escludibile,
			 :ls_flag_materia_prima,
			 :ll_lead_time
	FROM   anag_prodotti  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_prodotto = :ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	if isnull(ls_flag_escludibile) then ls_flag_escludibile =  'N'
	if isnull(ls_flag_materia_prima) then ls_flag_materia_prima =  'N'
	
	ll_progressivo = 0
	
	select max(progressivo)
	into   :ll_progressivo
	from   integrazioni_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_commessa = :il_anno_commessa and
			 num_commessa = :il_num_commessa;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	if isnull(ll_progressivo) then ll_progressivo = 0
	
	ll_progressivo = ll_progressivo + 1
	
	wf_visual_testo_formula(ls_cod_formula_quan_utilizzo)
	
	if ls_cod_formula_quan_utilizzo = "" then setnull(ls_cod_formula_quan_utilizzo)
	
	dw_varianti_commesse_quantita.accepttext()	
	
	ldd_quan_utilizzo = dw_varianti_commesse_quantita.getitemnumber( 1, "qta_utilizzo")
	ldd_quan_tecnica = dw_varianti_commesse_quantita.getitemnumber( 1, "qta_tecnica")
	ls_cod_versione_variante = dw_varianti_commesse_quantita.getitemstring( 1, "versione")
	
	if ldd_quan_utilizzo = 0 then
		if g_mb.messagebox("Integrazioni","Quantità dell'integrazione = 0; vuoi correggere?",Question!, YesNo!, 2) = 1 then
			return
		end if
	end if
	
	if tvi_campo.level = 1 then
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
		ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
	else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_padre = l_chiave_distinta.cod_versione_figlio
	end if
	ll_num_sequenza = l_chiave_distinta.num_sequenza
	
	setnull(ls_cod_prodotto_variante)
	
	select cod_prodotto
	into   :ls_cod_prodotto_variante, :ls_cod_versione_variante_padre
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :il_anno_commessa and
			num_commessa = :il_num_commessa and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;
		
	if not isnull(ls_cod_prodotto_variante) and len(ls_cod_prodotto_variante) > 0 then
		ls_cod_prodotto_padre = ls_cod_prodotto_variante
		ls_cod_versione_padre = ls_cod_versione_variante_padre
	end if
	
	select flag_ramo_descrittivo
	into   :ls_flag_ramo_descrittivo
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			 num_sequenza = :l_chiave_distinta.num_sequenza and
			 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			 cod_versione = :l_chiave_distinta.cod_versione_padre;
	
	if ls_flag_ramo_descrittivo = "S" then
		g_mb.messagebox("Apice", "Impossibile effettuare varianti o integrazioni su rami descrittivi.",stopsign!)
		return
	end if

	insert into integrazioni_commessa
			(cod_azienda,
			 anno_commessa,
			 num_commessa,
			 progressivo,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 des_estesa,
			 cod_formula_quan_utilizzo,
			 flag_escludibile,
			 flag_materia_prima,
			 lead_time,
			 cod_versione_padre,
			 cod_versione_figlio)
	 values
			 (:s_cs_xx.cod_azienda,
			  :il_anno_commessa,
			  :il_num_commessa,
			  :ll_progressivo,
			  :ls_cod_prodotto_padre,
			  :ll_num_sequenza,
			  :ls_cod_prodotto_figlio,
			  :ls_cod_misura,
			  :ldd_quan_tecnica,
			  :ldd_quan_utilizzo,
			  0,
			  0,
			  0,
			  0,
			  0,
			  null,
			  :ls_cod_formula_quan_utilizzo,
			  :ls_flag_escludibile,
			  :ls_flag_materia_prima,
			  :ll_lead_time,
			  :ls_cod_versione_padre,
			  :ls_cod_versione_variante);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Errore sul DB:" + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
elseif ls_flag_integraz_variante="V" then
	
//	tv_db.GetItem ( il_handle, tvi_campo )
	tv_db.GetItem (handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
	
	if handle = 1 then
		g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
		return
	end if
	
	ls_cod_prodotto_figlio = upper(dw_ricerca.getitemstring(dw_ricerca.getrow(), "rs_cod_prodotto"))
	
	// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
	ll_i = 1
	//numero del nodo da cui parto il_handle
	ll_handle = il_handle
	
	do while 1 = ll_i 
		//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
		GetItem (ll_handle, tvi_campo_2 )
		l_chiave_distinta_2 = tvi_campo_2.data
		if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
			//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
			ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
		else
			ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
		end if
		
//		if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
//			g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata", stopsign!)
//			return
//		end if
		//cerca il padre del nodo numero ll_handle
		ll_handle = FindItem ( ParentTreeItem!	,  ll_handle)
		if ll_handle = -1 then exit   // allora sono alla radice
	
	loop
	
	//*********fine*** 
	
	if isnull(ls_cod_prodotto_figlio) or len(ls_cod_prodotto_figlio) < 1 then
		 g_mb.messagebox("Sep","Selezionare un prodotto valido !",Information!)
		 return
	end if
		
	if l_chiave_distinta.flag_tipo_record = "I" then
		g_mb.messagebox("SEP", "Attenzione !!!~r~Non è logico fare la variante su una integrazione. Semmai modificare l'integrazione.", StopSign!)
		return
	end if
	
	select cod_azienda
	into   :ls_test
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :il_anno_commessa and
			num_commessa = :il_num_commessa and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione = :is_cod_versione;
	
	if sqlca.sqlcode <=0 then
		g_mb.messagebox("Sep","Attenzione! Esiste già una variante commessa per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
		return
	end if
	
	select cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 formula_tempo,	
			 des_estesa,
			 flag_materia_prima,
			 flag_ramo_descrittivo,
			 lead_time
	into   :ls_cod_misura,
			 :ldd_quan_tecnica,
			 :ldd_quan_utilizzo,
			 :ldd_dim_x,
			 :ldd_dim_y,
			 :ldd_dim_z,
			 :ldd_dim_t,
			 :ldd_coef_calcolo,
			 :ls_formula_tempo,
			 :ls_des_estesa,
			 :ls_flag_materia_prima,
			 :ls_flag_ramo_descrittivo,
			 :ll_lead_time
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
		    num_sequenza = :l_chiave_distinta.num_sequenza and
		    cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
		    cod_versione=:l_chiave_distinta.cod_versione_padre;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti per commessa:" + sqlca.sqlerrtext ,stopsign!)
		return
	end if
	
	if ls_flag_ramo_descrittivo = "S" then
		g_mb.messagebox("APICE","Impossibile fare varianti o integrazioni su rami descrittivi")
		return 0
	end if
	
	if isnull(ll_lead_time) or ll_lead_time = 0 then
		select lead_time
		into   :ll_lead_time
		from   anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;
		if sqlca.sqlcode <> 0 then ll_lead_time = 0
	end if
	
	dw_varianti_commesse_quantita.accepttext()	
	
	ldd_quan_utilizzo        = dw_varianti_commesse_quantita.getitemnumber(1, "qta_utilizzo")
	ldd_quan_tecnica         = dw_varianti_commesse_quantita.getitemnumber(1, "qta_tecnica")
	ls_cod_versione_variante = dw_varianti_commesse_quantita.getitemstring(1, "versione")

	if ldd_quan_utilizzo = 0 then
		g_mb.messagebox("Variante","La quantità della variante è zero.")
	else
		ldd_quan_utilizzo = ldd_quan_utilizzo
		ldd_quan_tecnica = ldd_quan_tecnica
	end if
	
	insert into varianti_commesse
	(cod_azienda,
	 anno_commessa,
	 num_commessa,
	 cod_prodotto_padre,
	 num_sequenza,
	 cod_prodotto_figlio,
	 cod_versione_figlio,
	 cod_versione,
	 cod_prodotto,
	 cod_misura,
	 quan_tecnica,
	 quan_utilizzo,
	 dim_x,
	 dim_y,
	 dim_z,
	 dim_t,
	 coef_calcolo,
	 formula_tempo,
	 flag_esclusione,
	 des_estesa,
	 flag_materia_prima,
	 lead_time,
	 cod_versione_variante)
	values
	(:s_cs_xx.cod_azienda,
	 :il_anno_commessa,
	 :il_num_commessa,
	 :l_chiave_distinta.cod_prodotto_padre,
	 :l_chiave_distinta.num_sequenza,
	 :l_chiave_distinta.cod_prodotto_figlio,
 	 :l_chiave_distinta.cod_versione_figlio,
	 :l_chiave_distinta.cod_versione_padre,
	 :ls_cod_prodotto_figlio, 
	 :ls_cod_misura,
	 :ldd_quan_tecnica,
	 :ldd_quan_utilizzo,
	 :ldd_dim_x,
	 :ldd_dim_y,
	 :ldd_dim_z,
	 :ldd_dim_t,
	 :ldd_coef_calcolo,
	 :ls_formula_tempo,
	 'N',
	 :ls_des_estesa,
	 :ls_flag_materia_prima,
	 :ll_lead_time,
	 :ls_cod_versione_variante);
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti per commessa:" + sqlca.sqlerrtext ,stopsign!)
		return
	end if
	
	tab_1.tabpage_4.dw_varianti_commessa.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	
end if

commit;
	
ll_risposta=wf_inizio()	


end event

event dragwithin;TreeViewItem		ltvi_Over

If GetItem(handle, ltvi_Over) = -1 Then
	SetDropHighlight(0)

	Return
End If


SetDropHighlight(handle)

il_handle = handle
end event

event rightclicked;treeviewitem			ltv_item
s_chiave_distinta		l_chiave_distinta
string						ls_tipo_nodo, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione_padre, ls_cod_versione_figlio, ls_titolo

if handle < 1 then return 

tv_db.getitem(handle, ltv_item)
il_handle = handle
l_chiave_distinta = ltv_item.data

ls_tipo_nodo = l_chiave_distinta.flag_tipo_record

ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio

m_tree_varianti lm_menu
lm_menu = create m_tree_varianti

if isnull(ls_cod_prodotto_padre) then ls_cod_prodotto_padre=""
if isnull(ls_cod_prodotto_figlio) then ls_cod_prodotto_figlio=""
if isnull(ls_cod_versione_padre) then ls_cod_versione_padre=""
if isnull(ls_cod_versione_figlio) then ls_cod_versione_figlio=""

ls_titolo = "Padre: "+ls_cod_prodotto_padre+" - Vers.:"+ls_cod_versione_padre
lm_menu.m_padre.text = ls_titolo

ls_titolo = "Figlio: "+ls_cod_prodotto_figlio+" - Vers.:"+ls_cod_versione_figlio
lm_menu.m_figlio.text = ls_titolo

lm_menu.m_azzeraquantità.visible = false
lm_menu.m_creavariante.visible = false
lm_menu.m_creaintegrazione.visible = false

//voci di menu dedicate al tree della commessa
lm_menu.m_scaricoparziale.visible = true
lm_menu.m_chiudicommessa.visible = true		//questa va abilitata solo sulla root

if handle = 1 then
	lm_menu.m_chiudicommessa.enabled = true
	lm_menu.m_scaricoparziale.enabled = false
end if


pcca.window_current  = w_varianti_commesse

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3515
integer height = 1932
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_distinta_det dw_distinta_det
end type

on tabpage_2.create
this.dw_distinta_det=create dw_distinta_det
this.Control[]={this.dw_distinta_det}
end on

on tabpage_2.destroy
destroy(this.dw_distinta_det)
end on

type dw_distinta_det from uo_cs_xx_dw within tabpage_2
integer x = 14
integer y = 12
integer width = 2784
integer height = 1640
integer taborder = 60
string dataobject = "d_varianti_commesse_distinta_det"
boolean border = false
end type

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	ib_proteggi_chiavi=false
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_sequenza
	string ls_cod_prodotto_padre
	ib_proteggi_chiavi=false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo

	tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
	
	if l_chiave_distinta.cod_prodotto_figlio="" then
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	end if
	
	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
   triggerevent("itemchanged")

   select max(distinta.num_sequenza)
   into   :ll_sequenza
   from   distinta
   where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
          distinta.cod_prodotto_padre = :ls_cod_prodotto_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
   ll_sequenza = ll_sequenza + 10
   setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
   setcolumn("cod_prodotto_figlio")
	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))	
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, & 
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	long ll_risposta
	
		ll_risposta=wf_inizio()	
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
		SetItem(l_Idx, "cod_versione", is_cod_versione)	
   END IF
NEXT

end event

type tabpage_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3515
integer height = 1932
long backcolor = 12632256
string text = "Varianti Disponibili"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_crea cb_crea
dw_varianti_lista dw_varianti_lista
st_prodotti_varianti st_prodotti_varianti
dw_varianti_dettaglio dw_varianti_dettaglio
st_dettaglio_variante st_dettaglio_variante
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
st_gruppi_varianti st_gruppi_varianti
end type

on tabpage_3.create
this.cb_crea=create cb_crea
this.dw_varianti_lista=create dw_varianti_lista
this.st_prodotti_varianti=create st_prodotti_varianti
this.dw_varianti_dettaglio=create dw_varianti_dettaglio
this.st_dettaglio_variante=create st_dettaglio_variante
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.st_gruppi_varianti=create st_gruppi_varianti
this.Control[]={this.cb_crea,&
this.dw_varianti_lista,&
this.st_prodotti_varianti,&
this.dw_varianti_dettaglio,&
this.st_dettaglio_variante,&
this.dw_distinta_gruppi_varianti,&
this.st_gruppi_varianti}
end on

on tabpage_3.destroy
destroy(this.cb_crea)
destroy(this.dw_varianti_lista)
destroy(this.st_prodotti_varianti)
destroy(this.dw_varianti_dettaglio)
destroy(this.st_dettaglio_variante)
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.st_gruppi_varianti)
end on

type cb_crea from commandbutton within tabpage_3
integer x = 2853
integer y = 508
integer width = 357
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Var."
end type

event clicked;string ls_cod_misura,ls_cod_prodotto_variante,ls_test,ls_des_estesa,ls_formula_tempo,ls_flag_materia_prima, ls_cod_versione_variante, &
       ls_flag_ramo_descrittivo
long   ll_lead_time
decimal ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if tab_1.tabpage_3.dw_distinta_gruppi_varianti.rowcount() <= 0 then
	g_mb.messagebox("Sep","Non esistono varianti preconfigurate per questo prodotto.",information!)
	return
end if

if il_handle = 1 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
	return
end if

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

ls_cod_misura = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"cod_misura")
ldd_quan_tecnica = tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"quan_tecnica")
ldd_quan_utilizzo= tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"quan_utilizzo") 
ldd_dim_x = tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"dim_x")
ldd_dim_y = tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"dim_y")
ldd_dim_z = tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"dim_z")
ldd_dim_t = tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"dim_t")
ldd_coef_calcolo= tab_1.tabpage_3.dw_varianti_dettaglio.getitemnumber(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"coef_calcolo")
ls_cod_prodotto_variante = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"cod_prodotto")
ls_cod_versione_variante = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"cod_versione")
ls_des_estesa = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"des_estesa")
ls_formula_tempo = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"formula_tempo")
ls_flag_materia_prima = tab_1.tabpage_3.dw_varianti_dettaglio.getitemstring(tab_1.tabpage_3.dw_varianti_dettaglio.getrow(),"flag_materia_prima")

select cod_azienda
into   :ls_test
from   varianti_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and    
		 anno_commessa = :il_anno_commessa	and    
		 num_commessa = :il_num_commessa		and    
		 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre	and    
		 cod_versione = :l_chiave_distinta.cod_versione_padre and
		 num_sequenza = :l_chiave_distinta.num_sequenza	and    
		 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio	and
		 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;

if sqlca.sqlcode <=0 then
	g_mb.messagebox("Sep","Attenzione! Esiste già una variante commessa per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
	return
end if

select lead_time, 
       flag_ramo_descrittivo
into   :ll_lead_time, 
       :ls_flag_ramo_descrittivo
from   distinta
where  cod_azienda         = :s_cs_xx.cod_azienda and
       cod_prodotto_padre  = :l_chiave_distinta.cod_prodotto_padre and
       cod_versione        = :l_chiave_distinta.cod_versione_padre and
		 num_sequenza        = :l_chiave_distinta.num_sequenza and
		 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
		 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep","Errore in ricerca ramo di distinta.",information!)
	return
end if

if ls_flag_ramo_descrittivo = "S" then
	g_mb.messagebox("sep","Attenzione!!! Non è possibile fare una variante su un ramo descrittivo.")
	return
end if
		 

insert into varianti_commesse
		(cod_azienda,
		 anno_commessa,
		 num_commessa,
		 cod_prodotto_padre,
		 cod_versione,
		 num_sequenza,
		 cod_prodotto_figlio,
		 cod_versione_figlio,
		 cod_prodotto,
		 cod_versione_variante,
		 cod_misura,
		 quan_tecnica,
		 quan_utilizzo,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 coef_calcolo,
		 formula_tempo,
		 flag_esclusione,
		 des_estesa,
		 flag_materia_prima,
		 cod_versione_variante,
		 lead_time)
values
		(:s_cs_xx.cod_azienda,
		 :il_anno_commessa,
		 :il_num_commessa,
		 :l_chiave_distinta.cod_prodotto_padre,
		 :l_chiave_distinta.cod_versione_padre,
		 :l_chiave_distinta.num_sequenza,
		 :l_chiave_distinta.cod_prodotto_figlio,
		 :l_chiave_distinta.cod_versione_figlio,
		 :ls_cod_prodotto_variante, 
		 :ls_cod_versione_variante,
		 :ls_cod_misura,
		 :ldd_quan_tecnica,
		 :ldd_quan_utilizzo,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ldd_coef_calcolo,
		 :ls_formula_tempo,
		 'N',
		 :ls_des_estesa,
		 :ls_flag_materia_prima,
		 :l_chiave_distinta.cod_versione_figlio,
		 :ll_lead_time);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti per commessa:" + sqlca.sqlerrtext ,stopsign!)
	return
end if

tab_1.tabpage_4.dw_varianti_commessa.Change_DW_Current( )
parent.triggerevent("pc_retrieve")
wf_inizio()

g_mb.messagebox("Sep","Creazione variante avvenuta con successo",information!)
end event

type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 2185
integer y = 104
integer width = 1134
integer height = 400
integer taborder = 150
string dataobject = "d_varianti_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_gruppo_variante

ls_cod_gruppo_variante = dw_distinta_gruppi_varianti.getitemstring(dw_distinta_gruppi_varianti.getrow(),"cod_gruppo_variante")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_variante)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type st_prodotti_varianti from statictext within tabpage_3
integer x = 2190
integer y = 12
integer width = 1129
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "PRODOTTI VARIANTI DEL GRUPPO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 18
integer y = 616
integer width = 2565
integer height = 1032
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_varianti_dettaglio"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type st_dettaglio_variante from statictext within tabpage_3
integer x = 18
integer y = 520
integer width = 2514
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "DETTAGLIO DELLA VARIANTE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
integer x = 14
integer y = 104
integer width = 1957
integer height = 400
integer taborder = 50
string dataobject = "d_distinta_gruppi_varianti"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_pickedrow;call super::pcd_pickedrow;dw_varianti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
						  
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if rowcount()>0 then
	dw_varianti_lista.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
else
	dw_varianti_lista.reset()
	dw_varianti_dettaglio.reset()
end if
end event

type st_gruppi_varianti from statictext within tabpage_3
integer x = 9
integer y = 12
integer width = 1897
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "GRUPPI DI VARIANTI DISPONIBILI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type tabpage_4 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3515
integer height = 1932
long backcolor = 12632256
string text = "Varianti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_rimuovi_reparto cb_rimuovi_reparto
cb_aggiungi_reparto cb_aggiungi_reparto
dw_reparti_variante dw_reparti_variante
dw_lista_reparti dw_lista_reparti
dw_varianti_commessa dw_varianti_commessa
end type

on tabpage_4.create
this.cb_rimuovi_reparto=create cb_rimuovi_reparto
this.cb_aggiungi_reparto=create cb_aggiungi_reparto
this.dw_reparti_variante=create dw_reparti_variante
this.dw_lista_reparti=create dw_lista_reparti
this.dw_varianti_commessa=create dw_varianti_commessa
this.Control[]={this.cb_rimuovi_reparto,&
this.cb_aggiungi_reparto,&
this.dw_reparti_variante,&
this.dw_lista_reparti,&
this.dw_varianti_commessa}
end on

on tabpage_4.destroy
destroy(this.cb_rimuovi_reparto)
destroy(this.cb_aggiungi_reparto)
destroy(this.dw_reparti_variante)
destroy(this.dw_lista_reparti)
destroy(this.dw_varianti_commessa)
end on

type cb_rimuovi_reparto from commandbutton within tabpage_4
integer x = 1477
integer y = 1764
integer width = 197
integer height = 100
integer taborder = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;string ls_cod_reparto_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio
long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_sequenza


ll_anno_registrazione = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "anno_commessa") 
ll_num_registrazione = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "num_commessa") 
ls_cod_prodotto_padre = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_prodotto_padre") 
ls_cod_versione_padre = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_versione") 
ll_num_sequenza = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "num_sequenza") 
ls_cod_prodotto_figlio = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_prodotto_figlio") 
ls_cod_versione_figlio	 = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_versione_figlio") 

ls_cod_reparto_produzione = dw_reparti_variante.getitemstring(dw_reparti_variante.getrow(),"varianti_commesse_prod_cod_reparto")

delete varianti_commesse_prod 
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_commessa = :ll_anno_registrazione and
		num_commessa = :ll_num_registrazione and
		cod_prodotto_padre = :ls_cod_prodotto_padre and
		cod_versione = :ls_cod_versione_padre and
		num_sequenza = :ll_num_sequenza and
		cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		cod_versione_figlio = :ls_cod_versione_figlio and
		cod_reparto = :ls_cod_reparto_produzione	;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in associazione reparto connesso alla variante" + sqlca.sqlerrtext)
	return 1
end if

commit;

tab_1.tabpage_4.dw_varianti_commessa.postevent("rowfocuschanged")

end event

type cb_aggiungi_reparto from commandbutton within tabpage_4
integer x = 1481
integer y = 1600
integer width = 197
integer height = 100
integer taborder = 130
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string ls_cod_reparto_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio
long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_sequenza


ll_anno_registrazione = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "anno_commessa") 
ll_num_registrazione = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "num_commessa") 
ls_cod_prodotto_padre = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_prodotto_padre") 
ls_cod_versione_padre = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_versione") 
ll_num_sequenza = tab_1.tabpage_4.dw_varianti_commessa.getitemnumber(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "num_sequenza") 
ls_cod_prodotto_figlio = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_prodotto_figlio") 
ls_cod_versione_figlio	 = tab_1.tabpage_4.dw_varianti_commessa.getitemstring(tab_1.tabpage_4.dw_varianti_commessa.getrow(), "cod_versione_figlio") 

ls_cod_reparto_produzione = dw_lista_reparti.getitemstring(dw_lista_reparti.getrow(),"anag_reparti_cod_reparto")

insert into varianti_commesse_prod (
			cod_azienda,
			anno_commessa,
			num_commessa,
			cod_prodotto_padre,
			cod_versione,
			num_sequenza,
			cod_prodotto_figlio,
			cod_versione_figlio,
			cod_reparto			)
values	(:s_cs_xx.cod_azienda,
			:ll_anno_registrazione,
			:ll_num_registrazione,
			 :ls_cod_prodotto_padre,
			:ls_cod_versione_padre,
			:ll_num_sequenza,
			:ls_cod_prodotto_figlio,
			:ls_cod_versione_figlio,
			:ls_cod_reparto_produzione			);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in associazione reparto connesso alla variante" + sqlca.sqlerrtext)
	return 1
end if

commit;

tab_1.tabpage_4.dw_varianti_commessa.postevent("rowfocuschanged")

end event

type dw_reparti_variante from datawindow within tabpage_4
integer x = 1856
integer y = 1536
integer width = 1202
integer height = 376
integer taborder = 130
boolean bringtotop = true
string title = "none"
string dataobject = "d_varianti_commessa_reparti_variante"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_lista_reparti from datawindow within tabpage_4
integer x = 23
integer y = 1536
integer width = 1202
integer height = 376
integer taborder = 120
boolean bringtotop = true
string title = "none"
string dataobject = "d_varianti_det_ord_ven_reparti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_varianti_commessa from uo_cs_xx_dw within tabpage_4
integer x = 18
integer y = 24
integer width = 3045
integer height = 1496
integer taborder = 140
string dataobject = "d_varianti_commessa"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_varianti_commessa,"cod_prodotto")
	case "b_ricerca_prodotto_padre"
		guo_ricerca.uof_ricerca_prodotto(dw_varianti_commessa,"cod_prodotto_padre")
	case "b_ricerca_prodotto_figlio"
		guo_ricerca.uof_ricerca_prodotto(dw_varianti_commessa,"cod_prodotto_figlio")
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile
	long ll_num_sequenza
	
	choose case i_colname
		
		case "cod_deposito_produzione"
			wf_filtra_reparti(data)
		
		case "cod_prodotto"

			if len(i_coltext) < 1 then
				 PCCA.Error = c_ValFailed
				 return
			end if   
			// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
	
			long ll_handle
			string  ls_cod_prodotto, ls_cod_figlio, ls_descrizione
			treeviewitem  tvi_campo_2
			s_chiave_distinta l_chiave_distinta_2
			//numero del nodo da cui parto il_handle
			ll_handle = il_handle

			if not isnull(data) and (len(data) > 0) then
				select cod_prodotto
				into :ls_descrizione
				from anag_prodotti
				where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto= :i_coltext;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
					return 1
				end if
			end if

			do while true
				//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
				tab_1.tabpage_1.tv_db.getItem (ll_handle, tvi_campo_2 )
				l_chiave_distinta_2 = tvi_campo_2.data
				if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
					//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
					ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
				else
					ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
				end if
				
				if upper(ls_cod_prodotto) = upper(i_coltext) then
					g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nCorreggere il dato.", stopsign!)
					return 1
				end if
				//cerca il padre del nodo numero ll_handle
				ll_handle = tab_1.tabpage_1.tv_db.findItem ( ParentTreeItem!	,  ll_handle)
				if ll_handle = -1 then exit   // allora sono alla radice
			
			loop

			//*********fine*** 	
		
			SELECT cod_misura_mag  
			INTO   :ls_cod_misura  
			FROM   anag_prodotti  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND   cod_prodotto = :i_coltext;

			if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
				setitem(getrow(), "cod_misura", ls_cod_misura)
			end if
			
		case "flag_esclusione"
			if data = "S" then
				ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
				ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
				ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
				ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")
	
				select flag_escludibile
				 into :ls_flag_escludibile
				 from distinta
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto_padre = :ls_cod_prodotto_padre and
						num_sequenza = :ll_num_sequenza and
						cod_prodotto_figlio = :ls_cod_prodotto_figlio and
						cod_versione = :ls_cod_versione;
			
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Varianti Commessa", "Errore durante l'Estrazione Flag Escludibile!")
					return 2
				end if
				
				if ls_flag_escludibile = "N" then
					g_mb.messagebox("Varianti Commessa", "Il dettaglio Distinta non è escludibile!")
					return 2
				end if
			end if
			
	end choose
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	string ls_cod_deposito_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio, ls_cod_reparto_prod,ls_cod_reparto
	long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_sequenza, ll_i, ll_y
	
	ls_cod_deposito_produzione = getitemstring(getrow(), "cod_deposito_produzione") 

	ll_anno_registrazione = getitemnumber(getrow(), "anno_commessa") 
	ll_num_registrazione = getitemnumber(getrow(), "num_commessa") 
	ls_cod_prodotto_padre = getitemstring(getrow(), "cod_prodotto_padre") 
	ls_cod_versione_padre = getitemstring(getrow(), "cod_versione") 
	ll_num_sequenza = getitemnumber(getrow(), "num_sequenza") 
	ls_cod_prodotto_figlio = getitemstring(getrow(), "cod_prodotto_figlio") 
	ls_cod_versione_figlio	 = getitemstring(getrow(), "cod_versione_figlio") 
	dw_lista_reparti.reset()
	dw_reparti_variante.reset()
	
	if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
	
		dw_lista_reparti.enabled = true
		dw_reparti_variante.enabled=true

		dw_lista_reparti.setredraw(false)
		dw_reparti_variante.setredraw(false)
		dw_lista_reparti.settransobject(sqlca)
		dw_lista_reparti.SetRowFocusIndicator(hand!)
		dw_lista_reparti.retrieve(s_cs_xx.cod_azienda,ls_cod_deposito_produzione )
		

		dw_reparti_variante.settransobject(sqlca)
		dw_reparti_variante.SetRowFocusIndicator(hand!)
		dw_reparti_variante.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ls_cod_prodotto_padre, ls_cod_Versione_padre, ll_num_sequenza, ls_cod_prodotto_figlio, ls_cod_versione_figlio )

		for ll_i = 1 to dw_reparti_variante.rowcount()
			
			ls_cod_reparto_prod = dw_reparti_variante.getitemstring(ll_i, "varianti_commesse_prod_cod_reparto")
			for ll_y = 1 to dw_lista_reparti.rowcount()
				
				 if ls_cod_reparto_prod = dw_lista_reparti.getitemstring(ll_y, "anag_reparti_cod_reparto") then
				 	dw_lista_reparti.deleterow(ll_y)
					ll_y --
				end if
				
			next
			
		next
		dw_lista_reparti.setredraw(true)
		dw_reparti_variante.setredraw(true)
		
	else
		
		dw_lista_reparti.enabled = false
		dw_reparti_variante.enabled=false
	end if
		
	
end if


end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old, &
			 ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_errore
	dec{4} ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
			 ldd_quan_impegnata,ldd_quan_utilizzo, ldd_quan_utilizzo_old
	long ll_i,ll_anno_commessa,ll_num_commessa,ll_null, ll_num_sequenza 
//	uo_magazzino luo_magazzino

	ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
	ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")
	ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
	ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
	ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
	ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")
	
	select cod_prodotto,
			 cod_versione,
			 quan_utilizzo
	into   :ls_cod_prodotto_old,
			 :ls_cod_versione_old,
			 :ldd_quan_utilizzo_old
	from   varianti_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa and
			 cod_prodotto_padre = :ls_cod_prodotto_padre and
			 num_sequenza = :ll_num_sequenza and
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
			 cod_versione = :ls_cod_versione;
	
	ls_cod_prodotto = this.getitemstring(this.getrow(), "cod_prodotto")
	ldd_quan_utilizzo = this.getitemnumber(this.getrow(), "quan_utilizzo")
			
	setnull(ll_null)
	
	if ls_cod_prodotto <> ls_cod_prodotto_old or ls_cod_versione <> ls_cod_versione_old or &
		ldd_quan_utilizzo <> ldd_quan_utilizzo_old then
		
//		luo_magazzino = create uo_magazzino
//		
//		if luo_magazzino.uof_impegna_mp_commessa(	false, &
//																true, &
//																ls_cod_prodotto_old, &
//																ls_cod_versione_old, &
//																ldd_quan_utilizzo_old, &
//																ll_anno_commessa, &
//																ll_num_commessa, &
//																ls_errore) = -1 then
//			messagebox("Apice","Errore durante il disimpegno",stopsign!)
//			destroy luo_magazzino
//			rollback;
//			return -1
//		end if
//		
		
//		if f_impegna_mp( ls_cod_prodotto_old, ls_cod_versione_old, ldd_quan_utilizzo_old, ll_anno_commessa_old, &
//							ll_num_commessa_old, ll_null, "varianti_commesse", False ) = -1 then // Prima Disimpegno la quantita vecchia
//					messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	
//		if luo_magazzino.uof_impegna_mp_commessa(	true, &
//																true, &
//																ls_cod_prodotto, &
//																ls_cod_versione, &
//																ldd_quan_utilizzo, &
//																ll_anno_commessa, &
//																ll_num_commessa, &
//																ls_errore) = -1 then
//			messagebox("Apice","Errore durante il disimpegno",stopsign!)
//			destroy luo_magazzino
//			rollback;
//			return -1
//		end if
		
//		if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_utilizzo, ll_anno_commessa, ll_num_commessa, &
//							ll_null, "varianti_commesse", True ) = -1 then // Poi Impegno la nuova quantita
//				messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	end if
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    il_anno_commessa, &
						  il_num_commessa, &
						  l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

type tabpage_5 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3515
integer height = 1932
long backcolor = 12632256
string text = "Integrazioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_integrazioni_commessa dw_integrazioni_commessa
end type

on tabpage_5.create
this.dw_integrazioni_commessa=create dw_integrazioni_commessa
this.Control[]={this.dw_integrazioni_commessa}
end on

on tabpage_5.destroy
destroy(this.dw_integrazioni_commessa)
end on

type dw_integrazioni_commessa from uo_cs_xx_dw within tabpage_5
integer x = 14
integer y = 12
integer width = 2418
integer height = 1024
integer taborder = 80
string dataobject = "d_integrazioni_commessa"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile
	long ll_num_sequenza
	choose case i_colname
	
	case "cod_prodotto_figlio"

		if len(i_coltext) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if   
	
		SELECT cod_misura_mag  
      INTO   :ls_cod_misura  
      FROM   anag_prodotti  
      WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	   AND   cod_prodotto = :i_coltext;

      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
         setitem(getrow(), "cod_misura", ls_cod_misura)
      end if
		
	case "flag_esclusione"
		if data = "S" then
			ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
			ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
			ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
			ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")

			select flag_escludibile
			 into :ls_flag_escludibile
			 from distinta
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione = :ls_cod_versione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Varianti Commessa", "Errore durante l'Estrazione Flag Escludibile!")
				return 2
			end if
			
			if ls_flag_escludibile = "N" then
				g_mb.messagebox("Varianti Commessa", "Il dettaglio Distinta non è escludibile!")
				return 2
			end if
		end if
	end choose
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_old, ls_cod_versione_old, &
			 ls_cod_prodotto_padre, ls_cod_prodotto_figlio,ls_errore
	dec{4} ldd_quan_assegnata,ldd_quan_prodotta,ldd_quan_in_ordine, &
			 ldd_quan_impegnata,ldd_quan_utilizzo, ldd_quan_utilizzo_old
	long ll_i,ll_anno_commessa,ll_num_commessa,ll_null, ll_progressivo
//	uo_magazzino luo_magazzino

	ll_anno_commessa = this.getitemnumber(this.getrow(), "anno_commessa")
	ll_num_commessa = this.getitemnumber(this.getrow(), "num_commessa")
	ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
	ll_progressivo = this.getitemnumber(this.getrow(), "progressivo")
	ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
	
	select cod_prodotto_figlio,
			 quan_utilizzo
	into   :ls_cod_prodotto_old,
			 :ldd_quan_utilizzo_old
	from   integrazioni_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa and
			 progressivo = :ll_progressivo;
	
	ls_cod_prodotto = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
	ldd_quan_utilizzo = this.getitemnumber(this.getrow(), "quan_utilizzo")
			
	setnull(ll_null)
	
	if ls_cod_prodotto <> ls_cod_prodotto_old or ls_cod_versione <> ls_cod_versione_old or &
		ldd_quan_utilizzo <> ldd_quan_utilizzo_old then
		
//		luo_magazzino = create uo_magazzino
		
	// Prima Disimpegno la quantita vecchia		
//		if luo_magazzino.uof_impegna_mp_commessa(	false, &
//																true, &
//																ls_cod_prodotto_old, &
//																ls_cod_versione_old, &
//																ldd_quan_utilizzo_old, &
//																ll_anno_commessa, &
//																ll_num_commessa, &
//																ls_errore) = -1 then
//			messagebox("Apice","Errore durante il disimpegno",stopsign!)
//			destroy luo_magazzino
//			rollback;
//			return -1
//		end if
		
//		if f_impegna_mp( ls_cod_prodotto_old, ls_cod_versione_old, ldd_quan_utilizzo_old, ll_anno_commessa, ll_num_commessa, ll_null, "varianti_commesse", False ) = -1 then 
//					messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	
	// Poi Impegno la nuova quantita
//		if luo_magazzino.uof_impegna_mp_commessa(	true, &
//																true, &
//																ls_cod_prodotto, &
//																ls_cod_versione, &
//																ldd_quan_utilizzo, &
//																ll_anno_commessa, &
//																ll_num_commessa, &
//																ls_errore) = -1 then
//			messagebox("Apice","Errore durante il disimpegno",stopsign!)
//			destroy luo_magazzino
//			rollback;
//			return -1
//		end if
//		if f_impegna_mp( ls_cod_prodotto, ls_cod_versione, ldd_quan_utilizzo, ll_anno_commessa, ll_num_commessa, &
//							ll_null, "varianti_commesse", True ) = -1 then 
//				messagebox("SEP","Errore durante l'aggiornamento prodotti",stopsign!)
//			return 1
//		end if
	end if
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
//	cb_ricerca_prod_integrazione.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
//	cb_ricerca_prod_integrazione.enabled=true
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    il_anno_commessa, &
						  il_num_commessa, &
						  l_chiave_distinta.num_sequenza)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_figlio"
		guo_ricerca.uof_ricerca_prodotto(tab_1.tabpage_5.dw_integrazioni_commessa,"cod_prodotto_figlio")
end choose
end event

