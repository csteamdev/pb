﻿$PBExportHeader$w_varianti_commesse_avanzamento.srw
$PBExportComments$Gestione Varianti per Commesse
forward
global type w_varianti_commesse_avanzamento from window
end type
type dw_mp from datawindow within w_varianti_commesse_avanzamento
end type
type qta_pf from statictext within w_varianti_commesse_avanzamento
end type
type pf from statictext within w_varianti_commesse_avanzamento
end type
type mov_mp from statictext within w_varianti_commesse_avanzamento
end type
type dep_mp from statictext within w_varianti_commesse_avanzamento
end type
type mov_sl from statictext within w_varianti_commesse_avanzamento
end type
type dep_sl from statictext within w_varianti_commesse_avanzamento
end type
type sl from statictext within w_varianti_commesse_avanzamento
end type
type mov_mp_t from statictext within w_varianti_commesse_avanzamento
end type
type mov_sl_t from statictext within w_varianti_commesse_avanzamento
end type
type dep_mp_t from statictext within w_varianti_commesse_avanzamento
end type
type dep_sl_t from statictext within w_varianti_commesse_avanzamento
end type
type sl_t from statictext within w_varianti_commesse_avanzamento
end type
type qta_pf_t from statictext within w_varianti_commesse_avanzamento
end type
type prodotto_finito_t from statictext within w_varianti_commesse_avanzamento
end type
type st_commessa from statictext within w_varianti_commesse_avanzamento
end type
type cb_prosegui from commandbutton within w_varianti_commesse_avanzamento
end type
type cb_annulla from commandbutton within w_varianti_commesse_avanzamento
end type
type r_pf from rectangle within w_varianti_commesse_avanzamento
end type
type r_sl from rectangle within w_varianti_commesse_avanzamento
end type
type r_mp from rectangle within w_varianti_commesse_avanzamento
end type
end forward

global type w_varianti_commesse_avanzamento from window
integer width = 3150
integer height = 2300
boolean titlebar = true
string title = "Conferma Dati Avanzamento"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event ue_materie_prime ( integer ai_anno_commessa,  long al_num_commessa,  string as_cod_semilavorato,  string as_cod_deposito_mp )
dw_mp dw_mp
qta_pf qta_pf
pf pf
mov_mp mov_mp
dep_mp dep_mp
mov_sl mov_sl
dep_sl dep_sl
sl sl
mov_mp_t mov_mp_t
mov_sl_t mov_sl_t
dep_mp_t dep_mp_t
dep_sl_t dep_sl_t
sl_t sl_t
qta_pf_t qta_pf_t
prodotto_finito_t prodotto_finito_t
st_commessa st_commessa
cb_prosegui cb_prosegui
cb_annulla cb_annulla
r_pf r_pf
r_sl r_sl
r_mp r_mp
end type
global w_varianti_commesse_avanzamento w_varianti_commesse_avanzamento

forward prototypes
public function integer wf_materie_prime (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_deposito_mp, ref string as_errore)
end prototypes

event ue_materie_prime(integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_cod_deposito_mp);string			ls_errore

if wf_materie_prime(ai_anno_commessa, al_num_commessa, as_cod_semilavorato, as_cod_deposito_mp, ls_errore)<0 then
	cb_prosegui.enabled = false
	g_mb.error(ls_errore)
end if



return
end event

public function integer wf_materie_prime (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_deposito_mp, ref string as_errore);
uo_magazzino			luo_mag

uo_funzioni_1			luo_f1

datetime					ldt_data_a

dec{4}					ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]

string						ls_deposito_mp, ls_where, ls_materia_prima[], ls_versione_prima[], ls_chiave[], ls_des_deposito

integer					li_ret

double					ldd_quantita_utilizzo[], ldd_giacenza

long						ll_new, ll_index, ll_find




ls_deposito_mp = as_deposito_mp
ls_where = "and cod_deposito=~~'"+ls_deposito_mp+"~~'"
ldt_data_a = datetime(today(), 00:00:00)
dw_mp.reset()


//lettura MP
luo_f1 = create uo_funzioni_1


//-1		errore critico
//1			warning e dato non elaborato
//0			tutto OK
li_ret = luo_f1.uof_trova_mat_prime_varianti(	ai_anno_commessa, al_num_commessa, as_cod_semilavorato, true, &
															ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_deposito_mp, as_errore)
destroy luo_f1

if isnull(ls_deposito_mp) or ls_deposito_mp="" then ls_deposito_mp = as_deposito_mp

luo_mag = CREATE uo_magazzino


ls_des_deposito = f_des_tabella("anag_depositi","cod_deposito='" + ls_deposito_mp +"'" ,"des_deposito")

for ll_index=1 to upperbound(ls_materia_prima[])
	
	ll_find = dw_mp.find("cod_prodotto='"+ls_materia_prima[ll_index]+"'", 1, dw_mp.rowcount())
	
	if ll_find>0 then
		//riga esistente, vai ad incremento
		dw_mp.setitem(ll_index, "quantita", dw_mp.getitemdecimal(ll_index, "quantita") + ldd_quantita_utilizzo[ll_index])
	else
		//----------------------------------------------------------------------------------------------------
		ll_new = dw_mp.insertrow(0)
		dw_mp.setitem(ll_index, "cod_prodotto", ls_materia_prima[ll_index])
		dw_mp.setitem(ll_index, "cod_deposito", ls_deposito_mp)
		dw_mp.setitem(ll_index, "des_deposito", ls_des_deposito)
		dw_mp.setitem(ll_index, "quantita", ldd_quantita_utilizzo[ll_index])
		dw_mp.setitem(ll_index, "um", f_des_tabella("anag_prodotti","cod_prodotto='" + ls_materia_prima[ll_index] +"'" ,"cod_misura_mag"))
		//*****************
		
		li_ret = luo_mag.uof_saldo_prod_date_decimal(ls_materia_prima[ll_index], ldt_data_a, ls_where, ld_quant_val[], as_errore, "D", &
																					ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
		if li_ret <0 then
			destroy luo_mag
			return -1
		end if
	
		//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
		if upperbound(ld_giacenza_stock[]) > 0 then
			ldd_giacenza = ld_giacenza_stock[1]
		end if
		
		if isnull(ldd_giacenza) then ldd_giacenza = 0
		//*****************
			
		dw_mp.setitem(ll_new, "giacenza", ldd_giacenza)
		//----------------------------------------------------------------------------------------------------
	end if
	
next

destroy luo_mag
return 0
end function

on w_varianti_commesse_avanzamento.create
this.dw_mp=create dw_mp
this.qta_pf=create qta_pf
this.pf=create pf
this.mov_mp=create mov_mp
this.dep_mp=create dep_mp
this.mov_sl=create mov_sl
this.dep_sl=create dep_sl
this.sl=create sl
this.mov_mp_t=create mov_mp_t
this.mov_sl_t=create mov_sl_t
this.dep_mp_t=create dep_mp_t
this.dep_sl_t=create dep_sl_t
this.sl_t=create sl_t
this.qta_pf_t=create qta_pf_t
this.prodotto_finito_t=create prodotto_finito_t
this.st_commessa=create st_commessa
this.cb_prosegui=create cb_prosegui
this.cb_annulla=create cb_annulla
this.r_pf=create r_pf
this.r_sl=create r_sl
this.r_mp=create r_mp
this.Control[]={this.dw_mp,&
this.qta_pf,&
this.pf,&
this.mov_mp,&
this.dep_mp,&
this.mov_sl,&
this.dep_sl,&
this.sl,&
this.mov_mp_t,&
this.mov_sl_t,&
this.dep_mp_t,&
this.dep_sl_t,&
this.sl_t,&
this.qta_pf_t,&
this.prodotto_finito_t,&
this.st_commessa,&
this.cb_prosegui,&
this.cb_annulla,&
this.r_pf,&
this.r_sl,&
this.r_mp}
end on

on w_varianti_commesse_avanzamento.destroy
destroy(this.dw_mp)
destroy(this.qta_pf)
destroy(this.pf)
destroy(this.mov_mp)
destroy(this.dep_mp)
destroy(this.mov_sl)
destroy(this.dep_sl)
destroy(this.sl)
destroy(this.mov_mp_t)
destroy(this.mov_sl_t)
destroy(this.dep_mp_t)
destroy(this.dep_sl_t)
destroy(this.sl_t)
destroy(this.qta_pf_t)
destroy(this.prodotto_finito_t)
destroy(this.st_commessa)
destroy(this.cb_prosegui)
destroy(this.cb_annulla)
destroy(this.r_pf)
destroy(this.r_sl)
destroy(this.r_mp)
end on

event open;s_cs_xx_parametri			lstr_parm

long							ll_num_commessa

integer						li_anno_commessa

string							ls_cod_prodotto, ls_descrizione, ls_cod_semilavorato, ls_cod_deposito_sl, ls_cod_deposito_mp, &
								ls_cod_tipo_mov_sl, ls_cod_tipo_mov_mp, ls_errore
								
dec{4}						ld_quantita


lstr_parm = message.powerobjectparm


li_anno_commessa = lstr_parm.parametro_ul_1
ll_num_commessa = lstr_parm.parametro_ul_2

ls_cod_semilavorato = lstr_parm.parametro_s_1_a[1]
ls_cod_deposito_sl = lstr_parm.parametro_s_1_a[2]
ls_cod_tipo_mov_sl = lstr_parm.parametro_s_1_a[3]

ls_cod_deposito_mp = lstr_parm.parametro_s_1_a[4]
ls_cod_tipo_mov_mp = lstr_parm.parametro_s_1_a[5]


select		cod_prodotto,
			quan_ordine
into	:ls_cod_prodotto,
		:ld_quantita
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:li_anno_commessa and
			num_commessa=:ll_num_commessa;

st_commessa.text = "Avanzamento Commessa n° : "+string(li_anno_commessa) + " / " + string(ll_num_commessa)

ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto='" + ls_cod_prodotto +"'" ,"des_prodotto")
if isnull(ls_descrizione) then ls_descrizione=""
pf.text = ls_cod_prodotto + " - " + ls_descrizione

ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto='" + ls_cod_prodotto +"'" ,"cod_misura_mag")
if isnull(ls_descrizione) then ls_descrizione=""
qta_pf.text = string(ld_quantita, "###,###,##0.00") + " " + ls_descrizione


ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto='" + ls_cod_semilavorato +"'" ,"des_prodotto")
if isnull(ls_descrizione) then ls_descrizione=""
sl.text = ls_cod_semilavorato + " - " + ls_descrizione

ls_descrizione = f_des_tabella("anag_depositi","cod_deposito='" + ls_cod_deposito_sl +"'" ,"des_deposito")
if isnull(ls_descrizione) then ls_descrizione=""
dep_sl.text = ls_cod_deposito_sl + " - " + ls_descrizione

ls_descrizione = f_des_tabella("tab_tipi_movimenti","cod_tipo_movimento='" + ls_cod_tipo_mov_sl +"'" ,"des_tipo_movimento")
if isnull(ls_descrizione) then ls_descrizione=""
mov_sl.text = ls_cod_tipo_mov_sl + " - " + ls_descrizione

ls_descrizione = f_des_tabella("anag_depositi","cod_deposito='" + ls_cod_deposito_mp +"'" ,"des_deposito")
if isnull(ls_descrizione) then ls_descrizione=""
dep_mp.text = ls_cod_deposito_mp + " - " + ls_descrizione

ls_descrizione = f_des_tabella("tab_tipi_movimenti","cod_tipo_movimento='" + ls_cod_tipo_mov_mp +"'" ,"des_tipo_movimento")
if isnull(ls_descrizione) then ls_descrizione=""
mov_mp.text = ls_cod_tipo_mov_mp + " - " + ls_descrizione


event post ue_materie_prime(li_anno_commessa, ll_num_commessa, ls_cod_semilavorato, ls_cod_deposito_mp)

end event

type dw_mp from datawindow within w_varianti_commesse_avanzamento
integer x = 37
integer y = 1016
integer width = 3067
integer height = 1004
integer taborder = 20
string title = "none"
string dataobject = "d_giacenza_prodotti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type qta_pf from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 304
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean focusrectangle = false
end type

type pf from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 196
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean focusrectangle = false
end type

type mov_mp from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 900
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 128
long backcolor = 12632256
boolean focusrectangle = false
end type

type dep_mp from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 800
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 128
long backcolor = 12632256
boolean focusrectangle = false
end type

type mov_sl from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 656
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dep_sl from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 556
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type sl from statictext within w_varianti_commesse_avanzamento
integer x = 599
integer y = 448
integer width = 2478
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type mov_mp_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 908
integer width = 471
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 128
long backcolor = 12632256
string text = "Movimenti MP:"
boolean focusrectangle = false
end type

type mov_sl_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 664
integer width = 471
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
string text = "Movimento SL:"
boolean focusrectangle = false
end type

type dep_mp_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 800
integer width = 471
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 128
long backcolor = 12632256
string text = "Prelievo MP da:"
boolean focusrectangle = false
end type

type dep_sl_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 556
integer width = 471
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
string text = "Versamento SL in:"
boolean focusrectangle = false
end type

type sl_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 448
integer width = 471
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
string text = "Semilavorato:"
boolean focusrectangle = false
end type

type qta_pf_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 304
integer width = 471
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Quantità PF:"
boolean focusrectangle = false
end type

type prodotto_finito_t from statictext within w_varianti_commesse_avanzamento
integer x = 69
integer y = 196
integer width = 471
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "PF Commessa"
boolean focusrectangle = false
end type

type st_commessa from statictext within w_varianti_commesse_avanzamento
integer x = 37
integer y = 36
integer width = 3067
integer height = 116
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Avanzamento Commessa n° :"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_prosegui from commandbutton within w_varianti_commesse_avanzamento
integer x = 1093
integer y = 2100
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prosegui"
boolean default = true
end type

event clicked;
s_cs_xx_parametri			lstr_parm

lstr_parm.parametro_b_1 = true

closewithreturn(parent, lstr_parm)
end event

type cb_annulla from commandbutton within w_varianti_commesse_avanzamento
integer x = 1577
integer y = 2100
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;
s_cs_xx_parametri			lstr_parm

lstr_parm.parametro_b_1 = false

closewithreturn(parent, lstr_parm)
end event

type r_pf from rectangle within w_varianti_commesse_avanzamento
long linecolor = 8421504
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 188
integer width = 3067
integer height = 224
end type

type r_sl from rectangle within w_varianti_commesse_avanzamento
long linecolor = 8421504
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 436
integer width = 3067
integer height = 320
end type

type r_mp from rectangle within w_varianti_commesse_avanzamento
long linecolor = 8421504
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 780
integer width = 3067
integer height = 216
end type

