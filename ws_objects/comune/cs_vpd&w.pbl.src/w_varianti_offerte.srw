﻿$PBExportHeader$w_varianti_offerte.srw
$PBExportComments$Window varianti offerte
forward
global type w_varianti_offerte from w_cs_xx_principale
end type
type tab_1 from tab within w_varianti_offerte
end type
type tabpage_1 from userobject within tab_1
end type
type dw_det_off_ven_det_conf_variabili from datawindow within tabpage_1
end type
type dw_varianti_commesse_quantita from datawindow within tabpage_1
end type
type dw_azioni from u_dw_search within tabpage_1
end type
type dw_ricerca from u_dw_search within tabpage_1
end type
type tv_db from treeview within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_det_off_ven_det_conf_variabili dw_det_off_ven_det_conf_variabili
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
tv_db tv_db
end type
type tabpage_2 from userobject within tab_1
end type
type dw_distinta_det from uo_cs_xx_dw within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_distinta_det dw_distinta_det
end type
type tabpage_3 from userobject within tab_1
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
end type
type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
end type
type st_prodotti_varianti from statictext within tabpage_3
end type
type st_gruppi_varianti from statictext within tabpage_3
end type
type cb_crea from commandbutton within tabpage_3
end type
type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
dw_varianti_lista dw_varianti_lista
st_prodotti_varianti st_prodotti_varianti
st_gruppi_varianti st_gruppi_varianti
cb_crea cb_crea
dw_varianti_dettaglio dw_varianti_dettaglio
end type
type tabpage_4 from userobject within tab_1
end type
type dw_varianti_offerte from uo_cs_xx_dw within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_varianti_offerte dw_varianti_offerte
end type
type tabpage_5 from userobject within tab_1
end type
type dw_integrazioni_det_off_ven from uo_cs_xx_dw within tabpage_5
end type
type tabpage_5 from userobject within tab_1
dw_integrazioni_det_off_ven dw_integrazioni_det_off_ven
end type
type tab_1 from tab within w_varianti_offerte
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_varianti_offerte from w_cs_xx_principale
integer width = 3342
integer height = 2420
string title = "Varianti Offerta"
event pc_azzera_qta_variante ( )
event pc_crea_integrazione ( )
event pc_crea_variante ( )
tab_1 tab_1
end type
global w_varianti_offerte w_varianti_offerte

type variables
long il_handle, il_cicli
string is_cod_prodotto_finito,is_cod_versione, is_nota
long  il_anno_registrazione,il_num_registrazione,il_prog_riga
boolean ib_modifiche_consentite=false

end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public subroutine wf_elimina_varianti ()
public subroutine wf_elimina_integrazioni ()
public subroutine wf_prezzo ()
public subroutine wf_stampa ()
public function integer wf_comprimi (long al_handle)
public function integer wf_espandi (long al_handle, long al_livello, long al_cicli)
public function integer wf_dragdrop_integraz (ref treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore)
public subroutine wf_cod_versione (string fs_cod_prodotto, ref string fs_cod_versione)
public subroutine wf_crea_nota (string fs_prodotto_nota[], decimal fdd_qta_nota[], string fs_modalita)
public function integer wf_azione (string fs_modalita)
public function integer wf_dragdrop_variante (treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore)
public function integer wf_trova (string fs_cod_prodotto, long al_handle)
end prototypes

event pc_azzera_qta_variante();wf_azione("ZV")

end event

event pc_crea_integrazione();
wf_azione("CI")

end event

event pc_crea_variante();
wf_azione("CV")

end event

public function integer wf_inizio ();string ls_errore,ls_des_prodotto, ls_ordinamento
long ll_risposta
treeviewitem tvi_campo
uo_imposta_tv_varianti luo_varianti

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
l_chiave_distinta.cod_versione_padre = is_cod_versione

this.setredraw(false)

tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.settransobject(sqlca)
tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione, il_prog_riga)

tab_1.tabpage_1.tv_db.deleteitem(0)

ls_ordinamento = tab_1.tabpage_1.dw_azioni.getitemstring(tab_1.tabpage_1.dw_azioni.getrow(),"flag_visione_dati")

select 	des_prodotto
into   		:ls_des_prodotto
from   	anag_prodotti
where  	cod_azienda=:s_cs_xx.cod_azienda and 
			cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;

ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1

ll_risposta=tab_1.tabpage_1.tv_db.insertitemlast(0, tvi_campo)

luo_varianti = CREATE uo_imposta_tv_varianti
luo_varianti.is_tipo_gestione = "varianti_offerte"
luo_varianti.is_cod_versione = is_cod_versione
luo_varianti.il_anno_registrazione = il_anno_registrazione
luo_varianti.il_num_registrazione = il_num_registrazione
luo_varianti.il_prog_riga_registrazione = il_prog_riga
luo_varianti.uof_imposta_tv(ref tab_1.tabpage_1.tv_db, l_chiave_distinta.cod_prodotto_padre, is_cod_versione, 1, 1, ls_ordinamento, ref ls_errore)
destroy luo_varianti

tab_1.tabpage_1.tv_db.expandall(1)
this.setredraw(true)

tab_1.tabpage_1.tv_db.selectitem(1)


return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

tab_1.tabpage_2.dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public subroutine wf_elimina_varianti ();if g_mb.messagebox("Sep", "Confermi l'eliminazione di tutte le varianti?",Question!, YesNo!, 2) = 1 then

	delete from varianti_det_off_ven
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_off_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	wf_inizio()

end if

return
end subroutine

public subroutine wf_elimina_integrazioni ();if g_mb.messagebox("Sep", "Confermi l'eliminazione di tutte le integrazioni ?",Question!, YesNo!, 2) = 1 then

	delete from integrazioni_det_off_ven
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_off_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Sep","Errore in cancellazione integrazioni " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	wf_inizio()
	
	commit;

end if

return
end subroutine

public subroutine wf_prezzo ();string ls_cod_valuta, ls_cod_cliente, ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_listino_prodotto, ls_sql, &
       ls_prezzo_vendita, ls_sconto, ls_progressivo
long   ll_i
dec{4} ld_quan_offerta, ld_prezzo_vendita, ld_cambio_ven
datetime ldt_data_consegna, ldt_data_registrazione
uo_condizioni_cliente luo_condizioni_cliente

select cod_valuta, 
       cod_cliente, 
		 cod_agente_1, 
		 cod_agente_2, 
		 cod_tipo_listino_prodotto,
		 data_registrazione,
		 cambio_ven
into   :ls_cod_valuta, 
       :ls_cod_cliente, 
		 :ls_cod_agente_1, 
		 :ls_cod_agente_2, 
		 :ls_cod_tipo_listino_prodotto,
		 :ldt_data_registrazione,
		 :ld_cambio_ven
from   tes_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione  and
		 num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata offerte di vendita~r~n" + sqlca.sqlerrtext)
	return
end if

select data_consegna,
       quan_offerta,
		 cod_versione
into   :ldt_data_consegna,
       :ld_quan_offerta,
		 :s_cs_xx.listino_db.cod_versione
from   det_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione  and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_off_ven = :il_prog_riga;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca riga offerte di vendita~r~n"+sqlca.sqlerrtext)
	return
end if

s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = il_anno_registrazione
s_cs_xx.listino_db.num_documento = il_num_registrazione
s_cs_xx.listino_db.prog_riga = il_prog_riga
s_cs_xx.listino_db.quan_prodotto_finito = ld_quan_offerta
s_cs_xx.listino_db.data_riferimento = ldt_data_consegna
s_cs_xx.listino_db.tipo_gestione = "varianti_det_off_ven"
s_cs_xx.listino_db.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
s_cs_xx.listino_db.cod_valuta = ls_cod_valuta
s_cs_xx.listino_db.cod_cliente = ls_cod_cliente
s_cs_xx.listino_db.data_inizio_val = ldt_data_consegna

luo_condizioni_cliente = CREATE uo_condizioni_cliente 
luo_condizioni_cliente.str_parametri.ldw_oggetto = tab_1.tabpage_4.dw_varianti_offerte
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
luo_condizioni_cliente.str_parametri.cambio_ven = double(ld_cambio_ven)
luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_consegna
luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
luo_condizioni_cliente.str_parametri.cod_prodotto = is_cod_prodotto_finito
luo_condizioni_cliente.str_parametri.dim_1 = 0
luo_condizioni_cliente.str_parametri.dim_2 = 0
luo_condizioni_cliente.str_parametri.quantita = ld_quan_offerta
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

if upperbound(luo_condizioni_cliente.str_output.variazioni) > 0 then
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
else
	ld_prezzo_vendita = 0
end if


ls_progressivo = "prog_riga_off_ven"
ls_prezzo_vendita = f_double_to_string(ld_prezzo_vendita)
ls_sql =" update det_off_ven"
ls_sql = ls_sql + " set prezzo_vendita = " + ls_prezzo_vendita
if upperbound(luo_condizioni_cliente.str_output.sconti) > 0 then
	for ll_i = 1 to upperbound(luo_condizioni_cliente.str_output.sconti)
		if ll_i > 10 then exit
		if luo_condizioni_cliente.str_output.sconti[ll_i] = 0 or isnull(luo_condizioni_cliente.str_output.sconti[ll_i]) then 
				ls_sconto = "0"
			else
				ls_sconto = f_double_to_string(luo_condizioni_cliente.str_output.sconti[ll_i])
				if isnull(ls_sconto) then ls_sconto = "0"
			end if
			ls_sql = ls_sql + ", sconto_" + string(ll_i) + " = " + ls_sconto
	next
end if
ls_sql =ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" anno_registrazione = " + string(il_anno_registrazione) + " and " + &
			" num_registrazione = " + string(il_num_registrazione) + " and " + &
			ls_progressivo + " = " + string(il_prog_riga)

execute immediate :ls_sql;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell'aggiornamento del prezzo di vendita. Dettaglio " + sqlca.sqlerrtext, exclamation!, ok!)
	rollback;
	destroy luo_condizioni_cliente
	return
end if
destroy luo_condizioni_cliente
commit;


end subroutine

public subroutine wf_stampa ();s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
s_cs_xx.parametri.parametro_s_2 = is_cod_versione
s_cs_xx.parametri.parametro_s_3 = "varianti_offerte"

s_cs_xx.parametri.parametro_i_1 = il_anno_registrazione
s_cs_xx.parametri.parametro_d_1 = il_num_registrazione
s_cs_xx.parametri.parametro_d_2 = il_prog_riga

window_open(w_report_distinta_varianti, -1) 
end subroutine

public function integer wf_comprimi (long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)

end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)
	
end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

tab_1.tabpage_1.tv_db.collapseitem( al_handle)

return 100
end function

public function integer wf_espandi (long al_handle, long al_livello, long al_cicli);long ll_handle, ll_padre, ll_i

treeviewitem ltv_item

il_cicli ++

if il_cicli > al_livello then
	return 100
end if

tab_1.tabpage_1.tv_db.getitem( al_handle, ltv_item)
tab_1.tabpage_1.tv_db.ExpandItem ( al_handle )	

ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	wf_espandi(ll_handle, al_livello, il_cicli)
	il_cicli --
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	il_cicli --
	wf_espandi(ll_handle, al_livello, il_cicli)
//	il_cicli --
	
end if

return 100
end function

public function integer wf_dragdrop_integraz (ref treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore);string					ls_cod_formula_quan_utilizzo,ls_cod_prodotto_figlio,ls_cod_misura,ls_flag_materia_prima,ls_flag_escludibile, & 
						ls_cod_prodotto_padre,ls_messaggio,ls_errore,ls_des_estesa,ls_formula_tempo,ls_cod_prodotto_variante,ls_test, &
						ls_flag_ramo_descrittivo,ls_cod_prodotto, ls_cod_versione_padre, ls_prodotto_nota[], ls_modalita, &
						ls_cod_versione_variante_padre, ls_cod_versione_variante, ls_rb_integraz_variante

integer				li_risposta

long					ll_risposta, ll_progressivo, ll_num_sequenza, ld_cont, ll_lead_time,ll_i, ll_handle, ll_count

dec{4}				ldd_quan_utilizzo, ldd_quan_tecnica, ldd_coef_calcolo, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, ldd_qta_nota[]

s_chiave_distinta	l_chiave_distinta, l_chiave_distinta_2, l_chiave_integraz_old
treeviewitem		ltvi_campo_2
any					la_cod_prodotto[]
datawindow			ldw_null
s_cs_xx_parametri lstr_parametri
boolean				lb_insert = true

//parametro fs_modalita
// "N"		modalita normale (dragdrop): il cod_prodotto e le qta sono lette dalle datawindow esterne (dw_ricerca e dw_varianti_commesse_quantita)
// "CI"

l_chiave_distinta = ftvi_campo.data

//se sei su un ramo di tipo Integrazione allora devi solo aggiornare
if l_chiave_distinta.flag_tipo_record = "I" then
	
	//mi salvo la chiave del nodo dell'integrazione, mi servirà per l'update finale
	l_chiave_integraz_old = l_chiave_distinta
	
	//spostati al nodo padre
	ll_handle = tab_1.tabpage_1.tv_db.FindItem (ParentTreeItem!,  il_handle)
	if ll_handle>0 then
		if tab_1.tabpage_1.tv_db.getitem(ll_handle, ftvi_campo) > 0 then
			
			
			//CurrentTreeItem!	
			
			il_handle = ll_handle
			l_chiave_distinta = ftvi_campo.data
			lb_insert = false
			
		else
			return 0
		end if
	else
		return 0
	end if
end if

tab_1.tabpage_1.dw_ricerca.accepttext()
//-------------------------------------------------
choose case fs_modalita
	case "N"
		//prodotto impostato sulla dw_ricerca e poi fatto dragdrop
		ls_cod_prodotto_figlio = upper(tab_1.tabpage_1.dw_ricerca.getitemstring(tab_1.tabpage_1.dw_ricerca.getrow(), "rs_cod_prodotto"))
		ls_cod_versione_variante = tab_1.tabpage_1.dw_ricerca.getitemstring(1, "cod_versione")
		//le qta le leggerai più sotto

	case "CI"
		ls_modalita = fs_modalita
		
		//creazione integrazione selezionata da anagrafica prodotti e quantità scelte da finestra pop-up (più sotto)
		setnull(ldw_null)
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
		guo_ricerca.uof_get_results( la_cod_prodotto[] )
		
		if upperbound(la_cod_prodotto[]) > 0 then
			ls_cod_prodotto_figlio = string(la_cod_prodotto[1])
		else
			//probabilmente hai fatto annulla dalla ricerca
			return 0
		end if
		wf_cod_versione(ls_cod_prodotto_figlio, ls_cod_versione_variante)
		//ls_cod_versione_variante = "001"
		
		ls_prodotto_nota[1] = ls_cod_prodotto_figlio
		
	case else
		fs_errore = "Valore argomento 'fs_modalita' non previsto (N, CI)"
		return -1
		
end choose

if ls_cod_prodotto_figlio = "" or isnull(ls_cod_prodotto_figlio) then
	fs_errore = "E' necessario specificare il prodotto Integrazione!"
	return -1
end if

tab_1.tabpage_1.dw_varianti_commesse_quantita.accepttext()
		
if l_chiave_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio   then
	 fs_errore = "Attenzione: padre e figlio non possono coincidere!"
	 return -1
end if
	
// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle
	
do while 1 = ll_i
	//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
	tab_1.tabpage_1.tv_db.GetItem (ll_handle, ltvi_campo_2 )
	l_chiave_distinta_2 = ltvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
		//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
	else
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	end if
	
	if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
		fs_errore = "Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata"
		return -1
	end if
	
	//cerca il padre del nodo numero ll_handle
	ll_handle = tab_1.tabpage_1.tv_db.FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice
loop


select		cod_misura_mag,
			flag_escludibile,
			flag_materia_prima,
			lead_time
into		:ls_cod_misura,
			:ls_flag_escludibile,
			:ls_flag_materia_prima,
			:ll_lead_time
from		anag_prodotti  
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto_figlio;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_flag_escludibile) then ls_flag_escludibile =  'N'
if isnull(ls_flag_materia_prima) then ls_flag_materia_prima =  'N'

ll_progressivo = 0

select max(progressivo)
into   :ll_progressivo
from   integrazioni_det_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_off_ven = :il_prog_riga;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_progressivo) then ll_progressivo = 0

ll_progressivo = ll_progressivo + 1

wf_visual_testo_formula(ls_cod_formula_quan_utilizzo)

if ls_cod_formula_quan_utilizzo = "" then setnull(ls_cod_formula_quan_utilizzo)


choose case fs_modalita
	case "N"
		ldd_quan_utilizzo = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(tab_1.tabpage_1.dw_varianti_commesse_quantita.getrow(), "qta_utilizzo")
		ldd_quan_tecnica = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(tab_1.tabpage_1.dw_varianti_commesse_quantita.getrow(), "qta_tecnica")
		
		if ldd_quan_utilizzo = 0 then
			if not g_mb.confirm("","Quantità dell'integrazione pari a ZERO. Vuoi correggere?", 2) then
				return 0
			end if
		end if
	
	case "CI"
		//creazione integrazione
		//digito le qta da una finestra
		lstr_parametri.parametro_d_1_a[1] =  l_chiave_distinta.quan_utilizzo
		lstr_parametri.parametro_d_1_a[2] =  0		//non so cosa passare, passo ZERO
		window_open_parm(w_menu_varianti_qta, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		if lstr_parametri.parametro_b_1 then
		else
			fs_errore = "Operazione annullata dall'utente!"
			return 0
		end if
		
		ldd_quan_utilizzo = lstr_parametri.parametro_d_1_a[1]
		ldd_quan_tecnica = lstr_parametri.parametro_d_1_a[2]
		
		ldd_qta_nota[1] = ldd_quan_utilizzo
		
	case else
		fs_errore = "(2) Valore argomento 'fs_modalita' non previsto (N, CI)"
		return -1
		
end choose

if ftvi_campo.level = 1 then
	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
else
	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	ls_cod_versione_padre = l_chiave_distinta.cod_versione_figlio
end if
ll_num_sequenza = l_chiave_distinta.num_sequenza

setnull(ls_cod_prodotto_variante)

select		cod_prodotto,
			cod_versione_variante
into		:ls_cod_prodotto_variante,
			:ls_cod_versione_variante_padre
from		varianti_det_off_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione and 
			prog_riga_off_ven = :il_prog_riga and 
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;
	
if not isnull(ls_cod_prodotto_variante) and len(ls_cod_prodotto_variante) > 0 then
	ls_cod_prodotto_padre = ls_cod_prodotto_variante
	ls_cod_versione_padre = ls_cod_versione_variante_padre
end if

select flag_ramo_descrittivo
into   :ls_flag_ramo_descrittivo
from   distinta
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;

if ls_flag_ramo_descrittivo = "S" then
	fs_errore = "Impossibile effettuare varianti o integrazioni su rami descrittivi."
	return -1
end if


if lb_insert then

	insert into integrazioni_det_off_ven
			(cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_off_ven,
			 progressivo,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 des_estesa,
			 cod_formula_quan_utilizzo,
			 flag_escludibile,
			 flag_materia_prima,
			 lead_time,
			 cod_versione_padre,
			 cod_versione_figlio)
	 values
			 (:s_cs_xx.cod_azienda,
			  :il_anno_registrazione,
			  :il_num_registrazione,
			  :il_prog_riga,
			  :ll_progressivo,
			  :ls_cod_prodotto_padre,
			  :ll_num_sequenza,
			  :ls_cod_prodotto_figlio,
			  :ls_cod_misura,
			  :ldd_quan_tecnica,
			  :ldd_quan_utilizzo,
			  0,
			  0,
			  0,
			  0,
			  0,
			  null,
			  :ls_cod_formula_quan_utilizzo,
			  :ls_flag_escludibile,
			  :ls_flag_materia_prima,
			  :ll_lead_time,
			  :ls_cod_versione_padre,
			  :ls_cod_versione_variante);
			  
else
	update 	integrazioni_det_off_ven
	set 		cod_prodotto_figlio=:ls_cod_prodotto_figlio,
				quan_tecnica=:ldd_quan_tecnica,
				quan_utilizzo=:ldd_quan_utilizzo
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione and
				prog_riga_off_ven = :il_prog_riga and
				cod_prodotto_padre = :l_chiave_integraz_old.cod_prodotto_padre and
				cod_prodotto_figlio = :l_chiave_integraz_old.cod_prodotto_figlio and
				cod_versione_padre = :l_chiave_integraz_old.cod_versione_padre and
				cod_versione_figlio =:l_chiave_integraz_old.cod_versione_figlio;
			
end if

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in inserimento/Aggiornamento Integrazioni Ordine:" + sqlca.sqlerrtext
	return -1
end if

//se arrivi fin qui aggiorna nella nota
if ls_modalita="CI" then
	wf_crea_nota(ls_prodotto_nota[], ldd_qta_nota[], ls_modalita)
end if

return 1
end function

public subroutine wf_cod_versione (string fs_cod_prodotto, ref string fs_cod_versione);//controlla se il prodotto passata ha versioni in distinta
//    se una sola allora considera quella
//    se non ne ha allora propone is_cod_versione  (se vuota mette fisso "001")
//    se più di una propone una finestra di scelta con menu a tendina
long					ll_count, ll_index
datastore			lds_data
string					ls_sql, ls_errore
s_cs_xx_parametri lstr_parametri

ls_sql = 		"select distinct cod_versione "+&
				"from distinta "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"cod_prodotto_padre='"+fs_cod_prodotto+"' "

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if isnull(ll_count) then ll_count=0

choose case ll_count
	case 0
		//seleziona is_cod_versione o se vuoto "001"
		if not isnull(is_cod_versione) and is_cod_versione<>"" then
			fs_cod_versione = is_cod_versione
		else
			fs_cod_versione = "001"
		end if
		
		return
		
	case 1
		//proponi esattamente l'unica versione in distinta
		fs_cod_versione = lds_data.getitemstring(1, "cod_versione")
		if isnull(fs_cod_versione) or fs_cod_versione="" then
			fs_cod_versione = "001"
		end if
		
		return
		
	case else
		//proponi la scelta
		lstr_parametri.parametro_s_1_a[1] = fs_cod_prodotto
		if not isnull(is_cod_versione) and is_cod_versione<>"" then
			lstr_parametri.parametro_s_1_a[2] = is_cod_versione
		else
			lstr_parametri.parametro_s_1_a[2] = "001"
		end if
		
		window_open_parm(w_menu_varianti_versione, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		fs_cod_versione = lstr_parametri.parametro_s_1_a[1]
		
end choose
	
	

end subroutine

public subroutine wf_crea_nota (string fs_prodotto_nota[], decimal fdd_qta_nota[], string fs_modalita);string ls_nota, ls_des_prodotto1, ls_des_prodotto2

if not ib_modifiche_consentite then return

ls_des_prodotto1 = f_des_tabella("anag_prodotti","cod_prodotto='" + fs_prodotto_nota[1] +"'" ,"des_prodotto")
ls_nota = "Il prodotto "+fs_prodotto_nota[1]+" "+ls_des_prodotto1 +" quantità "+string(fdd_qta_nota[1], "#####0.0")

if upperbound(fs_prodotto_nota[]) > 1 then
	ls_des_prodotto2 = f_des_tabella("anag_prodotti","cod_prodotto='" + fs_prodotto_nota[2] +"'" ,"des_prodotto")
	ls_des_prodotto2 = " "+fs_prodotto_nota[2]+" "+ls_des_prodotto2 +" quantità "+string(fdd_qta_nota[2], "#####0.0")
end if

choose case fs_modalita
	case"CV"
		ls_nota += " è stato sostituito dal prodotto "+ls_des_prodotto2
		
	case "Z"
		ls_nota += " è stato eliminato dalla composizione"
		
	case "CI"
		ls_nota += " è stato aggiunto alla composizione"
		
end choose

if len(is_nota) > 0 then is_nota += "~r~n"

is_nota += ls_nota

return
end subroutine

public function integer wf_azione (string fs_modalita);//fs_modalita
//"CV"			modalità crea variante (scegliere prodotto e relativa quantità)					pop-up menu
//"CI"				modalità crea integrazione (scegliere prodotto e relativa quantità)				pop-up menu
//"ZV"				modalità azzera variante (stesso prodotto che diventa variante e qtà=0)		pop-up menu
//"NV"			modalità normale ma su variante
//"NI"				modalita normale ma su integrazione

treeviewitem				ltvi_campo
long							ll_ret
string							ls_errore

if il_handle>0 then
else
	return -1
end if

if il_handle = 1 and (fs_modalita="NV" or fs_modalita="ZV"or fs_modalita="CV") then
	ls_errore = "Attenzione! Il prodotto finito non può essere associato ad una variante!"
	return -1
end if


//leggo il nodo del tree corrispondente a il_handle
tab_1.tabpage_1.tv_db.GetItem (il_handle, ltvi_campo )

choose case fs_modalita
	case "NI"
		ll_ret = wf_dragdrop_integraz(ltvi_campo, "N", ls_errore)
		
	case "NV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "N", ls_errore)
		
	case "ZV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "Z", ls_errore)
		
	case "CV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "CV", ls_errore)
		
	case "CI"
		//GetItem (handle, ltvi_campo )
		ll_ret = wf_dragdrop_integraz(ltvi_campo, "CI", ls_errore)
		
	case else
		ls_errore = "Attenzione! Azione non tra quelle previste (NI, NV, ZV, CV, CI)!"
		return -1
		
end choose


choose case ll_ret
	case is < 0
		rollback;
		 g_mb.error(ls_errore)
		 return -1
		 
	case 1
		if  (fs_modalita="NV" or fs_modalita="ZV" or fs_modalita="CV")then
			tab_1.tabpage_3.dw_varianti_dettaglio.Change_DW_Current( )
			this.triggerevent("pc_retrieve")
		end if
		//solo se ll_ret è 1 allora fai commit
		commit;
		
		ll_ret = wf_inizio()
		
	case else
		//probabilmente hai risposto no a qualche richiesta di conferma
		//se c'è un messaggio comunque mostralo all'utente
		rollback;
		if ls_errore<>"" and not isnull(ls_errore) then g_mb.show(ls_errore)
		
		return 0
		
end choose


end function

public function integer wf_dragdrop_variante (treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore);string					ls_cod_formula_quan_utilizzo, ls_cod_prodotto_figlio, ls_cod_misura, ls_flag_materia_prima, ls_flag_escludibile, & 
						ls_cod_prodotto_padre, ls_messaggio, ls_errore, ls_des_estesa, ls_formula_tempo, ls_cod_prodotto_variante, ls_test, &
						ls_flag_ramo_descrittivo, ls_cod_prodotto, ls_cod_figlio, ls_cod_versione_padre, ls_prodotto_nota[], &
						ls_cod_versione_variante_padre, ls_cod_versione_variante, ls_rb_integraz_variante, ls_modalita

integer				li_risposta
boolean				lb_insert = true

long					ll_risposta, ll_progressivo, ll_num_sequenza, ld_cont, ll_lead_time,ll_i, ll_handle

dec{4}				ldd_quan_utilizzo, ldd_quan_tecnica, ldd_coef_calcolo, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, ldd_qta_nota[]

s_chiave_distinta	l_chiave_distinta, l_chiave_distinta_2
s_cs_xx_parametri lstr_parametri
any					la_cod_prodotto[]
datawindow			ldw_null
treeviewitem		ltvi_campo_2


//parametro fs_modalita
// "N"		modalita normale (dragdrop): il cod_prodotto e le qta sono lette dalle datawindow esterne (dw_ricerca e dw_varianti_commesse_quantita)
// "Z"
// "CV"
// "CI"

l_chiave_distinta = ftvi_campo.data
tab_1.tabpage_1.dw_ricerca.accepttext()
//-------------------------------------------------
choose case fs_modalita
	case "N"
		//prodotto impostato sulla dw_ricerca e poi fatto dragdrop
		ls_cod_prodotto_figlio = upper(tab_1.tabpage_1.dw_ricerca.getitemstring(tab_1.tabpage_1.dw_ricerca.getrow(), "rs_cod_prodotto"))
		ls_cod_versione_variante = tab_1.tabpage_1.dw_ricerca.getitemstring(1, "cod_versione")
		//le qta le leggerai più sotto
		
	case "Z"
		ls_modalita = "ZV"
		//stesso prodotto del ramo e quantità azzerate
		ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_variante = l_chiave_distinta.cod_versione_figlio
		//le qta le prenderai più sotto, dopo averle poste a ZERO
		
		ls_prodotto_nota[1] = l_chiave_distinta.cod_prodotto_figlio
		ldd_qta_nota[1] = l_chiave_distinta.quan_utilizzo

	case "CV"
		ls_modalita = fs_modalita
		//creazione variante selezionata da anagrafica prodotti e quantità scelte da finestra pop-up
		setnull(ldw_null)
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
		guo_ricerca.uof_get_results( la_cod_prodotto[] )
		
		if upperbound(la_cod_prodotto[]) > 0 then
			ls_cod_prodotto_figlio = string(la_cod_prodotto[1])
		else
			//probabilmente hai fatto annulla dalla ricerca
			return 0
		end if
		wf_cod_versione(ls_cod_prodotto_figlio, ls_cod_versione_variante)
		//ls_cod_versione_variante = "001"

		ls_prodotto_nota[1] = l_chiave_distinta.cod_prodotto_figlio
		ldd_qta_nota[1] = l_chiave_distinta.quan_utilizzo
		ls_prodotto_nota[2] = ls_cod_prodotto_figlio		//variante sostitutiva
		
	case else
		fs_errore = "Valore argomento 'fs_modalita' non previsto (N, Z, CV)"
		return -1
		
end choose

if ls_cod_prodotto_figlio = "" or isnull(ls_cod_prodotto_figlio) then
	fs_errore = "E' necessario specificare il prodotto Variante!"
	return -1
end if
//------------------------------------------------

ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle

do while 1 = ll_i 
	//carico i dati del nodo numero ll_handle nel nodo ltvi_campo_2
	tab_1.tabpage_1.tv_db.GetItem (ll_handle, ltvi_campo_2 )
	l_chiave_distinta_2 = ltvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
		//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
	else
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	end if
	
	if fs_modalita<>"Z" then
		if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
			fs_errore = "Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata"
			return -1
		end if
	end if
	
	//cerca il padre del nodo numero ll_handle
	ll_handle = tab_1.tabpage_1.tv_db.FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice

loop

if isnull(ls_cod_prodotto_figlio) or len(ls_cod_prodotto_figlio) < 1 then
	 fs_errore = "Selezionare un prodotto valido !"
	 return -1
end if
	
if l_chiave_distinta.flag_tipo_record = "I" then
	fs_errore = "Attenzione !!!~r~Non è logico fare la variante su una integrazione. Al più modificare l'integrazione."
	return -1
end if

select		cod_azienda
into		:ls_test
from   	varianti_det_off_ven
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione and
			prog_riga_off_ven = :il_prog_riga and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo variante esistente: "+sqlca.sqlerrtext
	return -1
end if

lb_insert = true

if sqlca.sqlcode=0 and not isnull(ls_test) and ls_test<>"" then
	//variante già esistente
	if fs_modalita<>"CV" then
		//dai messaggio che già esiste
		fs_errore = "Attenzione! Esiste già una variante dettaglio ordine vendita per questo prodotto. "+&
						"Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione."
		return 0
	else
		//modalita "CV"
		//variante già esistente !!!!!!! attivati per la cancellazione ed il re-inserimento
		//###################################################
		lb_insert = false
	end if
	
end if

select cod_misura,
		 quan_tecnica,
		 quan_utilizzo,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 coef_calcolo,
		 formula_tempo,	
		 des_estesa,
		 flag_materia_prima,
		 flag_ramo_descrittivo,
		 lead_time
into     :ls_cod_misura,
		 :ldd_quan_tecnica,
		 :ldd_quan_utilizzo,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ldd_coef_calcolo,
		 :ls_formula_tempo,
		 :ls_des_estesa,
		 :ls_flag_materia_prima,
		 :ls_flag_ramo_descrittivo,
		 :ll_lead_time
from   distinta
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione=:l_chiave_distinta.cod_versione_padre;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in lettura dat da distinta:" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_ramo_descrittivo = "S" then
	fs_errore = "Impossibile fare varianti o integrazioni su rami descrittivi"
	return 0
end if

if isnull(ll_lead_time) or ll_lead_time = 0 then
	select lead_time
	into   :ll_lead_time
	from   anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;
	if sqlca.sqlcode <> 0 then ll_lead_time = 0
	
end if


choose case fs_modalita
	case"N"
		//vuol dire che devo leggere da dw_varianti_commesse_quantita (provengo da dragdrop)
		tab_1.tabpage_1.dw_varianti_commesse_quantita.accepttext()	
		ldd_quan_utilizzo        = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(1, "qta_utilizzo")
		ldd_quan_tecnica         = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(1, "qta_tecnica")
	
		if ldd_quan_utilizzo = 0 then
			if not g_mb.confirm("Variante","La quantità utilizzo della variante è zero. Proseguo?", 2) then return 0
		end if
	
	case "CV"
		//digito le qta da una finestra
		lstr_parametri.parametro_d_1_a[1] =  l_chiave_distinta.quan_utilizzo
		lstr_parametri.parametro_d_1_a[2] =  0		//non so cosa passare, passo ZERO
		window_open_parm(w_menu_varianti_qta, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		if lstr_parametri.parametro_b_1 then
		else
			fs_errore = "Operazione annullata dall'utente!"
			return 0
		end if
		
		ldd_quan_utilizzo = lstr_parametri.parametro_d_1_a[1]
		ldd_quan_tecnica = lstr_parametri.parametro_d_1_a[2]
		
		//per la nota
		ldd_qta_nota[2] = ldd_quan_utilizzo
	
	case "Z"
		//azzeramento quantità
		ldd_quan_utilizzo        = 0
		ldd_quan_tecnica        = 0
		
	case else
		fs_errore = "(2) Valore argomento 'fs_modalita' non previsto (N, Z, CV)"
		return -1
	
end choose

if lb_insert then
	insert into varianti_det_off_ven
			(cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_off_ven,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_versione,
			 cod_prodotto,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 formula_tempo,
			 flag_esclusione,
			 des_estesa,
			 flag_materia_prima,
			 lead_time,
			 cod_versione_variante)
	values
			(:s_cs_xx.cod_azienda,
			 :il_anno_registrazione,
			 :il_num_registrazione,
			 :il_prog_riga,
			 :l_chiave_distinta.cod_prodotto_padre,
			 :l_chiave_distinta.num_sequenza,
			 :l_chiave_distinta.cod_prodotto_figlio,
			 :l_chiave_distinta.cod_versione_figlio,
			 :l_chiave_distinta.cod_versione_padre,
			 :ls_cod_prodotto_figlio, 
			 :ls_cod_misura,
			 :ldd_quan_tecnica,
			 :ldd_quan_utilizzo,
			 :ldd_dim_x,
			 :ldd_dim_y,
			 :ldd_dim_z,
			 :ldd_dim_t,
			 :ldd_coef_calcolo,
			 :ls_formula_tempo,
			 'N',
			 :ls_des_estesa,
			 :ls_flag_materia_prima,
			 :ll_lead_time,
			 :ls_cod_versione_variante);
else
	
	//fai update di prodotto e quantità
	update 	varianti_det_off_ven
	set 		cod_prodotto=:ls_cod_prodotto_figlio,
				quan_tecnica=:ldd_quan_tecnica,
				quan_utilizzo=:ldd_quan_utilizzo
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione and
				prog_riga_off_ven = :il_prog_riga and
				cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
				num_sequenza = :l_chiave_distinta.num_sequenza and
				cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
				cod_versione = :l_chiave_distinta.cod_versione_padre;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in inserimento/Aggiornamento Varianti Ordine:" + sqlca.sqlerrtext
	return -1
end if

//se arrivi fin qui aggiorna nella nota
if ls_modalita="CV" or ls_modalita="Z" then
	wf_crea_nota(ls_prodotto_nota[], ldd_qta_nota[], ls_modalita)
end if


return 1


end function

public function integer wf_trova (string fs_cod_prodotto, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tab_1.tabpage_1.tv_db.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
				tab_1.tabpage_1.tv_db.setfocus()
				tab_1.tabpage_1.tv_db.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

on w_varianti_offerte.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_varianti_offerte.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;long ll_newx, ll_newidth


tab_1.tabpage_2.dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew +c_nodelete + c_nomodify,c_default)
tab_1.tabpage_3.dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)
tab_1.tabpage_4.dw_varianti_offerte.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)
tab_1.tabpage_5.dw_integrazioni_det_off_ven.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)
tab_1.tabpage_3.dw_varianti_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)
tab_1.tabpage_3.dw_varianti_dettaglio.set_dw_options(sqlca,tab_1.tabpage_3.dw_varianti_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)

uo_dw_main=tab_1.tabpage_2.dw_distinta_det

is_cod_prodotto_finito = s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_prodotto")
is_cod_versione = s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_versione")
il_anno_registrazione = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"anno_registrazione")
il_num_registrazione = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"num_registrazione")
il_prog_riga = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"prog_riga_off_ven")

tab_1.tabpage_1.dw_varianti_commesse_quantita.insertrow(0)
iuo_dw_main=tab_1.tabpage_2.dw_distinta_det

this.height = w_cs_xx_mdi.mdi_1.height - 50
ll_newidth = int( w_cs_xx_mdi.mdi_1.width / 2 )
if ll_newidth > this.width then 
	ll_newx = int( ll_newidth / 2 )
	this.width = ll_newidth
else
	ll_newx = this.x
end if
move(ll_newx, 0)


wf_inizio()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.tabpage_2.dw_distinta_det,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.tabpage_3.dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event resize;call super::resize;
setredraw(false)

tab_1.tabpage_1.dw_ricerca.x = 30
tab_1.tabpage_1.dw_ricerca.y = 50
tab_1.tabpage_1.dw_ricerca.width = 2405
tab_1.tabpage_1.dw_ricerca.height = 180

tab_1.tabpage_1.dw_azioni.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.dw_azioni.y = 260
tab_1.tabpage_1.dw_azioni.width = 3415
tab_1.tabpage_1.dw_azioni.height = 212

tab_1.tabpage_1.dw_varianti_commesse_quantita.x = tab_1.tabpage_1.dw_ricerca.x + tab_1.tabpage_1.dw_ricerca.width + 50
tab_1.tabpage_1.dw_varianti_commesse_quantita.y = 32
tab_1.tabpage_1.dw_varianti_commesse_quantita.width = 750
tab_1.tabpage_1.dw_varianti_commesse_quantita.height = 212

tab_1.tabpage_1.tv_db.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.tv_db.y = 488
tab_1.tabpage_1.tv_db.width = tab_1.width - tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.width - 25
tab_1.tabpage_1.tv_db.height = tab_1.height - tab_1.tabpage_1.dw_ricerca.height - tab_1.tabpage_1.dw_azioni.height - 250

tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.x = tab_1.tabpage_1.tv_db.x + tab_1.tabpage_1.tv_db.width + 25
tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.y = 488
tab_1.tabpage_1.dw_det_off_ven_det_conf_variabili.height = tab_1.tabpage_1.tv_db.height


tab_1.tabpage_2.dw_distinta_det.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_2.dw_distinta_det.y = 50
tab_1.tabpage_2.dw_distinta_det.width = 2587
tab_1.tabpage_2.dw_distinta_det.height = 1432

tab_1.tabpage_3.dw_distinta_gruppi_varianti.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.dw_distinta_gruppi_varianti.y = 100
tab_1.tabpage_3.dw_distinta_gruppi_varianti.width = 1925
tab_1.tabpage_3.dw_distinta_gruppi_varianti.height = 632

tab_1.tabpage_3.dw_varianti_lista.x = 1970
tab_1.tabpage_3.dw_varianti_lista.y = tab_1.tabpage_3.dw_distinta_gruppi_varianti.y
tab_1.tabpage_3.dw_varianti_lista.width = 1138
tab_1.tabpage_3.dw_varianti_lista.height = 632

tab_1.tabpage_3.cb_crea.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.cb_crea.y = 692
tab_1.tabpage_3.cb_crea.width = 361
tab_1.tabpage_3.cb_crea.height = 80

tab_1.tabpage_3.dw_varianti_dettaglio.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.dw_varianti_dettaglio.y = 788
tab_1.tabpage_3.dw_varianti_dettaglio.width = 2537
tab_1.tabpage_3.dw_varianti_dettaglio.height = 1020

tab_1.tabpage_4.dw_varianti_offerte.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_4.dw_varianti_offerte.y = 50
tab_1.tabpage_4.dw_varianti_offerte.width = 2766
tab_1.tabpage_4.dw_varianti_offerte.height = 1476


tab_1.tabpage_5.dw_integrazioni_det_off_ven.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_5.dw_integrazioni_det_off_ven.y = 50
tab_1.tabpage_5.dw_integrazioni_det_off_ven.width = 2400
tab_1.tabpage_5.dw_integrazioni_det_off_ven.height = 1316

setredraw(true)



end event

type tab_1 from tab within w_varianti_offerte
event create ( )
event destroy ( )
integer x = 9
integer y = 12
integer width = 3259
integer height = 2280
integer taborder = 90
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

event selectionchanged;choose case  newindex
	case 4
		iuo_dw_main = tab_1.tabpage_4.dw_varianti_offerte
	case 5
		iuo_dw_main=tab_1.tabpage_5.dw_integrazioni_det_off_ven	
end choose
end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3223
integer height = 2156
long backcolor = 12632256
string text = "Struttura"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_det_off_ven_det_conf_variabili dw_det_off_ven_det_conf_variabili
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
tv_db tv_db
end type

on tabpage_1.create
this.dw_det_off_ven_det_conf_variabili=create dw_det_off_ven_det_conf_variabili
this.dw_varianti_commesse_quantita=create dw_varianti_commesse_quantita
this.dw_azioni=create dw_azioni
this.dw_ricerca=create dw_ricerca
this.tv_db=create tv_db
this.Control[]={this.dw_det_off_ven_det_conf_variabili,&
this.dw_varianti_commesse_quantita,&
this.dw_azioni,&
this.dw_ricerca,&
this.tv_db}
end on

on tabpage_1.destroy
destroy(this.dw_det_off_ven_det_conf_variabili)
destroy(this.dw_varianti_commesse_quantita)
destroy(this.dw_azioni)
destroy(this.dw_ricerca)
destroy(this.tv_db)
end on

type dw_det_off_ven_det_conf_variabili from datawindow within tabpage_1
integer x = 1902
integer y = 472
integer width = 1307
integer height = 1620
integer taborder = 160
string title = "none"
string dataobject = "d_det_off_ven_det_conf_variabili"
boolean border = false
boolean livescroll = true
end type

type dw_varianti_commesse_quantita from datawindow within tabpage_1
integer x = 2450
integer y = 12
integer width = 750
integer height = 216
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_varianti_commesse_quantita"
boolean border = false
boolean livescroll = true
end type

type dw_azioni from u_dw_search within tabpage_1
event ue_aggiorna_distinta ( )
integer x = 27
integer y = 212
integer width = 2971
integer height = 200
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_azioni_varianti"
boolean border = false
end type

event ue_aggiorna_distinta();wf_inizio()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_elimina_var"
		wf_elimina_varianti()
		
	case "b_elimina_integraz"
		wf_elimina_integrazioni()
		
	case "b_prezzo_variante"
		wf_prezzo()
		
	case "b_stampa"
		wf_stampa()
		
	case "b_comprimi"
		long ll_tvi
		
		dw_azioni.accepttext()
		tv_db.setredraw( false)
		ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
		wf_comprimi(ll_tvi)
		tv_db.setredraw( true)

	case "b_espandi"
		long ll_handle, ll_livelli
		
		dw_azioni.accepttext()
		ll_livelli = dw_azioni.getitemnumber(dw_azioni.getrow(),"flag_livello")
		il_cicli = 0
		
		tv_db.setredraw( false)
		
		il_handle = tv_db.finditem(roottreeitem!,0)
		wf_comprimi(il_handle)
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			wf_espandi(il_handle, ll_livelli, il_cicli)
		end if
		
		tv_db.setredraw( true)		
end choose



end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_visione_dati"
		
		postevent("ue_aggiorna_distinta")
		
end choose
end event

type dw_ricerca from u_dw_search within tabpage_1
event ue_key pbm_dwnkey
integer x = 27
integer y = 12
integer width = 2377
integer height = 200
integer taborder = 100
string dragicon = "H:\CS_XX_50\cs_sep\Cs_sep.ico"
string dataobject = "d_varianti_commessa_ricerca"
boolean border = false
end type

event constructor;call super::constructor;this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "\11.5\arrow_left_ico.ico"
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		this.setcolumn("cod_versione")
		
	case "b_trova"
		string ls_cod_prodotto
		
		ls_cod_prodotto = dw_ricerca.getitemstring( 1, "rs_cod_prodotto")
		
		if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
			g_mb.messagebox( "SEP", "Attenzione:selezionare un prodotto per eseguire la ricerca!")
			return -1
		end if
		
		if isnull(il_handle) or il_handle = 0 then
			il_handle = tv_db.finditem(roottreeitem!,0)
		end if
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			if wf_trova(ls_cod_prodotto,il_handle) = 100 then
				g_mb.messagebox("Menu Principale","Prodotto non trovato",information!)
				tab_1.tabpage_1.tv_db.setfocus()
			end if
		end if		
		
	case else
		drag(Begin!)
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_null
long ll_test


setnull(ls_null)
// per default la versione del figlio viene proposta uguale
// alla versione del padre
choose case dwo.name
		
	case "rs_cod_prodotto"
		
		if not isnull(data) or data = "" then
			
			f_PO_LoadDDDW_DW( this, &
			                  "cod_versione", &
									sqlca, &
						         "distinta", &
									" cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  			" cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")						  
									  
			dw_ricerca.setitem(dw_ricerca.getrow(),"cod_versione",is_cod_versione)
			
		else
			
			dw_ricerca.setitem(dw_ricerca.getrow(),"cod_versione",ls_null)
			
		end if
		
end choose


end event

type tv_db from treeview within tabpage_1
integer x = 27
integer y = 472
integer width = 1865
integer height = 1620
integer taborder = 150
string dragicon = "Exclamation!"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event clicked;if handle<>0 then
	il_handle = handle
	tab_1.tabpage_2.dw_distinta_det.Change_DW_Current( )
	w_varianti_offerte.triggerevent("pc_retrieve")
	tab_1.tabpage_3.dw_distinta_gruppi_varianti.Change_DW_Current( )
	w_varianti_offerte.triggerevent("pc_retrieve")
	tab_1.tabpage_4.dw_varianti_offerte.Change_DW_Current( )
	w_varianti_offerte.triggerevent("pc_retrieve")
	tab_1.tabpage_5.dw_integrazioni_det_off_ven.Change_DW_Current( )
	w_varianti_offerte.triggerevent("pc_retrieve")
end if
end event

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")								//	1
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")							//	2
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")								//	3
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")					//	4
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")						//	5
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")					//	6
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")				//	7
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")					//	8
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")				//	9
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")			//	10
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante_vincol.bmp")				//	11

end event

event dragdrop;string 	ls_cod_formula_quan_utilizzo,ls_cod_prodotto_figlio,ls_cod_misura,ls_flag_materia_prima,ls_flag_escludibile, & 
		 	ls_cod_prodotto_padre,ls_messaggio,ls_errore,ls_des_estesa,ls_formula_tempo,ls_cod_prodotto_variante,ls_test, &
		 	ls_cod_prodotto, ls_cod_figlio,ls_flag_ramo_descrittivo
integer 	li_risposta
long 		ll_risposta, ll_progressivo, ll_num_sequenza, lL_cont, ll_i, ll_handle, ll_lead_time
dec{4} 	ldd_quan_utilizzo,ldd_quan_tecnica,ldd_coef_calcolo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t, &
       		ldd_quan_utilizzo_em,ldd_quan_tecnica_em
				 
s_chiave_distinta 	l_chiave_distinta,l_chiave_distinta_2
treeviewitem 		tvi_campo, tvi_campo_2



if il_handle>0 then
else
	return
end if


if tab_1.tabpage_1.dw_azioni.getitemstring(1,"flag_integraz_variante") = "I" then

	wf_azione("NI")
	
	GetItem (handle, tvi_campo )
	GetItem (il_handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
	
	
elseif tab_1.tabpage_1.dw_azioni.getitemstring(1,"flag_integraz_variante") = "V" then
	
	if handle = 1 then
		g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
		return
	end if
	
	wf_azione("NV")


end if

//commit;
	
//ll_risposta=wf_inizio()	


end event

event dragwithin;TreeViewItem		ltvi_Over

If GetItem(handle, ltvi_Over) = -1 Then
	SetDropHighlight(0)

	Return
End If


SetDropHighlight(handle)

il_handle = handle
end event

event rightclicked;treeviewitem			ltv_item
s_chiave_distinta		l_chiave_distinta
string						ls_tipo_nodo, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione_padre, ls_cod_versione_figlio, ls_titolo
boolean					lb_abilita_varianti, lb_abilita_integrazioni

if handle < 1 then return 

tv_db.getitem(handle, ltv_item)
il_handle = handle
l_chiave_distinta = ltv_item.data

ls_tipo_nodo = l_chiave_distinta.flag_tipo_record

lb_abilita_integrazioni = true

//se si tratta di un ramo integrazione non abilito le voci di menu relative alle varianti
if ls_tipo_nodo = "I" then
	lb_abilita_varianti = false
else
	lb_abilita_varianti = true
end if

ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio

m_tree_varianti_offerte lm_menu
lm_menu = create m_tree_varianti_offerte

//imposta immagini per le voci di menu
lm_menu.m_azzeraquantità.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "variante_azzera.bmp"
lm_menu.m_creavariante.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp"
lm_menu.m_creaintegrazione.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp"
//------------------------------------------

if isnull(ls_cod_prodotto_padre) then ls_cod_prodotto_padre=""
if isnull(ls_cod_prodotto_figlio) then ls_cod_prodotto_figlio=""
if isnull(ls_cod_versione_padre) then ls_cod_versione_padre=""
if isnull(ls_cod_versione_figlio) then ls_cod_versione_figlio=""

ls_titolo = "Padre: "+ls_cod_prodotto_padre+" - Vers.:"+ls_cod_versione_padre
lm_menu.m_padre.text = ls_titolo

ls_titolo = "Figlio: "+ls_cod_prodotto_figlio+" - Vers.:"+ls_cod_versione_figlio
lm_menu.m_figlio.text = ls_titolo

lm_menu.m_azzeraquantità.enabled = lb_abilita_varianti
lm_menu.m_creavariante.enabled = lb_abilita_varianti
lm_menu.m_creaintegrazione.enabled = lb_abilita_integrazioni

pcca.window_current  = w_varianti_offerte //parent

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3223
integer height = 2156
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_distinta_det dw_distinta_det
end type

on tabpage_2.create
this.dw_distinta_det=create dw_distinta_det
this.Control[]={this.dw_distinta_det}
end on

on tabpage_2.destroy
destroy(this.dw_distinta_det)
end on

type dw_distinta_det from uo_cs_xx_dw within tabpage_2
integer x = 119
integer y = 12
integer width = 2633
integer height = 1428
integer taborder = 60
string dataobject = "d_varianti_offerte_distinta_det"
boolean border = false
end type

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	ib_proteggi_chiavi=false
	
//   cb_ricerca_figlio_dist.enabled=true
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_sequenza
	string ls_cod_prodotto_padre
	ib_proteggi_chiavi=false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo

	tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
//   cb_ricerca_figlio_dist.enabled=true
	
	if l_chiave_distinta.cod_prodotto_figlio="" then
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	end if
	
	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
   triggerevent("itemchanged")

   select max(distinta.num_sequenza)
   into   :ll_sequenza
   from   distinta
   where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
          distinta.cod_prodotto_padre = :ls_cod_prodotto_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
   ll_sequenza = ll_sequenza + 10
   setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
   setcolumn("cod_prodotto_figlio")
	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))	
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, & 
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	long ll_risposta
	
		ll_risposta=wf_inizio()	
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
		SetItem(l_Idx, "cod_versione", is_cod_versione)	
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;//if i_extendmode then
//	
//   cb_ricerca_figlio_dist.enabled=false	
//	
//end if
end event

type tabpage_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3223
integer height = 2156
long backcolor = 12632256
string text = "Varianti Disponibili"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
dw_varianti_lista dw_varianti_lista
st_prodotti_varianti st_prodotti_varianti
st_gruppi_varianti st_gruppi_varianti
cb_crea cb_crea
dw_varianti_dettaglio dw_varianti_dettaglio
end type

on tabpage_3.create
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.dw_varianti_lista=create dw_varianti_lista
this.st_prodotti_varianti=create st_prodotti_varianti
this.st_gruppi_varianti=create st_gruppi_varianti
this.cb_crea=create cb_crea
this.dw_varianti_dettaglio=create dw_varianti_dettaglio
this.Control[]={this.dw_distinta_gruppi_varianti,&
this.dw_varianti_lista,&
this.st_prodotti_varianti,&
this.st_gruppi_varianti,&
this.cb_crea,&
this.dw_varianti_dettaglio}
end on

on tabpage_3.destroy
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.dw_varianti_lista)
destroy(this.st_prodotti_varianti)
destroy(this.st_gruppi_varianti)
destroy(this.cb_crea)
destroy(this.dw_varianti_dettaglio)
end on

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
integer x = 5
integer y = 92
integer width = 1915
integer height = 400
integer taborder = 110
string dataobject = "d_distinta_gruppi_varianti"
boolean vscrollbar = true
boolean border = false
end type

event pcd_pickedrow;call super::pcd_pickedrow;dw_varianti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if rowcount()>0 then
	dw_varianti_lista.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
else
	dw_varianti_lista.reset()
	dw_varianti_dettaglio.reset()
end if
end event

type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 1925
integer y = 92
integer width = 1138
integer height = 400
integer taborder = 80
string dataobject = "d_varianti_lista"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_gruppo_variante

ls_cod_gruppo_variante = dw_distinta_gruppi_varianti.getitemstring(dw_distinta_gruppi_varianti.getrow(),"cod_gruppo_variante")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_variante)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type st_prodotti_varianti from statictext within tabpage_3
integer x = 1925
integer y = 12
integer width = 1129
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "PRODOTTI VARIANTI DEL GRUPPO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_gruppi_varianti from statictext within tabpage_3
integer x = 5
integer y = 12
integer width = 1897
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "GRUPPI DI VARIANTI DISPONIBILI"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_crea from commandbutton within tabpage_3
integer x = 2725
integer y = 592
integer width = 297
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Var."
end type

event clicked;//string ls_cod_misura,ls_cod_prodotto_variante,ls_test,ls_des_estesa,ls_formula_tempo,ls_flag_materia_prima, &
//       ls_flag_ramo_descrittivo
//
//long ll_lead_time
//
//decimal ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
//
//s_chiave_distinta l_chiave_distinta
//
//treeviewitem tvi_campo
//
//
//if dw_distinta_gruppi_varianti.rowcount() <= 0 then
//	g_mb.messagebox("Sep","Non esistono varianti preconfigurate per questo prodotto.",information!)
//	return
//end if
//
//if il_handle = 1 then
//	g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
//	return
//end if
//
//tv_db.GetItem ( il_handle, tvi_campo )
//l_chiave_distinta = tvi_campo.data
//
//ls_cod_misura = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_misura")
//ldd_quan_tecnica = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_tecnica")
//ldd_quan_utilizzo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_utilizzo") 
//ldd_dim_x = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_x")
//ldd_dim_y = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_y")
//ldd_dim_z = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_z")
//ldd_dim_t = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_t")
//ldd_coef_calcolo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"coef_calcolo")
//ls_cod_prodotto_variante = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_prodotto")
//ls_des_estesa = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"des_estesa")
//ls_formula_tempo = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"formula_tempo")
//ls_flag_materia_prima = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"flag_materia_prima")
//
//select cod_azienda
//into   :ls_test
//from   varianti_det_off_ven
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_registrazione=:il_anno_registrazione
//and    num_registrazione=:il_num_registrazione
//and    prog_riga_off_ven=:il_prog_riga
//and    cod_prodotto_padre =:l_chiave_distinta.cod_prodotto_padre
//and    num_sequenza=:l_chiave_distinta.num_sequenza
//and    cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio;
//
//if sqlca.sqlcode =0 then
//	g_mb.messagebox("Sep","Attenzione! Esiste già una variante per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
//	return
//end if
//
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,information!)
//	return
//end if
//
//select lead_time, 
//       flag_ramo_descrittivo
//into   :ll_lead_time, 
//       :ls_flag_ramo_descrittivo
//from   distinta
//where  cod_azienda         = :s_cs_xx.cod_azienda and
//       cod_prodotto_padre  = :l_chiave_distinta.cod_prodotto_padre and
//       cod_versione        = :l_chiave_distinta.cod_versione_padre and
//		 num_sequenza        = :l_chiave_distinta.num_sequenza and
//		 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
//		 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
//if sqlca.sqlcode <> 0 then
//	g_mb.messagebox("Sep","Errore in ricerca ramo di distinta.",information!)
//	return
//end if
//
//if ls_flag_ramo_descrittivo = "S" then
//	g_mb.messagebox("sep","Attenzione!!! Non è possibile fare una variante su un ramo descrittivo.")
//	return
//end if
//
//
//insert into varianti_det_off_ven
//(cod_azienda,
// anno_registrazione,
// num_registrazione,
// prog_riga_off_ven,
// cod_prodotto_padre,
// cod_versione,
// num_sequenza,
// cod_prodotto_figlio,
// cod_versione_figlio,
// cod_prodotto,
// cod_misura,
// quan_tecnica,
// quan_utilizzo,
// dim_x,
// dim_y,
// dim_z,
// dim_t,
// coef_calcolo,
// formula_tempo,
// flag_esclusione,
// des_estesa,
// flag_materia_prima,
// cod_versione_variante,
// lead_time)
//values
//(:s_cs_xx.cod_azienda,
// :il_anno_registrazione,
// :il_num_registrazione,
// :il_prog_riga,
// :l_chiave_distinta.cod_prodotto_padre,
// :l_chiave_distinta.cod_versione_padre,
// :l_chiave_distinta.num_sequenza,
// :l_chiave_distinta.cod_prodotto_figlio,
// :l_chiave_distinta.cod_versione_figlio,
// :ls_cod_prodotto_variante, 
// :ls_cod_misura,
// :ldd_quan_tecnica,
// :ldd_quan_utilizzo,
// :ldd_dim_x,
// :ldd_dim_y,
// :ldd_dim_z,
// :ldd_dim_t,
// :ldd_coef_calcolo,
// :ls_formula_tempo,
// 'N',
// :ls_des_estesa,
// :ls_flag_materia_prima,
// :l_chiave_distinta.cod_versione_figlio,
// :ll_lead_time);
//
//if sqlca.sqlcode < 0 then
//	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti :" + sqlca.sqlerrtext ,stopsign!)
//	return
//end if
//
//dw_varianti_offerte.Change_DW_Current( )
//parent.triggerevent("pc_retrieve")
//
//wf_inizio()
//
//g_mb.messagebox("Sep","Creazione variante avvenuta con successo",information!)
end event

type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 27
integer y = 592
integer width = 2720
integer height = 1060
integer taborder = 110
string dataobject = "d_varianti_dettaglio"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre,l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type tabpage_4 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3223
integer height = 2156
long backcolor = 12632256
string text = "Varianti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_varianti_offerte dw_varianti_offerte
end type

on tabpage_4.create
this.dw_varianti_offerte=create dw_varianti_offerte
this.Control[]={this.dw_varianti_offerte}
end on

on tabpage_4.destroy
destroy(this.dw_varianti_offerte)
end on

type dw_varianti_offerte from uo_cs_xx_dw within tabpage_4
integer x = 27
integer y = 92
integer width = 2400
integer height = 1428
integer taborder = 70
string dataobject = "d_varianti_offerte"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if


choose case dwo.name
	case "b_ric_prod_variante"
		guo_ricerca.uof_ricerca_prodotto(tab_1.tabpage_4.dw_varianti_offerte,"cod_prodotto")
		
	
end choose

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile
	long ll_num_sequenza
	choose case i_colname
	
	case "cod_prodotto"

		if len(i_coltext) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if   
	
	// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
	
		long ll_handle
		string  ls_cod_prodotto, ls_cod_figlio, ls_descrizione
		treeviewitem  tvi_campo_2
		s_chiave_distinta l_chiave_distinta_2
		//numero del nodo da cui parto il_handle
		ll_handle = il_handle

		if not isnull(data) and (len(data) > 0) then
			select cod_prodotto
			into :ls_descrizione
			from anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto= :i_coltext;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
				return 1
			end if
		end if

		do while true
			//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
			tab_1.tabpage_1.tv_db.getItem (ll_handle, tvi_campo_2 )
			l_chiave_distinta_2 = tvi_campo_2.data
			if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
				//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
			else
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
			end if
			
			if upper(ls_cod_prodotto) = upper(i_coltext) then
				g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nCorreggere il dato.", stopsign!)
				return 1
			end if
			//cerca il padre del nodo numero ll_handle
			ll_handle = tab_1.tabpage_1.tv_db.findItem ( ParentTreeItem!	,  ll_handle)
			if ll_handle = -1 then exit   // allora sono alla radice
		
		loop

//*********fine*** 	
		
	
	
	
	
	
	
	
	
		SELECT cod_misura_mag  
      INTO   :ls_cod_misura  
      FROM   anag_prodotti  
      WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	   AND   cod_prodotto = :i_coltext;

      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
         setitem(getrow(), "cod_misura", ls_cod_misura)
      end if

	case "flag_esclusione"		
		if data = "S" then
			ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
			ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
			ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
			ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")

			select flag_escludibile
			 into :ls_flag_escludibile
			 from distinta
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione = :ls_cod_versione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Varianti Offerta di Vendita", "Errore durante l'Estrazione Flag Escludibile!")
				return 2
			end if
			
			if ls_flag_escludibile = "N" then
				g_mb.messagebox("Varianti Offerta di Vendita", "Il dettaglio Distinta non è escludibile!")
				return 2
			end if
		end if
	end choose
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    il_anno_registrazione, &
						  il_num_registrazione, &
						  il_prog_riga, &
						  l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

type tabpage_5 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 108
integer width = 3223
integer height = 2156
long backcolor = 12632256
string text = "Integrazioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_integrazioni_det_off_ven dw_integrazioni_det_off_ven
end type

on tabpage_5.create
this.dw_integrazioni_det_off_ven=create dw_integrazioni_det_off_ven
this.Control[]={this.dw_integrazioni_det_off_ven}
end on

on tabpage_5.destroy
destroy(this.dw_integrazioni_det_off_ven)
end on

type dw_integrazioni_det_off_ven from uo_cs_xx_dw within tabpage_5
integer x = 5
integer y = 32
integer width = 2400
integer height = 1316
integer taborder = 100
string dataobject = "d_integrazioni_det_off_ven"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione,&
	ls_descrizione, ls_flag_escludibile
	long ll_num_sequenza

	choose case i_colname
	
	case "cod_prodotto_figlio"

		if len(i_coltext) < 1 then
			PCCA.Error = c_ValFailed
			return
		end if   
			
		SELECT 	cod_misura_mag  
		INTO   	:ls_cod_misura  
		FROM   	anag_prodotti  
		WHERE  	cod_azienda = :s_cs_xx.cod_azienda  and
					cod_prodotto = :i_coltext;
	
		if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
			setitem(getrow(), "cod_misura", ls_cod_misura)
		end if
	
	case "flag_esclusione"		
		if data = "S" then
			ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
			ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
			ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
			ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")

			select flag_escludibile
			 into :ls_flag_escludibile
			 from distinta
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione = :ls_cod_versione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Varianti Ordine di Vendita", "Errore durante l'Estrazione Flag Escludibile!")
				return 2
			end if
			
			if ls_flag_escludibile = "N" then
				g_mb.messagebox("Varianti Ordine di Vendita", "Il dettaglio Distinta non è escludibile!")
				return 2
			end if
		end if
	end choose

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_integrazioni_det_off_ven.object.b_ricerca_padre.enabled=true
	dw_integrazioni_det_off_ven.object.b_ricerca_figlio.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_integrazioni_det_off_ven.object.b_ricerca_padre.enabled=true
	dw_integrazioni_det_off_ven.object.b_ricerca_figlio.enabled=true
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   il_anno_registrazione,&
						 il_num_registrazione,&
						 il_prog_riga,&
						 l_chiave_distinta.num_sequenza)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_integrazioni_det_off_ven.object.b_ricerca_padre.enabled=false
	dw_integrazioni_det_off_ven.object.b_ricerca_figlio.enabled=false
end if
end event

event buttonclicked;call super::buttonclicked;
if row>0 then
else
	return
end if

choose case dwo.name
	case "b_ric_prod_var"
		guo_ricerca.uof_ricerca_prodotto(tab_1.tabpage_5.dw_integrazioni_det_off_ven,"cod_prodotto_figlio")
		
end choose
end event

