﻿$PBExportHeader$w_varianti_ordini.srw
$PBExportComments$Window varianti ordini
forward
global type w_varianti_ordini from w_cs_xx_principale
end type
type st_commessa from statictext within w_varianti_ordini
end type
type cb_gen_fasi from commandbutton within w_varianti_ordini
end type
type cb_chiudi from commandbutton within w_varianti_ordini
end type
type st_1 from statictext within w_varianti_ordini
end type
type st_tempo from statictext within w_varianti_ordini
end type
type cb_verifica from commandbutton within w_varianti_ordini
end type
type cb_inserisci from commandbutton within w_varianti_ordini
end type
type lb_elenco_campi from listbox within w_varianti_ordini
end type
type tab_1 from tab within w_varianti_ordini
end type
type tabpage_1 from userobject within tab_1
end type
type tv_db from treeview within tabpage_1
end type
type dw_varianti_commesse_quantita from datawindow within tabpage_1
end type
type dw_azioni from u_dw_search within tabpage_1
end type
type dw_ricerca from u_dw_search within tabpage_1
end type
type tabpage_1 from userobject within tab_1
tv_db tv_db
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
end type
type tabpage_2 from userobject within tab_1
end type
type dw_distinta_det from uo_cs_xx_dw within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_distinta_det dw_distinta_det
end type
type tabpage_3 from userobject within tab_1
end type
type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
end type
type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
end type
type cb_crea from commandbutton within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_varianti_dettaglio dw_varianti_dettaglio
dw_varianti_lista dw_varianti_lista
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
cb_crea cb_crea
end type
type tabpage_4 from userobject within tab_1
end type
type dw_reparti_variante from datawindow within tabpage_4
end type
type cb_rimuovi_reparto from commandbutton within tabpage_4
end type
type cb_aggiungi_reparto from commandbutton within tabpage_4
end type
type dw_lista_reparti from datawindow within tabpage_4
end type
type dw_varianti_ordini from uo_cs_xx_dw within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_reparti_variante dw_reparti_variante
cb_rimuovi_reparto cb_rimuovi_reparto
cb_aggiungi_reparto cb_aggiungi_reparto
dw_lista_reparti dw_lista_reparti
dw_varianti_ordini dw_varianti_ordini
end type
type tabpage_5 from userobject within tab_1
end type
type dw_integrazioni_det_ord_ven from uo_cs_xx_dw within tabpage_5
end type
type tabpage_5 from userobject within tab_1
dw_integrazioni_det_ord_ven dw_integrazioni_det_ord_ven
end type
type tab_1 from tab within w_varianti_ordini
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_varianti_ordini from w_cs_xx_principale
integer width = 3589
integer height = 2280
string title = "Varianti Ordine"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
event pc_azzera_qta_variante ( )
event pc_crea_variante ( )
event pc_crea_integrazione ( )
st_commessa st_commessa
cb_gen_fasi cb_gen_fasi
cb_chiudi cb_chiudi
st_1 st_1
st_tempo st_tempo
cb_verifica cb_verifica
cb_inserisci cb_inserisci
lb_elenco_campi lb_elenco_campi
tab_1 tab_1
end type
global w_varianti_ordini w_varianti_ordini

type variables
long il_handle
string is_cod_prodotto_finito,is_cod_versione
long  il_anno_registrazione,il_num_registrazione,il_prog_riga, il_cicli
boolean ib_attiva_gestione_nota=false, ib_nota_dettaglio=false, ib_modifiche_consentite=false, ib_esiste_commessa=false
string is_nota, is_nota_precedente
end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public subroutine wf_elimina_integrazioni ()
public subroutine wf_prezzo ()
public subroutine wf_stampa ()
public subroutine wf_elimina_varianti ()
public function integer wf_dragdrop_variante (treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore)
public function integer wf_azione (string fs_modalita)
public function integer wf_dragdrop_integraz (ref treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore)
public subroutine wf_cod_versione (string fs_cod_prodotto, ref string fs_cod_versione)
public subroutine wf_crea_nota (string fs_prodotto_nota[], decimal fdd_qta_nota[], string fs_modalita)
public function boolean wf_controlla_stampati ()
public function integer wf_filtra_reparti (string fs_cod_deposito)
public function integer wf_espandi (long al_handle, long al_livello, long al_cicli)
public function integer wf_comprimi (long al_handle)
public subroutine wf_esiste_commessa ()
public function integer wf_ricalcola_quan_utlizzo ()
public subroutine wf_protezione_dw_reparti (boolean ab_proteggi)
public function integer wf_salva_reparti_date_pronto (ref string as_errore)
public function integer wf_trova (string fs_cod_prodotto, long al_handle)
end prototypes

event pc_azzera_qta_variante();

wf_azione("ZV")

//
//long					ll_handle, ll_ret
//treeviewitem		ltvi_campo
//string					ls_errore, ls_modalita
//
//
//if il_handle>0 then
//else
//	return
//end if
//ll_handle = il_handle
//
//if ll_handle = 1 then
//	g_mb.error("Attenzione! Il prodotto finito non può essere associato ad una variante!")
//	return
//end if
//
//tv_db.GetItem (ll_handle, ltvi_campo )
//
//ls_modalita="Z"			//modalità azzera quantita (stesso prodotto che diventa variante ma con qta utilizzo e tecnica azzerate)
//ll_ret = wf_dragdrop_variante(ltvi_campo, ls_modalita, ref ls_errore)
//
//choose case ll_ret
//	case is < 0
//		 rollback;
//		 g_mb.error(ls_errore)
//		 return
//		 
//	case 1
//		dw_varianti_dettaglio.Change_DW_Current( )
//		this.triggerevent("pc_retrieve")
//		//solo in questo caso fai commit e refresh albero
//		commit;
//		ll_ret = wf_inizio()
//		
//	case else
//		//probabilmente hai risposto no a qualche richiesta di conferma
//		//se c'è un messaggio comunque dallo
//		rollback;
//		if ls_errore<>"" and not isnull(ls_errore) then g_mb.show(ls_errore)
//		return
//		
//end choose
end event

event pc_crea_variante();
wf_azione("CV")


//long					ll_handle, ll_ret
//treeviewitem		ltvi_campo
//string					ls_errore, ls_modalita
//
//
//if il_handle>0 then
//else
//	return
//end if
//ll_handle = il_handle
//
//if ll_handle = 1 then
//	g_mb.error("Attenzione! Il prodotto finito non può essere associato ad una variante!")
//	return
//end if
//
//tv_db.GetItem (ll_handle, ltvi_campo )
//
//ls_modalita="CV"			//modalità crea variante (scegliere prodotto e relativa quantità)
//ll_ret = wf_dragdrop_variante(ltvi_campo, ls_modalita, ref ls_errore)
//
//choose case ll_ret
//	case is < 0
//		 rollback;
//		 g_mb.error(ls_errore)
//		 return
//		 
//	case 1
//		dw_varianti_dettaglio.Change_DW_Current( )
//		this.triggerevent("pc_retrieve")
//		//solo in questo caso fai commit e refresh albero
//		commit;
//		ll_ret = wf_inizio()
//		
//	case else
//		//probabilmente hai risposto no a qualche richiesta di conferma
//		//se c'è un messaggio comunque dallo
//		rollback;
//		if ls_errore<>"" and not isnull(ls_errore) then g_mb.show(ls_errore)
//		return
//		
//end choose
end event

event pc_crea_integrazione();
wf_azione("CI")

end event

public function integer wf_inizio ();string ls_errore,ls_des_prodotto,ls_ordinamento
long ll_risposta
treeviewitem tvi_campo
uo_imposta_tv_varianti luo_varianti

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
l_chiave_distinta.cod_versione_padre = is_cod_versione

this.setredraw(false)
tab_1.tabpage_1.tv_db.deleteitem(0)

ls_ordinamento = tab_1.tabpage_1.dw_azioni.getitemstring(tab_1.tabpage_1.dw_azioni.getrow(),"flag_visione_dati")

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;
			
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta=tab_1.tabpage_1.tv_db.insertitemlast(0, tvi_campo)

luo_varianti = CREATE uo_imposta_tv_varianti
luo_varianti.is_tipo_gestione = "varianti_ordini"
luo_varianti.is_cod_versione = is_cod_versione
luo_varianti.il_anno_registrazione = il_anno_registrazione
luo_varianti.il_num_registrazione = il_num_registrazione
luo_varianti.il_prog_riga_registrazione = il_prog_riga
luo_varianti.uof_imposta_tv(ref tab_1.tabpage_1.tv_db, l_chiave_distinta.cod_prodotto_padre, is_cod_versione, 1, 1, ls_ordinamento, ref ls_errore)
destroy luo_varianti

tab_1.tabpage_1.tv_db.expandall(1)
this.setredraw(true)

tab_1.tabpage_1.tv_db.selectitem(1)


return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

tab_1.tabpage_2.dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public subroutine wf_elimina_integrazioni ();string ls_errore

if not wf_controlla_stampati() then
	return
end if

if g_mb.messagebox("Sep", "Confermi l'eliminazione di tutte le integrazioni ?",Question!, YesNo!, 2) = 1 then

	delete from integrazioni_det_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_ord_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		g_mb.messagebox("Sep","Errore in cancellazione integrazioni " + ls_errore, stopsign!)
		return
	end if
	
	commit;
	
	wf_inizio()

end if
end subroutine

public subroutine wf_prezzo ();string ls_cod_valuta, ls_cod_cliente, ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_listino_prodotto, ls_sql, ls_prezzo_vendita, &
       ls_sconto, ls_progressivo
long   ll_i
dec{4} ld_quan_ordine, ld_prezzo_vendita, ld_cambio_ven
datetime ldt_data_consegna, ldt_data_registrazione
uo_condizioni_cliente luo_condizioni_cliente

select cod_valuta, 
       cod_cliente, 
		 cod_agente_1, 
		 cod_agente_2, 
		 cod_tipo_listino_prodotto,
		 data_registrazione,
		 cambio_ven
into   :ls_cod_valuta, 
       :ls_cod_cliente, 
		 :ls_cod_agente_1, 
		 :ls_cod_agente_2, 
		 :ls_cod_tipo_listino_prodotto,
		 :ldt_data_registrazione,
		 :ld_cambio_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione  and
		 num_registrazione = :il_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata ordini di vendita")
	return
end if

select data_consegna,
       quan_ordine,
		 cod_versione
into   :ldt_data_consegna,
       :ld_quan_ordine,
		 :s_cs_xx.listino_db.cod_versione
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione  and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca riga ordine di vendita")
	return
end if

s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = il_anno_registrazione
s_cs_xx.listino_db.num_documento = il_num_registrazione
s_cs_xx.listino_db.prog_riga = il_prog_riga
s_cs_xx.listino_db.quan_prodotto_finito = ld_quan_ordine
s_cs_xx.listino_db.data_riferimento = ldt_data_consegna
s_cs_xx.listino_db.tipo_gestione = "varianti_det_ord_ven"
s_cs_xx.listino_db.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
s_cs_xx.listino_db.cod_valuta = ls_cod_valuta
s_cs_xx.listino_db.cod_cliente = ls_cod_cliente
s_cs_xx.listino_db.data_inizio_val = ldt_data_consegna

luo_condizioni_cliente = CREATE uo_condizioni_cliente 
luo_condizioni_cliente.str_parametri.ldw_oggetto = tab_1.tabpage_4.dw_varianti_ordini
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
luo_condizioni_cliente.str_parametri.cambio_ven = double(ld_cambio_ven)
luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_consegna
luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
luo_condizioni_cliente.str_parametri.cod_prodotto = is_cod_prodotto_finito
luo_condizioni_cliente.str_parametri.dim_1 = 0
luo_condizioni_cliente.str_parametri.dim_2 = 0
luo_condizioni_cliente.str_parametri.quantita = ld_quan_ordine
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

if upperbound(luo_condizioni_cliente.str_output.variazioni) > 0 then
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
else
	ld_prezzo_vendita = 0
end if

ls_progressivo = "prog_riga_ord_ven"
ls_prezzo_vendita = f_double_to_string(ld_prezzo_vendita)
ls_sql =" update det_ord_ven"
ls_sql = ls_sql + " set prezzo_vendita = " + ls_prezzo_vendita
if upperbound(luo_condizioni_cliente.str_output.sconti) > 0 then
	for ll_i = 1 to upperbound(luo_condizioni_cliente.str_output.sconti)
		if ll_i > 10 then exit
		if luo_condizioni_cliente.str_output.sconti[ll_i] = 0 or isnull(luo_condizioni_cliente.str_output.sconti[ll_i]) then 
				ls_sconto = "0"
			else
				ls_sconto = f_double_to_string(luo_condizioni_cliente.str_output.sconti[ll_i])
				if isnull(ls_sconto) then ls_sconto = "0"
			end if
			ls_sql = ls_sql + ", sconto_" + string(ll_i) + " = " + ls_sconto
	next
end if

ls_sql =ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" anno_registrazione = " + string(il_anno_registrazione) + " and " + &
			" num_registrazione = " + string(il_num_registrazione) + " and " + &
			ls_progressivo + " = " + string(il_prog_riga)

execute immediate :ls_sql;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell'aggiornamento del prezzo di vendita. Dettaglio " + sqlca.sqlerrtext, &
				  exclamation!, ok!)
	rollback;
	destroy luo_condizioni_cliente
	return
end if

destroy luo_condizioni_cliente

commit;

end subroutine

public subroutine wf_stampa ();s_cs_xx.parametri.parametro_s_1 = is_cod_prodotto_finito
s_cs_xx.parametri.parametro_s_2 = is_cod_versione
s_cs_xx.parametri.parametro_s_3 = "varianti_ordini"

s_cs_xx.parametri.parametro_i_1 = il_anno_registrazione
s_cs_xx.parametri.parametro_d_1 = il_num_registrazione
s_cs_xx.parametri.parametro_d_2 = il_prog_riga

window_open(w_report_distinta_varianti, -1) 
end subroutine

public subroutine wf_elimina_varianti ();string ls_errore

if not wf_controlla_stampati() then
	return
end if

if g_mb.messagebox("Sep", "Confermi l'eliminazione di tutte le varianti?",Question!, YesNo!, 2) = 1 then
	
	delete from varianti_det_ord_ven_prod
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_ord_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		g_mb.messagebox("Sep","Errore in cancellazione varianti reparti " + ls_errore, stopsign!)
		return
	end if

	delete from varianti_det_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_ord_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		g_mb.messagebox("Sep","Errore in cancellazione varianti " + ls_errore, stopsign!)
		return
	end if
	
	delete from varianti_det_ord_ven_prod
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:il_anno_registrazione
	and   num_registrazione=:il_num_registrazione
	and   prog_riga_ord_ven=:il_prog_riga;
	
	if sqlca.sqlcode <0 then
		ls_errore = sqlca.sqlerrtext
		rollback;
		g_mb.messagebox("Sep","Errore in cancellazione reparti delle varianti " + ls_errore, stopsign!)
		return
	end if
	
	commit;
	
	wf_inizio()

end if
end subroutine

public function integer wf_dragdrop_variante (treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore);string					ls_cod_formula_quan_utilizzo, ls_cod_prodotto_figlio, ls_cod_misura, ls_flag_materia_prima, ls_flag_escludibile, & 
						ls_cod_prodotto_padre, ls_messaggio, ls_errore, ls_des_estesa, ls_formula_tempo, ls_cod_prodotto_variante, ls_test, &
						ls_flag_ramo_descrittivo, ls_cod_prodotto, ls_cod_figlio, ls_cod_versione_padre, ls_prodotto_nota[], &
						ls_cod_versione_variante_padre, ls_cod_versione_variante, ls_rb_integraz_variante, ls_modalita

integer				li_risposta
boolean				lb_insert = true

long					ll_risposta, ll_progressivo, ll_num_sequenza, ld_cont, ll_lead_time,ll_i, ll_handle

dec{4}				ldd_quan_utilizzo, ldd_quan_tecnica, ldd_coef_calcolo, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, ldd_qta_nota[]

s_chiave_distinta	l_chiave_distinta, l_chiave_distinta_2
s_cs_xx_parametri lstr_parametri
any					la_cod_prodotto[]
datawindow			ldw_null
treeviewitem		ltvi_campo_2


//parametro fs_modalita
// "N"		modalita normale (dragdrop): il cod_prodotto e le qta sono lette dalle datawindow esterne (dw_ricerca e dw_varianti_commesse_quantita)
// "Z"
// "CV"
// "CI"

l_chiave_distinta = ftvi_campo.data
tab_1.tabpage_1.dw_ricerca.accepttext()
//-------------------------------------------------
choose case fs_modalita
	case "N"
		//prodotto impostato sulla dw_ricerca e poi fatto dragdrop
		ls_cod_prodotto_figlio = upper(tab_1.tabpage_1.dw_ricerca.getitemstring(tab_1.tabpage_1.dw_ricerca.getrow(), "rs_cod_prodotto"))
		ls_cod_versione_variante = tab_1.tabpage_1.dw_ricerca.getitemstring(1, "cod_versione")
		//le qta le leggerai più sotto
		
	case "Z"
		ls_modalita = "ZV"
		//stesso prodotto del ramo e quantità azzerate
		ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
		ls_cod_versione_variante = l_chiave_distinta.cod_versione_figlio
		//le qta le prenderai più sotto, dopo averle poste a ZERO
		
		ls_prodotto_nota[1] = l_chiave_distinta.cod_prodotto_figlio
		ldd_qta_nota[1] = l_chiave_distinta.quan_utilizzo

	case "CV"
		ls_modalita = fs_modalita
		//creazione variante selezionata da anagrafica prodotti e quantità scelte da finestra pop-up
		setnull(ldw_null)
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
		guo_ricerca.uof_get_results( la_cod_prodotto[] )
		
		if upperbound(la_cod_prodotto[]) > 0 then
			ls_cod_prodotto_figlio = string(la_cod_prodotto[1])
		else
			//probabilmente hai fatto annulla dalla ricerca
			return 0
		end if
		wf_cod_versione(ls_cod_prodotto_figlio, ls_cod_versione_variante)
		//ls_cod_versione_variante = "001"

		ls_prodotto_nota[1] = l_chiave_distinta.cod_prodotto_figlio
		ldd_qta_nota[1] = l_chiave_distinta.quan_utilizzo
		ls_prodotto_nota[2] = ls_cod_prodotto_figlio		//variante sostitutiva
		
	case else
		fs_errore = "Valore argomento 'fs_modalita' non previsto (N, Z, CV)"
		return -1
		
end choose

if ls_cod_prodotto_figlio = "" or isnull(ls_cod_prodotto_figlio) then
	fs_errore = "E' necessario specificare il prodotto Variante!"
	return -1
end if
//------------------------------------------------

ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle

do while 1 = ll_i 
	//carico i dati del nodo numero ll_handle nel nodo ltvi_campo_2
	tab_1.tabpage_1.tv_db.GetItem (ll_handle, ltvi_campo_2 )
	l_chiave_distinta_2 = ltvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
		//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
	else
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	end if
	
	if fs_modalita<>"Z" then
		if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
			fs_errore = "Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata"
			return -1
		end if
	end if
	
	//cerca il padre del nodo numero ll_handle
	ll_handle = tab_1.tabpage_1.tv_db.FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice

loop

if isnull(ls_cod_prodotto_figlio) or len(ls_cod_prodotto_figlio) < 1 then
	 fs_errore = "Selezionare un prodotto valido !"
	 return -1
end if
	
if l_chiave_distinta.flag_tipo_record = "I" then
	fs_errore = "Attenzione !!!~r~Non è logico fare la variante su una integrazione. Al più modificare l'integrazione."
	return -1
end if

select	cod_azienda
into	:ls_test
from   varianti_det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione and
			prog_riga_ord_ven = :il_prog_riga and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo variante esistente: "+sqlca.sqlerrtext
	return -1
end if

lb_insert = true

if sqlca.sqlcode=0 and not isnull(ls_test) and ls_test<>"" then
	//variante già esistente
	if fs_modalita<>"CV" then
		//dai messaggio che già esiste
		fs_errore = "Attenzione! Esiste già una variante dettaglio ordine vendita per questo prodotto. "+&
						"Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione."
		return 0
	else
		//modalita "CV"
		//variante già esistente !!!!!!! attivati per la cancellazione ed il re-inserimento
		//###################################################
		lb_insert = false
	end if
	
end if

select cod_misura,
		 quan_tecnica,
		 quan_utilizzo,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 coef_calcolo,
		 formula_tempo,	
		 des_estesa,
		 flag_materia_prima,
		 flag_ramo_descrittivo,
		 lead_time
into     :ls_cod_misura,
		 :ldd_quan_tecnica,
		 :ldd_quan_utilizzo,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ldd_coef_calcolo,
		 :ls_formula_tempo,
		 :ls_des_estesa,
		 :ls_flag_materia_prima,
		 :ls_flag_ramo_descrittivo,
		 :ll_lead_time
from   distinta
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione=:l_chiave_distinta.cod_versione_padre;

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in lettura dat da distinta:" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_ramo_descrittivo = "S" then
	fs_errore = "Impossibile fare varianti o integrazioni su rami descrittivi"
	return 0
end if

if isnull(ll_lead_time) or ll_lead_time = 0 then
	select lead_time
	into   :ll_lead_time
	from   anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :l_chiave_distinta.cod_prodotto_figlio;
	if sqlca.sqlcode <> 0 then ll_lead_time = 0
	
end if


choose case fs_modalita
	case"N"
		//vuol dire che devo leggere da dw_varianti_commesse_quantita (provengo da dragdrop)
		tab_1.tabpage_1.dw_varianti_commesse_quantita.accepttext()	
		ldd_quan_utilizzo        = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(1, "qta_utilizzo")
		ldd_quan_tecnica         = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(1, "qta_tecnica")
	
		if ldd_quan_utilizzo = 0 then
			if not g_mb.confirm("Variante","La quantità utilizzo della variante è zero. Proseguo?", 2) then return 0
		end if
	
	case "CV"
		//digito le qta da una finestra
		lstr_parametri.parametro_d_1_a[1] =  l_chiave_distinta.quan_utilizzo
		lstr_parametri.parametro_d_1_a[2] =  0		//non so cosa passare, passo ZERO
		window_open_parm(w_menu_varianti_qta, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		if lstr_parametri.parametro_b_1 then
		else
			fs_errore = "Operazione annullata dall'utente!"
			return 0
		end if
		
		ldd_quan_utilizzo = lstr_parametri.parametro_d_1_a[1]
		ldd_quan_tecnica = lstr_parametri.parametro_d_1_a[2]
		
		//per la nota
		ldd_qta_nota[2] = ldd_quan_utilizzo
	
	case "Z"
		//azzeramento quantità
		ldd_quan_utilizzo        = 0
		ldd_quan_tecnica        = 0
		
	case else
		fs_errore = "(2) Valore argomento 'fs_modalita' non previsto (N, Z, CV)"
		return -1
	
end choose

if lb_insert then
	insert into varianti_det_ord_ven
			(cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_versione,
			 cod_prodotto,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 formula_tempo,
			 flag_esclusione,
			 des_estesa,
			 flag_materia_prima,
			 lead_time,
			 cod_versione_variante)
	values
			(:s_cs_xx.cod_azienda,
			 :il_anno_registrazione,
			 :il_num_registrazione,
			 :il_prog_riga,
			 :l_chiave_distinta.cod_prodotto_padre,
			 :l_chiave_distinta.num_sequenza,
			 :l_chiave_distinta.cod_prodotto_figlio,
			 :l_chiave_distinta.cod_versione_figlio,
			 :l_chiave_distinta.cod_versione_padre,
			 :ls_cod_prodotto_figlio, 
			 :ls_cod_misura,
			 :ldd_quan_tecnica,
			 :ldd_quan_utilizzo,
			 :ldd_dim_x,
			 :ldd_dim_y,
			 :ldd_dim_z,
			 :ldd_dim_t,
			 :ldd_coef_calcolo,
			 :ls_formula_tempo,
			 'N',
			 :ls_des_estesa,
			 :ls_flag_materia_prima,
			 :ll_lead_time,
			 :ls_cod_versione_variante);
else
	
	//fai update di prodotto e quantità
	update varianti_det_ord_ven
	set 	cod_prodotto=:ls_cod_prodotto_figlio,
			quan_tecnica=:ldd_quan_tecnica,
			quan_utilizzo=:ldd_quan_utilizzo
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione and
				prog_riga_ord_ven = :il_prog_riga and
				cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
				num_sequenza = :l_chiave_distinta.num_sequenza and
				cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
				cod_versione = :l_chiave_distinta.cod_versione_padre;
	
end if

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in inserimento/Aggiornamento Varianti Ordine:" + sqlca.sqlerrtext
	return -1
end if

//se arrivi fin qui aggiorna nella nota
if ls_modalita="CV" or ls_modalita="Z" then
	wf_crea_nota(ls_prodotto_nota[], ldd_qta_nota[], ls_modalita)
end if


return 1


end function

public function integer wf_azione (string fs_modalita);//fs_modalita
//"CV"			modalità crea variante (scegliere prodotto e relativa quantità)					pop-up menu
//"CI"				modalità crea integrazione (scegliere prodotto e relativa quantità)				pop-up menu
//"ZV"				modalità azzera variante (stesso prodotto che diventa variante e qtà=0)		pop-up menu
//"NV"			modalità normale ma su variante
//"NI"				modalita normale ma su integrazione

treeviewitem				ltvi_campo
long							ll_ret
string							ls_errore

if not wf_controlla_stampati() then
	return 0
end if

if il_handle>0 then
else
	return -1
end if

if il_handle = 1 and (fs_modalita="NV" or fs_modalita="ZV"or fs_modalita="CV") then
	ls_errore = "Attenzione! Il prodotto finito non può essere associato ad una variante!"
	return -1
end if


//leggo il nodo del tree corrispondente a il_handle
tab_1.tabpage_1.tv_db.GetItem (il_handle, ltvi_campo )

choose case fs_modalita
	case "NI"
		ll_ret = wf_dragdrop_integraz(ltvi_campo, "N", ls_errore)
		
	case "NV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "N", ls_errore)
		
	case "ZV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "Z", ls_errore)
		
	case "CV"
		ll_ret = wf_dragdrop_variante(ltvi_campo, "CV", ls_errore)
		
	case "CI"
		//GetItem (handle, ltvi_campo )
		ll_ret = wf_dragdrop_integraz(ltvi_campo, "CI", ls_errore)
		
	case else
		ls_errore = "Attenzione! Azione non tra quelle previste (NI, NV, ZV, CV, CI)!"
		return -1
		
end choose


choose case ll_ret
	case is < 0
		rollback;
		 g_mb.error(ls_errore)
		 return -1
		 
	case 1
		if  (fs_modalita="NV" or fs_modalita="ZV" or fs_modalita="CV")then
			tab_1.tabpage_3.dw_varianti_dettaglio.Change_DW_Current( )
			this.triggerevent("pc_retrieve")
		end if
		//solo se ll_ret è 1 allora fai commit
		commit;
		
		ll_ret = wf_inizio()
		
	case else
		//probabilmente hai risposto no a qualche richiesta di conferma
		//se c'è un messaggio comunque mostralo all'utente
		rollback;
		if ls_errore<>"" and not isnull(ls_errore) then g_mb.show(ls_errore)
		
		return 0
		
end choose


end function

public function integer wf_dragdrop_integraz (ref treeviewitem ftvi_campo, string fs_modalita, ref string fs_errore);string					ls_cod_formula_quan_utilizzo,ls_cod_prodotto_figlio,ls_cod_misura,ls_flag_materia_prima,ls_flag_escludibile, & 
						ls_cod_prodotto_padre,ls_messaggio,ls_errore,ls_des_estesa,ls_formula_tempo,ls_cod_prodotto_variante,ls_test, &
						ls_flag_ramo_descrittivo,ls_cod_prodotto, ls_cod_versione_padre, ls_prodotto_nota[], ls_modalita, &
						ls_cod_versione_variante_padre, ls_cod_versione_variante, ls_rb_integraz_variante

integer				li_risposta

long					ll_risposta, ll_progressivo, ll_num_sequenza, ld_cont, ll_lead_time,ll_i, ll_handle, ll_count

dec{4}				ldd_quan_utilizzo, ldd_quan_tecnica, ldd_coef_calcolo, ldd_dim_x, ldd_dim_y, ldd_dim_z, ldd_dim_t, ldd_qta_nota[]

s_chiave_distinta	l_chiave_distinta, l_chiave_distinta_2, l_chiave_integraz_old
treeviewitem		ltvi_campo_2
any					la_cod_prodotto[]
datawindow			ldw_null
s_cs_xx_parametri lstr_parametri
boolean				lb_insert = true

//parametro fs_modalita
// "N"		modalita normale (dragdrop): il cod_prodotto e le qta sono lette dalle datawindow esterne (dw_ricerca e dw_varianti_commesse_quantita)
// "CI"

l_chiave_distinta = ftvi_campo.data

//se sei su un ramo di tipo Integrazione allora devi solo aggiornare
if l_chiave_distinta.flag_tipo_record = "I" then
	
	//mi salvo la chiave del nodo dell'integrazione, mi servirà per l'update finale
	l_chiave_integraz_old = l_chiave_distinta
	
	//spostati al nodo padre
	ll_handle = tab_1.tabpage_1.tv_db.FindItem (ParentTreeItem!,  il_handle)
	if ll_handle>0 then
		if tab_1.tabpage_1.tv_db.getitem(ll_handle, ftvi_campo) > 0 then
			
			
			//CurrentTreeItem!	
			
			il_handle = ll_handle
			l_chiave_distinta = ftvi_campo.data
			lb_insert = false
			
		else
			return 0
		end if
	else
		return 0
	end if
end if

tab_1.tabpage_1.dw_ricerca.accepttext()
//-------------------------------------------------
choose case fs_modalita
	case "N"
		//prodotto impostato sulla dw_ricerca e poi fatto dragdrop
		ls_cod_prodotto_figlio = upper(tab_1.tabpage_1.dw_ricerca.getitemstring(tab_1.tabpage_1.dw_ricerca.getrow(), "rs_cod_prodotto"))
		ls_cod_versione_variante = tab_1.tabpage_1.dw_ricerca.getitemstring(1, "cod_versione")
		//le qta le leggerai più sotto

	case "CI"
		ls_modalita = fs_modalita
		
		//creazione integrazione selezionata da anagrafica prodotti e quantità scelte da finestra pop-up (più sotto)
		setnull(ldw_null)
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(ldw_null	, "")
		guo_ricerca.uof_get_results( la_cod_prodotto[] )
		
		if upperbound(la_cod_prodotto[]) > 0 then
			ls_cod_prodotto_figlio = string(la_cod_prodotto[1])
		else
			//probabilmente hai fatto annulla dalla ricerca
			return 0
		end if
		wf_cod_versione(ls_cod_prodotto_figlio, ls_cod_versione_variante)
		//ls_cod_versione_variante = "001"
		
		ls_prodotto_nota[1] = ls_cod_prodotto_figlio
		
	case else
		fs_errore = "Valore argomento 'fs_modalita' non previsto (N, CI)"
		return -1
		
end choose

if ls_cod_prodotto_figlio = "" or isnull(ls_cod_prodotto_figlio) then
	fs_errore = "E' necessario specificare il prodotto Integrazione!"
	return -1
end if

tab_1.tabpage_1.dw_varianti_commesse_quantita.accepttext()
		
if l_chiave_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio   then
	 fs_errore = "Attenzione: padre e figlio non possono coincidere!"
	 return -1
end if
	
// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
ll_i = 1
//numero del nodo da cui parto il_handle
ll_handle = il_handle
	
do while 1 = ll_i
	//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
	tab_1.tabpage_1.tv_db.GetItem (ll_handle, ltvi_campo_2 )
	l_chiave_distinta_2 = ltvi_campo_2.data
	
	if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
		//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
	else
		ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
	end if
	
	if upper(ls_cod_prodotto) = upper(ls_cod_prodotto_figlio) then
		fs_errore = "Impossibile inserire un prodotto come figlio di se stesso!~r~nOperazione annullata"
		return -1
	end if
	
	//cerca il padre del nodo numero ll_handle
	ll_handle = tab_1.tabpage_1.tv_db.FindItem ( ParentTreeItem!	,  ll_handle)
	if ll_handle = -1 then exit   // allora sono alla radice
loop


select		cod_misura_mag,
			flag_escludibile,
			flag_materia_prima,
			lead_time
into		:ls_cod_misura,
			:ls_flag_escludibile,
			:ls_flag_materia_prima,
			:ll_lead_time
from		anag_prodotti  
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto_figlio;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_flag_escludibile) then ls_flag_escludibile =  'N'
if isnull(ls_flag_materia_prima) then ls_flag_materia_prima =  'N'

ll_progressivo = 0

select max(progressivo)
into   :ll_progressivo
from   integrazioni_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore sul DB:" + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_progressivo) then ll_progressivo = 0

ll_progressivo = ll_progressivo + 1

wf_visual_testo_formula(ls_cod_formula_quan_utilizzo)

if ls_cod_formula_quan_utilizzo = "" then setnull(ls_cod_formula_quan_utilizzo)


choose case fs_modalita
	case "N"
		ldd_quan_utilizzo = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(tab_1.tabpage_1.dw_varianti_commesse_quantita.getrow(), "qta_utilizzo")
		ldd_quan_tecnica = tab_1.tabpage_1.dw_varianti_commesse_quantita.getitemnumber(tab_1.tabpage_1.dw_varianti_commesse_quantita.getrow(), "qta_tecnica")
		
		if ldd_quan_utilizzo = 0 then
			if not g_mb.confirm("","Quantità dell'integrazione pari a ZERO. Vuoi correggere?", 2) then
				return 0
			end if
		end if
	
	case "CI"
		//creazione integrazione
		//digito le qta da una finestra
		lstr_parametri.parametro_d_1_a[1] =  l_chiave_distinta.quan_utilizzo
		lstr_parametri.parametro_d_1_a[2] =  0		//non so cosa passare, passo ZERO
		window_open_parm(w_menu_varianti_qta, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		if lstr_parametri.parametro_b_1 then
		else
			fs_errore = "Operazione annullata dall'utente!"
			return 0
		end if
		
		ldd_quan_utilizzo = lstr_parametri.parametro_d_1_a[1]
		ldd_quan_tecnica = lstr_parametri.parametro_d_1_a[2]
		
		ldd_qta_nota[1] = ldd_quan_utilizzo
		
	case else
		fs_errore = "(2) Valore argomento 'fs_modalita' non previsto (N, CI)"
		return -1
		
end choose

if ftvi_campo.level = 1 then
	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
else
	ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	ls_cod_versione_padre = l_chiave_distinta.cod_versione_figlio
end if
ll_num_sequenza = l_chiave_distinta.num_sequenza

setnull(ls_cod_prodotto_variante)

select		cod_prodotto,
			cod_versione_variante
into		:ls_cod_prodotto_variante,
			:ls_cod_versione_variante_padre
from		varianti_det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione and 
			prog_riga_ord_ven = :il_prog_riga and 
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;
	
if not isnull(ls_cod_prodotto_variante) and len(ls_cod_prodotto_variante) > 0 then
	ls_cod_prodotto_padre = ls_cod_prodotto_variante
	ls_cod_versione_padre = ls_cod_versione_variante_padre
end if

select flag_ramo_descrittivo
into   :ls_flag_ramo_descrittivo
from   distinta
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
			num_sequenza = :l_chiave_distinta.num_sequenza and
			cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
			cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio and
			cod_versione = :l_chiave_distinta.cod_versione_padre;

if ls_flag_ramo_descrittivo = "S" then
	fs_errore = "Impossibile effettuare varianti o integrazioni su rami descrittivi."
	return -1
end if

//if fs_modalita="CI" then
//	//controllo se la integrazione già esiste; se già esiste e provieni dal menu destro CREA INTEGRAZIONE (fs_modalita="CI")
//	//allora probabilmente si tratta di un aggiornamento prodotto integrazione, quindi fai prima delete e poi insert
//	
//	select	count(*)
//	into	:ll_count
//	from   integrazioni_det_ord_ven
//	where	cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione = :il_anno_registrazione and
//				num_registrazione = :il_num_registrazione and
//				prog_riga_ord_ven = :il_prog_riga and
//				cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
//				num_sequenza = :l_chiave_distinta.num_sequenza and
//				cod_prodotto_figlio = :ls_cod_prodotto_figlio and			//:l_chiave_distinta.cod_prodotto_figlio and
//				cod_versione_padre = :l_chiave_distinta.cod_versione_padre and
//				cod_versione_figlio =:l_chiave_distinta.cod_versione_figlio;
//	
//	if sqlca.sqlcode<0 then
//		fs_errore = "Errore in controllo integrazione esistente: "+sqlca.sqlerrtext
//		return -1
//	end if
//
//	if sqlca.sqlcode=0 and ll_count>0 then
//		//integrazione già esistente !!!!!!! attivati per la cancellazione ed il re-inserimento
//		//###################################################
//	end if
//end if
//----------------------------------------------------------------------------------------

if lb_insert then

	insert into integrazioni_det_ord_ven
			(cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 progressivo,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_misura,
			 quan_tecnica,
			 quan_utilizzo,
			 dim_x,
			 dim_y,
			 dim_z,
			 dim_t,
			 coef_calcolo,
			 des_estesa,
			 cod_formula_quan_utilizzo,
			 flag_escludibile,
			 flag_materia_prima,
			 lead_time,
			 cod_versione_padre,
			 cod_versione_figlio)
	 values
			 (:s_cs_xx.cod_azienda,
			  :il_anno_registrazione,
			  :il_num_registrazione,
			  :il_prog_riga,
			  :ll_progressivo,
			  :ls_cod_prodotto_padre,
			  :ll_num_sequenza,
			  :ls_cod_prodotto_figlio,
			  :ls_cod_misura,
			  :ldd_quan_tecnica,
			  :ldd_quan_utilizzo,
			  0,
			  0,
			  0,
			  0,
			  0,
			  null,
			  :ls_cod_formula_quan_utilizzo,
			  :ls_flag_escludibile,
			  :ls_flag_materia_prima,
			  :ll_lead_time,
			  :ls_cod_versione_padre,
			  :ls_cod_versione_variante);
			  
else
	update integrazioni_det_ord_ven
	set 	cod_prodotto_figlio=:ls_cod_prodotto_figlio,
			quan_tecnica=:ldd_quan_tecnica,
			quan_utilizzo=:ldd_quan_utilizzo
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione and
					num_registrazione = :il_num_registrazione and
					prog_riga_ord_ven = :il_prog_riga and
					cod_prodotto_padre = :l_chiave_integraz_old.cod_prodotto_padre and
					//num_sequenza = :l_chiave_integraz_old.num_sequenza and
					cod_prodotto_figlio = :l_chiave_integraz_old.cod_prodotto_figlio and
					cod_versione_padre = :l_chiave_integraz_old.cod_versione_padre and
					cod_versione_figlio =:l_chiave_integraz_old.cod_versione_figlio;
			
end if

if sqlca.sqlcode < 0 then
	fs_errore = "Attenzione! Errore in inserimento/Aggiornamento Integrazioni Ordine:" + sqlca.sqlerrtext
	return -1
end if

//se arrivi fin qui aggiorna nella nota
if ls_modalita="CI" then
	wf_crea_nota(ls_prodotto_nota[], ldd_qta_nota[], ls_modalita)
end if


return 1
end function

public subroutine wf_cod_versione (string fs_cod_prodotto, ref string fs_cod_versione);//controlla se il prodotto passata ha versioni in distinta
//    se una sola allora considera quella
//    se non ne ha allora propone is_cod_versione  (se vuota mette fisso "001")
//    se più di una propone una finestra di scelta con menu a tendina
long					ll_count, ll_index
datastore			lds_data
string					ls_sql, ls_errore
s_cs_xx_parametri lstr_parametri

ls_sql = 		"select distinct cod_versione "+&
				"from distinta "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"cod_prodotto_padre='"+fs_cod_prodotto+"' "

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if isnull(ll_count) then ll_count=0

choose case ll_count
	case 0
		//seleziona is_cod_versione o se vuoto "001"
		if not isnull(is_cod_versione) and is_cod_versione<>"" then
			fs_cod_versione = is_cod_versione
		else
			fs_cod_versione = "001"
		end if
		
		return
		
	case 1
		//proponi esattamente l'unica versione in distinta
		fs_cod_versione = lds_data.getitemstring(1, "cod_versione")
		if isnull(fs_cod_versione) or fs_cod_versione="" then
			fs_cod_versione = "001"
		end if
		
		return
		
	case else
		//proponi la scelta
		lstr_parametri.parametro_s_1_a[1] = fs_cod_prodotto
		if not isnull(is_cod_versione) and is_cod_versione<>"" then
			lstr_parametri.parametro_s_1_a[2] = is_cod_versione
		else
			lstr_parametri.parametro_s_1_a[2] = "001"
		end if
		
		window_open_parm(w_menu_varianti_versione, 0, lstr_parametri)
		
		lstr_parametri = message.powerobjectparm
		
		fs_cod_versione = lstr_parametri.parametro_s_1_a[1]
		
end choose
	
	

end subroutine

public subroutine wf_crea_nota (string fs_prodotto_nota[], decimal fdd_qta_nota[], string fs_modalita);string ls_nota, ls_des_prodotto1, ls_des_prodotto2

if not ib_modifiche_consentite then return

ls_des_prodotto1 = f_des_tabella("anag_prodotti","cod_prodotto='" + fs_prodotto_nota[1] +"'" ,"des_prodotto")
ls_nota = "Il prodotto "+fs_prodotto_nota[1]+" "+ls_des_prodotto1 +" quantità "+string(fdd_qta_nota[1], "#####0.0")

if upperbound(fs_prodotto_nota[]) > 1 then
	ls_des_prodotto2 = f_des_tabella("anag_prodotti","cod_prodotto='" + fs_prodotto_nota[2] +"'" ,"des_prodotto")
	ls_des_prodotto2 = " "+fs_prodotto_nota[2]+" "+ls_des_prodotto2 +" quantità "+string(fdd_qta_nota[2], "#####0.0")
end if

choose case fs_modalita
	case"CV"
		ls_nota += " è stato sostituito dal prodotto "+ls_des_prodotto2
		
	case "Z"
		ls_nota += " è stato eliminato dalla composizione"
		
	case "CI"
		ls_nota += " è stato aggiunto alla composizione"
		
end choose

if len(is_nota) > 0 then is_nota += "~r~n"

is_nota += ls_nota

return
end subroutine

public function boolean wf_controlla_stampati ();long ll_count

select count(*)
into :ll_count
from   tes_ord_ven  
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione=:il_anno_registrazione and
			num_registrazione=:il_num_registrazione and
			flag_stampata_bcl = 'S';
			
if sqlca.sqlcode<0 then
	g_mb.error("","Errore in controllo ordine stampato: "+sqlca.sqlerrtext)
	ib_modifiche_consentite = false
	return false
end if

if ll_count>0 then
	g_mb.show("","Modifiche non consentite: l'ordine è stato già stampato!")
	ib_modifiche_consentite = false
	return false
end if

ib_modifiche_consentite = true
return true
end function

public function integer wf_filtra_reparti (string fs_cod_deposito);datawindowchild  		ldwc_child
integer					li_child
string						ls_filter

li_child = tab_1.tabpage_4.dw_varianti_ordini.GetChild("cod_reparto_produzione", ldwc_child)


if fs_cod_deposito="" or isnull(fs_cod_deposito) then
	//fai vedere tutti i reparti
	ls_filter = ""
else
	//filtra i reparti per deposito
	ls_filter = "cod_deposito='"+fs_cod_deposito+"'"
end if

ldwc_child.setfilter(ls_filter)
ldwc_child.filter()

return 1

end function

public function integer wf_espandi (long al_handle, long al_livello, long al_cicli);long ll_handle, ll_padre, ll_i

treeviewitem ltv_item

il_cicli ++

if il_cicli > al_livello then
	return 100
end if

tab_1.tabpage_1.tv_db.getitem( al_handle, ltv_item)
tab_1.tabpage_1.tv_db.ExpandItem ( al_handle )	

ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	wf_espandi(ll_handle, al_livello, il_cicli)
	il_cicli --
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	il_cicli --
	wf_espandi(ll_handle, al_livello, il_cicli)
//	il_cicli --
	
end if

return 100
end function

public function integer wf_comprimi (long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)

end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)	
	wf_comprimi(ll_handle)
	
end if

tab_1.tabpage_1.tv_db.collapseitem( ll_handle)

tab_1.tabpage_1.tv_db.collapseitem( al_handle)

return 100
end function

public subroutine wf_esiste_commessa ();long ll_anno_commessa, ll_num_commessa


select anno_commessa, num_commessa
into :ll_anno_commessa, :ll_num_commessa
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:il_anno_registrazione and
			num_registrazione=:il_num_registrazione and
			prog_riga_ord_ven=:il_prog_riga;

if ll_anno_commessa>0 and ll_num_commessa>0 then
	ib_esiste_commessa = true
	st_commessa.text = "Commessa n°:" + string(ll_anno_commessa)+"/"+string(ll_num_commessa)
else
	ib_esiste_commessa = false
	st_commessa.text = "Nessuna commessa associata!"
end if

return
end subroutine

public function integer wf_ricalcola_quan_utlizzo ();/*
 * asd
 */
 
string					ls_cod_formula, ls_cod_prodotto_padre, ls_cod_versione_padre, ls_cod_prodotto_variante, ls_cod_versione_variante, ls_cod_prodotto_figlio, ls_cod_versione_figlio, &
						ls_temp, ls_tipo_ritorno, ls_errore

long					ll_num_sequenza, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_ordine[]

dec{4}				ld_quan_utilizzo

integer				li_ret

s_chiave_distinta	lstr_distinta


ll_anno_registrazione = tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(1, "num_registrazione")
ll_prog_riga_ord_ven = tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(1, "prog_riga_ord_ven")

ls_cod_prodotto_padre = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_prodotto_padre")
ls_cod_versione_padre = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_versione")
ls_cod_prodotto_figlio = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_prodotto_figlio")
ls_cod_versione_figlio  = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_versione_figlio")
ls_cod_prodotto_variante = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_prodotto")
ls_cod_versione_variante = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(1, "cod_versione_variante")
ll_num_sequenza =  tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(1, "num_sequenza")


// Controllo la tab_formule_legami
select cod_formula
into :ls_cod_formula
from tab_formule_legami
where cod_azienda = :s_cs_xx.cod_azienda and
	tab_formule_legami.cod_prodotto_finito = :is_cod_prodotto_finito and
	tab_formule_legami.cod_versione = :is_cod_versione and
	tab_formule_legami.cod_prodotto_padre = :ls_cod_prodotto_padre and
	tab_formule_legami.cod_versione_padre = :ls_cod_versione_padre and
	tab_formule_legami.cod_prodotto_figlio = :ls_cod_prodotto_variante and
	tab_formule_legami.cod_versione_figlio = :ls_cod_versione_variante;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero del codice formula dalla tabella tab_formule_legami", sqlca)
	return -1
	
elseif sqlca.sqlcode = 100 or isnull(ls_cod_formula) or ls_cod_formula = "" then
	// non ho trovato nessuna formula, provo a cercare in distinta
	
	select cod_formula_quan_utilizzo
	into :ls_cod_formula
	from distinta
	where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto_padre = :ls_cod_prodotto_padre and
		cod_versione = :ls_cod_versione_padre and
		cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		cod_versione_figlio = :ls_cod_versione_figlio and 
		num_sequenza = :ll_num_sequenza;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il recupero del codice formula dalla tabella distinta", sqlca)
		return -1
		
	elseif sqlca.sqlcode = 100 or isnull(ls_cod_formula) or ls_cod_formula = "" then
		//nulla da calcolare ...
		return 0
	end if
end if


ll_ordine[1] = il_anno_registrazione
ll_ordine[2] = il_num_registrazione
ll_ordine[3] = il_prog_riga

lstr_distinta.cod_prodotto_padre = ls_cod_prodotto_padre
lstr_distinta.cod_versione_padre = ls_cod_versione_padre
lstr_distinta.num_sequenza = ll_num_sequenza
lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio
lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio


setnull(ld_quan_utilizzo)


li_ret = f_calcola_formula_db_v2(ls_cod_formula, ll_ordine[],  lstr_distinta, "ORD_VEN", ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, ls_errore)

if li_ret<0 then
	ls_errore = 	"Ramo ("+ls_cod_prodotto_padre+","+ls_cod_versione_padre+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio+","+ls_cod_versione_figlio+"): " + &
							ls_errore
							
	g_mb.error(ls_errore)
	return -1
end if

if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0

//ld_quan_utilizzo = f_calcola_formula_db(ls_cod_formula, "comp_det_ord_ven", il_anno_registrazione, il_num_registrazione, il_prog_riga)


if ld_quan_utilizzo = 0 then
	if not g_mb.confirm ("Attenzione il risultato della formula con codice '"+ls_cod_formula+"' è ZERO!~r~nLo memorizzo?") then return 0
else
	if not g_mb.confirm ("Il risultato del calcolo della formula è " + string(ld_quan_utilizzo) + "!~r~nLo memorizzo?") then return 0
end if

update varianti_det_ord_ven
set quan_utilizzo = :ld_quan_utilizzo
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione  = :ll_anno_registrazione and 
		 num_registrazione = :ll_num_registrazione and
		 prog_riga_ord_ven = :ll_prog_riga_ord_ven and
		 cod_prodotto_padre = :ls_cod_prodotto_padre and
		 cod_versione = :ls_cod_versione_padre and
		 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		 cod_versione_figlio = :ls_cod_versione_figlio and
		 num_sequenza = :ll_num_sequenza;
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante l'aggiornamento della quantità utilizzo.", sqlca)
	rollback;
	return -1
end if

// AGGIORNO VARIANTI COMMESSE
select anno_commessa, num_commessa
into :ll_anno_commessa, :ll_num_commessa
from det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione  = :il_anno_registrazione and 
		 num_registrazione = :il_num_registrazione and
		 prog_riga_ord_ven = :il_prog_riga;
		 
if sqlca.sqlcode < 0 then
	g_mb.error(g_str.format("Errore durante il recupero della commessa collegata alla riga d'ordine $1/$2/$3", il_anno_registrazione, il_num_registrazione, il_prog_riga), sqlca)
	rollback;
	return -1
end if

if not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) and ll_anno_commessa > 0 and ll_num_commessa > 0 then
	
	update varianti_commesse
	set quan_utilizzo = :ld_quan_utilizzo
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa  = :ll_anno_commessa and 
			 num_commessa = :ll_num_commessa and
			 cod_prodotto_padre = :ls_cod_prodotto_padre and
			 cod_versione = :ls_cod_versione_padre and
			 cod_prodotto_figlio = :ls_cod_prodotto_figlio and
			 cod_versione_figlio = :ls_cod_versione_figlio and
			 num_sequenza = :ll_num_sequenza;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.error("Errore durante l'aggiornamento della quantità utilizzo nella tabella varianti_commesse.", sqlca)
		rollback;
		return -1
	end if
	
end if

tab_1.tabpage_4.dw_varianti_ordini.triggerevent("pcd_retrieve")

return 0
end function

public subroutine wf_protezione_dw_reparti (boolean ab_proteggi);integer		li_tab, li_moltiplicatore
string			ls_testo
long			ll_colore_protezione, ll_colore_sprotezione, ll_backcolor

ll_colore_protezione = 15780518
ll_colore_sprotezione = 16777215				//bianco

li_tab = 10

if ab_proteggi then
	//vai in protezione della dw
	li_moltiplicatore = 0
	ls_testo = "Modifica"
	ll_backcolor = ll_colore_protezione
else
	//sproteggi la dw
	li_moltiplicatore = 1
	ls_testo = "Salva"
	ll_backcolor = ll_colore_sprotezione
end if

li_tab = li_tab * li_moltiplicatore
tab_1.tabpage_4.dw_reparti_variante.object.data_pronto.tabsequence = li_tab
tab_1.tabpage_4.dw_reparti_variante.object.data_pronto.Background.Color = ll_backcolor
 
li_tab = (li_tab + 10) * li_moltiplicatore
tab_1.tabpage_4.dw_reparti_variante.object.cod_reparto_arrivo.tabsequence = li_tab
tab_1.tabpage_4.dw_reparti_variante.object.cod_reparto_arrivo.Background.Color = ll_backcolor

li_tab = (li_tab + 10) * li_moltiplicatore
tab_1.tabpage_4.dw_reparti_variante.object.data_arrivo.tabsequence = li_tab
tab_1.tabpage_4.dw_reparti_variante.object.data_arrivo.Background.Color = ll_backcolor


tab_1.tabpage_4.dw_reparti_variante.object.p_calendario.enabled = ab_proteggi
tab_1.tabpage_4.dw_reparti_variante.object.b_modifica_salva.text = ls_testo

return
end subroutine

public function integer wf_salva_reparti_date_pronto (ref string as_errore);long			ll_tot, ll_index, ll_num_ordine, ll_riga_ordine, ll_sequenza, ll_row
string			ls_cod_reparto, ls_cod_reparto_arrivo, ls_cod_prodotto_padre, ls_versione_padre, ls_cod_prodotto_figlio, ls_versione_figlio
datetime		ldt_data_pronto, ldt_data_arrivo
integer		li_anno_ordine


ll_row = tab_1.tabpage_4.dw_varianti_ordini.getrow()
if ll_row>0 then
else
	return 1
end if


ll_tot = tab_1.tabpage_4.dw_reparti_variante.rowcount()
if ll_tot > 0 then
else
	as_errore = "Non ci sono righe da salvare!"
	return 1
end if


//leggo la PK principale
li_anno_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "anno_registrazione") 
ll_num_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "num_registrazione") 
ll_riga_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "prog_riga_ord_ven") 
ls_cod_prodotto_padre 	= tab_1.tabpage_4.dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_padre") 
ls_versione_padre 		= tab_1.tabpage_4.dw_varianti_ordini.getitemstring(ll_row, "cod_versione") 
ll_sequenza 					= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "num_sequenza") 
ls_cod_prodotto_figlio 	= tab_1.tabpage_4.dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_figlio") 
ls_versione_figlio	 		= tab_1.tabpage_4.dw_varianti_ordini.getitemstring(ll_row, "cod_versione_figlio") 


for ll_index=1 to ll_tot
	
	ls_cod_reparto = tab_1.tabpage_4.dw_reparti_variante.getitemstring(ll_index, "varianti_det_ord_ven_prod_cod_reparto")
	ldt_data_pronto = tab_1.tabpage_4.dw_reparti_variante.getitemdatetime(ll_index, "data_pronto")
	
	if isnull(ldt_data_pronto) or year(date(ldt_data_pronto))<1950 then
		as_errore = "La data pronto del reparto "+ls_cod_reparto+" non può essere vuota!"
		return 1
	end if
	
	ls_cod_reparto_arrivo = tab_1.tabpage_4.dw_reparti_variante.getitemstring(ll_index, "cod_reparto_arrivo")
	if ls_cod_reparto_arrivo="" then setnull(ls_cod_reparto_arrivo)
	
	ldt_data_arrivo = tab_1.tabpage_4.dw_reparti_variante.getitemdatetime(ll_index, "data_arrivo")
	
	update varianti_det_ord_ven_prod
	set 	data_pronto = :ldt_data_pronto,
			cod_reparto_arrivo = :ls_cod_reparto_arrivo,
			data_arrivo = :ldt_data_arrivo
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_ordine and
				num_registrazione = :ll_num_ordine and
				prog_riga_ord_ven = :ll_riga_ordine and
				cod_prodotto_padre = :ls_cod_prodotto_padre and
				cod_versione = :ls_versione_padre and
				num_sequenza = :ll_sequenza and
				cod_prodotto_figlio = :ls_cod_prodotto_figlio and
				cod_versione_figlio = :ls_versione_figlio and
				cod_reparto = :ls_cod_reparto;

	if sqlca.sqlcode<0 then
		as_errore = "Errore aggiornamento dati reparto "+ls_cod_reparto+" : "+sqlca.sqlerrtext
		return -1
	end if

next

 //se arrivi fin qui il salvataggio è andato a buon fine
 as_errore = ""
 
return 0
end function

public function integer wf_trova (string fs_cod_prodotto, long al_handle);long ll_handle, ll_padre

treeviewitem ltv_item


ll_handle = tab_1.tabpage_1.tv_db.finditem(childtreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,al_handle)

if ll_handle > 0 then
	
	tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
	
	if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
		tab_1.tabpage_1.tv_db.setfocus()
		tab_1.tabpage_1.tv_db.selectitem(ll_handle)
		il_handle = ll_handle
		return 0
	else
		if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
			return 0
		else
			return 100
		end if
	end if
	
end if

ll_padre = al_handle

do
	
	ll_padre = tab_1.tabpage_1.tv_db.finditem(parenttreeitem!,ll_padre)

	if ll_padre > 0 then
		
		ll_handle = tab_1.tabpage_1.tv_db.finditem(nexttreeitem!,ll_padre)
		
		if ll_handle > 0 then
		
			tab_1.tabpage_1.tv_db.getitem(ll_handle,ltv_item)
			
			if pos(upper(ltv_item.label),upper(fs_cod_prodotto),1) > 0 then
				tab_1.tabpage_1.tv_db.setfocus()
				tab_1.tabpage_1.tv_db.selectitem(ll_handle)
				il_handle = ll_handle
				return 0
			else
				if wf_trova(fs_cod_prodotto,ll_handle) = 0 then
					return 0
				else
					return 100
				end if
			end if
			
		end if
		
	end if

loop while ll_padre > 0

return 100
end function

on w_varianti_ordini.create
int iCurrent
call super::create
this.st_commessa=create st_commessa
this.cb_gen_fasi=create cb_gen_fasi
this.cb_chiudi=create cb_chiudi
this.st_1=create st_1
this.st_tempo=create st_tempo
this.cb_verifica=create cb_verifica
this.cb_inserisci=create cb_inserisci
this.lb_elenco_campi=create lb_elenco_campi
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_commessa
this.Control[iCurrent+2]=this.cb_gen_fasi
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_tempo
this.Control[iCurrent+6]=this.cb_verifica
this.Control[iCurrent+7]=this.cb_inserisci
this.Control[iCurrent+8]=this.lb_elenco_campi
this.Control[iCurrent+9]=this.tab_1
end on

on w_varianti_ordini.destroy
call super::destroy
destroy(this.st_commessa)
destroy(this.cb_gen_fasi)
destroy(this.cb_chiudi)
destroy(this.st_1)
destroy(this.st_tempo)
destroy(this.cb_verifica)
destroy(this.cb_inserisci)
destroy(this.lb_elenco_campi)
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;s_cs_xx_parametri lstr_parametri

lstr_parametri = message.powerobjectparm

is_cod_prodotto_finito 	= lstr_parametri.parametro_dw_1.getitemstring(lstr_parametri.parametro_dw_1.getrow(),"cod_prodotto")
is_cod_versione 			= lstr_parametri.parametro_dw_1.getitemstring(lstr_parametri.parametro_dw_1.getrow(),"cod_versione")
il_anno_registrazione 	= lstr_parametri.parametro_dw_1.getitemnumber(lstr_parametri.parametro_dw_1.getrow(),"anno_registrazione")
il_num_registrazione 		= lstr_parametri.parametro_dw_1.getitemnumber(lstr_parametri.parametro_dw_1.getrow(),"num_registrazione")
il_prog_riga 				= lstr_parametri.parametro_dw_1.getitemnumber(lstr_parametri.parametro_dw_1.getrow(),"prog_riga_ord_ven")

//Donato 28/03/2012
//verifica se esiste commessa sulla riga di ordine
wf_esiste_commessa()



tab_1.tabpage_2.dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew +c_nodelete + c_nomodify,c_default)
tab_1.tabpage_3.dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)
tab_1.tabpage_4.dw_varianti_ordini.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)
tab_1.tabpage_5.dw_integrazioni_det_ord_ven.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)
tab_1.tabpage_3.dw_varianti_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)
tab_1.tabpage_3.dw_varianti_dettaglio.set_dw_options(sqlca, tab_1.tabpage_3.dw_varianti_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)

tab_1.tabpage_1.dw_varianti_commesse_quantita.insertrow(0)
iuo_dw_main=tab_1.tabpage_2.dw_distinta_det

ib_attiva_gestione_nota = lstr_parametri.parametro_b_1

is_nota_precedente = lstr_parametri.parametro_s_1_a[1]
if isnull(is_nota_precedente) then is_nota_precedente=""
is_nota = is_nota_precedente

move(this.x, 0)
this.height = w_cs_xx_mdi.mdi_1.height - 50

tab_1.tabpage_4.dw_reparti_variante.object.p_calendario.filename = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"

//protezione datawindow    tab_1.tabpage_4.dw_reparti_variante
wf_protezione_dw_reparti(true)

wf_inizio()


end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(tab_1.tabpage_2.dw_distinta_det,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(tab_1.tabpage_3.dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_1.tabpage_4.dw_varianti_ordini, &
										  "cod_deposito_produzione", &
										  sqlca, &
										  "anag_depositi", &
										  "cod_deposito", &
										  "des_deposito", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
										  
f_po_loaddddw_dw(tab_1.tabpage_4.dw_reparti_variante, &
										  "cod_reparto_arrivo", &
										  sqlca, &
										  "anag_reparti", &
										  "cod_reparto", &
										  "des_reparto", &
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event resize;call super::resize;
setredraw(false)

cb_chiudi.x = 55
cb_chiudi.y = 12
cb_chiudi.width = 338
cb_chiudi.height = 80

cb_gen_fasi.x = 466
cb_gen_fasi.y = 12
cb_gen_fasi.width = 402
cb_gen_fasi.height = 80

tab_1.tabpage_1.dw_ricerca.x = 30
tab_1.tabpage_1.dw_ricerca.y = 50
tab_1.tabpage_1.dw_ricerca.width = 2405
tab_1.tabpage_1.dw_ricerca.height = 180

tab_1.tabpage_1.dw_azioni.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.dw_azioni.y = 260
tab_1.tabpage_1.dw_azioni.width = 3415
tab_1.tabpage_1.dw_azioni.height = 212

tab_1.tabpage_1.dw_varianti_commesse_quantita.x = 2720
tab_1.tabpage_1.dw_varianti_commesse_quantita.y = 32
tab_1.tabpage_1.dw_varianti_commesse_quantita.width = 750
tab_1.tabpage_1.dw_varianti_commesse_quantita.height = 212

tab_1.tabpage_1.tv_db.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_1.tv_db.y = 488
tab_1.tabpage_1.tv_db.width = 3419
tab_1.tabpage_1.tv_db.height = tab_1.height - tab_1.tabpage_1.dw_ricerca.height - tab_1.tabpage_1.dw_azioni.height - 250

tab_1.tabpage_2.dw_distinta_det.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_2.dw_distinta_det.y = 50
tab_1.tabpage_2.dw_distinta_det.width = 2587
tab_1.tabpage_2.dw_distinta_det.height = 1432

tab_1.tabpage_3.dw_distinta_gruppi_varianti.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.dw_distinta_gruppi_varianti.y = 50
tab_1.tabpage_3.dw_distinta_gruppi_varianti.width = 1925
tab_1.tabpage_3.dw_distinta_gruppi_varianti.height = 632

tab_1.tabpage_3.dw_varianti_lista.x = 1970
tab_1.tabpage_3.dw_varianti_lista.y = tab_1.tabpage_3.dw_distinta_gruppi_varianti.y
tab_1.tabpage_3.dw_varianti_lista.width = 1138
tab_1.tabpage_3.dw_varianti_lista.height = 632

tab_1.tabpage_3.cb_crea.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.cb_crea.y = 692
tab_1.tabpage_3.cb_crea.width = 361
tab_1.tabpage_3.cb_crea.height = 80

tab_1.tabpage_3.dw_varianti_dettaglio.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_3.dw_varianti_dettaglio.y = 788
tab_1.tabpage_3.dw_varianti_dettaglio.width = 2537
tab_1.tabpage_3.dw_varianti_dettaglio.height = 1020

tab_1.tabpage_4.dw_varianti_ordini.x =  tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_4.dw_varianti_ordini.y = 50
tab_1.tabpage_4.dw_varianti_ordini.width = 2766
tab_1.tabpage_4.dw_varianti_ordini.height = 1476


//-------------------------------
tab_1.tabpage_4.dw_lista_reparti.x = tab_1.tabpage_4.dw_varianti_ordini.x
tab_1.tabpage_4.dw_lista_reparti.y = tab_1.tabpage_4.dw_varianti_ordini.y + tab_1.tabpage_4.dw_varianti_ordini.height + 10
tab_1.tabpage_4.dw_lista_reparti.width = 1000		//int( ( tab_1.tabpage_4.width / 2 ) - 200)
tab_1.tabpage_4.dw_lista_reparti.height =  tab_1.tabpage_4.height - tab_1.tabpage_4.dw_varianti_ordini.height - 100

tab_1.tabpage_4.cb_aggiungi_reparto.x = tab_1.tabpage_4.dw_lista_reparti.x + tab_1.tabpage_4.dw_lista_reparti.width + 50
tab_1.tabpage_4.cb_aggiungi_reparto.y = tab_1.tabpage_4.dw_lista_reparti.y + int ( tab_1.tabpage_4.dw_lista_reparti.height / 3 )

tab_1.tabpage_4.cb_rimuovi_reparto.x = tab_1.tabpage_4.cb_aggiungi_reparto.x
tab_1.tabpage_4.cb_rimuovi_reparto.y = tab_1.tabpage_4.cb_aggiungi_reparto.y + 200

tab_1.tabpage_4.dw_reparti_variante.y = tab_1.tabpage_4.dw_lista_reparti.y
tab_1.tabpage_4.dw_reparti_variante.width = 2160
tab_1.tabpage_4.dw_reparti_variante.height =  tab_1.tabpage_4.dw_lista_reparti.height
tab_1.tabpage_4.dw_reparti_variante.x = tab_1.tabpage_4.cb_aggiungi_reparto.x + tab_1.tabpage_4.cb_aggiungi_reparto.width + 50


//tab_1.tabpage_4.cb_aggiungi_reparto.x = int(  tab_1.tabpage_4.width / 2 ) - int( tab_1.tabpage_4.cb_aggiungi_reparto.width / 2 )
//tab_1.tabpage_4.cb_aggiungi_reparto.y = tab_1.tabpage_4.dw_lista_reparti.y + int ( tab_1.tabpage_4.dw_lista_reparti.height / 3 )
//
//tab_1.tabpage_4.cb_rimuovi_reparto.x = int(  tab_1.tabpage_4.width / 2 ) - int( tab_1.tabpage_4.cb_rimuovi_reparto.width / 2 )
//tab_1.tabpage_4.cb_rimuovi_reparto.y = tab_1.tabpage_4.dw_lista_reparti.y + int ( ( tab_1.tabpage_4.dw_lista_reparti.height / 3)  * 2)
//
//tab_1.tabpage_4.dw_reparti_variante.y = tab_1.tabpage_4.dw_varianti_ordini.y + tab_1.tabpage_4.dw_varianti_ordini.height + 10
//tab_1.tabpage_4.dw_reparti_variante.width = int( ( tab_1.tabpage_4.width / 2 ) - 200)
//tab_1.tabpage_4.dw_reparti_variante.height =  tab_1.tabpage_4.height - tab_1.tabpage_4.dw_varianti_ordini.height - 100
//tab_1.tabpage_4.dw_reparti_variante.x = tab_1.tabpage_4.width - tab_1.tabpage_4.dw_reparti_variante.width - 10


tab_1.tabpage_5.dw_integrazioni_det_ord_ven.x = tab_1.tabpage_1.dw_ricerca.x
tab_1.tabpage_5.dw_integrazioni_det_ord_ven.y = 50
tab_1.tabpage_5.dw_integrazioni_det_ord_ven.width = 2400
tab_1.tabpage_5.dw_integrazioni_det_ord_ven.height = 1316

setredraw(true)



end event

type st_commessa from statictext within w_varianti_ordini
integer x = 882
integer y = 20
integer width = 2578
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Commessa n°:"
boolean focusrectangle = false
end type

type cb_gen_fasi from commandbutton within w_varianti_ordini
integer x = 466
integer y = 12
integer width = 402
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Gen.Var.Fasi"
end type

event clicked;string ls_errore, ls_flag_supervisore
long	ll_ret
uo_funzioni_1  luo_funzioni_1

// verifico che l'utente sia un supervisore
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	select flag_supervisore
	into 	:ls_flag_supervisore
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
else
	ls_flag_supervisore = "S"
end if

if ls_flag_supervisore = "S" then
	
	if ib_esiste_commessa then
		g_mb.error("APICE","Impossibile proseguire: è presente una commessa associata alla riga!")
		return
	end if

	luo_funzioni_1 = create uo_funzioni_1
	ll_ret = luo_funzioni_1.uof_gen_varianti_deposito_ord_ven ( il_anno_registrazione, il_num_registrazione, il_prog_riga, is_cod_prodotto_finito, is_cod_versione, ref ls_errore )
	if ll_ret < 0 then
		g_mb.error(ls_errore)
		rollback;
	else
		commit;
		
		wf_inizio()
		
	end if
	destroy luo_funzioni_1

else
	g_mb.warning("Per usare questa funzione l'utente deve avere il privilegio di Supervisore !")
end if


end event

type cb_chiudi from commandbutton within w_varianti_ordini
integer x = 55
integer y = 12
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;s_cs_xx_parametri lstr_parametri

if ib_attiva_gestione_nota and ib_modifiche_consentite then
	//è stata attivata la gestione della di dettaglio che memorizza le azioni fatte
	
	if is_nota_precedente<>is_nota  then
		//hai fatto modifiche, allora mostrale in questa finestra
		lstr_parametri.parametro_d_1_a[1] = il_anno_registrazione
		lstr_parametri.parametro_d_1_a[2] = il_num_registrazione
		lstr_parametri.parametro_d_1_a[3] = il_prog_riga
		lstr_parametri.parametro_s_1_a[1] = is_nota
		lstr_parametri.parametro_s_1_a[2] = is_nota_precedente
		
		
		window_open_parm(w_menu_varianti_nota, 0, lstr_parametri)
		
	end if
	
end if

close(parent)

end event

type st_1 from statictext within w_varianti_ordini
boolean visible = false
integer x = 3547
integer y = 1000
integer width = 690
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Tempo Calcolato in minuti:"
boolean focusrectangle = false
end type

type st_tempo from statictext within w_varianti_ordini
boolean visible = false
integer x = 3547
integer y = 1096
integer width = 521
integer height = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_verifica from commandbutton within w_varianti_ordini
event clicked pbm_bnclicked
boolean visible = false
integer x = 3547
integer y = 856
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Verifica"
end type

event clicked;string ls_campo,ls_formula,ls_risultato
double ldd_valore_campo
integer li_i,li_risposta

ls_formula = tab_1.tabpage_4.dw_varianti_ordini.getitemstring(tab_1.tabpage_4.dw_varianti_ordini.getrow(),"formula_tempo")

for li_i = 1 to 7
	lb_elenco_campi.selectitem(li_i)
	ls_campo = lb_elenco_campi.selecteditem()
	if pos(ls_formula,ls_campo) <> 0 then
		ldd_valore_campo  = tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(tab_1.tabpage_4.dw_varianti_ordini.getrow(),ls_campo)
		if isnull(ldd_valore_campo) then ldd_valore_campo=0
		li_risposta = f_sostituzione(ls_formula, ls_campo, string(ldd_valore_campo))
	end if
next

li_risposta = f_if(ls_formula)

if li_risposta < 0 then
	g_mb.messagebox("Sep",ls_formula,stopsign!)
	return
end if

ls_risultato = f_parser_parentesi(ls_formula)
if ls_risultato = "Errore" then
	g_mb.messagebox("Sep","Attenzione le librerie per il calcolo delle formule trigonometriche non sono installate correttamente. Verificare.",stopsign!)
	return -1
end if
st_tempo.text = ls_risultato
end event

type cb_inserisci from commandbutton within w_varianti_ordini
event clicked pbm_bnclicked
boolean visible = false
integer x = 3547
integer y = 760
integer width = 361
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Inserisci"
end type

event clicked;string ls_formula

tab_1.tabpage_4.dw_varianti_ordini.setcolumn("formula_tempo")

ls_formula = tab_1.tabpage_4.dw_varianti_ordini.gettext()

if isnull(ls_formula) then ls_formula=""

ls_formula = ls_formula + " " + lb_elenco_campi.selecteditem() + " "

tab_1.tabpage_4.dw_varianti_ordini.settext(ls_formula)


end event

type lb_elenco_campi from listbox within w_varianti_ordini
boolean visible = false
integer x = 3547
integer y = 260
integer width = 704
integer height = 480
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string item[] = {"QUAN_TECNICA","QUAN_UTILIZZO","DIM_X","DIM_Y","DIM_Z","DIM_T","COEF_CALCOLO"}
borderstyle borderstyle = stylelowered!
end type

type tab_1 from tab within w_varianti_ordini
event create ( )
event destroy ( )
integer x = 32
integer y = 108
integer width = 3529
integer height = 2068
integer taborder = 110
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

event selectionchanged;choose case  newindex
	case 4
		iuo_dw_main=tab_1.tabpage_4.dw_varianti_ordini
	case 5
		iuo_dw_main=tab_1.tabpage_5.dw_integrazioni_det_ord_ven	
end choose
end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 3493
integer height = 1948
long backcolor = 12632256
string text = "Struttura"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
tv_db tv_db
dw_varianti_commesse_quantita dw_varianti_commesse_quantita
dw_azioni dw_azioni
dw_ricerca dw_ricerca
end type

on tabpage_1.create
this.tv_db=create tv_db
this.dw_varianti_commesse_quantita=create dw_varianti_commesse_quantita
this.dw_azioni=create dw_azioni
this.dw_ricerca=create dw_ricerca
this.Control[]={this.tv_db,&
this.dw_varianti_commesse_quantita,&
this.dw_azioni,&
this.dw_ricerca}
end on

on tabpage_1.destroy
destroy(this.tv_db)
destroy(this.dw_varianti_commesse_quantita)
destroy(this.dw_azioni)
destroy(this.dw_ricerca)
end on

type tv_db from treeview within tabpage_1
integer x = 32
integer y = 488
integer width = 3419
integer height = 1432
integer taborder = 130
string dragicon = "Exclamation!"
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event clicked;if handle<>0 then
	il_handle = handle
	
	tab_1.tabpage_2.dw_distinta_det.Change_DW_Current( )
	w_varianti_ordini.triggerevent("pc_retrieve")
	tab_1.tabpage_3.dw_distinta_gruppi_varianti.Change_DW_Current( )
	w_varianti_ordini.triggerevent("pc_retrieve")
	tab_1.tabpage_4.dw_varianti_ordini.Change_DW_Current( )
	w_varianti_ordini.triggerevent("pc_retrieve")
	tab_1.tabpage_5.dw_integrazioni_det_ord_ven.Change_DW_Current( )
	w_varianti_ordini.triggerevent("pc_retrieve")
	
end if
end event

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")								//	1
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")							//	2
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")								//	3
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")					//	4
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")						//	5
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")					//	6
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")				//	7
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")					//	8
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")				//	9
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")			//	10
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante_vincol.bmp")				//	11

end event

event dragdrop;treeviewitem				ltvi_campo
string							ls_rb_integraz_variante,ls_errore
long							ll_ret, ll_risposta
boolean						lb_sovrascrivi
string							ls_modalita

dw_azioni.accepttext()
ls_rb_integraz_variante = dw_azioni.getitemstring(dw_azioni.getrow(), "flag_integraz_variante")

//in il_handle c'è già handle perchè nell'evento dragwithin è stato fatto questo
if il_handle>0 then
else
	return
end if

if ls_rb_integraz_variante="I" then
	//commit e rollback tutto gestito in wf_azione
	wf_azione("NI")
	
elseif ls_rb_integraz_variante="V" then
	if il_handle = 1 then
		g_mb.error("Attenzione! Il prodotto finito non può essere associato ad una variante!")
		return
	end if
	
	//commit e rollback tutto gestito in wf_azione
	wf_azione("NV")

end if

end event

event dragwithin;TreeViewItem		ltvi_Over

If GetItem(handle, ltvi_Over) = -1 Then
	SetDropHighlight(0)

	Return
End If


SetDropHighlight(handle)

il_handle = handle
end event

event rightclicked;treeviewitem			ltv_item
s_chiave_distinta		l_chiave_distinta
string						ls_tipo_nodo, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione_padre, ls_cod_versione_figlio, ls_titolo
boolean					lb_abilita_varianti, lb_abilita_integrazioni

if handle < 1 then return 

tv_db.getitem(handle, ltv_item)
il_handle = handle
l_chiave_distinta = ltv_item.data

ls_tipo_nodo = l_chiave_distinta.flag_tipo_record

lb_abilita_integrazioni = true

//se si tratta di un ramo integrazione non abilito le voci di menu relative alle varianti
if ls_tipo_nodo = "I" then
	lb_abilita_varianti = false
else
	lb_abilita_varianti = true
end if

ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
ls_cod_prodotto_figlio = l_chiave_distinta.cod_prodotto_figlio
ls_cod_versione_padre = l_chiave_distinta.cod_versione_padre
ls_cod_versione_figlio = l_chiave_distinta.cod_versione_figlio

m_tree_varianti lm_menu
lm_menu = create m_tree_varianti

//imposta immagini per le voci di menu
lm_menu.m_azzeraquantità.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "variante_azzera.bmp"
lm_menu.m_creavariante.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp"
lm_menu.m_creaintegrazione.menuimage = s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp"
//------------------------------------------

if isnull(ls_cod_prodotto_padre) then ls_cod_prodotto_padre=""
if isnull(ls_cod_prodotto_figlio) then ls_cod_prodotto_figlio=""
if isnull(ls_cod_versione_padre) then ls_cod_versione_padre=""
if isnull(ls_cod_versione_figlio) then ls_cod_versione_figlio=""

ls_titolo = "Padre: "+ls_cod_prodotto_padre+" - Vers.:"+ls_cod_versione_padre
lm_menu.m_padre.text = ls_titolo

ls_titolo = "Figlio: "+ls_cod_prodotto_figlio+" - Vers.:"+ls_cod_versione_figlio
lm_menu.m_figlio.text = ls_titolo

lm_menu.m_azzeraquantità.enabled = lb_abilita_varianti
lm_menu.m_creavariante.enabled = lb_abilita_varianti
lm_menu.m_creaintegrazione.enabled = lb_abilita_integrazioni

pcca.window_current  = w_varianti_ordini //parent

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu
end event

type dw_varianti_commesse_quantita from datawindow within tabpage_1
integer x = 2720
integer y = 32
integer width = 750
integer height = 212
integer taborder = 90
boolean bringtotop = true
string title = "none"
string dataobject = "d_varianti_commesse_quantita"
boolean border = false
boolean livescroll = true
end type

type dw_azioni from u_dw_search within tabpage_1
event ue_aggiorna_distinta ( )
integer x = 32
integer y = 260
integer width = 3415
integer height = 212
integer taborder = 140
boolean bringtotop = true
string dataobject = "d_azioni_varianti"
boolean border = false
end type

event ue_aggiorna_distinta();wf_inizio()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_elimina_var"
		wf_elimina_varianti()
		
	case "b_elimina_integraz"
		wf_elimina_integrazioni()
		
	case "b_prezzo_variante"
		wf_prezzo()
		
	case "b_stampa"
		wf_stampa()
		
	case "b_comprimi"
		long ll_tvi
		
		dw_azioni.accepttext()
		tv_db.setredraw( false)
		ll_tvi = tv_db.FindItem(RootTreeItem! , 0)
		wf_comprimi(ll_tvi)
		tv_db.setredraw( true)

	case "b_espandi"
		long ll_handle, ll_livelli
		
		dw_azioni.accepttext()
		ll_livelli = dw_azioni.getitemnumber(dw_azioni.getrow(),"flag_livello")
		il_cicli = 0
		
		tv_db.setredraw( false)
		
		il_handle = tv_db.finditem(roottreeitem!,0)
		wf_comprimi(il_handle)
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			wf_espandi(il_handle, ll_livelli, il_cicli)
		end if
		
		tv_db.setredraw( true)		
end choose



end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "flag_visione_dati"
		
		postevent("ue_aggiorna_distinta")
		
end choose
end event

type dw_ricerca from u_dw_search within tabpage_1
event ue_key pbm_dwnkey
integer x = 32
integer y = 52
integer width = 2450
integer height = 180
integer taborder = 50
string dragicon = "H:\CS_XX_50\cs_sep\Cs_sep.ico"
boolean bringtotop = true
string dataobject = "d_varianti_commessa_ricerca"
boolean border = false
end type

event clicked;call super::clicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_set_response( )
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		this.setcolumn("cod_versione")
		
	case "b_trova"
		string ls_cod_prodotto
		
		ls_cod_prodotto = dw_ricerca.getitemstring( 1, "rs_cod_prodotto")
		
		if isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
			g_mb.messagebox( "SEP", "Attenzione:selezionare un prodotto per eseguire la ricerca!")
			return -1
		end if
		
		if isnull(il_handle) or il_handle = 0 then
			il_handle = tv_db.finditem(roottreeitem!,0)
		end if
		
		tv_db.setfocus()
		
		tv_db.selectitem(il_handle)
		
		if il_handle > 0 then
			if wf_trova(ls_cod_prodotto,il_handle) = 100 then
				g_mb.messagebox("Menu Principale","Prodotto non trovato",information!)
				tab_1.tabpage_1.tv_db.setfocus()
			end if
		end if		
		
	case else
		drag(Begin!)
		
end choose
end event

event constructor;call super::constructor;this.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "\11.5\arrow_left_ico.ico"
end event

event itemchanged;call super::itemchanged;string ls_null
long ll_test


setnull(ls_null)
// per default la versione del figlio viene proposta uguale
// alla versione del padre
choose case dwo.name
		
	case "rs_cod_prodotto"
		
		if not isnull(data) or data = "" then
			
			f_PO_LoadDDDW_DW( this, &
			                  "cod_versione", &
									sqlca, &
						         "distinta", &
									" cod_versione ","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
						  			" cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto_padre = '" + data + "' ")						  
									  
			dw_ricerca.setitem(dw_ricerca.getrow(),"cod_versione",is_cod_versione)
			
		else
			
			dw_ricerca.setitem(dw_ricerca.getrow(),"cod_versione",ls_null)
			
		end if
		
end choose


end event

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 3493
integer height = 1948
long backcolor = 12632256
string text = "Dettaglio"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_distinta_det dw_distinta_det
end type

on tabpage_2.create
this.dw_distinta_det=create dw_distinta_det
this.Control[]={this.dw_distinta_det}
end on

on tabpage_2.destroy
destroy(this.dw_distinta_det)
end on

type dw_distinta_det from uo_cs_xx_dw within tabpage_2
integer x = 32
integer y = 52
integer width = 2587
integer height = 1432
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_varianti_ordini_distinta_det"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if

choose case dwo.name
	case "b_ric_prod_figlio"
		guo_ricerca.uof_ricerca_prodotto(dw_distinta_det,"cod_prodotto_figlio")
		
end choose
end event

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	ib_proteggi_chiavi=false
	
   dw_distinta_det.object.b_ric_prod_figlio.enabled=true
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_sequenza
	string ls_cod_prodotto_padre
	
	ib_proteggi_chiavi=false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo

	tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
	dw_distinta_det.object.b_ric_prod_figlio.enabled = true
	
	if l_chiave_distinta.cod_prodotto_figlio="" then
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	end if
	
	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
	triggerevent("itemchanged")

	select max(distinta.num_sequenza)
	into   :ll_sequenza
	from   distinta
	where	cod_azienda = :s_cs_xx.cod_azienda and 
		 		cod_prodotto_padre = :ls_cod_prodotto_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
	
	ll_sequenza = ll_sequenza + 10
	setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
	
	setcolumn("cod_prodotto_figlio")
	
	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))	
end if

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  l_chiave_distinta.cod_prodotto_padre, & 
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	long ll_risposta
	
		ll_risposta=wf_inizio()	
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
		SetItem(l_Idx, "cod_versione", is_cod_versione)	
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	
   dw_distinta_det.object.b_ric_prod_figlio.enabled=false	
	
end if
end event

type tabpage_3 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 3493
integer height = 1948
long backcolor = 12632256
string text = "Varianti Disponibili"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_varianti_dettaglio dw_varianti_dettaglio
dw_varianti_lista dw_varianti_lista
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
cb_crea cb_crea
end type

on tabpage_3.create
this.dw_varianti_dettaglio=create dw_varianti_dettaglio
this.dw_varianti_lista=create dw_varianti_lista
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.cb_crea=create cb_crea
this.Control[]={this.dw_varianti_dettaglio,&
this.dw_varianti_lista,&
this.dw_distinta_gruppi_varianti,&
this.cb_crea}
end on

on tabpage_3.destroy
destroy(this.dw_varianti_dettaglio)
destroy(this.dw_varianti_lista)
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.cb_crea)
end on

type dw_varianti_dettaglio from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 32
integer y = 788
integer width = 2537
integer height = 1020
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_varianti_dettaglio"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo


if il_handle>0 then
else
	return
end if

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
						  l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_varianti_lista from uo_cs_xx_dw within tabpage_3
event pcd_new pbm_custom52
integer x = 1970
integer y = 52
integer width = 1138
integer height = 632
integer taborder = 120
boolean bringtotop = true
string dataobject = "d_varianti_lista"
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_gruppo_variante

ls_cod_gruppo_variante = dw_distinta_gruppi_varianti.getitemstring(dw_distinta_gruppi_varianti.getrow(),"cod_gruppo_variante")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_variante)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within tabpage_3
integer x = 32
integer y = 52
integer width = 1925
integer height = 632
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_distinta_gruppi_varianti"
boolean vscrollbar = true
end type

event pcd_pickedrow;call super::pcd_pickedrow;dw_varianti_lista.Change_DW_Current( )
w_varianti_ordini.triggerevent("pc_retrieve")

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve( s_cs_xx.cod_azienda, &
                    l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  is_cod_versione, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if rowcount()>0 then
	dw_varianti_lista.Change_DW_Current( )
	w_varianti_ordini.triggerevent("pc_retrieve")
else
	dw_varianti_lista.reset()
	dw_varianti_dettaglio.reset()
end if
end event

type cb_crea from commandbutton within tabpage_3
integer x = 32
integer y = 692
integer width = 361
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Var."
end type

event clicked;string ls_cod_misura,ls_cod_prodotto_variante,ls_test,ls_des_estesa,ls_formula_tempo, & 
		 ls_flag_materia_prima,ls_flag_ramo_descrittivo
long ll_lead_time
decimal ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t, & 
		 ldd_coef_calcolo
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if dw_distinta_gruppi_varianti.rowcount() <= 0 then
	g_mb.messagebox("Sep","Non esistono varianti preconfigurate per questo prodotto.",information!)
	return
end if

if il_handle = 1 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
	return
end if

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

ls_cod_misura = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_misura")
ldd_quan_tecnica = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_tecnica")
ldd_quan_utilizzo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_utilizzo") 
ldd_dim_x = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_x")
ldd_dim_y = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_y")
ldd_dim_z = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_z")
ldd_dim_t = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_t")
ldd_coef_calcolo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"coef_calcolo")
ls_cod_prodotto_variante = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_prodotto")
ls_des_estesa = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"des_estesa")
ls_formula_tempo = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"formula_tempo")
ls_flag_materia_prima = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"flag_materia_prima")

select cod_azienda
into   :ls_test
from   varianti_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione  = :il_num_registrazione and
		 prog_riga_ord_ven  = :il_prog_riga and
		 cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre and
		 cod_versione       = :l_chiave_distinta.cod_versione_padre and
		 num_sequenza       = :l_chiave_distinta.num_sequenza and
		 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
		 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;

if sqlca.sqlcode <=0 then
	g_mb.messagebox("Sep","Attenzione! Esiste già una variante ordine per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
	return
end if

select lead_time, 
       flag_ramo_descrittivo
into   :ll_lead_time, 
       :ls_flag_ramo_descrittivo
from   distinta
where  cod_azienda         = :s_cs_xx.cod_azienda and
       cod_prodotto_padre  = :l_chiave_distinta.cod_prodotto_padre and
       cod_versione        = :l_chiave_distinta.cod_versione_padre and
		 num_sequenza        = :l_chiave_distinta.num_sequenza and
		 cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio and
		 cod_versione_figlio = :l_chiave_distinta.cod_versione_figlio;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Sep","Errore in ricerca ramo di distinta.",information!)
	return
end if

if ls_flag_ramo_descrittivo = "S" then
	g_mb.messagebox("sep","Attenzione!!! Non è possibile fare una variante su un ramo descrittivo.")
	return
end if
		 

insert into varianti_det_ord_ven
		(cod_azienda,
		 anno_registrazione,
		 num_registrazione,
		 prog_riga_ord_ven,
		 cod_prodotto_padre,
		 cod_versione,
		 num_sequenza,
		 cod_prodotto_figlio,
		 cod_versione_figlio,
		 cod_prodotto,
		 cod_misura,
		 quan_tecnica,
		 quan_utilizzo,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 coef_calcolo,
		 formula_tempo,
		 flag_esclusione,
		 des_estesa,
		 flag_materia_prima,
		 cod_versione_variante,
		 lead_time)
values
		(:s_cs_xx.cod_azienda,
		 :il_anno_registrazione,
		 :il_num_registrazione,
		 :il_prog_riga,
		 :l_chiave_distinta.cod_prodotto_padre,
		 :l_chiave_distinta.cod_versione_padre,
		 :l_chiave_distinta.num_sequenza,
		 :l_chiave_distinta.cod_prodotto_figlio,
		 :l_chiave_distinta.cod_versione_figlio,
		 :ls_cod_prodotto_variante, 
		 :ls_cod_misura,
		 :ldd_quan_tecnica,
		 :ldd_quan_utilizzo,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ldd_coef_calcolo,
		 :ls_formula_tempo,
		 'N',
		 :ls_des_estesa,
		 :ls_flag_materia_prima,
		 :l_chiave_distinta.cod_versione_figlio,
		 :ll_lead_time);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti per ordine:" + sqlca.sqlerrtext ,stopsign!)
	return
end if

tab_1.tabpage_4.dw_varianti_ordini.Change_DW_Current( )
w_varianti_ordini.triggerevent("pc_retrieve")
wf_inizio()

g_mb.messagebox("Sep","Creazione variante avvenuta con successo",information!)
end event

type tabpage_4 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 3493
integer height = 1948
long backcolor = 12632256
string text = "Varianti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_reparti_variante dw_reparti_variante
cb_rimuovi_reparto cb_rimuovi_reparto
cb_aggiungi_reparto cb_aggiungi_reparto
dw_lista_reparti dw_lista_reparti
dw_varianti_ordini dw_varianti_ordini
end type

on tabpage_4.create
this.dw_reparti_variante=create dw_reparti_variante
this.cb_rimuovi_reparto=create cb_rimuovi_reparto
this.cb_aggiungi_reparto=create cb_aggiungi_reparto
this.dw_lista_reparti=create dw_lista_reparti
this.dw_varianti_ordini=create dw_varianti_ordini
this.Control[]={this.dw_reparti_variante,&
this.cb_rimuovi_reparto,&
this.cb_aggiungi_reparto,&
this.dw_lista_reparti,&
this.dw_varianti_ordini}
end on

on tabpage_4.destroy
destroy(this.dw_reparti_variante)
destroy(this.cb_rimuovi_reparto)
destroy(this.cb_aggiungi_reparto)
destroy(this.dw_lista_reparti)
destroy(this.dw_varianti_ordini)
end on

type dw_reparti_variante from datawindow within tabpage_4
integer x = 1577
integer y = 1520
integer width = 1202
integer height = 420
integer taborder = 130
string title = "none"
string dataobject = "d_varianti_det_ord_ven_reparti_variante"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;boolean							lb_proteggi
string								ls_errore
integer							li_ret, li_anno_ordine
long								ll_num_ordine, ll_riga_ordine, ll_row
uo_calendario_prod_new		luo_cal_prod


if rowcount()>0 then
else
	return
end if


choose case dwo.name
	case "b_modifica_salva"
		if upper(dwo.text) = "SALVA" then
			//vuol dire che vuoi salvare
			
			tab_1.tabpage_4.dw_reparti_variante.accepttext()
			
			li_ret = wf_salva_reparti_date_pronto(ls_errore)
			if li_ret<0 then
				rollback;
				g_mb.error(ls_errore)
				return
				
			elseif li_ret=1 then
				rollback;
				
				if  ls_errore<>"" and not isnull(ls_errore) then
					g_mb.warning(ls_errore)
				end if
				
				return
				
			else
				//il salvataggio è andato a buon fine
				//ricalcola tutto (calendario produzione e det ord ven) per la sola riga ordine
				
				//leggo la PK principale
				ll_row = tab_1.tabpage_4.dw_varianti_ordini.getrow()
				li_anno_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "anno_registrazione") 
				ll_num_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "num_registrazione") 
				ll_riga_ordine 				= tab_1.tabpage_4.dw_varianti_ordini.getitemnumber(ll_row, "prog_riga_ord_ven") 
				
				
				luo_cal_prod = create uo_calendario_prod_new
				li_ret = luo_cal_prod.uof_salva_in_calendario(li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_errore)
				destroy luo_cal_prod
				
				if li_ret<0 then
					rollback;
					g_mb.error(ls_errore)
					return
				end if
				
			end if
			
			//se arrivi qui fai un bel commit
			commit;
			
			g_mb.success("Salvataggio effettuato con successo")
			
			lb_proteggi = true
			
		else
			//vuol dire che vuoi mettere in modalità editabile
			lb_proteggi = false
			
		end if
		
		wf_protezione_dw_reparti(lb_proteggi)
		
		
		
	case "p_calendario"
		//visualizzazione del calendario
		
end choose
end event

type cb_rimuovi_reparto from commandbutton within tabpage_4
integer x = 1344
integer y = 1764
integer width = 133
integer height = 100
integer taborder = 140
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<"
end type

event clicked;string								ls_cod_reparto_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio, ls_errore
long								ll_num_ordine,ll_riga_ordine, ll_num_sequenza, ll_row, ll_row_reparto
uo_calendario_prod_new		luo_cal_prod
integer							li_ret, li_anno_ordine



if ib_esiste_commessa then
	g_mb.error("APICE","Impossibile modificare: è presente una commessa associata alla riga!")
	return 1
end if

ll_row = dw_varianti_ordini.getrow()
if ll_row>0 then
else
	return
end if

ll_row_reparto = dw_reparti_variante.getrow()
if ll_row_reparto>0 then
else
	return
end if

li_anno_ordine = dw_varianti_ordini.getitemnumber(ll_row, "anno_registrazione") 
ll_num_ordine = dw_varianti_ordini.getitemnumber(ll_row, "num_registrazione") 
ll_riga_ordine = dw_varianti_ordini.getitemnumber(ll_row, "prog_riga_ord_ven") 
ls_cod_prodotto_padre = dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_padre") 
ls_cod_versione_padre = dw_varianti_ordini.getitemstring(ll_row, "cod_versione") 
ll_num_sequenza = dw_varianti_ordini.getitemnumber(ll_row, "num_sequenza") 
ls_cod_prodotto_figlio = dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_figlio") 
ls_cod_versione_figlio	 = dw_varianti_ordini.getitemstring(ll_row, "cod_versione_figlio") 

ls_cod_reparto_produzione = dw_reparti_variante.getitemstring(ll_row_reparto,"varianti_det_ord_ven_prod_cod_reparto")

//disimpegno il calendario produzione
//NOTA: il disimpegno verrà effettuato solo se in det_ord_ven_prod trova il reparto con data_pronto impostato,
//altrimenti nessuna operazione sarà effettuata sul calendario (ritorno pari a 1)
luo_cal_prod = create uo_calendario_prod_new
li_ret = luo_cal_prod.uof_disimpegna_calendario(li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_reparto_produzione, ls_errore)
destroy luo_cal_prod

if li_ret<0 then
	rollback;
	g_mb.warning("APICE", ls_errore)
	
	//in caso di problemi al calendario non blocco
	
else
	//vado avanti:
	//pulisco la det_ord_ven_prod relativamente al reparto eliminato
	delete from det_ord_ven_prod
	where		cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_ordine and 
				num_registrazione = :ll_num_ordine and
				prog_riga_ord_ven = :ll_riga_ordine and
				cod_reparto = :ls_cod_reparto_produzione;
	
	if sqlca.sqlcode<0 then
		ls_errore = "Errore in eliminazione dato da det_ord_ven_prod per il reparto "+ls_cod_reparto_produzione+" : "+sqlca.sqlerrtext
		rollback;
		g_mb.warning("APICE", ls_errore)
		
		//in caso di problemi al calendario non blocco
	end if
end if


delete varianti_det_ord_ven_prod 
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :li_anno_ordine and
		num_registrazione = :ll_num_ordine and
		prog_riga_ord_ven = :ll_riga_ordine and
		cod_prodotto_padre = :ls_cod_prodotto_padre and
		cod_versione = :ls_cod_versione_padre and
		num_sequenza = :ll_num_sequenza and
		cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		cod_versione_figlio = :ls_cod_versione_figlio and
		cod_reparto = :ls_cod_reparto_produzione	;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in disassociazione reparto connesso alla variante" + sqlca.sqlerrtext
	rollback;
	g_mb.error("APICE", ls_errore)
	return 1
end if

commit;

dw_varianti_ordini.postevent("rowfocuschanged")

end event

type cb_aggiungi_reparto from commandbutton within tabpage_4
integer x = 1344
integer y = 1580
integer width = 133
integer height = 100
integer taborder = 130
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = ">"
end type

event clicked;string					ls_cod_reparto_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio
long					ll_num_ordine, ll_riga_ordine, ll_num_sequenza, ll_row, ll_row_reparto
integer				li_anno_ordine


if ib_esiste_commessa then
	g_mb.error("APICE","Impossibile modificare: è presente una commessa associata alla riga!")
	return 1
end if


ll_row = dw_varianti_ordini.getrow()
if ll_row>0 then
else
	return
end if

ll_row_reparto = dw_lista_reparti.getrow()
if ll_row_reparto>0 then
else
	return
end if

li_anno_ordine = dw_varianti_ordini.getitemnumber(ll_row, "anno_registrazione") 
ll_num_ordine = dw_varianti_ordini.getitemnumber(ll_row, "num_registrazione") 
ll_riga_ordine = dw_varianti_ordini.getitemnumber(ll_row, "prog_riga_ord_ven") 
ls_cod_prodotto_padre = dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_padre") 
ls_cod_versione_padre = dw_varianti_ordini.getitemstring(ll_row, "cod_versione") 
ll_num_sequenza = dw_varianti_ordini.getitemnumber(ll_row, "num_sequenza") 
ls_cod_prodotto_figlio = dw_varianti_ordini.getitemstring(ll_row, "cod_prodotto_figlio") 
ls_cod_versione_figlio	 = dw_varianti_ordini.getitemstring(ll_row, "cod_versione_figlio") 

ls_cod_reparto_produzione = dw_lista_reparti.getitemstring(ll_row_reparto,"anag_reparti_cod_reparto")


insert into varianti_det_ord_ven_prod (
			cod_azienda,
			anno_registrazione,
			num_registrazione,
			prog_riga_ord_ven,
			cod_prodotto_padre,
			cod_versione,
			num_sequenza,
			cod_prodotto_figlio,
			cod_versione_figlio,
			cod_reparto			)
values	(:s_cs_xx.cod_azienda,
			:li_anno_ordine,
			:ll_num_ordine,
			:ll_riga_ordine,
			:ls_cod_prodotto_padre,
			:ls_cod_versione_padre,
			:ll_num_sequenza,
			:ls_cod_prodotto_figlio,
			:ls_cod_versione_figlio,
			:ls_cod_reparto_produzione			);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in associazione reparto connesso alla variante" + sqlca.sqlerrtext)
	return 1
end if

commit;

dw_varianti_ordini.postevent("rowfocuschanged")

end event

type dw_lista_reparti from datawindow within tabpage_4
integer x = 32
integer y = 1520
integer width = 1202
integer height = 420
integer taborder = 120
string title = "none"
string dataobject = "d_varianti_det_ord_ven_reparti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_varianti_ordini from uo_cs_xx_dw within tabpage_4
integer x = 18
integer y = 28
integer width = 2766
integer height = 1480
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_varianti_ordini"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if


choose case dwo.name
	case "b_ric_prod_variante"
		guo_ricerca.uof_ricerca_prodotto(dw_varianti_ordini,"cod_prodotto")
		
	case "b_ricalcola"
		wf_ricalcola_quan_utlizzo()
		
end choose

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile, ls_cod_deposito_produzione
	long ll_num_sequenza

	choose case i_colname
	
		case "cod_deposito_produzione"
			wf_filtra_reparti(data)
			
			ls_cod_deposito_produzione = getitemstring(getrow(), "cod_deposito_produzione") 
			dw_lista_reparti.settransobject(sqlca)
			dw_lista_reparti.retrieve(s_cs_xx.cod_azienda,ls_cod_deposito_produzione )			
	
		case "cod_prodotto"

			if len(i_coltext) < 1 then
				 PCCA.Error = c_ValFailed
				 return
			end if   
		
			// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
		
			long ll_handle
			string  ls_cod_prodotto, ls_cod_figlio, ls_descrizione
			treeviewitem  tvi_campo_2
			s_chiave_distinta l_chiave_distinta_2
			//numero del nodo da cui parto il_handle
			ll_handle = il_handle

			if not isnull(data) and (len(data) > 0) then
				select cod_prodotto
				into :ls_descrizione
				from anag_prodotti
				where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto= :i_coltext;
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
					return 1
				end if
			end if

			do while true
				//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
				tab_1.tabpage_1.tv_db.getItem (ll_handle, tvi_campo_2 )
				l_chiave_distinta_2 = tvi_campo_2.data
				if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
					//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
					ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
				else
					ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
				end if
				
				if upper(ls_cod_prodotto) = upper(i_coltext) then
					g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nCorreggere il dato.", stopsign!)
					return 1
				end if
				//cerca il padre del nodo numero ll_handle
				ll_handle = tab_1.tabpage_1.tv_db.findItem ( ParentTreeItem!	,  ll_handle)
				if ll_handle = -1 then exit   // allora sono alla radice
			
			loop
	
	//*********fine*** 	
		
	
			SELECT cod_misura_mag  
			INTO   :ls_cod_misura  
			FROM   anag_prodotti  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND   cod_prodotto = :i_coltext;

			if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
				setitem(getrow(), "cod_misura", ls_cod_misura)
			end if
			
		case "flag_esclusione"		
			if data = "S" then
				ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
				ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
				ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
				ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")
	
				select flag_escludibile
				 into :ls_flag_escludibile
				 from distinta
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto_padre = :ls_cod_prodotto_padre and
						num_sequenza = :ll_num_sequenza and
						cod_prodotto_figlio = :ls_cod_prodotto_figlio and
						cod_versione = :ls_cod_versione;
			
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Varianti Ordine di Vendita", "Errore durante l'Estrazione Flag Escludibile!")
					return 2
				end if
				
				if ls_flag_escludibile = "N" then
					g_mb.messagebox("Varianti Ordine di Vendita", "Il dettaglio Distinta non è escludibile!")
					return 2
				end if
			end if
	end choose

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	if ib_esiste_commessa then
		g_mb.warning("Attenzione: in presenza di una commessa la modifica non è ammessa")
		postevent("pcd_view")
	end if
	
	dw_varianti_ordini.object.b_ric_prod_variante.enabled=true
	tab_1.tabpage_1.dw_azioni.object.b_prezzo_variante.enabled=false

	dw_lista_reparti.enabled = false
	dw_reparti_variante.enabled=false
	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	//cb_inserisci.enabled = true
	//cb_verifica.enabled=false
	dw_varianti_ordini.object.b_ric_prod_variante.enabled=true
	tab_1.tabpage_1.dw_azioni.object.b_prezzo_variante.enabled=false

	dw_lista_reparti.enabled = false
	dw_reparti_variante.enabled=false

end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if il_handle>0 then
else
	return
end if

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(  s_cs_xx.cod_azienda, &
                    		  il_anno_registrazione, &
						  il_num_registrazione, &
						  il_prog_riga, &
						  l_chiave_distinta.cod_prodotto_padre, &
						  l_chiave_distinta.num_sequenza, &
						  l_chiave_distinta.cod_prodotto_figlio, &
						  l_chiave_distinta.cod_versione_padre, &
						  l_chiave_distinta.cod_versione_figlio)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	//cb_inserisci.enabled = false
	//cb_verifica.enabled=true
	
	dw_varianti_ordini.object.b_ric_prod_variante.enabled=false
	tab_1.tabpage_1.dw_azioni.object.b_prezzo_variante.enabled=true

	dw_lista_reparti.enabled = true
	dw_reparti_variante.enabled=true

end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	string ls_cod_deposito_produzione,ls_cod_prodotto_padre,ls_cod_versione_padre,ls_cod_prodotto_figlio,ls_cod_versione_figlio, ls_cod_reparto_prod,ls_cod_reparto
	long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_sequenza, ll_i, ll_y
	
	ls_cod_deposito_produzione = getitemstring(getrow(), "cod_deposito_produzione") 

	ll_anno_registrazione = getitemnumber(getrow(), "anno_registrazione") 
	ll_num_registrazione = getitemnumber(getrow(), "num_registrazione") 
	ll_prog_riga_ord_ven = getitemnumber(getrow(), "prog_riga_ord_ven") 
	ls_cod_prodotto_padre = getitemstring(getrow(), "cod_prodotto_padre") 
	ls_cod_versione_padre = getitemstring(getrow(), "cod_versione") 
	ll_num_sequenza = getitemnumber(getrow(), "num_sequenza") 
	ls_cod_prodotto_figlio = getitemstring(getrow(), "cod_prodotto_figlio") 
	ls_cod_versione_figlio	 = getitemstring(getrow(), "cod_versione_figlio") 
	dw_lista_reparti.reset()
	dw_reparti_variante.reset()
	
	if not isnull(ls_cod_deposito_produzione) and len(ls_cod_deposito_produzione) > 0 then
	
		dw_lista_reparti.enabled = true
		dw_reparti_variante.enabled=true

		dw_lista_reparti.setredraw(false)
		dw_reparti_variante.setredraw(false)
		dw_lista_reparti.settransobject(sqlca)
		dw_lista_reparti.SetRowFocusIndicator(hand!)
		dw_lista_reparti.retrieve(s_cs_xx.cod_azienda,ls_cod_deposito_produzione )
		

		dw_reparti_variante.settransobject(sqlca)
		dw_reparti_variante.SetRowFocusIndicator(hand!)
		dw_reparti_variante.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_Ven, ls_cod_prodotto_padre, ls_cod_Versione_padre, ll_num_sequenza, ls_cod_prodotto_figlio, ls_cod_versione_figlio )

		for ll_i = 1 to dw_reparti_variante.rowcount()
			
			ls_cod_reparto_prod = dw_reparti_variante.getitemstring(ll_i, "varianti_det_ord_ven_prod_cod_reparto")
			for ll_y = 1 to dw_lista_reparti.rowcount()
				
				 if ls_cod_reparto_prod = dw_lista_reparti.getitemstring(ll_y, "anag_reparti_cod_reparto") then
				 	dw_lista_reparti.deleterow(ll_y)
					ll_y --
				end if
				
			next
			
		next
		dw_lista_reparti.setredraw(true)
		dw_reparti_variante.setredraw(true)
		
	else
		
		dw_lista_reparti.enabled = false
		dw_reparti_variante.enabled=false
	end if
		
	
end if


end event

event updatestart;call super::updatestart;string			ls_cod_deposito_produzione, ls_cod_prodotto_padre, ls_cod_versione_padre, &
				ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_reparto, ls_errore
				
long			ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_num_sequenza, ll_index, ll_count

uo_calendario_prod_new				luo_prod


if i_extendmode then
	
	dwItemStatus l_status

	l_status = GetItemStatus(GetRow(), "cod_deposito_produzione", Primary!)
	
	if l_status <> NotModified! then
		
		ll_anno_registrazione = getitemnumber(getrow(), "anno_registrazione") 
		ll_num_registrazione = getitemnumber(getrow(), "num_registrazione") 
		ll_prog_riga_ord_ven = getitemnumber(getrow(), "prog_riga_ord_ven") 
		ls_cod_prodotto_padre = getitemstring(getrow(), "cod_prodotto_padre") 
		ls_cod_versione_padre = getitemstring(getrow(), "cod_versione") 
		ll_num_sequenza = getitemnumber(getrow(), "num_sequenza") 
		ls_cod_prodotto_figlio = getitemstring(getrow(), "cod_prodotto_figlio") 
		ls_cod_versione_figlio	 = getitemstring(getrow(), "cod_versione_figlio") 
		
		
		//disimpegna il calendario produzione -------------------------------------------------------------------------------------------------------
		luo_prod = create uo_calendario_prod_new
		
		for ll_index=1 to dw_reparti_variante.rowcount()
			ls_cod_reparto = dw_reparti_variante.getitemstring(ll_index, "varianti_det_ord_ven_prod_cod_reparto")
			
			setnull(ll_count)
			
			select count(*)
			into :ll_count
			from det_ord_ven_prod
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_prog_riga_ord_ven and
						cod_reparto = :ls_cod_reparto;
						
			if ll_count>0 then
				luo_prod.uof_disimpegna_calendario(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore)
			end if
			
		next
		
		destroy luo_prod
		//-------------------------------------------------------------------------------------------------------------------------------------------
		
		
		delete 	varianti_det_ord_ven_prod
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					cod_versione = :ls_cod_versione_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione_figlio = :ls_cod_versione_figlio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in cancellazione reparti connessi alla variante" + sqlca.sqlerrtext)
			return 1
		end if
		
		g_mb.warning("ATTENZIONE: è stato cambiato lo stabilimento e quindi è necessario reimpostare SUBITO il reparto!")
		
		postevent("rowfocuschanged")
		
		
	end if
	
end if
end event

type tabpage_5 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 104
integer width = 3493
integer height = 1948
long backcolor = 12632256
string text = "Integrazioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_integrazioni_det_ord_ven dw_integrazioni_det_ord_ven
end type

on tabpage_5.create
this.dw_integrazioni_det_ord_ven=create dw_integrazioni_det_ord_ven
this.Control[]={this.dw_integrazioni_det_ord_ven}
end on

on tabpage_5.destroy
destroy(this.dw_integrazioni_det_ord_ven)
end on

type dw_integrazioni_det_ord_ven from uo_cs_xx_dw within tabpage_5
integer x = 32
integer y = 52
integer width = 2400
integer height = 1316
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_integrazioni_det_ord_ven"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
if row>0 then
else
	return
end if

choose case dwo.name
	case "b_ric_prod_var"
		guo_ricerca.uof_ricerca_prodotto(dw_integrazioni_det_ord_ven,"cod_prodotto_figlio")
		
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile, ls_cod_versione_figlio
	long ll_num_sequenza

	choose case i_colname
	
	case "cod_prodotto_figlio"

		if len(i_coltext) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if   
	
		SELECT cod_misura_mag  
      INTO   :ls_cod_misura  
      FROM   anag_prodotti  
      WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	   AND   cod_prodotto = :i_coltext;

      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
         setitem(getrow(), "cod_misura", ls_cod_misura)
      end if
		
	case "flag_esclusione"		
		if data = "S" then
			ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
			ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
			ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
			ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")
			ls_cod_versione_figlio = this.getitemstring(this.getrow(), "cod_versione_figlio")

			select flag_escludibile
			 into :ls_flag_escludibile
			 from distinta
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione_figlio = :ls_cod_versione_figlio and
					cod_versione = :ls_cod_versione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Varianti Ordine di Vendita", "Errore durante l'Estrazione Flag Escludibile!")
				return 2
			end if
			
			if ls_flag_escludibile = "N" then
				g_mb.messagebox("Varianti Ordine di Vendita", "Il dettaglio Distinta non è escludibile!")
				return 2
			end if
		end if
	end choose

end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_integrazioni_det_ord_ven.object.b_ric_prod_integraz.enabled=true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	dw_integrazioni_det_ord_ven.object.b_ric_prod_integraz.enabled=true
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if il_handle>0 then
else
	return
end if

tab_1.tabpage_1.tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda, &
                   il_anno_registrazione,&
						 il_num_registrazione,&
						 il_prog_riga,&
						 l_chiave_distinta.num_sequenza)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_integrazioni_det_ord_ven.object.b_ric_prod_integraz.enabled=false
end if
end event

