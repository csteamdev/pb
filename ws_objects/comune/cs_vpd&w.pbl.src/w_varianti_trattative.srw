﻿$PBExportHeader$w_varianti_trattative.srw
$PBExportComments$Window varianti trattative
forward
global type w_varianti_trattative from w_cs_xx_principale
end type
type cb_crea from commandbutton within w_varianti_trattative
end type
type lb_elenco_campi from listbox within w_varianti_trattative
end type
type cb_inserisci from commandbutton within w_varianti_trattative
end type
type cb_verifica from commandbutton within w_varianti_trattative
end type
type st_1 from statictext within w_varianti_trattative
end type
type st_tempo from statictext within w_varianti_trattative
end type
type cb_crea_originale from commandbutton within w_varianti_trattative
end type
type cb_elimina_tutte from commandbutton within w_varianti_trattative
end type
type dw_varianti_dettaglio from uo_cs_xx_dw within w_varianti_trattative
end type
type tv_db from treeview within w_varianti_trattative
end type
type dw_folder from u_folder within w_varianti_trattative
end type
type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_varianti_trattative
end type
type dw_varianti_trattative from uo_cs_xx_dw within w_varianti_trattative
end type
type dw_varianti_lista from uo_cs_xx_dw within w_varianti_trattative
end type
type dw_distinta_det from uo_cs_xx_dw within w_varianti_trattative
end type
type s_chiave_distinta from structure within w_varianti_trattative
end type
end forward

type s_chiave_distinta from structure
	string		cod_prodotto_padre
	long		num_sequenza
	string		cod_prodotto_figlio
	double		quan_utilizzo
	string		cod_prodotto_variante
	double		quan_utilizzo_variante
end type

global type w_varianti_trattative from w_cs_xx_principale
integer width = 3314
integer height = 1600
string title = "Varianti Trattative"
cb_crea cb_crea
lb_elenco_campi lb_elenco_campi
cb_inserisci cb_inserisci
cb_verifica cb_verifica
st_1 st_1
st_tempo st_tempo
cb_crea_originale cb_crea_originale
cb_elimina_tutte cb_elimina_tutte
dw_varianti_dettaglio dw_varianti_dettaglio
tv_db tv_db
dw_folder dw_folder
dw_distinta_gruppi_varianti dw_distinta_gruppi_varianti
dw_varianti_trattative dw_varianti_trattative
dw_varianti_lista dw_varianti_lista
dw_distinta_det dw_distinta_det
end type
global w_varianti_trattative w_varianti_trattative

type variables
long il_handle
string is_cod_prodotto_finito,is_cod_versione
long  il_anno_trattativa,il_num_trattativa,il_prog_trattativa

end variables

forward prototypes
public function integer wf_inizio ()
public function integer wf_visual_testo_formula (string fs_cod_formula)
public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore)
end prototypes

public function integer wf_inizio ();string ls_errore,ls_des_prodotto
long ll_risposta
treeviewitem tvi_campo

s_chiave_distinta l_chiave_distinta
l_chiave_distinta.cod_prodotto_padre = is_cod_prodotto_finito
tv_db.setredraw(false)
tv_db.deleteitem(0)

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:l_chiave_distinta.cod_prodotto_padre;
ls_des_prodotto=trim(ls_des_prodotto)

tvi_campo.itemhandle = 1
tvi_campo.data = l_chiave_distinta
tvi_campo.label = l_chiave_distinta.cod_prodotto_padre + "," + ls_des_prodotto
tvi_campo.pictureindex = 1
tvi_campo.selectedpictureindex = 1
tvi_campo.overlaypictureindex = 1


ll_risposta=tv_db.insertitemlast(0, tvi_campo)

wf_imposta_tv(l_chiave_distinta.cod_prodotto_padre,1,1,ls_errore)

tv_db.expandall(1)
tv_db.setredraw(true)
tv_db.triggerevent("pcd_clicked")

return 0
end function

public function integer wf_visual_testo_formula (string fs_cod_formula);string ls_testo_formula

select testo_formula
 into  :ls_testo_formula
 from  tab_formule_db
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_formula = :fs_cod_formula;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Codice Formula Utilizzo", "Errore durante l'Estrazione Formule!")
	return -1
end if

dw_distinta_det.object.cf_testo_formula.text = f_sost_des_var_in_formula(ls_testo_formula, is_cod_prodotto_finito)
return 0
end function

public function integer wf_imposta_tv (string fs_cod_prodotto, integer fi_num_livello_cor, long fl_handle, ref string fs_errore);//	Funzione che imposta l'outliner
//
// nome: wf_imposta_ol
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//	Variabili passate: 		nome					 	tipo				passaggio per			commento
//							
//							 fs_cod_prodotto					string				valore
//							 fs_cod_prodotto_finito			string		  		valore             
//							 fi_num_livello_cor				integer				valore
//							 fl_handle							long   				valore
//							 fs_errore							string				riferimento
//
//		Creata il 12-05-97 
//		Autore Diego Ferrari

string  ls_cod_prodotto,ls_test_prodotto_f, ls_errore,ls_des_prodotto,ls_cod_versione, &
		  ls_cod_prodotto_variante,ls_des_prodotto_variante,ls_flag_materia_prima, & 
  		  ls_flag_materia_prima_variante
long    ll_num_figli,ll_handle,ll_num_righe
integer li_num_priorita,li_risposta
decimal  ldd_quan_utilizzo_variante
boolean lb_flag_variante
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo
datastore lds_righe_distinta

lb_flag_variante = false
ll_num_figli = 1
lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,is_cod_versione)
	
for ll_num_figli=1 to ll_num_righe
	l_chiave_distinta.cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	l_chiave_distinta.quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,"quan_utilizzo")
	l_chiave_distinta.num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	l_chiave_distinta.cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

	select cod_prodotto,
			 quan_utilizzo,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,
			 :ldd_quan_utilizzo_variante,
			 :ls_flag_materia_prima_variante
	from   varianti_det_trattative
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_trattativa=:il_anno_trattativa
	and    num_trattativa=:il_num_trattativa
	and    prog_trattativa=:il_prog_trattativa
	and    cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_padre
	and    num_sequenza=:l_chiave_distinta.num_sequenza
	and    cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio
	and    cod_versione =:is_cod_versione;

	if sqlca.sqlcode = 0 then
		l_chiave_distinta.cod_prodotto_variante = ls_cod_prodotto_variante
		l_chiave_distinta.quan_utilizzo_variante = ldd_quan_utilizzo_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante

	   select des_prodotto
		into   :ls_des_prodotto_variante
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:l_chiave_distinta.cod_prodotto_variante;

		ls_des_prodotto=trim(ls_des_prodotto_variante)

	   lb_flag_variante = true
	else
	   lb_flag_variante = false
	end if

   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:l_chiave_distinta.cod_prodotto_figlio;

	ls_des_prodotto=trim(ls_des_prodotto)

	tvi_campo.itemhandle = fl_handle
	tvi_campo.data = l_chiave_distinta
	
	if lb_flag_variante = true then
		tvi_campo.label = l_chiave_distinta.cod_prodotto_variante + "," + ls_des_prodotto_variante + " qtà:" + string(l_chiave_distinta.quan_utilizzo_variante)
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda
		and	 cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_variante;

	else
		tvi_campo.label = l_chiave_distinta.cod_prodotto_figlio + "," + ls_des_prodotto + " qtà:" + string(l_chiave_distinta.quan_utilizzo)
		
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda
		and	 cod_prodotto_padre=:l_chiave_distinta.cod_prodotto_figlio;

	end if

	if ls_flag_materia_prima = 'N' then
		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			tvi_campo.pictureindex = 3
			tvi_campo.selectedpictureindex = 3
			tvi_campo.overlaypictureindex = 3
			ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
			continue
		else
			tvi_campo.pictureindex = 2
			tvi_campo.selectedpictureindex = 2
			tvi_campo.overlaypictureindex = 2
			ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
			setnull(ls_test_prodotto_f)
		
			if lb_flag_variante = true then
				li_risposta=wf_imposta_tv(l_chiave_distinta.cod_prodotto_variante, fi_num_livello_cor + 1,ll_handle,ls_errore)
				lb_flag_variante = false
			else
				li_risposta=wf_imposta_tv(l_chiave_distinta.cod_prodotto_figlio, fi_num_livello_cor + 1,ll_handle,ls_errore)				
			end if
		
		end if
	else
		tvi_campo.pictureindex = 3
		tvi_campo.selectedpictureindex = 3
		tvi_campo.overlaypictureindex = 3
		ll_handle=tv_db.insertitemlast(fl_handle, tvi_campo)
		continue
	end if	
next

destroy(lds_righe_distinta)

return 0
end function

on w_varianti_trattative.create
int iCurrent
call super::create
this.cb_crea=create cb_crea
this.lb_elenco_campi=create lb_elenco_campi
this.cb_inserisci=create cb_inserisci
this.cb_verifica=create cb_verifica
this.st_1=create st_1
this.st_tempo=create st_tempo
this.cb_crea_originale=create cb_crea_originale
this.cb_elimina_tutte=create cb_elimina_tutte
this.dw_varianti_dettaglio=create dw_varianti_dettaglio
this.tv_db=create tv_db
this.dw_folder=create dw_folder
this.dw_distinta_gruppi_varianti=create dw_distinta_gruppi_varianti
this.dw_varianti_trattative=create dw_varianti_trattative
this.dw_varianti_lista=create dw_varianti_lista
this.dw_distinta_det=create dw_distinta_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_crea
this.Control[iCurrent+2]=this.lb_elenco_campi
this.Control[iCurrent+3]=this.cb_inserisci
this.Control[iCurrent+4]=this.cb_verifica
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_tempo
this.Control[iCurrent+7]=this.cb_crea_originale
this.Control[iCurrent+8]=this.cb_elimina_tutte
this.Control[iCurrent+9]=this.dw_varianti_dettaglio
this.Control[iCurrent+10]=this.tv_db
this.Control[iCurrent+11]=this.dw_folder
this.Control[iCurrent+12]=this.dw_distinta_gruppi_varianti
this.Control[iCurrent+13]=this.dw_varianti_trattative
this.Control[iCurrent+14]=this.dw_varianti_lista
this.Control[iCurrent+15]=this.dw_distinta_det
end on

on w_varianti_trattative.destroy
call super::destroy
destroy(this.cb_crea)
destroy(this.lb_elenco_campi)
destroy(this.cb_inserisci)
destroy(this.cb_verifica)
destroy(this.st_1)
destroy(this.st_tempo)
destroy(this.cb_crea_originale)
destroy(this.cb_elimina_tutte)
destroy(this.dw_varianti_dettaglio)
destroy(this.tv_db)
destroy(this.dw_folder)
destroy(this.dw_distinta_gruppi_varianti)
destroy(this.dw_varianti_trattative)
destroy(this.dw_varianti_lista)
destroy(this.dw_distinta_det)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects_1[]
windowobject l_objects_2[]
windowobject l_objects_3[]
windowobject l_objects_4[]

l_objects_1[1] = tv_db
l_objects_1[2] = cb_crea_originale
l_objects_1[3] = cb_elimina_tutte
dw_folder.fu_AssignTab(1, "&Struttura", l_Objects_1[])
l_objects_2[1] = dw_distinta_det
//l_objects_2[2] = cb_ricerca_figlio_dist
dw_folder.fu_AssignTab(2, "&Dettaglio", l_Objects_2[])
l_objects_3[1] = dw_distinta_gruppi_varianti
l_objects_3[2] = dw_varianti_lista
l_objects_3[3] = dw_varianti_dettaglio
l_objects_3[4] = cb_crea
dw_folder.fu_AssignTab(3, "&Varianti Disponibili", l_Objects_3[])
l_objects_4[1] = dw_varianti_trattative
l_objects_4[2] = cb_inserisci
l_objects_4[3] = cb_verifica
l_objects_4[4] = lb_elenco_campi
l_objects_4[5] = st_1
l_objects_4[6] = st_tempo
//l_objects_4[7] = cb_ricerca_prodotto_variante
dw_folder.fu_AssignTab(4, "&Varianti Trattative", l_Objects_4[])

dw_distinta_det.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew +c_nodelete + c_nomodify,c_default)

dw_distinta_gruppi_varianti.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)

dw_varianti_trattative.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew,c_default)

dw_varianti_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete,c_default)

dw_varianti_dettaglio.set_dw_options(sqlca,dw_varianti_lista,c_sharedata + c_scrollparent + c_nonew + c_nomodify + c_nodelete,c_default)


uo_dw_main=dw_distinta_det


dw_folder.fu_FolderCreate(4,4)
dw_folder.fu_SelectTab(1)

is_cod_prodotto_finito=s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_prodotto")
is_cod_versione=s_cs_xx.parametri.parametro_dw_1.getitemstring(s_cs_xx.parametri.parametro_dw_1.getrow(),"cod_versione")
il_anno_trattativa=s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"anno_trattativa")
il_num_trattativa=s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"num_trattativa")
il_prog_trattativa = s_cs_xx.parametri.parametro_dw_1.getitemnumber(s_cs_xx.parametri.parametro_dw_1.getrow(),"prog_trattativa")

wf_inizio()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_distinta_det,"cod_misura",sqlca,&
                 "tab_misure","cod_misura","des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//f_PO_LoadDDDW_DW(dw_distinta_det,"cod_prodotto_figlio",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_distinta_det,"cod_prodotto_padre",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//
f_PO_LoadDDDW_DW(dw_distinta_gruppi_varianti,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_varianti_trattative,"cod_prodotto_figlio",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//f_PO_LoadDDDW_DW(dw_varianti_trattative,"cod_prodotto_padre",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
f_PO_LoadDDDW_DW(dw_varianti_trattative,"cod_prodotto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_crea from commandbutton within w_varianti_trattative
integer x = 2853
integer y = 1360
integer width = 361
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Var."
end type

event clicked;string ls_cod_misura,ls_cod_prodotto_variante,ls_test,ls_des_estesa,ls_flag_materia_prima
decimal ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if dw_distinta_gruppi_varianti.rowcount() <= 0 then
	g_mb.messagebox("Sep","Non esistono varianti preconfigurate per questo prodotto.",information!)
	return
end if

if il_handle = 1 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
	return
end if

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

ls_cod_misura = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_misura")
ldd_quan_tecnica = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_tecnica")
ldd_quan_utilizzo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"quan_utilizzo") 
ldd_dim_x = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_x")
ldd_dim_y = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_y")
ldd_dim_z = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_z")
ldd_dim_t = dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"dim_t")
ldd_coef_calcolo= dw_varianti_dettaglio.getitemnumber(dw_varianti_dettaglio.getrow(),"coef_calcolo")
ls_cod_prodotto_variante = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"cod_prodotto")
ls_des_estesa = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"des_estesa")
ls_flag_materia_prima = dw_varianti_dettaglio.getitemstring(dw_varianti_dettaglio.getrow(),"flag_materia_prima")

select cod_azienda
into   :ls_test
from   varianti_det_trattative
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_trattativa=:il_anno_trattativa
and    num_trattativa=:il_num_trattativa
and    prog_trattativa=:il_prog_trattativa
and    cod_prodotto_padre =:l_chiave_distinta.cod_prodotto_padre
and    num_sequenza=:l_chiave_distinta.num_sequenza
and    cod_prodotto_figlio=:l_chiave_distinta.cod_prodotto_figlio;

if sqlca.sqlcode <=0 then
	g_mb.messagebox("Sep","Attenzione! Esiste già una variante per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
	return
end if

insert into varianti_det_trattative
(cod_azienda,
 anno_trattativa,
 num_trattativa,
 prog_trattativa,
 cod_prodotto_padre,
 num_sequenza,
 cod_prodotto_figlio,
 cod_versione,
 cod_prodotto,
 cod_misura,
 quan_tecnica,
 quan_utilizzo,
 dim_x,
 dim_y,
 dim_z,
 dim_t,
 coef_calcolo,
 flag_esclusione,
 des_estesa,
 flag_materia_prima)
values
(:s_cs_xx.cod_azienda,
 :il_anno_trattativa,
 :il_num_trattativa,
 :il_prog_trattativa,
 :l_chiave_distinta.cod_prodotto_padre,
 :l_chiave_distinta.num_sequenza,
 :l_chiave_distinta.cod_prodotto_figlio,
 :is_cod_versione,
 :ls_cod_prodotto_variante, 
 :ls_cod_misura,
 :ldd_quan_tecnica,
 :ldd_quan_utilizzo,
 :ldd_dim_x,
 :ldd_dim_y,
 :ldd_dim_z,
 :ldd_dim_t,
 :ldd_coef_calcolo,
 'N',
 :ls_des_estesa,
 :ls_flag_materia_prima);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti :" + sqlca.sqlerrtext ,stopsign!)
	return
end if

dw_varianti_trattative.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

wf_inizio()

g_mb.messagebox("Sep","Creazione variante avvenuta con successo",information!)
end event

type lb_elenco_campi from listbox within w_varianti_trattative
integer x = 2510
integer y = 160
integer width = 704
integer height = 480
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
string item[] = {"QUAN_TECNICA","QUAN_UTILIZZO","DIM_X","DIM_Y","DIM_Z","DIM_T","COEF_CALCOLO"}
borderstyle borderstyle = stylelowered!
end type

type cb_inserisci from commandbutton within w_varianti_trattative
event clicked pbm_bnclicked
integer x = 2510
integer y = 660
integer width = 361
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Inserisci"
end type

event clicked;string ls_formula

dw_varianti_trattative.setcolumn("formula_tempo")

ls_formula = dw_varianti_trattative.gettext()

if isnull(ls_formula) then ls_formula=""

ls_formula = ls_formula + " " + lb_elenco_campi.selecteditem() + " "

dw_varianti_trattative.settext(ls_formula)


end event

type cb_verifica from commandbutton within w_varianti_trattative
event clicked pbm_bnclicked
integer x = 2510
integer y = 756
integer width = 361
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Verifica"
end type

event clicked;string ls_campo,ls_formula,ls_risultato
double ldd_valore_campo
integer li_i,li_risposta

ls_formula = dw_varianti_trattative.getitemstring(dw_varianti_trattative.getrow(),"formula_tempo")

for li_i = 1 to 7
	lb_elenco_campi.selectitem(li_i)
	ls_campo = lb_elenco_campi.selecteditem()
	if pos(ls_formula,ls_campo) <> 0 then
		ldd_valore_campo  = dw_varianti_trattative.getitemnumber(dw_varianti_trattative.getrow(),ls_campo)
		if isnull(ldd_valore_campo) then ldd_valore_campo=0
		li_risposta = f_sostituzione(ls_formula, ls_campo, string(ldd_valore_campo))
	end if
next

li_risposta = f_if(ls_formula)

if li_risposta < 0 then
	g_mb.messagebox("Sep",ls_formula,stopsign!)
	return
end if

ls_risultato = f_parser_parentesi(ls_formula)
if ls_risultato = "Errore" then
	g_mb.messagebox("Sep","Attenzione le librerie per il calcolo delle formule trigonometriche non sono installate correttamente. Verificare.",stopsign!)
	return -1
end if
st_tempo.text = ls_risultato
end event

type st_1 from statictext within w_varianti_trattative
integer x = 2510
integer y = 900
integer width = 690
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Tempo Calcolato in minuti:"
boolean focusrectangle = false
end type

type st_tempo from statictext within w_varianti_trattative
integer x = 2510
integer y = 996
integer width = 521
integer height = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_crea_originale from commandbutton within w_varianti_trattative
event clicked pbm_bnclicked
integer x = 87
integer y = 1360
integer width = 361
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Crea Var."
end type

event clicked;string ls_cod_misura,ls_cod_prodotto_variante,ls_test,ls_formula_tempo,ls_des_estesa, & 
		 ls_flag_materia_prima
decimal ldd_quan_tecnica,ldd_quan_utilizzo,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_coef_calcolo
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

if il_handle = 1 then
	g_mb.messagebox("Sep","Attenzione! Il prodotto finito non può essere associato ad una variante!",exclamation!)
	return
end if

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

select cod_azienda
into   :ls_test
from   varianti_det_trattative
where  cod_azienda = :s_cs_xx.cod_azienda
and    anno_trattativa = :il_anno_trattativa
and    num_trattativa = :il_num_trattativa
and    prog_trattativa=:il_prog_trattativa
and    cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre
and    num_sequenza = :l_chiave_distinta.num_sequenza
and    cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio;

if sqlca.sqlcode <=0 then
	g_mb.messagebox("Sep","Attenzione! Esiste già una variante per questo prodotto. Se si vuole cambiarla, eliminare la variante corrente e salvare la modifica, quindi ripetere l'operazione.",information!)
	return
end if

select cod_misura,
		 quan_tecnica,
		 quan_utilizzo,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 coef_calcolo,
		 formula_tempo,	
		 des_estesa,
		 flag_materia_prima
into   :ls_cod_misura,
		 :ldd_quan_tecnica,
		 :ldd_quan_utilizzo,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ldd_coef_calcolo,
		 :ls_formula_tempo,
		 :ls_des_estesa,
		 :ls_flag_materia_prima
from   distinta
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto_padre = :l_chiave_distinta.cod_prodotto_padre
and    num_sequenza = :l_chiave_distinta.num_sequenza
and    cod_prodotto_figlio = :l_chiave_distinta.cod_prodotto_figlio
and    cod_versione=:is_cod_versione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti :" + sqlca.sqlerrtext ,stopsign!)
	return
end if

insert into varianti_det_trattative
(cod_azienda,
 anno_trattativa,
 num_trattativa,
 prog_trattativa,
 cod_prodotto_padre,
 num_sequenza,
 cod_prodotto_figlio,
 cod_versione,
 cod_prodotto,
 cod_misura,
 quan_tecnica,
 quan_utilizzo,
 dim_x,
 dim_y,
 dim_z,
 dim_t,
 coef_calcolo,
 formula_tempo,
 flag_esclusione,
 des_estesa,
 flag_materia_prima)
values
(:s_cs_xx.cod_azienda,
 :il_anno_trattativa,
 :il_num_trattativa,
 :il_prog_trattativa,
 :l_chiave_distinta.cod_prodotto_padre,
 :l_chiave_distinta.num_sequenza,
 :l_chiave_distinta.cod_prodotto_figlio,
 :is_cod_versione,
 :l_chiave_distinta.cod_prodotto_figlio, 
 :ls_cod_misura,
 :ldd_quan_tecnica,
 :ldd_quan_utilizzo,
 :ldd_dim_x,
 :ldd_dim_y,
 :ldd_dim_z,
 :ldd_dim_t,
 :ldd_coef_calcolo,
 :ls_formula_tempo,
 'N',
 :ls_des_estesa,
 :ls_flag_materia_prima);

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Attenzione! Errore in inserimento Varianti :" + sqlca.sqlerrtext ,stopsign!)
	return
end if

dw_varianti_trattative.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

wf_inizio()

g_mb.messagebox("Sep","Creazione variante avvenuta con successo",information!)
end event

type cb_elimina_tutte from commandbutton within w_varianti_trattative
integer x = 480
integer y = 1360
integer width = 681
integer height = 80
integer taborder = 71
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina Tutte le Varianti"
end type

event clicked;if g_mb.messagebox("Sep", "Confermi l'eliminazione di tutte le varianti?",Question!, YesNo!, 2) = 1 then

	delete from varianti_det_trattative
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_trattativa=:il_anno_trattativa
	and   num_trattativa=:il_num_trattativa
	and   prog_trattativa=:il_prog_trattativa;
	
	if sqlca.sqlcode <0 then
		g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
	
	wf_inizio()

end if
end event

type dw_varianti_dettaglio from uo_cs_xx_dw within w_varianti_trattative
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
integer x = 87
integer y = 540
integer width = 2738
integer height = 916
integer taborder = 90
string dataobject = "d_varianti_dettaglio"
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre,l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type tv_db from treeview within w_varianti_trattative
integer x = 64
integer y = 140
integer width = 3127
integer height = 1200
integer taborder = 140
string dragicon = "Exclamation!"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
boolean disabledragdrop = false
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

event constructor;this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "pf.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "semil.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "mp.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "integrazione.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "variante.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_aperta.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_in_corso.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_chiusa.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "fase_esterna.bmp")
this.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "ramo_descrittivo.bmp")

end event

event clicked;if handle<>0 then
	il_handle = handle
	dw_distinta_det.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_distinta_gruppi_varianti.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
	dw_varianti_trattative.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
end if
end event

type dw_folder from u_folder within w_varianti_trattative
integer x = 18
integer y = 20
integer width = 3241
integer height = 1460
integer taborder = 20
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;choose case i_selectedtab
	case 1
		tv_db.setfocus()
		tv_db.triggerevent("clicked")
	case 2
		wf_visual_testo_formula(dw_distinta_det.getitemstring(dw_distinta_det.getrow(), "cod_formula_quan_utilizzo"))
end choose
end event

type dw_distinta_gruppi_varianti from uo_cs_xx_dw within w_varianti_trattative
event pcd_retrieve pbm_custom60
integer x = 87
integer y = 140
integer width = 1893
integer height = 400
integer taborder = 40
string dataobject = "d_distinta_gruppi_varianti"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre,l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

if rowcount()>0 then
	dw_varianti_lista.Change_DW_Current( )
	parent.triggerevent("pc_retrieve")
else
	dw_varianti_lista.reset()
	dw_varianti_dettaglio.reset()
end if
end event

event pcd_pickedrow;call super::pcd_pickedrow;dw_varianti_lista.Change_DW_Current( )
parent.triggerevent("pc_retrieve")

end event

type dw_varianti_trattative from uo_cs_xx_dw within w_varianti_trattative
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
integer x = 87
integer y = 140
integer width = 2400
integer height = 1316
integer taborder = 60
string dataobject = "d_varianti_trattative"
borderstyle borderstyle = styleraised!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	cb_inserisci.enabled = true
//	cb_ricerca_prodotto_variante.enabled=true
	cb_verifica.enabled=false
end if
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,il_anno_trattativa,il_num_trattativa,il_prog_trattativa,l_chiave_distinta.cod_prodotto_padre,l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_inserisci.enabled = true
//	cb_ricerca_prodotto_variante.enabled=true
	cb_verifica.enabled=false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_inserisci.enabled = false
//	cb_ricerca_prodotto_variante.enabled=false
	cb_verifica.enabled=true
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	wf_inizio()
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_misura, ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_flag_escludibile
	long ll_num_sequenza

	choose case i_colname
	
	case "cod_prodotto"

		if len(i_coltext) < 1 then
          PCCA.Error = c_ValFailed
          return
      end if   
	// claudia  controllo che non ci sia un padre con lo stesso prodotto 22/02/06
	
		long ll_handle
		string  ls_cod_prodotto, ls_cod_figlio, ls_descrizione
		treeviewitem  tvi_campo_2
		s_chiave_distinta l_chiave_distinta_2
		//numero del nodo da cui parto il_handle
		ll_handle = il_handle

		if not isnull(data) and (len(data) > 0) then
			select cod_prodotto
			into :ls_descrizione
			from anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto= :i_coltext;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Sep","Codice prodotto inesistente!~r~nCorreggere il dato."+ sqlca.sqlerrtext)	
				return 1
			end if
		end if

		do while true
			//carico i dati del nodo numero ll_handle nel nodo tvi_campo_2
			tv_db.getItem (ll_handle, tvi_campo_2 )
			l_chiave_distinta_2 = tvi_campo_2.data
			if isnull(l_chiave_distinta_2.cod_prodotto_figlio) or len(l_chiave_distinta_2.cod_prodotto_figlio) < 1 then
				//il ramo di radice ha il codice solo sul padre e il figlio è vuoto
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_padre
			else
				ls_cod_prodotto = l_chiave_distinta_2.cod_prodotto_figlio
			end if
			
			if upper(ls_cod_prodotto) = upper(i_coltext) then
				g_mb.messagebox("Sep","Impossibile inserire un prodotto come figlio di se stesso!~r~nCorreggere il dato.", stopsign!)
				return 1
			end if
			//cerca il padre del nodo numero ll_handle
			ll_handle = tv_db.findItem ( ParentTreeItem!	,  ll_handle)
			if ll_handle = -1 then exit   // allora sono alla radice
		
		loop

//*********fine*** 	
		
	
		SELECT cod_misura_mag  
      INTO   :ls_cod_misura  
      FROM   anag_prodotti  
      WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	   AND   cod_prodotto = :i_coltext;

      if len(ls_cod_misura) > 0  and not isnull(ls_cod_misura) then
         setitem(getrow(), "cod_misura", ls_cod_misura)
      end if
	case "flag_esclusione"		
		if data = "S" then
			ls_cod_prodotto_padre = this.getitemstring(this.getrow(), "cod_prodotto_padre")
			ll_num_sequenza = this.getitemnumber(this.getrow(), "num_sequenza")
			ls_cod_prodotto_figlio = this.getitemstring(this.getrow(), "cod_prodotto_figlio")
			ls_cod_versione = this.getitemstring(this.getrow(), "cod_versione")

			select flag_escludibile
			 into :ls_flag_escludibile
			 from distinta
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto_padre = :ls_cod_prodotto_padre and
					num_sequenza = :ll_num_sequenza and
					cod_prodotto_figlio = :ls_cod_prodotto_figlio and
					cod_versione = :ls_cod_versione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Varianti Trattativa", "Errore durante l'Estrazione Flag Escludibile!")
				return 2
			end if
			
			if ls_flag_escludibile = "N" then
				g_mb.messagebox("Varianti Trattativa", "Il dettaglio Distinta non è escludibile!")
				return 2
			end if
		end if
	end choose

end if
end event

type dw_varianti_lista from uo_cs_xx_dw within w_varianti_trattative
event pcd_new pbm_custom52
event pcd_retrieve pbm_custom60
integer x = 1984
integer y = 140
integer width = 1138
integer height = 400
integer taborder = 130
string dataobject = "d_varianti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_gruppo_variante

ls_cod_gruppo_variante = dw_distinta_gruppi_varianti.getitemstring(dw_distinta_gruppi_varianti.getrow(),"cod_gruppo_variante")
l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_gruppo_variante)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

type dw_distinta_det from uo_cs_xx_dw within w_varianti_trattative
integer x = 87
integer y = 140
integer width = 2350
integer height = 1296
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_distinta_det"
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
s_chiave_distinta l_chiave_distinta
treeviewitem tvi_campo

tv_db.GetItem ( il_handle, tvi_campo )
l_chiave_distinta = tvi_campo.data

l_Error = Retrieve(s_cs_xx.cod_azienda,l_chiave_distinta.cod_prodotto_padre, & 
						 l_chiave_distinta.num_sequenza,l_chiave_distinta.cod_prodotto_figlio,is_cod_versione)
IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF



end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_sequenza
	string ls_cod_prodotto_padre
	ib_proteggi_chiavi=false
	s_chiave_distinta l_chiave_distinta
	treeviewitem tvi_campo

	tv_db.GetItem ( il_handle, tvi_campo )
	l_chiave_distinta = tvi_campo.data
//   cb_ricerca_figlio_dist.enabled=true
	
	if l_chiave_distinta.cod_prodotto_figlio="" then
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_padre
	else
		ls_cod_prodotto_padre = l_chiave_distinta.cod_prodotto_figlio
	end if
	
	setitem(getrow(), "cod_prodotto_padre",ls_cod_prodotto_padre)		
   triggerevent("itemchanged")

   select max(distinta.num_sequenza)
   into   :ll_sequenza
   from   distinta
   where  distinta.cod_azienda = :s_cs_xx.cod_azienda and 
          distinta.cod_prodotto_padre = :ls_cod_prodotto_padre;

   if isnull(ll_sequenza) then ll_sequenza = 0
   ll_sequenza = ll_sequenza + 10
   setitem(getrow(), "num_sequenza", ll_sequenza)
	triggerevent("itemchanged")
   setcolumn("cod_prodotto_figlio")
	wf_visual_testo_formula(this.getitemstring(this.getrow(), "cod_formula_quan_utilizzo"))
end if

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	
	ib_proteggi_chiavi=false
	
//   cb_ricerca_figlio_dist.enabled=true
	
end if
end event

event pcd_saveafter;call super::pcd_saveafter;if i_extendmode then
	long ll_risposta
	
		ll_risposta=wf_inizio()	
	
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)	
		SetItem(l_Idx, "cod_versione", is_cod_versione)	
   END IF
NEXT

end event

event pcd_delete;call super::pcd_delete;triggerevent("pcd_save")
end event

event pcd_view;call super::pcd_view;//if i_extendmode then
//	
//   cb_ricerca_figlio_dist.enabled=false	
//	
//end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_padre"
		guo_ricerca.uof_ricerca_prodotto(dw_distinta_det,"cod_prodotto_padre")
	case "b_ricerca_figlio"
		guo_ricerca.uof_ricerca_prodotto(dw_distinta_det,"cod_prodotto_figlio")
end choose
end event

