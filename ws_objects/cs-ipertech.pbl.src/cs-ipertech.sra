﻿$PBExportHeader$cs-ipertech.sra
$PBExportComments$Generated Application Object
forward
global type cs-ipertech from application
end type
global n_tran sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
pcca pcca
s_cs_xx s_cs_xx
str_conf_prodotto str_conf_prodotto
string r_divisione, gs_sessione,gs_licenze,gs_chiave
n_tran sqlci
integer gi_licenze

// variabili globali per algoritmo crypt/decrypt password
string	gs_crypt_accepted_chars[100]
int		gl_crypt_code_table[100,5]

// oggetto non-visual globale per messagbox custom
nvo_messagebox g_mb

// aggiunto per compatibilità con oggetti client configuratore
string	gs_cod_cliente

// oggetto per la gestione dei temi
s_themes s_themes

//stefanop: variabile di sessione
datetime gdt_session_start_at
uo_application guo_application
uo_ricerca guo_ricerca
uo_functions guo_functions
uo_str g_str

end variables

global type cs-ipertech from application
string appname = "cs-ipertech"
end type
global cs-ipertech cs-ipertech

type prototypes
FUNCTION Long GetTempPath(Long nBufferLength, REF String lpBuffer) LIBRARY "KERNEL32.DLL" ALIAS FOR "GetTempPathA;Ansi"

// stefanop 03/09/2010: apertura documenti per w_ole_v2
FUNCTION long ShellExecuteA(long hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, long nShowCmd) LIBRARY "SHELL32.DLL" ALIAS FOR "ShellExecuteA;ansi"
end prototypes

on cs-ipertech.create
appname="cs-ipertech"
message=create message
sqlca=create n_tran
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on cs-ipertech.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event systemerror;f_cs_xx_errore()
end event

event open;guo_application = create uo_application
guo_functions = create uo_functions
guo_ricerca = create uo_ricerca
g_mb = create nvo_messagebox
g_str = create uo_str

guo_application.uof_inizio(this, "", "", "", "", true, "", w_cs_xx_mdi)
end event

event close;guo_application.uof_fine()

destroy guo_ricerca
destroy guo_functions
destroy guo_application
destroy g_mb
destroy g_str

halt close
end event

