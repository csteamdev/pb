﻿$PBExportHeader$w_report_off_acq_euro.srw
$PBExportComments$Stampa Offerta Acquisto
forward
global type w_report_off_acq_euro from w_cs_xx_principale
end type
type dw_report_off_acq from uo_cs_xx_dw within w_report_off_acq_euro
end type
end forward

global type w_report_off_acq_euro from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Offerte Acquisto"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_off_acq dw_report_off_acq
end type
global w_report_off_acq_euro w_report_off_acq_euro

type variables
boolean ib_modifica=false, ib_nuovo=false
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

public subroutine wf_report ();string 	ls_stringa, ls_cod_tipo_off_acq, ls_cod_banca_clien_for, ls_cod_fornitore, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_off_fornitore, ls_cod_porto, ls_cod_resa, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_for, ls_rag_soc_2_for, &
		 	ls_indirizzo_for, ls_localita_for, ls_frazione_for, ls_cap_for, ls_provincia_for, &
		 	ls_partita_iva_for, ls_cod_fiscale_for, ls_cod_lingua, ls_flag_tipo_fornitore, &
		 	ls_des_tipo_off_acq, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , &
		 	ls_des_prodotto_anag, ls_flag_stampa_offerta, ls_cod_tipo_det_acq, &
		 	ls_des_prodotto_lingua, ls_cod_prod_fornitore, ls_cod_for_pot, ls_cod_fil_fornitore, &
		 	ls_rag_soc_1_for_pot, ls_rag_soc_2_for_pot, ls_indirizzo_for_pot, &
		 	ls_localita_for_pot, ls_frazione_for_pot, ls_cap_for_pot, ls_provincia_for_pot, &
		 	ls_partita_iva_for_pot, ls_cod_fiscale_for_pot, ls_cod_lingua_for_pot, &
		 	ls_flag_tipo_fornitore_for_pot, ls_rag_soc_1_fil, ls_rag_soc_2_fil, ls_indirizzo_fil, &
		 	ls_localita_fil, ls_frazione_fil, ls_cap_fil, ls_provincia_fil, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_formato, &
			ls_rif_interno, ls_cod_operatore, ls_des_operatore, ls_cod_vettore, ls_des_vettore, &
			ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, ls_flag, &
			ls_telefono_for, ls_fax_for, ls_telefono_fil, ls_fax_fil, ls_telefono_for_pot, ls_fax_for_pot
		 
long 	 	ll_errore, ll_anno_registrazione, ll_num_registrazione

dec{4} 	ld_tot_val_offerta, ld_sconto, ld_quan_ordinata, ld_prezzo_acquisto, ld_sconto_1, &
	    	ld_sconto_2, ld_val_riga, ld_sconto_pagamento
			 
dec{5}   ld_fat_conversione
		 
datetime ldt_data_off_fornitore, ldt_data_registrazione, ldt_data_consegna, ldt_data_scadenza


dw_report_off_acq.reset()

ll_anno_registrazione = dw_report_off_acq.i_parentdw.getitemnumber(dw_report_off_acq.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_report_off_acq.i_parentdw.getitemnumber(dw_report_off_acq.i_parentdw.i_selectedrows[1], "num_registrazione")

dw_report_off_acq.set_document_name("Offerta di Acquisto " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) )

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select tes_off_acq.cod_tipo_off_acq,   
		 tes_off_acq.data_registrazione,   
		 tes_off_acq.cod_fornitore,   
		 tes_off_acq.cod_valuta,   
		 tes_off_acq.cod_pagamento,   
		 tes_off_acq.sconto,   
		 tes_off_acq.cod_banca_clien_for,   
		 tes_off_acq.num_off_fornitore,   
		 tes_off_acq.data_off_fornitore,   
		 tes_off_acq.cod_porto,   
		 tes_off_acq.cod_resa,   
		 tes_off_acq.nota_testata,   
		 tes_off_acq.nota_piede,   
		 tes_off_acq.tot_val_offerta,
		 tes_off_acq.data_scadenza,
		 tes_off_acq.cod_for_pot,
		 tes_off_acq.cod_fil_fornitore,
		 tes_off_acq.flag_st_note_tes,
		 tes_off_acq.flag_st_note_pie,
		 cod_operatore,
		 cod_vettore,
		 cod_mezzo
into   :ls_cod_tipo_off_acq,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_fornitore,   
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_off_fornitore,   
		 :ldt_data_off_fornitore,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_offerta,
		 :ldt_data_scadenza,
		 :ls_cod_for_pot,
		 :ls_cod_fil_fornitore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_cod_operatore,
		 :ls_cod_vettore,
		 :ls_cod_mezzo
from   tes_off_acq  
where  tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_off_acq.anno_registrazione = :ll_anno_registrazione and 
		 tes_off_acq.num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_off_acq)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_off_fornitore)
	setnull(ldt_data_off_fornitore)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_offerta)
	setnull(ldt_data_scadenza)
	setnull(ls_cod_for_pot)
	setnull(ls_cod_fil_fornitore)
end if

if not isnull(ls_cod_operatore) then

	select des_operatore
	into   :ls_des_operatore
	from   tab_operatori
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 cod_operatore = :ls_cod_operatore;
				
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella select di tab_operatori: " + sqlca.sqlerrtext)
		setnull(ls_cod_operatore)
		setnull(ls_des_operatore)
	end if
	
end if	

if not isnull(ls_cod_vettore) then

	select rag_soc_1
	into   :ls_des_vettore
	from   anag_vettori
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 cod_vettore = :ls_cod_vettore;
				
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella select di anag_vettori: " + sqlca.sqlerrtext)
		setnull(ls_cod_vettore)
		setnull(ls_des_vettore)
	end if
	
end if

select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo   = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_mezzo   = :ls_cod_mezzo and  
       cod_lingua  = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return
end if	

select anag_fornitori.rag_soc_1,   
		anag_fornitori.rag_soc_2,   
		anag_fornitori.indirizzo,   
		anag_fornitori.localita,   
		anag_fornitori.frazione,   
		anag_fornitori.cap,   
		anag_fornitori.provincia,   
		anag_fornitori.partita_iva,   
		anag_fornitori.cod_fiscale,   
		anag_fornitori.cod_lingua,   
		anag_fornitori.flag_tipo_fornitore,
		rif_interno,
		anag_fornitori.telefono,
		anag_fornitori.fax
into   	:ls_rag_soc_1_for,   
		:ls_rag_soc_2_for,   
		:ls_indirizzo_for,   
		:ls_localita_for,   
		:ls_frazione_for,   
		:ls_cap_for,   
		:ls_provincia_for,   
		:ls_partita_iva_for,   
		:ls_cod_fiscale_for,   
		:ls_cod_lingua,   
		:ls_flag_tipo_fornitore,
		:ls_rif_interno,
		:ls_telefono_for,
		:ls_fax_for
from   anag_fornitori  
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fornitori.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_for)
	setnull(ls_rag_soc_2_for)
	setnull(ls_indirizzo_for)
	setnull(ls_localita_for)
	setnull(ls_frazione_for)
	setnull(ls_cap_for)
	setnull(ls_provincia_for)
	setnull(ls_partita_iva_for)
	setnull(ls_cod_fiscale_for)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_fornitore)
	setnull(ls_telefono_for)
	setnull(ls_fax_for)
else
	ls_telefono_for = "Tel. "+ ls_telefono_for
	ls_fax_for = "Fax. "+ ls_fax_for
end if

if ls_flag_tipo_fornitore = 'E' then
	ls_partita_iva_for = ls_cod_fiscale_for
end if

select anag_for_pot.rag_soc_1,   
		anag_for_pot.rag_soc_2,   
		anag_for_pot.indirizzo,   
		anag_for_pot.localita,   
		anag_for_pot.frazione,   
		anag_for_pot.cap,   
		anag_for_pot.provincia,   
		anag_for_pot.partita_iva,   
		anag_for_pot.cod_fiscale,
		anag_for_pot.cod_lingua,   
		anag_for_pot.flag_tipo_fornitore,
		anag_for_pot.telefono,
		anag_for_pot.fax
into   	:ls_rag_soc_1_for_pot,   
		:ls_rag_soc_2_for_pot,   
		:ls_indirizzo_for_pot,   
		:ls_localita_for_pot,   
		:ls_frazione_for_pot,   
		:ls_cap_for_pot,   
		:ls_provincia_for_pot,   
		:ls_partita_iva_for_pot,   
		:ls_cod_fiscale_for_pot,
		:ls_cod_lingua_for_pot,
		:ls_flag_tipo_fornitore_for_pot,
		:ls_telefono_for_pot,
		:ls_fax_for_pot
from   anag_for_pot  
where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and
       anag_for_pot.cod_for_pot = :ls_cod_for_pot;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_for_pot)
	setnull(ls_rag_soc_2_for_pot)
	setnull(ls_indirizzo_for_pot)
	setnull(ls_localita_for_pot)
	setnull(ls_frazione_for_pot)
	setnull(ls_cap_for_pot)
	setnull(ls_provincia_for_pot)
	setnull(ls_partita_iva_for_pot)
	setnull(ls_cod_fiscale_for_pot)
	setnull(ls_cod_lingua_for_pot)
	setnull(ls_flag_tipo_fornitore_for_pot)
	setnull(ls_telefono_for_pot)
	setnull(ls_fax_for_pot)
else
	ls_telefono_for_pot = "Tel. "+ ls_telefono_for_pot
	ls_fax_for_pot = "Fax. "+ ls_fax_for_pot
end if

if ls_flag_tipo_fornitore_for_pot = 'E' then
	ls_partita_iva_for_pot = ls_cod_fiscale_for_pot
end if

select anag_fil_fornitori.rag_soc_1,   
		anag_fil_fornitori.rag_soc_2,   
		anag_fil_fornitori.indirizzo,   
		anag_fil_fornitori.localita,   
		anag_fil_fornitori.frazione,   
		anag_fil_fornitori.cap,   
		anag_fil_fornitori.provincia,
		anag_fil_fornitori.telefono,
		anag_fil_fornitori.fax
into   	:ls_rag_soc_1_fil,   
		:ls_rag_soc_2_fil,   
		:ls_indirizzo_fil,   
		:ls_localita_fil,   
		:ls_frazione_fil,   
		:ls_cap_fil,   
		:ls_provincia_fil,
		:ls_telefono_fil,
		:ls_fax_fil
from   anag_fil_fornitori
where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
		 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_fil)
	setnull(ls_rag_soc_2_fil)
	setnull(ls_indirizzo_fil)
	setnull(ls_localita_fil)
	setnull(ls_frazione_fil)
	setnull(ls_cap_fil)
	setnull(ls_provincia_fil)
	setnull(ls_telefono_fil)
	setnull(ls_fax_fil)
else
	ls_telefono_fil = "Tel. "+ ls_telefono_fil
	ls_fax_fil = "Fax. "+ ls_fax_fil
end if

select tab_tipi_off_acq.des_tipo_off_acq  
into   :ls_des_tipo_off_acq  
from   tab_tipi_off_acq  
where  tab_tipi_off_acq.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_off_acq.cod_tipo_off_acq = :ls_cod_tipo_off_acq;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_off_acq)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       (tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua or
		  tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua_for_pot);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select anag_banche_clien_for.des_banca  
into   :ls_des_banca  
from   anag_banche_clien_for  
where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       (tab_porti_lingue.cod_lingua = :ls_cod_lingua or
		  tab_porti_lingue.cod_lingua = :ls_cod_lingua_for_pot);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       (tab_rese_lingue.cod_lingua = :ls_cod_lingua or
		  tab_rese_lingue.cod_lingua = :ls_cod_lingua_for_pot);

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

declare cu_dettagli cursor for 
	select   det_off_acq.cod_tipo_det_acq, 
				det_off_acq.cod_misura, 
				det_off_acq.quan_ordinata, 
				det_off_acq.prezzo_acquisto, 
				det_off_acq.sconto_1, 
				det_off_acq.sconto_2, 
				det_off_acq.imponibile_iva_valuta, 
				det_off_acq.data_consegna, 
				det_off_acq.nota_dettaglio, 
				det_off_acq.cod_prodotto, 
				det_off_acq.des_prodotto,
				det_off_acq.cod_prod_fornitore,
				det_off_acq.fat_conversione,
				det_off_acq.flag_st_note_det				
	from     det_off_acq 
	where    det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				det_off_acq.anno_registrazione = :ll_anno_registrazione and 
				det_off_acq.num_registrazione = :ll_num_registrazione
	order by det_off_acq.cod_azienda, 
				det_off_acq.anno_registrazione, 
				det_off_acq.num_registrazione,
				det_off_acq.prog_riga_off_acq;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ls_cod_tipo_det_acq, 
								  :ls_cod_misura, 
								  :ld_quan_ordinata, 
								  :ld_prezzo_acquisto,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ls_cod_prod_fornitore,
								  :ld_fat_conversione,
								  :ls_flag_st_note_det;

   if sqlca.sqlcode <> 0 then exit

	dw_report_off_acq.insertrow(0)
	dw_report_off_acq.setrow(dw_report_off_acq.rowcount())

	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_anno_registrazione", ll_anno_registrazione)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_num_registrazione", ll_num_registrazione)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_tipo_off_acq", ls_cod_tipo_off_acq)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_data_registrazione", ldt_data_registrazione)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_data_scadenza", ldt_data_scadenza)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_fornitore", ls_cod_fornitore)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_for_pot", ls_cod_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_fil_fornitore", ls_cod_fil_fornitore)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_valuta", ls_cod_valuta)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_pagamento", ls_cod_pagamento)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_sconto", ld_sconto)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_num_off_fornitore", ls_num_off_fornitore)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_data_off_fornitore", ldt_data_off_fornitore)
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then //nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if				
//--------------------------------------- Fine Modifica --------------------------------------------------------	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_nota_testata", ls_nota_testata)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_nota_piede", ls_nota_piede)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_porto", ls_cod_porto)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_cod_resa", ls_cod_resa)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tes_off_acq_tot_val_offerta", ld_tot_val_offerta)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_operatori_des_operatore", ls_des_operatore)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "cod_vettore", ls_cod_vettore)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "des_vettore", ls_des_vettore)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "des_mezzo", ls_des_mezzo)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "des_mezzo_lingua", ls_des_mezzo_lingua)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_rag_soc_1", ls_rag_soc_1_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_rag_soc_2", ls_rag_soc_2_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_indirizzo", ls_indirizzo_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_localita", ls_localita_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_frazione", ls_frazione_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_cap", ls_cap_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_provincia", ls_provincia_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_partita_iva", ls_partita_iva_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_rif_interno", ls_rif_interno)		
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_telefono", ls_telefono_for)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fornitori_fax", ls_fax_for)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_rag_soc_1", ls_rag_soc_1_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_rag_soc_2", ls_rag_soc_2_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_indirizzo", ls_indirizzo_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_localita", ls_localita_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_frazione", ls_frazione_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_cap", ls_cap_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_provincia", ls_provincia_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_partita_iva", ls_partita_iva_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_telefono", ls_telefono_for_pot)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_for_pot_fax", ls_fax_for_pot)

	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_rag_soc_1", ls_rag_soc_1_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_rag_soc_2", ls_rag_soc_2_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_indirizzo", ls_indirizzo_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_localita", ls_localita_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_frazione", ls_frazione_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_cap", ls_cap_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_provincia", ls_provincia_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_telefono", ls_telefono_fil)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_fil_fornitori_fax", ls_fax_fil)

	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_tipi_off_acq_des_tipo_off_acq", ls_des_tipo_off_acq)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)

	select tab_tipi_det_acq.flag_stampa_offerta  
	into   :ls_flag_stampa_offerta  
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_offerta)
	end if

	if ls_flag_stampa_offerta = 'S' then
		select anag_prodotti.des_prodotto  
		into   :ls_des_prodotto_anag  
		from   anag_prodotti  
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_anag)
		end if
		
		// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
		
		if ls_flag = "S" then
			setnull(ls_des_prodotto_lingua)
		else

			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 (anag_prodotti_lingue.cod_lingua = :ls_cod_lingua or
					  anag_prodotti_lingue.cod_lingua = :ls_cod_lingua_for_pot);
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
			
		end if
		
		// *** fine modifica

		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_cod_misura", ls_cod_misura)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_quan_ordinata", ld_quan_ordinata * ld_fat_conversione)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_prezzo_acquisto", ld_prezzo_acquisto / ld_fat_conversione)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_sconto_1", ld_sconto_1)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_sconto_2", ld_sconto_2)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_val_riga", ld_val_riga)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_data_consegna", ldt_data_consegna)
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_st_note_det = 'I' then //nota dettaglio
		select flag_st_note
		  into :ls_flag_st_note_det
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	end if		
	
	
	if ls_flag_st_note_det = 'N' then
		ls_nota_dettaglio = ""
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_nota_dettaglio", ls_nota_dettaglio)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_cod_prodotto", ls_cod_prodotto)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_des_prodotto", ls_des_prodotto)
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "det_off_acq_cod_prod_fornitore", ls_cod_prod_fornitore)

		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
		
		dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
	end if
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_rese_des_resa", ls_des_resa)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	
	dw_report_off_acq.setitem(dw_report_off_acq.getrow(), "formato", ls_formato)

loop
close cu_dettagli;
dw_report_off_acq.reset_dw_modified(c_resetchildren)

dw_report_off_acq.change_dw_current()

iuo_dw_main = dw_report_off_acq
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify,ls_path_firma,ls_cod_oggetto

dw_report_off_acq.ib_dw_report = true
iuo_dw_main = dw_report_off_acq
dw_report_off_acq.change_dw_current()

set_w_options(c_noresizewin)

dw_report_off_acq.set_dw_options(sqlca, &
                                 i_openparm, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
				   					   c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

select parametri_azienda.stringa
into   :ls_cod_oggetto
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'FR1';

if sqlca.sqlcode = 0 then
	select path_oggetto
	into   :ls_path_firma
	from   utenti_oggetti
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_utente = :s_cs_xx.cod_utente and 
      	 cod_oggetto = :ls_cod_oggetto;

	ls_modify = "firma.filename='" + ls_path_firma + "'~t"
	dw_report_off_acq.modify(ls_modify)
end if

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_off_acq.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_off_acq.modify(ls_modify)

dw_report_off_acq.postevent("clicked")
end event

on w_report_off_acq_euro.create
int iCurrent
call super::create
this.dw_report_off_acq=create dw_report_off_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_off_acq
end on

on w_report_off_acq_euro.destroy
call super::destroy
destroy(this.dw_report_off_acq)
end on

type dw_report_off_acq from uo_cs_xx_dw within w_report_off_acq_euro
integer x = 23
integer y = 20
integer width = 3657
integer height = 4520
integer taborder = 20
string dataobject = "d_report_off_acq_euro"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

