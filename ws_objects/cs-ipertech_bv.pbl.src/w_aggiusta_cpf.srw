﻿$PBExportHeader$w_aggiusta_cpf.srw
forward
global type w_aggiusta_cpf from window
end type
type cbx_mov_manuali from checkbox within w_aggiusta_cpf
end type
type cb_cerca_lista from commandbutton within w_aggiusta_cpf
end type
type cb_1 from commandbutton within w_aggiusta_cpf
end type
type dw_lista_varianti_versione_errata from datawindow within w_aggiusta_cpf
end type
type cbx_commesse_aperte from checkbox within w_aggiusta_cpf
end type
type st_17 from statictext within w_aggiusta_cpf
end type
type st_15 from statictext within w_aggiusta_cpf
end type
type sle_cod_prodotto from singlelineedit within w_aggiusta_cpf
end type
type st_16 from statictext within w_aggiusta_cpf
end type
type em_count_smp from editmask within w_aggiusta_cpf
end type
type ddlb_op_smp from dropdownlistbox within w_aggiusta_cpf
end type
type st_smp from statictext within w_aggiusta_cpf
end type
type cb_procedi from commandbutton within w_aggiusta_cpf
end type
type dw_var_ord_ven from datawindow within w_aggiusta_cpf
end type
type em_count_smp_coll from editmask within w_aggiusta_cpf
end type
type ddlb_op_smp_coll from dropdownlistbox within w_aggiusta_cpf
end type
type st_14 from statictext within w_aggiusta_cpf
end type
type em_count_smp_x from editmask within w_aggiusta_cpf
end type
type ddlb_op_smp_x from dropdownlistbox within w_aggiusta_cpf
end type
type st_13 from statictext within w_aggiusta_cpf
end type
type em_count_smp_y from editmask within w_aggiusta_cpf
end type
type ddlb_op_smp_y from dropdownlistbox within w_aggiusta_cpf
end type
type st_12 from statictext within w_aggiusta_cpf
end type
type cbx_smp_mancanti from checkbox within w_aggiusta_cpf
end type
type ddlb_op_having from dropdownlistbox within w_aggiusta_cpf
end type
type em_having from editmask within w_aggiusta_cpf
end type
type st_11 from statictext within w_aggiusta_cpf
end type
type dw_var_com from datawindow within w_aggiusta_cpf
end type
type sle_data_chiu_com from singlelineedit within w_aggiusta_cpf
end type
type sle_qta_com_prodotta from singlelineedit within w_aggiusta_cpf
end type
type sle_qta_com_ordinata from singlelineedit within w_aggiusta_cpf
end type
type sle_commessa from singlelineedit within w_aggiusta_cpf
end type
type sle_stato_commessa from singlelineedit within w_aggiusta_cpf
end type
type st_10 from statictext within w_aggiusta_cpf
end type
type st_9 from statictext within w_aggiusta_cpf
end type
type st_8 from statictext within w_aggiusta_cpf
end type
type st_7 from statictext within w_aggiusta_cpf
end type
type st_2 from statictext within w_aggiusta_cpf
end type
type dw_fatture from datawindow within w_aggiusta_cpf
end type
type cb_produzione from commandbutton within w_aggiusta_cpf
end type
type dw_ddt_trasf from datawindow within w_aggiusta_cpf
end type
type st_6 from statictext within w_aggiusta_cpf
end type
type dw_righe_ordini from datawindow within w_aggiusta_cpf
end type
type dw_movimenti from datawindow within w_aggiusta_cpf
end type
type cb_cerca from commandbutton within w_aggiusta_cpf
end type
type em_anno_ordine from editmask within w_aggiusta_cpf
end type
type st_5 from statictext within w_aggiusta_cpf
end type
type st_4 from statictext within w_aggiusta_cpf
end type
type st_3 from statictext within w_aggiusta_cpf
end type
type st_1 from statictext within w_aggiusta_cpf
end type
type dw_cpf_count from datawindow within w_aggiusta_cpf
end type
type r_1 from rectangle within w_aggiusta_cpf
end type
type r_2 from rectangle within w_aggiusta_cpf
end type
type em_num_ordine from editmask within w_aggiusta_cpf
end type
type em_anno_commessa from editmask within w_aggiusta_cpf
end type
type em_num_commessa from editmask within w_aggiusta_cpf
end type
type em_riga_ordine from editmask within w_aggiusta_cpf
end type
type em_chius_al from editmask within w_aggiusta_cpf
end type
type em_chius_dal from editmask within w_aggiusta_cpf
end type
end forward

global type w_aggiusta_cpf from window
integer width = 8018
integer height = 2784
boolean titlebar = true
string title = "Controllo CPF e Commesse"
boolean controlmenu = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cbx_mov_manuali cbx_mov_manuali
cb_cerca_lista cb_cerca_lista
cb_1 cb_1
dw_lista_varianti_versione_errata dw_lista_varianti_versione_errata
cbx_commesse_aperte cbx_commesse_aperte
st_17 st_17
st_15 st_15
sle_cod_prodotto sle_cod_prodotto
st_16 st_16
em_count_smp em_count_smp
ddlb_op_smp ddlb_op_smp
st_smp st_smp
cb_procedi cb_procedi
dw_var_ord_ven dw_var_ord_ven
em_count_smp_coll em_count_smp_coll
ddlb_op_smp_coll ddlb_op_smp_coll
st_14 st_14
em_count_smp_x em_count_smp_x
ddlb_op_smp_x ddlb_op_smp_x
st_13 st_13
em_count_smp_y em_count_smp_y
ddlb_op_smp_y ddlb_op_smp_y
st_12 st_12
cbx_smp_mancanti cbx_smp_mancanti
ddlb_op_having ddlb_op_having
em_having em_having
st_11 st_11
dw_var_com dw_var_com
sle_data_chiu_com sle_data_chiu_com
sle_qta_com_prodotta sle_qta_com_prodotta
sle_qta_com_ordinata sle_qta_com_ordinata
sle_commessa sle_commessa
sle_stato_commessa sle_stato_commessa
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_2 st_2
dw_fatture dw_fatture
cb_produzione cb_produzione
dw_ddt_trasf dw_ddt_trasf
st_6 st_6
dw_righe_ordini dw_righe_ordini
dw_movimenti dw_movimenti
cb_cerca cb_cerca
em_anno_ordine em_anno_ordine
st_5 st_5
st_4 st_4
st_3 st_3
st_1 st_1
dw_cpf_count dw_cpf_count
r_1 r_1
r_2 r_2
em_num_ordine em_num_ordine
em_anno_commessa em_anno_commessa
em_num_commessa em_num_commessa
em_riga_ordine em_riga_ordine
em_chius_al em_chius_al
em_chius_dal em_chius_dal
end type
global w_aggiusta_cpf w_aggiusta_cpf

type variables
boolean ib_automatico=false
string is_sql_righe_CPF_conteggio
//= 	&
//			"select count(*),"+&
//					"m.cod_deposito,"+&
//					"m.cod_tipo_movimento,"+&
//					"m.cod_prodotto,"+&
//					"m.anno_documento, "+&
//					"m.num_documento, "+&
//					"m.data_documento "+&
//					"from mov_magazzino as m "+&
//					"where m.cod_tipo_movimento in ('CPF') and data_documento is not null and flag_manuale='N' "
string is_sql_group_CPF_conteggio = 	"group by m.cod_deposito, m.cod_tipo_movimento,m.cod_prodotto, "+&
																"m.anno_documento, m.num_documento, m.data_documento "
string is_sql_orderby_CPF_conteggio =		"order by m.num_documento,m.data_documento,m.cod_deposito,m.cod_tipo_movimento,m.cod_prodotto"


string is_sql_righe_ordini = "select t.cod_deposito,d.anno_registrazione,d.num_registrazione,d.prog_riga_ord_ven,"+&
											"d.cod_prodotto,d.quan_ordine,d.quan_evasa,d.anno_commessa,d.num_commessa "+&
									"from det_ord_ven as d "+&
									"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
																"d.anno_registrazione=t.anno_registrazione and "+&
																"d.num_registrazione=t.num_registrazione "
end variables

forward prototypes
public function long wf_dw_cpf_count ()
public function long wf_prepara_dw_righe_ordini (integer fl_anno_commessa, long fl_num_commessa)
public function long wf_prepara_dw_righe_movimenti (integer fl_anno_commessa, long fl_num_commessa)
public function long wf_prepara_dw_ddt_trasf (long fl_row)
public function long wf_smp_mancanti (long fl_tot_count_cpf)
public function integer wf_conta_smp_xy_pf (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa)
public function integer wf_conta_smp_xy_2 (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa)
public subroutine wf_selectrow_count_cpf (long fl_row)
public function integer wf_conta_smp (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa)
public function integer wf_correggi_commesse_deposito_errato (long row, boolean ab_automatico, ref string as_message)
end prototypes

public function long wf_dw_cpf_count ();string ls_syntax, ls_error, ls_sql, ls_op_having, ls_cod_prodotto, ls_flag_manuali
long ll_rows, ll_num_commessa, ll_num_reg_ord_ven, ll_prog_riga_ord_ven
integer li_anno_commessa, li_anno_reg_ord_ven, il_having
datetime ldt_dal, ldt_al


ls_flag_manuali = "N"
if cbx_mov_manuali.checked then
	ls_flag_manuali = "S"
end if


dw_cpf_count.setfilter("")
dw_cpf_count.filter()

ls_sql = is_sql_righe_CPF_conteggio + " "

ls_sql += "join anag_commesse as comm on comm.cod_azienda=m.cod_azienda and "+&
															"comm.anno_commessa=m.anno_documento and "+&
															"comm.num_commessa=m.num_documento "

//if cbx_smp_mancanti.checked then
//	ls_sql +=		"join det_ord_ven as dord on dord.cod_azienda=m.cod_azienda and "+&
//                            								"dord.anno_commessa=m.anno_documento and "+&
//                            								"dord.num_commessa=m.num_documento "+&
//					"join tes_ord_ven as tord on tord.cod_azienda=dord.cod_azienda and "+&
//														 "tord.anno_registrazione=dord.anno_registrazione and "+&
//														 "tord.num_registrazione=dord.num_registrazione "+&
//					"join det_bol_ven as dbol on dbol.cod_azienda=dord.cod_azienda and "+&
//														 "dbol.anno_registrazione_ord_ven=dord.anno_registrazione and "+&
//														 "dbol.num_registrazione_ord_ven=dord.num_registrazione and "+&
//														 "dbol.prog_riga_ord_ven=dord.prog_riga_ord_ven "+&
//					"join tes_bol_ven as tbol on tbol.cod_azienda=dbol.cod_azienda and "+&
//														 "tbol.anno_registrazione=dbol.anno_registrazione and "+&
//														 "tbol.num_registrazione=dbol.num_registrazione "
//end if

ls_sql += "where m.cod_azienda='"+s_cs_xx.cod_azienda+"' and m.cod_tipo_movimento in ('CPF') and data_documento is not null and flag_manuale='"+ls_flag_manuali+"' "

//if cbx_smp_mancanti.checked then
//	ls_sql += "and tbol.cod_tipo_bol_ven in (select cod_tipo_bol_ven "+&
//														  "from tab_tipi_bol_ven "+&
//														 " where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//																	 "flag_tipo_bol_ven='T') and "+&
//				"tord.cod_tipo_ord_ven not in (  select cod_tipo_ord_ven "+&
//																"from tab_tipi_ord_ven "+&
//																"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//																	 		"(flag_tipo_bcl='B' or flag_tipo_bcl='D')) "
//end if


li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa = long(em_num_commessa.text)

li_anno_reg_ord_ven = integer(em_anno_ordine.text)
ll_num_reg_ord_ven = long(em_num_ordine.text)
ll_prog_riga_ord_ven = long(em_riga_ordine.text)
ls_cod_prodotto = sle_cod_prodotto.text

il_having = long(em_having.text)


ls_op_having = ddlb_op_having.text
if ls_op_having="" or isnull(ls_op_having) then
	ls_op_having = ">"
	ddlb_op_having.text = ">"
end if


if li_anno_commessa>0 and ll_num_commessa>0 then
	ls_sql += " and m.anno_documento= "+string(li_anno_commessa)+" and m.num_documento="+string(ll_num_commessa)

elseif li_anno_reg_ord_ven>0 and ll_num_reg_ord_ven>0 and ll_prog_riga_ord_ven>0 then
	select anno_commessa,
			num_commessa
	into :li_anno_commessa,
			:ll_num_commessa
	from det_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_reg_ord_ven and
			num_registrazione=:ll_num_reg_ord_ven and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode<0 then
		g_mb.messagebox("CS_TEAM", "Errore in lettura commessa ordine CPF! " + sqlca.sqlerrtext , StopSign!)
		return -1
		
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("CS_TEAM", "Riga Ordine inesistente! ", StopSign!)
		return -1
	
	elseif sqlca.sqlcode = 0 and isnull(li_anno_commessa) then
		g_mb.messagebox("CS_TEAM", "La Riga Ordine non ha commessa! ", StopSign!)
		return -1
	end if
	
	ls_sql += " and m.anno_documento= "+string(li_anno_commessa)+" and m.num_documento="+string(ll_num_commessa)
	
else
//	g_mb.messagebox("CS_TEAM", "Specificare una commessa oppure una riga ordine con commessa! ", StopSign!)
//	return -1
end if


if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	ls_sql += " and m.cod_prodotto='"+ls_cod_prodotto+"' "
end if

if cbx_smp_mancanti.checked then
	ls_sql += " and substring(m.cod_prodotto, 2, 1)='"+"0"+"' "
end if

ldt_dal = datetime(date(em_chius_dal.text), 00:00:00)
ldt_al = datetime(date(em_chius_al.text), 00:00:00)


if not isnull(ldt_dal) and year(date(ldt_dal)) > 1980 then
	if cbx_commesse_aperte.checked then
		ls_sql += " and (comm.data_chiusura is null or comm.data_chiusura >= '"+string(ldt_dal, s_cs_xx.db_funzioni.formato_data)+"') "
	else
		ls_sql += " and comm.data_chiusura >= '"+string(ldt_dal, s_cs_xx.db_funzioni.formato_data)+"' "
	end if
	
end if

if not isnull(ldt_al) and year(date(ldt_al)) > 1980 then
	if cbx_commesse_aperte.checked then
		ls_sql += " and (comm.data_chiusura is null or comm.data_chiusura <= '"+string(ldt_al, s_cs_xx.db_funzioni.formato_data)+"') "
	else
		ls_sql += " and comm.data_chiusura <= '"+string(ldt_al, s_cs_xx.db_funzioni.formato_data)+"' "
	end if
end if
	





//group by
ls_sql += " " + is_sql_group_CPF_conteggio

//having
ls_sql += " " + "having count(*)"+ls_op_having+string(il_having)

//order by
ls_sql += " " +	is_sql_orderby_CPF_conteggio


dw_cpf_count.setsqlselect(ls_sql)
ll_rows = dw_cpf_count.Retrieve()


if cbx_smp_mancanti.checked and ll_rows>0 then
	//ricerca mirata agli SMP mancanti
	ll_rows = wf_smp_mancanti(ll_rows)
end if

dw_cpf_count.title = "Count CPF per commessa: " + string(ll_rows)

if ll_rows > 0 then wf_selectrow_count_cpf(1)



//commentato e spostato in pulsante dedicato
//senno per cercare una commessa ci mette 6 anni e 7 mesi

//dw_lista_varianti_versione_errata.settransobject(sqlca)
//
//if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then ls_cod_prodotto = "%"
//dw_lista_varianti_versione_errata.retrieve(ls_cod_prodotto,ldt_dal,ldt_al)

return ll_rows
end function

public function long wf_prepara_dw_righe_ordini (integer fl_anno_commessa, long fl_num_commessa);string ls_syntax, ls_error, ls_sql
long ll_rows

ls_sql = is_sql_righe_ordini + " where d.cod_azienda='"+s_cs_xx.cod_azienda+"' "
ls_sql += " and d.anno_commessa="+string(fl_anno_commessa) +" and d.num_commessa="+string(fl_num_commessa)

ls_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)

if Len(ls_error) > 0 then
	g_mb.messagebox("CS_TEAM", "Errore creazione sintassi SQL righe ordini! " + ls_error , Exclamation!)
	return -1
else
	dw_righe_ordini.create(ls_syntax, ls_error)
	if Len(ls_error) > 0 then
		g_mb.messagebox("CS_TEAM", "Errore creazione datawindow righe ordini! " + ls_error , Exclamation!)
		return -1
	end if
end if

dw_righe_ordini.SetTransObject(SQLCA)
ll_rows = dw_righe_ordini.Retrieve()

if ll_rows>0 then
else
	wf_prepara_dw_ddt_trasf(-1)
end if

return ll_rows
end function

public function long wf_prepara_dw_righe_movimenti (integer fl_anno_commessa, long fl_num_commessa);string ls_syntax, ls_error, ls_sql, ls_flag_manuali
long ll_rows

ls_flag_manuali = "N"
if cbx_mov_manuali.checked then
	ls_flag_manuali = "S"
end if


ll_rows = dw_movimenti.Retrieve(s_cs_xx.cod_azienda, fl_anno_commessa, fl_num_commessa, ls_flag_manuali)

dw_movimenti.title = "Movimenti per Commessa: " + string(ll_rows)

return ll_rows
end function

public function long wf_prepara_dw_ddt_trasf (long fl_row);string ls_syntax, ls_error, ls_sql
long ll_rows
integer li_anno_ordine
long ll_num_ordine, ll_prog_riga_ord_ven

if fl_row>0 then
	li_anno_ordine = dw_righe_ordini.getitemnumber(fl_row, 2)
	ll_num_ordine = dw_righe_ordini.getitemnumber(fl_row, 3)
	ll_prog_riga_ord_ven = dw_righe_ordini.getitemnumber(fl_row, 4)
else
	li_anno_ordine = -1
	ll_num_ordine = -1
	ll_prog_riga_ord_ven = -1
end if

dw_ddt_trasf.retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven)
dw_fatture.retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven)
dw_var_ord_ven.retrieve(s_cs_xx.cod_azienda, li_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven)

dw_ddt_trasf.title = "DDT su riga ordine "+string(li_anno_ordine) + "/" + string(ll_num_ordine) + "/" + string(ll_prog_riga_ord_ven)
dw_fatture.title = "FATT su riga ordine "+string(li_anno_ordine) + "/" + string(ll_num_ordine) + "/" + string(ll_prog_riga_ord_ven)

return 0
end function

public function long wf_smp_mancanti (long fl_tot_count_cpf);string					ls_cod_prodotto_mov
long					ll_index, ll_num_commessa
integer				li_anno_commessa

integer				li_ret

open(w_stato_elaborazione)

//scorri i movimenti trovati e conta gli SMP relativi
for ll_index=1 to fl_tot_count_CPF
	Yield()
	ls_cod_prodotto_mov = dw_cpf_count.getitemstring(ll_index, 4)
	li_anno_commessa = dw_cpf_count.getitemnumber(ll_index, 5)
	ll_num_commessa = dw_cpf_count.getitemnumber(ll_index, 6)
	
	if isvalid(w_stato_elaborazione) then w_stato_elaborazione.st_log.text =  "Attendere...~r~n~r~nElab. Mov. "+string(ll_index)+" di " + string(fl_tot_count_CPF)
	
	li_ret = wf_conta_smp(ls_cod_prodotto_mov, li_anno_commessa, ll_num_commessa)
	if li_ret<0 then
		//errore
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		return -1
	end if
	
	if li_ret=1 then dw_cpf_count.setitem(ll_index, "colore", 1)
	
next

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

dw_cpf_count.setfilter("colore=1")
dw_cpf_count.filter()
return dw_cpf_count.rowcount()



end function

public function integer wf_conta_smp_xy_pf (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa);integer			li_anno_reg_mov[], li_anno_mov
long				ll_num_reg_mov[], ll_num_mov, ll_count, ll_min_coll, ll_min_y, ll_min_x, ll_min

ll_min_coll = long(em_count_smp_coll.text)
ll_min_y = long(em_count_smp_y.text)
ll_min_x = long(em_count_smp_x.text)

//il movimento è comunque uno solo, leggo anno e numero -------------------------------------------------
select count(*)
into :ll_count
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto_mov and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa and
			cod_tipo_movimento='CPF';

if ll_count>1 then
	g_mb.error("Attenzione esistono "+string(ll_count)+" movimenti CPF per la commessa: "&
									+string(fi_anno_commessa) +"/"+ string(fl_num_commessa)+" e prodotto: "+&
									fs_cod_prodotto_mov+": escluso da questa elaborazione, "+&
									"verificare successivamente!")
	return 0
end if

select anno_registrazione, num_registrazione
into :li_anno_mov, :ll_num_mov
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto_mov and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa and
			cod_tipo_movimento='CPF';
if sqlca.sqlcode<0 then
	g_mb.error("(wf_conta_smp_xy_pf) Errore in lettura dati movimento (commessa: "&
									+string(fi_anno_commessa) +"/"+ string(fl_num_commessa)+", prodotto: "+fs_cod_prodotto_mov+"): "+sqlca.sqlerrtext)
	return -1
end if
//-------------------------------------------------------------------------------------------------------------------

if mid(fs_cod_prodotto_mov, 2, 1) = "0" and right(fs_cod_prodotto_mov, 1) <> "Y" and right(fs_cod_prodotto_mov, 1) <> "X" then
	//configurato, è un PF o comunque non un semilavorato X o Y
	//devi leggere l'insieme dei movimenti e vedere se c'è un x, un Y e controllare quantii scarichi ha avuto
	ll_count = wf_conta_smp_xy_2(fs_cod_prodotto_mov, fi_anno_commessa, fl_num_commessa)
	return ll_count
	
else
	//si tratta di un SL Y, SL X oppure un non configurato in ordine NON sfuso, probabilmente è un motore con commessa associata
	//conta quanti scarichi SMP si sono avuti
	
	select count(*)
	into :ll_count
	from mov_magazzino
	where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_movimento='SMP' and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa and
			num_registrazione<:ll_num_mov;
	
	if sqlca.sqlcode<0 then
		g_mb.error("(wf_conta_smp_xy_pf) Errore in conteggio SMP prodotti collegati: "+sqlca.sqlerrtext)
		return -1
	end if
	
	if isnull(ll_count) then ll_count=0
	
	if right(fs_cod_prodotto_mov, 1) = "Y" then
		ll_min = ll_min_y
	elseif right(fs_cod_prodotto_mov, 1) = "X" then
		ll_min = ll_min_x
	else
		ll_min = ll_min_coll
	end if
	
	if ll_count < ll_min then
		//segnalalo
		return 1
	end if
end if
	
return 0
end function

public function integer wf_conta_smp_xy_2 (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa);integer			li_anno_mov_cu
long				ll_num_mov_cu, ll_count, ll_min, ll_min_y, ll_min_x, ll_num_reg_mov_min, ll_passo
string				ls_cod_prodotto_cu
string				ls_tipo_primo_prodotto //Y, X

ll_min_y = long(em_count_smp_y.text)
ll_min_x = long(em_count_smp_x.text)
ll_num_reg_mov_min = 0
ll_passo = 0

//estrapolo i CPF X e/o Y legati a questo set di movimenti
declare cu_mov cursor for  
	select anno_registrazione, num_registrazione, cod_prodotto
	from mov_magazzino
	where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_movimento='CPF' and
			anno_documento=:fi_anno_commessa and num_documento=:fl_num_commessa
	order by num_registrazione asc;

open cu_mov;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore in OPEN cursore 'cu_mov' (wf_conta_smp_xy_2)" + sqlca.sqlerrtext)
	return -1
end if

do while true
	fetch cu_mov into :li_anno_mov_cu, :ll_num_mov_cu, :ls_cod_prodotto_cu;

	if sqlca.sqlcode < 0 then
		close cu_mov;
		g_mb.error("Errore in FETCH cursore 'cu_mov'  (wf_conta_smp_xy_2)" + sqlca.sqlerrtext)
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	
	select count(*)
	into :ll_count
	from mov_magazzino
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_documento=:fi_anno_commessa and
				num_documento=:fl_num_commessa and
				cod_tipo_movimento='SMP' and
				num_registrazione<=:ll_num_mov_cu and
				num_registrazione>:ll_num_reg_mov_min;
	
	if ll_passo = 1 then
		//hai già fatto il passo 1
		//considero già le condizioni per il PF: se prima ho fatto una Y allora devo verificare i minimi per la X e viceversa
		if ls_tipo_primo_prodotto="Y" then
			ll_min = ll_min_x
			
		elseif ls_tipo_primo_prodotto="X" then
			ll_min = ll_min_y
		end if
	else
		//sei al passo 1 (inizio)
		
		//leggi il CPF oggetto del movimento
		choose case right(ls_cod_prodotto_cu, 1)
			case "Y"
				ll_min = ll_min_y
				ls_tipo_primo_prodotto = "Y"
				
			case "X"
				ll_min = ll_min_x
				ls_tipo_primo_prodotto = "X"
				
			case else
				//sembra che non ci siano CPF per X o Y, considera la somma dei minimi
				ll_min = ll_min_y + ll_min_x
				ls_tipo_primo_prodotto = ""
			
		end choose
		
	end if
	
	
	if ll_count < ll_min then
		//segnalo ed esco dal cursore
		close cu_mov;
		return 1
	end if
	
	//sposto su il limite inferiore per il controllo dei movimenti e continuo a leggere nel cursore
	ll_num_reg_mov_min = ll_num_mov_cu
	ll_passo += 1
	
	if ls_tipo_primo_prodotto="" then
		//esco dal cursore in ogni caso
		exit
	end if

loop

close cu_mov;


return 0
end function

public subroutine wf_selectrow_count_cpf (long fl_row);integer li_anno_commessa
long ll_num_commessa
string ls_quan_ordine, ls_quan_prodotta, ls_data_chiusura, ls_flag_tipo_avanzamento, ls_commessa
decimal ld_quan_ordine, ld_quan_prodotta
datetime ldt_data_chiusura

if fl_row>0 then

	li_anno_commessa = dw_cpf_count.getitemnumber(fl_row, 5)
	ll_num_commessa =  dw_cpf_count.getitemnumber(fl_row, 6)
	
	select		flag_tipo_avanzamento,
				quan_ordine,
				quan_prodotta,
				data_chiusura
	into 		:ls_flag_tipo_avanzamento,
				:ld_quan_ordine,
				:ld_quan_prodotta,
				:ldt_data_chiusura
	from anag_commesse
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:li_anno_commessa and
				num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in lettura dati commessa! "+sqlca.sqlerrtext)
		
		ls_commessa = "Errore lettura commessa"
		ls_flag_tipo_avanzamento = ""
		ls_quan_ordine=""
		ls_quan_prodotta=""
		ls_data_chiusura =""
		
	elseif sqlca.sqlcode=100 then
		ls_commessa = "COMMESSA INESISTENTE"
		ls_flag_tipo_avanzamento = ""
		ls_quan_ordine=""
		ls_quan_prodotta=""
		ls_data_chiusura =""
	else
		ls_commessa = string(li_anno_commessa) + "/" + string(ll_num_commessa)
		ls_quan_ordine=string(ld_quan_ordine, "###,###,##.00")
		ls_quan_prodotta=string(ld_quan_prodotta, "###,###,##.00")
		ls_data_chiusura = string(ldt_data_chiusura, "dd-mm-yyyy")
	end if

else
	li_anno_commessa = -1
	ll_num_commessa = -1
	
	ls_flag_tipo_avanzamento = ""
	ls_quan_ordine=""
	ls_quan_prodotta=""
	ls_data_chiusura =""
end if

sle_stato_commessa.text = ls_flag_tipo_avanzamento
sle_commessa.text = ls_commessa
sle_qta_com_ordinata.text = ls_quan_ordine
sle_qta_com_prodotta.text = ls_quan_prodotta
sle_data_chiu_com.text = ls_data_chiusura

wf_prepara_dw_righe_ordini(li_anno_commessa, ll_num_commessa)
wf_prepara_dw_righe_movimenti(li_anno_commessa, ll_num_commessa)


dw_VAR_COM.retrieve(s_cs_xx.cod_azienda, li_anno_commessa, ll_num_commessa)

end subroutine

public function integer wf_conta_smp (string fs_cod_prodotto_mov, integer fi_anno_commessa, long fl_num_commessa);integer			li_anno_reg_mov[], li_anno_mov
long				ll_num_reg_mov[], ll_num_mov, ll_count, ll_min
string				ls_operatore

ll_min = long(em_count_smp.text)
ls_operatore = ddlb_op_smp.text


//il movimento è comunque uno solo, leggo anno e numero -------------------------------------------------
select count(*)
into :ll_count
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto_mov and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa and
			cod_tipo_movimento='CPF';

if ll_count>1 then
	g_mb.error("Attenzione esistono "+string(ll_count)+" movimenti CPF per la commessa: "&
									+string(fi_anno_commessa) +"/"+ string(fl_num_commessa)+" e prodotto: "+&
									fs_cod_prodotto_mov+": escluso da questa elaborazione, "+&
									"verificare successivamente!")
	return 0
end if

select anno_registrazione, num_registrazione
into :li_anno_mov, :ll_num_mov
from mov_magazzino
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto_mov and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa and
			cod_tipo_movimento='CPF';
if sqlca.sqlcode<0 then
	g_mb.error("(wf_conta_smp) Errore in lettura dati movimento (commessa: "&
									+string(fi_anno_commessa) +"/"+ string(fl_num_commessa)+", prodotto: "+fs_cod_prodotto_mov+"): "+sqlca.sqlerrtext)
	return -1
end if
//-------------------------------------------------------------------------------------------------------------------

if mid(fs_cod_prodotto_mov, 2, 1) = "0" then
	//si tratta di un SL Y, SL X oppure un non configurato in ordine NON sfuso, probabilmente è un motore con commessa associata
	//conta quanti scarichi SMP si sono avuti
	select count(*)
	into :ll_count
	from mov_magazzino
	where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_movimento='SMP' and
			anno_documento=:fi_anno_commessa and
			num_documento=:fl_num_commessa;
	
	if sqlca.sqlcode<0 then
		g_mb.error("(wf_conta_smp) Errore in conteggio SMP collegati: "+sqlca.sqlerrtext)
		return -1
	end if
	
	if isnull(ll_count) then ll_count=0
	
	if ls_operatore = "<" then
		if ll_count < ll_min then
			//segnalalo
			return 1
		end if
	else
		//<=
		if ll_count <= ll_min then
			//segnalalo
			return 1
		end if
	end if
	
	
end if
	
return 0
end function

public function integer wf_correggi_commesse_deposito_errato (long row, boolean ab_automatico, ref string as_message);boolean				lb_solo_trasferimenti
string				ls_errore, ls_depositi_prod[], ls_cod_prodotto, ls_cod_versione, ls_reparti[], ls_semilav[]
integer				li_anno_commessa, li_ret, li_anno_ordine
long					ll_num_commessa, ll_count, ll_num_ordine, ll_riga_ordine, ll_currow
uo_funzioni_1		luo_funz


if row >0 then
	li_anno_commessa = dw_lista_varianti_versione_errata.getitemnumber(row, "det_ord_ven_anno_commessa")
	ll_num_commessa = dw_lista_varianti_versione_errata.getitemnumber(row, "det_ord_ven_num_commessa")
	
	if li_anno_commessa=0 or ll_num_commessa=0 or isnull(li_anno_commessa) or isnull(ll_num_commessa) then
		rollback;
		as_message = "Commessa non associata alla riga ordine"
		return -2
	end if	
else
	as_message = "Riga DW non coerente!"
	return -1
end if

update 	varianti_commesse
set		cod_versione_variante = cod_versione_figlio
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_commessa = :li_anno_commessa and
			num_commessa = :ll_num_commessa;
			
if sqlca.sqlcode < 0 then
	rollback;
	as_message = "Errore in UPDATE sql tabella varianti commesse.~r~n" + sqlca.sqlerrtext
	return -1
end if		
			

li_anno_ordine = dw_lista_varianti_versione_errata.getitemnumber(row, "tes_ord_ven_anno_registrazione")
ll_num_ordine = dw_lista_varianti_versione_errata.getitemnumber(row, "det_ord_ven_num_registrazione")
ll_riga_ordine = dw_lista_varianti_versione_errata.getitemnumber(row, "det_ord_ven_prog_riga_ord_ven")
ls_cod_prodotto = dw_lista_varianti_versione_errata.getitemstring(row, "det_ord_ven_cod_prodotto")
ls_cod_versione = dw_lista_varianti_versione_errata.getitemstring(row, "det_ord_ven_cod_versione")

w_cs_xx_mdi.setmicrohelp(g_str.format("In elaborazione ordine $1/$2/$3 prodotto $3", li_anno_ordine, ll_num_ordine,ll_riga_ordine,ls_cod_prodotto ) )

update 	varianti_det_ord_ven
set		cod_versione_variante = cod_versione_figlio
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_ordine and
			num_registrazione = :ll_num_ordine and
			prog_riga_ord_ven = :ll_riga_ordine;
			
if sqlca.sqlcode < 0 then
	rollback;
	as_message = "Errore in UPDATE sql tabella varianti ordini.~r~n" + sqlca.sqlerrtext
	return -1
end if		
			



if not ab_automatico  then
	if not g_mb.confirm( "Rigenerare i movimenti per la commessa "+string(li_anno_commessa)+"/"+string(ll_num_commessa) + " ?") then return 0
end if

luo_funz = create uo_funzioni_1

	
	li_ret = luo_funz.uof_trova_reparti_e_semilavorati(true, li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_prodotto, ls_cod_versione, &
												 ls_reparti[], ls_semilav[], ls_depositi_prod[], ls_errore)
	
	if li_ret < 0 then
		destroy luo_funz
		rollback;
		as_message = "Errore in ricerca reparti (uof_trova_reparti_e_semilavorati): " + ls_errore
		return -1
	end if
	
	if upperbound(ls_depositi_prod[]) = 0 then
		lb_solo_trasferimenti = false
		goto PROSEGUI
	end if
	
	li_ret = luo_funz.uof_se_commessa_con_trasf(li_anno_commessa, ll_num_commessa, ls_depositi_prod[], ls_errore)
	
	if li_ret<0 then
		destroy luo_funz
		rollback;
		as_message = "Errore in verifica trasferimenti~r~n" +  ls_errore
		return  -1
		
	elseif li_ret=1 then
		//commessa non soggetta a trasferimento
		lb_solo_trasferimenti = false
	else
		//torna 0 quindi la commessa è soggetta a trasferimento
		lb_solo_trasferimenti = true
	end if
	
//end if


PROSEGUI:

li_ret = luo_funz.uof_riallinea_mov_commessa(li_anno_commessa, ll_num_commessa, lb_solo_trasferimenti, ls_errore)

choose case li_ret
	case is < 0
		rollback;
		destroy luo_funz
		as_message = "Errore in riallineamento movimenti magazzino~r~n " + ls_errore
		return -1
		
	case 1
		//dai il messaggio (in caso di elaborazione multipla salterà la commessa ma andrà avanti)
		rollback;
		destroy luo_funz
		as_message =  ls_errore
		return -1
		
	case else
		//tutto a posto, operazioe completata
		commit;
		destroy luo_funz
		as_message = "Operazione effettuata con successo!"
end choose

return 0
end function

on w_aggiusta_cpf.create
this.cbx_mov_manuali=create cbx_mov_manuali
this.cb_cerca_lista=create cb_cerca_lista
this.cb_1=create cb_1
this.dw_lista_varianti_versione_errata=create dw_lista_varianti_versione_errata
this.cbx_commesse_aperte=create cbx_commesse_aperte
this.st_17=create st_17
this.st_15=create st_15
this.sle_cod_prodotto=create sle_cod_prodotto
this.st_16=create st_16
this.em_count_smp=create em_count_smp
this.ddlb_op_smp=create ddlb_op_smp
this.st_smp=create st_smp
this.cb_procedi=create cb_procedi
this.dw_var_ord_ven=create dw_var_ord_ven
this.em_count_smp_coll=create em_count_smp_coll
this.ddlb_op_smp_coll=create ddlb_op_smp_coll
this.st_14=create st_14
this.em_count_smp_x=create em_count_smp_x
this.ddlb_op_smp_x=create ddlb_op_smp_x
this.st_13=create st_13
this.em_count_smp_y=create em_count_smp_y
this.ddlb_op_smp_y=create ddlb_op_smp_y
this.st_12=create st_12
this.cbx_smp_mancanti=create cbx_smp_mancanti
this.ddlb_op_having=create ddlb_op_having
this.em_having=create em_having
this.st_11=create st_11
this.dw_var_com=create dw_var_com
this.sle_data_chiu_com=create sle_data_chiu_com
this.sle_qta_com_prodotta=create sle_qta_com_prodotta
this.sle_qta_com_ordinata=create sle_qta_com_ordinata
this.sle_commessa=create sle_commessa
this.sle_stato_commessa=create sle_stato_commessa
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_2=create st_2
this.dw_fatture=create dw_fatture
this.cb_produzione=create cb_produzione
this.dw_ddt_trasf=create dw_ddt_trasf
this.st_6=create st_6
this.dw_righe_ordini=create dw_righe_ordini
this.dw_movimenti=create dw_movimenti
this.cb_cerca=create cb_cerca
this.em_anno_ordine=create em_anno_ordine
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_1=create st_1
this.dw_cpf_count=create dw_cpf_count
this.r_1=create r_1
this.r_2=create r_2
this.em_num_ordine=create em_num_ordine
this.em_anno_commessa=create em_anno_commessa
this.em_num_commessa=create em_num_commessa
this.em_riga_ordine=create em_riga_ordine
this.em_chius_al=create em_chius_al
this.em_chius_dal=create em_chius_dal
this.Control[]={this.cbx_mov_manuali,&
this.cb_cerca_lista,&
this.cb_1,&
this.dw_lista_varianti_versione_errata,&
this.cbx_commesse_aperte,&
this.st_17,&
this.st_15,&
this.sle_cod_prodotto,&
this.st_16,&
this.em_count_smp,&
this.ddlb_op_smp,&
this.st_smp,&
this.cb_procedi,&
this.dw_var_ord_ven,&
this.em_count_smp_coll,&
this.ddlb_op_smp_coll,&
this.st_14,&
this.em_count_smp_x,&
this.ddlb_op_smp_x,&
this.st_13,&
this.em_count_smp_y,&
this.ddlb_op_smp_y,&
this.st_12,&
this.cbx_smp_mancanti,&
this.ddlb_op_having,&
this.em_having,&
this.st_11,&
this.dw_var_com,&
this.sle_data_chiu_com,&
this.sle_qta_com_prodotta,&
this.sle_qta_com_ordinata,&
this.sle_commessa,&
this.sle_stato_commessa,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_2,&
this.dw_fatture,&
this.cb_produzione,&
this.dw_ddt_trasf,&
this.st_6,&
this.dw_righe_ordini,&
this.dw_movimenti,&
this.cb_cerca,&
this.em_anno_ordine,&
this.st_5,&
this.st_4,&
this.st_3,&
this.st_1,&
this.dw_cpf_count,&
this.r_1,&
this.r_2,&
this.em_num_ordine,&
this.em_anno_commessa,&
this.em_num_commessa,&
this.em_riga_ordine,&
this.em_chius_al,&
this.em_chius_dal}
end on

on w_aggiusta_cpf.destroy
destroy(this.cbx_mov_manuali)
destroy(this.cb_cerca_lista)
destroy(this.cb_1)
destroy(this.dw_lista_varianti_versione_errata)
destroy(this.cbx_commesse_aperte)
destroy(this.st_17)
destroy(this.st_15)
destroy(this.sle_cod_prodotto)
destroy(this.st_16)
destroy(this.em_count_smp)
destroy(this.ddlb_op_smp)
destroy(this.st_smp)
destroy(this.cb_procedi)
destroy(this.dw_var_ord_ven)
destroy(this.em_count_smp_coll)
destroy(this.ddlb_op_smp_coll)
destroy(this.st_14)
destroy(this.em_count_smp_x)
destroy(this.ddlb_op_smp_x)
destroy(this.st_13)
destroy(this.em_count_smp_y)
destroy(this.ddlb_op_smp_y)
destroy(this.st_12)
destroy(this.cbx_smp_mancanti)
destroy(this.ddlb_op_having)
destroy(this.em_having)
destroy(this.st_11)
destroy(this.dw_var_com)
destroy(this.sle_data_chiu_com)
destroy(this.sle_qta_com_prodotta)
destroy(this.sle_qta_com_ordinata)
destroy(this.sle_commessa)
destroy(this.sle_stato_commessa)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_2)
destroy(this.dw_fatture)
destroy(this.cb_produzione)
destroy(this.dw_ddt_trasf)
destroy(this.st_6)
destroy(this.dw_righe_ordini)
destroy(this.dw_movimenti)
destroy(this.cb_cerca)
destroy(this.em_anno_ordine)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.dw_cpf_count)
destroy(this.r_1)
destroy(this.r_2)
destroy(this.em_num_ordine)
destroy(this.em_anno_commessa)
destroy(this.em_num_commessa)
destroy(this.em_riga_ordine)
destroy(this.em_chius_al)
destroy(this.em_chius_dal)
end on

event open;integer li_anno

is_sql_righe_CPF_conteggio = dw_cpf_count.getsqlselect()

li_anno = f_anno_esercizio()

em_anno_commessa.text = string(li_anno)
em_anno_ordine.text = string(li_anno)

dw_cpf_count.SetTransObject(SQLCA)
dw_VAR_COM.SetTransObject(SQLCA)
dw_movimenti.SetTransObject(SQLCA)
dw_ddt_trasf.SetTransObject(SQLCA)
dw_fatture.SetTransObject(SQLCA)
dw_var_ord_ven.SetTransObject(SQLCA)

ddlb_op_having.text = ">"

ddlb_op_smp_y.text = "<"
ddlb_op_smp_x.text = "<"
ddlb_op_smp_coll.text = "<"

ddlb_op_smp.text = "<="


em_chius_dal.text = string(date(2012,1,1), "dd/mm/yy")
em_chius_al.text = string(date(2012,12,31), "dd/mm/yy")


end event

type cbx_mov_manuali from checkbox within w_aggiusta_cpf
integer x = 2423
integer y = 512
integer width = 434
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "CPF manuali"
boolean lefttext = true
end type

type cb_cerca_lista from commandbutton within w_aggiusta_cpf
integer x = 1605
integer y = 924
integer width = 736
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<< cerca (lista affianco)"
end type

event clicked;string			ls_cod_prodotto
datetime		ldt_dal, ldt_al



ls_cod_prodotto = sle_cod_prodotto.text
ldt_dal = datetime(date(em_chius_dal.text), 00:00:00)
ldt_al = datetime(date(em_chius_al.text), 00:00:00)

dw_lista_varianti_versione_errata.settransobject(sqlca)

if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then ls_cod_prodotto = "%"
dw_lista_varianti_versione_errata.retrieve(ls_cod_prodotto,ldt_dal,ldt_al)
end event

type cb_1 from commandbutton within w_aggiusta_cpf
integer x = 14
integer y = 2600
integer width = 987
integer height = 92
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Correggi Depositi ticket #2014/73"
end type

event clicked;string ls_errore
long ll_ret, ll_i

for ll_i = 1 to dw_lista_varianti_versione_errata.rowcount()
	ll_ret = wf_correggi_commesse_deposito_errato(ll_i, true, ls_errore) 
	choose case ll_ret 
		case 0
			commit;
		case -1
			rollback;
			g_mb.error(ls_errore)
		case -2
			rollback;
			g_mb.warning(ls_errore)
		case else
			rollback;
			g_mb.messagebox("Messaggio", ls_errore)
	end choose
next

g_mb.messagebox("Rigenerazione Movimenti Magazzino Commesse", "L'elaborazione è stata eseguita automaticamente.")


end event

type dw_lista_varianti_versione_errata from datawindow within w_aggiusta_cpf
integer x = 14
integer y = 12
integer width = 1513
integer height = 2572
integer taborder = 20
string title = "none"
string dataobject = "d_lista_varianti_versione_errata1"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose  case dwo.name
	case "b_ricalcola"
		string ls_errore
		long ll_ret
			ll_ret = wf_correggi_commesse_deposito_errato(row, false, ls_errore) 
			choose case ll_ret 
				case 0
					commit;
					g_mb.messagebox("Elaborazione corretta", ls_errore)
				case -1
					rollback;
					g_mb.error(ls_errore)
				case -2
					rollback;
					g_mb.warning(ls_errore)
				case else
					rollback;
					g_mb.messagebox("Messaggio", ls_errore)
			end choose
		
		
//	boolean				lb_solo_trasferimenti, lb_automatico
//	string				ls_errore, ls_depositi_prod[], ls_cod_prodotto, ls_cod_versione, ls_reparti[], ls_semilav[]
//	integer				li_anno_commessa, li_ret, li_anno_ordine
//	long					ll_num_commessa, ll_count, ll_num_ordine, ll_riga_ordine, ll_currow
//	uo_funzioni_1		luo_funz
//	
//	
//	lb_automatico = ib_automatico
//	ib_automatico = false
//	
//	if row >0 then
//		li_anno_commessa = getitemnumber(row, "det_ord_ven_anno_commessa")
//		ll_num_commessa = getitemnumber(row, "det_ord_ven_num_commessa")
//		
//		if li_anno_commessa=0 or ll_num_commessa=0 or isnull(li_anno_commessa) or isnull(ll_num_commessa) then
//			g_mb.error("Commessa non associata alla riga ordine")
//			return
//		end if	
//	else
//		g_mb.error("Riga DW non coerente!")
//		return
//	end if
//	
//	update 	varianti_commesse
//	set		cod_versione_variante = cod_versione_figlio
//	where 	cod_azienda = :s_cs_xx.cod_azienda and
//				anno_commessa = :li_anno_commessa and
//				num_commessa = :ll_num_commessa;
//				
//	if sqlca.sqlcode < 0 then
//		rollback;
//		g_mb.error("Errore in UPDATE sql tabella varianti commesse.~r~n" + sqlca.sqlerrtext)
//		return
//	end if		
//				
//
//	li_anno_ordine = getitemnumber(row, "tes_ord_ven_anno_registrazione")
//	ll_num_ordine = getitemnumber(row, "det_ord_ven_num_registrazione")
//	ll_riga_ordine = getitemnumber(row, "det_ord_ven_prog_riga_ord_ven")
//	ls_cod_prodotto = getitemstring(row, "det_ord_ven_cod_prodotto")
//	ls_cod_versione = getitemstring(row, "det_ord_ven_cod_versione")
//	
//	w_cs_xx_mdi.setmicrohelp(g_str.format("In elaborazione ordine $1/$2/$3 prodotto $3", li_anno_ordine, ll_num_ordine,ll_riga_ordine,ls_cod_prodotto ) )
//	
//	update 	varianti_det_ord_ven
//	set		cod_versione_variante = cod_versione_figlio
//	where 	cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione = :li_anno_ordine and
//				num_registrazione = :ll_num_ordine and
//				prog_riga_ord_ven = :ll_riga_ordine;
//				
//	if sqlca.sqlcode < 0 then
//		rollback;
//		g_mb.error("Errore in UPDATE sql tabella varianti ordini.~r~n" + sqlca.sqlerrtext)
//		return
//	end if		
//				
//
//	
//
//	if not lb_automatico  then
//		if not g_mb.confirm( "Rigenerare i movimenti per la commessa "+string(li_anno_commessa)+"/"+string(ll_num_commessa) + " ?") then return
//	end if
//	
//	luo_funz = create uo_funzioni_1
//	
//		
//		li_ret = luo_funz.uof_trova_reparti_e_semilavorati(true, li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_prodotto, ls_cod_versione, &
//													 ls_reparti[], ls_semilav[], ls_depositi_prod[], ls_errore)
//		
//		if li_ret < 0 then
//			destroy luo_funz
//			rollback;
//			g_mb.error("Errore in ricerca reparti (uof_trova_reparti_e_semilavorati): " + ls_errore)
//			return
//		end if
//		
//		if upperbound(ls_depositi_prod[]) = 0 then
//			lb_solo_trasferimenti = false
//			goto PROSEGUI
//		end if
//		
//		li_ret = luo_funz.uof_se_commessa_con_trasf(li_anno_commessa, ll_num_commessa, ls_depositi_prod[], ls_errore)
//		
//		if li_ret<0 then
//			destroy luo_funz
//			rollback;
//			g_mb.error(ls_errore)
//			return
//			
//		elseif li_ret=1 then
//			//commessa non soggetta a trasferimento
//			lb_solo_trasferimenti = false
//		else
//			//torna 0 quindi la commessa è soggetta a trasferimento
//			lb_solo_trasferimenti = true
//		end if
//		
//	//end if
//	
//	
//	PROSEGUI:
//	
//	li_ret = luo_funz.uof_riallinea_mov_commessa(li_anno_commessa, ll_num_commessa, lb_solo_trasferimenti, ls_errore)
//	
//	choose case li_ret
//		case is < 0
//			rollback;
//			destroy luo_funz
//			g_mb.error(ls_errore)
//			return
//			
//		case 1
//			//dai il messaggio (in caso di elaborazione multipla salterà la commessa ma andrà avanti)
//			rollback;
//			destroy luo_funz
//			g_mb.warning(ls_errore)
//			return
//			
//		case else
//			//tutto a posto, operazioe completata
//			commit;
//			destroy luo_funz
//			g_mb.success("Operazione effettuata con successo!")
//	end choose

		
end choose
end event

type cbx_commesse_aperte from checkbox within w_aggiusta_cpf
integer x = 2533
integer y = 40
integer width = 485
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "anche aperte"
end type

type st_17 from statictext within w_aggiusta_cpf
integer x = 2583
integer y = 152
integer width = 82
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "al"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_15 from statictext within w_aggiusta_cpf
integer x = 1559
integer y = 152
integer width = 667
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Chiusura commessa Dal:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_cod_prodotto from singlelineedit within w_aggiusta_cpf
integer x = 2126
integer y = 380
integer width = 709
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_16 from statictext within w_aggiusta_cpf
integer x = 1641
integer y = 408
integer width = 462
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prodotto CPF:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_count_smp from editmask within w_aggiusta_cpf
boolean visible = false
integer x = 2016
integer y = 712
integer width = 206
integer height = 88
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "10"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "0~~9999"
end type

type ddlb_op_smp from dropdownlistbox within w_aggiusta_cpf
boolean visible = false
integer x = 1819
integer y = 704
integer width = 174
integer height = 376
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string item[] = {"<","<=",""}
borderstyle borderstyle = stylelowered!
end type

type st_smp from statictext within w_aggiusta_cpf
boolean visible = false
integer x = 1559
integer y = 712
integer width = 247
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SMP:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_procedi from commandbutton within w_aggiusta_cpf
integer x = 2542
integer y = 828
integer width = 457
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Rigenera Mov."
end type

event clicked;integer				li_anno_commessa, li_ret, li_anno_ordine
long					ll_num_commessa, ll_count, ll_num_ordine, ll_riga_ordine, ll_currow
uo_funzioni_1		luo_funz
string					ls_errore, ls_depositi_prod[], ls_cod_prodotto, ls_cod_versione, ls_reparti[], ls_semilav[]
boolean				lb_solo_trasferimenti


li_anno_commessa = integer(em_anno_commessa.text)
ll_num_commessa = long(em_num_commessa.text)
ll_currow = dw_cpf_count.getrow()

if li_anno_commessa>0 and ll_num_commessa>0 then
else

	if ll_currow>0 then
		li_anno_commessa = dw_cpf_count.getitemnumber(ll_currow, "anno_documento")
		ll_num_commessa = dw_cpf_count.getitemnumber(ll_currow, "num_documento")
		
		if li_anno_commessa>0 and ll_num_commessa>0 then
		else
			g_mb.error("Digitare un anno/numero commessa nelle apposite caselle oppure selezionarla dalla lista CPF!")
		return
		end if
		
	else
		g_mb.error("Digitare un anno/numero commessa nelle apposite caselle oppure selezionarla dalla lista CPF!")
		return
	end if
	
end if


if cbx_smp_mancanti.checked then
	
	select anno_registrazione, num_registrazione, prog_riga_ord_ven,
			cod_prodotto, cod_versione
	into 	:li_anno_ordine, :ll_num_ordine, :ll_riga_ordine,
			:ls_cod_prodotto, :ls_cod_versione
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and 
				anno_commessa=:li_anno_commessa and
				num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode<0 then
		
	elseif sqlca.sqlcode=100 then
		g_mb.error("La commessa non è associata a nessun ordine")
	end if
	
else
	lb_solo_trasferimenti = false
end if

if not g_mb.confirm( "Rigenerare i movimenti per la commessa "+string(li_anno_commessa)+"/"+string(ll_num_commessa) + " ?") then return

luo_funz = create uo_funzioni_1

if cbx_smp_mancanti.checked then
	
	li_ret = luo_funz.uof_trova_reparti_e_semilavorati(true, li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_prodotto, ls_cod_versione, &
												 ls_reparti[], ls_semilav[], ls_depositi_prod[], ls_errore)
	
	if li_ret < 0 then
		destroy luo_funz
		rollback;
		g_mb.error("Errore in ricerca reparti (uof_trova_reparti_e_semilavorati): " + ls_errore)
		return
	end if
	
	if upperbound(ls_depositi_prod[]) = 0 then
		lb_solo_trasferimenti = false
		goto PROSEGUI
	end if
	
	li_ret = luo_funz.uof_se_commessa_con_trasf(li_anno_commessa, ll_num_commessa, ls_depositi_prod[], ls_errore)
	
	if li_ret<0 then
		//in ls_errore il messaggio
		destroy luo_funz
		rollback;
		g_mb.error(ls_errore)
		return
		
	elseif li_ret=1 then
		//commessa non soggetta a trasferimento
		lb_solo_trasferimenti = false
	else
		//torna 0 quindi la commessa è soggetta a trasferimento
		lb_solo_trasferimenti = true
	end if
	
end if


PROSEGUI:

li_ret = luo_funz.uof_riallinea_mov_commessa(li_anno_commessa, ll_num_commessa, lb_solo_trasferimenti, ls_errore)
destroy luo_funz

choose case li_ret
	case is < 0
		rollback;
		g_mb.error(ls_errore)
		return
		
	case 1
		//dai il messaggio (in caso di elaborazione multipla salterà la commessa ma andrà avanti)
		g_mb.warning(ls_errore)
		rollback;
		return
		
	case else
		//tutto a posto, operazioe completata
		commit;
		g_mb.success("Operazione effettuata con successo!")
		//cb_cerca.postevent(clicked!)
		if ll_currow>0 then
			wf_selectrow_count_cpf(ll_currow)
		else
			cb_cerca.postevent(clicked!)
		end if
end choose


end event

type dw_var_ord_ven from datawindow within w_aggiusta_cpf
integer x = 5819
integer y = 512
integer width = 2171
integer height = 516
integer taborder = 50
boolean titlebar = true
string title = "Varianti riga ordine con Dep. produzione associati"
string dataobject = "d_var_ord_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type em_count_smp_coll from editmask within w_aggiusta_cpf
boolean visible = false
integer x = 8585
integer y = 336
integer width = 206
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "5"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "0~~9999"
end type

type ddlb_op_smp_coll from dropdownlistbox within w_aggiusta_cpf
boolean visible = false
integer x = 8389
integer y = 332
integer width = 174
integer height = 376
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string item[] = {"<","",""}
borderstyle borderstyle = stylelowered!
end type

type st_14 from statictext within w_aggiusta_cpf
boolean visible = false
integer x = 8123
integer y = 340
integer width = 265
integer height = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SMP coll.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_count_smp_x from editmask within w_aggiusta_cpf
boolean visible = false
integer x = 9280
integer y = 92
integer width = 206
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "5"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "0~~9999"
end type

type ddlb_op_smp_x from dropdownlistbox within w_aggiusta_cpf
boolean visible = false
integer x = 9093
integer y = 92
integer width = 174
integer height = 376
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string item[] = {"<","",""}
borderstyle borderstyle = stylelowered!
end type

type st_13 from statictext within w_aggiusta_cpf
boolean visible = false
integer x = 8827
integer y = 92
integer width = 247
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SMP ~"X~":"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_count_smp_y from editmask within w_aggiusta_cpf
boolean visible = false
integer x = 8535
integer y = 76
integer width = 206
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "0~~9999"
end type

type ddlb_op_smp_y from dropdownlistbox within w_aggiusta_cpf
boolean visible = false
integer x = 8334
integer y = 84
integer width = 174
integer height = 376
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string item[] = {"<","",""}
borderstyle borderstyle = stylelowered!
end type

type st_12 from statictext within w_aggiusta_cpf
boolean visible = false
integer x = 8073
integer y = 92
integer width = 247
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SMP ~"Y~":"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_smp_mancanti from checkbox within w_aggiusta_cpf
integer x = 1577
integer y = 604
integer width = 1326
integer height = 104
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SMP mancanti (esclude prodotti 2a cifra NON 0)"
boolean lefttext = true
end type

event clicked;boolean lb_visible

if this.checked then
	ddlb_op_having.text = "="
	lb_visible = true
else
	ddlb_op_having.text = ">"
	lb_visible = false
end if

em_having.text = "1"

st_smp.visible= lb_visible
ddlb_op_smp.visible= lb_visible
em_count_smp.visible= lb_visible
end event

type ddlb_op_having from dropdownlistbox within w_aggiusta_cpf
integer x = 1806
integer y = 496
integer width = 210
integer height = 376
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string item[] = {">","=",">="}
borderstyle borderstyle = stylelowered!
end type

type em_having from editmask within w_aggiusta_cpf
integer x = 2034
integer y = 496
integer width = 265
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "0~~9999"
end type

type st_11 from statictext within w_aggiusta_cpf
integer x = 1568
integer y = 508
integer width = 219
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Count:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_var_com from datawindow within w_aggiusta_cpf
integer x = 5819
integer y = 28
integer width = 2171
integer height = 480
integer taborder = 30
boolean titlebar = true
string title = "Varianti Commessa con Dep. produzione associati"
string dataobject = "d_var_com"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type sle_data_chiu_com from singlelineedit within w_aggiusta_cpf
integer x = 3186
integer y = 1164
integer width = 357
integer height = 80
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type sle_qta_com_prodotta from singlelineedit within w_aggiusta_cpf
integer x = 3607
integer y = 1080
integer width = 357
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type sle_qta_com_ordinata from singlelineedit within w_aggiusta_cpf
integer x = 2761
integer y = 1080
integer width = 357
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type sle_commessa from singlelineedit within w_aggiusta_cpf
integer x = 1838
integer y = 1080
integer width = 443
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type sle_stato_commessa from singlelineedit within w_aggiusta_cpf
integer x = 1829
integer y = 1164
integer width = 1029
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_10 from statictext within w_aggiusta_cpf
integer x = 1550
integer y = 1176
integer width = 274
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stato:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_9 from statictext within w_aggiusta_cpf
integer x = 2816
integer y = 1176
integer width = 343
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Data Chius.:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_8 from statictext within w_aggiusta_cpf
integer x = 3145
integer y = 1092
integer width = 443
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Quan.Prodotta:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_7 from statictext within w_aggiusta_cpf
integer x = 2304
integer y = 1092
integer width = 443
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Quan.Ordinata:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_aggiusta_cpf
integer x = 1554
integer y = 1092
integer width = 274
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Comm. N°"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_fatture from datawindow within w_aggiusta_cpf
integer x = 1559
integer y = 2304
integer width = 3214
integer height = 376
integer taborder = 40
boolean titlebar = true
string title = "Fatture"
string dataobject = "d_fatture_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_produzione from commandbutton within w_aggiusta_cpf
integer x = 4334
integer y = 1280
integer width = 402
integer height = 92
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Stato Prod."
end type

event clicked;integer li_anno_registrazione
long ll_num_registrazione, ll_row

ll_row = dw_righe_ordini.getrow()

if ll_row>0 then
else
	return
end if

li_anno_registrazione = dw_righe_ordini.getitemnumber(ll_row, 2)
ll_num_registrazione = dw_righe_ordini.getitemnumber(ll_row, 3)

s_cs_xx.parametri.parametro_d_1 = li_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
		
window_open(w_det_ord_ven_stato,-1)
end event

type dw_ddt_trasf from datawindow within w_aggiusta_cpf
integer x = 1559
integer y = 1784
integer width = 3214
integer height = 504
integer taborder = 20
boolean titlebar = true
string title = "DDT di trasf."
string dataobject = "d_bolle_trasf"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_6 from statictext within w_aggiusta_cpf
integer x = 2459
integer y = 288
integer width = 87
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_righe_ordini from datawindow within w_aggiusta_cpf
integer x = 1559
integer y = 1276
integer width = 3214
integer height = 488
integer taborder = 20
boolean titlebar = true
string title = "Righe Ordini"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
	
	wf_prepara_dw_ddt_trasf(currentrow)
else
	wf_prepara_dw_ddt_trasf(-1)
end if


end event

type dw_movimenti from datawindow within w_aggiusta_cpf
integer x = 4805
integer y = 1064
integer width = 3186
integer height = 1684
integer taborder = 20
boolean titlebar = true
string title = "Movimenti per Commessa"
string dataobject = "d_movimenti_commessa"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;//long ll_index, ll_tot, ll_num_mov
//string ls_selezionato
//boolean lb_trovato = false
//integer li_anno_mov
//
//accepttext()
//
//if row>0 then
//else
//	return
//end if
//
//return
//
//ll_tot = rowcount()
//if ll_tot>0 then
//else
//	g_mb.error("Nessun movimento presente da aggiornare!")
//	return
//end if
//
//
//choose case dwo.name
//	case "b_salva"
//		//###############################################################################
//		if this.update() = 1 then
//			commit;
//			g_mb.show("Salvataggio effettuato con successo!")
//		else
//			g_mb.error("Errore in update!")
//			rollback;
//			return
//		end if
//		//###############################################################################
//	
//	
//	case "b_elimina"
//		//###############################################################################
//		for ll_index=1 to ll_tot
//			ls_selezionato = getitemstring(ll_index, "selezionato")
//			
//			if ls_selezionato = "S" then
//				lb_trovato = true
//			end if
//		next
//		
//		if not lb_trovato then
//			g_mb.error("Nessun movimento selezionato!")
//			return
//		end if
//		
//		for ll_index=ll_tot to 1 step -1
//			ls_selezionato = getitemstring(ll_index, "selezionato")
//			
//			if ls_selezionato = "S" then
//				li_anno_mov = getitemnumber(ll_index, "anno_registrazione")
//				ll_num_mov = getitemnumber(ll_index, "num_registrazione")
//				
//				delete from mov_magazzino
//				where 	cod_azienda=:s_cs_xx.cod_azienda and
//							num_registrazione=:li_anno_mov and
//							anno_registrazione=:ll_num_mov;
//				
//				if sqlca.sqlcode<0 then
//					g_mb.error("Errore in elimina movimento: "+sqlca.sqlerrtext)
//					rollback;
//					return
//				end if
//				
//			end if
//		next
//		
//		//###############################################################################
//		
//end choose
end event

type cb_cerca from commandbutton within w_aggiusta_cpf
integer x = 2537
integer y = 924
integer width = 457
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;wf_dw_cpf_count()
end event

type em_anno_ordine from editmask within w_aggiusta_cpf
integer x = 1810
integer y = 272
integer width = 293
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "1~~9999"
end type

type st_5 from statictext within w_aggiusta_cpf
integer x = 2085
integer y = 288
integer width = 87
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_4 from statictext within w_aggiusta_cpf
integer x = 1573
integer y = 288
integer width = 219
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_aggiusta_cpf
integer x = 1554
integer y = 44
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Comm:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_aggiusta_cpf
integer x = 1970
integer y = 44
integer width = 87
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_cpf_count from datawindow within w_aggiusta_cpf
integer x = 3054
integer y = 36
integer width = 2752
integer height = 992
integer taborder = 10
boolean titlebar = true
string title = "Count CPF per commessa"
string dataobject = "d_conteggio_cpf"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;
wf_selectrow_count_cpf(currentrow)

end event

type r_1 from rectangle within w_aggiusta_cpf
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 1541
integer y = 1044
integer width = 3214
integer height = 216
end type

type r_2 from rectangle within w_aggiusta_cpf
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 1545
integer y = 16
integer width = 1477
integer height = 1008
end type

type em_num_ordine from editmask within w_aggiusta_cpf
integer x = 2158
integer y = 276
integer width = 329
integer height = 88
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######0"
boolean spin = true
double increment = 1
string minmax = "0~~9999999"
end type

type em_anno_commessa from editmask within w_aggiusta_cpf
integer x = 1774
integer y = 28
integer width = 206
integer height = 88
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
double increment = 1
string minmax = "1~~9999"
end type

type em_num_commessa from editmask within w_aggiusta_cpf
integer x = 2043
integer y = 32
integer width = 329
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######0"
boolean spin = true
double increment = 1
string minmax = "0~~9999999"
end type

type em_riga_ordine from editmask within w_aggiusta_cpf
integer x = 2523
integer y = 276
integer width = 302
integer height = 88
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######0"
boolean spin = true
double increment = 1
string minmax = "1~~9999999"
end type

type em_chius_al from editmask within w_aggiusta_cpf
integer x = 2674
integer y = 128
integer width = 343
integer height = 104
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yy"
boolean dropdowncalendar = true
end type

type em_chius_dal from editmask within w_aggiusta_cpf
integer x = 2235
integer y = 128
integer width = 343
integer height = 104
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yy"
boolean dropdowncalendar = true
end type

