﻿$PBExportHeader$w_det_bol_ven_ptenda.srw
$PBExportComments$Finestra Dettagli bolle (Edreditata da det_bol_ven)
forward
global type w_det_bol_ven_ptenda from w_det_bol_ven
end type
type dw_vis_comp_det_ord_ven from datawindow within w_det_bol_ven_ptenda
end type
type p_1 from picture within w_det_bol_ven_ptenda
end type
end forward

global type w_det_bol_ven_ptenda from w_det_bol_ven
integer height = 2572
dw_vis_comp_det_ord_ven dw_vis_comp_det_ord_ven
p_1 p_1
end type
global w_det_bol_ven_ptenda w_det_bol_ven_ptenda

forward prototypes
public subroutine wf_vis_comp_det_ord_ven (long fn_anno_registrazione, long fn_num_registrazione, long fn_prog_riga_ord_ven)
end prototypes

public subroutine wf_vis_comp_det_ord_ven (long fn_anno_registrazione, long fn_num_registrazione, long fn_prog_riga_ord_ven);string ls_cod_prodotto_finito, ls_cod_tessuto, ls_colore_tessuto, ls_cod_tessuto_mant, ls_cod_colore_mant,   &
       ls_cod_verniciatura, ls_cod_comando_1, ls_note, ls_des_vernice_fc, ls_des_comando_1, ls_cod_comando_2, &
		 ls_des_comando_2, ls_flag_add_comando_1, ls_flag_add_comando_2, ls_flag_add_tessuto, ls_flag_add_verniciatura  
long   ll_riga		 
double ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_alt_mantovana 

dw_vis_comp_det_ord_ven.reset()
dw_vis_comp_det_ord_ven.setredraw(false)

SELECT cod_prod_finito,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		cod_tessuto,   
		cod_non_a_magazzino,   
		cod_tessuto_mant,   
		cod_mant_non_a_magazzino,   
		alt_mantovana,   
		cod_verniciatura,   
		cod_comando,   
		note,   
		des_vernice_fc,   
		des_comando_1,   
		cod_comando_2,   
		des_comando_2,   
		flag_addizionale_comando_1,   
		flag_addizionale_comando_2,   
		flag_addizionale_tessuto,   
		flag_addizionale_vernic  
 INTO :ls_cod_prodotto_finito,   
		:ld_dim_x,   
		:ld_dim_y,   
		:ld_dim_z,   
		:ld_dim_t,   
		:ls_cod_tessuto,   
		:ls_colore_tessuto,   
		:ls_cod_tessuto_mant,   
		:ls_cod_colore_mant,   
		:ld_alt_mantovana,   
		:ls_cod_verniciatura,   
		:ls_cod_comando_1,   
		:ls_note,   
		:ls_des_vernice_fc,   
		:ls_des_comando_1,   
		:ls_cod_comando_2,   
		:ls_des_comando_2,   
		:ls_flag_add_comando_1,   
		:ls_flag_add_comando_2,   
		:ls_flag_add_tessuto,   
		:ls_flag_add_verniciatura  
 FROM comp_det_ord_ven  
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = :fn_anno_registrazione ) AND  
		( num_registrazione = :fn_num_registrazione ) AND  
		( prog_riga_ord_ven = :fn_prog_riga_ord_ven )   ;
if sqlca.sqlcode = 100 then
	return
end if

ll_riga = dw_vis_comp_det_ord_ven.insertrow(0)

dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_prod_finito", ls_cod_prodotto_finito)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_x",ld_dim_x)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_y",ld_dim_y)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_z",ld_dim_z)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_t",ld_dim_t)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_tessuto",ls_cod_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_non_a_magazzino",ls_colore_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_verniciatura",ls_cod_verniciatura)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"des_vernice_fc",ls_des_vernice_fc)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_comando",ls_cod_comando_1)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_comando_2",ls_cod_comando_2)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_comando_1",ls_flag_add_comando_1)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_comando_2",ls_flag_add_comando_2)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_tessuto",ls_flag_add_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_vernic",ls_flag_add_verniciatura)

dw_vis_comp_det_ord_ven.resetupdate()
dw_vis_comp_det_ord_ven.setredraw(true)

return
end subroutine

on w_det_bol_ven_ptenda.create
int iCurrent
call super::create
this.dw_vis_comp_det_ord_ven=create dw_vis_comp_det_ord_ven
this.p_1=create p_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_vis_comp_det_ord_ven
this.Control[iCurrent+2]=this.p_1
end on

on w_det_bol_ven_ptenda.destroy
call super::destroy
destroy(this.dw_vis_comp_det_ord_ven)
destroy(this.p_1)
end on

event pc_setwindow;call super::pc_setwindow;p_1.picturename = s_cs_xx.volume + s_cs_xx.risorse + "SUC.BMP"
dw_det_bol_ven_lista.setrowfocusindicator(p_1)
end event

type cb_sconti from w_det_bol_ven`cb_sconti within w_det_bol_ven_ptenda
end type

type cb_des_mov from w_det_bol_ven`cb_des_mov within w_det_bol_ven_ptenda
end type

type uo_1 from w_det_bol_ven`uo_1 within w_det_bol_ven_ptenda
end type

type cb_corrispondenze from w_det_bol_ven`cb_corrispondenze within w_det_bol_ven_ptenda
end type

type cb_prodotti_note_ricerca from w_det_bol_ven`cb_prodotti_note_ricerca within w_det_bol_ven_ptenda
end type

type dw_folder from w_det_bol_ven`dw_folder within w_det_bol_ven_ptenda
integer x = 18
integer y = 16
integer width = 4539
end type

event dw_folder::po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab
	case 1
		dw_det_bol_ven_lista.change_dw_focus(dw_det_bol_ven_lista)
	case 2
		dw_det_bol_ven_det_1.change_dw_focus(dw_det_bol_ven_det_1)
end choose
		
end event

type dw_det_bol_ven_lista from w_det_bol_ven`dw_det_bol_ven_lista within w_det_bol_ven_ptenda
event ue_posiziona_ultima_riga ( )
integer x = 41
integer width = 4475
end type

event dw_det_bol_ven_lista::ue_posiziona_ultima_riga();setrow( rowcount() )
end event

event dw_det_bol_ven_lista::rowfocuschanged;call super::rowfocuschanged;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven

if i_extendmode and getrow() > 0 then
	ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione_ord_ven")
	ll_num_registrazione = getitemnumber(getrow(),"num_registrazione_ord_ven")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	wf_vis_comp_det_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
end if
end event

event dw_det_bol_ven_lista::ue_key;//
//		ATTENZIONE in questo eventio tenere disabilitata la proprità EXTEND
//
string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long   ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
	case "sconto_2"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
	case "prezzo_vendita"
		choose case key

			case keyTab! 
				if keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
					this.triggerevent("pcd_save")
					this.postevent("pcd_new")
				end if
				
			case keyenter! 
				this.triggerevent("pcd_save")
				this.postevent("pcd_new")
				
			case keyF1!, keyF2!, keyF3!, keyF4!, keyF5!
				if keyflags = 1 then
					setpointer(hourglass!)
					ldt_data_registrazione = dw_det_bol_ven_lista.i_parentdw.getitemdatetime(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
					ls_cod_cliente = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
					ls_cod_valuta = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
					ls_cod_agente_1 = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
					ls_cod_agente_2 = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
					ld_cambio_ven = dw_det_bol_ven_lista.i_parentdw.getitemnumber(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
					ls_cod_tipo_det_ven = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_tipo_det_ven")
					ls_cod_prodotto = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_prodotto")
					ld_quantita = dw_det_bol_ven_lista.getitemnumber(dw_det_bol_ven_lista.getrow(), "quan_fatturata")
					choose case key
						case keyF1!
							ls_listino = 'LI1'
						case keyF2!
							ls_listino = 'LI2'
						case keyF3!
							ls_listino = 'LI3'
						case keyF4!
							ls_listino = 'LI4'
						case keyF5!
							ls_listino = 'LI5'
						case else
							return
					end choose
					if ls_listino <> "LI5" then
						select stringa
						into  :ls_stringa
						from  parametri_azienda
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_parametro = :ls_listino ;
						if sqlca.sqlcode = 0 and mid(f_flag_controllo(),1,1) = "S" then
							iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
							iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
							iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
							iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
							iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
							iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
							iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
							iuo_condizioni_cliente.str_parametri.dim_1 = 0
							iuo_condizioni_cliente.str_parametri.dim_2 = 0
							iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
							iuo_condizioni_cliente.str_parametri.valore = 0
							iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
							iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
							iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_consegnata"
							iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
							iuo_condizioni_cliente.wf_condizioni_cliente()
						end if			
						setpointer(arrow!)
					else
						select prezzo_acquisto
						into   :ld_prezzo_acquisto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
						dw_det_bol_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
					end if			
					
				end if
			
			case keyF11! 
				// EnMe 04/07/2012 per caricamento prezzo da ultima fattura di acquisto in caso di bolle di trasferimento interno
				string ls_sql_prezzo,ls_rag_soc_for
				long ll_ret, ll_anno_reg_mov_mag, ll_num_reg_mov_mag, ll_anno_reg_tes_fat,ll_num_reg_tes_fat
				dec{4} ld_prezzo_trasferimento
				datetime ldt_data_protocollo
				datastore lds_fatture
		
				ls_cod_prodotto = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_prodotto")
				ldt_data_registrazione = dw_det_bol_ven_lista.i_parentdw.getitemdatetime(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")

				ls_sql_prezzo = 	"select tes_fat_acq.data_protocollo, mov_magazzino.val_movimento, mov_magazzino.anno_registrazione,mov_magazzino.num_registrazione, tes_fat_acq.anno_registrazione, tes_fat_acq.num_registrazione, anag_fornitori.rag_soc_1 from tes_fat_acq " + &
										" join det_fat_acq on tes_fat_acq.anno_registrazione =  det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione =  det_fat_acq.num_registrazione " + &
										" join anag_fornitori on tes_fat_acq.cod_azienda =  anag_fornitori.cod_azienda and tes_fat_acq.cod_fornitore =  anag_fornitori.cod_fornitore " + &
										" join mov_magazzino on  det_fat_acq.anno_registrazione_mov_mag = mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione " + &
										" where data_protocollo <= '"+string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and flag_agg_mov='S' and det_fat_acq.cod_prodotto = '" + ls_cod_prodotto + "' " + &
										" order by  tes_fat_acq.data_protocollo DESC , tes_fat_acq.num_registrazione DESC , det_fat_acq.prog_riga_fat_acq DESC "
							
				
				ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo)
				
				if ll_ret < 0 then
					destroy lds_fatture
					g_mb.error("Si è verificato un errore cercando il prezzo del prodotto " + ls_cod_prodotto + " dalle fatture di acquisto ")
					return
				end if
				
				if ll_ret > 0 then
					
					ldt_data_protocollo = lds_fatture.getitemdatetime(1, 1)
					ld_prezzo_trasferimento = lds_fatture.getitemnumber (1, 2)
					ll_anno_reg_mov_mag = lds_fatture.getitemnumber (1, 3)
					ll_num_reg_mov_mag = lds_fatture.getitemnumber (1, 4)
					ll_anno_reg_tes_fat = lds_fatture.getitemnumber (1, 5)
					ll_num_reg_tes_fat = lds_fatture.getitemnumber (1, 6)
					ls_rag_soc_for = lds_fatture.getitemstring (1, 7)
					
					setitem(getrow(),"prezzo_vendita", ld_prezzo_trasferimento)
					w_cs_xx_mdi.setmicrohelp("Applicato Prezzo di acquisto "+string(ld_prezzo_trasferimento,"###,##0.00")+" riferito alla data " + string(ldt_data_protocollo,"dd/mm/yyyy") + " (Mov Mag:" + string(ll_anno_reg_mov_mag) + "-" + string(ll_num_reg_mov_mag) + "  Nr.Int.FT:" + string(ll_anno_reg_tes_fat) + "-" + string(ll_num_reg_tes_fat) + " Fornitore: " + ls_rag_soc_for + ")" )
				else
					w_cs_xx_mdi.setmicrohelp("Nessun prezzo di trasferimento reperibile dalle fatture di acquisto.")
				end if
				
				destroy lds_fatture
				// fine modifica 04/07/2012
				
				setpointer(arrow!)
			
		end choose
		
//	case "cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()		
//		end if
//	case "des_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_pos_ricerca = this.gettext()
//			s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//			s_cs_xx.parametri.parametro_tipo_ricerca = 2
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()		
//		end if

	// Giulio: 03/11/2011 modifica ricerca prodotti
	case "cod_prodotto", "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_bol_ven_lista, "cod_prodotto")	
		end if
end choose


end event

event dw_det_bol_ven_lista::getfocus;call super::getfocus;dw_det_bol_ven_lista.setrowfocusindicator(p_1)
end event

event dw_det_bol_ven_lista::updatestart;call super::updatestart;long ll_i, ll_prog_riga_bol_ven, ll_anno_registrazione, ll_num_registrazione, ll_anno_reg_mov_mag,ll_num_reg_mov_mag
dec{4} ld_prezzo_vendita

for ll_i = 1 to this.deletedcount()
	
	ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = getitemnumber(ll_i, "num_registrazione", delete!, true)
	ll_prog_riga_bol_ven = getitemnumber(ll_i, "prog_riga_bol_ven", delete!, true)
	
	//se ho generato al bolla di vendita da un ordine di acquisto
	//è stata salvata la referenza in colonne della tabella det_ord_acq
	//		anno_reg_bol_ven
	//		num_reg_bol_ven
	//		prog_riga_bol_ven
	//Quindi per evitare errore di Fk mettere NULL in queste colonne

	update det_ord_acq
	set 	anno_reg_bol_ven = null,
			num_reg_bol_ven = null,
			prog_riga_bol_ven = null
	where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_reg_bol_ven = :ll_anno_registrazione
		and    num_reg_bol_ven = :ll_num_registrazione
		and    prog_riga_bol_ven =: ll_prog_riga_bol_ven;

	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante eliminazione referenza con ordine di acquisto: "+sqlca.sqlerrtext)
		return 1
	end if
		
next

for ll_i = 1 to rowcount()
	
	if getitemstatus(ll_i, "prezzo_vendita", primary!) = DataModified!	then
		ll_anno_reg_mov_mag = getitemnumber(ll_i, "anno_registrazione_mov_mag")
		ll_num_reg_mov_mag = getitemnumber(ll_i, "num_registrazione_mov_mag")
		if not isnull(ll_anno_reg_mov_mag) and ll_anno_reg_mov_mag > 0 then
		
			ld_prezzo_vendita =  getitemnumber(ll_i, "prezzo_vendita")
			
			update mov_magazzino
			set val_movimento = :ld_prezzo_vendita
			where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_reg_mov_mag and
						num_registrazione =:ll_num_reg_mov_mag;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in fase di aggiornamento prezzo del movimento di magazzino.~r~n" + sqlca.sqlerrtext)
			end if
		end if
	end if
next
end event

type dw_documenti from w_det_bol_ven`dw_documenti within w_det_bol_ven_ptenda
end type

type dw_det_bol_ven_det_1 from w_det_bol_ven`dw_det_bol_ven_det_1 within w_det_bol_ven_ptenda
integer height = 1256
end type

event dw_det_bol_ven_det_1::rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_bol_ven_lista.setrow(getrow())
end if
end event

event dw_det_bol_ven_det_1::ue_key;//
//		ATTENZIONE in questo eventio tenere disabilitata la proprità EXTEND
//
string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long   ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "prezzo_vendita"
		choose case key

			case keyF1!, keyF2!, keyF3!, keyF4!, keyF5!
				if keyflags = 1 then
					setpointer(hourglass!)
					ldt_data_registrazione = dw_det_bol_ven_lista.i_parentdw.getitemdatetime(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
					ls_cod_cliente = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
					ls_cod_valuta = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
					ls_cod_agente_1 = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
					ls_cod_agente_2 = dw_det_bol_ven_lista.i_parentdw.getitemstring(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
					ld_cambio_ven = dw_det_bol_ven_lista.i_parentdw.getitemnumber(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
					ls_cod_tipo_det_ven = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_tipo_det_ven")
					ls_cod_prodotto = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_prodotto")
					ld_quantita = dw_det_bol_ven_lista.getitemnumber(dw_det_bol_ven_lista.getrow(), "quan_fatturata")
					choose case key
						case keyF1!
							ls_listino = 'LI1'
						case keyF2!
							ls_listino = 'LI2'
						case keyF3!
							ls_listino = 'LI3'
						case keyF4!
							ls_listino = 'LI4'
						case keyF5!
							ls_listino = 'LI5'
						case else
							return
					end choose
					if ls_listino <> "LI5" then
						select stringa
						into  :ls_stringa
						from  parametri_azienda
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_parametro = :ls_listino ;
						if sqlca.sqlcode = 0 and mid(f_flag_controllo(),1,1) = "S" then
							iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
							iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
							iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
							iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
							iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
							iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
							iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
							iuo_condizioni_cliente.str_parametri.dim_1 = 0
							iuo_condizioni_cliente.str_parametri.dim_2 = 0
							iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
							iuo_condizioni_cliente.str_parametri.valore = 0
							iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
							iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
							iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_consegnata"
							iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
							iuo_condizioni_cliente.wf_condizioni_cliente()
						end if			
						setpointer(arrow!)
					else
						select prezzo_acquisto
						into   :ld_prezzo_acquisto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto;
						dw_det_bol_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
					end if			
					
				end if
			
			case keyF11! 
				// EnMe 04/07/2012 per caricamento prezzo da ultima fattura di acquisto in caso di bolle di trasferimento interno
				string ls_sql_prezzo,ls_rag_soc_for
				long ll_ret,ll_anno_reg_mov_mag,ll_num_reg_mov_mag,ll_anno_reg_tes_fat,ll_num_reg_tes_fat
				dec{4} ld_prezzo_trasferimento
				datetime ldt_data_protocollo
				datastore lds_fatture
		
				ls_cod_prodotto = dw_det_bol_ven_lista.getitemstring(dw_det_bol_ven_lista.getrow(), "cod_prodotto")
				ldt_data_registrazione = dw_det_bol_ven_lista.i_parentdw.getitemdatetime(dw_det_bol_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")

				ls_sql_prezzo = 	"select data_protocollo, mov_magazzino.val_movimento from tes_fat_acq " + &
										" join det_fat_acq on tes_fat_acq.anno_registrazione =  det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione =  det_fat_acq.num_registrazione " + &
										" join mov_magazzino on  det_fat_acq.anno_registrazione_mov_mag = mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione " + &
										" where data_protocollo <= '"+string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and flag_agg_mov='S' and det_fat_acq.cod_prodotto = '" + ls_cod_prodotto + "' " + &
										" order by  tes_fat_acq.data_protocollo DESC , tes_fat_acq.num_registrazione DESC , det_fat_acq.prog_riga_fat_acq DESC "
							
				
				ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo)
				
				if ll_ret < 0 then
					destroy lds_fatture
					g_mb.error("Si è verificato un errore cercando il prezzo del prodotto " + ls_cod_prodotto + " dalle fatture di acquisto ")
					return
				end if
				
				if ll_ret > 0 then
					
					ldt_data_protocollo = lds_fatture.getitemdatetime(ll_ret, 1)
					ld_prezzo_trasferimento = lds_fatture.getitemnumber (ll_ret, 2)
					ll_anno_reg_mov_mag = lds_fatture.getitemnumber (1, 3)
					ll_num_reg_mov_mag = lds_fatture.getitemnumber (1, 4)
					ll_anno_reg_tes_fat = lds_fatture.getitemnumber (1, 5)
					ll_num_reg_tes_fat = lds_fatture.getitemnumber (1, 6)
					ls_rag_soc_for = lds_fatture.getitemstring (1, 7)
					
					setitem(getrow(),"prezzo_vendita", ld_prezzo_trasferimento)
					w_cs_xx_mdi.setmicrohelp("Applicato Prezzo di acquisto "+string(ld_prezzo_trasferimento,"###,##0.00")+" riferito alla data " + string(ldt_data_protocollo,"dd/mm/yyyy") + " (Mov Mag:" + string(ll_anno_reg_mov_mag) + "-" + string(ll_num_reg_mov_mag) + "  Nr.Int.FT:" + string(ll_anno_reg_tes_fat) + "-" + string(ll_num_reg_tes_fat) + " Fornitore: " + ls_rag_soc_for + ")" )
				else
					w_cs_xx_mdi.setmicrohelp("Nessun prezzo di trasferimento reperibile dalle fatture di acquisto.")
				end if
				
				destroy lds_fatture
				// fine modifica 04/07/2012
				
				setpointer(arrow!)
		end choose
		
	//Giulio: 03/11/2011 modificata la ricerca dei prodotti
	case "cod_prodotto", "des_prodotto", "nota_dettaglio"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_bol_ven_det_1,"cod_prodotto")	
		end if
end choose


end event

type dw_vis_comp_det_ord_ven from datawindow within w_det_bol_ven_ptenda
integer x = 41
integer y = 1800
integer width = 3474
integer height = 660
integer taborder = 71
boolean bringtotop = true
string dataobject = "w_vis_comp_det_ord_ven"
boolean border = false
boolean livescroll = true
end type

type p_1 from picture within w_det_bol_ven_ptenda
integer x = 46
integer y = 240
integer width = 73
integer height = 60
string picturename = "H:\cs_70\framework\RISORSE\PREC.BMP"
boolean focusrectangle = false
end type

