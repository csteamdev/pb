﻿$PBExportHeader$w_lista_bol_ven.srw
$PBExportComments$Finestra modifica importi addebiti clienti fatture vendita
forward
global type w_lista_bol_ven from w_cs_xx_risposta
end type
type cb_2 from commandbutton within w_lista_bol_ven
end type
type cb_1 from commandbutton within w_lista_bol_ven
end type
type dw_lista from datawindow within w_lista_bol_ven
end type
type st_2 from statictext within w_lista_bol_ven
end type
type cbx_1 from checkbox within w_lista_bol_ven
end type
type st_1 from statictext within w_lista_bol_ven
end type
type cb_ok from commandbutton within w_lista_bol_ven
end type
end forward

global type w_lista_bol_ven from w_cs_xx_risposta
integer width = 2706
integer height = 964
string title = "Lista Bolle Numerazione Interrotta"
boolean controlmenu = false
boolean resizable = false
cb_2 cb_2
cb_1 cb_1
dw_lista dw_lista
st_2 st_2
cbx_1 cbx_1
st_1 st_1
cb_ok cb_ok
end type
global w_lista_bol_ven w_lista_bol_ven

type variables
integer il_anno_registrazione, il_num_registrazione

string  is_cod_documento, is_numeratore_documento

long    il_anno_documento
end variables

forward prototypes
public function integer wf_lista_buchi_numerazione (ref string fs_messaggio)
end prototypes

public function integer wf_lista_buchi_numerazione (ref string fs_messaggio);string   ls_flag_segnalata
long     ll_numero_max, ll_i, ll_anno_appo, ll_num_appo, ll_riga, ll_count, ll_num, ll_appo
boolean  lb_doppia
datetime ldt_confronto, ldt_data_registrazione, ldt_null, ldt_data_bolla

select max(num_documento)
into   :ll_numero_max  
from   tes_bol_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_documento = :is_cod_documento and
		 numeratore_documento = :is_numeratore_documento and
		 anno_documento = :il_anno_documento;	

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore durante la lettura della tabella numeratori: " + sqlca.sqlerrtext
	return -1
elseif isnull(ll_numero_max) then
	ll_numero_max = 1
	st_2.text = "Ultimo numero usato: " + string(0)
elseif not isnull(ll_numero_max) then
	st_2.text = "Ultimo numero usato: " + string(ll_numero_max)
end if

// *** torno indietro al massimo di 30 giorni

ldt_confronto = datetime( today(), 00:00:00)
ldt_confronto = datetime( relativedate(date(today()), -30), 00:00:00)

//select min(num_documento)
//into   :ll_num
//from   tes_bol_ven
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_documento = :is_cod_documento and
//		 numeratore_documento = :is_numeratore_documento and
//		 anno_documento = :il_anno_documento and
//		 data_registrazione = :ldt_confronto;	
//		 
//if isnull(ll_num) then ll_num = 1		 

ll_appo = 0
for ll_i = ll_numero_max to 1 step -1
	
//	ll_appo = ll_appo + 1
//	if ll_appo > ll_num then exit
//	st_1.text = string(ll_appo) + "/" + string(ll_num)
	
	// *** conto se ho una bolla doppia: vuol dire che ho accantonato una bolla ed ho già riassegnato
	//     il suo numero, quindi potrebbe darmi errore
	
	lb_doppia = false
	
	setnull(ll_anno_appo)
	setnull(ll_num_appo)
	

	select count(*)
	into   :ll_count
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_documento = :is_cod_documento and
			 numeratore_documento = :is_numeratore_documento and
			 anno_documento = :il_anno_documento and
			 num_documento = :ll_i;
			 
	if not isnull(ll_count) and ll_count > 1 then   

		lb_doppia = true
		
		// *** il numero è già stato riassegnato quindi continuo
		continue		
				 
	else
			 
		select anno_registrazione,
				 num_registrazione,
				 flag_segnalata,
				 data_registrazione,
				 data_bolla
		into   :ll_anno_appo,
				 :ll_num_appo,
				 :ls_flag_segnalata,
				 :ldt_data_registrazione,
				 :ldt_data_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :is_cod_documento and
				 numeratore_documento = :is_numeratore_documento and
				 anno_documento = :il_anno_documento and
				 num_documento = :ll_i;
	end if
			 
	if sqlca.sqlcode < 0 then             // errore
		fs_messaggio = "Errore durante la ricerca della bolla: " + sqlca.sqlerrtext
		return -1	
	elseif sqlca.sqlcode = 0 then         // *** esiste la bolla, controllo che non sia accantonata
		
		if not isnull(ls_flag_segnalata) and ls_flag_segnalata = "S" then         // *** la bolla è accantonata			
			ll_riga = dw_lista.insertrow(0)
			dw_lista.setitem( ll_riga, "cod_documento", is_cod_documento)
			dw_lista.setitem( ll_riga, "numeratore_documento", is_numeratore_documento)
			dw_lista.setitem( ll_riga, "anno_documento", il_anno_documento)
			dw_lista.setitem( ll_riga, "num_documento", ll_i)
			dw_lista.setitem( ll_riga, "anno_registrazione", ll_anno_appo)
			dw_lista.setitem( ll_riga, "num_registrazione", ll_num_appo)
			dw_lista.setitem( ll_riga, "flag_cancellata", "N")
			dw_lista.setitem( ll_riga, "flag_accantonata", "S")
			dw_lista.setitem( ll_riga, "data_bolla", ldt_data_bolla)
		else  
			if not isnull(ldt_data_registrazione) and ldt_data_registrazione < ldt_confronto then exit
			continue
		end if
	
	elseif sqlca.sqlcode = 100 then		  // *** non esiste la bolla, quindi è stata cancellata
		
		ll_riga = dw_lista.insertrow(0)
		dw_lista.setitem( ll_riga, "cod_documento", is_cod_documento)
		dw_lista.setitem( ll_riga, "numeratore_documento", is_numeratore_documento)
		dw_lista.setitem( ll_riga, "anno_documento", il_anno_documento)
		dw_lista.setitem( ll_riga, "num_documento", ll_i)
		dw_lista.setitem( ll_riga, "anno_registrazione", ll_anno_appo)
		dw_lista.setitem( ll_riga, "num_registrazione", ll_num_appo)
		dw_lista.setitem( ll_riga, "flag_cancellata", "S")
		dw_lista.setitem( ll_riga, "flag_accantonata", "N")
		
		// *** mi trovo la data più vicina
		
		select MIN(data_bolla)
		into   :ldt_data_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_documento = :is_cod_documento and
				 numeratore_documento = :is_numeratore_documento and
				 anno_documento = :il_anno_documento and
				 num_documento >= :ll_i;
				 
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore durante la ricerca data bolla: " + sqlca.sqlerrtext
			return -1				
		end if		
		dw_lista.setitem( ll_riga, "data_bolla", ldt_data_bolla)
		
	end if
	
next

if dw_lista.rowcount() > 0 then
	
	dw_lista.SetRow( 1)
	dw_lista.SelectRow( 1, TRUE)	
	
end if
return 0
end function

on w_lista_bol_ven.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_lista=create dw_lista
this.st_2=create st_2
this.cbx_1=create cbx_1
this.st_1=create st_1
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_lista
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.cbx_1
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.cb_ok
end on

on w_lista_bol_ven.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_lista)
destroy(this.st_2)
destroy(this.cbx_1)
destroy(this.st_1)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;long   ll_ret
string ls_messaggio

//set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_lista.settransobject( sqlca)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2


is_cod_documento = s_cs_xx.parametri.parametro_s_10
is_numeratore_documento = s_cs_xx.parametri.parametro_s_11
il_anno_documento = s_cs_xx.parametri.parametro_ul_1


setnull(s_cs_xx.parametri.parametro_s_10)
setnull(s_cs_xx.parametri.parametro_s_11)
setnull(s_cs_xx.parametri.parametro_ul_1)
end event

event open;call super::open;dw_lista.postevent("pcd_retrieve")
end event

type cb_2 from commandbutton within w_lista_bol_ven
integer x = 731
integer y = 780
integer width = 699
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa &Lista"
end type

event clicked;dw_lista.print()
end event

type cb_1 from commandbutton within w_lista_bol_ven
integer x = 23
integer y = 780
integer width = 699
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Stampa Bolla"
end type

event clicked;s_cs_xx.parametri.parametro_ul_1 = -100

close(parent)
end event

type dw_lista from datawindow within w_lista_bol_ven
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 20
integer width = 2651
integer height = 580
integer taborder = 30
string title = "none"
string dataobject = "d_lista_bol_ven_ext"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;string ls_messaggio
long   ll_ret

ll_ret = wf_lista_buchi_numerazione( ref ls_messaggio)
if ll_ret < 0 then
	g_mb.messagebox( "Apice", ls_messaggio)
end if
//ll_ret = dw_lista.insertrow(0)
//ll_ret = dw_lista.insertrow(0)
//ll_ret = dw_lista.insertrow(0)
//ll_ret = dw_lista.insertrow(0)
//ll_ret = dw_lista.insertrow(0)
//ll_ret = dw_lista.insertrow(0)
//
//dw_lista.SetRow( 1)
//dw_lista.SelectRow( 1, TRUE)	
end event

event clicked;long ll_ret

for ll_ret = 1 to dw_lista.rowcount()
	dw_lista.selectrow( ll_ret, false)
next
if row > 0 then
	dw_lista.SetRow( row)
	dw_lista.SelectRow( row, TRUE)	
end if
end event

type st_2 from statictext within w_lista_bol_ven
integer x = 23
integer y = 620
integer width = 937
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
boolean focusrectangle = false
end type

type cbx_1 from checkbox within w_lista_bol_ven
integer x = 983
integer y = 620
integer width = 1211
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Procedi con la num. progressiva successiva."
end type

type st_1 from statictext within w_lista_bol_ven
integer x = 23
integer y = 700
integer width = 1371
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_lista_bol_ven
integer x = 2309
integer y = 780
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string ls_errore
long   ll_ret

if cbx_1.checked then
	ll_ret = g_mb.messagebox( "APICE", "Sei sicuro di voler procedere con la numerazione successiva anche in presenza di buchi?", Exclamation!, OKCancel!, 2)
	IF ll_ret = 1 THEN
		
		s_cs_xx.parametri.parametro_ul_1 = 0
		
	ELSE
		return
	END IF	
else
	if dw_lista.rowcount() < 1 then
		g_mb.messagebox( "APICE", "Attenzione: è impossibile selezionare bolle!", stopsign!)
		return
	end if
	if dw_lista.getrow() < 1 then
		g_mb.messagebox( "APICE", "Attenzione: è impossibile selezionare bolle!", stopsign!)
		return		
	end if
	
	g_mb.messagebox( "APICE", "Attenzione: controllare che data e ora partenza sia congrua!")
	
	s_cs_xx.parametri.parametro_ul_1 = dw_lista.getitemnumber( dw_lista.getrow(), "num_documento")	
	s_cs_xx.parametri.parametro_data_1 = dw_lista.getitemdatetime( dw_lista.getrow(), "data_bolla")
end if

close(parent)
end event

