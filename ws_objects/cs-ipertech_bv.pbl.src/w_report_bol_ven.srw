﻿$PBExportHeader$w_report_bol_ven.srw
$PBExportComments$Finestra Report Bolle Vendita
forward
global type w_report_bol_ven from w_cs_xx_principale
end type
type cb_3 from commandbutton within w_report_bol_ven
end type
type cb_2 from commandbutton within w_report_bol_ven
end type
type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
end type
type cb_1 from commandbutton within w_report_bol_ven
end type
end forward

global type w_report_bol_ven from w_cs_xx_principale
integer width = 4009
integer height = 4980
string title = "Stampa Bolle Uscita"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
event ue_avviso_spedizione ( )
cb_3 cb_3
cb_2 cb_2
dw_report_bol_ven dw_report_bol_ven
cb_1 cb_1
end type
global w_report_bol_ven w_report_bol_ven

type variables
boolean ib_modifica=false, ib_nuovo=false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione
long il_num_copie

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = false
long    il_totale_pagine, il_pagina_corrente

boolean ib_pdf = false
end variables

forward prototypes
public subroutine wf_report ()
public function integer wf_imposta_barcode ()
public subroutine wf_impostazioni ()
end prototypes

event ue_avviso_spedizione();uo_avviso_spedizioni luo_avviso_sped

string ls_tipo_gestione, ls_messaggio, ls_path_logo_intestazione
long ll_ret


if il_anno_registrazione>0 and il_num_registrazione>0 then
	
	luo_avviso_sped = create uo_avviso_spedizioni

	ls_tipo_gestione = "bol_ven"
	
	//passaggio path file dell'intestazione report ---------------------------------------
	try
		ls_path_logo_intestazione = dw_report_bol_ven.Describe ("intestazione.filename") 
	catch (throwable err)
		ls_path_logo_intestazione = ""
	end try
	
	luo_avviso_sped.is_path_logo_intestazione = ls_path_logo_intestazione
	//-----------------------------------------------------------------------------------------
	
	//passa l'oggetto dw del report
	luo_avviso_sped.idw_report = dw_report_bol_ven
	
	ll_ret = luo_avviso_sped.uof_avviso_spedizione(il_anno_registrazione,il_num_registrazione, ls_tipo_gestione, ls_messaggio)

	if ll_ret<0 then
		g_mb.error(ls_messaggio)
		rollback;
		return
	end if

	commit;
	
	destroy luo_avviso_sped
	
else
	g_mb.error("Nessun documento presente (>0)!")
	rollback;
	return
end if
end event

public subroutine wf_report ();boolean lb_prima_riga=false,lb_flag_prodotto_cliente=false, lb_flag_nota_dettaglio=false, lb_flag_nota_piede=false

string ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_porto, ls_cod_resa, ls_cod_non_a_magazzino, &
		 ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_cod_verniciatura, &
		 ls_des_tipo_bol_ven, ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_dimensioni, &
		 ls_des_resa_lingua, ls_des_prodotto_anag, ls_flag_stampa_bolla, ls_cod_tipo_det_ven, &
		 ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_documento, ls_telefono, ls_fax, &
		 ls_numeratore_documento, ls_cod_vettore, ls_cod_inoltro, ls_cod_causale, ls_des_verniciatura, &
		 ls_aspetto_beni, ls_rag_soc_1_vet, ls_rag_soc_2_vet, ls_indirizzo_vet, ls_cod_prodotto_finito, &
		 ls_localita_vet, ls_frazione_vet, ls_cap_vet, ls_provincia_vet, ls_des_causale_lingua, &
		 ls_rag_soc_1_ino, ls_rag_soc_2_ino, ls_indirizzo_ino, ls_des_causale, ls_des_colore_tessuto, &
		 ls_localita_ino, ls_frazione_ino, ls_cap_ino, ls_provincia_ino, ls_cod_fornitore, ls_des_tipo_det_ven, &
		 ls_cod_deposito_tras, ls_cod_fil_fornitore, ls_flag_tipo_bol_ven, ls_des_deposito, ls_descrizione_riga, &
		 ls_cod_tessuto, ls_des_gruppo_tessuto, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua,  ls_flag_st_note_tes, &
		 ls_flag_st_note_pie, ls_flag_st_note_det, ls_cod_misura_mag, ls_null, ls_stampa_nota_testata, ls_stampa_nota_piede, ls_stampa_nota_video, ls_flag, &
		 ls_tipo_bol_ven, ls_parametro_colli,  ls_des_deposito_dep, ls_cod_deposito, &
		 ls_provincia_dep, ls_cap_dep, ls_indirizzo_dep, ls_telefono_dep, ls_fax_dep, &
		 ls_provincia_dep_arrivo, ls_cap_dep_arrivo, ls_indirizzo_dep_arrivo, ls_telefono_dep_arrivo, ls_fax_dep_arrivo, &
		 ls_cod_cliente_ordine, ls_rag_soc_cliente, ls_cod_contatto, ls_frazione_dep_arrivo,ls_rag_soc_1_dep_arrivo, ls_localita_dep_arrivo, &
		 ls_cod_nazione, ls_tab_nazioni_des_nazione, ls_stato_cli, ls_des_breve_tessuto, ls_flag_tipo_lista_bolle, ls_sql, ls_variabili, ls_cod_pagamento, &
		 ls_des_pagamento, ls_des_pagamento_lingua, ls_stampa_nota_testata_prod, ls_stampa_nota_piede_prod, ls_stampa_nota_video_prod
		 
long 	ll_errore, ll_anno_documento, ll_num_documento, ll_num_colli, ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, &
	  	ll_prog_riga_ord_ven, li_risp, ll_righe, ll_row
		  
dec{4} ld_quan_consegnata, ld_peso_lordo, ld_peso_netto, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_quantita_um, ld_prezzo_um,ld_dim_x_riga, ld_dim_y_riga, ld_val_gtot, ld_dim_x_riga_ddt, &
		ld_dim_y_riga_ddt, ld_volume, ld_imponibile_iva_bolla, ld_importo_iva_bolla, ld_tot_val_bolla, ld_imponibile_riga, ld_sconto_1, ld_sconto_2, ld_prezzo_vendita, ld_det_bol_ven_num_colli, &
		ld_det_bol_ven_peso_lordo

dec{5} ld_fat_conversione_ven

datetime ldt_data_bolla, ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto, ldt_data_registrazione

datastore lds_righe
uo_stampa_dati_prodotto luo_stampa_dati_prodotto

dw_report_bol_ven.reset()
setnull(ls_null)

wf_imposta_barcode()

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

// stefanop 01/01/2010; stabilisce se stampare oppure no il numero colli nei doc fiscali (Flag SCL)
// Ticket 2010/55
select flag  
into  :ls_parametro_colli
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'F' and  
      parametri_azienda.cod_parametro = 'SCL';

if sqlca.sqlcode <> 0 then
	ls_parametro_colli = "N"
end if
// ----

ls_flag_tipo_lista_bolle = "N"

Select flag_tipo_lista_bolle
into 	:ls_flag_tipo_lista_bolle
from 	con_vendite
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	ls_flag_tipo_lista_bolle = "N"
end if

select cod_tipo_bol_ven,
		 data_registrazione,
		 cod_cliente,   
		 cod_porto,   
		 cod_resa,   
		 cod_mezzo,   
		 nota_testata,   
		 nota_piede,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia, 
		 cod_documento,
		 numeratore_documento,
		 anno_documento,
		 num_documento,
		 data_bolla,
		 cod_vettore,
		 cod_inoltro,
		 cod_causale,
		 aspetto_beni,
		 num_colli,
		 cod_fornitore,
		 cod_deposito_tras,
		 cod_fil_fornitore,
		 data_inizio_trasporto,
		 ora_inizio_trasporto,
		 peso_lordo,
		 peso_netto,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 cod_deposito,
		 cod_contatto,
		 volume,
		 cod_pagamento,
		 imponibile_iva,
		 importo_iva,
		 tot_val_bolla
into   :ls_cod_tipo_bol_ven,
		 :ldt_data_registrazione,
	 	 :ls_cod_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_cod_mezzo,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_documento,
		 :ls_numeratore_documento,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_bolla,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_cod_causale,
		 :ls_aspetto_beni,
		 :ll_num_colli,
		 :ls_cod_fornitore,
		 :ls_cod_deposito_tras,
		 :ls_cod_fil_fornitore,
		 :ldt_data_inizio_trasporto,
		 :ldt_ora_inizio_trasporto,
		 :ld_peso_lordo,
		 :ld_peso_netto,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_cod_deposito,
		 :ls_cod_contatto,
		 :ld_volume,
		 :ls_cod_pagamento,
		 :ld_imponibile_iva_bolla,
		 :ld_importo_iva_bolla,
		 :ld_tot_val_bolla
from   tes_bol_ven  
where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_bol_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_bol_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_bol_ven)
	setnull(ls_cod_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_cod_documento)
	setnull(ls_numeratore_documento)
	setnull(ll_anno_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_bolla)
	setnull(ls_cod_vettore)
	setnull(ls_cod_inoltro)
	setnull(ls_cod_causale)
	setnull(ls_aspetto_beni)
	setnull(ll_num_colli)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_deposito_tras)
	setnull(ls_cod_pagamento)
	setnull(ld_imponibile_iva_bolla)
	setnull(ld_importo_iva_bolla)
	setnull(ld_tot_val_bolla)
end if

// stefanop: 19/12/2011: aggiunto deposito
select 
	localita,
	provincia,
	cap,
	indirizzo,
	telefono,
	fax
into 
	:ls_des_deposito_dep,
	:ls_provincia_dep,
	:ls_cap_dep,
	:ls_indirizzo_dep,
	:ls_telefono_dep,
	:ls_fax_dep
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito_dep) then ls_des_deposito_dep = ""
if isnull(ls_provincia_dep) then ls_provincia_dep = ""
if isnull(ls_cap_dep) then ls_cap_dep = ""
if isnull(ls_indirizzo_dep) then ls_indirizzo_dep = ""
if isnull(ls_telefono_dep) then ls_telefono_dep = ""
if isnull(ls_fax_dep) then ls_fax_dep = ""
// ----

// pulisco campi
setnull(ls_rag_soc_1_cli)
setnull(ls_rag_soc_2_cli)
setnull(ls_indirizzo_cli)
setnull(ls_localita_cli)
setnull(ls_frazione_cli)
setnull(ls_cap_cli)
setnull(ls_provincia_cli)
setnull(ls_partita_iva)
setnull(ls_cod_fiscale)
setnull(ls_cod_lingua)
setnull(ls_flag_tipo_cliente)
setnull(ls_telefono)
setnull(ls_fax)
	
if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	select rag_soc_1,   
		 	rag_soc_2,   
			indirizzo,   
		 	localita,   
		 	frazione,   
		 	cap,   
		 	provincia,   
		 	partita_iva,   
		 	cod_fiscale,   
		 	cod_lingua,   
		 	flag_tipo_cliente,
		 	telefono,
		 	fax,
			cod_nazione,
			stato
    into 	:ls_rag_soc_1_cli,   
		 	:ls_rag_soc_2_cli,   
		 	:ls_indirizzo_cli,   
		 	:ls_localita_cli,   
		 	:ls_frazione_cli,   
		 	:ls_cap_cli,   
		 	:ls_provincia_cli,   
		 	:ls_partita_iva,   
		 	:ls_cod_fiscale,   
		 	:ls_cod_lingua,   
		 	:ls_flag_tipo_cliente,
		 	:ls_telefono,
		 	:ls_fax,
			:ls_cod_nazione,
			:ls_stato_cli
	from   anag_clienti  
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;

	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
			ls_partita_iva = ls_cod_fiscale
			ls_provincia_cli = ls_cod_nazione
		end if
	end if

elseif not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 flag_tipo_fornitore,
			 cod_nazione,
			 stato
	into   	 :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,   
			 :ls_flag_tipo_cliente,
			 :ls_cod_nazione,
			 :ls_stato_cli
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C'  then
			ls_partita_iva = ls_cod_fiscale
			ls_provincia_cli = ls_cod_nazione
		end if
	end if
	
	if not isnull(ls_cod_fil_fornitore) then
		setnull(ls_stato_cli)
		
		select rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli
		from   anag_fil_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore and
				 cod_fil_fornitore = :ls_cod_fil_fornitore;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_rag_soc_1_cli)
			setnull(ls_rag_soc_2_cli)
			setnull(ls_indirizzo_cli)
			setnull(ls_localita_cli)
			setnull(ls_frazione_cli)
			setnull(ls_cap_cli)
			setnull(ls_provincia_cli)
		end if
	end if
elseif not isnull(ls_cod_contatto) and ls_cod_contatto <> "" then
	
	setnull(ls_stato_cli)
	
	select anag_contatti.rag_soc_1,   
		 anag_contatti.rag_soc_2,   
		 anag_contatti.indirizzo,   
		 anag_contatti.localita,   
		 anag_contatti.frazione,   
		 anag_contatti.cap,   
		 anag_contatti.provincia,   
		 anag_contatti.partita_iva,   
		 anag_contatti.cod_fiscale,   
		 anag_contatti.cod_lingua,   
		 anag_contatti.flag_tipo_cliente,
		 anag_contatti.telefono,
		 anag_contatti.fax
	into   :ls_rag_soc_1_cli,   
		 :ls_rag_soc_2_cli,   
		 :ls_indirizzo_cli,   
		 :ls_localita_cli,   
		 :ls_frazione_cli,   
		 :ls_cap_cli,   
		 :ls_provincia_cli,   
		 :ls_partita_iva,   
		 :ls_cod_fiscale,   
		 :ls_cod_lingua,   
		 :ls_flag_tipo_cliente,
		 :ls_telefono,
		 :ls_fax
	from   anag_contatti  
	where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_contatti.cod_contatto = :ls_cod_contatto;

	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' then
			ls_partita_iva = ls_cod_fiscale
		end if
	end if
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

// --- pagamento
select des_pagamento
into   :ls_des_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if


select tab_tipi_bol_ven.des_tipo_bol_ven,
		 tab_tipi_bol_ven.flag_tipo_bol_ven
into   :ls_des_tipo_bol_ven, 
		 :ls_flag_tipo_bol_ven
from   tab_tipi_bol_ven  
where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_bol_ven)
	setnull(ls_flag_tipo_bol_ven)
end if

if g_str.isnotempty(ls_cod_nazione) then
	
	select des_nazione
	into :ls_tab_nazioni_des_nazione
	from tab_nazioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_nazione = :ls_cod_nazione;
	
end if

if ls_flag_tipo_bol_ven = "T" then
     dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Deposito:'")

	// Stefanop: 17/05/2012: dopo consultazione con Beatrice si è deciso che la ragione sociale è quella del cliente
	// o fornitore e NON quella del deposito
	// ---------------------------------------
	//	ls_rag_soc_1_cli = ls_rag_soc_1
	//	ls_rag_soc_2_cli = ls_rag_soc_2   
	//	ls_indirizzo_cli = ls_indirizzo
	//	ls_localita_cli = ls_localita
	//	ls_frazione_cli = ls_frazione
	//	ls_cap_cli = ls_cap
	//	ls_provincia_cli = ls_provincia
	// ---------------------------------------
	
	select 
		localita,
		provincia,
		cap,
		indirizzo,
		telefono,
		fax,
		frazione,
		des_deposito
	into 
		:ls_localita_dep_arrivo,
		:ls_provincia_dep_arrivo,
		:ls_cap_dep_arrivo,
		:ls_indirizzo_dep_arrivo,
		:ls_telefono_dep_arrivo,
		:ls_fax_dep_arrivo,
		:ls_frazione_dep_arrivo,
		:ls_rag_soc_1_dep_arrivo
	from anag_depositi
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_deposito = :ls_cod_deposito_tras;
		
	if sqlca.sqlcode <> 0 then	
		setnull(ls_localita_dep_arrivo)
		setnull(ls_provincia_dep_arrivo)
		setnull(ls_cap_dep_arrivo)
		setnull(ls_indirizzo_dep_arrivo)
		setnull(ls_telefono_dep_arrivo)
		setnull(ls_fax_dep_arrivo)
		setnull(ls_frazione_dep_arrivo)
		setnull(ls_rag_soc_1_dep_arrivo)
	end if
	
	if isnull(ls_localita_dep_arrivo) then ls_localita_dep_arrivo = ""
	if isnull(ls_provincia_dep_arrivo) then ls_provincia_dep_arrivo = ""
	if isnull(ls_cap_dep_arrivo) then ls_cap_dep_arrivo = ""
	if isnull(ls_indirizzo_dep_arrivo) then ls_indirizzo_dep_arrivo = ""
	if isnull(ls_telefono_dep_arrivo) then ls_telefono_dep_arrivo = ""
	if isnull(ls_fax_dep_arrivo) then ls_fax_dep_arrivo = ""
	if isnull(ls_frazione_dep_arrivo) then ls_frazione_dep_arrivo = ""
	if isnull(ls_rag_soc_1_dep_arrivo) then ls_rag_soc_1_dep_arrivo = ""
	
elseif ls_flag_tipo_bol_ven = "V" then
	// aggiunto contatto
	if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
		dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Cliente:'")
	else
		dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Contatto:'")
	end if
elseif ls_flag_tipo_bol_ven = "R" or &
		 ls_flag_tipo_bol_ven = "C" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Fornitore:'")
end if

select tab_porti.des_porto  
into :ls_des_porto  
from tab_porti  
where tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
		tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_mezzo = :ls_cod_mezzo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if

select des_causale
into   :ls_des_causale  
from   tab_causali_trasp  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_causale = :ls_cod_causale;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_causale)
end if

select des_causale
into   :ls_des_causale_lingua  
from   tab_causali_trasp_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_causale = :ls_cod_causale and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_causale_lingua)
end if


select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_vet,   
		 :ls_rag_soc_2_vet,   
		 :ls_indirizzo_vet,   
		 :ls_localita_vet,   
		 :ls_frazione_vet,   
		 :ls_cap_vet,   
		 :ls_provincia_vet  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_vet)
	setnull(ls_rag_soc_2_vet)
	setnull(ls_indirizzo_vet)
	setnull(ls_localita_vet)
	setnull(ls_frazione_vet)
	setnull(ls_cap_vet)
	setnull(ls_provincia_vet)
end if

select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_ino,   
		 :ls_rag_soc_2_ino,   
		 :ls_indirizzo_ino,   
		 :ls_localita_ino,   
		 :ls_frazione_ino,   
		 :ls_cap_ino,   
		 :ls_provincia_ino  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_ino)
	setnull(ls_rag_soc_2_ino)
	setnull(ls_indirizzo_ino)
	setnull(ls_localita_ino)
	setnull(ls_frazione_ino)
	setnull(ls_cap_ino)
	setnull(ls_provincia_ino)
end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
if ls_flag_st_note_tes = 'I' then   //nota di testata
	select flag_st_note
	  into :ls_flag_st_note_tes
	  from tab_tipi_bol_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
end if		

if ls_flag_st_note_tes = 'N' then
	ls_nota_testata = ""
end if			


if ls_flag_st_note_pie = 'I' then //nota di piede
	select flag_st_note
	  into :ls_flag_st_note_pie
	  from tab_tipi_bol_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
end if			

if ls_flag_st_note_pie = 'N' then
	ls_nota_piede = ""
end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------	

// -----------------------------  modifica Enrico 21/01/2003 per stampa note fisse solo nei documenti -----------------
// -- stefanop 26/02/2010: Controllo se devo passare un fornitore o un cliente
// Ticket 2010/55
select flag_tipo_bol_ven
into :ls_tipo_bol_ven
from tab_tipi_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
if ls_tipo_bol_ven = "R" or ls_tipo_bol_ven = "C" then // Fornitore
	if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "STAMPA_BOL_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
		g_mb.error(ls_stampa_nota_testata)
	else
		if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
	end if
else // Cliente
	if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_BOL_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
		g_mb.error(ls_stampa_nota_testata)
	else
		if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
	end if
end if
// ----
		
if not isnull(ls_stampa_nota_testata) and len(ls_stampa_nota_testata) > 0 then
	if isnull(ls_nota_testata) or len(ls_nota_testata) = 0 then
		ls_nota_testata = ls_stampa_nota_testata
	else
		ls_nota_testata = ls_stampa_nota_testata + "~r~n" + ls_nota_testata
	end if
end if
if not isnull(ls_stampa_nota_piede) and len(ls_stampa_nota_piede) > 0 then
	if isnull(ls_nota_piede) or len(ls_nota_piede) = 0 then
		ls_nota_piede = ls_stampa_nota_piede
	else
		ls_nota_piede =  ls_nota_piede + "~r~n" + ls_stampa_nota_piede
	end if
end if
if not isnull(ls_stampa_nota_video) and len(ls_stampa_nota_video) > 0 then
	g_mb.messagebox("NOTA PER L'OPERATORE",ls_stampa_nota_video + "~r~nPREMERE INVIO PER PROSEGUIRE",information!)
end if

// --------------------------------------------------------------------------------------------------------------------
ls_sql = 	" select   cod_tipo_det_ven, cod_misura, quan_consegnata, nota_dettaglio, cod_prodotto, des_prodotto,fat_conversione_ven,anno_registrazione_ord_ven,num_registrazione_ord_ven,prog_riga_ord_ven,flag_st_note_det,quantita_um,prezzo_um,dim_x,dim_y, prezzo_vendita, sconto_1, sconto_2, imponibile_iva, cod_iva " + &
			" from det_bol_ven " + &
			" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
			" anno_registrazione = " + string(il_anno_registrazione) + " and  "  + &
			" num_registrazione = " + string(il_num_registrazione) + &
			" order by cod_azienda, anno_registrazione, num_registrazione, prog_riga_bol_ven "

ll_righe = guo_functions.uof_crea_datastore(ref lds_righe, ls_sql)
if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true
ll_row = 0

lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false
luo_stampa_dati_prodotto = create uo_stampa_dati_prodotto

do while true
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		ll_row ++
		if ll_row > ll_righe then
			
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
			
		else		
			ls_cod_tipo_det_ven 				= lds_righe.getitemstring(ll_row, 1)
			ls_cod_misura 						= lds_righe.getitemstring(ll_row, 2)
			ld_quan_consegnata 				= lds_righe.getitemnumber(ll_row, 3)
			ls_nota_dettaglio  					= lds_righe.getitemstring(ll_row, 4)
			ls_cod_prodotto 					= lds_righe.getitemstring(ll_row, 5)
			ls_des_prodotto 					= lds_righe.getitemstring(ll_row, 6)
			ld_fat_conversione_ven 			= lds_righe.getitemnumber(ll_row, 7)
			ll_anno_registrazione_ord_ven 	= lds_righe.getitemnumber(ll_row, 8)
			ll_num_registrazione_ord_ven 	= lds_righe.getitemnumber(ll_row, 9)
			ll_prog_riga_ord_ven 				= lds_righe.getitemnumber(ll_row, 10)
			ls_flag_st_note_det 				= lds_righe.getitemstring(ll_row, 11)
			ld_quantita_um 					= lds_righe.getitemnumber(ll_row, 12)
			ld_prezzo_um 						= lds_righe.getitemnumber(ll_row, 13)
			ld_dim_x_riga_ddt 				= lds_righe.getitemnumber(ll_row, 14)
			ld_dim_y_riga_ddt 				= lds_righe.getitemnumber(ll_row, 15)
			ld_prezzo_vendita	 				= lds_righe.getitemnumber(ll_row, 16)
			ld_sconto_1							= lds_righe.getitemnumber(ll_row, 17)
			ld_sconto_2			 				= lds_righe.getitemnumber(ll_row, 18)
			ld_imponibile_riga	 				= lds_righe.getitemnumber(ll_row, 19)
			/*
			if not isnull(ls_cod_prodotto) then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_BOL_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata_prod, ref ls_stampa_nota_piede_prod, ref ls_stampa_nota_video_prod) < 0 then
					g_mb.error(ls_stampa_nota_testata)
				else
					if len(ls_stampa_nota_testata_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata_prod},"~r~n")
					if len(ls_stampa_nota_piede_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede_prod},"~r~n")
					if len(ls_stampa_nota_video_prod) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video_prod)
				end if
			end if	
			*/
		end if		

		if ls_flag_st_note_det = "I"				 then //nota dettaglio
			select flag_st_note_bl
			  into :ls_flag_st_note_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		if ls_flag_st_note_det = "N" then
			ls_nota_dettaglio = ""
		end if				
	end if
			

	dw_report_bol_ven.insertrow(0)
	dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())
	
   setnull(ls_cod_prodotto_finito)
	ld_dim_x = 0
	ld_dim_y = 0
	ld_dim_z = 0
	ld_dim_t = 0
	setnull(ls_cod_non_a_magazzino)
	setnull(ls_cod_verniciatura)
	setnull(ls_cod_tessuto)
	
	setnull(ls_cod_misura_mag)

	select flag_stampa_bolla,
			 des_tipo_det_ven
	into   :ls_flag_stampa_bolla,
	       :ls_des_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_bolla)
	end if

	if lb_prima_riga then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", "Vs Codice")
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif lb_flag_nota_dettaglio then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false		
	elseif lb_flag_nota_piede then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_nota_piede)
	else	
		if ls_flag_stampa_bolla = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
			
			if isnull(ls_cod_misura_mag) then ls_cod_misura_mag = ls_cod_misura
			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else			
	
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				end if
				
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			if not isnull(ls_cod_fornitore) then
				select tab_prod_fornitori.cod_prod_fornitore  
				into   :ls_cod_prod_cliente
				from   tab_prod_fornitori  
				where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_prod_fornitori.cod_prodotto = :ls_cod_prodotto and   
						 tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;
		
				if sqlca.sqlcode <> 0 then
					setnull(ls_cod_prod_cliente)
				end if
			end if
			
			
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_misura", ls_cod_misura)
			if ls_flag_tipo_bol_ven <> "T" and ls_cod_misura <> ls_cod_misura_mag and ld_fat_conversione_ven <> 0  then
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_quan_consegnata", ld_quantita_um)
			else
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_misura", ls_cod_misura_mag)
				dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_quan_consegnata", ld_quan_consegnata)
			end if
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", ls_cod_prodotto)
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_des_prodotto)
			
			// EnMe 01-08-2017 nuova gestione nota dettaglio prodotto
			if not isnull(ls_cod_prodotto) then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_BOL_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata_prod, ref ls_stampa_nota_piede_prod, ref ls_stampa_nota_video_prod) < 0 then
					g_mb.error(ls_stampa_nota_testata)
				else
					if len(ls_stampa_nota_testata_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata_prod},"~r~n")
					if len(ls_stampa_nota_piede_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede_prod},"~r~n")
					if len(ls_stampa_nota_video_prod) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video_prod)
				end if
			end if				

			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			elseif not isnull(ls_des_prodotto_anag) then
				ls_descrizione_riga = ls_des_prodotto_anag
			else
				ls_descrizione_riga = ls_des_tipo_det_ven
			end if
			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
				lb_flag_nota_dettaglio = true
			end if
	
			// select dettagli complementari
			if ll_anno_registrazione_ord_ven > 0 and not isnull(ll_anno_registrazione_ord_ven) then
				
				ls_variabili = ""
				luo_stampa_dati_prodotto.uof_stampa_variabili( ls_cod_prodotto, "ORD_VEN", "BOL_VEN", ls_cod_tipo_bol_ven, ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, ls_variabili)
				if len(ls_variabili) > 0 then
					ls_descrizione_riga= ls_des_prodotto + "~r~n" + ls_variabili
				else
					ls_descrizione_riga= ls_des_prodotto
				end if

				select cod_prod_finito, dim_x, dim_y, dim_z, dim_t, cod_non_a_magazzino, cod_verniciatura, cod_tessuto
				into   :ls_cod_prodotto_finito, :ld_dim_x, :ld_dim_y, :ld_dim_z, :ld_dim_t, :ls_cod_non_a_magazzino, :ls_cod_verniciatura, :ls_cod_tessuto
				from   comp_det_ord_ven  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione_ord_ven and
						 num_registrazione = :ll_num_registrazione_ord_ven and  
						 prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
				if sqlca.sqlcode = 0 then			
				else
					
					// non è un prodotto configurato
					// verifico se era un optional configurato
					select dim_x, dim_y
					into   	:ld_dim_x_riga, :ld_dim_y_riga
					from   det_ord_ven  
					where cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :ll_anno_registrazione_ord_ven and
							 num_registrazione = :ll_num_registrazione_ord_ven and  
							 prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
					if sqlca.sqlcode = 0 then			
						if ld_dim_x_riga > 0 then
							dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(),"comp_det_dim_x", ld_dim_x_riga )
						end if
						if ld_dim_y_riga > 0 then
							dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(),"comp_det_dim_y", ld_dim_y_riga)
						end if
					end if
				end if
	//			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_descrizione_riga)
			else
				
				if  ls_flag_tipo_lista_bolle =  "X" then 
					// tipo lista bolle con dettaglio analitico tipo NewFischer
						if ld_dim_x_riga_ddt > 0 and not isnull(ld_dim_x_riga_ddt) then
							dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(),"comp_det_dim_x", ld_dim_x_riga_ddt )
						end if
						if ld_dim_y_riga_ddt > 0 and not isnull(ld_dim_y_riga_ddt) then
							dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(),"comp_det_dim_y", ld_dim_y_riga_ddt)
						end if
				end if
				
			end if
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_descrizione_riga)
	
		else
			dw_report_bol_ven.deleterow(dw_report_bol_ven.getrow())
			continue
		end if
	end if
		
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_documento", ls_cod_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_numeratore_documento", ls_numeratore_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_anno_documento", ll_anno_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_documento", ll_num_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_tipo_bol_ven", ls_cod_tipo_bol_ven)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_data_bolla", ldt_data_bolla)
	
	if ls_flag_tipo_bol_ven = "V" then
		// aggiunto contatto
		if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_cliente)
		else
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_contatto)
		end if
	
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo", ls_indirizzo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_localita", ls_localita)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_frazione", ls_frazione)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap", ls_cap)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia", ls_provincia)

	elseif ls_flag_tipo_bol_ven = "R" or &
			 ls_flag_tipo_bol_ven = "C" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_fornitore)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo", ls_indirizzo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_localita", ls_localita)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_frazione", ls_frazione)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap", ls_cap)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia", ls_provincia)
	
	elseif ls_flag_tipo_bol_ven = "T" then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_deposito_tras)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_1", ls_rag_soc_1_dep_arrivo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_2", "Tel. " + ls_telefono_dep_arrivo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo", ls_indirizzo_dep_arrivo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_localita", ls_localita_dep_arrivo) //ls_localita)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_frazione", ls_frazione_dep_arrivo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap", ls_cap_dep_arrivo)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia", ls_provincia_dep_arrivo)

		// SE BOLLA DI TRASFERIMENTO METTO IL CODICE INTERNO
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", ls_cod_prodotto)

		// se bolla trasferimento metto in coda anche il riferimento ordine ed il cliente
		ls_descrizione_riga = dw_report_bol_ven.getitemstring(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto")
		ls_rag_soc_cliente = ""
		ls_cod_cliente_ordine = ""
		
		select cod_cliente
		into   :ls_cod_cliente_ordine
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione_ord_ven and
				 num_registrazione = :ll_num_registrazione_ord_ven ;
		if sqlca.sqlcode = 0 then			
			
			ls_descrizione_riga += " (ORD." + string(ll_anno_registrazione_ord_ven) + "-" + string(ll_num_registrazione_ord_ven)

			select rag_soc_abbreviata
			into :ls_rag_soc_cliente
			from anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					cod_cliente = :ls_cod_cliente_ordine;
			
			if sqlca.sqlcode = 0 and not isnull(ls_rag_soc_cliente) and len(ls_rag_soc_cliente) > 0 then 
				ls_descrizione_riga += " " + ls_rag_soc_cliente
			end if
			
			ls_descrizione_riga += ")"
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_descrizione_riga)
			
		else
			ls_rag_soc_cliente = ""
			ls_cod_cliente_ordine = ""
		end if
	end if
	
	
//	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_nota_testata", ls_nota_testata)
//	setnull(ls_nota_testata)
//	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_nota_piede", ls_nota_piede)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_porto", ls_cod_porto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_resa", ls_cod_resa)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasporto", ls_des_causale)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasp_lingua", ls_des_causale_lingua)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_aspetto_beni", ls_aspetto_beni)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_colli", ll_num_colli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_peso_lordo", ld_peso_lordo)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_peso_netto", ld_peso_netto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_volume", ld_volume)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_telefono", ls_telefono)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_fax", ls_fax)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_cod_nazione", ls_cod_nazione)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_nazioni_des_nazione", ls_tab_nazioni_des_nazione)
	
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_tipi_bol_ven_des_tipo_bol_ven", ls_des_tipo_bol_ven)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_mezzo", ls_cod_mezzo)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_1", ls_rag_soc_1_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_2", ls_rag_soc_2_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_indirizzo", ls_indirizzo_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_localita", ls_localita_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_frazione", ls_frazione_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_cap", ls_cap_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_provincia", ls_provincia_vet)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_1", ls_rag_soc_1_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_2", ls_rag_soc_2_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_indirizzo", ls_indirizzo_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_localita", ls_localita_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_frazione", ls_frazione_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_cap", ls_cap_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_provincia", ls_provincia_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_data_inizio_trasporto", ldt_data_inizio_trasporto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_ora_inizio_trasporto", ldt_ora_inizio_trasporto)
	
	// -- stefanop 01/03/2010: Inserisco se posso stampare i colli (parametro SCL)
	// Ticket 2010/55
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_stampa_colli", ls_parametro_colli)	
	// ----
	
	// -- stefanop 19/12/2011: deposito
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_anno_registrazione", il_anno_registrazione)	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_registrazione", il_num_registrazione)	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_des_deposito", ls_des_deposito_dep)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia_dep", ls_provincia_dep)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap_dep", ls_cap_dep)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo_dep", ls_indirizzo_dep)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_telefono_dep", ls_telefono_dep)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_fax_dep", ls_fax_dep)
	// ----
	
	// -- Enrico 28-03-2017 aggiunto per Plastinds
	try
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)

		if ls_flag_tipo_bol_ven <> "T" and ls_cod_misura <> ls_cod_misura_mag and ld_fat_conversione_ven <> 0  then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_prezzo_vendita", ld_prezzo_um)
		else
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_prezzo_vendita", ld_prezzo_vendita)
		end if
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_sconto_1", ld_sconto_1)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_sconto_2", ld_sconto_2)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_imponibile_iva", ld_imponibile_riga)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_imponibile_iva", ld_imponibile_iva_bolla)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_importo_iva", ld_importo_iva_bolla)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_tot_val_bolla", ld_tot_val_bolla)
		
	catch (RuntimeError err2)
	end try
	
	
	lb_prima_riga = false
	if lb_flag_nota_piede then exit
loop
//close cu_dettagli;
destroy luo_stampa_dati_prodotto
destroy lds_righe
dw_report_bol_ven.reset_dw_modified(c_resetchildren)
end subroutine

public function integer wf_imposta_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
                messagebox("SEP","Report 1 tende da sole: " + ls_errore,stopsign!)
                return -1
end if
                              
dw_report_bol_ven.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_bol_ven.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
end function

public subroutine wf_impostazioni ();string ls_cod_cliente, ls_cod_lingua, ls_dataobject, ls_logo_testata,ls_cod_tipo_bol_ven, ls_cod_fornitore, ls_cod_contatto
 
//ticket 2014/513
//inserito caso di ddt a contatto, altrimenti visualizza stocazzo come logo !!!!
 
select cod_cliente, cod_fornitore, cod_contatto, cod_tipo_bol_ven
into   	:ls_cod_cliente, :ls_cod_fornitore, :ls_cod_contatto, :ls_cod_tipo_bol_ven
from   tes_bol_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;

if g_str.isnotempty(ls_cod_cliente) then
	
	select cod_lingua
	into :ls_cod_lingua
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
			 
elseif g_str.isnotempty(ls_cod_fornitore) then
	
	select cod_lingua
	into :ls_cod_lingua
	from anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;

elseif g_str.isnotempty(ls_cod_contatto) then
	
	select cod_lingua
	into :ls_cod_lingua
	from anag_contatti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_contatto = :ls_cod_contatto;

else
	return
end if

if sqlca.sqlcode = 0 and g_str.isnotempty(ls_cod_lingua) then
	
	if g_str.isnotempty(ls_cod_cliente) then
		
		select  	dataobject, 
					 logo_testata
		into	 	:ls_dataobject, 
				 	:ls_logo_testata
		from	 	tab_tipi_bol_ven_lingue_dw
		where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
					 cod_lingua = :ls_cod_lingua and
					 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			
			// *** non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
			select dataobject,
					logo_testata
			into	 :ls_dataobject,
					:ls_logo_testata
			from	 tab_tipi_bol_ven_lingue_dw
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
					 cod_lingua = :ls_cod_lingua and
					 ( cod_cliente IS NULL or cod_cliente = '' );
					 
			if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
				// *** esiste un particolare dataobject per quel tipo di fattura per quella lingua
				dw_report_bol_ven.dataobject = ls_dataobject					
			else 
				
				// *** non esiste per quel cliente/lingua un modulo specifico; controllo se esiste a livello di tipo bolla
				select dataobject,
						logo_testata
				into	 :ls_dataobject,
						:ls_logo_testata
				from	 tab_tipi_bol_ven_lingue_dw
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
						 cod_lingua is null and
						 ( cod_cliente IS NULL or cod_cliente = '' );
						 
				if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
					// *** esiste un particolare dataobject per quel tipo di fattura per quella lingua
					dw_report_bol_ven.dataobject = ls_dataobject					
				else 
					// *** altrimenti lascio stare tutto com'è
				end if		
				
			end if		
			
		end if
		
	else 
		
		// verifico impostazioni per fornitore / contatto
		select dataobject,
				logo_testata
		into	 :ls_dataobject,
				:ls_logo_testata
		from	 tab_tipi_bol_ven_lingue_dw
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
				 cod_lingua = :ls_cod_lingua and
				 ( cod_cliente IS NULL or cod_cliente = '' );
				 
		if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
			// esiste un particolare dataobject per quel tipo di fattura per quella lingua
			dw_report_bol_ven.dataobject = ls_dataobject					
		else 
			
			//  non esiste per quel cliente/lingua un modulo specifico; controllo se esiste a livello di tipo bolla
			select dataobject,
					logo_testata
			into	 :ls_dataobject,
					:ls_logo_testata
			from	 tab_tipi_bol_ven_lingue_dw
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
					 cod_lingua is null and
					 ( cod_cliente IS NULL or cod_cliente = '' );
					 
			if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
				//  esiste un particolare dataobject per quel tipo di fattura per quella lingua
				dw_report_bol_ven.dataobject = ls_dataobject					
			else 
				// altrimenti lascio stare tutto com'è
			end if		
			
		end if		

	end if		
else
	if g_str.isempty(ls_cod_lingua) then
		//  non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
		select dataobject,
				logo_testata
		into	 :ls_dataobject,
				:ls_logo_testata
		from	 tab_tipi_bol_ven_lingue_dw
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
				 cod_lingua is null and
				 ( cod_cliente IS NULL or cod_cliente = '' );
				 
		if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
			//  esiste un particolare dataobject per quel tipo di bolla per quella lingua
			dw_report_bol_ven.dataobject = ls_dataobject					
		end if
		
		select dataobject,
				logo_testata
		into	 :ls_dataobject,
				:ls_logo_testata
		from	 tab_tipi_bol_ven_lingue_dw
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven and
				 cod_lingua is null and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
			//  esiste un particolare dataobject per quel tipo di bolla per quella lingua
			dw_report_bol_ven.dataobject = ls_dataobject					
		end if
		
	end if		
end if

if g_str.isempty(ls_logo_testata) then
	
	select parametri_azienda.stringa
	into   :ls_logo_testata
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO1';
			 
end if

if g_str.isnotempty(ls_logo_testata) then
	dw_report_bol_ven.modify( "intestazione.filename='" + s_cs_xx.volume + ls_logo_testata + "'~t")
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_cod_tipo_bol_ven, ls_cod_cliente, ls_flag_tipo_soggetto, ls_cod_fornitore, &
		ls_cod_contatto
long ll_copie, ll_copie_cee, ll_copie_extra_cee, ll_ret

dw_report_bol_ven.ib_dw_report=true

set_w_options(c_noresizewin + c_NoAutoSize)
save_on_close(c_socnosave)

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

dw_report_bol_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
dw_report_bol_ven.set_document_name("Bolla di vendita " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))

wf_impostazioni()

ib_estero = false

select cod_cliente,
		cod_fornitore,
		cod_contatto,
       	cod_tipo_bol_ven
into   :ls_cod_cliente,
		:ls_cod_fornitore,
		:ls_cod_contatto,
		 :ls_cod_tipo_bol_ven
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
		 
if sqlca.sqlcode = 0 then
	
	if not isnull(ls_cod_cliente) then
		
		select flag_tipo_cliente
		into   :ls_flag_tipo_soggetto
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
	elseif not isnull(ls_cod_fornitore) then
				
		select flag_tipo_fornitore
		into   :ls_flag_tipo_soggetto
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;

	elseif not isnull(ls_cod_contatto) then
		select flag_tipo_cliente
		into   :ls_flag_tipo_soggetto
		from   anag_contatti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_contatto = :ls_cod_contatto;
				 
	end if
	
	if sqlca.sqlcode = 0 then
		
		select num_copie,
				 num_copie_cee,
				 num_copie_extra_cee
		into   :ll_copie,
				 :ll_copie_cee,
				 :ll_copie_extra_cee
		from   tab_tipi_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
				 
		if sqlca.sqlcode = 0 then
			
			choose case ls_flag_tipo_soggetto
				case "C"
					ib_estero = true
					il_num_copie = ll_copie_cee
				case "E"
					ib_estero = true
					il_num_copie = ll_copie_extra_cee
				case else
					ib_estero = false
					il_num_copie = ll_copie
			end choose
			
		end if
		
	else
		il_num_copie = 1
	end if
	
end if

cb_1.text = "Stampa " + string(il_num_copie) + " Copie"

dw_report_bol_ven.object.datawindow.print.preview = 'Yes'
dw_report_bol_ven.object.datawindow.print.preview.rulers = 'Yes'
dw_report_bol_ven.postevent("pcd_print")
end event

on w_report_bol_ven.create
int iCurrent
call super::create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.dw_report_bol_ven=create dw_report_bol_ven
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_3
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_report_bol_ven
this.Control[iCurrent+4]=this.cb_1
end on

on w_report_bol_ven.destroy
call super::destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.dw_report_bol_ven)
destroy(this.cb_1)
end on

event ue_anteprima_pdf;//LASCIARE CHECK ANCESTOR SCRIPT DISATTIVATO
//la variabile ib_pdf viene messa a true prima di creare il pdf e poi rimessa a false
//per evitare di generare un pdf con foglio bianco ....
//il problema lo si evince dallo script dell'evento printpage


if isvalid(pcca.window_currentdw) then
	
	//########################################
	ib_pdf = true
	//########################################
	
	pcca.window_currentdw.triggerevent("ue_anteprima_pdf")
	
	//########################################
	ib_pdf = false
	//########################################
end if

end event

type cb_3 from commandbutton within w_report_bol_ven
integer x = 1737
integer y = 20
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pag. Succ."
end type

event clicked;dw_report_bol_ven.scrollnextpage()
end event

type cb_2 from commandbutton within w_report_bol_ven
integer x = 1349
integer y = 20
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pag. Prec."
end type

event clicked;dw_report_bol_ven.scrollpriorpage()

end event

type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
integer x = 23
integer y = 140
integer width = 3863
integer height = 4680
integer taborder = 20
string dataobject = "d_report_bol_ven_gibus"
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event printpage;call super::printpage;
// donato 09/05/2014: devo controlla se sto esportando in PDF
// In tal caso della mail non devo fare nessun controllo sul numero di pagina altrimenti il saveas della datawindow
// mi ritorna sempre una foglio bianco!

if ib_pdf then return 0


if not(ib_stampa) then return 1
if pagenumber <> il_pagina_corrente then return 1
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

event pcd_saverowsas;//LASCIARE CHECK ANCESTOR SCRIPT DISATTIVATO
//la variabile ib_pdf viene messa a true prima di creare il pdf e poi rimessa a false
//per evitare di generare un pdf con foglio bianco ....
//il problema lo si evince dallo script dell'evento printpage


//#######################################
ib_pdf = true
//#######################################

saveas()

//#######################################
ib_pdf = false
//#######################################
end event

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then
	// 28-03-2017: solo per Plastinds nascondo prezzi
	if dwo.name="b_nascondi_prezzi" then
		dw_report_bol_ven.Object.det_bol_ven_prezzo_vendita.Visible = 0
		dw_report_bol_ven.Object.det_bol_ven_sconto_1.Visible = 0
		dw_report_bol_ven.Object.det_bol_ven_sconto_2.Visible = 0
		dw_report_bol_ven.Object.sconti_t.Visible = 0
		dw_report_bol_ven.Object.det_bol_ven_imponibile_iva.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_imponibile_iva.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_importo_iva.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_tot_val_bolla.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_imponibile_iva_t.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_importo_iva_t.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_tot_val_bolla_t.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_prezzo_vendita_t.Visible = 0
		dw_report_bol_ven.Object.tes_bol_ven_sconti_t.Visible = 0
		dw_report_bol_ven.Object.det_bol_ven_imponibile_iva_t.Visible = 0
	end if
end if
end event

type cb_1 from commandbutton within w_report_bol_ven
integer x = 37
integer y = 20
integer width = 503
integer height = 88
integer taborder = 1
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA COPIE"
end type

event clicked;string ls_error
long ll_i, ll_y
uo_moduli_stampa luo_moduli_stampa


ib_stampa = true

for il_pagina_corrente = 1 to il_totale_pagine
	
	for ll_i = 1 to il_num_copie
		
		choose case ll_i
			case 1 
				dw_report_bol_ven.object.st_copia.text = "Copia Destinatario"
			case 2
				dw_report_bol_ven.object.st_copia.text = "Copia Mittente"
			case 3
				dw_report_bol_ven.object.st_copia.text = "Copia Vettore"
			case else
				dw_report_bol_ven.object.st_copia.text = "- - -"
		end choose
		
		dw_report_bol_ven.triggerevent("pcd_print")
		
	next
	
	parent.postevent("ue_avviso_spedizione")
	
next
/*
STEFANOP */
ib_pdf = true
luo_moduli_stampa = create uo_moduli_stampa
luo_moduli_stampa.uof_stampa_moduli(dw_report_bol_ven, "BOLVEN", "", "", ls_error)
destroy luo_moduli_stampa
/**/
end event

