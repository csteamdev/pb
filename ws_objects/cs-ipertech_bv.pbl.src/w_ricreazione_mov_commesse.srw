﻿$PBExportHeader$w_ricreazione_mov_commesse.srw
forward
global type w_ricreazione_mov_commesse from window
end type
type em_data_dal from editmask within w_ricreazione_mov_commesse
end type
type st_3 from statictext within w_ricreazione_mov_commesse
end type
type st_2 from statictext within w_ricreazione_mov_commesse
end type
type st_path_origine from statictext within w_ricreazione_mov_commesse
end type
type st_1 from statictext within w_ricreazione_mov_commesse
end type
type em_data_al from editmask within w_ricreazione_mov_commesse
end type
type cb_inizio2 from commandbutton within w_ricreazione_mov_commesse
end type
type st_log from statictext within w_ricreazione_mov_commesse
end type
type hpb_1 from hprogressbar within w_ricreazione_mov_commesse
end type
type tab_1 from tab within w_ricreazione_mov_commesse
end type
type page_1 from userobject within tab_1
end type
type mle_1 from multilineedit within page_1
end type
type page_1 from userobject within tab_1
mle_1 mle_1
end type
type page_2 from userobject within tab_1
end type
type mle_2 from multilineedit within page_2
end type
type page_2 from userobject within tab_1
mle_2 mle_2
end type
type page_3 from userobject within tab_1
end type
type mle_3 from multilineedit within page_3
end type
type page_3 from userobject within tab_1
mle_3 mle_3
end type
type tab_1 from tab within w_ricreazione_mov_commesse
page_1 page_1
page_2 page_2
page_3 page_3
end type
type cb_inizio from commandbutton within w_ricreazione_mov_commesse
end type
type cbx_solo_trasf from checkbox within w_ricreazione_mov_commesse
end type
end forward

global type w_ricreazione_mov_commesse from window
integer width = 3991
integer height = 2636
boolean titlebar = true
string title = "Ricreazione movimenti commesse"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
em_data_dal em_data_dal
st_3 st_3
st_2 st_2
st_path_origine st_path_origine
st_1 st_1
em_data_al em_data_al
cb_inizio2 cb_inizio2
st_log st_log
hpb_1 hpb_1
tab_1 tab_1
cb_inizio cb_inizio
cbx_solo_trasf cbx_solo_trasf
end type
global w_ricreazione_mov_commesse w_ricreazione_mov_commesse

type variables
string is_file_elaborate				= "elaborate.log"
string is_file_ddt_multipli				= "ddt_multipli.log"
string is_file_commesse_escluse	= "escluse.log"

string is_path_origine
end variables

forward prototypes
public subroutine wf_scrivi_log_mle (integer fi_tipo_log, string fs_messaggio)
end prototypes

public subroutine wf_scrivi_log_mle (integer fi_tipo_log, string fs_messaggio);long ll_handle
string ls_file


if fi_tipo_log = 1 then
	//log commesse elaborate
	tab_1.page_1.mle_1.text += fs_messaggio
	tab_1.page_1.mle_1.scroll(tab_1.page_1.mle_1.linecount())
	tab_1.page_1.mle_1.SetRedraw(true)
	ls_file = is_path_origine + is_file_elaborate
	
elseif fi_tipo_log = 2 then
	//log ddt multipli
	tab_1.page_2.mle_2.text += fs_messaggio
	tab_1.page_2.mle_2.scroll(tab_1.page_2.mle_2.linecount())
	tab_1.page_2.mle_2.SetRedraw(true)
	ls_file = is_path_origine + is_file_ddt_multipli
	
elseif fi_tipo_log = 3 then
	//log commesse eseluse
	tab_1.page_3.mle_3.text += fs_messaggio
	tab_1.page_3.mle_3.scroll(tab_1.page_3.mle_3.linecount())
	tab_1.page_3.mle_3.SetRedraw(true)
	ls_file = is_path_origine + is_file_commesse_escluse
	
end if

if fi_tipo_log = 1 or fi_tipo_log = 2 or fi_tipo_log = 3 then
	ll_handle = fileopen(ls_file, linemode!, write!)
	filewrite(ll_handle, string(datetime(today(), now()), "dd/mm/yyyy hh:mm:ss") + "~t" + fs_messaggio)
	fileclose(ll_handle)
end if

return


end subroutine

on w_ricreazione_mov_commesse.create
this.em_data_dal=create em_data_dal
this.st_3=create st_3
this.st_2=create st_2
this.st_path_origine=create st_path_origine
this.st_1=create st_1
this.em_data_al=create em_data_al
this.cb_inizio2=create cb_inizio2
this.st_log=create st_log
this.hpb_1=create hpb_1
this.tab_1=create tab_1
this.cb_inizio=create cb_inizio
this.cbx_solo_trasf=create cbx_solo_trasf
this.Control[]={this.em_data_dal,&
this.st_3,&
this.st_2,&
this.st_path_origine,&
this.st_1,&
this.em_data_al,&
this.cb_inizio2,&
this.st_log,&
this.hpb_1,&
this.tab_1,&
this.cb_inizio,&
this.cbx_solo_trasf}
end on

on w_ricreazione_mov_commesse.destroy
destroy(this.em_data_dal)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_path_origine)
destroy(this.st_1)
destroy(this.em_data_al)
destroy(this.cb_inizio2)
destroy(this.st_log)
destroy(this.hpb_1)
destroy(this.tab_1)
destroy(this.cb_inizio)
destroy(this.cbx_solo_trasf)
end on

event open;date ldt_data_min_chiusura

ldt_data_min_chiusura = date(2012, 7, 10)

em_data_al.text = string(ldt_data_min_chiusura, "dd/mm/yyyy")


is_path_origine = guo_functions.uof_get_user_documents_folder( )

st_path_origine.text = is_path_origine

end event

type em_data_dal from editmask within w_ricreazione_mov_commesse
integer x = 242
integer y = 232
integer width = 453
integer height = 96
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type st_3 from statictext within w_ricreazione_mov_commesse
integer x = 859
integer y = 248
integer width = 183
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "al:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_ricreazione_mov_commesse
integer x = 37
integer y = 248
integer width = 183
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "dal:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_path_origine from statictext within w_ricreazione_mov_commesse
integer x = 1678
integer y = 144
integer width = 2235
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_ricreazione_mov_commesse
integer x = 32
integer y = 148
integer width = 1294
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elabora solo le commesse chiuse nel periodo dal"
boolean focusrectangle = false
end type

type em_data_al from editmask within w_ricreazione_mov_commesse
integer x = 1061
integer y = 232
integer width = 453
integer height = 96
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type cb_inizio2 from commandbutton within w_ricreazione_mov_commesse
integer x = 1193
integer y = 28
integer width = 306
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Procedi"
end type

event clicked;string				ls_confirm, ls_errore, ls_sql, ls_ddt_trasf_multipli
boolean			lb_solo_trasf
integer			li_anno_commessa_cu, li_ret, li_anno_comm_el[]
long				ll_num_commessa_cu, ll_count, ll_index, ll_num_comm_el[]
uo_funzioni_1	luo_funz
datetime			ldt_data_elab, ldt_data_al, ldt_data_dal


ls_confirm = "Iniziare l'elaborazione delle commesse"

lb_solo_trasf = true

tab_1.page_1.mle_1.text = ""
tab_1.page_2.mle_2.text = ""
ldt_data_al = datetime(date(em_data_al.text), 00:00:00)
ldt_data_dal = datetime(date(em_data_dal.text), 00:00:00)

if isnull(ldt_data_al) or year(date(ldt_data_al))<=1950 then
	ldt_data_al = datetime(date(2012, 07, 10), 00:00:00)
end if

if isnull(ldt_data_dal) or year(date(ldt_data_dal))<=1950 then
	setnull(ldt_data_dal)
end if

if lb_solo_trasf then ls_confirm += " (solo quelle soggette a trasferimento intragruppo)"

ls_confirm += " ?"

if not g_mb.confirm(ls_confirm) then return

st_log.text = "Preparazione dati commesse da elaborare ..."

ls_sql =	"select distinct c.anno_commessa, c.num_commessa "+&
			"from anag_commesse as c "+&
			"join det_ord_ven as dord on dord.cod_azienda=c.cod_azienda and "+&
												"dord.anno_commessa=c.anno_commessa and "+&
												"dord.num_commessa=c.num_commessa "+&
					"join det_bol_ven as dbol on dbol.cod_azienda=dord.cod_azienda and "+&
														"dbol.anno_registrazione_ord_ven=dord.anno_registrazione and "+&
														"dbol.num_registrazione_ord_ven=dord.num_registrazione and "+&
														"dbol.prog_riga_ord_ven=dord.prog_riga_ord_ven "+&
			"where 	c.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"c.flag_tipo_avanzamento='7' and "+&
						"dbol.cod_tipo_det_ven='TRI' "+&
						" and c._flag_elab='N' "+&
						" and c.data_chiusura <= '" + string(ldt_data_al, s_cs_xx.db_funzioni.formato_data) + "' "
						
if not isnull(ldt_data_dal) then
	ls_sql += " and c.data_chiusura >= '" + string(ldt_data_dal, s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += "order by c.anno_commessa, c.num_commessa "

//creazione cursore commesse produzione (solo quelle chiuse e soggete a trasferimento interno) ----------------------------------------------------------------
declare cu_commesse dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_commesse;

if sqlca.sqlcode < 0 then
	ls_errore = "~r~nErrore in apertura cursore cu_commesse: " + sqlca.sqlerrtext
	wf_scrivi_log_mle(1, "~r~n" + ls_errore)
	st_log.text = "Fine con ERRORI !!"
	rollback;
	return
end if

do while true
	fetch cu_commesse into :li_anno_commessa_cu, :ll_num_commessa_cu;
	
	if sqlca.sqlcode < 0 then
		ls_errore = "~r~nErrore in fetch cursore cu_commesse:" + sqlca.sqlerrtext
		wf_scrivi_log_mle(1, "~r~n" + ls_errore)
		st_log.text = "Fine con ERRORI !!"
		close cu_commesse;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	ll_index += 1
	ls_confirm = "Caricamento commessa "+string(li_anno_commessa_cu)+"/"+string(ll_num_commessa_cu)
	st_log.text = ls_confirm + " "+ string(ll_index) + " di " + string(ll_count)
	
	li_anno_comm_el[ll_index] = li_anno_commessa_cu
	ll_num_comm_el[ll_index] = ll_num_commessa_cu
	
loop
close cu_commesse;

//inizio elaborazione ------------------------------------------------------------------------------
ll_count =  upperbound(li_anno_comm_el[])
hpb_1.maxposition = ll_count
hpb_1.setstep = 1

for ll_index=1 to upperbound(li_anno_comm_el[])
	Yield()
	
	li_anno_commessa_cu = li_anno_comm_el[ll_index]
	ll_num_commessa_cu = ll_num_comm_el[ll_index]
	
	//procedi
	ls_confirm = "Elaborazione commessa "+string(li_anno_commessa_cu)+"/"+string(ll_num_commessa_cu)
	st_log.text = ls_confirm + " "+ string(ll_index) + " di " + string(ll_count)
	
	ls_confirm = "----------------------------------------------------------------------------------------------------------------------~r~n" + ls_confirm
	
	hpb_1.stepit( )
	
	luo_funz = create uo_funzioni_1
	ls_ddt_trasf_multipli = ""
	li_ret = luo_funz.uof_riallinea_mov_commessa(li_anno_commessa_cu, ll_num_commessa_cu, lb_solo_trasf, ls_errore)
	ls_ddt_trasf_multipli = luo_funz.is_ddt_trasf_multipli_log
	destroy luo_funz
	
	if ls_ddt_trasf_multipli <> "" and not isnull(ls_ddt_trasf_multipli) then
		wf_scrivi_log_mle(2, ls_ddt_trasf_multipli)
	end if
	
	ldt_data_elab = datetime(today(), now())
	
	choose case li_ret
		case is < 0
			rollback;
			ls_confirm += "~r~n" + ls_errore
			st_log.text = "Fine con ERRORI !!"
			wf_scrivi_log_mle(1, ls_confirm)
			return
			
		case 1
			//dai il messaggio (in caso di elaborazione multipla salterà la commessa, ma andrà avanti)
			wf_scrivi_log_mle(3, ls_confirm + "~r~n" + ls_errore)
			rollback;
			continue
		
		case 2
			//qualche problema su commesse di righe collegate
			ls_confirm += "~r~n" + ls_errore
			wf_scrivi_log_mle(1, ls_confirm)
			
			update anag_commesse
			set 	_flag_elab = 'S',
					_data_elab = :ldt_data_elab
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_commessa=:li_anno_commessa_cu and
						num_commessa=:ll_num_commessa_cu;
			
			commit;
			continue
			
		case else
			//tutto a posto, operazione completata
			ls_confirm += "~r~nOperazione effettuata con successo!"
			wf_scrivi_log_mle(1, ls_confirm)
			
			update anag_commesse
			set 	_flag_elab = 'S',
					_data_elab = :ldt_data_elab
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_commessa=:li_anno_commessa_cu and
						num_commessa=:ll_num_commessa_cu;
		
			commit;
			continue
			
	end choose
	
next

wf_scrivi_log_mle(1, "~r~nFINE ######################################################")
st_log.text = "Fine!"

end event

type st_log from statictext within w_ricreazione_mov_commesse
integer x = 1536
integer y = 44
integer width = 2391
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_ricreazione_mov_commesse
integer x = 37
integer y = 348
integer width = 3909
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type tab_1 from tab within w_ricreazione_mov_commesse
integer x = 14
integer y = 456
integer width = 3927
integer height = 2060
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
page_1 page_1
page_2 page_2
page_3 page_3
end type

on tab_1.create
this.page_1=create page_1
this.page_2=create page_2
this.page_3=create page_3
this.Control[]={this.page_1,&
this.page_2,&
this.page_3}
end on

on tab_1.destroy
destroy(this.page_1)
destroy(this.page_2)
destroy(this.page_3)
end on

type page_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3890
integer height = 1936
long backcolor = 12632256
string text = "Log Commesse elaborate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_1 mle_1
end type

on page_1.create
this.mle_1=create mle_1
this.Control[]={this.mle_1}
end on

on page_1.destroy
destroy(this.mle_1)
end on

type mle_1 from multilineedit within page_1
integer x = 14
integer y = 16
integer width = 3867
integer height = 1904
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type page_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3890
integer height = 1936
long backcolor = 12632256
string text = "Log DDT trasf multipli"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_2 mle_2
end type

on page_2.create
this.mle_2=create mle_2
this.Control[]={this.mle_2}
end on

on page_2.destroy
destroy(this.mle_2)
end on

type mle_2 from multilineedit within page_2
integer x = 9
integer y = 8
integer width = 3881
integer height = 1936
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type page_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3890
integer height = 1936
long backcolor = 12632256
string text = "Log Commesse escluse"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
mle_3 mle_3
end type

on page_3.create
this.mle_3=create mle_3
this.Control[]={this.mle_3}
end on

on page_3.destroy
destroy(this.mle_3)
end on

type mle_3 from multilineedit within page_3
integer x = 9
integer y = 20
integer width = 3877
integer height = 1896
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_inizio from commandbutton within w_ricreazione_mov_commesse
boolean visible = false
integer x = 3639
integer y = 76
integer width = 306
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Procedi"
end type

event clicked;string				ls_confirm, ls_errore, ls_sql, ls_ddt_trasf_multipli
boolean			lb_solo_trasf
integer			li_anno_commessa_cu, li_ret
long				ll_num_commessa_cu, ll_count, ll_index
uo_funzioni_1	luo_funz
datastore		lds_dati


ls_confirm = "Iniziare l'elaborazione delle commesse"

lb_solo_trasf = true

tab_1.page_1.mle_1.text = ""
tab_1.page_2.mle_2.text = ""

if lb_solo_trasf then ls_confirm += " (solo quelle soggette a trasferimento intragruppo)"

ls_confirm += " ?"

if not g_mb.confirm(ls_confirm) then return

st_log.text = "Preparazione dati commesse da elaborare ..."

ls_sql =	"select distinct c.anno_commessa, c.num_commessa "+&
			"from anag_commesse as c "+&
			"join det_ord_ven as dord on dord.cod_azienda=c.cod_azienda and "+&
												"dord.anno_commessa=c.anno_commessa and "+&
												"dord.num_commessa=c.num_commessa "+&
					"join det_bol_ven as dbol on dbol.cod_azienda=dord.cod_azienda and "+&
														"dbol.anno_registrazione_ord_ven=dord.anno_registrazione and "+&
														"dbol.num_registrazione_ord_ven=dord.num_registrazione and "+&
														"dbol.prog_riga_ord_ven=dord.prog_riga_ord_ven "+&
			"where 	c.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"c.flag_tipo_avanzamento='7' and "+&
						"dbol.cod_tipo_det_ven='TRI' "+&
			"order by c.anno_commessa, c.num_commessa "

//creo il datastore solo per il conteggio delle righe da elaborare
ll_count = guo_functions.uof_crea_datastore(lds_dati, ls_sql, ls_errore)
destroy lds_dati;
if ll_count<0 then
	wf_scrivi_log_mle(1, "~r~n" + ls_errore)
	st_log.text = "Fine con ERRORI !!"
	rollback;
	return
end if

if isnull(ll_count) then ll_count = 0
ll_index = 0
hpb_1.maxposition = ll_count
hpb_1.setstep = 1


//creazione cursore commesse produzione (solo quelle chiuse e soggete a trasferimento interno) ----------------------------------------------------------------
declare cu_commesse dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_commesse;

if sqlca.sqlcode < 0 then
	ls_errore = "~r~nErrore in apertura cursore cu_commesse: " + sqlca.sqlerrtext
	wf_scrivi_log_mle(1, "~r~n" + ls_errore)
	st_log.text = "Fine con ERRORI !!"
	rollback;
	return
end if

do while true
	fetch cu_commesse into :li_anno_commessa_cu, :ll_num_commessa_cu;
	Yield()
	
	if sqlca.sqlcode < 0 then
		ls_errore = "~r~nErrore in fetch cursore cu_commesse:" + sqlca.sqlerrtext
		wf_scrivi_log_mle(1, "~r~n" + ls_errore)
		st_log.text = "Fine con ERRORI !!"
		close cu_commesse;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	ll_index += 1
	
	ls_confirm = "Elaborazione commessa "+string(li_anno_commessa_cu)+"/"+string(ll_num_commessa_cu)
	st_log.text = ls_confirm + " "+ string(ll_index) + " di " + string(ll_count)
	
	ls_confirm = "----------------------------------------------------------------------------------------------------------------------~r~n" + ls_confirm
	//wf_scrivi_log_mle(1, "~r~n" + ls_confirm)
	hpb_1.stepit( )
	
	luo_funz = create uo_funzioni_1
	ls_ddt_trasf_multipli = ""
	li_ret = luo_funz.uof_riallinea_mov_commessa(li_anno_commessa_cu, ll_num_commessa_cu, lb_solo_trasf, ls_errore)
	ls_ddt_trasf_multipli = luo_funz.is_ddt_trasf_multipli_log
	destroy luo_funz
	
	if ls_ddt_trasf_multipli <> "" and not isnull(ls_ddt_trasf_multipli) then
		wf_scrivi_log_mle(2, ls_ddt_trasf_multipli)
	end if
	
	choose case li_ret
		case is < 0
			rollback;
			ls_confirm += "~r~n" + ls_errore
			st_log.text = "Fine con ERRORI !!"
			wf_scrivi_log_mle(1, ls_confirm)
			return
			
		case 1
			//dai il messaggio (in caso di elaborazione multipla salterà la commessa, ma andrà avanti)
			wf_scrivi_log_mle(3, ls_confirm + "~r~n" + ls_errore)
			continue
		
		case 2
			//qualche problema su commesse di righe collegate
			ls_confirm += "~r~n" + ls_errore
			wf_scrivi_log_mle(1, ls_confirm)
			continue
			
		case else
			//tutto a posto, operazione completata
			ls_confirm += "~r~nOperazione effettuata con successo!"
			wf_scrivi_log_mle(1, ls_confirm)
			
	end choose
	
loop

close cu_commesse;

//se arrivi fin qui fai un unico commit
commit;
wf_scrivi_log_mle(1, "~r~nFINE ######################################################")
st_log.text = "Fine!"

end event

type cbx_solo_trasf from checkbox within w_ricreazione_mov_commesse
integer x = 32
integer y = 28
integer width = 1070
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "solo soggette a trasf. intragruppo"
boolean checked = true
boolean lefttext = true
end type

