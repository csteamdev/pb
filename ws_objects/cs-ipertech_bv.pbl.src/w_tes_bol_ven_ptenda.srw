﻿$PBExportHeader$w_tes_bol_ven_ptenda.srw
forward
global type w_tes_bol_ven_ptenda from w_tes_bol_ven
end type
end forward

global type w_tes_bol_ven_ptenda from w_tes_bol_ven
integer width = 4727
integer height = 2840
event pc_menu_conferma ( )
end type
global w_tes_bol_ven_ptenda w_tes_bol_ven_ptenda

event pc_menu_conferma;call super::pc_menu_conferma;string ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_str, &
		ls_cod_contatto, ls_flag_segue_fatture, ls_cod_causale, ls_cod_documento
long ll_anno_registrazione, ll_num_registrazione, ll_result


ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(1, "num_registrazione")
ls_cod_contatto = tab_dettaglio.det_1.dw_1.getitemstring(1, "cod_contatto")
ls_cod_causale = tab_dettaglio.det_1.dw_1.getitemstring(1, "cod_causale")
ls_cod_documento = tab_dettaglio.det_1.dw_1.getitemstring(1, "cod_documento")

ls_tes_tabella = "tes_bol_ven"
ls_det_tabella = "det_bol_ven"
ls_prog_riga = "prog_riga_bol_ven"
ls_quantita = "quan_consegnata"

if isnull(ls_cod_documento) or ls_cod_documento = "" then
	g_mb.warning("Impossibile confermare una bolla senza numero fiscale assegnato!")
	return
end if

// stefanop: 09/02/2012se la bolla ha un contatto assegnato allora vuol dire che la bolla
// è di tipo omaggio e devo controllare la causale se ha il flag_segue_fattura impostato
if not isnull(ls_cod_contatto) and ls_cod_contatto <> "" then
	select flag_segue_fattura
	into :ls_flag_segue_fatture
	from tab_causali_trasp
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_causale = :ls_cod_causale;
			
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in controllo del flag segue fattura in Causali Trasporto.", sqlca)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		return
	end if
	
	if ls_flag_segue_fatture = "S" then
		g_mb.warning("La bolla che si sta per confermare  è per un Contatto; la causale del trasporto non può essere fatturabile.")
		return
	end if
end if
// ----

ll_result = f_conferma_mov_mag(ls_tes_tabella, &
							 ls_det_tabella, &
							 ls_prog_riga, &
							 ls_quantita, &
							 ll_anno_registrazione, &
							 ll_num_registrazione, &
							 ls_messaggio)
if ll_result = -1 then
	g_mb.messagebox("Conferma Bolla", "Errore durante la conferma della bolla~r~nDettaglio errore:" + ls_messaggio + "~r~n", Information!)
	rollback;
elseif ll_result = 0 then
	commit;
	g_mb.messagebox("Conferma Bolla","Bolla confermata con successo")
end if

triggerevent("pc_retrieve")
end event

on w_tes_bol_ven_ptenda.create
call super::create
end on

on w_tes_bol_ven_ptenda.destroy
call super::destroy
end on

event pc_menu_stampa;/* TOLTO ANCESTOR */
string							ls_window = "w_report_bol_ven_euro", ls_bve, ls_messaggio
long							ll_anno_reg_mov, ll_num_reg_mov, ll_count
datetime						ldt_data_stock, ldt_data_reg
window						lw_window
integer						li_ret
long							ll_anno_registrazione, ll_num_registrazione, ll_num_documento
string							ls_cod_documento, ls_cod_cliente
s_open_parm	lstr_open_parm


s_cs_xx.parametri.parametro_i_1 = 0
ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")
s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")
ls_cod_cliente = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_cliente")
ldt_data_reg = tab_dettaglio.det_1.dw_1.getitemdatetime(tab_dettaglio.det_1.dw_1.getrow(), "data_registrazione")



declare cu_det_bol_ven cursor for
	select anno_reg_des_mov, num_reg_des_mov
	from det_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;

open cu_det_bol_ven;

do while true
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.error("Anno registrazione " + string(ll_anno_registrazione) + "  num_registrazione " + string(ll_num_registrazione) + " non esistono in DET_BOL_VEV")
		exit
	end if	
	
	select count(*)
	into :ll_count
	from dest_mov_magazzino	  
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_reg_mov and
			 num_registrazione = :ll_num_reg_mov and
			 (cod_deposito is null or cod_ubicazione is null or cod_lotto is null or data_stock is null or prog_stock is null);
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo della dest_mov_magazzino (" + string(ll_anno_reg_mov) + "/" + string(ll_num_reg_mov) + ")", sqlca)
		close cu_det_bol_ven;
		return
	elseif ll_count > 0 then
		g_mb.error("Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " sono incompleti")
		close cu_det_bol_ven;
		return
	end if
loop

close cu_det_bol_ven;

// Calcolo il documento
triggerevent("pc_menu_calcola")


//***************************************************************
li_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_data_reg, "SBV", ls_messaggio)

if li_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	return
elseif li_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//***************************************************************



// *** Michela 24/01/2006: sviluppo funzione 5 specifica storico bolle di ptenda.
//                         l'utente ha la possibilità di modificare gli addebiti del cliente prima di stampare in definitiva
//                         Il documento però non deve avere numero fiscale.
if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	
	ls_cod_cliente = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_cliente") 
	
	select cod_documento,
			num_documento
	into :ls_cod_documento,
		  :ll_num_documento
	from tes_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode = 0 then
		
		// *** controllo se ci sono addebiti
		// stefanop: 09/02/2012: controllo solo se è impostato il cliente
		if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
		
			select count(*)
			into   :ll_count
			from   anag_clienti_addebiti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_cliente = :ls_cod_cliente and
					 flag_mod_valore_bolle = 'S';
					 
			if not isnull(ll_count) and ll_count > 0 then
				if isnull(ls_cod_documento) and ( isnull(ll_num_documento) or ll_num_documento = 0) then
					lstr_open_parm.anno_registrazione = ll_anno_registrazione
					lstr_open_parm.num_registrazione = ll_num_registrazione
					window_open_parm(w_mod_importi_bol_ven, 0, lstr_open_parm)
				end if	
			end if
			
		end if
		
	end if		
end if

// *** fine modifica
if isnull(tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_documento")) then
	s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
	window_open(w_tipo_stampa_bol_ven, 0)
	tab_dettaglio.det_1.dw_1.triggerevent("pcd_retrieve")
end if

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	guo_functions.uof_get_parametro_azienda("BVE", ls_bve)
	
	if isnull(ls_bve) or ls_bve = "" then
		window_open(w_report_bol_ven, -1)
	else
		window_type_open(lw_window,ls_window, -1)
	end if
	
end if
end event

event pc_menu_dettagli;/* TOLTO ANCESTOR */

if tab_dettaglio.det_1.dw_1.getrow() > 0 then
	
	s_cs_xx.parametri.parametro_w_bol_ven = this
	
	window_open_parm(w_det_bol_ven_ptenda, -1, tab_dettaglio.det_1.dw_1)
	
end if

end event

event pc_setwindow;call super::pc_setwindow;ib_menu_conferma = true
ib_menu_trasporto = false
end event

type tab_dettaglio from w_tes_bol_ven`tab_dettaglio within w_tes_bol_ven_ptenda
end type

on tab_dettaglio.create
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
end on

type det_1 from w_tes_bol_ven`det_1 within tab_dettaglio
end type

type dw_1 from w_tes_bol_ven`dw_1 within det_1
end type

type det_2 from w_tes_bol_ven`det_2 within tab_dettaglio
end type

type dw_2 from w_tes_bol_ven`dw_2 within det_2
end type

type det_3 from w_tes_bol_ven`det_3 within tab_dettaglio
end type

type dw_3 from w_tes_bol_ven`dw_3 within det_3
end type

type det_4 from w_tes_bol_ven`det_4 within tab_dettaglio
end type

type dw_4 from w_tes_bol_ven`dw_4 within det_4
end type

type tab_ricerca from w_tes_bol_ven`tab_ricerca within w_tes_bol_ven_ptenda
integer height = 2700
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_tes_bol_ven`ricerca within tab_ricerca
integer height = 2576
end type

type dw_ricerca from w_tes_bol_ven`dw_ricerca within ricerca
integer height = 2544
end type

type selezione from w_tes_bol_ven`selezione within tab_ricerca
integer height = 2576
end type

type tv_selezione from w_tes_bol_ven`tv_selezione within selezione
end type

