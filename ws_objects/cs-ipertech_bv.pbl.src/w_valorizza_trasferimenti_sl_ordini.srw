﻿$PBExportHeader$w_valorizza_trasferimenti_sl_ordini.srw
forward
global type w_valorizza_trasferimenti_sl_ordini from w_std_principale
end type
type st_3 from statictext within w_valorizza_trasferimenti_sl_ordini
end type
type st_2 from statictext within w_valorizza_trasferimenti_sl_ordini
end type
type st_1 from statictext within w_valorizza_trasferimenti_sl_ordini
end type
type em_fine from editmask within w_valorizza_trasferimenti_sl_ordini
end type
type em_inizio from editmask within w_valorizza_trasferimenti_sl_ordini
end type
type cb_annulla from commandbutton within w_valorizza_trasferimenti_sl_ordini
end type
type cb_rollback from commandbutton within w_valorizza_trasferimenti_sl_ordini
end type
type cb_valorizza from commandbutton within w_valorizza_trasferimenti_sl_ordini
end type
type st_log from statictext within w_valorizza_trasferimenti_sl_ordini
end type
type cb_excel from commandbutton within w_valorizza_trasferimenti_sl_ordini
end type
type cb_test_valorizza from commandbutton within w_valorizza_trasferimenti_sl_ordini
end type
type dw_1 from uo_std_dw within w_valorizza_trasferimenti_sl_ordini
end type
end forward

global type w_valorizza_trasferimenti_sl_ordini from w_std_principale
integer width = 3913
integer height = 1904
string title = "Valorizzazione Acquisti"
st_3 st_3
st_2 st_2
st_1 st_1
em_fine em_fine
em_inizio em_inizio
cb_annulla cb_annulla
cb_rollback cb_rollback
cb_valorizza cb_valorizza
st_log st_log
cb_excel cb_excel
cb_test_valorizza cb_test_valorizza
dw_1 dw_1
end type
global w_valorizza_trasferimenti_sl_ordini w_valorizza_trasferimenti_sl_ordini

type variables
uo_log luo_log

private:
	string is_desktop
	
	datetime idt_date_limit = datetime(date("01/04/2012"), 00:00:00)
	
	boolean ib_annulla = false
end variables

forward prototypes
public function integer wf_valorizza ()
public subroutine wf_set_caso (long al_row, string as_caso)
public subroutine wf_set_caso (long al_row, string as_caso, decimal ad_prezzo)
public function integer wf_convalida ()
public function integer wf_calcola_prezzo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, ref decimal fd_prezzo, ref string fs_errore)
end prototypes

public function integer wf_valorizza ();/**
 * stefanop
 * 24/07/2012
 *
 * Processo di test del valore di prezzo
 *
 * Casi:
 * 1: data inferiore al limite
 **/
 
string ls_cod_prodotto, ls_error
long ll_rows, ll_i
decimal{4} ld_mov_magazzino_val_movimento, ld_det_bol_ven_prezzo_vendita, ld_prezzo_calcolato
datetime ldt_tes_bol_ven_data_bolla, ldt_limite
uo_prodotti luo_prodotti

ll_rows = dw_1.rowcount()
luo_prodotti = create uo_prodotti

for ll_i = 1 to ll_rows
	
	setmicrohelp("Processate " + string(ll_i) + " di " + string(ll_rows))
	yield()
	
	ldt_tes_bol_ven_data_bolla = dw_1.getitemdatetime(ll_i, "tes_bol_ven_data_bolla")
	ls_cod_prodotto = dw_1.getitemstring(ll_i, "mov_magazzino_cod_prodotto")
	
	// Non processo le bolle con data inferiore al limite impostato
	if ldt_tes_bol_ven_data_bolla < idt_date_limit then 
		wf_set_caso(ll_i, "1")
		continue
	end if
	
	ld_mov_magazzino_val_movimento = dw_1.getitemdecimal(ll_i, "mov_magazzino_val_movimento")
	ld_det_bol_ven_prezzo_vendita = dw_1.getitemdecimal(ll_i, "det_bol_ven_prezzo_vendita")
	
	// CASO 2: il valore del movimento è > 0 e <> dal valore del DDT
	if ld_mov_magazzino_val_movimento > 0 and ld_mov_magazzino_val_movimento <> ld_det_bol_ven_prezzo_vendita then
		wf_set_caso(ll_i, "2", ld_mov_magazzino_val_movimento)
		continue
	
	// CASO 3: il valore del movimento e del DDT sono a 0; prevelo il prezzo dall'ultima fattura
	elseif ld_mov_magazzino_val_movimento = 0 and ld_det_bol_ven_prezzo_vendita = 0 then

		ld_prezzo_calcolato = luo_prodotti.uof_ultimo_prezzo_mov_mag(ls_cod_prodotto, ldt_tes_bol_ven_data_bolla, ls_error)
		
		if ld_prezzo_calcolato > 0 then
			wf_set_caso(ll_i, "3", ld_prezzo_calcolato)
		else
			wf_set_caso(ll_i, "4", ld_prezzo_calcolato)
		end if
	end if

next

return 0
end function

public subroutine wf_set_caso (long al_row, string as_caso);wf_set_caso(al_row, as_caso, 0)
end subroutine

public subroutine wf_set_caso (long al_row, string as_caso, decimal ad_prezzo);dw_1.setitem(al_row, "flag_caso", as_caso)
dw_1.setitem(al_row, "prezzo_calcolato", ad_prezzo)
end subroutine

public function integer wf_convalida ();/**
 * stefanop
 * 24/07/2012
 *
 * Consolida i prezzi nel databsae
 **/
 
long ll_rows, ll_i

if not g_mb.confirm("Salvare i nuovi prezzi? L'operazione non può essere annullata!") then
	return 1
end if


ll_rows = dw_1.rowcount()

for ll_i = 1 to ll_rows
	
next
end function

public function integer wf_calcola_prezzo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, ref decimal fd_prezzo, ref string fs_errore);// *****************************************************************************************************************
// FUNZIONE DI CALCOLO DEL PREZZO DEL PRODOTTO FINITO DEL CONFIGURATORE
// SENZA PASSARE DENTRO LA MASCHERA DEL CONFIGURATORE
//
// 									AUTORE: EnMe 29/1/2002
// ----------------------------------  calcolo del prezzo  --------------------------------------------------- //
boolean lb_test
string ls_valuta_lire, ls_cod_valuta, ls_sql, ls_prezzo_vendita, ls_valore_riga, ls_fat_conversione, ls_cod_tessuto, ls_errore,&
		ls_sconto, ls_provvigione_1, ls_provvigione_2, ls_quantita_um, ls_prezzo_um, ls_null,ls_flag_prezzo_superficie, &
		ls_cod_gruppo_var_quan_distinta,ls_tabella_det_doc, ls_progressivo,ls_cod_tipo_det_ven,ls_cod_versione,&
		ls_flag_uso_quan_distinta, ls_tabella_varianti, ls_messaggio
long ll_i, ll_y
dec{4} ld_variazioni[], ld_gruppi_sconti[], ld_provv_1, ld_provv_2, ld_valore_riga, ld_dimensione_1, ld_dimensione_2, &
		ld_dimensione_3, ld_quan_ordinata, ld_provvigione_1, ld_provvigione_2, ld_prezzo_um, ld_quantita_um,ld_quan_tessuto, &
		ld_dim_x, ld_dim_y, ld_dim_z, ld_prezzo_vendita
dec{5} ld_fat_conversione
uo_gruppi_sconti luo_gruppi_sconti
uo_condizioni_cliente luo_condizioni_cliente



setnull(ls_null)
ls_tabella_det_doc = "det_ord_ven"
ls_progressivo = "prog_riga_ord_ven"
ls_tabella_varianti = "varianti_det_ord_ven"

luo_condizioni_cliente = CREATE uo_condizioni_cliente
s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = fl_anno_registrazione
s_cs_xx.listino_db.num_documento = fl_num_registrazione
s_cs_xx.listino_db.prog_riga = fl_prog_riga_ord_ven
//s_cs_xx.listino_db.tipo_gestione = "ORD_VEN"
s_cs_xx.listino_db.tipo_gestione = "varianti_det_ord_ven"

select cod_versione,
		quan_ordine
into   :s_cs_xx.listino_db.cod_versione,
		:s_cs_xx.listino_db.quan_prodotto_finito
from   det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;

select cod_cliente, 
		cod_valuta, 
		cod_tipo_listino_prodotto, 
		data_registrazione,
		cod_agente_1
into  :s_cs_xx.listino_db.cod_cliente, 
      :s_cs_xx.listino_db.cod_valuta, 
		:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
		:s_cs_xx.listino_db.data_riferimento,
		:luo_condizioni_cliente.str_parametri.cod_agente_1
from  tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :fl_anno_registrazione and
      num_registrazione = :fl_num_registrazione ;

select dim_x,
		dim_y,
		dim_z
into   	:ld_dim_x,
		:ld_dim_y,
		:ld_dim_z
from   comp_det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;
		

luo_condizioni_cliente.ib_setitem=false
luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
luo_condizioni_cliente.str_parametri.cod_prodotto = fs_cod_prodotto
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
luo_condizioni_cliente.str_parametri.cambio_ven = 1
luo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
luo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
luo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_null
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

//for ll_i = 1 to 10
//	if str_conf_prodotto.stato = "N" and not str_conf_prodotto.flag_cambio_modello then
//										//            ^^ aggiunto per spec_varie_5; funzione 14
//		if upperbound(luo_condizioni_cliente.str_output.sconti) >= ll_i then
//			istr_riepilogo.sconti[ll_i] = luo_condizioni_cliente.str_output.sconti[ll_i]
//		else
//			istr_riepilogo.sconti[ll_i] = 0
//		end if
//	else
//		istr_riepilogo.sconti[ll_i] = str_conf_prodotto.sconti[ll_i]
//	end if
//next

//if str_conf_prodotto.stato = "N" then
//	ld_provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
//	ld_provvigione_2 = luo_condizioni_cliente.str_output.provvigione_2
//else
//	ld_provvigione_1 = str_conf_prodotto.provvigione_1
//	ld_provvigione_2 = str_conf_prodotto.provvigione_2
//end if
//if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
//if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
if upperbound(luo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(luo_condizioni_cliente.str_output.variazioni)) then
	ld_prezzo_vendita = 0
	ld_fat_conversione = 1
else
	ld_fat_conversione = 1
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
end if

//	 calcolo del valore netto riga //
select cod_tipo_det_ven_prodotto,
		flag_prezzo_superficie,
		flag_uso_quan_distinta,
		cod_gruppo_var_quan_distinta
into   :ls_cod_tipo_det_ven,
		:ls_flag_prezzo_superficie,
		:ls_flag_uso_quan_distinta,
		:ls_cod_gruppo_var_quan_distinta
from   tab_flags_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipi dettaglio nella tabella parametri configuratore. Dettaglio errore "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_cod_tipo_det_ven) then
	fs_errore = "Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella flags configuratore"
	return -1
end if


// ----------------------- gestione del prezzo per metro quadarato con il fattore di conversione ------------------------

if ls_flag_prezzo_superficie = "S" then
	if ld_dim_x > 0 then
		if luo_condizioni_cliente.str_output.min_fat_larghezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_larghezza) and ld_dim_x < luo_condizioni_cliente.str_output.min_fat_larghezza then
			ld_dimensione_1 = luo_condizioni_cliente.str_output.min_fat_larghezza
		else
			ld_dimensione_1 = ld_dim_x
		end if
	end if
	if ld_dim_y > 0 then
		if luo_condizioni_cliente.str_output.min_fat_altezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_altezza) and ld_dim_y < luo_condizioni_cliente.str_output.min_fat_altezza then
			ld_dimensione_2 = luo_condizioni_cliente.str_output.min_fat_altezza
		else
			ld_dimensione_2 = ld_dim_y
		end if
	end if
	if ld_dim_z > 0 then
		if luo_condizioni_cliente.str_output.min_fat_profondita > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_profondita) and ld_dim_z < luo_condizioni_cliente.str_output.min_fat_profondita then
			ld_dimensione_3 = luo_condizioni_cliente.str_output.min_fat_profondita
		else
			ld_dimensione_3 = ld_dim_x
		end if
	end if
	
	// --------------------- nuova gestione vuoto per pieno -------------------------
	//                         nicola ferrari 23/05/2000
	if ls_flag_uso_quan_distinta = "N" then
		ld_fat_conversione = round((ld_dimensione_1 * ld_dimensione_2) / 10000,2)
		if ld_fat_conversione <= 0 or isnull(ld_fat_conversione) then
			fs_errore = "Attenzione la quantità risultante dal calcolo DIM1 x DIM2 è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_fat_conversione = 1
		end if
	else
		uo_conf_varianti uo_ins_varianti
		uo_ins_varianti = CREATE uo_conf_varianti
		if uo_ins_varianti.uof_ricerca_quan_mp(fs_cod_prodotto,  &
														s_cs_xx.listino_db.cod_versione, &
														s_cs_xx.listino_db.quan_prodotto_finito, &
														ls_cod_gruppo_var_quan_distinta, &
														fl_anno_registrazione, &
														fl_num_registrazione, &
														fl_prog_riga_ord_ven, &
														ls_tabella_varianti, &
														ls_cod_tessuto, &
														ld_quan_tessuto,&
														ls_errore) = -1 then
			fs_errore = "Errore nella ricerca quantità tessuto in disntita base. Dettaglio errore: " + ls_errore
			rollback;
			return -1
		end if

		if ld_quan_tessuto <= 0 or isnull(ld_quan_tessuto) then
			fs_errore = "Attenzione la quantità trovata nel gruppo variante " + ls_cod_gruppo_var_quan_distinta + " è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_quan_tessuto = 1
		end if
		destroy uo_ins_varianti
		ld_fat_conversione = round(ld_quan_tessuto,2)
	end if

	// ---------------------- fine vuoto per pieno ----------------------------------
	// controllo il minimale di superficie
	if luo_condizioni_cliente.str_output.min_fat_superficie > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_superficie) and ld_fat_conversione < luo_condizioni_cliente.str_output.min_fat_superficie then
		ld_fat_conversione = luo_condizioni_cliente.str_output.min_fat_superficie
	end if
	ld_quantita_um = s_cs_xx.listino_db.quan_prodotto_finito * ld_fat_conversione
	ld_prezzo_um = ld_prezzo_vendita
	ld_prezzo_vendita = ld_prezzo_vendita * ld_fat_conversione
end if	

if luo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, str_conf_prodotto.cod_valuta, ld_prezzo_vendita, ls_messaggio) = -1 then
	fs_errore = "Errore durante l'arrotondamento del prezzo di vendita: " + ls_messaggio
	rollback;
	return -1
end if

fd_prezzo = ld_prezzo_vendita


rollback;

return 0

end function

on w_valorizza_trasferimenti_sl_ordini.create
int iCurrent
call super::create
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_fine=create em_fine
this.em_inizio=create em_inizio
this.cb_annulla=create cb_annulla
this.cb_rollback=create cb_rollback
this.cb_valorizza=create cb_valorizza
this.st_log=create st_log
this.cb_excel=create cb_excel
this.cb_test_valorizza=create cb_test_valorizza
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.em_fine
this.Control[iCurrent+5]=this.em_inizio
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.cb_rollback
this.Control[iCurrent+8]=this.cb_valorizza
this.Control[iCurrent+9]=this.st_log
this.Control[iCurrent+10]=this.cb_excel
this.Control[iCurrent+11]=this.cb_test_valorizza
this.Control[iCurrent+12]=this.dw_1
end on

on w_valorizza_trasferimenti_sl_ordini.destroy
call super::destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_fine)
destroy(this.em_inizio)
destroy(this.cb_annulla)
destroy(this.cb_rollback)
destroy(this.cb_valorizza)
destroy(this.st_log)
destroy(this.cb_excel)
destroy(this.cb_test_valorizza)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_1.settransobject(sqlca)

is_desktop = guo_functions.uof_get_user_desktop_folder()

luo_log = create uo_log

luo_log.open( "c:\temp\log_val_trasf_sl_ordini.txt")

end event

event resize;call super::resize;
// PULSANTI
cb_valorizza.move(newwidth - cb_valorizza.width - 20, newheight - cb_valorizza.height - 20)
cb_excel.move(cb_valorizza.x - cb_excel.width - 20, cb_valorizza.y)
cb_test_valorizza.move(cb_excel.x - cb_test_valorizza.width - 20, cb_valorizza.y)
cb_annulla.move(cb_excel.x - cb_test_valorizza.width - cb_annulla.width - 20, cb_valorizza.y)
cb_rollback.move(cb_excel.x - cb_test_valorizza.width - cb_annulla.width - cb_valorizza.width - cb_rollback.width - 20, cb_valorizza.y)

st_log.move(20, cb_valorizza.y + 10)
st_log.width = cb_test_valorizza.x - 100


dw_1.move(20,20)
dw_1.resize(newwidth - 40, cb_test_valorizza.y - 100)

end event

type st_3 from statictext within w_valorizza_trasferimenti_sl_ordini
integer x = 1760
integer y = 1580
integer width = 2080
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Attenzione il prezzo RICALCOLATO è di listino; gli sconti vengono messi dopo"
boolean focusrectangle = false
end type

type st_2 from statictext within w_valorizza_trasferimenti_sl_ordini
integer x = 754
integer y = 1600
integer width = 274
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Data Fine:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_valorizza_trasferimenti_sl_ordini
integer y = 1600
integer width = 343
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Data Inizio:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_fine from editmask within w_valorizza_trasferimenti_sl_ordini
integer x = 1051
integer y = 1600
integer width = 343
integer height = 60
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

event modified;datetime ldt_data_inizio, ldt_data_fine


ldt_data_inizio = datetime(date(em_inizio.text), 00:00:00)
ldt_data_fine =datetime(date(this.text), 00:00:00)

if not isnull(ldt_data_inizio) and not isnull(ldt_data_fine) then

	dw_1.retrieve(ldt_data_inizio, ldt_data_fine)
	
end if
end event

type em_inizio from editmask within w_valorizza_trasferimenti_sl_ordini
integer x = 366
integer y = 1600
integer width = 343
integer height = 60
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

event modified;datetime ldt_data_inizio, ldt_data_fine


ldt_data_inizio = datetime(date(this.text), 00:00:00)
ldt_data_fine =datetime(date(em_fine.text), 00:00:00)

if not isnull(ldt_data_inizio) and not isnull(ldt_data_fine) then

	dw_1.retrieve(ldt_data_inizio, ldt_data_fine)
	
end if
end event

type cb_annulla from commandbutton within w_valorizza_trasferimenti_sl_ordini
integer x = 2583
integer y = 1660
integer width = 425
integer height = 100
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ferma Elab."
end type

event clicked;ib_annulla=true
end event

type cb_rollback from commandbutton within w_valorizza_trasferimenti_sl_ordini
integer x = 1760
integer y = 1660
integer width = 389
integer height = 100
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Rollback"
end type

event clicked;rollback;
end event

type cb_valorizza from commandbutton within w_valorizza_trasferimenti_sl_ordini
integer x = 2149
integer y = 1660
integer width = 425
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "2 - Consolida"
end type

event clicked;string ls_messaggio
long ll_i, ll_anno_bolla, ll_num_bolla, ll_prog_riga_bol_ven, ll_anno_bolla_prec, ll_num_bolla_prec,ll_anno_reg_mov_mag,ll_num_reg_mov_mag
dec{4} ld_prezzo, ld_imponibile_iva, ld_val_movimento, ld_quan_consegnata, ld_sbt, ld_prezzo_scontato, ld_sconto_1
uo_calcola_documento_euro luo_calcola_documento_euro

guo_functions.uof_get_parametro_azienda(  "SBT", ld_sbt)
if isnull(ld_sbt) or ld_sbt = 0 then
	if not(g_mb.confirm("Parametro SBT SCONTO BOLLE TRASFERIMENTO è ZERO: Proseguo?") ) then return
end if

for ll_i = 1 to dw_1.rowcount()	
	ld_prezzo = dw_1.getitemnumber(ll_i, "prezzo_ricalcolato")
	
	ll_anno_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_anno_registrazione")
	ll_num_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_num_registrazione")
	ll_prog_riga_bol_ven = dw_1.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")

	if ld_prezzo > 0 and not isnull(ld_prezzo) then

		ld_sconto_1 = dw_1.getitemnumber(ll_i, "det_ord_ven_sconto_1")
		
		ld_prezzo_scontato = ld_prezzo - (ld_prezzo / 100 *  ld_sconto_1)
		ld_prezzo_scontato = ld_prezzo_scontato - ( ld_prezzo_scontato / 100 * ld_sbt )
		ld_prezzo_scontato = round(ld_prezzo_scontato, 2)

		update det_bol_ven
		set prezzo_vendita = :ld_prezzo_scontato,
			 sconto_1 = 0,
			 sconto_2 = 0,
			 sconto_3 = 0,
			 sconto_4 = 0,
			 sconto_5 = 0,
			 sconto_6 = 0,
			 sconto_7 = 0,
			 sconto_8 = 0,
			 sconto_9 = 0,
			 sconto_10 = 0,
			 provvigione_1 = 0,
			 provvigione_2 = 0
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_bolla and
				num_registrazione = :ll_num_bolla and
				prog_riga_bol_ven = :ll_prog_riga_bol_ven ;
		if sqlca.sqlcode < 0 then
			g_mb.error(g_str.format("Errore in consolidamento nella riga bolla $1 / $2 / $3",  ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven) )
			rollback;
			return
		end if
		
		st_log.text = g_str.format("Consolidata riga bolla: $1 / $2 / $3 prezzo vecchio=$4  prezzo nuovo=$5",  ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven,ld_prezzo,ld_prezzo_scontato)
		luo_log.log( g_str.format( "Consolidata riga bolla $1 / $2 / $3 prezzo vecchio=$4  prezzo nuovo=$5", ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven,ld_prezzo,ld_prezzo_scontato) )
		
	else
		luo_log.log( g_str.format( "Salto Consolidamento riga bolla $1 / $2 / $3 per prezzo <= 0", ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven) )
	end if
			
next

commit;

// ---------------------------------------------
// ora eseguo il ricalcolo del documento
// ---------------------------------------------
dw_1.setsort( "tes_bol_ven_anno_registrazione, tes_bol_ven_num_registrazione, det_bol_ven_prog_riga_bol_ven")
dw_1.sort()

ll_anno_bolla_prec = 0
ll_num_bolla_prec = 0
luo_calcola_documento_euro = create uo_calcola_documento_euro

for ll_i = 1 to dw_1.rowcount()
	
	ll_anno_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_anno_registrazione")
	ll_num_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_num_registrazione")
	
	if ll_anno_bolla_prec <> ll_anno_bolla or ll_num_bolla_prec <> ll_num_bolla then
		st_log.text = g_str.format("Calcolo bolla: $1 / $2",  ll_anno_bolla, ll_num_bolla) 
		luo_log.log( g_str.format( "Ricalcolo bolla $1 / $2 / $3 per prezzo <= 0", ll_anno_bolla, ll_num_bolla) )
		//a true per non fare di nuovo il controllo sull'esposizione cliente
		luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
		//-----------------	
		if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_bolla,ll_num_bolla,"bol_ven",ls_messaggio) <> 0 then
			//Donato 05-11-2008 dare msg solo se c'è
			if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
			
			rollback;
		else
			commit;
		end if	
		ll_anno_bolla_prec = ll_anno_bolla
		ll_num_bolla_prec = ll_num_bolla
	end if
next

destroy luo_calcola_documento_euro
	
commit;

// ---------------------------------------------
// aggiorno valore del movimento di magazzino
for ll_i = 1 to dw_1.rowcount()
	
	ll_anno_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_anno_registrazione")
	ll_num_bolla = dw_1.getitemnumber(ll_i, "tes_bol_ven_num_registrazione")
	ll_prog_riga_bol_ven = dw_1.getitemnumber(ll_i, "det_bol_ven_prog_riga_bol_ven")

	st_log.text = g_str.format("Aggiornamento Movimento Magazzino: $1 / $2 / $3",  ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven) 
	
	select anno_registrazione_mov_mag,
			num_registrazione_mov_mag,
			imponibile_iva,
			quan_consegnata
	into 	:ll_anno_reg_mov_mag,
			:ll_num_reg_mov_mag,
			:ld_imponibile_iva,
			:ld_quan_consegnata
	from	det_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_bolla and
			num_registrazione = :ll_num_bolla and
			prog_riga_bol_ven = :ll_prog_riga_bol_ven;
	if sqlca.sqlcode = 0 and not isnull(ll_anno_reg_mov_mag) and ll_anno_reg_mov_mag > 0 and ld_quan_consegnata <> 0 then
		
		ld_val_movimento = round(ld_imponibile_iva / ld_quan_consegnata, 4)
		
		update mov_magazzino
		set val_movimento = :ld_val_movimento
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_mov_mag and
				num_registrazione = :ll_num_reg_mov_mag;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in aggiornamento movimenti di magazzino")
			rollback;
			return
		end if
		
		luo_log.log( g_str.format("Movimento $4/$5 - Riga Bolla $1/$2/$3 - Aggiornato con valore $6", ll_anno_bolla, ll_num_bolla,ll_prog_riga_bol_ven,ll_anno_reg_mov_mag,ll_num_reg_mov_mag,ld_val_movimento))
		
	end if
	
	// uno per uno,   altrimenti fa troppi lock
	commit;
	
next


end event

type st_log from statictext within w_valorizza_trasferimenti_sl_ordini
integer x = 23
integer y = 1700
integer width = 1669
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_excel from commandbutton within w_valorizza_trasferimenti_sl_ordini
integer x = 3451
integer y = 1660
integer width = 402
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Excel"
end type

event clicked;/**
 * stefanop
 * 24/07/2012
 *
 * Esporto la DW in excel
 **/
 
string ls_path, ls_file
int li_rc
 
if g_mb.confirm("Esportare in Excel la lista?") then
	
	if GetFileSaveName ( "Salva", ls_path, ls_file, "Excel", "Excel (*.xls),*.xls" , is_desktop) = 1 then
		
		dw_1.SaveAs(ls_path, Excel8!, true)
		
	end if
end if
end event

type cb_test_valorizza from commandbutton within w_valorizza_trasferimenti_sl_ordini
integer x = 3017
integer y = 1660
integer width = 425
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 - Valorizza"
end type

event clicked;boolean lb_apri_configuratore
string ls_flag_tessuto, ls_flag_mantovana, ls_flag_altezza_mantovana,  ls_flag_tessuto_mantovana, ls_flag_cordolo, ls_flag_passamaneria, &
          ls_flag_verniciatura, ls_flag_comando, ls_flag_pos_comando, ls_flag_alt_asta, ls_flag_supporto, ls_flag_luce_finita,   &
		 ld_limite_max_dim_x, ld_limite_max_dim_y, ld_limite_max_dim_z, ld_limite_max_dim_t, ld_limite_min_dim_x, ld_limite_min_dim_y,   &
		 ld_limite_min_dim_z, ld_limite_min_dim_t, ls_flag_secondo_comando, ls_flag_tessuto_1, ls_flag_mantovana_1, ls_flag_altezza_mantovana_1, &
		 ls_flag_tessuto_mantovana_1, ls_flag_cordolo_1, ls_flag_passamaneria_1, ls_flag_verniciatura_1, ls_flag_comando_1, ls_flag_pos_comando_1,&
		 ls_flag_alt_asta_1, ls_flag_supporto_1, ls_flag_luce_finita_1, ld_limite_max_dim_x_1, ld_limite_max_dim_y_1, ld_limite_max_dim_z_1, &
		 ld_limite_max_dim_t_1, ld_limite_min_dim_x_1, ld_limite_min_dim_y_1, ld_limite_min_dim_z_1, ld_limite_min_dim_t_1, &
		 ls_flag_secondo_comando_1, ls_cod_modello, ls_cod_prodotto_origine, ld_des_dim_x,ld_des_dim_y,ld_des_dim_z,ld_des_dim_t, &
		 ls_messaggio_modifica, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto_finito, ls_messaggio,ls_cod_tipo_ord_ven, &
		 ls_des_prodotto_mag, ls_mp_escluse[], ls_testo_esclusioni, ls_cod_prodotto_storico,ls_cod_prodotto_raggruppato, ls_null, &
		 ls_materia_prima[], ls_errore, ls_cod_versione, ls_versione_mat_prima[]

integer li_anno_commessa,li_risposta

long   ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,ll_num_commessa, ll_null, ll_righe_escluse[], ll_ret, ll_riga

double ld_quantita_utilizzo[]

dec{4} ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t, ld_quan_ordine, ld_quan_raggruppo,ld_prezzo_vendita

datetime ldt_adesso

uo_funzioni_1 luo_funzioni_1

uo_storicizzazione luo_storicizzazione

uo_gestione_conversioni luo_gestione_conversioni

setnull(ls_null)


//ll_riga = dw_1.getrow()

for ll_riga = 1 to dw_1.rowcount()
	
	if ib_annulla then
		ib_annulla=false
		rollback;
		return
	end if

	ll_anno_registrazione = dw_1.getitemnumber(ll_riga,"tes_ord_ven_anno_registrazione")
	ll_num_registrazione = dw_1.getitemnumber(ll_riga,"tes_ord_ven_num_registrazione")
	
	ll_prog_riga_ord_ven = dw_1.getitemnumber(ll_riga,"det_ord_ven_prog_riga_ord_ven")
	
	st_log.text = g_str.format("Elaborazione riga $1/$2 ordine $3 / $4 / $5 ", ll_riga,dw_1.rowcount(),ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven )
	dw_1.scrolltorow(ll_riga)
	Yield()
	
	ld_dim_x = dw_1.getitemnumber(ll_riga, "comp_det_ord_ven_dim_x")
	ld_dim_y = dw_1.getitemnumber(ll_riga, "comp_det_ord_ven_dim_y")
	ld_dim_z = dw_1.getitemnumber(ll_riga, "comp_det_ord_ven_dim_z")
	ld_dim_t = dw_1.getitemnumber(ll_riga, "comp_det_ord_ven_dim_t")
	
	ls_cod_modello = dw_1.getitemstring(ll_riga, "comp_det_ord_ven_cod_modello")
	ls_cod_prodotto_origine = dw_1.getitemstring(ll_riga, "det_ord_ven_cod_prodotto")
	ls_cod_tipo_det_ven = dw_1.getitemstring(ll_riga,"det_ord_ven_cod_tipo_det_ven")
	ld_quan_ordine = dw_1.getitemnumber(ll_riga,"det_ord_ven_quan_ordine")
	
	// trovo il codice prodotto di cui eseguire la storicizzazione
	ls_cod_versione = dw_1.getitemstring(ll_riga, "det_ord_ven_cod_versione")
	ls_cod_prodotto_storico = dw_1.getitemstring(ll_riga, "det_bol_ven_cod_prodotto")
	ls_cod_modello = ls_cod_prodotto_storico
	// trovo le materie prime
	
	if pos(ls_cod_prodotto_storico,ls_cod_prodotto_origine,1) = 0  then
		dw_1.setitem(ll_riga, "prezzo_ricalcolato", -1 )
		dw_1.setitem(ll_riga, "commento", "Prodotto ordine diverso da prodotto trasferito" )
		rollback;
		continue
	end if
	
	luo_funzioni_1 = create uo_funzioni_1 
	
	luo_funzioni_1.uof_trova_mat_prime_varianti (	ls_cod_prodotto_storico, &
																		ls_cod_versione, &
																		ls_null, &
																		ref ls_materia_prima[], &
																		ref ls_versione_mat_prima[], &
																		ref ld_quantita_utilizzo[], &
																		ld_quan_ordine, &
																		dw_1.getitemnumber(ll_riga, "tes_ord_ven_anno_registrazione"), &
																		dw_1.getitemnumber(ll_riga, "tes_ord_ven_num_registrazione"), &
																		dw_1.getitemnumber(ll_riga, "det_ord_ven_prog_riga_ord_ven"), &
																		"varianti_det_ord_ven", &
																		ls_null, &
																		ref ls_errore )
																		 
	destroy luo_funzioni_1
	
	luo_storicizzazione = create uo_storicizzazione
	
	// imposto alcuni parametri di istanza per passare i dati necessari
	luo_storicizzazione.is_flag_ar = "S"
	luo_storicizzazione.is_cod_prodotto_origine = ls_cod_prodotto_origine
	luo_storicizzazione.is_cod_prodotto_ar      = ls_cod_prodotto_storico
	luo_storicizzazione.is_cod_materia_prima[] = ls_materia_prima[]
	luo_storicizzazione.id_quan_utilizzo[] = ld_quantita_utilizzo[]
	luo_storicizzazione.id_valore_storico = s_cs_xx.parametri.parametro_d_1
	
	ll_ret = luo_storicizzazione.uof_storicizza(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ls_testo_esclusioni, ls_messaggio )
	
	if ll_ret = 1 then
		dw_1.setitem(ll_riga, "prezzo_ricalcolato", -1 )
		dw_1.setitem(ll_riga, "commento", "Errore in trasf. AR~r~n" + ls_messaggio + "~r~n" + sqlca.sqlerrtext )
		rollback;
		continue
	end if
	
	SELECT flag_tessuto,   
			flag_tipo_mantovana,   
			flag_altezza_mantovana,   
			flag_tessuto_mantovana,   
			flag_cordolo,   
			flag_passamaneria,   
			flag_verniciatura,   
			flag_comando,   
			flag_pos_comando,   
			flag_alt_asta,   
			flag_supporto,   
			flag_luce_finita,   
			limite_max_dim_x,   
			limite_max_dim_y,   
			limite_max_dim_z,   
			limite_max_dim_t,   
			limite_min_dim_x,   
			limite_min_dim_y,   
			limite_min_dim_z,   
			limite_min_dim_t,   
			flag_secondo_comando  
	 INTO :ls_flag_tessuto,   
			:ls_flag_mantovana,   
			:ls_flag_altezza_mantovana,   
			:ls_flag_tessuto_mantovana,   
			:ls_flag_cordolo,   
			:ls_flag_passamaneria,   
			:ls_flag_verniciatura,   
			:ls_flag_comando,   
			:ls_flag_pos_comando,   
			:ls_flag_alt_asta,   
			:ls_flag_supporto,   
			:ls_flag_luce_finita,   
			:ld_limite_max_dim_x,   
			:ld_limite_max_dim_y,   
			:ld_limite_max_dim_z,   
			:ld_limite_max_dim_t,   
			:ld_limite_min_dim_x,   
			:ld_limite_min_dim_y,   
			:ld_limite_min_dim_z,   
			:ld_limite_min_dim_t,   
			:ls_flag_secondo_comando  
	 FROM tab_flags_configuratore
	 WHERE cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :ls_cod_modello;
	if sqlca.sqlcode <> 0 then
		dw_1.setitem(ll_riga, "prezzo_ricalcolato", -1 )
		dw_1.setitem(ll_riga, "commento", "Mancano i parametri nella tabella flag_configuratore: riga saltata" )
		rollback;
		continue
	end if
	
	// *********************  pulisco i passi che adesso non sono più validi ****************************************
	
	if ls_flag_luce_finita = "N" or isnull(ls_flag_luce_finita) then
		update comp_det_ord_ven
		set    flag_misura_luce_finita = 'L'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_tessuto = "N" or isnull(ls_flag_tessuto) then
		update comp_det_ord_ven
		set    cod_tessuto = null, cod_non_a_magazzino = null, flag_fc_tessuto = 'N', flag_addizionale_tessuto = 'N'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_tessuto_mantovana = "N" or isnull(ls_flag_tessuto_mantovana) then
		update comp_det_ord_ven
		set    cod_tessuto_mant = null, cod_mant_non_a_magazzino = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_altezza_mantovana = "N" or isnull(ls_flag_altezza_mantovana) then
		update comp_det_ord_ven
		set    alt_mantovana = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_mantovana = "N" or isnull(ls_flag_mantovana) then
		update comp_det_ord_ven
		set    flag_tipo_mantovana = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_cordolo = "N" or isnull(ls_flag_cordolo) then
		update comp_det_ord_ven
		set    flag_cordolo = 'N', colore_cordolo = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_passamaneria = "N" or isnull(ls_flag_passamaneria) then
		update comp_det_ord_ven
		set    flag_passamaneria = 'N', colore_passamaneria = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_verniciatura = "N" or isnull(ls_flag_verniciatura) then
		update comp_det_ord_ven
		set    cod_verniciatura = null, flag_fc_vernic = 'N', des_vernice_fc = null, flag_addizionale_vernic = 'N'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_comando = "N" or isnull(ls_flag_comando) then
		update comp_det_ord_ven
		set    cod_comando = null, flag_addizionale_comando_1 ='N', flag_fc_comando_1 = 'N', des_comando_1 = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_pos_comando = "N" or isnull(ls_flag_pos_comando) then
		update comp_det_ord_ven
		set    pos_comando = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_alt_asta = "N" or isnull(ls_flag_alt_asta) then
		update comp_det_ord_ven
		set    alt_asta = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_secondo_comando = "N" or isnull(ls_flag_secondo_comando) then
		update comp_det_ord_ven
		set    cod_comando_2 = null, flag_addizionale_comando_2 ='N', flag_fc_comando_2 = 'N', des_comando_2 = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	if ls_flag_supporto = "N" or isnull(ls_flag_supporto) then
		update comp_det_ord_ven
		set    cod_tipo_supporto = null, flag_addizionale_supporto = 'N', flag_fc_supporto ='N'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
	
	str_conf_prodotto.flag_cambio_modello = false
	setnull(str_conf_prodotto.messaggio_modifiche_modello)
	
	select des_prodotto
	into   :ls_des_prodotto_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_modello;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante ricerca della descrizione del prodotto "+ls_cod_modello+".~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	update det_ord_ven
	set cod_prodotto = :ls_cod_modello,
		 des_prodotto = :ls_des_prodotto_mag
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven= :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante cambio prodotto nella riga di dettaglio.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	ll_ret = wf_calcola_prezzo(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,  ls_cod_prodotto_storico, ld_prezzo_vendita,ls_messaggio)
	
	if ll_ret = -1 then
		dw_1.setitem(ll_riga, "prezzo_ricalcolato", -1 )
		dw_1.setitem(ll_riga, "commento", "Errore in calcolo prezzo  " + ls_messaggio)
		rollback;
		continue
	end if
	
	luo_log.log( g_str.format( "Valorizzata riga bolla $1 / $2 / $3", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven) )
	
	dw_1.setitem(ll_riga, "prezzo_ricalcolato", ld_prezzo_vendita)
	
	rollback;
next

rollback;

end event

type dw_1 from uo_std_dw within w_valorizza_trasferimenti_sl_ordini
integer x = 23
integer y = 20
integer width = 3817
integer height = 1540
integer taborder = 10
string dataobject = "d_valorizza_sl_trasferimenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_colora = false
end type

