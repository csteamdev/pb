﻿$PBExportHeader$w_configuratore.srw
$PBExportComments$Finestra Configuratore di Prodotto
forward
global type w_configuratore from w_cs_xx_risposta
end type
type dw_trasferimenti from datawindow within w_configuratore
end type
type dw_help from datawindow within w_configuratore
end type
type dw_steps from datawindow within w_configuratore
end type
type p_1 from picture within w_configuratore
end type
type cbx_apri_dopo from checkbox within w_configuratore
end type
type cb_annulla from commandbutton within w_configuratore
end type
type cb_indietro from commandbutton within w_configuratore
end type
type cb_avanti from commandbutton within w_configuratore
end type
type cb_fine from commandbutton within w_configuratore
end type
type st_cod_prodotto_finito from statictext within w_configuratore
end type
type cb_note_esterne from cb_documenti_compilati within w_configuratore
end type
type cb_stampa from commandbutton within w_configuratore
end type
type st_prezzo from statictext within w_configuratore
end type
type st_2 from statictext within w_configuratore
end type
type st_3 from statictext within w_configuratore
end type
type st_4 from statictext within w_configuratore
end type
type st_sconti from statictext within w_configuratore
end type
type dw_passo_6 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_5 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_4 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_3 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_2 from uo_cs_xx_dw within w_configuratore
end type
type dw_fine from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_8 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_7 from uo_cs_xx_dw within w_configuratore
end type
type dw_passo_1 from uo_cs_xx_dw within w_configuratore
end type
type dw_inizio from uo_cs_xx_dw within w_configuratore
end type
type dw_nota_prodotto_finito from uo_cs_xx_dw within w_configuratore
end type
type dw_nota_prodotto_finito_lista from datawindow within w_configuratore
end type
type ole_1 from olecontrol within w_configuratore
end type
end forward

global type w_configuratore from w_cs_xx_risposta
integer width = 5870
integer height = 2340
string title = "CONFIGURATORE DI PRODOTTO"
boolean controlmenu = false
boolean resizable = false
windowtype windowtype = child!
windowstate windowstate = maximized!
event ue_chiudi_configuratore ( )
event ue_set_focus_inizio ( )
dw_trasferimenti dw_trasferimenti
dw_help dw_help
dw_steps dw_steps
p_1 p_1
cbx_apri_dopo cbx_apri_dopo
cb_annulla cb_annulla
cb_indietro cb_indietro
cb_avanti cb_avanti
cb_fine cb_fine
st_cod_prodotto_finito st_cod_prodotto_finito
cb_note_esterne cb_note_esterne
cb_stampa cb_stampa
st_prezzo st_prezzo
st_2 st_2
st_3 st_3
st_4 st_4
st_sconti st_sconti
dw_passo_6 dw_passo_6
dw_passo_5 dw_passo_5
dw_passo_4 dw_passo_4
dw_passo_3 dw_passo_3
dw_passo_2 dw_passo_2
dw_fine dw_fine
dw_passo_8 dw_passo_8
dw_passo_7 dw_passo_7
dw_passo_1 dw_passo_1
dw_inizio dw_inizio
dw_nota_prodotto_finito dw_nota_prodotto_finito
dw_nota_prodotto_finito_lista dw_nota_prodotto_finito_lista
ole_1 ole_1
end type
global w_configuratore w_configuratore

type variables
boolean					ib_nuovo_prima_volta = false	
string						is_linea_prodotto, is_classe_merceolo, is_mini_help[8],is_nome_passo_optional
integer					ii_num_passo, ii_num_passo_old, ii_quantita_passi, ii_passo_richiesto
wstr_riepilogo			istr_riepilogo
wstr_flags				istr_flags
uo_condizioni_cliente iuo_condizioni_cliente				
w_cs_xx_principale 	iw_parent
datetime					idt_data_partenza, idt_date_reparti[]
string						is_reparti[]
datastore				ids_backup_vardetordven_prod, ids_backup_vardetordven
boolean					ib_backup_vardetordven_prod = false
string						is_logostandard = "<LOGO STANDARD>", is_path_cliente, is_logo_etichette, is_path_logo_standard
end variables

forward prototypes
public function integer wf_crea_varianti_esclusione (string fs_cod_prodotto, string fs_cod_versione)
public function double wf_verifica_rapp_misure (double fd_dim_1, double fd_dim_2, ref string fs_messaggio)
public function integer wf_calcoli_plisse (double fd_larghezza_finita, double fd_altezza_finita, string fs_flag_autoblocco, string fs_flag_misura_luce_finita, string fs_cod_tessuto, string fs_cod_non_a_magazzino, ref string fs_messaggio)
public function integer wf_init_cod_prodotto (ref string fs_messaggio)
public function integer wf_rowfocuschanged (datawindow fdw_passo, long fl_currentrow, ref string fs_messaggio)
public function integer wf_crea_varianti ()
public function integer wf_crea_add_mp_non_ric ()
public subroutine wf_set_default (ref datawindow fdw_data_window)
public subroutine wf_set_dddw (ref datawindow fdw_data_window)
public subroutine wf_spostamento (string fs_tipo_spostamento)
public function integer wf_item_changed (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer wf_crea_addizionali_passo (datawindow fdw_passo)
public function integer wf_memorizza_dati ()
public subroutine wf_visualizza_dw ()
public function integer wf_leggi_flag_configuratore (string fs_tabella_comp, string fs_colonna_prog_riga, ref string errore)
public function integer wf_pcd_new_dw (datawindow fdw_passo)
public subroutine wf_azzera_variabili ()
public function integer wf_clicked (ref datawindow fdw_passo, string fs_oggetto, ref string fs_messaggio)
public function integer wf_reset_tessuti (datawindow fdw_passo, string fs_tessuto_mantovana)
public function double wf_calcola_totale ()
public function integer wf_gest_mp_non_richieste (decimal fd_dim_1, decimal fd_dim_2, decimal fd_dim_3, decimal fd_dim_4, decimal fd_altezza_comando)
public subroutine wf_costruisci_passi_new ()
public function integer wf_costruisci_passo_nota (ref uo_cs_xx_dw dw_passo)
public function integer wf_peso_netto (integer ai_anno_documento, long ai_numero_documento, long ai_riga_documento, ref string as_messaggio)
public function integer wf_aggiorna_varianti_e_calendario (long al_singola_riga, ref string as_errore)
public function integer wf_backup_var_det_ord_ven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer wf_imposta_varianti_prod_da_backup ()
public subroutine wf_ripristina_proforma (datastore ads_fattura_proforma)
public function integer wf_imposta_dddw_loghi (ref string as_errore)
public subroutine wf_preview_logo (string as_file)
public function integer wf_controlla_formule_dimensioni (ref string as_errore)
public function boolean wf_abilita_dimensioni (string fs_cod_prodotto_modello, integer ai_num_dimensione)
end prototypes

event ue_chiudi_configuratore();close (this)
end event

event ue_set_focus_inizio();dw_inizio.change_dw_current()
dw_inizio.setfocus()
end event

public function integer wf_crea_varianti_esclusione (string fs_cod_prodotto, string fs_cod_versione);/*	Funzione che scorre la distinta base e inserisce la variante con flag_esclusione = 'N' 
	se non esistono altre varianti per quella materia prima (non per i semilavorati)

 nome: wf_crea_varianti_esclusione
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 fs_cod_prodotto	 		string			valore
							 fs_cod_versione			string			valore

Creata il 03-09-98 
Autore Mauro Bordin

EnMe 03/02/2009
		Adeguamento per l'utilizzo della gestione offerte
*/
string			ls_cod_prodotto_figlio[], ls_tabella, ls_progressivo, ls_cod_misura, &
				ls_des_estesa, ls_formula_tempo, ls_sql_ins, ls_sql_sel, ls_flag_escludibile, ls_cod_formula_quan_utilizzo,&
				ls_cod_versione_figlio[], ls_temp, ls_tipo_ritorno, ls_errore, ls_cod_formula_quan_tecnica, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio
				
dec{4}		ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long			ll_conteggio,ll_t,ll_num_sequenza, ll_ordine[]

integer		li_risposta, li_count

boolean		lb_materia_prima[]

s_chiave_distinta	lstr_distinta


choose case str_conf_prodotto.tipo_gestione

	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"

	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"

end choose

ll_conteggio = 1

declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_escludibile,
		  cod_formula_quan_tecnica,
		  cod_formula_prod_figlio,
		  cod_formula_vers_figlio
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda 
and     cod_prodotto_padre = :fs_cod_prodotto
and     cod_versione=:fs_cod_versione;

open righe_distinta_3;

do while 1 = 1

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_escludibile,
			:ls_cod_formula_quan_tecnica,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		close righe_distinta_3;
		g_mb.error("Errore nel DB: " + SQLCA.SQLErrText)
		return -1
	end if

	SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda
	AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_conteggio]
	and    cod_versione=:ls_cod_versione_figlio[ll_conteggio];

	if sqlca.sqlcode=-1 then // Errore
		g_mb.error("Errore nel DB: "+SQLCA.SQLErrText)		
		rollback;
		return -1
	end if

	if li_count = 0 then
		lb_materia_prima[ll_conteggio] = true
		
		declare cu_varianti dynamic cursor for sqlsa;
		
		ls_sql_sel ="select count(*) from " + ls_tabella + &
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
						" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
						ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento) + " and " + &
						" cod_prodotto_padre = '" + fs_cod_prodotto + "' and " + &
						" num_sequenza = " + string(ll_num_sequenza) + " and " + &
						" cod_prodotto_figlio = '" + ls_cod_prodotto_figlio[ll_conteggio] + "' and " + &
						" cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "' and " + &
						" cod_versione = '" + fs_cod_versione + "'"
		prepare sqlsa from :ls_sql_sel ;

		open dynamic cu_varianti;
		
		fetch cu_varianti into :li_count;
		
		if sqlca.sqlcode = -1 then
			g_mb.error("Configuratore di Prodotto: Errore nella Ricerca delle Varianti: "+sqlca.sqlerrtext)
			close cu_varianti;
			rollback;
			return -1
		end if
		
		close cu_varianti;
		
		
		if li_count = 0 then
			if isnull(ls_cod_misura) then ls_cod_misura = ""
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0

			if isnull(ld_dim_x) then ld_dim_x = 0
			if isnull(ld_dim_y) then ld_dim_y = 0
			if isnull(ld_dim_z) then ld_dim_z = 0
			if isnull(ld_dim_t) then ld_dim_t = 0
			if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
			if isnull(ls_des_estesa) then ls_des_estesa = ""
			if isnull(ls_formula_tempo) then ls_formula_tempo = ""

			if ls_cod_formula_quan_utilizzo <> "" and not isnull(ls_cod_formula_quan_utilizzo) then
				
				
				setnull(ld_quan_utilizzo)
				
				
				ll_ordine[1] = str_conf_prodotto.anno_documento
				ll_ordine[2] = str_conf_prodotto.num_documento
				ll_ordine[3] = str_conf_prodotto.prog_riga_documento
				
				lstr_distinta.cod_prodotto_padre = fs_cod_prodotto
				lstr_distinta.cod_versione_padre = fs_cod_versione
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				li_risposta = f_calcola_formula_db_v2(ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, str_conf_prodotto.tipo_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, ls_errore)
				if li_risposta<0 then
					ls_errore = 	"Ramo ("+fs_cod_prodotto+","+fs_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											ls_errore	
					close righe_distinta_3;
					rollback;
					
					g_mb.error(ls_errore)
					
					return -1
				end if
				
//				ld_quan_utilizzo = f_calcola_formula_db(ls_cod_formula_quan_utilizzo, ls_tabella, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento)
//				if ld_quan_utilizzo = -1 then
//					close righe_distinta_3;
//					rollback;
//					return -1
//				end if
				
			end if
			
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			
			ld_quan_utilizzo = round(ld_quan_utilizzo,4)
	
			ls_sql_ins =" insert into " + ls_tabella + &
							"(cod_azienda," + &
							"anno_registrazione," + &
							"num_registrazione," + &
							ls_progressivo + "," + &
							"cod_prodotto_padre," + &
							"num_sequenza," + &
							"cod_prodotto_figlio," + &
							"cod_versione_figlio," + &
							"cod_versione," + &
							"cod_misura," + &
							"cod_prodotto," + &
							"cod_versione_variante," + &
							"quan_tecnica," + &
							"quan_utilizzo," + &
							"dim_x," + &
							"dim_y," + &
							"dim_z," + &
							"dim_t," + &
							"coef_calcolo," + &
							"flag_esclusione," + &
							"formula_tempo," + &
							"des_estesa)" + &
						"VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
							string(str_conf_prodotto.anno_documento) + ", " + &   
							string(str_conf_prodotto.num_documento) + ", " + &   
							string(str_conf_prodotto.prog_riga_documento) + ", '" + &   
							fs_cod_prodotto + "', " + &
							string(ll_num_sequenza) + ", '" + &   
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', '" + &
							fs_cod_versione + "', '" + &
							ls_cod_misura + "', '" + &
							ls_cod_prodotto_figlio[ll_conteggio] + "', " + &
							ls_cod_versione_figlio[ll_conteggio] + "', " + &
							f_double_to_string(ld_quan_tecnica) + ", " + &   
							f_double_to_string(ld_quan_utilizzo) + ", " + &   
							f_double_to_string(ld_dim_x) + ", " + &   
							f_double_to_string(ld_dim_y) + ", " + &   
							f_double_to_string(ld_dim_z) + ", " + &   
							f_double_to_string(ld_dim_t) + ", " + &   
							f_double_to_string(ld_coef_calcolo) + ", '" + &   
							ls_flag_escludibile + "', '" + &
							ls_formula_tempo + "', '" + &
							ls_des_estesa + "')" 
	
			execute immediate :ls_sql_ins;
			if sqlca.sqlcode = -1 then
				g_mb.error("Configuratore di Prodotto: Errore nell'inserimento della Variante: "+sqlca.sqlerrtext)
				close righe_distinta_3;
				rollback;
				return -1
			end if		
		end if
		lb_materia_prima[ll_conteggio] = true
	elseif li_count > 0 then
		lb_materia_prima[ll_conteggio] = false
	end if	
	ll_conteggio++	
loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1
	if not lb_materia_prima[ll_t] then
		li_risposta = wf_crea_varianti_esclusione(ls_cod_prodotto_figlio[ll_t],fs_cod_versione)
	
//	else Materia Prima (già inserita variante)

	end if
next

return 0
end function

public function double wf_verifica_rapp_misure (double fd_dim_1, double fd_dim_2, ref string fs_messaggio);//Funzione che Verifica la correttezza delle due Dimensioni in base alla tabella Rapporti Misure Attuale
//
// nome: verifica_rapp_misure
// tipo: double
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//									fd_dim_1				 double			valore
//									fd_dim_2				 double			valore
//							
//		Creata il 14-10-98 
//		Autore Mauro Bordin
//    modificata il 14/10/1998

string ls_flag_calcolo_hp
double ld_dim_hp
datetime ldt_oggi

ldt_oggi = datetime(date(s_cs_xx.db_funzioni.oggi))

if not(fd_dim_1 = 0 or isnull(fd_dim_1) or fd_dim_2 = 0 or isnull(fd_dim_2) or &
	istr_flags.cod_tab_rapp_misure = "" or isnull(istr_flags.cod_tab_rapp_misure)) then
	select det_tab_rapp_misure.dim_hp,
			det_tab_rapp_misure.flag_calcolo_hp
	 into :ld_dim_hp,
			:ls_flag_calcolo_hp
	 from det_tab_rapp_misure,
			tes_tab_rapp_misure
	where tes_tab_rapp_misure.cod_azienda = det_tab_rapp_misure.cod_azienda and
			tes_tab_rapp_misure.cod_tab_rapp_misure = det_tab_rapp_misure.cod_tab_rapp_misure and
			( tes_tab_rapp_misure.cod_azienda = :s_cs_xx.cod_azienda and
			tes_tab_rapp_misure.cod_tab_rapp_misure = :istr_flags.cod_tab_rapp_misure and
			( tes_tab_rapp_misure.flag_blocco = 'N' or ( tes_tab_rapp_misure.flag_blocco = 'S' and 
			tes_tab_rapp_misure.data_blocco > :ldt_oggi )) and
			(:fd_dim_1 between det_tab_rapp_misure.lim_inf_dim_h and det_tab_rapp_misure.lim_sup_dim_h or
			det_tab_rapp_misure.lim_inf_dim_h = 0 and det_tab_rapp_misure.lim_sup_dim_h = 0 ) and
			(:fd_dim_2 between det_tab_rapp_misure.lim_inf_dim_s and det_tab_rapp_misure.lim_sup_dim_s or
			det_tab_rapp_misure.lim_inf_dim_s = 0 and det_tab_rapp_misure.lim_sup_dim_s = 0 ));
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore durante l'Estrazione Tabella Rapporti Misure"
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("Verifica Tabella Rapporti Misure", "Dimensioni non Coerenti con la Tabella Rapporti Misure")
		return 0
	end if
	if ls_flag_calcolo_hp = "H" then // Dim_HP è uguale a Dim_1 (H)
		return fd_dim_1
	elseif ls_flag_calcolo_hp = "S" then // Dim_HP è uguale a Dim_2 (S)
		return fd_dim_2
	elseif ls_flag_calcolo_hp = "F" then // Dim_HP è uguale a Dim_HP (Fisso)
		return ld_dim_hp
	else
		return 0
	end if
else
	return 0
end if
end function

public function integer wf_calcoli_plisse (double fd_larghezza_finita, double fd_altezza_finita, string fs_flag_autoblocco, string fs_flag_misura_luce_finita, string fs_cod_tessuto, string fs_cod_non_a_magazzino, ref string fs_messaggio);double ld_prof_inf_plisse, ld_prof_sup_plisse, ld_pos_iniziale_punzoni, ld_pos_finale_punzoni, &
		 ld_quantita_pieghe, ld_intervallo_punzoni_plisse
integer li_num_fili_plisse, li_num_boccole_plisse

if fd_larghezza_finita < 1.8 then
	fs_messaggio = "Larghezza Troppo Piccola (Min 1,7)"
	return -1
end if
if fs_flag_misura_luce_finita = "L" then
	if fs_flag_autoblocco = "S" then
		ld_prof_inf_plisse = fd_larghezza_finita - 0.8
		ld_prof_sup_plisse = fd_larghezza_finita - 1.7
	elseif fs_flag_autoblocco = "N" then
		ld_prof_inf_plisse = fd_larghezza_finita - 0.4
		ld_prof_sup_plisse = fd_larghezza_finita - 0.4
	end if
elseif fs_flag_misura_luce_finita = "F" then
	if istr_riepilogo.flag_autoblocco = "S" then
		ld_prof_inf_plisse = fd_larghezza_finita - 0.4
		ld_prof_sup_plisse = fd_larghezza_finita - 1.7
	elseif fs_flag_autoblocco = "N" then
		ld_prof_inf_plisse = fd_larghezza_finita - 0.4
		ld_prof_sup_plisse = fd_larghezza_finita - 0.4
	end if
end if

select quantita_fili,
		quantita_boccole
 into :li_num_fili_plisse,
		:li_num_boccole_plisse
 from tab_fili_plisse
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tessuto = :fs_cod_tessuto and
		cod_non_a_magazzino = :fs_cod_non_a_magazzino and
		:ld_prof_inf_plisse between lim_inf_profilo_inf and lim_sup_profilo_inf;
		
if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore durante l'Estrazione numero fili Plissè"
	return -1
elseif sqlca.sqlcode = 100 then
	select quantita_fili,
			quantita_boccole
	 into :li_num_fili_plisse,
			:li_num_boccole_plisse
	 from tab_fili_plisse
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tessuto = :fs_cod_tessuto and
			cod_non_a_magazzino = :fs_cod_non_a_magazzino and
			lim_inf_profilo_inf = 0 and
			lim_sup_profilo_inf = 0;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore durante l'Estrazione numero fili Plissè"
		return -1
	elseif sqlca.sqlcode = 100 then
		li_num_fili_plisse = 0
		li_num_boccole_plisse = 0
	end if
end if

select quantita_pieghe
 into :ld_quantita_pieghe
 from tab_pieghe_plisse
where cod_azienda = :s_cs_xx.cod_azienda and
		:fd_altezza_finita between lim_inf_dimensione and lim_sup_dimensione;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore durante l'Estrazione numero fili Plissè"
	return -1
elseif sqlca.sqlcode = 100 then
	select quantita_pieghe
	 into :ld_quantita_pieghe
	 from tab_pieghe_plisse
	where cod_azienda = :s_cs_xx.cod_azienda and
			lim_inf_dimensione = 0 and
			lim_sup_dimensione = 0;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore durante l'Estrazione numero Pieghe Plissè"
		return -1
	elseif sqlca.sqlcode = 100 then
		ld_quantita_pieghe = 0
	end if
end if

ld_pos_iniziale_punzoni = 2.2
ld_pos_finale_punzoni = ld_prof_inf_plisse - 2.2

if li_num_fili_plisse > 2 then
	ld_intervallo_punzoni_plisse = round((ld_pos_finale_punzoni - ld_pos_iniziale_punzoni) / (li_num_fili_plisse - 1), 1)
else
	ld_intervallo_punzoni_plisse = ld_pos_finale_punzoni - ld_pos_iniziale_punzoni
end if

istr_riepilogo.prof_inf_plisse = ld_prof_inf_plisse
istr_riepilogo.prof_sup_plisse = ld_prof_sup_plisse
istr_riepilogo.num_fili_plisse = li_num_fili_plisse
istr_riepilogo.num_boccole_plisse = li_num_boccole_plisse
istr_riepilogo.num_pieghe_plisse = ld_quantita_pieghe
istr_riepilogo.intervallo_punzoni_plisse = ld_intervallo_punzoni_plisse
return 0
end function

public function integer wf_init_cod_prodotto (ref string fs_messaggio);//	Funzione che partendo dal modello passato come parametro mi inizializza le variabili di istanza
//		quali la classe merceologica, la linea del prodotto, il codice prodotto finito
//		restituisce il codice prodotto finito
//
// nome: wf_init_cod_prodotto
// tipo: string
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//									str_conf_prodotto.cod_modello	    String			valore
//
//		Creata il 31-08-98 
//		Autore Mauro Bordin
//    modificata il 31/08/1998
//    ovviamente risistemata al 17/12/1999 da Enrico Menegotto perchè xe tutto rotto (KZ)
//
/*
string ls_cod_veloce

if dw_inizio.getrow() < 1 then return 0
str_conf_prodotto.cod_modello = dw_inizio.getitemstring(1,"rs_cod_modello")
if isnull(str_conf_prodotto.cod_modello) then return 0
is_linea_prodotto = left(str_conf_prodotto.cod_modello, 1)
is_classe_merceolo = mid(str_conf_prodotto.cod_modello, 2, 1)

//if isnull(str_conf_prodotto.cod_prodotto_finito) or str_conf_prodotto.cod_prodotto_finito = "" then
//	if is_classe_merceolo = "0" then		
//		str_conf_prodotto.cod_prodotto_finito = replace(istr_flags.cod_iniziale, 1, 4, trim(left(str_conf_prodotto.cod_modello,4)) + fill("0", 4 - len(trim(left(trim(str_conf_prodotto.cod_modello),4)))))
//	else
//		str_conf_prodotto.cod_prodotto_finito = replace(istr_flags.cod_iniziale, 1, 5, trim(left(str_conf_prodotto.cod_modello,5)) + fill("0", 5 - len(trim(left(trim(str_conf_prodotto.cod_modello),5)))))
//	end if
//end if

str_conf_prodotto.cod_prodotto_finito = str_conf_prodotto.cod_modello + "     "

// --------------------------------------  inizio ---------------------------------------------

if is_classe_merceolo <> "0" and is_classe_merceolo <> "S" then
	if istr_riepilogo.dimensione_1 > 999 then
		fs_messaggio = "Dimensione Troppo Grande (Max 999)"
		return -1
	else
		str_conf_prodotto.cod_prodotto_finito = replace(str_conf_prodotto.cod_prodotto_finito, 7, 3, fill("0", 3 - len(trim(string(integer(istr_riepilogo.dimensione_1))))) + trim(string(integer(istr_riepilogo.dimensione_1))))
	end if
end if

// ----------------------------------------  tessuto ---------------------------------------------
if not isnull(istr_riepilogo.cod_non_a_magazzino) and len(istr_riepilogo.cod_non_a_magazzino) > 0 and not isnull(istr_riepilogo.cod_tessuto) and len(istr_riepilogo.cod_tessuto) > 0  then
	choose case is_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_non_a_magazzino = :istr_riepilogo.cod_non_a_magazzino and
					cod_tessuto = :istr_riepilogo.cod_tessuto;
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto non Valido"
				return -1
			elseif SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante la ricerca proprietà tessuto in tabella tessuti. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 5, 1, ls_cod_veloce)
	end choose
end if
//

// ------------------------------------------ comandi --------------------------------------------------------
if not isnull(istr_riepilogo.cod_comando) and len(istr_riepilogo.cod_comando) > 0 then
	choose case is_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :istr_riepilogo.cod_comando;
			if sqlca.sqlcode = 100 then
				fs_messaggio = "Codice comando non Valido"
				return -1
			elseif sqlca.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione del codice veloce dal tipo comando"
				return -1
			end if
			if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
				fs_messaggio = "Al comando " + istr_riepilogo.cod_comando + " non è stato associato alcun codice veloce: verificare"
				return -1
			end if
			str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 7, 2, ls_cod_veloce)
	end choose
end if
// ----------------------------------- lastre / verniciature ---------------------------------------------

if not isnull(istr_riepilogo.cod_verniciatura) and len(istr_riepilogo.cod_verniciatura) > 0 then
	select cod_veloce
	into  :ls_cod_veloce
	from  tab_verniciatura
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_verniciatura = :istr_riepilogo.cod_verniciatura;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Autocomposizione codice: codice verniciatura " + istr_riepilogo.cod_verniciatura + " non trovato in tabella verniciature"
		return -1
	elseif SQLCA.sqlcode <> 0 then
		fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
		fs_messaggio = "Alla verniciatura " + istr_riepilogo.cod_verniciatura + " non è stato associato alcun codice veloce: verificare"
		return -1
	end if
	str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 6, 1, ls_cod_veloce)
end if

// -----------------------------------  visualizzo il codicie finito ----------------------------------------

*/
string ls_errore
uo_set_default luo_set_default

luo_set_default = create uo_set_default
if luo_set_default.uof_init_codice_prodotto( str_conf_prodotto.cod_modello, &
																istr_riepilogo.dimensione_1,&
																istr_riepilogo.cod_non_a_magazzino,&
																istr_riepilogo.cod_tessuto,&
																istr_riepilogo.cod_comando,&
																istr_riepilogo.cod_verniciatura,&
																str_conf_prodotto.cod_prodotto_finito, ls_errore) < 0 then
	g_mb.warning(ls_errore)
end if
destroy luo_set_default
st_cod_prodotto_finito.text = " Codice Prodotto :  " + str_conf_prodotto.cod_prodotto_finito

return 0



end function

public function integer wf_rowfocuschanged (datawindow fdw_passo, long fl_currentrow, ref string fs_messaggio);//	Funzione che gestisce gli eventi rowfocuschanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_rowfocuschanged
// tipo: none
// ----------------------------------------------------------------------------------------
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fl_currentrow		 long				valore
//									fs_messaggio		 String			referenza
//
//		Creata il 05/10/2000
//		Autore Enrico Menegotto
// -----------------------------------------------------------------------------------------
string ls_nome_dw
uo_rowfocuschanged luo_rowfocuschanged

ls_nome_dw = fdw_passo.dataobject
uo_rowfocuschanged = CREATE uo_rowfocuschanged
choose case ls_nome_dw
	case "d_ext_inizio"
	case "d_ext_tessuto"
	case "d_ext_vern_lastra"
	case "d_ext_comandi"
	case "d_ext_supporto_flags"
	case "d_ext_optional"
		if uo_rowfocuschanged.uof_rowfocuschanged_optional(fdw_passo, fl_currentrow, fs_messaggio) = -1 then
			return -1
		end if
end choose
destroy luo_rowfocuschanged

return 0

end function

public function integer wf_crea_varianti ();string						ls_errore, ls_errore_vendita_sl, ls_vuoto[]
long						ll_ret
uo_conf_varianti		luo_conf_varianti
uo_riassunto			luo_riassunto

luo_conf_varianti = CREATE uo_conf_varianti
luo_conf_varianti.is_gestione = str_conf_prodotto.tipo_gestione
luo_conf_varianti.il_anno_registrazione = str_conf_prodotto.anno_documento
luo_conf_varianti.il_num_registrazione = str_conf_prodotto.num_documento
luo_conf_varianti.il_prog_riga = str_conf_prodotto.prog_riga_documento
luo_conf_varianti.is_cod_modello = str_conf_prodotto.cod_modello
luo_conf_varianti.is_cod_versione = str_conf_prodotto.cod_versione

luo_conf_varianti.is_errore_vendita_sl = ""
luo_conf_varianti.ib_fase_su_radice = false
luo_conf_varianti.ib_nofase_su_figli = true

//06/02/2014 Donato
//create queste var. di istanza che vengono valorizzate durante il calcolo delle formule su distinta base
//qui le ripulisco prima della valorizzazione
//###################################################################
//azzero variabile riepilogo calcolo formule
luo_conf_varianti.is_warning_formule[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule_qta[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule_qtatec[] = ls_vuoto[]
//###################################################################


ll_ret = luo_conf_varianti.uof_crea_varianti(istr_riepilogo, istr_flags, ref ls_errore)

//06/02/2014 Donato
//questa funzione accoda nella datawindow finale il riepilogo sulle formule calcoate sui rami di distinta base
//###################################################################
luo_riassunto = create uo_riassunto
luo_riassunto.uof_riassunto_formule(luo_conf_varianti, dw_fine)
destroy luo_riassunto
//###################################################################

//06/02/2014 Donato
//ri-azzero variabile riepilogo calcolo formule
luo_conf_varianti.is_warning_formule[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule_qta[] = ls_vuoto[]
luo_conf_varianti.is_elaborazione_formule_qtatec[] = ls_vuoto[]
//###################################################################


if ll_ret < 0 then
	g_mb.error(ls_errore)
	return -1
end if

ls_errore_vendita_sl = luo_conf_varianti.is_errore_vendita_sl
destroy luo_conf_varianti

if ls_errore_vendita_sl<>"" and not isnull(ls_errore_vendita_sl) then
	g_mb.warning("Errore NON bloccante: "+ls_errore_vendita_sl + " La configurazione proseguirà adesso normalmente..." )
end if




end function

public function integer wf_crea_add_mp_non_ric ();/*
Funzione che crea le addizionali delle materie prime automatiche, nel caso che queste ultime lo siano.
In tal caso aggiunge una riga al documento della gestione corrente.

nome: wf_crea_addizionali_passo
tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							 fdw_passo				    DataWindow		valore

Creata il 31-08-98 
Autore Mauro Bordin (PAJASSO coe rode e i cerchi in lega)
modificata il 31/08/1998
Enrico 13/10/1999: numero mp automatiche illimitato

*/
string ls_cod_misura, ls_cod_iva, ls_tabella, ls_progressivo, ls_sql_ins, ls_cod_prodotto, ls_nota, &
		 ls_cod_tipo_dettaglio_vendita, ls_cod_versione, ls_flag_doc_suc_or, ls_flag_st_note_or, ls_flag_gen_commessa, ls_flag_presso_cg, &
		 ls_flag_presso_pt, ls_cod_iva_det_ven, ls_cod_iva_esente
dec{5} ld_fat_conversione_ven
dec{4} ld_prezzo_vendita
long   ll_i, ll_num_mp, ll_numero
datetime ldt_data_esenzione_iva

ls_nota = ""

choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		ls_tabella = "det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"

	case "OFF_VEN"
		ls_tabella = "det_off_ven"
		ls_progressivo = "prog_riga_off_ven"

end choose

ll_num_mp = upperbound(istr_riepilogo.mp_automatiche)
for ll_i = 1 to ll_num_mp
	if istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica <> "" and not isnull(istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica) then
		select flag_addizionale,
				 cod_tipo_det_ven
		into   :istr_riepilogo.mp_automatiche[ll_i].flag_addizionale,
				 :istr_riepilogo.mp_automatiche[ll_i].cod_tipo_det_ven
		from   tab_mp_non_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_mp_non_richiesta = :istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica and
				 prog_mp_non_richiesta = :istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore durante l'Estrazione Materia Prima automatica " + istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica,  exclamation!, ok!)
			rollback;
			return -1
		end if
	end if
	
	if istr_riepilogo.mp_automatiche[ll_i].flag_addizionale = "S" then
		ls_nota = ls_nota + ". Addizionale al Prodotto: " + str_conf_prodotto.cod_modello 
		ls_cod_prodotto = istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica
		ls_cod_tipo_dettaglio_vendita = istr_riepilogo.mp_automatiche[ll_i].cod_tipo_det_ven
		
		select flag_doc_suc_or, 
				flag_st_note_or,
				cod_iva
		into   :ls_flag_doc_suc_or, 
				:ls_flag_st_note_or,
				:ls_cod_iva_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_dettaglio_vendita;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore in ricerca flag_note in tipo dettaglio prodotto. Dettaglio errore "+sqlca.sqlerrtext)
			ROLLBACK;
			return -1
		end if

		ls_flag_gen_commessa = "S"
		ls_flag_presso_cg = "N"
		ls_flag_presso_pt = "N"
		uo_default_prodotto luo_default_prodotto
		luo_default_prodotto = CREATE uo_default_prodotto
		if luo_default_prodotto.uof_flag_gen_commessa(ls_cod_prodotto) = 0 then
			ls_flag_gen_commessa = luo_default_prodotto.is_flag_gen_commessa
			ls_flag_presso_cg = luo_default_prodotto.is_flag_presso_cg
			ls_flag_presso_pt = luo_default_prodotto.is_flag_presso_pt
		else
			ls_flag_gen_commessa = "S"
			ls_flag_presso_cg = "N"
			ls_flag_presso_pt = "N"
		end if
		destroy luo_default_prodotto
		
		select cod_misura_ven,
				 fat_conversione_ven,
				 cod_iva,
				 prezzo_vendita
		into	 :ls_cod_misura,
				 :ld_fat_conversione_ven,
				 :ls_cod_iva,
				 :ld_prezzo_vendita
		from	 anag_prodotti
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Prodotto Addizionale non corretto")
			return -1
		end if
		
		if isnull(ls_cod_iva) then
			ls_cod_iva = ls_cod_iva_det_ven
		end if
		
		if not isnull(str_conf_prodotto.cod_contatto) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva_esente,
					 :ldt_data_esenzione_iva
			from   anag_contatti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_contatto = :str_conf_prodotto.cod_contatto;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_iva_esente) and ls_cod_iva_esente <> "" and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
					ls_cod_iva = ls_cod_iva_esente
				end if
			end if
		elseif not isnull(str_conf_prodotto.cod_cliente) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva_esente,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_cliente = :str_conf_prodotto.cod_cliente;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_iva_esente) and ls_cod_iva_esente <> "" and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
					ls_cod_iva = ls_cod_iva_esente
				end if
			end if
		end if
		
		
		select cod_versione
		into   :ls_cod_versione
		from   distinta_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto and
		       flag_predefinita = 'S';
		if sqlca.sqlcode <> 0 then
			ls_cod_versione = "null"
		else
			ls_cod_versione = "'" + ls_cod_versione + "'"
		end if

		if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then
			ls_cod_prodotto = "null"
		else
			ls_cod_prodotto = "'" + ls_cod_prodotto + "'"
		end if

		if ls_cod_tipo_dettaglio_vendita = "" or isnull(ls_cod_tipo_dettaglio_vendita) then
			ls_cod_tipo_dettaglio_vendita = "null"
		else
			ls_cod_tipo_dettaglio_vendita = "'" + ls_cod_tipo_dettaglio_vendita + "'"
		end if
		
		if ls_cod_misura = "" or isnull(ls_cod_misura) then
			ls_cod_misura = "null"
		else
			ls_cod_misura = "'" + ls_cod_misura + "'"
		end if
		
		if ls_cod_iva = "" or isnull(ls_cod_iva) then
			ls_cod_iva = "null"
		else
			ls_cod_iva = "'" + ls_cod_iva + "'"
		end if
		
		if ls_nota = "" or isnull(ls_nota) then
			ls_nota = "null"
		else
			ls_nota = "'" + ls_nota + "'"
		end if

		if ls_flag_doc_suc_or = "" or isnull(ls_flag_doc_suc_or) then
			ls_flag_doc_suc_or = "null"
		else
			ls_flag_doc_suc_or = "'" + ls_flag_doc_suc_or + "'"
		end if

		if ls_flag_st_note_or = "" or isnull(ls_flag_st_note_or) then
			ls_flag_st_note_or = "null"
		else
			ls_flag_st_note_or = "'" + ls_flag_st_note_or + "'"
		end if

		if ls_flag_gen_commessa = "" or isnull(ls_flag_gen_commessa) then
			ls_flag_gen_commessa = "null"
		else
			ls_flag_gen_commessa = "'" + ls_flag_gen_commessa + "'"
		end if
		
		if ls_flag_presso_pt = "" or isnull(ls_flag_presso_pt) then
			ls_flag_presso_pt = "null"
		else
			ls_flag_presso_pt = "'" + ls_flag_presso_pt + "'"
		end if

		if ls_flag_presso_cg = "" or isnull(ls_flag_presso_cg) then
			ls_flag_presso_cg = "null"
		else
			ls_flag_presso_cg = "'" + ls_flag_presso_cg + "'"
		end if

		
		choose case str_conf_prodotto.tipo_gestione
			case "ORD_VEN"
				str_conf_prodotto.ultima_riga_inserita ++
				
				select prog_riga_ord_ven
				into	:ll_numero
				from  det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :str_conf_prodotto.anno_documento and
						num_registrazione = :str_conf_prodotto.num_documento and
						prog_riga_ord_ven = :str_conf_prodotto.ultima_riga_inserita;
				if sqlca.sqlcode = 0 or sqlca.sqlcode = -1 then
					g_mb.messagebox("Configuratore di Prodotto", "Il numero di riga "+string(str_conf_prodotto.ultima_riga_inserita)+" esiste già nell'ordine; verificare!", exclamation!, ok!)
					rollback;
					return -1
				end if
				
			case "OFF_VEN"
				str_conf_prodotto.ultima_riga_inserita ++
				
				select prog_riga_off_ven
				into	:ll_numero
				from  det_off_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :str_conf_prodotto.anno_documento and
						num_registrazione = :str_conf_prodotto.num_documento and
						prog_riga_off_ven = :str_conf_prodotto.ultima_riga_inserita;
				if sqlca.sqlcode = 0 or sqlca.sqlcode = -1 then
					g_mb.messagebox("Configuratore di Prodotto", "Il numero di riga "+string(str_conf_prodotto.ultima_riga_inserita)+" esiste già nell'offerta; verificare!", exclamation!, ok!)
					rollback;
					return -1
				end if
		end choose

		ls_sql_ins =	"insert into " + ls_tabella + &
								" (cod_azienda, " + &
								"anno_registrazione, " + &
								"num_registrazione, " + &
								ls_progressivo + ", " + &
								"cod_prodotto, " + &
								"cod_tipo_det_ven, " + &
								"cod_misura, " + &
								"des_prodotto, " + &
								"quan_ordine, " + &
								"prezzo_vendita, " + &
								"fat_conversione_ven, " + &
								"sconto_1, " + &
								"sconto_2, " + &
								"provvigione_1, " + &
								"provvigione_2, " + &
								"cod_iva, " + &
								"data_consegna, " + &
								"val_riga, " + &
								"quan_in_evasione, " + &
								"quan_evasa, " + &
								"flag_evasione, " + &
								"flag_blocco, " + &
								"nota_dettaglio, " + &
								"anno_registrazione_off, " + &
								"num_registrazione_off, " + &
								"prog_riga_off_ven, " + &
								"anno_commessa, " + &
								"num_commessa, " + &
								"cod_centro_costo, " + &
								"sconto_3, " + &
								"sconto_4, " + &
								"sconto_5, " + &
								"sconto_6, " + &
								"sconto_7, " + &
								"sconto_8, " + &
								"sconto_9, " + &
								"sconto_10, " + &
								"flag_gen_commessa, " + &
								"flag_presso_ptenda" + &
								"flag_presso_cgibus, " + &
							   "flag_doc_suc_det, " + &  
							   "flag_st_note_det, " + &  
								"num_riga_appartenenza, " + &
								"cod_versione) " + &
							"VALUES ('" + s_cs_xx.cod_azienda + "', " + &
								string(str_conf_prodotto.anno_documento) + ", " + &
								string(str_conf_prodotto.num_documento) + ", " + &
								string(str_conf_prodotto.ultima_riga_inserita) + ", " + &
								ls_cod_prodotto + ", " + &
								ls_cod_tipo_dettaglio_vendita + ", " + &
								ls_cod_misura + ", " + &
								"null, " + &
								"0, " + &
								string(ld_prezzo_vendita) + ", " + &
								f_double_to_string(ld_fat_conversione_ven) + ", " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								ls_cod_iva + ", " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"'A', " + &
								"'N', " + &
								ls_nota + ", " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								ls_flag_gen_commessa + ", " + &
								ls_flag_presso_pt + ", " + &
								ls_flag_presso_cg  + ", " + &
								ls_flag_doc_suc_or + ", " + &
								ls_flag_st_note_or + ", " + &
								string(str_conf_prodotto.prog_riga_documento) + ", " + &
								"" + ls_cod_versione + ")"
								
		execute immediate :ls_sql_ins;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore durante la Creazione della riga Addizionale mp automatica " + ls_cod_prodotto + " Dettaglio=" + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return -1
		end if

	end if 
next
return 0
end function

public subroutine wf_set_default (ref datawindow fdw_data_window);//	Funzione che inizializza ai valori di default (flag_default delle rispettive tabelle) i campi
//		della datawindow passata come parametro
//		viene lanciata ogni volta che si passa di Dw in DW
//
// nome: wf_set_default
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//									fdw_data_window	 DataWindow		valore
//
//		Creata il 31-08-98 
//		Autore Mauro Bordin
//    modificata il 31/08/1998
//    =========================================
//    EnMe 18/1/2001 per nota di dettaglio del prodotto finito
//	  EnMe 30/03/2012 gestione delle 3 verniciature

string ls_str
uo_set_default luo_set_defaults
uo_riassunto   luo_riassunto

luo_set_defaults = CREATE uo_set_default
luo_set_defaults.uof_set_default(fdw_data_window, dw_inizio, is_linea_prodotto, &
                                is_classe_merceolo, str_conf_prodotto.cod_prodotto_finito, str_conf_prodotto.cod_modello)

if fdw_data_window.dataobject = "d_ext_fine" then
	luo_riassunto = CREATE uo_riassunto
	ls_str = luo_riassunto.uof_riassunto(dw_inizio, dw_fine)
	fdw_data_window.setitem(1, "rs_riassunto", ls_str)
	destroy luo_riassunto
end if

if fdw_data_window.dataobject = "d_ext_note" then
	cb_note_esterne.visible = true
else
	cb_note_esterne.visible = false
end if

if fdw_data_window.dataobject = "d_ext_optional" then
	//fdw_data_window.height = 1350
	luo_set_defaults.uof_set_default(dw_nota_prodotto_finito, dw_inizio, is_linea_prodotto, &
											  is_classe_merceolo, str_conf_prodotto.cod_prodotto_finito, str_conf_prodotto.cod_modello)
end if
destroy luo_set_defaults

end subroutine

public subroutine wf_set_dddw (ref datawindow fdw_data_window);//	Funzione che mi carica le DropDown DataWindow della DataWindow passata come parametro
//
// nome: wf_set_dddw
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//									fdw_data_window	 DataWindow		valore
//
//	CREATA IL 31-08-1998 
//	Autore Mauro Bordin
//	modificata il 31/08/1998
//	MODIFICATA IL 06/10/2000  PER NUOVA GESTIONE OPTIONAL
//	modificata da EnMe il 31/1/2007 per limiti sui comandi


choose case fdw_data_window.dataobject
	case "d_ext_tessuto", "d_ext_tessuto_dk", "d_ext_tessuto_alu"
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_tessuto",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tessuto from tab_tessuti where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = ''))")

		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_tessuto_mant",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tessuto from tab_tessuti where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = ''))")

		if fdw_data_window.dataobject="d_ext_tessuto_alu" then
			DataWindowChild  ldw_child
			
			fdw_data_window.GetChild ("rs_flag_tasca", REF ldw_child )	
			ldw_child.settransobject(sqlca)
			ldw_child.retrieve(s_cs_xx.cod_azienda, str_conf_prodotto.cod_modello)

			fdw_data_window.GetChild ("rs_cod_tipo_tasca_1", REF ldw_child )	
			ldw_child.settransobject(sqlca)
			ldw_child.retrieve(s_cs_xx.cod_azienda, str_conf_prodotto.cod_modello)

			fdw_data_window.GetChild ("rs_cod_tipo_tasca_2", REF ldw_child )	
			ldw_child.settransobject(sqlca)
			ldw_child.retrieve(s_cs_xx.cod_azienda, str_conf_prodotto.cod_modello)

			f_PO_LoadDDDW_DW(fdw_data_window,"cod_reparto_telo",sqlca,&
								  "anag_reparti", &
								  "cod_reparto", &
								  "des_reparto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
								  
			f_PO_LoadDDDW_DW(fdw_data_window,"rs_flag_tipo_unione",sqlca,&
								  "tab_tipi_unione_tessuti", &
								  "cod_tipo_unione_tessuto", &
								  "des_tipo_unione_tessuto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ")
end if	

	case "d_ext_vern_lastra"
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_verniciatura",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = '' ) and ( cod_classe_merceolo like '%" + &
							  is_classe_merceolo + "%' or cod_classe_merceolo is null or cod_classe_merceolo = ''))")

		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_colore_lastra",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_colore_lastra from tab_colori_lastre where cod_azienda = '" + &
							  s_cs_xx.cod_azienda  + "' and cod_modello = '" + str_conf_prodotto.cod_modello + "' )")

		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_tipo_lastra",sqlca,&
							  "tab_tipi_lastre","cod_tipo_lastra","des_tipo_lastra",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + str_conf_prodotto.cod_modello + "'" )

	case "d_ext_tipi_stampi"
		
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_tipo_stampo",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = '' ) and ( cod_classe_merceolo = 'S'))")

	case "d_ext_comandi"
		
			f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_comando",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_comando from tab_limiti_comando where cod_azienda = '" + &
								  s_cs_xx.cod_azienda + "' and cod_modello_tenda = '" + str_conf_prodotto.cod_modello + "' and flag_secondo_comando = 'N')")
								  
			f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_comando_2",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_comando from tab_limiti_comando where cod_azienda = '" + &
								  s_cs_xx.cod_azienda + "' and cod_modello_tenda = '" + str_conf_prodotto.cod_modello + "' and flag_secondo_comando = 'S')")
								  
	case "d_ext_supporto_flags"		
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_supporto",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tipo_supporto from tab_tipi_supporto where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = ''))")

	case "d_ext_verniciature"
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_verniciatura",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = '' ) and ( cod_classe_merceolo like '%" + &
							  is_classe_merceolo + "%' or cod_classe_merceolo is null or cod_classe_merceolo = ''))")

		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_verniciatura_2",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = '' ) and ( cod_classe_merceolo like '%" + &
							  is_classe_merceolo + "%' or cod_classe_merceolo is null or cod_classe_merceolo = ''))")

		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_verniciatura_3",sqlca,&
							  "anag_prodotti","cod_prodotto","des_prodotto",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
							  s_cs_xx.cod_azienda + "' and ( cod_linea_prodotto like '%" + is_linea_prodotto + "%' or cod_linea_prodotto is null or cod_linea_prodotto = '' ) and ( cod_classe_merceolo like '%" + &
							  is_classe_merceolo + "%' or cod_classe_merceolo is null or cod_classe_merceolo = ''))")


case "d_ext_optional"		
		f_PO_LoadDDDW_DW(fdw_data_window,"rs_cod_tipo_det_ven",sqlca,&
							  "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


					  
/* Caricamente manuale della DROP dei prodotti optional in base alla seguente logica
- cerco nelle righe ordine della stagioe corrente + precedente gli optionals più usati e li metto in elenco in ordine di priorità
- unisco alla ricerca gli optioanl dalla tabella TAB_OPTIONALS
*/
INTEGER          l_ColNbr,     l_Idx,      l_Jdx, ll_anno_esercizio
INTEGER          l_ColPos,     l_Start,    l_Count, li_ret
LONG             l_Error
STRING           l_SQLString,  l_ErrStr, ls_Required
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
DATAWINDOWCHILD  l_DWC

ll_anno_esercizio = f_anno_esercizio()
l_dwDescribe = "rs_cod_prodotto.ID"
l_ColNbr     = Integer(fdw_data_window.Describe(l_dwDescribe))
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = fdw_data_window.Describe(l_dwDescribe)


l_SQLString = "select table1.prodotto as prodotto, '[' +cast(count(*) as varchar(10))  + ']  ' + table1.descrizione as descrizione, count(*)   as frequenza, table1.cod_azienda as azienda " + &
" from (	 " + &
" SELECT det_ord_ven_b.cod_azienda, det_ord_ven_b.cod_prodotto as prodotto,  det_ord_ven_b.des_prodotto as descrizione " + &
" FROM  det_ord_ven det_ord_ven_a " + &
" LEFT OUTER JOIN det_ord_ven det_ord_ven_b ON det_ord_ven_a.cod_azienda = det_ord_ven_b.cod_azienda AND "+&
																	"det_ord_ven_a.anno_registrazione = det_ord_ven_b.anno_registrazione AND "+&
																	"det_ord_ven_a.num_registrazione = det_ord_ven_b.num_registrazione AND "+&
																	"det_ord_ven_a.prog_riga_ord_ven = det_ord_ven_b.num_riga_appartenenza " + &
" WHERE  det_ord_ven_b.cod_tipo_det_ven='OPT' AND det_ord_ven_a.cod_prodotto='" + str_conf_prodotto.cod_modello +  "' AND det_ord_ven_a.anno_registrazione>="+string(ll_anno_esercizio - 1)+" " + &
" union all " + &
" select tab_optionals.cod_azienda, tab_optionals.cod_optional, anag_prodotti.des_prodotto " + &
" from tab_optionals, anag_prodotti " + &
"  where tab_optionals.cod_azienda = anag_prodotti.cod_azienda and tab_optionals.cod_optional = anag_prodotti.cod_prodotto and tab_optionals.cod_azienda =  '" + s_cs_xx.cod_azienda + "' and tab_optionals.cod_modello = '" + str_conf_prodotto.cod_modello +  "' " + &
" ) as table1 " + &
" group by table1.prodotto, table1.descrizione, table1.cod_azienda " + &
" order by frequenza DESC , descrizione "

/* QUERY ORIGINARIA CON IL COD_AZIENDA IN WHERE

l_SQLString = "select  table1.prodotto as prodotto, '[' +cast(count(*) as varchar(10))  + ']  ' + table1.descrizione as descrizione, count(*)   as frequenza " + &
" from (	 " + &
" SELECT det_ord_ven_b.cod_azienda, det_ord_ven_b.cod_prodotto as prodotto,  det_ord_ven_b.des_prodotto as descrizione " + &
" FROM  det_ord_ven det_ord_ven_a " + &
" LEFT OUTER JOIN det_ord_ven det_ord_ven_b ON det_ord_ven_a.cod_azienda = det_ord_ven_b.cod_azienda AND det_ord_ven_a.anno_registrazione = det_ord_ven_b.anno_registrazione AND det_ord_ven_a.num_registrazione = det_ord_ven_b.num_registrazione AND det_ord_ven_a.prog_riga_ord_ven = det_ord_ven_b.num_riga_appartenenza    " + &
" WHERE ( det_ord_ven_a.cod_azienda =  '" + s_cs_xx.cod_azienda + "' ) AND   ( det_ord_ven_b.cod_tipo_det_ven = 'OPT' ) AND   ( det_ord_ven_a.cod_prodotto = '" + str_conf_prodotto.cod_modello +  "' ) AND   ( det_ord_ven_a.anno_registrazione  >= "+string(ll_anno_esercizio - 1)+" )    " + &
" union all " + &
" select tab_optionals.cod_optional, anag_prodotti.des_prodotto " + &
" from tab_optionals, anag_prodotti " + &
"  where tab_optionals.cod_azienda = anag_prodotti.cod_azienda and tab_optionals.cod_optional = anag_prodotti.cod_prodotto and tab_optionals.cod_azienda =  '" + s_cs_xx.cod_azienda + "' and tab_optionals.cod_modello = '" + str_conf_prodotto.cod_modello +  "' " + &
" ) as table1 " + &
" group by table1.prodotto, table1.descrizione " + &
" order by frequenza DESC , descrizione "

*/


L_Error = fdw_data_window.GetChild(l_ColStr, l_DWC)

l_Error = l_DWC.SetTransObject(sqlca)
l_dwModify = "DataWindow.Table.Select='" + w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//imposto il filter per azienda corrente
//non lo metto in where perchè sembra che la query sia molto lenta ...
//quindi ho tolto dalla where il cod_azienda e cerco di gestirlo con il filter sulla datawindow
li_ret = l_DWC.setfilter("#4='"+s_cs_xx.cod_azienda+"'")
li_ret = l_DWC.filter()

l_Error = l_DWC.Retrieve()

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

ls_Required = fdw_data_window.Describe("rs_cod_prodotto" + ".DDDW.Required")

IF ls_Required = "no" THEN
   l_DWC.InsertRow(1)
END IF


end choose
end subroutine

public subroutine wf_spostamento (string fs_tipo_spostamento);//wf_avanti : funzione che mi sposta
//argomenti : fs_tipo_spostamento, tipo di spostamento ('A' avanti, 'I' indietro, 'P' posiziona)
string ls_messaggio


wf_memorizza_dati()

choose case fs_tipo_spostamento
	case "A" // Avanti
		if ii_num_passo = 99 then return
		ii_num_passo_old = ii_num_passo
		if ii_num_passo < ii_quantita_passi then
			ii_num_passo ++
		elseif ii_num_passo = ii_quantita_passi then
			ii_num_passo = 99
		else
			g_mb.messagebox("Configuratore Prodotto", "Numero Passo Sbagliato")
		end if
		
	case "I" // Indietro
		if ii_num_passo = 0 then return
		ii_num_passo_old = ii_num_passo
		if ii_num_passo <> 99 then
			ii_num_passo --
		else
			ii_num_passo = ii_quantita_passi
		end if

	case "P" // Posiziona
		ii_num_passo_old = ii_num_passo
		ii_num_passo = ii_passo_richiesto
end choose
wf_visualizza_dw()
if wf_init_cod_prodotto(ls_messaggio) <> 0 then
	g_mb.messagebox("Autocomposizione codice", ls_messaggio + "IL CODICE POTREBBE ESSERE ERRATO !!", Information!)
end if

end subroutine

public function integer wf_item_changed (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
// ----------------------------------------------------------------------------------------
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore Enrico Menegotto
//    modificata il 31/08/1998
//    modificato il 30/07/1999 ptenda
// -----------------------------------------------------------------------------------------

string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale
string ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo, ls_nome_dw, &
		 ls_cod_versione_default
long li_row, li_null, ll_i
double ld_dim_hp
uo_itemchanged uo_itemchg

li_row = 1
setnull(li_null)

ls_nome_dw = fdw_passo.dataobject
uo_itemchg = CREATE uo_itemchanged
uo_itemchg.is_cod_modello = str_conf_prodotto.cod_modello
choose case ls_nome_dw
	case "d_ext_inizio"
		if uo_itemchg.uof_itemchanged_inizio(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_tessuto","d_ext_tessuto_dk","d_ext_tessuto_alu"
		choose case uo_itemchg.uof_itemchanged_tessuto(fdw_passo, fs_col_name, fs_data, fs_messaggio)
			case -1
				return -1
			case -2
				return -2
	end choose
	case "d_ext_vern_lastra"
		if uo_itemchg.uof_itemchanged_vern_lastra(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_comandi"
		if uo_itemchg.uof_itemchanged_comandi(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1

		end if
	case "d_ext_supporto_flags"
		if uo_itemchg.uof_itemchanged_supporto_flags(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_plisse"
		if uo_itemchg.uof_itemchanged_plisse(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_optional"
		if uo_itemchg.uof_itemchanged_optional(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_nota_prodotto_finito"
		if uo_itemchg.uof_itemchanged_nota_dettaglio(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
	case "d_ext_verniciature"
		if uo_itemchg.uof_itemchanged_verniciature(fdw_passo, fs_col_name, fs_data, fs_messaggio) = -1 then
			return -1
		end if
end choose
destroy uo_itemchg


if fs_col_name = "rs_cod_modello_manuale" or fs_col_name = "rs_cod_modello" then
	string ls_errore
	datetime ldt_oggi
	
	if fs_data <> fdw_passo.getitemstring(li_row,"rs_cod_modello_manuale") or fs_data <> fdw_passo.getitemstring(li_row,"rs_cod_modello") then
		wf_azzera_variabili()
	end if
	
	ldt_oggi = datetime(today(), 00:00:00)
	fdw_passo.setitem(li_row, "rs_des_prodotto", "")
	fdw_passo.setitem(li_row, "rn_prog_des_modello", li_null)
	fdw_passo.setitem(li_row, "rs_cod_versione", "")
	str_conf_prodotto.cod_modello = fs_data
	f_PO_LoadDDDW_DW(dw_inizio,"rs_cod_versione",sqlca,&
						  "distinta_padri","cod_versione","des_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + fs_data + "' and " + &
						  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")			 
						  
	select cod_versione
	into   :ls_cod_versione_default
	from   distinta_padri
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :fs_data and
			 flag_predefinita = 'S' and
			 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
	if sqlca.sqlcode = 0 and isnull(str_conf_prodotto.cod_versione) then
		fdw_passo.setitem(li_row, "rs_cod_versione", ls_cod_versione_default)
		str_conf_prodotto.cod_versione = ls_cod_versione_default
	end if
	
	if not isnull(str_conf_prodotto.cod_versione) and str_conf_prodotto.cod_versione<>"" then
		fdw_passo.setitem(li_row, "rs_cod_versione", str_conf_prodotto.cod_versione)
	else
		g_mb.warning("Non è stato possibile recuperare la versione di default del prodotto. Ricordati di inserirla prima di andare avanti!")
	end if
	wf_gest_mp_non_richieste(fdw_passo.getitemnumber(1, "rd_dimensione_1"), &
									   fdw_passo.getitemnumber(1, "rd_dimensione_2"), &
									   fdw_passo.getitemnumber(1, "rd_dimensione_3"), &
									   fdw_passo.getitemnumber(1, "rd_dimensione_4"), &
									   0)

//	wf_init_cod_prodotto(fs_data)
	select cod_tab_rapp_misure,
			cod_iniziale,
			flag_tessuto,
			flag_tipo_mantovana,
			flag_altezza_mantovana,
			flag_tessuto_mantovana,
			flag_cordolo,
			flag_passamaneria,
			flag_verniciatura,
			flag_verniciatura_2,
			flag_verniciatura_3,
			flag_spessore_lastra,
			flag_colore_lastra,
			flag_tipo_lastra,
			flag_comando,
			flag_pos_comando,
			flag_alt_asta,
			flag_supporto,
			flag_kit_laterale,
			flag_luce_finita,
			cod_rollo,
			flag_rollo,
			cod_cappotta_frontale,
			flag_cappotta_frontale,
			flag_num_gambe_campate,
			flag_decoro,
			flag_scritta,
			flag_plisse,
			flag_prezzo_superficie,
			flag_secondo_comando,
			flag_uso_quan_distinta,
			cod_gruppo_var_quan_distinta
	 into :istr_flags.cod_tab_rapp_misure,
			:istr_flags.cod_iniziale,
			:istr_flags.flag_tessuto,
			:istr_flags.flag_tipo_mantovana,
			:istr_flags.flag_altezza_mantovana,
			:istr_flags.flag_tessuto_mantovana,
			:istr_flags.flag_cordolo,
			:istr_flags.flag_passamaneria,
			:istr_flags.flag_verniciatura,
			:istr_flags.flag_verniciatura_2,
			:istr_flags.flag_verniciatura_3,
			:istr_flags.flag_spessore_lastra,
			:istr_flags.flag_colore_lastra,
			:istr_flags.flag_tipo_lastra,
			:istr_flags.flag_comando,
			:istr_flags.flag_pos_comando,
			:istr_flags.flag_alt_asta,
			:istr_flags.flag_supporto,
			:istr_flags.flag_kit_laterale,
			:istr_flags.flag_luce_finita,
			:istr_flags.cod_rollo,
			:istr_flags.flag_rollo,
			:istr_flags.cod_cappotta_frontale,
			:istr_flags.flag_cappotta_frontale,
			:istr_flags.flag_num_gambe_campate,
			:istr_flags.flag_decoro,
			:istr_flags.flag_scritta,
			:istr_flags.flag_plisse,
			:istr_flags.flag_prezzo_superficie,
			:istr_flags.flag_secondo_comando,
			:istr_flags.flag_uso_quan_distinta,
			:istr_flags.cod_gruppo_var_quan_distinta
	 from tab_flags_configuratore
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_modello = :str_conf_prodotto.cod_modello;

	if SQLCA.sqlcode = 100 then
		fs_messaggio = "Tabella dei Flags non Completata per Questo Modello di Tenda"
		return -1
	elseif SQLCA.sqlcode = -1 then
		fs_messaggio = "Errore durante l'Estrazione Tabella Flags per Questo Modello di Tenda"
		return -1
	end if
	
	wf_costruisci_passi_new()
	
	if not isnull(istr_flags.flag_prezzo_superficie) then
		fdw_passo.setitem(li_row, "rs_flag_prezzo_superficie", istr_flags.flag_prezzo_superficie)
	end if
//------------------------------------  FLAG LUCE FINITA -----------------------------------

	if istr_flags.flag_luce_finita = "S" then
		fdw_passo.object.rs_flag_misura_luce_finita.protect = 0
	elseif istr_flags.flag_luce_finita = "N" then
		fdw_passo.object.rs_flag_misura_luce_finita.protect = 1
		istr_riepilogo.flag_misura_luce_finita = "F"
	else
		fs_messaggio = "Errore durante l'Estrazione Descrizioni Variabili"
		return -1
	end if

//------------------------------------  DIMENSIONE 1  --------------------------------------
	istr_flags.flag_dimensione_1 = "N"
	istr_flags.flag_dimensione_2 = "N"
	istr_flags.flag_dimensione_3 = "N"
	istr_flags.flag_dimensione_4 = "N"
	istr_flags.flag_dimensione_5 = "N"
	istr_flags.flag_dimensione_6 = "N"
	istr_flags.flag_dimensione_7 = "N"
	istr_flags.flag_dimensione_8 = "N"
	istr_flags.flag_dimensione_9 = "N"
	
	for ll_i = 1 to 9
		fdw_passo.setitem(1, "rd_dimensione_" + string(ll_i), 0)			
		if wf_abilita_dimensioni( fs_data, ll_i) then 
			choose case ll_i
				case 1
					istr_flags.flag_dimensione_1 = "S"
				case 2
					istr_flags.flag_dimensione_2 = "S"
				case 3
					istr_flags.flag_dimensione_3 = "S"
				case 4
					istr_flags.flag_dimensione_4 = "S"
				case 5
					istr_flags.flag_dimensione_5 = "S"
				case 6
					istr_flags.flag_dimensione_6 = "S"
				case 7
					istr_flags.flag_dimensione_7 = "S"
				case 8
					istr_flags.flag_dimensione_8 = "S"
				case 9
					istr_flags.flag_dimensione_9 = "S"
			end choose
		end if
	next
					
/*
	select des_variabile
	 into :ls_des_dim_x  
	 from tab_des_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :fs_data and
			flag_ipertech_apice='S' and
			cod_variabile in ( select cod_variabile
										from tab_variabili_formule
										where cod_azienda = :s_cs_xx.cod_azienda and 
												nome_campo_database = 'dim_x');

	if SQLCA.sqlcode = 0 then
		fdw_passo.object.rd_dimensione_1_t.color = 8388608
		fdw_passo.object.rd_dimensione_1_t.text = ls_des_dim_x
		fdw_passo.object.rd_dimensione_1.background.color = 16777215
		fdw_passo.object.rd_dimensione_1.protect = 0
		istr_flags.flag_dimensione_1 = "S"
	elseif SQLCA.sqlcode = 100 then
		fdw_passo.object.rd_dimensione_1_t.color = 8421504
		fdw_passo.object.rd_dimensione_1_t.text = "Dimensione 1:"
		fdw_passo.object.rd_dimensione_1.background.color = 79741120
		fdw_passo.object.rd_dimensione_1.protect = 1	
		istr_flags.flag_dimensione_1 = "N"
	elseif SQLCA.sqlcode = -1 then
		istr_flags.flag_dimensione_1 = "N"
		fs_messaggio = "Errore durante l'Estrazione Descrizioni Variabili"
		return -1
	end if
//-----------------------------------  DIMENSIONE 2  ---------------------------------------
	fdw_passo.setitem(1, "rd_dimensione_2", 0)			
	
	select des_variabile
	 into :ls_des_dim_y
	 from tab_des_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :fs_data and
			flag_ipertech_apice='S' and
			cod_variabile in ( select cod_variabile
										from tab_variabili_formule
										where cod_azienda = :s_cs_xx.cod_azienda and 
												nome_campo_database = 'dim_y');

	if SQLCA.sqlcode = 0 then
		istr_flags.flag_dimensione_2 = "S"
		fdw_passo.object.rd_dimensione_2_t.color = 8388608
		fdw_passo.object.rd_dimensione_2_t.text = ls_des_dim_y
		fdw_passo.object.rd_dimensione_2.background.color = 16777215
		fdw_passo.object.rd_dimensione_2.protect = 0
	elseif SQLCA.sqlcode = 100 then
		istr_flags.flag_dimensione_2 = "N"
		fdw_passo.object.rd_dimensione_2_t.color = 8421504
		fdw_passo.object.rd_dimensione_2_t.text = "Dimensione 2:"
		fdw_passo.object.rd_dimensione_2.background.color = 79741120
		fdw_passo.object.rd_dimensione_2.protect = 1	
	elseif SQLCA.sqlcode = -1 then
		istr_flags.flag_dimensione_2 = "N"
		fs_messaggio = "Errore durante l'Estrazione Descrizioni Variabili"
		return -1
	end if

//---------------------------------  DIMENSIONE 3  -----------------------------------------
	fdw_passo.setitem(1, "rd_dimensione_3", 0)			
	
	select des_variabile
	 into :ls_des_dim_z
	 from tab_des_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :fs_data and
			flag_ipertech_apice='S' and
			cod_variabile in ( select cod_variabile
										from tab_variabili_formule
										where cod_azienda = :s_cs_xx.cod_azienda and 
												nome_campo_database = 'dim_z');

	if SQLCA.sqlcode = 0 then
		istr_flags.flag_dimensione_3 = "S"
		fdw_passo.object.rd_dimensione_3_t.color = 8388608
		fdw_passo.object.rd_dimensione_3_t.text = ls_des_dim_z
		fdw_passo.object.rd_dimensione_3.background.color = 16777215
		fdw_passo.object.rd_dimensione_3.protect = 0
	elseif SQLCA.sqlcode = 100 then
		istr_flags.flag_dimensione_3 = "N"
		fdw_passo.object.rd_dimensione_3_t.color = 8421504
		fdw_passo.object.rd_dimensione_3_t.text = "Dimensione 3:"
		fdw_passo.object.rd_dimensione_3.background.color = 79741120
		fdw_passo.object.rd_dimensione_3.protect = 1	
	elseif SQLCA.sqlcode = -1 then
		istr_flags.flag_dimensione_3 = "N"
		fs_messaggio = "Errore durante l'Estrazione Descrizioni Variabili"
		return -1
	end if

//----------------------------------- DIMENSIONE 4  -----------------------------------------
	fdw_passo.setitem(1, "rd_dimensione_4", 0)			
	
	select des_variabile
	 into :ls_des_dim_t
	 from tab_des_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :fs_data and
			flag_ipertech_apice='S' and
			cod_variabile in ( select cod_variabile
										from tab_variabili_formule
										where cod_azienda = :s_cs_xx.cod_azienda and 
												nome_campo_database = 'dim_t');

	if SQLCA.sqlcode = 0 then
		istr_flags.flag_dimensione_4 = "S"
		fdw_passo.object.rd_dimensione_4_t.color = 8388608
		fdw_passo.object.rd_dimensione_4_t.text = ls_des_dim_t
		fdw_passo.object.rd_dimensione_4.background.color = 16777215
		fdw_passo.object.rd_dimensione_4.protect = 0
	elseif SQLCA.sqlcode = 100 then
		istr_flags.flag_dimensione_4 = "N"
		fdw_passo.object.rd_dimensione_4_t.color = 8421504
		fdw_passo.object.rd_dimensione_4_t.text = "Dimensione 4:"
		fdw_passo.object.rd_dimensione_4.background.color = 79741120
		fdw_passo.object.rd_dimensione_4.protect = 1	
	elseif SQLCA.sqlcode = -1 then
		istr_flags.flag_dimensione_4 = "N"
		fs_messaggio = "Errore durante l'Estrazione Descrizioni Variabili"
		return -1
	end if
*/
	
//------------------------------------ LINEE SCONTO  ---------------------------------------------	
	string ls_messaggio
	double ld_gruppi_sconti[], ld_provv_1, ld_provv_2
	uo_gruppi_sconti luo_gruppi_sconti
	
	for ll_i = 1 to 10
			ld_gruppi_sconti[ll_i] = 0
	next
	st_2.text = " Ricerca linee di sconto ......."
	luo_gruppi_sconti = CREATE uo_gruppi_sconti
	luo_gruppi_sconti.fuo_ricerca_condizioni(dw_inizio.getitemstring(1, "rs_cod_modello"), &
															str_conf_prodotto.cod_cliente, &
															str_conf_prodotto.cod_valuta, &
															str_conf_prodotto.cod_agente_1, &
															str_conf_prodotto.data_documento, &
															ld_gruppi_sconti[], &
															ld_provv_1, &
															ld_provv_2, &
															ls_messaggio)
	st_sconti.text = ""														
	if upperbound(ld_gruppi_sconti) > 0 or not isnull(upperbound(ld_gruppi_sconti)) then
		for ll_i = 1 to 10
			if ld_gruppi_sconti[ll_i] > 0 then
				if ll_i > 0 and st_sconti.text <> "" then st_sconti.text = st_sconti.text + " + "
				st_sconti.text = st_sconti.text + string(ld_gruppi_sconti[ll_i], "##0.0#")
			end if
		next
	end if	
end if
st_2.text = " "
//st_cod_prodotto_finito.text = " Codice Prodotto :  " + str_conf_prodotto.cod_prodotto_finito
return 0

end function

public function integer wf_crea_addizionali_passo (datawindow fdw_passo);/*	Funzione che verifica se per la DW passata come parametro ci sono dei campi addizionali, in
		tal caso aggiunge una riga al documento della gestione corrente

nome: wf_crea_addizionali_passo
tipo: integer
  
Variabili passate: 			nome					 tipo				passaggio per			commento
								 	fdw_passo			 DataWindow		valore

Creata il 31-08-98 
Autore Enrico Menegotto

modificata il 31/08/1998
    aggiunto gestione standard listini EnMe 6/12/1999

modificato il 17/07/2000 da Enrico
    sistemato creazione riga addizionale EnMe
	 
EnMe 23/5/2001	 
	sistemato caricamento esenzione iva da anag_clienti   
	
EnMe 03/02/2009
	Implementato la funzione per adeguamento alla gestione offerte


*/
string ls_cod_prodotto_1, ls_cod_prodotto_2, ls_cod_prodotto_3, ls_nota_1, ls_nota_2, ls_nota_3, ls_addizionale_1, &
		 ls_addizionale_2, ls_addizionale_3, ls_cod_non_a_magazzino, ls_cod_misura, ls_cod_iva, ls_tabella, &
		 ls_progressivo, ls_sql_ins, ls_cod_prodotto, ls_nota, ls_cod_tipo_dettaglio_vendita, ls_cod_tipo_det_ven_1, &
		 ls_cod_tipo_det_ven_2, ls_cod_tipo_det_ven_std, ls_des_prodotto, ls_cod_versione, ls_data_consegna,&
		 ls_cod_tipo_det_ven, ls_sql, ls_provvigione_1, ls_provvigione_2, ls_flag_doc_suc_or, ls_flag_st_note_or, &
		 ls_flag_gen_commessa, ls_flag_presso_cg, ls_flag_presso_pt, ls_cod_iva_esente,ls_cod_prodotto_raggruppato, &
		 ls_tabella_varianti, ls_quantita, ls_cod_iva_det_ven, ls_cod_tipo_ord_ven

long   ll_i, ll_max, ll_num_addizionali, ll_numero, ll_add

dec{5} ld_fat_conversione_ven

dec{4} ld_prezzo_vendita, ld_quan_prodotto, ld_sconti[], ld_provvigione_1, ld_provvigione_2, ld_quan_impegnata, ld_cambio_ven

datetime ldt_data_esenzione_iva

ls_cod_prodotto_1 = ""
ls_cod_prodotto_2 = ""
ls_cod_prodotto_3 = ""
ls_nota_1 = ""
ls_nota_2 = ""
ls_nota_3 = ""
ls_addizionale_1 = "N"
ls_addizionale_2 = "N"
ls_addizionale_3 = "N"
ll_add = 0


choose case str_conf_prodotto.tipo_gestione
		
	case "ORD_VEN"
		ls_tabella = "det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_varianti = "varianti_det_ord_ven"
		ls_quantita 	= "quan_ordine"
		
	case "OFF_VEN"
		ls_tabella = "det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_varianti = "varianti_det_off_ven"
		ls_quantita 	= "quan_offerta"
		
end choose


// trovo il tipo dettaglio opportuno da usare per le addizionali

setnull(ls_cod_tipo_det_ven_std)

if str_conf_prodotto.tipo_gestione = "ORD_VEN" then

	select 	cod_tipo_ord_ven
	into   	:ls_cod_tipo_ord_ven
	from   	tes_ord_ven
	where  	cod_azienda = :s_cs_xx.cod_azienda and
			 	anno_registrazione = :str_conf_prodotto.anno_documento and
			 	num_registrazione  = :str_conf_prodotto.num_documento;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipi ordini dalla testata. Dettaglio errore "+sqlca.sqlerrtext)
		ROLLBACK;
		return -1
	end if
	
	if isnull(ls_cod_tipo_ord_ven) then
		g_mb.messagebox("Configuratore", "Tipo Ordine non impostato nella testata ordine." )
		ROLLBACK;
		return -1
	end if
	
	select cod_tipo_det_ven_addizionali
	into 	:ls_cod_tipo_det_ven_std
	from 	tab_flags_conf_tipi_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello and 
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		ROLLBACK;
		return -1
	end if
	
end if

if isnull(ls_cod_tipo_det_ven_std) then
	select cod_tipo_det_ven_addizionali
	into   :ls_cod_tipo_det_ven_std
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		ROLLBACK;
		return -1
	end if
	if isnull(ls_cod_tipo_det_ven_std) then
		g_mb.messagebox("Configuratore","Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella parametri configuratore")
		ROLLBACK;
		return -1
	end if
end if


choose case fdw_passo.dataobject
	case "d_ext_tessuto","d_ext_tessuto_dk","d_ext_tessuto_alu"
		if istr_flags.flag_tessuto = "S" then
			ls_addizionale_1 = fdw_passo.getitemstring(1, "rs_tess_addizionale")
			if ls_addizionale_1 = "S" then
				ls_cod_prodotto_1 = fdw_passo.getitemstring(1, "rs_cod_tessuto")
				ls_cod_non_a_magazzino = fdw_passo.getitemstring(1, "rs_cod_non_a_magazzino")
				ls_nota_1 = "Tessuto: " + ls_cod_prodotto_1 + ", Cod non a Magazzino: " + ls_cod_non_a_magazzino
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_1
			end if
		end if
	case "d_ext_comandi"
		if istr_flags.flag_comando = "S" then
			ls_addizionale_1 = fdw_passo.getitemstring(1, "rs_com_flag_addizionale")
			if ls_addizionale_1 = "S" then
				ls_cod_prodotto_1 = fdw_passo.getitemstring(1, "rs_cod_comando")
				ls_nota_1 = "Comando: " + ls_cod_prodotto_1
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_1
			end if
		end if
		if istr_flags.flag_secondo_comando = "S" then
			ls_addizionale_2 = fdw_passo.getitemstring(1, "rs_com_2_flag_addizionale")
			if ls_addizionale_2 = "S" then
				ls_cod_prodotto_2 = fdw_passo.getitemstring(1, "rs_cod_comando_2")
				ls_nota_2 = "Secondo Comando: " + ls_cod_prodotto_2
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_2
			end if
		end if
	case "d_ext_vern_lastra"
		if istr_flags.flag_colore_lastra = "S" then
			ls_addizionale_1 = fdw_passo.getitemstring(1, "rs_col_addizionale")
			if ls_addizionale_1 = "S" then
				ls_cod_prodotto_1 = fdw_passo.getitemstring(1, "rs_cod_colore_lastra")
				ls_nota_1 = "Colore Lastra: " + ls_cod_prodotto_1
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_1
			end if
		end if
		if istr_flags.flag_verniciatura = "S" then
			ls_addizionale_2 = fdw_passo.getitemstring(1, "rs_vern_addizionale")
			if ls_addizionale_2 = "S" then
				ls_cod_prodotto_2 = fdw_passo.getitemstring(1, "rs_cod_verniciatura")
				ls_nota_2 = "Verniciatura: " + ls_cod_prodotto_2
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_2
			end if
		end if
	case "d_ext_supporto_flags"
		ls_addizionale_1 = fdw_passo.getitemstring(1, "rs_flag_addizionale")
		if ls_addizionale_1 = "S" then
			ls_cod_prodotto_1 = fdw_passo.getitemstring(1, "rs_cod_supporto")
			ls_nota_1 = "Supporto: " + ls_cod_prodotto_1
			ll_add ++
			istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_1
		end if
		ls_addizionale_2 = fdw_passo.getitemstring(1, "rs_flag_rollo")
		if ls_addizionale_2 = "S" then
			ls_cod_prodotto_2 = istr_flags.cod_rollo
			ls_nota_2 = "Rollò: " + ls_cod_prodotto_2
			ll_add ++
			istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_2
		end if
		ls_addizionale_3 = fdw_passo.getitemstring(1, "rs_flag_cappotta_frontale")
		if ls_addizionale_3 = "S" then
			ls_cod_prodotto_3 = istr_flags.cod_cappotta_frontale
			ls_nota_3 = "Cappotta Frontale: " + ls_cod_prodotto_3
			ll_add ++
			istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_3
		end if
	case "d_ext_verniciature"
		if istr_flags.flag_verniciatura = "S" then
			ls_addizionale_1 = fdw_passo.getitemstring(1, "rs_vern_addizionale")
			if ls_addizionale_1 = "S" then
				ls_cod_prodotto_1 = fdw_passo.getitemstring(1, "rs_cod_verniciatura")
				ls_nota_1 = "Verniciatura: " + ls_cod_prodotto_1
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_1
			end if
		end if
		if istr_flags.flag_verniciatura_2 = "S" then
			ls_addizionale_2 = fdw_passo.getitemstring(1, "rs_vern_addizionale_2")
			if ls_addizionale_2 = "S" then
				ls_cod_prodotto_2 = fdw_passo.getitemstring(1, "rs_cod_verniciatura_2")
				ls_nota_2 = "Verniciatura 2: " + ls_cod_prodotto_2
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_2
			end if
		end if
		if istr_flags.flag_verniciatura_3 = "S" then
			ls_addizionale_3 = fdw_passo.getitemstring(1, "rs_vern_addizionale_3")
			if ls_addizionale_3 = "S" then
				ls_cod_prodotto_3 = fdw_passo.getitemstring(1, "rs_cod_verniciatura_3")
				ls_nota_2 = "Verniciatura 3: " + ls_cod_prodotto_3
				ll_add ++
				istr_riepilogo.cod_prodotto_addizionale[ll_add] = ls_cod_prodotto_3
			end if
		end if
end choose

// nessuna addizione allora fuori rapidamente
if ls_addizionale_1 = "N" and ls_addizionale_2 = "N" and ls_addizionale_3 = "N" then return 0

// ------------------------------------- leggo la struttura della distinta ------------------------------------
string ls_materie_prime[], ls_messaggi
dec{4} ld_quantita[]
str_distinta lstr_distinta[]

f_esplodi_distinta_varianti ( dw_inizio.getitemstring(1,"rs_cod_modello"), &
										dw_inizio.getitemstring(1,"rs_cod_versione"), &
										ls_materie_prime[], &
										ld_quantita[], &
										1, &
										str_conf_prodotto.anno_documento, &
										str_conf_prodotto.num_documento, &
										str_conf_prodotto.prog_riga_documento, &
										ls_tabella_varianti, &
										lstr_distinta[], &
										ls_messaggi )
// -------------------------------------------------------------------------------------------------------------

for ll_num_addizionali = 1 to 3
	if (ls_addizionale_1 = "S" and ls_cod_prodotto_1 <> "" and not isnull(ls_cod_prodotto_1) and ll_num_addizionali = 1) or &
		(ls_addizionale_2 = "S" and ls_cod_prodotto_2 <> "" and not isnull(ls_cod_prodotto_2) and ll_num_addizionali = 2) or &
		(ls_addizionale_3 = "S" and ls_cod_prodotto_3 <> "" and not isnull(ls_cod_prodotto_3) and ll_num_addizionali = 3) then
		
		if ll_num_addizionali = 1 then		
			ls_cod_prodotto = ls_cod_prodotto_1
			if ls_cod_tipo_det_ven_1 <> "" and not isnull(ls_cod_tipo_det_ven_1) then
				ls_cod_tipo_dettaglio_vendita = ls_cod_tipo_det_ven_1
			else
				ls_cod_tipo_dettaglio_vendita = ls_cod_tipo_det_ven_std
			end if
		elseif ll_num_addizionali = 2 then
			ls_cod_prodotto = ls_cod_prodotto_2
			if ls_cod_tipo_det_ven_1 <> "" and not isnull(ls_cod_tipo_det_ven_1) then
				ls_cod_tipo_dettaglio_vendita = ls_cod_tipo_det_ven_1
			else
				ls_cod_tipo_dettaglio_vendita = ls_cod_tipo_det_ven_std
			end if
		elseif ll_num_addizionali = 3 then
			ls_cod_prodotto = ls_cod_prodotto_3
			ls_cod_tipo_dettaglio_vendita = ls_cod_tipo_det_ven_std
		end if			
		
		select cod_misura_ven,
				 fat_conversione_ven,
				 cod_iva,
				 prezzo_vendita,
				 des_prodotto
		into	 :ls_cod_misura,
				 :ld_fat_conversione_ven,
				 :ls_cod_iva,
				 :ld_prezzo_vendita,
				 :ls_des_prodotto
		from	 anag_prodotti
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Prodotto Addizionale non corretto")
			return -1
		end if
		
		// --------------------------   aggiunto da enrico 23-5-2001 --------------------------- //
		
		if not isnull(str_conf_prodotto.cod_contatto) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva_esente,
					 :ldt_data_esenzione_iva
			from   anag_contatti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_contatto = :str_conf_prodotto.cod_contatto;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_iva_esente) and ls_cod_iva_esente <> "" and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
					ls_cod_iva = ls_cod_iva_esente
				end if
			end if
		elseif not isnull(str_conf_prodotto.cod_cliente) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva_esente,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_cliente = :str_conf_prodotto.cod_cliente;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_iva_esente) and ls_cod_iva_esente <> "" and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
					ls_cod_iva = ls_cod_iva_esente
				end if
			end if
		end if
		// ^^^^^ ----------  fine aggiunta enrico ----------------------------------------------- //
		
		//  -----------------------  MODIFICA STAMPA NOTE - GEN_COMMESSA - PRESSO PT E PRESSO CG --------------------
		select flag_doc_suc_or, flag_st_note_or
		into   :ls_flag_doc_suc_or, :ls_flag_st_note_or
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_dettaglio_vendita;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore","Errore in ricerca flag_note in tipo dettaglio prodotto. Dettaglio errore "+sqlca.sqlerrtext)
			ROLLBACK;
			return -1
		end if

		ls_flag_gen_commessa = "S"
		ls_flag_presso_cg = "N"
		ls_flag_presso_pt = "N"
		uo_default_prodotto luo_default_prodotto
		luo_default_prodotto = CREATE uo_default_prodotto
		if luo_default_prodotto.uof_flag_gen_commessa(ls_cod_prodotto) = 0 then
			ls_flag_gen_commessa = luo_default_prodotto.is_flag_gen_commessa
			ls_flag_presso_cg = luo_default_prodotto.is_flag_presso_cg
			ls_flag_presso_pt = luo_default_prodotto.is_flag_presso_pt
		else
			ls_flag_gen_commessa = "S"
			ls_flag_presso_cg = "N"
			ls_flag_presso_pt = "N"
		end if
		destroy luo_default_prodotto
		
		//  -----------------------  calcolo del prezzo addizionale  -----------------------------
		string ls_null
		long ll_y
		
		setnull(ls_null)
		
		// ------------------  MODIFICA DI ENRICO PER PTENDA ------------------------------------------------
		// CERCO NON SOLO LE MATERIE PRIME COME IN PRECEDENZA, MA CERCO IN TUTTA LA DISTINTA L'ADDIZIONALE; INFATTI POTREBBE 
		// ANCHE ESSERE UN SEMILAVORATO COME UN SUPPORTO
		
		// VECCHIO SCRIPT 
//		ll_max = upperbound(ls_materie_prime)
//		if ll_max > 0 then
//			// controllo se ci sono delle materie prime segnate come addizionali; nel caso ci siano cerco la loro
//			// quantità in distinta per moltiplicarla per la quantità del prodotto finito e poi cerco il loro prezzo
//			// nei listini di vendita.
//			for ll_i = 1 to ll_max
//				if ls_materie_prime[ll_i] = ls_cod_prodotto then
//					// QUANTITA' ADDIZIONALE = Q.TA UTILIZZO * Q.TA PRODOTTO FINITO
//					ld_quan_prodotto = ld_quantita[ll_i] * w_configuratore.istr_riepilogo.quan_vendita
//					exit
//				end if
//				ld_quan_prodotto = w_configuratore.istr_riepilogo.quan_vendita
//			next
//		end if

//		NUOVO SCRIPT
		ll_max = upperbound(lstr_distinta)
		if ll_max > 0 then
			// controllo se ci sono delle materie prime segnate come addizionali; nel caso ci siano cerco la loro
			// quantità in distinta per moltiplicarla per la quantità del prodotto finito e poi cerco il loro prezzo
			// nei listini di vendita.
			for ll_i = 1 to ll_max
				if lstr_distinta[ll_i].cod_figlio = ls_cod_prodotto then
					// QUANTITA' ADDIZIONALE = Q.TA UTILIZZO * Q.TA PRODOTTO FINITO
					ld_quan_prodotto = lstr_distinta[ll_i].quan_utilizzo * w_configuratore.istr_riepilogo.quan_vendita
					exit
				end if
				// se l'addizionale non è presente in distinta allora prendo la quantità del prodotto finito
				ld_quan_prodotto = w_configuratore.istr_riepilogo.quan_vendita
			next
		end if
		
		//  FINE MODIFICA QUANTITA' ADDIZIONALI
		// ----------------------------------------------------------------------------------------
		ll_y = 0		
		iuo_condizioni_cliente.str_parametri.ldw_oggetto = dw_inizio
		iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = str_conf_prodotto.cod_tipo_listino_prodotto
		iuo_condizioni_cliente.str_parametri.cod_valuta = str_conf_prodotto.cod_valuta
		iuo_condizioni_cliente.str_parametri.cambio_ven = 1
		iuo_condizioni_cliente.str_parametri.data_riferimento = str_conf_prodotto.data_documento
		iuo_condizioni_cliente.str_parametri.cod_cliente = str_conf_prodotto.cod_cliente
		iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		iuo_condizioni_cliente.str_parametri.dim_1 = 0
		iuo_condizioni_cliente.str_parametri.dim_2 = 0
		iuo_condizioni_cliente.str_parametri.quantita = ld_quan_prodotto
		iuo_condizioni_cliente.str_parametri.valore = 0
		iuo_condizioni_cliente.str_parametri.cod_agente_1 = str_conf_prodotto.cod_agente_1
		iuo_condizioni_cliente.str_parametri.colonna_quantita = ""
		iuo_condizioni_cliente.str_parametri.colonna_prezzo = ""
		iuo_condizioni_cliente.ib_setitem = false
		iuo_condizioni_cliente.ib_setitem_provvigioni = false
		iuo_condizioni_cliente.wf_condizioni_cliente()
	
		if upperbound(iuo_condizioni_cliente.str_output.variazioni) > 0 and not isnull(upperbound(iuo_condizioni_cliente.str_output.variazioni)) then
			ld_prezzo_vendita = iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
		end if
		
//		if str_conf_prodotto.stato = "N" then
			ld_provvigione_1 = iuo_condizioni_cliente.str_output.provvigione_1
			ld_provvigione_2 = iuo_condizioni_cliente.str_output.provvigione_2
//		else
//			ld_provvigione_1 = str_conf_prodotto.provvigione_1
//			ld_provvigione_2 = str_conf_prodotto.provvigione_2
//		end if
		if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
		if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
	
		// ------------------------------------- calcolo del valore riga ----------------------------------------
		dec{4} ld_sconto_pagamento, ld_sconto_tot, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
				 ld_val_sconto_pagamento, ld_val_riga_netto, ld_tot_val_documento, ld_sconto_testata
		string ls_flag_tipo_det_ven, ls_lire, ls_flag_sola_iva, ls_cod_pagamento
				 
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'CVL';
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", exclamation!, ok!)
			return -1
		end if

		choose case str_conf_prodotto.tipo_gestione

			case "ORD_VEN"

				select cod_pagamento,
						 sconto
				into   :ls_cod_pagamento,
						 :ld_sconto_testata
				from   tes_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :str_conf_prodotto.anno_documento and
						 num_registrazione = :str_conf_prodotto.num_documento ;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Errore in ricerca dati di testata documento.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
					return -1
				end if

			case "OFF_VEN"

				select cod_pagamento,
						 sconto
				into   :ls_cod_pagamento,
						 :ld_sconto_testata
				from   tes_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :str_conf_prodotto.anno_documento and
						 num_registrazione = :str_conf_prodotto.num_documento ;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Errore in ricerca dati di testata documento.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
					return -1
				end if

		end choose


		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
	
		select flag_tipo_det_ven,
				 flag_sola_iva,
				 cod_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_flag_sola_iva,
				 :ls_cod_iva_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_dettaglio_vendita;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento.", &
						  exclamation!, ok!)
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			iuo_condizioni_cliente.uof_calcola_valore_sconto(iuo_condizioni_cliente.str_output.sconti[],ld_prezzo_vendita  * ld_quan_prodotto, ld_sconto_tot, ld_val_riga_netto)
//			f_calcola_sconto_decimal(iuo_condizioni_cliente.str_output.sconti[],ld_prezzo_vendita  * ld_quan_prodotto, ld_sconto_tot, ld_val_riga_netto)
	
			if str_conf_prodotto.cod_valuta = ls_lire then
				ld_val_riga_netto = round(ld_val_riga_netto, 0)
			else
				ld_val_riga_netto = round(ld_val_riga_netto,2)
			end if   
	
			if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" or ls_flag_tipo_det_ven = "N" then
//				ld_val_riga_netto = ld_val_riga_netto - ((ld_val_riga_netto * ld_sconto_testata) / 100)
//				ld_val_riga_netto = ld_val_riga_netto - ((ld_val_riga_netto * ld_sconto_pagamento) / 100)
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_riga_netto = ld_val_riga_netto * -1
			else
				ld_val_riga_netto = ld_val_riga_netto
			end if
			
			choose case str_conf_prodotto.tipo_gestione
				case "ORD_VEN"
				
					update tes_ord_ven 
					set tot_val_ordine = tot_val_ordine + :ld_val_riga_netto
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :str_conf_prodotto.anno_documento and
							 num_registrazione = :str_conf_prodotto.num_documento ;
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Attenzione", "Errore in ricerca dati di testata documento.~r~r" + SQLCA.SQLERRTEXT, exclamation!, ok!)
						return -1
					end if
					
				case "OFF_VEN"
				
					update tes_off_ven 
					set tot_val_offerta = tot_val_offerta + :ld_val_riga_netto
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :str_conf_prodotto.anno_documento and
							 num_registrazione = :str_conf_prodotto.num_documento ;
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("Attenzione", "Errore in ricerca dati di testata documento.~r~r" + SQLCA.SQLERRTEXT, exclamation!, ok!)
						return -1
					end if
					
			end choose
					
		end if

		//  -----------------------  creazione del dettaglio ordine ------------------------------
		if isnull(ls_cod_iva) then
			ls_cod_iva = ls_cod_iva_det_ven
		end if
		
		
		select cod_versione
		into   :ls_cod_versione
		from   distinta_padri
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and
				 flag_predefinita = 'S';
				 
		if sqlca.sqlcode <> 0 then
			ls_cod_versione = "null"
		else
			ls_cod_versione = "'" + ls_cod_versione + "'"
		end if
		
		if ld_provvigione_1 = 0 or isnull(ld_provvigione_1) then 
			ls_provvigione_1 = "0"
		else
			ls_provvigione_1 = f_double_to_string(ld_provvigione_1)
			if isnull(ls_provvigione_1) then ls_provvigione_1 = "0"
		end if
		if ld_provvigione_2 = 0 or isnull(ld_provvigione_2) then 
			ls_provvigione_2 = "0"
		else
			ls_provvigione_2 = f_double_to_string(ld_provvigione_2)
			if isnull(ls_provvigione_2) then ls_provvigione_2 = "0"
		end if

		if ls_cod_prodotto = "" or isnull(ls_cod_prodotto) then
			ls_cod_prodotto = "null"
		else
			ls_cod_prodotto = "'" + ls_cod_prodotto + "'"
		end if

		if ls_des_prodotto = "" or isnull(ls_des_prodotto) then
			ls_des_prodotto = "null"
		else
			ls_des_prodotto = "'" + ls_des_prodotto + "'"
		end if

		if ls_cod_tipo_dettaglio_vendita = "" or isnull(ls_cod_tipo_dettaglio_vendita) then
			ls_cod_tipo_det_ven = "null"
		else
			ls_cod_tipo_det_ven = "'" + ls_cod_tipo_dettaglio_vendita + "'"
		end if
		
		if ls_cod_misura = "" or isnull(ls_cod_misura) then
			ls_cod_misura = "null"
		else
			ls_cod_misura = "'" + ls_cod_misura + "'"
		end if
		
		if ls_cod_iva = "" or isnull(ls_cod_iva) then
			ls_cod_iva = "null"
		else
			ls_cod_iva = "'" + ls_cod_iva + "'"
		end if
		
		if ls_nota = "" or isnull(ls_nota) then
			ls_nota = "null"
		else
			ls_nota = "'" + ls_nota + "'"
		end if
		
		ld_sconti[] = iuo_condizioni_cliente.str_output.sconti[]
		
		ll_max=upperbound(iuo_condizioni_cliente.str_output.sconti)
		if isnull(ll_max) then ll_max = 0
		for ll_i = 1 to 10
			if ll_max >= ll_i then
				ld_sconti[ll_i] = iuo_condizioni_cliente.str_output.sconti[ll_i]
			else
				ld_sconti[ll_i] = 0
			end if
		next

		if isnull(str_conf_prodotto.data_consegna) then
			ls_data_consegna = " null"
		else
			ls_data_consegna = "'" + string(str_conf_prodotto.data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
		end if

		if ls_flag_doc_suc_or = "" or isnull(ls_flag_doc_suc_or) then
			ls_flag_doc_suc_or = "null"
		else
			ls_flag_doc_suc_or = "'" + ls_flag_doc_suc_or + "'"
		end if

		if ls_flag_st_note_or = "" or isnull(ls_flag_st_note_or) then
			ls_flag_st_note_or = "null"
		else
			ls_flag_st_note_or = "'" + ls_flag_st_note_or + "'"
		end if

		if ls_flag_gen_commessa = "" or isnull(ls_flag_gen_commessa) then
			ls_flag_gen_commessa = "null"
		else
			ls_flag_gen_commessa = "'" + ls_flag_gen_commessa + "'"
		end if
		
		if ls_flag_presso_pt = "" or isnull(ls_flag_presso_pt) then
			ls_flag_presso_pt = "null"
		else
			ls_flag_presso_pt = "'" + ls_flag_presso_pt + "'"
		end if

		if ls_flag_presso_cg = "" or isnull(ls_flag_presso_cg) then
			ls_flag_presso_cg = "null"
		else
			ls_flag_presso_cg = "'" + ls_flag_presso_cg + "'"
		end if
		
		choose case str_conf_prodotto.tipo_gestione
			case "ORD_VEN"
				str_conf_prodotto.ultima_riga_inserita ++
		
				select prog_riga_ord_ven
				into	:ll_numero
				from  det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :str_conf_prodotto.anno_documento and
						num_registrazione = :str_conf_prodotto.num_documento and
						prog_riga_ord_ven = :str_conf_prodotto.ultima_riga_inserita;
				if sqlca.sqlcode = 0 or sqlca.sqlcode = -1 then
					g_mb.messagebox("Configuratore di Prodotto", "Il numero di riga "+string(str_conf_prodotto.ultima_riga_inserita)+" esiste già nell'ordine; verificare!", exclamation!, ok!)
					rollback;
					return -1
				end if
				
			case "OFF_VEN"
				str_conf_prodotto.ultima_riga_inserita ++
		
				select prog_riga_off_ven
				into	:ll_numero
				from  det_off_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :str_conf_prodotto.anno_documento and
						num_registrazione = :str_conf_prodotto.num_documento and
						prog_riga_off_ven = :str_conf_prodotto.ultima_riga_inserita;
				if sqlca.sqlcode = 0 or sqlca.sqlcode = -1 then
					g_mb.messagebox("Configuratore di Prodotto", "Il numero di riga "+string(str_conf_prodotto.ultima_riga_inserita)+" esiste già nell'offerta; verificare!", exclamation!, ok!)
					rollback;
					return -1
				end if
				
		end choose
		
		ls_sql_ins =	"insert into " + ls_tabella + &
								" (cod_azienda, " + &
								"anno_registrazione, " + &
								"num_registrazione, " + &
								ls_progressivo + ", " + &
								"cod_prodotto, " + &
								"cod_tipo_det_ven, " + &
								"cod_misura, " + &
								"des_prodotto, " + &
								ls_quantita + ", " + &
								"prezzo_vendita, " + &
								"fat_conversione_ven, " + &
								"sconto_1, " + &
								"sconto_2, " + &
								"provvigione_1, " + &
								"provvigione_2, " + &
								"cod_iva, " + &
								"data_consegna, " + &
								"val_riga, " + &
								"imponibile_iva, " + &
								"nota_dettaglio, " + &
								"cod_centro_costo, " + &
								"sconto_3, " + &
								"sconto_4, " + &
								"sconto_5, " + &
								"sconto_6, " + &
								"sconto_7, " + &
								"sconto_8, " + &
								"sconto_9, " + &
								"sconto_10, " + &
								"num_confezioni, " + &
								"num_pezzi_confezione, " + &
								"num_riga_appartenenza, " + &
								"flag_gen_commessa, " + &
								"flag_presso_ptenda, " + &
								"flag_presso_cgibus, " + &
							   "flag_doc_suc_det, " + &  
							   "flag_st_note_det, " + &  
								"cod_versione " 
						
			if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
								ls_sql_ins += ", " + &
								"anno_registrazione_off, " + &
								"num_registrazione_off, " + &
								"prog_riga_off_ven, " + &
								"anno_commessa, " + &
								"num_commessa, " + &
								"quan_evasa, " + &
								"quan_in_evasione, " + &
								"flag_blocco, " + &
								"flag_evasione) "
				
			else
								ls_sql_ins += ") "
			end if
						
				ls_sql_ins +="VALUES ('" + s_cs_xx.cod_azienda + "', " + &
								string(str_conf_prodotto.anno_documento) + ", " + &
								string(str_conf_prodotto.num_documento) + ", " + &
								string(str_conf_prodotto.ultima_riga_inserita) + ", " + &
								ls_cod_prodotto + ", " + &
								ls_cod_tipo_det_ven + ", " + &
								ls_cod_misura + ", " + &
								ls_des_prodotto + ", " + &
								f_double_to_string(ld_quan_prodotto) + ", " + &
								f_double_to_string(ld_prezzo_vendita) + ", " + &
								f_double_to_string(ld_fat_conversione_ven) + ", " + &
								f_double_to_string(ld_sconti[1]) + ", " + &
								f_double_to_string(ld_sconti[2]) + ", " + &
								ls_provvigione_1 + ", " + &
								ls_provvigione_2 + ", " + &
								ls_cod_iva + ", " + &
								ls_data_consegna + ", " + &
								f_double_to_string(ld_val_riga_netto) + ", " + &
								f_double_to_string(ld_val_riga_netto) + ", " + &
								ls_nota + ", " + &
								"null, " + &
								f_double_to_string(ld_sconti[3]) + ", " + &
								f_double_to_string(ld_sconti[4]) + ", " + &
								f_double_to_string(ld_sconti[5]) + ", " + &
								f_double_to_string(ld_sconti[6]) + ", " + &
								f_double_to_string(ld_sconti[7]) + ", " + &
								f_double_to_string(ld_sconti[8]) + ", " + &
								f_double_to_string(ld_sconti[9]) + ", " + &
								f_double_to_string(ld_sconti[10]) + ", " + &
								"0, " + &
								"0, " + &
								string(str_conf_prodotto.prog_riga_documento) + ", " + &
								ls_flag_gen_commessa + ", " + &
								ls_flag_presso_pt  + ", " + &
								ls_flag_presso_cg  + ", " + &
								ls_flag_doc_suc_or + ", " + &
								ls_flag_st_note_or + ", " + &
								"" + ls_cod_versione
								
			if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
								ls_sql_ins += ", " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"null, " + &
								"0, " + &
								"0, " + &
								"'N', " + &
								"'A') "
			else
								ls_sql_ins +=  + ")"
			end if

		
		execute immediate :ls_sql_ins;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore durante la Creazione della riga Addizionale. Dettaglio: " + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return -1
		else
//			str_conf_prodotto.ultima_riga_inserita ++
		end if
		
		setnull(ls_nota)
		
		// --------------------------------- aggiornamento impegnato a magazzino -------------------------------------
		if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
			
			select flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_dettaglio_vendita;
					 
			if sqlca.sqlcode = 0 then
				
				if ls_flag_tipo_det_ven = "M" then
					
					ls_sql = " update anag_prodotti set quan_impegnata = quan_impegnata + " + f_double_to_string(ld_quan_prodotto) + &
								" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = " + ls_cod_prodotto
					execute immediate :ls_sql;
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata in magazzino." + sqlca.sqlerrtext, exclamation!, ok!)
						rollback;
						return -1
					end if
					
					// enme 08/1/2006 gestione prodotto raggruppato
					setnull(ls_cod_prodotto_raggruppato)
					
					select cod_prodotto_raggruppato
					into   :ls_cod_prodotto_raggruppato
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto;
							 
					if not isnull(ls_cod_prodotto_raggruppato) then
						
						ld_quan_impegnata = dec(ld_quan_prodotto)
						ld_quan_impegnata = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_impegnata, "M")
			
						ls_sql = " update anag_prodotti set quan_impegnata = quan_impegnata + " + f_decimal_string(ld_quan_impegnata) + &
									" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = " + ls_cod_prodotto_raggruppato
						execute immediate :ls_sql;
						if sqlca.sqlcode = -1 then
							g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata in magazzino." + sqlca.sqlerrtext, exclamation!, ok!)
							rollback;
							return -1
						end if
						
					end if
					
				end if
		
			end if
			
		end if
		
	end if
next
return 0

end function

public function integer wf_memorizza_dati ();string ls_nome_dw
datawindow ldw_passo

choose case ii_num_passo
	case 0
		ls_nome_dw = dw_inizio.dataobject
		ldw_passo = dw_inizio
	case 1
		ls_nome_dw = dw_passo_1.dataobject
		ldw_passo = dw_passo_1
	case 2
		ls_nome_dw = dw_passo_2.dataobject
		ldw_passo = dw_passo_2
	case 3
		ls_nome_dw = dw_passo_3.dataobject
		ldw_passo = dw_passo_3
	case 4
		ls_nome_dw = dw_passo_4.dataobject
		ldw_passo = dw_passo_4
	case 5
		ls_nome_dw = dw_passo_5.dataobject
		ldw_passo = dw_passo_5
	case 6
		ls_nome_dw = dw_passo_6.dataobject
		ldw_passo = dw_passo_6
	case 7
		ls_nome_dw = dw_passo_7.dataobject
		ldw_passo = dw_passo_7
	case 8
		ls_nome_dw = dw_passo_8.dataobject
		ldw_passo = dw_passo_8
	case 99
		ls_nome_dw = dw_fine.dataobject
		ldw_passo = dw_fine
end choose

choose case ls_nome_dw
	case "dw_inizio"
		istr_riepilogo.dimensione_1 = ldw_passo.getitemnumber(1,"rd_dimensione_1")
		istr_riepilogo.dimensione_2 = ldw_passo.getitemnumber(1,"rd_dimensione_2")
		istr_riepilogo.dimensione_3 = ldw_passo.getitemnumber(1,"rd_dimensione_3")
		istr_riepilogo.dimensione_4 = ldw_passo.getitemnumber(1,"rd_dimensione_4")
		istr_riepilogo.dimensione_5 = ldw_passo.getitemnumber(1,"rd_dimensione_5")
		istr_riepilogo.dimensione_6 = ldw_passo.getitemnumber(1,"rd_dimensione_6")
		istr_riepilogo.dimensione_7 = ldw_passo.getitemnumber(1,"rd_dimensione_7")
		istr_riepilogo.dimensione_8 = ldw_passo.getitemnumber(1,"rd_dimensione_8")
		istr_riepilogo.dimensione_9 = ldw_passo.getitemnumber(1,"rd_dimensione_9")
		istr_riepilogo.quan_vendita = ldw_passo.getitemnumber(1,"rn_quan_vendita")
		istr_riepilogo.flag_allegati = ldw_passo.getitemstring(1,"rs_flag_allegati")
		if istr_flags.flag_luce_finita = "S" then
			istr_riepilogo.flag_misura_luce_finita = ldw_passo.getitemstring(1,"rs_flag_misura_luce_finita")
		else
			istr_riepilogo.flag_misura_luce_finita = "F"
		end if
		istr_riepilogo.nota_prodotto = ldw_passo.getitemstring(1,"nota_prodotto_riga")
	case "d_ext_tessuto", "d_ext_tessuto_dk", "d_ext_tessuto_alu"
		istr_riepilogo.cod_tessuto = ""
		istr_riepilogo.cod_non_a_magazzino = ""
		istr_riepilogo.cod_tessuto_mant = ""
		istr_riepilogo.cod_mant_non_a_magazzino = ""
		istr_riepilogo.alt_mantovana = 0
		istr_riepilogo.flag_tipo_mantovana = ""
		istr_riepilogo.colore_cordolo = ""
		istr_riepilogo.colore_passamaneria = ""
		istr_riepilogo.flag_fc_tessuto = ""
		istr_riepilogo.cod_modello_telo_finito = ""
		if istr_flags.flag_tessuto = "S" then
			istr_riepilogo.cod_tessuto = ldw_passo.getitemstring(1,"rs_cod_tessuto")
			istr_riepilogo.cod_non_a_magazzino = ldw_passo.getitemstring(1,"rs_cod_non_a_magazzino")
			istr_riepilogo.flag_addizionale_tessuto = ldw_passo.getitemstring(1,"rs_tess_addizionale")
			istr_riepilogo.flag_fc_tessuto = ldw_passo.getitemstring(1,"rs_tess_fuori_campionario")
		end if
		if istr_flags.flag_tessuto_mantovana = "S" then
			istr_riepilogo.cod_tessuto_mant = ldw_passo.getitemstring(1,"rs_cod_tessuto_mant")
			istr_riepilogo.cod_mant_non_a_magazzino = ldw_passo.getitemstring(1,"rs_cod_mant_non_a_magazzino")
		end if
		if istr_flags.flag_altezza_mantovana = "S" then istr_riepilogo.alt_mantovana = ldw_passo.getitemnumber(1,"rn_alt_mantovana")
		if istr_flags.flag_tipo_mantovana = "S" then 
			if not isnull(istr_riepilogo.cod_tessuto_mant) and len(istr_riepilogo.cod_tessuto_mant) > 1 then
				istr_riepilogo.flag_tipo_mantovana = ldw_passo.getitemstring(1,"rs_flag_tipo_mantovana")
			else
				setnull(istr_riepilogo.flag_tipo_mantovana)
			end if
		end if
		if istr_flags.flag_cordolo = "S" then istr_riepilogo.colore_cordolo = ldw_passo.getitemstring(1,"rs_colore_cordolo")
		if istr_flags.flag_passamaneria = "S" then istr_riepilogo.colore_passamaneria = ldw_passo.getitemstring(1,"rs_colore_passamaneria")
		
		if ls_nome_dw = "d_ext_tessuto_dk" then 
			istr_riepilogo.cod_modello_telo_finito = ldw_passo.getitemstring(1,"rs_cod_modello_telo_finito")
		end if
		
		if ls_nome_dw = "d_ext_tessuto_alu" then 
			if isnull(ldw_passo.getitemstring(1,"rn_diam_tubetto_telo_sup")) then
				istr_riepilogo.diam_tubetto_telo_sup = "0"
			else
				istr_riepilogo.diam_tubetto_telo_sup = ldw_passo.getitemstring(1,"rn_diam_tubetto_telo_sup")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rn_diam_tubetto_telo_inf")) then
				istr_riepilogo.diam_tubetto_telo_inf = "0"
			else
				istr_riepilogo.diam_tubetto_telo_inf = ldw_passo.getitemstring(1,"rn_diam_tubetto_telo_inf")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rn_diam_tubetto_mant")) then
				istr_riepilogo.diam_tubetto_mant =  "0"
			else
				istr_riepilogo.diam_tubetto_mant = ldw_passo.getitemstring(1,"rn_diam_tubetto_mant")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_flag_tipo_unione")) then
				istr_riepilogo.flag_tipo_unione = ""
			else
				istr_riepilogo.flag_tipo_unione = ldw_passo.getitemstring(1,"rs_flag_tipo_unione")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_flag_orientamento_unione")) then
				istr_riepilogo.flag_orientamento_unione = ""
			else
				istr_riepilogo.flag_orientamento_unione = ldw_passo.getitemstring(1,"rs_flag_orientamento_unione")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_flag_tasca")) then
				istr_riepilogo.cod_tipo_tasca = ""
			else				
				istr_riepilogo.cod_tipo_tasca = ldw_passo.getitemstring(1,"rs_flag_tasca")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_cod_tipo_tasca_1")) then
				istr_riepilogo.cod_tipo_tasca_1 = ""
			else				
				istr_riepilogo.cod_tipo_tasca_1 = ldw_passo.getitemstring(1,"rs_cod_tipo_tasca_1")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_cod_tipo_tasca_2")) then
				istr_riepilogo.cod_tipo_tasca_2 = ""
			else				
				istr_riepilogo.cod_tipo_tasca_2 = ldw_passo.getitemstring(1,"rs_cod_tipo_tasca_2")
			end if
			
			if isnull(ldw_passo.getitemstring(1,"rs_flag_mantovana")) then
				istr_riepilogo.flag_mantovana = ""
			else
				istr_riepilogo.flag_mantovana = ldw_passo.getitemstring(1,"rs_flag_mantovana")
			end if
			
			if isnull(ldw_passo.getitemnumber(1,"rn_rapporto_mantovana")) then
				istr_riepilogo.rapporto_mantovana = 0
			else
				istr_riepilogo.rapporto_mantovana = ldw_passo.getitemnumber(1,"rn_rapporto_mantovana")
			end if
			
			if isnull(ldw_passo.getitemnumber(1,"rn_num_rinforzi")) then
				istr_riepilogo.num_rinforzi = 0
			else
				istr_riepilogo.num_rinforzi = ldw_passo.getitemnumber(1,"rn_num_rinforzi")
			end if
			if isnull(ldw_passo.getitemnumber(1,"rn_dimensione_rinforzo")) then
				istr_riepilogo.dimensione_rinforzo = 0
			else
				istr_riepilogo.dimensione_rinforzo = ldw_passo.getitemnumber(1,"rn_dimensione_rinforzo")
			end if
			if isnull(ldw_passo.getitemstring(1,"cod_reparto_telo")) then
				istr_riepilogo.cod_reparto_telo = ""
			else				
				istr_riepilogo.cod_reparto_telo = ldw_passo.getitemstring(1,"cod_reparto_telo")
			end if
			
		end if
		
	case "d_ext_comandi"
		istr_riepilogo.cod_comando = ""
		istr_riepilogo.pos_comando =  ""
		istr_riepilogo.alt_asta = 0
		istr_riepilogo.des_comando_1 = ldw_passo.getitemstring(1,"nota_comando_1")
		istr_riepilogo.des_comando_2 = ldw_passo.getitemstring(1,"nota_comando_2")
		istr_riepilogo.flag_fc_comando_1 = ""
		istr_riepilogo.flag_fc_comando_2 = ""
		if istr_flags.flag_comando = "S" then 
			istr_riepilogo.cod_comando = ldw_passo.getitemstring(1,"rs_cod_comando")
			istr_riepilogo.flag_addizionale_comando_1 = ldw_passo.getitemstring(1,"rs_com_flag_addizionale")
			istr_riepilogo.flag_fc_comando_1 = ldw_passo.getitemstring(1,"rs_com_flag_fuori_campionario")
		end if
		if istr_flags.flag_pos_comando = "S" then istr_riepilogo.pos_comando = ldw_passo.getitemstring(1,"rs_pos_comando")
		if istr_flags.flag_alt_asta = "S" then istr_riepilogo.alt_asta = ldw_passo.getitemnumber(1,"rn_alt_asta")
		if istr_flags.flag_secondo_comando = "S" then  
			istr_riepilogo.cod_comando_2 = ldw_passo.getitemstring(1,"rs_cod_comando_2")
			istr_riepilogo.flag_addizionale_comando_2 = ldw_passo.getitemstring(1,"rs_com_2_flag_addizionale")
			istr_riepilogo.flag_fc_comando_2 = ldw_passo.getitemstring(1,"rs_com_2_flag_fuori_campionario")
		end if
	case "d_ext_note"
		istr_riepilogo.note = ldw_passo.getitemstring(1,"rs_note")
	case "d_ext_plisse"
		if istr_flags.flag_plisse = "S" then
			istr_riepilogo.flag_autoblocco = ldw_passo.getitemstring(1,"rs_flag_autoblocco")
			istr_riepilogo.num_fili_plisse = ldw_passo.getitemnumber(1,"rn_num_fili_plisse")
			istr_riepilogo.num_boccole_plisse = ldw_passo.getitemnumber(1,"rn_num_boccole_plisse")
			istr_riepilogo.intervallo_punzoni_plisse = ldw_passo.getitemnumber(1,"rn_intervallo_punzoni_plisse")
			istr_riepilogo.num_pieghe_plisse = ldw_passo.getitemnumber(1,"rn_num_pieghe_plisse")
			istr_riepilogo.prof_inf_plisse = ldw_passo.getitemnumber(1,"rn_prof_inf_plisse")
			istr_riepilogo.prof_sup_plisse = ldw_passo.getitemnumber(1,"rn_prof_sup_plisse")
		else
			istr_riepilogo.flag_autoblocco = ""
			istr_riepilogo.num_fili_plisse = 0
			istr_riepilogo.num_boccole_plisse = 0
			istr_riepilogo.intervallo_punzoni_plisse = 0
			istr_riepilogo.num_pieghe_plisse = 0
			istr_riepilogo.prof_inf_plisse = 0
			istr_riepilogo.prof_sup_plisse = 0
		end if
	case "d_ext_supporto_flags"
		istr_riepilogo.cod_tipo_supporto = ""
		istr_riepilogo.flag_cappotta_frontale = ""
		istr_riepilogo.flag_rollo = ""
		istr_riepilogo.flag_kit_laterale = ""
		istr_riepilogo.num_gambe = 0
		istr_riepilogo.num_campate = 0
		istr_riepilogo.flag_fc_supporto = ""
		if istr_flags.flag_supporto = "S" then 
			istr_riepilogo.cod_tipo_supporto = ldw_passo.getitemstring(1,"rs_cod_supporto")
			istr_riepilogo.flag_addizionale_supporto = ldw_passo.getitemstring(1,"rs_flag_addizionale")
			istr_riepilogo.flag_fc_supporto = ldw_passo.getitemstring(1,"rs_flag_fuori_campionario")
		end if
		if istr_flags.flag_cappotta_frontale = "S" then	istr_riepilogo.flag_cappotta_frontale = ldw_passo.getitemstring(1,"rs_flag_cappotta_frontale")
		if istr_flags.flag_rollo = "S" then istr_riepilogo.flag_rollo = ldw_passo.getitemstring(1,"rs_flag_rollo")
		if istr_flags.flag_kit_laterale = "S" then istr_riepilogo.flag_kit_laterale = ldw_passo.getitemstring(1,"rs_flag_kit_laterale")
		if istr_flags.flag_num_gambe_campate = "S"then 
			istr_riepilogo.num_gambe = ldw_passo.getitemnumber(1,"rn_num_gambe")
			istr_riepilogo.num_campate = ldw_passo.getitemnumber(1,"rn_num_campate")
		end if		
	case "d_ext_tipi_stampi"
	case "d_ext_vern_lastra"
		istr_riepilogo.cod_verniciatura =  ""
		istr_riepilogo.cod_colore_lastra = ""
		istr_riepilogo.cod_tipo_lastra =  ""
		istr_riepilogo.spes_lastra = ""
		istr_riepilogo.flag_fc_vernic = ""
		if istr_flags.flag_verniciatura = "S" then 
			istr_riepilogo.cod_verniciatura = ldw_passo.getitemstring(1,"rs_cod_verniciatura")
			istr_riepilogo.flag_addizionale_vernic = ldw_passo.getitemstring(1,"rs_vern_addizionale")
			istr_riepilogo.flag_fc_vernic = ldw_passo.getitemstring(1,"rs_vern_fuori_campionario")
		end if
		if istr_flags.flag_colore_lastra = "S" then istr_riepilogo.cod_colore_lastra = ldw_passo.getitemstring(1,"rs_cod_colore_lastra")
		if istr_flags.flag_tipo_lastra = "S" then istr_riepilogo.cod_tipo_lastra = ldw_passo.getitemstring(1,"rs_cod_tipo_lastra")
		if istr_flags.flag_spessore_lastra = "S" then istr_riepilogo.spes_lastra = ldw_passo.getitemstring(1,"rs_spessore_lastra")
		istr_riepilogo.des_vernice_fc = ldw_passo.getitemstring(1,"rs_des_vernice_fc")
	case "d_ext_verniciature"
		istr_riepilogo.cod_verniciatura =  ""
		istr_riepilogo.flag_fc_vernic = ""
		if istr_flags.flag_verniciatura = "S" then 
			istr_riepilogo.cod_verniciatura = ldw_passo.getitemstring(1,"rs_cod_verniciatura")
			istr_riepilogo.flag_addizionale_vernic = ldw_passo.getitemstring(1,"rs_vern_addizionale")
			istr_riepilogo.flag_fc_vernic = ldw_passo.getitemstring(1,"rs_vern_fuori_campionario")
		end if
		istr_riepilogo.des_vernice_fc = ldw_passo.getitemstring(1,"rs_des_vernice_fc")

		istr_riepilogo.cod_verniciatura_2 =  ""
		istr_riepilogo.flag_fc_vernic_2 = ""
		if istr_flags.flag_verniciatura_2 = "S" then 
			istr_riepilogo.cod_verniciatura_2 = ldw_passo.getitemstring(1,"rs_cod_verniciatura_2")
			istr_riepilogo.flag_addizionale_vernic_2 = ldw_passo.getitemstring(1,"rs_vern_addizionale_2")
			istr_riepilogo.flag_fc_vernic_2 = ldw_passo.getitemstring(1,"rs_vern_fuori_campionario_2")
		end if
		istr_riepilogo.des_vernice_fc_2 = ldw_passo.getitemstring(1,"rs_des_vernice_fc_2")

		istr_riepilogo.cod_verniciatura_3 =  ""
		istr_riepilogo.flag_fc_vernic_3 = ""
		if istr_flags.flag_verniciatura_3 = "S" then 
			istr_riepilogo.cod_verniciatura_3 = ldw_passo.getitemstring(1,"rs_cod_verniciatura_3")
			istr_riepilogo.flag_addizionale_vernic_3 = ldw_passo.getitemstring(1,"rs_vern_addizionale_3")
			istr_riepilogo.flag_fc_vernic_3 = ldw_passo.getitemstring(1,"rs_vern_fuori_campionario_3")
		end if
		istr_riepilogo.des_vernice_fc_3 = ldw_passo.getitemstring(1,"rs_des_vernice_fc_3")
	case "d_ext_optional"
		string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_des_prodotto
		long ll_num_righe, ll_i
		str_optional lstr_optional_null[]
		
		ll_num_righe = ldw_passo.rowcount()
		istr_riepilogo.optional = lstr_optional_null
		if ll_num_righe > 0 and not isnull(ll_num_righe) then
			for ll_i = 1 to ll_num_righe
				ls_cod_tipo_det_ven = ldw_passo.getitemstring(ll_i, "rs_cod_tipo_det_ven")
				ls_cod_prodotto     = ldw_passo.getitemstring(ll_i, "rs_cod_prodotto")
				ls_des_prodotto     = ldw_passo.getitemstring(ll_i, "rs_des_prodotto")
				if not isnull(ls_cod_tipo_det_ven) and not isnull(ls_des_prodotto) then
					istr_riepilogo.optional[ll_i].cod_tipo_det_ven = ldw_passo.getitemstring(ll_i, "rs_cod_tipo_det_ven")
					istr_riepilogo.optional[ll_i].cod_prodotto     = ldw_passo.getitemstring(ll_i, "rs_cod_prodotto")
					istr_riepilogo.optional[ll_i].des_prodotto     = ldw_passo.getitemstring(ll_i, "rs_des_prodotto")
					istr_riepilogo.optional[ll_i].quan_vendita     = ldw_passo.getitemnumber(ll_i, "rd_quan_vendita")
					istr_riepilogo.optional[ll_i].note             = ldw_passo.getitemstring(ll_i, "rs_note")
					istr_riepilogo.optional[ll_i].cod_misura       = ldw_passo.getitemstring(ll_i, "rs_cod_misura")
					istr_riepilogo.optional[ll_i].cod_iva          = ldw_passo.getitemstring(ll_i, "rs_cod_iva")
					istr_riepilogo.optional[ll_i].sconto_1     	  = ldw_passo.getitemnumber(ll_i, "sconto_1")
					istr_riepilogo.optional[ll_i].sconto_2     	  = ldw_passo.getitemnumber(ll_i, "sconto_2")
					istr_riepilogo.optional[ll_i].sconto_3     	  = ldw_passo.getitemnumber(ll_i, "sconto_3")
					istr_riepilogo.optional[ll_i].sconto_4     	  = ldw_passo.getitemnumber(ll_i, "sconto_4")
					istr_riepilogo.optional[ll_i].sconto_5     	  = ldw_passo.getitemnumber(ll_i, "sconto_5")
					istr_riepilogo.optional[ll_i].sconto_6     	  = ldw_passo.getitemnumber(ll_i, "sconto_6")
					istr_riepilogo.optional[ll_i].sconto_7     	  = ldw_passo.getitemnumber(ll_i, "sconto_7")
					istr_riepilogo.optional[ll_i].sconto_8     	  = ldw_passo.getitemnumber(ll_i, "sconto_8")
					istr_riepilogo.optional[ll_i].sconto_9     	  = ldw_passo.getitemnumber(ll_i, "sconto_9")
					istr_riepilogo.optional[ll_i].sconto_10     	  = ldw_passo.getitemnumber(ll_i, "sconto_10")
					istr_riepilogo.optional[ll_i].provvigione_1 	  = ldw_passo.getitemnumber(ll_i, "provvigione_1")
					istr_riepilogo.optional[ll_i].provvigione_2 	  = ldw_passo.getitemnumber(ll_i, "provvigione_2")
					istr_riepilogo.optional[ll_i].prezzo_vendita   = ldw_passo.getitemnumber(ll_i, "prezzo_vendita")
					istr_riepilogo.optional[ll_i].dim_x   = ldw_passo.getitemnumber(ll_i, "dim_x")
					if isnull(istr_riepilogo.optional[ll_i].dim_x) then istr_riepilogo.optional[ll_i].dim_x = 0
					istr_riepilogo.optional[ll_i].dim_y   = ldw_passo.getitemnumber(ll_i, "dim_y")
					if isnull(istr_riepilogo.optional[ll_i].dim_y) then istr_riepilogo.optional[ll_i].dim_y = 0
				end if
			next
		end if	
		istr_riepilogo.nota_dettaglio = dw_nota_prodotto_finito.getitemstring(1,"nota_dettaglio")
		istr_riepilogo.nota_prodotto = dw_nota_prodotto_finito.getitemstring(1,"nota_prodotto_riga")
		istr_riepilogo.flag_trasporta_nota_ddt = dw_nota_prodotto_finito.getitemstring(1,"flag_trasporta_nota_ddt")
		
		try
			istr_riepilogo.flag_azzera_dt = dw_nota_prodotto_finito.getitemstring(1,"flag_azzera_dt")
		catch (runtimeerror err)
			istr_riepilogo.flag_azzera_dt = "N"
		end try
		
end choose
return 0

end function

public subroutine wf_visualizza_dw ();//	Funzione che visualizza e da il fuoco alla finestra corrispondente al passo corrente, ne
//		assegna i valori di default (wf_set_default) e gestisce l'abilitazione o meno dei tasti
//		avanti e indietro
//
// nome: wf_visualizza_dw
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//		Creata il 31-08-98 
//		Autore Mauro Bordin
//    modificata il 31/08/1998


dw_inizio.visible = false
dw_passo_1.visible = false
dw_passo_2.visible = false
dw_passo_3.visible = false
dw_passo_4.visible = false
dw_passo_5.visible = false
dw_passo_6.visible = false
dw_passo_7.visible = false
dw_passo_8.visible = false
dw_fine.visible = false
dw_nota_prodotto_finito.visible = false
dw_nota_prodotto_finito_lista.visible = false

dw_help.setredraw(false)
dw_help.reset()
dw_help.insertrow(0)
if s_cs_xx.admin then
	dw_help.setitem(1,"flag_admin","S")
else
	dw_help.setitem(1,"flag_admin","N")
end if

cb_indietro.enabled = true
cb_avanti.enabled = true

choose case ii_num_passo
	case 0
		dw_inizio.visible = true
		cb_indietro.enabled = false
		dw_inizio.setfocus()
		cb_fine.enabled = false
		dw_help.setitem(1,"help", "Indicare le Dimensioni del Prodotto scelto e la tipologia di misura utilizzata. Per avanzare premere INVIO, per retrocedere premere ESC, per spostarsi tra i vari campi premere TAB")
		dw_inizio.setfocus()
	case 1
		dw_passo_1.visible = true
		dw_passo_1.setfocus()
		dw_help.setitem(1, "help",is_mini_help[ii_num_passo])
		wf_set_default(ref dw_passo_1)
		cb_fine.enabled = false
		if dw_passo_1.dataobject = is_nome_passo_optional then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_1.setfocus()
	case 2
		dw_passo_2.visible = true
		dw_passo_2.setfocus()
		dw_help.setitem(1,"help", is_mini_help[ii_num_passo] )
		wf_set_default(ref dw_passo_2)
		cb_fine.enabled = false
		if dw_passo_2.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_2.setfocus()
	case 3
		dw_passo_3.visible = true
		dw_passo_3.setfocus()
		dw_help.setitem(1,"help",is_mini_help[ii_num_passo])
		wf_set_default(ref dw_passo_3)
		cb_fine.enabled = false
		if dw_passo_3.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_3.setfocus()
	case 4
		dw_passo_4.visible = true
		dw_passo_4.setfocus()
		dw_help.setitem(1,"help", is_mini_help[ii_num_passo] )
		wf_set_default(ref dw_passo_4)
		cb_fine.enabled = false
		if dw_passo_4.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_4.setfocus()
	case 5
		dw_passo_5.visible = true
		dw_passo_5.setfocus()
		dw_help.setitem(1,"help", is_mini_help[ii_num_passo] )
		wf_set_default(ref dw_passo_5)
		cb_fine.enabled = false
		if dw_passo_5.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_5.setfocus()
	case 6
		dw_passo_6.visible = true
		dw_passo_6.setfocus()
		dw_help.setitem(1,"help", is_mini_help[ii_num_passo])
		wf_set_default(ref dw_passo_6)
		cb_fine.enabled = false
		if dw_passo_6.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_6.setfocus()
	case 7
		dw_passo_7.visible = true
		dw_passo_7.setfocus()
		dw_help.setitem(1, "help",is_mini_help[ii_num_passo])
		wf_set_default(ref dw_passo_7)
		cb_fine.enabled = false
		if dw_passo_7.dataobject = is_nome_passo_optional then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_7.setfocus()
	case 8
		dw_passo_8.visible = true
		dw_passo_8.setfocus()
		dw_help.setitem(1,"help", is_mini_help[ii_num_passo])
		wf_set_default(ref dw_passo_8)
		cb_fine.enabled = false
		if dw_passo_8.dataobject = is_nome_passo_optional  then 
			dw_nota_prodotto_finito.visible = true
			dw_nota_prodotto_finito_lista.visible = true
		end if
		dw_passo_8.setfocus()
	case 99
		dw_fine.visible = true
		dw_help.setitem(1,"help", "Riassunto della configurazione; è possibile stampare tramite il tasto STAMPA; SHIFT-F1 = Gen+Chiudi COMM; SHIFT-F2=Gen. Comm.")
		dw_fine.setfocus()		
		wf_set_default(ref dw_fine)
		cb_avanti.enabled = false
		cb_fine.enabled = true
		dw_fine.setfocus()		
end choose
dw_help.setredraw(true)
end subroutine

public function integer wf_leggi_flag_configuratore (string fs_tabella_comp, string fs_colonna_prog_riga, ref string errore);string ls_sql

declare cu_comp dynamic cursor for sqlsa;

ls_sql = "select cod_prod_finito, " + &
         "cod_modello, " + &
         "dim_x, " + &
         "dim_y, " + &
         "dim_z, " + &
         "dim_t, " + &
         "dim_05, " + &
         "dim_06, " + &
         "dim_07, " + &
         "dim_08, " + &
         "dim_09, " + &
         "cod_tessuto, " + &
         "cod_non_a_magazzino, " + &
         "cod_tessuto_mant, " + &
         "cod_mant_non_a_magazzino, " + &
         "flag_tipo_mantovana, " + &
         "alt_mantovana, " + &
         "flag_cordolo, " + &
         "flag_passamaneria, " + &
         "cod_verniciatura, " + &
         "cod_comando, " + &
         "pos_comando, " + &
         "alt_asta, " + &
         "cod_colore_lastra, " + &
         "cod_tipo_lastra, " + &
         "spes_lastra, " + &
         "cod_tipo_supporto, " + &
         "flag_cappotta_frontale, " + &
         "flag_rollo, " + &
         "flag_kit_laterale, " + &
         "flag_misura_luce_finita, " + &
         "num_gambe, " + &
         "num_campate, " + &
         "cod_tipo_stampo, " + &
         "cod_mat_sis_stampaggio, " + &
         "rev_stampo, " + &
         "num_gambe, " + &
         "num_campate, " + &
         "note ," + &
         "flag_autoblocco ," + &
         "num_fili_plisse ," + &
         "num_boccole_plisse ," + &
         "intervallo_punzoni_plisse ," + &
         "num_pieghe_plisse, " + &
         "prof_inf_plisse, " + &
         "prof_sup_plisse, " + &
         "colore_passamaneria, " + &
         "colore_cordolo, " + &
         "des_comando_1, " + &
         "cod_comando_2, " + &
         "des_comando_2, " + &
         "flag_addizionale_comando_1, " + &
         "flag_addizionale_comando_2, " + &
         "flag_addizionale_supporto, " + &
         "flag_addizionale_tessuto, " + &
         "flag_addizionale_vernic, " + &
         "flag_addizionale_vernic_2, " + &
         "flag_addizionale_vernic_3, " + &
         "flag_fc_comando_1, " + &
         "flag_fc_comando_2, " + &
         "flag_fc_supporto, " + &
         "flag_fc_tessuto, " + &
         "flag_fc_vernic, " + &
         "flag_fc_vernic_2, " + &
         "flag_fc_vernic_3, " + &
         "des_vernice_fc, " + &
         "des_vernic_fc_2, " + &
         "des_vernic_fc_3, " + &
         "flag_allegati, " + &
         "flag_tessuto_cliente, " + &
         "flag_tessuto_mant_cliente, " + &
         "num_punzoni_plisse, " + &
         "cod_verniciatura_2, " + &
         "cod_verniciatura_3," + &
		"flag_azzera_dt, " + &
		"cod_modello_telo_finito, " + &
		"diam_tubetto_telo_sup , " + &
		"diam_tubetto_telo_inf , " + &
		"diam_tubetto_mant , " + &
		"flag_tipo_unione , " + &
		"flag_orientamento_unione , " + &
		"cod_tipo_tasca , " + &
		"cod_tipo_tasca_2 , " + &
		"cod_tipo_tasca_3 , " + &
		"flag_mantovana , " + &
		"rapporto_mantovana , " + &
		"num_rinforzi , " + &
		"dimensione_rinforzo, " + &
		"cod_reparto " + &
"from " + fs_tabella_comp + " " + &
"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
         "anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
         "num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
         fs_colonna_prog_riga + " = "  + string(str_conf_prodotto.prog_riga_documento)

prepare sqlsa from :ls_sql;

open dynamic cu_comp;
if sqlca.sqlcode = -1 then
	errore = "Errore in open cursore cu_comp lettura parametri configuratore.~r~n" + sqlca.sqlerrtext
	return -1
end if

fetch cu_comp into
	:str_conf_prodotto.cod_prodotto_finito,
	:str_conf_prodotto.cod_modello,   
	:istr_riepilogo.dimensione_1,   
	:istr_riepilogo.dimensione_2,   
	:istr_riepilogo.dimensione_3,   
	:istr_riepilogo.dimensione_4,   
	:istr_riepilogo.dimensione_5,   
	:istr_riepilogo.dimensione_6,   
	:istr_riepilogo.dimensione_7,   
	:istr_riepilogo.dimensione_8,   
	:istr_riepilogo.dimensione_9,   
	:istr_riepilogo.cod_tessuto,   
	:istr_riepilogo.cod_non_a_magazzino,   
	:istr_riepilogo.cod_tessuto_mant,   
	:istr_riepilogo.cod_mant_non_a_magazzino,   
	:istr_riepilogo.flag_tipo_mantovana,   
	:istr_riepilogo.alt_mantovana,   
	:istr_riepilogo.flag_cordolo,   
	:istr_riepilogo.flag_passamaneria,   
	:istr_riepilogo.cod_verniciatura,   
	:istr_riepilogo.cod_comando,
	:istr_riepilogo.pos_comando,
	:istr_riepilogo.alt_asta,
	:istr_riepilogo.cod_colore_lastra,   
	:istr_riepilogo.cod_tipo_lastra,   
	:istr_riepilogo.spes_lastra,   
	:istr_riepilogo.cod_tipo_supporto,   
	:istr_riepilogo.flag_cappotta_frontale,   
	:istr_riepilogo.flag_rollo,   
	:istr_riepilogo.flag_kit_laterale,   
	:istr_riepilogo.flag_misura_luce_finita,   
	:istr_riepilogo.num_gambe,   
	:istr_riepilogo.num_campate,   
	:istr_riepilogo.cod_tipo_stampo,   
	:istr_riepilogo.cod_mat_sis_stampaggio,   
	:istr_riepilogo.rev_stampo,
	:istr_riepilogo.num_gambe,   
	:istr_riepilogo.num_campate,
	:istr_riepilogo.note,
	:istr_riepilogo.flag_autoblocco ,
	:istr_riepilogo.num_fili_plisse ,
	:istr_riepilogo.num_boccole_plisse ,
	:istr_riepilogo.intervallo_punzoni_plisse ,
	:istr_riepilogo.num_pieghe_plisse,
	:istr_riepilogo.prof_inf_plisse ,
	:istr_riepilogo.prof_sup_plisse,
	:istr_riepilogo.colore_passamaneria,
	:istr_riepilogo.colore_cordolo,
	:istr_riepilogo.des_comando_1,
	:istr_riepilogo.cod_comando_2,
	:istr_riepilogo.des_comando_2,
	:istr_riepilogo.flag_addizionale_comando_1,
	:istr_riepilogo.flag_addizionale_comando_2,
	:istr_riepilogo.flag_addizionale_supporto,
	:istr_riepilogo.flag_addizionale_tessuto,
	:istr_riepilogo.flag_addizionale_vernic,
	:istr_riepilogo.flag_addizionale_vernic_2,
	:istr_riepilogo.flag_addizionale_vernic_3,
	:istr_riepilogo.flag_fc_comando_1,
	:istr_riepilogo.flag_fc_comando_2,
	:istr_riepilogo.flag_fc_supporto,
	:istr_riepilogo.flag_fc_tessuto,
	:istr_riepilogo.flag_fc_vernic,
	:istr_riepilogo.flag_fc_vernic_2,
	:istr_riepilogo.flag_fc_vernic_3,
	:istr_riepilogo.des_vernice_fc,
	:istr_riepilogo.des_vernice_fc_2,
	:istr_riepilogo.des_vernice_fc_3,
	:istr_riepilogo.flag_allegati,
	:istr_riepilogo.flag_tessuto_cliente,
	:istr_riepilogo.flag_tessuto_mant_cliente,
	:istr_riepilogo.num_punzoni_plisse,
	:istr_riepilogo.cod_verniciatura_2,
	:istr_riepilogo.cod_verniciatura_3,
	:istr_riepilogo.flag_azzera_dt,
	:istr_riepilogo.cod_modello_telo_finito,
	:istr_riepilogo.diam_tubetto_telo_sup,	
	:istr_riepilogo.diam_tubetto_telo_inf,
	:istr_riepilogo.diam_tubetto_mant,
	:istr_riepilogo.flag_tipo_unione,
	:istr_riepilogo.flag_orientamento_unione,
	:istr_riepilogo.cod_tipo_tasca,
	:istr_riepilogo.cod_tipo_tasca_1,
	:istr_riepilogo.cod_tipo_tasca_2,
	:istr_riepilogo.flag_mantovana,
	:istr_riepilogo.rapporto_mantovana,
	:istr_riepilogo.num_rinforzi,
	:istr_riepilogo.dimensione_rinforzo,
	:istr_riepilogo.cod_reparto_telo ;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell'estrazione Componenti")
end if
close cu_comp;

if isnull(istr_riepilogo.flag_cordolo) or istr_riepilogo.flag_cordolo = "" then istr_riepilogo.flag_cordolo = "N"
if isnull(istr_riepilogo.flag_passamaneria) or istr_riepilogo.flag_passamaneria = "" then istr_riepilogo.flag_passamaneria = "N"
if isnull(istr_riepilogo.flag_cappotta_frontale) or istr_riepilogo.flag_cappotta_frontale = "" then istr_riepilogo.flag_cappotta_frontale = "N"
if isnull(istr_riepilogo.flag_rollo) or istr_riepilogo.flag_rollo = "" then istr_riepilogo.flag_rollo = "N"
if isnull(istr_riepilogo.flag_kit_laterale) or istr_riepilogo.flag_kit_laterale = "" then istr_riepilogo.flag_kit_laterale = "N"
if isnull(istr_riepilogo.flag_autoblocco) or istr_riepilogo.flag_autoblocco = "" then istr_riepilogo.flag_autoblocco = "S"
if isnull(istr_riepilogo.flag_misura_luce_finita) or istr_riepilogo.flag_misura_luce_finita = "" then istr_riepilogo.flag_misura_luce_finita = "F"
if isnull(istr_riepilogo.flag_addizionale_comando_1) or istr_riepilogo.flag_addizionale_comando_1 = "" then istr_riepilogo.flag_addizionale_comando_1 = "N"
if isnull(istr_riepilogo.flag_addizionale_comando_2) or istr_riepilogo.flag_addizionale_comando_2 = "" then istr_riepilogo.flag_addizionale_comando_2 = "N"
if isnull(istr_riepilogo.flag_addizionale_supporto) or istr_riepilogo.flag_addizionale_supporto = "" then istr_riepilogo.flag_addizionale_supporto = "N"
if isnull(istr_riepilogo.flag_addizionale_tessuto) or istr_riepilogo.flag_addizionale_tessuto = "" then istr_riepilogo.flag_addizionale_tessuto = "N"
if isnull(istr_riepilogo.flag_addizionale_vernic) or istr_riepilogo.flag_addizionale_vernic = "" then istr_riepilogo.flag_addizionale_vernic = "N"
if isnull(istr_riepilogo.flag_addizionale_vernic_2) or istr_riepilogo.flag_addizionale_vernic_2 = "" then istr_riepilogo.flag_addizionale_vernic_2 = "N"
if isnull(istr_riepilogo.flag_addizionale_vernic_3) or istr_riepilogo.flag_addizionale_vernic_3 = "" then istr_riepilogo.flag_addizionale_vernic_3 = "N"
if isnull(istr_riepilogo.flag_fc_comando_1) or istr_riepilogo.flag_fc_comando_1 = "" then istr_riepilogo.flag_fc_comando_1 = "N"
if isnull(istr_riepilogo.flag_fc_comando_2) or istr_riepilogo.flag_fc_comando_2 = "" then istr_riepilogo.flag_fc_comando_2 = "N"
if isnull(istr_riepilogo.flag_fc_supporto) or istr_riepilogo.flag_fc_supporto = "" then istr_riepilogo.flag_fc_supporto = "N"
if isnull(istr_riepilogo.flag_fc_tessuto) or istr_riepilogo.flag_fc_tessuto = "" then istr_riepilogo.flag_fc_tessuto = "N"
if isnull(istr_riepilogo.flag_fc_vernic) or istr_riepilogo.flag_fc_vernic = "" then istr_riepilogo.flag_fc_vernic = "N"
if isnull(istr_riepilogo.flag_fc_vernic_2) or istr_riepilogo.flag_fc_vernic_2 = "" then istr_riepilogo.flag_fc_vernic_2 = "N"
if isnull(istr_riepilogo.flag_fc_vernic_3) or istr_riepilogo.flag_fc_vernic_3 = "" then istr_riepilogo.flag_fc_vernic_3 = "N"
if isnull(istr_riepilogo.flag_allegati) or istr_riepilogo.flag_allegati = "" then istr_riepilogo.flag_allegati = "N"
if isnull(istr_riepilogo.flag_tessuto_cliente) or istr_riepilogo.flag_tessuto_cliente = "" then istr_riepilogo.flag_tessuto_cliente = "N"
if isnull(istr_riepilogo.flag_tessuto_mant_cliente) or istr_riepilogo.flag_tessuto_mant_cliente = "" then istr_riepilogo.flag_tessuto_mant_cliente = "N"
if isnull(istr_riepilogo.flag_azzera_dt) or istr_riepilogo.flag_azzera_dt = "" then istr_riepilogo.flag_azzera_dt = "N"
if isnull(istr_riepilogo.diam_tubetto_telo_sup) then istr_riepilogo.diam_tubetto_telo_sup = "0"
if isnull(istr_riepilogo.diam_tubetto_telo_inf) then istr_riepilogo.diam_tubetto_telo_inf = "0"
if isnull(istr_riepilogo.diam_tubetto_mant) then istr_riepilogo.diam_tubetto_mant = "0"
if isnull(istr_riepilogo.num_rinforzi) then istr_riepilogo.num_rinforzi = 0
if isnull(istr_riepilogo.dimensione_rinforzo) then istr_riepilogo.dimensione_rinforzo = 0

istr_flags.flag_genera_commessa = false

// ----------------------------- carico optional in struttura riepilogo ---------------------------------------------------------
if str_conf_prodotto.stato = "M"  then// sono entrato in modifica
	// funzione di caricamento optional
	uo_optional luo_optional
	luo_optional = CREATE uo_optional
	if luo_optional.uof_leggi_optional(str_conf_prodotto.tipo_gestione, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, istr_riepilogo.optional, errore) <> 0 then 		return -1
end if
// ----------------------------- fine carimento optional -------------------------------------------------------------------------
return 0
end function

public function integer wf_pcd_new_dw (datawindow fdw_passo);/*	Funzione di gestione dell' evento PCD_NEW per le varie DW

 nome: wf_pcd_new_dw
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 fdw_passo				    DataWindow		valore

		Creata il 31-08-98 
		Autore Mauro Bordin
    modificata il 31/08/1998
    modificata il 06/10/2000 per nuova gestione optional
		
EnMe 22/04/2012
		Gestione di 3 verniciature

EnMe 08/05/2012
		Tipi dettaglio prodotto, addizionali, optionals gestite come eccezioni in tabella specifica
*/	
string ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_des_variabile, ls_errore
datetime ldt_oggi
long     ll_null


setnull(ll_null)
ldt_oggi = datetime(today(),00:00:00)

choose case fdw_passo.dataobject
	case "d_ext_inizio"
		if not isnull(str_conf_prodotto.cod_modello) then
			string ls_messaggio, ls_cod_versione
			fdw_passo.setitem(1, "rs_cod_modello", str_conf_prodotto.cod_modello)
			fdw_passo.setitem(1, "rs_cod_modello_manuale", str_conf_prodotto.cod_modello)
			fdw_passo.setitem(1, "rn_prog_des_modello", ll_null)
			fdw_passo.setitem(1, "rs_des_prodotto", str_conf_prodotto.des_prodotto)
			if wf_item_changed (fdw_passo, "rs_cod_modello_manuale", str_conf_prodotto.cod_modello, ref ls_messaggio) = -1 then
				g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
				return -1
			end if

			if not isnull(str_conf_prodotto.cod_modello) and isnull(str_conf_prodotto.cod_versione) then
				select cod_versione
				into   :str_conf_prodotto.cod_versione
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    cod_prodotto = :str_conf_prodotto.cod_modello
				and    flag_predefinita = 'S'
				and    ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi))	;
				if sqlca.sqlcode = 0 then 
					fdw_passo.setitem(1,"rs_cod_versione",ls_cod_versione)
				elseif sqlca.sqlcode = 100 then
					g_mb.messagebox("Configuratore","Errore nella versione del modello di prodotto; potrebbe non esistere la versione o esistere più versioni predefinite per lo stesso modello di prodotto")
					setnull(str_conf_prodotto.cod_versione)
				elseif sqlca.sqlcode <> 0 then
					g_mb.messagebox("Configuratore","Errore nella ricerca della versione del modello di prodotto")
					setnull(str_conf_prodotto.cod_versione)
				end if
			end if	
			if not isnull(str_conf_prodotto.cod_versione) then
				fdw_passo.setitem(1, "rs_cod_versione", str_conf_prodotto.cod_versione)
			end if	
			if not isnull(str_conf_prodotto.des_prodotto) then
				fdw_passo.setitem(1, "rs_des_prodotto", str_conf_prodotto.des_prodotto)
			end if	
			
			if not isnull(istr_riepilogo.dimensione_1) and istr_riepilogo.dimensione_1 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_1", istr_riepilogo.dimensione_1)
			end if
			if not isnull(istr_riepilogo.dimensione_2) and istr_riepilogo.dimensione_2 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_2", istr_riepilogo.dimensione_2)
			end if
			if not isnull(istr_riepilogo.dimensione_3) and istr_riepilogo.dimensione_3 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_3", istr_riepilogo.dimensione_3)
			end if
			if not isnull(istr_riepilogo.dimensione_4) and istr_riepilogo.dimensione_4 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_4", istr_riepilogo.dimensione_4)
			end if
			if not isnull(istr_riepilogo.dimensione_5) and istr_riepilogo.dimensione_5 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_5", istr_riepilogo.dimensione_5)
			end if
			if not isnull(istr_riepilogo.dimensione_6) and istr_riepilogo.dimensione_6 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_6", istr_riepilogo.dimensione_6)
			end if
			if not isnull(istr_riepilogo.dimensione_7) and istr_riepilogo.dimensione_7 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_7", istr_riepilogo.dimensione_7)
			end if
			if not isnull(istr_riepilogo.dimensione_8) and istr_riepilogo.dimensione_8 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_8", istr_riepilogo.dimensione_8)
			end if
			if not isnull(istr_riepilogo.dimensione_9) and istr_riepilogo.dimensione_9 <> 0 then
				fdw_passo.setitem(1, "rd_dimensione_9", istr_riepilogo.dimensione_9)
			end if
			
			if not isnull(istr_riepilogo.flag_misura_luce_finita) and istr_riepilogo.flag_misura_luce_finita <> "" then
				fdw_passo.setitem(1, "rs_flag_misura_luce_finita", istr_riepilogo.flag_misura_luce_finita)
			end if
			if not isnull(istr_riepilogo.quan_vendita) and istr_riepilogo.quan_vendita > 0 then
				fdw_passo.setitem(1, "rn_quan_vendita", istr_riepilogo.quan_vendita)
			end if
			if not isnull(istr_riepilogo.flag_allegati) and istr_riepilogo.flag_allegati <> "" then
				fdw_passo.setitem(1, "rs_flag_allegati", istr_riepilogo.flag_allegati)
			end if
			if not isnull(str_conf_prodotto.des_prodotto) and istr_riepilogo.flag_allegati <> "" then
				fdw_passo.setitem(1, "rs_flag_allegati", istr_riepilogo.flag_allegati)
			end if
			if not isnull(istr_riepilogo.nota_prodotto) and istr_riepilogo.nota_prodotto <> "" then
				fdw_passo.setitem(1, "nota_prodotto_riga", istr_riepilogo.nota_prodotto)
			end if
			wf_gest_mp_non_richieste(istr_riepilogo.dimensione_1, istr_riepilogo.dimensione_2,  istr_riepilogo.dimensione_3,  istr_riepilogo.dimensione_4, istr_riepilogo.alt_asta)
		end if
		fdw_passo.setcolumn("rs_cod_modello_manuale")

		if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
			if wf_imposta_dddw_loghi(ls_errore)<0 then
				g_mb.error(ls_errore)
			end if
		end if

	case "d_ext_vern_lastra"
		if not isnull(istr_riepilogo.cod_verniciatura)	and istr_riepilogo.cod_verniciatura <> "" then
			fdw_passo.setitem(1, "rs_cod_verniciatura", istr_riepilogo.cod_verniciatura)
			fdw_passo.setitem(1, "rs_cod_verniciatura_manuale", istr_riepilogo.cod_verniciatura)
		end if
		if not isnull(istr_riepilogo.des_vernice_fc)	and istr_riepilogo.des_vernice_fc <> "" then
			fdw_passo.setitem(1, "rs_des_vernice_fc", istr_riepilogo.des_vernice_fc)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_vernic)	and len(istr_riepilogo.flag_addizionale_vernic) > 0 then
			fdw_passo.setitem(1, "rs_vern_addizionale", istr_riepilogo.flag_addizionale_vernic)
		end if
		if not isnull(istr_riepilogo.flag_fc_vernic)	and len(istr_riepilogo.flag_fc_vernic) > 0 then
			fdw_passo.setitem(1, "rs_vern_fuori_campionario", istr_riepilogo.flag_fc_vernic)
		end if
		if not isnull(istr_riepilogo.cod_colore_lastra)	and istr_riepilogo.cod_colore_lastra <> "" then
			fdw_passo.setitem(1, "rs_cod_colore_lastra", istr_riepilogo.cod_colore_lastra)
			fdw_passo.setitem(1, "rs_cod_colore_lastra_manuale", istr_riepilogo.cod_colore_lastra)
		end if
		if not isnull(istr_riepilogo.cod_tipo_lastra)	and istr_riepilogo.cod_tipo_lastra <> "" then
			fdw_passo.setitem(1, "rs_cod_tipo_lastra", istr_riepilogo.cod_tipo_lastra)
			fdw_passo.setitem(1, "rs_cod_tipo_lastra_manuale", istr_riepilogo.cod_tipo_lastra)
		end if

	case "d_ext_verniciature"
		if not isnull(istr_riepilogo.cod_verniciatura)	and istr_riepilogo.cod_verniciatura <> "" then
			fdw_passo.setitem(1, "rs_cod_verniciatura", istr_riepilogo.cod_verniciatura)
			fdw_passo.setitem(1, "rs_cod_verniciatura_manuale", istr_riepilogo.cod_verniciatura)
		end if
		if not isnull(istr_riepilogo.des_vernice_fc)	and istr_riepilogo.des_vernice_fc <> "" then
			fdw_passo.setitem(1, "rs_des_vernice_fc", istr_riepilogo.des_vernice_fc)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_vernic)	and len(istr_riepilogo.flag_addizionale_vernic) > 0 then
			fdw_passo.setitem(1, "rs_vern_addizionale", istr_riepilogo.flag_addizionale_vernic)
		end if
		if not isnull(istr_riepilogo.flag_fc_vernic)	and len(istr_riepilogo.flag_fc_vernic) > 0 then
			fdw_passo.setitem(1, "rs_vern_fuori_campionario", istr_riepilogo.flag_fc_vernic)
		end if

		if not isnull(istr_riepilogo.cod_verniciatura_2)	and istr_riepilogo.cod_verniciatura_2 <> "" then
			fdw_passo.setitem(1, "rs_cod_verniciatura_2", istr_riepilogo.cod_verniciatura_2)
			fdw_passo.setitem(1, "rs_cod_verniciatura_manuale_2", istr_riepilogo.cod_verniciatura_2)
		end if
		if not isnull(istr_riepilogo.des_vernice_fc_2)	and istr_riepilogo.des_vernice_fc_2 <> "" then
			fdw_passo.setitem(1, "rs_des_vernice_fc_2", istr_riepilogo.des_vernice_fc_2)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_vernic_2)	and len(istr_riepilogo.flag_addizionale_vernic_2) > 0 then
			fdw_passo.setitem(1, "rs_vern_addizionale_2", istr_riepilogo.flag_addizionale_vernic_2)
		end if
		if not isnull(istr_riepilogo.flag_fc_vernic_2)	and len(istr_riepilogo.flag_fc_vernic_2) > 0 then
			fdw_passo.setitem(1, "rs_vern_fuori_campionario_2", istr_riepilogo.flag_fc_vernic_2)
		end if

		if not isnull(istr_riepilogo.cod_verniciatura_3)	and istr_riepilogo.cod_verniciatura_3 <> "" then
			fdw_passo.setitem(1, "rs_cod_verniciatura_3", istr_riepilogo.cod_verniciatura_3)
			fdw_passo.setitem(1, "rs_cod_verniciatura_manuale_3", istr_riepilogo.cod_verniciatura_3)
		end if
		if not isnull(istr_riepilogo.des_vernice_fc_3)	and istr_riepilogo.des_vernice_fc_3 <> "" then
			fdw_passo.setitem(1, "rs_des_vernice_fc_3", istr_riepilogo.des_vernice_fc_3)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_vernic_3)	and len(istr_riepilogo.flag_addizionale_vernic_3) > 0 then
			fdw_passo.setitem(1, "rs_vern_addizionale_3", istr_riepilogo.flag_addizionale_vernic_3)
		end if
		if not isnull(istr_riepilogo.flag_fc_vernic_3)	and len(istr_riepilogo.flag_fc_vernic_3) > 0 then
			fdw_passo.setitem(1, "rs_vern_fuori_campionario_3", istr_riepilogo.flag_fc_vernic_3)
		end if

	case "d_ext_comandi"
		// primo comando
		if not isnull(istr_riepilogo.cod_comando)	and istr_riepilogo.cod_comando <> "" then
			fdw_passo.setitem(1, "rs_cod_comando", istr_riepilogo.cod_comando)
			fdw_passo.setitem(1, "rs_cod_comando_manuale", istr_riepilogo.cod_comando)
		end if
		if not isnull(istr_riepilogo.pos_comando)	and istr_riepilogo.pos_comando <> "" then
			fdw_passo.setitem(1, "rs_pos_comando", istr_riepilogo.pos_comando)
		end if
		if not isnull(istr_riepilogo.alt_asta)	and istr_riepilogo.alt_asta <> 0 then
			fdw_passo.setitem(1, "rn_alt_asta", istr_riepilogo.alt_asta)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_comando_1)	and len(istr_riepilogo.flag_addizionale_comando_1) > 0 then
			fdw_passo.setitem(1, "rs_com_flag_addizionale", istr_riepilogo.flag_addizionale_comando_1)
		end if
		if not isnull(istr_riepilogo.flag_fc_comando_1)	and len(istr_riepilogo.flag_fc_comando_1) > 0 then
			fdw_passo.setitem(1, "rs_com_flag_fuori_campionario", istr_riepilogo.flag_fc_comando_1)
		end if
		if not isnull(istr_riepilogo.des_comando_1)	and len(istr_riepilogo.des_comando_1) > 0 then
			fdw_passo.setitem(1, "nota_comando_1", istr_riepilogo.des_comando_1)
		end if
		
		// secondo comando
		if not isnull(istr_riepilogo.cod_comando_2)	and istr_riepilogo.cod_comando_2 <> "" then
			fdw_passo.setitem(1, "rs_cod_comando_2", istr_riepilogo.cod_comando_2)
			fdw_passo.setitem(1, "rs_cod_comando_2_manuale", istr_riepilogo.cod_comando_2)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_comando_2)	and len(istr_riepilogo.flag_addizionale_comando_2) > 0 then
			fdw_passo.setitem(1, "rs_com_2_flag_addizionale", istr_riepilogo.flag_addizionale_comando_2)
		end if
		if not isnull(istr_riepilogo.flag_fc_comando_2)	and len(istr_riepilogo.flag_fc_comando_2) > 0 then
			fdw_passo.setitem(1, "rs_com_2_flag_fuori_campionario", istr_riepilogo.flag_fc_comando_2)
		end if
		if not isnull(istr_riepilogo.des_comando_2)	and len(istr_riepilogo.des_comando_2) > 0 then
			fdw_passo.setitem(1, "nota_comando_2", istr_riepilogo.des_comando_2)
		end if
		
		wf_gest_mp_non_richieste(istr_riepilogo.dimensione_1, istr_riepilogo.dimensione_2,  istr_riepilogo.dimensione_3,  istr_riepilogo.dimensione_4, istr_riepilogo.alt_asta)

	case "d_ext_supporto_flags"
		if not isnull(istr_riepilogo.cod_tipo_supporto)	and istr_riepilogo.cod_tipo_supporto <> "" then
			fdw_passo.setitem(1, "rs_cod_supporto", istr_riepilogo.cod_tipo_supporto)
			fdw_passo.setitem(1, "rs_cod_supporto_manuale", istr_riepilogo.cod_tipo_supporto)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_supporto)	and istr_riepilogo.flag_addizionale_supporto <> "" then
			fdw_passo.setitem(1, "rs_flag_addizionale", istr_riepilogo.flag_addizionale_supporto)
		end if
		if not isnull(istr_riepilogo.flag_fc_supporto)	and istr_riepilogo.flag_fc_supporto <> "" then
			fdw_passo.setitem(1, "rs_flag_fuori_campionario", istr_riepilogo.flag_fc_supporto)
		end if
		if not isnull(istr_flags.cod_cappotta_frontale) and istr_flags.cod_cappotta_frontale <> "" then
			string ls_cod_cappotta_frontale
			select des_prodotto
			into	 :ls_cod_cappotta_frontale
			from	 anag_prodotti
			where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :istr_flags.cod_cappotta_frontale;
					 
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Configuratore di Prodotto", "Errore durante l'Estrazione Descrizione Cappotta Frontale")
				return -1
			elseif sqlca.sqlcode = 100 then
				ls_cod_cappotta_frontale = ""
			end if
			fdw_passo.object.des_cappotta_frontale.text = ls_cod_cappotta_frontale
		end if
		if not isnull(istr_riepilogo.flag_cappotta_frontale) and istr_riepilogo.flag_cappotta_frontale <> "" then
			fdw_passo.setitem(1, "rs_flag_cappotta_frontale", istr_riepilogo.flag_cappotta_frontale)
		end if
		if not isnull(istr_riepilogo.num_gambe) and istr_riepilogo.num_gambe <> 0 then
			fdw_passo.setitem(1, "rn_num_gambe", istr_riepilogo.num_gambe)
		end if
		if not isnull(istr_riepilogo.num_campate) and istr_riepilogo.num_campate <> 0 then
			fdw_passo.setitem(1, "rn_num_campate", istr_riepilogo.num_campate)
		end if
		if not isnull(istr_riepilogo.flag_rollo) and istr_riepilogo.flag_rollo <> "" then
			fdw_passo.setitem(1, "rs_flag_rollo", istr_riepilogo.flag_rollo)
		end if
		if not isnull(istr_flags.cod_rollo) and istr_flags.cod_rollo <> "" then
			string ls_cod_rollo
			select des_prodotto
			into	 :ls_cod_rollo
			from	 anag_prodotti
			where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :istr_flags.cod_rollo;
					 
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Configuratore di Prodotto", "Errore durante l'Estrazione Descrizione Rollò")
				return -1
			elseif sqlca.sqlcode = 100 then
				ls_cod_rollo = ""
			end if
			fdw_passo.object.des_rollo.text = ls_cod_rollo
		end if
		if not isnull(istr_riepilogo.flag_kit_laterale) and istr_riepilogo.flag_kit_laterale <> "" then
			fdw_passo.setitem(1, "rs_flag_kit_laterale", istr_riepilogo.flag_kit_laterale)
		end if

	case "d_ext_tessuto", "d_ext_tessuto_dk", "d_ext_tessuto_alu"
		
		if not isnull(istr_riepilogo.cod_tessuto)	and istr_riepilogo.cod_tessuto <> "" then
			fdw_passo.setitem(1, "rs_cod_tessuto", istr_riepilogo.cod_tessuto)
			fdw_passo.setitem(1, "rs_cod_tessuto_manuale", istr_riepilogo.cod_tessuto)
		end if
		if not isnull(istr_riepilogo.cod_non_a_magazzino)	and istr_riepilogo.cod_non_a_magazzino <> "" then
			fdw_passo.setitem(1, "rs_cod_non_a_magazzino", istr_riepilogo.cod_non_a_magazzino)
		end if
		if not isnull(istr_riepilogo.cod_tessuto_mant)	and istr_riepilogo.cod_tessuto_mant <> "" then
			fdw_passo.setitem(1, "rs_cod_tessuto_mant", istr_riepilogo.cod_tessuto_mant)
			fdw_passo.setitem(1, "rs_cod_tessuto_mant_manuale", istr_riepilogo.cod_tessuto_mant)
		end if
		if not isnull(istr_riepilogo.cod_mant_non_a_magazzino)	and istr_riepilogo.cod_mant_non_a_magazzino <> "" then
			fdw_passo.setitem(1, "rs_cod_mant_non_a_magazzino", istr_riepilogo.cod_mant_non_a_magazzino)
		end if
		if not isnull(istr_riepilogo.alt_mantovana)	and istr_riepilogo.alt_mantovana <> 0 then
			fdw_passo.setitem(1, "rn_alt_mantovana", istr_riepilogo.alt_mantovana)
		end if
		if not isnull(istr_riepilogo.flag_tipo_mantovana)	and istr_riepilogo.flag_tipo_mantovana <> "" then
			fdw_passo.setitem(1, "rs_flag_tipo_mantovana", istr_riepilogo.flag_tipo_mantovana)
		end if
		if not isnull(istr_riepilogo.colore_cordolo)	and istr_riepilogo.colore_cordolo <> "" then
			fdw_passo.setitem(1, "rs_colore_cordolo", istr_riepilogo.colore_cordolo)
		end if
		if not isnull(istr_riepilogo.colore_passamaneria)	and istr_riepilogo.colore_passamaneria <> "" then
			fdw_passo.setitem(1, "rs_colore_passamaneria", istr_riepilogo.colore_passamaneria)
		end if
		if not isnull(istr_riepilogo.flag_addizionale_tessuto)	and istr_riepilogo.flag_addizionale_tessuto <> "" then
			fdw_passo.setitem(1, "rs_tess_addizionale", istr_riepilogo.flag_addizionale_tessuto)
		end if
		if not isnull(istr_riepilogo.flag_fc_tessuto)	and istr_riepilogo.flag_fc_tessuto <> "" then
			fdw_passo.setitem(1, "rs_tess_fuori_campionario", istr_riepilogo.flag_fc_tessuto)
		end if
		if not isnull(istr_riepilogo.flag_tessuto_cliente)	and istr_riepilogo.flag_tessuto_cliente <> "" then
			fdw_passo.setitem(1, "rs_flag_tessuto_cliente", istr_riepilogo.flag_tessuto_cliente)
		end if
		if not isnull(istr_riepilogo.flag_tessuto_mant_cliente)	and istr_riepilogo.flag_tessuto_mant_cliente <> "" then
			fdw_passo.setitem(1, "rs_flag_tessuto_mant_cliente", istr_riepilogo.flag_tessuto_mant_cliente)
		end if
		if fdw_passo.dataobject = "d_ext_tessuto_dk"  then
			if not isnull(istr_riepilogo.cod_modello_telo_finito)	and istr_riepilogo.cod_modello_telo_finito <> "" then
				fdw_passo.setitem(1, "rs_cod_modello_telo_finito", istr_riepilogo.cod_modello_telo_finito)
			end if
			fdw_passo.setitem(1, "pos_comando", istr_riepilogo.pos_comando)
		end if
		
		if fdw_passo.dataobject = "d_ext_tessuto_alu" then 
			fdw_passo.setitem(1,"rn_diam_tubetto_telo_sup",istr_riepilogo.diam_tubetto_telo_sup)
			fdw_passo.setitem(1,"rn_diam_tubetto_telo_inf",istr_riepilogo.diam_tubetto_telo_inf)
			fdw_passo.setitem(1,"rn_diam_tubetto_mant",istr_riepilogo.diam_tubetto_mant)
			if not isnull(istr_riepilogo.flag_tipo_unione)	and istr_riepilogo.flag_tipo_unione <> "" then
				fdw_passo.setitem(1,"rs_flag_tipo_unione",istr_riepilogo.flag_tipo_unione)
			else
				fdw_passo.setitem(1,"rs_flag_tipo_unione","000")
			end if
			if not isnull(istr_riepilogo.flag_orientamento_unione)	and istr_riepilogo.flag_orientamento_unione <> "" then
				fdw_passo.setitem(1,"rs_flag_orientamento_unione",istr_riepilogo.flag_orientamento_unione)
			end if
			if not isnull(istr_riepilogo.cod_tipo_tasca)	and istr_riepilogo.cod_tipo_tasca <> "" then
				fdw_passo.setitem(1,"rs_flag_tasca",istr_riepilogo.cod_tipo_tasca)
			end if
			if not isnull(istr_riepilogo.cod_tipo_tasca_1)	and istr_riepilogo.cod_tipo_tasca_1 <> "" then
				fdw_passo.setitem(1,"rs_cod_tipo_tasca_1",istr_riepilogo.cod_tipo_tasca_1)
			end if
			if not isnull(istr_riepilogo.cod_tipo_tasca_2)	and istr_riepilogo.cod_tipo_tasca_2 <> "" then
				fdw_passo.setitem(1,"rs_cod_tipo_tasca_2",istr_riepilogo.cod_tipo_tasca_2)
			end if
			if not isnull(istr_riepilogo.flag_mantovana)	and istr_riepilogo.flag_mantovana <> "" then
				fdw_passo.setitem(1,"rs_flag_mantovana",istr_riepilogo.flag_mantovana)
			end if
			fdw_passo.setitem(1,"rn_rapporto_mantovana",istr_riepilogo.rapporto_mantovana)
			fdw_passo.setitem(1,"rn_num_rinforzi",istr_riepilogo.num_rinforzi)
			fdw_passo.setitem(1,"rn_dimensione_rinforzo",istr_riepilogo.dimensione_rinforzo )
			if not isnull(istr_riepilogo.cod_reparto_telo) and istr_riepilogo.cod_reparto_telo <> "" then
				fdw_passo.setitem(1,"cod_reparto_telo",istr_riepilogo.cod_reparto_telo )
			else
				fdw_passo.setitem(1,"cod_reparto_telo","R_EST4" )
			end if
		end if

	case "d_ext_note"
		if not isnull(istr_riepilogo.note)	and istr_riepilogo.note <> "" then
			fdw_passo.setitem(1, "rs_note", istr_riepilogo.note)
		end if
	
	//-----------------------------------------------------------------
	case "d_ext_nota_prodotto_finito"
		if isnull(istr_riepilogo.flag_azzera_dt) or istr_riepilogo.flag_azzera_dt="" &
				or (istr_riepilogo.flag_azzera_dt<>'N' and istr_riepilogo.flag_azzera_dt<>'S') then
			fdw_passo.setitem(1, "flag_azzera_dt", "N")
		else
			fdw_passo.setitem(1, "flag_azzera_dt", istr_riepilogo.flag_azzera_dt)
		end if
	//-----------------------------------------------------------------
		
	case "d_ext_plisse"
		if not isnull(istr_riepilogo.flag_autoblocco) and istr_riepilogo.flag_autoblocco <> "" then
			fdw_passo.setitem(1, "rs_flag_autoblocco", istr_riepilogo.flag_autoblocco)
		end if
		if not isnull(istr_riepilogo.num_fili_plisse) and istr_riepilogo.num_fili_plisse <> 0 then
			fdw_passo.setitem(1, "rn_num_fili_plisse", istr_riepilogo.num_fili_plisse)
		end if
		if not isnull(istr_riepilogo.num_boccole_plisse) and istr_riepilogo.num_boccole_plisse <> 0 then
			fdw_passo.setitem(1, "rn_num_boccole_plisse", istr_riepilogo.num_boccole_plisse)
		end if
		if not isnull(istr_riepilogo.intervallo_punzoni_plisse) and istr_riepilogo.intervallo_punzoni_plisse <> 0 then
			fdw_passo.setitem(1, "rn_intervallo_punzoni_plisse", istr_riepilogo.intervallo_punzoni_plisse)
		end if
		if not isnull(istr_riepilogo.num_pieghe_plisse) and istr_riepilogo.num_pieghe_plisse <> 0 then
			fdw_passo.setitem(1, "rn_num_pieghe_plisse", istr_riepilogo.num_pieghe_plisse)
		end if
		if not isnull(istr_riepilogo.prof_inf_plisse) and istr_riepilogo.prof_inf_plisse <> 0 then
			fdw_passo.setitem(1, "rn_prof_inf_plisse", istr_riepilogo.prof_inf_plisse)
		end if
		if not isnull(istr_riepilogo.prof_sup_plisse) and istr_riepilogo.prof_sup_plisse <> 0 then
			fdw_passo.setitem(1, "rn_prof_sup_plisse", istr_riepilogo.prof_sup_plisse)
		end if
		
	case "d_ext_optional"
		string ls_cod_tipo_det_ven_optional
		long ll_i, ll_righe, ll_riga
		
		fdw_passo.reset()
		fdw_passo.setrowfocusindicator(Hand!)
		
		// Se il passo richiede dimensioni allora abilito o diassitivo i campi in base alle caratteristiche del prodotto
//		fdw_passo.Object.dim_x.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"
//		fdw_passo.Object.dim_y.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"
		fdw_passo.Object.dim_x.Background.Color = "0~tIf(flag_dimensioni = 'S', 134217752, rgb(192,192,192) )"
		fdw_passo.Object.dim_y.Background.Color = "0~tIf(flag_dimensioni = 'S', 134217752, rgb(192,192,192) )"
		
		ll_righe = upperbound(istr_riepilogo.optional)
		if ll_righe > 0 and not isnull(ll_righe) then
			for ll_i = 1 to ll_righe
				ll_riga = fdw_passo.insertrow(0)
				fdw_passo.setitem(ll_riga, "rs_cod_tipo_det_ven", istr_riepilogo.optional[ll_i].cod_tipo_det_ven)
				fdw_passo.setitem(ll_riga, "rs_cod_prodotto", istr_riepilogo.optional[ll_i].cod_prodotto)
				fdw_passo.setitem(ll_riga, "rs_des_prodotto", istr_riepilogo.optional[ll_i].des_prodotto)
				fdw_passo.setitem(ll_riga, "rd_quan_vendita", istr_riepilogo.optional[ll_i].quan_vendita)
				fdw_passo.setitem(ll_riga, "sconto_1", istr_riepilogo.optional[ll_i].sconto_1)
				fdw_passo.setitem(ll_riga, "sconto_2", istr_riepilogo.optional[ll_i].sconto_2)
				fdw_passo.setitem(ll_riga, "sconto_3", istr_riepilogo.optional[ll_i].sconto_3)
				fdw_passo.setitem(ll_riga, "sconto_4", istr_riepilogo.optional[ll_i].sconto_4)
				fdw_passo.setitem(ll_riga, "sconto_5", istr_riepilogo.optional[ll_i].sconto_5)
				fdw_passo.setitem(ll_riga, "sconto_6", istr_riepilogo.optional[ll_i].sconto_6)
				fdw_passo.setitem(ll_riga, "sconto_7", istr_riepilogo.optional[ll_i].sconto_7)
				fdw_passo.setitem(ll_riga, "sconto_8", istr_riepilogo.optional[ll_i].sconto_8)
				fdw_passo.setitem(ll_riga, "sconto_9", istr_riepilogo.optional[ll_i].sconto_9)
				fdw_passo.setitem(ll_riga, "sconto_10",istr_riepilogo.optional[ll_i].sconto_10)
				fdw_passo.setitem(ll_riga, "provvigione_1", istr_riepilogo.optional[ll_i].provvigione_1)
				fdw_passo.setitem(ll_riga, "provvigione_2", istr_riepilogo.optional[ll_i].provvigione_2)
				fdw_passo.setitem(ll_riga, "prezzo_vendita", istr_riepilogo.optional[ll_i].prezzo_vendita)
				fdw_passo.setitem(ll_riga, "rs_note", istr_riepilogo.optional[ll_i].note)
				fdw_passo.setitem(ll_riga, "rs_cod_misura", istr_riepilogo.optional[ll_i].cod_misura)
				fdw_passo.setitem(ll_riga, "rs_cod_iva", istr_riepilogo.optional[ll_i].cod_iva)
				fdw_passo.setitem(ll_riga, "dim_x", istr_riepilogo.optional[ll_i].dim_x)
				fdw_passo.setitem(ll_riga, "dim_y", istr_riepilogo.optional[ll_i].dim_y)
				fdw_passo.setitem(ll_riga, "flag_dimensioni", istr_riepilogo.optional[ll_i].flag_dimensioni)

				if istr_riepilogo.optional[ll_i].flag_dimensioni = "S" then
					ls_des_variabile = ""
					
					select des_variabile
					into   :ls_des_variabile
					from tab_des_variabili 
					where cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :istr_riepilogo.optional[ll_i].cod_prodotto and
							 cod_variabile = 'DIM_1';
					if sqlca.sqlcode <> 0 then ls_des_variabile = " - - - "
					fdw_passo.setitem(ll_riga, "des_dim_x", ls_des_variabile)
					
					ls_des_variabile = ""
					
					select des_variabile
					into   :ls_des_variabile
					from tab_des_variabili 
					where cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :istr_riepilogo.optional[ll_i].cod_prodotto and
							 cod_variabile = 'DIM_2';
					if sqlca.sqlcode <> 0 then ls_des_variabile = " - - - "
					fdw_passo.setitem(ll_riga, "des_dim_y", ls_des_variabile)
				end if
				
			next
		end if
		ll_riga = fdw_passo.insertrow(0)
		
		// trovo il tipo dettaglio opportuno da usare per le addizionali
		
		setnull(ls_cod_tipo_det_ven_optional)
		setnull(ls_cod_tipo_ord_ven)
		
		if upper(str_conf_prodotto.tipo_gestione) = "ORD_VEN" then
			select cod_tipo_ord_ven
			into   :ls_cod_tipo_ord_ven
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :str_conf_prodotto.anno_documento and
					 num_registrazione  = :str_conf_prodotto.num_documento;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Configuratore","Errore in ricerca tipi ordini dalla testata. Dettaglio errore "+sqlca.sqlerrtext)
				return -1
			end if
			
			if isnull(ls_cod_tipo_ord_ven) then
				g_mb.messagebox("Configuratore", "Tipo Ordine non impostato nella testata ordine." )
				return -1
			end if
	
			select cod_tipo_det_ven_optionals
			into 	:ls_cod_tipo_det_ven_optional
			from 	tab_flags_conf_tipi_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :str_conf_prodotto.cod_modello and 
					 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
				return -1
			end if
		end if
		
		
		if isnull(ls_cod_tipo_det_ven_optional) then
			select cod_tipo_det_ven_optionals
			into   :ls_cod_tipo_det_ven_optional
			from   tab_flags_configuratore
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :str_conf_prodotto.cod_modello;
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
				return -1
			end if
			if isnull(ls_cod_tipo_det_ven_optional) then
				g_mb.messagebox("Configuratore","Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella parametri configuratore")
				return -1
			end if
		end if
				 
		if not isnull(ls_cod_tipo_det_ven_optional) then
			fdw_passo.setitem(ll_riga, "rs_cod_tipo_det_ven", ls_cod_tipo_det_ven_optional)
		end if
		fdw_passo.setcolumn(2)
		
end choose
return 0
end function

public subroutine wf_azzera_variabili ();str_mp_automatiche lstr_mp_automatiche[]
str_optional lstr_optional[]
double ld_null[]

//str_conf_prodotto.stato = "N"
setnull(str_conf_prodotto.cod_versione)
setnull(str_conf_prodotto.des_prodotto)
istr_riepilogo.dimensione_1 = 0
istr_riepilogo.dimensione_2 = 0
istr_riepilogo.dimensione_3 = 0
istr_riepilogo.dimensione_4 = 0
istr_riepilogo.dimensione_5 = 0
istr_riepilogo.dimensione_6 = 0
istr_riepilogo.dimensione_7 = 0
istr_riepilogo.dimensione_8 = 0
istr_riepilogo.dimensione_9 = 0
setnull(istr_riepilogo.cod_tessuto)
setnull(istr_riepilogo.cod_non_a_magazzino)
setnull(istr_riepilogo.cod_tessuto_mant)
setnull(istr_riepilogo.cod_mant_non_a_magazzino)
setnull(istr_riepilogo.flag_tipo_mantovana)
setnull(istr_riepilogo.flag_tessuto_cliente)
setnull(istr_riepilogo.flag_tessuto_mant_cliente)
setnull(istr_riepilogo.cod_verniciatura)
setnull(istr_riepilogo.cod_verniciatura_2)
setnull(istr_riepilogo.cod_verniciatura_3)
setnull(istr_riepilogo.cod_comando)
setnull(istr_riepilogo.pos_comando)
setnull(istr_riepilogo.flag_addizionale_comando_1)
setnull(istr_riepilogo.cod_comando_2)
setnull(istr_riepilogo.des_comando_2)
setnull(istr_riepilogo.flag_addizionale_comando_2)
setnull(istr_riepilogo.cod_tipo_supporto)
setnull(istr_riepilogo.flag_addizionale_supporto)
setnull(istr_riepilogo.note)
setnull(istr_riepilogo.colore_passamaneria)
setnull(istr_riepilogo.colore_cordolo)
setnull(istr_riepilogo.des_vernice_fc)
setnull(istr_riepilogo.des_vernice_fc_2)
setnull(istr_riepilogo.des_vernice_fc_3)
setnull(istr_riepilogo.des_comando_1)
setnull(istr_riepilogo.flag_addizionale_vernic)
setnull(istr_riepilogo.flag_addizionale_vernic_2)
setnull(istr_riepilogo.flag_addizionale_vernic_3)
setnull(istr_riepilogo.flag_fc_vernic)
setnull(istr_riepilogo.flag_fc_vernic_2)
setnull(istr_riepilogo.flag_fc_vernic_3)
setnull(istr_riepilogo.cod_verniciatura_2)
setnull(istr_riepilogo.cod_verniciatura_3)

istr_riepilogo.alt_mantovana=0
istr_riepilogo.alt_asta=0
istr_riepilogo.flag_misura_luce_finita="F"
istr_riepilogo.mp_automatiche[] = lstr_mp_automatiche[]
istr_riepilogo.prezzo_vendita = 0
istr_riepilogo.sconti = ld_null
istr_riepilogo.maggiorazioni = ld_null
istr_riepilogo.optional = lstr_optional

setnull(istr_riepilogo.cod_modello_telo_finito)

istr_riepilogo.diam_tubetto_telo_sup = "0"
istr_riepilogo.diam_tubetto_telo_inf = "0"
istr_riepilogo.diam_tubetto_mant = "0"
setnull(istr_riepilogo.flag_tipo_unione)
setnull(istr_riepilogo.flag_orientamento_unione)
setnull(istr_riepilogo.cod_tipo_tasca)
istr_riepilogo.flag_mantovana="S"
istr_riepilogo.rapporto_mantovana = 0
istr_riepilogo.num_rinforzi = 0
setnull(istr_riepilogo.cod_tipo_tasca_1)
setnull(istr_riepilogo.cod_tipo_tasca_2)
setnull(istr_riepilogo.cod_reparto_telo)

return 
end subroutine

public function integer wf_clicked (ref datawindow fdw_passo, string fs_oggetto, ref string fs_messaggio);//	Funzione che gestisce evento clicked delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_clieck
// tipo: none
// ----------------------------------------------------------------------------------------
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_oggetto			 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-01-2007 
//		Autore Enrico Menegotto
// -----------------------------------------------------------------------------------------
string ls_nome_dw, ls_null

ls_nome_dw = fdw_passo.dataobject
choose case ls_nome_dw
	case "d_ext_inizio"

	case "d_ext_tessuto"
		
	case "d_ext_tessuto_alu"
		setnull(ls_null)
		choose case lower(fs_oggetto)
			case "b_reset_tasca_1"
					fdw_passo.setitem(fdw_passo.getrow(), "rs_flag_tasca", ls_null)
			case "b_reset_tasca_2"
					fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tipo_tasca_1", ls_null)
			case "b_reset_tasca_3"
					fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tipo_tasca_2", ls_null)
		end choose
		
 	case "d_ext_vern_lastra"

 	case "d_ext_verniciature"

	case "d_ext_comandi"
		if fs_oggetto = "b_comandi" then
			if not isnull(w_configuratore.istr_riepilogo.cod_verniciatura) then
				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_comando",sqlca,&
				  "anag_prodotti","cod_prodotto","des_prodotto",&
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in (SELECT tab_limiti_comando.cod_comando  " + &
					" FROM tab_limiti_comando,   tab_limiti_comando_vern  " + &
					" WHERE ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and    " + &
					" ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and    " + &
					" ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) and    " + &
					" ( ( tab_limiti_comando.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND    " + &
					" ( tab_limiti_comando_vern.cod_verniciatura = '"+w_configuratore.istr_riepilogo.cod_verniciatura+"' ) AND    " + &
					" ( tab_limiti_comando.cod_modello_tenda = '" + str_conf_prodotto.cod_modello + "' ))) ")
					
				fdw_passo.Modify("b_comandi.Enabled ='No'")
			end if
		end if
	case "d_ext_supporto_flags"

	case "d_ext_plisse"
		
	case "d_ext_optional"

	case "d_ext_nota_prodotto_finito"

end choose



return 0
end function

public function integer wf_reset_tessuti (datawindow fdw_passo, string fs_tessuto_mantovana);uo_itemchanged uo_itemchg

uo_itemchg = CREATE uo_itemchanged
uo_itemchg.uof_reset_tessuto(fdw_passo, fs_tessuto_mantovana)

destroy uo_itemchg

return 0
end function

public function double wf_calcola_totale ();double ld_totale = 0
double ld_prezzo, ld_sconto
long start_pos=1
string old_str, new_str, mystring
long ll_index, ll_rows

//impostazione decimal separetor
if dec("2,1") = 2.1 then
	//decimal sep = ,
	old_str = "."
	new_str = ","
else
	//decimal sep = .
	old_str = ","
	new_str = "."
end if

//rimpiazza la virgola con il punto --------PREZZO------
mystring = st_prezzo.text
if not isnull(mystring) and mystring<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(mystring, old_str, start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 mystring = Replace(mystring, start_pos, &
		 Len(old_str), new_str)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(mystring, old_str, &
		 start_pos+Len(new_str))
	LOOP	
else
	mystring="0"
end if
ld_prezzo = double(mystring)
//---------------------------------------------------------------------
//rimpiazza la virgola con il punto --------SCONTO------
mystring = st_sconti.text
if not isnull(mystring) and mystring<>"" then
	
	// Find the first occurrence of old_str.
	start_pos = Pos(mystring, old_str, start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 mystring = Replace(mystring, start_pos, &
		 Len(old_str), new_str)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(mystring, old_str, &
		 start_pos+Len(new_str))
	LOOP	
else
	mystring="0"
end if
ld_sconto = double(mystring)
//---------------------------------------------------------------------
if isnull(ld_sconto) then ld_sconto = 0
if ld_prezzo > 0 then ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100

choose case is_nome_passo_optional
	case dw_passo_1.dataobject
		ll_rows = dw_passo_1.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_1.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_1.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_2.dataobject
		ll_rows = dw_passo_2.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_2.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_2.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_3.dataobject
		ll_rows = dw_passo_3.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_3.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_3.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_4.dataobject
		ll_rows = dw_passo_4.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_4.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_4.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_5.dataobject
		ll_rows = dw_passo_5.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_5.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_5.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_6.dataobject
		ll_rows = dw_passo_6.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_6.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_6.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_7.dataobject
		ll_rows = dw_passo_7.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_7.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_7.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
	case dw_passo_8.dataobject
		ll_rows = dw_passo_8.rowcount()
		for ll_index = 1 to ll_rows
			ld_prezzo = dw_passo_8.getitemdecimal(ll_index, "prezzo_vendita")
			ld_sconto = dw_passo_8.getitemdecimal(ll_index, "sconto_1")
			if isnull(ld_prezzo) then ld_prezzo = 0
			if isnull(ld_sconto) then ld_sconto = 0
			if ld_prezzo > 0 then
				ld_totale += ld_prezzo - (ld_prezzo * ld_sconto) / 100
			end if			
		next
		
end choose

return ld_totale
end function

public function integer wf_gest_mp_non_richieste (decimal fd_dim_1, decimal fd_dim_2, decimal fd_dim_3, decimal fd_dim_4, decimal fd_altezza_comando);/*Funzione che gestisce il caricamento delle Materie Prime

Creata il 04-09-98 
Autore Enrico Menegotto
modificata il 04/09/1998
modificata in Ptenda 30/07/1999
enrico 13/10/1999: numero illimitato di mp automatiche

EnMe 03/02/2009: Adeguata la funzione per l'uso con le offerte clienti
EnMe 29/03/2012: 	gestione della seconda e terza verniciatura 
							gestione anche della dimensione 3 e 4 per cercare le MP automatiche	
EnMe 11/01/2013	Esportata funzione nell'oggetto uo_conf_varianti
*/
string ls_errore
long 	ll_ret
uo_conf_varianti luo_conf_varianti

luo_conf_varianti = CREATE uo_conf_varianti
ll_ret = luo_conf_varianti.uof_carica_mp_automatiche( 	str_conf_prodotto.cod_modello, &
																		fd_dim_1, &
																		fd_dim_2, &
																		fd_dim_3, &
																		fd_dim_4, &
																		fd_altezza_comando, &
																		istr_riepilogo.cod_verniciatura, &
																		istr_riepilogo.cod_verniciatura_2, &
																		istr_riepilogo.cod_verniciatura_3, &
																		ref istr_riepilogo.mp_automatiche, &
																		ref ls_errore)
															  
choose case ll_ret
	case -1
		g_mb.error(ls_errore + "~r~nOBJECT:uo_conf_varianti / Function:uof_carica_mp_automatiche")
		return -1
end choose

return 0
end function

public subroutine wf_costruisci_passi_new ();//Funzione che costruisce i passi del configuratore a partire dal modello scelto (str_conf_prodotto.cod_modello)
//		assegna le datawindow e i commenti dei vari passi estraendoli dalla tabella tab_passi_configuratore
//		dopodichè chiama la funzione wf_set_dddw per inizializzare le Drop Down DW di ogni DW
//
// nome: wf_costruisci_passi
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//		Creata il 06/04/2012
//		Autore Enrico Menegotto

string ls_nome_data_window, ls_des_visualizzata, ls_mini_help, ls_str,ls_path_immagine_passo
integer li_count, li_num_passi, li_altezza, li_begin, li_end, ll_i


select count(*)
 into :li_num_passi
 from tab_passi_configuratore
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_modello = :str_conf_prodotto.cod_modello;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Configuratore Prodotto", "Errore durante la Costruzione dei Passi")
	return
end if

declare cur_passi_configuratore cursor for
  select nome_data_window,   
         des_visualizzata,
			mini_help,
			path_immagine_passo
    from tab_passi_configuratore
   where cod_azienda = :s_cs_xx.cod_azienda and
         cod_modello = :str_conf_prodotto.cod_modello
order by cod_modello ASC, prog_passo_configuratore ASC;
open cur_passi_configuratore;


li_count = 0
li_begin = 25
//li_altezza = 150
li_altezza = 900

dw_steps.reset()
dw_steps.insertrow(0)
dw_steps.setredraw(false)
dw_steps.setitem(1, "step_start", "Inizio")

for ll_i = 1 to 8
	dw_steps.modify("step_" + string(ll_i) + ".visible=0")
	dw_steps.modify("step_" + string(ll_i) + ".y=1")
next

dw_steps.setitem(1, "step_end", "Fine Configurazione")

// inizio routine di costruzione dei passi

do while true
	
	fetch cur_passi_configuratore into :ls_nome_data_window, :ls_des_visualizzata, :ls_mini_help, :ls_path_immagine_passo;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in costruzione passi~r~n" + sqlca.sqlerrtext)
		return 
	end if

	li_count ++

	choose case li_count
		case 1
			ls_str = dw_passo_1.dataobject
			dw_passo_1.dataobject = ls_nome_data_window
			dw_passo_1.height = 1420
			if ls_str = "" then 
				dw_passo_1.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
			else
				dw_passo_1.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_1)
			
			dw_steps.setitem(1,"step_1", ls_des_visualizzata)
			dw_steps.Object.step_1.visible = 1
//			dw_steps.Object.step_1.y = li_begin + li_altezza
			dw_steps.Object.step_1.x = li_begin + integer(dw_steps.Object.step_1.width)
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_1)
		case 2
			ls_str = dw_passo_2.dataobject
			dw_passo_2.dataobject = ls_nome_data_window
			dw_passo_2.height = 1420
			if ls_str = "" then
				dw_passo_2.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
			else
				dw_passo_2.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_2)
			
			dw_steps.setitem(1,"step_2", ls_des_visualizzata)
			dw_steps.Object.step_2.visible = 1
//			dw_steps.Object.step_2.y = li_begin + ( li_altezza * 2 )
			dw_steps.Object.step_2.x = integer(dw_steps.Object.step_1.x) + integer(dw_steps.Object.step_1.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_2)
		case 3
			ls_str = dw_passo_3.dataobject
			dw_passo_3.dataobject = ls_nome_data_window
			dw_passo_3.height = 1420
			if ls_str = "" then
				dw_passo_3.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_3.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_3)
			
			dw_steps.setitem(1,"step_3", ls_des_visualizzata)
			dw_steps.Object.step_3.visible = 1
//			dw_steps.Object.step_3.y = li_begin + ( li_altezza * 3 )
			dw_steps.Object.step_3.x = integer(dw_steps.Object.step_2.x) + integer(dw_steps.Object.step_2.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_3)
		case 4
			ls_str = dw_passo_4.dataobject
			dw_passo_4.dataobject = ls_nome_data_window
			dw_passo_4.height = 1420
			if ls_str = "" then
				dw_passo_4.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_4.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_4)
			
			dw_steps.setitem(1,"step_4", ls_des_visualizzata)
			dw_steps.Object.step_4.visible = 1
//			dw_steps.Object.step_4.y = li_begin + ( li_altezza * 4 )
			dw_steps.Object.step_4.x = integer(dw_steps.Object.step_3.x) + integer(dw_steps.Object.step_3.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_4)
		case 5
			ls_str = dw_passo_5.dataobject
			dw_passo_5.dataobject = ls_nome_data_window
			dw_passo_5.height = 1420
			if ls_str = "" then
				dw_passo_5.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_5.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_5)
			
			dw_steps.setitem(1,"step_5", ls_des_visualizzata)
			dw_steps.Object.step_5.visible = 1
//			dw_steps.Object.step_5.y = li_begin + ( li_altezza * 5 )
			dw_steps.Object.step_5.x = integer(dw_steps.Object.step_4.x) + integer(dw_steps.Object.step_4.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_5)
		case 6
			ls_str = dw_passo_6.dataobject
			dw_passo_6.dataobject = ls_nome_data_window
			dw_passo_6.height = 1420
			if ls_str = "" then 
				dw_passo_6.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_6.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_6)
			
			dw_steps.setitem(1,"step_6", ls_des_visualizzata)
			dw_steps.Object.step_6.visible = 1
//			dw_steps.Object.step_6.y = li_begin + ( li_altezza * 6 )
			dw_steps.Object.step_6.x = integer(dw_steps.Object.step_5.x) + integer(dw_steps.Object.step_5.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_6)
		case 7
			ls_str = dw_passo_7.dataobject
			dw_passo_7.dataobject = ls_nome_data_window
			dw_passo_7.height = 1420
			if ls_str = "" then
				dw_passo_7.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_7.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_7)
			
			dw_steps.setitem(1,"step_2", ls_des_visualizzata)
			dw_steps.Object.step_7.visible = 1
//			dw_steps.Object.step_7.y = li_begin + ( li_altezza * 7 )
			dw_steps.Object.step_7.x = integer(dw_steps.Object.step_6.x) + integer(dw_steps.Object.step_6.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_7)
		case 8
			ls_str = dw_passo_8.dataobject
			dw_passo_8.dataobject = ls_nome_data_window
			dw_passo_8.height = 1420
			if ls_str = "" then
				dw_passo_8.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
				//		  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
			else
				dw_passo_8.insertrow(0)
			end if
			wf_set_dddw(ref dw_passo_8)
			
			dw_steps.setitem(1,"step_2", ls_des_visualizzata)
			dw_steps.Object.step_8.visible = 1
//			dw_steps.Object.step_8.y = li_begin + ( li_altezza * 8 )
			dw_steps.Object.step_8.x = integer(dw_steps.Object.step_7.x) + integer(dw_steps.Object.step_7.width) + 50
			
			is_mini_help[li_count] = ls_mini_help
			wf_costruisci_passo_nota(dw_passo_8)
	end choose
loop

ii_quantita_passi = li_count
close cur_passi_configuratore;
dw_steps.setredraw(true)

wf_visualizza_dw()
end subroutine

public function integer wf_costruisci_passo_nota (ref uo_cs_xx_dw dw_passo);string ls_str

if dw_passo.dataobject = is_nome_passo_optional then
	dw_passo.height = 1400
	ls_str = dw_nota_prodotto_finito.dataobject
	dw_nota_prodotto_finito.dataobject = 'd_ext_nota_prodotto_finito'
	if ls_str = '' then
		dw_nota_prodotto_finito.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, c_default+ c_nohighlightselected)
		dw_nota_prodotto_finito.width=2482
		dw_nota_prodotto_finito.height=456
	else
		dw_nota_prodotto_finito.insertrow(0)
	end if
	
	dw_nota_prodotto_finito_lista.dataobject = 'd_ext_nota_prodotto_finito_lista_note'
	
	//Donato 30/12/2014
	//anomalia 2014/74, cambiando codice modello e rimettendolo poi subito di nuovo sparivano le note prodotto selezionabili ...
	//La retrieve delle note di prodotto disponibili da selezionare conviene farla sempre, a patto che ci sia il dataobject ...
	//quindi ho aggiunto la istruzione seguente
	ls_str = dw_nota_prodotto_finito_lista.dataobject
	
	//e commentato l'IF più sotto       if ls_str = '' then...         sostituendolo con         if ls_str <> '' and not isnull(ls_str) then...
	//prima, infatti, ls_str, se valorizzata, era con il dataobject "d_ext_nota_prodotto_finito" 
	//e quando si cambiava codice modello, non passava mai più dentro l'IF, in quanto trovava la variabile diversa da stringa vuoto
	//Cioè faceva la retrieve solo all'inizio e poi mai più, quindi cambiando modello non si aggiornavano più ...
	
	//if ls_str = '' then
	if ls_str <> '' and not isnull(ls_str) then
		dw_nota_prodotto_finito_lista.y = dw_nota_prodotto_finito.y + 22
		dw_nota_prodotto_finito_lista.height = dw_nota_prodotto_finito.height - 35
		dw_nota_prodotto_finito_lista.settransobject(sqlca)
		dw_nota_prodotto_finito_lista.retrieve(s_cs_xx.cod_azienda, str_conf_prodotto.cod_modello )
	end if
	
end if

return 0
end function

public function integer wf_peso_netto (integer ai_anno_documento, long ai_numero_documento, long ai_riga_documento, ref string as_messaggio);//calcolo peso netto riga configurata e relative collegate
dec{5} ld_peso_netto_unitario
long ll_riga_collegata
uo_funzioni_1 luo_funzioni_1
integer li_ret

luo_funzioni_1 = create uo_funzioni_1

as_messaggio = ""

//riga principale
ld_peso_netto_unitario  = 0
li_ret = luo_funzioni_1.uof_calcola_peso_riga_ordine(ai_anno_documento, ai_numero_documento, ai_riga_documento, ld_peso_netto_unitario, as_messaggio)

if li_ret<0 then
	as_messaggio = "Errore in fase di calcolo peso netto unitario della riga "+string(ai_riga_documento) + ": " + as_messaggio
	destroy luo_funzioni_1
	rollback;
	return -1
else
	//faccio update riga principale -----------------
	update det_ord_ven
	set peso_netto=:ld_peso_netto_unitario
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_documento and
				num_registrazione=:ai_numero_documento and
				prog_riga_ord_ven=:ai_riga_documento;
	
	if sqlca.sqlcode<0 then
		as_messaggio = "Errore in fase di aggiornamento peso netto unitario della riga "+string(ai_riga_documento) + ": " + sqlca.sqlerrtext
		destroy luo_funzioni_1
		rollback;
		return -1
	end if
	//--------------------------------------------------
end if

//righe collegate, se ce ne sono ...
declare cu_optionals cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_documento and
				num_registrazione=:ai_numero_documento and
				num_riga_appartenenza=:ai_riga_documento and
				cod_prodotto is not null
	order by prog_riga_ord_ven;

open cu_optionals;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in apertura cursore 'cu_optionals'~r~n" + sqlca.sqlerrtext
	destroy luo_funzioni_1
	rollback;
	return -1
end if

do while true
	fetch cu_optionals into :ll_riga_collegata;

	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore 'cu_optionals'~r~n" + sqlca.sqlerrtext
		close cu_optionals;
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	ld_peso_netto_unitario  = 0
	li_ret = luo_funzioni_1.uof_calcola_peso_riga_ordine(ai_anno_documento, ai_numero_documento, ll_riga_collegata, ld_peso_netto_unitario, as_messaggio)
	
	if li_ret<0 then
		as_messaggio = "Errore in fase di calcolo peso netto unitario della riga collegata "+string(ll_riga_collegata) + ": " + as_messaggio
		destroy luo_funzioni_1
		close cu_optionals;
		rollback;
		return -1
	else
		
		//faccio update riga collegata ----------------------
		update det_ord_ven
		set peso_netto=:ld_peso_netto_unitario
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno_documento and
					num_registrazione=:ai_numero_documento and
					prog_riga_ord_ven=:ll_riga_collegata;
		
		if sqlca.sqlcode<0 then
			as_messaggio = "Errore in fase di aggiornamento peso netto unitario della riga "+string(ai_riga_documento) + ": " + sqlca.sqlerrtext
			close cu_optionals;
			destroy luo_funzioni_1
			rollback;
			return -1
		end if
		//--------------------------------------------------
	end if
loop

close cu_optionals;
destroy luo_funzioni_1

return 0
end function

public function integer wf_aggiorna_varianti_e_calendario (long al_singola_riga, ref string as_errore);string									ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], ls_cod_prod, ls_cod_vers, ls_riga_ordine
date									ldt_dtp_rep_par[], ldt_dtp_rep_arr[], ldd_vuoto[]
integer								li_ret, ll_index
uo_produzione						luo_prod
uo_calendario_prod_new			luo_cal_prod
string									ls_cod_prodotto[], ls_cod_versione[], ls_flag_tipo_bcl
long									ll_righe_ordine[], ll_temp
integer								li_i
datetime								ldt_vuoto[], ldt_date_reparti[]




if al_singola_riga>0  then
	//solo riga corrente
	ll_righe_ordine[1] = al_singola_riga
	
	select cod_prodotto, cod_versione
	into :ls_cod_prodotto[1], :ls_cod_versione[1]
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:str_conf_prodotto.anno_documento and
				num_registrazione=:str_conf_prodotto.num_documento and
				prog_riga_ord_ven=:ll_righe_ordine[1];
	
else
	//il ricalcolo riguarda tutto l'ordine
	
	ib_backup_vardetordven_prod = false
	
	declare cu_righe cursor for  
	select prog_riga_ord_ven, cod_prodotto, cod_versione
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:str_conf_prodotto.anno_documento and
				num_registrazione=:str_conf_prodotto.num_documento and
				cod_prodotto is not null and cod_versione is not null and cod_versione <>'' and
				(num_riga_appartenenza=0 or num_riga_appartenenza is null);

	open cu_righe;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in OPEN cursore cu_righe (wf_aggiorna_varianti_e_calendario): " + sqlca.sqlerrtext
		return -1
	end if

	

	do while true
		
		fetch cu_righe into :ll_temp, :ls_cod_prod, :ls_cod_vers;
	
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in fetch cursore cu_righe  (wf_aggiorna_varianti_e_calendario):" + sqlca.sqlerrtext
			close cu_righe;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		li_i += 1
		ll_righe_ordine[li_i] = ll_temp
		ls_cod_prodotto[li_i] = ls_cod_prod
		ls_cod_versione[li_i] = ls_cod_vers
		
	loop
	close cu_righe;
end if
//----------------------------------------------------------------------


luo_prod = create uo_produzione
luo_cal_prod = create uo_calendario_prod_new


luo_prod.uof_trova_tipo(str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, ls_flag_tipo_bcl, as_errore)
as_errore = ""

//controlla se hai deciso di spostare la data partenza dell'intero ordine
if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
	luo_prod.idt_data_partenza = idt_data_partenza
end if

is_reparti[] = ls_vuoto[]

for li_i=1 to upperbound(ll_righe_ordine[])
	ls_riga_ordine = "Riga Ordine "+string(str_conf_prodotto.anno_documento)+"/"+string(str_conf_prodotto.num_documento)+"/"+string(ll_righe_ordine[li_i])+"~r~n~r~n"

	ls_dep_par[] = ls_vuoto[]
	ls_rep_par[] = ls_vuoto[]
	ls_dep_arr[] = ls_vuoto[]
	ls_rep_arr[] = ls_vuoto[]
	
	ldt_dtp_rep_par[] = ldd_vuoto[]
	ldt_dtp_rep_arr[] = ldd_vuoto[]
	
	luo_prod.il_riga_ordine = -1
	
	//li_ret = -1
	if ls_flag_tipo_bcl = "A" then
		if ib_backup_vardetordven_prod and upperbound(ll_righe_ordine[])=1 and isvalid(ids_backup_vardetordven_prod) then
			//in tal caso upperbound(ll_righe_ordine[]) è sicuramente 1 e si vuole conservare la configurazione della tabella det_ord_ven_prod
			//che si aveva all'inizio della configurazione (tutto salvato in ids_backup_vardetordven_prod), a patto di trovare gli stessi reparti di produzione
			//li_ret = wf_imposta_varianti_prod_da_backup(ls_rep_par[], ldt_dtp_rep_par[], ls_rep_arr[], ldt_dtp_rep_arr[])
			li_ret = wf_imposta_varianti_prod_da_backup()
			luo_prod.il_riga_ordine = ll_righe_ordine[li_i]
		else
			li_ret = -1
		end if
	end if
	
	
	//if li_ret<0 then
		//ricalcolo le date prima del salvataggio definitivo ------
		li_ret = luo_prod.uof_cal_trasferimenti(	str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, &
															ls_cod_prodotto[li_i], ls_cod_versione[li_i], &
															ls_dep_par[], ls_rep_par[], ldt_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldt_dtp_rep_arr[], as_errore)
	//end if
	
	if ll_righe_ordine[li_i]=str_conf_prodotto.prog_riga_documento then
		is_reparti[] = ls_rep_par[]
	end if
	
	
	//se non ci sono errori salvo -------------------------------
	//se ci sono errori, in questa fase me ne frego ... o meglio inutile che vado avanti
	//probabilmente non hai trovato navette o eventi
	if li_ret = 0 then
		//se torna ZERO sicuramente ho le date e i reparti
		
		ldt_date_reparti[] =  ldt_vuoto[]
		
		for ll_index = 1 to upperbound(ls_rep_par[])
			
			if ls_rep_arr[ll_index]="" then setnull(ls_rep_arr[ll_index])
			
			if ls_flag_tipo_bcl = "A" then
				
				update varianti_det_ord_ven_prod
				set		data_pronto = :ldt_dtp_rep_par[ll_index],
							data_arrivo = :ldt_dtp_rep_arr[ll_index],
							cod_reparto_arrivo = :ls_rep_arr[ll_index]
				where 	anno_registrazione = :str_conf_prodotto.anno_documento and 
							num_registrazione = :str_conf_prodotto.num_documento and
							prog_riga_ord_ven = :ll_righe_ordine[li_i] and
							cod_reparto = :ls_rep_par[ll_index];
				
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in UPDATE tabella 'varianti_det_ord_ven_prod' con data_pronto.~r~n" + sqlca.sqlerrtext
					
					destroy luo_prod
					destroy uo_calendario_prod_new
					return -1
				end if
				
			else
				//tenda tecnica
				update det_ord_ven
				set		cod_reparto_sfuso = :ls_rep_par[ll_index],
						data_pronto_sfuso = :ldt_dtp_rep_par[ll_index],
						cod_reparto_arrivo = :ls_rep_arr[ll_index],
						data_arrivo = :ldt_dtp_rep_arr[ll_index]
				where 	anno_registrazione = :str_conf_prodotto.anno_documento and 
							num_registrazione = :str_conf_prodotto.num_documento and
							prog_riga_ord_ven = :ll_righe_ordine[li_i];
				
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in update det_ord_ven (cod_reparto-data_pronto).~r~n" + sqlca.sqlerrtext
					
					destroy luo_prod
					destroy uo_calendario_prod_new
					return -1
				end if
				
			end if
			
			ldt_date_reparti[ll_index] = datetime(ldt_dtp_rep_par[ll_index] ,00:00:00)
			
		next
	
		//se arrivi fin qui aggiorna l'impegno in calendario
		if upperbound(ls_rep_par[])>0 then
			li_ret = luo_cal_prod. uof_salva_in_calendario(	str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, ll_righe_ordine[li_i], &
																	ls_rep_par[], ldt_date_reparti[], as_errore)
			
			if li_ret < 0 then
				destroy luo_prod
				destroy uo_calendario_prod_new
				return -1
			end if
		end if
		
	else
		as_errore = ls_riga_ordine + as_errore
		destroy luo_prod
		destroy uo_calendario_prod_new
		return 1
	end if

next

destroy luo_prod
destroy uo_calendario_prod_new


return 0
end function

public function integer wf_backup_var_det_ord_ven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);/*
Questa funzione viene chiamata all'apertura del configuratore, quando si entra in modifica
Memorizza lo stato della tabella det_ord_ven_prod nel datastore di istanza "ids_backup_vardetordven_prod":
	- cod_reparto produzione
	- data_pronto
	- cod_reparto_arrivo
	- data_arrivo

Alla fine del configuratore, dopo che le varianti e le varianti_produzione sono state cancellate e re-inserite,
vengono ciclate, e se il reparto presente in varianti_det_ord_ven_prod è presente nel datastore
vengono riportati i dati relativi a data pronto reparto arrivo e data arrivo in varianti_det_ord_ven_prod.

*/

string				ls_sql
long				ll_ret

//conservo solo quelle con deposito produzione -------------------------------------------------------------------------------------------------------
ls_sql = 	"select distinct cod_prodotto_padre, num_sequenza, cod_prodotto_figlio,"+&
					 "cod_versione, cod_versione_figlio, cod_deposito_produzione "+&
			"from varianti_det_ord_ven "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"anno_registrazione="+string(ai_anno_ordine)+" and "+&
					"num_registrazione="+string(al_num_ordine)+" and "+&
					"prog_riga_ord_ven="+string(al_riga_ordine)+" and "+&
					"cod_deposito_produzione is not null "+&
			"order by num_sequenza"

ll_ret = guo_functions.uof_crea_datastore(ids_backup_vardetordven, ls_sql, as_errore)

if ll_ret>0 then
else
	ib_backup_vardetordven_prod = false
	return 0
end if


//conservo solo quelle con cod_reparto e legate ad un deposito produzione -----------------------------------------------------------------------
ls_sql = 	"select distinct a.cod_prodotto_padre, a.num_sequenza, a.cod_prodotto_figlio,"+&
					 "a.cod_versione, a.cod_versione_figlio, a.cod_reparto,"+&
					 "a.data_pronto, a.cod_reparto_arrivo, a.data_arrivo, b.cod_deposito_produzione "+&
			"from varianti_det_ord_ven_prod as a "+&
			"join varianti_det_ord_ven as b on b.cod_azienda=a.cod_azienda and "+&
												"b.anno_registrazione=a.anno_registrazione and "+&
												"b.num_registrazione=a.num_registrazione and "+&
												"b.prog_riga_ord_ven=a.prog_riga_ord_ven and "+&
												"b.cod_prodotto_padre=a.cod_prodotto_padre and "+&
												"b.num_sequenza=a.num_sequenza and "+&
												"b.cod_prodotto_figlio=a.cod_prodotto_figlio and "+&
												"b.cod_versione=a.cod_versione and "+&
												"b.cod_versione_figlio=a.cod_versione_figlio "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"a.anno_registrazione="+string(ai_anno_ordine)+" and "+&
					"a.num_registrazione="+string(al_num_ordine)+" and "+&
					"a.prog_riga_ord_ven="+string(al_riga_ordine)+" and "+&
					"a.cod_reparto is not null and "+&
					"b.cod_deposito_produzione is not null "

ll_ret = guo_functions.uof_crea_datastore(ids_backup_vardetordven_prod, ls_sql, as_errore)

if ll_ret>0 then
	ib_backup_vardetordven_prod = true
else
	ib_backup_vardetordven_prod = false
	ids_backup_vardetordven.reset()
end if

return 0
end function

public function integer wf_imposta_varianti_prod_da_backup ();/*
Questa funzione viene chiamata, quando si vuole tentare di re-impostare la tabella varianti_det_ord_ven_prod allo stato che aveva
prima dell'inizio della configurazione. E' un tentativo che andrà a buon fine solo se la tabella varianti_det_ord_ven non è cvambiata
Cioè se è stata creata manualmente una variante dalla finestra delle varianti e poi si rientra nel configuratore sicuramente
questa funzione non ce la farà e quindi verranno re-impostate lòe varianti lette dalla distinta base e non quelle salvate prima dell'inizio della configurazione
*/


string					ls_cod_reparto, ls_err
long					ll_k, ll_index, ll_index2, ll_tot

string					ls_cod_prodotto_padre_cu, ls_cod_prodotto_figlio_cu, ls_cod_versione_cu, ls_cod_versione_figlio_cu, ls_cod_deposito_prod_cu, ls_cod_dep_prod
string					ls_cod_prodotto_padre_ds, ls_cod_prodotto_figlio_ds, ls_cod_versione_ds, ls_cod_versione_figlio_ds, ls_cod_deposito_prod_ds
string					ls_cod_prodotto_padre_ds2, ls_cod_prodotto_figlio_ds2, ls_cod_versione_ds2, ls_cod_versione_figlio_ds2, ls_cod_deposito_prod_ds2
long					ll_num_sequenza_cu, ll_num_sequenza_ds, ll_num_sequenza_ds2

datastore			lds_varianti
string					ls_sql



//leggo le varianti con deposito produzione, e ripristino il deposito iniziale e relativi reparti
ls_sql = "select distinct 	cod_prodotto_padre,"+&
								"num_sequenza,"+&
								"cod_prodotto_figlio,"+&
								"cod_versione,"+&
								"cod_versione_figlio,"+&
								"cod_deposito_produzione "+&
		"from varianti_det_ord_ven "+&
		"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"anno_registrazione="+string(str_conf_prodotto.anno_documento)+" and "+&
					"num_registrazione="+string(str_conf_prodotto.num_documento)+" and "+&
					"prog_riga_ord_ven="+string(str_conf_prodotto.prog_riga_documento)+" and "+&
					"cod_deposito_produzione is not null "

ll_tot = guo_functions.uof_crea_datastore(lds_varianti, ls_sql, ls_err)


for ll_k=1 to ll_tot
	//leggo
	ls_cod_prodotto_padre_cu		= lds_varianti.getitemstring(ll_k, "cod_prodotto_padre")
	ll_num_sequenza_cu				= lds_varianti.getitemnumber(ll_k, "num_sequenza")
	ls_cod_prodotto_figlio_cu		= lds_varianti.getitemstring(ll_k, "cod_prodotto_figlio")
	ls_cod_versione_cu				= lds_varianti.getitemstring(ll_k, "cod_versione")
	ls_cod_versione_figlio_cu		=lds_varianti.getitemstring(ll_k, "cod_versione_figlio")
	ls_cod_deposito_prod_cu		= lds_varianti.getitemstring(ll_k, "cod_deposito_produzione")
	
	
	for ll_index=1 to ids_backup_vardetordven.rowcount()
		//leggo
		ls_cod_prodotto_padre_ds		= ids_backup_vardetordven.getitemstring(ll_index, "cod_prodotto_padre")
		ll_num_sequenza_ds				= ids_backup_vardetordven.getitemnumber(ll_index, "num_sequenza")
		ls_cod_prodotto_figlio_ds		= ids_backup_vardetordven.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione_ds				= ids_backup_vardetordven.getitemstring(ll_index, "cod_versione")
		ls_cod_versione_figlio_ds		= ids_backup_vardetordven.getitemstring(ll_index, "cod_versione_figlio")
		ls_cod_deposito_prod_ds		= ids_backup_vardetordven.getitemstring(ll_index, "cod_deposito_produzione")
		
		if 	ls_cod_prodotto_padre_cu			= ls_cod_prodotto_padre_ds 	and 	&
			ll_num_sequenza_cu					= ll_num_sequenza_ds 			and 	&
			ls_cod_prodotto_figlio_cu			= ls_cod_prodotto_figlio_ds 	and	&
			ls_cod_versione_cu					= ls_cod_versione_ds 			and	&
			ls_cod_versione_figlio_cu			= ls_cod_versione_figlio_ds			then
		
			//trovata nel datastore di istanza
			
			//se il deposito è cambiato lo ripristino e ripristino pure le varianti --------------------------------------------------------------
			if ls_cod_deposito_prod_cu <> ls_cod_deposito_prod_ds then
				
				ls_cod_dep_prod = ls_cod_deposito_prod_ds
				
				update varianti_det_ord_ven
				set cod_deposito_produzione=:ls_cod_deposito_prod_ds
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:str_conf_prodotto.anno_documento and
							num_registrazione=:str_conf_prodotto.num_documento and
							prog_riga_ord_ven=:str_conf_prodotto.prog_riga_documento and
							cod_prodotto_padre=:ls_cod_prodotto_padre_cu and
							num_sequenza=:ll_num_sequenza_cu and
							cod_prodotto_figlio=:ls_cod_prodotto_figlio_cu and
							cod_versione=:ls_cod_versione_cu and
							cod_versione_figlio=:ls_cod_versione_figlio_cu;
				
			else
				ls_cod_dep_prod = ls_cod_deposito_prod_cu
			end if
			
			//qui ripristino comunque le varianti produzione legate al deposito della varianta trovata prima -------------------------------
			delete from varianti_det_ord_ven_prod
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:str_conf_prodotto.anno_documento and
						num_registrazione=:str_conf_prodotto.num_documento and
						prog_riga_ord_ven=:str_conf_prodotto.prog_riga_documento and
						cod_prodotto_padre=:ls_cod_prodotto_padre_cu and
						num_sequenza=:ll_num_sequenza_cu and
						cod_prodotto_figlio=:ls_cod_prodotto_figlio_cu and
						cod_versione=:ls_cod_versione_cu and
						cod_versione_figlio=:ls_cod_versione_figlio_cu;
			
	
			for ll_index2=1 to ids_backup_vardetordven_prod.rowcount()
				
				//1.	cod_prodotto_padre
				//2.	num_sequenza
				//3.	cod_prodotto_figlio
				//4.	cod_versione
				//5.	cod_versione_figlio
				//6.	cod_reparto
				//7.	data_pronto
				//8.	cod_reparto_arrivo
				//9.	data_arrivo
				//10.	cod_deposito_produzione
				
				//leggo
				ls_cod_prodotto_padre_ds2			= ids_backup_vardetordven_prod.getitemstring(ll_index2, 1)
				ll_num_sequenza_ds2				= ids_backup_vardetordven_prod.getitemnumber(ll_index2, 2)
				ls_cod_prodotto_figlio_ds2			= ids_backup_vardetordven_prod.getitemstring(ll_index2, 3)
				ls_cod_versione_ds2					= ids_backup_vardetordven_prod.getitemstring(ll_index2, 4)
				ls_cod_versione_figlio_ds2			= ids_backup_vardetordven_prod.getitemstring(ll_index2, 5)
				ls_cod_deposito_prod_ds2 			= ids_backup_vardetordven_prod.getitemstring(ll_index2, 10)
				
				
				if 	ls_cod_prodotto_padre_cu			= ls_cod_prodotto_padre_ds2			 and 		&
					ll_num_sequenza_cu					= ll_num_sequenza_ds2					 and 		&
					ls_cod_prodotto_figlio_cu			= ls_cod_prodotto_figlio_ds2			 and		&
					ls_cod_versione_cu					= ls_cod_versione_ds2					 and		&
					ls_cod_versione_figlio_cu			= ls_cod_versione_figlio_ds2			 and		&
					ls_cod_dep_prod			 			= ls_cod_deposito_prod_ds2				then
				
				
					ls_cod_reparto = ids_backup_vardetordven_prod.getitemstring(ll_index2, 6)
				
					//re-inserisco la PK
					insert into varianti_det_ord_ven_prod
					(	cod_azienda,
						anno_registrazione,
						num_registrazione,
						prog_riga_ord_ven,
						cod_prodotto_padre,
						num_sequenza,
						cod_prodotto_figlio,
						cod_versione,
						cod_versione_figlio,
						cod_reparto	)
					values	(	:s_cs_xx.cod_azienda,
									:str_conf_prodotto.anno_documento,
									:str_conf_prodotto.num_documento,
									:str_conf_prodotto.prog_riga_documento,
									:ls_cod_prodotto_padre_cu,
									:ll_num_sequenza_cu,
									:ls_cod_prodotto_figlio_cu,
									:ls_cod_versione_cu,
									:ls_cod_versione_figlio_cu,
									:ls_cod_reparto);
					
				end if
				
			next	//ciclo datastore istanza varianti produzione prima del configuratore
			
		end if
		
	next	//ciclo datastore istanza varianti prima del configuratore
	
next	//ciclo datastore varianti messe dal configuratore

destroy lds_varianti;

//##########################################################################################


return 0

end function

public subroutine wf_ripristina_proforma (datastore ads_fattura_proforma);long			ll_index, ll_num_ordine, ll_riga_ordine, ll_num_fatt, ll_riga_fatt
integer		li_anno_ordine, li_anno_fatt


//in lds_proforma_collegate ho i dati cosi configurati in colonna
//1.		anno ordine vendita
//2.		numero ordine vendita
//3.		riga ordine vendita
//4.		anno fattura proforma
//5.		numero fattura proforma
//6.		riga fattura proforma

for ll_index=1 to ads_fattura_proforma.rowcount()
	li_anno_ordine = ads_fattura_proforma.getitemnumber(ll_index, 1)
	ll_num_ordine = ads_fattura_proforma.getitemnumber(ll_index, 2)
	ll_riga_ordine = ads_fattura_proforma.getitemnumber(ll_index, 3)
	li_anno_fatt = ads_fattura_proforma.getitemnumber(ll_index, 4)
	ll_num_fatt = ads_fattura_proforma.getitemnumber(ll_index, 5)
	ll_riga_fatt = ads_fattura_proforma.getitemnumber(ll_index, 6)
	
	//-------------------------------------------------------------
	//ricollego riga ordine a riga fattura proforma
	update det_fat_ven
	set		anno_reg_ord_ven=:li_anno_ordine,
			num_reg_ord_ven=:ll_num_ordine,
			prog_riga_ord_ven=:ll_riga_ordine
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_fatt and
				num_registrazione=:ll_num_fatt and
				prog_riga_fat_ven=:ll_riga_fatt;
	//-------------------------------------------------------------
	
next

return
end subroutine

public function integer wf_imposta_dddw_loghi (ref string as_errore);string						ls_condizione, ls_files_trovati[],ls_sql
long						ll_index, ll_tot, ll_new
datawindowchild		ldw_child


//leggo la lista dei fle presenti nel perocrso

ls_condizione = "*.*"
//ls_condizione = auo_params.uof_get_string(3, "*.*")


ll_tot = guo_functions.uof_estrai_files(is_path_cliente, ls_condizione, ls_files_trovati[], as_errore)

if ll_tot < 0 then
	as_errore = "Errore durante l'estrazione dei files logo cliente: " + as_errore
	return -1
elseif ll_tot = 0 then
	//nessun file presente
end if

 
dw_inizio.GetChild("logo_etichetta", ldw_child)
ldw_child.SetTransObject(SQLCA)
ldw_child.Retrieve()
 
ll_new = ldw_child.insertrow(0)
ldw_child.setitem(ll_new, "email_mittente", is_logostandard)


for ll_index=1 to ll_tot
	ll_new = ldw_child.insertrow(0)
	ldw_child.setitem(ll_new, "email_mittente", ls_files_trovati[ll_index])
next

dw_inizio.setitem(dw_inizio.getrow(), "logo_etichetta", is_logo_etichette)

wf_preview_logo(is_logo_etichette)

return 0
end function

public subroutine wf_preview_logo (string as_file);string			ls_file



if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
	setpointer(Hourglass!)

	if as_file=is_logostandard or g_str.isempty(as_file) then
		//path del logo standard
		ls_file = is_path_logo_standard
		
	else
		ls_file = is_path_cliente + as_file
	end if
	
	
	if fileExists(ls_file) then
		dw_inizio.object.p_logo.filename = ls_file
		dw_inizio.object.file_inesistente_t.visible = false
	else
		dw_inizio.object.p_logo.filename = ""
		dw_inizio.object.file_inesistente_t.visible = true
	end if
	
	setpointer(Arrow!)

end if

return
end subroutine

public function integer wf_controlla_formule_dimensioni (ref string as_errore);string								ls_valore, ls_cod_formula[4],ls_cod_variabile_dim_x ,ls_cod_variabile_dim_y ,ls_cod_variabile_dim_z ,ls_cod_variabile_dim_t , &
									ls_des_variabile_dim_x ,ls_des_variabile_dim_y ,ls_des_variabile_dim_z ,ls_des_variabile_dim_t, ls_message
integer							li_ret, ll_i
decimal 							ld_valore
uo_formule_calcolo			luo_formule


if str_conf_prodotto.anno_documento>0 and str_conf_prodotto.num_documento>0 and str_conf_prodotto.prog_riga_documento>0 then
else
	as_errore = "Errore calcolo formula controllo dimensioni. E' obbligatorio fornire il riferimento riga documento (2)"
	return -1
end if

if str_conf_prodotto.tipo_gestione <> "ORD_VEN" and str_conf_prodotto.tipo_gestione <> "OFF_VEN" then
	as_errore = "Errore calcolo formula controllo dimensioni. E' al di fuori del contesto ordine o offerta vendita!"
	return -1
end if

select 	C.cod_formula_dim_x,
			C.cod_formula_dim_y,
			C.cod_formula_dim_z,
			C.cod_formula_dim_t,
			C.cod_variabile_dim_x,
			C.cod_variabile_dim_y,
			C.cod_variabile_dim_z,
			C.cod_variabile_dim_t,
			F1.note, 
			F2.note, 
			F3.note, 
			F4.note
into		:ls_cod_formula[1],
			:ls_cod_formula[2],
			:ls_cod_formula[3],
			:ls_cod_formula[4],
			:ls_cod_variabile_dim_x ,
			:ls_cod_variabile_dim_y ,
			:ls_cod_variabile_dim_z ,
			:ls_cod_variabile_dim_t ,
			:ls_des_variabile_dim_x ,
			:ls_des_variabile_dim_y ,
			:ls_des_variabile_dim_z ,
			:ls_des_variabile_dim_t
from	tab_flags_configuratore C
		left join tab_variabili_formule F1 on  F1.cod_azienda=C.cod_azienda and F1.cod_variabile= C.cod_variabile_dim_x
		left join tab_variabili_formule F2 on  F2.cod_azienda=C.cod_azienda and F2.cod_variabile= C.cod_variabile_dim_y
		left join tab_variabili_formule F3 on  F3.cod_azienda=C.cod_azienda and F3.cod_variabile= C.cod_variabile_dim_z
		left join tab_variabili_formule F4 on  F4.cod_azienda=C.cod_azienda and F4.cod_variabile= C.cod_variabile_dim_t
where 	C.cod_azienda = :s_cs_xx.cod_azienda and
			C.cod_modello = :str_conf_prodotto.cod_modello;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore ricerca formule controllo dimensioni modello " + sqlca.sqlerrtext
	return -1
end if

luo_formule = create uo_formule_calcolo


luo_formule.ii_anno_reg_ord_ven = str_conf_prodotto.anno_documento
luo_formule.il_num_reg_ord_ven = str_conf_prodotto.num_documento
luo_formule.il_prog_riga_ord_ven = str_conf_prodotto.prog_riga_documento

setnull(luo_formule.is_cod_prodotto_padre)
setnull(luo_formule.is_cod_versione_padre)
setnull(luo_formule.il_num_sequenza)
setnull(luo_formule.is_cod_prodotto_figlio)
setnull(luo_formule.is_cod_versione_figlio)

luo_formule.is_contesto = str_conf_prodotto.tipo_gestione

for ll_i = 1 to 4
	if isnull(ls_cod_formula[ll_i]) then continue 
	li_ret = luo_formule.uof_calcola_formula_db(ls_cod_formula[ll_i], ld_valore, ls_valore, as_errore)
//	as_tipo_ritorno = luo_formule.is_tipo_ritorno
	if li_ret < 0 then
		as_errore = g_str.format("Errore calcolo formula controllo dimensioni $1. $2 ", ls_cod_formula[ll_i], as_errore)
		return -1
	end if
	
	if ld_valore = 0 then
		ls_message = "Attenzione, valore $1 della dimensioni $2-$3 è fuori dei limiti consentiti "
		choose case ll_i
			case 1
				g_str.format(ls_message, istr_riepilogo.dimensione_1 , ls_cod_variabile_dim_x, ls_des_variabile_dim_x)
			case 2
				g_str.format(ls_message, istr_riepilogo.dimensione_2, ls_cod_variabile_dim_y, ls_des_variabile_dim_y)
			case 3
				g_str.format(ls_message, istr_riepilogo.dimensione_3, ls_cod_variabile_dim_z, ls_des_variabile_dim_z)
			case 4
				g_str.format(ls_message, istr_riepilogo.dimensione_4, ls_cod_variabile_dim_t, ls_des_variabile_dim_t)
		end choose
		as_errore = ls_message
		return -1
	end if	
next

destroy luo_formule


return 0




end function

public function boolean wf_abilita_dimensioni (string fs_cod_prodotto_modello, integer ai_num_dimensione);string ls_nome_campo_database, ls_des_variabile, ls_columnname, ls_flag_blocco_valore

choose case ai_num_dimensione
	case 1
		ls_nome_campo_database = "dim_x"
	case 2
		ls_nome_campo_database = "dim_y"
	case 3
		ls_nome_campo_database = "dim_z"
	case 4
		ls_nome_campo_database = "dim_t"
	case else
		ls_nome_campo_database = "dim_" + string(ai_num_dimensione,"00")
end choose

ls_columnname = "rd_dimensione_" + string(ai_num_dimensione)

select des_variabile, flag_blocco_valore
 into :ls_des_variabile, :ls_flag_blocco_valore
 from tab_des_variabili
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :fs_cod_prodotto_modello and
		flag_ipertech_apice='S' and
		cod_variabile in ( select cod_variabile
									from tab_variabili_formule
									where cod_azienda = :s_cs_xx.cod_azienda and 
											nome_campo_database = :ls_nome_campo_database );

if sqlca.sqlcode = 0 then
	dw_inizio.modify(ls_columnname+".visible = 1")
	if ls_flag_blocco_valore = "N" then
		dw_inizio.modify(ls_columnname+"_t.color = 8388608")
		dw_inizio.modify(ls_columnname+"_t.text = '"+ls_des_variabile+"'")
		dw_inizio.modify(ls_columnname+".background.color = 16777215")
		dw_inizio.modify(ls_columnname+".protect = 0")
		return false
	else
		dw_inizio.modify(ls_columnname+"_t.color = 8421504")
		dw_inizio.modify(ls_columnname+"_t.text = '"+ls_des_variabile+"'")
		dw_inizio.modify(ls_columnname+".background.color = 79741120")
		dw_inizio.modify(ls_columnname+".protect = 1")
		return true
	end if
else
	dw_inizio.modify(ls_columnname+".visible = 0")
	dw_inizio.modify(ls_columnname+"_t.color = 8421504")
	dw_inizio.modify(ls_columnname+"_t.text =' ' ")
	dw_inizio.modify(ls_columnname+".background.color = 79741120")
	
end if

return false

end function

on w_configuratore.create
int iCurrent
call super::create
this.dw_trasferimenti=create dw_trasferimenti
this.dw_help=create dw_help
this.dw_steps=create dw_steps
this.p_1=create p_1
this.cbx_apri_dopo=create cbx_apri_dopo
this.cb_annulla=create cb_annulla
this.cb_indietro=create cb_indietro
this.cb_avanti=create cb_avanti
this.cb_fine=create cb_fine
this.st_cod_prodotto_finito=create st_cod_prodotto_finito
this.cb_note_esterne=create cb_note_esterne
this.cb_stampa=create cb_stampa
this.st_prezzo=create st_prezzo
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_sconti=create st_sconti
this.dw_passo_6=create dw_passo_6
this.dw_passo_5=create dw_passo_5
this.dw_passo_4=create dw_passo_4
this.dw_passo_3=create dw_passo_3
this.dw_passo_2=create dw_passo_2
this.dw_fine=create dw_fine
this.dw_passo_8=create dw_passo_8
this.dw_passo_7=create dw_passo_7
this.dw_passo_1=create dw_passo_1
this.dw_inizio=create dw_inizio
this.dw_nota_prodotto_finito=create dw_nota_prodotto_finito
this.dw_nota_prodotto_finito_lista=create dw_nota_prodotto_finito_lista
this.ole_1=create ole_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_trasferimenti
this.Control[iCurrent+2]=this.dw_help
this.Control[iCurrent+3]=this.dw_steps
this.Control[iCurrent+4]=this.p_1
this.Control[iCurrent+5]=this.cbx_apri_dopo
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.cb_indietro
this.Control[iCurrent+8]=this.cb_avanti
this.Control[iCurrent+9]=this.cb_fine
this.Control[iCurrent+10]=this.st_cod_prodotto_finito
this.Control[iCurrent+11]=this.cb_note_esterne
this.Control[iCurrent+12]=this.cb_stampa
this.Control[iCurrent+13]=this.st_prezzo
this.Control[iCurrent+14]=this.st_2
this.Control[iCurrent+15]=this.st_3
this.Control[iCurrent+16]=this.st_4
this.Control[iCurrent+17]=this.st_sconti
this.Control[iCurrent+18]=this.dw_passo_6
this.Control[iCurrent+19]=this.dw_passo_5
this.Control[iCurrent+20]=this.dw_passo_4
this.Control[iCurrent+21]=this.dw_passo_3
this.Control[iCurrent+22]=this.dw_passo_2
this.Control[iCurrent+23]=this.dw_fine
this.Control[iCurrent+24]=this.dw_passo_8
this.Control[iCurrent+25]=this.dw_passo_7
this.Control[iCurrent+26]=this.dw_passo_1
this.Control[iCurrent+27]=this.dw_inizio
this.Control[iCurrent+28]=this.dw_nota_prodotto_finito
this.Control[iCurrent+29]=this.dw_nota_prodotto_finito_lista
this.Control[iCurrent+30]=this.ole_1
end on

on w_configuratore.destroy
call super::destroy
destroy(this.dw_trasferimenti)
destroy(this.dw_help)
destroy(this.dw_steps)
destroy(this.p_1)
destroy(this.cbx_apri_dopo)
destroy(this.cb_annulla)
destroy(this.cb_indietro)
destroy(this.cb_avanti)
destroy(this.cb_fine)
destroy(this.st_cod_prodotto_finito)
destroy(this.cb_note_esterne)
destroy(this.cb_stampa)
destroy(this.st_prezzo)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_sconti)
destroy(this.dw_passo_6)
destroy(this.dw_passo_5)
destroy(this.dw_passo_4)
destroy(this.dw_passo_3)
destroy(this.dw_passo_2)
destroy(this.dw_fine)
destroy(this.dw_passo_8)
destroy(this.dw_passo_7)
destroy(this.dw_passo_1)
destroy(this.dw_inizio)
destroy(this.dw_nota_prodotto_finito)
destroy(this.dw_nota_prodotto_finito_lista)
destroy(this.ole_1)
end on

event pc_setwindow;call super::pc_setwindow;string					ls_prog_riga, ls_tabella_comp, ls_sql, ls_tabella_det, ls_flag, ls_errore, ls_path, ls_cod_cliente, ls_LET

s_cs_xx.parametri.parametro_d_10 = 0


dw_nota_prodotto_finito.visible=false
dw_nota_prodotto_finito_lista.visible=false

setredraw(false)
set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

ii_num_passo_old = 0
ii_num_passo = 0
ii_quantita_passi = 0
setnull(str_conf_prodotto.cod_modello)
is_nome_passo_optional = 'd_ext_optional'

if isnull(str_conf_prodotto.tipo_gestione) or str_conf_prodotto.tipo_gestione = "" then
	g_mb.messagebox("Configuratro di prodotto","Impossibile eseguire il configuratore senza indicare il tipo gestione")
	return
end if

if str_conf_prodotto.tipo_gestione = "TES_ORD_VEN" then
	str_conf_prodotto.tipo_gestione = "ORD_VEN"
	dw_trasferimenti.visible = true
	return
end if

choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		ls_tabella_det = "det_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		ls_prog_riga = "prog_riga_ord_ven"
		
		dw_trasferimenti.visible = true
		
		selectblob note_esterne
		into       :istr_riepilogo.note_esterne
		from       comp_det_ord_ven
		where 	  cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :str_conf_prodotto.anno_documento and
					  num_registrazione = :str_conf_prodotto.num_documento and
					  prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
		
		if sqlca.sqlcode <> 0 then
			setnull(istr_riepilogo.note_esterne)
		end if
		
		select		logo_etichetta, cod_cliente
		into 		:is_logo_etichette, :ls_cod_cliente
		from	tes_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :str_conf_prodotto.anno_documento and
					num_registrazione = :str_conf_prodotto.num_documento;
		
		if g_str.isempty(is_logo_etichette) then is_logo_etichette = is_logostandard
		is_path_cliente = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+ls_cod_cliente+"\"
		
		//path del logo standard
		guo_functions.uof_get_parametro_azienda("LET", ls_LET)
		
		if g_str.isempty(ls_LET) then
			is_path_logo_standard = ""
		else
			is_path_logo_standard = s_cs_xx.volume + ls_LET
		end if
		
		
		select		quan_ordine, 
					nota_dettaglio, 
					nota_prodotto, 
					flag_trasporta_nota_ddt
		into	:istr_riepilogo.quan_vendita, 
				:istr_riepilogo.nota_dettaglio, 
				:istr_riepilogo.nota_prodotto, 
				:istr_riepilogo.flag_trasporta_nota_ddt
		from   det_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :str_conf_prodotto.anno_documento and
					num_registrazione = :str_conf_prodotto.num_documento and
					prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
					
		if sqlca.sqlcode <> 0 then
			istr_riepilogo.quan_vendita = 1
		end if
		
		//faccio un backup della situazione varianti_det_ord_ven_prod
		wf_backup_var_det_ord_ven_prod(str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento,ls_errore)
		//---------------------------------------------------------------------
		
	case "OFF_VEN"
		ls_tabella_det = "det_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		ls_prog_riga = "prog_riga_off_ven"
		
		selectblob note_esterne
		into       :istr_riepilogo.note_esterne
		from       comp_det_off_ven
		where 	  cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :str_conf_prodotto.anno_documento and
					  num_registrazione = :str_conf_prodotto.num_documento and
					  prog_riga_off_ven = :str_conf_prodotto.prog_riga_documento;
		
		if sqlca.sqlcode <> 0 then
			setnull(istr_riepilogo.note_esterne)
		end if
		
		try
			dw_inizio.object.logo_etichetta.visible = false
			dw_inizio.object.logo_etichetta_t.visible = false
			dw_inizio.object.p_logo.visible = false
		catch (runtimeerror err2)
		end try
		
		
		select quan_offerta, 
		       nota_dettaglio, 
				 flag_doc_suc_det
		into   :istr_riepilogo.quan_vendita, 
		       :istr_riepilogo.nota_dettaglio, 
				 :istr_riepilogo.flag_trasporta_nota_ddt
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione = :str_conf_prodotto.num_documento and
				 prog_riga_off_ven = :str_conf_prodotto.prog_riga_documento;
		if sqlca.sqlcode <> 0 then
			istr_riepilogo.quan_vendita = 1
		end if
		
		istr_riepilogo.nota_prodotto = ""
		
end choose


if wf_leggi_flag_configuratore (ls_tabella_comp, ls_prog_riga, ls_errore) = -1 then
	g_mb.messagebox("Configuratore",ls_errore)
	return
end if

dw_inizio.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, &
								 c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
								 
dw_fine.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, &
								  c_noresizedw + c_nohighlightselected + c_cursorrowpointer)


str_conf_prodotto.ultima_riga_inserita = 0

select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CG1';
if sqlca.sqlcode = 0 and ls_flag = "S" then
	str_conf_prodotto.apri_configuratore = true
	cbx_apri_dopo.visible = true
else
	str_conf_prodotto.apri_configuratore = false
	cbx_apri_dopo.visible = false
end if

// ----------------------------  aggiunto per cambio modello da folder complementi ---------------------------------------------
//                             ( spec_varie_5; funzione14:gestione armature )
if str_conf_prodotto.flag_cambio_modello then
	g_mb.messagebox("Configuratore", str_conf_prodotto.messaggio_modifiche_modello)
end if

postevent("ue_set_focus_inizio")


//------------------------------------------------------------------------------------
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\forklift.png"
dw_trasferimenti.object.p_forklift.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\truck.png"
dw_trasferimenti.object.p_truck.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "menu\icona_utente.png"
dw_trasferimenti.object.p_cliente.FileName = ls_path
//------------------------------------------------------------------------------------

//imposto la formula per il colore del testo sul riassunto (default è NERO)
try
	dw_fine.modify("rs_dettaglio.color=~"0~tcolore~"")
catch (throwable err)
end try


end event

event key;call super::key;choose case key
	case KeyEnter!
		wf_spostamento("A")
	case KeyEscape!
		wf_spostamento("I")
end choose
end event

event pc_setddlb;call super::pc_setddlb;
f_PO_LoadDDDW_DW(dw_inizio,"rs_cod_modello",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")
												  
end event

event close;call super::close;setnull(s_cs_xx.parametri.parametro_s_4)
destroy iuo_condizioni_cliente

wf_azzera_variabili()
end event

event open;call super::open;iuo_condizioni_cliente = create uo_condizioni_cliente
setredraw(true)
iw_parent = message.powerobjectparm

end event

type dw_trasferimenti from datawindow within w_configuratore
boolean visible = false
integer x = 5147
integer y = 24
integer width = 686
integer height = 2200
integer taborder = 160
string title = "none"
string dataobject = "d_ext_trasferimenti"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;boolean								lb_ricalcola, lb_esiste_riga
datetime								ldt_data_partenza
long									ll_index, ll_count, ll_riga_ordine
uo_calendario_prod_new			luo_cal_prod
string									ls_errore


if str_conf_prodotto.tipo_gestione<>"ORD_VEN" then return

lb_ricalcola = false
lb_esiste_riga = false
ll_riga_ordine = -1

//if s_cs_xx.admin then
//	
//	if rowcount() > 0 then
//		lb_ricalcola = g_mb.confirm("Premere SI per effettuare il ricalcolo del log dei trasferimenti, NO per continuare e visualizzare il calendario produzione!")
//	else
//		lb_ricalcola = true
//	end if
//	
//	if lb_ricalcola then
//		
//		if str_conf_prodotto.anno_documento>0 and str_conf_prodotto.num_documento>0 and str_conf_prodotto.prog_riga_documento>0 then
//			ll_riga_ordine = str_conf_prodotto.prog_riga_documento
//			lb_esiste_riga = true
//		end if
//		
//		dw_trasferimenti.object.testo_t.text = "DEBUG TRASF."
//		
//		luo_cal_prod = create uo_calendario_prod_new
//		luo_cal_prod.uof_dw_cal_produzione(		lb_esiste_riga, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, ll_riga_ordine, &
//															"", "", idt_data_partenza, is_reparti[], idt_date_reparti[], dw_trasferimenti, ls_errore)
//		
//		destroy luo_cal_prod
//	
//		return
//	end if
//end if


if row>0 then
else
	return
end if


//se arrivi fin qui visualizza calendario
	
//leggo attuale data partenza dell'ordine, oppure se è stata GIà AMBIATA PASSO QUELLA ...
if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
	ldt_data_partenza = idt_data_partenza
	
else
	select data_consegna
	into :ldt_data_partenza
	from tes_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:str_conf_prodotto.anno_documento and
			num_registrazione=:str_conf_prodotto.num_documento;

	if sqlca.sqlcode<0 then
		g_mb.error("Errore in lettura data partenza dell'ordine: "+sqlca.sqlerrtext)
		return
	end if

end if

ll_count = 0

s_cs_xx.parametri.parametro_s_4_a[] = is_reparti[]
s_cs_xx.parametri.parametro_data_1_a[] = idt_date_reparti[]
s_cs_xx.parametri.parametro_data_4 = ldt_data_partenza
setnull(ldt_data_partenza)

window_open(w_cal_produzione_reparti, 0)


if s_cs_xx.parametri.parametro_b_2 then
	//data partenza cambiata
	idt_data_partenza = s_cs_xx.parametri.parametro_data_4
	g_mb.success("La Data Partenza sarà cambiata in "+string(idt_data_partenza, "dd/mm/yyyy") + " alla fine della configurazione.")
	dw_inizio.event post ue_cal_trasferimenti()
	
elseif not isnull(idt_data_partenza) and year(date(idt_data_partenza)) > 1950 then
	//vuol dire che era stata cambiata in un precedente tentativo
	//quindi lascia inalterata idt_data_partenza

else
	s_cs_xx.parametri.parametro_b_2 = false
	setnull(s_cs_xx.parametri.parametro_data_4)
end if




end event

type dw_help from datawindow within w_configuratore
integer x = 23
integer y = 20
integer width = 1065
integer height = 1696
integer taborder = 170
string title = "none"
string dataobject = "d_ext_help"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string ls_str, ls_nome_dw
long ll_cont

choose case dwo.name
	case "b_modifica"
		ls_str = this.getitemstring(1,"help")
		openwithparm(w_configuratore_note_help, ls_str)
		ls_str = message.stringparm
		this.setitem(1,"help",ls_str)
		
	case "b_save"
		setnull(ls_str)
		setnull(ls_nome_dw)
		ls_str = this.getitemstring(1,"help")
		
		choose case ii_num_passo
			case 0
				//
			case 1
				ls_nome_dw = dw_passo_1.dataobject
			case 2
				ls_nome_dw = dw_passo_2.dataobject
			case 3
				ls_nome_dw = dw_passo_3.dataobject
			case 4
				ls_nome_dw = dw_passo_4.dataobject
			case 5
				ls_nome_dw = dw_passo_5.dataobject
			case 6
				ls_nome_dw = dw_passo_6.dataobject
			case 7
				ls_nome_dw = dw_passo_7.dataobject
			case 8
				ls_nome_dw = dw_passo_8.dataobject
			case 99
				
		end choose	
		
		if not isnull(ls_str) and not isnull(ls_nome_dw) then
				
			select 	count(*)
			into 		:ll_cont
			from 		tab_passi_configuratore
			where 	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_modello = :str_conf_prodotto.cod_modello and
						nome_data_window = :ls_nome_dw;
	
			if ll_cont = 1 then
				update tab_passi_configuratore
				set mini_help = :ls_str
				where 	cod_azienda = :s_cs_xx.cod_azienda and 
							cod_modello = :str_conf_prodotto.cod_modello and
							nome_data_window = :ls_nome_dw;
			
				commit;
			end if
				
		end if
	
	
end choose
end event

type dw_steps from datawindow within w_configuratore
integer x = 32
integer y = 2120
integer width = 4398
integer height = 88
integer taborder = 170
string title = "none"
string dataobject = "d_ext_step_list"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if isvalid(dwo) then
	string ls_name, ls_step
	integer ll_step
	
	ls_name = dwo.name
	
	if upper(ls_name) = "STEP_START" or upper(ls_name) = "STEP_END" then return
	
	if UPPER( left( ls_name,4) ) = "STEP" then
		ls_step = right(ls_name,1)
		ll_step = integer(ls_step)
		
		ii_passo_richiesto = ll_step
		wf_spostamento("P")		
	end if
end if
		
return
end event

type p_1 from picture within w_configuratore
boolean visible = false
integer x = 901
integer y = 1488
integer width = 206
integer height = 124
boolean originalsize = true
string picturename = "C:\CS_90\framework\RISORSE\DOCUMENTO.bmp"
boolean focusrectangle = false
end type

event doubleclicked;ole_1.activate(offsite!)
visible = false

end event

type cbx_apri_dopo from checkbox within w_configuratore
integer x = 3273
integer y = 2004
integer width = 334
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Apri Dopo"
boolean checked = true
boolean lefttext = true
end type

type cb_annulla from commandbutton within w_configuratore
integer x = 2862
integer y = 1904
integer width = 361
integer height = 80
integer taborder = 240
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;// -------------------------  aggiunto per centro gibus specifica SEMPLIFICAZIONI -------------------------------------------- //
if not(cbx_apri_dopo.checked) then
	// così il configuratore non si riapre più in automatico in base al parametro aziendale CG1; 
	// vedi specifica \gibus\progetti\semplificazioni\
	str_conf_prodotto.apri_configuratore = false
end if
// --------------------------         FINE AGGIUNTA -------------------------------------------------------------------------- //

// --------------------- aggiunto da Enrico per cambio da response a child di questa window ---------------------------- //

if isvalid(iw_parent) then
	iw_parent.postevent("ue_post_configuratore")
end if

parent.postevent("ue_chiudi_configuratore")

if isvalid(ids_backup_vardetordven_prod) then destroy ids_backup_vardetordven_prod
if isvalid(ids_backup_vardetordven) then destroy ids_backup_vardetordven
end event

type cb_indietro from commandbutton within w_configuratore
integer x = 2039
integer y = 1904
integer width = 361
integer height = 80
integer taborder = 260
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<< &Indietro"
end type

event clicked;s_cs_xx.parametri.parametro_d_10 = 0

choose case ii_num_passo
	case 0
		dw_inizio.accepttext()
	case 1
		dw_passo_1.accepttext()
	case 2
		dw_passo_2.accepttext()
	case 3
		dw_passo_3.accepttext()
	case 4
		dw_passo_4.accepttext()
	case 5
		dw_passo_5.accepttext()
	case 6
		dw_passo_6.accepttext()
	case 7
		dw_passo_7.accepttext()
	case 8
		dw_passo_8.accepttext()
end choose
wf_spostamento("I")
end event

type cb_avanti from commandbutton within w_configuratore
integer x = 2405
integer y = 1904
integer width = 361
integer height = 80
integer taborder = 220
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Avanti >>"
end type

event clicked;string ls_errore
long ll_ret

choose case ii_num_passo
	case 0
		dw_inizio.accepttext()
		uo_itemchanged luo_itemchanged
		luo_itemchanged = create uo_itemchanged
		ll_ret = luo_itemchanged.uof_validazione_dimensioni(dw_inizio, ls_errore)
		if ll_ret < 0 then
			g_mb.warning(ls_errore)
		end if
	case 1
		dw_passo_1.accepttext()
		if dw_passo_1.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 2
		dw_passo_2.accepttext()
		if dw_passo_2.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 3
		dw_passo_3.accepttext()
		if dw_passo_3.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 4
		dw_passo_4.accepttext()
		if dw_passo_4.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 5
		dw_passo_5.accepttext()
		if dw_passo_5.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 6
		dw_passo_6.accepttext()
		if dw_passo_6.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 7
		dw_passo_7.accepttext()
		if dw_passo_7.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
	case 8
		dw_passo_8.accepttext()
		if dw_passo_8.dataobject = is_nome_passo_optional then dw_nota_prodotto_finito.accepttext()
end choose
wf_spostamento("A")
end event

type cb_fine from commandbutton within w_configuratore
integer x = 3250
integer y = 1904
integer width = 361
integer height = 80
integer taborder = 250
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Con&figura"
end type

event clicked;/*		
EnMe 22/04/2012
		Gestione di 3 verniciature

EnMe 08/05/2012
		Tipi dettaglio prodotto, addizionali, optionals gestite come eccezioni in tabella specifica
*/		

boolean 									lb_corretto, lb_sovrascrivi_sconti=false, lb_blocco_prezzo=false,lb_test

string 									ls_cod_versione, ls_sql_del, ls_tabella, ls_progressivo, ls_tabella_det_doc, ls_tabella_tes_doc, ls_null, &
											ls_cod_tipo_det_ven, ls_messaggio, ls_flag_stampata_bcl, ls_null_array[], ls_flag_prezzo_bloccato, &
											ls_cod_tipo_ord_ven, ls_valuta_lire, ls_cod_valuta, ls_sql, ls_prezzo_vendita, ls_valore_riga, ls_fat_conversione, &
											ls_sconto, ls_provvigione_1, ls_provvigione_2, ls_quantita_um, ls_prezzo_um, ls_logo_etichetta,ls_logo_etichetta_new,&
											ls_flag_calendario_prod,ls_cod_prodotto_telo
				 
long 										ll_tempo, ll_inizio, ll_fine, ll_anno_comm, ll_num_comm, ll_count_addizionali, ll_i, ll_y, ll_z

double 									ld_variazioni[], ld_gruppi_sconti[], ld_provv_1, ld_provv_2, ld_valore_riga, ld_dimensione_1, ld_dimensione_2, &
											ld_dimensione_3, ld_fat_conversione, ld_quan_ordinata, ld_provvigione_1, ld_provvigione_2, &
											ld_prezzo_um, ld_quantita_um
										 
dec{4} 									ld_prezzo_vendita_origine, ld_sconto_1_origine, ld_sconto_2_origine, ld_provvigione_1_origine, ld_provvigione_2_origine, &
		 									ldd_prezzo_vendita, ldd_fat_conversione, ld_prezzo_vendita
											 
uo_scrivi_tabelle 						luo_scrivi_tabelle
uo_gruppi_sconti 						luo_gruppi_sconti
uo_funzioni_1 							luo_funzioni_1
uo_condizioni_cliente 					luo_listini
uo_spese_trasporto					luo_spese_trasporto

integer 									li_ret
datastore								lds_proforma_collegate
boolean									lb_ripristina_proforma



// ------------------------------  CONTROLLO SE LA RIGA HA UNA COMMESSA COLLEGATA; SE SI ALLORA BLOCCO  --------------------------
ls_flag_prezzo_bloccato = "N"

choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		setnull(ll_anno_comm)
		setnull(ll_num_comm)
		
		select anno_commessa, num_commessa, flag_prezzo_bloccato
		into   :ll_anno_comm, :ll_num_comm, :ls_flag_prezzo_bloccato
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione  = :str_conf_prodotto.num_documento and
				 prog_riga_ord_ven  = :str_conf_prodotto.prog_riga_documento;
		if sqlca.sqlcode = -1 then
			g_mb.error("Configuratore di Prodotto", "Errore nella ricerca riga ordine. Dettaglio errore " + sqlca.sqlerrtext)
			rollback;
			return
		end if
		if not isnull(ll_anno_comm) or not isnull(ll_num_comm) then
			g_mb.messagebox("Configuratore","Impossibile rielaborare perchè la commessa è già stata generata e chiusa con successo in precedenza",StopSign!)
			return
		end if
		if str_conf_prodotto.stato = "M" then
			select flag_stampata_bcl
			into   :ls_flag_stampata_bcl
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :str_conf_prodotto.anno_documento and
					 num_registrazione  = :str_conf_prodotto.num_documento;
			if sqlca.sqlcode <> 0 then ls_flag_stampata_bcl = "N"
			if ls_flag_stampata_bcl = "S" then
				if g_mb.messagebox("Configuratore","Gli ordini per la produzione devono essere ristampati?", Question!, YesNo!, 2) = 1 then
					ls_flag_stampata_bcl = "N"
				end if
				update tes_ord_ven
				set    flag_stampata_bcl = :ls_flag_stampata_bcl
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :str_conf_prodotto.anno_documento and
						 num_registrazione  = :str_conf_prodotto.num_documento;
			end if
		end if
		
	case "OFF_VEN"
		setnull(ll_anno_comm)
		setnull(ll_num_comm)
		
		select  flag_prezzo_bloccato
		into    :ls_flag_prezzo_bloccato
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione  = :str_conf_prodotto.num_documento and
				 prog_riga_off_ven  = :str_conf_prodotto.prog_riga_documento;
		if sqlca.sqlcode = -1 then
			g_mb.error("Configuratore di Prodotto", "Errore nella ricerca riga offerta. Dettaglio errore " + sqlca.sqlerrtext)
			rollback;
			return
		end if
end choose

// ----------------------  Gestione del PREZZO BLOCCATO  -------------------------------------------------------
ld_prezzo_vendita_origine = 0
ld_sconto_1_origine = 0
ld_sconto_2_origine = 0
ld_provvigione_1_origine = 0
ld_provvigione_2_origine = 0

if ls_flag_prezzo_bloccato = "S" and str_conf_prodotto.stato = "M" then
	
	choose case str_conf_prodotto.tipo_gestione
		case "ORD_VEN"
			
			select 	prezzo_vendita, 
						sconto_1, 
						sconto_2,
						provvigione_1, 
						provvigione_2
			into    	:ld_prezzo_vendita_origine, 
						:ld_sconto_1_origine, 
						:ld_sconto_2_origine, 
						:ld_provvigione_1_origine, 
						:ld_provvigione_2_origine
			from 		det_ord_ven
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :str_conf_prodotto.anno_documento and
						num_registrazione = :str_conf_prodotto.num_documento and
						prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
			if sqlca.sqlcode = -1 then
				//errore
			end if
			
//			ls_messaggio = "ATTENZIONE: PREZZO BLOCCATO~r~nCONDIZIONE PRECEDENTE: Prezzo=" + string(ld_prezzo_vendita_origine) + " Sconti " + string(ld_sconto_1_origine) + "+" + string(ld_sconto_2_origine) + " Provv. " + string(ld_provvigione_1_origine) + " - " + string(ld_provvigione_2_origine)
	
//			g_mb.messagebox("PREZZO BLOCCATO", ls_messaggio)
	end choose
end if

// ------------------- ALUSISTEMI 14/09/2020 --------------------------------------------------------- //
// Gestione del prezzo e aumento del prezzo del telo sulla base del campionario/modello

select A.cod_prodotto_telo
into	:ls_cod_prodotto_telo
from 	tab_tipi_camp_listino_teli A
left outer join tab_tessuti T on 	A.cod_azienda = T.cod_azienda and
										A.cod_tipo_campionario = T.cod_tipo_campionario 
where A.cod_azienda = :s_cs_xx.cod_azienda and
		A.cod_modello = :str_conf_prodotto.cod_modello and
		T.cod_tessuto = :istr_riepilogo.cod_tessuto and
		T.cod_non_a_magazzino = :istr_riepilogo.cod_non_a_magazzino;
if sqlca.sqlcode < 0 then		
	g_mb.messagebox("Configuratore","Errore in ricerca del LISTINO TELO per il campionario associato al tessuto "+sqlca.sqlerrtext)
	ROLLBACK;
	return -1
end if

if not isnull(ls_cod_prodotto_telo) then
	istr_riepilogo.cod_modello_telo_finito = ls_cod_prodotto_telo
end if

// ----------------------------------------------------------------------------------------------------------------------------------------- //
setnull(ls_null)
ll_inizio = (minute(now()) * 60) + second(now())
st_2.text = " Creazione delle varianti in corso ......."
lb_corretto = true

luo_scrivi_tabelle = CREATE uo_scrivi_tabelle
if luo_scrivi_tabelle.uof_crea_comp_det() = -1 then 
	lb_corretto = false		// Creo la riga in Comp_det_[ord]_ven
	destroy luo_scrivi_tabelle
	return
end if

/* ----- EnMe 26/03/2020 [coronavirus] Aggiunto controllo dimensioni tramite formula ---- */ 
if wf_controlla_formule_dimensioni( ls_messaggio) < 0 then
	g_mb.warning( ls_messaggio)
	rollback;
	return
end if

ls_messaggio = ""

//#############################################################################
lb_ripristina_proforma = false

//prima di distruggere l'ogegtto controllo se devo ripristinare collegamenti a fatture proforma
//cse di ripassaggio dal configuratore (infatti lem righe riferite vengono in tal caso cancellate e reinserite)
//il ricollegamento viene fatto + in basso, dipo la funzione de re-inserimento ...
if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
	
	lb_ripristina_proforma = luo_scrivi_tabelle.uof_get_ripristino_proforma()
	if lb_ripristina_proforma then
		lds_proforma_collegate = luo_scrivi_tabelle.ids_data_proforma
	end if
	
end if
//#############################################################################

//ora posso tranquillamente distruggere l'oggetto
destroy luo_scrivi_tabelle




choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_tabella_det_doc = "det_ord_ven"
		ls_tabella_tes_doc = "tes_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		s_cs_xx.listino_db.tipo_gestione = "varianti_det_ord_ven"

		// cancello tabella varianti
		ls_sql_del ="delete from " + ls_tabella + "_prod" + &
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
						" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
						ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)
		
		execute immediate :ls_sql_del;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nella Cancellazione delle Varianti. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return
		end if


	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_tabella_det_doc = "det_off_ven"
		ls_tabella_tes_doc = "tes_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		s_cs_xx.listino_db.tipo_gestione = "varianti_det_off_ven"

end choose

ls_sql_del ="delete from " + ls_tabella + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
				" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
				ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)

execute immediate :ls_sql_del;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nella Cancellazione delle Varianti. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	rollback;
	return
end if


if wf_crea_varianti() = -1 then lb_corretto = false
//if wf_crea_varianti_esclusione(str_conf_prodotto.cod_modello, dw_inizio.getitemstring(1, "rs_cod_versione")) = -1 then lb_corretto = false // Creo le Varianti con Esclusione

istr_riepilogo.cod_prodotto_addizionale[] = ls_null_array[]

st_2.text = " Creazione delle addizionali in corso ......."
if wf_crea_addizionali_passo(dw_passo_1) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_1
st_2.text = " Creazione delle addizionali in corso ......1"
if wf_crea_addizionali_passo(dw_passo_2) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_2
st_2.text = " Creazione delle addizionali in corso ......2"
if wf_crea_addizionali_passo(dw_passo_3) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_3
st_2.text = " Creazione delle addizionali in corso ......3"
if wf_crea_addizionali_passo(dw_passo_4) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_4
st_2.text = " Creazione delle addizionali in corso ......4"
if wf_crea_addizionali_passo(dw_passo_5) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_5
st_2.text = " Creazione delle addizionali in corso ......5"
if wf_crea_addizionali_passo(dw_passo_6) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_6
st_2.text = " Creazione delle addizionali in corso ......6"
if wf_crea_addizionali_passo(dw_passo_7) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_7
st_2.text = " Creazione delle addizionali in corso ......7"
if wf_crea_addizionali_passo(dw_passo_8) = -1 then lb_corretto = false // Creo le Addizionali per i Campi in dw_passo_8
st_2.text = " Creazione delle addizionali automatiche ....."
if wf_crea_add_mp_non_ric() = -1 then lb_corretto = false // Creo le Addizionali per le materie prime non richieste

// --------------------------------- aggiunta optional ------------------------------------------------------------------------

uo_optional luo_optional
luo_optional = CREATE uo_optional
if luo_optional.uof_scrivi_optional(str_conf_prodotto.tipo_gestione, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, istr_riepilogo.optional, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio,  exclamation!, ok!)
	rollback;
	return -1
end if	
destroy luo_optional

//#############################################################################
//controllo se devo ripristinare collegamenti a fatture proforma (vale solo per ordini non per offerte)
//caso di ripassaggio dal configuratore, infatti le righe riferite vengono in tal caso cancellate (funzione uof_crea_comp_det) e reinserite (funzione wf_crea_addizionali_passo + uof_scrivi_optional)
//il ricollegamento viene fatto + in basso, dipo la funzione de re-inserimento ...
if lb_ripristina_proforma then
	//in lds_proforma_collegate ho i dati cosi configurati in colonna
	//1.		anno ordine vendita
	//2.		numero ordine vendita
	//3.		riga ordine vendita
	//4.		anno fattura proforma
	//5.		numero fattura proforma
	//6.		riga fattura proforma
	wf_ripristina_proforma(lds_proforma_collegate)
end if

//#############################################################################


if lb_corretto then
	s_cs_xx.parametri.parametro_s_4 = str_conf_prodotto.cod_prodotto_finito
//	close (parent)
else
	ROLLBACK;
	return -1
end if

// ----------------------------------  calcolo del prezzo  --------------------------------------------------- //

st_2.text = " Calcolo prezzo prodotto finito in corso ......."
s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.cod_versione = dw_inizio.getitemstring(1, "rs_cod_versione")
s_cs_xx.listino_db.quan_prodotto_finito = istr_riepilogo.quan_vendita
s_cs_xx.listino_db.anno_documento = str_conf_prodotto.anno_documento
s_cs_xx.listino_db.num_documento = str_conf_prodotto.num_documento
s_cs_xx.listino_db.prog_riga = str_conf_prodotto.prog_riga_documento
s_cs_xx.listino_db.tipo_gestione = ls_tabella


choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"

		select cod_cliente, 
				 cod_valuta, 
				 cod_tipo_listino_prodotto, 
				 data_registrazione,
				 cambio_ven
		into  :s_cs_xx.listino_db.cod_cliente, 
				:s_cs_xx.listino_db.cod_valuta, 
				:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
				:s_cs_xx.listino_db.data_riferimento,
				:s_cs_xx.listino_db.cambio_ven
		from  tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :str_conf_prodotto.anno_documento and
				num_registrazione = :str_conf_prodotto.num_documento ;

	case "OFF_VEN"

		select cod_cliente, 
				 cod_valuta, 
				 cod_tipo_listino_prodotto, 
				 data_registrazione,
				 cambio_ven
		into  :s_cs_xx.listino_db.cod_cliente, 
				:s_cs_xx.listino_db.cod_valuta, 
				:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
				:s_cs_xx.listino_db.data_riferimento,
				:s_cs_xx.listino_db.cambio_ven
		from  tes_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :str_conf_prodotto.anno_documento and
				num_registrazione = :str_conf_prodotto.num_documento ;
				
		if isnull(s_cs_xx.listino_db.cod_cliente) then
			s_cs_xx.listino_db.cod_cliente = "XXXXXX"
		end if
		
end choose

iuo_condizioni_cliente.str_parametri.ldw_oggetto = dw_inizio
iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
iuo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
iuo_condizioni_cliente.str_parametri.cambio_ven = s_cs_xx.listino_db.cambio_ven
iuo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
iuo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
iuo_condizioni_cliente.str_parametri.cod_prodotto = dw_inizio.getitemstring(1, "rs_cod_modello")
iuo_condizioni_cliente.str_parametri.dim_1 = istr_riepilogo.dimensione_1
iuo_condizioni_cliente.str_parametri.dim_2 = istr_riepilogo.dimensione_2
iuo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
iuo_condizioni_cliente.str_parametri.valore = 0
iuo_condizioni_cliente.str_parametri.cod_agente_1 = str_conf_prodotto.cod_agente_1
iuo_condizioni_cliente.str_parametri.cod_agente_2 = str_conf_prodotto.cod_agente_2
iuo_condizioni_cliente.str_parametri.colonna_quantita = ""
iuo_condizioni_cliente.str_parametri.colonna_prezzo = ""
iuo_condizioni_cliente.ib_setitem = false
iuo_condizioni_cliente.ib_setitem_provvigioni = false
iuo_condizioni_cliente.is_prodotti_addizionali = istr_riepilogo.cod_prodotto_addizionale
iuo_condizioni_cliente.lb_flag_escludi_linee_sconto=false
iuo_condizioni_cliente.wf_condizioni_cliente()

for ll_i = 1 to 10
	
	if str_conf_prodotto.stato = "N" or ( str_conf_prodotto.stato = "M" and ib_nuovo_prima_volta ) then
		// si tratta di un nuovo prodotto configurato
		
		if upperbound(iuo_condizioni_cliente.str_output.sconti) >= ll_i then
			istr_riepilogo.sconti[ll_i] = iuo_condizioni_cliente.str_output.sconti[ll_i]
		else
			istr_riepilogo.sconti[ll_i] = 0
		end if
	
	else
		
		// in modifica anche con cambio modello, ma sempre sentro al configuratore
		if str_conf_prodotto.stato = "M" and NOT str_conf_prodotto.flag_cambio_modello then
			if upperbound(iuo_condizioni_cliente.str_output.sconti) >= ll_i then
				istr_riepilogo.sconti[ll_i] = iuo_condizioni_cliente.str_output.sconti[ll_i]
			else
				istr_riepilogo.sconti[ll_i] = 0
			end if
		end if
		
		// in modifica ma se il flag_cambio_modello="S" allora provengo dal pulsante ricacola
		if str_conf_prodotto.stato = "M" and str_conf_prodotto.flag_cambio_modello then
			istr_riepilogo.sconti[ll_i] = str_conf_prodotto.sconti[ll_i]
		end if
		
	end if	
		
	
next

if str_conf_prodotto.stato = "N" then
	ld_provvigione_1 = iuo_condizioni_cliente.str_output.provvigione_1
	ld_provvigione_2 = iuo_condizioni_cliente.str_output.provvigione_2
else
	ld_provvigione_1 = str_conf_prodotto.provvigione_1
	ld_provvigione_2 = str_conf_prodotto.provvigione_2
end if
if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
st_sconti.text = ""														
lb_test = true
for ll_i = 1 to 10
	if istr_riepilogo.sconti[ll_i] > 0 then
		if ll_i > 0 and st_sconti.text <> "" then st_sconti.text = st_sconti.text + " + "
		st_sconti.text = st_sconti.text + string(istr_riepilogo.sconti[ll_i], "##0.0#")
		lb_test = false
		st_4.text = "*" + st_4.text 
	end if
next

// ----------------  calcolo il costo del telo finito e lo sommo al prezzo del sistema ------------------------------ //
dec{4} ld_prezzo_telo_finito
ld_prezzo_telo_finito = 0

if not isnull(istr_riepilogo.cod_modello_telo_finito) and len(istr_riepilogo.cod_modello_telo_finito) > 0 then

	luo_listini = create uo_condizioni_cliente
	
	luo_listini.str_parametri.ldw_oggetto = dw_inizio
	luo_listini.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
	luo_listini.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
	luo_listini.str_parametri.cambio_ven = s_cs_xx.listino_db.cambio_ven
	luo_listini.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
	luo_listini.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
	luo_listini.str_parametri.cod_prodotto = istr_riepilogo.cod_modello_telo_finito
	luo_listini.str_parametri.dim_1 = istr_riepilogo.dimensione_1
	luo_listini.str_parametri.dim_2 = istr_riepilogo.dimensione_2
	luo_listini.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
	luo_listini.str_parametri.valore = 0
	luo_listini.str_parametri.cod_agente_1 = str_conf_prodotto.cod_agente_1
	luo_listini.str_parametri.cod_agente_2 = ls_null
	luo_listini.str_parametri.colonna_quantita = ""
	luo_listini.str_parametri.colonna_prezzo = ""
	luo_listini.ib_setitem = false
	luo_listini.ib_setitem_provvigioni = false
	luo_listini.lb_flag_escludi_linee_sconto=true
	luo_listini.wf_condizioni_cliente()
	
if upperbound(luo_listini.str_output.variazioni) < 1 or isnull(upperbound(luo_listini.str_output.variazioni)) then
	ld_prezzo_telo_finito = 0
else
	ld_prezzo_telo_finito = luo_listini.str_output.variazioni[upperbound(luo_listini.str_output.variazioni)]
end if

end if
// ----------------  fine DEKORA 12/2/2014 --------------------------------------------------------------------------------------------------------- //

if upperbound(iuo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(iuo_condizioni_cliente.str_output.variazioni)) then
	st_prezzo.text = "0,00"
	ld_prezzo_vendita = 0
	ld_fat_conversione = 1
else
	ld_prezzo_vendita = iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
	if ld_prezzo_telo_finito > 0 then ld_prezzo_vendita += ld_prezzo_telo_finito
	st_prezzo.text = string(	ld_prezzo_vendita,"###,###,###,##0.0000")
	ld_fat_conversione = 1
end if

// calcolo del valore netto riga //
setnull(ls_cod_tipo_det_ven)
setnull(ls_cod_tipo_ord_ven)

if str_conf_prodotto.tipo_gestione = "ORD_VEN" then

	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :str_conf_prodotto.anno_documento and
			 num_registrazione  = :str_conf_prodotto.num_documento;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipi ordini dalla testata. Dettaglio errore "+sqlca.sqlerrtext)
		ROLLBACK;
		return -1
	end if
	
	if isnull(ls_cod_tipo_ord_ven) then
		g_mb.messagebox("Configuratore", "Tipo Ordine non impostato nella testata ordine." )
		ROLLBACK;
		return -1
	end if
	
	select cod_tipo_det_ven_prodotto
	into 	:ls_cod_tipo_det_ven
	from 	tab_flags_conf_tipi_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello and 
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		ROLLBACK;
		return -1
	end if
	
end if


if isnull(ls_cod_tipo_det_ven) then
	select cod_tipo_det_ven_prodotto
	into   :ls_cod_tipo_det_ven
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		ROLLBACK;
		return -1
	end if
	if isnull(ls_cod_tipo_det_ven) then
		g_mb.messagebox("Configuratore","Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella parametri configuratore")
		ROLLBACK;
		return -1
	end if
end if
// ----------------------- gestione del prezzo per metro quadarato con il fattore di conversione ------------------------

//	ld_fat_conversione = 1
//	ld_prezzo_vendita = iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
if istr_flags.flag_prezzo_superficie = "S" then
	if istr_flags.flag_dimensione_1 = "S" then
		if iuo_condizioni_cliente.str_output.min_fat_larghezza > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_larghezza) and istr_riepilogo.dimensione_1 < iuo_condizioni_cliente.str_output.min_fat_larghezza then
			ld_dimensione_1 = iuo_condizioni_cliente.str_output.min_fat_larghezza
		else
			ld_dimensione_1 = istr_riepilogo.dimensione_1
		end if
	end if
	if istr_flags.flag_dimensione_2 = "S" then
		if iuo_condizioni_cliente.str_output.min_fat_altezza > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_altezza) and istr_riepilogo.dimensione_2 < iuo_condizioni_cliente.str_output.min_fat_altezza then
			ld_dimensione_2 = iuo_condizioni_cliente.str_output.min_fat_altezza
		else
			ld_dimensione_2 = istr_riepilogo.dimensione_2
		end if
	end if
	if istr_flags.flag_dimensione_3 = "S" then
		if iuo_condizioni_cliente.str_output.min_fat_profondita > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_profondita) and istr_riepilogo.dimensione_3 < iuo_condizioni_cliente.str_output.min_fat_profondita then
			ld_dimensione_3 = iuo_condizioni_cliente.str_output.min_fat_profondita
		else
			ld_dimensione_3 = istr_riepilogo.dimensione_1
		end if
	end if
	
	// --------------------- nuova gestione vuoto per pieno -------------------------
	//                         nicola ferrari 23/05/2000
	if istr_flags.flag_uso_quan_distinta = "N" then
		ld_fat_conversione = round((ld_dimensione_1 * ld_dimensione_2) / 10000,2)
		if ld_fat_conversione <= 0 or isnull(ld_fat_conversione) then
			g_mb.messagebox("Configuratore", "Attenzione la quantità risultante dal calcolo DIM1 x DIM2 è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!")
			ld_fat_conversione = 1
		end if
	else
		string ls_cod_tessuto, ls_errore
		dec{4} ld_quan_tessuto
		uo_conf_varianti uo_ins_varianti
		
		uo_ins_varianti = CREATE uo_conf_varianti
		if uo_ins_varianti.uof_ricerca_quan_mp(str_conf_prodotto.cod_modello,  &
														s_cs_xx.listino_db.cod_versione, &
														s_cs_xx.listino_db.quan_prodotto_finito, &
														istr_flags.cod_gruppo_var_quan_distinta, &
														str_conf_prodotto.anno_documento, &
														str_conf_prodotto.num_documento, &
														str_conf_prodotto.prog_riga_documento, &
														ls_tabella, &
														ls_cod_tessuto, &
														ld_quan_tessuto,&
														ls_errore) = -1 then
			g_mb.messagebox("Configuratore","Errore nella ricerca quantità tessuto in disntita base. Dettaglio errore: " + ls_errore)
			rollback;
			return
		end if

		if ld_quan_tessuto <= 0 or isnull(ld_quan_tessuto) then
			g_mb.messagebox("Configuratore", "Attenzione la quantità trovata nel gruppo variante " + istr_flags.cod_gruppo_var_quan_distinta + " è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!")
			ld_quan_tessuto = 1
		end if
		destroy uo_ins_varianti
		ld_fat_conversione = round(ld_quan_tessuto,2)
	end if

//
	// ---------------------- fine vuoto per pieno ----------------------------------
	// controllo il minimale di superficie
	if iuo_condizioni_cliente.str_output.min_fat_superficie > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_superficie) and ld_fat_conversione < iuo_condizioni_cliente.str_output.min_fat_superficie then
		ld_fat_conversione = iuo_condizioni_cliente.str_output.min_fat_superficie
	end if
	ld_quantita_um = s_cs_xx.listino_db.quan_prodotto_finito * ld_fat_conversione
	ld_prezzo_um = ld_prezzo_vendita
	ldd_prezzo_vendita = dec(ld_prezzo_vendita)
	ldd_fat_conversione = dec(ld_fat_conversione)
	ld_prezzo_vendita = ldd_prezzo_vendita * ldd_fat_conversione
end if	

// >>>> decimali del prezzo di vendita
if iuo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, str_conf_prodotto.cod_valuta, ld_prezzo_vendita, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore","Errore durante l'arrotondamento del prezzo di vendita: " + ls_messaggio)
	rollback;
	return -1
end if

// ----------------------  Gestione del PREZZO BLOCCATO  -------------------------------------------------------
lb_blocco_prezzo = false
if ls_flag_prezzo_bloccato = "S" and str_conf_prodotto.stato = "M" then
	ls_messaggio =         "COND. PRECED.: Prezzo=" + string(ld_prezzo_vendita_origine,"###,##0.00##") + " %Sc. " + string(ld_sconto_1_origine,"#0")     + "+ " + string(ld_sconto_2_origine,"#0") + " %Prov. "    + string(ld_provvigione_1_origine,"#0") + " - " + string(ld_provvigione_2_origine,"#0")
	ls_messaggio +="~r~nCOND. NUOVA: Prezzo=" + string(ld_prezzo_vendita,"###,##0.00##")             + " %Sc. " + string(istr_riepilogo.sconti[1],"#0") + "+" + string(istr_riepilogo.sconti[2],"#0") + " %Prov. " + string(ld_provvigione_1,"#0")             + " - " + string(ld_provvigione_2,"#0")
	ls_messaggio += "~r~nMANTENGO LA PRECEDENTE CONDIZIONE ? "
	if g_mb.messagebox("PREZZO BLOCCATO", ls_messaggio, Question!, YesNo!, 2) = 1 then lb_blocco_prezzo = true
end if

ls_messaggio = ""

if lb_blocco_prezzo then
	ld_prezzo_vendita = ld_prezzo_vendita_origine
	istr_riepilogo.sconti[1] = ld_sconto_1_origine
	istr_riepilogo.sconti[2] = ld_sconto_2_origine
	ld_provvigione_1 = ld_provvigione_1_origine
	ld_provvigione_2 = ld_provvigione_2_origine
end if


ls_cod_valuta = s_cs_xx.listino_db.cod_valuta
ld_quan_ordinata = s_cs_xx.listino_db.quan_prodotto_finito

// -------------------------------  calcolo il valore della riga -------------------------------------------------
luo_scrivi_tabelle = CREATE uo_scrivi_tabelle
if luo_scrivi_tabelle.uof_calcola_val_riga(ls_cod_tipo_det_ven, &
														 s_cs_xx.listino_db.cod_valuta, &
														 s_cs_xx.listino_db.quan_prodotto_finito, &
														 ld_prezzo_vendita, &
														 iuo_condizioni_cliente.str_output.sconti[], &
														 str_conf_prodotto.sconto_testata, &
														 str_conf_prodotto.sconto_pagamento ,&
														 ld_valore_riga, &
														 ls_messaggio ) = -1 then 
	g_mb.messagebox("Configuratore",ls_messaggio)
	destroy luo_scrivi_tabelle
	rollback;
	return
end if
destroy luo_scrivi_tabelle

//	------------------------------// SQL aggiornamento riga dettaglio ///--------------------------------------------

if ld_provvigione_1 = 0 or isnull(ld_provvigione_1) then 
	ls_provvigione_1 = "0"
else
	ls_provvigione_1 = f_double_to_string(ld_provvigione_1)
	if isnull(ls_provvigione_1) then ls_provvigione_1 = "0"
end if
if ld_provvigione_2 = 0 or isnull(ld_provvigione_2) then 
	ls_provvigione_2 = "0"
else
	ls_provvigione_2 = f_double_to_string(ld_provvigione_2)
	if isnull(ls_provvigione_2) then ls_provvigione_2 = "0"
end if

if ld_prezzo_vendita = 0 or isnull(ld_prezzo_vendita) then 
	ls_prezzo_vendita = "0"
else
	ls_prezzo_vendita = f_double_to_string(ld_prezzo_vendita)
	if isnull(ls_prezzo_vendita) then ls_prezzo_vendita = "0"
end if

if ld_valore_riga = 0 or isnull(ld_valore_riga) then 
	ls_valore_riga = "0"
else
	ls_valore_riga = f_double_to_string(ld_valore_riga)
	if isnull(ls_valore_riga) then ls_valore_riga = "0"
end if

if ld_fat_conversione = 0 or isnull(ld_fat_conversione) then 
	ls_fat_conversione = "0"
else
	ls_fat_conversione = f_double_to_string(ld_fat_conversione)
	if isnull(ls_fat_conversione) then ls_fat_conversione = "1"
end if

if ld_prezzo_um = 0 or isnull(ld_prezzo_um) then 
	ls_prezzo_um = "0"
else
	ls_prezzo_um = f_double_to_string(ld_prezzo_um)
	if isnull(ls_prezzo_um) then ls_prezzo_um = "0"
end if

if ld_quantita_um = 0 or isnull(ld_quantita_um) then 
	ls_quantita_um = "0"
else
	ls_quantita_um = f_double_to_string(ld_quantita_um)
	if isnull(ls_quantita_um) then ls_quantita_um = "0"
end if

ls_sql =" update " + ls_tabella_det_doc
ls_sql = ls_sql + " set prezzo_vendita = " + ls_prezzo_vendita
ls_sql = ls_sql + ", prezzo_um = " + ls_prezzo_um
ls_sql = ls_sql + ", quantita_um = " + ls_quantita_um

choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		ls_sql = ls_sql + ", val_riga = " + ls_valore_riga
		ls_sql = ls_sql + ", imponibile_iva = " + ls_valore_riga
	case "OFF_VEN"
		//ls_sql = ls_sql + ", val_riga = " + ls_valore_riga
		ls_sql = ls_sql + ", imponibile_iva = " + ls_valore_riga
end choose

ls_sql = ls_sql + ", fat_conversione_ven = " + ls_fat_conversione

for ll_i = 1 to upperbound(istr_riepilogo.sconti)
	if ll_i > 10 then exit
	if istr_riepilogo.sconti[ll_i] = 0 or isnull(istr_riepilogo.sconti[ll_i]) then 
		ls_sconto = "0"
	else
		ls_sconto = f_double_to_string(istr_riepilogo.sconti[ll_i])
		if isnull(ls_sconto) then ls_sconto = "0"
	end if
	ls_sql = ls_sql + ", sconto_" + string(ll_i) + " = " + ls_sconto
next

ls_sql = ls_sql + ", provvigione_1 = " + ls_provvigione_1 + ", provvigione_2 = " + ls_provvigione_2

ls_sql =ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  " anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
					  " num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
					    ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)
	

execute immediate :ls_sql;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell'aggiornamento del prezzo di vendita. Dettaglio " + sqlca.sqlerrtext, exclamation!, ok!)
	rollback;
	return
end if

/* ------------------------------------- Viropa 28/8/2007 aggiorno il prezzo delle addizionali che --------------
						devono avere separata indicazione rispetto al prezzo di vendita												
						(vedi proogetto  VIROPA_STARTUP)																							*/
string ls_cod_tipo_det_ven_addizionale, ls_valore_riga_add


ll_count_addizionali = upperbound(iuo_condizioni_cliente.str_output.str_addizionali)
if isnull(ll_count_addizionali) then ll_count_addizionali = 0

for ll_z = 1 to ll_count_addizionali

	ld_prezzo_vendita = iuo_condizioni_cliente.str_output.str_addizionali[ll_z].valore
	
	if iuo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, str_conf_prodotto.cod_valuta, ld_prezzo_vendita, ls_messaggio) = -1 then
		g_mb.messagebox("Configuratore","Errore durante l'arrotondamento del prezzo di vendita: " + ls_messaggio)
		rollback;
		return -1
	end if
	
	if ld_prezzo_vendita = 0 or isnull(ld_prezzo_vendita) then 
		ls_prezzo_vendita = "0"
	else
		ls_prezzo_vendita = f_double_to_string(ld_prezzo_vendita)
		if isnull(ls_prezzo_vendita) then ls_prezzo_vendita = "0"
	end if
	
	select cod_tipo_det_ven
	into	 :ls_cod_tipo_det_ven_addizionale
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :str_conf_prodotto.anno_documento and
		    num_registrazione = :str_conf_prodotto.num_documento and
			 num_riga_appartenenza = :str_conf_prodotto.prog_riga_documento and
			 cod_prodotto = :iuo_condizioni_cliente.str_output.str_addizionali[ll_z].cod_addizionale;
	
	// Enme 28/01/2009
	// Aggiunto per calcolo fido cliente: devo valorizzare tutte le righe altrimenti salta il fido
	luo_scrivi_tabelle = CREATE uo_scrivi_tabelle
	if luo_scrivi_tabelle.uof_calcola_val_riga(ls_cod_tipo_det_ven, &
															 s_cs_xx.listino_db.cod_valuta, &
															 s_cs_xx.listino_db.quan_prodotto_finito, &
															 ld_prezzo_vendita, &
															 istr_riepilogo.sconti[], &
															 str_conf_prodotto.sconto_testata, &
															 str_conf_prodotto.sconto_pagamento ,&
															 ld_valore_riga, &
															 ls_messaggio ) = -1 then 
		g_mb.messagebox("Configuratore",ls_messaggio)
		destroy luo_scrivi_tabelle
		rollback;
		return
	end if
	destroy luo_scrivi_tabelle
	
	if ld_valore_riga = 0 or isnull(ld_valore_riga) then 
		ls_valore_riga_add = "0"
	else
		ls_valore_riga_add = f_double_to_string(ld_valore_riga)
		if isnull(ls_valore_riga_add) then ls_valore_riga_add = "0"
	end if
	
	
	ls_sql =" update " + ls_tabella_det_doc
	ls_sql = ls_sql + " set prezzo_vendita = " + ls_prezzo_vendita
	ls_sql = ls_sql + ", fat_conversione_ven = 1"
	
	if str_conf_prodotto.tipo_gestione = "ORD_VEN" then 
		ls_sql = ls_sql + ", val_riga = " + ls_valore_riga_add
	end if
	
	ls_sql = ls_sql + ", imponibile_iva = " + ls_valore_riga_add
	
	for ll_i = 1 to upperbound(istr_riepilogo.sconti)
		if ll_i > 10 then exit
		if istr_riepilogo.sconti[ll_i] = 0 or isnull(istr_riepilogo.sconti[ll_i]) then 
			ls_sconto = "0"
		else
			ls_sconto = f_double_to_string(istr_riepilogo.sconti[ll_i])
			if isnull(ls_sconto) then ls_sconto = "0"
		end if
		ls_sql = ls_sql + ", sconto_" + string(ll_i) + " = " + ls_sconto
	next
	
	ls_sql = ls_sql + ", provvigione_1 = " + ls_provvigione_1 + ", provvigione_2 = " + ls_provvigione_2
	
	ls_sql =ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						  " anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
						  " num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
						  " num_riga_appartenenza = " + string(str_conf_prodotto.prog_riga_documento) + " and " + &
						  " cod_prodotto = '" + iuo_condizioni_cliente.str_output.str_addizionali[ll_z].cod_addizionale + "'"
	

	execute immediate :ls_sql;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell'aggiornamento del prezzo di vendita. Dettaglio " + sqlca.sqlerrtext, exclamation!, ok!)
		rollback;
		return
	end if
next



// -----------   gestioneimpegno reparti calendario --------------------------------------------------------------------------
select flag_calendario_prod
into	:ls_flag_calendario_prod
from	con_vendite
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	ls_flag_calendario_prod = "S"
end if

if str_conf_prodotto.tipo_gestione = "ORD_VEN" and ls_flag_calendario_prod ="S" then
	
	st_2.text = " Aggiornamento calendario produzione in corso ......."
	
	if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
		//devo ricalcolare date e reparti per tutte le righe dell'ordine
		li_ret = wf_aggiorna_varianti_e_calendario(0, ls_errore)
	else
		//solo riga conte
		li_ret = wf_aggiorna_varianti_e_calendario(str_conf_prodotto.prog_riga_documento, ls_errore)
	end if
	
	if li_ret<0 then
		rollback;
		g_mb.error("Attenzione", ls_errore)
		return
		
	elseif ls_errore<>"" and li_ret=1 then
		
		//mostro nel log ma vai avanti lo stesso
		dw_trasferimenti.reset()
		li_ret = dw_trasferimenti.insertrow(0)
		dw_trasferimenti.setitem(li_ret, "testo", ls_errore)
		
	end if
	
end if
//---------------------------------------------------------------------------------------------------------------------------------

//Donato 24/01/2012: SOLo PER ORDINI DI VENDITA
//calcolo peso netto unitario della riga e delle rispettive righe collegate
//+ calcolo/impostazione spese trasporto su riga principale ed eventuali righe riferite
//################################################
ls_messaggio = ""
s_cs_xx.parametri.parametro_s_7= ""
if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
	st_2.text = " Calcolo peso netto unitario e impostazione spese trasporto riga configurata e righe collegate..."
	li_ret = wf_peso_netto(str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, ls_messaggio)

	if li_ret<0 then
		rollback;
		g_mb.error("Attenzione", ls_messaggio)
		return
	end if
	
	luo_spese_trasporto = create uo_spese_trasporto
	li_ret = luo_spese_trasporto.uof_imposta_spese_ord_ven(	str_conf_prodotto.anno_documento, &
																				str_conf_prodotto.num_documento, &
																				str_conf_prodotto.prog_riga_documento, true, ls_messaggio)
	destroy luo_spese_trasporto
	
	if li_ret<0 then
		rollback;
		g_mb.error("Attenzione", ls_messaggio)
		return
	end if
	
	//-----------------------------------------------------------------------------------------------------------
	//aggiungo qui il pezzo per salvare eventualmente il logo etichette in testata ordine di vendita
	select logo_etichetta
	into :ls_logo_etichetta
	from tes_ord_ven
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :str_conf_prodotto.anno_documento and
				num_registrazione = :str_conf_prodotto.num_documento;
				
	if isnull(ls_logo_etichetta) then ls_logo_etichetta = ""
	
	ls_logo_etichetta_new = dw_inizio.getitemstring(1, "logo_etichetta")
	if g_str.isempty(ls_logo_etichetta_new) or ls_logo_etichetta_new=is_logostandard then
		ls_logo_etichetta_new = ""
	end if
	
	if ls_logo_etichetta_new <> ls_logo_etichetta then
		update tes_ord_ven
		set logo_etichetta = :ls_logo_etichetta_new
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :str_conf_prodotto.anno_documento and
					num_registrazione = :str_conf_prodotto.num_documento;
		
		s_cs_xx.parametri.parametro_s_7= "ETICHETTA"
		s_cs_xx.parametri.parametro_s_6 = ls_logo_etichetta_new
	end if
	//-----------------------------------------------------------------------------------------------------------
	
	//il commit viene fatto dopo ...
end if
//##############################################


COMMIT using sqlca;

if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
	
	st_2.text = " Controllo fuori fido in corso ......."
	
//GESTIONE FIDO
//Donato 05-11-2008 facciamo solo il controllo dell'esposizione e di eventuali scaduti non pagati
//inserire qui il codice per il calcolo dell'esposizione

	uo_fido_cliente l_uo_fido_cliente
	long ll_return, ll_cur_row
	string ls_cod_cliente, ls_flag_aut_sblocco
	
	l_uo_fido_cliente = create uo_fido_cliente
	
	ls_cod_cliente = str_conf_prodotto.cod_cliente
	if not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then
		select flag_aut_sblocco_fido
		into   :ls_flag_aut_sblocco
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione  = :str_conf_prodotto.num_documento;	
		
		if sqlca.sqlcode <> 0 then
			ls_flag_aut_sblocco = "N"
		end if
		
		l_uo_fido_cliente.is_flag_autorizza_sblocco = ls_flag_aut_sblocco
	
		ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
		destroy l_uo_fido_cliente
	
		if ll_return = -1 then
			g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
			rollback;
			return
		end if
	
		if ll_return = 2 then
			//blocca perchè non sono autorizzato
			rollback;
			return
		end if
	else
		
	end if
//fine modifica GESTIONE FIDO

end if

// ------------------------------------- calcolo tempo elaborazione ------------------------------------------

ll_fine = (minute(now()) * 60) + second(now())
ll_tempo = ll_fine - ll_inizio
st_2.text = "Tempo Conf.: " + string(ll_tempo,"####") + " secondi"
COMMIT using sqlca;


//dopo aver fatto il commit verifico se per caso l'utente aveva scelto di spostare la data consegna dell'ordine
//con doppio clic sull'oggetto semaforo e poi su una colonna data diversa dalla consegna prevista
if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
	s_cs_xx.parametri.parametro_data_4 = idt_data_partenza
else
	setnull(s_cs_xx.parametri.parametro_data_4)
end if
//-----------------------------------------------------------------------------------------------------------------------



// ------------------------------------- genero anche la commessa e se richiesto la chiudo ------------------------------------
//                                            (e poi magari faccio anche il caffè)
if istr_flags.flag_genera_commessa then
	integer li_anno_commessa
	long    ll_num_commessa, ll_null
	
	luo_funzioni_1 = CREATE uo_funzioni_1
	
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :str_conf_prodotto.anno_documento and
			 num_registrazione  = :str_conf_prodotto.num_documento;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Creazione commessa","Errore in ricerca ordine: creazione commessa interrotta",stopsign!)
		rollback using sqlca;
		istr_flags.flag_genera_commessa = FALSE
		istr_flags.flag_avanza_commessa = FALSE
		return
	end if
	
	setnull(ll_null)	
	st_2.text = st_2.text + "... generazione commessa in corso"
	if luo_funzioni_1.uof_genera_commesse(ls_cod_tipo_ord_ven, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, ll_null, li_anno_commessa, ll_num_commessa, ls_errore) = 0 then
		if	istr_flags.flag_avanza_commessa then
			st_2.text = "Chiusura della commessa " + string(li_anno_commessa, "####") + "-" + string(ll_num_commessa,"######") + " in corso"
			
			
			if luo_funzioni_1.uof_avanza_commessa(li_anno_commessa, ll_num_commessa, ls_null,ls_errore) = 0 then
				st_2.text = "GENERATA e CHIUSA commessa " + string(li_anno_commessa, "####") + "-" + string(ll_num_commessa,"######")
				istr_flags.flag_genera_commessa = FALSE
				istr_flags.flag_avanza_commessa = FALSE
				commit using sqlca;
			else
				g_mb.messagebox("Generazione commessa","Errore in chiusura commessa.~r~n" + ls_errore)
				rollback using sqlca;
			end if
		else
			st_2.text = "Generata " + string(li_anno_commessa, "####") + "-" + string(ll_num_commessa,"######")
			istr_flags.flag_genera_commessa = FALSE
			istr_flags.flag_avanza_commessa = FALSE
			commit using sqlca;
		end if			
	else
		g_mb.messagebox("Generazione commessa","Errore in generazione commessa.~r~n" + ls_errore)
		rollback using sqlca;
	end if
end if	

istr_flags.flag_genera_commessa = FALSE
istr_flags.flag_avanza_commessa = FALSE
COMMIT using sqlca;
destroy luo_funzioni_1

// -------------------------  aggiunto per centro gibus specifica SEMPLIFICAZIONI -------------------------------------------- //
if not(cbx_apri_dopo.checked) then
	// così il configuratore non si riapre più in automatico in base al parametro aziendale CG1; 
	// vedi specifica \gibus\progetti\semplificazioni\
	str_conf_prodotto.apri_configuratore = false
end if

if str_conf_prodotto.stato = "N" then
	// aggiunto questa variabile altrimenti mi azzera gli sconti anche con nuove configurazioni.
	// segnalato da Beatrice il 29/07/2004
	ib_nuovo_prima_volta = true
end if

str_conf_prodotto.stato = "M"
str_conf_prodotto.provvigione_1 = ld_provvigione_1
str_conf_prodotto.provvigione_2 = ld_provvigione_2

end event

type st_cod_prodotto_finito from statictext within w_configuratore
integer x = 27
integer y = 1904
integer width = 1509
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 15780518
boolean enabled = false
boolean focusrectangle = false
end type

type cb_note_esterne from cb_documenti_compilati within w_configuratore
integer x = 1257
integer y = 1292
integer height = 80
integer taborder = 230
string text = "Docu&mento"
end type

event clicked;call super::clicked;//blob lbl_null

//setnull(lbl_null)

//s_cs_xx.parametri.parametro_bl_1 = lbl_null
s_cs_xx.parametri.parametro_bl_1 = istr_riepilogo.note_esterne

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
	istr_riepilogo.note_esterne = s_cs_xx.parametri.parametro_bl_1
end if

setpointer(arrow!)
end event

type cb_stampa from commandbutton within w_configuratore
integer x = 1559
integer y = 1904
integer width = 361
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_job

ll_job = printopen()
choose case ii_num_passo
	case 0
		dw_inizio.triggerevent("pcd_print")
	case 1
		dw_passo_1.triggerevent("pcd_print")
	case 2
		dw_passo_2.triggerevent("pcd_print")
	case 3
		dw_passo_3.triggerevent("pcd_print")
	case 4
		dw_passo_4.triggerevent("pcd_print")
	case 5
		dw_passo_5.triggerevent("pcd_print")
	case 6
		dw_passo_6.triggerevent("pcd_print")
	case 7
		dw_passo_7.triggerevent("pcd_print")
	case 8
		dw_passo_8.triggerevent("pcd_print")
	case 99
		printdatawindow(ll_job, dw_fine)
end choose

printclose(ll_job)
end event

type st_prezzo from statictext within w_configuratore
integer x = 1787
integer y = 2004
integer width = 594
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_2 from statictext within w_configuratore
integer x = 27
integer y = 2004
integer width = 1509
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type st_3 from statictext within w_configuratore
integer x = 1559
integer y = 2004
integer width = 206
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Prezzo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_configuratore
integer x = 2405
integer y = 2004
integer width = 206
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Sconti:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_sconti from statictext within w_configuratore
integer x = 2633
integer y = 2004
integer width = 594
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_passo_6 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 160
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_6, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_5 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 170
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_5, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_4 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 180
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_4, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_3 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 190
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		selectblob note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		else
			p_1.visible = false
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_3, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_2 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
event ue_reset_folder_tessuto ( )
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 200
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_2, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_fine from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 130
string dataobject = "d_ext_fine"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose

uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_8 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 140
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_8, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_7 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
boolean visible = false
integer x = 1120
integer y = 200
integer width = 4000
integer height = 1420
integer taborder = 150
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//end choose
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_7, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_passo_1 from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
integer x = 1120
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 210
boolean bringtotop = true
string dataobject = ""
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key
end event

event itemchanged;call super::itemchanged;string ls_messaggio

choose case wf_item_changed (this, getcolumnname(), data, ref ls_messaggio)
	case -1
		g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
		setfocus(this)
		return 2
		
	case -2
		if g_mb.messagebox("Configuratore di Prodotto", ls_messaggio, Question!,YesNo!,2) = 1 then
				if g_mb.messagebox("Configuratore di Prodotto", "SEI PROPRIO SICURO DI VOLER PROCEDERE ?", Question!,YesNo!,2) = 1 then
				setfocus(this)
				return 0
			else
				wf_reset_tessuti( this, "TESSUTO")
				setfocus(this)
				return 1
			end if
		else
			wf_reset_tessuti( this, "TESSUTO")
			setfocus(this)
			return 1
		end if
		
end choose


blob   lb_blob
long   ll_cont
choose case getcolumnname()
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
			
		end if		
	case "rs_cod_comando_manuale", "rs_cod_comando"
		
		setnull(lb_blob)
		
		select note_esterne
		 INTO :lb_blob  
		 FROM tab_comandi  
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_comando = :data ;
					  
		LL_CONT = LEN(lb_blob)
		
		if not isnull(lb_blob) and ll_cont > 0 then
			ole_1.objectdata = lb_blob
			p_1.visible = true
		end if

end choose

setfocus(this)
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event clicked;call super::clicked;string ls_messaggio

if i_extendmode then
	if isvalid(dwo) then
		if wf_clicked(ref dw_passo_1, dwo.name, ref ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore", ls_messaggio)
			return
		end if
	end if
end if

return
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_inizio from uo_cs_xx_dw within w_configuratore
event ue_key pbm_dwnkey
event ue_cal_trasferimenti ( )
integer x = 1125
integer y = 32
integer width = 4000
integer height = 1816
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_ext_inizio"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;//choose case key
//	case KeyEnter!
//		if this.dataobject <> "d_ext_note" then
//			wf_spostamento("A")
//		end if
//	case keyF1!
//		if keyflags = 1 and this.getcolumnname() = "rs_cod_modello_manuale" then
//			this.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_s_1 = "rs_cod_modello"
//			s_cs_xx.parametri.parametro_tipo_ricerca = 1
//			s_cs_xx.parametri.parametro_pos_ricerca = "2"
//			if not isvalid(w_prodotti_ricerca_response) then
//				window_open(w_prodotti_ricerca_response, 0)
//			end if
//		end if
//end choose
//
//string ls_messaggio
uo_key luo_key

luo_key = CREATE uo_key
luo_key.uof_key(this, key, keyflags)
destroy luo_key

//
// provato ad aggiungere ma non viene completamente giusto.......
//
//if wf_item_changed (this, getcolumnname(), getitemstring(getrow(),getcolumnname()), ref ls_messaggio) = -1 then
//	messagebox("Configuratore di Prodotto", ls_messaggio)
//	return 2
//end if

end event

event ue_cal_trasferimenti();string				ls_cod_prodotto_finito, ls_cod_versione_finito, ls_errore

uo_calendario_prod_new			luo_cal_prod


if str_conf_prodotto.tipo_gestione<>"ORD_VEN" then return


luo_cal_prod = create uo_calendario_prod_new
luo_cal_prod.uof_dw_cal_produzione(		false, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, -1, &
													str_conf_prodotto.cod_modello, str_conf_prodotto.cod_versione, idt_data_partenza, &
													is_reparti[], idt_date_reparti[], dw_trasferimenti, ls_errore)

destroy	luo_cal_prod

ib_backup_vardetordven_prod = false

end event

event itemchanged;call super::itemchanged;string ls_messaggio, ls_cod_versione, ls_cod_prodotto

if wf_item_changed (this, getcolumnname(), data, ref ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	setfocus(this)
	return 2
end if

blob   lb_blob
long   ll_cont
choose case getcolumnname()
	
case "logo_etichetta"
	wf_preview_logo(data)
	
	case "rs_cod_modello_manuale", "rs_cod_modello"
		if not isnull(data) and len(data) > 0 then 
			select count(*)
			into   :ll_cont
			from   anag_prodotti_blob
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :data;
					 
			if ll_cont > 0 then
			
				s_cs_xx.parametri.parametro_s_1 = data
				open(w_elenco_blob)
				
				if not isnull(s_cs_xx.parametri.parametro_s_1) then
					setnull(lb_blob)
					
					selectblob blob
					into       :lb_blob
					from       anag_prodotti_blob
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  cod_prodotto = :data and
								  cod_blob = :s_cs_xx.parametri.parametro_s_1;
								  
					LL_CONT = LEN(lb_blob)
					
					if not isnull(lb_blob)  and ll_cont > 0 then
						ole_1.objectdata = lb_blob
						ole_1.activate(offsite!)
					end if
				end if	
			end if
		end if
		
		ls_cod_versione = getitemstring(getrow(), "rs_cod_versione")
		
		if ls_cod_versione<>"" and not isnull(ls_cod_versione) and data <>"" then
			// visualizzazione possibile calendario dei trasferimenti
			event post ue_cal_trasferimenti()
		end if
	
	case "rs_cod_versione"
		
		ls_cod_prodotto = getitemstring(getrow(), "rs_cod_modello_manuale")
		
		if data <>"" and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
			// visualizzazione possibile calendario dei trasferimenti
			event post ue_cal_trasferimenti()
		end if
	case "rd_dimensione_1"
//		uof_calcola_formula_inizio(  fdw_passo, 1)


end choose

setfocus(this)


end event

event pcd_new;call super::pcd_new;//if not isnull(s_cs_xx.parametri.parametro_s_2) then
//	string ls_messaggio
//	dw_inizio.setitem(1, "rs_cod_modello", s_cs_xx.parametri.parametro_s_2)
//	dw_inizio.setitem(1, "rs_cod_modello_manuale", s_cs_xx.parametri.parametro_s_2)
//	if wf_item_changed (dw_inizio, "rs_cod_modello_manuale", s_cs_xx.parametri.parametro_s_2, ref ls_messaggio) = -1 then
//		messagebox("Configuratore di Prodotto", ls_messaggio)
//		return 2
//	end if
//	setnull(s_cs_xx.parametri.parametro_s_2)
//	setnull(s_cs_xx.parametri.parametro_s_3)
//end if
if wf_pcd_new_dw(this) = -1 then
	cb_annulla.postevent(clicked!)
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_nota_prodotto_finito from uo_cs_xx_dw within w_configuratore
integer x = 1120
integer y = 1428
integer width = 2482
integer height = 464
integer taborder = 80
string dataobject = ""
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_messaggio
if wf_item_changed (this, getcolumnname(), data, ref ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return 2
end if
end event

event pcd_new;call super::pcd_new;if wf_pcd_new_dw(this) = -1 then
	return 2
end if

end event

event rowfocuschanged;call super::rowfocuschanged;string ls_messaggio
if wf_rowfocuschanged(this, currentrow, ls_messaggio) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
	return
end if
end event

event editchanged;call super::editchanged;accepttext()
end event

event doubleclicked;// ATTENZIONE LASCIARE: sempre extend ancestor DISATTIVATO !!!!! 
return
end event

type dw_nota_prodotto_finito_lista from datawindow within w_configuratore
integer x = 3607
integer y = 1428
integer width = 1509
integer height = 464
integer taborder = 260
string title = "none"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;string ls_nota_prodotto, ls_str
if row > 0 then
	ls_nota_prodotto = getitemstring(row, "nota_prodotto")
	ls_str = dw_nota_prodotto_finito.getitemstring(1,"nota_dettaglio")
	if isnull(ls_str) then ls_str = ""
	if len(ls_str) > 0 then
		ls_str += "~r~n"
	end if
	ls_str += ls_nota_prodotto
	dw_nota_prodotto_finito.setitem(1, "nota_dettaglio", ls_str )
end if
end event

type ole_1 from olecontrol within w_configuratore
boolean visible = false
integer x = 4571
integer y = 20
integer width = 279
integer height = 752
integer taborder = 160
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_configuratore.win"
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
05w_configuratore.bin 
2000000a00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff000000010003000c00000000000000c046000000000000000000000000000000cf63fe6001d6e4ec00000003000001c000000000004f00010065006c003000310061004e0069007400650076000000000000000000000000000000000000000000000000000000000000000000000000000000000102001a00000002ffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000012a0000000000430001006d006f004f0070006a006200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020012ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004c000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffe00000003000000040000000500000006fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
20fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe000100000a03ffffffff0003000c00000000000000c0460000000000000c20454c4f6b6361500065676100000000000000086b6361500065676171b239f40000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000126694e0002616c6f637461622e5c3a43004f43494e422e414c00005441380003004300000073555c3a5c73726549524e45317e4f434e454d2e7070415c61746144636f4c5c545c6c615c706d654f43494e2820414c422e29361f0054415400000020455059455c3a4348434954455454455458542e504c3e200a0d315400370a0d00430000005c003a0073005500720065005c0073004e004500490052004f00430031007e004d002e004e00450041005c007000700061004400610074004c005c0063006f006c00610054005c006d0065005c00700049004e004f00430041004c00280020002900360042002e005400410000000a0069004e006f00630061006c0062002e007400610000000d003a0043004e005c00430049004c004f002e00410041004200000054000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
15w_configuratore.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
