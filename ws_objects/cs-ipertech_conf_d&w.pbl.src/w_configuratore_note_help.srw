﻿$PBExportHeader$w_configuratore_note_help.srw
$PBExportComments$Finestra Configuratore di Prodotto
forward
global type w_configuratore_note_help from window
end type
type cb_1 from commandbutton within w_configuratore_note_help
end type
type dw_configuratore_note_help from datawindow within w_configuratore_note_help
end type
end forward

global type w_configuratore_note_help from window
integer width = 2450
integer height = 872
windowtype windowtype = response!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
dw_configuratore_note_help dw_configuratore_note_help
end type
global w_configuratore_note_help w_configuratore_note_help

on w_configuratore_note_help.create
this.cb_1=create cb_1
this.dw_configuratore_note_help=create dw_configuratore_note_help
this.Control[]={this.cb_1,&
this.dw_configuratore_note_help}
end on

on w_configuratore_note_help.destroy
destroy(this.cb_1)
destroy(this.dw_configuratore_note_help)
end on

event open;dw_configuratore_note_help.reset()

dw_configuratore_note_help.insertrow(0)

dw_configuratore_note_help.setitem(1,"help", message.stringparm)
end event

type cb_1 from commandbutton within w_configuratore_note_help
integer x = 2030
integer y = 748
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Chiudi"
end type

event clicked;string ls_str

dw_configuratore_note_help.accepttext()
ls_str = dw_configuratore_note_help.getitemstring(1,"help")

CloseWithReturn(Parent, ls_str)
end event

type dw_configuratore_note_help from datawindow within w_configuratore_note_help
integer x = 9
integer y = 12
integer width = 2405
integer height = 716
integer taborder = 10
string title = "none"
string dataobject = "d_ext_configuratore_note_help"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

