﻿$PBExportHeader$w_elenco_blob.srw
forward
global type w_elenco_blob from window
end type
type dw_1 from datawindow within w_elenco_blob
end type
end forward

global type w_elenco_blob from window
integer width = 1257
integer height = 788
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 134217752
string icon = "AppIcon!"
dw_1 dw_1
end type
global w_elenco_blob w_elenco_blob

event open;dw_1.settransobject(sqlca)
dw_1.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_1)
end event

on w_elenco_blob.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on w_elenco_blob.destroy
destroy(this.dw_1)
end on

type dw_1 from datawindow within w_elenco_blob
integer x = 5
integer y = 8
integer width = 1216
integer height = 692
integer taborder = 10
string title = "none"
string dataobject = "d_elenco_blob_prodotto"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = getitemstring(row,"cod_blob")
	close(parent)
end if
end event

