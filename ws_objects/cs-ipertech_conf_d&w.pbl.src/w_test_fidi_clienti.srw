﻿$PBExportHeader$w_test_fidi_clienti.srw
forward
global type w_test_fidi_clienti from w_cs_xx_principale
end type
type st_msg_impresa from statictext within w_test_fidi_clienti
end type
type st_25 from statictext within w_test_fidi_clienti
end type
type sle_esposizione_2 from singlelineedit within w_test_fidi_clienti
end type
type em_datarif from editmask within w_test_fidi_clienti
end type
type dw_cliente from u_dw_search within w_test_fidi_clienti
end type
type st_21 from statictext within w_test_fidi_clienti
end type
type st_24 from statictext within w_test_fidi_clienti
end type
type st_23 from statictext within w_test_fidi_clienti
end type
type st_22 from statictext within w_test_fidi_clienti
end type
type st_20 from statictext within w_test_fidi_clienti
end type
type r_2 from rectangle within w_test_fidi_clienti
end type
type sle_ord_ven_a_p_2 from singlelineedit within w_test_fidi_clienti
end type
type sle_ord_ven_in_sped_2 from singlelineedit within w_test_fidi_clienti
end type
type sle_ord_ven_ass_2 from singlelineedit within w_test_fidi_clienti
end type
type sle_bol_ven_2 from singlelineedit within w_test_fidi_clienti
end type
type sle_fat_ven_2 from singlelineedit within w_test_fidi_clienti
end type
type st_19 from statictext within w_test_fidi_clienti
end type
type st_18 from statictext within w_test_fidi_clienti
end type
type st_17 from statictext within w_test_fidi_clienti
end type
type st_16 from statictext within w_test_fidi_clienti
end type
type st_15 from statictext within w_test_fidi_clienti
end type
type sle_esposizione from singlelineedit within w_test_fidi_clienti
end type
type st_esposizione from statictext within w_test_fidi_clienti
end type
type sle_fuori_fido from singlelineedit within w_test_fidi_clienti
end type
type st_14 from statictext within w_test_fidi_clienti
end type
type sle_fido from singlelineedit within w_test_fidi_clienti
end type
type st_13 from statictext within w_test_fidi_clienti
end type
type st_12 from statictext within w_test_fidi_clienti
end type
type sle_saldo_cont_impresa from singlelineedit within w_test_fidi_clienti
end type
type sle_fat_ven from singlelineedit within w_test_fidi_clienti
end type
type sle_ord_ven_a_p from singlelineedit within w_test_fidi_clienti
end type
type sle_ord_ven_in_sped from singlelineedit within w_test_fidi_clienti
end type
type sle_ord_ven_ass from singlelineedit within w_test_fidi_clienti
end type
type sle_bol_ven from singlelineedit within w_test_fidi_clienti
end type
type st_11 from statictext within w_test_fidi_clienti
end type
type st_10 from statictext within w_test_fidi_clienti
end type
type st_9 from statictext within w_test_fidi_clienti
end type
type st_8 from statictext within w_test_fidi_clienti
end type
type st_7 from statictext within w_test_fidi_clienti
end type
type st_6 from statictext within w_test_fidi_clienti
end type
type st_5 from statictext within w_test_fidi_clienti
end type
type cb_chiudi from commandbutton within w_test_fidi_clienti
end type
type cb_calcola from commandbutton within w_test_fidi_clienti
end type
type sle_scad_aperte_impresa from singlelineedit within w_test_fidi_clienti
end type
type st_1 from statictext within w_test_fidi_clienti
end type
type st_2 from statictext within w_test_fidi_clienti
end type
type st_3 from statictext within w_test_fidi_clienti
end type
type st_4 from statictext within w_test_fidi_clienti
end type
type sle_riba_insolute_impresa from singlelineedit within w_test_fidi_clienti
end type
type sle_scaduto_aperto_impresa from singlelineedit within w_test_fidi_clienti
end type
type sle_riba_a_scadere_impresa from singlelineedit within w_test_fidi_clienti
end type
type r_1 from rectangle within w_test_fidi_clienti
end type
end forward

global type w_test_fidi_clienti from w_cs_xx_principale
integer width = 3369
integer height = 2320
string title = "test"
st_msg_impresa st_msg_impresa
st_25 st_25
sle_esposizione_2 sle_esposizione_2
em_datarif em_datarif
dw_cliente dw_cliente
st_21 st_21
st_24 st_24
st_23 st_23
st_22 st_22
st_20 st_20
r_2 r_2
sle_ord_ven_a_p_2 sle_ord_ven_a_p_2
sle_ord_ven_in_sped_2 sle_ord_ven_in_sped_2
sle_ord_ven_ass_2 sle_ord_ven_ass_2
sle_bol_ven_2 sle_bol_ven_2
sle_fat_ven_2 sle_fat_ven_2
st_19 st_19
st_18 st_18
st_17 st_17
st_16 st_16
st_15 st_15
sle_esposizione sle_esposizione
st_esposizione st_esposizione
sle_fuori_fido sle_fuori_fido
st_14 st_14
sle_fido sle_fido
st_13 st_13
st_12 st_12
sle_saldo_cont_impresa sle_saldo_cont_impresa
sle_fat_ven sle_fat_ven
sle_ord_ven_a_p sle_ord_ven_a_p
sle_ord_ven_in_sped sle_ord_ven_in_sped
sle_ord_ven_ass sle_ord_ven_ass
sle_bol_ven sle_bol_ven
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
cb_chiudi cb_chiudi
cb_calcola cb_calcola
sle_scad_aperte_impresa sle_scad_aperte_impresa
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
sle_riba_insolute_impresa sle_riba_insolute_impresa
sle_scaduto_aperto_impresa sle_scaduto_aperto_impresa
sle_riba_a_scadere_impresa sle_riba_a_scadere_impresa
r_1 r_1
end type
global w_test_fidi_clienti w_test_fidi_clienti

forward prototypes
public subroutine wf_stato_dellarte ()
public subroutine wf_modifiche_impresa ()
public subroutine wf_modifiche_apice ()
public subroutine wf_modifiche_esposizione ()
end prototypes

public subroutine wf_stato_dellarte ();string ls_cod_capoconto, ls_cod_cliente, ls_cod_impresa, ls_fuori_fido
dec{4}	ld_valore, ld_fido
datetime ldt_oggi
uo_impresa luo_impresa
long ll_anno

//ldt_oggi = datetime(today(),00:00:00)
ldt_oggi = datetime(date(em_datarif.text),00:00:00)
//ls_cod_cliente = sle_cod_cliente.text

ls_cod_cliente = dw_cliente.getitemstring(1,"cod_cliente")

//prm cliente ------------
select
	fido,
	flag_fuori_fido
into
	:ld_fido,
	:ls_fuori_fido
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura fido cliente: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	ld_fido = 0
	ls_fuori_fido = "N"
end if

//Validazione dei valori ottenuti
if isnull(ld_fido) then
	ld_fido = 0
end if

if isnull(ls_fuori_fido) then
	ls_fuori_fido = "N"
end if

sle_fido.text = string(ld_fido)
if ls_fuori_fido = "S" then
	sle_fuori_fido.text = "Si"
else
	sle_fuori_fido.text = "No"
end if
//------------------------

select
	cod_capoconto
into
	:ls_cod_capoconto
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura codice capoconto cliente: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("", "Errore in lettura codice capoconto cliente: cliente non trovato")
	return
end if

luo_impresa = create uo_impresa
ls_cod_impresa = luo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
destroy luo_impresa

//##############################################################################
//STATO DELL'ARTE
setnull(ld_valore)
setpointer(HourGlass!)

ll_anno = year(date(ldt_oggi))

if s_cs_xx.parametri.impresa and isvalid(sqlci) then
	st_msg_impresa.text = ""
	
	SELECT
		Sum(dba.saldo_mensile.dare) - Sum(dba.saldo_mensile.avere) AS saldo
	INTO
		:ld_valore
	FROM
		dba.saldo_mensile
		JOIN dba.conto ON dba.saldo_mensile.id_conto = dba.conto.id_conto
		JOIN dba.sottomastro ON dba.conto.id_sottomastro = dba.sottomastro.id_sottomastro
	WHERE
		dba.sottomastro.uso = 'C' AND
		dba.saldo_mensile.anno = :ll_anno AND
		dba.conto.codice = :ls_cod_impresa
	GROUP BY
		dba.conto.codice,
		dba.conto.descrizione
	USING
		sqlci;
		
	if sqlci.sqlcode < 0 then
		g_mb.messagebox("", "Errore in lettura saldo contabile cliente da IMPRESA: " + sqlci.sqlerrtext)
		return
	elseif sqlci.sqlcode = 100 then
	//		as_messaggio = "Errore in lettura saldo contabile cliente da IMPRESA: dati cliente non trovati"
	//		return -1
		ld_valore = 0
	end if
else
	st_msg_impresa.text = "Connessione IMPRESA NON DISPONIBILE!"
end if

if isnull(ld_valore) then
	ld_valore = 0
end if
sle_saldo_cont_impresa.text = string(ld_valore)
setpointer(Arrow!)

sle_esposizione.text = string(ld_valore)

//Lettura valore totale delle fatture stampate in definitivo ma non ancora contabilizzate (AD_TOT_FAT_VEN)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(imponibile_iva)
into
	:ld_valore
from
	tes_fat_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente and
	cod_documento is not null and
	numeratore_documento is not null and
	anno_documento is not null and
	num_documento is not null and
	flag_contabilita = 'N' and
	data_fattura <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_fat_ven.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione.text = string(dec(sle_esposizione.text) + ld_valore)

//Lettura valore totale delle bolle stampate in definitivo ma non ancora fatturate (AD_TOT_BOL_VEN)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(imponibile_iva)
into
	:ld_valore
from
	tes_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente and
	cod_documento is not null and
	numeratore_documento is not null and
	anno_documento is not null and
	num_documento is not null and
	flag_gen_fat = 'N' and
	data_bolla <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale bolle di vendita confermate non contabilizzate: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if

sle_bol_ven.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione.text = string(dec(sle_esposizione.text) + ld_valore)


//Lettura valore totale degli ordini assegnati (AD_TOT_ORD_VEN_ASS)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * det_ord_ven.quan_in_evasione)
into
	:ld_valore
from det_ord_ven
join tes_ord_ven on det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda
	and det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione
	and det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
	
where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda
	and tes_ord_ven.cod_cliente = :ls_cod_cliente
	and tes_ord_ven.data_registrazione <= :ldt_oggi
	and det_ord_ven.quan_ordine <> 0;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita assegnati: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_ord_ven_ass.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione.text = string(dec(sle_esposizione.text) + ld_valore)

//Lettura valore totale degli ordini in spedizione (bolle ancora non stampate in definitivo) (AD_TOT_ORD_VEN_SPE)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(imponibile_iva)
into
	:ld_valore
from
	tes_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente and
	data_bolla is null and
	flag_gen_fat = 'N' and
	data_registrazione <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita in spedizione: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_ord_ven_in_sped.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione.text = string(dec(sle_esposizione.text) + ld_valore)

//Lettura valore totale degli ordini aperti e parziali (AD_TOT_ORD_VEN_A_P)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * (det_ord_ven.quan_ordine - det_ord_ven.quan_evasa - det_ord_ven.quan_in_evasione))
into
	:ld_valore
from
	tes_ord_ven,
	det_ord_ven
where
	det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and
	det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and
	det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and
	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
	tes_ord_ven.cod_cliente = :ls_cod_cliente and
	tes_ord_ven.data_registrazione <= :ldt_oggi and
	det_ord_ven.quan_ordine <> 0;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita aperti e parziali: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if

sle_ord_ven_a_p.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione.text = string(dec(sle_esposizione.text) + ld_valore)



end subroutine

public subroutine wf_modifiche_impresa ();string ls_cod_capoconto, ls_cod_cliente, ls_cod_impresa, ls_fuori_fido
dec{4}	ld_valore, ld_fido
datetime ldt_oggi, ldt_oggi_toll_riba
uo_impresa luo_impresa
long ll_anno, ll_gg_riba

//ldt_oggi = datetime(today(),00:00:00)
ldt_oggi = datetime(date(em_datarif.text),00:00:00)
ls_cod_cliente = dw_cliente.getitemstring(1,"cod_cliente")

if s_cs_xx.parametri.impresa and isvalid(sqlci) then
	st_msg_impresa.text = ""
	
	select numero
	into :ll_gg_riba
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_parametro = 'RIB' and flag_parametro = 'N';
		
	if sqlca.sqlcode = 0 then
	else
		ll_gg_riba = -1
	end if
	
	//prm cliente ------------
	select
		fido,
		flag_fuori_fido
	into
		:ld_fido,
		:ls_fuori_fido
	from
		anag_clienti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("", "Errore in lettura fido cliente: " + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 100 then
		ld_fido = 0
		ls_fuori_fido = "N"
	end if
	
	//Validazione dei valori ottenuti
	if isnull(ld_fido) then
		ld_fido = 0
	end if
	
	if isnull(ls_fuori_fido) then
		ls_fuori_fido = "N"
	end if
	
	sle_fido.text = string(ld_fido)
	if ls_fuori_fido = "S" then
		sle_fuori_fido.text = "Si"
	else
		sle_fuori_fido.text = "No"
	end if
	//------------------------
	
	select
		cod_capoconto
	into
		:ls_cod_capoconto
	from
		anag_clienti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("", "Errore in lettura codice capoconto cliente: " + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("", "Errore in lettura codice capoconto cliente: cliente non trovato")
		return
	end if
	
	luo_impresa = create uo_impresa
	ls_cod_impresa = luo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
	destroy luo_impresa
	
	//IMPRESA ---------------------------------------------------------------------
	//scadenze aperte impresa ------------------------escludere le RIBA
	setnull(ld_valore)
	
	setpointer(HourGlass!)
	
	select sum(saldo)
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where dba.scadenza.tipo_scadenza='A' and dba.scadenza.saldo<>0 and dba.conto.codice=:ls_cod_impresa
		and dba.tipo_pagamento.codice not in ('RIBA','RB')
	using sqlci;
	
	if isnull(ld_valore) then ld_valore = 0
	
	sle_scad_aperte_impresa.text = string(ld_valore)
	setpointer(Arrow!)
	sle_esposizione_2.text = string(ld_valore)
	
	//riba insolute ----------------------------------------solo tra le RIBA
	setnull(ld_valore)
	
	setpointer(HourGlass!)
	
	if ll_gg_riba = -1 then
		//nessuna tolleranza per le RIBA
		ldt_oggi_toll_riba = ldt_oggi
	else
		//calcola la data dopo la quale considerare insolute le RIBA
		ldt_oggi_toll_riba = datetime(RelativeDate(date(ldt_oggi), - ll_gg_riba),00:00:00)
	end if
	
	select sum(saldo)
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where (dba.scadenza.esito_pagamento='I' or dba.scadenza.data_scadenza < :ldt_oggi_toll_riba)
		and dba.conto.codice=:ls_cod_impresa and dba.tipo_pagamento.codice in ('RIBA','RB')
	using sqlci;
	
	if isnull(ld_valore) then ld_valore = 0
	
	sle_riba_insolute_impresa.text = string(ld_valore)
	setpointer(Arrow!)
	sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)
	
	//scaduto aperto -------------------------------------tranne le RIBA
	setnull(ld_valore)
	
	setpointer(HourGlass!)
	
	select sum(saldo)
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where dba.scadenza.tipo_scadenza='A' and dba.scadenza.saldo <> 0 and dba.scadenza.data_scadenza < :ldt_oggi
		and dba.conto.codice=:ls_cod_impresa and dba.tipo_pagamento.codice not in ('RIBA','RB')
	using sqlci;
	
	if isnull(ld_valore) then ld_valore = 0
	
	sle_scaduto_aperto_impresa.text = string(ld_valore)
	setpointer(Arrow!)
	
	
	//riba a scadere ------------------------------------------solo le RIBA
	setnull(ld_valore)
	
	setpointer(HourGlass!)
	
	select sum(isnull(dare,0) - isnull(avere,0) )
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where dba.scadenza.tipo_scadenza='A' //and dba.scadenza.saldo <> 0 
		and dba.scadenza.data_scadenza >= :ldt_oggi_toll_riba
		and dba.conto.codice=:ls_cod_impresa
		and dba.tipo_pagamento.codice in ('RIBA','RB')
		and dba.scadenza.esito_pagamento<>'I'
	using sqlci;
	
	if isnull(ld_valore) then ld_valore = 0
	
	sle_riba_a_scadere_impresa.text = string(ld_valore)
	setpointer(Arrow!)
	sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)

else
	st_msg_impresa.text = "Connessione IMPRESA NON DISPONIBILE!"
end if


end subroutine

public subroutine wf_modifiche_apice ();string ls_cod_capoconto, ls_cod_cliente, ls_cod_impresa, ls_fuori_fido
dec{4}	ld_valore, ld_fido
datetime ldt_oggi, ldt_data_gg_gos
uo_impresa luo_impresa
long ll_anno, ll_gg_gos

//ldt_oggi = datetime(today(),00:00:00)
ldt_oggi = datetime(date(em_datarif.text),00:00:00)
ls_cod_cliente = dw_cliente.getitemstring(1,"cod_cliente")


//prm cliente ------------
select
	fido,
	flag_fuori_fido
into
	:ld_fido,
	:ls_fuori_fido
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura fido cliente: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	ld_fido = 0
	ls_fuori_fido = "N"
end if

//Validazione dei valori ottenuti
if isnull(ld_fido) then
	ld_fido = 0
end if

if isnull(ls_fuori_fido) then
	ls_fuori_fido = "N"
end if

sle_fido.text = string(ld_fido)
if ls_fuori_fido = "S" then
	sle_fuori_fido.text = "Si"
else
	sle_fuori_fido.text = "No"
end if
//------------------------

select
	cod_capoconto
into
	:ls_cod_capoconto
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura codice capoconto cliente: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("", "Errore in lettura codice capoconto cliente: cliente non trovato")
	return
end if

luo_impresa = create uo_impresa
ls_cod_impresa = luo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, ls_cod_cliente)
destroy luo_impresa

//##############################################################################
//Lettura valore totale delle fatture stampate in definitivo ma non ancora contabilizzate (AD_TOT_FAT_VEN)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(imponibile_iva)
into
	:ld_valore
from
	tes_fat_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente and
	cod_documento is not null and
	numeratore_documento is not null and
	anno_documento is not null and
	num_documento is not null and
	flag_contabilita = 'N' and
	data_fattura <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_fat_ven_2.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)

//Lettura valore totale delle bolle stampate in definitivo ma non ancora fatturate (AD_TOT_BOL_VEN)
setnull(ld_valore)
setpointer(HourGlass!)

//come prima ma con l'esclusione delle bolle con causale di tipo non fatturabile
select
	sum(tes_bol_ven.imponibile_iva)
into
	:ld_valore
from
	tes_bol_ven
join tab_causali_trasp on tab_causali_trasp.cod_azienda=tes_bol_ven.cod_azienda
	and tab_causali_trasp.cod_causale=tes_bol_ven.cod_causale
where
	tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_bol_ven.cod_cliente = :ls_cod_cliente
	and tes_bol_ven.cod_documento is not null and tes_bol_ven.numeratore_documento is not null
	and tes_bol_ven.anno_documento is not null and tes_bol_ven.num_documento is not null
	and tes_bol_ven.flag_gen_fat = 'N' and tes_bol_ven.data_bolla <= :ldt_oggi and tab_causali_trasp.flag_segue_fattura='S';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale bolle di vendita confermate non contabilizzate: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if

sle_bol_ven_2.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)


//Lettura valore totale degli ordini assegnati (AD_TOT_ORD_VEN_ASS)
setnull(ld_valore)
setpointer(HourGlass!)

select numero
into :ll_gg_gos
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'GOS' and flag_parametro = 'N';
	
if sqlca.sqlcode = 0 then
else
	ll_gg_gos = -1
end if

//come prima ma con l'esclusione degli ordini bloccati
if ll_gg_gos = -1 then
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * det_ord_ven.quan_in_evasione)
	into
		:ld_valore
	from
		tes_ord_ven,
		det_ord_ven
	where
		det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and
		det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and
		det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :ls_cod_cliente and
		tes_ord_ven.data_registrazione <= :ldt_oggi and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <>'S';
else
	ldt_data_gg_gos = datetime(RelativeDate(date(ldt_oggi), -ll_gg_gos), 00:00:00)
	
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * det_ord_ven.quan_in_evasione)
	into
		:ld_valore
	from
		tes_ord_ven,
		det_ord_ven
	where
		det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and
		det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and
		det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :ls_cod_cliente and
		tes_ord_ven.data_registrazione <= :ldt_oggi and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <>'S'
		and tes_ord_ven.data_registrazione >= :ldt_data_gg_gos;
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita assegnati: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_ord_ven_ass_2.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)

//Lettura valore totale degli ordini in spedizione (bolle ancora non stampate in definitivo) (AD_TOT_ORD_VEN_SPE)
setnull(ld_valore)
setpointer(HourGlass!)

select
	sum(imponibile_iva)
into
	:ld_valore
from
	tes_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente and
	data_bolla is null and
	flag_gen_fat = 'N' and
	data_registrazione <= :ldt_oggi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita in spedizione: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if
sle_ord_ven_in_sped_2.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)

//Lettura valore totale degli ordini aperti e parziali (AD_TOT_ORD_VEN_A_P)
setnull(ld_valore)
setpointer(HourGlass!)

//come prima ma con l'esclusione degli ordini bloccati
if ll_gg_gos = -1 then
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * (det_ord_ven.quan_ordine - det_ord_ven.quan_evasa - det_ord_ven.quan_in_evasione))
	into
		:ld_valore
	from
		tes_ord_ven,
		det_ord_ven
	where
		det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and
		det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and
		det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :ls_cod_cliente and
		tes_ord_ven.data_registrazione <= :ldt_oggi and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <>'S';
else		
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * (det_ord_ven.quan_ordine - det_ord_ven.quan_evasa - det_ord_ven.quan_in_evasione))
	into
		:ld_valore
	from
		tes_ord_ven,
		det_ord_ven
	where
		det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and
		det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and
		det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione and
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :ls_cod_cliente and
		tes_ord_ven.data_registrazione <= :ldt_oggi and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <>'S'
		and tes_ord_ven.data_registrazione >= :ldt_data_gg_gos;
end if

if sqlca.sqlcode < 0 then
	g_mb.messagebox("", "Errore in lettura totale ordini di vendita aperti e parziali: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 or isnull(ld_valore) then
	ld_valore = 0
end if

sle_ord_ven_a_p_2.text = string(ld_valore)
setpointer(Arrow!)
sle_esposizione_2.text = string(dec(sle_esposizione_2.text) + ld_valore)



end subroutine

public subroutine wf_modifiche_esposizione ();
end subroutine

on w_test_fidi_clienti.create
int iCurrent
call super::create
this.st_msg_impresa=create st_msg_impresa
this.st_25=create st_25
this.sle_esposizione_2=create sle_esposizione_2
this.em_datarif=create em_datarif
this.dw_cliente=create dw_cliente
this.st_21=create st_21
this.st_24=create st_24
this.st_23=create st_23
this.st_22=create st_22
this.st_20=create st_20
this.r_2=create r_2
this.sle_ord_ven_a_p_2=create sle_ord_ven_a_p_2
this.sle_ord_ven_in_sped_2=create sle_ord_ven_in_sped_2
this.sle_ord_ven_ass_2=create sle_ord_ven_ass_2
this.sle_bol_ven_2=create sle_bol_ven_2
this.sle_fat_ven_2=create sle_fat_ven_2
this.st_19=create st_19
this.st_18=create st_18
this.st_17=create st_17
this.st_16=create st_16
this.st_15=create st_15
this.sle_esposizione=create sle_esposizione
this.st_esposizione=create st_esposizione
this.sle_fuori_fido=create sle_fuori_fido
this.st_14=create st_14
this.sle_fido=create sle_fido
this.st_13=create st_13
this.st_12=create st_12
this.sle_saldo_cont_impresa=create sle_saldo_cont_impresa
this.sle_fat_ven=create sle_fat_ven
this.sle_ord_ven_a_p=create sle_ord_ven_a_p
this.sle_ord_ven_in_sped=create sle_ord_ven_in_sped
this.sle_ord_ven_ass=create sle_ord_ven_ass
this.sle_bol_ven=create sle_bol_ven
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.cb_chiudi=create cb_chiudi
this.cb_calcola=create cb_calcola
this.sle_scad_aperte_impresa=create sle_scad_aperte_impresa
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.sle_riba_insolute_impresa=create sle_riba_insolute_impresa
this.sle_scaduto_aperto_impresa=create sle_scaduto_aperto_impresa
this.sle_riba_a_scadere_impresa=create sle_riba_a_scadere_impresa
this.r_1=create r_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_msg_impresa
this.Control[iCurrent+2]=this.st_25
this.Control[iCurrent+3]=this.sle_esposizione_2
this.Control[iCurrent+4]=this.em_datarif
this.Control[iCurrent+5]=this.dw_cliente
this.Control[iCurrent+6]=this.st_21
this.Control[iCurrent+7]=this.st_24
this.Control[iCurrent+8]=this.st_23
this.Control[iCurrent+9]=this.st_22
this.Control[iCurrent+10]=this.st_20
this.Control[iCurrent+11]=this.r_2
this.Control[iCurrent+12]=this.sle_ord_ven_a_p_2
this.Control[iCurrent+13]=this.sle_ord_ven_in_sped_2
this.Control[iCurrent+14]=this.sle_ord_ven_ass_2
this.Control[iCurrent+15]=this.sle_bol_ven_2
this.Control[iCurrent+16]=this.sle_fat_ven_2
this.Control[iCurrent+17]=this.st_19
this.Control[iCurrent+18]=this.st_18
this.Control[iCurrent+19]=this.st_17
this.Control[iCurrent+20]=this.st_16
this.Control[iCurrent+21]=this.st_15
this.Control[iCurrent+22]=this.sle_esposizione
this.Control[iCurrent+23]=this.st_esposizione
this.Control[iCurrent+24]=this.sle_fuori_fido
this.Control[iCurrent+25]=this.st_14
this.Control[iCurrent+26]=this.sle_fido
this.Control[iCurrent+27]=this.st_13
this.Control[iCurrent+28]=this.st_12
this.Control[iCurrent+29]=this.sle_saldo_cont_impresa
this.Control[iCurrent+30]=this.sle_fat_ven
this.Control[iCurrent+31]=this.sle_ord_ven_a_p
this.Control[iCurrent+32]=this.sle_ord_ven_in_sped
this.Control[iCurrent+33]=this.sle_ord_ven_ass
this.Control[iCurrent+34]=this.sle_bol_ven
this.Control[iCurrent+35]=this.st_11
this.Control[iCurrent+36]=this.st_10
this.Control[iCurrent+37]=this.st_9
this.Control[iCurrent+38]=this.st_8
this.Control[iCurrent+39]=this.st_7
this.Control[iCurrent+40]=this.st_6
this.Control[iCurrent+41]=this.st_5
this.Control[iCurrent+42]=this.cb_chiudi
this.Control[iCurrent+43]=this.cb_calcola
this.Control[iCurrent+44]=this.sle_scad_aperte_impresa
this.Control[iCurrent+45]=this.st_1
this.Control[iCurrent+46]=this.st_2
this.Control[iCurrent+47]=this.st_3
this.Control[iCurrent+48]=this.st_4
this.Control[iCurrent+49]=this.sle_riba_insolute_impresa
this.Control[iCurrent+50]=this.sle_scaduto_aperto_impresa
this.Control[iCurrent+51]=this.sle_riba_a_scadere_impresa
this.Control[iCurrent+52]=this.r_1
end on

on w_test_fidi_clienti.destroy
call super::destroy
destroy(this.st_msg_impresa)
destroy(this.st_25)
destroy(this.sle_esposizione_2)
destroy(this.em_datarif)
destroy(this.dw_cliente)
destroy(this.st_21)
destroy(this.st_24)
destroy(this.st_23)
destroy(this.st_22)
destroy(this.st_20)
destroy(this.r_2)
destroy(this.sle_ord_ven_a_p_2)
destroy(this.sle_ord_ven_in_sped_2)
destroy(this.sle_ord_ven_ass_2)
destroy(this.sle_bol_ven_2)
destroy(this.sle_fat_ven_2)
destroy(this.st_19)
destroy(this.st_18)
destroy(this.st_17)
destroy(this.st_16)
destroy(this.st_15)
destroy(this.sle_esposizione)
destroy(this.st_esposizione)
destroy(this.sle_fuori_fido)
destroy(this.st_14)
destroy(this.sle_fido)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.sle_saldo_cont_impresa)
destroy(this.sle_fat_ven)
destroy(this.sle_ord_ven_a_p)
destroy(this.sle_ord_ven_in_sped)
destroy(this.sle_ord_ven_ass)
destroy(this.sle_bol_ven)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.cb_chiudi)
destroy(this.cb_calcola)
destroy(this.sle_scad_aperte_impresa)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.sle_riba_insolute_impresa)
destroy(this.sle_scaduto_aperto_impresa)
destroy(this.sle_riba_a_scadere_impresa)
destroy(this.r_1)
end on

event open;call super::open;em_datarif.text = string(date(today()))
end event

type st_msg_impresa from statictext within w_test_fidi_clienti
integer x = 1669
integer y = 240
integer width = 1577
integer height = 200
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_25 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 1840
integer width = 805
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Esposizione Cliente:"
boolean focusrectangle = false
end type

type sle_esposizione_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 1820
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type em_datarif from editmask within w_test_fidi_clienti
integer x = 503
integer y = 40
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
end type

type dw_cliente from u_dw_search within w_test_fidi_clienti
integer x = 937
integer y = 20
integer width = 2080
integer height = 120
integer taborder = 30
string dataobject = "d_test_fidi_clienti"
boolean border = false
end type

event clicked;call super::clicked;//choose case dwo.name
//		
//	case "b_cliente"
//		
//		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
//		s_cs_xx.parametri.parametro_uo_dw_search = dw_tes_ord_ven_ricerca
//		s_cs_xx.parametri.parametro_s_1 = "cod_cliente"		
//		if not isvalid(w_clienti_ricerca) then
//			window_open(w_clienti_ricerca, 0)
//		end if
//		w_clienti_ricerca.show()		
//		
//end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_cliente,"cod_cliente")
end choose
end event

type st_21 from statictext within w_test_fidi_clienti
integer x = 1189
integer y = 1420
integer width = 402
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
string text = "come prima"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_24 from statictext within w_test_fidi_clienti
integer x = 526
integer y = 1640
integer width = 1065
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 65535
long backcolor = 12632256
string text = "come prima ma senza quelli bloccati"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_23 from statictext within w_test_fidi_clienti
integer x = 526
integer y = 1220
integer width = 1065
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 65535
long backcolor = 12632256
string text = "come prima ma senza quelli bloccati"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_22 from statictext within w_test_fidi_clienti
integer x = 526
integer y = 1020
integer width = 1065
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 65535
long backcolor = 12632256
string text = "come prima ma senza le causali non fatturabili"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_20 from statictext within w_test_fidi_clienti
integer x = 1189
integer y = 820
integer width = 402
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
string text = "come prima"
alignment alignment = right!
boolean focusrectangle = false
end type

type r_2 from rectangle within w_test_fidi_clienti
integer linethickness = 4
long fillcolor = 12632256
integer x = 1646
integer y = 220
integer width = 1646
integer height = 1960
end type

type sle_ord_ven_a_p_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 1700
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ord_ven_in_sped_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 1480
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ord_ven_ass_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 1280
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_bol_ven_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 1080
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_fat_ven_2 from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 880
integer width = 663
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_19 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 1720
integer width = 910
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot. Ord. Ven. aperti/parz. in APICE:"
boolean focusrectangle = false
end type

type st_18 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 1520
integer width = 818
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot. Ord. Ven. in sped. in APICE:"
boolean focusrectangle = false
end type

type st_17 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 1320
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot. Ord. Ven. ass. in APICE:"
boolean focusrectangle = false
end type

type st_16 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 1120
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot. Bol. Ven. in APICE:"
boolean focusrectangle = false
end type

type st_15 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 920
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Tot. Fat. Ven in APICE:"
boolean focusrectangle = false
end type

type sle_esposizione from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1820
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_esposizione from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1840
integer width = 805
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Esposizione Cliente:"
boolean focusrectangle = false
end type

type sle_fuori_fido from singlelineedit within w_test_fidi_clienti
integer x = 2971
integer y = 1940
integer width = 128
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_14 from statictext within w_test_fidi_clienti
integer x = 2697
integer y = 1960
integer width = 274
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Fuori Fido:"
boolean focusrectangle = false
end type

type sle_fido from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1940
integer width = 663
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_13 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1960
integer width = 183
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Fido:"
boolean focusrectangle = false
end type

type st_12 from statictext within w_test_fidi_clienti
integer x = 1646
integer y = 140
integer width = 503
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Stato dell~'arte"
boolean focusrectangle = false
end type

type sle_saldo_cont_impresa from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 480
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_fat_ven from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 880
integer width = 663
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ord_ven_a_p from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1700
integer width = 663
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ord_ven_in_sped from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1480
integer width = 663
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_ord_ven_ass from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1280
integer width = 663
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_bol_ven from singlelineedit within w_test_fidi_clienti
integer x = 1669
integer y = 1080
integer width = 663
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_11 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1720
integer width = 910
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tot. Ord. Ven. aperti/parz. in APICE:"
boolean focusrectangle = false
end type

type st_10 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1500
integer width = 818
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tot. Ord. Ven. in sped. in APICE:"
boolean focusrectangle = false
end type

type st_9 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1300
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tot. Ord. Ven. ass. in APICE:"
boolean focusrectangle = false
end type

type st_8 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 1100
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tot. Bol. Ven. in APICE:"
boolean focusrectangle = false
end type

type st_7 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 900
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tot. Fat. Ven in APICE:"
boolean focusrectangle = false
end type

type st_6 from statictext within w_test_fidi_clienti
integer x = 2354
integer y = 500
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Saldo Contabile in IMPRESA:"
boolean focusrectangle = false
end type

type st_5 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 140
integer width = 585
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "MODIFICHE"
boolean focusrectangle = false
end type

type cb_chiudi from commandbutton within w_test_fidi_clienti
integer y = 20
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "chiudi"
boolean cancel = true
end type

event clicked;close(parent)
end event

type cb_calcola from commandbutton within w_test_fidi_clienti
integer x = 3017
integer y = 40
integer width = 320
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "GO"
end type

event clicked;wf_stato_dellarte()

wf_modifiche_impresa()

wf_modifiche_apice()

//wf_modifiche_esposizione()

end event

type sle_scad_aperte_impresa from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 260
integer width = 663
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 260
integer width = 846
integer height = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 16711680
long backcolor = 12632256
string text = "Scadenze aperte in IMPRESA      (no RIBA):"
boolean focusrectangle = false
end type

type st_2 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 560
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 16711680
long backcolor = 12632256
string text = "RIBA insolute in IMPRESA:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 400
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 8388608
long backcolor = 12632256
string text = "di cui Scaduto aperto"
boolean focusrectangle = false
end type

type st_4 from statictext within w_test_fidi_clienti
integer x = 23
integer y = 680
integer width = 777
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 16711680
long backcolor = 12632256
string text = "RIBA a scadere in IMPRESA:"
boolean focusrectangle = false
end type

type sle_riba_insolute_impresa from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 540
integer width = 663
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_scaduto_aperto_impresa from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 380
integer width = 663
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_riba_a_scadere_impresa from singlelineedit within w_test_fidi_clienti
integer x = 937
integer y = 660
integer width = 663
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type r_1 from rectangle within w_test_fidi_clienti
integer linethickness = 4
long fillcolor = 12632256
integer y = 220
integer width = 1623
integer height = 1960
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_cliente
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"


end event

