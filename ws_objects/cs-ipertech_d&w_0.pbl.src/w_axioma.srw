﻿$PBExportHeader$w_axioma.srw
forward
global type w_axioma from w_cs_xx_principale
end type
type cbx_debug from checkbox within w_axioma
end type
type cb_reset from commandbutton within w_axioma
end type
type dw_log from datawindow within w_axioma
end type
type cb_1 from commandbutton within w_axioma
end type
type st_temp_folder from statictext within w_axioma
end type
type em_1 from editmask within w_axioma
end type
type st_1 from statictext within w_axioma
end type
type cb_connect from commandbutton within w_axioma
end type
type mle_xml from multilineedit within w_axioma
end type
type dw_det from uo_std_dw within w_axioma
end type
type dw_lista from uo_std_dw within w_axioma
end type
end forward

global type w_axioma from w_cs_xx_principale
integer width = 4005
integer height = 3072
string title = "Axioma"
boolean center = true
cbx_debug cbx_debug
cb_reset cb_reset
dw_log dw_log
cb_1 cb_1
st_temp_folder st_temp_folder
em_1 em_1
st_1 st_1
cb_connect cb_connect
mle_xml mle_xml
dw_det dw_det
dw_lista dw_lista
end type
global w_axioma w_axioma

type variables
private:
	boolean ib_connesso = false
	transaction it_axioma
	
	string is_user_folder
end variables

forward prototypes
public function boolean wf_salva_xml (long al_row)
public function integer wf_leggi_xml (string as_filename)
public subroutine wf_parse_xml (string as_filename)
end prototypes

public function boolean wf_salva_xml (long al_row);string ls_xml, ls_7zip, ls_zip_file, ls_file
long ll_xml_id, ll_xml_riga_id
blob lb_blob

try
	
	 ll_xml_id = dw_lista.getitemnumber(al_row, "ax_exp_apice_xml_id")
	 ll_xml_riga_id = 0
	 
	 selectblob xml_file
	 into :lb_blob
	 from TB$XMLDB
	 where
		xmlf_id = :ll_xml_id and
		xmlf_riga = :ll_xml_riga_id
	using it_axioma;
		
	if it_axioma.sqlcode = 100 then
		g_mb.error("Record non trovato.", it_axioma)
		return false
	end if
	
	if it_axioma.sqlcode <> 0 then
		g_mb.error("Errore durante il reucpero del file blob dal database AXIOMA", it_axioma)
		g_mb.error("Errore durante il reucpero del file blob dal database AXIOMA " + it_axioma.sqlerrtext, it_axioma)
		return false
	end if
	
	ls_file = is_user_folder + string(ll_xml_id)
	ls_zip_file = ls_file + ".zip"
	
	guo_functions.uof_blob_to_file(lb_blob, ls_zip_file)
	
	ls_7zip = s_cs_xx.volume + s_cs_xx.risorse + '7z\7za.exe e -y "' + ls_zip_file + '" -o"' + is_user_folder + '"'
	
	run(ls_7zip, Minimized!)
	
	// aspetto 1 secondo per attendere che lo zip estragga il file
	sleep(1)
	
	wf_leggi_xml(ls_file)
	wf_parse_xml(ls_file)
	
catch(runtimeerror ex)
	g_mb.error(ex.getmessage())
finally
	
	if fileexists(ls_file) then filedelete(ls_file)
	if fileexists(ls_zip_file) then filedelete(ls_zip_file)

end try


return true
end function

public function integer wf_leggi_xml (string as_filename);string ls_xml,ls_buffer
long ll_handle, ll_buffer

ll_handle = fileopen(as_filename, LineMode!)
if ll_handle < 0 then
	messagebox("err", "Errore in OPEN del file " + as_filename)
	return -1
end if

ls_xml = ""
do
	ll_buffer = fileread(ll_handle, ls_buffer)
	ls_xml += ls_buffer + "~r~n"
loop while ll_buffer > 0


fileclose(ll_handle)

mle_xml.text =  ls_xml
end function

public subroutine wf_parse_xml (string as_filename);string ls_test, ls_COD_STRUTTURA, ls_COD_COLORE_STRUTTURA, ls_nome, ls_des_prodotto,ls_cod_misura_mag
int li_i, li_j, li_row
boolean lb_test
PBDOM_BUILDER lpbdom_builder
PBDOM_DOCUMENT lxml_document
PBDOM_ELEMENT lxml_elements[], lxml_root, lxml_oggetto[], ls_empty[]

try
	lpbdom_builder = create PBDOM_BUILDER
	lxml_document = lpbdom_builder.buildfromfile(as_filename)
	catch (runtimeerror ex)
	g_mb.error(ex.getmessage())
end try

// leggo nodi root
if not lxml_document.GetContent(lxml_elements) then
	messagebox("errore","Errore in acquisizione nodi file XML.")
	return
end if

// nodo root CONFIGURAZIONE
lxml_root = lxml_elements[3]

lxml_root.getchildelements("OGGETTO", lxml_oggetto)

for li_i = 1 to upperbound(lxml_oggetto)
	
	ls_nome = lxml_oggetto[li_i].getattribute("NOME").gettext()
	
	if ls_nome <> "COMPONENTE" then continue
	
	lxml_elements = ls_empty
	lxml_oggetto[li_i].getchildelements("ATTRIBUTO", lxml_elements)
	
	if li_i > 1 then li_row = dw_det.insertrow(0)
	
	for li_j = 1 to upperbound(lxml_elements)
		
		setnull(ls_test)
		
		ls_nome = lxml_elements[li_j].getattribute("NOME").gettext()
		
		ls_test    = lxml_elements[li_j].getattribute("VALORE").gettext()
		
		if li_i = 1 then
			
			if  ls_nome = "COD_STRUTTURA" then
				ls_COD_STRUTTURA = ls_test
			elseif ls_nome = "APC_COD_COLORE_STRUTTURA" then
				ls_COD_COLORE_STRUTTURA = ls_test
			end if
			
		else
			
			choose case ls_nome
				case "APC_CODICE_MATERIALE"
					dw_det.setitem(li_row, "APC_CODICE_MATERIALE", ls_test)
					
					select des_prodotto, cod_misura_mag
					into :ls_des_prodotto, :ls_cod_misura_mag
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_test;
					choose case sqlca.sqlcode
						case 0
							dw_det.setitem(li_row, "apc_des_prodotto", ls_des_prodotto)
							dw_det.setitem(li_row, "apc_um",ls_cod_misura_mag)
						case 100
							dw_det.setitem(li_row, "apc_des_prodotto", "Prodotto inesistente in anagrafica")
						case else
							dw_det.setitem(li_row, "apc_des_prodotto", "Errore:" + sqlca.sqlerrtext)
					end choose
					
				case "APC_QUANTITA"
					dw_det.setitem(li_row, "APC_QUANTITA",  lxml_elements[li_j].getattribute("VALORE").getdoublevalue())
					
				case "APC_GRUPPO_VARIANTE"
					dw_det.setitem(li_row, "APC_GRUPPO_VARIANTE", ls_test)
					
				case "APC_SPECIFICA_1"
					dw_det.setitem(li_row, "APC_SPECIFICA_1", ls_test)
					
				case "APC_SPECIFICA_2"
					dw_det.setitem(li_row, "APC_SPECIFICA_2", ls_test)
					
				case "APC_SPECIFICA_3"
					dw_det.setitem(li_row, "APC_SPECIFICA_3", ls_test)

				case "ARTS5"
					dw_det.setitem(li_row, "ARTS5", ls_test)
					
			end choose
			
		end if
		
	next
	
next

dw_det.setsort("apc_gruppo_variante, apc_specifica_1")
dw_det.sort()
end subroutine

on w_axioma.create
int iCurrent
call super::create
this.cbx_debug=create cbx_debug
this.cb_reset=create cb_reset
this.dw_log=create dw_log
this.cb_1=create cb_1
this.st_temp_folder=create st_temp_folder
this.em_1=create em_1
this.st_1=create st_1
this.cb_connect=create cb_connect
this.mle_xml=create mle_xml
this.dw_det=create dw_det
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_debug
this.Control[iCurrent+2]=this.cb_reset
this.Control[iCurrent+3]=this.dw_log
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.st_temp_folder
this.Control[iCurrent+6]=this.em_1
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.cb_connect
this.Control[iCurrent+9]=this.mle_xml
this.Control[iCurrent+10]=this.dw_det
this.Control[iCurrent+11]=this.dw_lista
end on

on w_axioma.destroy
call super::destroy
destroy(this.cbx_debug)
destroy(this.cb_reset)
destroy(this.dw_log)
destroy(this.cb_1)
destroy(this.st_temp_folder)
destroy(this.em_1)
destroy(this.st_1)
destroy(this.cb_connect)
destroy(this.mle_xml)
destroy(this.dw_det)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;is_user_folder = guo_functions.uof_get_user_temp_folder()
st_temp_folder.text = is_user_folder
end event

event close;call super::close;// stefanop: 12/09/2014: aggiunto su segnalazione di Alessandro, le connessioni rimangono appese
if ib_connesso then
	disconnect using it_axioma;
end if
end event

type cbx_debug from checkbox within w_axioma
integer x = 517
integer y = 2100
integer width = 837
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Elaborazione di Debug"
boolean checked = true
end type

type cb_reset from commandbutton within w_axioma
integer x = 3474
integer y = 2088
integer width = 462
integer height = 100
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Reset"
end type

event clicked;dw_det.reset()
mle_xml.text=""
dw_log.reset()
end event

type dw_log from datawindow within w_axioma
integer x = 23
integer y = 2204
integer width = 3913
integer height = 728
integer taborder = 40
string title = "none"
string dataobject = "d_axioma_log_import"
boolean border = false
end type

type cb_1 from commandbutton within w_axioma
integer x = 23
integer y = 2088
integer width = 462
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Applica Varianti"
end type

event clicked;string ls_cod_modello, ls_errore, ls_cod_versione_predefinita, ls_path_file
long ll_riga, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, ll_testid, ll_previd
uo_axioma luo_axioma
uo_funzioni_1 luo_funzioni_1
uo_produzione luo_produzione


ll_riga = dw_lista.getrow()

ll_anno_registrazione = dw_lista.getitemnumber(ll_riga, "ax_exp_apice_anno_reg_ord_ven")
ll_num_registrazione = dw_lista.getitemnumber(ll_riga, "ax_exp_apice_num_reg_ord_ven")
ll_prog_riga_ord_ven = dw_lista.getitemnumber(ll_riga, "ax_exp_apice_prog_riga_ord_ven")
ls_path_file 				= dw_lista.getitemstring(ll_riga, "ax_exp_apice_path_pdf")
ll_previd = dw_lista.getitemnumber(ll_riga, "ax_exp_apice_previd")
ll_testid = dw_lista.getitemnumber(ll_riga, "ax_exp_apice_testid")

luo_axioma=create uo_axioma

luo_axioma.uof_get_comp_det_tostring("COD_MODELLO", dw_det, ls_cod_modello)

if ll_prog_riga_ord_ven > 0 and not isnull(ll_prog_riga_ord_ven) then
	
	luo_produzione = create uo_produzione
	
	ll_ret = luo_produzione.uof_elimina_riga_ordine( 	ll_anno_registrazione, &
																	ll_num_registrazione, &
																	ll_prog_riga_ord_ven, &
																	ref ls_errore)
	
	if ll_ret = 0 then
		destroy luo_produzione
		if len(ls_errore) > 0 then
			// ci sono avvisi
			g_mb.warning(ls_errore)
		end if
	else
		rollback;
		g_mb.error(ls_errore)
		destroy luo_produzione
		return -1
	end if
	ll_prog_riga_ord_ven = 0

end if

ll_ret = luo_axioma.uof_importa_riga_documento( 	ll_anno_registrazione, &
														ll_num_registrazione, &
														ref ll_prog_riga_ord_ven, &
														ls_cod_modello, &
														ls_path_file, &
														ref dw_det,&
														"ORD_VEN")

if ll_ret = -1 then
	rollback using it_axioma;
else
	// consolido quanto fatto.
	if not cbx_debug.checked then
		update ax_exp_apice
		set 	prog_riga_ord_ven = :ll_prog_riga_ord_ven,
				flag_elaborato_apice = 'S'
		where testid = :ll_testid  and previd= :ll_previd
		using it_axioma;
		
		if it_axioma.sqlcode < 0 then
			g_mb.messagebox("Import Axioma","Errore in aggiornamento TEST-ID e PREV-ID in Axioma.~r~n" + it_axioma.sqlerrtext)
			rollback using it_axioma;
		else
			commit using it_axioma;
		end if
	end if
end if

dw_log.reset()
dw_log.insertrow(0)
dw_log.setitem(dw_log.getrow(),"log", luo_axioma.is_log_errori)

destroy luo_axioma
end event

type st_temp_folder from statictext within w_axioma
integer x = 933
integer y = 28
integer width = 2999
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial Narrow"
long textcolor = 33554432
long backcolor = 12632256
string text = "Path Cartella Temporanea"
boolean focusrectangle = false
end type

type em_1 from editmask within w_axioma
integer x = 690
integer y = 32
integer width = 183
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "11"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##"
end type

type st_1 from statictext within w_axioma
integer x = 411
integer y = 40
integer width = 265
integer height = 72
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Profilo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_connect from commandbutton within w_axioma
integer x = 23
integer y = 20
integer width = 343
integer height = 104
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Connetti"
end type

event clicked;string ls_errore


ib_connesso = guo_functions.uof_crea_transazione("database_" + string(em_1.text), it_axioma, ls_errore)
if not ib_connesso then
	
	g_mb.error("Errore durante la connessione con il database di AXIOMA.~r~n" + ls_errore)
	return
	
end if


dw_lista.settransobject(it_axioma)
dw_lista.retrieve()
end event

type mle_xml from multilineedit within w_axioma
integer x = 2761
integer y = 140
integer width = 1170
integer height = 720
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_det from uo_std_dw within w_axioma
integer x = 23
integer y = 880
integer width = 3909
integer height = 1180
integer taborder = 20
string dataobject = "d_axioma_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean ib_colora = false
end type

type dw_lista from uo_std_dw within w_axioma
integer x = 23
integer y = 140
integer width = 2715
integer height = 728
integer taborder = 10
string dataobject = "d_axioma_lista_exp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean ib_colora = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_carica"
		
		cb_reset.triggerevent("clicked")
		
		wf_salva_xml(row)
		
end choose
end event

