﻿$PBExportHeader$w_canc_mov_mag_commesse.srw
forward
global type w_canc_mov_mag_commesse from window
end type
type dw_1 from datawindow within w_canc_mov_mag_commesse
end type
type st_3 from statictext within w_canc_mov_mag_commesse
end type
type cb_2 from commandbutton within w_canc_mov_mag_commesse
end type
type cb_1 from commandbutton within w_canc_mov_mag_commesse
end type
type st_2 from statictext within w_canc_mov_mag_commesse
end type
type st_1 from statictext within w_canc_mov_mag_commesse
end type
end forward

global type w_canc_mov_mag_commesse from window
integer width = 3584
integer height = 2004
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 8421376
string icon = "AppIcon!"
boolean clientedge = true
boolean center = true
dw_1 dw_1
st_3 st_3
cb_2 cb_2
cb_1 cb_1
st_2 st_2
st_1 st_1
end type
global w_canc_mov_mag_commesse w_canc_mov_mag_commesse

on w_canc_mov_mag_commesse.create
this.dw_1=create dw_1
this.st_3=create st_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.dw_1,&
this.st_3,&
this.cb_2,&
this.cb_1,&
this.st_2,&
this.st_1}
end on

on w_canc_mov_mag_commesse.destroy
destroy(this.dw_1)
destroy(this.st_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.st_1)
end on

type dw_1 from datawindow within w_canc_mov_mag_commesse
integer x = 27
integer y = 704
integer width = 3474
integer height = 1008
integer taborder = 20
string title = "none"
string dataobject = "d_canc_mov_mag_commesse"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_canc_mov_mag_commesse
integer x = 736
integer y = 1744
integer width = 2766
integer height = 112
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 8421376
boolean border = true
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_canc_mov_mag_commesse
integer x = 14
integer y = 1728
integer width = 713
integer height = 140
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "2 - Esegui"
end type

event clicked;string 	ls_errore, ls_str
long 		ll_file, ll_ret, ll_rows, ll_i, ll_anno_commessa, ll_num_commessa, ll_anno_registrazione, &
			ll_num_registrazione, ll_prog_riga_ord_ven
uo_storicizzazione luo_storicizzazione


ll_rows = dw_1.rowcount()


if messagebox("UTILITY", "Proseguo cancellando le commesse?",question!,yesno!,2) = 2 then return


ll_file = fileopen("c:\cancella_commesse.log", linemode!, write!, LockWrite!, Replace! )

filewrite(ll_file, " *********  INIZIO ELABORAZIONE ************* " + string(today()))


luo_storicizzazione = CREATE uo_storicizzazione

for ll_i = 1 to ll_rows
	
	ll_anno_commessa = dw_1.getitemnumber(ll_i, "anno_commessa")
	ll_num_commessa = dw_1.getitemnumber(ll_i, "num_commessa")
	ll_anno_registrazione = dw_1.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_1.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = dw_1.getitemnumber(ll_i, "prog_riga_ord_ven")
	
	ls_str = "ORDINE~t" + &
				string(ll_anno_registrazione) + "~t" + &
				string(ll_num_registrazione) + "~t" + &
				string(ll_prog_riga_ord_ven) + "~t" + &
				"COMMESSA" + "~t" + &
				string(ll_prog_riga_ord_ven) + "~t" + &
				string(ll_prog_riga_ord_ven) + "~t" + &
				string(ll_prog_riga_ord_ven)
	
	st_3.text = ls_str
	
	Yield()
	
	ll_ret = luo_storicizzazione.uof_elimina_mov_mag_commesse(ll_anno_commessa, ll_num_commessa, ls_errore)
	
	if ll_ret < 0 then
		
			ls_str += "~tERRORE NELLA FUNZIONE uof_elimina_mov_mag_commesse~t" + ls_errore
			
			filewrite(ll_file, ls_str)
		
			rollback;
			
			continue
	
	else
		
		// CANCELLO RIFERIMENTO ALLA COMMESSA NELLA RIGA ORDINE
		
		update det_ord_ven
		set 	 anno_commessa = null, 
			 	 num_commessa = null,
			 	 flag_gen_commessa = 'N'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				
		if sqlca.sqlcode < 0 then
			
			ls_str += "~tERRORE DURANTE UPDATE DET_ORD_VEN~t" + sqlca.sqlerrtext
			
			filewrite(ll_file, ls_str)

			rollback;
			
			continue 
			
		end if
		
		
		// CANCELLO RIFERIMENTO ALLA COMMESSA NELLA BOLLA
		
		update det_bol_ven
		set 	 anno_commessa = null, 
			 	 num_commessa = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa;
				
		if sqlca.sqlcode < 0 then
			
			ls_str += "~tERRORE DURANTE UPDATE DET_BOL_VEN~t" + sqlca.sqlerrtext
			
			filewrite(ll_file, ls_str)

			rollback;
			
			continue 
			
		end if
		
		
		
		// CANCELLO RIFERIMENTO ALLA COMMESSA NELLA FATTURA
		
		update det_fat_ven
		set 	 anno_commessa = null, 
			 	 num_commessa = null
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa;
				
		if sqlca.sqlcode < 0 then
			
			ls_str += "~tERRORE DURANTE UPDATE DET_FAT_VEN~t" + sqlca.sqlerrtext
			
			filewrite(ll_file, ls_str)

			rollback;
			
			continue 
			
		end if
		
		// cancello effettivamente la commessa 
		
		delete anag_commesse
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa = :ll_num_commessa ;
		
		if sqlca.sqlcode < 0 then
			
			ls_str += "~tERRORE DURANTE DELETE ANAG_COMMESSE~t" + sqlca.sqlerrtext
			
			filewrite(ll_file, ls_str)

			rollback;
			
			continue 
			
		end if
		
		
	end if
	
	ls_str += "~tCANCELLATO TUTTO"

	filewrite(ll_file, ls_str)
	
	commit;

	
next

filewrite(ll_file, " *********  FINE ELABORAZIONE ************* " + string(today()))

fileclose(ll_file)
end event

type cb_1 from commandbutton within w_canc_mov_mag_commesse
integer x = 23
integer y = 548
integer width = 713
integer height = 140
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "1 - Carica Dati"
end type

event clicked;dw_1.settransobject(sqlca)
dw_1.retrieve()
end event

type st_2 from statictext within w_canc_mov_mag_commesse
integer x = 27
integer y = 236
integer width = 3397
integer height = 224
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 8421376
string text = "questa utilità è stata creata per cancellare commesse già chiuse, e relativi movimenti di magazzino."
boolean focusrectangle = false
end type

type st_1 from statictext within w_canc_mov_mag_commesse
integer x = 41
integer y = 44
integer width = 2921
integer height = 140
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 8421376
string text = "UTILITA~' CANCELLAZIONE MOVIMENTI COMMESSE."
boolean focusrectangle = false
end type

