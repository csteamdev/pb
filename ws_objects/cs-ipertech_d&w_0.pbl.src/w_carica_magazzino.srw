﻿$PBExportHeader$w_carica_magazzino.srw
$PBExportComments$Procedira Personalizzata di carico magazzino a q.ta definita.
forward
global type w_carica_magazzino from window
end type
type cb_log from commandbutton within w_carica_magazzino
end type
type st_8 from statictext within w_carica_magazzino
end type
type cb_2 from commandbutton within w_carica_magazzino
end type
type st_7 from statictext within w_carica_magazzino
end type
type em_cod_prodotto from editmask within w_carica_magazzino
end type
type st_5 from statictext within w_carica_magazzino
end type
type st_6 from statictext within w_carica_magazzino
end type
type st_record from statictext within w_carica_magazzino
end type
type st_4 from statictext within w_carica_magazzino
end type
type cb_1 from commandbutton within w_carica_magazzino
end type
type em_cod_tipo_movimento from editmask within w_carica_magazzino
end type
type st_3 from statictext within w_carica_magazzino
end type
type st_2 from statictext within w_carica_magazzino
end type
type st_1 from statictext within w_carica_magazzino
end type
type em_quan_movimento from editmask within w_carica_magazzino
end type
end forward

global type w_carica_magazzino from window
integer x = 1074
integer y = 484
integer width = 2848
integer height = 1476
boolean titlebar = true
string title = "UTILITA~' DI CARICO MAGAZZINO"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 79741120
cb_log cb_log
st_8 st_8
cb_2 cb_2
st_7 st_7
em_cod_prodotto em_cod_prodotto
st_5 st_5
st_6 st_6
st_record st_record
st_4 st_4
cb_1 cb_1
em_cod_tipo_movimento em_cod_tipo_movimento
st_3 st_3
st_2 st_2
st_1 st_1
em_quan_movimento em_quan_movimento
end type
global w_carica_magazzino w_carica_magazzino

forward prototypes
public function integer wf_scrivi_log (string fs_messaggio)
public function integer wf_azzera_log ()
end prototypes

public function integer wf_scrivi_log (string fs_messaggio);integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio,ls_default

ls_nome_file = "\movimenti.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

li_file = fileopen(ls_volume + ls_nome_file, linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if

ls_messaggio = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + fs_messaggio
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

public function integer wf_azzera_log ();integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio

ls_nome_file = "\movimenti.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

li_file = fileopen(ls_volume + ls_nome_file, linemode!, Write!, LockWrite!, replace!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if

ls_messaggio = ""
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

on w_carica_magazzino.create
this.cb_log=create cb_log
this.st_8=create st_8
this.cb_2=create cb_2
this.st_7=create st_7
this.em_cod_prodotto=create em_cod_prodotto
this.st_5=create st_5
this.st_6=create st_6
this.st_record=create st_record
this.st_4=create st_4
this.cb_1=create cb_1
this.em_cod_tipo_movimento=create em_cod_tipo_movimento
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.em_quan_movimento=create em_quan_movimento
this.Control[]={this.cb_log,&
this.st_8,&
this.cb_2,&
this.st_7,&
this.em_cod_prodotto,&
this.st_5,&
this.st_6,&
this.st_record,&
this.st_4,&
this.cb_1,&
this.em_cod_tipo_movimento,&
this.st_3,&
this.st_2,&
this.st_1,&
this.em_quan_movimento}
end on

on w_carica_magazzino.destroy
destroy(this.cb_log)
destroy(this.st_8)
destroy(this.cb_2)
destroy(this.st_7)
destroy(this.em_cod_prodotto)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_record)
destroy(this.st_4)
destroy(this.cb_1)
destroy(this.em_cod_tipo_movimento)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_quan_movimento)
end on

event open;integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio,ls_default

ls_nome_file = "\movimenti.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

st_4.TEXT = "I MOVIMENTI SARANNO MEMORIZZATI NEL FILE " + ls_volume + ls_nome_file

wf_azzera_log()
end event

type cb_log from commandbutton within w_carica_magazzino
integer x = 2469
integer y = 940
integer width = 320
integer height = 100
integer taborder = 12
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "APRI LOG"
end type

event clicked;integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio,ls_default

ls_nome_file = "\movimenti.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

run("notepad.exe " + ls_volume + ls_nome_file)

end event

type st_8 from statictext within w_carica_magazzino
integer x = 846
integer y = 820
integer width = 206
integer height = 120
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
string text = "2)"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_carica_magazzino
integer x = 1074
integer y = 640
integer width = 846
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "AZZERA PROGRESSIVI"
end type

event clicked;if g_mb.messagebox("APICE","Azzero i progressivi di magazzino?",Question!,YesNo!,2) = 1 then
	
	string ls_prodotto	
	ls_prodotto = em_cod_prodotto.text
	
	if ls_prodotto <> "" and not isnull(ls_prodotto) then
		update anag_prodotti
		set saldo_quan_anno_prec = 0,
			 saldo_quan_inizio_anno = 0,
			 saldo_quan_ultima_chiusura = 0,
			 prog_quan_entrata = 0,
			 val_quan_entrata = 0,
			 prog_quan_acquistata = 0,
			 val_quan_acquistata = 0,
			 prog_quan_uscita = 0,
			 val_quan_uscita = 0,
			 prog_quan_venduta = 0,
			 val_quan_venduta = 0
		where cod_azienda = :s_cs_xx.cod_azienda and 
		      cod_prodotto = :ls_prodotto;
	else
		update anag_prodotti
		set saldo_quan_anno_prec = 0,
			 saldo_quan_inizio_anno = 0,
			 saldo_quan_ultima_chiusura = 0,
			 prog_quan_entrata = 0,
			 val_quan_entrata = 0,
			 prog_quan_acquistata = 0,
			 val_quan_acquistata = 0,
			 prog_quan_uscita = 0,
			 val_quan_uscita = 0,
			 prog_quan_venduta = 0,
			 val_quan_venduta = 0
		where cod_azienda = :s_cs_xx.cod_azienda;
	end if
	if sqlca.sqlcode <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore in azzeramento progressivi. " + sqlca.sqlerrtext)
		wf_scrivi_log("Errore in azzeramento progressivi. " + sqlca.sqlerrtext)
	else
		commit;
	g_mb.messagebox("APICE","Azzeramento progressivi eseguito con successo !!!")
	end if
end if
end event

type st_7 from statictext within w_carica_magazzino
integer x = 846
integer y = 640
integer width = 206
integer height = 120
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 67108864
string text = "1)"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_cod_prodotto from editmask within w_carica_magazzino
integer x = 1499
integer y = 468
integer width = 658
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_5 from statictext within w_carica_magazzino
integer x = 311
integer y = 476
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "CODICE PRODOTTO DA CARICARE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_carica_magazzino
integer x = 18
integer y = 1184
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "record corrente:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_record from statictext within w_carica_magazzino
integer y = 1260
integer width = 2766
integer height = 80
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_carica_magazzino
integer x = 23
integer y = 1080
integer width = 2766
integer height = 76
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
boolean enabled = false
string text = "I MOVIMENTI SARANNO MEMORIZZATI NEL FILE C:\MOVIMENTI.LOG"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_carica_magazzino
integer x = 1074
integer y = 820
integer width = 846
integer height = 100
integer taborder = 2
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ESEGUI MOVIMENTI DI CARICO"
end type

event clicked;string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], &
       ls_cod_cliente[], ls_referenza, ls_cod_tipo_movimento, ls_cod_prodotto, ls_des_prodotto, ls_sql, ls_prodotto
long   ll_prog_stock[], ll_anno_registrazione[], ll_num_registrazione[], ll_num_documento, &
		 ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_num_prodotti, ll_i, ll_cont
double ll_quantita, ll_valore
datetime ldt_data_stock[], ldt_data_documento, ldt_data_registrazione
datastore lds_prodotti

uo_magazzino luo_mag


setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])
setnull(ls_cod_cliente[1])
setnull(ls_cod_fornitore[1])

ls_cod_tipo_movimento = em_cod_tipo_movimento.text
ll_quantita = double(em_quan_movimento.text)
ll_valore   = 0
ll_num_documento = 0
ldt_data_documento = datetime(today(), 00:00:00)
ldt_data_registrazione = ldt_data_documento
ls_referenza = "movimento iniziale"
ls_prodotto = em_cod_prodotto.text

if ll_quantita = 0 then
	if g_mb.messagebox("APICE","Attenzione movimento con quantità ZERO: Proseguo?",Question!,YesNo!,2) = 2 then return
end if

setnull(ls_cod_deposito[1])
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_lotto[1])
setnull(ldt_data_stock[1])
setnull(ll_prog_stock[1])

lds_prodotti = CREATE datastore
lds_prodotti.dataobject = 'd_carica_magazzino_1000000'
lds_prodotti.settransobject(sqlca)
ll_cont = lds_prodotti.retrieve(s_cs_xx.cod_azienda)
if ls_prodotto <> "" and not isnull(ls_prodotto) then
	lds_prodotti.setfilter("cod_prodotto = '" + ls_prodotto + "'")
	lds_prodotti.filter()
end if
ll_num_prodotti = lds_prodotti.rowcount()


for ll_i = 1 to ll_num_prodotti
	ls_cod_prodotto = lds_prodotti.getitemstring(ll_i,"cod_prodotto")
	ls_des_prodotto = lds_prodotti.getitemstring(ll_i,"des_prodotto")
	yield()	
	if mid(ls_cod_prodotto, 2, 1) <> "0" or left(ls_cod_prodotto, 2) = "E0"then
		st_record.text = string(ll_i) + "/" + string(ll_num_prodotti) + " -  Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto
	
		if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
			st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV"
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in creazione DEST_MOV") 
			rollback;
			continue
		end if
		
		if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
										 ll_num_reg_dest_stock, &
										 ls_cod_tipo_movimento, &
										 ls_cod_prodotto) = -1 then
			st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV"
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " Errore in verifica DEST_MOV") 
			rollback;
			continue
		end if
		
		luo_mag = create uo_magazzino
		
		if luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
									em_cod_tipo_movimento.text, &
									"S", &
									ls_cod_prodotto, &
									ll_quantita, &
									ll_valore, &
									ll_num_documento, &
									ldt_data_documento, &
									ls_referenza, &
									ll_anno_reg_dest_stock, &
									ll_num_reg_dest_stock, &
									ls_cod_deposito[], &
									ls_cod_ubicazione[], &
									ls_cod_lotto[], &
									ldt_data_stock[], &
									ll_prog_stock[], &
									ls_cod_fornitore[], &
									ls_cod_cliente[], &
									ll_anno_registrazione[], &
									ll_num_registrazione[] ) =  0 then
									
			destroy luo_mag
									
			if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, ll_num_reg_dest_stock) = -1 then
				rollback;
				continue
			end if
			st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " movimentato con successo !!" + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) 
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " movimentato con successo !! " + string(ll_anno_registrazione[1]) + "/" + string(ll_num_registrazione[1]) )
			commit;
		else
			destroy luo_mag			
			st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore."
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " non eseguito a causa di un errore.")
			rollback;
			continue
		end if	
	else
		st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " SALTATO MOVIMENTO"
		wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + " SALTATO MOVIMENTO")
	end if
next
commit;
st_record.text = "FINITO CREAZIONE MOVIMENTI COMMIT"
wf_scrivi_log("FINITO CREAZIONE MOVIMENTI COMMIT")
g_mb.messagebox("APICE","Elaborazione terminata: verificare il file di log per eventuali errori")

cb_log.triggerevent(clicked!)
return			

end event

type em_cod_tipo_movimento from editmask within w_carica_magazzino
integer x = 1499
integer y = 368
integer width = 658
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "INI"
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_3 from statictext within w_carica_magazzino
integer x = 311
integer y = 376
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "CODICE MOVIMENTO DA USARE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_carica_magazzino
integer x = 23
integer y = 20
integer width = 2766
integer height = 200
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "QUESTA PROCEDURA ESEGUIRA~' IL MOVIMENTO DI MAGAZZINO  RICHIESTO PER TUTTI I PRODOTTI CHE NON HANNO ZERO NELLA 2° POSIZIONE E PER TUTTI QUELLI CHE INIZIANO PER ~"E0~""
alignment alignment = center!
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_carica_magazzino
integer x = 311
integer y = 276
integer width = 1170
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "QUANTITA~' DA MOVIMENTARE IN MAGAZZINO"
boolean focusrectangle = false
end type

type em_quan_movimento from editmask within w_carica_magazzino
integer x = 1499
integer y = 268
integer width = 658
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1000000"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###,###,###,###"
boolean spin = true
string displaydata = "~b"
double increment = 1
string minmax = "0~~99999999"
end type

