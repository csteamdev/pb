﻿$PBExportHeader$w_cartellini_ean.srw
forward
global type w_cartellini_ean from w_cs_xx_principale
end type
type dw_ricerca from u_dw_search within w_cartellini_ean
end type
type dw_lista from uo_cs_xx_dw within w_cartellini_ean
end type
end forward

global type w_cartellini_ean from w_cs_xx_principale
integer width = 3995
integer height = 2484
string title = "Anagrafica Cartellini EAN"
dw_ricerca dw_ricerca
dw_lista dw_lista
end type
global w_cartellini_ean w_cartellini_ean

type variables


string			is_sql_base
end variables

forward prototypes
public function integer wf_retrieve (ref string as_errore)
end prototypes

public function integer wf_retrieve (ref string as_errore);

string			ls_sql, ls_cod_prodotto, ls_cod_deposito
long				ll_progressivo


dw_ricerca.accepttext()

ls_sql = is_sql_base

ls_cod_prodotto = dw_ricerca.getitemstring(1, "cod_prodotto")
ls_cod_deposito = dw_ricerca.getitemstring(1, "cod_deposito")
ll_progressivo = dw_ricerca.getitemnumber(1, "progressivo")

ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	ls_sql += " and cod_prodotto like '"+ls_cod_prodotto+"'"
end if

if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then
	ls_sql += " and cod_deposito='"+ls_cod_deposito+"'"
end if

if ll_progressivo>0 and not isnull(ll_progressivo) then
	ls_sql += " and progressivo="+string(ll_progressivo)
end if


ls_sql += " order by cod_prodotto, cod_deposito"

dw_lista.setsqlselect(ls_sql)
dw_lista.retrieve()

return 0
end function

on w_cartellini_ean.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.dw_lista
end on

on w_cartellini_ean.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_options(		sqlca, &
									pcca.null_object, &
									c_noretrieveonopen, &
									c_default)
									
dw_lista.change_dw_current( )


is_sql_base = dw_lista.getsqlselect()
end event

event pc_setddlb;call super::pc_setddlb;string ls_where_depositi


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"


f_po_loaddddw_dw(dw_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 ls_where_depositi)


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"

f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 ls_where_depositi)
end event

type dw_ricerca from u_dw_search within w_cartellini_ean
integer x = 27
integer y = 32
integer width = 3899
integer height = 504
integer taborder = 10
string dataobject = "d_cartellini_ean_sel"
end type

event buttonclicked;call super::buttonclicked;

choose case dwo.name
		
	case "b_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
		
	case "b_cerca"
		dw_lista.change_dw_current( )
		parent.postevent("pc_retrieve")
		
		
end choose
end event

type dw_lista from uo_cs_xx_dw within w_cartellini_ean
integer x = 27
integer y = 564
integer width = 3899
integer height = 1792
integer taborder = 10
string dataobject = "d_cartellini_ean"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;

long				ll_ret

string			ls_errore


ll_ret = wf_retrieve(ls_errore)

if ll_ret<0 then
	g_mb.error(ls_errore)
	return
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
next
end event

