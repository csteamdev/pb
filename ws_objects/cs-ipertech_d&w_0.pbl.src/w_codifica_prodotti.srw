﻿$PBExportHeader$w_codifica_prodotti.srw
$PBExportComments$finestra per la nuova codifica prodotti PTENDA
forward
global type w_codifica_prodotti from Window
end type
type sle_1 from singlelineedit within w_codifica_prodotti
end type
type mle_1 from multilineedit within w_codifica_prodotti
end type
type cb_codifica from commandbutton within w_codifica_prodotti
end type
end forward

global type w_codifica_prodotti from Window
int X=833
int Y=361
int Width=3045
int Height=1241
boolean TitleBar=true
string Title="Codifica Prodotti"
long BackColor=79741120
boolean ControlMenu=true
boolean MinBox=true
boolean MaxBox=true
boolean Resizable=true
sle_1 sle_1
mle_1 mle_1
cb_codifica cb_codifica
end type
global w_codifica_prodotti w_codifica_prodotti

on w_codifica_prodotti.create
this.sle_1=create sle_1
this.mle_1=create mle_1
this.cb_codifica=create cb_codifica
this.Control[]={ this.sle_1,&
this.mle_1,&
this.cb_codifica}
end on

on w_codifica_prodotti.destroy
destroy(this.sle_1)
destroy(this.mle_1)
destroy(this.cb_codifica)
end on

type sle_1 from singlelineedit within w_codifica_prodotti
int X=46
int Y=1021
int Width=1847
int Height=93
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
boolean DisplayOnly=true
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type mle_1 from multilineedit within w_codifica_prodotti
int X=46
int Y=21
int Width=2949
int Height=981
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean AutoHScroll=true
boolean AutoVScroll=true
long TextColor=33554432
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_codifica from commandbutton within w_codifica_prodotti
int X=2675
int Y=1041
int Width=307
int Height=81
int TabOrder=30
string Text="&Codifica"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
string ls_error, ls_cod_prodotto_new[], ls_des_prodotto_new[], ls_temp

string ls_cod_azienda, ls_cod_prodotto, ls_cod_comodo, ls_des_prodotto, &
		 ls_cod_deposito, ls_cod_ubicazione, ls_flag_decimali, ls_cod_cat_mer, ls_cod_responsabile, &
		 ls_cod_misura_mag, ls_cod_misura_peso, ls_cod_misura_vol, ls_flag_classe_abc, &
		 ls_flag_sotto_scorta, ls_flag_lifo, ls_cod_prodotto_alt, ls_cod_misura_acq, &
		 ls_cod_fornitore, ls_cod_gruppo_acq, ls_cod_gruppo_ven, ls_cod_iva, ls_cod_misura_ven, &
		 ls_flag_blocco, ls_cod_livello_prod_1, ls_cod_livello_prod_2, ls_cod_livello_prod_3, &
		 ls_cod_livello_prod_4, ls_cod_livello_prod_5, ls_cod_nomenclatura, ls_cod_imballo

		
double ld_peso_lordo, ld_peso_netto, ld_volume, ld_saldo_quan_anno_prec, ld_saldo_quan_inizio_anno, &
		 ld_val_inizio_anno, ld_saldo_quan_ultima_chiusura, ld_prog_quan_entrata, ld_val_quan_entrata, &
		 ld_prog_quan_uscita, ld_val_quan_uscita, ld_prog_quan_acquistata, ld_val_quan_acquistata, &
		 ld_prog_quan_venduta, ld_val_quan_venduta, ld_quan_ordinata, ld_quan_impegnata, &
		 ld_quan_assegnata, ld_quan_in_spedizione, ld_quan_anticipi, ld_costo_standard, &
		 ld_costo_ultimo, ld_lotto_economico, ld_livello_riordino, ld_scorta_minima, &
		 ld_scorta_massima, ld_indice_rotazione, ld_consumo_medio, ld_fat_conversione_acq, &
		 ld_prezzo_acquisto, ld_sconto_acquisto, ld_quan_minima, ld_inc_ordine, ld_quan_massima, &
		 ld_fat_conversione_ven, ld_prezzo_vendita, ld_sconto, ld_provvigione
      
long  ll_pezzi_collo, ll_tempo_app, ll_cod_barre
datetime ldt_data_creazione, ldt_data_blocco
integer li_cont, li_W, li_len, li_ok

mle_1.FontCharSet = ANSI!
mle_1.text=""

DECLARE cur_prod CURSOR FOR 
select cod_azienda, cod_prodotto, cod_comodo, cod_barre,
       des_prodotto, data_creazione, cod_deposito, cod_ubicazione,
       flag_decimali, cod_cat_mer, cod_responsabile, cod_misura_mag,
       cod_misura_peso, peso_lordo, peso_netto, cod_misura_vol,
       volume, pezzi_collo, flag_classe_abc, flag_sotto_scorta,
		 flag_lifo, saldo_quan_anno_prec, saldo_quan_inizio_anno, val_inizio_anno,
       saldo_quan_ultima_chiusura, prog_quan_entrata, val_quan_entrata,
		 prog_quan_uscita, val_quan_uscita, prog_quan_acquistata, val_quan_acquistata,
       prog_quan_venduta, val_quan_venduta, quan_ordinata, quan_impegnata,
       quan_assegnata, quan_in_spedizione, quan_anticipi, costo_standard,
       costo_ultimo, lotto_economico, livello_riordino, scorta_minima,
       scorta_massima, indice_rotazione, consumo_medio, cod_prodotto_alt,
       cod_misura_acq, fat_conversione_acq, cod_fornitore, cod_gruppo_acq,
       prezzo_acquisto, sconto_acquisto, quan_minima, inc_ordine,
       quan_massima, tempo_app, cod_misura_ven, fat_conversione_ven,
       cod_gruppo_ven, cod_iva, prezzo_vendita, sconto,
       provvigione, flag_blocco, data_blocco, cod_livello_prod_1,
       cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4,
       cod_livello_prod_5, cod_nomenclatura, cod_imballo 
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda
	and cod_prodotto like 'R%W%'
using sqlca;


OPEN cur_prod ;
DO while 0=0
	FETCH cur_prod INTO :ls_cod_azienda, :ls_cod_prodotto, :ls_cod_comodo, :ll_cod_barre,
       :ls_des_prodotto, :ldt_data_creazione, :ls_cod_deposito, :ls_cod_ubicazione,
       :ls_flag_decimali, :ls_cod_cat_mer, :ls_cod_responsabile, :ls_cod_misura_mag,
       :ls_cod_misura_peso, :ld_peso_lordo, :ld_peso_netto, :ls_cod_misura_vol,
       :ld_volume, :ll_pezzi_collo, :ls_flag_classe_abc, :ls_flag_sotto_scorta,
		 :ls_flag_lifo, :ld_saldo_quan_anno_prec, :ld_saldo_quan_inizio_anno, :ld_val_inizio_anno,
       :ld_saldo_quan_ultima_chiusura, :ld_prog_quan_entrata, :ld_val_quan_entrata,
		 :ld_prog_quan_uscita, :ld_val_quan_uscita, :ld_prog_quan_acquistata, :ld_val_quan_acquistata,
       :ld_prog_quan_venduta, :ld_val_quan_venduta, :ld_quan_ordinata, :ld_quan_impegnata,
       :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi, :ld_costo_standard,
       :ld_costo_ultimo, :ld_lotto_economico, :ld_livello_riordino, :ld_scorta_minima,
       :ld_scorta_massima, :ld_indice_rotazione, :ld_consumo_medio, :ls_cod_prodotto_alt,
       :ls_cod_misura_acq, :ld_fat_conversione_acq, :ls_cod_fornitore, :ls_cod_gruppo_acq,
       :ld_prezzo_acquisto, :ld_sconto_acquisto, :ld_quan_minima, :ld_inc_ordine,
       :ld_quan_massima, :ll_tempo_app, :ls_cod_misura_ven, :ld_fat_conversione_ven,
       :ls_cod_gruppo_ven, :ls_cod_iva, :ld_prezzo_vendita, :ld_sconto,
       :ld_provvigione, :ls_flag_blocco, :ldt_data_blocco, :ls_cod_livello_prod_1,
       :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4,
       :ls_cod_livello_prod_5, :ls_cod_nomenclatura, :ls_cod_imballo;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		return -1
	end if
	
	sle_1.text="Prodotto: " + ls_cod_prodotto
	li_ok=0	
	li_W = Pos ( ls_cod_prodotto, "W")
	li_len = Len (ls_cod_prodotto) 
	ls_cod_prodotto_new[1]= Left ( ls_cod_prodotto, li_W - 1) + "1" + Right ( ls_cod_prodotto, li_len - li_W) 
	ls_des_prodotto_new[1]= ls_des_prodotto +" BI"
	ls_cod_prodotto_new[2]= Left ( ls_cod_prodotto, li_W - 1) + "2" + Right ( ls_cod_prodotto, li_len - li_W) 
	ls_des_prodotto_new[2]= ls_des_prodotto +" NE"
	ls_cod_prodotto_new[3]= Left ( ls_cod_prodotto, li_W - 1) + "6" + Right ( ls_cod_prodotto, li_len - li_W) 
	ls_des_prodotto_new[3]= ls_des_prodotto +" MA"
	ls_cod_prodotto_new[4]= Left ( ls_cod_prodotto, li_W - 1) + "7" + Right ( ls_cod_prodotto, li_len - li_W) 
	ls_des_prodotto_new[4]= ls_des_prodotto +" VE"
	
	for li_cont=1 to 4
		
		INSERT INTO anag_prodotti 
		(cod_azienda, cod_prodotto, cod_comodo, cod_barre,
			 des_prodotto, data_creazione, cod_deposito, cod_ubicazione,
			 flag_decimali, cod_cat_mer, cod_responsabile, cod_misura_mag,
			 cod_misura_peso, peso_lordo, peso_netto, cod_misura_vol,
			 volume, pezzi_collo, flag_classe_abc, flag_sotto_scorta,
			 flag_lifo, saldo_quan_anno_prec, saldo_quan_inizio_anno, val_inizio_anno,
			 saldo_quan_ultima_chiusura, prog_quan_entrata, val_quan_entrata,
			 prog_quan_uscita, val_quan_uscita, prog_quan_acquistata, val_quan_acquistata,
			 prog_quan_venduta, val_quan_venduta, quan_ordinata, quan_impegnata,
			 quan_assegnata, quan_in_spedizione, quan_anticipi, costo_standard,
			 costo_ultimo, lotto_economico, livello_riordino, scorta_minima,
			 scorta_massima, indice_rotazione, consumo_medio, cod_prodotto_alt,
			 cod_misura_acq, fat_conversione_acq, cod_fornitore, cod_gruppo_acq,
			 prezzo_acquisto, sconto_acquisto, quan_minima, inc_ordine,
			 quan_massima, tempo_app, cod_misura_ven, fat_conversione_ven,
			 cod_gruppo_ven, cod_iva, prezzo_vendita, sconto,
			 provvigione, flag_blocco, data_blocco, cod_livello_prod_1,
			 cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4,
			 cod_livello_prod_5, cod_nomenclatura, cod_imballo )
		VALUES (:ls_cod_azienda, :ls_cod_prodotto_new[li_cont], :ls_cod_comodo, :ll_cod_barre,
			 :ls_des_prodotto_new[li_cont], :ldt_data_creazione, :ls_cod_deposito, :ls_cod_ubicazione,
			 :ls_flag_decimali, :ls_cod_cat_mer, :ls_cod_responsabile, :ls_cod_misura_mag,
			 :ls_cod_misura_peso, :ld_peso_lordo, :ld_peso_netto, :ls_cod_misura_vol,
			 :ld_volume, :ll_pezzi_collo, :ls_flag_classe_abc, :ls_flag_sotto_scorta,
			 :ls_flag_lifo, :ld_saldo_quan_anno_prec, :ld_saldo_quan_inizio_anno, :ld_val_inizio_anno,
			 :ld_saldo_quan_ultima_chiusura, :ld_prog_quan_entrata, :ld_val_quan_entrata,
			 :ld_prog_quan_uscita, :ld_val_quan_uscita, :ld_prog_quan_acquistata, :ld_val_quan_acquistata,
			 :ld_prog_quan_venduta, :ld_val_quan_venduta, :ld_quan_ordinata, :ld_quan_impegnata,
			 :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi, :ld_costo_standard,
			 :ld_costo_ultimo, :ld_lotto_economico, :ld_livello_riordino, :ld_scorta_minima,
			 :ld_scorta_massima, :ld_indice_rotazione, :ld_consumo_medio, :ls_cod_prodotto_alt,
			 :ls_cod_misura_acq, :ld_fat_conversione_acq, :ls_cod_fornitore, :ls_cod_gruppo_acq,
			 :ld_prezzo_acquisto, :ld_sconto_acquisto, :ld_quan_minima, :ld_inc_ordine,
			 :ld_quan_massima, :ll_tempo_app, :ls_cod_misura_ven, :ld_fat_conversione_ven,
			 :ls_cod_gruppo_ven, :ls_cod_iva, :ld_prezzo_vendita, :ld_sconto,
			 :ld_provvigione, :ls_flag_blocco, :ldt_data_blocco, :ls_cod_livello_prod_1,
			 :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4,
			 :ls_cod_livello_prod_5, :ls_cod_nomenclatura, :ls_cod_imballo) 
		USING sqlca ;
		
		if sqlca.sqlcode<0 then
			ls_temp = mle_1.text + "Errore inserzione: vecchio codice: "+ ls_cod_prodotto &
			+ "  nuovo codice: " + ls_cod_prodotto_new[li_cont] + ", " + SQLCA.SQLErrText + " ~r~n "
			mle_1.text =ls_temp
			li_ok=1
			exit	// non inserisce gli altri prodotti derivati da questo prodotto
		end if

	next
	
	if li_ok=0 then
			delete from anag_prodotti 
			where cod_azienda=:s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto
			using sqlca;
		
			if sqlca.sqlcode<0 then
				ls_temp = mle_1.text + "Errore cancellazione prodotto " + ls_cod_prodotto + ",  ~r~n "
				mle_1.text =ls_temp
			end if
	end if
LOOP
CLOSE cur_prod ;

DECLARE cur_prod2 CURSOR FOR 
select cod_azienda, cod_prodotto, cod_comodo, cod_barre,
       des_prodotto, data_creazione, cod_deposito, cod_ubicazione,
       flag_decimali, cod_cat_mer, cod_responsabile, cod_misura_mag,
       cod_misura_peso, peso_lordo, peso_netto, cod_misura_vol,
       volume, pezzi_collo, flag_classe_abc, flag_sotto_scorta,
		 flag_lifo, saldo_quan_anno_prec, saldo_quan_inizio_anno, val_inizio_anno,
       saldo_quan_ultima_chiusura, prog_quan_entrata, val_quan_entrata,
		 prog_quan_uscita, val_quan_uscita, prog_quan_acquistata, val_quan_acquistata,
       prog_quan_venduta, val_quan_venduta, quan_ordinata, quan_impegnata,
       quan_assegnata, quan_in_spedizione, quan_anticipi, costo_standard,
       costo_ultimo, lotto_economico, livello_riordino, scorta_minima,
       scorta_massima, indice_rotazione, consumo_medio, cod_prodotto_alt,
       cod_misura_acq, fat_conversione_acq, cod_fornitore, cod_gruppo_acq,
       prezzo_acquisto, sconto_acquisto, quan_minima, inc_ordine,
       quan_massima, tempo_app, cod_misura_ven, fat_conversione_ven,
       cod_gruppo_ven, cod_iva, prezzo_vendita, sconto,
       provvigione, flag_blocco, data_blocco, cod_livello_prod_1,
       cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4,
       cod_livello_prod_5, cod_nomenclatura, cod_imballo 
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda
	and ( 	cod_prodotto like 'A%W%' 
	      or cod_prodotto like 'B%W%' 
			or cod_prodotto like 'C%W%' 
			or cod_prodotto like 'D%W%' 
			or cod_prodotto like 'F%W%'
			or cod_prodotto like 'H%W%' 
			or cod_prodotto like 'M%W%' 
			or cod_prodotto like 'N%W%' 
			or cod_prodotto like 'P%W%' 
			or cod_prodotto like 'T%W%')
using sqlca;

OPEN cur_prod2 ;
DO while 0=0
	FETCH cur_prod2 INTO :ls_cod_azienda, :ls_cod_prodotto, :ls_cod_comodo, :ll_cod_barre,
       :ls_des_prodotto, :ldt_data_creazione, :ls_cod_deposito, :ls_cod_ubicazione,
       :ls_flag_decimali, :ls_cod_cat_mer, :ls_cod_responsabile, :ls_cod_misura_mag,
       :ls_cod_misura_peso, :ld_peso_lordo, :ld_peso_netto, :ls_cod_misura_vol,
       :ld_volume, :ll_pezzi_collo, :ls_flag_classe_abc, :ls_flag_sotto_scorta,
		 :ls_flag_lifo, :ld_saldo_quan_anno_prec, :ld_saldo_quan_inizio_anno, :ld_val_inizio_anno,
       :ld_saldo_quan_ultima_chiusura, :ld_prog_quan_entrata, :ld_val_quan_entrata,
		 :ld_prog_quan_uscita, :ld_val_quan_uscita, :ld_prog_quan_acquistata, :ld_val_quan_acquistata,
       :ld_prog_quan_venduta, :ld_val_quan_venduta, :ld_quan_ordinata, :ld_quan_impegnata,
       :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi, :ld_costo_standard,
       :ld_costo_ultimo, :ld_lotto_economico, :ld_livello_riordino, :ld_scorta_minima,
       :ld_scorta_massima, :ld_indice_rotazione, :ld_consumo_medio, :ls_cod_prodotto_alt,
       :ls_cod_misura_acq, :ld_fat_conversione_acq, :ls_cod_fornitore, :ls_cod_gruppo_acq,
       :ld_prezzo_acquisto, :ld_sconto_acquisto, :ld_quan_minima, :ld_inc_ordine,
       :ld_quan_massima, :ll_tempo_app, :ls_cod_misura_ven, :ld_fat_conversione_ven,
       :ls_cod_gruppo_ven, :ls_cod_iva, :ld_prezzo_vendita, :ld_sconto,
       :ld_provvigione, :ls_flag_blocco, :ldt_data_blocco, :ls_cod_livello_prod_1,
       :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4,
       :ls_cod_livello_prod_5, :ls_cod_nomenclatura, :ls_cod_imballo;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	if sqlca.sqlcode<>0 then
		g_mb.messagebox("Errore nel DB ", SQLCA.SQLErrText)
		return -1
	end if
	
	sle_1.text="Prodotto: " + ls_cod_prodotto
	li_ok=0
	li_W = Pos ( ls_cod_prodotto, "W")
	li_len = Len (ls_cod_prodotto) 
	ls_cod_prodotto_new[1]= Left ( ls_cod_prodotto, li_W - 1) + "1" + Right ( ls_cod_prodotto, li_len - li_W) 
// 	ls_des_prodotto_new[1] = "AlEx " + ls_des_prodotto
		INSERT INTO anag_prodotti 
		(cod_azienda, cod_prodotto, cod_comodo, cod_barre,
			 des_prodotto, data_creazione, cod_deposito, cod_ubicazione,
			 flag_decimali, cod_cat_mer, cod_responsabile, cod_misura_mag,
			 cod_misura_peso, peso_lordo, peso_netto, cod_misura_vol,
			 volume, pezzi_collo, flag_classe_abc, flag_sotto_scorta,
			 flag_lifo, saldo_quan_anno_prec, saldo_quan_inizio_anno, val_inizio_anno,
			 saldo_quan_ultima_chiusura, prog_quan_entrata, val_quan_entrata,
			 prog_quan_uscita, val_quan_uscita, prog_quan_acquistata, val_quan_acquistata,
			 prog_quan_venduta, val_quan_venduta, quan_ordinata, quan_impegnata,
			 quan_assegnata, quan_in_spedizione, quan_anticipi, costo_standard,
			 costo_ultimo, lotto_economico, livello_riordino, scorta_minima,
			 scorta_massima, indice_rotazione, consumo_medio, cod_prodotto_alt,
			 cod_misura_acq, fat_conversione_acq, cod_fornitore, cod_gruppo_acq,
			 prezzo_acquisto, sconto_acquisto, quan_minima, inc_ordine,
			 quan_massima, tempo_app, cod_misura_ven, fat_conversione_ven,
			 cod_gruppo_ven, cod_iva, prezzo_vendita, sconto,
			 provvigione, flag_blocco, data_blocco, cod_livello_prod_1,
			 cod_livello_prod_2, cod_livello_prod_3, cod_livello_prod_4,
			 cod_livello_prod_5, cod_nomenclatura, cod_imballo )
		VALUES (:ls_cod_azienda, :ls_cod_prodotto_new[1], :ls_cod_comodo, :ll_cod_barre,
			 :ls_des_prodotto, :ldt_data_creazione, :ls_cod_deposito, :ls_cod_ubicazione,
			 :ls_flag_decimali, :ls_cod_cat_mer, :ls_cod_responsabile, :ls_cod_misura_mag,
			 :ls_cod_misura_peso, :ld_peso_lordo, :ld_peso_netto, :ls_cod_misura_vol,
			 :ld_volume, :ll_pezzi_collo, :ls_flag_classe_abc, :ls_flag_sotto_scorta,
			 :ls_flag_lifo, :ld_saldo_quan_anno_prec, :ld_saldo_quan_inizio_anno, :ld_val_inizio_anno,
			 :ld_saldo_quan_ultima_chiusura, :ld_prog_quan_entrata, :ld_val_quan_entrata,
			 :ld_prog_quan_uscita, :ld_val_quan_uscita, :ld_prog_quan_acquistata, :ld_val_quan_acquistata,
			 :ld_prog_quan_venduta, :ld_val_quan_venduta, :ld_quan_ordinata, :ld_quan_impegnata,
			 :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi, :ld_costo_standard,
			 :ld_costo_ultimo, :ld_lotto_economico, :ld_livello_riordino, :ld_scorta_minima,
			 :ld_scorta_massima, :ld_indice_rotazione, :ld_consumo_medio, :ls_cod_prodotto_alt,
			 :ls_cod_misura_acq, :ld_fat_conversione_acq, :ls_cod_fornitore, :ls_cod_gruppo_acq,
			 :ld_prezzo_acquisto, :ld_sconto_acquisto, :ld_quan_minima, :ld_inc_ordine,
			 :ld_quan_massima, :ll_tempo_app, :ls_cod_misura_ven, :ld_fat_conversione_ven,
			 :ls_cod_gruppo_ven, :ls_cod_iva, :ld_prezzo_vendita, :ld_sconto,
			 :ld_provvigione, :ls_flag_blocco, :ldt_data_blocco, :ls_cod_livello_prod_1,
			 :ls_cod_livello_prod_2, :ls_cod_livello_prod_3, :ls_cod_livello_prod_4,
			 :ls_cod_livello_prod_5, :ls_cod_nomenclatura, :ls_cod_imballo) 
		USING sqlca ;
		
		if sqlca.sqlcode<0 then
			ls_temp = mle_1.text + "Errore inserzione: vecchio codice: "+ ls_cod_prodotto &
			+ "  nuovo codice: " + ls_cod_prodotto_new[1] +", " + SQLCA.SQLErrText + "  ~r~n "
			mle_1.text = ls_temp
			li_ok=1
		end if

	if li_ok=0 then
			delete from anag_prodotti 
			where cod_azienda=:s_cs_xx.cod_azienda
				and cod_prodotto = :ls_cod_prodotto
			using sqlca;
		
			if sqlca.sqlcode<0 then
				ls_temp = mle_1.text + "Errore cancellazione prodotto " + ls_cod_prodotto  + ",    ~r~n "
				mle_1.text = ls_temp
			end if
		end if
LOOP
CLOSE cur_prod ;

commit;

g_mb.messagebox("Codifica nuovi prodotti","Operazione Finita")
end event

