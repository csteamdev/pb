﻿$PBExportHeader$w_esporta_listini_excel.srw
forward
global type w_esporta_listini_excel from w_cs_xx_principale
end type
type st_msg2 from statictext within w_esporta_listini_excel
end type
type sle_tipo from singlelineedit within w_esporta_listini_excel
end type
type sle_msg from singlelineedit within w_esporta_listini_excel
end type
type st_msg from statictext within w_esporta_listini_excel
end type
type cb_1 from commandbutton within w_esporta_listini_excel
end type
type cb_3 from commandbutton within w_esporta_listini_excel
end type
type dw_selezione_2 from uo_cs_xx_dw within w_esporta_listini_excel
end type
type cb_sfoglia from commandbutton within w_esporta_listini_excel
end type
type cb_importa from commandbutton within w_esporta_listini_excel
end type
type dw_selezione from uo_cs_xx_dw within w_esporta_listini_excel
end type
type cb_esporta from commandbutton within w_esporta_listini_excel
end type
type dw_folder from u_folder within w_esporta_listini_excel
end type
end forward

global type w_esporta_listini_excel from w_cs_xx_principale
integer width = 2734
integer height = 1488
string title = "Esporta Listino"
boolean resizable = false
st_msg2 st_msg2
sle_tipo sle_tipo
sle_msg sle_msg
st_msg st_msg
cb_1 cb_1
cb_3 cb_3
dw_selezione_2 dw_selezione_2
cb_sfoglia cb_sfoglia
cb_importa cb_importa
dw_selezione dw_selezione
cb_esporta cb_esporta
dw_folder dw_folder
end type
global w_esporta_listini_excel w_esporta_listini_excel

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables
uo_excel_listini iuo_excel
string is_flag_incorporo_iva = "N"
string is_flag_scorporo_iva = "N"
string is_cod_tipo_listino_prodotto, is_cod_valuta
string is_filelog, is_flag_tipo_importazione
string is_cod_cat_prodotto_configurato
string is_formato_numero = "0,0000"
datetime idt_data_inizio_val
boolean ib_comune_OK=false
string is_OCF
end variables

forward prototypes
public function integer wf_carica_dimensioni (string fs_cod_prodotto, ref decimal fd_x[], ref decimal fd_y[])
public subroutine wf_rimpiazza (ref string as_mystring)
public function integer wf_scrivi_note (string fs_foglio, long fl_riga, long fl_colonna, boolean fb_sfuso)
public function integer wf_sfuso (ref long fl_riga, ref long fl_colonna)
public function integer wf_sfuso_cliente (ref long fl_riga, ref long fl_colonna)
public function integer wf_configurato (ref long fl_riga, ref long fl_colonna)
public function string wf_calcola_valore (string fs_cod_prodotto, string fs_flag_sconto_mag_prezzo, ref decimal fd_valore)
public subroutine wf_ricerca_prodotto_like (ref string fs_stringa_ricerca)
public function string wf_estrai_prodotto_cliente (string fs_input, ref string fs_prodotto, ref string fs_cliente)
protected function integer wf_configurato_cliente (ref long fl_riga, ref long fl_colonna)
public subroutine wf_log (string fs_val)
public function string wf_get_today_now ()
public function integer wf_importa_configurato (string fs_cod_prodotto)
public function integer wf_importa_configurato_cliente (string fs_cod_prodotto, string fs_cod_cliente)
public function integer wf_importa_sfuso ()
public function integer wf_importa_sfuso_cliente (string fs_cod_cliente)
public subroutine wf_incorpora_iva (string fs_cod_prodotto, ref decimal fd_valore)
public subroutine wf_scorpora_iva (string fs_cod_prodotto, ref decimal fd_valore)
public function integer wf_estrai_valore (string fs_valore, string fs_tipo, ref decimal fd_valore, ref string fs_msg)
public function integer wf_prepara_ds_sfuso (ref datastore fds_data, string fs_foglio)
public function integer wf_estrai_valore_dim (string fs_valore, string fs_cod_prodotto, ref string fs_tipo, ref decimal fd_valore, ref string fs_msg)
public function integer wf_prepara_ds_dimensioni (string fs_foglio, string fs_cod_prodotto, decimal fd_x[], decimal fd_y[], ref datastore fds_data)
public function integer wf_controlla_dimensioni (string fs_cod_prodotto, string fs_foglio, ref boolean fb_dimensioni, ref decimal fd_x_excel[], ref decimal fd_y_excel[])
public function integer wf_prepara_ds_varianti (string fs_foglio, string fs_flag, ref long fl_offset, ref datastore fds_data)
public function integer wf_estrai_data (string fs_foglio, ref datetime fdt_data_inizio_val)
public function integer wf_carica_ds_varianti (ref datastore fds_varianti, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, boolean fb_comune)
public function boolean wf_controlla_data_listino_comune (datetime fdt_data_inizio_val, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, string fs_sheets[])
public function boolean wf_controlla_data (datetime fdt_data_inizio_val, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, string fs_cod_prodotto, boolean fb_standard)
public function string wf_calcola_valore_var (string fs_cod_prodotto, string fs_flag_sconto_mag_prezzo, ref decimal fd_valore)
public function integer wf_get_destinatari (string as_input, string as_sep, ref string as_array[])
public function integer wf_crea_allegato (ref string fs_allegato)
public function integer wf_invia_mail ()
public function integer wf_elabora_dimensioni (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, decimal fd_limite_dim_1, decimal fd_limite_dim_2, ref decimal fd_variazione, ref string fs_flag_sconto_mag_prezzo, ref string fs_variazione, boolean fb_comune)
public function integer wf_get_provv_sfuso (string fs_cod_tipo_listini_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_agente, string fs_cod_cliente, string fs_cod_prodotto, double fd_quan_scaglione, double fd_sconto_tot, ref double fd_provvigione)
public function integer wf_get_desk_dir (ref string fs_desktop_dir)
public function long wf_controlla_padre_versione (string fs_cod_prodotto_variante, string fs_cod_prodotto_padre, string fs_cod_versione, ref string fs_messaggio)
protected function integer wf_configurato_cliente (ref long fl_riga, ref long fl_colonna, string fs_cod_prodotto)
public function integer wf_importa_configurato_var_client (string fs_cod_prodotto)
public function integer wf_prepara_ds_varianti_client (string fs_foglio, ref long fl_offset, ref datastore fds_data)
public function integer wf_get_versione_default (string fs_cod_prodotto, ref string fs_cod_versione)
end prototypes

public function integer wf_carica_dimensioni (string fs_cod_prodotto, ref decimal fd_x[], ref decimal fd_y[]);string ls_sql, ls_errore, ls_syntax
decimal ld_limite_dimensione
long ll_i, ll_J
string ls_flag_tipo_dim_x, ls_flag_tipo_dim_y
datastore lds_x, lds_y

//vedo i caratteri delle dimensioni del prodotto
select flag_tipo_dimensione_1,
       flag_tipo_dimensione_2
into   :ls_flag_tipo_dim_x,
		 :ls_flag_tipo_dim_y
from   tab_prodotti_dimensioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
//	g_mb.messagebox("APICE","Non sono state impostate le dimensioni in tabella prodotti dimensioni "+&
//						"per il prodotto '"+fs_cod_prodotto+"'",stopsign!)
	return -1
end if

//dimensione X
ls_sql = "select limite_dimensione "+&
			  "from tab_prodotti_dimensioni_det " + &
			  "where cod_azienda='"+s_cs_xx.cod_azienda+"' and flag_tipo_dimensione = '"+ls_flag_tipo_dim_x+"' " + &
				"and cod_prodotto = '"+fs_cod_prodotto+"' "+&
			  "order by limite_dimensione asc "

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dim 1: " + ls_errore)
	return -1
end if

lds_x = create datastore
lds_x.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_x
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dim 1: " + ls_errore)
	return -1
end if

lds_x.settransobject(sqlca)
if lds_x.retrieve() = -1 then
	destroy lds_x
	return -1
end if	

ll_i = 0
for ll_J = 1 to lds_x.rowcount()
	ld_limite_dimensione = lds_x.getitemdecimal(ll_J,"limite_dimensione")
	ll_i += 1
	fd_x[ll_i] = ld_limite_dimensione
next

destroy lds_x;
//-----------------------------------------------

//dimensione y
ls_sql = "select limite_dimensione "+&
			  "from tab_prodotti_dimensioni_det " + &
			  "where cod_azienda='"+s_cs_xx.cod_azienda+"' and flag_tipo_dimensione = '"+ls_flag_tipo_dim_y+"' " + &
				"and cod_prodotto = '"+fs_cod_prodotto+"' "+&
			  "order by limite_dimensione asc "

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dim 2: " + ls_errore)
	return -1
end if

lds_y = create datastore
lds_y.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_y
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dim 2: " + ls_errore)
	return -1
end if

lds_y.settransobject(sqlca)
if lds_y.retrieve() = -1 then
	destroy lds_y
	return -1
end if	

ll_i = 0
for ll_J = 1 to lds_y.rowcount()
	ld_limite_dimensione = lds_y.getitemdecimal(ll_J,"limite_dimensione")
	ll_i += 1
	fd_y[ll_i] = ld_limite_dimensione
next

destroy lds_y;

return 1
end function

public subroutine wf_rimpiazza (ref string as_mystring);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(as_mystring) and as_mystring<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(as_mystring, ",", start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 as_mystring = Replace(as_mystring, start_pos, Len(","), ".")
		 // Find the next occurrence of old_str.
		 start_pos = Pos(as_mystring, ",", start_pos+Len("."))
	LOOP	
else
	as_mystring="0"
end if
end subroutine

public function integer wf_scrivi_note (string fs_foglio, long fl_riga, long fl_colonna, boolean fb_sfuso);long ll_riga, ll_colonna
string ls_valore, ls_colore, ls_flag_incorporo_iva_r

//lascio una riga bianca prima di scrivere le note
ll_riga = fl_riga + 2

if fb_sfuso then
	//PRODOTTO SFUSO
	ll_colonna = 2
else
	//PRODOTTO CONFIGURATO
	ll_colonna = fl_colonna
end if

ls_colore = "none"

iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga,		ll_colonna,		"(*1)", 													false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga,		ll_colonna+1,	"S=Sconto sul prezzo di listino (-%)",				false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+1,	ll_colonna+1,	"M=Maggiorazione sul prezzo di listino (+%)",	false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+2,	ll_colonna+1,	"A=Aumento in valore sul prezzo di listino (+)", 	false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+3,	ll_colonna+1,	"D=Diminuzione in valore sul prezzo di listino (-)", false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+4,	ll_colonna+1,	"P=Prezzo fisso o sostituzione di prezzo", 		false,false,ls_colore)

iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga,		ll_colonna+5,		"(*2)", 								false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga,		ll_colonna+5+1,	"N=Nessuna origine prezzo",	false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+1,	ll_colonna+5+1,	"V=Prezzo Vendita", 				false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+2,	ll_colonna+5+1,	"L=Listino", 							false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+3,	ll_colonna+5+1,	"S=Costo Standard", 				false,false,ls_colore)
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+4,	ll_colonna+5+1,	"P=Costo Produzione", 			false,false,ls_colore)

ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
if ls_flag_incorporo_iva_r = "S" then
	ls_valore = "Tutti i prezzi si intendono con IVA incorporata"
else
	ls_valore = "Tutti i prezzi si intendono con IVA non incorporata"
end if
iuo_excel.uof_scrivi_nota(fs_foglio,ll_riga+6, ll_colonna, ls_valore, false,false,ls_colore)

return 1
end function

public function integer wf_sfuso (ref long fl_riga, ref long fl_colonna);string 		ls_cod_prodotto_da_r,ls_cod_prodotto_a_r,ls_cod_tipo_listino_prodotto_r, ls_cod_cliente_provv, &
				ls_cod_valuta_r, ls_flag_estrai_ultima_r, ls_flag_incorporo_iva_r,&
				ls_syntax, ls_errore, ls_sql, ls_where, ls_cod_prodotto_like_r, ls_cod_prodotto_like_r_vista
				
datetime		ldt_data_inizio_nuova_r, ldt_da_data_r, ldt_a_data_r

datastore	lds_datastore

long			ll_i, ll_tot

double		ld_provvigione, ld_quan_scaglione

dw_selezione.accepttext()

ls_cod_tipo_listino_prodotto_r = dw_selezione.getitemstring(1,"cod_tipo_listino_prodotto")
ls_cod_valuta_r = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_prodotto_da_r = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a_r = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_prodotto_like_r = dw_selezione.getitemstring(1,"cod_prodotto_speciale")
ldt_da_data_r = dw_selezione.getitemdatetime(1,"da_data")
ldt_a_data_r = dw_selezione.getitemdatetime(1,"a_data")
ls_flag_estrai_ultima_r = dw_selezione.getitemstring(1,"flag_estrai_ultima")
ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
ldt_data_inizio_nuova_r = dw_selezione.getitemdatetime(1,"data_inizio_nuova")

//nella where c'è almeno il cod_azienda
ls_where = "where a.cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_tipo_listino_prodotto_r<>"" and not isnull(ls_cod_tipo_listino_prodotto_r) then
	ls_where += " and a.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_prodotto_r+"'"
end if
if ls_cod_valuta_r<>"" and not isnull(ls_cod_valuta_r) then
	ls_where += " and a.cod_valuta='"+ls_cod_valuta_r+"'"
end if
if ls_flag_estrai_ultima_r <> "S" then
	if not isnull(ldt_da_data_r) then
		ls_where += " and a.data_inizio_val>='"+string(ldt_da_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
	if not isnull(ldt_a_data_r) then
		ls_where += " and a.data_inizio_val<='"+string(ldt_a_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
end if

//questo deve essere necessariamente l'ultimo ---------------------

//verifica se hai scelto la ricerca per prodotto like
if ls_cod_prodotto_like_r <> "" and not isnull(ls_cod_prodotto_like_r) then
	//ricerca like
	ls_cod_prodotto_like_r_vista = ls_cod_prodotto_like_r
	wf_ricerca_prodotto_like(ls_cod_prodotto_like_r)
	ls_cod_prodotto_like_r = "a."+ls_cod_prodotto_like_r
	ls_where += " and "+ls_cod_prodotto_like_r
else
	//ricerca da-a
	if ls_cod_prodotto_da_r<>"" and not isnull(ls_cod_prodotto_da_r) then
		ls_where += " and a.cod_prodotto>='"+ls_cod_prodotto_da_r+"'"
	else
		ls_cod_prodotto_da_r = ""
	end if
	if ls_cod_prodotto_a_r<>"" and not isnull(ls_cod_prodotto_a_r) then
		ls_where += " and a.cod_prodotto<='"+ls_cod_prodotto_a_r+"'"
	else
		ls_cod_prodotto_a_r = ""
	end if
end if
//------------------------------------------------------------------------

//senza cliente e prodotto configurato da listini vendite
//ls_sql = "select '#SFUSO'+a.cod_prodotto as foglio,"+&
ls_sql = "select '#SFUSO' as foglio,"+&
				"a.cod_azienda,a.cod_tipo_listino_prodotto,a.cod_valuta,a.data_inizio_val,a.progressivo,"+&
				"a.cod_prodotto,p.des_prodotto,isnull(a.cod_cliente,'') as cod_cliente,"+&
				"'' as des_cliente,p.cod_cat_mer,"+&
				"a.scaglione_1,a.scaglione_2,a.scaglione_3,a.scaglione_4,a.scaglione_5,"+&
				"a.flag_sconto_mag_prezzo_1,a.flag_sconto_mag_prezzo_2,a.flag_sconto_mag_prezzo_3,"+&
					"a.flag_sconto_mag_prezzo_4,a.flag_sconto_mag_prezzo_5,"+&
				"a.variazione_1,a.variazione_2,a.variazione_3,a.variazione_4,"+&
					"a.variazione_5,"+&
				"a.flag_origine_prezzo_1,a.flag_origine_prezzo_2,a.flag_origine_prezzo_3,a.flag_origine_prezzo_4,"+&
					"a.flag_origine_prezzo_5,"+&
				"a.minimo_fatt_superficie,p.cod_misura_ven "+&
			"from listini_ven_comune as a "+&
			"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
				 "and p.cod_prodotto=a.cod_prodotto "+&
			ls_where +&
				"and a.cod_prodotto is not null and a.cod_cliente is null "

//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
if is_OCF<>"" then
	ls_sql += "and ((p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' and p.cod_cat_mer<>'"+is_OCF+"') or p.cod_cat_mer is null) "
else
	ls_sql += "and (p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer is null) "
end if
//----------------------------------------------------------------------------------------------------------------

ls_sql += "order by a.cod_prodotto,a.data_inizio_val desc,a.progressivo asc "

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini (SFUSO): " + ls_errore)
	return -1
end if

lds_datastore = create datastore
lds_datastore.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini (SFUSO): " + ls_errore)
	return -1
end if

lds_datastore.settransobject(sqlca)
if lds_datastore.retrieve() = -1 then
	destroy lds_datastore
	return -1
end if

ll_tot = lds_datastore.rowcount()
/*NOTA
In caso di impostaggio del periodo da-a devo considerare, a parità di prodotto e cliente,
sempre la prima riga, che grazie all'ordinamento impostato è quella + recente
Se invece è impostato il flag ultima condizione (cioè la + recente), non sarà passato
il filtro per data validità, ma prenderò sempre e comunque la prima riga
a parità di prodotto e cliente

STRUTTURA DEL DATASTORE
	foglio								cod_azienda					cod_tipo_listino_prodotto		cod_valuta
	data_inizio_val					progressivo					cod_prodotto					des_prodotto
	cod_cliente						des_cliente					cod_cat_mer					scaglione_1
	scaglione_2						scaglione_3					scaglione_4						scaglione_5
	flag_sco_mag_pr_1			flag_sco_mag_pr_2		flag_sco_mag_pr_3			flag_sc_mag_pr_4
	flag_sco_mag_pr_5			variazione_1				variazione_2					variazione_3
	variazione_4					variazione_5				flag_orig_prezzo_1			flag_orig_prezzo_2	
	flag_orig_prezzo_3			flag_orig_prezzo_4		flag_origine_prezzo_5		minimo_fatt_sup
	cod_misura_ven
*/
string			ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_foglio_cu, ls_cod_tipo_listino_prodotto_cu, &
				ls_cod_valuta_cu, ls_des_cliente_cu, ls_cod_cat_mer_cu, ls_cod_livello_prod_1,&
				ls_prodotto_corrente, ls_cliente_corrente, ls_valore, ls_flag_sconto_mag_prezzo, &
				ls_valore_sconto_mag_prezzo, ls_val_generico, ls_cod_variante, ls_des_prodotto, ls_appo, ls_cod_agente
				
long			ll_progressivo_cu, &
				ll_riga, ll_col, ll_col2, ll_i_scaglioni
				//ll_row_offset

datetime		ldt_data_val_cu, &
				ldt_data_val_corrente

decimal		ld_variazione, ld_valore, ld_val_generico

if ll_tot > 0 then
	ls_foglio_cu = lds_datastore.getitemstring(1, 1)
	
	iuo_excel.uof_crea_foglio(ls_foglio_cu)
	if not isnull(ls_cod_prodotto_like_r_vista) and ls_cod_prodotto_like_r_vista<> "" then
		ls_valore = "LISTINO GENERALE - Prodotto LIKE "+ls_cod_prodotto_like_r_vista
	else
		ls_valore = "LISTINO GENERALE da "+ls_cod_prodotto_da_r+" a "+ls_cod_prodotto_a_r
	end if
	
//	ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(1, 3)
//	ls_cod_valuta_cu = lds_datastore.getitemstring(1, 4)
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,1,ls_cod_tipo_listino_prodotto_cu,false,false,"none")
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,2,ls_cod_valuta_cu,false,false,"none")
	
	ll_riga = 1
	ll_col = 1
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col, ls_valore,false,true,"none")
end if

for ll_i = 1 to ll_tot
	yield()
	//TUTTO IN UNO STESSO FOGLIO UNICO "#SFUSO"
	ls_cod_prodotto_cu = lds_datastore.getitemstring(ll_i, 7)
	ls_cod_cliente_cu = lds_datastore.getitemstring(ll_i, 9)
	ldt_data_val_cu = lds_datastore.getitemdatetime(ll_i, 5)
	
	st_msg.text = "Elaborazione Prodotto Sfuso: #SFUSO"+&
				ls_cod_prodotto_cu+" :"+string(ll_i)+" di "+string(ll_tot)
	
	if ls_cod_prodotto_cu=ls_prodotto_corrente and &
					ls_cod_cliente_cu=ls_cliente_corrente then continue
	
	//memorizzo il prodotto e il cliente corrente
	ls_prodotto_corrente = ls_cod_prodotto_cu
	ls_cliente_corrente = ls_cod_cliente_cu
	ldt_data_val_corrente = ldt_data_val_cu
	
	//continuo a leggere gli altri valori
	ls_foglio_cu = lds_datastore.getitemstring(ll_i, 1)
	ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(ll_i, 3)
	ls_cod_valuta_cu = lds_datastore.getitemstring(ll_i, 4)
	ll_progressivo_cu = lds_datastore.getitemnumber(ll_i, 6)
	ls_des_cliente_cu = lds_datastore.getitemstring(ll_i, 10)
	ls_cod_cat_mer_cu = lds_datastore.getitemstring(ll_i, 11)
	
	ls_des_prodotto = lds_datastore.getitemstring(ll_i, 8)

	//--------------------------------------------------------------------------------------------
	if ll_i = 1 then //solo la prima volta metto l'intestazione
		ll_riga += 1
		ll_col = 1
		iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,"VALIDITA'",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col, "CODICE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,"DESCRIZIONE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"SCAGLIONE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"Q.TA' SCAGLIONE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"TIPO VAR.(*1)",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"UM",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"VALORE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"ORIGINE (*2)",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"MIN.SUPERFICIE",false,true,"35")
		ll_col +=1
		iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"PROVV.",false,true,"35")
	end if
	//-------------------------------------------------------------------------------------------
	
	//ripeto il ciclo fino a 5 volte se occorre in base al valore in variazione_i
	for ll_i_scaglioni=1 to 5
		yield()
		//leggo il valore dell variazione_i (i=1..5)
		ld_variazione = lds_datastore.getitemdecimal(ll_i, 22 + ll_i_scaglioni - 1) //variazione_iesima (1..5)
		if (not isnull(ld_variazione) and ld_variazione > 0 and ll_i_scaglioni>1) &
														or ll_i_scaglioni = 1 then
			//------------------------------------------------
			ll_riga += 1				//mi sposto di una riga in giù
			ll_col = 1					//torno alla colonna 1
			
			//validita
			iuo_excel.uof_scrivi_data(ls_foglio_cu,ll_riga, ll_col, ldt_data_val_corrente,false,false,"none","gg/mm/aaaa")
			
			//codice
			ll_col += 1
			//iuo_excel.uof_scriv	i			(ls_foglio_cu,ll_riga, 		ll_col,		ls_prodotto_corrente,false,false,"none")
			iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col,				ls_prodotto_corrente,	false,false,"none")
			
			//descrizione
			ll_col += 1
			iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_des_prodotto,false,false,"none")
			
			//n° scaglione (i-mo)
			ll_col += 1
			iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ll_i_scaglioni,false,false,"none","0")
			
			//qta scaglione (i-mo)
			ll_col += 1
			ld_val_generico = lds_datastore.getitemdecimal(ll_i, 12 + ll_i_scaglioni - 1)
			iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
			
			//salvo in una double la quan scaglione
			ld_quan_scaglione = ld_val_generico
			
			//tipo variazione (i-mo)
			ll_col += 1
			ls_val_generico = lds_datastore.getitemstring(ll_i, 17 + ll_i_scaglioni - 1)
			iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
			
			//um
			ll_col += 1
			ls_val_generico = lds_datastore.getitemstring(ll_i, 33)
			iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
			
			//valore (i-mo)
			setnull(ls_val_generico)
			ls_val_generico = lds_datastore.getitemstring(ll_i, 17 + ll_i_scaglioni - 1)	//riprendo il tipo variazione
			ld_val_generico = lds_datastore.getitemdecimal(ll_i, 22 + ll_i_scaglioni - 1)
			ls_appo = wf_calcola_valore(ls_cod_prodotto_cu, ls_val_generico,ld_val_generico)
			if ls_val_generico <> "P" then
				//and ls_val_generico <> "M" and ls_val_generico <> "A" then
				ll_col += 1
				//iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col,ls_appo,false,false,"none")
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
			else
				ll_col += 1
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
			end if
			
			//origine (i-mo)
			ll_col += 1
			ls_val_generico = lds_datastore.getitemstring(ll_i, 27 + ll_i_scaglioni - 1)
			iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
			
			//minimo sup
			ll_col += 1
			ld_val_generico = lds_datastore.getitemdecimal(ll_i, 32)
			iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
			
			//provvigione
			ll_col += 1
			
			setnull(ls_cod_agente)
			ls_cod_cliente_provv = "CODICEASSURDO"
			
			wf_get_provv_sfuso(	ls_cod_tipo_listino_prodotto_cu, ls_cod_valuta_cu,&
												ldt_data_val_cu, ls_cod_agente, &
												ls_cod_cliente_provv, ls_cod_prodotto_cu,&
												ld_quan_scaglione, 0,&
												ld_provvigione)
			
			//ld_val_generico = 0
			iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_provvigione,false,false,"none",is_formato_numero)
			
			//estrai classifica prodotto -------------------------------------------------------------------------------------------------
			select cod_livello_prod_1
			into :ls_cod_livello_prod_1
			from anag_prodotti
			where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu;
			
			if isnull(ls_cod_livello_prod_1) then ls_cod_livello_prod_1=""
			
			//codice
			ll_col += 1
			iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col, ls_cod_livello_prod_1,	false,false,"none")
			//-----------------------------------------------------------------------------------------------------------------------------------
		else
			exit
		end if
	next
	
//	if ll_i = ll_tot then
//		wf_scrivi_note(ls_foglio_cu, ll_riga, 2, true)
//	end if
	
	fl_riga = ll_riga
	fl_colonna = ll_col
		
	//end if
next

if ll_tot>0 then wf_scrivi_note(ls_foglio_cu, ll_riga, 2, true)

destroy lds_datastore
return 1
end function

public function integer wf_sfuso_cliente (ref long fl_riga, ref long fl_colonna);string 		ls_cod_prodotto_da_r,ls_cod_prodotto_a_r,ls_cod_tipo_listino_prodotto_r,&
				ls_cod_valuta_r, ls_flag_estrai_ultima_r, ls_flag_incorporo_iva_r,&
				ls_syntax, ls_errore, ls_sql, ls_where, ls_sql_cli, ls_syntax_cli, ls_errore_cli, ls_cod_cliente_cli,&
				ls_cod_prodotto_like_r, ls_cod_prodotto_like_r_vista
				
datetime		ldt_data_inizio_nuova_r, ldt_da_data_r, ldt_a_data_r

datastore	lds_datastore, lds_datastore_cli

long			ll_i, ll_tot, ll_tot_cli, ll_i_cli
//----
string			ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_foglio_cu, ls_cod_tipo_listino_prodotto_cu, &
				ls_cod_valuta_cu, ls_des_cliente_cu, ls_cod_cat_mer_cu, &
				ls_prodotto_corrente, ls_cliente_corrente, ls_valore, ls_flag_sconto_mag_prezzo, &
				ls_valore_sconto_mag_prezzo, ls_val_generico, ls_cod_variante, ls_des_prodotto, &
				ls_appo, ls_cod_agente, ls_cod_livello_prod_1, ls_cod_cliente
				
long			ll_progressivo_cu, &
				ll_riga, ll_col, ll_col2, ll_i_scaglioni
				//ll_row_offset

datetime		ldt_data_val_cu, &
				ldt_data_val_corrente
				
decimal		ld_variazione, ld_valore, ld_val_generico
double		ld_quan_scaglione, ld_provvigione
boolean		ib_nuovo_foglio = true
boolean		ib_scrivi_nota = false

dw_selezione.accepttext()
ls_cod_tipo_listino_prodotto_r = dw_selezione.getitemstring(1,"cod_tipo_listino_prodotto")
ls_cod_valuta_r = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_prodotto_da_r = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a_r = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_prodotto_like_r = dw_selezione.getitemstring(1,"cod_prodotto_speciale")
ldt_da_data_r = dw_selezione.getitemdatetime(1,"da_data")
ldt_a_data_r = dw_selezione.getitemdatetime(1,"a_data")
ls_flag_estrai_ultima_r = dw_selezione.getitemstring(1,"flag_estrai_ultima")
ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
ldt_data_inizio_nuova_r = dw_selezione.getitemdatetime(1,"data_inizio_nuova")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")

//nella where c'è almeno il cod_azienda
ls_where = "where a.cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_tipo_listino_prodotto_r<>"" and not isnull(ls_cod_tipo_listino_prodotto_r) then
	ls_where += " and a.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_prodotto_r+"'"
end if
if ls_cod_valuta_r<>"" and not isnull(ls_cod_valuta_r) then
	ls_where += " and a.cod_valuta='"+ls_cod_valuta_r+"'"
end if
if ls_flag_estrai_ultima_r <> "S" then
	if not isnull(ldt_da_data_r) then
		ls_where += " and a.data_inizio_val>='"+string(ldt_da_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
	if not isnull(ldt_a_data_r) then
		ls_where += " and a.data_inizio_val<='"+string(ldt_a_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
end if

//questo deve essere necessariamente l'ultimo ---------------------
//verifica se hai scelto la ricerca per prodotto like
if ls_cod_prodotto_like_r <> "" and not isnull(ls_cod_prodotto_like_r) then
	//ricerca like
	ls_cod_prodotto_like_r_vista = ls_cod_prodotto_like_r
	wf_ricerca_prodotto_like(ls_cod_prodotto_like_r)
	ls_cod_prodotto_like_r = "a."+ls_cod_prodotto_like_r
	ls_where += " and "+ls_cod_prodotto_like_r
else
	//ricerca da-a
	if ls_cod_prodotto_da_r<>"" and not isnull(ls_cod_prodotto_da_r) then
		ls_where += " and a.cod_prodotto>='"+ls_cod_prodotto_da_r+"'"
	else
		ls_cod_prodotto_da_r = ""
	end if
	if ls_cod_prodotto_a_r<>"" and not isnull(ls_cod_prodotto_a_r) then
		ls_where += " and a.cod_prodotto<='"+ls_cod_prodotto_a_r+"'"
	else
		ls_cod_prodotto_a_r = ""
	end if
end if

if not isnull(ls_cod_cliente) then
	ls_where += " and a.cod_cliente = '"+ls_cod_cliente+"'"
end if	
//------------------------------------------------------------------------

ls_sql_cli =  "select distinct a.cod_cliente,"+&
							"c.rag_soc_1+isnull(' ,'+c.localita,'')+isnull('('+c.provincia+')','') des_cliente "+&
					"from listini_ven_locale as a "+&
					"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
						 "and p.cod_prodotto=a.cod_prodotto "+&
					"join anag_clienti as c on c.cod_azienda=a.cod_azienda "+&
						 "and c.cod_cliente=a.cod_cliente "+&
					ls_where +&
						"and a.cod_prodotto is not null and a.cod_cliente is not null "

//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
if is_OCF<>"" then
	ls_sql_cli += "and ((p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' and p.cod_cat_mer<>'"+is_OCF+"') or p.cod_cat_mer is null) "
else
	ls_sql_cli += "and (p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer is null) "
end if
//----------------------------------------------------------------------------------------------------------------

ls_sql_cli += "order by a.cod_cliente "

ls_syntax_cli = sqlca.syntaxfromsql(ls_sql_cli, 'style(type=grid)', ls_errore_cli)

if not isnull(ls_errore_cli) and len(trim(ls_errore_cli)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini clienti (SFUSO-CLIENTI): " + ls_errore_cli)
	return -1
end if

lds_datastore_cli = create datastore
lds_datastore_cli.create(ls_syntax_cli, ls_errore_cli)

if not isnull(ls_errore_cli) and len(trim(ls_errore_cli)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini  clienti (SFUSO-CLIENTI): " + ls_errore_cli)
	return -1
end if

lds_datastore_cli.settransobject(sqlca)
if lds_datastore_cli.retrieve() = -1 then
	destroy lds_datastore_cli
	return -1
end if
					
ll_tot_cli = lds_datastore_cli.rowcount()

for ll_i_cli = 1 to ll_tot_cli
	yield()	
	
	//ogni iterata è un foglio di lavoro
	ls_cod_cliente_cli = lds_datastore_cli.getitemstring(ll_i_cli, 1)
	ls_des_cliente_cu = lds_datastore_cli.getitemstring(ll_i_cli, 2)
	ls_foglio_cu = "#SFUSO("+ls_cod_cliente_cli+")"
	
	//creo il foglio di lavoro opportuno
	iuo_excel.uof_crea_foglio(ls_foglio_cu)
	
	//scrivo il titolo
	ll_riga = 1
	
	ll_col = 1
	
	if ls_cod_prodotto_like_r_vista<> "" and not isnull(ls_cod_prodotto_like_r_vista) then
		ls_valore = ls_des_cliente_cu+" - Prodotto LIKE "+ls_cod_prodotto_like_r_vista
	else
		ls_valore = ls_des_cliente_cu+" da "+ls_cod_prodotto_da_r+" a "+ls_cod_prodotto_a_r
	end if
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col, ls_valore,false,true,"none")
	
	//RECUPERARE l'AGENTE ********************
	select cod_agente_1
	into :ls_cod_agente
	from anag_clienti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_cliente=:ls_cod_cliente_cli;
	
	if isnull(ls_cod_agente) then ls_cod_agente = ""
	
	if ls_cod_agente<> "" then
		select rag_soc_1
		into :ls_valore
		from anag_agenti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_agente=:ls_cod_agente;
			
		iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col+10, ls_cod_agente,false,true,"none")
		iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col+11, ls_valore,false,true,"none")
	end if
	
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col+9, "AGENTE:",false,true,"none")	
	
	
	//INTESTAZIONE----------------------------------------------------------------
	ll_riga += 1
	ll_col = 1
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,"VALIDITA'",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col, "CODICE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,"DESCRIZIONE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"SCAGLIONE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"Q.TA' SCAGLIONE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"TIPO VAR.(*1)",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"UM",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"VALORE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"ORIGINE (*2)",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"MIN.SUPERFICIE",false,true,"35")
	ll_col +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga, ll_col,"PROVV.",false,true,"35")
	//-------------------------------------------------------------------------------------------
	
	//elaboriamo le righe degli sfusi
	ls_sql = "select '#SFUSO('+a.cod_cliente+')' as foglio,"+&
				"a.cod_azienda,a.cod_tipo_listino_prodotto,a.cod_valuta,a.data_inizio_val,a.progressivo,"+&
				"a.cod_prodotto,p.des_prodotto,a.cod_cliente,"+&
				"c.rag_soc_1+isnull(' ,'+c.localita,'')+isnull('('+c.provincia+')','') des_cliente,p.cod_cat_mer,"+&
				"a.scaglione_1,a.scaglione_2,a.scaglione_3,a.scaglione_4,a.scaglione_5,"+&
				"a.flag_sconto_mag_prezzo_1,a.flag_sconto_mag_prezzo_2,a.flag_sconto_mag_prezzo_3,"+&
					"a.flag_sconto_mag_prezzo_4,a.flag_sconto_mag_prezzo_5,"+&
				"a.variazione_1,a.variazione_2,a.variazione_3,a.variazione_4,"+&
					"a.variazione_5,"+&
				"a.flag_origine_prezzo_1,a.flag_origine_prezzo_2,a.flag_origine_prezzo_3,a.flag_origine_prezzo_4,"+&
					"a.flag_origine_prezzo_5,"+&
				"a.minimo_fatt_superficie,p.cod_misura_ven "+&
			"from listini_ven_locale as a "+&
			"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
				 "and p.cod_prodotto=a.cod_prodotto "+&
			"join anag_clienti as c on c.cod_azienda=a.cod_azienda "+&
				 "and c.cod_cliente=a.cod_cliente "+&
			ls_where +&
				"and a.cod_prodotto is not null and a.cod_cliente is not null "+&
				"and a.cod_cliente='"+ls_cod_cliente_cli+"' "
			
	
	//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
	if is_OCF<>"" then
		ls_sql += "and ((p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' and p.cod_cat_mer<>'"+is_OCF+"') or p.cod_cat_mer is null) "
	else
		ls_sql += "and (p.cod_cat_mer<>'"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer is null) "
	end if
	//----------------------------------------------------------------------------------------------------------------
	
	ls_sql += "order by a.cod_cliente,a.cod_prodotto,a.data_inizio_val desc,a.progressivo asc "
	
	
	ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)
	if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
		g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini (SFUSO-CLIENTI): " + ls_errore)
		return -1
	end if
	
	lds_datastore = create datastore
	lds_datastore.create(ls_syntax, ls_errore)
	
	if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
		destroy lds_datastore
		g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini (SFUSO): " + ls_errore)
		return -1
	end if
	
	lds_datastore.settransobject(sqlca)
	if lds_datastore.retrieve() = -1 then
		destroy lds_datastore
		return -1
	end if
	
	ll_tot = lds_datastore.rowcount()
	/*NOTA
	In caso di impostaggio del periodo da-a devo considerare, a parità di prodotto e cliente,
	sempre la prima riga, che grazie all'ordinamento impostato è quella + recente
	Se invece è impostato il flag ultima condizione (cioè la + recente), non sarà passato
	il filtro per data validità, ma prenderò sempre e comunque la prima riga
	a parità di prodotto e cliente
	
	STRUTTURA DEL DATASTORE
		foglio								cod_azienda					cod_tipo_listino_prodotto		cod_valuta
		data_inizio_val					progressivo					cod_prodotto					des_prodotto
		cod_cliente						des_cliente					cod_cat_mer					scaglione_1
		scaglione_2						scaglione_3					scaglione_4						scaglione_5
		flag_sco_mag_pr_1			flag_sco_mag_pr_2		flag_sco_mag_pr_3			flag_sc_mag_pr_4
		flag_sco_mag_pr_5			variazione_1				variazione_2					variazione_3
		variazione_4					variazione_5				flag_orig_prezzo_1			flag_orig_prezzo_2	
		flag_orig_prezzo_3			flag_orig_prezzo_4		flag_origine_prezzo_5		minimo_fatt_sup
		cod_misura_ven
	*/
	for ll_i = 1 to ll_tot
		yield()
		ls_cod_prodotto_cu = lds_datastore.getitemstring(ll_i, 7)
		ls_cod_cliente_cu = lds_datastore.getitemstring(ll_i, 9)
		ldt_data_val_cu = lds_datastore.getitemdatetime(ll_i, 5)
		
		st_msg.text = "Elaborazione Prodotto Sfuso per Clienti: #SFUSO"+&
				ls_cod_prodotto_cu+"("+ls_cod_cliente_cu+") :"+&
				string(ll_i)+" di "+string(ll_tot)
		
		//controllo se devo scartare la riga (data più vecchia o non ultima condizione)
		if ls_cod_prodotto_cu=ls_prodotto_corrente and &
					ls_cod_cliente_cu=ls_cliente_corrente then continue
		
		//memorizzo il prodotto e il cliente corrente
		ls_prodotto_corrente = ls_cod_prodotto_cu
		ls_cliente_corrente = ls_cod_cliente_cu
		ldt_data_val_corrente = ldt_data_val_cu
		
		//continuo a leggere gli altri valori
		ls_foglio_cu = lds_datastore.getitemstring(ll_i, 1)
		ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(ll_i, 3)
		ls_cod_valuta_cu = lds_datastore.getitemstring(ll_i, 4)
		ll_progressivo_cu = lds_datastore.getitemnumber(ll_i, 6)
		ls_des_cliente_cu = lds_datastore.getitemstring(ll_i, 10)
		ls_cod_cat_mer_cu = lds_datastore.getitemstring(ll_i, 11)
		ls_des_prodotto = lds_datastore.getitemstring(ll_i, 8)
		
		//ripeto il ciclo fino a 5 volte se occorre in base al valore in variazione_i
		for ll_i_scaglioni=1 to 5
			yield()
			//leggo il valore dell variazione_i (i=1..5)
			ld_variazione = lds_datastore.getitemdecimal(ll_i, 22 + ll_i_scaglioni - 1) //variazione_iesima (1..5)
			
			if (not isnull(ld_variazione) and ld_variazione > 0 and ll_i_scaglioni>1) &
														or ll_i_scaglioni = 1 then
				
				//------------------------------------------------
				ll_riga += 1				//mi sposto di una riga in giù
				ll_col = 1					//torno alla colonna 1
				
				//validita
				iuo_excel.uof_scrivi_data(ls_foglio_cu,ll_riga, ll_col,ldt_data_val_corrente,false,false,"none","gg/mm/aaaa")
				
				//codice
				ll_col += 1
				iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col,ls_prodotto_corrente,false,false,"none")
				
				//descrizione
				ll_col += 1
				iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_des_prodotto,false,false,"none")
				
				//n° scaglione (i-mo)
				ll_col += 1
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ll_i_scaglioni,false,false,"none","0")
				
				//qta scaglione (i-mo)
				ll_col += 1
				ld_val_generico = lds_datastore.getitemdecimal(ll_i, 12 + ll_i_scaglioni - 1)
				
				//salvo la quan scaglione (mi serve per recuperare la provvigione, vedi piu' sotto)
				ld_quan_scaglione = ld_val_generico
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
				
				//tipo variazione (i-mo)
				ll_col += 1
				ls_val_generico = lds_datastore.getitemstring(ll_i, 17 + ll_i_scaglioni - 1)
				iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
				
				//um
				ll_col += 1
				ls_val_generico = lds_datastore.getitemstring(ll_i, 33)
				iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
				
				//valore (i-mo)
				setnull(ls_val_generico)
				ls_val_generico = lds_datastore.getitemstring(ll_i, 17 + ll_i_scaglioni - 1)	//riprendo il tipo variazione
				ld_val_generico = lds_datastore.getitemdecimal(ll_i, 22 + ll_i_scaglioni - 1)
				ls_appo = wf_calcola_valore(ls_cod_prodotto_cu, ls_val_generico,ld_val_generico)
				if ls_val_generico <> "P" then
					//and ls_val_generico <> "M" and ls_val_generico <> "A" then
					ll_col += 1
					//iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col,ls_appo,false,false,"none")
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
				else
					ll_col += 1
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
				end if
				
				//origine (i-mo)
				ll_col += 1
				ls_val_generico = lds_datastore.getitemstring(ll_i, 27 + ll_i_scaglioni - 1)
				iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga, ll_col,ls_val_generico,false,false,"none")
				
				//minimo sup
				ll_col += 1
				ld_val_generico = lds_datastore.getitemdecimal(ll_i, 32)
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_val_generico,false,false,"none",is_formato_numero)
				
				//provvigione
				ll_col += 1
				
				
				if not isnull(ls_cod_agente) and ls_cod_agente<>"" then
					wf_get_provv_sfuso(	ls_cod_tipo_listino_prodotto_cu, ls_cod_valuta_cu,&
												ldt_data_val_cu, ls_cod_agente, &
												ls_cod_cliente_cu, ls_cod_prodotto_cu,&
												ld_quan_scaglione, 0,&
												ld_provvigione)
				else
					ld_provvigione = 0
				end if
				
				iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga, ll_col,ld_provvigione,false,false,"none",is_formato_numero)
				//--------------------------------------------
				
				//estrai classifica prodotto -------------------------------------------------------------------------------------------------
				select cod_livello_prod_1
				into :ls_cod_livello_prod_1
				from anag_prodotti
				where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu;
				
				if isnull(ls_cod_livello_prod_1) then ls_cod_livello_prod_1=""
				
				//codice
				ll_col += 1
				iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga, ll_col, ls_cod_livello_prod_1,	false,false,"none")
				//-----------------------------------------------------------------------------------------------------------------------------------
			else
				exit
			end if
		next
		
	next
	
	wf_scrivi_note(ls_foglio_cu, ll_riga, 2, true)
	fl_riga = ll_riga
	fl_colonna = ll_col
next

destroy lds_datastore_cli
destroy lds_datastore

return 1



















end function

public function integer wf_configurato (ref long fl_riga, ref long fl_colonna);string 		ls_cod_prodotto_da_r,ls_cod_prodotto_a_r,ls_cod_tipo_listino_prodotto_r,&
				ls_cod_valuta_r, ls_flag_estrai_ultima_r, ls_flag_incorporo_iva_r,&
				ls_syntax, ls_errore, ls_sql, ls_where, ls_where2, &
				ls_cod_prodotto_like_r, ls_des_prodotto_listino, ls_flag_ordina_per_prodotto
				
datetime		ldt_data_inizio_nuova_r, ldt_da_data_r, ldt_a_data_r

decimal		ld_min_fat_sup_cu

datastore	lds_datastore

long			ll_i, ll_tot, ll_riga_appo, ll_col_appo

dw_selezione.accepttext()

ls_cod_tipo_listino_prodotto_r = dw_selezione.getitemstring(1,"cod_tipo_listino_prodotto")
ls_cod_valuta_r = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_prodotto_da_r = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a_r = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_prodotto_like_r = dw_selezione.getitemstring(1,"cod_prodotto_speciale")
ldt_da_data_r = dw_selezione.getitemdatetime(1,"da_data")
ldt_a_data_r = dw_selezione.getitemdatetime(1,"a_data")
ls_flag_estrai_ultima_r = dw_selezione.getitemstring(1,"flag_estrai_ultima")
ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
ldt_data_inizio_nuova_r = dw_selezione.getitemdatetime(1,"data_inizio_nuova")

ls_flag_ordina_per_prodotto = dw_selezione.getitemstring(1,"flag_ordina_per_prodotto")
if isnull(ls_flag_ordina_per_prodotto) or ls_flag_ordina_per_prodotto="" then ls_flag_ordina_per_prodotto = "N"

//nella where c'è almeno il cod_azienda
ls_where = "where a.cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_tipo_listino_prodotto_r<>"" and not isnull(ls_cod_tipo_listino_prodotto_r) then
	ls_where += " and a.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_prodotto_r+"'"
end if
if ls_cod_valuta_r<>"" and not isnull(ls_cod_valuta_r) then
	ls_where += " and a.cod_valuta='"+ls_cod_valuta_r+"'"
end if
if ls_flag_estrai_ultima_r <> "S" then
	if not isnull(ldt_da_data_r) then
		ls_where += " and a.data_inizio_val>='"+string(ldt_da_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
	if not isnull(ldt_a_data_r) then
		ls_where += " and a.data_inizio_val<='"+string(ldt_a_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
end if
ls_where2 = ls_where
//questo deve essere necessariamente l'ultimo ---------------------

//verifica se hai scelto la ricerca per prodotto like
if ls_cod_prodotto_like_r <> "" and not isnull(ls_cod_prodotto_like_r) then
	//ricerca like
	wf_ricerca_prodotto_like(ls_cod_prodotto_like_r)
	ls_cod_prodotto_like_r = "a."+ls_cod_prodotto_like_r
	ls_where += " and "+ls_cod_prodotto_like_r
else
	//ricerca da-a
	if ls_cod_prodotto_da_r<>"" and not isnull(ls_cod_prodotto_da_r) then
		ls_where += " and a.cod_prodotto>='"+ls_cod_prodotto_da_r+"'"
	end if
	if ls_cod_prodotto_a_r<>"" and not isnull(ls_cod_prodotto_a_r) then
		ls_where += " and a.cod_prodotto<='"+ls_cod_prodotto_a_r+"'"
	end if
end if
//------------------------------------------------------------------------

//senza cliente e prodotto configurato da listini vendite comune
ls_sql = "select a.cod_prodotto as foglio,"+&
				"a.cod_azienda,a.cod_tipo_listino_prodotto,a.cod_valuta,a.data_inizio_val,a.progressivo,"+&
				"a.cod_prodotto,isnull(a.cod_cliente,'') as cod_cliente,'' as des_cliente,p.cod_cat_mer,"+&
				"a.scaglione_1,a.scaglione_2,a.scaglione_3,a.scaglione_4,a.scaglione_5,"+&
				"a.flag_sconto_mag_prezzo_1,a.flag_sconto_mag_prezzo_2,a.flag_sconto_mag_prezzo_3,"+&
					"a.flag_sconto_mag_prezzo_4,a.flag_sconto_mag_prezzo_5,"+&
				"a.flag_origine_prezzo_1,a.flag_origine_prezzo_2,a.flag_origine_prezzo_3,a.flag_origine_prezzo_4,"+&
					"a.flag_origine_prezzo_5,"+&
				"a.minimo_fatt_superficie,p.cod_misura_ven "+&
			"from listini_ven_comune as a "+&
			"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
					 "and p.cod_prodotto=a.cod_prodotto "+&
			ls_where +&
					" and a.cod_prodotto is not null and a.cod_cliente is null "
					
//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
if is_OCF<>"" then
	ls_sql += "and (p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer='"+is_OCF+"') "
else
	ls_sql += "and p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' "
end if
//----------------------------------------------------------------------------------------------------------------
				
				
ls_sql += "order by a.cod_prodotto,a.data_inizio_val desc,a.progressivo asc "

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini (CONFIGURATO): " + ls_errore)
	return -1
end if

lds_datastore = create datastore
lds_datastore.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini (CONFIGURATO): " + ls_errore)
	return -1
end if

lds_datastore.settransobject(sqlca)
if lds_datastore.retrieve() = -1 then
	destroy lds_datastore
	return -1
end if

ll_tot = lds_datastore.rowcount()
/*NOTA
In caso di impostaggio del periodo da-a devo considerare, a parità di prodotto e cliente,
sempre la prima riga, che grazie all'ordinamento impostato è quella + recente
Se invece è impostato il flag ultima condizione (cioè la + recente), non sarà passato
il filtro per data validità, ma prenderò sempre e comunque la prima riga
a parità di prodotto e cliente

STRUTTURA DEL DATASTORE
	foglio								cod_azienda				cod_tipo_listino_prodotto		cod_valuta
	data_inizio_val					progressivo				cod_prodotto					cod_cliente
	des_cliente						cod_cat_mer			scaglione_1						scaglione_2
	scaglione_3						scaglione_4				scaglione_5						flag_sco_mag_pr_1
	flag_sco_mag_pr_2			flag_sco_mag_pr_3	flag_sc_mag_pr_4				flag_sco_mag_pr_5,
	flag_orig_prezzo_1			flag_orig_prezzo_2	flag_orig_prezzo_3			flag_orig_prezzo_4
	flag_origine_prezzo_5		minimo_fatt_sup		cod_misura_ven
*/

string			ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_foglio_cu, ls_cod_tipo_listino_prodotto_cu, &
				ls_cod_valuta_cu, ls_des_cliente_cu, ls_cod_cat_mer_cu, &
				ls_prodotto_corrente, ls_cliente_corrente, ls_valore, ls_flag_sconto_mag_prezzo, &
				ls_valore_sconto_mag_prezzo, ls_val_generico, ls_cod_variante
				
long			ll_progressivo_cu, &
				ll_count_dim_x, ll_i_dim_x, ll_count_dim_y, ll_i_dim_y, ll_riga_var, ll_col_var, ll_col_var2,&
				ll_tot_varianti, ll_i_varianti, ll_i_scaglioni
				//ll_row_offset

datetime		ldt_data_val_cu, &
				ldt_data_val_corrente
				
decimal		ld_dim_x[], ld_dim_y[], ld_null_v[], ld_valore, ld_val_generico

datastore	lds_varianti

for ll_i = 1 to ll_tot
	yield()
	//ogni iterata in questo ciclo è un foglio di lavoro excel...
	ls_cod_prodotto_cu = lds_datastore.getitemstring(ll_i, 7)
	ls_cod_cliente_cu = lds_datastore.getitemstring(ll_i, 8)
	ldt_data_val_cu = lds_datastore.getitemdatetime(ll_i, 5)
	
	st_msg.text = "Elaborazione Prodotto Configurato: "+ls_cod_prodotto_cu+" :"+&
						string(ll_i)+" di "+string(ll_tot)
	
	if ls_cod_prodotto_cu=ls_prodotto_corrente and &
					ls_cod_cliente_cu=ls_cliente_corrente then continue
	
	//memorizzo il prodotto e il cliente corrente
	ls_prodotto_corrente = ls_cod_prodotto_cu
	ls_cliente_corrente = ls_cod_cliente_cu
	ldt_data_val_corrente = ldt_data_val_cu
	
	//continuo a leggere gli altri valori
	ls_foglio_cu = lds_datastore.getitemstring(ll_i, 1)
	ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(ll_i, 3)
	ls_cod_valuta_cu = lds_datastore.getitemstring(ll_i, 4)
	ll_progressivo_cu = lds_datastore.getitemnumber(ll_i, 6)
	ls_des_cliente_cu = lds_datastore.getitemstring(ll_i, 9)
	ls_cod_cat_mer_cu = lds_datastore.getitemstring(ll_i, 10)
	
	ld_min_fat_sup_cu = lds_datastore.getitemdecimal(ll_i, 26)
	
	//creo il foglio di lavoro opportuno
	iuo_excel.uof_crea_foglio(ls_foglio_cu)
	
	//si tratta di un prodotto configurato (no sfuso): devo caricare la griglia delle dimensioni
	//intanto scrivo il titolo nel foglio di lavoro ----------------------------------------------
//	ll_row_offset = 0
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,1,ls_cod_tipo_listino_prodotto_cu,false,false,"none")
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,2,ls_cod_valuta_cu,false,false,"none")	
	
	select des_prodotto
	into :ls_des_prodotto_listino
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu;
	
	if isnull(ls_des_prodotto_listino) or ls_des_prodotto_listino="" then
		ls_valore = "LISTINO GENERALE (DAL "+string(date(ldt_data_val_cu))+")"
	else
		ls_valore = "LISTINO GENERALE - "+ls_des_prodotto_listino+" - (DAL "+string(date(ldt_data_val_cu))+")"
	end if
		
	iuo_excel.uof_scrivi(ls_foglio_cu,1,1,ls_valore,false,true,"none")
	//--------------------------------------------------------------------------------------------
	
	//carico le due diverse dimensioni per il prodotto che sto scorrendo
	ld_dim_x = ld_null_v
	ld_dim_y = ld_null_v
	if wf_carica_dimensioni(ls_cod_prodotto_cu, ld_dim_x[], ld_dim_y[]) = -1 then
//		destroy lds_datastore;
//		rollback;
//		return -1
	end if
	
	ll_i_dim_y = 1
	ll_i_dim_x = 1
	
	if upperbound(ld_dim_x[])>0 and upperbound(ld_dim_y[])>0 then
		//devo disegnare la griglia delle dimensioni		
		iuo_excel.uof_scrivi(ls_foglio_cu,2, 1,"DIM",false,true,"36")
		
		//scrivo la dim 1 in orizzontale
		ll_count_dim_x = upperbound(ld_dim_x)
		for ll_i_dim_x = 1 to ll_count_dim_x
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu,2, ll_i_dim_x + 1,ld_dim_x[ll_i_dim_x],false,true,"36")
		next
		
		//scrivo la dim 2 in verticale + le relative variazioni incolonnate per dim 1
		ll_count_dim_y = upperbound(ld_dim_y)
		for ll_i_dim_y = 1 to ll_count_dim_y
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu, ll_i_dim_y + 2, 1,ld_dim_y[ll_i_dim_y],false,true,"36")
			
			//ora lancio la query che mi deve caricare le variazioni fissate le due coordinate dim1 e dim2
			setnull(ld_valore)
			for ll_i_dim_x = 1 to ll_count_dim_x
				yield()
				wf_elabora_dimensioni(ls_cod_tipo_listino_prodotto_cu, &
											ls_cod_valuta_cu,&
											ldt_data_val_cu, &
											ll_progressivo_cu,&
											ld_dim_x[ll_i_dim_x], ld_dim_y[ll_i_dim_y], &
											ld_valore,&
											ls_flag_sconto_mag_prezzo, ls_valore_sconto_mag_prezzo, true)
											
				if ls_flag_sconto_mag_prezzo <> "P" then 
					//and ls_flag_sconto_mag_prezzo <> "M" and ls_flag_sconto_mag_prezzo <> "A" then
					iuo_excel.uof_scrivi_codice(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ls_valore_sconto_mag_prezzo,false,false,"none")
				else
					//dato numerico puro (Aumento-Diminuzione-Prezzo)
					iuo_excel.uof_scrivi_dec(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ld_valore,false,false,"none", is_formato_numero)
				end if
			next
		next
	end if
	
	//scrivo il min fatt sup
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_i_dim_y + 4, 1, "MFsup:",false,true,"36")
	if isnull(ld_min_fat_sup_cu) then ld_min_fat_sup_cu = 0
	iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_i_dim_y + 4, 2, ld_min_fat_sup_cu,false,false,"none",is_formato_numero)
	
	
	//alla fine di questo ciclo ho disegnato la griglia delle DIM
	//conservo in ll_i_dim_y e ll_i_dim_x i valori dell'angolo in basso a destra					
	//indice di riga excel:				ll_i_dim_y
	//indice di colonna excel:			ll_i_dim_x
						
	//devo ora scrivere la griglia delle varianti
	//mi sposto di due colonne a destra e comincio a scrivere dalla seconda riga con VAR
	ll_riga_var = 2
	ll_col_var = upperbound(ld_dim_x[]) + 1 + 2 	//dimX + DIM + 2
	ll_col_var2 = ll_col_var	//memorizzo da quale colonna parto
	
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,"VAR",false,true,"40")
	
	//scrivo il resto della intestazione della griglia VAR ------------------------------------					
	//listino standard: prodotto CONFIGURATO
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2, "CLIENTE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,"RAG. SOC.",false,true,"40")
	
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIANTE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"DES. VARIANTE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"SCAGLIONE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"TIPO VAR.(*1)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIAZ.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"ORIGINE (*2)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"PROVV.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"COD.PRODOTTO PADRE.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VERS.DEFAULT",false,true,"40")
	
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"ESCLUDI LS",false,true,"40")
	//-------------------------------------------------------------------------------------------
	lds_varianti = create datastore
	ll_tot_varianti = wf_carica_ds_varianti(lds_varianti,ls_cod_tipo_listino_prodotto_cu, &
								ls_cod_valuta_cu, ldt_data_val_cu,&
								ll_progressivo_cu, true)
	if ll_tot_varianti > 0 then
		/*Struttura del datastore
		cod_azienda						cod_tipo_listino_prodotto		cod_valuta						data_inizio_val
		progressivo						cod_prodotto_listino			cod_cliente						rag_soc_1
		cod_prodotto_figlio			des_prodotto					scaglione_1						scaglione_2
		scaglione_3						scaglione_4						scaglione_5						flag_sconto_mag_prezzo_1	
		flag_sconto_mag_prezzo_2	flag_sconto_mag_prezzo_3	flag_sconto_mag_prezzo_4	flag_sconto_mag_prezzo_5
		variazione_1					variazione_2					variazione_3					variazione_4
		variazione_5					flag_origine_prezzo_1		flag_origine_prezzo_2		flag_origine_prezzo_3
		flag_origine_prezzo_4		flag_origine_prezzo_5		provvigione						cod_prodotto_padre
		versione_default			flag_escludi_linee_sconto
		*/
		for ll_i_varianti = 1 to ll_tot_varianti
			yield()
			//ripeto il ciclo fino a 5 volte se occorre in base al valore in variazione_i
			for ll_i_scaglioni=1 to 5
				yield()
				//leggo il valore dell variazione_i (i=1..5)
				ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_iesima (1..5)
				if (not isnull(ld_val_generico) and ld_val_generico > 0 and ll_i_scaglioni>1) &
																or ll_i_scaglioni = 1 then
					//------------------------------------------------
					
					ll_riga_var += 1				//mi sposto di una riga in giù
					ll_col_var2 = ll_col_var		//torno alla colonna in cui è specificato "VAR"
					
					//scrivo n° di riga
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti),false,true,"none")
					//iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti + ll_i_scaglioni - 1),false,true,"none")
					
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 7) //qui ho il cod_cliente
					if ls_val_generico <> "" then
						ll_col_var2 += 1
						iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
						ll_col_var2 += 1
						ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 8) //qui ho la rag_soc_1
						iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					else
						//lascio le due celle vuote
						ll_col_var2 += 1
						iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,"",false,false,"none")
						ll_col_var2 += 1
						ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 7)
						iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,"",false,false,"none")
					end if
					//inserisco variante e desc variante
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 9) //variante
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//memorizzo il cod_prodotto (variante)
					ls_cod_variante = ls_val_generico
					//------------------
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 10)	//des_variante
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//il resto per il listino del prodotto configurato (con o senza cliente) è lo stesso
					//inserisco da scaglione in poi
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 11 + ll_i_scaglioni - 1) //scaglione_i
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 16 + ll_i_scaglioni - 1)	//flag_sconto_mag_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//qui occorre interpretare la variazione in base al flag_sconto_mag_prezzo_i
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_i
				
					string ls_appo
					
					ls_appo = wf_calcola_valore(ls_cod_variante, ls_val_generico,ld_val_generico)
					if ls_val_generico <> "P" then//and ls_val_generico <> "M" and ls_val_generico <> "A" then
						ll_col_var2 += 1
						//iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_appo,false,false,"none")
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					else
						ll_col_var2 += 1
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					end if
					//------------------------------------

					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 26 + ll_i_scaglioni - 1)	//flag_origine_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 31)	//provvigione
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 32)	//cod_prodotto_padre
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 33)	//versione default
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 34)	//flag_escludi linee sconto
					if isnull(ls_val_generico) or ls_val_generico="" then ls_val_generico = "N"
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//------------------------------------------------
				else
					exit
				end if
			next
		next
	else
		//return 1
	end if
	destroy lds_varianti
	
	wf_scrivi_note(ls_foglio_cu, ll_riga_var, ll_col_var, false)

	fl_riga = ll_i_dim_y
	fl_colonna = ll_i_dim_x
		
	//end if
	
	if ls_flag_ordina_per_prodotto="S" then
		wf_configurato_cliente(ll_riga_appo, ll_col_appo, ls_cod_prodotto_cu)
	end if
next

destroy lds_datastore
return 1
end function

public function string wf_calcola_valore (string fs_cod_prodotto, string fs_flag_sconto_mag_prezzo, ref decimal fd_valore);string ls_return
//decimal ld_dec_appo

//ld_dec_appo = fd_valore

//((devo incorporare l'iva?
if is_flag_incorporo_iva = "S" then
	//l'iva va incorporata solo se =P o =A o =D; se S o M non va fatto
	if fs_flag_sconto_mag_prezzo <> "S" and fs_flag_sconto_mag_prezzo<>"M" then
		wf_incorpora_iva(fs_cod_prodotto, fd_valore)
	end if
else
end if

choose case fs_flag_sconto_mag_prezzo
	case "S"  // sconto in %								(-xx.yy%)
		fd_valore = round(fd_valore, 2)
		ls_return = string(fd_valore)
		ls_return = "-" + ls_return + "%"
		
	case "M" //maggiorazione in %						(+xx.yy%)
		fd_valore = round(fd_valore, 2)
		ls_return = string(fd_valore)
		ls_return = "+"+ls_return + "%"
		
	case "A" //aumento									(+xxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		fd_valore = round(fd_valore, 2)
		ls_return = "+" + string(fd_valore)
		
	case "D" //diminuzione								(-xxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		fd_valore = round(fd_valore, 2)
		ls_return = "-" + string(fd_valore)
		
	case "P"  //prezzo										(xxxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		// verifoco il formato dati quanti decimali ha dopo la virgola
		fd_valore = round(fd_valore, len( mid(is_formato_numero, pos(is_formato_numero,","))))
		ls_return = string(fd_valore)
		wf_rimpiazza(ls_return)
		
	case else
		ls_return = "errore"
		
end choose

return ls_return
		
end function

public subroutine wf_ricerca_prodotto_like (ref string fs_stringa_ricerca);long			ll_i, ll_y, ll_pos


// ****************** aggiungo in automatico un asterisco alla fine della stringa *******
if right(trim(fs_stringa_ricerca), 1) <> "*" and pos(fs_stringa_ricerca, "&") < 1 then
	fs_stringa_ricerca += "*"
end if
// ***************************************************************************************

ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next


//****************** correzione ricerca con apostrofo - Michele 23/04/2002 ******************

ll_pos = 1

do

   ll_pos = pos(fs_stringa_ricerca,"'",ll_pos)
	
   if ll_pos <> 0 then
      fs_stringa_ricerca = replace(fs_stringa_ricerca,ll_pos,1,"''")
      ll_pos = ll_pos + 2
   end if	

loop while ll_pos <> 0

//*******************************************************************************************

if not isnull(fs_stringa_ricerca) and len(fs_stringa_ricerca) > 0 then
	if pos(fs_stringa_ricerca, "%") > 0 then 
		fs_stringa_ricerca = "cod_prodotto like '" + fs_stringa_ricerca + "'"
	else
		fs_stringa_ricerca = "cod_prodotto = '" + fs_stringa_ricerca + "'"
	end if
end if

end subroutine

public function string wf_estrai_prodotto_cliente (string fs_input, ref string fs_prodotto, ref string fs_cliente);long ll_pos, ll_pos2
string ls_appo, ls_msg

//controlla l'inputo del nome del foglio excel --------------------------
/*
Gli unici casi ammessi sono:
1.	CODICEPRODOTTO														es. "A020"
2. CODICEPRODOTTO(CODICECLIENTE)							es. "A020(0318)"
3. #SFUSO
4. #SFUSO(CODICECLIENTE)											es. "#SFUSO(0318)"

la procedura controlla anche che CODICEPRODOTTO e  CODICECLIENTE esistano nelle anagrafiche
Se tutto ok negli argomenti byref fs_prodotto e fs_cliente ci sono i rispettivi codici estratti ed inoltre
il parametro di ritorno stringa è VUOTO

Se c'è stato un problema bisogna ignorare gli eventuali valori byref fs_prodotto e fs_cliente e sicuramente
la stringa di ritorno non è vuota ma indicativa del problema riscontrato

*/

ls_msg = ""

ls_appo = left(fs_input, 6)

if left(fs_input, 6) = "#SFUSO" then
	//foglio di sfuso
	fs_prodotto = "#"
	
	//verifica se c'è un codice cliente subito dopo #SFUSO
	ll_pos = pos(fs_input, "(",6)
	if ll_pos > 0 then
		//esiste la parentesi aperta
		
		if ll_pos > 7 then
			//la parentesi aperta è dopo: es. #SFUSOxxx(....
			ls_msg = "problemi sul nome foglio sfuso-cliente estratto: foglio "+fs_input
			return ls_msg
		end if
		
		//verifica se esiste la parentesi chiusa anche dopo
		ll_pos2 = pos(fs_input, ")",ll_pos)
		fs_cliente = mid(fs_input, ll_pos + 1, ll_pos2 - ll_pos - 1)
		
		//controlla l'esistenza del cliente
		select cod_cliente
		into :ls_appo
		from anag_clienti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:fs_cliente;
		if sqlca.sqlcode = 0 then
			//ok per prodotto(#) e cliente
			return ""
		else
			//problemi sul codice cliente estratto
			ls_msg = "problemi sul codice cliente estratto: foglio "+fs_input
			return ls_msg
		end if
		
	else
		if fs_input <> "#SFUSO" then
			ls_msg = "problemi sul nome foglio sfuso estratto: foglio "+fs_input
			return ls_msg
		end if
		
		//foglio di sfusi senza clienti
		fs_cliente = ""
		return ""
	end if
	
else
	//prodotto configurato
	
	ll_pos = pos(fs_input, "(")
	if ll_pos > 0 then
		//configurato con cliente
		
		ll_pos2 = pos(fs_input, ")")
		fs_cliente = mid(fs_input, ll_pos + 1, ll_pos2 - ll_pos - 1)
		fs_prodotto = left(fs_input, ll_pos - 1)
		
		//verifica esistenza del prodotto
		select cod_prodotto
		into :ls_appo
		from anag_prodotti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_prodotto;
			
		if sqlca.sqlcode = 0 then
			fs_prodotto = ls_appo
		else
			//problemi sul codice prodotto estratto
			ls_msg = "problemi sul codice prodotto estratto: foglio "+fs_input
			return ls_msg
		end if
		
		//controlla l'esistenza del cliente
		select cod_cliente
		into :ls_appo
		from anag_clienti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:fs_cliente;
		if sqlca.sqlcode = 0 then
			
		else
			//problemi sul codice cliente estratto
			ls_msg = "problemi sul codice cliente estratto: foglio "+fs_input
			return ls_msg
		end if
		
	else
		//configurato senza cliente: tutto l'input è il prodotto
		
		//verifica esistenza del prodotto
		select cod_prodotto
		into :ls_appo
		from anag_prodotti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_input;
			
		if sqlca.sqlcode = 0 then
			fs_prodotto = fs_input
			fs_cliente = ""
			return ""
		else
			//problemi sul codice prodotto estratto
			ls_msg = "problemi sul codice prodotto estratto: foglio "+fs_input
			return ls_msg
		end if
	end if
end if
end function

protected function integer wf_configurato_cliente (ref long fl_riga, ref long fl_colonna);string 		ls_cod_prodotto_da_r,ls_cod_prodotto_a_r,ls_cod_tipo_listino_prodotto_r,&
				ls_cod_valuta_r, ls_flag_estrai_ultima_r, ls_flag_incorporo_iva_r,&
				ls_syntax, ls_errore, ls_sql, ls_where, ls_where2, ls_cod_prodotto_like_r, ls_des_prodotto_listino, ls_cod_cliente
				
datetime		ldt_data_inizio_nuova_r, ldt_da_data_r, ldt_a_data_r

decimal		ld_min_fat_sup_cu

datastore	lds_datastore

long			ll_i, ll_tot

dw_selezione.accepttext()

ls_cod_tipo_listino_prodotto_r = dw_selezione.getitemstring(1,"cod_tipo_listino_prodotto")
ls_cod_valuta_r = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_prodotto_da_r = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a_r = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_prodotto_like_r = dw_selezione.getitemstring(1,"cod_prodotto_speciale")
ldt_da_data_r = dw_selezione.getitemdatetime(1,"da_data")
ldt_a_data_r = dw_selezione.getitemdatetime(1,"a_data")
ls_flag_estrai_ultima_r = dw_selezione.getitemstring(1,"flag_estrai_ultima")
ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
ldt_data_inizio_nuova_r = dw_selezione.getitemdatetime(1,"data_inizio_nuova")

//nella where c'è almeno il cod_azienda
ls_where = "where a.cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_tipo_listino_prodotto_r<>"" and not isnull(ls_cod_tipo_listino_prodotto_r) then
	ls_where += " and a.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_prodotto_r+"'"
end if
if ls_cod_valuta_r<>"" and not isnull(ls_cod_valuta_r) then
	ls_where += " and a.cod_valuta='"+ls_cod_valuta_r+"'"
end if
if ls_flag_estrai_ultima_r <> "S" then
	if not isnull(ldt_da_data_r) then
		ls_where += " and a.data_inizio_val>='"+string(ldt_da_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
	if not isnull(ldt_a_data_r) then
		ls_where += " and a.data_inizio_val<='"+string(ldt_a_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
end if
ls_where2 = ls_where

//questo deve essere necessariamente l'ultimo ---------------------

//verifica se hai scelto la ricerca per prodotto like
if ls_cod_prodotto_like_r <> "" and not isnull(ls_cod_prodotto_like_r) then
	//ricerca like
	wf_ricerca_prodotto_like(ls_cod_prodotto_like_r)
	ls_cod_prodotto_like_r = "a."+ls_cod_prodotto_like_r
	ls_where += " and "+ls_cod_prodotto_like_r
else
	//ricerca da-a
	if ls_cod_prodotto_da_r<>"" and not isnull(ls_cod_prodotto_da_r) then
		ls_where += " and a.cod_prodotto>='"+ls_cod_prodotto_da_r+"'"
	end if
	if ls_cod_prodotto_a_r<>"" and not isnull(ls_cod_prodotto_a_r) then
		ls_where += " and a.cod_prodotto<='"+ls_cod_prodotto_a_r+"'"
	end if
end if
//------------------------------------------------------------------------
if not isnull(ls_cod_cliente) then
	ls_where += " and a.cod_cliente = '"+ls_cod_cliente+"'"
end if	

//con cliente e prodotto configurato da listini vendite
ls_sql ="select a.cod_prodotto+'('+a.cod_cliente+')' as foglio,"+&
				"a.cod_azienda,a.cod_tipo_listino_prodotto,a.cod_valuta,a.data_inizio_val,a.progressivo,"+&
				"a.cod_prodotto,a.cod_cliente,c.rag_soc_1+isnull(' ,'+c.localita,'')+isnull('('+c.provincia+')','') as des_cliente,"+&
				"p.cod_cat_mer,"+&
				"a.scaglione_1,a.scaglione_2,a.scaglione_3,a.scaglione_4,a.scaglione_5,"+&
				"a.flag_sconto_mag_prezzo_1,a.flag_sconto_mag_prezzo_2,a.flag_sconto_mag_prezzo_3,"+&
					"a.flag_sconto_mag_prezzo_4,a.flag_sconto_mag_prezzo_5,"+&
				"a.flag_origine_prezzo_1,a.flag_origine_prezzo_2,a.flag_origine_prezzo_3,a.flag_origine_prezzo_4,"+&
					"a.flag_origine_prezzo_5,"+&
				"a.minimo_fatt_superficie,p.cod_misura_ven "+&
			"from listini_ven_locale as a "+&
			"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
			    "and p.cod_prodotto=a.cod_prodotto "+&
			"join anag_clienti as c on c.cod_azienda=a.cod_azienda "+&
			    "and c.cod_cliente=a.cod_cliente "+&
			ls_where +&
					" and a.cod_prodotto is not null and a.cod_cliente is not null "
			

//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
if is_OCF<>"" then
	ls_sql += "and (p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer='"+is_OCF+"') "
else
	ls_sql += "and p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' "
end if
//----------------------------------------------------------------------------------------------------------------

ls_sql += "order by a.cod_prodotto,a.cod_cliente,a.data_inizio_val desc,a.progressivo asc "


ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini (CONFIGURATO-Clienti): " + ls_errore)
	return -1
end if

lds_datastore = create datastore
lds_datastore.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini (CONFIGURATO Clienti): " + ls_errore)
	return -1
end if

lds_datastore.settransobject(sqlca)
if lds_datastore.retrieve() = -1 then
	destroy lds_datastore
	return -1
end if

ll_tot = lds_datastore.rowcount()
/*NOTA
In caso di impostaggio del periodo da-a devo considerare, a parità di prodotto e cliente,
sempre la prima riga, che grazie all'ordinamento impostato è quella + recente
Se invece è impostato il flag ultima condizione (cioè la + recente), non sarà passato
il filtro per data validità, ma prendero sempre e comunque la prima riga
a parità di prodotto e cliente

STRUTTURA DEL DATASTORE
	foglio								cod_azienda				cod_tipo_listino_prodotto		cod_valuta
	data_inizio_val					progressivo				cod_prodotto					cod_cliente
	des_cliente						cod_cat_mer			scaglione_1						scaglione_2
	scaglione_3						scaglione_4				scaglione_5						flag_sco_mag_pr_1
	flag_sco_mag_pr_2			flag_sco_mag_pr_3	flag_sc_mag_pr_4				flag_sco_mag_pr_5,
	flag_orig_prezzo_1			flag_orig_prezzo_2	flag_orig_prezzo_3			flag_orig_prezzo_4
	flag_origine_prezzo_5		minimo_fatt_sup		cod_misura_ven
*/

string			ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_foglio_cu, ls_cod_tipo_listino_prodotto_cu, &
				ls_cod_valuta_cu, ls_des_cliente_cu, ls_cod_cat_mer_cu, &
				ls_prodotto_corrente, ls_cliente_corrente, ls_valore, ls_flag_sconto_mag_prezzo, &
				ls_valore_sconto_mag_prezzo, ls_val_generico, ls_cod_variante, ls_cod_agente
				
long			ll_progressivo_cu, &
				ll_count_dim_x, ll_i_dim_x, ll_count_dim_y, ll_i_dim_y, ll_riga_var, ll_col_var, ll_col_var2, &
				ll_tot_varianti, ll_i_varianti, ll_i_scaglioni
				//ll_row_offset

datetime	ldt_data_val_cu, &
				ldt_data_val_corrente
				
decimal		ld_dim_x[], ld_dim_y[], ld_null_v[], ld_valore, ld_val_generico

datastore	lds_varianti

for ll_i = 1 to ll_tot
	yield()
	//ogni iterata in questo ciclo è un foglio di lavoro excel...
	ls_cod_prodotto_cu = lds_datastore.getitemstring(ll_i, 7)
	ls_cod_cliente_cu = lds_datastore.getitemstring(ll_i, 8)
	ldt_data_val_cu = lds_datastore.getitemdatetime(ll_i, 5)
	
	st_msg.text = "Elaborazione Prodotto Configurato per Clienti: "+&
				ls_cod_prodotto_cu+"("+ls_cod_cliente_cu+") :"+&
				string(ll_i)+" di "+string(ll_tot)
	
	if ls_cod_prodotto_cu=ls_prodotto_corrente and &
					ls_cod_cliente_cu=ls_cliente_corrente then continue
	
	//memorizzo il prodotto e il cliente corrente
	ls_prodotto_corrente = ls_cod_prodotto_cu
	ls_cliente_corrente = ls_cod_cliente_cu
	ldt_data_val_corrente = ldt_data_val_cu
	
	//continuo a leggere gli altri valori
	ls_foglio_cu = lds_datastore.getitemstring(ll_i, 1)
	ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(ll_i, 3)
	ls_cod_valuta_cu = lds_datastore.getitemstring(ll_i, 4)
	ll_progressivo_cu = lds_datastore.getitemnumber(ll_i, 6)
	ls_des_cliente_cu = lds_datastore.getitemstring(ll_i, 9)
	ls_cod_cat_mer_cu = lds_datastore.getitemstring(ll_i, 10)
	ld_min_fat_sup_cu = lds_datastore.getitemdecimal(ll_i, 26)
	
	//creo il foglio di lavoro opportuno
	iuo_excel.uof_crea_foglio(ls_foglio_cu)
	
	//si tratta di un prodotto configurato (no sfuso): devo caricare la griglia delle dimensioni
	//intanto scrivo il titolo nel foglio di lavoro ----------------------------------------------
//	ll_row_offset = 0
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,1,ls_cod_tipo_listino_prodotto_cu,false,false,"none")
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,2,ls_cod_valuta_cu,false,false,"none")
	
//	ll_row_offset += 1
	
	select des_prodotto
	into :ls_des_prodotto_listino
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu;
	
	if isnull(ls_des_prodotto_listino) or ls_des_prodotto_listino="" then
		ls_valore = ls_des_cliente_cu + " - (DAL "+string(date(ldt_data_val_cu))+")"
	else
		ls_valore = ls_des_cliente_cu + " - " + ls_des_prodotto_listino + " - (DAL "+string(date(ldt_data_val_cu))+")"
	end if
		
	iuo_excel.uof_scrivi(ls_foglio_cu, 1,1,ls_valore,false,true,"none")
	//--------------------------------------------------------------------------------------------		
	
	//carico le due diverse dimensioni per il prodotto che sto scorrendo
	ld_dim_x = ld_null_v
	ld_dim_y = ld_null_v
	if wf_carica_dimensioni(ls_cod_prodotto_cu, ld_dim_x[], ld_dim_y[]) = -1 then
//		destroy lds_datastore;
//		rollback;
//		return -1
	end if
	
	ll_i_dim_x = 1
	ll_i_dim_y = 1
	
	if upperbound(ld_dim_x[])>0 and upperbound(ld_dim_y[])>0 then
		//devo disegnare la griglia delle dimensioni
		
		iuo_excel.uof_scrivi(ls_foglio_cu,2,1,"DIM",false,true,"36")
		
		//scrivo la dim 1 in orizzontale
		ll_count_dim_x = upperbound(ld_dim_x)
		for ll_i_dim_x = 1 to ll_count_dim_x
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu,2,ll_i_dim_x + 1,ld_dim_x[ll_i_dim_x],false,true,"36")
		next
		
		//scrivo la dim 2 in verticale + le relative variazioni incolonnate per dim 1
		ll_count_dim_y = upperbound(ld_dim_y)
		for ll_i_dim_y = 1 to ll_count_dim_y
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu, ll_i_dim_y + 2, 1,ld_dim_y[ll_i_dim_y],false,true,"36")
			
			//ora lancio la query che mi deve caricare le variazioni fissate le due coordinate dim1 e dim2
			setnull(ld_valore)
			for ll_i_dim_x = 1 to ll_count_dim_x
				yield()
				wf_elabora_dimensioni(ls_cod_tipo_listino_prodotto_cu, &
											ls_cod_valuta_cu,&
											ldt_data_val_cu, &
											ll_progressivo_cu,&
											ld_dim_x[ll_i_dim_x], ld_dim_y[ll_i_dim_y], &
											ld_valore,&
											ls_flag_sconto_mag_prezzo, ls_valore_sconto_mag_prezzo, false)
											
				if ls_flag_sconto_mag_prezzo <> "P" then
					//and ls_flag_sconto_mag_prezzo <> "M" and ls_flag_sconto_mag_prezzo <> "A" then
					iuo_excel.uof_scrivi_codice(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ls_valore_sconto_mag_prezzo,false,false,"none")
				else
					//dato numerico puro (Prezzo)
					iuo_excel.uof_scrivi_dec(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ld_valore,false,false,"none", is_formato_numero)
				end if
			next
		next
	end if	
	
	
	//scrivo il min fatt sup
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_i_dim_y + 4, 1, "MFsup:",false,true,"36")
	if isnull(ld_min_fat_sup_cu) then ld_min_fat_sup_cu = 0
	iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_i_dim_y + 4, 2, ld_min_fat_sup_cu,false,false,"none",is_formato_numero)
	
	
	//alla fine di questo ciclo ho disegnato la griglia delle DIM
	//conservo in ll_i_dim_y e ll_i_dim_x i valori dell'angolo in basso a destra					
	//indice di riga excel:			ll_i_dim_y
	//indice di colonna excel:		ll_i_dim_x
							
	//devo ora scrivere la griglia delle varianti
	//mi sposto di due colonne a destra e comincio a scrivere dalla seconda riga con VAR
	ll_riga_var = 2
	ll_col_var = upperbound(ld_dim_x[]) + 1 + 2		//dimx + cellaDIM + 2
	ll_col_var2 = ll_col_var	//memorizzo da quale colonna parto
	
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,"VAR",false,true,"40")
	
	//ora che so la posizione della colonna VAR
	//SCRIVO L'AGENTE ------------------------------------------------------	
	select cod_agente_1
	into :ls_cod_agente
	from anag_clienti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_cliente=:ls_cod_cliente_cu;
	
	if isnull(ls_cod_agente) then ls_cod_agente = ""
	
	iuo_excel.uof_scrivi(ls_foglio_cu,1, ll_col_var2, "AGENTE:",false,true,"none")
	
	if ls_cod_agente<> "" then				
		select rag_soc_1
		into :ls_valore
		from anag_agenti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_agente=:ls_cod_agente;
			
		iuo_excel.uof_scrivi_codice(ls_foglio_cu,1,ll_col_var2+1,ls_cod_agente,false,true,"none")
		iuo_excel.uof_scrivi(ls_foglio_cu,1, ll_col_var2+2, ls_valore,false,true,"none")
	end if
	//--------------------------------------------------------------------------
	
	//scrivo il resto della intestazione della griglia VAR ------------------------------------					
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIANTE",false,true,"40")
	
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"DES. VARIANTE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"SCAGLIONE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"TIPO VAR.(*1)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIAZ.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"ORIGINE (*2)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"PROVV.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"COD.PRODOTTO PADRE.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VERS.DEFAULT",false,true,"40")
	//-------------------------------------------------------------------------------------------
	lds_varianti = create datastore
	ll_tot_varianti = wf_carica_ds_varianti(lds_varianti,ls_cod_tipo_listino_prodotto_cu, &
								ls_cod_valuta_cu, ldt_data_val_cu,&
								ll_progressivo_cu, false)
	if ll_tot_varianti > 0 then
		/*Struttura del datastore
		cod_azienda						cod_tipo_listino_prodotto		cod_valuta						data_inizio_val
		progressivo						cod_prodotto_listino			cod_cliente						rag_soc_1
		cod_prodotto_figlio			des_prodotto					scaglione_1						scaglione_2
		scaglione_3						scaglione_4						scaglione_5						flag_sconto_mag_prezzo_1	
		flag_sconto_mag_prezzo_2	flag_sconto_mag_prezzo_3	flag_sconto_mag_prezzo_4	flag_sconto_mag_prezzo_5
		variazione_1					variazione_2					variazione_3					variazione_4
		variazione_5					flag_origine_prezzo_1		flag_origine_prezzo_2		flag_origine_prezzo_3
		flag_origine_prezzo_4		flag_origine_prezzo_5		provvigione						cod_prodotto_padre
		versione_default
		*/
		for ll_i_varianti = 1 to ll_tot_varianti
			yield()
			//ripeto il ciclo fino a 5 volte se occorre in base al valore in variazione_i
			for ll_i_scaglioni=1 to 5
				yield()
				//leggo il valore dell variazione_i (i=1..5)
				ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_iesima (1..5)
				if (not isnull(ld_val_generico) and ld_val_generico > 0 and ll_i_scaglioni>1) &
																or ll_i_scaglioni = 1 then
					//------------------------------------------------
					
					ll_riga_var += 1				//mi sposto di una riga in giù
					ll_col_var2 = ll_col_var		//torno alla colonna in cui è specificato "VAR"
					
					//scrivo n° di riga
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti),false,true,"none")
					//iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti + ll_i_scaglioni - 1),false,true,"none")
					
					//listino cliente prodotto configurato **************************
					//non esistono le colonne del cliente, è fissato: quindi vado subito alle colonne della variante
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 9) //variante
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//memorizzo il cod_prodotto (variante)
					ls_cod_variante = ls_val_generico
					//------------------
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 10)	//des_variante
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//il resto per il listino del prodotto configurato (con o senza cliente) è lo stesso
					//inserisco da scaglione in poi
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 11 + ll_i_scaglioni - 1) //scaglione_i
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 16 + ll_i_scaglioni - 1)	//flag_sconto_mag_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//qui occorre interpretare la variazione in base al flag_sconto_mag_prezzo_i
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_i
					//-----------------------------------------
					
					string ls_appo
					
					ls_appo = wf_calcola_valore(ls_cod_variante, ls_val_generico,ld_val_generico)
					if ls_val_generico <> "P" then
						//and ls_val_generico <> "M" and ls_val_generico <> "A" then
						ll_col_var2 += 1
						//iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_appo,false,false,"none")
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					else
						ll_col_var2 += 1
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					end if
					//------------------------------------

					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 26 + ll_i_scaglioni - 1)	//flag_origine_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 31)	//provvigione
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 32)	//cod prodotto padre
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 33)	//versione default
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//------------------------------------------------
				else
					exit
				end if
			next
		next
	else
		//return 1
	end if
	destroy lds_varianti
	
	wf_scrivi_note(ls_foglio_cu, ll_riga_var, ll_col_var, false)
	
next

destroy lds_datastore
return 1
end function

public subroutine wf_log (string fs_val);long ll_ret

ll_ret = fileopen(is_filelog, LineMode!, Write!)

filewrite(ll_ret, fs_val)

fileclose(ll_ret)
end subroutine

public function string wf_get_today_now ();string ls_ret

ls_ret = string(datetime(today(),now())) + ": "

return ls_ret
end function

public function integer wf_importa_configurato (string fs_cod_prodotto);decimal			ld_x_excel[], ld_y_excel[]
long				ll_riga, ll_colonna, li_ret, ll_i, ll_offset, ll_counter, ll_rows, ll_progressivo, ll_prog_listino_prod, ll_count_scaglione
string			ls_valore, ls_foglio, ls_cod_prodotto_old, ls_cod_cliente_old, ls_flag_sconto_mag_prezzo
string			ls_cod_prodotto_var, ls_cod_cliente_var, ls_versione_default, ls_cod_prodotto_padre_var
decimal			ld_valore, ld_num_scaglione, ld_limite_dim_1, ld_limite_dim_2, ld_variazione
datetime		ldt_valore
boolean		lb_dim
decimal			ld_scaglione_var, ld_variazione_var, ld_provvigione_var, ld_minimo_fatt_sup
string			ls_flag_sconto_mag_prezzo_var, ls_flag_origine_prezzo_var
datastore		lds_dim, lds_var
boolean		lb_MFS_trovata = false
boolean		lb_VAR_trovata = false



//verifica che la data di creazione sia non esistente per il prodotto
if wf_controlla_data(idt_data_inizio_val, is_cod_tipo_listino_prodotto, is_cod_valuta,fs_cod_prodotto, true) then
else
	rollback;
	return -1
end if

ls_foglio = fs_cod_prodotto

li_ret = wf_controlla_dimensioni(fs_cod_prodotto, ls_foglio, lb_dim, ld_x_excel, ld_y_excel)
if li_ret = 0 then
	//log già scritto
	return 0
end if

//se lb_dim=false allora siamo in assenza di dimensioni......
if lb_dim then
	//leggi e memorizza le dimensioni
	li_ret = wf_prepara_ds_dimensioni(ls_foglio, fs_cod_prodotto, ld_x_excel, ld_y_excel, lds_dim)
	if li_ret = 0 then
		//log già scritto
		return 0
	end if
end if

//cerca la posizione della cella con contenuto VAR
ll_riga = 2
ll_colonna = 1
for ll_colonna = 1 to 100
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if ls_valore = "VAR" then				
		lb_VAR_trovata = true
		exit
	end if
next

if not lb_VAR_trovata then
	//errore in lettura codice VAR: salta il foglio e logga		
	wf_log(wf_get_today_now() + " Stringa 'VAR' non trovata in lettura dati foglio: "+ls_foglio)
	wf_log("-------------------------------------------------------------------")
	return 0
end if

//leggiamo adesso le varianti
ll_offset = ll_colonna
////leggiamo adesso le varianti
//ll_offset = upperbound(ld_x_excel) + 3

li_ret = wf_prepara_ds_varianti(ls_foglio, "STD", ll_offset, lds_var)
if li_ret = 0 then
	//log già scritto
	return 0
end if

//cerchiamo di leggere il min fatt superficie
//"MFsup:"
for ll_i = 1 to 100
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_i, 1, ls_valore, ld_valore, ldt_valore, "S")
	if ls_valore = "MFsup:" then				
		lb_MFS_trovata = true
		exit
	end if
next

if not lb_MFS_trovata then
	//errore in lettura codice VAR: salta il foglio e logga		
	wf_log(wf_get_today_now() + " Stringa 'MFsup:' non trovata in lettura dati foglio: "+ls_foglio)
	wf_log("non sarà importata la Superficie Minima Fatturabile")	
	wf_log("-------------------------------------------------------------------")
	ld_minimo_fatt_sup = 0
else
	//cerca di acquisire il valore
	li_ret = iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_i, 2, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then
		ld_minimo_fatt_sup = ld_valore
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " Valore della Superficie Minima Fatturabile non specificato in lettura dati foglio: "+ls_foglio)
		wf_log("non sarà importata la Superficie Minima Fatturabile")	
		ld_minimo_fatt_sup = 0
	end if
end if

//importazione
//occorre inserire una riga in listini_ven_comune
//n righe in listini_ven_dim_comune
//m righe in listini_prod_comune (cliente vuoto) oe/o k righe in listini_prod_comune_var_client (con cliente)

//la var. di istanza "idt_data_inizio_val" è sicuramente valorizzata xchè stiamo nel listino Comune
//calcola il max + 1 del progressivo (per sicurezza)
select max(progressivo)
into :ll_progressivo
//from listini_ven_comune
from listini_vendite
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
	cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val;
	
if isnull(ll_progressivo) then ll_progressivo = 0
ll_progressivo += 1

insert into listini_ven_comune
	(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
	cod_prodotto, flag_tipo_scaglioni, scaglione_1,
	flag_sconto_mag_prezzo_1, flag_origine_prezzo_1, minimo_fatt_superficie)
values
	(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_progressivo,
	:fs_cod_prodotto, 'Q', 99999999.0000, 'P', 'P', :ld_minimo_fatt_sup);
	
if sqlca.sqlcode = 0 then
else
	//errore in inserimento
	g_mb.messagebox("APICE", "Errore in inserimento listino comune: Configurato senza cliente:"+fs_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
	rollback;
	return -1
end if

//ciclo per inserire le dimensioni
/*STRUTTURA del DATASTORE delle dimensioni ----------------------------------
1.	num_scaglione
2.	limite_dimensione_1
3.	limite_dimensione_2
4.	flag_sconto_mag_prezzo
5.	variazione
------------------------------------------------------------------------------------------------------
*/

if isvalid(lds_dim) then
	ll_rows = lds_dim.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	//leggo le info dal datastore
	ld_num_scaglione = lds_dim.getitemdecimal(ll_i, 1)
	ld_limite_dim_1 = lds_dim.getitemdecimal(ll_i, 2)
	ld_limite_dim_2 = lds_dim.getitemdecimal(ll_i, 3)
	ls_flag_sconto_mag_prezzo = lds_dim.getitemstring(ll_i, 4)
	ld_variazione = lds_dim.getitemdecimal(ll_i, 5)
	
	//faccio l'inserimento
	insert into listini_ven_dim_comune
	(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
	num_scaglione,limite_dimensione_1,limite_dimensione_2,flag_sconto_mag_prezzo,variazione)
	values
	(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_progressivo,
	:ld_num_scaglione,:ld_limite_dim_1,:ld_limite_dim_2,:ls_flag_sconto_mag_prezzo,:ld_variazione);
	
	if sqlca.sqlcode = 0 then
	else
		//errore in inserimento
		g_mb.messagebox("APICE", "Errore in inserimento dimensioni listino: Configurato senza cliente:"+fs_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
		rollback;
		return -1
	end if
next

/*
STRUTTURA del DATASTORE delle varianti ----------------------------------
1.  prog_var
2.	cod_cliente
3.	cod_prodotto_figlio
4.	scaglione_1
5.	flag_sconto_mag_prezzo_1
6.	variazione_1
7.	flag_origine_prezzo_1
8.	provvigione
9.	cod_prodotto_padre
10.versione_default
--------------------------------------------------------------------------------------------------
*/


//nel ciclo, se il prodotto (nell'ambito dello stesso cod_cliente) si ripete aggiungi un nuovo scaglione in tabella
//la var. di istanza "idt_data_inizio_val" è sicuramente valorizzata xchè stiamo nel listino Comune
ls_cod_prodotto_old = ""
ls_cod_cliente_old = ""
ll_counter = 1

if isvalid(lds_var) then
	ll_rows = lds_var.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	//lettura delle variabili dal datastore
	ls_cod_cliente_var = lds_var.getitemstring(ll_i, 2)
	ls_cod_prodotto_var = lds_var.getitemstring(ll_i, 3)
	ld_scaglione_var = lds_var.getitemdecimal(ll_i, 4)
	ls_flag_sconto_mag_prezzo_var = lds_var.getitemstring(ll_i, 5)
	ld_variazione_var = lds_var.getitemdecimal(ll_i, 6)
	ls_flag_origine_prezzo_var = lds_var.getitemstring(ll_i, 7)
	ld_provvigione_var = lds_var.getitemdecimal(ll_i, 8)
	ls_cod_prodotto_padre_var = lds_var.getitemstring(ll_i, 9)
	
	//ls_versione_default = lds_var.getitemstring(ll_i, 10)
	if wf_get_versione_default(fs_cod_prodotto, ls_versione_default) < 0 then
		//msg già dato
		rollback;
		return -1
	end if
	
	if isnull(ls_cod_cliente_var) then ls_cod_cliente_var = ""
	
	if ls_cod_cliente_var=ls_cod_cliente_old and ls_cod_prodotto_var=ls_cod_prodotto_old then
		//stesso prodotto e stesso cliente: aggiungi un nuovo scaglione (fai update) #################################
		
		ll_counter += 1		//counter che tiene traccia del numero scaglione a cui siamo arrivati
		
		//controlla se per caso è superiore al 5 (max 5 scaglioni)
		if ll_counter > 5 then
			//salta la riga e logga
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Msg: Hai superato il n° max pari a 5 scaglioni! Riga di excel ignorata")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		
		if ls_cod_cliente_var="" then
			//la tabella interessata è listini_prod_comune
			
			//verifica che lo scaglione non sia già presente -----------------------------------------------------------
			setnull(ll_count_scaglione)
			select count(*)
			into :ll_count_scaglione
			from listini_prod_comune
			where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and 
						(scaglione_1=:ld_scaglione_var or scaglione_2=:ld_scaglione_var or scaglione_3=:ld_scaglione_var or
							scaglione_4=:ld_scaglione_var or scaglione_5=:ld_scaglione_var);
							
			if ll_count_scaglione>0 then
				//si tratta di uno scaglione duplicato: salta la riga e logga
				ll_counter -= 1    //ripristina il valore del counter
				
				wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
													string(ld_scaglione_var)+" - Riga di excel ignorata!")
				wf_log("---------------------------------------------------------------------")
				continue
			end if
			//-----------------------------------------------------------------------------------------------------------------------
			
			choose case ll_counter
				case 2
					update listini_prod_comune
					set scaglione_2 = :ld_scaglione_var, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_var,
							variazione_2 = :ld_variazione_var, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod;
						
				case 3
					update listini_prod_comune
					set scaglione_3 = :ld_scaglione_var, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_var,
							variazione_3 = :ld_variazione_var, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod;
						
				case 4
					update listini_prod_comune
					set scaglione_4 = :ld_scaglione_var, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_var,
							variazione_4 = :ld_variazione_var, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod;
						
				case 5
					update listini_prod_comune
					set scaglione_5 = :ld_scaglione_var, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_var,
							variazione_5 = :ld_variazione_var, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod;
						
			end choose
			
			if sqlca.sqlcode = 0 then
			else
				//errore in aggiornamento
				g_mb.messagebox("APICE", "Configurato senza cliente"+fs_cod_prodotto+".Errore in aggiornamento (listini_prod_comune) listino varianti (scaglione "+string(ll_counter)+"): variante"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			
		else
			// nel caso di importazione comune non devo salvare queste
			/*
			//la tabella interessata è listini_prod_comune_var_client			
			//verifica che lo scaglione non sia già presente -----------------------------------------------------------
			setnull(ll_count_scaglione)
			select count(*)
			into :ll_count_scaglione
			from listini_prod_comune_var_client
			where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and 
						(scaglione_1=:ld_scaglione_var or scaglione_2=:ld_scaglione_var or scaglione_3=:ld_scaglione_var or
							scaglione_4=:ld_scaglione_var or scaglione_5=:ld_scaglione_var);

			if ll_count_scaglione>0 then
				//si tratta di uno scaglione duplicato: salta la riga e logga
				ll_counter -= 1    //ripristina il valore del counter
				
				wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Cliente:"+ls_cod_cliente_var+" -  Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
													string(ld_scaglione_var)+" - Riga di excel ignorata!")
				wf_log("---------------------------------------------------------------------")
				continue
			end if
			//-----------------------------------------------------------------------------------------------------------------------
			
			choose case ll_counter
				case 2
					update listini_prod_comune_var_client
					set scaglione_2 = :ld_scaglione_var, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_var,
							variazione_2 = :ld_variazione_var, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
						
				case 3
					update listini_prod_comune_var_client
					set scaglione_3 = :ld_scaglione_var, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_var,
							variazione_3 = :ld_variazione_var, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
						
				case 4
					update listini_prod_comune_var_client
					set scaglione_4 = :ld_scaglione_var, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_var,
							variazione_4 = :ld_variazione_var, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
						
				case 5
					update listini_prod_comune_var_client
					set scaglione_5 = :ld_scaglione_var, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_var,
							variazione_5 = :ld_variazione_var, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo_var
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
						cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
						cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
						prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
						
			end choose
		
			if sqlca.sqlcode = 0 then
			else
				//errore in aggiornamento
				g_mb.messagebox("APICE", "Configurato senza cliente:"+fs_cod_prodotto+". Errore in aggiornamento (listini_prod_comune_var_client) listino varianti (scaglione "+string(ll_counter)+"): variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			*/
			
		end if		
	else
		//scaglione n°1 (fai insert) #######################################################################
		ll_counter = 1		//reset del numero scaglione
		
		if ls_cod_cliente_var="" then
			//la tabella interessata è listini_prod_comune					
			
			//calcolo il max prog_listino_produzione
			select max(prog_listino_produzione)
			into :ll_prog_listino_prod
			//from listini_prod_comune
			from listini_produzione
			where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
				cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and
				cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default;
						
			if isnull(ll_prog_listino_prod) then ll_prog_listino_prod = 0
			ll_prog_listino_prod += 1
			
			insert into listini_prod_comune
			(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
			cod_prodotto_listino,cod_versione,prog_listino_produzione,
			cod_prodotto_padre,cod_prodotto_figlio,flag_tipo_scaglioni,
			scaglione_1,flag_sconto_mag_prezzo_1,variazione_1,flag_origine_prezzo_1,
			provvigione_1, cod_cliente)
			values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_progressivo,
			:fs_cod_prodotto,:ls_versione_default,:ll_prog_listino_prod,
			:ls_cod_prodotto_padre_var,:ls_cod_prodotto_var,'Q',
			:ld_scaglione_var,:ls_flag_sconto_mag_prezzo_var,:ld_variazione_var,:ls_flag_origine_prezzo_var,
			:ld_provvigione_var, null);
			
			if sqlca.sqlcode = 0 then
			else
				//errore in inserimento
				g_mb.messagebox("APICE", "Errore in inserimento varianti listino (listini_prod_comune): Configurato senza cliente:"+fs_cod_prodotto+" variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			
		else
			// nel caso di importazione comune non devo salvare queste
			/*
			
			//la tabella interessata è listini_prod_comune_var_client			
			//non devo calcolare il max+1 ma cercare di leggere la PK corrispondente dalla listini_prod_comune			
			select max(prog_listino_produzione)
			into :ll_prog_listino_prod
			from listini_prod_comune
			where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
				cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and
				cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and
				cod_prodotto_figlio=:ls_cod_prodotto_var and cod_cliente is null;
			
			if sqlca.sqlcode <> 0 or ll_prog_listino_prod<=0 or isnull(ll_prog_listino_prod) then
				g_mb.messagebox("APICE", "Chiave non trovata in listini_prod_comune: Configurato standard:"+fs_cod_prodotto+&
																		" variante: "+ls_cod_prodotto_var+" - cliente:"+ls_cod_cliente_var+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			
			/*
			//calcolo il max prog_listino_produzione
			select max(prog_listino_produzione)
			into :ll_prog_listino_prod
			from listini_produzione
			where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
				cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and
				cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and
				cod_cliente=:ls_cod_cliente_var;
						
			if isnull(ll_prog_listino_prod) then ll_prog_listino_prod = 0
			ll_prog_listino_prod += 1
			*/
			
			insert into listini_prod_comune_var_client
			(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
			cod_prodotto_listino,cod_versione,prog_listino_produzione,
			cod_prodotto_padre,cod_prodotto_figlio,flag_tipo_scaglioni,
			scaglione_1,flag_sconto_mag_prezzo_1,variazione_1,flag_origine_prezzo_1,
			provvigione_1, cod_cliente)
			values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_progressivo,
			:fs_cod_prodotto,:ls_versione_default,:ll_prog_listino_prod,
			:ls_cod_prodotto_padre_var,:ls_cod_prodotto_var,'Q',
			:ld_scaglione_var,:ls_flag_sconto_mag_prezzo_var,:ld_variazione_var,:ls_flag_origine_prezzo_var,
			:ld_provvigione_var, :ls_cod_cliente_var);
			
			if sqlca.sqlcode = 0 then
			else
				//errore in inserimento
				g_mb.messagebox("APICE", "Errore in inserimento varianti listino (listini_prod_comune_var_client): Configurato senza cliente:"+fs_cod_prodotto+" variante: "+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			*/			
		end if
		
		//mi salvo i nuovi valori
		ls_cod_cliente_old = ls_cod_cliente_var
		ls_cod_prodotto_old = ls_cod_prodotto_var
	end if
	
next

destroy lds_dim
destroy lds_var

wf_log(wf_get_today_now() + "Prodotto: Configurato: "+fs_cod_prodotto+": importazione completata")

return 1
end function

public function integer wf_importa_configurato_cliente (string fs_cod_prodotto, string fs_cod_cliente);decimal			ld_x_excel[], ld_y_excel[]
long				ll_riga, ll_colonna, li_ret, ll_i, ll_offset, ll_counter, ll_rows, ll_progressivo, ll_prog_listino_prod,ll_count_scaglione
string			ls_valore, ls_foglio, ls_cod_prodotto_old, ls_flag_sconto_mag_prezzo
string			ls_cod_prodotto_var, ls_cod_cliente_var, ls_versione_default, ls_cod_prodotto_padre_var
decimal			ld_valore, ld_num_scaglione, ld_limite_dim_1, ld_limite_dim_2, ld_variazione
datetime		ldt_valore, ldt_data_inizio_val
boolean		lb_dim
decimal			ld_scaglione_var, ld_variazione_var, ld_provvigione_var, ld_minimo_fatt_sup
string			ls_flag_sconto_mag_prezzo_var, ls_flag_origine_prezzo_var
datastore		lds_dim, lds_var
boolean		lb_MFS_trovata=false
boolean		lb_VAR_trovata = false

ls_foglio = fs_cod_prodotto+"("+fs_cod_cliente+")"

li_ret = wf_controlla_dimensioni(fs_cod_prodotto, ls_foglio, lb_dim, ld_x_excel, ld_y_excel)
if li_ret = 0 then
	//log già scritto
	return 0
end if

//se lb_dim=false allora siamo in assenza di dimensioni......
if lb_dim then
	//leggi e memorizza le dimensioni
	li_ret = wf_prepara_ds_dimensioni(ls_foglio, fs_cod_prodotto, ld_x_excel, ld_y_excel, lds_dim)
	if li_ret = 0 then
		//log già scritto
		return 0
	end if
end if

//cerca la posizione della cella con contenuto VAR
ll_riga = 2
ll_colonna = 1
for ll_colonna = 1 to 100
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if ls_valore = "VAR" then				
		lb_VAR_trovata = true
		exit
	end if
next

if not lb_VAR_trovata then
	//errore in lettura codice VAR: salta il foglio e logga		
	wf_log(wf_get_today_now() + " Stringa 'VAR' non trovata in lettura dati foglio: "+ls_foglio)
	wf_log("-------------------------------------------------------------------")
	return 0
end if

//leggiamo adesso le varianti
ll_offset = ll_colonna
////leggiamo adesso le varianti
//ll_offset = upperbound(ld_x_excel) + 3

li_ret = wf_prepara_ds_varianti(ls_foglio, "CLI", ll_offset, lds_var)
if li_ret = 0 then
	//log già scritto
	return 0
end if

//cerchiamo di leggere il min fatt superficie
//"MFsup:"
for ll_i = 1 to 100
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_i, 1, ls_valore, ld_valore, ldt_valore, "S")
	if ls_valore = "MFsup:" then				
		lb_MFS_trovata = true
		exit
	end if
next

if not lb_MFS_trovata then
	//errore in lettura codice VAR: salta il foglio e logga		
	wf_log(wf_get_today_now() + " Stringa 'MFsup:' non trovata in lettura dati foglio: "+ls_foglio)
	wf_log("non sarà importato la Superficie Minima Fatturabile")	
	wf_log("-------------------------------------------------------------------")
	ld_minimo_fatt_sup = 0
else
	//cerca di acquisire il valore
	li_ret = iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_i, 2, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then
		ld_minimo_fatt_sup = ld_valore
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " Valore della Superficie Minima Fatturabile non specificata in lettura dati foglio: "+ls_foglio)
		wf_log("non sarà importato la Superficie Minima Fatturabile")	
		ld_minimo_fatt_sup = 0
	end if
end if

//importazione
//occorre inserire una riga in listini_ven_comune
//n righe in listini_ven_dim_comune
//m righe in listini_prod_comune (cliente vuoto) oe/o k righe in listini_prod_comune_var_client (con cliente)

//se la  var. di istanza "idt_data_inizio_val" è valorizzata allora proveniamo dall'elaborazione di TUTTO
//ignora la data di excel e considera quella della variabile di istanza
if not isnull(idt_data_inizio_val) then
	//hai caricato prima il listino comune (xchè hai specificato TUTTI in tipo importazione)
	//ignora le date di excel
	ldt_data_inizio_val = idt_data_inizio_val
else
	//cerca di leggere la data specificata nel foglio: estrapolandola dalla riga 1 colonna 1
	if wf_estrai_data(ls_foglio, ldt_data_inizio_val) = 1 then
		//OK: continua considerando la variabile "ldt_data_inizio_val"
	else
		//salta la riga e logga
		wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Il sistema non è riuscito ad estrapolare la data del listino (riga 1 colonna 1)! Foglio excel ignorato!")
		wf_log("---------------------------------------------------------------------")
		return 1
	end if
end if

//verifica che la data di creazione sia non esistente per il prodotto
if wf_controlla_data(ldt_data_inizio_val, is_cod_tipo_listino_prodotto, is_cod_valuta,fs_cod_prodotto, false) then
else
	//log e salta foglio
	wf_log(wf_get_today_now() + " errore in lettura data validità foglio: "+ls_foglio)
	wf_log(wf_get_today_now() + " data antecedente:  il foglio non è stato elaborato!")
	wf_log("--------------------------------------------------------------------------------------------")
	return 0
end if

//calcola il max + 1 del progressivo (per sicurezza)
select max(progressivo)
into :ll_progressivo
//from listini_ven_locale
from listini_vendite
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
	cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val;
	
if isnull(ll_progressivo) then ll_progressivo = 0
ll_progressivo += 1

insert into listini_ven_locale
	(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
	cod_prodotto, cod_cliente,flag_tipo_scaglioni, scaglione_1,
	flag_sconto_mag_prezzo_1, flag_origine_prezzo_1, minimo_fatt_superficie)
values
	(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_progressivo,
	:fs_cod_prodotto,:fs_cod_cliente, 'Q', 99999999.0000, 'P', 'P',:ld_minimo_fatt_sup);
	
if sqlca.sqlcode = 0 then
else
	//errore in inserimento
	g_mb.messagebox("APICE", "Errore in inserimento listino locale: Configurato con cliente:"+fs_cod_prodotto+"("+fs_cod_cliente+") "+ sqlca.sqlerrtext, StopSign!)
	rollback;
	return -1
end if

//ciclo per inserire le dimensioni
/*STRUTTURA del DATASTORE delle dimensioni ----------------------------------
1.	num_scaglione
2.	limite_dimensione_1
3.	limite_dimensione_2
4.	flag_sconto_mag_prezzo
5.	variazione
------------------------------------------------------------------------------------------------------
*/
if isvalid(lds_dim) then
	ll_rows = lds_dim.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	//leggo le info dal datastore
	ld_num_scaglione = lds_dim.getitemdecimal(ll_i, 1)
	ld_limite_dim_1 = lds_dim.getitemdecimal(ll_i, 2)
	ld_limite_dim_2 = lds_dim.getitemdecimal(ll_i, 3)
	ls_flag_sconto_mag_prezzo = lds_dim.getitemstring(ll_i, 4)
	ld_variazione = lds_dim.getitemdecimal(ll_i, 5)
	
	//faccio l'inserimento
	insert into listini_ven_dim_locale
	(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
	num_scaglione,limite_dimensione_1,limite_dimensione_2,flag_sconto_mag_prezzo,variazione)
	values
	(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_progressivo,
	:ld_num_scaglione,:ld_limite_dim_1,:ld_limite_dim_2,:ls_flag_sconto_mag_prezzo,:ld_variazione);
	
	if sqlca.sqlcode = 0 then
	else
		//errore in inserimento
		g_mb.messagebox("APICE", "Errore in inserimento dimensioni listino: Configurato con cliente:"+fs_cod_prodotto+"("+fs_cod_cliente+") "+ sqlca.sqlerrtext, StopSign!)
		rollback;
		return -1
	end if
next

/*
STRUTTURA del DATASTORE delle varianti ----------------------------------
1.  prog_var
2.	cod_cliente  (IGNORA IN QUESTO CASO SARà VUOTO)
3.	cod_prodotto_figlio
4.	scaglione_1
5.	flag_sconto_mag_prezzo_1
6.	variazione_1
7.	flag_origine_prezzo_1
8.	provvigione
9.	cod_prodotto_padre
10.versione_default
--------------------------------------------------------------------------------------------------
*/


//nel ciclo, se il prodotto si ripete aggiungi un nuovo scaglione in tabella
//N.B. nel caso configurato con cliente non esistono le colonne in excel relative al cliente
ls_cod_prodotto_old = ""
ll_counter = 1

if isvalid(lds_var) then
	ll_rows = lds_var.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	//lettura delle variabili dal datastore
	
	ls_cod_prodotto_var = lds_var.getitemstring(ll_i, 3)
	ld_scaglione_var = lds_var.getitemdecimal(ll_i, 4)
	ls_flag_sconto_mag_prezzo_var = lds_var.getitemstring(ll_i, 5)
	ld_variazione_var = lds_var.getitemdecimal(ll_i, 6)
	ls_flag_origine_prezzo_var = lds_var.getitemstring(ll_i, 7)
	ld_provvigione_var = lds_var.getitemdecimal(ll_i, 8)
	ls_cod_prodotto_padre_var = lds_var.getitemstring(ll_i, 9)
	
	//ls_versione_default = lds_var.getitemstring(ll_i, 10)
	if wf_get_versione_default(fs_cod_prodotto, ls_versione_default) < 0 then
		//msg già dato
		rollback;
		return -1
	end if
	
	
	if ls_cod_prodotto_var=ls_cod_prodotto_old then
		//stesso prodotto: aggiungi un nuovo scaglione (fai update) #################################
		
		ll_counter += 1		//counter che tiene traccia del numero scaglione a cui siamo arrivati
		
		//controlla se per caso è superiore al 5 (max 5 scaglioni)
		if ll_counter > 5 then
			//salta la riga e logga
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Msg: Hai superato il n° max pari a 5 scaglioni! Riga di excel ignorata")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		
		//verifica che lo scaglione non sia già presente -----------------------------------------------------------
		setnull(ll_count_scaglione)
		select count(*)
		into :ll_count_scaglione
		from listini_prod_locale
		where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and 
					(scaglione_1=:ld_scaglione_var or scaglione_2=:ld_scaglione_var or scaglione_3=:ld_scaglione_var or
						scaglione_4=:ld_scaglione_var or scaglione_5=:ld_scaglione_var);
						
		if ll_count_scaglione>0 then
			//si tratta di uno scaglione duplicato: salta la riga e logga
			ll_counter -= 1  //ripristina il valore del counter
			
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
												string(ld_scaglione_var)+" - Riga di excel ignorata!")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		//-----------------------------------------------------------------------------------------------------------------------
		
		choose case ll_counter
			case 2
				update listini_prod_locale
				set scaglione_2 = :ld_scaglione_var, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_var,
						variazione_2 = :ld_variazione_var, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod;
					
			case 3
				update listini_prod_locale
				set scaglione_3 = :ld_scaglione_var, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_var,
						variazione_3 = :ld_variazione_var, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod;
					
			case 4
				update listini_prod_locale
				set scaglione_4 = :ld_scaglione_var, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_var,
						variazione_4 = :ld_variazione_var, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod;
					
			case 5
				update listini_prod_locale
				set scaglione_5 = :ld_scaglione_var, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_var,
						variazione_5 = :ld_variazione_var, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod;
					
		end choose
		
		if sqlca.sqlcode = 0 then
		else
			//errore in aggiornamento
			g_mb.messagebox("APICE", "Errore in aggiornamento varianti listino (listini_prod_locale): Configurato con cliente:"+fs_cod_prodotto+"("+fs_cod_cliente+") variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
	else
		//scaglione n°1 (fai insert) #######################################################################
		ll_counter = 1		//reset del numero scaglione
		
		//calcolo il max prog_listino_produzione
		select max(prog_listino_produzione)
		into :ll_prog_listino_prod
		//from listini_prod_locale
		from listini_produzione
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
			cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and
			cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default;
					
		if isnull(ll_prog_listino_prod) then ll_prog_listino_prod = 0
		ll_prog_listino_prod += 1
		
		insert into listini_prod_locale
		(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
		cod_prodotto_listino,cod_versione,prog_listino_produzione,
		cod_prodotto_padre,cod_prodotto_figlio,flag_tipo_scaglioni,
		scaglione_1,flag_sconto_mag_prezzo_1,variazione_1,flag_origine_prezzo_1,cod_cliente,
		provvigione_1)
		values
		(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_progressivo,
		:fs_cod_prodotto,:ls_versione_default,:ll_prog_listino_prod,
		:ls_cod_prodotto_padre_var,:ls_cod_prodotto_var,'Q',
		:ld_scaglione_var,:ls_flag_sconto_mag_prezzo_var,:ld_variazione_var,:ls_flag_origine_prezzo_var,:fs_cod_cliente,
		:ld_provvigione_var);
		
		if sqlca.sqlcode = 0 then
		else
			//errore in inserimento
			g_mb.messagebox("APICE", "Errore in inserimento varianti listino (listini_prod_locale): Configurato con cliente:"+fs_cod_prodotto+"("+fs_cod_cliente+") variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
		
		//mi salvo il nuovo valore
		ls_cod_prodotto_old = ls_cod_prodotto_var
	end if
	
next

destroy lds_dim
destroy lds_var

wf_log(wf_get_today_now() + "Prodotto: Configurato: "+fs_cod_prodotto+" con cliente:"+fs_cod_cliente+" importazione completata")

return 1
end function

public function integer wf_importa_sfuso ();string			ls_foglio
string			ls_cod_prodotto, ls_cod_prodotto_old
datastore		lds_data
long				ll_rows, ll_i, ll_progressivo, ll_prog_provvigione, ll_count_scaglione
integer			li_ret, ll_counter
decimal			ld_min_fatt_sup, ld_qta_scaglione, ld_variazione, ld_provvigione
string			ls_flag_tipo_scaglioni, ls_flag_sconto_mag_prezzo, ls_flag_origine_prezzo

ls_foglio =  "#SFUSO"
li_ret = wf_prepara_ds_sfuso(lds_data, ls_foglio)

if li_ret = 0 then
	//log già scritto
	return 0
end if

/*STRUTTURA del DATASTORE
1.  data_inizio_val
2.	 progressivo
3.	cod_prodotto
4.	 cod_cliente
5.	 scaglione_1
6.	 flag_sconto_mag_prezzo_1
7.	 variazione_1
8.	 flag_origine_prezzo_1
9.	 minimo_fatt_superficie
10.provvigione
11.cod agente		(NON USATO SE STANDARD)
*/

//nel ciclo, se il prodotto si ripete aggiungi un nuovo scaglione in tabella
//la var. di istanza "idt_data_inizio_val" è sicuramente valorizzata xchè stiamo nel listino Comune
ls_cod_prodotto_old = ""
ll_counter = 1

if isvalid(lds_data) then
	ll_rows = lds_data.rowcount()
else
	ll_rows = 0
end if

//verifica che la data di creazione sia non esistente per il prodotto (listino comune)
for ll_i = 1 to ll_rows
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 3)
	
	if wf_controlla_data(idt_data_inizio_val, is_cod_tipo_listino_prodotto, is_cod_valuta,ls_cod_prodotto,true) then
	else
		rollback;
		return -1
	end if
next

for ll_i = 1 to ll_rows
	
	//leggo i valori da inserire dal datastore
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 3)
	ls_flag_tipo_scaglioni = "Q"
	ld_qta_scaglione = lds_data.getitemdecimal(ll_i, 5)
	ls_flag_sconto_mag_prezzo = lds_data.getitemstring(ll_i, 6)
	ld_variazione = lds_data.getitemdecimal(ll_i, 7)
	ls_flag_origine_prezzo = lds_data.getitemstring(ll_i, 8)
	ld_min_fatt_sup = lds_data.getitemdecimal(ll_i, 9)
	ld_provvigione = lds_data.getitemdecimal(ll_i, 10)
	
	if ls_cod_prodotto <> ls_cod_prodotto_old then
		//primo scaglione di un nuovo prodotto
		ll_counter = 1
		
		//calcola il max + 1 del progressivo
		select max(progressivo)
		into :ll_progressivo
		//from listini_ven_comune
		from listini_vendite
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
			cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val;
			
		if isnull(ll_progressivo) then ll_progressivo = 0
		ll_progressivo += 1
		
		insert into listini_ven_comune
			(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
			cod_prodotto, minimo_fatt_superficie, flag_tipo_scaglioni, scaglione_1,
			flag_sconto_mag_prezzo_1, variazione_1, flag_origine_prezzo_1)
		values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_progressivo,
			:ls_cod_prodotto, :ld_min_fatt_sup, :ls_flag_tipo_scaglioni, :ld_qta_scaglione,
			:ls_flag_sconto_mag_prezzo, :ld_variazione, :ls_flag_origine_prezzo);
			
		if sqlca.sqlcode = 0 then
		else
			//errore in inserimento
			g_mb.messagebox("APICE", "Errore in inserimento listino: sfuso senza cliente:"+ls_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
		
		//salvo il valore del nuovo prodotto elaborato
		ls_cod_prodotto_old = ls_cod_prodotto
		
		//salvo la provvigione in scaglione 1
		if ld_provvigione>0 then
			
			//max + 1 del progressivo in base all'indice cu_provvigioni (cod_azienda,cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo)
			select max(progressivo)
			into :ll_prog_provvigione
			from provvigioni
			where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
				cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val;
			
			if isnull(ll_prog_provvigione) then ll_prog_provvigione=0
			
			ll_prog_provvigione += 1
			
			insert into provvigioni
			(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
			cod_cat_mer, cod_prodotto, cod_categoria, cod_cliente, cod_agente, flag_tipo_scaglioni,
			scaglione_1,variazione_1)
			values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :idt_data_inizio_val, :ll_prog_provvigione,			
			null, :ls_cod_prodotto, null, null, null, 'V',
			:ld_qta_scaglione, :ld_provvigione);
			
			if sqlca.sqlcode = 0 then
			else
				//errore in inserimento provvigione
				g_mb.messagebox("APICE", "Errore in inserimento provvigione: sfuso standard:"+ls_cod_prodotto &
									+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			
		end if		
		
		
	else
		//è un altro scaglione dello stesso prodotto: fare update
		
		ll_counter += 1
		
		//controlla se per caso è superiore al 5 (max 5 scaglioni)
		if ll_counter > 5 then
			//salta la riga e logga
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Prodotto: "+ls_cod_prodotto+" Msg: Hai superato il n° max pari a 5 scaglioni! Riga di excel ignorata")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		
		//verifica che lo scaglione non sia già presente -----------------------------------------------------------
		setnull(ll_count_scaglione)
		select count(*)
		into :ll_count_scaglione
		from listini_ven_comune
		where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
					(scaglione_1=:ld_qta_scaglione or scaglione_2=:ld_qta_scaglione or scaglione_3=:ld_qta_scaglione or
						scaglione_4=:ld_qta_scaglione or scaglione_5=:ld_qta_scaglione);
						
		if ll_count_scaglione>0 then
			//si tratta di uno scaglione duplicato: salta la riga e logga
			ll_counter -= 1  //ripristina il valore del counter
			
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Prodotto Sfuso: "+ls_cod_prodotto+" Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
												string(ld_qta_scaglione)+" - Riga di excel ignorata!")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		//-----------------------------------------------------------------------------------------------------------------------
		
		choose case ll_counter
			case 2
				update listini_ven_comune
				set scaglione_2 = :ld_qta_scaglione, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo,
						variazione_2 = :ld_variazione, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo;
					
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+"): sfuso senza cliente:"+ls_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if ld_provvigione>0 then
					update provvigioni
					set scaglione_2 = :ld_qta_scaglione,
						variazione_2 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente is null and cod_agente is null;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso standard:"+ls_cod_prodotto+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
					
			case 3
				update listini_ven_comune
				set scaglione_3 = :ld_qta_scaglione, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo,
						variazione_3 = :ld_variazione, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+"): sfuso senza cliente:"+ls_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if ld_provvigione>0 then
					update provvigioni
					set scaglione_3 = :ld_qta_scaglione,
						variazione_3 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente is null and cod_agente is null;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso standard:"+ls_cod_prodotto+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
			case 4
				update listini_ven_comune
				set scaglione_4 = :ld_qta_scaglione, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo,
						variazione_4 = :ld_variazione, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+"): sfuso senza cliente:"+ls_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if ld_provvigione>0 then
					update provvigioni
					set scaglione_4 = :ld_qta_scaglione,
						variazione_4 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente is null and cod_agente is null;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso standard:"+ls_cod_prodotto+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
			case 5
				update listini_ven_comune
				set scaglione_5 = :ld_qta_scaglione, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo,
						variazione_5 = :ld_variazione, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+"): sfuso senza cliente:"+ls_cod_prodotto+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if ld_provvigione>0 then
					update provvigioni
					set scaglione_5 = :ld_qta_scaglione,
						variazione_5 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente is null and cod_agente is null;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso standard:"+ls_cod_prodotto+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
		end choose
		
	end if
next

destroy lds_data

wf_log(wf_get_today_now() + "Prodotto: SFUSO: importazione completata")

return 1
end function

public function integer wf_importa_sfuso_cliente (string fs_cod_cliente);string			ls_foglio, ls_cod_prodotto
string			ls_cod_prodotto_old
datastore		lds_data
long				ll_rows, ll_count_scaglione
long				ll_i, ll_progressivo, ll_prog_provvigione
decimal			ld_valore
datetime		ldt_valore, ldt_data_inizio_val
integer			li_ret, ll_counter
decimal			ld_min_fatt_sup, ld_qta_scaglione, ld_variazione, ld_provvigione
string			ls_flag_tipo_scaglioni, ls_flag_sconto_mag_prezzo, ls_flag_origine_prezzo, ls_cod_agente

ls_foglio =  "#SFUSO("+fs_cod_cliente+")"
li_ret = wf_prepara_ds_sfuso(lds_data, ls_foglio)

if li_ret = 0 then
	return 0
end if

/*STRUTTURA del DATASTORE
1.  data_inizio_val
2.	 progressivo
3.	cod_prodotto
4.	 cod_cliente
5.	 scaglione_1
6.	 flag_sconto_mag_prezzo_1
7.	 variazione_1
8.	 flag_origine_prezzo_1
9.	 minimo_fatt_superficie
10.provvigione
11.cod agente
*/

//nel ciclo, se il prodotto si ripete aggiungi un nuovo scaglione in tabella
ls_cod_prodotto_old = ""
ll_counter = 1

if isvalid(lds_data) then
	ll_rows = lds_data.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	
	//leggo i valori da inserire dal datastore
	ldt_data_inizio_val = lds_data.getitemdatetime(ll_i, 1)
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 3)
	ls_flag_tipo_scaglioni = "Q"
	ld_qta_scaglione = lds_data.getitemdecimal(ll_i, 5)
	ls_flag_sconto_mag_prezzo = lds_data.getitemstring(ll_i, 6)
	ld_variazione = lds_data.getitemdecimal(ll_i, 7)
	ls_flag_origine_prezzo = lds_data.getitemstring(ll_i, 8)
	ld_min_fatt_sup = lds_data.getitemdecimal(ll_i, 9)
	ld_provvigione = lds_data.getitemdecimal(ll_i, 10)
	ls_cod_agente = lds_data.getitemstring(ll_i, 11)
	
	if not isnull(idt_data_inizio_val) then
		//hai caricato prima il listino comune (xchè hai specificato TUTTI in tipo importazione)
		//ignora le date di excel
		ldt_data_inizio_val = idt_data_inizio_val
	end if
	
	//verifica che la data di creazione sia non esistente per il prodotto (listino comune)
	if wf_controlla_data(ldt_data_inizio_val, is_cod_tipo_listino_prodotto, is_cod_valuta,ls_cod_prodotto,false) then
	else
		//log e salta foglio
		wf_log(wf_get_today_now() + " errore in lettura data validità foglio: "+ls_foglio)
		wf_log(wf_get_today_now() + " data antecedente:  il foglio non è stato elaborato!")
		wf_log("--------------------------------------------------------------------------------------------")
		return 0
	end if
	
	if ls_cod_prodotto <> ls_cod_prodotto_old then
		//primo scaglione di un nuovo prodotto
		ll_counter = 1
		
		//calcola il max + 1 del progressivo
		select max(progressivo)
		into :ll_progressivo
		//from listini_ven_locale
		from listini_vendite
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
			cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val;
			
		if isnull(ll_progressivo) then ll_progressivo = 0
		ll_progressivo += 1
		
		insert into listini_ven_locale
			(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
			cod_prodotto, cod_cliente, minimo_fatt_superficie, flag_tipo_scaglioni,
			scaglione_1, flag_sconto_mag_prezzo_1, variazione_1, flag_origine_prezzo_1)
		values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_progressivo,
			:ls_cod_prodotto, :fs_cod_cliente, :ld_min_fatt_sup, :ls_flag_tipo_scaglioni,
			:ld_qta_scaglione, 	:ls_flag_sconto_mag_prezzo, :ld_variazione, :ls_flag_origine_prezzo);
			
		if sqlca.sqlcode = 0 then
		else
			//errore in inserimento
			g_mb.messagebox("APICE", "Errore in inserimento listino: sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" " &
								+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
		
		//salvo il valore del nuovo prodotto elaborato
		ls_cod_prodotto_old = ls_cod_prodotto
		
		//salvo la provvigione in scaglione 1
		if not isnull(ls_cod_agente) and ls_cod_agente<>""  and ld_provvigione>0 then
			
			//max + 1 del progressivo in base all'indice cu_provvigioni (cod_azienda,cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo)
			select max(progressivo)
			into :ll_prog_provvigione
			from provvigioni
			where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
				cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val;
			
			if isnull(ll_prog_provvigione) then ll_prog_provvigione=0
			
			ll_prog_provvigione += 1
			
			insert into provvigioni
			(cod_azienda, cod_tipo_listino_prodotto, cod_valuta, data_inizio_val, progressivo,
			cod_cat_mer, cod_prodotto, cod_categoria, cod_cliente, cod_agente, flag_tipo_scaglioni,
			scaglione_1,variazione_1)
			values
			(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_prog_provvigione,			
			null, :ls_cod_prodotto, null, :fs_cod_cliente, :ls_cod_agente, 'V',
			:ld_qta_scaglione, :ld_provvigione);
			
			if sqlca.sqlcode = 0 then
			else
				//errore in inserimento provvigione
				g_mb.messagebox("APICE", "Errore in inserimento provvigione: sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" " &
									+ sqlca.sqlerrtext, StopSign!)
				rollback;
				return -1
			end if
			
		end if		
		
	else
		//è un altro scaglione dello stesso prodotto: fare update
		
		ll_counter += 1
		
		//controlla se per caso è superiore al 5 (max 5 scaglioni)
		if ll_counter > 5 then
			//salta la riga e logga
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Prodotto: "+ls_cod_prodotto+" Msg: Hai superato il n° max pari a 5 scaglioni! Riga di excel ignorata")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		
		//verifica che lo scaglione non sia già presente -----------------------------------------------------------
		setnull(ll_count_scaglione)
		select count(*)
		into :ll_count_scaglione
		from listini_ven_locale
		where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:idt_data_inizio_val and progressivo=:ll_progressivo and
					(scaglione_1=:ld_qta_scaglione or scaglione_2=:ld_qta_scaglione or scaglione_3=:ld_qta_scaglione or
						scaglione_4=:ld_qta_scaglione or scaglione_5=:ld_qta_scaglione);
						
		if ll_count_scaglione>0 then
			//si tratta di uno scaglione duplicato: salta la riga e logga
			ll_counter -= 1  //ripristina il valore del counter
			
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Prodotto Sfuso: "+ls_cod_prodotto+" Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
												string(ld_qta_scaglione)+" - Riga di excel ignorata!")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		//-----------------------------------------------------------------------------------------------------------------------
		
		choose case ll_counter
			case 2
				update listini_ven_locale
				set scaglione_2 = :ld_qta_scaglione, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo,
						variazione_2 = :ld_variazione, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+&
												"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if not isnull(ls_cod_agente) and ls_cod_agente<>""  and ld_provvigione>0 then
					update provvigioni
					set scaglione_2 = :ld_qta_scaglione,
						variazione_2 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente=:fs_cod_cliente and cod_agente=:ls_cod_agente;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
			case 3
				update listini_ven_locale
				set scaglione_3 = :ld_qta_scaglione, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo,
						variazione_3 = :ld_variazione, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+&
												"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if not isnull(ls_cod_agente) and ls_cod_agente<>""  and ld_provvigione>0 then
					update provvigioni
					set scaglione_3 = :ld_qta_scaglione,
						variazione_3 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente=:fs_cod_cliente and cod_agente=:ls_cod_agente;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
			case 4
				update listini_ven_locale
				set scaglione_4 = :ld_qta_scaglione, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo,
						variazione_4 = :ld_variazione, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+&
												"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if not isnull(ls_cod_agente) and ls_cod_agente<>""  and ld_provvigione>0 then
					update provvigioni
					set scaglione_4 = :ld_qta_scaglione,
						variazione_4 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente=:fs_cod_cliente and cod_agente=:ls_cod_agente;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
			case 5
				update listini_ven_locale
				set scaglione_5 = :ld_qta_scaglione, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo,
						variazione_5 = :ld_variazione, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo;
				
				if sqlca.sqlcode = 0 then
				else
					//errore in aggiornamento
					g_mb.messagebox("APICE", "Errore in aggiornamento listino (scaglione "+string(ll_counter)+&
												"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
					rollback;
					return -1
				end if
				
				//aggiornamento scaglione successivo provvigione ---------------------
				if not isnull(ls_cod_agente) and ls_cod_agente<>""  and ld_provvigione>0 then
					update provvigioni
					set scaglione_5 = :ld_qta_scaglione,
						variazione_5 = :ld_provvigione
					where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
							cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_prog_provvigione and
							cod_prodotto=:ls_cod_prodotto and cod_cliente=:fs_cod_cliente and cod_agente=:ls_cod_agente;
							
					if sqlca.sqlcode = 0 then
					else
						//errore in aggiornamento
						g_mb.messagebox("APICE", "Errore in aggiornamento provvigione (scaglione "+string(ll_counter)+&
													"): sfuso con cliente:"+ls_cod_prodotto+" - cliente: "+fs_cod_cliente+" "+ sqlca.sqlerrtext, StopSign!)
						rollback;
						return -1
					end if					
				end if
				//-------------------------------------------------------------------------------------------
				
		end choose
				
	end if
next

destroy lds_data

wf_log(wf_get_today_now() + "Prodotto: SFUSO per cliente "+fs_cod_cliente+": importazione completata")

return 1
end function

public subroutine wf_incorpora_iva (string fs_cod_prodotto, ref decimal fd_valore);decimal ld_iva

//ritorna il prezzo IVA inclusa

select cod_iva
into :ld_iva
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_prodotto=:fs_cod_prodotto;
	
if ld_iva<>0 and not isnull(ld_iva) then
else
	//prendi default 20% iva
	ld_iva=20
end if

fd_valore=fd_valore*(1 + ld_iva / 100)
end subroutine

public subroutine wf_scorpora_iva (string fs_cod_prodotto, ref decimal fd_valore);decimal ld_iva

//ritorna il prezzo IVA esclusa

select cod_iva
into :ld_iva
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_prodotto=:fs_cod_prodotto;
	
if ld_iva<>0 and not isnull(ld_iva) then
else
	ld_iva= long(dw_selezione_2.getitemnumber(1,"percentuale_iva"))
end if

ld_iva = 100 + ld_iva
fd_valore = round((fd_valore * 100) / ld_iva,4)
end subroutine

public function integer wf_estrai_valore (string fs_valore, string fs_tipo, ref decimal fd_valore, ref string fs_msg);string ls_appo
long ll_pos1, ll_pos2

ls_appo = fs_valore

choose case fs_tipo
	case "S"
		//	"-xxx.yy%"
		ll_pos1 = pos(ls_appo, "-")
		ll_pos1 += 1
		ls_appo = mid(ls_appo, ll_pos1, len(ls_appo))
		
		ll_pos2 = pos(ls_appo, "%")
		if ll_pos2 = 0 then
			//non c'è il percentuale: prendo tutto
			ls_appo = trim(ls_appo)
		else
			ls_appo = left(ls_appo, ll_pos2 - 1)
			ls_appo = trim(ls_appo)
		end if
		
		try
			if not isnumber(ls_appo) then
				fs_msg = "sconto specificato invalido:"+fs_valore
				return -1
			end if
			
			fd_valore = dec(ls_appo)
			
		catch (RuntimeError rte1)
			fs_msg = "sconto specificato invalido:"+fs_valore
			return -1
		end try
			
		//arrotondo a 2 cifre decimali
		fd_valore = round(fd_valore, 2)
		
		if fd_valore>100 then
			fs_msg = "sconto specificato superiore al 100%"
			return -1
		end if
	
	case "M"
		//	"+xxx.yy%"
		ll_pos1 = pos(ls_appo, "+")
		ll_pos1 += 1
		ls_appo = mid(ls_appo, ll_pos1, len(ls_appo))
		
		ll_pos2 = pos(ls_appo, "%")
		if ll_pos2 = 0 then
			//non c'è il percentuale: prendo tutto
			ls_appo = trim(ls_appo)
		else
			ls_appo = left(ls_appo, ll_pos2 - 1)
			ls_appo = trim(ls_appo)
		end if
		
		try
			if not isnumber(ls_appo) then
				fs_msg = "maggiorazione specificata invalida:"+fs_valore
				return -1
			end if
			
			fd_valore = dec(ls_appo)
			fd_valore = abs(fd_valore)
			
		catch (RuntimeError rte2)
			fs_msg = "maggiorazione specificata invalida:"+fs_valore
			return -1
		end try
		
		//arrotondo a 2 cifre decimali
		fd_valore = round(fd_valore, 2)
		
		if fd_valore>100 then
			fs_msg = "maggiorazione specificata superiore al 100%"
			return -1
		end if

	
	case "A"
		//	"+xxxxxx.yy"		
		ll_pos1 = pos(ls_appo, "+")
		ll_pos1 += 1
		ls_appo = mid(ls_appo, ll_pos1, len(ls_appo))
		ls_appo = trim(ls_appo)
		
		try
			if not isnumber(ls_appo) then
				fs_msg = "aumento specificato invalido:"+fs_valore
				return -1
			end if
			
			fd_valore = dec(ls_appo)
			fd_valore = abs(fd_valore)
			
		catch (RuntimeError rte3)
			fs_msg = "aumento specificato invalido:"+fs_valore
			return -1
		end try
		
		//arrotondo a 2 cifre decimali
		fd_valore = round(fd_valore, 2)
	
	
	case "D"
		//	"-xxxxxx.yy"
		ll_pos1 = pos(ls_appo, "-")
		ll_pos1 += 1
		ls_appo = mid(ls_appo, ll_pos1, len(ls_appo))
		ls_appo = trim(ls_appo)
		
		try
			if not isnumber(ls_appo) then
				fs_msg = "diminuzione specificata invalida:"+fs_valore
				return -1
			end if
			
			fd_valore = dec(ls_appo)
			
		catch (RuntimeError rte4)
			fs_msg = "diminuzione specificata invalida:"+fs_valore
			return -1
		end try
		
		//arrotondo a 2 cifre decimali
		fd_valore = round(fd_valore, 2)
		
	case else
		fs_msg = "caso non compreso tra i valori (S-M-A-D)"
		return -1

end choose

return 1
end function

public function integer wf_prepara_ds_sfuso (ref datastore fds_data, string fs_foglio);string			ls_sql, ls_valore, ls_tipo_variazione, ls_cod_prodotto, ls_msg, ls_cod_agente
long				ll_rows, ll_riga, ll_colonna, ll_newrow, ll_count
decimal			ld_valore
datetime		ldt_valore
integer			li_ret

//mi serve un datastore vuoto dove ricoverare i dati del foglio excel dello sfuso
ls_sql = "select "+&						
						"data_inizio_val,"+&
						"progressivo,"+&
						"cod_prodotto,"+&
						"cod_cliente,"+&
						"scaglione_1,"+&
						"flag_sconto_mag_prezzo_1,"+&
						"variazione_1,"+&
						"flag_origine_prezzo_1,"+&
						"minimo_fatt_superficie,"+&
						"0.0000 as provvigione,"+&
						"cod_azienda as cod_agente "+&
			"from listini_vendite "+&			
			"where cod_azienda='VALOREASSURDO' "
			
if f_crea_datastore(fds_data, ls_sql) then
else
	wf_log(wf_get_today_now() + " Errore di datastore: Foglio SFUSO")
	return 0
end if

//fs_foglio = "#SFUSO"
ll_riga = 3	//i dati sono presenti a partire dalla riga n°3

//il codice agente è sulla colonna provvigione

do while 1=1
	//le colonne nel foglio di sfuso sono fissate a 11
	/*	
	1.VALIDITA (datetime)					 2.CODICE (stringa)			 		3.DESCRIZIONE (stringa)		4.SCAGLIONE (integer)				 
	5.QTA SCAGLIONE (decimal)		 6.TIPO VARIAZ. (stringa) 			7.UM (stringa)						8.VALORE (decimal/stringa)		 
	9.ORIGINE (stringa)					10.MINIMO_SUP (decimal)			11.PROVVIGIONE (decimal)
	*/
			
	//VALIDITA ----------------------------------------------------------------------------------------------------------------------------------
	ll_colonna = 1
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "T")
	if li_ret = 1 then
		ll_newrow = fds_data.insertrow(0)	//inserisci la riga
		fds_data.setitem(ll_newrow, 1, ldt_valore)
		
	elseif li_ret = 0 then
		//i dati da leggere sono finiti
		exit
	else
		//errore in lettura: salta il foglio e logga
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
		
	//CODICE ------------------------------------------------------------------------------------------------------------------------------------
	ll_colonna = 2
	setnull(ls_cod_prodotto)
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prodotto, ld_valore, ldt_valore, "S")
	if li_ret = 1 then
		
		//verifica la esistenza del prodotto in anagrafica prodotti
		select count(*)
		into :ll_count
		from anag_prodotti
		where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto;
		
		if ll_count > 0 then
		else
			//errore in lettura: logga		
			wf_log(wf_get_today_now() + " prodotto sfuso '"+ls_cod_prodotto+"' non trovato in anagrafica: "+fs_foglio)
			wf_log("Il prodotto "+ls_cod_prodotto+" verrà saltato!")
			wf_log("-------------------------------------------------------------------")
			
			//poichè il prodotto non esiste lo salto, ma prima elimino la riga dal datastore
			fds_data.deleterow(ll_newrow)
			
			ll_riga += 1
			continue
			//return 0
			
		end if
		
		fds_data.setitem(ll_newrow, 3, ls_cod_prodotto)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
       
	//QTA SCAGLIONE---------------------------------------------------------------------------------------------------------------------------
	ll_colonna = 5
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then		
		fds_data.setitem(ll_newrow, 5, ld_valore)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//TIPO VARIAZ --------------------------------------------------------------------------------------------------------------------------------
	//S (sconto:				 "-xxx.yy%"		)
	//M (maggiorazione:	"+xxx.yy%"		)
	//A (aumento:				"+xxxxx.yy"		)
	//D (diminuzione:		"-xxxxx.yy"		)
	//P (prezzo:					xxxxx.yy			)
	ll_colonna = 6
	setnull(ls_tipo_variazione)
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_tipo_variazione, ld_valore, ldt_valore, "S")
	if li_ret = 1 then		
		fds_data.setitem(ll_newrow, 6, ls_tipo_variazione)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//VALORE --------------------------------------------------------------------------------------------------------------------------------
	//in base al valore assunto das tipo variaz.
	//S (sconto:				 "-xxx.yy%"		)
	//M (maggiorazione:	"+xxx.yy%"		)
	//A (aumento:				"+xxxxx.yy"		)
	//D (diminuzione:		"-xxxxx.yy"		)
	//P (prezzo:					xxxxx.yy			)
	ll_colonna = 8
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	/*
	if ls_tipo_variazione="P" then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
		//chiama qui una funzione che converte in decimale....
		if wf_estrai_valore(ls_valore, ls_tipo_variazione, ld_valore, ls_msg) = -1 then
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log(wf_get_today_now() + " "+ls_msg)
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	*/
	if li_ret = 1 then
		//verifica se devi fare lo scorporo iva (scorora se P, A o D)
		if is_flag_scorporo_iva = "S" and (ls_tipo_variazione="P" or ls_tipo_variazione="A" or ls_tipo_variazione="D") then 
			wf_scorpora_iva(ls_cod_prodotto, ld_valore)
		end if
		
		fds_data.setitem(ll_newrow, 7, ld_valore)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//ORIGINE --------------------------------------------------------------------------------------------------------------------------------	
	ll_colonna = 9
	setnull(ls_tipo_variazione)
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if li_ret = 1 then		
		fds_data.setitem(ll_newrow, 8, ls_valore)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//MIN SUPERFICIE --------------------------------------------------------------------------------------------------------------------------	
	ll_colonna = 10
	setnull(ls_tipo_variazione)
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then		
		fds_data.setitem(ll_newrow, 9, ld_valore)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//PROVIGIONE --------------------------------------------------------------------------------------------------------------------------------	
	ll_colonna = 11
	
	//solo alla prima lettura: acquisisci l'agente
	if ll_riga = 3 and pos(fs_foglio, "(")>0 then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, 1, ll_colonna, ls_cod_agente, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			select count(*)
			into :ll_count
			from anag_agenti
			where anag_agenti.cod_azienda=:s_cs_xx.cod_azienda and anag_agenti.cod_agente=:ls_cod_agente;
			
			if ll_count > 0 then
			else
				//errore in lettura: logga		
				wf_log(wf_get_today_now() + " agente non trovato in anagrafica: "+fs_foglio)
				wf_log("-------------------------------------------------------------------")
				setnull(ls_cod_agente)
			end if
		else
			//errore in lettura: logga		
			wf_log(wf_get_today_now() + " agente non inserito in excel: "+fs_foglio)
			wf_log("-------------------------------------------------------------------")
			setnull(ls_cod_agente)			
		end if
	end if
	
	fds_data.setitem(ll_newrow, 11, ls_cod_agente)
	
	setnull(ls_tipo_variazione)
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then		
		fds_data.setitem(ll_newrow, 10, ld_valore)
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	ll_riga += 1
loop

//arrivato qui ho il datastore riempito da elaborare

ll_rows = fds_data.rowcount()

fds_data.setsort("cod_prodotto asc, scaglione_1 asc")
fds_data.sort()

return 1
end function

public function integer wf_estrai_valore_dim (string fs_valore, string fs_cod_prodotto, ref string fs_tipo, ref decimal fd_valore, ref string fs_msg);string ls_appo
long ll_pos1, ll_pos2

ls_appo = fs_valore
ls_appo = trim(ls_appo)

//devo capire il tipo
/*	S		"-xxx.yy%" 	(max 100)
	M		+xxx.yy%	(max 100)
	A		+xxxx.yy
	D		-xxxx.yy
	P		 xxxxxx.yy
*/
ll_pos1 = pos(ls_appo, "%")
if ll_pos1 > 0 then
	//Sconto o Maggiorazione
	//togli il percentuale
	ls_appo = left(ls_appo, len(ls_appo) - 1)
	
	ll_pos2 = pos(ls_appo, "-")
	if ll_pos2 > 0 then
		//Sconto			
		
		if isnumber(ls_appo) then
		else
			fs_msg = "sconto specificato invalido:"+fs_valore
			return -1
		end if
		
		fs_tipo = "S"
		fd_valore = dec(ls_appo)
		fd_valore = abs(fd_valore)
		if fd_valore > 100 then
			fs_msg = "Sconto specificato superiore al 100%"
			return -1
		end if
	
	else
		//Maggiorazione: verifica se c'è il +?????????
		//ll_pos2 = pos(ls_appo, "+")
		if isnumber(ls_appo) then
		else
			fs_msg = "Maggiorazione specificata invalido:"+fs_valore
			return -1
		end if
		
		fs_tipo = "M"
		fd_valore = dec(ls_appo)
		fd_valore = abs(fd_valore)
		if fd_valore > 100 then
			fs_msg = "Maggiorazione specificata superiore al 100%"
			return -1
		end if
	end if
else	//non esiste il % --------------------------------------------------------------
	//Aumento(+) o Diminuzione(-) o Prezzo():  in ogni caso controlla se scorporare l'iva
	ll_pos2 = pos(ls_appo, "-")
	
	if ll_pos2 > 0 then
		//Diminuzione
		if isnumber(ls_appo) then
		else
			fs_msg = "Diminuzione specificata invalida:"+fs_valore
			return -1
		end if
		
		fs_tipo = "D"
		fd_valore = dec(ls_appo)
		fd_valore = abs(fd_valore)
		
	else
		ll_pos2 = pos(ls_appo, "+")
		
		if ll_pos2 > 0 then
			//Aumento
			if isnumber(ls_appo) then
			else
				fs_msg = "Aumento specificato invalido:"+fs_valore
				return -1
			end if
			
			fs_tipo = "A"
			fd_valore = dec(ls_appo)
			fd_valore = abs(fd_valore)
			
		else
			//Prezzo
			
			if isnumber(ls_appo) then
			else
				fs_msg = "Prezzo specificato invalido:"+fs_valore
				return -1
			end if
			
			fs_tipo = "P"
			fd_valore = dec(ls_appo)
			fd_valore = abs(fd_valore)			
		end if
	end if
	
	//scorpora l'iva se necessario
	if is_flag_scorporo_iva="S" and fd_valore > 0 then wf_scorpora_iva(fs_cod_prodotto, fd_valore)
end if

//arrotonda a 2 cifre decimali
fd_valore = round(fd_valore, 2)

return 1
end function

public function integer wf_prepara_ds_dimensioni (string fs_foglio, string fs_cod_prodotto, decimal fd_x[], decimal fd_y[], ref datastore fds_data);//legge la griglia delle dimensioni dal foglio excel e la prepara in un datastore

string			ls_sql, ls_valore, ls_tipo_variazione, ls_cod_prodotto, ls_msg
long				ll_rows, ll_riga, ll_colonna, ll_newrow, ll_x, ll_y
decimal			ld_valore, ld_appo
datetime		ldt_valore
integer			li_ret

//mi serve un datastore vuoto dove ricoverare i dati del foglio excel delle dimensioni
ls_sql = "select "+&
						"num_scaglione,"+&
						"limite_dimensione_1,"+&
						"limite_dimensione_2,"+&
						"flag_sconto_mag_prezzo,"+&
						"variazione "+&
			"from listini_vendite_dimensioni "+&			
			"where cod_azienda='VALOREASSURDO' "
			
if f_crea_datastore(fds_data, ls_sql) then
else
	wf_log(wf_get_today_now() + " Errore di datastore: Foglio "+fs_foglio)
	return 0
end if

//i dati dei valori delle dimensioni sono presenti a partire dalla riga n°3 e colonna 2
//ogni riga del datastore sarà una cella di dimensione: verranno riportate le dimensioni x e y e il relativo valore
//noi partiamo da un valore in meno per riga e per colonna
ll_riga = 2
ll_colonna = 1

//le righe/colonne delle dimensioni del foglio di configurato sono dinamiche
//ma grazie a upperbound(fd_x) e upperbound(fd_y) queste dimensioni le so a-priori

//leggo in senso orizzontale da sx a dx (scorro le varie dim x) e poi passo alla riga sotto (nuova dim y)
/*	FOGLIO EXCEL
	..........................................................
	DIM	 X1		 X2		 X3		...
	Y1		d11		d12		d13		...
	Y2		d21		d22		d23		...
	...		...			...			...			...
*/
for ll_y = 1 to upperbound(fd_y)
	ll_riga += 1	//sono in corrispondenza di Yi
	ll_colonna = 1		//IMPORTANTE: rimetto l'indice di colonna ad 1 !!!!!!!!!!!!!!!!!!!!
		
	for ll_x = 1 to upperbound(fd_x)
		ll_colonna += 1	//sono in corrispondenza di Xi
		
		//leggo il valore tipo stringa (in teoria non dovrei avere problemi, se ce ne sono loggo e non elaboro il foglio)
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
		if li_ret = 1 then		
			//mi occorre sapere che tipo di dato ho letto per distinguere il flag_sconto_mag_prezzo
			/*	S (sconto:				 "-xxx.yy%"		max 100)			esiste il carattere % ed il carattere -
				M (maggiorazione:	"+xxx.yy%"		max 100)			esiste il carattere % (facoltativo: ed il carattere +)
				A (aumento:				"+xxxxx.yy"				)			esiste solo carattere +
				D (diminuzione:		"-xxxxx.yy"					)			esiste solo il carattere -
				P (prezzo:					xxxxx.yy					)			dato numerico puro senza segno				*/
			
			//f.ne a cui passo il val stringa e mi restituisce il val decimal e il flag_sconto_mag_prezzo
			//tiene conto anche dell'eventuale scorporo iva (solo P, A, D)
			li_ret = wf_estrai_valore_dim(ls_valore, fs_cod_prodotto, ls_tipo_variazione, ld_valore, ls_msg)
			if li_ret = 1 then
				//arrivato fin qui inserisco la riga nel datastore
				ll_newrow = fds_data.insertrow(0)
				fds_data.setitem(ll_newrow, "num_scaglione", 1)
				
				//controlla se sei nel caso 99999999........(excel mette un numero con diverse cifre decimali)
				//allora per uniformità con quello che era salvato nel listino metto 99999999.00
				setnull(ld_appo)
				ld_appo = fd_x[ll_x]
				if truncate(fd_x[ll_x], 0) = 99999999 then
					ld_appo = truncate(fd_x[ll_x], 0)
				end if
				fds_data.setitem(ll_newrow, "limite_dimensione_1", ld_appo)
				//fds_data.setitem(ll_newrow, "limite_dimensione_1", fd_x[ll_x])
				
				setnull(ld_appo)
				ld_appo = fd_y[ll_y]
				if truncate(fd_y[ll_y], 0) = 99999999 then
					ld_appo = truncate(fd_y[ll_y], 0)
				end if
				fds_data.setitem(ll_newrow, "limite_dimensione_2", ld_appo)
				//fds_data.setitem(ll_newrow, "limite_dimensione_2", fd_y[ll_y])
				
				fds_data.setitem(ll_newrow, "flag_sconto_mag_prezzo", ls_tipo_variazione)
				fds_data.setitem(ll_newrow, "variazione", ld_valore)
				
			else
				//errore in lettura dimensioni: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati (DIM) foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log(wf_get_today_now() + " "+ls_msg)
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
			
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati (DIM) foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
		//---------------------------------------------------------------------------------------------------------------------------------------------------
		
	next
next

fds_data.setsort("limite_dimensione_1 asc, limite_dimensione_2 asc")
fds_data.sort()

return 1
end function

public function integer wf_controlla_dimensioni (string fs_cod_prodotto, string fs_foglio, ref boolean fb_dimensioni, ref decimal fd_x_excel[], ref decimal fd_y_excel[]);decimal			ld_x[], ld_y[]
long				ll_riga, ll_colonna, li_ret, ll_i
string			ls_valore
decimal			ld_valore
datetime		ldt_valore

//controlla che non siano state manomesse le dimensioni
//DIM si trova nella riga 2 colonna 1
//attenzione se tale cella è vuota vuol dire che il prodotto non aveva le dimensioni!!!!

//carica da APICE quello che c'è (tab_prodotti_dimensioni e tab_prodotti_dimensioni_det x prodotto)
wf_carica_dimensioni(fs_cod_prodotto, ld_x [], ld_y [])

//prova a leggere il valore DIM
ll_riga = 2
ll_colonna = 1
li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
if li_ret = 1 and upper(ls_valore) = "DIM" then		
	//leggi le dimensioni da excel
	
	//dim1 in orizzontale -----------------------------------------
	for ll_i = 1 to 100
		ll_colonna += 1
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then
			
			string ls_1
			ls_1 = string(ld_valore)
			
			if isnumber(string(ld_valore)) then
				fd_x_excel[ll_i] = ld_valore
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log(wf_get_today_now() + " problemi nella lettura di una dimensione 1:  il foglio non è stato elaborato!")
				wf_log("--------------------------------------------------------------------------------------------")
				return 0
			end if
			
		elseif li_ret = 0 then
			//i dati da leggere sono finiti
			exit
		
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log(wf_get_today_now() + " problemi nella lettura di una dimensione 1:  il foglio non è stato elaborato!")
			wf_log("--------------------------------------------------------------------------------------------")
			return 0
		end if
	next
	
	//dim2 in verticale ------------------------------------------
	ll_riga = 2
	ll_colonna = 1
	for ll_i = 1 to 100
		ll_riga += 1
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then
			
			ls_1=string(ld_valore)
			
			if isnumber(string(ld_valore)) then
				fd_y_excel[ll_i] = ld_valore
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log(wf_get_today_now() + " problemi nella lettura di una dimensione 1:  il foglio non è stato elaborato!")
				wf_log("--------------------------------------------------------------------------------------------")
				return 0
			end if
			
		elseif li_ret = 0 then
			//i dati da leggere sono finiti
			exit
			
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + +" errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log(wf_get_today_now() + " problemi nella lettura di una dimensione 1:  il foglio non è stato elaborato!")
			wf_log("--------------------------------------------------------------------------------------------")
			return 0
		end if
	next
	
	//confronta le dimensioni -------------------------------------
	
	//per numero di valori: dim 1
	if upperbound(ld_x) <> upperbound(fd_x_excel) then
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio)
		wf_log(wf_get_today_now() + " incongruenza nella numero totale delle dimensioni 1 (tra excel e APICE):  il foglio non è stato elaborato!")
		wf_log("--------------------------------------------------------------------------------------------")
		return 0
	end if
	
	//per numero di valori: dim 2
	if upperbound(ld_y) <> upperbound(fd_y_excel) then
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio)
		wf_log(wf_get_today_now() + " incongruenza nella numero totale delle dimensioni 2 (tra excel e APICE):  il foglio non è stato elaborato!")
		wf_log("--------------------------------------------------------------------------------------------")
		return 0
	end if
	
	//per valori: dim 1 (okkio al caso 99999999.9999.....in tal caso confronta le parti intere perchè excel fa un burdello!!!!)
	for ll_i=1 to upperbound(ld_x)
		if (ld_x[ll_i] = fd_x_excel[ll_i]) or &
					((truncate(fd_x_excel[ll_i],0)=99999999) and (truncate(ld_x[ll_i],0) = truncate(fd_x_excel[ll_i],0)) ) then
					
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio)
			wf_log(wf_get_today_now() + " incongruenza nel valore n° "+string(ll_i)+" per la dimensione 1")
			wf_log(wf_get_today_now() + " Valore in APICE:"+string(ld_x[ll_i]) + " - valore in EXCEL:"+string(fd_x_excel[ll_i]))
			wf_log(wf_get_today_now() + " il foglio non è stato elaborato!")
			wf_log("--------------------------------------------------------------------------------------------")
			return 0
		end if
	next
	
	//per valori. dim 2
	for ll_i=1 to upperbound(ld_y)
		if (ld_y[ll_i] = fd_y_excel[ll_i]) or &
					((truncate(fd_y_excel[ll_i],0)=99999999) and (truncate(ld_y[ll_i],0) = truncate(fd_y_excel[ll_i],0)) ) then
			
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio)
			wf_log(wf_get_today_now() + " incongruenza nel valore n° "+string(ll_i)+" per la dimensione 2")
			wf_log(wf_get_today_now() + " Valore in APICE:"+string(ld_y[ll_i]) + " - valore in EXCEL:"+string(fd_y_excel[ll_i]))
			wf_log(wf_get_today_now() + " il foglio non è stato elaborato!")
			wf_log("--------------------------------------------------------------------------------------------")
			return 0
		end if
	next
	
	//se arrivi fin qui è tutto OK
	fb_dimensioni = true

else
	//nella cella non hai trovato DIM e nient'altro: probabile che non esistano le dimensioni: verifica
	if upperbound(ld_x) = 0 and upperbound(ld_y) = 0 then
		//OK
		fb_dimensioni = false
	else
		//problema: qualcuno ha inserito successivamente le dimensioni in APICE
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log(wf_get_today_now() + " non sono state trovate le dimensioni su excel mentre sono presenti in APICE!  Il foglio non è stato elaborato!")
		wf_log("--------------------------------------------------------------------------------------------")
		return 0
	end if
end if

return 1
end function

public function integer wf_prepara_ds_varianti (string fs_foglio, string fs_flag, ref long fl_offset, ref datastore fds_data);//se fs_flag = "STD" è un configurato
string			ls_sql, ls_valore, ls_msg, ls_cod_versione_default, ls_cod_prodotto_padre
string			ls_tipo_var_std, ls_tipo_var_pers
string			ls_cod_prod_std, ls_cod_prod_pers, ls_cod_prod_variante
long				ll_rows, ll_riga, ll_colonna, ll_newrow
decimal			ld_valore, ld_num_var
datetime		ldt_valore
integer			li_ret
boolean		lb_std

//mi serve un datastore vuoto dove ricoverare i dati del foglio excel delle varianti
ls_sql = "select "+&
						"100 as prog_var,"+&
						"cod_cliente,"+&
						"cod_prodotto_figlio,"+&
						"scaglione_1,"+&
						"flag_sconto_mag_prezzo_1,"+&
						"variazione_1,"+&
						"flag_origine_prezzo_1,"+&
						"0.00 as provvigione,"+&
						"cod_prodotto_padre,"+&
						"'      ' as versione_default "+&
			"from listini_produzione "+&			
			"where cod_azienda='VALOREASSURDO' "
			
if f_crea_datastore(fds_data, ls_sql) then
else
	wf_log(wf_get_today_now() + " Errore di datastore: Foglio: "+fs_foglio)
	return 0
end if

ll_riga = 3	//i dati sono presenti a partire dalla riga n°3

//la etichetta VAR, presente sulla riga 2 
//mentre il n° colonna è:	upperbound(ld_x_excel[] + 3) e vale anche nel caso in cui non ci siano DIM (0 + 3 = 3)
//questa info è nell'argomento fl_offset
fl_offset = fl_offset - 1		//decremento perchè poi viene re-incrementato

lb_std = false
if fs_flag="STD" then
	lb_std = true
end if

do while 1=1
	//le colonne nel foglio di sfuso sono fissate a :
	//         12 (se configurato- standard)       e             10 (se configurato-cliente)
	
	/*	  1. 					è un progressivo numerico (ultimo criterio di ordinamento)
		  2.	COD CLIENTE									  2.	VARIANTE (cod_prodotto_figlio)
		  3.	RAG.SOC.										 	  3.	DES-VARIANTE
		  4.	VARIANTE (cod_prodotto_figlio)			  4.	SCAGLIONE
		  5.	DES-VARIANTE									  5.	TIPO VAR (flag_sconto_mag_prezzo)
		  6.	SCAGLIONE										  6.	VARIAZ.	
		  7.	TIPO VAR	(flag_sconto_mag_prezzo)	  7.	ORIGINE (flag_origine_prezzo)
		  8.	VARIAZ.												  8.	PROVV.
		  9.	ORIGINE (flag_origine_prezzo)			  9.	cod_prodotto_padre
		10.	PROVV.												10.	versione_default
		11.	cod_prodotto_padre
		12.	versione_default
	*/
	
	//colonna VAR : usato solo per sapere se ci sono ancora dati da leggere
	ll_colonna = fl_offset +1
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_num_var, ldt_valore, "D")
	if li_ret = 1 then
		ll_newrow = fds_data.insertrow(0)	//inserisci la riga
		fds_data.setitem(ll_newrow, 1, ld_num_var)
	elseif li_ret = 0 then
		//i dati da leggere sono finiti
		exit
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	//--------------------------------------------------------------------------

	
	
	//2.	COD CLIENTE				  2.	VARIANTE (cod_prodotto_figlio)--------------------------------------------------------
	ll_colonna = fl_offset + 2
	if lb_std then
		//se la lettura da errore assumi cliente vuoto.......
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
		if li_ret = 1 then				
		else
			setnull(ls_valore)
		end if
		fds_data.setitem(ll_newrow, 2, ls_valore)	//cod_cliente			
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prod_pers, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 3, ls_cod_prod_pers)	//cod_prodotto_figlio
			ls_cod_prod_variante = ls_cod_prod_pers
		elseif li_ret = 0 then
			//i dati da leggere sono finiti
			exit
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	
	//-------------------------------------------------------------------------------------------------------------------
	//3.	RAG.SOC.	 	  3.	DES-VARIANTE--------------------------------------------------------------
	//non mi interessa -----------------------------------------------------------
	
	//4.	VARIANTE (cod_prodotto_figlio)			  4.	SCAGLIONE
	ll_colonna = fl_offset + 4
	if lb_std then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prod_std, ld_valore, ldt_valore, "S")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 3, ls_cod_prod_std)	//cod_prodotto_figlio
			ls_cod_prod_variante = ls_cod_prod_std
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if		
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 4, ld_valore)	//scaglione
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	
	//5.	DES-VARIANTE		TIPO VAR (flag_sconto_mag_prezzo)
	ll_colonna = fl_offset + 5
	if lb_std then
		//non leggo un bel niente.....
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_tipo_var_pers, ld_valore, ldt_valore, "S")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 5, ls_tipo_var_pers)	//flag_sconto_mag_prezzo
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if

	//6.	SCAGLIONE			VARIAZ.	 
	ll_colonna = fl_offset + 6
	if lb_std then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 4, ld_valore)	//scaglione
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if		
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then				
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
		/*
		if ls_tipo_var_pers="P" then
			li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
			if li_ret = 1 then				
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
		else
			li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
			if li_ret = 1 then
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
			
			//chiama qui una funzione che converte in decimale....
			if wf_estrai_valore(ls_valore, ls_tipo_var_pers, ld_valore, ls_msg) = -1 then
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log(wf_get_today_now() +  + " " +ls_msg)
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
		end if
		*/
		
		//verifica se devi fare lo scorporo iva (scorpora se P, A o D)
		if is_flag_scorporo_iva = "S" and (ls_tipo_var_pers="P" or ls_tipo_var_pers="A" or ls_tipo_var_pers="D") then 
			wf_scorpora_iva(ls_cod_prod_pers, ld_valore)
		end if
		
		fds_data.setitem(ll_newrow, 6, ld_valore)	//variazione	
	end if
	
	
	// 7.	TIPO VAR	(flag_sconto_mag_prezzo)	  7.	ORIGINE (flag_origine_prezzo)
	ll_colonna = fl_offset + 7
	if lb_std then		
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_tipo_var_std, ld_valore, ldt_valore, "S")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 5, ls_tipo_var_std)	//flag_sconto_mag_prezzo
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 7, ls_valore)	//flag_origine_prezzo
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	

	// 8.	VARIAZ.												  8.	PROVV.
	ll_colonna = fl_offset + 8
	if lb_std then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
		/*
		if ls_tipo_var_std="P" then
			li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
			if li_ret = 1 then
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
		else
			li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
			if li_ret = 1 then
			else
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
			
			//chiama qui una funzione che converte in decimale....
			if wf_estrai_valore(ls_valore, ls_tipo_var_std, ld_valore, ls_msg) = -1 then
				//errore in lettura: salta il foglio e logga		
				wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
				wf_log(wf_get_today_now() + " " +ls_msg)
				wf_log("-------------------------------------------------------------------")
				return 0
			end if
		end if
		*/
		
		//verifica se devi fare lo scorporo iva (scorpora se P, A o D)
		if is_flag_scorporo_iva = "S" and (ls_tipo_var_std="P" or ls_tipo_var_std="A" or ls_tipo_var_std="D") then 
			wf_scorpora_iva(ls_cod_prod_std, ld_valore)
		end if
		
		fds_data.setitem(ll_newrow, 6, ld_valore)	//variazione	
		
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_tipo_var_std, ld_valore, ldt_valore, "D")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 8, ld_valore)	//provvigione
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	
	
	//  9.	ORIGINE (flag_origine_prezzo)			  9.	cod_prodotto_padre
	ll_colonna = fl_offset + 9
	if lb_std then		
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 7, ls_valore)	//flag_origine_prezzo
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prodotto_padre, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 9, ls_cod_prodotto_padre)	//cod_prodotto_padre
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	

	//	10.	PROVV.												10.	versione_default
	ll_colonna = fl_offset + 10
	if lb_std then		
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
		if li_ret = 1 then			
			fds_data.setitem(ll_newrow, 8, ld_valore)	//provvigione
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	else
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_versione_default, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 10, ls_cod_versione_default)	//versione_default
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	
	
	//11.	SOLO STD:    cod_prodotto_padre
	ll_colonna = fl_offset + 11
	if lb_std then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prodotto_padre, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 9, ls_cod_prodotto_padre)	//cod_prodotto_padre
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
	
	
	//	12. SOLO STD:     	versione_default	
	ll_colonna = fl_offset + 12
	if lb_std then
		li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_versione_default, ld_valore, ldt_valore, "S")
		if li_ret = 1 then
			fds_data.setitem(ll_newrow, 10, ls_cod_versione_default)	//versione_default
		else
			//errore in lettura: salta il foglio e logga		
			wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
			wf_log("-------------------------------------------------------------------")
			return 0
		end if
	end if
		
	////verifica la congruità dei dati relativi a cod_prodotto_padre e versione defaut
	////estrai il prodotto dal nome foglio
	//wf_estrai_prodotto_cliente(fs_foglio, ls_cod_prodotto_padre, ls_valore)
	
	//controlla che la variante esista in anagrafica
	if wf_controlla_padre_versione(ls_cod_prod_variante, ls_cod_prodotto_padre, ls_cod_versione_default, ls_msg) <= 0 then
		//salta il foglio e logga
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+": - "+ls_msg)
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	ll_riga += 1
loop

//arrivato qui ho il datastore riempito da elaborare

ll_rows = fds_data.rowcount()

fds_data.setsort("cod_cliente asc, cod_prodotto_figlio asc, scaglione_1 asc")
fds_data.sort()

return 1
end function

public function integer wf_estrai_data (string fs_foglio, ref datetime fdt_data_inizio_val);long li_ret, ll_riga, ll_colonna
string ls_valore, ls_appo
decimal ld_valore
datetime ldt_valore

ll_riga = 1
ll_colonna = 1

li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
if li_ret = 1 then			
	//OK: ora cerca di estrapolare la data
else
	//errore in lettura: salta il foglio e logga		
	wf_log(wf_get_today_now() + " errore in lettura dati foglio (DATA NUOVO LISTINO NON TROVATA): "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
	wf_log("-------------------------------------------------------------------")
	return -1
end if		

if ls_valore <> "" and not isnull(ls_valore) then
else
	//errore in lettura: salta il foglio e logga		
	wf_log(wf_get_today_now() + " errore in lettura dati foglio (DATA NUOVO LISTINO NON TROVATA): "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
	wf_log("-------------------------------------------------------------------")
	return -1
end if

//esempi
//"LISTINO GENERALE DAL (04/02/2010)"
//TENDE DA SOLE SPA, ESTE (PD) - (DAL 04/02/2010)
//ragioniamo così: leggiamo da destra
//facciamo il trim del valore stringa a destra
ls_appo = trim(ls_valore)

ls_appo = mid(ls_appo, len(ls_appo) - 10, 10)
if isdate(ls_appo) then
	fdt_data_inizio_val = datetime(date(ls_appo), time("00:00:00"))
else
	//errore in lettura: salta il foglio e logga		
	wf_log(wf_get_today_now() + " errore in lettura dati foglio (il valore della data '"+ls_appo+"' non è valido): "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
	wf_log("-------------------------------------------------------------------")
	return -1
end if

return 1
end function

public function integer wf_carica_ds_varianti (ref datastore fds_varianti, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, boolean fb_comune);string ls_sql, ls_syntax, ls_errore
long ll_ret

//se fb_comune è true si tratta di un foglio standard
if fb_comune then
	ls_sql = "select "+&
					"l.cod_azienda,l.cod_tipo_listino_prodotto,l.cod_valuta,l.data_inizio_val,l.progressivo,l.cod_prodotto_listino,"+&
					"isnull(l.cod_cliente,'') as cod_cliente,isnull(cli.rag_soc_1,'') as rag_soc_1,"+&
					"l.cod_prodotto_figlio,p.des_prodotto,"+&
					"l.scaglione_1,l.scaglione_2,l.scaglione_3,l.scaglione_4,l.scaglione_5,"+&
					"l.flag_sconto_mag_prezzo_1,l.flag_sconto_mag_prezzo_2,l.flag_sconto_mag_prezzo_3,"+&
								"l.flag_sconto_mag_prezzo_4,l.flag_sconto_mag_prezzo_5,"+&
					"l.variazione_1,l.variazione_2,l.variazione_3,l.variazione_4,l.variazione_5,"+&
					"l.flag_origine_prezzo_1,l.flag_origine_prezzo_2,l.flag_origine_prezzo_3,l.flag_origine_prezzo_4,"+&
								"l.flag_origine_prezzo_5,"+&
					"isnull(provvigione_1, 0) as provvigione,"+&
					"l.cod_prodotto_padre,l.cod_versione as versione_default "+&
					",'N' as flag_escludi_linee_sconto "+&
				"from listini_prod_comune as l "+&
				"join anag_prodotti p on p.cod_azienda=l.cod_azienda and p.cod_prodotto=l.cod_prodotto_figlio "+&
				"left outer join anag_clienti cli on cli.cod_azienda=l.cod_azienda and cli.cod_cliente=l.cod_cliente "+&				
				"where l.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"l.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"' "+&
					"and l.cod_valuta='"+fs_cod_valuta+"' and "+&
					"l.data_inizio_val='"+string(fdt_data_inizio_val,s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"l.progressivo="+string(fl_progressivo)+" "					
					
	ls_sql += "union "+&
				"select c.cod_azienda,c.cod_tipo_listino_prodotto,c.cod_valuta,c.data_inizio_val,c.progressivo,c.cod_prodotto_listino,"+&
						"c.cod_cliente,isnull(cli.rag_soc_1,'') as rag_soc_1,"+&
						"c.cod_prodotto_figlio,p.des_prodotto,"+&
						"c.scaglione_1,c.scaglione_2,c.scaglione_3,c.scaglione_4,c.scaglione_5,"+&
						"c.flag_sconto_mag_prezzo_1,c.flag_sconto_mag_prezzo_2,c.flag_sconto_mag_prezzo_3,"+&
									"c.flag_sconto_mag_prezzo_4,c.flag_sconto_mag_prezzo_5,"+&
						"c.variazione_1,c.variazione_2,c.variazione_3,c.variazione_4,c.variazione_5,"+&
						"c.flag_origine_prezzo_1,c.flag_origine_prezzo_2,c.flag_origine_prezzo_3,c.flag_origine_prezzo_4,"+&
									"c.flag_origine_prezzo_5,"+&
						"isnull(provvigione_1,0) as provvigione,"+&
						"c.cod_prodotto_padre,c.cod_versione as versione_default "+&
						",c.flag_escludi_linee_sconto "+&
				"from listini_prod_comune_var_client as c "+&
				"join anag_prodotti p on p.cod_azienda=c.cod_azienda and p.cod_prodotto=c.cod_prodotto_figlio "+&
				"left outer join anag_clienti cli on cli.cod_azienda=c.cod_azienda and cli.cod_cliente=c.cod_cliente "+&
				"where c.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"c.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"'"+&
					"and c.cod_valuta='"+fs_cod_valuta+"' and "+&
					"c.data_inizio_val='"+string(fdt_data_inizio_val,s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"c.progressivo="+string(fl_progressivo) + " " +&					
					"and c.cod_cliente is not null "
else
	ls_sql = "select "+&
					"l.cod_azienda,l.cod_tipo_listino_prodotto,l.cod_valuta,l.data_inizio_val,l.progressivo,l.cod_prodotto_listino,"+&
					"isnull(l.cod_cliente,'') as cod_cliente,isnull(cli.rag_soc_1,'') as rag_soc_1,"+&
					"l.cod_prodotto_figlio,p.des_prodotto,"+&
					"l.scaglione_1,l.scaglione_2,l.scaglione_3,l.scaglione_4,l.scaglione_5,"+&
					"l.flag_sconto_mag_prezzo_1,l.flag_sconto_mag_prezzo_2,l.flag_sconto_mag_prezzo_3,"+&
								"l.flag_sconto_mag_prezzo_4,l.flag_sconto_mag_prezzo_5,"+&
					"l.variazione_1,l.variazione_2,l.variazione_3,l.variazione_4,l.variazione_5,"+&
					"l.flag_origine_prezzo_1,l.flag_origine_prezzo_2,l.flag_origine_prezzo_3,l.flag_origine_prezzo_4,"+&
								"l.flag_origine_prezzo_5,"+&
					"isnull(provvigione_1, 0) as provvigione,"+&
					"l.cod_prodotto_padre,l.cod_versione as versione_default "+&
				"from listini_prod_locale as l "+&
				"join anag_prodotti p on p.cod_azienda=l.cod_azienda and p.cod_prodotto=l.cod_prodotto_figlio "+&
				"left outer join anag_clienti cli on cli.cod_azienda=l.cod_azienda and cli.cod_cliente=l.cod_cliente "+&
				"where l.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"l.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"' "+&
					"and l.cod_valuta='"+fs_cod_valuta+"' and "+&
					"l.data_inizio_val='"+string(fdt_data_inizio_val,s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"l.progressivo="+string(fl_progressivo)+" "
end if

ls_sql += "order by cod_cliente, cod_prodotto_figlio "

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore varianti: " + ls_errore)
	return -1
end if

fds_varianti.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy fds_varianti
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore varianti: " + ls_errore)
	return -1
end if

fds_varianti.settransobject(sqlca)
ll_ret = fds_varianti.retrieve()

return ll_ret
end function

public function boolean wf_controlla_data_listino_comune (datetime fdt_data_inizio_val, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, string fs_sheets[]);datetime ldt_appo
long ll_m1,  ll_m2

select max(data_inizio_val)
into :ldt_appo
from listini_ven_comune
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:fs_cod_tipo_listino_prodotto and 
	cod_valuta=:fs_cod_valuta;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore durante la lettura del max data validità nel listini comune!",Exclamation!)
	return false
end if

ll_m1= Month(date(fdt_data_inizio_val))
ll_m2= Month(date(ldt_appo))

if date(fdt_data_inizio_val) <= date(ldt_appo) then
	g_mb.messagebox("APICE","La nuova data di validità specificata è inferiore ad una data "+&
											"pre-esistente nel listino comune!"+ char(13)+&
											"Nuova data: "+string(fdt_data_inizio_val,"dd/MM/yyyy")+char(13)+&
											"Data pre-esistente: "+string(ldt_appo,"dd/MM/yyyy"),Exclamation!)
	return false
end if

return true
end function

public function boolean wf_controlla_data (datetime fdt_data_inizio_val, string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, string fs_cod_prodotto, boolean fb_standard);datetime ldt_appo
long ll_m1,  ll_m2

select max(data_inizio_val)
into :ldt_appo
from listini_ven_comune
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:fs_cod_tipo_listino_prodotto and 
	cod_valuta=:fs_cod_valuta and cod_prodotto=:fs_cod_prodotto;
	
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore durante la lettura del max data validità nel listini comune!",Exclamation!)
	return false
end if

if isnull(ldt_appo) then ldt_appo = datetime(date(1980, 1, 1), time("00:00:00"))

ll_m1= Month(date(fdt_data_inizio_val))
ll_m2= Month(date(ldt_appo))

if fb_standard then
	//caso standard
	
	if date(fdt_data_inizio_val) <= date(ldt_appo) then
		g_mb.messagebox("APICE","La nuova data di validità specificata è inferiore ad una data "+&
												"pre-esistente nel listino comune!"+ char(13)+&
												"Nuova data: "+string(fdt_data_inizio_val,"dd/MM/yyyy")+char(13)+&
												"Data pre-esistente: "+string(ldt_appo,"dd/MM/yyyy")+char(13)+&
												"Prodotto: "+fs_cod_prodotto,Exclamation!)
		return false
	end if
else
	//caso personalizzato
	if date(fdt_data_inizio_val) < date(ldt_appo) then
		g_mb.messagebox("APICE","La data di validità specificata in excel è inferiore o uguale ad una data "+&
												"pre-esistente nel listino comune!"+ char(13)+&
												"Nuova data: "+string(fdt_data_inizio_val,"dd/MM/yyyy")+char(13)+&
												"Data pre-esistente: "+string(ldt_appo,"dd/MM/yyyy")+char(13)+&
												"Prodotto: "+fs_cod_prodotto+char(13)+&
												"Il foglio non sarà elaborato: verifica nel log",Exclamation!)
		return false
	end if
end if

return true
end function

public function string wf_calcola_valore_var (string fs_cod_prodotto, string fs_flag_sconto_mag_prezzo, ref decimal fd_valore);string ls_return
//decimal ld_dec_appo

//ld_dec_appo = fd_valore

//((devo incorporare l'iva?
if is_flag_incorporo_iva = "S" then
	//l'iva va incorporata solo se =P o =A o =D; se S o M non va fatto
	if fs_flag_sconto_mag_prezzo <> "S" and fs_flag_sconto_mag_prezzo<>"M" then
		wf_incorpora_iva(fs_cod_prodotto, fd_valore)
	end if
else
end if

choose case fs_flag_sconto_mag_prezzo
	case "S"  // sconto in %								(-xx.yy%)
		fd_valore = round(fd_valore, 2)
		ls_return = string(fd_valore)
		ls_return = "-" + ls_return + "%"
		
	case "M" //maggiorazione in %						(+xx.yy%)
		fd_valore = round(fd_valore, 2)
		ls_return = string(fd_valore)
		ls_return = "+"+ls_return + "%"
		
	case "A" //aumento									(+xxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		fd_valore = round(fd_valore, 2)
		ls_return = "+" + string(fd_valore)
		
	case "D" //diminuzione								(-xxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		fd_valore = round(fd_valore, 2)
		ls_return = "-" + string(fd_valore)
		
	case "P"  //prezzo										(xxxxxx.yy)
		//prima arrotonda all 2-nda cifra decimale (es.3.265 -> 3.27 e 3.264 -> 3.26)
		fd_valore = round(fd_valore, 2)
		ls_return = string(fd_valore)
		wf_rimpiazza(ls_return)
		
	case else
		ls_return = "errore"
		
end choose

return ls_return
		
end function

public function integer wf_get_destinatari (string as_input, string as_sep, ref string as_array[]);long		ll_pos, ll_element
string	ls_array[], ls_element

as_array = ls_array
ll_element = 0

do
	ll_pos = pos(as_input,as_sep,1)     

	if ll_pos > 0 then
		ls_element        = left(as_input,(ll_pos - 1))
		as_input            = right(as_input,(len(as_input) - ll_pos - (len(as_sep) - 1) ))
	else
		ls_element        = as_input
		as_input            = ""
	end if

	ll_element++    
	ls_array[ll_element] = ls_element

loop while ll_pos > 0

as_array = ls_array

return 0


end function

public function integer wf_crea_allegato (ref string fs_allegato);string ls_file, ls_sheets[], ls_appo, ls_path, ls_file_mail, ls_file_origine
uo_excel_listini luo_excel
long ll_tot, ll_i

dw_selezione_2.accepttext()
ls_file_origine = dw_selezione_2.getitemstring(1, "path_excel")

f_getuserpath(ls_path)
ls_file_mail = "Listino-"+string(datetime(today(),now()),"ddMMyy-hhmmss")+".xls"

luo_excel = create uo_excel_listini

luo_excel.is_path_file = ls_file_origine
luo_excel.ib_oleobjectvisible = false

luo_excel.uof_open( )

luo_excel.uof_get_sheets(ls_sheets)
ll_tot = upperbound(ls_sheets)

for ll_i = 1 to ll_tot
	ls_appo = ls_sheets[ll_i]
	if pos(ls_appo, "(") > 0 or pos(ls_appo, ")")>0 then
		//foglio personalizzato: toglilo
		luo_excel.uof_delete_sheet(ls_appo)
	end if
next

luo_excel.uof_save_as(ls_path+ls_file_mail)

luo_excel.uof_close( )
destroy luo_excel

fs_allegato = ls_path+ls_file_mail

return 1
end function

public function integer wf_invia_mail ();string ls_path, ls_tipo, ls_file_allegato
long ll_i, ll_rows, ll_count, ll_indice, li_ret
string ls_LDL, ls_sql, ls_appo, ls_email, ls_cod_dest[], ls_destinatari[], ls_oggetto, ls_messaggio, ls_allegati[], ls_msg
uo_outlook luo_outlook

if wf_crea_allegato(ls_file_allegato)=1 then
	ls_allegati[1] = ls_file_allegato
else
	messagebox( "OMNIA", "Attenzione: non è stato possibile creare l'allegato excel del listino comune da inviare via e-mail!")
	return -1
end if

ls_tipo = "M"

select stringa
into   :ls_LDL
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LDL' and flag_parametro="S";

if sqlca.sqlcode <> 0 then	
	messagebox( "OMNIA", "Attenzione: impostare il parametro stringa LDL (dest1,dest2;...) dei destinatari e-mail: tabella parametri_azienda!")
	return -1
end if

if isnull(ls_LDL) or ls_LDL="" then
	g_mb.messagebox( "OMNIA", "Il valore del parametro 'LDL' non può essere vuoto! (controllare il parametro aziendale LDL)")
	return -1
end if

wf_get_destinatari(ls_LDL, ",", ls_cod_dest)

ll_rows = upperbound(ls_cod_dest)
ll_indice = 0

if ll_rows = 0 then
	g_mb.messagebox( "OMNIA", "L'elaborazione del parametro 'LDL' non ha dato nessun risultato! (controllare il parametro aziendale LDL)")
	return -1
end if

for ll_i=1 to ll_rows
	//leggi il valore	
	
	ls_appo = ls_cod_dest[ll_i]
	
	//verifica che esista in tab_ind_dest
	select count(*)
	into :ll_count
	from tab_ind_dest
	where cod_azienda=:s_cs_xx.cod_azienda and cod_destinatario=:ls_appo;
	
	if ll_count = 1 then
	else
		//problemi
		g_mb.messagebox("APICE","Problemi in lettura destinatario: "+ls_appo, Exclamation!)
		continue
	end if
	
	select indirizzo
	into :ls_email
	from tab_ind_dest
	where cod_azienda=:s_cs_xx.cod_azienda and cod_destinatario=:ls_appo;
	
	if ls_email="" or isnull(ls_email) then
		g_mb.messagebox("APICE","Indirizzo vuoto per il destinatario: "+ls_appo, Exclamation!)
		continue
	end if
	
	ll_indice+=1
	ls_destinatari[ll_indice] = ls_email
	
next

luo_outlook = create uo_outlook

ls_oggetto = "Inserito nuovo Listino Comune"
ls_messaggio = "E' stato inserito un nuovo listino comune: il file è allegato all'e-mail"

// TODO
/*
li_ret = luo_outlook.uof_invio_outlook_redemption(0,ls_tipo, ls_oggetto, ls_messaggio, ls_destinatari[], ls_allegati[], true, ls_msg)												
if li_ret <> 0 then
	g_mb.messagebox("APICE", "Errore in fase di invio listino~r~n" + ls_messaggio)
	return -1
else
	// tutto bene
end if
*/

if not luo_outlook.uof_invio_sendmail(ls_destinatari[],ls_oggetto,ls_messaggio,ls_allegati[], ls_msg) then
	g_mb.messagebox("APICE", "Errore in fase di invio listino~r~n" + ls_msg)
	return -1
end if
																	
destroy luo_outlook

return 1
end function

public function integer wf_elabora_dimensioni (string fs_cod_tipo_listino_prodotto, string fs_cod_valuta, datetime fdt_data_inizio_val, long fl_progressivo, decimal fd_limite_dim_1, decimal fd_limite_dim_2, ref decimal fd_variazione, ref string fs_flag_sconto_mag_prezzo, ref string fs_variazione, boolean fb_comune);datastore lds_datastore
string ls_sql, ls_syntax, ls_errore, ls_cod_prodotto
string ls_dim_x, ls_dim_y

if fd_limite_dim_1 > 99999999 then 
	ls_dim_x = "99999999"
else
	ls_dim_x = string(fd_limite_dim_1)
	wf_rimpiazza(ls_dim_x)
end if

if fd_limite_dim_2 > 99999999 then 
	ls_dim_y = "99999999"
else
	ls_dim_y = string(fd_limite_dim_2)
	wf_rimpiazza(ls_dim_y)
end if

if fb_comune then
	//pesca dal comune
	ls_sql = "select "+&
					"b.variazione,b.flag_sconto_mag_prezzo,a.cod_prodotto,"+&
					"a.cod_cliente,b.limite_dimensione_1,b.limite_dimensione_2 "+&
				"from listini_ven_comune as a "+&
				"join listini_ven_dim_comune as b on b.cod_azienda=a.cod_azienda "+&
					"and a.cod_tipo_listino_prodotto=b.cod_tipo_listino_prodotto and a.cod_valuta=b.cod_valuta "+&
					"and a.data_inizio_val=b.data_inizio_val and a.progressivo=b.progressivo "+&
				"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"a.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"' and "+&
					"a.cod_valuta='"+fs_cod_valuta+"' and "+&
					"a.data_inizio_val='"+string(fdt_data_inizio_val,s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"a.progressivo="+string(fl_progressivo)+" and "+&
					"b.limite_dimensione_1="+ls_dim_x+" and b.limite_dimensione_2="+ls_dim_y
else
	//pesca dal locale
	ls_sql = "select "+&
					"b.variazione,b.flag_sconto_mag_prezzo,a.cod_prodotto,"+&
					"a.cod_cliente,b.limite_dimensione_1,b.limite_dimensione_2 "+&
				"from listini_ven_locale as a "+&
				"join listini_ven_dim_locale as b on b.cod_azienda=a.cod_azienda "+&
					"and a.cod_tipo_listino_prodotto=b.cod_tipo_listino_prodotto and a.cod_valuta=b.cod_valuta "+&
					"and a.data_inizio_val=b.data_inizio_val and a.progressivo=b.progressivo "+&
				"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"a.cod_tipo_listino_prodotto='"+fs_cod_tipo_listino_prodotto+"' and "+&
					"a.cod_valuta='"+fs_cod_valuta+"' and "+&
					"a.data_inizio_val='"+string(fdt_data_inizio_val,s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"a.progressivo="+string(fl_progressivo)+" and "+&
					"b.limite_dimensione_1="+ls_dim_x+" and b.limite_dimensione_2="+ls_dim_y
end if

ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore delle variazioni (no sfuso): " + ls_errore)
	return -1
end if

lds_datastore = create datastore
lds_datastore.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore delle variazioni (no sfuso): " + ls_errore)
	return -1
end if

lds_datastore.settransobject(sqlca)
if lds_datastore.retrieve() = -1 then
	destroy lds_datastore
	return -1
end if	

if lds_datastore.rowcount() > 0 then
	fd_variazione = lds_datastore.getitemdecimal(1, 1)
	fs_flag_sconto_mag_prezzo = lds_datastore.getitemstring(1, 2)
	ls_cod_prodotto = lds_datastore.getitemstring(1, 3)
	
	//if fs_flag_sconto_mag_prezzo <> "P" then
		fs_variazione = wf_calcola_valore(ls_cod_prodotto, fs_flag_sconto_mag_prezzo, fd_variazione)
	//end if
	
	if isnull(fd_variazione) then fd_variazione = 0.0
else
	 fd_variazione = 0.0
end if

//N.B in fd_variazione c'è il valore decimale			es. 123.45,  	12  ecc...
//     in fs_variazione il valore in formato stringa	es. "123.45",	"12%"

destroy lds_datastore
return 0
end function

public function integer wf_get_provv_sfuso (string fs_cod_tipo_listini_prodotto, string fs_cod_valuta, datetime fdt_data_riferimento_val, string fs_cod_agente, string fs_cod_cliente, string fs_cod_prodotto, double fd_quan_scaglione, double fd_sconto_tot, ref double fd_provvigione);uo_condizioni_cliente		luo_cond
integer							li_ret
double							ld_variazione[]

luo_cond = create uo_condizioni_cliente

li_ret = luo_cond.wf_ricerca_provv_agenti(	fs_cod_tipo_listini_prodotto, fs_cod_valuta,&
																fdt_data_riferimento_val, fs_cod_agente,&
																fs_cod_cliente, fs_cod_prodotto,&
																fd_quan_scaglione, fd_sconto_tot, ld_variazione[])
if li_ret >= 0 then
	if upperbound(ld_variazione) > 0 then
		fd_provvigione = ld_variazione[1]
		if isnull(fd_provvigione) then fd_provvigione = 0		
	else
		fd_provvigione = 0		
	end if
else
	fd_provvigione = 0
	destroy luo_cond
	return -1
end if

destroy luo_cond

return 1
end function

public function integer wf_get_desk_dir (ref string fs_desktop_dir);String ls_path, ls_appo[]
long ll_owner, ll_ret, ll_i

setnull(ll_owner)

//CSIDL_DESKTOPDIRECTORY			valore esadecimale: 10		valore decimale: 16

/*
if ( !(SHGetSpecialFolderPath(NULL, s, CSIDL_DESKTOP, FALSE) ) )  // Desktop
      {
            // problem
      }
*/
//ls_path = Space(255)

if RegistryGet("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", &
							"Personal", RegString!, ls_path) = 1 then
	fs_desktop_dir = ls_path + "\"
else
	f_getuserpath(ls_path)
	fs_desktop_dir = ls_path
end if

//for ll_i=1 to 100
//	ll_ret = SHGetSpecialFolderPath(0, ls_path, ll_i, false)
//	ls_appo[ll_i] = ls_path
//next



//ll_ret = SHGetFolderPath(0, 5, 0, 0, ls_path)
//ll_ret = SHGetKnownFolderPath(0, 5, 0, ls_path)

//ll_ret = pos(ls_path, char(0))
//
//ls_path = left(ls_path, ll_ret - 1)
//
//fs_desktop_dir = ls_path
//
//if SHGetSpecialFolderPath(ll_owner, ls_path, 10, false) = 1 then
//	fs_desktop_dir = ls_path
//else
//	return -1
//end if

return 1

end function

public function long wf_controlla_padre_versione (string fs_cod_prodotto_variante, string fs_cod_prodotto_padre, string fs_cod_versione, ref string fs_messaggio);long ll_ret


select count(*)
into :ll_ret
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_prodotto=:fs_cod_prodotto_variante;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in lettura da tabella prodotti per verifica prodotto-variante: "+char(13)+&
								"codice: "+fs_cod_prodotto_variante + sqlca.sqlerrtext
								
	return -1
end if

if isnull(ll_ret) then ll_ret = 0
if ll_ret = 0 then
	fs_messaggio = "Prodotto-variante non trovato in anagrafica prodotti: "+char(13)+&
								"prodotto: "+fs_cod_prodotto_variante
	return ll_ret
end if

//----------------------------------------

select count(*)
into :ll_ret
from anag_prodotti
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_prodotto=:fs_cod_prodotto_padre;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in lettura da tabella prodotti per verifica prodotto-padre: "+char(13)+&
								"codice: "+fs_cod_prodotto_padre + sqlca.sqlerrtext
								
	return -1
end if

if isnull(ll_ret) then ll_ret = 0
if ll_ret = 0 then
	fs_messaggio = "Prodotto-padre non trovato in anagrafica prodotti: "+char(13)+&
								"prodotto: "+fs_cod_prodotto_padre
	return ll_ret
end if


//select count(*)
//into :ll_ret
//from distinta_padri
//where cod_azienda=:s_cs_xx.cod_azienda and
//	cod_prodotto=:fs_cod_prodotto_variante and cod_versione=:fs_cod_versione and
//	flag_blocco='N';
//	
//if sqlca.sqlcode < 0 then
//	fs_messaggio = "Errore in lettura da tabella distinta_padri: "+char(13)+&
//								"prodotto: "+fs_cod_prodotto_variante + " - versione: "+fs_cod_versione+ char(13) +&
//								sqlca.sqlerrtext
//								
//	return -1
//end if
//
//if isnull(ll_ret) then ll_ret = 0
//if ll_ret = 0 then
//	fs_messaggio = "Dati non trovati in lettura da tabella distinta_padri: "+char(13)+&
//								"prodotto: "+fs_cod_prodotto_variante + " - versione: "+fs_cod_versione+ char(13)
//end if

return ll_ret
end function

protected function integer wf_configurato_cliente (ref long fl_riga, ref long fl_colonna, string fs_cod_prodotto);string 		ls_cod_prodotto_da_r,ls_cod_prodotto_a_r,ls_cod_tipo_listino_prodotto_r,&
				ls_cod_valuta_r, ls_flag_estrai_ultima_r, ls_flag_incorporo_iva_r,&
				ls_syntax, ls_errore, ls_sql, ls_where, ls_where2, ls_cod_prodotto_like_r, ls_des_prodotto_listino, ls_cod_cliente
				
datetime		ldt_data_inizio_nuova_r, ldt_da_data_r, ldt_a_data_r

decimal		ld_min_fat_sup_cu

datastore	lds_datastore

long			ll_i, ll_tot

dw_selezione.accepttext()

ls_cod_tipo_listino_prodotto_r = dw_selezione.getitemstring(1,"cod_tipo_listino_prodotto")
ls_cod_valuta_r = dw_selezione.getitemstring(1,"cod_valuta")
ls_cod_prodotto_da_r = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a_r = dw_selezione.getitemstring(1,"a_cod_prodotto")
ls_cod_prodotto_like_r = dw_selezione.getitemstring(1,"cod_prodotto_speciale")
ldt_da_data_r = dw_selezione.getitemdatetime(1,"da_data")
ldt_a_data_r = dw_selezione.getitemdatetime(1,"a_data")
ls_flag_estrai_ultima_r = dw_selezione.getitemstring(1,"flag_estrai_ultima")
ls_flag_incorporo_iva_r = dw_selezione.getitemstring(1,"flag_incorporo_iva")
ldt_data_inizio_nuova_r = dw_selezione.getitemdatetime(1,"data_inizio_nuova")

//nella where c'è almeno il cod_azienda
ls_where = "where a.cod_azienda='"+s_cs_xx.cod_azienda+"'"

if ls_cod_tipo_listino_prodotto_r<>"" and not isnull(ls_cod_tipo_listino_prodotto_r) then
	ls_where += " and a.cod_tipo_listino_prodotto='"+ls_cod_tipo_listino_prodotto_r+"'"
end if
if ls_cod_valuta_r<>"" and not isnull(ls_cod_valuta_r) then
	ls_where += " and a.cod_valuta='"+ls_cod_valuta_r+"'"
end if
if ls_flag_estrai_ultima_r <> "S" then
	if not isnull(ldt_da_data_r) then
		ls_where += " and a.data_inizio_val>='"+string(ldt_da_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
	if not isnull(ldt_a_data_r) then
		ls_where += " and a.data_inizio_val<='"+string(ldt_a_data_r,s_cs_xx.db_funzioni.formato_data)+"'"
	end if
end if
ls_where2 = ls_where

//questo deve essere necessariamente l'ultimo ---------------------

//verifica se hai scelto la ricerca per prodotto like
if ls_cod_prodotto_like_r <> "" and not isnull(ls_cod_prodotto_like_r) then
	//ricerca like
	wf_ricerca_prodotto_like(ls_cod_prodotto_like_r)
	ls_cod_prodotto_like_r = "a."+ls_cod_prodotto_like_r
	ls_where += " and "+ls_cod_prodotto_like_r
else
	//ricerca da-a
	if ls_cod_prodotto_da_r<>"" and not isnull(ls_cod_prodotto_da_r) then
		ls_where += " and a.cod_prodotto>='"+ls_cod_prodotto_da_r+"'"
	end if
	if ls_cod_prodotto_a_r<>"" and not isnull(ls_cod_prodotto_a_r) then
		ls_where += " and a.cod_prodotto<='"+ls_cod_prodotto_a_r+"'"
	end if
end if
//------------------------------------------------------------------------
if not isnull(ls_cod_cliente) then
	ls_where += " and a.cod_cliente = '"+ls_cod_cliente+"'"
end if	

//con cliente e prodotto configurato da listini vendite
ls_sql ="select a.cod_prodotto+'('+a.cod_cliente+')' as foglio,"+&
				"a.cod_azienda,a.cod_tipo_listino_prodotto,a.cod_valuta,a.data_inizio_val,a.progressivo,"+&
				"a.cod_prodotto,a.cod_cliente,c.rag_soc_1+isnull(' ,'+c.localita,'')+isnull('('+c.provincia+')','') as des_cliente,"+&
				"p.cod_cat_mer,"+&
				"a.scaglione_1,a.scaglione_2,a.scaglione_3,a.scaglione_4,a.scaglione_5,"+&
				"a.flag_sconto_mag_prezzo_1,a.flag_sconto_mag_prezzo_2,a.flag_sconto_mag_prezzo_3,"+&
					"a.flag_sconto_mag_prezzo_4,a.flag_sconto_mag_prezzo_5,"+&
				"a.flag_origine_prezzo_1,a.flag_origine_prezzo_2,a.flag_origine_prezzo_3,a.flag_origine_prezzo_4,"+&
					"a.flag_origine_prezzo_5,"+&
				"a.minimo_fatt_superficie,p.cod_misura_ven "+&
			"from listini_ven_locale as a "+&
			"join anag_prodotti as p on p.cod_azienda=a.cod_azienda "+&
			    "and p.cod_prodotto=a.cod_prodotto "+&
			"join anag_clienti as c on c.cod_azienda=a.cod_azienda "+&
			    "and c.cod_cliente=a.cod_cliente "+&
			ls_where +&
					" and a.cod_prodotto is not null and a.cod_cliente is not null "+&
					" and a.cod_prodotto='"+fs_cod_prodotto+"' "
			
//Donato 19/11/2012 Modifica per includere/escludere gli OPTIONALS CONFIGURABILI -------------
if is_OCF<>"" then
	ls_sql += "and (p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' or p.cod_cat_mer='"+is_OCF+"') "
else
	ls_sql += "and p.cod_cat_mer='"+is_cod_cat_prodotto_configurato+"' "
end if
//----------------------------------------------------------------------------------------------------------------

ls_sql += "order by a.cod_prodotto,a.cod_cliente,a.data_inizio_val desc,a.progressivo asc "


ls_syntax = sqlca.syntaxfromsql(ls_sql, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore dei listini (CONFIGURATO-Clienti): " + ls_errore)
	return -1
end if

lds_datastore = create datastore
lds_datastore.create(ls_syntax, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	destroy lds_datastore
	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore dei listini (CONFIGURATO Clienti): " + ls_errore)
	return -1
end if

lds_datastore.settransobject(sqlca)
if lds_datastore.retrieve() = -1 then
	destroy lds_datastore
	return -1
end if

ll_tot = lds_datastore.rowcount()
/*NOTA
In caso di impostaggio del periodo da-a devo considerare, a parità di prodotto e cliente,
sempre la prima riga, che grazie all'ordinamento impostato è quella + recente
Se invece è impostato il flag ultima condizione (cioè la + recente), non sarà passato
il filtro per data validità, ma prendero sempre e comunque la prima riga
a parità di prodotto e cliente

STRUTTURA DEL DATASTORE
	foglio								cod_azienda				cod_tipo_listino_prodotto		cod_valuta
	data_inizio_val					progressivo				cod_prodotto					cod_cliente
	des_cliente						cod_cat_mer			scaglione_1						scaglione_2
	scaglione_3						scaglione_4				scaglione_5						flag_sco_mag_pr_1
	flag_sco_mag_pr_2			flag_sco_mag_pr_3	flag_sc_mag_pr_4				flag_sco_mag_pr_5,
	flag_orig_prezzo_1			flag_orig_prezzo_2	flag_orig_prezzo_3			flag_orig_prezzo_4
	flag_origine_prezzo_5		minimo_fatt_sup		cod_misura_ven
*/

string			ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_foglio_cu, ls_cod_tipo_listino_prodotto_cu, &
				ls_cod_valuta_cu, ls_des_cliente_cu, ls_cod_cat_mer_cu, &
				ls_prodotto_corrente, ls_cliente_corrente, ls_valore, ls_flag_sconto_mag_prezzo, &
				ls_valore_sconto_mag_prezzo, ls_val_generico, ls_cod_variante, ls_cod_agente
				
long			ll_progressivo_cu, &
				ll_count_dim_x, ll_i_dim_x, ll_count_dim_y, ll_i_dim_y, ll_riga_var, ll_col_var, ll_col_var2, &
				ll_tot_varianti, ll_i_varianti, ll_i_scaglioni
				//ll_row_offset

datetime	ldt_data_val_cu, &
				ldt_data_val_corrente
				
decimal		ld_dim_x[], ld_dim_y[], ld_null_v[], ld_valore, ld_val_generico

datastore	lds_varianti

for ll_i = 1 to ll_tot
	yield()
	//ogni iterata in questo ciclo è un foglio di lavoro excel...
	ls_cod_prodotto_cu = lds_datastore.getitemstring(ll_i, 7)
	ls_cod_cliente_cu = lds_datastore.getitemstring(ll_i, 8)
	ldt_data_val_cu = lds_datastore.getitemdatetime(ll_i, 5)
	
	st_msg.text = "Elaborazione Prodotto Configurato per Clienti: "+&
				ls_cod_prodotto_cu+"("+ls_cod_cliente_cu+") :"+&
				string(ll_i)+" di "+string(ll_tot)
	
	if ls_cod_prodotto_cu=ls_prodotto_corrente and &
					ls_cod_cliente_cu=ls_cliente_corrente then continue
	
	//memorizzo il prodotto e il cliente corrente
	ls_prodotto_corrente = ls_cod_prodotto_cu
	ls_cliente_corrente = ls_cod_cliente_cu
	ldt_data_val_corrente = ldt_data_val_cu
	
	//continuo a leggere gli altri valori
	ls_foglio_cu = lds_datastore.getitemstring(ll_i, 1)
	ls_cod_tipo_listino_prodotto_cu = lds_datastore.getitemstring(ll_i, 3)
	ls_cod_valuta_cu = lds_datastore.getitemstring(ll_i, 4)
	ll_progressivo_cu = lds_datastore.getitemnumber(ll_i, 6)
	ls_des_cliente_cu = lds_datastore.getitemstring(ll_i, 9)
	ls_cod_cat_mer_cu = lds_datastore.getitemstring(ll_i, 10)
	
	ld_min_fat_sup_cu = lds_datastore.getitemdecimal(ll_i, 26)
	
	//creo il foglio di lavoro opportuno
	iuo_excel.uof_crea_foglio(ls_foglio_cu)
	
	//si tratta di un prodotto configurato (no sfuso): devo caricare la griglia delle dimensioni
	//intanto scrivo il titolo nel foglio di lavoro ----------------------------------------------
//	ll_row_offset = 0
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,1,ls_cod_tipo_listino_prodotto_cu,false,false,"none")
//	iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_row_offset,2,ls_cod_valuta_cu,false,false,"none")
	
//	ll_row_offset += 1
	
	select des_prodotto
	into :ls_des_prodotto_listino
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_prodotto=:ls_cod_prodotto_cu;
	
	if isnull(ls_des_prodotto_listino) or ls_des_prodotto_listino="" then
		ls_valore = ls_des_cliente_cu + " - (DAL "+string(date(ldt_data_val_cu))+")"
	else
		ls_valore = ls_des_cliente_cu + " - " + ls_des_prodotto_listino + " - (DAL "+string(date(ldt_data_val_cu))+")"
	end if
		
	iuo_excel.uof_scrivi(ls_foglio_cu, 1,1,ls_valore,false,true,"none")
	//--------------------------------------------------------------------------------------------		
	
	//carico le due diverse dimensioni per il prodotto che sto scorrendo
	ld_dim_x = ld_null_v
	ld_dim_y = ld_null_v
	if wf_carica_dimensioni(ls_cod_prodotto_cu, ld_dim_x[], ld_dim_y[]) = -1 then
//		destroy lds_datastore;
//		rollback;
//		return -1
	end if
	
	ll_i_dim_x = 1
	ll_i_dim_y = 1
	
	if upperbound(ld_dim_x[])>0 and upperbound(ld_dim_y[])>0 then
		//devo disegnare la griglia delle dimensioni
		
		iuo_excel.uof_scrivi(ls_foglio_cu,2,1,"DIM",false,true,"36")
		
		//scrivo la dim 1 in orizzontale
		ll_count_dim_x = upperbound(ld_dim_x)
		for ll_i_dim_x = 1 to ll_count_dim_x
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu,2,ll_i_dim_x + 1,ld_dim_x[ll_i_dim_x],false,true,"36")
		next
		
		//scrivo la dim 2 in verticale + le relative variazioni incolonnate per dim 1
		ll_count_dim_y = upperbound(ld_dim_y)
		for ll_i_dim_y = 1 to ll_count_dim_y
			yield()
			iuo_excel.uof_scrivi(ls_foglio_cu, ll_i_dim_y + 2, 1,ld_dim_y[ll_i_dim_y],false,true,"36")
			
			//ora lancio la query che mi deve caricare le variazioni fissate le due coordinate dim1 e dim2
			setnull(ld_valore)
			for ll_i_dim_x = 1 to ll_count_dim_x
				yield()
				wf_elabora_dimensioni(ls_cod_tipo_listino_prodotto_cu, &
											ls_cod_valuta_cu,&
											ldt_data_val_cu, &
											ll_progressivo_cu,&
											ld_dim_x[ll_i_dim_x], ld_dim_y[ll_i_dim_y], &
											ld_valore,&
											ls_flag_sconto_mag_prezzo, ls_valore_sconto_mag_prezzo, false)
											
				if ls_flag_sconto_mag_prezzo <> "P" then
					//and ls_flag_sconto_mag_prezzo <> "M" and ls_flag_sconto_mag_prezzo <> "A" then
					iuo_excel.uof_scrivi_codice(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ls_valore_sconto_mag_prezzo,false,false,"none")
				else
					//dato numerico puro (Prezzo)
					iuo_excel.uof_scrivi_dec(ls_foglio_cu, ll_i_dim_y + 2, ll_i_dim_x + 1,ld_valore,false,false,"none", is_formato_numero)
				end if
			next
		next
	end if
	
	
	//scrivo il min fatt sup
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_i_dim_y + 4, 1, "MFsup:",false,true,"36")
	if isnull(ld_min_fat_sup_cu) then ld_min_fat_sup_cu = 0
	iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_i_dim_y + 4, 2, ld_min_fat_sup_cu,false,false,"none",is_formato_numero)
	
	
	//alla fine di questo ciclo ho disegnato la griglia delle DIM
	//conservo in ll_i_dim_y e ll_i_dim_x i valori dell'angolo in basso a destra					
	//indice di riga excel:			ll_i_dim_y
	//indice di colonna excel:		ll_i_dim_x
							
	//devo ora scrivere la griglia delle varianti
	//mi sposto di due colonne a destra e comincio a scrivere dalla seconda riga con VAR
	ll_riga_var = 2
	ll_col_var = upperbound(ld_dim_x[]) + 1 + 2		//dimx + cellaDIM + 2
	ll_col_var2 = ll_col_var	//memorizzo da quale colonna parto
	
	iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,"VAR",false,true,"40")
	
	//ora che so la posizione della colonna VAR
	//SCRIVO L'AGENTE ------------------------------------------------------	
	select cod_agente_1
	into :ls_cod_agente
	from anag_clienti
	where cod_azienda=:s_cs_xx.cod_azienda and cod_cliente=:ls_cod_cliente_cu;
	
	if isnull(ls_cod_agente) then ls_cod_agente = ""
	
	iuo_excel.uof_scrivi(ls_foglio_cu,1, ll_col_var2, "AGENTE:",false,true,"none")
	
	if ls_cod_agente<> "" then				
		select rag_soc_1
		into :ls_valore
		from anag_agenti
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_agente=:ls_cod_agente;
			
		iuo_excel.uof_scrivi_codice(ls_foglio_cu,1,ll_col_var2+1,ls_cod_agente,false,true,"none")
		iuo_excel.uof_scrivi(ls_foglio_cu,1, ll_col_var2+2, ls_valore,false,true,"none")
	end if
	//--------------------------------------------------------------------------
	
	//scrivo il resto della intestazione della griglia VAR ------------------------------------					
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIANTE",false,true,"40")
	
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"DES. VARIANTE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"SCAGLIONE",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"TIPO VAR.(*1)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VARIAZ.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"ORIGINE (*2)",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"PROVV.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"COD.PRODOTTO PADRE.",false,true,"40")
	ll_col_var2 +=1
	iuo_excel.uof_scrivi(ls_foglio_cu, ll_riga_var, ll_col_var2,"VERS.DEFAULT",false,true,"40")
	//-------------------------------------------------------------------------------------------
	lds_varianti = create datastore
	ll_tot_varianti = wf_carica_ds_varianti(lds_varianti,ls_cod_tipo_listino_prodotto_cu, &
								ls_cod_valuta_cu, ldt_data_val_cu,&
								ll_progressivo_cu, false)
	if ll_tot_varianti > 0 then
		/*Struttura del datastore
		cod_azienda						cod_tipo_listino_prodotto		cod_valuta						data_inizio_val
		progressivo						cod_prodotto_listino			cod_cliente						rag_soc_1
		cod_prodotto_figlio			des_prodotto					scaglione_1						scaglione_2
		scaglione_3						scaglione_4						scaglione_5						flag_sconto_mag_prezzo_1	
		flag_sconto_mag_prezzo_2	flag_sconto_mag_prezzo_3	flag_sconto_mag_prezzo_4	flag_sconto_mag_prezzo_5
		variazione_1					variazione_2					variazione_3					variazione_4
		variazione_5					flag_origine_prezzo_1		flag_origine_prezzo_2		flag_origine_prezzo_3
		flag_origine_prezzo_4		flag_origine_prezzo_5		provvigione						cod_prodotto_padre
		versione_default
		*/
		for ll_i_varianti = 1 to ll_tot_varianti
			yield()
			//ripeto il ciclo fino a 5 volte se occorre in base al valore in variazione_i
			for ll_i_scaglioni=1 to 5
				yield()
				//leggo il valore dell variazione_i (i=1..5)
				ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_iesima (1..5)
				if (not isnull(ld_val_generico) and ld_val_generico > 0 and ll_i_scaglioni>1) &
																or ll_i_scaglioni = 1 then
					//------------------------------------------------
					
					ll_riga_var += 1				//mi sposto di una riga in giù
					ll_col_var2 = ll_col_var		//torno alla colonna in cui è specificato "VAR"
					
					//scrivo n° di riga
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti),false,true,"none")
					//iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,string(ll_i_varianti + ll_i_scaglioni - 1),false,true,"none")
					
					//listino cliente prodotto configurato **************************
					//non esistono le colonne del cliente, è fissato: quindi vado subito alle colonne della variante
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 9) //variante
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//memorizzo il cod_prodotto (variante)
					ls_cod_variante = ls_val_generico
					//------------------
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 10)	//des_variante
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//il resto per il listino del prodotto configurato (con o senza cliente) è lo stesso
					//inserisco da scaglione in poi
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 11 + ll_i_scaglioni - 1) //scaglione_i
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 16 + ll_i_scaglioni - 1)	//flag_sconto_mag_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					//qui occorre interpretare la variazione in base al flag_sconto_mag_prezzo_i
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 21 + ll_i_scaglioni - 1) //variazione_i
					//-----------------------------------------
					
					string ls_appo
					
					ls_appo = wf_calcola_valore(ls_cod_variante, ls_val_generico,ld_val_generico)
					if ls_val_generico <> "P" then
						//and ls_val_generico <> "M" and ls_val_generico <> "A" then
						ll_col_var2 += 1
						//iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_appo,false,false,"none")
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					else
						ll_col_var2 += 1
						iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					end if
					//------------------------------------

					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 26 + ll_i_scaglioni - 1)	//flag_origine_prezzo_i
					iuo_excel.uof_scrivi(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ld_val_generico = lds_varianti.getitemdecimal(ll_i_varianti, 31)	//provvigione
					iuo_excel.uof_scrivi_dec(ls_foglio_cu,ll_riga_var, ll_col_var2,ld_val_generico,false,false,"none",is_formato_numero)
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 32)	//cod prodotto padre
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					
					ll_col_var2 += 1
					ls_val_generico = lds_varianti.getitemstring(ll_i_varianti, 33)	//versione default
					iuo_excel.uof_scrivi_codice(ls_foglio_cu,ll_riga_var, ll_col_var2,ls_val_generico,false,false,"none")
					//------------------------------------------------
				else
					exit
				end if
			next
		next
	else
		//return 1
	end if
	destroy lds_varianti
	
	wf_scrivi_note(ls_foglio_cu, ll_riga_var, ll_col_var, false)
	
next

destroy lds_datastore
return 1
end function

public function integer wf_importa_configurato_var_client (string fs_cod_prodotto);decimal			ld_x_excel[], ld_y_excel[]
long				ll_riga, ll_colonna, li_ret, ll_i, ll_offset, ll_counter, ll_rows, ll_progressivo, ll_prog_listino_prod, ll_count_scaglione, ll_count
string			ls_valore, ls_foglio, ls_cod_prodotto_old, ls_cod_cliente_old, ls_flag_sconto_mag_prezzo
string			ls_cod_prodotto_var, ls_cod_cliente_var, ls_versione_default, ls_cod_prodotto_padre_var
decimal			ld_valore, ld_num_scaglione, ld_limite_dim_1, ld_limite_dim_2, ld_variazione
datetime		ldt_valore, ldt_data_inizio_val
boolean		lb_dim
decimal			ld_scaglione_var, ld_variazione_var, ld_provvigione_var
string			ls_flag_sconto_mag_prezzo_var, ls_flag_origine_prezzo_var, ls_flag_escludi_linee_sconto
datastore		lds_dim, lds_var
boolean 		lb_VAR_trovata = false

ls_foglio = fs_cod_prodotto

//cerca la posizione della cella con contenuto VAR
ll_riga = 2
ll_colonna = 1
for ll_colonna = 1 to 100
	iuo_excel.uof_leggi_ottimistico(ls_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if ls_valore = "VAR" then				
		lb_VAR_trovata = true
		exit
	end if
next

if not lb_VAR_trovata then
	//errore in lettura codice VAR: salta il foglio e logga		
	wf_log(wf_get_today_now() + " Stringa 'VAR' non trovata in lettura dati foglio: "+ls_foglio)
	wf_log("-------------------------------------------------------------------")
	return 0
end if

//leggiamo adesso le varianti
ll_offset = ll_colonna

li_ret = wf_prepara_ds_varianti_client(ls_foglio, ll_offset, lds_var)
if li_ret = 0 then
	//log già scritto
	return 0
end if

//importazione
//occorre recuperare una PK valida dalla listini_vendite

/*
31/03/2010----------------------------------------------------------------------------------------------
La data di importazione sarà presa dal max della data stessa dalla listini vendite
con cod cliente null (quindi sarà una condizione comune)
Inoltre l'altra parte della PK sarà presa dalla corrispondente variante del comune
*/

select max(data_inizio_val)
into :ldt_data_inizio_val
from listini_vendite
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
	cod_valuta=:is_cod_valuta and cod_prodotto=:fs_cod_prodotto and
	cod_cat_mer is null and cod_cliente is null and cod_categoria is null;
	
//check se hai trovato qualcosa
if sqlca.sqlcode <> 0 then
	//errore in select max
	g_mb.messagebox("APICE", "Foglio:"+fs_cod_prodotto+". Errore lettura max data inizio validità: "+ sqlca.sqlerrtext, StopSign!)
	rollback;
	return -1
end if

//recupera la PK con questa data
select progressivo
into :ll_progressivo
from listini_vendite
where cod_azienda=:s_cs_xx.cod_azienda and
	cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
	cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and
	cod_prodotto=:fs_cod_prodotto and cod_cat_mer is null and 
	cod_cliente is null and cod_categoria is null;

/*
STRUTTURA del DATASTORE delle varianti ----------------------------------
1.  prog_var
2.	cod_cliente
3.	cod_prodotto_figlio
4.	scaglione_1
5.	flag_sconto_mag_prezzo_1
6.	variazione_1
7.	flag_origine_prezzo_1
8.	provvigione
9.	cod_prodotto_padre
10.versione_default
11.flag_escludi_linee_sconto   (solo per foglio standard confezionato e varianti per cliente  listini_prod_comune_var_client)
--------------------------------------------------------------------------------------------------
*/


//nel ciclo, se il prodotto (nell'ambito dello stesso cod_cliente) si ripete aggiungi un nuovo scaglione in tabella
//la var. di istanza "idt_data_inizio_val" è sicuramente valorizzata xchè stiamo nel listino Comune
ls_cod_prodotto_old = ""
ls_cod_cliente_old = ""
ll_counter = 1

if isvalid(lds_var) then
	ll_rows = lds_var.rowcount()
else
	ll_rows = 0
end if

for ll_i = 1 to ll_rows
	//lettura delle variabili dal datastore
	ls_cod_cliente_var = lds_var.getitemstring(ll_i, 2)
	ls_cod_prodotto_var = lds_var.getitemstring(ll_i, 3)
	ld_scaglione_var = lds_var.getitemdecimal(ll_i, 4)
	ls_flag_sconto_mag_prezzo_var = lds_var.getitemstring(ll_i, 5)
	ld_variazione_var = lds_var.getitemdecimal(ll_i, 6)
	ls_flag_origine_prezzo_var = lds_var.getitemstring(ll_i, 7)
	ld_provvigione_var = lds_var.getitemdecimal(ll_i, 8)
	ls_cod_prodotto_padre_var = lds_var.getitemstring(ll_i, 9)
	
	ls_flag_escludi_linee_sconto = lds_var.getitemstring(ll_i, 11)
	
	//ls_versione_default = lds_var.getitemstring(ll_i, 10)
	if wf_get_versione_default(fs_cod_prodotto, ls_versione_default) < 0 then
		//msg già dato
		rollback;
		return -1
	end if
	
	select count(*)
	into :ll_count
	from anag_clienti
	where cod_azienda=:s_cs_xx.cod_azienda and
		cod_cliente=:ls_cod_cliente_var;
		
	if sqlca.sqlcode = 0 and ll_count=1 then
	else
		g_mb.messagebox("APICE", "Foglio:"+fs_cod_prodotto+". Codice cliente '"+ls_cod_cliente_var+"' inesistente;  variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
		rollback;
		return -1
	end if
	
	if ls_cod_cliente_var=ls_cod_cliente_old and ls_cod_prodotto_var=ls_cod_prodotto_old then
		//stesso prodotto e stesso cliente: aggiungi un nuovo scaglione (fai update) #################################
		
		ll_counter += 1		//counter che tiene traccia del numero scaglione a cui siamo arrivati
		
		//controlla se per caso è superiore al 5 (max 5 scaglioni)
		if ll_counter > 5 then
			//salta la riga e logga
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Msg: Hai superato il n° max pari a 5 scaglioni! Riga di excel ignorata")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
			
		//la tabella interessata è listini_prod_comune_var_client			
		//verifica che lo scaglione non sia già presente -----------------------------------------------------------
		setnull(ll_count_scaglione)
		select count(*)
		into :ll_count_scaglione
		from listini_prod_comune_var_client
		where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and 
					(scaglione_1=:ld_scaglione_var or scaglione_2=:ld_scaglione_var or scaglione_3=:ld_scaglione_var or
						scaglione_4=:ld_scaglione_var or scaglione_5=:ld_scaglione_var);

		if ll_count_scaglione>0 then
			//si tratta di uno scaglione duplicato: salta la riga e logga
			ll_counter -= 1    //ripristina il valore del counter
			
			wf_log(wf_get_today_now() + " Foglio: "+ls_foglio+" Variante: "+ls_cod_prodotto_var+" Cliente:"+ls_cod_cliente_var+" -  Msg: Riga di scaglione duplicata! Q.tà scaglione:"+&
												string(ld_scaglione_var)+" - Riga di excel ignorata!")
			wf_log("---------------------------------------------------------------------")
			continue
		end if
		//-----------------------------------------------------------------------------------------------------------------------
		
		choose case ll_counter
			case 2
				update listini_prod_comune_var_client
				set scaglione_2 = :ld_scaglione_var, flag_sconto_mag_prezzo_2 = :ls_flag_sconto_mag_prezzo_var,
						variazione_2 = :ld_variazione_var, 	flag_origine_prezzo_2 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
					
			case 3
				update listini_prod_comune_var_client
				set scaglione_3 = :ld_scaglione_var, flag_sconto_mag_prezzo_3 = :ls_flag_sconto_mag_prezzo_var,
						variazione_3 = :ld_variazione_var, 	flag_origine_prezzo_3 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
					
			case 4
				update listini_prod_comune_var_client
				set scaglione_4 = :ld_scaglione_var, flag_sconto_mag_prezzo_4 = :ls_flag_sconto_mag_prezzo_var,
						variazione_4 = :ld_variazione_var, 	flag_origine_prezzo_4 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
					
			case 5
				update listini_prod_comune_var_client
				set scaglione_5 = :ld_scaglione_var, flag_sconto_mag_prezzo_5 = :ls_flag_sconto_mag_prezzo_var,
						variazione_5 = :ld_variazione_var, 	flag_origine_prezzo_5 = :ls_flag_origine_prezzo_var
				where cod_azienda=:s_cs_xx.cod_azienda and cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
					cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and progressivo=:ll_progressivo and
					cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and 
					prog_listino_produzione=:ll_prog_listino_prod and cod_cliente=:ls_cod_cliente_var;
					
		end choose
	
		if sqlca.sqlcode = 0 then
		else
			//errore in aggiornamento
			g_mb.messagebox("APICE", "Configurato senza cliente:"+fs_cod_prodotto+". Errore in aggiornamento (listini_prod_comune_var_client) listino varianti (scaglione "+string(ll_counter)+"): variante:"+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
		
		
	else
		//scaglione n°1 (fai insert) #######################################################################
		ll_counter = 1		//reset del numero scaglione
		
			
		//la tabella interessata è listini_prod_comune_var_client			
		//non devo calcolare il max+1 ma cercare di leggere la PK corrispondente dalla listini_prod_comune			
		select max(prog_listino_produzione)
		into :ll_prog_listino_prod
		from listini_prod_comune
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
			cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and
			cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and
			cod_prodotto_figlio=:ls_cod_prodotto_var and cod_cliente is null;
		
		if sqlca.sqlcode <> 0 or ll_prog_listino_prod<=0 or isnull(ll_prog_listino_prod) then
			g_mb.messagebox("APICE", "Chiave non trovata in listini_prod_comune: Configurato standard:"+fs_cod_prodotto+&
																	" variante: "+ls_cod_prodotto_var+" - cliente:"+ls_cod_cliente_var+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
		
		/*
		//calcolo il max prog_listino_produzione
		select max(prog_listino_produzione)
		into :ll_prog_listino_prod
		from listini_produzione
		where cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_listino_prodotto=:is_cod_tipo_listino_prodotto and
			cod_valuta=:is_cod_valuta and data_inizio_val=:ldt_data_inizio_val and
			cod_prodotto_listino=:fs_cod_prodotto and cod_versione=:ls_versione_default and
			cod_cliente=:ls_cod_cliente_var;
					
		if isnull(ll_prog_listino_prod) then ll_prog_listino_prod = 0
		ll_prog_listino_prod += 1
		*/
		
		insert into listini_prod_comune_var_client
		(cod_azienda, cod_tipo_listino_prodotto,cod_valuta,data_inizio_val,progressivo,
		cod_prodotto_listino,cod_versione,prog_listino_produzione,
		cod_prodotto_padre,cod_prodotto_figlio,flag_tipo_scaglioni,
		scaglione_1,flag_sconto_mag_prezzo_1,variazione_1,flag_origine_prezzo_1,
		provvigione_1, cod_cliente, flag_escludi_linee_sconto)
		values
		(:s_cs_xx.cod_azienda, :is_cod_tipo_listino_prodotto, :is_cod_valuta, :ldt_data_inizio_val, :ll_progressivo,
		:fs_cod_prodotto,:ls_versione_default,:ll_prog_listino_prod,
		:ls_cod_prodotto_padre_var,:ls_cod_prodotto_var,'Q',
		:ld_scaglione_var,:ls_flag_sconto_mag_prezzo_var,:ld_variazione_var,:ls_flag_origine_prezzo_var,
		:ld_provvigione_var, :ls_cod_cliente_var, :ls_flag_escludi_linee_sconto);
		
		if sqlca.sqlcode = 0 then
		else
			//errore in inserimento
			g_mb.messagebox("APICE", "Errore in inserimento varianti listino (listini_prod_comune_var_client): Configurato senza cliente:"+fs_cod_prodotto+" variante: "+ls_cod_prodotto_var+" "+ sqlca.sqlerrtext, StopSign!)
			rollback;
			return -1
		end if
	
	
		//mi salvo i nuovi valori
		ls_cod_cliente_old = ls_cod_cliente_var
		ls_cod_prodotto_old = ls_cod_prodotto_var
	end if
	
next

destroy lds_dim
destroy lds_var

wf_log(wf_get_today_now() + "Prodotto: Configurato: "+fs_cod_prodotto+": varianto con cliente, importazione completata")

return 1
end function

public function integer wf_prepara_ds_varianti_client (string fs_foglio, ref long fl_offset, ref datastore fds_data);//se fs_flag = "STD" è un configurato
string			ls_sql, ls_valore, ls_msg, ls_cod_versione_default, ls_cod_prodotto_padre
string			ls_tipo_var_std, ls_tipo_var_pers
string			ls_cod_prod_std, ls_cod_prod_pers, ls_cod_prod_variante, ls_flag_escludi_linee_sconto
long				ll_rows, ll_riga, ll_colonna, ll_newrow
decimal			ld_valore, ld_num_var
datetime		ldt_valore
integer			li_ret

//mi serve un datastore vuoto dove ricoverare i dati del foglio excel delle varianti
ls_sql = "select "+&
						"100 as prog_var,"+&
						"cod_cliente,"+&
						"cod_prodotto_figlio,"+&
						"scaglione_1,"+&
						"flag_sconto_mag_prezzo_1,"+&
						"variazione_1,"+&
						"flag_origine_prezzo_1,"+&
						"0.00 as provvigione,"+&
						"cod_prodotto_padre,"+&
						"'      ' as versione_default "+&
						",'N' as flag_escludi_linee_sconto "+&
			"from listini_produzione "+&			
			"where cod_azienda='VALOREASSURDO' "
			
if f_crea_datastore(fds_data, ls_sql) then
else
	wf_log(wf_get_today_now() + " Errore di datastore: Foglio: "+fs_foglio)
	return 0
end if

ll_riga = 3	//i dati sono presenti a partire dalla riga n°3

//la etichetta VAR, presente sulla riga 2 
//mentre il n° colonna è:	upperbound(ld_x_excel[] + 3) e vale anche nel caso in cui non ci siano DIM (0 + 3 = 3)
//questa info è nell'argomento fl_offset
fl_offset = fl_offset - 1		//decremento perchè poi viene re-incrementato

do while 1=1
	//le colonne nel foglio di confez sono fissate a :
	//         13 (se configurato- standard)       e             10 (se configurato-cliente)
	
	/*	  1. 					è un progressivo numerico (ultimo criterio di ordinamento)
		  2.	COD CLIENTE									  2.	VARIANTE (cod_prodotto_figlio)
		  3.	RAG.SOC.										 	  3.	DES-VARIANTE
		  4.	VARIANTE (cod_prodotto_figlio)			  4.	SCAGLIONE
		  5.	DES-VARIANTE									  5.	TIPO VAR (flag_sconto_mag_prezzo)
		  6.	SCAGLIONE										  6.	VARIAZ.	
		  7.	TIPO VAR	(flag_sconto_mag_prezzo)	  7.	ORIGINE (flag_origine_prezzo)
		  8.	VARIAZ.												  8.	PROVV.
		  9.	ORIGINE (flag_origine_prezzo)			  9.	cod_prodotto_padre
		10.	PROVV.												10.	versione_default
		11.	cod_prodotto_padre
		12.	versione_default								13. flag_escludi_linee_sconto
	*/
	
	//colonna VAR : usato solo per sapere se ci sono ancora dati da leggere
	ll_colonna = fl_offset +1
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_num_var, ldt_valore, "D")
	if li_ret = 1 then
		ll_newrow = fds_data.insertrow(0)	//inserisci la riga
		fds_data.setitem(ll_newrow, 1, ld_num_var)
	elseif li_ret = 0 then
		//i dati da leggere sono finiti
		exit
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	//--------------------------------------------------------------------------

	//2.	COD CLIENTE				  2.	VARIANTE (cod_prodotto_figlio)--------------------------------------------------------
	ll_colonna = fl_offset + 2
	//se la lettura da errore assumi assenza di cliente e quindi salta
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if li_ret = 1 then
		if isnull(ls_valore) or ls_valore="" then
			fds_data.deleterow(ll_newrow)
			ll_riga += 1
			continue
		end if
	else		
		fds_data.deleterow(ll_newrow)
		ll_riga += 1
		continue
	end if
	fds_data.setitem(ll_newrow, 2, ls_valore)	//cod_cliente			
	
	
	//-------------------------------------------------------------------------------------------------------------------
	//3.	RAG.SOC.	 	  3.	DES-VARIANTE--------------------------------------------------------------
	//non mi interessa -----------------------------------------------------------
	
	//4.	VARIANTE (cod_prodotto_figlio)			  4.	SCAGLIONE
	ll_colonna = fl_offset + 4
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prod_std, ld_valore, ldt_valore, "S")
	if li_ret = 1 then			
		fds_data.setitem(ll_newrow, 3, ls_cod_prod_std)	//cod_prodotto_figlio
		ls_cod_prod_variante = ls_cod_prod_std
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if		
	

	//6.	SCAGLIONE			VARIAZ.	 
	ll_colonna = fl_offset + 6
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then			
		fds_data.setitem(ll_newrow, 4, ld_valore)	//scaglione
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if		
	
	
	// 7.	TIPO VAR	(flag_sconto_mag_prezzo)	  7.	ORIGINE (flag_origine_prezzo)
	ll_colonna = fl_offset + 7
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_tipo_var_std, ld_valore, ldt_valore, "S")
	if li_ret = 1 then			
		fds_data.setitem(ll_newrow, 5, ls_tipo_var_std)	//flag_sconto_mag_prezzo
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if


	// 8.	VARIAZ.												  8.	PROVV.
	ll_colonna = fl_offset + 8
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//verifica se devi fare lo scorporo iva (scorpora se P, A o D)
	if is_flag_scorporo_iva = "S" and (ls_tipo_var_std="P" or ls_tipo_var_std="A" or ls_tipo_var_std="D") then 
		wf_scorpora_iva(ls_cod_prod_std, ld_valore)
	end if
	fds_data.setitem(ll_newrow, 6, ld_valore)	//variazione	

	
	//  9.	ORIGINE (flag_origine_prezzo)			  9.	cod_prodotto_padre
	ll_colonna = fl_offset + 9
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "S")
	if li_ret = 1 then			
		fds_data.setitem(ll_newrow, 7, ls_valore)	//flag_origine_prezzo
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	

	//	10.	PROVV.												10.	versione_default
	ll_colonna = fl_offset + 10
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_valore, ld_valore, ldt_valore, "D")
	if li_ret = 1 then			
		fds_data.setitem(ll_newrow, 8, ld_valore)	//provvigione
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	
	//11.	SOLO STD:    cod_prodotto_padre
	ll_colonna = fl_offset + 11
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_prodotto_padre, ld_valore, ldt_valore, "S")
	if li_ret = 1 then
		fds_data.setitem(ll_newrow, 9, ls_cod_prodotto_padre)	//cod_prodotto_padre
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	
	//	12. SOLO STD:     	versione_default	
	ll_colonna = fl_offset + 12
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_cod_versione_default, ld_valore, ldt_valore, "S")
	if li_ret = 1 then
		fds_data.setitem(ll_newrow, 10, ls_cod_versione_default)	//versione_default
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
		
	////verifica la congruità dei dati relativi a cod_prodotto_padre e versione defaut
	////estrai il prodotto dal nome foglio
	//wf_estrai_prodotto_cliente(fs_foglio, ls_cod_prodotto_padre, ls_valore)
	
	//controlla che la variante esista in anagrafica
	if wf_controlla_padre_versione(ls_cod_prod_variante, ls_cod_prodotto_padre, ls_cod_versione_default, ls_msg) <= 0 then
		//salta il foglio e logga
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+": - "+ls_msg)
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	//	13. SOLO STD:     	flag_escludi_linee_sconto	
	ll_colonna = fl_offset + 13
	li_ret = iuo_excel.uof_leggi_ottimistico(fs_foglio, ll_riga, ll_colonna, ls_flag_escludi_linee_sconto, ld_valore, ldt_valore, "S")
	
	if isnull(ls_flag_escludi_linee_sconto) or ls_flag_escludi_linee_sconto="" then ls_flag_escludi_linee_sconto="N"
	
	if li_ret = 1 then
		fds_data.setitem(ll_newrow, 11, ls_flag_escludi_linee_sconto)	//versione_default
	else
		//errore in lettura: salta il foglio e logga		
		wf_log(wf_get_today_now() + " errore in lettura dati foglio: "+fs_foglio+" - riga: "+string(ll_riga)+" - colonna: "+string(ll_colonna))
		wf_log("-------------------------------------------------------------------")
		return 0
	end if
	
	
	ll_riga += 1
loop

//arrivato qui ho il datastore riempito da elaborare

ll_rows = fds_data.rowcount()

fds_data.setsort("cod_cliente asc, cod_prodotto_figlio asc, scaglione_1 asc")
fds_data.sort()

return 1
end function

public function integer wf_get_versione_default (string fs_cod_prodotto, ref string fs_cod_versione);

select dp.cod_versione
into :fs_cod_versione
from distinta_padri dp
where dp.cod_azienda=:s_cs_xx.cod_azienda and dp.cod_prodotto=:fs_cod_prodotto and
	dp.flag_predefinita='S';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante la lettura della versione default; Prodotto: "+fs_cod_prodotto + " "+sqlca.sqlerrtext,StopSign!)
	return -1
end if

return 1
end function

on w_esporta_listini_excel.create
int iCurrent
call super::create
this.st_msg2=create st_msg2
this.sle_tipo=create sle_tipo
this.sle_msg=create sle_msg
this.st_msg=create st_msg
this.cb_1=create cb_1
this.cb_3=create cb_3
this.dw_selezione_2=create dw_selezione_2
this.cb_sfoglia=create cb_sfoglia
this.cb_importa=create cb_importa
this.dw_selezione=create dw_selezione
this.cb_esporta=create cb_esporta
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_msg2
this.Control[iCurrent+2]=this.sle_tipo
this.Control[iCurrent+3]=this.sle_msg
this.Control[iCurrent+4]=this.st_msg
this.Control[iCurrent+5]=this.cb_1
this.Control[iCurrent+6]=this.cb_3
this.Control[iCurrent+7]=this.dw_selezione_2
this.Control[iCurrent+8]=this.cb_sfoglia
this.Control[iCurrent+9]=this.cb_importa
this.Control[iCurrent+10]=this.dw_selezione
this.Control[iCurrent+11]=this.cb_esporta
this.Control[iCurrent+12]=this.dw_folder
end on

on w_esporta_listini_excel.destroy
call super::destroy
destroy(this.st_msg2)
destroy(this.sle_tipo)
destroy(this.sle_msg)
destroy(this.st_msg)
destroy(this.cb_1)
destroy(this.cb_3)
destroy(this.dw_selezione_2)
destroy(this.cb_sfoglia)
destroy(this.cb_importa)
destroy(this.dw_selezione)
destroy(this.cb_esporta)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path, ls_database
windowobject lw_oggetti[]
long li_risposta

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
													
dw_selezione_2.set_dw_options(sqlca, &
										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )

// *** folder													
lw_oggetti[1] = dw_selezione_2
lw_oggetti[2] = cb_importa
lw_oggetti[3] = cb_sfoglia
lw_oggetti[4] = st_msg2
dw_folder.fu_assigntab(2, "Importazione", lw_oggetti[])

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_esporta
lw_oggetti[3] = st_msg
dw_folder.fu_assigntab(1, "Esportazione", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

//abilita il tab dell'importazione se non sei collegato con ptenda (es. cgibus e viropa)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilodefault, "database", ls_database)

//Donato 19/11/2012 ---------------------------------------------------------------------------------
//codice categoria merceologica prodotto per OPTIONALS CONFIGURABILI
guo_functions.uof_get_parametro_azienda("OCF", is_OCF)
if isnull(is_OCF) then is_OCF=""
//--------------------------------------------------------------------------------------------------------

select 	cod_cat_mer 
into   	:is_cod_cat_prodotto_configurato
from 		tab_cat_mer_tabelle 
where 	cod_azienda = :s_cs_xx.cod_azienda and
      		nome_tabella = 'tab_modelli';
if sqlca.sqlcode <> 0 then
	is_cod_cat_prodotto_configurato = ""
end if


end event

event pc_setddlb;call super::pc_setddlb;//dw di esportazione
//f_po_loaddddw_dw(dw_selezione, &
//                 "cod_tipo_listino_prodotto", &
//                 sqlca, &
//                 "tab_tipi_listini_prodotti", &
//                 "cod_tipo_listino_prodotto", &
//                 "des_tipo_listino_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))") 
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &                 
			 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//dw di importazione
//f_po_loaddddw_dw(dw_selezione_2, &
//                 "cod_tipo_listino_prodotto", &
//                 sqlca, &
//                 "tab_tipi_listini_prodotti", &
//                 "cod_tipo_listino_prodotto", &
//                 "des_tipo_listino_prodotto", &
//			"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
f_po_loaddddw_dw(dw_selezione_2, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ") or (data_blocco is null))") 
					  
f_po_loaddddw_dw(dw_selezione_2, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

type st_msg2 from statictext within w_esporta_listini_excel
integer x = 41
integer y = 1232
integer width = 2569
integer height = 84
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type sle_tipo from singlelineedit within w_esporta_listini_excel
boolean visible = false
integer x = 2382
integer y = 1520
integer width = 279
integer height = 112
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "M"
borderstyle borderstyle = stylelowered!
end type

type sle_msg from singlelineedit within w_esporta_listini_excel
integer x = 1070
integer y = 1644
integer width = 1477
integer height = 316
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_msg from statictext within w_esporta_listini_excel
integer x = 41
integer y = 1152
integer width = 2569
integer height = 84
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_esporta_listini_excel
boolean visible = false
integer x = 1682
integer y = 1540
integer width = 247
integer height = 60
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "test colori"
end type

event clicked;iuo_excel.uof_inizio()

iuo_excel.uof_test_colori("Foglio1")
end event

type cb_3 from commandbutton within w_esporta_listini_excel
boolean visible = false
integer x = 1952
integer y = 1512
integer width = 402
integer height = 112
integer taborder = 70
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "mail"
end type

event clicked;integer li_ret

//li_ret = wf_invia_mail()

string ls_pippo
wf_get_desk_dir(ls_pippo)

messagebox("",ls_pippo)
end event

type dw_selezione_2 from uo_cs_xx_dw within w_esporta_listini_excel
integer x = 37
integer y = 112
integer width = 2619
integer height = 900
integer taborder = 40
string dataobject = "d_sel_importa_listini_excel"
boolean border = false
end type

type cb_sfoglia from commandbutton within w_esporta_listini_excel
integer x = 2363
integer y = 144
integer width = 110
integer height = 180
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;string		ls_path, docpath, docname[]
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS),*.XLS,", &
   											ls_path)

if li_ret < 1 then return
li_count = Upperbound(docname)

if li_count = 1 then
	dw_selezione_2.setitem(1, "path_excel", docpath)
end if
end event

type cb_importa from commandbutton within w_esporta_listini_excel
integer x = 2167
integer y = 1024
integer width = 357
integer height = 92
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;datetime		ldt_data_inizio_val

date				ldt_oggi

string			ls_file, ls_flag_scorporo_iva, ls_flag_tipo_importazione, ls_cod_tipo_listino_prodotto, ls_cod_valuta,&
					ls_sheets[], ls_cod_prodotto_cu, ls_cod_cliente_cu, ls_msg
					
boolean		lb_controlla_data_filtro = false
boolean		lb_invia_mail = false

long				ll_i, ll_tot, ll_ret


st_msg2.text = ""
ib_comune_OK = false
dw_selezione_2.accepttext()

setnull(is_cod_tipo_listino_prodotto)
setnull(is_cod_valuta)
setnull(idt_data_inizio_val)

ldt_oggi = today()

ls_cod_tipo_listino_prodotto = dw_selezione_2.getitemstring(1,"cod_tipo_listino_prodotto")
if ls_cod_tipo_listino_prodotto="" or isnull(ls_cod_tipo_listino_prodotto) then
	g_mb.messagebox("APICE","Selezionare la tipologia del nuovo listino!", Information!)
	dw_selezione_2.setcolumn("cod_tipo_listino_prodotto")
	return
end if
is_cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto

ls_cod_valuta = dw_selezione_2.getitemstring(1,"cod_valuta")
if ls_cod_valuta="" or isnull(ls_cod_valuta) then
	g_mb.messagebox("APICE","Selezionare la valuta del nuovo listino!", Information!)
	dw_selezione_2.setcolumn("cod_valuta")
	return
end if
is_cod_valuta = ls_cod_valuta

//C=listino Comune	L=listino Locale		T=Tutto
ls_flag_tipo_importazione = dw_selezione_2.getitemstring(1,"flag_tipo_importazione")

setnull(is_flag_tipo_importazione)
choose case ls_flag_tipo_importazione
	case "C"	//check data da dw
		//caricamento solo dei dati relativi al listino comune e
		//solo su una data validità NON esistente posta ad un valore
		//maggiore di qualsiasi data già esistente nel listino comune
		//alla fine devi inviare una mail
		lb_controlla_data_filtro = true
		is_flag_tipo_importazione = ls_flag_tipo_importazione
		lb_invia_mail = true
		ib_comune_OK = true
		
	case "L"
		//caricamento solo dei dati relativi al listino locale e
		//solo su data validità esistente, quindi NON considerare la nuova data validità
		//ma considerare quella del foglio EXCEL
		//infatti si presume che sia stato già caricato un listino comune nella data specificata
		//non inviare nessuna mail alla fine
		lb_controlla_data_filtro = false
		is_flag_tipo_importazione = ls_flag_tipo_importazione
		lb_invia_mail = false
		ib_comune_OK = false
		
	case "T"	//check data da dw
		//caricamento dei dati relativi al listino comune e locale
		//solo su una data validità NON esistente posta ad un valore
		//maggiore di qualsiasi data già esistente nel listino comune
		//alla fine devi inviare una mail
		lb_controlla_data_filtro = true
		is_flag_tipo_importazione = ls_flag_tipo_importazione
		lb_invia_mail = true
		ib_comune_OK = true
		
end choose

ls_file = dw_selezione_2.getitemstring(1,"path_excel")
if isnull(ls_file) or ls_file = "" then
	g_mb.messagebox("APICE","E' necessario indicare il file excel da importare",Exclamation!)
	return
end if

ls_flag_scorporo_iva = dw_selezione_2.getitemstring(1,"flag_scorporo_iva")
if isnull(ls_flag_scorporo_iva) or ls_flag_scorporo_iva="" then ls_flag_scorporo_iva = "N"

is_flag_scorporo_iva = ls_flag_scorporo_iva

if not FileExists(ls_file) then
	g_mb.messagebox("APICE","Il file non esiste oppure è stato spostato!",Exclamation!)
	return
end if

if g_mb.messagebox("APICE","Importare il listino specificato dal file '"+&
							ls_file+"' ?",Question!,YesNo!,2)=1 then	
else
	return
end if

if ls_flag_tipo_importazione="L" then
	//avvisa anche se l'utente ha controllato le date nei fogli excel relativi ai listini locali
	if g_mb.messagebox("APICE","Hai controllato le nuove date per i listini personalizzati (locali) specificato dal file '"+&
							ls_file+"' ? "+char(13)+&
							"Premi NO per annullare e controllare, SI per proseguire.",Question!,YesNo!,2)=1 then	
	else
		return
	end if
end if

iuo_excel = create uo_excel_listini
is_flag_scorporo_iva = ls_flag_scorporo_iva

iuo_excel.is_path_file = ls_file
iuo_excel.ib_oleobjectvisible = false

iuo_excel.uof_open( )

iuo_excel.uof_get_sheets(ls_sheets)

ll_tot = upperbound(ls_sheets)

if isnull(ll_tot) or ll_tot <= 0 then
	g_mb.messagebox("APICE","Non esistono fogli di lavoro nel file excel specificato!",Exclamation!)
	iuo_excel.uof_close( )
	destroy iuo_excel
	return
end if

ldt_data_inizio_val = dw_selezione_2.getitemdatetime(1,"data_inizio_nuova")
if lb_controlla_data_filtro then
	
	//controlla se hai specificato la nuova data di validità ---------------------------------------------------
	if isnull(ldt_data_inizio_val) then
		g_mb.messagebox("APICE","E' necessario indicare la data inizio validità",Exclamation!)
		iuo_excel.uof_close( )
		destroy iuo_excel
		return
	end if
	
//	//controlla se la data specificata è inferiore ad una già esistente nel listino comune ----------
//	if not wf_controlla_data_listino_comune(ldt_data_inizio_val, &
//								ls_cod_tipo_listino_prodotto, ls_cod_valuta,ls_sheets) then
//		//l'eventuale messaggio è stato già dato dalla funzione
//		iuo_excel.uof_close( )
//		destroy iuo_excel
//		return
//	end if
	
	//segnala se la data specificata è inferiore a quella di sistema -------------------------------------
	if date(ldt_data_inizio_val) < ldt_oggi then
		if g_mb.messagebox("APICE","La nuova data inizio validità è inferiore alla data odierna. Continuare?", &
						Question!, YesNo!,2) = 2 then
			iuo_excel.uof_close( )
		destroy iuo_excel
		return
		end if
	end if
	
	idt_data_inizio_val = ldt_data_inizio_val
end if



//se arrivi fin qui riserva un nome di file per loggare tutto
//<path utente>\data-e-ora_COD_UTENTE.log
setnull(is_filelog)

wf_get_desk_dir(is_filelog)
//f_getuserpath(is_filelog)

is_filelog +=string(datetime(today(),now()),"ddMMyyyy-hhmmss")+"_"+s_cs_xx.cod_utente+".log"

wf_log("--------------------------------------------------------------------------------------------")
wf_log(wf_get_today_now() + " INIZIO ELABORAZIONE IMPORTAZIONE da file: "+ls_file)

wf_log(wf_get_today_now() + " Impostazione del filtro:")
wf_log(wf_get_today_now() + " Cod.Tipo Listino Prodotto: "+ls_cod_tipo_listino_prodotto)
wf_log(wf_get_today_now() + " Cod.Valuta: "+ls_cod_valuta)
wf_log(wf_get_today_now() + " Flag Tipo Importazione: "+ls_flag_tipo_importazione)

if not isnull(ldt_data_inizio_val) then &
	wf_log(wf_get_today_now() + " Nuova Data Inizio Val.: "+string(ldt_data_inizio_val, "dd/MM/yyyy"))

if ls_flag_tipo_importazione = "L" and not isnull(ldt_data_inizio_val) then &
	wf_log(wf_get_today_now() + " ****La nuova Data Inizio Val. che è stata specificata: "+string(ldt_data_inizio_val, "dd/MM/yyyy")+" sarà ignorata")

wf_log("--------------------------------------------------------------------------------------------")

if ls_flag_tipo_importazione = "C" or ls_flag_tipo_importazione="T" then
	//IMPORTA LE CONDIZIONI COMUNI (STANDARD) : #SFUSO oppure ad es. B020
	
	wf_log(wf_get_today_now() + "INIZIO ELABORAZIONE IMPORTAZIONE CONDIZIONI COMUNI")
	wf_log("--------------------------------------------------------------------------------------------")
	
	//esamina tutti i fogli presenti nel file excel
	for ll_i = 1 to ll_tot
		//se il nome del foglio contiene il cliente allora si tratta di condizioni per cliente: PER ORA SALTALO
		
		//azzero le variabili
		ls_cod_prodotto_cu = ""
		ls_cod_cliente_cu = ""
		ls_msg = ""
		ls_msg = wf_estrai_prodotto_cliente(ls_sheets[ll_i], ls_cod_prodotto_cu, ls_cod_cliente_cu)
		if ls_msg <> "" then
			//c'è stato un problema nell'estrazione prodotto-cliente dal nome del foglio: LOGGA!
			wf_log(wf_get_today_now() + ls_msg + "ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' IGNORATA")
			wf_log("--------------------------------------------------------------------------------------------")
			ib_comune_OK = false
			continue
		else
			if ls_cod_cliente_cu <> "" then
				//foglio per cliente: SALTA e LOGGA il SALTO
				wf_log(wf_get_today_now() + "ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' IGNORATA perchè si tratta di condizioni per Cliente: '"+ls_cod_cliente_cu+"'")
				wf_log("--------------------------------------------------------------------------------------------")
				continue
			end if
		end if
		
		wf_log(wf_get_today_now() + "INIZIO ELABORAZIONE FOGLIO STANDARD '"+ls_sheets[ll_i]+"' ")
		if ls_cod_prodotto_cu = "#" then
			wf_log(wf_get_today_now() + "Prodotto: SFUSO")
		else
			wf_log(wf_get_today_now() + "Prodotto: '"+ls_cod_prodotto_cu+"'")
		end if		
		wf_log("--------------------------------------------------------------------------------------------")
		
		//chiama la funzione per l'inserimento listino comune (considera la data specificata nel filtro)
		if ls_cod_prodotto_cu = "#" then
			st_msg2.text = "Importazione Foglio #SFUSO"
			ll_ret = wf_importa_sfuso() 
			if ll_ret < 0 then
				//rollback e messaggio di errore già dati
				iuo_excel.uof_close( )
				destroy iuo_excel
				return
			elseif ll_ret=0 then
				//spegni il semaforo per il comune
				ib_comune_OK = false
			end if
		else
			st_msg2.text = "Importazione Foglio "+ls_cod_prodotto_cu
			ll_ret = wf_importa_configurato(ls_cod_prodotto_cu)
			if ll_ret < 0 then
				//rollback e messaggio di errore già dati
				iuo_excel.uof_close( )
				destroy iuo_excel
				return
			elseif ll_ret = 0 then
				//spegni il semaforo per il comune
				ib_comune_OK = false
			end if
		end if
	next
end if

if ls_flag_tipo_importazione = "L" or ls_flag_tipo_importazione="T" then
	//IMPORTA LE CONDIZIONI LOCALI (PERSONALIZZATE) : #SFUSO(cliente1) oppure ad es. B020(cliente1)
	
	wf_log(wf_get_today_now() + "INIZIO ELABORAZIONE IMPORTAZIONE CONDIZIONI LOCALI")
	wf_log("--------------------------------------------------------------------------------------------")	
	
	//se "T" ho già importato il listino comune prima
	
	//esamina tutti i fogli presenti nel file excel
	for ll_i = 1 to ll_tot
		//se il nome del foglio contiene il cliente allora si tratta di condizioni per cliente: ELABORA SOLO QUESTI
		
		//azzero le variabili
		ls_cod_prodotto_cu = ""
		ls_cod_cliente_cu = ""
		ls_msg = ""
		ls_msg = wf_estrai_prodotto_cliente(ls_sheets[ll_i], ls_cod_prodotto_cu, ls_cod_cliente_cu)
		if ls_msg <> "" then
			//c'è stato un problema nell'estrazione prodotto-cliente dal nome del foglio: LOGGA!
			wf_log(wf_get_today_now() + ls_msg + "ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' IGNORATA")
			wf_log("--------------------------------------------------------------------------------------------")
			continue
			
		elseif ls_cod_cliente_cu = "" then
			//foglio standard (senza cliente):
			
			//Donato 09/03/2011
			//###########################################################
			//NOTA: considera solo i fogli del configurato, e non quelli dello sfuso
			
			if pos(ls_sheets[ll_i], "#SFUSO") > 0 then
				//si tratta di un foglio sfuso senza cliente: salta
				wf_log(wf_get_today_now() + ls_msg + "ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' IGNORATA")
				wf_log("si tratta di uno sfuso con condizioni comuni!!!!!")
				wf_log("--------------------------------------------------------------------------------------------")
				continue
			end if
			
			//###########################################################
				
			wf_log(wf_get_today_now() + "INIZIO ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' ")
			
			//verifica ed importa solo le varianti con codice cliente specificato
			st_msg2.text = "Importazione Foglio "+ls_cod_prodotto_cu + " (solo varianti per clienti)"
			if wf_importa_configurato_var_client(ls_cod_prodotto_cu)<0 then
				//rollback e messaggio di errore già dati
				iuo_excel.uof_close( )
				destroy iuo_excel
				return
			end if
//				wf_log(wf_get_today_now() + "ELABORAZIONE FOGLIO '"+ls_sheets[ll_i]+"' IGNORATA perchè NON si tratta di condizioni per Cliente")
//				wf_log("--------------------------------------------------------------------------------------------")
//				continue
			
		else
			
			wf_log(wf_get_today_now() + "INIZIO ELABORAZIONE FOGLIO CLIENTE '"+ls_sheets[ll_i]+"' ")
			if ls_cod_prodotto_cu = "#" then
				wf_log(wf_get_today_now() + "Prodotto: SFUSO")
			else
				wf_log(wf_get_today_now() + "Prodotto: '"+ls_cod_prodotto_cu+"'")
			end if		
			wf_log(wf_get_today_now() + "Cliente: '"+ls_cod_cliente_cu+"'")
			wf_log("--------------------------------------------------------------------------------------------")
			
			//chiama la funzione per l'inserimento listino personalizzato
			if ls_cod_prodotto_cu = "#" then
				st_msg2.text = "Importazione Foglio #SFUSO("+ls_cod_cliente_cu + ")"
				if wf_importa_sfuso_cliente(ls_cod_cliente_cu) < 0 then
					//rollback e messaggio di errore già dati
					iuo_excel.uof_close( )
					destroy iuo_excel
					return
				end if
			else
				st_msg2.text = "Importazione Foglio "+ls_cod_prodotto_cu+"("+ls_cod_cliente_cu + ")"
				if wf_importa_configurato_cliente(ls_cod_prodotto_cu, ls_cod_cliente_cu) < 0 then
					//rollback e messaggio di errore già dati
					iuo_excel.uof_close( )
					destroy iuo_excel
					return
				end if
			end if
			
		end if
		
	next
end if

wf_log("--------------------------------------------------------------------------------------------")
wf_log(wf_get_today_now()+"FINE ELABORAZIONE PER IMPORTAZIONE")
wf_log("--------------------------------------------------------------------------------------------")

iuo_excel.uof_close( )

commit;
st_msg2.text = "Importazione terminata"
g_mb.messagebox("APICE", "Operazione terminata! Log scritto in "+is_filelog, Information!)

run("notepad.exe " + is_filelog)
//g_mb.messagebox("APICE", "Operazione terminata! Log scritto in "+is_filelog, Information!)

destroy iuo_excel

//if lb_invia_mail and ib_comune_OK then
//	wf_invia_mail()
//end if

end event

type dw_selezione from uo_cs_xx_dw within w_esporta_listini_excel
integer x = 37
integer y = 112
integer width = 2619
integer height = 900
integer taborder = 20
string dataobject = "d_sel_esporta_listini_excel"
boolean border = false
end type

event itemchanged;call super::itemchanged;datetime ldt_null
string ls_null

setnull(ldt_null)
setnull(ls_null)

choose case dwo.name
		
	case "flag_da_a_prodotto"
		if data = "S" then
			//ricerca da prodotto a prodotto
			object.da_cod_prodotto_t.text = "Da Prodotto:"
			dw_selezione.object.b_ricerca_prodotto_da.visible = true
		else
			//un solo campo ricerca prodotto e deve essere una tenda
			object.da_cod_prodotto_t.text = "Prodotto"
			dw_selezione.object.b_ricerca_prodotto_a.visible = false
		end if
		
	case "flag_estrai_ultima"
		if data = "S" then
			//disabilita i campi da data a data validita
			setitem(row,"da_data", ldt_null)
			setitem(row,"a_data", ldt_null)
			
			dw_selezione.object.da_data.protect = 1
			dw_selezione.object.a_data.protect = 1
		else
			//abilita i campi da data a data validita
			dw_selezione.object.da_data.protect = 0
			dw_selezione.object.a_data.protect = 0
		end if
	
	case "da_cod_prodotto"
		if data <> "" and not isnull(data) then
			setitem(row, "a_cod_prodotto", data)
		end if
	
end choose
end event

event editchanged;call super::editchanged;string ls_null

setnull(ls_null)

if row > 0 then
	choose case dwo.name
		case "cod_prodotto_speciale"
			if isnull(data) or data = "" then
				//risabilita la ricerca like
			else
				//abilita la ricerca like
				setitem(row,"da_cod_prodotto",ls_null)
				setitem(row,"a_cod_prodotto",ls_null)
			end if
			
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"da_cod_prodotto")
		
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_selezione,"a_cod_prodotto")

	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type cb_esporta from commandbutton within w_esporta_listini_excel
integer x = 2167
integer y = 1024
integer width = 357
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta"
end type

event clicked;string			ls_flag_incorporo_iva, ls_flag_da_a_prodotto, ls_tenda, ls_cod_prodotto_da, ls_flag_sfuso_confezionato, &
				ls_cod_prodotto_a, ls_cod_cat_mer, ls_estrai_cond_clienti, ls_flag_ordina_per_prodotto, ls_cod_cliente
long			ll_riga, ll_colonna

iuo_excel = create uo_excel_listini

iuo_excel.ib_OleObjectVisible = true//false


dw_selezione.accepttext()

st_msg.text = ""

ls_flag_sfuso_confezionato =  dw_selezione.getitemstring(1,"flag_sfuso_confezionato")

ls_flag_incorporo_iva = dw_selezione.getitemstring(1,"flag_incorporo_iva")
if isnull(ls_flag_incorporo_iva) or ls_flag_incorporo_iva="" then ls_flag_incorporo_iva = "N"
is_flag_incorporo_iva = ls_flag_incorporo_iva

ls_flag_da_a_prodotto = dw_selezione.getitemstring(1,"flag_da_a_prodotto")
if isnull(ls_flag_da_a_prodotto) or ls_flag_da_a_prodotto = "" then ls_flag_da_a_prodotto = "S"

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")

ls_cod_prodotto_da = dw_selezione.getitemstring(1,"da_cod_prodotto")
ls_cod_prodotto_a = dw_selezione.getitemstring(1,"a_cod_prodotto")

ls_flag_ordina_per_prodotto = dw_selezione.getitemstring(1,"flag_ordina_per_prodotto")
if isnull(ls_flag_ordina_per_prodotto) or ls_flag_ordina_per_prodotto="" then ls_flag_ordina_per_prodotto = "N"

//g_mb.messagebox("","L'esportazione sta per iniziare: "+&
//					"NON cliccare sul foglio di lavoro Excel durante la scrittura delle informazioni."+char(13)+&
//					"Premere OK per iniziare!",Information!)

//if ls_flag_da_a_prodotto = "S" then
	//da prodotto a prodotto
	iuo_excel.uof_inizio()
	
	if ls_flag_sfuso_confezionato = "S" or ls_flag_sfuso_confezionato = "T" then
		if isnull(ls_cod_cliente) then
			st_msg.text = "Elaborazione Prodotti Configurati"
			wf_configurato(ll_riga, ll_colonna)				//serie di cartelle excel tipo A020, A021, A022, ecc...
		else		
			if ls_flag_ordina_per_prodotto="N" then
				st_msg.text = "Elaborazione Prodotti Configurati per Clienti"
				wf_configurato_cliente(ll_riga, ll_colonna)	//serie di cartelle excel tipo A020(cli1), A020(cli2), A021(cli3), ecc...
			end if
		end if
	end if
	
	if ls_flag_sfuso_confezionato = "N" or ls_flag_sfuso_confezionato = "T" then
		if isnull(ls_cod_cliente) then
			st_msg.text = "Elaborazione Sfuso"
			wf_sfuso(ll_riga, ll_colonna)						//una cartella excel tipo #SFUSO
		else		
			st_msg.text = "Elaborazione Sfuso per Clienti"
			wf_sfuso_cliente(ll_riga, ll_colonna)			//serie di cartelle excel tipo #SFUSO(cli1), #SFUSO(cli2), ecc..
		end if
		st_msg.text = "Elaborazione Terminata"
	end if	
	iuo_excel.uof_visible(true)
//else
	/*
	if isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da="" then
		g_mb.messagebox("APICE", "E' necessario selezionare un prodotto!", Exclamation!)
		destroy iuo_excel
		return
	end if
	
		
	if upper(is_cod_cat_prodotto_configurato) = ls_tenda or upper(is_cod_cat_prodotto_configurato)=is_OCF then
	else
		g_mb.messagebox("APICE", "E' necessario selezionare una tenda!", Exclamation!)
		destroy iuo_excel
		return
	end if
	
	ls_estrai_cond_clienti = dw_selezione.getitemstring(1,"flag_estrai_cond_clienti")
	if isnull(ls_estrai_cond_clienti) or ls_estrai_cond_clienti="" then ls_estrai_cond_clienti = "N"
	
	//chiamare qui la funzione opportuna
	g_mb.messagebox("APICE","Funzionalità in attesa di chiarimenti dal cliente!")
	*/
//end if

destroy iuo_excel
end event

type dw_folder from u_folder within w_esporta_listini_excel
integer x = 27
integer y = 12
integer width = 2661
integer height = 1376
integer taborder = 30
boolean border = false
end type

