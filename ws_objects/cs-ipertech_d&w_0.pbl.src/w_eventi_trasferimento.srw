﻿$PBExportHeader$w_eventi_trasferimento.srw
forward
global type w_eventi_trasferimento from w_cs_xx_principale
end type
type dw_ricerca from uo_dw_search within w_eventi_trasferimento
end type
type dw_eventi_trasferimento from uo_cs_xx_dw within w_eventi_trasferimento
end type
type dw_folder from u_folder within w_eventi_trasferimento
end type
end forward

global type w_eventi_trasferimento from w_cs_xx_principale
integer width = 3607
integer height = 2280
string title = "Eventi Trasferimenti"
dw_ricerca dw_ricerca
dw_eventi_trasferimento dw_eventi_trasferimento
dw_folder dw_folder
end type
global w_eventi_trasferimento w_eventi_trasferimento

type variables
string			is_sql_base
end variables

forward prototypes
public function integer wf_ricerca ()
end prototypes

public function integer wf_ricerca ();
long				ll_count
integer			li_num_evento
string				ls_sql, ls_cod_reparto_partenza, ls_cod_reparto_arrivo, ls_cod_deposito_arrivo


ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

li_num_evento = dw_ricerca.getitemnumber(1, "num_evento")

if li_num_evento>0 then
	ls_sql += " and num_evento="+string(li_num_evento)
end if

ls_cod_reparto_partenza = dw_ricerca.getitemstring(1, "cod_reparto_partenza")
if ls_cod_reparto_partenza<>"" and not isnull(ls_cod_reparto_partenza) then
	ls_sql += " and cod_reparto_partenza='"+ls_cod_reparto_partenza+"'"
end if

if li_num_evento=3 then
	ls_cod_deposito_arrivo = dw_ricerca.getitemstring(1, "cod_deposito_arrivo")
	
	if ls_cod_deposito_arrivo<>"" and not isnull(ls_cod_deposito_arrivo) then
		ls_sql += " and cod_deposito_arrivo='"+ls_cod_deposito_arrivo+"'"
	end if
	
else
	ls_cod_reparto_arrivo = dw_ricerca.getitemstring(1, "cod_reparto_arrivo")
	
	if ls_cod_reparto_arrivo<>"" and not isnull(ls_cod_reparto_arrivo) then
		ls_sql += " and cod_reparto_arrivo='"+ls_cod_reparto_arrivo+"'"
	end if
	
end if


dw_eventi_trasferimento.setsqlselect(ls_sql)
ll_count = dw_eventi_trasferimento.retrieve()


return ll_count
end function

event pc_setwindow;call super::pc_setwindow;windowobject							lw_oggetti[]


set_w_options(c_autoposition)

dw_eventi_trasferimento.set_dw_key("cod_azienda")
dw_eventi_trasferimento.set_dw_key("progressivo")

dw_eventi_trasferimento.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_noretrieveonopen, &
						 c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)
                  //          c_default + c_NoHighlightSelected)

iuo_dw_main = dw_eventi_trasferimento


lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[1] = dw_eventi_trasferimento
dw_folder.fu_assigntab(2, "Dati", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)


dw_eventi_trasferimento.Modify("cod_deposito_arrivo.Visible='0~tIf(num_evento=3,1,0)'")
dw_eventi_trasferimento.Modify("deposito_arrivo_t.Visible='0~tIf(num_evento=3,1,0)'")
dw_eventi_trasferimento.Modify("cf_deposito_partenza.Visible='0~tIf(num_evento=3,1,0)'")

dw_eventi_trasferimento.Modify("cod_reparto_arrivo.Visible='0~tIf(num_evento=3,0,1)'")
dw_eventi_trasferimento.Modify("reparto_arrivo_t.Visible='0~tIf(num_evento=3,0,1)'")
dw_eventi_trasferimento.Modify("cf_reparto_arrivo.Visible='0~tIf(num_evento=3,0,1)'")


is_sql_base = dw_eventi_trasferimento.getsqlselect()
end event

on w_eventi_trasferimento.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.dw_eventi_trasferimento=create dw_eventi_trasferimento
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.dw_eventi_trasferimento
this.Control[iCurrent+3]=this.dw_folder
end on

on w_eventi_trasferimento.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.dw_eventi_trasferimento)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_eventi_trasferimento, &
                 "cod_reparto_partenza", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_eventi_trasferimento, &
                 "cod_reparto_arrivo", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_eventi_trasferimento, &
                 "cod_deposito_arrivo", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
//------------------------------------------------------------------------

f_po_loaddddw_dw(dw_ricerca, &
                 "cod_reparto_partenza", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ricerca, &
                 "cod_reparto_arrivo", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ricerca, &
                 "cod_deposito_arrivo", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event activate;call super::activate;dw_eventi_trasferimento.setrowfocusindicator(Hand!)
end event

type dw_ricerca from uo_dw_search within w_eventi_trasferimento
integer x = 23
integer y = 128
integer width = 2779
integer height = 644
integer taborder = 20
string dataobject = "d_eventi_trasferimento_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cerca"
		dw_eventi_trasferimento.change_dw_current( )
		parent.postevent("pc_retrieve")
		
end choose
end event

type dw_eventi_trasferimento from uo_cs_xx_dw within w_eventi_trasferimento
integer x = 23
integer y = 128
integer width = 3479
integer height = 2024
integer taborder = 10
string dataobject = "d_eventi_trasferimento"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_count


ll_count = wf_ricerca()

if ll_count < 0 then
   pcca.error = c_fatal
end if

dw_folder.fu_selecttab(2)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_progressivo


for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		
   end if
	
   if isnull(this.getitemnumber(ll_i, "progressivo")) then
		
		select max(progressivo)
		into :ll_progressivo
		from tab_eventi_trasferimento
		where cod_azienda = :s_cs_xx.cod_azienda;
		
		if ll_progressivo < 1 or isnull(ll_progressivo) then
			ll_progressivo = 1
		else
			ll_progressivo ++
		end if
		
		this.setitem(ll_i, "progressivo", ll_progressivo)
   end if
next
end event

event updatestart;call super::updatestart;long ll_i 
if i_extendmode then
	
	for ll_i = 1 to rowcount()
		
		if getitemstatus(ll_i, 0,Primary!) = NewModified! or getitemstatus(ll_i, 0,Primary!)=DataModified! then
			
			if isnull(getitemstring(ll_i, "cod_reparto_partenza")) then
				g_mb.messagebox("Apice","E' necessario specificare un deposito di partenza")
				setrow(ll_i)
				return 1
			end if

			if isnull(getitemstring(ll_i, "cod_reparto_arrivo")) and isnull(getitemstring(ll_i, "cod_deposito_arrivo")) then
				g_mb.messagebox("Apice","E' necessario specificare un deposito o un reparto di arrivo")
				setrow(ll_i)
				return 1
			end if
			
			if not isnull(getitemstring(ll_i, "cod_reparto_arrivo")) and not isnull(getitemstring(ll_i, "cod_deposito_arrivo")) then
				g_mb.messagebox("Apice","Non è corretto specificare sia il deposito che il reparto di arrivo")
				setrow(ll_i)
				return 1
			end if
		end if
		
	next
	
end if

end event

event itemchanged;call super::itemchanged;string ls_null

if i_extendmode then
	
	if isvalid(dwo) then
		
		setnull(ls_null)
		
		choose case dwo.name
			case "cod_deposito_arrivo"
				if not isnull(data) then setitem(row, "cod_reparto_arrivo", ls_null)
				
			case "cod_reparto_arrivo"
				if not isnull(data) then setitem(row, "cod_deposito_arrivo", ls_null)
				
				
		end choose	
	end if
end if
		
end event

type dw_folder from u_folder within w_eventi_trasferimento
integer width = 3515
integer height = 2176
integer taborder = 10
boolean border = false
end type

