﻿$PBExportHeader$w_importa_prodotti.srw
$PBExportComments$Finestra Importa Prodotti Progetto Tenda
forward
global type w_importa_prodotti from w_cs_xx_principale
end type
type cb_importa from uo_cb_ok within w_importa_prodotti
end type
type cb_1 from uo_cb_close within w_importa_prodotti
end type
type cbx_aggiorna from checkbox within w_importa_prodotti
end type
type st_1 from statictext within w_importa_prodotti
end type
type gb_1 from groupbox within w_importa_prodotti
end type
type rb_totale from radiobutton within w_importa_prodotti
end type
type rb_selezionati from radiobutton within w_importa_prodotti
end type
type dw_errore_imp from uo_cs_xx_dw within w_importa_prodotti
end type
type dw_importa_prodotti_orig from uo_cs_xx_dw within w_importa_prodotti
end type
type dw_folder from u_folder within w_importa_prodotti
end type
type dw_importa_prodotti_dest from uo_cs_xx_dw within w_importa_prodotti
end type
end forward

global type w_importa_prodotti from w_cs_xx_principale
int Width=3082
int Height=1497
boolean TitleBar=true
string Title="Importazione Prodotti"
cb_importa cb_importa
cb_1 cb_1
cbx_aggiorna cbx_aggiorna
st_1 st_1
gb_1 gb_1
rb_totale rb_totale
rb_selezionati rb_selezionati
dw_errore_imp dw_errore_imp
dw_importa_prodotti_orig dw_importa_prodotti_orig
dw_folder dw_folder
dw_importa_prodotti_dest dw_importa_prodotti_dest
end type
global w_importa_prodotti w_importa_prodotti

type variables
transaction i_sqlorig
end variables

forward prototypes
public function long wf_importa (long al_riga, ref long al_riga_sel)
end prototypes

public function long wf_importa (long al_riga, ref long al_riga_sel);long ll_conta, ll_i, ll_lungo
string ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_cod_iva, &
       ls_controllo, ls_cod_valuta, ls_cod_deposito
datetime ldt_oggi


select parametri_azienda.stringa
into   :ls_cod_valuta
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Parametro LIR non trovato.", &
              exclamation!, ok!)
   return ll_conta
end if

ll_conta = 0
ls_cod_prodotto = upper(trim(dw_importa_prodotti_orig.getitemstring(al_riga,"cod_prodotto")))
ls_des_prodotto = dw_importa_prodotti_orig.getitemstring(al_riga,"des_prodotto")
ls_cod_misura = upper(trim(dw_importa_prodotti_orig.getitemstring(al_riga,"cod_misura")))
ls_cod_deposito = "001"

INSERT INTO anag_depositi  
		( cod_azienda,   
		  cod_deposito,   
		  des_deposito,   
		  indirizzo,   
		  localita,   
		  frazione,   
		  cap,   
		  provincia,   
		  telefono,   
		  fax,   
		  telex,   
		  cod_documento,   
		  flag_blocco,   
		  data_blocco,   
		  numeratore_documento )  
VALUES ( :s_cs_xx.cod_azienda,   
		  '001',   
		  'Deposito generale',   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  null,   
		  'N',   
		  null,   
		  null )  ;



if isnull(ls_cod_misura) or ls_cod_misura = "" then
	ls_cod_misura = 'UN'

	insert into tab_misure  
					(cod_azienda,   
					 cod_misura,   
					 des_misura,   
					 fat_conversione,   
					 flag_blocco,   
					 data_blocco)  
	values      (:s_cs_xx.cod_azienda,   
					 'UN',   
					 'UN',   
					 1,   
					 'N',   
					 null);

end if

ls_cod_iva = "20"

st_1.text = ls_cod_prodotto + " | " + ls_des_prodotto

// Inserimento di un'unità di misura di default

insert into tab_ive  
            (cod_azienda,   
             cod_iva,   
             des_iva,   
             aliquota,   
             flag_calcolo,   
             flag_blocco,   
             data_blocco)  
values      (:s_cs_xx.cod_azienda,   
             :ls_cod_iva,   
             :ls_cod_iva,   
             0,   
             'S',   
             'N',   
             null);



select anag_prodotti.cod_prodotto
into   :ls_controllo  
from   anag_prodotti  
where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
       anag_prodotti.cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode = 0 then
   if cbx_aggiorna.checked then 
      update anag_prodotti  
      set    des_prodotto = :ls_des_prodotto,
				 cod_deposito = :ls_cod_deposito,
             cod_misura_mag = :ls_cod_misura,   
             cod_misura_acq = :ls_cod_misura,   
             cod_misura_ven = :ls_cod_misura,   
             cod_iva = :ls_cod_iva
      WHERE  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda AND  
             anag_prodotti.cod_prodotto = :ls_cod_prodotto;

      if sqlca.sqlcode = 0 then
			ll_conta = 1
         for ll_i = 1 to dw_importa_prodotti_dest.rowcount()
            if dw_importa_prodotti_dest.getitemstring(ll_i, "cod_prodotto") = ls_cod_prodotto then
               dw_importa_prodotti_dest.setitem(ll_i, "des_prodotto", ls_des_prodotto)
               dw_importa_prodotti_dest.setitem(ll_i, "cod_misura_mag", ls_cod_misura)
         	   al_riga_sel = ll_i
               exit
            end if
			next
      end if
		
      if sqlca.sqlcode = -1 then
         dw_errore_imp.insertrow(0)
    	   dw_errore_imp.setrow(dw_errore_imp.rowcount())
    		dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_prodotto + " | " + sqlca.sqlerrtext)
   	end if
   end if
elseif sqlca.sqlcode = 100 then
	
	ldt_oggi = datetime(today(), 00:00:00)
   insert into anag_prodotti  
               (cod_azienda,   
                cod_prodotto,   
                cod_comodo,   
                cod_barre,   
                des_prodotto,   
                data_creazione,   
                cod_deposito,   
                cod_ubicazione,   
                flag_decimali,   
                cod_cat_mer,   
                cod_responsabile,   
                cod_misura_mag,   
                cod_misura_peso,   
                peso_lordo,   
                peso_netto,   
                cod_misura_vol,   
                volume,   
                pezzi_collo,   
                flag_classe_abc,   
                flag_sotto_scorta,   
                flag_lifo,   
                saldo_quan_anno_prec,   
                saldo_quan_inizio_anno,   
                val_inizio_anno,   
                saldo_quan_ultima_chiusura,   
                prog_quan_entrata,   
                val_quan_entrata,   
                prog_quan_uscita,   
                val_quan_uscita,   
                prog_quan_acquistata,   
                val_quan_acquistata,   
                prog_quan_venduta,   
                val_quan_venduta,   
                quan_ordinata,   
                quan_impegnata,   
                quan_assegnata,   
                quan_in_spedizione,   
                quan_anticipi,   
                costo_standard,   
                costo_ultimo,   
                lotto_economico,   
                livello_riordino,   
                scorta_minima,   
                scorta_massima,   
                indice_rotazione,   
                consumo_medio,   
                cod_prodotto_alt,   
                cod_misura_acq,   
                fat_conversione_acq,   
                cod_fornitore,   
                cod_gruppo_acq,   
                prezzo_acquisto,   
                sconto_acquisto,   
                quan_minima,   
                inc_ordine,   
                quan_massima,   
                tempo_app,   
                cod_misura_ven,   
                fat_conversione_ven,   
                cod_gruppo_ven,   
                cod_iva,   
                prezzo_vendita,   
                sconto,   
                provvigione,   
                flag_blocco,   
                data_blocco,
					 cod_livello_prod_1,
					 cod_livello_prod_2,
					 cod_livello_prod_3,
					 cod_livello_prod_4,
					 cod_livello_prod_5,
					 cod_nomenclatura)  
   values      (:s_cs_xx.cod_azienda,   
                :ls_cod_prodotto,   
                null,   
                0,   
                :ls_des_prodotto,   
                :ldt_oggi,   
                null,   
                null,   
                 'S',   
                null,   
                null,   
                :ls_cod_misura,   
                null,   
                0,   
                0,   
                null,   
                0,   
                0,   
                'A',   
                'S',   
                'S',   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                null,   
                :ls_cod_misura,   
                1,   
                null,   
                null,   
                0,   
                0,   
                0,   
                0,   
                0,   
                0,   
                :ls_cod_misura,   
                1,   
                null,   
                :ls_cod_iva,   
                0,   
                0,   
                0,   
                'N',   
                null,
                null,
                null,
                null,
                null,
                null,
					 null);
						
   if sqlca.sqlcode = 0 then
	   ll_conta = 1
      dw_importa_prodotti_dest.insertrow(0)
      dw_importa_prodotti_dest.setrow(dw_importa_prodotti_dest.rowcount())
      dw_importa_prodotti_dest.setitem(dw_importa_prodotti_dest.getrow(), "cod_prodotto", ls_cod_prodotto)
      dw_importa_prodotti_dest.setitem(dw_importa_prodotti_dest.getrow(), "des_prodotto", ls_des_prodotto)
      dw_importa_prodotti_dest.setitem(dw_importa_prodotti_dest.getrow(), "cod_misura_mag", ls_cod_misura)
		al_riga_sel = dw_importa_prodotti_dest.getrow()	
   end if

   if sqlca.sqlcode = -1 then
      dw_errore_imp.insertrow(0)
 	   dw_errore_imp.setrow(dw_errore_imp.rowcount())
  		dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_prodotto + " | " + sqlca.sqlerrtext)
	end if
elseif sqlca.sqlcode = -1 then
   dw_errore_imp.insertrow(0)
   dw_errore_imp.setrow(dw_errore_imp.rowcount())
   dw_errore_imp.setitem(dw_errore_imp.getrow(), "errore", ls_cod_prodotto + " | " + sqlca.sqlerrtext)
end if

return ll_conta
end function

event pc_setwindow;call super::pc_setwindow;string ls_dsn

ls_dsn = f_parametri_imp(1)

i_sqlorig = CREATE transaction

i_sqlorig.servername = ""
i_sqlorig.logid = ""
i_sqlorig.logpass = ""
i_sqlorig.dbms = "Odbc"
i_sqlorig.database = ""
i_sqlorig.userid = ""
i_sqlorig.dbpass = ""
i_sqlorig.dbparm = "Connectstring='DSN=" + ls_dsn + "'"

f_po_connect(i_sqlorig, TRUE)

set_w_options(c_noenablepopup)

windowobject lw_oggetti[]

lw_oggetti[1] = dw_importa_prodotti_orig
dw_folder.fu_assigntab(1, "Origine Dati", lw_oggetti[])
lw_oggetti[1] = dw_importa_prodotti_dest
dw_folder.fu_assigntab(2, "Destinazione Dati", lw_oggetti[])
lw_oggetti[1] = dw_errore_imp
dw_folder.fu_assigntab(3, "Errori", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_importa_prodotti_orig.set_dw_options(i_sqlorig, &
                                        pcca.null_object, &
                                        c_multiselect + &
												    c_nonew + &
												    c_nomodify + &
													 c_nodelete + &
													 c_disablecc + &
													 c_disableccinsert, &
                                        c_default)

dw_importa_prodotti_dest.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_multiselect + &
													 c_nonew + &
													 c_nomodify + &
													 c_nodelete + &
												  	 c_disablecc + &
													 c_disableccinsert, &
                                        c_viewmodeblack)
													 
dw_errore_imp.set_dw_options(sqlca, &
                             pcca.null_object, &
									  c_nonew + &
									  c_nomodify + &
									  c_nodelete + &
									  c_disablecc + &
									  c_disableccinsert, &
                             c_viewmodeblack)
													 

save_on_close(c_socnosave)

end event

on w_importa_prodotti.create
int iCurrent
call w_cs_xx_principale::create
this.cb_importa=create cb_importa
this.cb_1=create cb_1
this.cbx_aggiorna=create cbx_aggiorna
this.st_1=create st_1
this.gb_1=create gb_1
this.rb_totale=create rb_totale
this.rb_selezionati=create rb_selezionati
this.dw_errore_imp=create dw_errore_imp
this.dw_importa_prodotti_orig=create dw_importa_prodotti_orig
this.dw_folder=create dw_folder
this.dw_importa_prodotti_dest=create dw_importa_prodotti_dest
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_importa
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cbx_aggiorna
this.Control[iCurrent+4]=st_1
this.Control[iCurrent+5]=gb_1
this.Control[iCurrent+6]=rb_totale
this.Control[iCurrent+7]=rb_selezionati
this.Control[iCurrent+8]=dw_errore_imp
this.Control[iCurrent+9]=dw_importa_prodotti_orig
this.Control[iCurrent+10]=dw_folder
this.Control[iCurrent+11]=dw_importa_prodotti_dest
end on

on w_importa_prodotti.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_importa)
destroy(this.cb_1)
destroy(this.cbx_aggiorna)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.rb_totale)
destroy(this.rb_selezionati)
destroy(this.dw_errore_imp)
destroy(this.dw_importa_prodotti_orig)
destroy(this.dw_folder)
destroy(this.dw_importa_prodotti_dest)
end on

event close;call super::close;commit;
end event

type cb_importa from uo_cb_ok within w_importa_prodotti
event clicked pbm_bnclicked
int X=2263
int Y=1301
int Width=366
int Height=81
int TabOrder=60
string Text="&Importa"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long ll_riga, ll_errore, ll_selected[], ll_i, ll_conta, ll_totale, ll_riga_sel[], al_riga_sel


setpointer(hourglass!)

ll_totale = 0
if rb_totale.checked then
   for ll_riga = 1 to dw_importa_prodotti_orig.rowcount()
      ll_conta = wf_importa(ll_riga, al_riga_sel)
      ll_totale = ll_totale + ll_conta
      if ll_totale > 0 then ll_riga_sel[ll_totale] = al_riga_sel
      if mod(ll_riga,10) = 0 then 
         commit;
      end if
   next
   st_1.text = "Importazione Terminata. Record Letti: " + string(dw_importa_prodotti_orig.rowcount()) + &
               " Record Scritti: " + string(ll_totale)
else
   dw_importa_prodotti_orig.get_selected_rows(ll_selected[])
   ll_i = 1
	for ll_riga = 1 to upperbound(ll_selected)
      ll_conta = wf_importa(ll_selected[ll_riga], al_riga_sel)
      ll_totale = ll_totale + ll_conta
      if ll_totale > 0 then ll_riga_sel[ll_totale] = al_riga_sel
      if mod(ll_riga,10) = 0 then 
         commit;
      end if
   next
   st_1.text = "Importazione Terminata. Record Letti: " + string(upperbound(ll_selected)) + &
               " Record Scritti: " + string(ll_totale)
end if

if ll_errore < 0 then
   pcca.error = c_fatal
end if

dw_importa_prodotti_dest.set_selected_rows(ll_totale, &
                                          ll_riga_sel[], &
                                          c_ignorechanges, &
                                          c_refreshchildren, &
                                          c_refreshview)

dw_importa_prodotti_dest.setsort("cod_prodotto A")
dw_importa_prodotti_dest.sort() 

setpointer(arrow!)
end event

type cb_1 from uo_cb_close within w_importa_prodotti
int X=2652
int Y=1301
int Width=366
int Height=81
int TabOrder=50
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_aggiorna from checkbox within w_importa_prodotti
int X=23
int Y=1301
int Width=823
int Height=61
boolean BringToTop=true
string Text="Aggiornare Dati già Importati"
BorderStyle BorderStyle=StyleLowered!
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_importa_prodotti
int X=23
int Y=961
int Width=2972
int Height=61
boolean Enabled=false
boolean BringToTop=true
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_importa_prodotti
int X=23
int Y=1061
int Width=846
int Height=221
int TabOrder=70
string Text="Tipo Importazione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_totale from radiobutton within w_importa_prodotti
int X=46
int Y=1121
int Width=801
int Height=61
boolean BringToTop=true
string Text="Totale"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_selezionati from radiobutton within w_importa_prodotti
int X=46
int Y=1201
int Width=801
int Height=61
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_errore_imp from uo_cs_xx_dw within w_importa_prodotti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2949
int Height=781
int TabOrder=10
string DataObject="d_errore_imp"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_importa_prodotti_orig from uo_cs_xx_dw within w_importa_prodotti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2949
int Height=781
int TabOrder=30
string DataObject="d_importa_prodotti_orig"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_importa_prodotti
int X=1
int Y=21
int Width=3018
int Height=921
int TabOrder=20
end type

type dw_importa_prodotti_dest from uo_cs_xx_dw within w_importa_prodotti
event pcd_retrieve pbm_custom60
int X=23
int Y=121
int Width=2949
int Height=781
int TabOrder=40
string DataObject="d_importa_prodotti_dest"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

