﻿$PBExportHeader$w_kazzarola.srw
forward
global type w_kazzarola from window
end type
type st_3 from statictext within w_kazzarola
end type
type st_2 from statictext within w_kazzarola
end type
type cb_4 from commandbutton within w_kazzarola
end type
type st_1 from statictext within w_kazzarola
end type
type sle_1 from singlelineedit within w_kazzarola
end type
type cb_3 from commandbutton within w_kazzarola
end type
type st_stato from statictext within w_kazzarola
end type
type cb_2 from commandbutton within w_kazzarola
end type
type cb_1 from commandbutton within w_kazzarola
end type
type dw_origine from datawindow within w_kazzarola
end type
end forward

global type w_kazzarola from window
integer width = 4210
integer height = 1996
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_3 st_3
st_2 st_2
cb_4 cb_4
st_1 st_1
sle_1 sle_1
cb_3 cb_3
st_stato st_stato
cb_2 cb_2
cb_1 cb_1
dw_origine dw_origine
end type
global w_kazzarola w_kazzarola

type variables
transaction i_tran
end variables

on w_kazzarola.create
this.st_3=create st_3
this.st_2=create st_2
this.cb_4=create cb_4
this.st_1=create st_1
this.sle_1=create sle_1
this.cb_3=create cb_3
this.st_stato=create st_stato
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_origine=create dw_origine
this.Control[]={this.st_3,&
this.st_2,&
this.cb_4,&
this.st_1,&
this.sle_1,&
this.cb_3,&
this.st_stato,&
this.cb_2,&
this.cb_1,&
this.dw_origine}
end on

on w_kazzarola.destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_4)
destroy(this.st_1)
destroy(this.sle_1)
destroy(this.cb_3)
destroy(this.st_stato)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_origine)
end on

type st_3 from statictext within w_kazzarola
integer x = 549
integer y = 1636
integer width = 1765
integer height = 64
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "PER IL RIPRISTINO QUANTITA~' IL DB ORGINE DEVE ESSERE IL DB DI PROVA"
boolean focusrectangle = false
end type

type st_2 from statictext within w_kazzarola
integer x = 453
integer y = 1400
integer width = 1422
integer height = 128
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "PER LA CANCELLAZIONE IL DB ORIGINE DEVE ESSERE QUELLO UFFICIALE"
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_kazzarola
integer x = 1906
integer y = 1116
integer width = 402
integer height = 84
integer taborder = 40
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "connetti"
end type

event clicked;st_stato.text = "connessione al db di origine in corso"

i_tran = create transaction

if f_po_getconnectinfo("", "database_2" , i_tran) <> 0 then
   g_mb.messagebox("","Errore in caricamento dati sezione registro 2")
	close(parent)
end if

i_tran.Database = sle_1.text

if f_po_connect(i_tran, true) <> 0 then
   g_mb.messagebox("","Errore durante la connect" + i_tran.sqlerrtext)
	close(parent)
end if

st_stato.text = "caricamento dati ...."

dw_origine.settransobject(i_tran)
dw_origine.retrieve()


st_stato.text = "pronto !"

end event

type st_1 from statictext within w_kazzarola
integer x = 736
integer y = 1120
integer width = 393
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "DB ORIGINE:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_kazzarola
integer x = 1147
integer y = 1108
integer width = 741
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "test_ptenda"
borderstyle borderstyle = stylelowered!
end type

type cb_3 from commandbutton within w_kazzarola
integer x = 3721
integer y = 1108
integer width = 425
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Lista"
end type

event clicked;dw_origine.print()
end event

type st_stato from statictext within w_kazzarola
integer x = 23
integer y = 1796
integer width = 4142
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_kazzarola
integer x = 23
integer y = 1604
integer width = 507
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ripristina quantità"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione
dec{4} ld_quan_movimento


declare cu_movimenti cursor  for
select anno_registrazione, num_registrazione, quan_movimento
from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = 2006 
order by num_registrazione
using i_tran;


open cu_movimenti;

do while true
	fetch cu_movimenti into :ll_anno_registrazione, :ll_num_registrazione, :ld_quan_movimento;
	
	if i_tran.sqlcode = 100 then exit
	
	if i_tran.sqlcode = -1 then
		g_mb.messagebox("fetch",i_tran.sqlerrtext)
		rollback;
		return
	end if
	
	yield()
	
	st_stato.text = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
	
	update mov_magazzino
	set    quan_movimento = :ld_quan_movimento
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione
   using sqlca;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("update",sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit using sqlca;
	
loop

close cu_movimenti;

rollback using i_tran;
end event

type cb_1 from commandbutton within w_kazzarola
integer x = 27
integer y = 1396
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "cancella"
end type

event clicked;long					ll_i, ll_ret
uo_magazzino		luo_mag

for ll_i = 1 to dw_origine.rowcount()

	luo_mag = create uo_magazzino
	ll_ret = luo_mag.uof_elimina_movimenti(dw_origine.getitemnumber(ll_i,"anno_registrazione"), dw_origine.getitemnumber(ll_i,"num_registrazione"), true)
	destroy luo_mag
	
	if ll_ret=0 then
		dw_origine.setitem(ll_i, "referenza", "Cancellato OK")
		commit using sqlca;
	else
		dw_origine.setitem(ll_i, "referenza", "Errore in cancellazione")
		rollback using sqlca;
	end if
	
next

end event

type dw_origine from datawindow within w_kazzarola
integer x = 23
integer y = 20
integer width = 4137
integer height = 1068
integer taborder = 10
string title = "none"
string dataobject = "d_movimenti_da_cancellare"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

