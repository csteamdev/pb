﻿$PBExportHeader$w_param_premi_produzione.srw
forward
global type w_param_premi_produzione from w_cs_xx_principale
end type
type cb_regole_aggiuntive from commandbutton within w_param_premi_produzione
end type
type dw_impps_isfas from uo_cs_xx_dw within w_param_premi_produzione
end type
type em_anno from editmask within w_param_premi_produzione
end type
type cb_duplica from commandbutton within w_param_premi_produzione
end type
type cb_causali from commandbutton within w_param_premi_produzione
end type
type cb_perc_fatt_rep from commandbutton within w_param_premi_produzione
end type
type cb_premi_periodo from commandbutton within w_param_premi_produzione
end type
type cb_premi_aree from commandbutton within w_param_premi_produzione
end type
type dw_impp_isfa from uo_cs_xx_dw within w_param_premi_produzione
end type
type dw_premi from uo_cs_xx_dw within w_param_premi_produzione
end type
type r_1 from rectangle within w_param_premi_produzione
end type
type r_2 from rectangle within w_param_premi_produzione
end type
type r_3 from rectangle within w_param_premi_produzione
end type
type r_4 from rectangle within w_param_premi_produzione
end type
type r_5 from rectangle within w_param_premi_produzione
end type
type r_6 from rectangle within w_param_premi_produzione
end type
end forward

global type w_param_premi_produzione from w_cs_xx_principale
integer width = 4329
integer height = 2148
string title = "Impostazioni Premi Annuali"
cb_regole_aggiuntive cb_regole_aggiuntive
dw_impps_isfas dw_impps_isfas
em_anno em_anno
cb_duplica cb_duplica
cb_causali cb_causali
cb_perc_fatt_rep cb_perc_fatt_rep
cb_premi_periodo cb_premi_periodo
cb_premi_aree cb_premi_aree
dw_impp_isfa dw_impp_isfa
dw_premi dw_premi
r_1 r_1
r_2 r_2
r_3 r_3
r_4 r_4
r_5 r_5
r_6 r_6
end type
global w_param_premi_produzione w_param_premi_produzione

on w_param_premi_produzione.create
int iCurrent
call super::create
this.cb_regole_aggiuntive=create cb_regole_aggiuntive
this.dw_impps_isfas=create dw_impps_isfas
this.em_anno=create em_anno
this.cb_duplica=create cb_duplica
this.cb_causali=create cb_causali
this.cb_perc_fatt_rep=create cb_perc_fatt_rep
this.cb_premi_periodo=create cb_premi_periodo
this.cb_premi_aree=create cb_premi_aree
this.dw_impp_isfa=create dw_impp_isfa
this.dw_premi=create dw_premi
this.r_1=create r_1
this.r_2=create r_2
this.r_3=create r_3
this.r_4=create r_4
this.r_5=create r_5
this.r_6=create r_6
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_regole_aggiuntive
this.Control[iCurrent+2]=this.dw_impps_isfas
this.Control[iCurrent+3]=this.em_anno
this.Control[iCurrent+4]=this.cb_duplica
this.Control[iCurrent+5]=this.cb_causali
this.Control[iCurrent+6]=this.cb_perc_fatt_rep
this.Control[iCurrent+7]=this.cb_premi_periodo
this.Control[iCurrent+8]=this.cb_premi_aree
this.Control[iCurrent+9]=this.dw_impp_isfa
this.Control[iCurrent+10]=this.dw_premi
this.Control[iCurrent+11]=this.r_1
this.Control[iCurrent+12]=this.r_2
this.Control[iCurrent+13]=this.r_3
this.Control[iCurrent+14]=this.r_4
this.Control[iCurrent+15]=this.r_5
this.Control[iCurrent+16]=this.r_6
end on

on w_param_premi_produzione.destroy
call super::destroy
destroy(this.cb_regole_aggiuntive)
destroy(this.dw_impps_isfas)
destroy(this.em_anno)
destroy(this.cb_duplica)
destroy(this.cb_causali)
destroy(this.cb_perc_fatt_rep)
destroy(this.cb_premi_periodo)
destroy(this.cb_premi_aree)
destroy(this.dw_impp_isfa)
destroy(this.dw_premi)
destroy(this.r_1)
destroy(this.r_2)
destroy(this.r_3)
destroy(this.r_4)
destroy(this.r_5)
destroy(this.r_6)
end on

event pc_setwindow;call super::pc_setwindow;

dw_premi.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_impp_isfa.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_impps_isfas.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

dw_premi.setrowfocusindicator(Hand!)

iuo_dw_main = dw_premi


em_anno.text = string(year(today()))


end event

type cb_regole_aggiuntive from commandbutton within w_param_premi_produzione
integer x = 2697
integer y = 1932
integer width = 475
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Regole Aggiunt."
end type

event clicked;window_open(w_premi_regole_add, -1)

end event

type dw_impps_isfas from uo_cs_xx_dw within w_param_premi_produzione
integer x = 2185
integer y = 48
integer width = 1001
integer height = 1756
integer taborder = 50
boolean titlebar = true
string title = "Scostamento da VFSA (stabilimento)"
string dataobject = "d_impps_isfas"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
	
else
	
	ll_errore = Find("isfas = 100", 1, RowCount())
	if ll_errore>0 then
		scrolltorow(ll_errore)
		selectrow(0, false)
		selectrow(ll_errore, true)
	end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
decimal ld_valore

for ll_index = 1 to rowcount()
	ld_valore = getitemdecimal(ll_index, "isfas")
	
	if isnull(ld_valore) or ld_valore<=0 then
		g_mb.error("Attenzione", "Inserire un valore ISFAS maggiore di 0!")
		
		pcca.error = c_fatal
		return
	end if
	
	ld_valore = getitemdecimal(ll_index, "impps")
	
	if isnull(ld_valore) or ld_valore<0 then
		g_mb.error("Attenzione", "Inserire un valore IMPPS maggiore o uguale a 0!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

type em_anno from editmask within w_param_premi_produzione
integer x = 489
integer y = 48
integer width = 402
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####"
boolean spin = true
double increment = 1
string minmax = "2000~~9999"
end type

type cb_duplica from commandbutton within w_param_premi_produzione
integer x = 69
integer y = 48
integer width = 402
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica Anno"
end type

event clicked;string ls_anno
long ll_row, ll_anno_old, ll_anno_new,ll_count

ll_row = dw_premi.getrow()

if ll_row>0 then
	
	//anno da duplicare
	ll_anno_old = dw_premi.getitemnumber(ll_row, "anno_riferimento")
	if ll_anno_old > 0 then
	else
		return
	end if
else
	
end if

//da inserire ex novo
ls_anno = em_anno.text

if isnull(ls_anno) or ls_anno="" then
	g_mb.error("Inserire il valore del nuovo anno!")
	return
end if

if isnumber(ls_anno) then
	ll_anno_new = long(ls_anno)
else
	g_mb.error("Inserire un valore numerico per il nuovo anno!")
	return
end if

//verifica se già non esiste
select count(*)
into :ll_count
from premi
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_new;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in controllo esistenza anno: "+sqlca.sqlerrtext)
	return
end if

if ll_count>0 then
	g_mb.error("L'anno "+string(ll_anno_new)+" risulta già presente!")
	return
else
	//prosegui
end if

if g_mb.confirm("Duplicare i dati dell'anno "+string(ll_anno_old)+" nel nuovo anno "+string(ll_anno_new)+"?") then
else
	//uscita
	return
end if

//premi ------------------------------------------------------------------------
insert into premi  
         ( cod_azienda,   
           anno_riferimento,   
           flag_gen_vp,   
           flag_feb_vp,   
           flag_mar_vp,   
           flag_apr_vp,   
           flag_mag_vp,   
           flag_giu_vp,   
           flag_lug_vp,   
           flag_ago_vp,   
           flag_set_vp,   
           flag_ott_vp,   
           flag_nov_vp,   
           flag_dic_vp,   
           flag_gen_vio,   
           flag_feb_vio,   
           flag_mar_vio,   
           flag_apr_vio,   
           flag_mag_vio,   
           flag_giu_vio,   
           flag_lug_vio,   
           flag_ago_vio,   
           flag_set_vio,   
           flag_ott_vio,   
           flag_nov_vio,   
           flag_dic_vio,   
           flag_gen_via,   
           flag_feb_via,   
           flag_mar_via,   
           flag_apr_via,   
           flag_mag_via,   
           flag_giu_via,   
           flag_lug_via,   
           flag_ago_via,   
           flag_set_via,   
           flag_ott_via,   
           flag_nov_via,   
           flag_dic_via )  
		select  cod_azienda,   
				  :ll_anno_new,   
				  flag_gen_vp,   
				  flag_feb_vp,   
				  flag_mar_vp,   
				  flag_apr_vp,   
				  flag_mag_vp,   
				  flag_giu_vp,   
				  flag_lug_vp,   
				  flag_ago_vp,   
				  flag_set_vp,   
				  flag_ott_vp,   
				  flag_nov_vp,   
				  flag_dic_vp,   
				  flag_gen_vio,   
				  flag_feb_vio,   
				  flag_mar_vio,   
				  flag_apr_vio,   
				  flag_mag_vio,   
				  flag_giu_vio,   
				  flag_lug_vio,   
				  flag_ago_vio,   
				  flag_set_vio,   
				  flag_ott_vio,   
				  flag_nov_vio,   
				  flag_dic_vio,   
				  flag_gen_via,   
				  flag_feb_via,   
				  flag_mar_via,   
				  flag_apr_via,   
				  flag_mag_via,   
				  flag_giu_via,   
				  flag_lug_via,   
				  flag_ago_via,   
				  flag_set_via,   
				  flag_ott_via,   
				  flag_nov_via,   
				  flag_dic_via
		from premi
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_old;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in inserimento in tabella premi: "+sqlca.sqlerrtext)
	rollback;
	return
end if


//premi_aree -------------------------------------------------------------------
insert into premi_aree  
         ( cod_azienda,   
           anno_riferimento,   
           cod_centro_costo,   
           vpr,   
           p,   
           vhs_area,   
           bio,   
           via_area,   
           bia,   
           ipia_1,   
           ipia_2,   
           ipia_3,   
           flag_1,   
           flag_2,   
           flag_3,   
           flag_4,   
           flag_5,   
           flag_6,   
           flag_7,   
           flag_8,   
           flag_9,   
           flag_10,   
           flag_11,   
           flag_12 )  
		select	cod_azienda,   
				  :ll_anno_new,   
				  cod_centro_costo,   
				  vpr,   
				  p,   
				  vhs_area,   
				  bio,   
				  via_area,   
				  bia,   
				  ipia_1,   
				  ipia_2,   
				  ipia_3,   
				  flag_1,   
				  flag_2,   
				  flag_3,   
				  flag_4,   
				  flag_5,   
				  flag_6,   
				  flag_7,   
				  flag_8,   
				  flag_9,   
				  flag_10,   
				  flag_11,   
				  flag_12
		from premi_aree
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_old;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in inserimento in tabella premi_aree: "+sqlca.sqlerrtext)
	rollback;
	return
end if


//premi_periodo ----------------------------------------------------------------
insert into premi_periodo  
         ( cod_azienda,   
           anno_riferimento,   
           progressivo,   
           data_dal,   
           data_al,   
           vfla,   
           t,   
           is_area,   
           ix_area )  
		select  cod_azienda,   
				  :ll_anno_new,   
				  progressivo,   
				  data_dal,   
				  data_al,   
				  vfla,   
				  t,   
				  is_area,   
				  ix_area
		from premi_periodo
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_old;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in inserimento in tabella premi_periodo: "+sqlca.sqlerrtext)
	rollback;
	return
end if


//premi_periodo_aree------------------------------------------------------------
 insert into premi_periodo_aree  
         ( cod_azienda,   
           anno_riferimento,   
           progressivo,   
           cod_centro_costo,   
           rpr_area )  
 		select  cod_azienda,   
				  :ll_anno_new,   
				  progressivo,   
				  cod_centro_costo,   
				  rpr_area
		from premi_periodo_aree
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_old;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in inserimento in tabella premi_periodo_aree: "+sqlca.sqlerrtext)
	rollback;
	return
end if

//premi_periodo_depositi------------------------------------------------------------
 insert into premi_periodo_depositi  
         ( cod_azienda,   
           anno_riferimento,   
           progressivo,   
           cod_deposito,   
           vfsa_deposito )  
 		select  cod_azienda,   
				  :ll_anno_new,   
				  progressivo,   
				  cod_deposito,   
				  vfsa_deposito
		from premi_periodo_depositi
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_old;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in inserimento in tabella premi_periodo_depositi: "+sqlca.sqlerrtext)
	rollback;
	return
end if

//se arrivi fin qui fai il commit, e ripresenta i dati
commit;
g_mb.error("Duplicazione effettuata con successo!")


dw_premi.change_dw_current( )
parent.postevent("pc_retrieve")
end event

type cb_causali from commandbutton within w_param_premi_produzione
integer x = 2181
integer y = 1888
integer width = 475
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Causali"
end type

event clicked;window_open(w_premi_causali, -1)
end event

type cb_perc_fatt_rep from commandbutton within w_param_premi_produzione
integer x = 2697
integer y = 1848
integer width = 475
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Regole Produz."
end type

event clicked;window_open(w_premi_prodotti_regole, -1)

end event

type cb_premi_periodo from commandbutton within w_param_premi_produzione
integer x = 745
integer y = 1888
integer width = 658
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Periodi (VFLA-T-IS-IX)"
end type

event clicked;window_open_parm(w_premi_periodo, -1, dw_premi)
end event

type cb_premi_aree from commandbutton within w_param_premi_produzione
integer x = 69
integer y = 1888
integer width = 658
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "VPR / BIO / BIA"
end type

event clicked;window_open_parm(w_premi_aree, -1, dw_premi)
end event

type dw_impp_isfa from uo_cs_xx_dw within w_param_premi_produzione
integer x = 3241
integer y = 48
integer width = 1001
integer height = 1956
integer taborder = 20
boolean titlebar = true
string title = "Scostamento da VFTA (totale)"
string dataobject = "d_impp_isfa"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
	
else

	ll_errore = Find("isfa = 100", 1, RowCount())
	if ll_errore>0 then
		scrolltorow(ll_errore)
		selectrow(0, false)
		selectrow(ll_errore, true)
	end if
	
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
decimal ld_valore

for ll_index = 1 to rowcount()
	ld_valore = getitemdecimal(ll_index, "isfa")
	
	if isnull(ld_valore) or ld_valore<=0 then
		g_mb.error("Attenzione", "Inserire un valore ISFAT maggiore di 0!")
		
		pcca.error = c_fatal
		return
	end if
	
	ld_valore = getitemdecimal(ll_index, "impp")
	
	if isnull(ld_valore) or ld_valore<0 then
		g_mb.error("Attenzione", "Inserire un valore IMPPT maggiore o uguale a 0!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

type dw_premi from uo_cs_xx_dw within w_param_premi_produzione
integer x = 50
integer y = 200
integer width = 2089
integer height = 1668
integer taborder = 10
string dataobject = "d_param_premi"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_anno, ll_index

for ll_index = 1 to rowcount()
	ll_anno = getitemnumber(ll_index, "anno_riferimento")
	
	if isnull(ll_anno) or ll_anno<=2000 then
		g_mb.error("Attenzione", "Inserire un anno di riferimento maggiore o uguale a 2000!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

event pcd_new;call super::pcd_new;cb_premi_aree.enabled = false
cb_premi_periodo.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_premi_aree.enabled = false
cb_premi_periodo.enabled = false
end event

event pcd_delete;call super::pcd_delete;cb_premi_aree.enabled = false
cb_premi_periodo.enabled = false
end event

event pcd_view;call super::pcd_view;cb_premi_aree.enabled = true
cb_premi_periodo.enabled = true
end event

type r_1 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 180
integer width = 2117
integer height = 1840
end type

type r_2 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 3223
integer y = 24
integer width = 1038
integer height = 1996
end type

type r_3 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2167
integer y = 1836
integer width = 512
integer height = 184
end type

type r_4 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 37
integer y = 24
integer width = 2112
integer height = 140
end type

type r_5 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2167
integer y = 24
integer width = 1038
integer height = 1796
end type

type r_6 from rectangle within w_param_premi_produzione
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2688
integer y = 1836
integer width = 517
integer height = 184
end type

