﻿$PBExportHeader$w_premi_aree.srw
forward
global type w_premi_aree from w_cs_xx_principale
end type
type dw_detail from uo_cs_xx_dw within w_premi_aree
end type
type dw_lista from uo_cs_xx_dw within w_premi_aree
end type
end forward

global type w_premi_aree from w_cs_xx_principale
integer width = 2519
integer height = 2164
string title = "Configurazione Premi - Aree Aziendali"
dw_detail dw_detail
dw_lista dw_lista
end type
global w_premi_aree w_premi_aree

on w_premi_aree.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_lista
end on

on w_premi_aree.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("anno_riferimento")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)
												
dw_detail.set_dw_options(	sqlca, &
									dw_lista, &
									c_sharedata + c_scrollparent, &
									c_default)
									
iuo_dw_main = dw_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_lista, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_detail, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_detail from uo_cs_xx_dw within w_premi_aree
integer x = 27
integer y = 628
integer width = 2405
integer height = 1412
integer taborder = 20
string dataobject = "d_premi_aree_1"
borderstyle borderstyle = stylelowered!
end type

type dw_lista from uo_cs_xx_dw within w_premi_aree
integer x = 27
integer y = 24
integer width = 2405
integer height = 584
integer taborder = 20
string dataobject = "d_premi_aree_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_riferimento

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")

parent.title = "Anno rif. "+string(ll_anno_riferimento)+" - Configurazione Premi - C.di Costo"

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_riferimento)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_riferimento

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_riferimento")) or &
      this.getitemnumber(ll_i, "anno_riferimento") = 0 then
      this.setitem(ll_i, "anno_riferimento", ll_anno_riferimento)
   end if
next

end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_centro_costo
long ll_index

for ll_index = 1 to rowcount()
	
	ls_cod_centro_costo = getitemstring(ll_index, "cod_centro_costo")
	if isnull(ls_cod_centro_costo) or ls_cod_centro_costo="" then
		g_mb.error("Attenzione", "Selezionare il centro di costo!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

event pcd_new;call super::pcd_new;long ll_anno_riferimento

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")

setitem(getrow(), "anno_riferimento", ll_anno_riferimento)
end event

