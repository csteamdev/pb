﻿$PBExportHeader$w_premi_calcolo.srw
forward
global type w_premi_calcolo from w_cs_xx_principale
end type
type st_msg from statictext within w_premi_calcolo
end type
type st_1 from statictext within w_premi_calcolo
end type
type dw_selezione from u_dw_search within w_premi_calcolo
end type
type dw_sel_dipen from datawindow within w_premi_calcolo
end type
type dw_sel_cdc from datawindow within w_premi_calcolo
end type
type cb_elabora from commandbutton within w_premi_calcolo
end type
end forward

global type w_premi_calcolo from w_cs_xx_principale
integer width = 2981
integer height = 2392
string title = "Calcolo premi Dipendenti"
boolean resizable = false
event ue_set_periodo ( )
st_msg st_msg
st_1 st_1
dw_selezione dw_selezione
dw_sel_dipen dw_sel_dipen
dw_sel_cdc dw_sel_cdc
cb_elabora cb_elabora
end type
global w_premi_calcolo w_premi_calcolo

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables
string is_azienda_db_presenze = ""

//valore della colonna gruppo in tabella dipen, per fare in modo che le ore
//ordinarie, starordinarie, flessibili e diaria non vengano conteggiate
//nel calcolo di OL reparto e quindi RP reparto
string is_escludi_da_rp = ""			//"QUADRO"
end variables

forward prototypes
public function integer wf_sel_desel_dipendenti (string fs_cdc, string fs_sel)
public function integer wf_retrieve_cdc_dipen (ref string fs_errore)
public function integer wf_elabora (ref string fs_errore)
public function integer wf_calcola_vpt (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref decimal fd_vflp, ref string fs_errore)
public function integer wf_calcola_via (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref string fs_errore)
public function integer wf_calcola_vio (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref string fs_errore)
end prototypes

event ue_set_periodo();datetime ldt_inizio

ldt_inizio = datetime(date(today()), 00:00:00)

dw_selezione.setitem(1, "mese_anno", ldt_inizio)

setnull(ldt_inizio)
dw_selezione.setitem(1, "data_elaborazione", ldt_inizio)
end event

public function integer wf_sel_desel_dipendenti (string fs_cdc, string fs_sel);long ll_index
string ls_cdc

for ll_index=1 to dw_sel_dipen.rowcount()
	
	if fs_cdc = "SEL-DESEL-TUTTI" then
		//indipendentemente dal centro di costo estendi l'azione a tutti
		dw_sel_dipen.setitem(ll_index, "sel", fs_sel)
		
	else
		//estendi all'azione solo ai dipendenti del centro di costo passato
		ls_cdc = dw_sel_dipen.getitemstring(ll_index, "cenco")
		
		if ls_cdc=fs_cdc then
			dw_sel_dipen.setitem(ll_index, "sel", fs_sel)
		end if
		
	end if
	
next

return 1
end function

public function integer wf_retrieve_cdc_dipen (ref string fs_errore);string ls_odbc_presenze, ls_uid_presenze, ls_pwd_presenze, ls_dbparm
transaction ltran_presenze
long ll_index

//-------------------------------------------------------
select stringa
into :ls_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro DBP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or ls_odbc_presenze="" or isnull(ls_odbc_presenze) then
	fs_errore = "Parametro DBP (odbc DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if

//legge dal parametro aziendale UTP (UID database presenze)
select stringa
into :ls_uid_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='UTP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro UTP: "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_uid_presenze) then ls_uid_presenze = ""

//legge dal parametro aziendalePAP (password database presenze)
select stringa
into :ls_pwd_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='PAP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro PAP: "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_pwd_presenze) then ls_pwd_presenze = ""


//recupero il codice azienda database presenze ------------------------------------
select stringa
into :is_azienda_db_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='ADP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro ADP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or is_azienda_db_presenze="" or isnull(is_azienda_db_presenze) then
	fs_errore = "Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if
//-------------------------------------------------------

ltran_presenze = create transaction

ltran_presenze.dbms = "ODBC"
ltran_presenze.autocommit = false

//ltran_presenze.dbparm = "Connectstring='DSN="+ls_odbc_presenze+"',DisableBind=1"
ls_dbparm = "ConnectString='DSN="+ls_odbc_presenze

if ls_uid_presenze<>"" then
	ls_dbparm += ";UID="+ls_uid_presenze
end if

if ls_pwd_presenze <> "" then
	ls_dbparm += ";PWD="+ls_pwd_presenze
end if

ls_dbparm+= "',CommitOnDisconnect='No',DisableBind=1"

ltran_presenze.dbparm = ls_dbparm

if f_po_connect(ltran_presenze, true) <> 0 then
	fs_errore = "Errore in connessione DB presenze: "+ltran_presenze.sqlerrtext
	
   return -1
end if

//-------------------------------------------------------
//NOTA
//aggiungere eventuali filtri per non elaborare matricole che non hanno diritto al premio
//flag_diritto_a_premi='S'
dw_sel_cdc.settransobject(ltran_presenze)
dw_sel_dipen.settransobject(ltran_presenze)

dw_sel_cdc.retrieve(is_azienda_db_presenze)
dw_sel_dipen.retrieve(is_azienda_db_presenze)

for ll_index = 1 to dw_sel_dipen.rowcount()
	dw_sel_dipen.setitem(ll_index, "gruppo_escludi", is_escludi_da_rp)
next


destroy ltran_presenze;

//-------------------------------------------------------

return 1
end function

public function integer wf_elabora (ref string fs_errore);string ls_flag_vpt, ls_flag_vio, ls_flag_via, ls_sel, ls_matricola, ls_cdc
datetime ldt_inizio_calcolo, ldt_inizio, ldt_fine, ldt_null,ldt_date_v[], ldt_data_elaborazione
long ll_index, ll_tot, ll_count, ll_count_tot, ll_anno_rif, ll_mese_rif
boolean lb_almeno_uno = false, lb_righe_fatture
decimal ld_vflp
uo_premio_produzione luo_premio
string ls_sql_UFF_MAG_AUT[],ls_sql_UFF_MAG_AUT_descriz[], ls_cdc_VFLP

dw_selezione.accepttext()
dw_sel_dipen.accepttext()
dw_sel_cdc.accepttext()

st_msg.text = "Inizio elaborazione ..."

//-----------------------
ls_flag_vpt = dw_selezione.getitemstring(1, "flag_calcola_vpt")
ls_flag_vio = dw_selezione.getitemstring(1, "flag_calcola_vio")
ls_flag_via = dw_selezione.getitemstring(1, "flag_calcola_via")

ldt_data_elaborazione = dw_selezione.getitemdatetime(1, "data_elaborazione")
if year(date(ldt_data_elaborazione)) <= 2000 then setnull(ldt_data_elaborazione)

if ls_flag_vpt<>"S" and ls_flag_vio<>"S" and ls_flag_via<>"S" then
	g_mb.error("Selezionare almeno un premio da calcolare!")
	
	return 0
end if

ldt_inizio_calcolo = dw_selezione.getitemdatetime(1, "mese_anno")
if isnull(ldt_inizio_calcolo) or year(date(ldt_inizio_calcolo)) < 2000 then
	g_mb.error("Selezionare la data (Mese/Anno) di calcolo!")
	
	return 0
end if


//-----------------------
ll_tot = dw_sel_dipen.rowcount()
ll_count_tot = 0
ll_count = 0
for ll_index=1 to ll_tot
	Yield()
	
	ls_sel = dw_sel_dipen.getitemstring(ll_index, "sel")
	if ls_sel="S" then
		lb_almeno_uno = true
		
		//non esco più dal ciclo perchè mi serve sapere quanti dipendenti devo elaborare
		//exit
		ll_count_tot += 1
	end if
next

if not lb_almeno_uno then
	g_mb.error("Selezionare almeno un dipendente oppure un Centro di Costo!")
	
	return 0
end if

//-----------------------------------------------------------------------------------
//se arrivi fin qui inizia il calcolo


//################################################################################################
//se previsto il calcolo VPT allora valuto subito VFLP
//leggo VLFP solo una volta e non per ogni dipendente
if ls_flag_vpt="S" then
	
	setpointer(Hourglass!)
	st_msg.text = "Lettura VFLP ..."
	
	ll_anno_rif = year(date(ldt_inizio_calcolo))
	ldt_inizio = datetime(date(ll_anno_rif, 1, 1), 00:00:00)		//il punto di inizio è sempre il 1° gennaio
	
	//il punto di fine è la fine del mese di ldt_inizio_calcolo  (impostato nel filtro MM/YYYY)
	ll_mese_rif = month(date(ldt_inizio_calcolo))
	
	luo_premio = create uo_premio_produzione
	luo_premio.uof_get_inizio_fine_mese(ll_mese_rif,ll_anno_rif,ldt_null,ldt_fine,ldt_date_v[])
	
	//recupero il codice di almeno un CdC che non ripartisce il fatturato
	if luo_premio.uof_get_cdc_noripartizione(ls_sql_UFF_MAG_AUT[],ls_sql_UFF_MAG_AUT_descriz[],fs_errore) < 0 then
		//in fs_errore il messaggio
		setpointer(Arrow!)
		destroy luo_premio;
		
		return -1
	end if
	
	destroy luo_premio;
	
	if upperbound(ls_sql_UFF_MAG_AUT[])>0 then
		lb_righe_fatture = false
		//prendo il primo CdC senza ripartizione
		ls_cdc_VFLP = ls_sql_UFF_MAG_AUT[1]
	else
		lb_righe_fatture = true
		g_mb.show("Nessun CdC senza ripartizione del fatturato è stato trovato! VFLP sarà calcolato dalle righe fatture")
	end if
	
	//calcolo il fatturato totale vflp fino a fine periodo "fdt_fine"
	
	if not lb_righe_fatture then
	
		select sum(fl)
		into :ld_vflp
		from premi_rp_online
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_centro_costo=:ls_cdc_VFLP and
				data_giorno>=:ldt_inizio and data_giorno<=:ldt_fine
		using sqlca;
	
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura VFLP da premi_rp_online "+sqlca.sqlerrtext
			setpointer(Arrow!)
			
			return -1
		end if
		
	else
	
		select sum(isnull(a.imponibile_iva,0))
		into :ld_vflp
		from det_fat_ven as a
		join tes_fat_ven as t on t.cod_azienda=a.cod_azienda and
										t.anno_registrazione=a.anno_registrazione and
										t.num_registrazione=a.num_registrazione
		where a.cod_azienda=:s_cs_xx.cod_azienda and
				t.data_fattura is not null and t.data_fattura>=:ldt_inizio and t.data_fattura<=:ldt_fine
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura VFLP da fatture vendita "+sqlca.sqlerrtext
			setpointer(Arrow!)
			
			return -1
		end if
	end if
	
	if isnull(ld_vflp) then ld_vflp =0
	
	setpointer(Arrow!)
end if
//################################################################################################

for ll_index=1 to ll_tot
	Yield()
	
	ls_sel = dw_sel_dipen.getitemstring(ll_index, "sel")
	
	if ls_sel = "S" then
		ls_matricola = dw_sel_dipen.getitemstring(ll_index, "matricola")
		ls_cdc = dw_sel_dipen.getitemstring(ll_index, "cenco")
		
		ll_count += 1
		
		//VPT ##############################################################
		if ls_flag_vpt="S" then
			
			st_msg.text = "Calcolo premio VPT per Dipendente matricola "+ls_matricola + &
								"      " + string(ll_count) + " di " + string(ll_count_tot)
			
			if wf_calcola_vpt(ls_matricola,ls_cdc,ldt_inizio_calcolo, ldt_data_elaborazione,ld_vflp,fs_errore)<0 then
				//in fs_errore il messaggio
				return -1
			end if
		end if
		
		//VIO #############################################################
		if ls_flag_vio="S" then
			
			st_msg.text = "Calcolo premio VIO per Dipendente matricola "+ls_matricola + &
								"      " + string(ll_count) + " di " + string(ll_count_tot)
			
			if wf_calcola_vio(ls_matricola,ls_cdc,ldt_inizio_calcolo, ldt_data_elaborazione,fs_errore)<0 then
				//in fs_errore il messaggio
				return -1
			end if
		end if
		
		//VIA ##############################################################
		if ls_flag_via="S" then
			
			st_msg.text = "Calcolo premio VIA per Dipendente matricola "+ls_matricola + &
								"      " + string(ll_count) + " di " + string(ll_count_tot)
			
			if wf_calcola_via(ls_matricola,ls_cdc,ldt_inizio_calcolo, ldt_data_elaborazione,fs_errore)<0 then
				//in fs_errore il messaggio
				return -1
			end if
		end if
		
		//#################################################################
		
	end if

next

return 1
end function

public function integer wf_calcola_vpt (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref decimal fd_vflp, ref string fs_errore);//CALCOLO PREMIO VPT ad personam
//
//									VPT = P * T * IMPP * (IP oppure IXrep) * VPR
//
// P e VPR sono in premi_aree (anno, cdc)
// T è in premi_periodo (anno, periodo)
// IMPP è funzione di ISFA (tabella premi_impp_isfa)
//					con ISFA = (VFLP/VFLA) * 100  cioè rapporto a 100 del fatturato effettivo con quello atteso 
//																									a fine periodo "ldt_data_fine"
//
// IP è il rapporto tra RPrep ed RPRrep moltiplicato tutto per IAPP
//			dove RPrep lo leggo dall'elaborazione on-line in premi_rp_online al giorno del fine periodo "ldt_data_fine"
//				  RPRrep è statistico (premi_periodo_aree, fissato anno, CdC della persona e periodo) a fine periodo
//				  IAPP è il rapporto HOFP/HOPP  (ore ordinarie/ore ordinarie previste fino al giorno del fine periodo "ldt_data_fine")
//						-> si noti che IAPP in taluni mesi e per alcuni CdC può essere imposto ad 1


uo_premio_produzione		luo_premio
datetime						ldt_data_inizio, ldt_data_fine, ldt_date_v[], ldt_null, ldt_today, ldt_now
decimal						ld_impp, ld_vfla, ld_hofp, ld_hopp, ld_ol, ld_fl, ld_ip, ld_vpt, ld_vpr,&
								ld_rpr_reparto, ld_ix_area, ld_is_area, ld_bpt
integer						li_p, li_t
long							ll_count, ll_anno_rif, ll_mese_rif
string						ls_dipendente

luo_premio = create uo_premio_produzione

//imposto il periodo di calcolo ---------------------------------------------------
ll_anno_rif = year(date(fdt_data_calcolo))
ldt_data_inizio = datetime(date(ll_anno_rif, 1, 1), 00:00:00)		//il punto di inizio è sempre il 1° gennaio

//il punto di fine è la fine del mese di fdt_data_calcolo  (impostato nel filtro MM/YYYY)
ll_mese_rif = month(date(fdt_data_calcolo))
luo_premio.uof_get_inizio_fine_mese(ll_mese_rif,ll_anno_rif,ldt_null,ldt_data_fine,ldt_date_v[])

setnull(ldt_null)
//----------------------------------------------------------------------------------

//fd_vflp è stato già calcolato prima, ma lo passo lo stesso byref, resterà invariato, tantoè uguale per tutti
//a parità di periodo
if luo_premio.uof_get_vpt(fs_matricola,ldt_data_inizio,ldt_data_fine,ls_dipendente,li_p,ld_vpr,li_t,ld_bpt,&
								  ld_is_area,ld_ix_area,fd_vflp,ld_vfla,ld_impp,ld_fl,ld_ol,ld_rpr_reparto,ld_hofp,ld_hopp,&
								  ld_ip,ld_vpt,fs_errore) < 0 then
	//in fs_errore il messaggio
	destroy luo_premio;
	
	return -1
end if

destroy luo_premio;


//se arrivi fin qui aggiorna in tabella

select count(*)
into :ll_count
from premi_operai_vpt
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:ll_anno_rif and
		mese_riferimento=:ll_mese_rif
using sqlca;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in controllo esistenza record in premi_operai_vpt per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
	return -1
	
end if

setnull(ldt_null)

if isnull(fdt_elaborazione) or year(date(fdt_elaborazione)) <= 2000 then
	ldt_today = datetime(today(), 00:00:00)
else
	ldt_today = datetime(date(fdt_elaborazione), 00:00:00)
end if

ldt_now = datetime(date(1900, 1, 1), now())

if isnull(ll_count) or ll_count = 0 then
	//fai insert
	
	insert into premi_operai_vpt  
         ( cod_azienda,   
           matricola,   
           anno_riferimento,   
           mese_riferimento,
			  nome_cognome,
           cod_centro_costo,   
           vpt,   
           vpt_erogato,
           data_erogazione,   
           flag_erogazione,   
           note_erogazione,
			  ip,
			  fl,
			  ol,
           hofp,
			  hopp,
           bpt,
			  vflp,
			  vfla,
			  impp,
           data_elab,   
           ora_elab,
			  ix_area,
			  is_area)  
	values(:s_cs_xx.cod_azienda,   
          :fs_matricola,   
          :ll_anno_rif,   
          :ll_mese_rif, 
			 :ls_dipendente,
          :fs_cdc,   
          :ld_vpt,   
          0,   
          :ldt_null,   
          'N',   
          '',   
          :ld_ip,   
          :ld_fl,
			 :ld_ol,
			 :ld_hofp,
			 :ld_hopp,
			 :ld_bpt,
			 :fd_vflp,
			 :ld_vfla,
			 :ld_impp, 
          :ldt_today,   
          :ldt_now,
			 :ld_ix_area,
			 :ld_is_area)
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in inserimento dati calcolo VPT per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	//***********************************************************************
	//decidiamo in tal caso di lasciare inalterati i seguenti campi
	//		data_erogazione 
	//		flag_erogazione
	//		note_erogazione
	//***********************************************************************
	
	//fai update
	update premi_operai_vpt  
	set	nome_cognome		=:ls_dipendente,
			cod_centro_costo 	=:fs_cdc,   
			vpt 				  	=:ld_vpt,   
			ip 				  	=:ld_ip,   
			fl					  	=:ld_fl,
			ol						=:ld_ol,
			hofp					=:ld_hofp,
			hopp					=:ld_hopp,
			bpt					=:ld_bpt,
			vflp					=:fd_vflp,
			vfla					=:ld_vfla,
			impp					=:ld_impp,
			data_elab		  	=:ldt_today,   
			ora_elab			  	=:ldt_now,
			ix_area				=:ld_ix_area,
			is_area				=:ld_is_area
	where cod_azienda			=:s_cs_xx.cod_azienda and
			matricola			=:fs_matricola and
			anno_riferimento	=:ll_anno_rif and
			mese_riferimento	=:ll_mese_rif
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in aggiornamento dati calcolo VPT per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
end if


return 1
end function

public function integer wf_calcola_via (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref string fs_errore);//CALCOLO PREMIO VIA ad personam
//
//									VIA = IPIA * VIAarea
//
// dove VIAarea è nella tabella premi_aree  ->  (anno_riferimento,cod_centro_costo)
//
//      IPIA dipende da NR (NR=n° ingressi NON oltre 1H dal normale or. di lavoro)
//									può essere 0, 1 o >1 in un determinato mese
//									Nei tre casi vedere il rispettivo valore di IPIA
//									in tabella premi_aree (ipia_3,ipia_2,ipia_1) per anno_riferimento,cod_centro_costo
//
//Se non si è nel mese di erogazione si pone VIA=0
//
//in caso di permesso nel mese il premio viene azzerato
//
//ATTENZIONE MANCA L'AZZERAMENTO AUTOMATICO DI VIA IN CASO DI PERMESSO!!! VERIFICARE!!!
//ATTENZIONE: deve essereci almeno una presenza di tipo ORDI nel mese ....

uo_premio_produzione		luo_premio
datetime						ldt_data_inizio, ldt_data_fine, ldt_date_v[], ldt_null, ldt_today, ldt_now
decimal						ld_ipia, ld_via, ld_via_area
integer						li_nr
long							ll_count, ll_anno_rif, ll_mese_rif
string						ls_dipendente

luo_premio = create uo_premio_produzione

//imposto primo e ultimo del mese di calcolo --------------------------------------
ll_mese_rif = month(date(fdt_data_calcolo))
ll_anno_rif = year(date(fdt_data_calcolo))

luo_premio.uof_get_inizio_fine_mese(ll_mese_rif,ll_anno_rif,ldt_data_inizio,ldt_data_fine,ldt_date_v[])
//----------------------------------------------------------------------------------

if luo_premio.uof_get_nr_ipia_via(fs_matricola,ldt_data_inizio,ldt_data_fine,ls_dipendente, &
										li_nr,ld_ipia,ld_via_area,ld_via,fs_errore) < 0 then
	//in fs_errore il messaggio
	destroy luo_premio;
	
	return -1
end if

destroy luo_premio;

//se arrivi fin qui aggiorna in tabella

select count(*)
into :ll_count
from premi_operai_via
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:ll_anno_rif and
		mese_riferimento=:ll_mese_rif
using sqlca;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in controllo esistenza record in premi_operai_via per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
	return -1
	
end if

setnull(ldt_null)

if isnull(fdt_elaborazione) or year(date(fdt_elaborazione)) <= 2000 then
	ldt_today = datetime(today(), 00:00:00)
else
	ldt_today = datetime(date(fdt_elaborazione), 00:00:00)
end if

ldt_now = datetime(date(1900, 1, 1), now())

if isnull(ll_count) or ll_count = 0 then
	//fai insert
	
	insert into premi_operai_via  
         ( cod_azienda,   
           matricola,   
           anno_riferimento,   
           mese_riferimento,
			  nome_cognome,
           cod_centro_costo,   
           via,   
           via_erogato,
           data_erogazione,   
           flag_erogazione,   
           note_erogazione,   
           ipia,
			  nr,
           via_area,   
           data_elab,   
           ora_elab )  
	values(:s_cs_xx.cod_azienda,   
          :fs_matricola,   
          :ll_anno_rif,   
          :ll_mese_rif, 
			 :ls_dipendente,
          :fs_cdc,   
          :ld_via,   
          0,   
          :ldt_null,   
          'N',   
          '',   
          :ld_ipia,   
			 :li_nr,
          :ld_via_area,   
          :ldt_today,   
          :ldt_now )
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in inserimento dati calcolo VIA per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	//***********************************************************************
	//decidiamo in tal caso di lasciare inalterati i seguenti campi
	//		data_erogazione 
	//		flag_erogazione
	//		note_erogazione
	//***********************************************************************
	
	//fai update
	update premi_operai_via  
	set	nome_cognome		=:ls_dipendente,
			cod_centro_costo 	=:fs_cdc,   
			via 				  	=:ld_via,   
			ipia 				  	=:ld_ipia,   
			nr				  		=:li_nr,   
			via_area				=:ld_via_area,
			data_elab		  	=:ldt_today,   
			ora_elab			  	=:ldt_now
	where cod_azienda			=:s_cs_xx.cod_azienda and
			matricola			=:fs_matricola and
			anno_riferimento	=:ll_anno_rif and
			mese_riferimento	=:ll_mese_rif
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in aggiornamento dati calcolo VIA per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
end if


return 1

end function

public function integer wf_calcola_vio (string fs_matricola, string fs_cdc, datetime fdt_data_calcolo, datetime fdt_elaborazione, ref string fs_errore);
//CALCOLO PREMIO VIO ad personam
//
//									VIO = HSP * VHSarea
//
// dove VHSarea è nella tabella premi_aree  ->  (anno_riferimento,cod_centro_costo)
//
//      HSP = OREstraord + OREfless nel mese in esame
//
//in caso di NC gravi il premio può essere azzerato o diminuito dalla direzione


uo_premio_produzione		luo_premio
datetime						ldt_data_inizio, ldt_data_fine, ldt_date_v[], ldt_null, ldt_today, ldt_now
decimal						ld_hsp, ld_vhs, ld_vio
long							ll_count, ll_anno_rif, ll_mese_rif
string						ls_dipendente

luo_premio = create uo_premio_produzione

//imposto primo e ultimo del mese di calcolo --------------------------------------
ll_mese_rif = month(date(fdt_data_calcolo))
ll_anno_rif = year(date(fdt_data_calcolo))

luo_premio.uof_get_inizio_fine_mese(ll_mese_rif,ll_anno_rif,ldt_data_inizio,ldt_data_fine,ldt_date_v[])
//----------------------------------------------------------------------------------

if luo_premio.uof_get_hsp_vhs_vio(	fs_matricola,ldt_data_inizio,ldt_data_fine,ls_dipendente,&
												ld_hsp,ld_vhs,ld_vio,fs_errore) < 0 then
	//in fs_errore il messaggio
	destroy luo_premio;
	
	return -1
end if

destroy luo_premio;

//se arrivi fin qui aggiorna in tabella

select count(*)
into :ll_count
from premi_operai_vio
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:ll_anno_rif and
		mese_riferimento=:ll_mese_rif
using sqlca;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in controllo esistenza record in premi_operai_vio per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
	return -1
	
end if

setnull(ldt_null)

if isnull(fdt_elaborazione) or year(date(fdt_elaborazione)) <= 2000 then
	ldt_today = datetime(today(), 00:00:00)
else
	ldt_today = datetime(date(fdt_elaborazione), 00:00:00)
end if

ldt_now = datetime(date(1900, 1, 1), now())

if isnull(ll_count) or ll_count = 0 then
	//fai insert
	
	insert into premi_operai_vio  
         ( cod_azienda,   
           matricola,   
           anno_riferimento,   
           mese_riferimento,
			  nome_cognome,
           cod_centro_costo,   
           vio,   
           vio_erogato,
           data_erogazione,   
           flag_erogazione,   
           note_erogazione,   
           hsp,   
           vhs,   
           data_elab,   
           ora_elab )  
	values(:s_cs_xx.cod_azienda,   
          :fs_matricola,   
          :ll_anno_rif,   
          :ll_mese_rif, 
			 :ls_dipendente,
          :fs_cdc,   
          :ld_vio,   
          0,   
          :ldt_null,   
          'N',   
          '',   
          :ld_hsp,   
          :ld_vhs,   
          :ldt_today,   
          :ldt_now )
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in inserimento dati calcolo VIO per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	//***********************************************************************
	//decidiamo in tal caso di lasciare inalterati i seguenti campi
	//		data_erogazione 
	//		flag_erogazione
	//		note_erogazione
	//***********************************************************************
	
	//fai update
	update premi_operai_vio  
	set	nome_cognome		=:ls_dipendente,
			cod_centro_costo =:fs_cdc,   
			vio 				  =:ld_vio,   
			hsp 				  =:ld_hsp,   
			vhs				  =:ld_vhs,   
			data_elab		  =:ldt_today,   
			ora_elab			  =:ldt_now
	where cod_azienda			=:s_cs_xx.cod_azienda and
			matricola			=:fs_matricola and
			anno_riferimento	=:ll_anno_rif and
			mese_riferimento	=:ll_mese_rif
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in aggiornamento dati calcolo VIO per dipendente "+fs_matricola + " " + sqlca.sqlerrtext
		return -1
	end if
	
end if


return 1
end function

on w_premi_calcolo.create
int iCurrent
call super::create
this.st_msg=create st_msg
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.dw_sel_dipen=create dw_sel_dipen
this.dw_sel_cdc=create dw_sel_cdc
this.cb_elabora=create cb_elabora
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_msg
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_sel_dipen
this.Control[iCurrent+5]=this.dw_sel_cdc
this.Control[iCurrent+6]=this.cb_elabora
end on

on w_premi_calcolo.destroy
call super::destroy
destroy(this.st_msg)
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.dw_sel_dipen)
destroy(this.dw_sel_cdc)
destroy(this.cb_elabora)
end on

event pc_setwindow;call super::pc_setwindow;string ls_errore

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

postevent("ue_set_periodo")


//recupero i gruppi da escludere nella elaborazione di OLrep ------------------------------------
//li visualizzo in rosso nella lista dipendenti
select stringa
into :is_escludi_da_rp
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='GDP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	//fs_errore = "Errore in lettura parametro GDP: "+sqlca.sqlerrtext
	g_mb.error("Errore in lettura parametro GDP, gruppi da escludere per OL: "+sqlca.sqlerrtext)
	return
	
elseif sqlca.sqlcode=100 or is_escludi_da_rp="" or isnull(is_escludi_da_rp) then
	//fs_errore = "Parametro GDP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	g_mb.error("Parametro ADP (gruppi da escludere per OL) non trovato oppure "+&
								"non impostato nella tabella parametri aziendali!")
	return
	
end if
//################################################################################################

if wf_retrieve_cdc_dipen(ls_errore)	< 0 then
	g_mb.error(ls_errore)
	
	return
end if

end event

type st_msg from statictext within w_premi_calcolo
integer x = 37
integer y = 556
integer width = 2912
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_premi_calcolo
integer x = 37
integer y = 668
integer width = 2912
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 8421504
string text = "CENTRI DI COSTO E DIPENDENTI COINVOLTI NEL CALCOLO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_selezione from u_dw_search within w_premi_calcolo
integer x = 27
integer y = 32
integer width = 2912
integer height = 468
integer taborder = 30
string dataobject = "d_premi_calcolo_sel"
boolean border = false
end type

type dw_sel_dipen from datawindow within w_premi_calcolo
integer x = 1344
integer y = 752
integer width = 1591
integer height = 1544
integer taborder = 30
string dataobject = "d_premi_calcolo_dipen_sel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;string ls_comando

ls_comando = "SEL-DESEL-TUTTI"

if row>0 then
	choose case dwo.name
		case "b_sel"
			
			if dwo.text = "Tutti" then
				//hai scelto di selezionare tutti
				wf_sel_desel_dipendenti(ls_comando, "S")
				
				//alla fine rinomina il pulsante
				dwo.text = "Nessuno"
				
			else
				//hai scelto di DE-selezionare tutti
				wf_sel_desel_dipendenti(ls_comando, "N")
				
				//alla fine rinomina il pulsante
				dwo.text = "Tutti"
			end if
			
	end choose
end if
end event

type dw_sel_cdc from datawindow within w_premi_calcolo
integer x = 37
integer y = 752
integer width = 1275
integer height = 1544
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_premi_calcolo_cdc_sel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;string ls_cod_cdc

if row > 0 then
	choose case dwo.name
		case "sel"
			
			ls_cod_cdc = getitemstring(row, "codice")
			wf_sel_desel_dipendenti(ls_cod_cdc, data)
			
	end choose
	
end if
end event

type cb_elabora from commandbutton within w_premi_calcolo
integer x = 1317
integer y = 448
integer width = 347
integer height = 92
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;string ls_errore
long ll_ret

ll_ret = wf_elabora(ls_errore)

if ll_ret < 0 then
	g_mb.error(ls_errore)
	
	st_msg.text = "Elaborazione terminata con errori!"
	
	rollback using sqlca;
	return
	
elseif ll_ret=0 then
	//non ha passato la validazione dei campi di impostazione
	//messaggio già dato esci senza fare niente
	return
	
else
	commit using sqlca;
	
	g_mb.show("Elaborazione terminata con successo!") 
	st_msg.text = "Elaborazione terminata con successo!"
	
	return
end if

end event

