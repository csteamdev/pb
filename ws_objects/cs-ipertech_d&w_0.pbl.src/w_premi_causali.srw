﻿$PBExportHeader$w_premi_causali.srw
forward
global type w_premi_causali from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_premi_causali
end type
end forward

global type w_premi_causali from w_cs_xx_principale
integer width = 2373
integer height = 1664
string title = "Causali Presenze"
boolean minbox = false
dw_lista dw_lista
end type
global w_premi_causali w_premi_causali

type variables

string is_azienda_db_presenze = ""
end variables

forward prototypes
public function integer wf_connetti_presenze (ref transaction ftran_presenze)
public function integer wf_disconnetti_presenze (ref transaction ftran_presenze)
end prototypes

public function integer wf_connetti_presenze (ref transaction ftran_presenze);string ls_odbc_presenze, ls_uid_presenze, ls_pwd_presenze, ls_dbparm


//-------------------------------------------------------
select stringa
into :ls_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura parametro DBP: "+sqlca.sqlerrtext)	
	return -1
	
elseif sqlca.sqlcode=100 or ls_odbc_presenze="" or isnull(ls_odbc_presenze) then
	g_mb.error("Parametro DBP (odbc DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!")
	return -1
	
end if

//legge dal parametro aziendale UTP (UID database presenze)
select stringa
into :ls_uid_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='UTP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura parametro UTP: "+sqlca.sqlerrtext)
	return -1
end if
if isnull(ls_uid_presenze) then ls_uid_presenze = ""

//legge dal parametro aziendalePAP (password database presenze)
select stringa
into :ls_pwd_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='PAP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura parametro PAP: "+sqlca.sqlerrtext)
	return -1
end if
if isnull(ls_pwd_presenze) then ls_pwd_presenze = ""


ftran_presenze = create transaction

ftran_presenze.dbms = "ODBC"
ftran_presenze.autocommit = false

//ftran_presenze.dbparm = "Connectstring='DSN="+is_odbc_presenze+"',DisableBind=1"
ls_dbparm = "ConnectString='DSN="+ls_odbc_presenze

if ls_uid_presenze<>"" then
	ls_dbparm += ";UID="+ls_uid_presenze
end if

if ls_pwd_presenze <> "" then
	ls_dbparm += ";PWD="+ls_pwd_presenze
end if

ls_dbparm+= "',CommitOnDisconnect='No',DisableBind=1"

ftran_presenze.dbparm = ls_dbparm

if f_po_connect(ftran_presenze, true) <> 0 then
	g_mb.error("Errore in connessione DB presenze: "+ftran_presenze.sqlerrtext)
	
   return -1
end if

return 1
end function

public function integer wf_disconnetti_presenze (ref transaction ftran_presenze);
disconnect using ftran_presenze;
destroy ftran_presenze;

return 1
end function

on w_premi_causali.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_premi_causali.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;transaction ltran_presenze

if wf_connetti_presenze(ltran_presenze) > 0 then

	f_po_loaddddw_dw(dw_lista, &
						  "cod_causale", &
						  ltran_presenze, &
						  "causali", &
						  "causale", &
						  "descrizione", &
						  "azienda = '" + is_azienda_db_presenze + "'")
	
	wf_disconnetti_presenze(ltran_presenze)
	
end if

end event

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

//recupero il codice azienda database presenze ------------------------------------
select stringa
into :is_azienda_db_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='ADP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura parametro ADP: "+sqlca.sqlerrtext)
	
   return -1
	
elseif sqlca.sqlcode=100 or is_azienda_db_presenze="" or isnull(is_azienda_db_presenze) then
	g_mb.error("Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!")
	
   return -1
end if
//-------------------------------------------------------
end event

type dw_lista from uo_cs_xx_dw within w_premi_causali
integer x = 27
integer y = 24
integer width = 2272
integer height = 1504
integer taborder = 10
string dataobject = "d_premi_causali_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   
next

end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
string ls_cod_causale, ls_flag_tipo_causale

for ll_index = 1 to rowcount()
	
	ls_cod_causale = getitemstring(ll_index, "cod_causale")
	if isnull(ls_cod_causale) or ls_cod_causale="" then
		g_mb.error("Attenzione", "Selezionare la causale!")
		
		pcca.error = c_fatal
		return
	end if
	
	ls_flag_tipo_causale = getitemstring(ll_index, "flag_tipo_causale")
	if isnull(ls_flag_tipo_causale) or ls_flag_tipo_causale="" then
		g_mb.error("Attenzione", "Selezionare la Tipologia di Causale!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

