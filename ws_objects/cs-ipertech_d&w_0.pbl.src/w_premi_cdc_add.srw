﻿$PBExportHeader$w_premi_cdc_add.srw
forward
global type w_premi_cdc_add from w_cs_xx_principale
end type
type st_descrizione from statictext within w_premi_cdc_add
end type
type st_descrizione_2 from statictext within w_premi_cdc_add
end type
type st_4 from statictext within w_premi_cdc_add
end type
type st_3 from statictext within w_premi_cdc_add
end type
type dw_lista from uo_cs_xx_dw within w_premi_cdc_add
end type
end forward

global type w_premi_cdc_add from w_cs_xx_principale
integer width = 1897
integer height = 1872
string title = "Suddivisione Fatturato per Cdc in % - dettaglio"
st_descrizione st_descrizione
st_descrizione_2 st_descrizione_2
st_4 st_4
st_3 st_3
dw_lista dw_lista
end type
global w_premi_cdc_add w_premi_cdc_add

on w_premi_cdc_add.create
int iCurrent
call super::create
this.st_descrizione=create st_descrizione
this.st_descrizione_2=create st_descrizione_2
this.st_4=create st_4
this.st_3=create st_3
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_descrizione
this.Control[iCurrent+2]=this.st_descrizione_2
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.dw_lista
end on

on w_premi_cdc_add.destroy
call super::destroy
destroy(this.st_descrizione)
destroy(this.st_descrizione_2)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("regola")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)
									
iuo_dw_main = dw_lista
end event

type st_descrizione from statictext within w_premi_cdc_add
integer x = 27
integer y = 8
integer width = 1810
integer height = 224
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_descrizione_2 from statictext within w_premi_cdc_add
integer x = 27
integer y = 1112
integer width = 1810
integer height = 244
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_4 from statictext within w_premi_cdc_add
integer x = 110
integer y = 1524
integer width = 1655
integer height = 220
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "il sistema sostituirà il carattere % con la lettera dello stabilimento corretta (C-S-V-P). Successivamente leggerà il codice del relativo centro di costo dalla anagrafica reparti per il reparto cosi ottenuto"
boolean focusrectangle = false
end type

type st_3 from statictext within w_premi_cdc_add
integer x = 27
integer y = 1376
integer width = 1810
integer height = 132
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "%_MG per indicare, dove la regola lo permette, ad es., il centro di costo Magazzino generico"
boolean focusrectangle = false
end type

type dw_lista from uo_cs_xx_dw within w_premi_cdc_add
integer x = 27
integer y = 240
integer width = 1810
integer height = 856
integer taborder = 10
string dataobject = "d_premi_cdc_add_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")

parent.title = "Percentuali per regola '"+ls_regola+"'"

st_descrizione.text = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "descrizione")
st_descrizione_2.text = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "descrizione_2")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_regola)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(getitemstring(ll_i, "regola")) or getitemstring(ll_i, "regola") = "" then
		setitem(ll_i, "regola", ls_regola)
	end if
   
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
decimal ld_perc
string ls_cod_cdc

for ll_index = 1 to rowcount()
	
	ls_cod_cdc = getitemstring(ll_index, "cod_centro_costo")
	if isnull(ls_cod_cdc) or ls_cod_cdc="" then
		g_mb.error("Attenzione", "Selezionare il Centro di Costo!")
		
		pcca.error = c_fatal
		return
	end if
	
	ld_perc = getitemdecimal(ll_index, "percentuale")
	if isnull(ld_perc) or ld_perc<=0 then
		g_mb.error("Attenzione", "Inserire la percentuale per il Centro di Costo!")
		
		pcca.error = c_fatal
		return
	end if
next
end event

event pcd_new;call super::pcd_new;string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")

setitem(getrow(), "regola", ls_regola)
end event

