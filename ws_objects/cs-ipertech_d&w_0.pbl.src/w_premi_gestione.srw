﻿$PBExportHeader$w_premi_gestione.srw
forward
global type w_premi_gestione from w_cs_xx_principale
end type
type dw_report from uo_std_dw within w_premi_gestione
end type
type dw_premi_vpt from uo_cs_xx_dw within w_premi_gestione
end type
type dw_premi_via from uo_cs_xx_dw within w_premi_gestione
end type
type dw_premi_vio from uo_cs_xx_dw within w_premi_gestione
end type
type dw_folder from u_folder within w_premi_gestione
end type
type dw_selezione from u_dw_search within w_premi_gestione
end type
type st_1 from statictext within w_premi_gestione
end type
type em_perc_vpt from editmask within w_premi_gestione
end type
type cbx_tutti from checkbox within w_premi_gestione
end type
type em_data_eroga from editmask within w_premi_gestione
end type
type cb_eroga from commandbutton within w_premi_gestione
end type
end forward

global type w_premi_gestione from w_cs_xx_principale
integer width = 3086
integer height = 2088
string title = "Gestione Premi Dipendenti"
boolean resizable = false
event ue_set_periodo ( )
dw_report dw_report
dw_premi_vpt dw_premi_vpt
dw_premi_via dw_premi_via
dw_premi_vio dw_premi_vio
dw_folder dw_folder
dw_selezione dw_selezione
st_1 st_1
em_perc_vpt em_perc_vpt
cbx_tutti cbx_tutti
em_data_eroga em_data_eroga
cb_eroga cb_eroga
end type
global w_premi_gestione w_premi_gestione

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables
string is_azienda_db_presenze = ""
end variables

forward prototypes
public function integer wf_retrieve_dipen (ref string fs_errore)
public function integer wf_retrieve_vio ()
public function integer wf_note_erogazione (ref datawindow fdw_data, long fl_row)
public subroutine wf_filtro (ref string fs_cod_centro_costo, ref string fs_matricola, ref long fl_anno_inizio, ref long fl_mese_inizio, ref long fl_anno_fine, ref long fl_mese_fine)
public function integer wf_retrieve_vpt ()
public function integer wf_retrieve_via ()
public function integer wf_lettere_erogazione (long fl_mese, long fl_anno, ref string fs_errore)
public function integer wf_stampa (ref datawindow fdw_data)
public function integer wf_report ()
public function integer wf_eroga (long fl_row, string fs_sel, ref string fs_errore)
end prototypes

event ue_set_periodo();datetime ldt_inizio, ldt_fine

ldt_inizio = datetime(date(year(today()), 1, 1), 00:00:00)
ldt_fine = datetime(date(today()), 23:59:59)

dw_selezione.setitem(1, "mese_anno", ldt_inizio)
dw_selezione.setitem(1, "mese_anno_al", ldt_fine)
end event

public function integer wf_retrieve_dipen (ref string fs_errore);string ls_odbc_presenze, ls_uid_presenze, ls_pwd_presenze, ls_dbparm
transaction ltran_presenze

//-------------------------------------------------------
select stringa
into :ls_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro DBP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or ls_odbc_presenze="" or isnull(ls_odbc_presenze) then
	fs_errore = "Parametro DBP (odbc DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if

//legge dal parametro aziendale UTP (UID database presenze)
select stringa
into :ls_uid_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='UTP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro UTP: "+sqlca.sqlerrtext
	return -1
	
end if
if isnull(ls_uid_presenze) then ls_uid_presenze = ""

//legge dal parametro aziendalePAP (password database presenze)
select stringa
into :ls_pwd_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='PAP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro PAP: "+sqlca.sqlerrtext
	return -1
	
end if
if isnull(ls_pwd_presenze) then ls_pwd_presenze = ""


//recupero il codice azienda database presenze ------------------------------------
select stringa
into :is_azienda_db_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='ADP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro ADP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or is_azienda_db_presenze="" or isnull(is_azienda_db_presenze) then
	fs_errore = "Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if
//-------------------------------------------------------
ltran_presenze = create transaction

ltran_presenze.dbms = "ODBC"
ltran_presenze.autocommit = false

//ltran_presenze.dbparm = "Connectstring='DSN="+ls_odbc_presenze+"',DisableBind=1"
ls_dbparm = "ConnectString='DSN="+ls_odbc_presenze

if ls_uid_presenze<>"" then
	ls_dbparm += ";UID="+ls_uid_presenze
end if

if ls_pwd_presenze <> "" then
	ls_dbparm += ";PWD="+ls_pwd_presenze
end if

ls_dbparm+= "',CommitOnDisconnect='No',DisableBind=1"

ltran_presenze.dbparm = ls_dbparm

if f_po_connect(ltran_presenze, true) <> 0 then
	fs_errore = "Errore in connessione DB presenze: "+ltran_presenze.sqlerrtext
	
   return -1
end if

//-------------------------------------------------------

//f_PO_LoadDDDW_DW(dw_selezione,"matricola",ltran_presenze,&
//                 "dipen","matricola","cognome + ' ' + nome", &
//                 "dipen.azienda='" + is_azienda_db_presenze + "' and "+&
//					  "dipen.ca5='SI'") 
f_PO_LoadDDDW_SORT(dw_selezione,"matricola",ltran_presenze,&
                 "dipen","matricola","cognome + ' ' + nome", &
                 "dipen.azienda='" + is_azienda_db_presenze + "' and "+&
					  "dipen.ca5='SI'",&
					  "cognome,nome") 

destroy ltran_presenze;

//-------------------------------------------------------

return 1
end function

public function integer wf_retrieve_vio ();string ls_matricola,ls_cod_centro_costo
long ll_mese_inizio, ll_mese_fine, ll_anno_inizio, ll_anno_fine, ll_ret

//recupero le info del filtro
wf_filtro(ls_cod_centro_costo,ls_matricola,ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

//eseguo retrieve
ll_ret = dw_premi_vio.retrieve(s_cs_xx.cod_azienda,ls_cod_centro_costo,ls_matricola,&
								ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

return ll_ret
end function

public function integer wf_note_erogazione (ref datawindow fdw_data, long fl_row);string ls_note

ls_note = fdw_data.getitemstring(fl_row, "note_erogazione")

//imposto il limit delle note a 100
s_cs_xx.parametri.parametro_b_2 = true
s_cs_xx.parametri.parametro_i_2 = 100

openwithparm(w_inserisci_altro_valore, ls_note)

ls_note = message.stringparm

if ls_note="cs_team_esc_pressed" then
else
	fdw_data.setitem(fl_row, "note_erogazione", ls_note)
end if

return 1
end function

public subroutine wf_filtro (ref string fs_cod_centro_costo, ref string fs_matricola, ref long fl_anno_inizio, ref long fl_mese_inizio, ref long fl_anno_fine, ref long fl_mese_fine);datetime ldt_inizio, ldt_fine

dw_selezione.accepttext()

ldt_inizio = dw_selezione.getitemdatetime(1, "mese_anno")
ldt_fine = dw_selezione.getitemdatetime(1, "mese_anno_al")
fs_matricola = dw_selezione.getitemstring(1, "matricola")
fs_cod_centro_costo = dw_selezione.getitemstring(1, "cod_centro_costo")


if not isnull(ldt_inizio) and year(date(ldt_inizio))>2000 then
	fl_mese_inizio = month(date(ldt_inizio))
	fl_anno_inizio = year(date(ldt_inizio))
else
	setnull(fl_mese_inizio)
	setnull(fl_anno_inizio)
end if

if not isnull(ldt_fine) and year(date(ldt_fine))>2000 then
	fl_mese_fine = month(date(ldt_fine))
	fl_anno_fine = year(date(ldt_fine))
else
	setnull(fl_mese_fine)
	setnull(fl_anno_fine)
end if

if fs_matricola="" then setnull(fs_matricola)
if fs_cod_centro_costo="" then setnull(fs_cod_centro_costo)


end subroutine

public function integer wf_retrieve_vpt ();string ls_matricola,ls_cod_centro_costo
long ll_mese_inizio, ll_mese_fine, ll_anno_inizio, ll_anno_fine, ll_ret

//recupero le info del filtro
wf_filtro(ls_cod_centro_costo,ls_matricola,ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

//eseguo retrieve
ll_ret = dw_premi_vpt.retrieve(s_cs_xx.cod_azienda,ls_cod_centro_costo,ls_matricola,&
								ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

return ll_ret
end function

public function integer wf_retrieve_via ();string ls_matricola,ls_cod_centro_costo
long ll_mese_inizio, ll_mese_fine, ll_anno_inizio, ll_anno_fine, ll_ret

//recupero le info del filtro
wf_filtro(ls_cod_centro_costo,ls_matricola,ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

//eseguo retrieve
ll_ret = dw_premi_via.retrieve(s_cs_xx.cod_azienda,ls_cod_centro_costo,ls_matricola,&
								ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

return ll_ret
end function

public function integer wf_lettere_erogazione (long fl_mese, long fl_anno, ref string fs_errore);

return 1
end function

public function integer wf_stampa (ref datawindow fdw_data);
fdw_data.setredraw(false)
fdw_data.object.datawindow.color = rgb(255,255,255)
fdw_data.print(true, true)
fdw_data.object.datawindow.color = rgb(192,192,192)
fdw_data.setredraw(true)

return 1
end function

public function integer wf_report ();string ls_matricola,ls_cod_centro_costo, ls_errore
long ll_mese_inizio, ll_mese_fine, ll_anno_inizio, ll_anno_fine, ll_index, ll_index2, ll_new
uo_premio_produzione luo_premio

string ls_cdc[], ls_des_cdc[], ls_matr[], ls_dip[], ls_sql
datastore lds_premio
integer li_mese
decimal ld_premio, ld_erogato, ld_maturato, ld_ip
datetime ldt_data_erogazione, ldt_data_premi, ldt_inizio[], ldt_fine[]

dw_report.reset()

//recupero le info del filtro
wf_filtro(ls_cod_centro_costo,ls_matricola,ll_anno_inizio,ll_mese_inizio,ll_anno_fine,ll_mese_fine)

//cursore dipendenti ------------------------------------
luo_premio = create uo_premio_produzione

luo_premio.uof_get_periodi_lettera(ll_anno_inizio,  ldt_inizio[], ldt_fine[])

if luo_premio.uof_get_dipendenti(ls_cod_centro_costo,ls_matricola,ls_cdc[],ls_des_cdc[],ls_matr[],ls_dip[],ls_errore) < 0 then
	destroy luo_premio
	g_mb.error(ls_errore)
	
	return -1
end if
destroy luo_premio
//------------------------------------------------------

for ll_index=1 to upperbound(ls_matr[])
	
	
	//VIA ###############################################################
	ll_new = dw_report.insertrow(0)
	dw_report.setitem(ll_new,"centro_costo",ls_cdc[ll_index]+" "+ls_des_cdc[ll_index])
	dw_report.setitem(ll_new,"dipendente",ls_dip[ll_index])
	dw_report.setitem(ll_new,"premio","VIA")
	
	ls_sql = "select mese_riferimento,via "+&
				"from premi_operai_via "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr[ll_index]+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//elabora le ultime 3 colonne di riepilogo
	//-------------------------------------------------------------
	ldt_data_erogazione = datetime(date(ll_anno_inizio,7,31),00:00:00)  //31 luglio
	
	//1)
	//erogato prima del 12/8
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_erogato)
	
	//-------------------------------------------------------------
	
	//2)
	//erogato il 12/8
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_erogato)
	
	//-------------------------------------------------------------
	
	//3)
	//erogato tra 12/8escluso e 15/12escluso
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
			
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_erogato)
	
	
	//VIO ###############################################################
	ll_new = dw_report.insertrow(0)
	dw_report.setitem(ll_new,"centro_costo",ls_cdc[ll_index]+" "+ls_des_cdc[ll_index])
	dw_report.setitem(ll_new,"dipendente",ls_dip[ll_index])
	dw_report.setitem(ll_new,"premio","VIO")
	
	ls_sql = "select mese_riferimento,vio "+&
				"from premi_operai_vio "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr[ll_index]+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//elabora le ultime 3 colonne di riepilogo
	//-------------------------------------------------------------
	ldt_data_erogazione = datetime(date(ll_anno_inizio,7,31),00:00:00)  //31 luglio
	
	//1)
	//erogato prima del 12/8
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_erogato)
	
	//-------------------------------------------------------------
	
	//2)
	//erogato il 12/8
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_erogato)
	
	//-------------------------------------------------------------
	
	//3)
	//erogato tra 12/8escluso e 15/12escluso
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
			
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_erogato)
	
	
	//VPT ###############################################################
	ll_new = dw_report.insertrow(0)
	dw_report.setitem(ll_new,"centro_costo",ls_cdc[ll_index]+" "+ls_des_cdc[ll_index])
	dw_report.setitem(ll_new,"dipendente",ls_dip[ll_index])
	dw_report.setitem(ll_new,"premio","VPT")
	
	ls_sql = "select mese_riferimento,vpt,ip "+&
				"from premi_operai_vpt "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr[ll_index]+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	ld_ip = 0
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		//conserva IP del mese più alto
		ld_ip = lds_premio.getitemdecimal(lds_premio.rowcount(), 3)
		dw_report.setitem(ll_new,"ip",ld_ip)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//VPT maturato: vpt relativo al mese più alto finora calcolato (vpt_maturato)
	integer li_mese_rif
	
	select max(mese_riferimento)
	into :li_mese_rif
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio;
			
	if li_mese_rif>0 then
		select vpt
		into :ld_premio
		from premi_operai_vpt
		where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr[ll_index] and 
			anno_riferimento=:ll_anno_inizio and
			mese_riferimento=:li_mese_rif;
			
		dw_report.setitem(ll_new,"vpt_maturato",ld_premio)
			
	end if
	
	//1)
	//erogato prima del 12/8
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr[ll_index] and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_premio)
	
	//2)
	//erogato il 12/8
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr[ll_index] and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_premio)
	
	//3)
	//erogato tra il 12/8escluso e 15/12escluso
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr[ll_index] and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_premio)
	
next

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"


return 1
end function

public function integer wf_eroga (long fl_row, string fs_sel, ref string fs_errore);string ls_perc
long ll_perc
decimal ld_vpt_maturato
datetime ldt_null

if fs_sel="S" then
				
	ls_perc = em_perc_vpt.text
	if isnumber(ls_perc) then
		ll_perc = long(ls_perc)
		
		ld_vpt_maturato = dw_premi_vpt.getitemdecimal(fl_row,"vpt")
		ld_vpt_maturato = (ld_vpt_maturato * ll_perc) / 100
		
	else
		fs_errore = "Impostare una percentuale numerica da erogare per VPT!"
		return -1
	end if
	
else
	setnull(ldt_null)
	ld_vpt_maturato = 0
	dw_premi_vpt.setitem(fl_row, "data_erogazione", ldt_null)
end if

dw_premi_vpt.setitem(fl_row, "vpt_erogato", ld_vpt_maturato)


return 1
end function

on w_premi_gestione.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_premi_vpt=create dw_premi_vpt
this.dw_premi_via=create dw_premi_via
this.dw_premi_vio=create dw_premi_vio
this.dw_folder=create dw_folder
this.dw_selezione=create dw_selezione
this.st_1=create st_1
this.em_perc_vpt=create em_perc_vpt
this.cbx_tutti=create cbx_tutti
this.em_data_eroga=create em_data_eroga
this.cb_eroga=create cb_eroga
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_premi_vpt
this.Control[iCurrent+3]=this.dw_premi_via
this.Control[iCurrent+4]=this.dw_premi_vio
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_selezione
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.em_perc_vpt
this.Control[iCurrent+9]=this.cbx_tutti
this.Control[iCurrent+10]=this.em_data_eroga
this.Control[iCurrent+11]=this.cb_eroga
end on

on w_premi_gestione.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_premi_vpt)
destroy(this.dw_premi_via)
destroy(this.dw_premi_vio)
destroy(this.dw_folder)
destroy(this.dw_selezione)
destroy(this.st_1)
destroy(this.em_perc_vpt)
destroy(this.cbx_tutti)
destroy(this.em_data_eroga)
destroy(this.cb_eroga)
end on

event pc_setwindow;call super::pc_setwindow;string ls_errore,ls_path_logo_1,ls_modify
windowobject l_objects[ ], l_vuoto[]

dw_report.ib_dw_report = true

//set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_premi_vpt.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nodelete,c_default)
dw_premi_vio.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nodelete,c_default)
dw_premi_via.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen + c_nonew + c_nodelete,c_default)

dw_premi_vpt.set_dw_key("cod_azienda")
dw_premi_vpt.set_dw_key("matricola")
dw_premi_vpt.set_dw_key("anno_riferimento")
dw_premi_vpt.set_dw_key("mese_riferimento")

dw_premi_vio.set_dw_key("cod_azienda")
dw_premi_vio.set_dw_key("matricola")
dw_premi_vio.set_dw_key("anno_riferimento")
dw_premi_vio.set_dw_key("mese_riferimento")

dw_premi_via.set_dw_key("cod_azienda")
dw_premi_via.set_dw_key("matricola")
dw_premi_via.set_dw_key("anno_riferimento")
dw_premi_via.set_dw_key("mese_riferimento")


l_objects = l_vuoto
l_objects[1] = dw_selezione
dw_folder.fu_AssignTab(1, "Ricerca", l_Objects[])

l_objects = l_vuoto
l_objects[1] = dw_premi_vpt
l_objects[2] = em_perc_vpt
l_objects[3] = cbx_tutti
l_objects[4] = em_data_eroga
l_objects[5] = cb_eroga
dw_folder.fu_AssignTab(2, "Produttività (VPT)", l_Objects[])

l_objects = l_vuoto
l_objects[1] = dw_premi_vio
dw_folder.fu_AssignTab(3, "Impegno Straordinario (VIO)", l_Objects[])

l_objects = l_vuoto
l_objects[1] = dw_premi_via
dw_folder.fu_AssignTab(4, "Presenza (VIA)", l_Objects[])

l_objects = l_vuoto
l_objects[1] = dw_report
l_objects[2] = st_1
dw_folder.fu_AssignTab(5, "Report", l_Objects[])


dw_folder.fu_FolderCreate(5,5)
dw_folder.fu_SelectTab(1)


postevent("ue_set_periodo")

move(0,0)
resize(w_cs_xx_mdi.mdi_1.width - 50,w_cs_xx_mdi.mdi_1.height - 50)

select stringa
into   :ls_path_logo_1
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
		 flag_parametro = 'S' and &
		 cod_parametro = 'LO3';	

ls_path_logo_1 =  s_cs_xx.volume + ls_path_logo_1
ls_modify = "intestazione.filename='" +ls_path_logo_1 + "'~t"
dw_report.modify(ls_modify)


end event

event resize;call super::resize;
//questa va bene con le impostazioni originali
dw_selezione.x = 50
dw_selezione.y = 168
dw_selezione.width = 2967
dw_selezione.height = 484

//ripristino la posizione originale relativa
dw_folder.x = 23
dw_folder.y = 16
//nuove impostazioni larghezza-lunghezza
dw_folder.width = newwidth - 100
dw_folder.height = newheight - 100

//ripristino la posizione originale relativa
dw_premi_vio.x = 50
dw_premi_vio.y = 168
//nuove impostazioni larghezza-lunghezza
dw_premi_vio.width = dw_folder.width - 120
dw_premi_vio.height = dw_folder.height - 50 - 120

//le altre due impostate come dw_premi_vio
dw_premi_via.x = dw_premi_vio.x
dw_premi_via.y = dw_premi_vio.y
dw_premi_via.width = dw_premi_vio.width
dw_premi_via.height = dw_premi_vio.height

dw_premi_vpt.x = dw_premi_vio.x
dw_premi_vpt.y = dw_premi_vio.y
dw_premi_vpt.width = dw_premi_vio.width
dw_premi_vpt.height = dw_premi_vio.height

dw_report.x = dw_premi_vio.x
dw_report.y = dw_premi_vio.y + 50
dw_report.width = dw_premi_vio.width
dw_report.height = dw_premi_vio.height - 50

em_perc_vpt.x = 1193
em_perc_vpt.y = 188
em_perc_vpt.width = 325
em_perc_vpt.height = 84

cbx_tutti.x = 1563
cbx_tutti.y = 188
cbx_tutti.width = 402
cbx_tutti.height = 80

em_data_eroga.x = 2043
em_data_eroga.y = 176
em_data_eroga.width = 434
em_data_eroga.height = 104

cb_eroga.x = 2546
cb_eroga.y = 184
cb_eroga.width = 343
cb_eroga.height = 88

end event

event pc_setddlb;call super::pc_setddlb;string ls_errore

f_po_loaddddw_dw(dw_selezione, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

if wf_retrieve_dipen(ls_errore)	< 0 then
	g_mb.error(ls_errore)
	
	return
end if
end event

type dw_report from uo_std_dw within w_premi_gestione
integer x = 50
integer y = 256
integer width = 2981
integer height = 1692
integer taborder = 60
string dataobject = "d_premi_gestione_report"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_premi_vpt from uo_cs_xx_dw within w_premi_gestione
integer x = 50
integer y = 168
integer width = 2981
integer height = 1780
integer taborder = 50
string dataobject = "d_premi_gestione_vpt"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_retrieve_vpt()
end event

event buttonclicked;call super::buttonclicked;long ll_index, ll_row

if row>0 then
	choose case dwo.name
		
		case "b_note"
			wf_note_erogazione(dw_premi_vpt, row)
			
		case "b_elimina"
			ll_row = rowcount()
			
			if ll_row>0 then
			else
				g_mb.confirm("Nessuna riga visualizzata! Impossibile eliminare!")
			end if
			
			if g_mb.confirm("Eliminare le righe relative al premio VPT visualizzate a video?") then
				for ll_index=1 to ll_row
					this.deleterow(1)
				next
				
				if this.update() > 0 then
					commit;
				else
					g_mb.error("Errore durante la conferma dell'eliminazione!")
				end if
				
				//in ogni caso rifai la retrieve
				dw_premi_vpt.change_dw_current()
				parent.triggerevent("pc_retrieve")
				
			end if
			
		case "b_stampa"
			wf_stampa(dw_premi_vpt)
		
	end choose
end if
end event

event pcd_delete;call super::pcd_delete;dw_premi_vpt.object.b_note.enabled = false
cb_eroga.enabled = false
end event

event pcd_new;call super::pcd_new;dw_premi_vpt.object.b_note.enabled = false
cb_eroga.enabled = false
end event

event pcd_view;call super::pcd_view;dw_premi_vpt.object.b_note.enabled = true
cb_eroga.enabled = false
end event

event pcd_modify;call super::pcd_modify;dw_premi_vpt.object.b_note.enabled = false
cb_eroga.enabled = true
end event

event itemchanged;call super::itemchanged;string ls_errore

if row>0 then
	choose case dwo.name
		case "flag_erogazione"
			
			if wf_eroga(row,data,ls_errore) < 0 then
				g_mb.error(ls_errore)
				return 1
			end if
			
	end choose
end if
end event

type dw_premi_via from uo_cs_xx_dw within w_premi_gestione
integer x = 50
integer y = 168
integer width = 2981
integer height = 1780
integer taborder = 40
string dataobject = "d_premi_gestione_via"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_retrieve_via()
end event

event buttonclicked;call super::buttonclicked;long ll_index, ll_row

if row>0 then
	choose case dwo.name
		
		case "b_note"
			wf_note_erogazione(dw_premi_via, row)
		
		case "b_stampa"
			wf_stampa(dw_premi_via)
			
		case "b_elimina"
			ll_row = rowcount()
			
			if ll_row>0 then
			else
				g_mb.confirm("Nessuna riga visualizzata! Impossibile eliminare!")
			end if
			
			if g_mb.confirm("Eliminare le righe relative al premio VIA visualizzate a video?") then
				for ll_index=1 to ll_row
					this.deleterow(1)
				next
				
				if this.update() > 0 then
					commit;
				else
					g_mb.error("Errore durante la conferma dell'eliminazione!")
				end if
				
				//in ogni caso rifai la retrieve
				dw_premi_via.change_dw_current()
				parent.triggerevent("pc_retrieve")
				
			end if
			
	end choose
end if
end event

event pcd_delete;call super::pcd_delete;dw_premi_via.object.b_note.enabled = false
end event

event pcd_new;call super::pcd_new;dw_premi_via.object.b_note.enabled = false
end event

event pcd_view;call super::pcd_view;dw_premi_via.object.b_note.enabled = true
end event

event pcd_modify;call super::pcd_modify;dw_premi_via.object.b_note.enabled = false
end event

type dw_premi_vio from uo_cs_xx_dw within w_premi_gestione
integer x = 50
integer y = 168
integer width = 2981
integer height = 1780
integer taborder = 30
string dataobject = "d_premi_gestione_vio"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event buttonclicked;call super::buttonclicked;long ll_row, ll_index

if row>0 then
	choose case dwo.name
		
		case "b_note"
			wf_note_erogazione(dw_premi_vio, row)
		
		case "b_stampa"
			wf_stampa(dw_premi_vio)
			
		case "b_elimina"
			ll_row = rowcount()
			
			if ll_row>0 then
			else
				g_mb.confirm("Nessuna riga visualizzata! Impossibile eliminare!")
			end if
			
			if g_mb.confirm("Eliminare le righe relative al premio VIO visualizzate a video?") then
				for ll_index=1 to ll_row
					this.deleterow(1)
				next
				
				if this.update() > 0 then
					commit;
				else
					g_mb.error("Errore durante la conferma dell'eliminazione!")
				end if
				
				//in ogni caso rifai la retrieve
				dw_premi_vio.change_dw_current()
				parent.triggerevent("pc_retrieve")
				
			end if
		
	end choose
end if
end event

event pcd_new;call super::pcd_new;dw_premi_vio.object.b_note.enabled = false
end event

event pcd_delete;call super::pcd_delete;dw_premi_vio.object.b_note.enabled = false
end event

event pcd_modify;call super::pcd_modify;dw_premi_vio.object.b_note.enabled = false
end event

event pcd_view;call super::pcd_view;dw_premi_vio.object.b_note.enabled = true
end event

event pcd_retrieve;call super::pcd_retrieve;wf_retrieve_vio()
end event

type dw_folder from u_folder within w_premi_gestione
integer x = 23
integer y = 16
integer width = 3031
integer height = 1968
integer taborder = 40
boolean border = false
end type

type dw_selezione from u_dw_search within w_premi_gestione
integer x = 50
integer y = 168
integer width = 2981
integer height = 564
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_premi_gestione_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string ls_flag_dati

if row>0 then
	choose case dwo.name
			
		case "b_cerca"
			//--------------------------------------
			setpointer(Hourglass!)
			
			dw_selezione.accepttext()
			ls_flag_dati = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_dati")
			
			if ls_flag_dati = "A" or ls_flag_dati="T" then
				dw_premi_via.change_dw_current()
				parent.triggerevent("pc_retrieve")
				
				dw_premi_vio.change_dw_current()
				parent.triggerevent("pc_retrieve")
				
				dw_premi_vpt.change_dw_current()
				parent.triggerevent("pc_retrieve")
			end if
			
			if ls_flag_dati = "R" or ls_flag_dati="T" then
				wf_report()
			end if
			
			if ls_flag_dati = "A" or ls_flag_dati="T" then
				dw_folder.fu_SelectTab(2)
			else
				dw_folder.fu_SelectTab(5)
			end if
			
			if ls_flag_dati="A" then
				dw_report.reset()
			elseif ls_flag_dati="R" then
				dw_premi_vpt.reset()
				dw_premi_vio.reset()
				dw_premi_via.reset()
			end if
			
			setpointer(Arrow!)
			
	end choose
end if



end event

type st_1 from statictext within w_premi_gestione
integer x = 73
integer y = 88
integer width = 480
integer height = 48
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 16711680
long backcolor = 12632256
string text = "Stampa Report"
boolean focusrectangle = false
end type

event clicked;dw_report.print(true, true)
end event

type em_perc_vpt from editmask within w_premi_gestione
integer x = 1193
integer y = 188
integer width = 325
integer height = 84
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "50"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "0~~100"
end type

type cbx_tutti from checkbox within w_premi_gestione
integer x = 1563
integer y = 188
integer width = 402
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Eroga Tutti"
boolean lefttext = true
end type

type em_data_eroga from editmask within w_premi_gestione
integer x = 2043
integer y = 176
integer width = 434
integer height = 104
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type cb_eroga from commandbutton within w_premi_gestione
integer x = 2546
integer y = 184
integer width = 343
integer height = 88
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta"
end type

event clicked;long ll_index
string ls_azione, ls_errore, ls_msg_conferma, ls_temp
date ldt_data
datetime ldt_data_imposta

if cbx_tutti.checked then
	ls_azione = "S"
	ldt_data = date(em_data_eroga.text)
	
	if isnull(ldt_data) or year(ldt_data) <= 2000 then
		g_mb.error("Impostare una data erogazione valida!")
		return
	end if
	
	ldt_data_imposta = datetime(ldt_data, 00:00:00)
	
	ls_msg_conferma = "L'operazione imposterà la % di erogazione (solo dove il flag eroga è NO). Continuare?"
else
	ls_azione = "N"
	
	// si vuole togliere lo stato erogazione
	ls_msg_conferma = "Resettare l'impostazione dell'erogazione per tutte le righe a video?"
end if

if g_mb.confirm(ls_msg_conferma) then
else
	return
end if

for ll_index=1 to dw_premi_vpt.rowcount()
	
	if ls_azione="S" then
		//se il flag è impostato a SI salta la riga!
		ls_temp = dw_premi_vpt.getitemstring(ll_index,"flag_erogazione")
		if ls_temp="S" then
			continue
		end if
	end if
	
	if wf_eroga(ll_index,ls_azione,ls_errore) < 0 then
		g_mb.error(ls_errore)
		return
	end if
	
	dw_premi_vpt.setitem(ll_index,"flag_erogazione",ls_azione)
	
	if ls_azione="S" then
		dw_premi_vpt.setitem(ll_index,"data_erogazione",ldt_data_imposta)
	end if
	
next
end event

