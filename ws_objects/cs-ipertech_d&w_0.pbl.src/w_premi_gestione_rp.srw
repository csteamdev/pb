﻿$PBExportHeader$w_premi_gestione_rp.srw
forward
global type w_premi_gestione_rp from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_premi_gestione_rp
end type
type cb_elabora from commandbutton within w_premi_gestione_rp
end type
end forward

global type w_premi_gestione_rp from w_cs_xx_principale
integer width = 2720
integer height = 1008
string title = "Gestione Calcolo RP - FL - OL"
boolean resizable = false
event ue_set_mese_corrente ( )
dw_selezione dw_selezione
cb_elabora cb_elabora
end type
global w_premi_gestione_rp w_premi_gestione_rp

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables

end variables

forward prototypes
public function integer wf_elabora ()
public function integer wf_elabora_anno (ref string fs_errore)
public function integer wf_elabora_mese (long fl_mese, ref string fs_errore)
end prototypes

event ue_set_mese_corrente();long ll_mese_corrente

ll_mese_corrente = month(today())
dw_selezione.setitem(1, "mese", ll_mese_corrente)
end event

public function integer wf_elabora ();string ls_flag_mese, ls_flag_anno, ls_errore
long ll_mese

dw_selezione.accepttext()

ls_flag_anno = dw_selezione.getitemstring(1, "flag_elabora_anno")
ls_flag_mese = dw_selezione.getitemstring(1, "flag_elabora_mese")

if ls_flag_mese <> "S" and ls_flag_anno<>"S" then
	g_mb.error("Selezionare almeno una tipologia di elaborazione!")
	
	return 1
end if

if ls_flag_mese = "S" then
	ll_mese = dw_selezione.getitemnumber(1, "mese")
end if

if not g_mb.confirm("Elaborare i dati?") then
	return 1
end if

if ls_flag_anno="S" then
	if wf_elabora_anno(ls_errore) < 0 then
		rollback using sqlca;
		g_mb.error(ls_errore)
		
		return -1
	else
		commit using sqlca;
	end if
end if

if ls_flag_mese="S" and ll_mese>0 then
	if wf_elabora_mese(ll_mese,ls_errore) < 0 then
		rollback using sqlca;
		return -1
	else
		commit using sqlca;
	end if
end if

g_mb.show("Elaborazione terminata!") 

return 1
end function

public function integer wf_elabora_anno (ref string fs_errore);long 							ll_anno_rif,ll_mese_inizio,ll_mese_fine,ll_gg_ddt,ll_giorno_oggi
uo_premio_produzione		luo_premio
datetime						ldtm_data_elab,ldtm_ora_elab,ldtm_array[],ldtm_data_inizio,ldtm_data_fine
string						ls_tipo,ls_CdC[]
decimal						ld_importi_CdC[],ld_totale


//genero data/ora elaborazione on-line FLrep --------------------------
ldtm_data_elab = datetime(date(today()), 00:00:00)
ldtm_ora_elab = datetime(date(1900, 1, 1), now())
//---------------------------------------------------------------------

ll_gg_ddt = dw_selezione.getitemnumber(1, "num_gg_ddt")

//giorno del mese precedente a quello corrente, dopo il quale inizia a considerare le fatture al posto dei D.d.T. (report mensile RP)
if isnull(ll_gg_ddt) or ll_gg_ddt<0 then
	ll_gg_ddt = 10		//default
end if


//calcola sul bollettato per il mese corrente, mentre sul fatturato per i mesi precedenti
//fino a gennaio (inizio anno)
ll_mese_inizio = 1  					//il mese inizio è sempre gennaio
ll_mese_fine = month(today()) 	//il mese fine è sempre quello corrente
ll_anno_rif = year(today())
ll_giorno_oggi = day(today())		//numero del giorno del mese corrente

//ripulisci la premi_rp_online, dei dati relativi all'elaborazione dell'anno di riferimento
delete from premi_rp_online
where cod_azienda=:s_cs_xx.cod_azienda and
		year(data_giorno)=:ll_anno_rif;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in cancellazione premi_rp_online: "+sqlca.sqlerrtext
	return -1
end if

luo_premio = create uo_premio_produzione

do while ll_mese_inizio <= ll_mese_fine
	luo_premio.uof_get_inizio_fine_mese(ll_mese_inizio, ll_anno_rif, ldtm_data_inizio, ldtm_data_fine, ldtm_array[])
	
	//se (siamo nel mese corrente) oppure (nel mese precedente a quello corrente e giorno inferiore o uguale a ll_gg_ddt)
	//elabora bollettato/fatturato mensile ------------------------------------------------
	if ll_mese_inizio = ll_mese_fine or &
				((ll_mese_inizio = ll_mese_fine - 1) and ll_giorno_oggi<=ll_gg_ddt ) then
		ls_tipo = "B"		//calcola sui DDT
	else
		ls_tipo = "F"		//calcola sulle Fatture
	end if
	
//	//elabora bollettato/fatturato mensile ------------------------------------------------
//	if ll_mese_inizio = ll_mese_fine then
//		ls_tipo = "B"		//calcola sui DDT
//	else
//		ls_tipo = "F"		//calcola sulle Fatture
//	end if
	
	
	
	//luo_premio.ii_verbose = 2
	if luo_premio.uof_fatbol_cdc(ldtm_data_inizio,&
											ldtm_data_fine,&
											ls_tipo,ls_CdC[],ld_importi_CdC[],ld_totale, fs_errore) < 0 then
		destroy luo_premio;
		
		//in fs_errore il messaggio
		return -1
	end if
	//--------------------------------------------------------------------------------
	
	//elabora OLrep (N.B per tutti i CdC, nel periodo considerato)
	//per ogni CdC, preleva i dati dal vettore ed inserisci in premi_rp_online
	if luo_premio.uof_rp_cdc_online(ldtm_data_inizio,&
											ldtm_data_fine,&
											ls_CdC[], ld_importi_CdC[], ld_totale, ls_tipo, fs_errore) < 0 then
		destroy luo_premio;									
		
		//in fs_errore il messaggio
		return -1
	end if
	
	//passa la mese successivo
	ll_mese_inizio += 1
	
loop

//-------------------------------------------------------------------------------------------
update premi_rp_online
set 	data_elab = :ldtm_data_elab,
		ora_elab = :ldtm_ora_elab
where cod_azienda=:s_cs_xx.cod_azienda and
		year(data_giorno)=:ll_anno_rif;
		
if sqlca.sqlcode<0 then
	destroy luo_premio;
	fs_errore="Errore in aggiornamento data/ora elaborazione premi_rp_online: "+sqlca.sqlerrtext
	return -1
end if
//-------------------------------------------------------------------------------------------

destroy luo_premio;

return 1
end function

public function integer wf_elabora_mese (long fl_mese, ref string fs_errore);datetime						ldtm_data_elab,ldtm_ora_elab,ldtm_data_inizio,ldtm_data_fine,ldtm_array[]
uo_premio_produzione		luo_premio
long							ll_anno_rif
string						ls_CdC[]
decimal						ld_importi_CdC[],ld_totale



//genero data/ora elaborazione on-line FLrep --------------------------
ldtm_data_elab = datetime(date(today()), 00:00:00)
ldtm_ora_elab = datetime(date(1900, 1, 1), now())
//---------------------------------------------------------------------
////QUOTIDIANO (sul mese corrente e sulle righe ordini chiusi da produzione)
//ll_mese_inizio = month(today())	//mese corrente
//ll_mese_fine = ll_mese_inizio		//mese inizio e fine coincidono col mese corrente
ll_anno_rif = year(today())

//ripulisci la premi_rp_mese_online, dei dati relativi all'elaborazione nel mese
delete from premi_rp_mese_online
where cod_azienda=:s_cs_xx.cod_azienda and
		year(data_giorno)=:ll_anno_rif and
		month(data_giorno)=:fl_mese;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in cancellazione premi_rp_mese_online: "+sqlca.sqlerrtext
	
	return -1
end if

luo_premio = create uo_premio_produzione

//get inizio-fine mese
luo_premio.uof_get_inizio_fine_mese(fl_mese, ll_anno_rif, ldtm_data_inizio, ldtm_data_fine, ldtm_array[])

do while ldtm_data_inizio <= ldtm_data_fine
	
	//elabora bollettato del giorno ------------------------------------------------
		
	//luo_premio.ii_verbose = 2
	if luo_premio.uof_ordini_cdc(ldtm_data_inizio,&
											ldtm_data_inizio,&
											ls_CdC[],ld_importi_CdC[],ld_totale, fs_errore) < 0 then
		destroy luo_premio;
		
		//in fs_errore il messaggio
		return -1
	end if
	//--------------------------------------------------------------------------------
	
	//elabora OLrep (N.B per tutti i CdC, nel periodo considerato)
		
	//per ogni CdC, preleva i dati dal vettore ed inserisci in premi_rp_mese_online
	if luo_premio.uof_rp_cdc_mese_online(	ldtm_data_inizio,&
														ldtm_data_inizio,&
														ls_CdC[], ld_importi_CdC[], ld_totale, fs_errore) < 0 then
		destroy luo_premio;
		
		//in fs_errore il messaggio
		return -1
	end if
	
	//passa al giorno successivo
	ldtm_data_inizio = datetime(relativedate(date(ldtm_data_inizio), 1), 00:00:00)
	
loop

destroy luo_premio;

//-------------------------------------------------------------------------------------------
update premi_rp_mese_online
set 	data_elab = :ldtm_data_elab,
		ora_elab = :ldtm_ora_elab
where cod_azienda=:s_cs_xx.cod_azienda and
			year(data_giorno)=:ll_anno_rif and
			month(data_giorno)=:fl_mese;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in aggiornamento data/ora elaborazione premi_rp_mese_online: "+sqlca.sqlerrtext
	
	return -1
end if
//-------------------------------------------------------------------------------------------

return 1
end function

on w_premi_gestione_rp.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_elabora=create cb_elabora
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_elabora
end on

on w_premi_gestione_rp.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_elabora)
end on

event pc_setwindow;call super::pc_setwindow;
set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
													
// *** folder													

iuo_dw_main = dw_selezione

postevent("ue_set_mese_corrente")
end event

type dw_selezione from uo_cs_xx_dw within w_premi_gestione_rp
integer x = 37
integer y = 52
integer width = 2619
integer height = 828
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_premi_gestione_rp_sel"
boolean border = false
end type

type cb_elabora from commandbutton within w_premi_gestione_rp
integer x = 1143
integer y = 716
integer width = 347
integer height = 92
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;
wf_elabora()
end event

