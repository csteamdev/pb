﻿$PBExportHeader$w_premi_periodo.srw
forward
global type w_premi_periodo from w_cs_xx_principale
end type
type cb_vfsa from commandbutton within w_premi_periodo
end type
type cb_premi_periodo_aree from commandbutton within w_premi_periodo
end type
type dw_premi_periodo from uo_cs_xx_dw within w_premi_periodo
end type
end forward

global type w_premi_periodo from w_cs_xx_principale
integer width = 2967
integer height = 1280
string title = "Configurazione Premi - Periodo"
cb_vfsa cb_vfsa
cb_premi_periodo_aree cb_premi_periodo_aree
dw_premi_periodo dw_premi_periodo
end type
global w_premi_periodo w_premi_periodo

on w_premi_periodo.create
int iCurrent
call super::create
this.cb_vfsa=create cb_vfsa
this.cb_premi_periodo_aree=create cb_premi_periodo_aree
this.dw_premi_periodo=create dw_premi_periodo
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_vfsa
this.Control[iCurrent+2]=this.cb_premi_periodo_aree
this.Control[iCurrent+3]=this.dw_premi_periodo
end on

on w_premi_periodo.destroy
call super::destroy
destroy(this.cb_vfsa)
destroy(this.cb_premi_periodo_aree)
destroy(this.dw_premi_periodo)
end on

event pc_setwindow;call super::pc_setwindow;dw_premi_periodo.set_dw_key("cod_azienda")
dw_premi_periodo.set_dw_key("anno_riferimento")

dw_premi_periodo.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)
									
iuo_dw_main = dw_premi_periodo
end event

type cb_vfsa from commandbutton within w_premi_periodo
integer x = 2226
integer y = 1072
integer width = 663
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "VFSA rif. nel periodo"
end type

event clicked;window_open_parm(w_premi_periodo_depositi, -1, dw_premi_periodo)
end event

type cb_premi_periodo_aree from commandbutton within w_premi_periodo
integer x = 37
integer y = 1072
integer width = 663
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "RP rif. nel periodo"
end type

event clicked;window_open_parm(w_premi_periodo_aree, -1, dw_premi_periodo)
end event

type dw_premi_periodo from uo_cs_xx_dw within w_premi_periodo
integer x = 32
integer y = 36
integer width = 2857
integer height = 1016
integer taborder = 10
string dataobject = "d_premi_periodo_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long ll_anno_riferimento

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")

setitem(getrow(), "anno_riferimento", ll_anno_riferimento)
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_riferimento

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")

parent.title = "Anno Rif."+string(ll_anno_riferimento)+" - Param.Statistico Fatt.Lordo atteso (VFLA) nel periodo"

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_riferimento)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_riferimento, ll_prog

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(getitemnumber(ll_i, "anno_riferimento")) or &
      getitemnumber(ll_i, "anno_riferimento") = 0 then
      setitem(ll_i, "anno_riferimento", ll_anno_riferimento)
   end if
	
	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo")<=0 then
		
		select max(progressivo)
		into:ll_prog
		from premi_periodo
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_riferimento=:ll_anno_riferimento;
		
		if sqlca.sqlcode<0 then
			g_mb.error("Errore in lettura max progressivo:"+sqlca.sqlerrtext)
			return
		end if
		
		if isnull(ll_prog) then ll_prog = 0
		ll_prog += 1
		
      setitem(ll_i, "progressivo", ll_prog)
   end if
	
	
next

end event

event pcd_validaterow;call super::pcd_validaterow;datetime ldt_data_dal, ldt_data_al
long ll_index
decimal ld_vfla, ld_t, ld_is, ld_ix

for ll_index = 1 to rowcount()
	
	ldt_data_dal = getitemdatetime(ll_index, "data_dal")
	if isnull(ldt_data_dal) or year(date(ldt_data_dal))<=1950 then
		g_mb.error("Attenzione", "Immettere una data inizio periodo valida!")
		
		pcca.error = c_fatal
		return
	end if
	
	ldt_data_al = getitemdatetime(ll_index, "data_al")
	if isnull(ldt_data_al) or year(date(ldt_data_al))<=1950 then
		g_mb.error("Attenzione", "Immettere una data fine periodo valida!")
		
		pcca.error = c_fatal
		return
	end if
	
	if ldt_data_al <= ldt_data_dal then
		g_mb.error("Attenzione", "La data fine periodo deve essere superiore a quella inizio!")
		
		pcca.error = c_fatal
		return
	end if
	
	//-------------
	ld_vfla = getitemdecimal(ll_index, "vfla")
	if isnull(ld_vfla) or ld_vfla<=0 then
		g_mb.error("Attenzione", "Inserire il valore del fatturato lordo atteso (VFLA) nel periodo!")
		
		pcca.error = c_fatal
		return
	end if
	
	//-------------
	ld_t = getitemdecimal(ll_index, "t")
	if isnull(ld_t) or ld_t<=0 then
		g_mb.error("Attenzione", "Inserire il valore % di BPT (T) per l'area e il periodo!")
		
		pcca.error = c_fatal
		return
	end if
	
	if ld_t>=0 and ld_t<=100 then
		//OK
	else
		g_mb.error("Attenzione", "il valore % di BPT (T) deve essere compreso tra 0 e 100!")
		
		pcca.error = c_fatal
		return
	end if
	
	//-------------
	ld_is = getitemdecimal(ll_index, "is_area")
	if isnull(ld_is) or ld_is<0 then
		g_mb.error("Attenzione", "Inserire il valore minimo di produttività (IS) atteso nel periodo per l'area!")
		
		pcca.error = c_fatal
		return
	end if
	
	if ld_is>=0 and ld_is<=1 then
	else
		g_mb.error("Attenzione", "Il valore minimo di produttività (IS) atteso nel periodo per l'area deve essere compreso tra 0 e 1!")
		
		pcca.error = c_fatal
		return
	end if
	
	//-------------
	ld_ix = getitemdecimal(ll_index, "ix_area")
	if isnull(ld_ix) or ld_ix<0 then
		g_mb.error("Attenzione", "Inserire il valore massimo di produttività (IX) atteso nel periodo per l'area!")
		
		pcca.error = c_fatal
		return
	end if
	
	if ld_ix>=1 and ld_ix<=2 then
	else
		g_mb.error("Attenzione", "Il valore massimo di produttività (IX) atteso nel periodo per l'area deve essere compreso tra 1 e 2!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

