﻿$PBExportHeader$w_premi_periodo_depositi.srw
forward
global type w_premi_periodo_depositi from w_cs_xx_principale
end type
type dw_premi_periodo_depositi from uo_cs_xx_dw within w_premi_periodo_depositi
end type
end forward

global type w_premi_periodo_depositi from w_cs_xx_principale
integer width = 2816
integer height = 964
string title = "Configurazione Premi - VSFE di rif. nel periodo"
dw_premi_periodo_depositi dw_premi_periodo_depositi
end type
global w_premi_periodo_depositi w_premi_periodo_depositi

on w_premi_periodo_depositi.create
int iCurrent
call super::create
this.dw_premi_periodo_depositi=create dw_premi_periodo_depositi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_premi_periodo_depositi
end on

on w_premi_periodo_depositi.destroy
call super::destroy
destroy(this.dw_premi_periodo_depositi)
end on

event pc_setwindow;call super::pc_setwindow;dw_premi_periodo_depositi.set_dw_key("cod_azienda")
dw_premi_periodo_depositi.set_dw_key("anno_riferimento")
dw_premi_periodo_depositi.set_dw_key("progressivo")

dw_premi_periodo_depositi.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)
									
iuo_dw_main = dw_premi_periodo_depositi
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_premi_periodo_depositi, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_premi_periodo_depositi from uo_cs_xx_dw within w_premi_periodo_depositi
integer x = 32
integer y = 36
integer width = 2720
integer height = 792
integer taborder = 10
string dataobject = "d_premi_periodo_depositi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;long ll_anno_riferimento, ll_progressivo

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

setitem(getrow(), "anno_riferimento", ll_anno_riferimento)
setitem(getrow(), "progressivo", ll_progressivo)
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_riferimento, ll_progressivo
datetime ldt_data_dal, ldt_data_al
string ls_data_dal, ls_data_al

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")
ldt_data_dal = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_dal")
ldt_data_al = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_al")

ls_data_dal = string(ldt_data_dal, "dd/mm/yy")
ls_data_al = string(ldt_data_al, "dd/mm/yy")

parent.title = "Configurazione Premi - VFSA Stabilimenti di rif. nel periodo dal "+ls_data_dal+" al "+ls_data_al

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_riferimento, ll_progressivo)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_riferimento, ll_progressivo

ll_anno_riferimento = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_riferimento")
ll_progressivo = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "progressivo")

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(getitemnumber(ll_i, "anno_riferimento")) or getitemnumber(ll_i, "anno_riferimento") = 0 then
      setitem(ll_i, "anno_riferimento", ll_anno_riferimento)
   end if
	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo") = 0 then
      setitem(ll_i, "progressivo", ll_progressivo)
   end if
	
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
decimal  ld_vfsa
string ls_cod_deposito

for ll_index = 1 to rowcount()
	
	ls_cod_deposito = getitemstring(ll_index, "cod_deposito")
	if isnull(ls_cod_deposito) or ls_cod_deposito="" then
		g_mb.error("Attenzione", "Selezionare il deposito/stabilimento!")
		
		pcca.error = c_fatal
		return
	end if
	
	ld_vfsa = getitemdecimal(ll_index, "vfsa_deposito")
	if isnull(ld_vfsa) or ld_vfsa<=0 then
		g_mb.error("Attenzione", "Inserire il valore del fatturato di stabilimento atteso (VFSA) atteso nel periodo per il deposito!")
		
		pcca.error = c_fatal
		return
	end if
	
next
end event

