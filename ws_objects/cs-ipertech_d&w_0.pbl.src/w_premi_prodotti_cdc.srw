﻿$PBExportHeader$w_premi_prodotti_cdc.srw
forward
global type w_premi_prodotti_cdc from w_cs_xx_principale
end type
type st_4 from statictext within w_premi_prodotti_cdc
end type
type st_3 from statictext within w_premi_prodotti_cdc
end type
type dw_lista from uo_cs_xx_dw within w_premi_prodotti_cdc
end type
end forward

global type w_premi_prodotti_cdc from w_cs_xx_principale
integer width = 1897
integer height = 1220
string title = "Suddivisione Fatturato per Cdc in % - dettaglio"
st_4 st_4
st_3 st_3
dw_lista dw_lista
end type
global w_premi_prodotti_cdc w_premi_prodotti_cdc

on w_premi_prodotti_cdc.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_3=create st_3
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.dw_lista
end on

on w_premi_prodotti_cdc.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;//f_po_loaddddw_dw(dw_lista, &
//                 "cod_centro_costo", &
//                 sqlca, &
//                 "tab_centri_costo", &
//                 "cod_centro_costo", &
//                 "des_centro_costo", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//
//
end event

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("regola")

dw_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_default)
									
iuo_dw_main = dw_lista
end event

type st_4 from statictext within w_premi_prodotti_cdc
integer x = 91
integer y = 128
integer width = 1655
integer height = 220
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "il sistema sostituirà il carattere % con la lettera dello stabilimento corretta (C-S-V-P). Successivamente leggerà il codice del relativo centro di costo dalla anagrafica reparti per il reparto cosi ottenuto"
boolean focusrectangle = false
end type

type st_3 from statictext within w_premi_prodotti_cdc
integer x = 91
integer y = 48
integer width = 1655
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 134217856
long backcolor = 12632256
string text = "%_CF per indicare, ad es., il centro di costo confezioni generico"
boolean focusrectangle = false
end type

type dw_lista from uo_cs_xx_dw within w_premi_prodotti_cdc
integer x = 27
integer y = 372
integer width = 1810
integer height = 724
integer taborder = 10
string dataobject = "d_premi_cdc_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")
parent.title = "Percentuali per regola '"+ls_regola+"'"

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_regola)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i
string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(getitemstring(ll_i, "regola")) or getitemstring(ll_i, "regola") = "" then
		setitem(ll_i, "regola", ls_regola)
	end if
   
next
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_index
decimal ld_perc
string ls_cod_cdc

for ll_index = 1 to rowcount()
	
	ls_cod_cdc = getitemstring(ll_index, "cod_centro_costo")
	if isnull(ls_cod_cdc) or ls_cod_cdc="" then
		g_mb.error("Attenzione", "Selezionare il Centro di Costo!")
		
		pcca.error = c_fatal
		return
	end if
	
	ld_perc = getitemdecimal(ll_index, "percentuale")
	if isnull(ld_perc) or ld_perc<=0 then
		g_mb.error("Attenzione", "Inserire la percentuale per il Centro di Costo!")
		
		pcca.error = c_fatal
		return
	end if
next
end event

event pcd_new;call super::pcd_new;string ls_regola

ls_regola = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "regola")

setitem(getrow(), "regola", ls_regola)
end event

