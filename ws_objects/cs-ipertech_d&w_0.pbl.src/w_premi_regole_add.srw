﻿$PBExportHeader$w_premi_regole_add.srw
forward
global type w_premi_regole_add from w_cs_xx_principale
end type
type cb_percentuali from commandbutton within w_premi_regole_add
end type
type dw_lista from uo_cs_xx_dw within w_premi_regole_add
end type
end forward

global type w_premi_regole_add from w_cs_xx_principale
integer width = 3136
integer height = 1028
string title = "Suddivisione Fatturato (REGOLE AGGIUNTIVE)"
cb_percentuali cb_percentuali
dw_lista dw_lista
end type
global w_premi_regole_add w_premi_regole_add

on w_premi_regole_add.create
int iCurrent
call super::create
this.cb_percentuali=create cb_percentuali
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_percentuali
this.Control[iCurrent+2]=this.dw_lista
end on

on w_premi_regole_add.destroy
call super::destroy
destroy(this.cb_percentuali)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_options(	sqlca, &
									pcca.null_object, &
									c_default, + &
									c_default)
end event

type cb_percentuali from commandbutton within w_premi_regole_add
integer x = 2661
integer y = 828
integer width = 402
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Percentuali"
end type

event clicked;window_open_parm(w_premi_cdc_add, -1, dw_lista)
end event

type dw_lista from uo_cs_xx_dw within w_premi_regole_add
integer x = 27
integer y = 24
integer width = 3035
integer height = 788
integer taborder = 10
string dataobject = "d_premi_prodotti_regole_add"
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   
next

end event

event pcd_delete;//ancestor script disattivato perchè non deve essere eseguito
return

//cb_percentuali.enabled = false
end event

event pcd_new;//ancestor script disattivato perchè non deve essere eseguito
return

//cb_percentuali.enabled = false
end event

event pcd_modify;call super::pcd_modify;
cb_percentuali.enabled = false
end event

event pcd_view;call super::pcd_view;
cb_percentuali.enabled = true
end event

