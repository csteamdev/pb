﻿$PBExportHeader$w_premi_report_lettere.srw
forward
global type w_premi_report_lettere from w_cs_xx_principale
end type
type st_msg from statictext within w_premi_report_lettere
end type
type st_1 from statictext within w_premi_report_lettere
end type
type dw_sel_dipen from datawindow within w_premi_report_lettere
end type
type dw_sel_cdc from datawindow within w_premi_report_lettere
end type
type cb_stampa from commandbutton within w_premi_report_lettere
end type
type dw_selezione from uo_cs_xx_dw within w_premi_report_lettere
end type
type cb_report from commandbutton within w_premi_report_lettere
end type
type dw_report from uo_cs_xx_dw within w_premi_report_lettere
end type
type dw_folder from u_folder within w_premi_report_lettere
end type
end forward

global type w_premi_report_lettere from w_cs_xx_principale
integer width = 5111
integer height = 2384
string title = "Lettere Premi"
boolean resizable = false
event ue_set_anno_rif ( )
st_msg st_msg
st_1 st_1
dw_sel_dipen dw_sel_dipen
dw_sel_cdc dw_sel_cdc
cb_stampa cb_stampa
dw_selezione dw_selezione
cb_report cb_report
dw_report dw_report
dw_folder dw_folder
end type
global w_premi_report_lettere w_premi_report_lettere

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables
string is_azienda_db_presenze = ""

//valore della colonna gruppo in tabella dipen, per fare in modo che le ore
//ordinarie, starordinarie, flessibili e diaria non vengano conteggiate
//nel calcolo di OL reparto e quindi RP reparto
string is_escludi_da_rp = ""			//"QUADRO"
end variables

forward prototypes
public function integer wf_sel_desel_dipendenti (string fs_cdc, string fs_sel)
public function integer wf_report (ref string fs_errore)
public function string wf_oggetto ()
public function integer wf_retrieve_cdc_dipen (ref string fs_errore)
public function integer wf_premi_mese (long fl_row, string fs_matricola, long fl_mese, long fl_anno, boolean fb_anno_prec, ref string fs_errore)
public function string wf_testo (datetime fdt_data, ref string fs_mese_rif, ref string fs_mese_prec_rif)
end prototypes

event ue_set_anno_rif();datetime ldt_data
long ll_anno, ll_mese

ll_anno = year(today())
ll_mese = month(today())

if ll_mese = 1 then
	//siamo a gennaio, quindi vai a dicembre anno precedente
	ll_anno = ll_anno - 1
	ll_mese = 12
else
	//vai a mese precedente
	ll_mese = ll_mese - 1
end if

ldt_data = datetime(date(ll_anno, ll_mese, 1), 00:00:00)
dw_selezione.setitem(1, "mese_anno", ldt_data)


end event

public function integer wf_sel_desel_dipendenti (string fs_cdc, string fs_sel);long ll_index
string ls_cdc

for ll_index=1 to dw_sel_dipen.rowcount()
	
	if fs_cdc = "SEL-DESEL-TUTTI" then
		//indipendentemente dal centro di costo estendi l'azione a tutti
		dw_sel_dipen.setitem(ll_index, "sel", fs_sel)
		
	else
		//estendi all'azione solo ai dipendenti del centro di costo passato
		ls_cdc = dw_sel_dipen.getitemstring(ll_index, "cenco")
		
		if ls_cdc=fs_cdc then
			dw_sel_dipen.setitem(ll_index, "sel", fs_sel)
		end if
		
	end if
	
next

return 1
end function

public function integer wf_report (ref string fs_errore);string ls_matricola,ls_cod_centro_costo, ls_errore
long ll_mese_inizio, ll_mese_fine, ll_anno_inizio, ll_anno_fine,&
		ll_index, ll_index2, ll_new,ll_tot, ll_count_tot
uo_premio_produzione luo_premio

string ls_cdc, ls_des_cdc, ls_matr, ls_dip
string ls_sql, ls_sel, ls_azienda, ls_oggetto, ls_testo
datastore lds_premio
integer li_mese
decimal ld_premio, ld_erogato, ld_maturato, ld_ip
datetime ldt_data_erogazione, ldt_data_premi, ldt_inizio[], ldt_fine[]
boolean lb_almeno_uno

dw_selezione.accepttext()
dw_sel_dipen.accepttext()
dw_sel_cdc.accepttext()

st_msg.text = "Inizio elaborazione ..."
dw_report.reset()

lb_almeno_uno = false
ll_tot = dw_sel_dipen.rowcount()
ll_count_tot = 0
//ll_count = 0

ldt_data_premi = dw_selezione.getitemdatetime(1, "mese_anno")
if isnull(ldt_data_premi) or year(date(ldt_data_premi)) < 2000 then
	g_mb.error("Selezionare la data (Mese/Anno) di riferimento per le lettere premi!")
	st_msg.text = "Selezionare la data (Mese/Anno) di riferimento per le lettere premi!"
	return 0
end if
ll_anno_inizio=year(date(ldt_data_premi))

if ll_anno_inizio<=2000 then
	g_mb.error("Selezionare l'anno di riferimento (> di 2000)!")
	st_msg.text = "Selezionare l'anno di riferimento (> di 2000)!"
end if

//recupero le date dei periodi
luo_premio = create uo_premio_produzione
luo_premio.uof_get_periodi_lettera(ll_anno_inizio,  ldt_inizio[], ldt_fine[])
destroy luo_premio;
//----------------------------------

for ll_index=1 to ll_tot
	Yield()
	
	ls_sel = dw_sel_dipen.getitemstring(ll_index, "sel")
	if ls_sel="S" then
		lb_almeno_uno = true
		
		//non esco più dal ciclo perchè mi serve sapere quanti dipendenti devo elaborare
		//exit
		ll_count_tot += 1
	end if
next

if not lb_almeno_uno then
	g_mb.error("Selezionare almeno un dipendente oppure un Centro di Costo!")
	st_msg.text = "Selezionare almeno un dipendente oppure un Centro di Costo!"
	
	return 0
end if

//inizio costruzione report ----------------------------------------------------------------
select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura ragione sociale azienda: "+sqlca.sqlerrtext
	return -1
end if


ls_oggetto = wf_oggetto()
ls_testo = "Con la presente le forniamo uno schema riassuntivo del totale premio produzione maturato "+&
				"relativamente all'anno "+string(ll_anno_inizio)

for ll_index=1 to ll_tot //upperbound(ls_matr[])
	
	ls_sel = dw_sel_dipen.getitemstring(ll_index, "sel")
	if ls_sel<>"S" then continue
	
	ls_cdc = dw_sel_dipen.getitemstring(ll_index, "cenco")
	ls_des_cdc = dw_sel_dipen.getitemstring(ll_index, "des_cenco")
	ls_matr = dw_sel_dipen.getitemstring(ll_index, "matricola")
	ls_dip = dw_sel_dipen.getitemstring(ll_index, "cognome") + " " + &
						dw_sel_dipen.getitemstring(ll_index, "nome")
	
	st_msg.text = "Elaborazione Dipendente "+ls_dip
	
	//VIA ###############################################################
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new,"oggetto", ls_oggetto)
	dw_report.setitem(ll_new,"testo", ls_testo)
	dw_report.setitem(ll_new,"azienda", ls_azienda)
	
	dw_report.setitem(ll_new,"centro_costo",ls_cdc+" "+ls_des_cdc)
	dw_report.setitem(ll_new,"dipendente",ls_dip)
	dw_report.setitem(ll_new,"premio","VIA")
	
	ls_sql = "select mese_riferimento,via "+&
				"from premi_operai_via "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//elabora le ultime 3 colonne di riepilogo
	//-------------------------------------------------------------
	ldt_data_erogazione = datetime(date(ll_anno_inizio,7,31),00:00:00)  //31 luglio
	
	//1)
	//erogato prima del 12/8
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_erogato)
	
	//-------------------------------------------------------------
	
	//2)
	//erogato il 12/8
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_erogato)
	//-------------------------------------------------------------
	
	//3)erogato tra 12/8escluso e 15/12escluso
	select sum(via_erogato)
	into :ld_erogato
	from premi_operai_via
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
			
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_erogato)
	
	
	//VIO ###############################################################
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new,"oggetto", ls_oggetto)
	dw_report.setitem(ll_new,"testo", ls_testo)
	dw_report.setitem(ll_new,"azienda", ls_azienda)
	
	dw_report.setitem(ll_new,"centro_costo",ls_cdc+" "+ls_des_cdc)
	dw_report.setitem(ll_new,"dipendente",ls_dip)
	dw_report.setitem(ll_new,"premio","VIO")
	
	ls_sql = "select mese_riferimento,vio "+&
				"from premi_operai_vio "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//elabora le ultime 3 colonne di riepilogo
	//-------------------------------------------------------------
	ldt_data_erogazione = datetime(date(ll_anno_inizio,7,31),00:00:00)  //31 luglio
	
	//1)
	//erogato prima del 12/8
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_erogato)
	
	//-------------------------------------------------------------
	
	//2) 
	//erogato il 12/8
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_erogato)
	
	//-------------------------------------------------------------
	
	//3) 
	//erogato tra il 12/8escluso e 15/12escluso
	select sum(vio_erogato)
	into :ld_erogato
	from premi_operai_vio
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
	
	if isnull(ld_erogato) then ld_erogato = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_erogato)
	
	
	//VPT ###############################################################
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new,"oggetto", ls_oggetto)
	dw_report.setitem(ll_new,"testo", ls_testo)
	dw_report.setitem(ll_new,"azienda", ls_azienda)
	
	dw_report.setitem(ll_new,"centro_costo",ls_cdc+" "+ls_des_cdc)
	dw_report.setitem(ll_new,"dipendente",ls_dip)
	dw_report.setitem(ll_new,"premio","VPT")
	
	ls_sql = "select mese_riferimento,vpt,ip "+&
				"from premi_operai_vpt "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"matricola='"+ls_matr+"' and "+&
						"anno_riferimento="+string(ll_anno_inizio)+" "+&
				"order by mese_riferimento "
	if not f_crea_datastore(lds_premio, ls_sql) then
		//errore datastore, messaggio già dato
		return -1
	end if
	
	lds_premio.retrieve()
	ld_ip = 0
	for ll_index2=1 to lds_premio.rowcount()
		
		li_mese = lds_premio.getitemnumber(ll_index2, 1)
		ld_premio = lds_premio.getitemdecimal(ll_index2, 2)
		
		//conserva IP del mese più alto
		ld_ip = lds_premio.getitemdecimal(lds_premio.rowcount(), 3)
		dw_report.setitem(ll_new,"ip",ld_ip)
		
		dw_report.setitem(ll_new,"mese_"+string(li_mese),ld_premio)
		
	next
	destroy lds_premio;
	
	//VPT maturato: vpt relativo al mese più alto finora calcolato (vpt_maturato)
	integer li_mese_rif
	
	select max(mese_riferimento)
	into :li_mese_rif
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio;
			
	if li_mese_rif>0 then
		select vpt
		into :ld_premio
		from premi_operai_vpt
		where cod_azienda=:s_cs_xx.cod_azienda and
			matricola=:ls_matr and 
			anno_riferimento=:ll_anno_inizio and
			mese_riferimento=:li_mese_rif;
			
		dw_report.setitem(ll_new,"vpt_maturato",ld_premio)
			
	end if
	
	//1)
	//VPT erogato (prima del 12/8)
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[1] and data_erogazione<=:ldt_fine[1];
		//month(data_erogazione)<>8;
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"erogato_luglio",ld_premio)
	
	//2)
	//VPT erogato (al 12/8)
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[2] and data_erogazione<=:ldt_fine[2];
		//month(data_erogazione)=8;
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"da_erog_ago",ld_premio)
	
	
	//3)
	//VPT erogato (tra 12/8escluso e 15/12 escluso)
	select sum(vpt_erogato)
	into :ld_premio
	from premi_operai_vpt
	where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:ls_matr and 
		anno_riferimento=:ll_anno_inizio and
		data_erogazione >=:ldt_inizio[3] and data_erogazione<=:ldt_fine[3];
		
	if isnull(ld_premio) then ld_premio = 0
	dw_report.setitem(ll_new,"da_erog_ago_nov",ld_premio)
	
next

dw_report.sort()
dw_report.groupcalc()

dw_report.object.datawindow.print.preview = "Yes"
end function

public function string wf_oggetto ();string ls_oggetto

ls_oggetto = "Aggiornamento premio produzione"

return ls_oggetto
end function

public function integer wf_retrieve_cdc_dipen (ref string fs_errore);string ls_odbc_presenze, ls_uid_presenze, ls_pwd_presenze, ls_dbparm
transaction ltran_presenze
long ll_index

//-------------------------------------------------------
select stringa
into :ls_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro DBP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or ls_odbc_presenze="" or isnull(ls_odbc_presenze) then
	fs_errore = "Parametro DBP (odbc DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if

//legge dal parametro aziendale UTP (UID database presenze)
select stringa
into :ls_uid_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='UTP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro UTP: "+sqlca.sqlerrtext
	return -1
	
end if
if isnull(ls_uid_presenze) then ls_uid_presenze = ""

//legge dal parametro aziendalePAP (password database presenze)
select stringa
into :ls_pwd_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='PAP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro PAP: "+sqlca.sqlerrtext
	return -1
	
end if
if isnull(ls_pwd_presenze) then ls_pwd_presenze = ""


//recupero il codice azienda database presenze ------------------------------------
select stringa
into :is_azienda_db_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='ADP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro ADP: "+sqlca.sqlerrtext
	
   return -1
	
elseif sqlca.sqlcode=100 or is_azienda_db_presenze="" or isnull(is_azienda_db_presenze) then
	fs_errore = "Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	
   return -1
end if
//-------------------------------------------------------

ltran_presenze = create transaction

ltran_presenze.dbms = "ODBC"
ltran_presenze.autocommit = false

//ltran_presenze.dbparm = "Connectstring='DSN="+ls_odbc_presenze+"',DisableBind=1"
ls_dbparm = "ConnectString='DSN="+ls_odbc_presenze

if ls_uid_presenze<>"" then
	ls_dbparm += ";UID="+ls_uid_presenze
end if

if ls_pwd_presenze <> "" then
	ls_dbparm += ";PWD="+ls_pwd_presenze
end if

ls_dbparm+= "',CommitOnDisconnect='No',DisableBind=1"

ltran_presenze.dbparm = ls_dbparm

if f_po_connect(ltran_presenze, true) <> 0 then
	fs_errore = "Errore in connessione DB presenze: "+ltran_presenze.sqlerrtext
	
   return -1
end if

//-------------------------------------------------------
//NOTA
//aggiungere eventuali filtri per non elaborare matricole che non hanno diritto al premio
//flag_diritto_a_premi='S'
dw_sel_cdc.settransobject(ltran_presenze)
dw_sel_dipen.settransobject(ltran_presenze)

dw_sel_cdc.retrieve(is_azienda_db_presenze)
dw_sel_dipen.retrieve(is_azienda_db_presenze)


for ll_index = 1 to dw_sel_dipen.rowcount()
	dw_sel_dipen.setitem(ll_index, "gruppo_escludi", is_escludi_da_rp)
next


destroy ltran_presenze;

//-------------------------------------------------------

return 1
end function

public function integer wf_premi_mese (long fl_row, string fs_matricola, long fl_mese, long fl_anno, boolean fb_anno_prec, ref string fs_errore);datetime			ldt_data_aggiornamento,ldt_data_erogazione
decimal			ld_premio,ld_erogato,ld_maturato
string			ls_note_erogazione,ls_note
integer			li_flag_erogazione
long				ll_mese_calcolo

if fb_anno_prec then
	ll_mese_calcolo = fl_mese - 1
else
	ll_mese_calcolo = fl_mese
end if


//VPT -------------------------------------------------------------------------------------------------
select data_elab,
		 data_erogazione,
		 vpt,
		 note_erogazione
into 	:ldt_data_aggiornamento,
		:ldt_data_erogazione,
		:ld_premio,
		:ls_note_erogazione
from premi_operai_vpt 
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
	anno_riferimento=:fl_anno and 
	mese_riferimento=:ll_mese_calcolo;
	
if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura premio VPT mese "+string(ll_mese_calcolo)+": "+sqlca.sqlerrtext
	return -1
end if

if year(date(ldt_data_aggiornamento))<=1950 then setnull(ldt_data_aggiornamento)
if year(date(ldt_data_erogazione))<=1950 then setnull(ldt_data_erogazione)

if not fb_anno_prec then
	if ls_note_erogazione<>"" and not isnull(ls_note_erogazione) then
		li_flag_erogazione += 1
		
		ls_note += "("+string(li_flag_erogazione)+") "+ls_note_erogazione+"~r~n"
		dw_report.setitem(fl_row, "flag_note_vpt", string(li_flag_erogazione))
	end if
	
	dw_report.setitem(fl_row, "data_agg_vpt", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "data_erog_vpt", ldt_data_erogazione)
	dw_report.setitem(fl_row, "vpt_maturato", ld_premio)
	
else
	
	dw_report.setitem(fl_row, "data_agg_vpt_prec", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "vpt_maturato_prec", ld_premio)
end if
	
	
//VIO -------------------------------------------------------------------------------------------------
select data_elab,
		 data_erogazione,
		 vio,
		 note_erogazione
into 	:ldt_data_aggiornamento,
		:ldt_data_erogazione,
		:ld_premio,
		:ls_note_erogazione
from premi_operai_vio 
where cod_azienda=:s_cs_xx.cod_azienda and
	matricola=:fs_matricola and
	anno_riferimento=:fl_anno and 
	mese_riferimento=:ll_mese_calcolo;
	
if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura premio VIO mese "+string(ll_mese_calcolo)+": "+sqlca.sqlerrtext
	return -1
end if

if year(date(ldt_data_aggiornamento))<=1950 then setnull(ldt_data_aggiornamento)
if year(date(ldt_data_erogazione))<=1950 then setnull(ldt_data_erogazione)


if not fb_anno_prec then
	if ls_note_erogazione<>"" and not isnull(ls_note_erogazione) then
		li_flag_erogazione += 1
		
		ls_note += "("+string(li_flag_erogazione)+") "+ls_note_erogazione+"~r~n"
		dw_report.setitem(fl_row, "flag_note_vio", string(li_flag_erogazione))
	end if
	
	dw_report.setitem(fl_row, "data_agg_vio", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "data_erog_vio", ldt_data_erogazione)
	dw_report.setitem(fl_row, "vio_maturato", ld_premio)
	
else
	dw_report.setitem(fl_row, "data_agg_vio_prec", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "vio_maturato_prec", ld_premio)
end if



//VIA -------------------------------------------------------------------------------------------------
select data_elab,
		 data_erogazione,
		 via,
		 note_erogazione
into 	:ldt_data_aggiornamento,
		:ldt_data_erogazione,
		:ld_premio,
		:ls_note_erogazione
from premi_operai_via 
where cod_azienda=:s_cs_xx.cod_azienda and
	matricola=:fs_matricola and
	anno_riferimento=:fl_anno and 
	mese_riferimento=:ll_mese_calcolo;
	
if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura premio VIA mese "+string(ll_mese_calcolo)+": "+sqlca.sqlerrtext
	return -1
end if

if year(date(ldt_data_aggiornamento))<=1950 then setnull(ldt_data_aggiornamento)
if year(date(ldt_data_erogazione))<=1950 then setnull(ldt_data_erogazione)

if not fb_anno_prec then
	if ls_note_erogazione<>"" and not isnull(ls_note_erogazione) then
		li_flag_erogazione += 1
		
		ls_note += "("+string(li_flag_erogazione)+") "+ls_note_erogazione+"~r~n"
		dw_report.setitem(fl_row, "flag_note_via", string(li_flag_erogazione))
	end if
	
	dw_report.setitem(fl_row, "data_agg_via", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "data_erog_via", ldt_data_erogazione)
	dw_report.setitem(fl_row, "via_maturato", ld_premio)
	
else
	dw_report.setitem(fl_row, "data_agg_via_prec", ldt_data_aggiornamento)
	dw_report.setitem(fl_row, "via_maturato_prec", ld_premio)
	
end if



//recupera il erogato ad oggi (VPT,VIO,VIA) e maturato (solo VIO e VIA)
		
//VPT erogato al mese selezionato
select sum(vpt_erogato)
into :ld_erogato
from premi_operai_vpt
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:fl_anno and
		mese_riferimento<=:ll_mese_calcolo;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura VPT erogato al mese selezionato: "+sqlca.sqlerrtext
	
	return -1
end if
if isnull(ld_erogato) then ld_erogato = 0

if not fb_anno_prec then
	dw_report.setitem(fl_row, "vpt_cumul_erogato", ld_erogato)
	
else
	
end if


//VIO erogato e maturato al mese selezionato
select sum(vio_erogato), sum(vio)
into :ld_erogato, :ld_maturato
from premi_operai_vio
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:fl_anno and
		mese_riferimento<=:ll_mese_calcolo;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura VIO erogato/maturato al mese selezionato: "+sqlca.sqlerrtext
	
	return -1
end if
if isnull(ld_erogato) then ld_erogato = 0
if isnull(ld_maturato) then ld_maturato = 0

if not fb_anno_prec then
	dw_report.setitem(fl_row, "vio_cumul_erogato", ld_erogato)
	dw_report.setitem(fl_row, "vio_cumul_maturato", ld_maturato)
else
	
end if


//VIA erogato e maturato al mese selezionato
select sum(via_erogato), sum(via)
into :ld_erogato, :ld_maturato
from premi_operai_via
where cod_azienda=:s_cs_xx.cod_azienda and
		matricola=:fs_matricola and
		anno_riferimento=:fl_anno and
		mese_riferimento<=:ll_mese_calcolo;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura VIA erogato/maturato al mese selezionato: "+sqlca.sqlerrtext
	
	return -1
end if
if isnull(ld_erogato) then ld_erogato = 0
if isnull(ld_maturato) then ld_maturato = 0

if not fb_anno_prec then
	dw_report.setitem(fl_row, "via_cumul_erogato", ld_erogato)
	dw_report.setitem(fl_row, "via_cumul_maturato", ld_maturato)
else
	
end if

//scrivi le note (solo mese di riferimneto)
if ls_note<>"" and not isnull(ls_note) and not fb_anno_prec then
	dw_report.setitem(fl_row, "annotazioni", ls_note)
end if


return 1
end function

public function string wf_testo (datetime fdt_data, ref string fs_mese_rif, ref string fs_mese_prec_rif);string ls_testo

ls_testo = " con la presente le forniamo uno schema riassuntivo del totale premio produzione maturato fino a"

choose case month(date(fdt_data))
	case 1
		ls_testo += " Gennaio "
		fs_mese_rif = "Gennaio"
		fs_mese_prec_rif = ""
		
	case 2
		ls_testo += " Febbraio "
		fs_mese_rif = "Febbraio"
		fs_mese_prec_rif = "Gennaio"
		
	case 3
		ls_testo += " Marzo "
		fs_mese_rif = "Marzo"
		fs_mese_prec_rif = "Febbraio"
		
	case 4
		ls_testo += " Aprile "
		fs_mese_rif = "Aprile"
		fs_mese_prec_rif = "Marzo"
		
	case 5
		ls_testo += " Maggio "
		fs_mese_rif = "Maggio"
		fs_mese_prec_rif = "Aprile"
		
	case 6
		ls_testo += " Giugno "
		fs_mese_rif = "Giugno"
		fs_mese_prec_rif = "Maggio"
		
	case 7
		ls_testo += " Luglio "
		fs_mese_rif = "Luglio"
		fs_mese_prec_rif = "Giugno"
		
	case 8
		ls_testo += " Agosto "
		fs_mese_rif = "Agosto"
		fs_mese_prec_rif = "Luglio"
		
	case 9
		ls_testo += " Settembre "
		fs_mese_rif = "Settembre"
		fs_mese_prec_rif = "Agosto"
		
	case 10
		ls_testo += " Ottobre "
		fs_mese_rif = "Ottobre"
		fs_mese_prec_rif = "Settembre"
		
	case 11
		ls_testo += " Novembre "
		fs_mese_rif = "Novembre"
		fs_mese_prec_rif = "Ottobre"
		
	case 12
		ls_testo += " Dicembre "
		fs_mese_rif = "Dicembre"
		fs_mese_prec_rif = "Novembre"
		
	case else
		ls_testo += " <ERRORE> "
		fs_mese_rif = ""
		fs_mese_prec_rif = ""
		
end choose

ls_testo += string(year(date(fdt_data)))


return ls_testo
end function

on w_premi_report_lettere.create
int iCurrent
call super::create
this.st_msg=create st_msg
this.st_1=create st_1
this.dw_sel_dipen=create dw_sel_dipen
this.dw_sel_cdc=create dw_sel_cdc
this.cb_stampa=create cb_stampa
this.dw_selezione=create dw_selezione
this.cb_report=create cb_report
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_msg
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_sel_dipen
this.Control[iCurrent+4]=this.dw_sel_cdc
this.Control[iCurrent+5]=this.cb_stampa
this.Control[iCurrent+6]=this.dw_selezione
this.Control[iCurrent+7]=this.cb_report
this.Control[iCurrent+8]=this.dw_report
this.Control[iCurrent+9]=this.dw_folder
end on

on w_premi_report_lettere.destroy
call super::destroy
destroy(this.st_msg)
destroy(this.st_1)
destroy(this.dw_sel_dipen)
destroy(this.dw_sel_cdc)
destroy(this.cb_stampa)
destroy(this.dw_selezione)
destroy(this.cb_report)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path, ls_database, ls_path_logo_1, ls_errore, ls_modify
windowobject lw_oggetti[], lw_vuoto[]
long li_risposta

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
													
// *** folder													

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_report
lw_oggetti[3] = st_1
lw_oggetti[4] = dw_sel_cdc
lw_oggetti[5] = dw_sel_dipen
lw_oggetti[6] = st_msg
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti = lw_vuoto

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_report

postevent("ue_set_anno_rif")


//recupero i gruppi da escludere nella elaborazione di OLrep ------------------------------------
//li visualizzo in rosso nella lista dipendenti
select stringa
into :is_escludi_da_rp
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='GDP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	//fs_errore = "Errore in lettura parametro GDP: "+sqlca.sqlerrtext
	g_mb.error("Errore in lettura parametro GDP, gruppi da escludere per OL: "+sqlca.sqlerrtext)
	return
	
elseif sqlca.sqlcode=100 or is_escludi_da_rp="" or isnull(is_escludi_da_rp) then
	//fs_errore = "Parametro GDP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	g_mb.error("Parametro ADP (gruppi da escludere per OL) non trovato oppure "+&
								"non impostato nella tabella parametri aziendali!")
	return
	
end if
//##################################################################################################


if wf_retrieve_cdc_dipen(ls_errore)	< 0 then
	g_mb.error(ls_errore)
	
	return
end if

select stringa
into   :ls_path_logo_1
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and &
		 flag_parametro = 'S' and &
		 cod_parametro = 'LO3';	

ls_path_logo_1 =  s_cs_xx.volume + ls_path_logo_1
ls_modify = "intestazione.filename='" +ls_path_logo_1 + "'~t"
dw_report.modify(ls_modify)

end event

type st_msg from statictext within w_premi_report_lettere
integer x = 82
integer y = 408
integer width = 3579
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto per l~'elaborazione!"
boolean focusrectangle = false
end type

type st_1 from statictext within w_premi_report_lettere
integer x = 82
integer y = 544
integer width = 2912
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 8421504
string text = "CENTRI DI COSTO E DIPENDENTI per report lettere premi"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_sel_dipen from datawindow within w_premi_report_lettere
integer x = 1390
integer y = 628
integer width = 1591
integer height = 1544
integer taborder = 50
string dataobject = "d_premi_calcolo_dipen_sel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string ls_comando

ls_comando = "SEL-DESEL-TUTTI"

if row>0 then
	choose case dwo.name
		case "b_sel"
			
			if dwo.text = "Tutti" then
				//hai scelto di selezionare tutti
				wf_sel_desel_dipendenti(ls_comando, "S")
				
				//alla fine rinomina il pulsante
				dwo.text = "Nessuno"
				
			else
				//hai scelto di DE-selezionare tutti
				wf_sel_desel_dipendenti(ls_comando, "N")
				
				//alla fine rinomina il pulsante
				dwo.text = "Tutti"
			end if
			
	end choose
end if
end event

type dw_sel_cdc from datawindow within w_premi_report_lettere
integer x = 82
integer y = 628
integer width = 1275
integer height = 1544
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_premi_calcolo_cdc_sel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;string ls_cod_cdc

if row > 0 then
	choose case dwo.name
		case "sel"
			
			ls_cod_cdc = getitemstring(row, "codice")
			wf_sel_desel_dipendenti(ls_cod_cdc, data)
			
	end choose
	
end if
end event

type cb_stampa from commandbutton within w_premi_report_lettere
integer x = 3429
integer y = 172
integer width = 347
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
dw_report.print(true, true)
end event

type dw_selezione from uo_cs_xx_dw within w_premi_report_lettere
integer x = 41
integer y = 112
integer width = 2930
integer height = 272
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_premi_report_lettere_sel"
boolean border = false
end type

type cb_report from commandbutton within w_premi_report_lettere
integer x = 1033
integer y = 200
integer width = 347
integer height = 92
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;string ls_errore
long ll_ret

setpointer(hourglass!)

ll_ret = wf_report(ls_errore)

if ll_ret < 0 then
	setpointer(arrow!)
	g_mb.error(ls_errore)
	
	st_msg.text = "Elaborazione terminata con errori!"
	
	rollback using sqlca;
	return
	
elseif ll_ret=0 then
	//non ha passato la validazione dei campi di impostazione
	//messaggio già dato esci senza fare niente
	setpointer(arrow!)
	
	return
	
else
	setpointer(arrow!)
	st_msg.text = "Elaborazione terminata con successo!"
	
	dw_folder.fu_selecttab(2)
	
	return
end if

end event

type dw_report from uo_cs_xx_dw within w_premi_report_lettere
integer x = 87
integer y = 268
integer width = 4965
integer height = 1996
integer taborder = 30
string dataobject = "d_premi_report_lettere"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_premi_report_lettere
integer x = 27
integer y = 16
integer width = 5051
integer height = 2280
integer taborder = 30
boolean border = false
end type

