﻿$PBExportHeader$w_premi_rp_report.srw
forward
global type w_premi_rp_report from w_cs_xx_principale
end type
type cb_pdf from commandbutton within w_premi_rp_report
end type
type cb_stampa from commandbutton within w_premi_rp_report
end type
type dw_selezione from uo_cs_xx_dw within w_premi_rp_report
end type
type cb_report from commandbutton within w_premi_rp_report
end type
type dw_report from uo_cs_xx_dw within w_premi_rp_report
end type
type dw_folder from u_folder within w_premi_rp_report
end type
type htb_1 from htrackbar within w_premi_rp_report
end type
end forward

global type w_premi_rp_report from w_cs_xx_principale
integer width = 4485
integer height = 2820
string title = "Report RP - FL - OL"
boolean resizable = false
event ue_set_anno_rif ( )
cb_pdf cb_pdf
cb_stampa cb_stampa
dw_selezione dw_selezione
cb_report cb_report
dw_report dw_report
dw_folder dw_folder
htb_1 htb_1
end type
global w_premi_rp_report w_premi_rp_report

type prototypes
//Function Long FindWindow(String lpClassName, String lpWindowName) Library "user32" Alias for "FindWindowA"
//Function Long SHGetSpecialFolderLocation(long hwndOwner, Long nFolder, Long pidl) Library "shell32" //Alias for "SHGetSpecialFolderLocationA"
//Function Long SHGetPathFromIDList(Long pidl, String pszPath) Library "shell32" Alias for "SHGetPathFromIDListA"

//Function Long SHGetSpecialFolderPath(Long hwndOwner, REF String lpszPath, long nFolder, boolean fCreate ) Library "shell32" Alias for "SHGetSpecialFolderPathA"

//Function Long SHGetFolderPath(Long hwndOwner, Long nFolder, Long hToken, Long dwFlags, REF String pszPath) Library "shfolder" Alias for "SHGetFolderPathA"

//Function Long SHGetKnownFolderPath(long rfid, long dwFlags, long Token, REF string ppszPath) Library "shell32" Alias for "SHGetKnownFolderPathA"



end prototypes

type variables

end variables

forward prototypes
public function integer wf_report ()
end prototypes

event ue_set_anno_rif();long	ll_anno

ll_anno = year(today())

dw_selezione.setitem(1, "anno", ll_anno)
end event

public function integer wf_report ();string		ls_flag_tipo_elaborazione, ls_cdc
long			ll_anno, ll_mese
datetime		ldt_mese_anno, ldt_inizio, ldt_fine

dw_selezione.accepttext()


ls_cdc = dw_selezione.getitemstring(1, "cod_centro_costo")

if isnull(ls_cdc) or ls_cdc="" then
	g_mb.error("Selezionare il centro di costo!")
	return -1
end if

ls_flag_tipo_elaborazione = dw_selezione.getitemstring(1, "flag_tipo_elaborazione")

if ls_flag_tipo_elaborazione = "A" then
	//report mensili
	ll_anno = dw_selezione.getitemnumber(1, "anno")
	
	if ll_anno>0 then
		dw_report.dataobject = "d_rprep_online_graph"
		dw_report.settransobject(sqlca)
		
		ldt_inizio = datetime(date(ll_anno, 1, 1), 00:00:00)
		ldt_fine = datetime(date(ll_anno, 12, 31), 23:59:59)
		
		dw_report.retrieve(s_cs_xx.cod_azienda,ls_cdc,ldt_inizio,ldt_fine)
		
	else
		g_mb.error("E' necessario specificare l'anno di riferimento!")
		return -1
	end if
	
else
	//report giornaliero
	ldt_mese_anno = dw_selezione.getitemdatetime(1, "mese_anno")
	
	ll_mese = month(date(ldt_mese_anno))
	
	if year(date(ldt_mese_anno)) >= 2000 and ll_mese > 0 then
		
		
		dw_report.dataobject = "d_rprep_online_mese_graph2"
		dw_report.settransobject(sqlca)
		
		dw_report.retrieve(s_cs_xx.cod_azienda,ls_cdc,ll_mese)
		
	else
		g_mb.error("E' necessario specificare mese/anno di riferimento!")
		return -1
	end if
	
end if
	

dw_report.object.datawindow.print.preview = "yes"
dw_report.change_dw_current()

return 1
end function

on w_premi_rp_report.create
int iCurrent
call super::create
this.cb_pdf=create cb_pdf
this.cb_stampa=create cb_stampa
this.dw_selezione=create dw_selezione
this.cb_report=create cb_report
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.htb_1=create htb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_pdf
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
this.Control[iCurrent+7]=this.htb_1
end on

on w_premi_rp_report.destroy
call super::destroy
destroy(this.cb_pdf)
destroy(this.cb_stampa)
destroy(this.dw_selezione)
destroy(this.cb_report)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.htb_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path, ls_database, ls_path_logo_1
windowobject lw_oggetti[], lw_vuoto[]
long li_risposta

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_selezione.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
													
// *** folder													

lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_report
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti = lw_vuoto

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
lw_oggetti[3] = htb_1
lw_oggetti[4] = cb_pdf
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_report

postevent("ue_set_anno_rif")

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type cb_pdf from commandbutton within w_premi_rp_report
integer x = 3520
integer y = 160
integer width = 347
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PDF"
end type

event clicked;
dw_report.Object.DataWindow.Zoom = 100
htb_1.position = 100

dw_report.saveas("",PDF!,true)
end event

type cb_stampa from commandbutton within w_premi_rp_report
integer x = 3886
integer y = 160
integer width = 347
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
dw_report.Object.DataWindow.Zoom = 100
htb_1.position = 100

dw_report.print(true, true)
end event

type dw_selezione from uo_cs_xx_dw within w_premi_rp_report
integer x = 41
integer y = 112
integer width = 2889
integer height = 828
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_premi_report_rp_sel"
boolean border = false
end type

type cb_report from commandbutton within w_premi_rp_report
integer x = 1225
integer y = 704
integer width = 347
integer height = 92
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Report"
end type

event clicked;
setpointer(hourglass!)

if wf_report() < 0 then
else
	dw_folder.fu_selecttab(2)
end if

setpointer(arrow!)
end event

type dw_report from uo_cs_xx_dw within w_premi_rp_report
integer x = 82
integer y = 268
integer width = 4320
integer height = 2380
integer taborder = 30
string dataobject = "d_rprep_online_graph"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_premi_rp_report
integer x = 18
integer y = 12
integer width = 4421
integer height = 2692
integer taborder = 30
boolean border = false
end type

type htb_1 from htrackbar within w_premi_rp_report
integer x = 73
integer y = 168
integer width = 1989
integer height = 84
boolean bringtotop = true
integer maxposition = 100
integer position = 100
integer tickfrequency = 10
end type

event moved;dw_report.Object.DataWindow.Zoom = scrollpos
end event

