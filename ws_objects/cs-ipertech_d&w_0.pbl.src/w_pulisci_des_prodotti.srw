﻿$PBExportHeader$w_pulisci_des_prodotti.srw
forward
global type w_pulisci_des_prodotti from window
end type
type sle_descrizione from singlelineedit within w_pulisci_des_prodotti
end type
type sle_codice from singlelineedit within w_pulisci_des_prodotti
end type
type st_5 from statictext within w_pulisci_des_prodotti
end type
type st_3 from statictext within w_pulisci_des_prodotti
end type
type st_1 from statictext within w_pulisci_des_prodotti
end type
type sle_tabella from singlelineedit within w_pulisci_des_prodotti
end type
type st_4 from statictext within w_pulisci_des_prodotti
end type
type st_6 from statictext within w_pulisci_des_prodotti
end type
type st_record from statictext within w_pulisci_des_prodotti
end type
type cb_1 from commandbutton within w_pulisci_des_prodotti
end type
type st_2 from statictext within w_pulisci_des_prodotti
end type
end forward

global type w_pulisci_des_prodotti from window
integer x = 1074
integer y = 484
integer width = 2400
integer height = 1104
boolean titlebar = true
string title = "CARICO MAGAZZINO AUTOMATICO"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 79741120
sle_descrizione sle_descrizione
sle_codice sle_codice
st_5 st_5
st_3 st_3
st_1 st_1
sle_tabella sle_tabella
st_4 st_4
st_6 st_6
st_record st_record
cb_1 cb_1
st_2 st_2
end type
global w_pulisci_des_prodotti w_pulisci_des_prodotti

forward prototypes
public function integer wf_scrivi_log (string fs_messaggio)
end prototypes

public function integer wf_scrivi_log (string fs_messaggio);integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio

ls_nome_file = "\descrizioni.log"

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "vol", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG indicante nome del file")
	return -1
end if

li_file = fileopen(ls_volume + ls_nome_file, linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if

ls_messaggio = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + fs_messaggio
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0
end function

on w_pulisci_des_prodotti.create
this.sle_descrizione=create sle_descrizione
this.sle_codice=create sle_codice
this.st_5=create st_5
this.st_3=create st_3
this.st_1=create st_1
this.sle_tabella=create sle_tabella
this.st_4=create st_4
this.st_6=create st_6
this.st_record=create st_record
this.cb_1=create cb_1
this.st_2=create st_2
this.Control[]={this.sle_descrizione,&
this.sle_codice,&
this.st_5,&
this.st_3,&
this.st_1,&
this.sle_tabella,&
this.st_4,&
this.st_6,&
this.st_record,&
this.cb_1,&
this.st_2}
end on

on w_pulisci_des_prodotti.destroy
destroy(this.sle_descrizione)
destroy(this.sle_codice)
destroy(this.st_5)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.sle_tabella)
destroy(this.st_4)
destroy(this.st_6)
destroy(this.st_record)
destroy(this.cb_1)
destroy(this.st_2)
end on

type sle_descrizione from singlelineedit within w_pulisci_des_prodotti
integer x = 709
integer y = 400
integer width = 1207
integer height = 92
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type sle_codice from singlelineedit within w_pulisci_des_prodotti
integer x = 709
integer y = 300
integer width = 1207
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_pulisci_des_prodotti
integer x = 366
integer y = 400
integer width = 334
integer height = 92
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Descrizione:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_pulisci_des_prodotti
integer x = 457
integer y = 300
integer width = 247
integer height = 92
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Codice:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_pulisci_des_prodotti
integer x = 457
integer y = 200
integer width = 247
integer height = 92
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Tabella:"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_tabella from singlelineedit within w_pulisci_des_prodotti
integer x = 709
integer y = 200
integer width = 1207
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_pulisci_des_prodotti
integer x = 46
integer y = 672
integer width = 2286
integer height = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
boolean enabled = false
string text = "LE DESCRIZIONI VARIATE SARANNO MEMORIZZATE NEL FILE C:\DESCRIZIONI.TXT"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_pulisci_des_prodotti
integer x = 37
integer y = 820
integer width = 402
integer height = 76
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "record corrente:"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_record from statictext within w_pulisci_des_prodotti
integer x = 32
integer y = 896
integer width = 2002
integer height = 88
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_pulisci_des_prodotti
integer x = 937
integer y = 524
integer width = 631
integer height = 108
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Controlla Descrizioni"
end type

event clicked;string ls_cod_prodotto, ls_des_prodotto, ls_sql, ls_des_prodotto_old
long   ll_pos, ll_len

declare cu_tabella dynamic cursor for sqlsa;
ls_sql = "select " + sle_codice.text + "," + sle_descrizione.text + " from "+ sle_tabella.text +" where cod_azienda = '" + s_cs_xx.cod_azienda + "'"
prepare sqlsa from :ls_sql;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Pulizia caratteri","Sintassi SQL Errara~r~n" + ls_sql)
	return
end if
open dynamic cu_tabella;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Pulizia caratteri","Sintassi SQL Errara~r~n" + ls_sql)
	return
end if

do while 1=1
   fetch cu_tabella into :ls_cod_prodotto, :ls_des_prodotto;
   if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Pulizia caratteri","Errore durante il FETCH sulla tabella " + sle_tabella.text + "~r~nDettaglio:" + sqlca.sqlerrtext)
		return
	end if
		
	st_record.text = "Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto
	yield()	
	ls_des_prodotto_old = ls_des_prodotto
	if not isnull(ls_des_prodotto) then
		do
			if pos(ls_des_prodotto, "'") > 0 then
				ll_pos = pos(ls_des_prodotto, "'")
				ll_len = len(ls_des_prodotto)
				ls_des_prodotto = mid(ls_des_prodotto, 1, ll_pos -1) + "'''" + mid(ls_des_prodotto, ll_pos + 1, ll_len)
			end if
		loop until pos(ls_des_prodotto, "'") < 1
		if ls_des_prodotto_old <> ls_des_prodotto then
			wf_scrivi_log("Prodotto: " + ls_cod_prodotto + " VARIATO DESCRIZIONE DA: " + ls_des_prodotto_old + " IN --->" + ls_des_prodotto) 
		end if
	end if
loop
close cu_tabella;
commit;
return
end event

type st_2 from statictext within w_pulisci_des_prodotti
integer x = 18
integer y = 20
integer width = 2318
integer height = 136
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "QUESTA PROCEDURA ELIMINA DALLE DESCRIZIONI DEI PRODOTTI DI MAGAZZINO QUALSIASI CARATTERE NON AMMESSO."
alignment alignment = center!
boolean focusrectangle = false
end type

