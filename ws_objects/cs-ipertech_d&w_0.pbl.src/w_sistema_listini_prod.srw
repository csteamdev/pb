﻿$PBExportHeader$w_sistema_listini_prod.srw
forward
global type w_sistema_listini_prod from window
end type
type st_1 from statictext within w_sistema_listini_prod
end type
type cb_1 from commandbutton within w_sistema_listini_prod
end type
end forward

global type w_sistema_listini_prod from window
integer width = 1792
integer height = 544
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
st_1 st_1
cb_1 cb_1
end type
global w_sistema_listini_prod w_sistema_listini_prod

on w_sistema_listini_prod.create
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.st_1,&
this.cb_1}
end on

on w_sistema_listini_prod.destroy
destroy(this.st_1)
destroy(this.cb_1)
end on

type st_1 from statictext within w_sistema_listini_prod
integer x = 23
integer y = 240
integer width = 1669
integer height = 100
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_sistema_listini_prod
integer x = 640
integer y = 60
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;string ls_cod_tipo_listino_prodotto, ls_cod_valuta, ls_cod_prodotto
datetime ldt_data_inizio_val
long ll_progressivo

declare cu_listini cursor for  
  select listini_vendite.cod_tipo_listino_prodotto,   
         listini_vendite.cod_valuta,   
         listini_vendite.data_inizio_val,   
         listini_vendite.progressivo,   
         listini_vendite.cod_prodotto  
    from listini_vendite
where cod_azienda = :s_cs_xx.cod_azienda;

open cu_listini;

do while true
	fetch cu_listini into :ls_cod_tipo_listino_prodotto, :ls_cod_valuta , :ldt_data_inizio_val , :ll_progressivo, :ls_cod_prodotto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in fetch cursore. ~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	st_1.text = ls_cod_tipo_listino_prodotto + "-" + ls_cod_valuta  + "-" +  string(ldt_data_inizio_val,"dd/mm/yyyy")  + "-" +  string(ll_progressivo)  + "-" + ls_cod_prodotto
	
	if not isnull(ls_cod_prodotto) then
	
		UPDATE listini_produzione  
		  SET cod_prodotto_listino = :ls_cod_prodotto  
		WHERE ( listini_produzione.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				( listini_produzione.cod_tipo_listino_prodotto = :ls_cod_tipo_listino_prodotto ) AND  
				( listini_produzione.cod_valuta = :ls_cod_valuta ) AND  
				( listini_produzione.data_inizio_val = :ldt_data_inizio_val ) AND  
				( listini_produzione.progressivo = :ll_progressivo );
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in update prodotto "+ls_cod_prodotto+". ~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if
loop

close cu_listini;
commit;
end event

