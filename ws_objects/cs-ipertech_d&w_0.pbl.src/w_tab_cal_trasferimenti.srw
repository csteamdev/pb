﻿$PBExportHeader$w_tab_cal_trasferimenti.srw
forward
global type w_tab_cal_trasferimenti from w_cs_xx_principale
end type
type dw_ricerca from uo_dw_search within w_tab_cal_trasferimenti
end type
type dw_lista from uo_cs_xx_dw within w_tab_cal_trasferimenti
end type
end forward

global type w_tab_cal_trasferimenti from w_cs_xx_principale
integer width = 4987
integer height = 2384
string title = "Calendario Trasferimenti"
dw_ricerca dw_ricerca
dw_lista dw_lista
end type
global w_tab_cal_trasferimenti w_tab_cal_trasferimenti

type variables
string			is_sql_base = ""
end variables

forward prototypes
public function integer wf_esporta (ref string as_errore)
public function integer wf_importa (ref string as_errore)
public function integer wf_ricerca (ref string as_errore)
public function integer wf_seleziona_file (ref string as_errore)
public function string wf_set_sql_string (boolean ab_lista)
end prototypes

public function integer wf_esporta (ref string as_errore);
string					ls_sql, ls_deposito, ls_foglio
long					ll_count, ll_index, ll_column
integer				li_ret
datastore			lds_data
uo_excel_listini		luo_excel
datetime				ldt_data

ls_sql = wf_set_sql_string(false)

ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_count<0 then
	return -1
	
elseif ll_count=0 then
	as_errore = "Nessuna riga da esportare con il filtro selezionato!"
	return 1
end if

ls_foglio = "Foglio1"

luo_excel = create uo_excel_listini
luo_excel.ib_OleObjectVisible = true
luo_excel.uof_inizio()

luo_excel.uof_crea_foglio(ls_foglio)

for ll_index=1 to ll_count
	//data_partenza ------------------------------------------------------------------------------------
	ldt_data = lds_data.getitemdatetime(ll_index, "data_partenza")
	luo_excel.uof_scrivi_data(ls_foglio, ll_index, 1, ldt_data, false, false, "none", "gg/mm/aaaa")
	
	//cod_deposito_partenza --------------------------------------------------------------------------
	ls_deposito = lds_data.getitemstring(ll_index, "cod_deposito_partenza")
	luo_excel.uof_scrivi_codice(ls_foglio, ll_index, 2, ls_deposito, false, false, "none")
	
	//data_arrivo ---------------------------------------------------------------------------------------
	ldt_data = lds_data.getitemdatetime(ll_index, "data_arrivo")
	luo_excel.uof_scrivi_data(ls_foglio, ll_index, 3, ldt_data, false, false, "none", "gg/mm/aaaa")
	
	//cod_deposito_arrivo -----------------------------------------------------------------------------
	ls_deposito = lds_data.getitemstring(ll_index, "cod_deposito_arrivo")
	luo_excel.uof_scrivi_codice(ls_foglio, ll_index, 4, ls_deposito, false, false, "none")
	
next

//luo_excel.uof_save( )
//luo_excel.uof_close( )
//
destroy luo_excel

return 0
end function

public function integer wf_importa (ref string as_errore);string				ls_cod_deposito_partenza, ls_cod_deposito_arrivo, ls_path, ls_sql
date				ldt_data_partenza_ds, ldt_data_arrivo_ds
datetime			ldt_data_partenza, ldt_data_arrivo
datastore		lds_data
long				ll_row, ll_column, ll_new
uo_excel			luo_excel


//----------------------------------------------------------------------------------------------------------------------------------------------------
ls_path = dw_ricerca.getitemstring(1, "path")
if ls_path="" or isnull(ls_path) then
	as_errore = "Indicare il file excel da importare!"
	return 1
end if

//----------------------------------------------------------------------------------------------------------------------------------------------------
if not g_mb.confirm("Confermi l'importazione da excel dei nuovi dati del calendario trasferimenti? "+ &
					"L'operazione cancellerà tutti i dati presenti rimpiazzandoli con quelli contenuti in excel!") then
	
	as_errore = "Operazione annullata!"
	return 1
end if


//cancellazione tabella calendario trasferimenti -----------------------------------------------------------------------------------------------
delete from tab_cal_trasferimenti
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode<0 then
	as_errore = "Errore in cancellazione dati tabella tab_cal_trasferimenti: "+sqlca.sqlerrtext
	return -1
end if

//creo datastore di appoggio dei dati ----------------------------------------------------------------------------------------------------------
ls_sql = "select data_partenza, cod_deposito_partenza, data_arrivo, cod_deposito_arrivo "+&
			"from tab_cal_trasferimenti "+&
			"where cod_azienda='NOTEXIST'"
guo_functions.uof_crea_datastore(lds_data, ls_sql)

//creo oggetto excel per leggerlo ---------------------------------------------------------------------------------------------------------------
luo_excel = create uo_excel

if not luo_excel.uof_open(ls_path, false, false) then
	as_errore = luo_excel.uof_get_error()
	destroy luo_excel
	return -1
end if

ll_row = 1
ll_new = 0

do while true

	//colonna 1: data partenza ###############
	ll_column = 1
	ldt_data_partenza_ds = luo_excel.uof_read_date(ll_row, ll_column)
	
	if isnull(ldt_data_partenza_ds) or year(ldt_data_partenza_ds) < 1950 then exit
	
	//colonna 2: deposito partenza #############
	ll_column += 1
	ls_cod_deposito_partenza = luo_excel.uof_read_string(ll_row, ll_column)
	
	if isnull(ls_cod_deposito_partenza) or ls_cod_deposito_partenza="" then exit
	
	//colonna 3: data arrivo #################
	ll_column += 1
	ldt_data_arrivo_ds = luo_excel.uof_read_date(ll_row, ll_column)
	
	//colonna 4: deposito arrivo ##############
	ll_column += 1
	ls_cod_deposito_arrivo = luo_excel.uof_read_string(ll_row, ll_column)
	
	//inserisco nel datastore ******************************************************************************
	ll_new = lds_data.insertrow(0)
	lds_data.setitem(ll_new, "data_partenza", datetime(ldt_data_partenza_ds, 00:00:00 ))
	lds_data.setitem(ll_new, "cod_deposito_partenza", ls_cod_deposito_partenza)
	lds_data.setitem(ll_new, "data_arrivo", datetime(ldt_data_arrivo_ds, 00:00:00 ))
	lds_data.setitem(ll_new, "cod_deposito_arrivo", ls_cod_deposito_arrivo)
	//*************************************************************************************************

	//vado alla riga successiva
	ll_row += 1
loop

destroy luo_excel


//inserisco in tabella tab_cal_trasferimenti, che dovrebbe essere vuota!
ll_new = lds_data.rowcount()
lds_data.setsort("data_partenza asc, data_arrivo asc")
lds_data.sort()

for ll_row=1 to ll_new
	
	ldt_data_partenza = lds_data.getitemdatetime(ll_row, "data_partenza")
	ls_cod_deposito_partenza = lds_data.getitemstring(ll_row, "cod_deposito_partenza")
	ldt_data_arrivo =  lds_data.getitemdatetime(ll_row, "data_arrivo")
	ls_cod_deposito_arrivo = lds_data.getitemstring(ll_row, "cod_deposito_arrivo")
	
	insert into tab_cal_trasferimenti
		(	cod_azienda,
			progressivo,
			data_partenza,
			cod_deposito_partenza,
			data_arrivo,
			cod_deposito_arrivo)
	values	(	:s_cs_xx.cod_azienda,
					:ll_row,
					:ldt_data_partenza,
					:ls_cod_deposito_partenza,
					:ldt_data_arrivo,
					:ls_cod_deposito_arrivo);
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in inserimento in tab_cal_trasferimenti: " +sqlca.sqlerrtext
		rollback;
		return -1
	end if
next

//se arrivi fin qui fai il commit
commit;

destroy luo_excel

as_errore = "Importazione effettuata con successo!"

return 0
end function

public function integer wf_ricerca (ref string as_errore);
string			ls_sql


ls_sql = wf_set_sql_string(true)

dw_lista.setsqlselect(ls_sql)

dw_lista.change_dw_current()
this.postevent("pc_retrieve")

return 0



end function

public function integer wf_seleziona_file (ref string as_errore);

string		ls_path, docpath, docname[]
integer	li_count, li_ret

ls_path = s_cs_xx.volume
li_ret = GetFileOpenName("Seleziona File XLS da importare", docpath, docname[], "DOC", &
   											+ "Files Excel (*.XLS;*XLSX),*.XLS;*XLSX", &
   											ls_path)

if li_ret < 1 then
	as_errore = "Operazione non effettuata!"
	return 1
end if
li_count = Upperbound(docname)

if li_count = 1 then
	dw_ricerca.setitem(1, "path", docpath)
end if

return 0
end function

public function string wf_set_sql_string (boolean ab_lista);
string			ls_cod_deposito_partenza, ls_cod_deposito_arrivo, ls_sql
datetime		ldt_data_partenza_dal, ldt_data_partenza_al, ldt_data_arrivo_dal, ldt_data_arrivo_al
long			ll_count

if ab_lista then
	ls_sql = is_sql_base			//colonne della dw_lista (compreso azienda e progressivo) e poi from tab_cal_trasferimenti
else
	ls_sql = "select data_partenza, cod_deposito_partenza, data_arrivo, cod_deposito_arrivo from tab_cal_trasferimenti "
end if
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_cod_deposito_partenza = dw_ricerca.getitemstring(1, "cod_deposito_partenza")
ls_cod_deposito_arrivo = dw_ricerca.getitemstring(1, "cod_deposito_arrivo")

ldt_data_partenza_dal = dw_ricerca.getitemdatetime(1, "data_partenza_dal")
ldt_data_partenza_al = dw_ricerca.getitemdatetime(1, "data_partenza_al")

ldt_data_arrivo_dal = dw_ricerca.getitemdatetime(1, "data_arrivo_dal")
ldt_data_arrivo_al = dw_ricerca.getitemdatetime(1, "data_arrivo_al")

if ls_cod_deposito_partenza<>"" and not isnull(ls_cod_deposito_partenza) then
	ls_sql += "and cod_deposito_partenza='"+ls_cod_deposito_partenza+"' "
end if

if ls_cod_deposito_arrivo<>"" and not isnull(ls_cod_deposito_arrivo) then
	ls_sql += "and cod_deposito_arrivo='"+ls_cod_deposito_arrivo+"' "
end if

if not isnull(ldt_data_partenza_dal) and year(date(ldt_data_partenza_dal))>1950 then
	ls_sql += "and data_partenza >= '" + string(ldt_data_partenza_dal, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_partenza_al) and year(date(ldt_data_partenza_al))>1950 then
	ls_sql += "and data_partenza <= '" + string(ldt_data_partenza_al, s_cs_xx.db_funzioni.formato_data) + "' "
end if


if not isnull(ldt_data_arrivo_dal) and year(date(ldt_data_arrivo_dal))>1950 then
	ls_sql += "and data_arrivo >= '" + string(ldt_data_arrivo_dal, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_arrivo_al) and year(date(ldt_data_arrivo_al))>1950 then
	ls_sql += "and data_arrivo <= '" + string(ldt_data_arrivo_al, s_cs_xx.db_funzioni.formato_data) + "' "
end if

return ls_sql
end function

on w_tab_cal_trasferimenti.create
int iCurrent
call super::create
this.dw_ricerca=create dw_ricerca
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca
this.Control[iCurrent+2]=this.dw_lista
end on

on w_tab_cal_trasferimenti.destroy
call super::destroy
destroy(this.dw_ricerca)
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;
string ls_where_depositi


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"
//ls_where_depositi = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))"


f_po_loaddddw_dw(		dw_ricerca, &
								"cod_deposito_partenza", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								ls_where_depositi)

f_po_loaddddw_dw(		dw_ricerca, &
								"cod_deposito_arrivo", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								ls_where_depositi)
								
f_po_loaddddw_dw(		dw_lista, &
								"cod_deposito_partenza", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								ls_where_depositi)

f_po_loaddddw_dw(		dw_lista, &
								"cod_deposito_arrivo", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								ls_where_depositi)

end event

event pc_setwindow;call super::pc_setwindow;



dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("progressivo")
dw_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)

iuo_dw_main = dw_lista

dw_lista.change_dw_current()

is_sql_base = dw_lista.getsqlselect()

end event

type dw_ricerca from uo_dw_search within w_tab_cal_trasferimenti
integer x = 27
integer y = 16
integer width = 1367
integer height = 2232
integer taborder = 10
string dataobject = "d_tab_cal_trasferimenti_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string			ls_msg
integer		li_ret

if row>0 then
else
	return
end if

this.accepttext()

ls_msg = ""

choose case dwo.name
	case "b_cerca"
		li_ret = wf_ricerca(ls_msg)
		
	case "b_esporta"
		li_ret = wf_esporta(ls_msg)
		
	case "b_importa"
		li_ret = wf_importa(ls_msg)
	
	case "b_sel"
		li_ret = wf_seleziona_file(ls_msg)
		
end choose

if li_ret<0 then
	//error
	g_mb.error(ls_msg)
	
elseif li_ret>0 then
	//exclamation
	g_mb.warning(ls_msg)
	
else
	if ls_msg<>"" and not isnull(ls_msg) then g_mb.success(ls_msg)
	
	if dwo.name= "b_importa" then
		//rifai la retrieve
		wf_ricerca(ls_msg)
	end if
	
end if
end event

type dw_lista from uo_cs_xx_dw within w_tab_cal_trasferimenti
integer x = 1413
integer y = 16
integer width = 3515
integer height = 2244
integer taborder = 10
string dataobject = "d_tab_cal_trasferimenti"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_setkey;call super::pcd_setkey;

long ll_i, ll_progressivo

setnull(ll_progressivo)

for ll_i = 1 to rowcount()
	
	if getitemstring(ll_i, "cod_azienda") = "" or isnull(getitemstring(ll_i, "cod_azienda")) then setitem(ll_i,"cod_azienda", s_cs_xx.cod_azienda)
	
	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo")<=0 then
		
		if isnull(ll_progressivo) then
			//è la prima volta, recupero il max  del progressivo in tabella
			select max(progressivo)
			into :ll_progressivo
			from tab_cal_trasferimenti
			where 	cod_azienda = :s_cs_xx.cod_azienda;
			
			if isnull(ll_progressivo) then ll_progressivo = 0
						
		else
			//non è la prima volta, recupero dal contatore "ll_progressivo"
		end if
	
		ll_progressivo += 1
		
		setitem(ll_i, "progressivo", ll_progressivo)
	end if
	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long			ll_count

ll_count = dw_lista.retrieve()

if ll_count < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_validaterow;call super::pcd_validaterow;

string 		ls_deposito_partenza, ls_deposito_arrivo, ls_riga
datetime		ldt_data_arrivo, ldt_data_partenza
long			ll_i



for ll_i = 1 to rowcount()
	
	ls_riga = "(Riga "+string(ll_i)+")"
	
	ls_deposito_partenza = getitemstring(ll_i, "cod_deposito_partenza")
	if ls_deposito_partenza = "" or isnull(ls_deposito_partenza) then
		g_mb.error("Il deposito partenza è obbligatorio! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	ls_deposito_arrivo = getitemstring(ll_i, "cod_deposito_arrivo")
	if ls_deposito_arrivo = "" or isnull(ls_deposito_arrivo) then
		g_mb.error("Il deposito arrivo è obbligatorio! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	if ls_deposito_partenza=ls_deposito_arrivo then
		g_mb.error("Il deposito arrivo coincide con quello di partenza! Verificare! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	ldt_data_partenza = getitemdatetime(ll_i, "data_partenza")
	if isnull(ldt_data_partenza) or year(date(ldt_data_partenza))<1950 then
		g_mb.error("La data di partenza è obbligatoria! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	ldt_data_arrivo = getitemdatetime(ll_i, "data_arrivo")
	if isnull(ldt_data_arrivo) or year(date(ldt_data_arrivo))<1950 then
		g_mb.error("La data di arrivo è obbligatoria! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	if ldt_data_arrivo<ldt_data_partenza then
		g_mb.error("La data di arrivo è inferiore alla data partenza! Verificare! " + ls_riga)
		pcca.error = c_fatal
		return
	end if
	
	
next

end event

