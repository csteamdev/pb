﻿$PBExportHeader$w_tab_des_linea_prodotto.srw
forward
global type w_tab_des_linea_prodotto from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_des_linea_prodotto
end type
end forward

global type w_tab_des_linea_prodotto from w_cs_xx_principale
integer width = 3643
integer height = 2484
string title = "Descrizioni Linea Prodotti"
dw_lista dw_lista
end type
global w_tab_des_linea_prodotto w_tab_des_linea_prodotto

on w_tab_des_linea_prodotto.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_des_linea_prodotto.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;

dw_lista.set_dw_options(		sqlca, &
									pcca.null_object, &
									c_default, &
									c_default)
									
dw_lista.change_dw_current( )
end event

type dw_lista from uo_cs_xx_dw within w_tab_des_linea_prodotto
integer x = 27
integer y = 24
integer width = 3557
integer height = 2332
integer taborder = 10
string dataobject = "d_tab_des_linea_prodotto"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;

long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
next
end event

