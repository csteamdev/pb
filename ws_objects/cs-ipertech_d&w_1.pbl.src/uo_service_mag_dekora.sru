﻿$PBExportHeader$uo_service_mag_dekora.sru
forward
global type uo_service_mag_dekora from uo_service_base
end type
end forward

global type uo_service_mag_dekora from uo_service_base
end type
global uo_service_mag_dekora uo_service_mag_dekora

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
public function integer uof_crea_xml (string as_remote_folder)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);/**
 * stefanop
 * 26/09/2014
 *
 * Il servizio si occupa di allineanre i tessuti modificati nel database 
 * paradox di Dekora
 **/
 
string ls_sql, ls_error, ls_cod_tessuto, ls_des_tessuto, ls_profilo_paradox, ls_parametri[], ls_remote_folder
int li_exists
long ll_rows, ll_i
dec{0} ld_parametri[]
datastore lds_store
transaction tran_paradox

iuo_log.info("Servizio uo_service_mag_dekora avviato.")

// 1. Profilo database
ls_profilo_paradox = auo_params.uof_get_string(1, "")
if g_str.isempty(ls_profilo_paradox) then
	iuo_log.error("Non è stato indicato come parametro il numero del profilo database da usare per il collegamento remoto.")
	return -1
else
	if not guo_functions.uof_crea_transazione("database_" + ls_profilo_paradox, tran_paradox, ls_error) then
		iuo_log.error("Errore durante la connessione al database tramite il profilo di registro database_" + ls_profilo_paradox + ". " + ls_error)
		return -1
	else
		iuo_log.debug("Connessione al database avvenuta con sucesso. Profilo di registro corrispondente: " + ls_profilo_paradox)
	end if
end if

// 2. Codice Azienda [OPZIONALE]
uof_set_s_cs_xx(auo_params.uof_get_string(2, "A01"))

// 3. Creo file tessuto
guo_functions.uof_get_parametro_azienda("RFX", ls_remote_folder)
if g_str.isempty(ls_remote_folder) then
	iuo_log.error("E' necessario impostare il parametro aziendale RFX con la cartella remota dove copiare i file XML.")
	return -1
elseif not g_str.end_with(ls_remote_folder, "\") then
	ls_remote_folder += "\"
end if

if uof_crea_xml(ls_remote_folder) < 0 then
	return -1
end if

ls_sql = "select distinct cod_tessuto from tab_tessuti where cod_azienda='" + s_cs_xx.cod_azienda + "' and flag_modificato='S' order by cod_tessuto"
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)

if ll_rows > 0 then
	
	iuo_log.info("Trovati " + string(ll_rows) + " tessuti da aggiornare.")
		
	for ll_i = 1 to ll_rows
		
		ls_cod_tessuto = lds_store.getitemstring(ll_i, "cod_tessuto")
		
		select des_prodotto
		into :ls_des_tessuto
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_tessuto;
				 
		if sqlca.sqlcode < 0 or sqlca.sqlcode = 100 or isnull(ls_des_tessuto) then
			ls_des_tessuto = "<non definito>"
		end if
		
		iuo_log.debug("Elaboro il tessuto " + ls_cod_tessuto)
		
		// Recupero parametri del tessuto
		SELECT
			c_width,   
			offset_x,   
			offset_y,   
			cut_speed,   
			beam_power,   
			acc_preset,   
			special_param,   
			laser_min_th,   
			laser_max_th,   
			laser_corr,   
			marking_power,   
			marking_speed,   
			mark_Preset,   
			mark_min_th,   
			mark_max_th,   
			mark_corr,   
			mark_type,   
			laser_enabled,   
			wheel_cut_speed,   
			wheel_acc_preset,   
			tensioning_speed,   
			load_correction  
		INTO
			:ld_parametri[1],
			:ld_parametri[2],
			:ld_parametri[3],
			:ld_parametri[4],
			:ld_parametri[5],
			:ld_parametri[6],
			:ld_parametri[7],
			:ld_parametri[8],
			:ld_parametri[9],
			:ld_parametri[10],
			:ld_parametri[11],
			:ld_parametri[12],
			:ld_parametri[13],
			:ld_parametri[14],
			:ld_parametri[15],
			:ld_parametri[16],
			:ld_parametri[17],
			:ls_parametri[18],
			:ld_parametri[19],
			:ld_parametri[20],
			:ld_parametri[21],
			:ld_parametri[22]
		FROM tab_tessuti_parametri
		WHERE cod_azienda = :s_cs_xx.cod_azienda and 
					cod_tessuto = :ls_cod_tessuto;
					
		if sqlca.sqlcode < 0 then
			iuo_log.error("Errore durante il recupero dei parametri (tab_tessuti_parametri) per il tessuto " + ls_cod_tessuto + ". " + g_str.safe(sqlca.sqlerrtext))
			ll_rows = -1
			exit
		elseif sqlca.sqlcode = 100 then
			iuo_log.warning("Impossibile recuperare i parametri per tessuto " + ls_cod_tessuto + " in quanto non sono stati caricati.")
			continue
		end if
		
		if ls_parametri[18] = "S" then
			ld_parametri[18] = 1
		else
			ld_parametri[18] = 0
		end if
		
		// Esiste il tessuto?
		select count(*)
		into :li_exists
		from cloths
		where cloth_id = :ls_cod_tessuto
		using tran_paradox;
		
		if tran_paradox.sqlcode < 0 then
			iuo_log.error("Errore durante il controllo dell'esistenza del tessuto " + ls_cod_tessuto + " nel database esterno.~r~n" + g_str.safe(tran_paradox.sqlerrtext))
			rollback using tran_paradox;
			disconnect using tran_paradox;
			ll_rows = -1
			exit
		elseif tran_paradox.sqlcode = 100 or li_exists = 0 then
			iuo_log.debug("Tessuto " + ls_cod_tessuto + " non trovato; quindi verrà inserito")
			
			// Non esiste devo inserirlo
			INSERT INTO cloths (
				cloth_id,
				cloth_description,
				c_width,   
				offset_x,   
				offset_y,   
				cut_speed,   
				beam_power,   
				acc_preset,   
				special_param,   
				laser_min_th,   
				laser_max_th,   
				laser_corr,   
				marking_power,   
				marking_speed,   
				mark_Preset,   
				mark_min_th,   
				mark_max_th,   
				mark_corr,   
				mark_type,   
				laser_enabled,   
				wheel_cut_speed,   
				wheel_acc_preset,   
				tensioning_speed,   
				load_correction
			) VALUES (
				:ls_cod_tessuto,
				:ls_des_tessuto,
				:ld_parametri[1],
				:ld_parametri[2],
				:ld_parametri[3],
				:ld_parametri[4],
				:ld_parametri[5],
				:ld_parametri[6],
				:ld_parametri[7],
				:ld_parametri[8],
				:ld_parametri[9],
				:ld_parametri[10],
				:ld_parametri[11],
				:ld_parametri[12],
				:ld_parametri[13],
				:ld_parametri[14],
				:ld_parametri[15],
				:ld_parametri[16],
				:ld_parametri[17],
				:ld_parametri[18],
				:ld_parametri[19],
				:ld_parametri[20],
				:ld_parametri[21],
				:ld_parametri[22]
			) using tran_paradox;
			
		else
			// Esiste, devo solo aggiornare
			iuo_log.debug("Tessuto " + ls_cod_tessuto + " già esistente; quindi verrà aggiornato.")
			
			UPDATE cloths
			SET
				cloth_description = :ls_des_tessuto,
				c_width = :ld_parametri[1],
				offset_x= :ld_parametri[2],   
				offset_y = :ld_parametri[3],  
				cut_speed = :ld_parametri[4],
				beam_power = :ld_parametri[5],
				acc_preset = :ld_parametri[6],
				special_param = :ld_parametri[7],
				laser_min_th = :ld_parametri[8],
				laser_max_th = :ld_parametri[9],
				laser_corr = :ld_parametri[10],
				marking_power = :ld_parametri[11],
				marking_speed = :ld_parametri[12],
				mark_Preset = :ld_parametri[13],
				mark_min_th = :ld_parametri[14],
				mark_max_th = :ld_parametri[15],
				mark_corr = :ld_parametri[16],
				mark_type = :ld_parametri[17],
				laser_enabled = :ld_parametri[18],
				wheel_cut_speed = :ld_parametri[19],
				wheel_acc_preset = :ld_parametri[20],
				tensioning_speed = :ld_parametri[21],
				load_correction = :ld_parametri[22]
			WHERE cloth_id = :ls_cod_tessuto
			USING tran_paradox;
			
		end if
	
		if tran_paradox.sqlcode = 0 then 
			// Tessuto memorizzato con successo
			commit using tran_paradox;
			
			update tab_tessuti
			set flag_modificato = 'N'
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :ls_cod_tessuto;
			
			commit;
					 
			iuo_log.info("Aggiornamento del tessuto " + ls_cod_tessuto + " eseguita con successo")
			
		elseif tran_paradox.sqlcode < 0 then
			iuo_log.error("Errore durante l'aggiornamento del tessuto " + ls_cod_tessuto + ". " + g_str.safe(tran_paradox.sqlerrtext))
			rollback using tran_paradox;
			ll_rows = -1
			exit
		end if
			
	next
elseif ll_rows = 0 then
	iuo_log.info("Nessun tessuto da aggiornare.")
else
	iuo_log.error("Errore durante la creazione del datastore.~r~n" + g_str.safe(ls_error))
end if

iuo_log.debug("Procedura terminata")
	
// Pulizia variabili
//disconnect using tran_paradox;
destroy lds_store
return ll_rows
end function

public function integer uof_crea_xml (string as_remote_folder);/**
 * stefanop
 * 07/10/2014
 *
 * Crea il file XML per Auno
 *
 * Ritorna: -1 error, >= 0 ok
 **/
 
string ls_temp_file, ls_source_file[], ls_cod_tessuto, ls_files[]
long ll_count, ll_i
decimal{4} ld_decimal[]
uo_file_text luo_mat, luo_lastre
datastore lds_store
uo_string_builder luo_sb, luo_sb_lastre

//ls_temp_file = guo_functions.uof_get_user_temp_folder( )
ls_files[1] = as_remote_folder + "ewmat.xml"
ls_files[2] = as_remote_folder + "ewlastre.xml"

ls_source_file[1] = s_cs_xx.volume + s_cs_xx.risorse + "mag_tessuti\ewmat.xml"
ls_source_file[2] = s_cs_xx.volume + s_cs_xx.risorse + "mag_tessuti\ewlastre.xml"

// Check
if not fileexists(ls_source_file[1]) then
	iuo_log.error("Il file ewmat.xml non è stato trovato nelle risorse. (" + g_str.safe(ls_source_file[1]) + ")")
	return -1
end if

if not fileexists(ls_source_file[2]) then
	iuo_log.error("Il file ewlastre.xml non è stato trovato nelle risorse. (" + g_str.safe(ls_source_file[2]) + ")")
	return -1
end if

if fileexists(ls_files[1]) then filedelete(ls_files[1])
if fileexists(ls_files[2]) then filedelete(ls_files[2])

if filecopy(ls_source_file[1], ls_files[1]) < 0 then
	iuo_log.error("Errore durante la copia del file di origne (" + g_str.safe(ls_source_file[1]) + ") nella cartella temporanea (" + g_str.safe(ls_files[1]) + ")")
 	return -1
end if

if filecopy(ls_source_file[2], ls_files[2]) < 0 then
	iuo_log.error("Errore durante la copia del file di origne (" + g_str.safe(ls_source_file[2]) + ") nella cartella temporanea (" + g_str.safe(ls_files[2]) + ")")
 	return -1
end if
// ----------------

luo_mat = create uo_file_text
luo_lastre = create uo_file_text

luo_mat.open(ls_files[1], true)
luo_lastre.open(ls_files[2], true)

luo_mat.write("<ROWDATA>").new_line()
luo_lastre.write("<ROWDATA>").new_line()

ll_count = guo_functions.uof_crea_datastore(lds_store, "select * from tab_tessuti_parametri where cod_azienda='" + s_cs_xx.cod_azienda + "'")
for ll_i = 1 to ll_count
	luo_sb = create uo_string_builder
	luo_sb_lastre = create uo_string_builder
	
	ls_cod_tessuto = lds_store.getitemstring(ll_i, "cod_tessuto")
	ld_decimal[1] = lds_store.getitemnumber(ll_i, "rif_x1")
	ld_decimal[2] = lds_store.getitemnumber(ll_i, "rif_y1")
	ld_decimal[3] = lds_store.getitemnumber(ll_i, "rif_x2")
	ld_decimal[4] = lds_store.getitemnumber(ll_i, "rif_y2")
	ld_decimal[5] = lds_store.getitemnumber(ll_i, "trav_x")
	ld_decimal[6] = lds_store.getitemnumber(ll_i, "trav_y")
	
	luo_sb.append("<ROW ID_MAGAZ=~"" + string(ll_i) + "~" ") &
			.append("ID_ANAGRAFICA=~"" + string(ll_i) + "~" ID_ANAGRAFICA_PADRE=~"0~"") &
			.append("CODICE_ANAGRAFICA=~"" + ls_cod_tessuto + "~" ") &
			.append("DESCR_ANAGRAFICA=~"" + ls_cod_tessuto + "~" ") &
			.append("CONDITION=~"~" ID_TIPITABELLA=~"1~" ID_TIPIPARTE=~"12~" ") &
			.append("ID_TIPOLOGIE=~"0~" ID_TIPIPRODOTTO=~"0~" CATEGORIE=~"0~" ID_TIPIMATERIALE=~"26~" ") &
			.append("DESCMAT=~"1~" ID_UM=~"0~" " ) &
			.append("RIFY1=~"" + string(ld_decimal[2]) + "~" ") &
			.append("SPESMAT=~"1~" ") &
			.append("RIFX1=~"" + string(ld_decimal[1]) + "~" RIFX2=~"" + string(ld_decimal[3]) + "~" RIFY2=~"" + string(ld_decimal[4]) + "~" ") &
			.append("DISTMIN=~"0~" TRAVX=~"" + string(ld_decimal[5]) + "~" TRAVY=~"" + string(ld_decimal[6]) + "~" DIMOFFX=~"0~" ") &
			.append("DIMOFFY=~"0~" SPESFIRST=~"0~" SPESPLAS=~"0~" SPESSEC=~"0~" PESO=~"0~" COLORE=~"~" ") &
			.append("GIUNTO=~"0~" COLMAT=~"0~" VENATURA=~"0~" AUTOTRIM=~"0~" ") &
			.append("TRIMANG=~"20~" MOLATURA=~"0~" LARGHEZZA=~"0~" TIPOINCROCIO=~"0~" BOCCAGLIO=~"0~" ING2MIS=~"0~" RIFCLI=~"0~" ") &
			.append("ID_PERSONE=~"0~" DATAINS=~"" + string(today() , "yyyymmdd") + "~" ") &
			.append("DATAORD=~"18991230~" DATACONS=~"18991230~" STATO=~"0~" DELETED=~"0~" PREV=~"0~" DEF=~"0~" AREATOT=~"0~" ") &
			.append("NUMPOS=~"0~" QTAPZTOT=~"0~" NOTES=~"~" IMPORTFILE=~"~" ORDSTD=~"~" PREFISSO=~"~" TIPOWORK=~"1~" COSTO=~"0~" ") &
			.append("PRIOWORK=~"0~" AUTOTRASF=~"0~" AUTOCOMPL=~"0~" MONOMAT=~"0~" FULLPIECE=~"0~" ID_PARCO=~"0~" LASTUSER=~"0~" ") &
			.append("LASTDATE=~"18991230~" LASTCLIENT=~"0~" ID_OGGETTI=~"0~" NOTE=~"~" PREZZO_ACQUISTO=~"0~" ") &
			.append("DESCRFX_1=~"0~" ICO=~"0~" ICOSTATE=~"0~" ID_UNMIS_BUY=~"0~" ID_UNMIS_SELL=~"0~" USERCREATE=~"~" DATECREATE=~"18991230~" ") &
			.append("CLIENTCREATE=~"~" ID_IVA=~"0~" ID_TIPITABELLA_PADRE=~"0~" INCPERC1=~"0~" INCPERC2=~"0~" INCPERC3=~"0~" USEDESCR=~"~" ") &
			.append("CAN_DELETE=~"1~" INTERNAL=~"0~"/> ")
	
	luo_sb_lastre.append("<ROW ID_MAGAZ=~"" + string(ll_i) + "~" ") &
						.append("ID_LASTRE=~"" + string(ll_i) + "~"  POSLAS=~"" + string(ll_i) + "~" ") &
						.append("DIMXLAS=~"" + string(ld_decimal[5]) + "~" DIMYLAS=~"" + string(ld_decimal[6]) + "~" ID_UM=~"0~" CAVALLETTO=~"~" ") &
						.append("TIPOTRAVLAS=~"X~" OFFCUT=~"N~" OFFGENBY=~"0~" PRIOLAS=~"0~" GIACENZA=~"100~" LASTUSER=~"~" ") &
						.append("LASTDATE=~"18991230~" LASTCLIENT=~"0~" ID_RACKANAG=~"0~" ID_STOCKANAG=~"0~" USERCREATE=~"~" ") &
						.append("DATECREATE=~"18991230~" CLIENTCREATE=~"~" NOTES=~"~" FlagSpigoloPartenza=~"1~" Sagoma=~"~" />")
	
	luo_mat.write(luo_sb.tostring()).new_line()
	luo_lastre.write(luo_sb_lastre.tostring()).new_line()
	
	destroy luo_sb
	destroy luo_sb_lastre
next

luo_mat.write("</ROWDATA>").new_line()
luo_mat.write("</DATAPACKET>").new_line()
luo_lastre.write("</ROWDATA>").new_line()
luo_lastre.write("</DATAPACKET>").new_line()

destroy luo_mat
return 0
end function

on uo_service_mag_dekora.create
call super::create
end on

on uo_service_mag_dekora.destroy
call super::destroy
end on

