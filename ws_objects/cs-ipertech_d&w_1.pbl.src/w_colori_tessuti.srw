﻿$PBExportHeader$w_colori_tessuti.srw
$PBExportComments$Finestra Colori Tessuti
forward
global type w_colori_tessuti from w_cs_xx_principale
end type
type dw_colori_tessuti from uo_cs_xx_dw within w_colori_tessuti
end type
end forward

global type w_colori_tessuti from w_cs_xx_principale
integer width = 5106
integer height = 2536
string title = "Colori Tessuti"
dw_colori_tessuti dw_colori_tessuti
end type
global w_colori_tessuti w_colori_tessuti

event pc_setwindow;call super::pc_setwindow;dw_colori_tessuti.set_dw_key("cod_azienda")
dw_colori_tessuti.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_colori_tessuti
end event

on w_colori_tessuti.create
int iCurrent
call super::create
this.dw_colori_tessuti=create dw_colori_tessuti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_colori_tessuti
end on

on w_colori_tessuti.destroy
call super::destroy
destroy(this.dw_colori_tessuti)
end on

type dw_colori_tessuti from uo_cs_xx_dw within w_colori_tessuti
integer x = 23
integer y = 20
integer width = 5029
integer height = 2400
string dataobject = "d_colori_tessuti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_delete;call super::pcd_delete;

object.b_null.enabled = false
end event

event pcd_modify;call super::pcd_modify;

object.b_null.enabled = false
end event

event pcd_new;call super::pcd_new;

object.b_null.enabled = false
end event

event pcd_view;call super::pcd_view;

object.b_null.enabled = true
end event

event buttonclicked;call super::buttonclicked;dec{4}		ld_null
string			ls_cod_colore_tessuto, ls_errore


if row>0 then
else
	return
end if

choose case dwo.name
	case "b_null"
		ls_cod_colore_tessuto = getitemstring(row, "cod_colore_tessuto")
		
		if g_str.isnotempty(ls_cod_colore_tessuto) then
			if g_mb.confirm( "Impostare il Val GTOT a NULL per il colore-tessuto "+ls_cod_colore_tessuto+" ?" ) then
				setnull(ld_null)
				
				update tab_colori_tessuti
				set val_gtot = null
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_colore_tessuto=:ls_cod_colore_tessuto;
				
				if sqlca.sqlcode<0 then
					ls_errore = sqlca.sqlerrtext
					rollback;
					g_mb.error("Errore in impostazione a NULL: " + ls_errore)
					return
				else
					commit;
					
					setitem(row, "val_gtot", ld_null)
					setitemstatus(row, "val_gtot", Primary!, NotModified!)
					return
				end if
			end if
		end if
		
end choose
end event

