﻿$PBExportHeader$w_crea_tessuti.srw
$PBExportComments$Finestra Creazione / Duplicazione tessuti
forward
global type w_crea_tessuti from w_cs_xx_risposta
end type
type dw_selezione_colori_tessuti from datawindow within w_crea_tessuti
end type
type dw_tessuti_default from datawindow within w_crea_tessuti
end type
type cb_1 from commandbutton within w_crea_tessuti
end type
end forward

global type w_crea_tessuti from w_cs_xx_risposta
integer width = 2825
integer height = 2048
string title = "Creazione Tessuti"
dw_selezione_colori_tessuti dw_selezione_colori_tessuti
dw_tessuti_default dw_tessuti_default
cb_1 cb_1
end type
global w_crea_tessuti w_crea_tessuti

forward prototypes
public function integer wf_carica_tessuti ()
end prototypes

public function integer wf_carica_tessuti ();string ls_cod_colore, ls_des_colore, ls_sql
long ll_riga

declare cu_colori dynamic cursor for sqlsa;
ls_sql = "select cod_colore_tessuto, des_colore_tessuto from tab_colori_tessuti where cod_azienda = '"+s_cs_xx.cod_azienda+"' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_colore_tessuto "
prepare sqlsa from :ls_sql;

open dynamic cu_colori;

do while 1=1
   fetch cu_colori into :ls_cod_colore, :ls_des_colore;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	ll_riga = dw_selezione_colori_tessuti.insertrow(0)
	dw_selezione_colori_tessuti.setitem(ll_riga,"cod_colore", ls_cod_colore)
	dw_selezione_colori_tessuti.setitem(ll_riga,"des_colore", ls_des_colore)
loop
close cu_colori;
return 0
commit;
dw_selezione_colori_tessuti.resetupdate()

end function

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tessuti_default,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
/*
f_PO_LoadDDDW_DW(dw_tessuti_default,"cod_tessuto_cliente",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_tessuti')")
*/

f_PO_LoadDDDW_DW(dw_tessuti_default,&
 					  "cod_tipo_campionario",&
					   sqlca,&
                 "tab_tipi_campionari", &
					  "cod_tipo_campionario",&
					  "des_tipo_campionario",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;//dw_selezione_colori_tessuti.set_dw_options(sqlca,pcca.null_object,c_disablecc, c_default)
wf_carica_tessuti()
//dw_selezione_colori_tessuti.set_dw_options(sqlca,pcca.null_object,c_newonopen + c_disablecc, c_default)

//dw_selezione_colori_tessuti.triggerevent("pcd_modify")
//dw_tessuti_default.triggerevent("pcd_new")
//save_on_close(c_socnosave)
dw_tessuti_default.insertrow(0)
dw_tessuti_default.resetupdate()
end event

on w_crea_tessuti.create
int iCurrent
call super::create
this.dw_selezione_colori_tessuti=create dw_selezione_colori_tessuti
this.dw_tessuti_default=create dw_tessuti_default
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione_colori_tessuti
this.Control[iCurrent+2]=this.dw_tessuti_default
this.Control[iCurrent+3]=this.cb_1
end on

on w_crea_tessuti.destroy
call super::destroy
destroy(this.dw_selezione_colori_tessuti)
destroy(this.dw_tessuti_default)
destroy(this.cb_1)
end on

type dw_selezione_colori_tessuti from datawindow within w_crea_tessuti
integer x = 23
integer y = 20
integer width = 2729
integer height = 1000
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_selezione_colori_tessuti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;if row = 0 then
	if dwo.name = 'cod_colore_t' then
		dw_selezione_colori_tessuti.setsort("cod_colore")
		dw_selezione_colori_tessuti.sort()
	end if
	if dwo.name = 'des_colore_t' then
		dw_selezione_colori_tessuti.setsort("des_colore")
		dw_selezione_colori_tessuti.sort()
	end if
end if
end event

type dw_tessuti_default from datawindow within w_crea_tessuti
integer x = 23
integer y = 1040
integer width = 2738
integer height = 760
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_tessuti_default"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tessuti_default,"cod_tessuto")
	case "b_tessuto_cliente"
		guo_ricerca.uof_ricerca_prodotto(dw_tessuti_default,"cod_tessuto_cliente")
	
end choose
end event

type cb_1 from commandbutton within w_crea_tessuti
integer x = 2391
integer y = 1816
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;string ls_cod_colore, ls_des_colore, ls_cod_tessuto, ls_cod_gruppo_variante, ls_linea_prodotto, ls_cod_veloce, &
       ls_flag_reparto_taglio, ls_flag_addizionale, ls_flag_fuori_campionario, ls_flag_selezione, ls_cod_tipo_campionario
long   ll_i, ll_perc_maggiorazione
double ld_altezza_tessuto

dw_tessuti_default.accepttext()

ls_cod_tessuto = dw_tessuti_default.getitemstring(1,"cod_tessuto")
ls_cod_gruppo_variante = dw_tessuti_default.getitemstring(1,"cod_gruppo_variante")
ls_linea_prodotto = dw_tessuti_default.getitemstring(1,"linea_prodotto")
ls_cod_veloce = dw_tessuti_default.getitemstring(1,"cod_veloce")
ld_altezza_tessuto = dw_tessuti_default.getitemnumber(1,"altezza_tessuto")
ll_perc_maggiorazione = dw_tessuti_default.getitemnumber(1,"perc_maggiorazione")
ls_flag_reparto_taglio = dw_tessuti_default.getitemstring(1,"flag_reparto_taglio")
ls_flag_addizionale = dw_tessuti_default.getitemstring(1,"flag_addizionale")
ls_flag_fuori_campionario = dw_tessuti_default.getitemstring(1,"flag_fuori_campionario")
ls_cod_tipo_campionario = dw_tessuti_default.getitemstring(1,"cod_tipo_campionario")

if isnull(ls_cod_tessuto) then
	g_mb.messagebox("Duplicazione","Gruppo tessuti non indicato")
	return
end if

if isnull(ls_cod_gruppo_variante) then
	g_mb.messagebox("Duplicazione","Gruppo variante non indicato")
	return
end if

if isnull(ls_linea_prodotto) then
	g_mb.messagebox("Duplicazione","Linea del prodotto non indicato")
	return
end if

if isnull(ls_cod_veloce) then
	g_mb.messagebox("Duplicazione","Codice veloce non indicato")
	return
end if

if isnull(ld_altezza_tessuto) or ld_altezza_tessuto = 0 then
	g_mb.messagebox("Duplicazione","Altezza tessuto non specificata")
	return
end if

if g_mb.messagebox("Duplicazione","Proseguo con l'elaborazione?",Question!,YesNo!, 2) = 2 then return
if g_mb.messagebox("Duplicazione","Sei sicuro?",Question!,YesNo!, 2) = 2 then return

for ll_i = 1 to dw_selezione_colori_tessuti.rowcount()
	ls_cod_colore = dw_selezione_colori_tessuti.getitemstring(ll_i, "cod_colore")
	ls_des_colore = dw_selezione_colori_tessuti.getitemstring(ll_i, "des_colore")
	ls_flag_selezione = dw_selezione_colori_tessuti.getitemstring(ll_i, "flag_selezione")
	
	if not isnull(ls_cod_colore) and ls_flag_selezione = "S" then
		
		insert into tab_tessuti  
				( cod_azienda,   
				  cod_tessuto,   
				  cod_non_a_magazzino,   
				  cod_veloce,   
				  cod_gruppo_variante,   
				  cod_linea_prodotto,   
				  quan_disponibile,   
				  quan_impegnata,   
				  alt_tessuto,   
				  perc_maggiorazione,   
				  flag_reparto_taglio,   
				  flag_fuori_campionario,   
				  flag_addizionale,   
				  flag_default,   
				  flag_blocco,   
				  data_blocco,
				  cod_tessuto_mantovana,   
				  cod_colore_mantovana,   
				  colore_passamaneria,   
				  colore_cordolo,
				  cod_tipo_campionario)  
		values ( :s_cs_xx.cod_azienda,   
				  :ls_cod_tessuto,   
				  :ls_cod_colore,   
				  :ls_cod_veloce,   
				  :ls_cod_gruppo_variante,   
				  :ls_linea_prodotto,   
				  0,   
				  0,   
				  :ld_altezza_tessuto,   
				  :ll_perc_maggiorazione,   
				  :ls_flag_reparto_taglio,   
				  :ls_flag_fuori_campionario,   
				  :ls_flag_addizionale,   
				  'N',   
				  'N',   
				  null,
				  :ls_cod_tessuto,   
				  :ls_cod_colore,   
				  null,
				  null,
				  :ls_cod_tipo_campionario)  ;
		choose case sqlca.sqlcode 
			case 100
				f_scrivi_log("CREAZIONE TESSUTO: tessuto:" + ls_cod_tessuto + " Colore:"+ls_cod_colore+" è già esistente")
				rollback;
			case 0
				f_scrivi_log("CREAZIONE TESSUTO: tessuto:" + ls_cod_tessuto + " Colore:"+ls_cod_colore+" inserito correttamente")
			case else
				f_scrivi_log("CREAZIONE TESSUTO: tessuto:" + ls_cod_tessuto + " Colore:"+ls_cod_colore+" errore:"+string(sqlca.sqlcode)+" Dettaglio errore:"+sqlca.sqlerrtext)
				rollback;
		end choose	
		
	end if
next

COMMIT;

g_mb.messagebox("APICE","Elaborazione terminata correttamemte")
dw_selezione_colori_tessuti.reset()
dw_tessuti_default.reset()
wf_carica_tessuti()
dw_tessuti_default.insertrow(0)
end event

