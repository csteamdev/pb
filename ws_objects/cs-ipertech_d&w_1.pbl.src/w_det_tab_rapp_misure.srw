﻿$PBExportHeader$w_det_tab_rapp_misure.srw
$PBExportComments$Finestra Dettaglio Tabella Rapporti Misure
forward
global type w_det_tab_rapp_misure from w_cs_xx_principale
end type
type dw_det_tab_rapp_misure_lista from uo_cs_xx_dw within w_det_tab_rapp_misure
end type
type dw_det_tab_rapp_misure_det from uo_cs_xx_dw within w_det_tab_rapp_misure
end type
end forward

global type w_det_tab_rapp_misure from w_cs_xx_principale
int Width=2346
int Height=1345
boolean TitleBar=true
string Title="Dettaglio Tabella Rapporti Misure"
dw_det_tab_rapp_misure_lista dw_det_tab_rapp_misure_lista
dw_det_tab_rapp_misure_det dw_det_tab_rapp_misure_det
end type
global w_det_tab_rapp_misure w_det_tab_rapp_misure

type variables
boolean ib_new
end variables

forward prototypes
public function integer wf_verifica_dimensioni (double fd_lim_inf_dim_h, double fd_lim_sup_dim_h, double fd_lim_inf_dim_s, double fd_lim_sup_dim_s, string fs_cod_tab_rapp_misure)
public subroutine wf_tipo_hp (string as_tipo_hp)
end prototypes

public function integer wf_verifica_dimensioni (double fd_lim_inf_dim_h, double fd_lim_sup_dim_h, double fd_lim_inf_dim_s, double fd_lim_sup_dim_s, string fs_cod_tab_rapp_misure);// Funzione che verifica l'eventuale incompatibilità tra i limiti delle dimensioni passate come argomento
// e i limiti delle dimensioni dei limiti_tab_rapp_misure già presenti in det_tab_rapp_misure
// Argomenti :
// 	fd_lim_inf_dim_h : limite inferiore dimensione 1
// 	fd_lim_sup_dim_h : limite superiore dimensione 1
// 	fd_lim_inf_dim_s : limite inferiore dimensione 2
// 	fd_lim_sup_dim_s : limite superiore dimensione 2
//		fs_cod_tab_rapp_misure : tabella su cui gestire i limiti
// Ritorna :
//		0 : Limiti Validi
//		-1: Limiti non Validi
// limiti_tab_rapp_misure 17/08/1998 (che afa ragassi!!!)

double ld_lim_inf_dim_h, ld_lim_sup_dim_h, ld_lim_inf_dim_s, ld_lim_sup_dim_s

if	(fd_lim_inf_dim_h > fd_lim_sup_dim_h and fd_lim_sup_dim_h <> 0) or &
	(fd_lim_inf_dim_s > fd_lim_sup_dim_s and fd_lim_sup_dim_s <> 0) then
	return -1
end if

declare cur_limiti_tab_rapp_misure cursor for
  select lim_inf_dim_h,
         lim_sup_dim_h,
         lim_inf_dim_s,
         lim_sup_dim_s
    from det_tab_rapp_misure 
   where cod_azienda = :s_cs_xx.cod_azienda 
	  and cod_tab_rapp_misure = :fs_cod_tab_rapp_misure;

open cur_limiti_tab_rapp_misure;

fetch cur_limiti_tab_rapp_misure into :ld_lim_inf_dim_h, :ld_lim_sup_dim_h, :ld_lim_inf_dim_s, :ld_lim_sup_dim_s;

do while sqlca.sqlcode = 0
	if 	(ld_lim_inf_dim_h >= fd_lim_inf_dim_h and ld_lim_inf_dim_h <= fd_lim_sup_dim_h) or &
			(ld_lim_sup_dim_h >= fd_lim_inf_dim_h and ld_lim_sup_dim_h <= fd_lim_sup_dim_h) or &
			(ld_lim_inf_dim_s >= fd_lim_inf_dim_s and ld_lim_inf_dim_s <= fd_lim_sup_dim_s) or &
			(ld_lim_sup_dim_s >= fd_lim_inf_dim_s and ld_lim_sup_dim_s <= fd_lim_sup_dim_s) or &
			(fd_lim_inf_dim_h >= ld_lim_inf_dim_h and fd_lim_inf_dim_h <= ld_lim_sup_dim_h) or &
			(fd_lim_sup_dim_h >= ld_lim_inf_dim_h and fd_lim_sup_dim_h <= ld_lim_sup_dim_h) or &
			(fd_lim_inf_dim_s >= ld_lim_inf_dim_s and fd_lim_inf_dim_s <= ld_lim_sup_dim_s) or &
			(fd_lim_sup_dim_s >= ld_lim_inf_dim_s and fd_lim_sup_dim_s <= ld_lim_sup_dim_s) then
		close cur_limiti_tab_rapp_misure;
		return -1	
	end if
	fetch cur_limiti_tab_rapp_misure into :ld_lim_inf_dim_h, :ld_lim_sup_dim_h, :ld_lim_inf_dim_s, :ld_lim_sup_dim_s;
loop
close cur_limiti_tab_rapp_misure;

return 0
end function

public subroutine wf_tipo_hp (string as_tipo_hp);if as_tipo_hp = "H" or as_tipo_hp = "S" then
	dw_det_tab_rapp_misure_det.Object.dim_hp.Visible = 0
	dw_det_tab_rapp_misure_det.Object.dim_hp_t.Visible = 0
elseif as_tipo_hp = "F" then
	dw_det_tab_rapp_misure_det.Object.dim_hp.Visible = 1
	dw_det_tab_rapp_misure_det.Object.dim_hp_t.Visible = 1
end if

end subroutine

on w_det_tab_rapp_misure.create
int iCurrent
call w_cs_xx_principale::create
this.dw_det_tab_rapp_misure_lista=create dw_det_tab_rapp_misure_lista
this.dw_det_tab_rapp_misure_det=create dw_det_tab_rapp_misure_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_det_tab_rapp_misure_lista
this.Control[iCurrent+2]=dw_det_tab_rapp_misure_det
end on

on w_det_tab_rapp_misure.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_det_tab_rapp_misure_lista)
destroy(this.dw_det_tab_rapp_misure_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_tab_rapp_misure_lista.set_dw_key("cod_azienda")
dw_det_tab_rapp_misure_lista.set_dw_key("cod_tab_rapp_misure")
dw_det_tab_rapp_misure_lista.set_dw_key("prog_tab_rapp_misure")
dw_det_tab_rapp_misure_lista.set_dw_options(sqlca,i_openparm,c_scrollparent,c_default)
dw_det_tab_rapp_misure_det.set_dw_options(sqlca,dw_det_tab_rapp_misure_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_det_tab_rapp_misure_lista
ib_new = false
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_det_tab_rapp_misure_lista,"cod_tab_rapp_misure",sqlca,&
                 "tes_tab_rapp_misure","cod_tab_rapp_misure","des_tab_rapp_misure",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_det_tab_rapp_misure_det,"cod_tab_rapp_misure",sqlca,&
                 "tes_tab_rapp_misure","cod_tab_rapp_misure","des_tab_rapp_misure",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_delete;call super::pc_delete;ib_new = false
end event

type dw_det_tab_rapp_misure_lista from uo_cs_xx_dw within w_det_tab_rapp_misure
int X=23
int Y=21
int Width=2263
int Height=521
int TabOrder=10
string DataObject="d_det_tab_rapp_misure_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tab_rapp_misure
long  l_error

ls_cod_tab_rapp_misure = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tab_rapp_misure")
l_error = Retrieve(s_cs_xx.cod_azienda, ls_cod_tab_rapp_misure)

if l_error < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;long  l_idx
string ls_cod_tab_rapp_misure
ls_cod_tab_rapp_misure = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tab_rapp_misure")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tab_rapp_misure")) THEN
      SetItem(l_Idx, "cod_tab_rapp_misure", ls_cod_tab_rapp_misure)
   END IF
NEXT
end event

event pcd_save;call super::pcd_save;ib_new = false
end event

event pcd_new;call super::pcd_new;string ls_cod_tab_rapp_misure
ls_cod_tab_rapp_misure = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tab_rapp_misure")

dw_det_tab_rapp_misure_det.setitem(getrow(), "cod_tab_rapp_misure", ls_cod_tab_rapp_misure)
wf_tipo_hp("F")
ib_new = true
end event

event pcd_modify;call super::pcd_modify;ib_new = false
end event

event pcd_view;call super::pcd_view;ib_new = false
end event

event updatestart;call super::updatestart;if ib_new then
	string ls_cod_tab_rapp_misure
	integer li_prog_tab_rapp_misure
	
	ls_cod_tab_rapp_misure = this.getitemstring(this.getrow(), "cod_tab_rapp_misure")
	li_prog_tab_rapp_misure = 0
	
	select max(prog_tab_rapp_misure)
	 into :li_prog_tab_rapp_misure  
	 from det_tab_rapp_misure
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tab_rapp_misure = :ls_cod_tab_rapp_misure;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Errore durante il Calcolo del Progressivo Limite X tab_rapp_misure")
		pcca.error = c_fatal
		return -1
	end if
	if li_prog_tab_rapp_misure = 0 or isnull(li_prog_tab_rapp_misure) then
		li_prog_tab_rapp_misure = 1
	else
		li_prog_tab_rapp_misure = li_prog_tab_rapp_misure + 1
	end if
	this.setitem(this.getrow(), "prog_tab_rapp_misure", li_prog_tab_rapp_misure)
end if
end event

event pcd_pickedrow;call super::pcd_pickedrow;wf_tipo_hp(this.getitemstring(this.getrow(), "flag_calcolo_hp"))
end event

type dw_det_tab_rapp_misure_det from uo_cs_xx_dw within w_det_tab_rapp_misure
int X=23
int Y=561
int Width=2263
int Height=661
int TabOrder=20
boolean BringToTop=true
string DataObject="d_det_tab_rapp_misure_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	double ld_lim_inf_dim_h, ld_lim_sup_dim_h, ld_lim_inf_dim_s, ld_lim_sup_dim_s
	string ls_cod_tab_rapp_misure
	choose case i_colname
		case "lim_inf_dim_h"
			ls_cod_tab_rapp_misure = this.getitemstring(row, "cod_tab_rapp_misure")
			ld_lim_sup_dim_h = this.getitemnumber(row, "lim_sup_dim_h")
			ld_lim_inf_dim_s = this.getitemnumber(row, "lim_inf_dim_s")
			ld_lim_sup_dim_s = this.getitemnumber(row, "lim_sup_dim_s")
			if wf_verifica_dimensioni(double(data), ld_lim_sup_dim_h, ld_lim_inf_dim_s, ld_lim_sup_dim_s, &
											  ls_cod_tab_rapp_misure) = -1 then
				g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Limite non Valido")
				return 2
			end if
		case "lim_sup_dim_h"
			ls_cod_tab_rapp_misure = this.getitemstring(row, "cod_tab_rapp_misure")
			ld_lim_inf_dim_h = this.getitemnumber(row, "lim_inf_dim_h")
			ld_lim_inf_dim_s = this.getitemnumber(row, "lim_inf_dim_s")
			ld_lim_sup_dim_s = this.getitemnumber(row, "lim_sup_dim_s")
			if wf_verifica_dimensioni(ld_lim_inf_dim_h, double(data), ld_lim_inf_dim_s, ld_lim_sup_dim_s, &
											  ls_cod_tab_rapp_misure) = -1 then
				g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Limite non Valido")
				return 2
			end if
		case "lim_inf_dim_s"
			ls_cod_tab_rapp_misure = this.getitemstring(row, "cod_tab_rapp_misure")
			ld_lim_inf_dim_h = this.getitemnumber(row, "lim_inf_dim_h")
			ld_lim_sup_dim_h = this.getitemnumber(row, "lim_sup_dim_h")
			ld_lim_sup_dim_s = this.getitemnumber(row, "lim_sup_dim_s")
			if wf_verifica_dimensioni(ld_lim_inf_dim_h, ld_lim_sup_dim_h, double(data), ld_lim_sup_dim_s, &
											  ls_cod_tab_rapp_misure) = -1 then
				g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Limite non Valido")
				return 2
			end if
		case "lim_sup_dim_s"
			ls_cod_tab_rapp_misure = this.getitemstring(row, "cod_tab_rapp_misure")
			ld_lim_inf_dim_h = this.getitemnumber(row, "lim_inf_dim_h")
			ld_lim_sup_dim_h = this.getitemnumber(row, "lim_sup_dim_h")
			ld_lim_inf_dim_s = this.getitemnumber(row, "lim_inf_dim_s")
			if wf_verifica_dimensioni(ld_lim_inf_dim_h, ld_lim_sup_dim_h, ld_lim_inf_dim_s, double(data), &
											  ls_cod_tab_rapp_misure) = -1 then
				g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Limite non Valido")
				return 2
			end if
		case "cod_tab_rapp_misure"
			ld_lim_inf_dim_h = this.getitemnumber(row, "lim_inf_dim_h")
			ld_lim_sup_dim_h = this.getitemnumber(row, "lim_sup_dim_h")
			ld_lim_inf_dim_s = this.getitemnumber(row, "lim_inf_dim_s")
			ld_lim_sup_dim_s = this.getitemnumber(row, "lim_sup_dim_s")
			if wf_verifica_dimensioni(ld_lim_inf_dim_h, ld_lim_sup_dim_h, ld_lim_inf_dim_s, ld_lim_sup_dim_s, &
											  i_coltext) = -1 then
				g_mb.messagebox("Dettaglio Tabella Rapporti Misure", "Modello non Valido")
				return 2
			end if
		case "flag_calcolo_hp"
			wf_tipo_hp(data)
	end choose
end if
end event

