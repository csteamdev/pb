﻿$PBExportHeader$w_duplica_mp_automatiche.srw
$PBExportComments$Finestra duplicazione MP automatiche
forward
global type w_duplica_mp_automatiche from w_cs_xx_principale
end type
type dw_ext_duplica_mp_automatiche from uo_cs_xx_dw within w_duplica_mp_automatiche
end type
type cb_1 from cb_prod_ricerca within w_duplica_mp_automatiche
end type
type mle_1 from multilineedit within w_duplica_mp_automatiche
end type
type cb_2 from commandbutton within w_duplica_mp_automatiche
end type
type cb_3 from cb_prod_ricerca within w_duplica_mp_automatiche
end type
type st_1 from statictext within w_duplica_mp_automatiche
end type
end forward

global type w_duplica_mp_automatiche from w_cs_xx_principale
integer width = 2651
integer height = 1196
string title = "Duplicazione MP"
dw_ext_duplica_mp_automatiche dw_ext_duplica_mp_automatiche
cb_1 cb_1
mle_1 mle_1
cb_2 cb_2
cb_3 cb_3
st_1 st_1
end type
global w_duplica_mp_automatiche w_duplica_mp_automatiche

event pc_setwindow;call super::pc_setwindow;dw_ext_duplica_mp_automatiche.set_dw_options(sqlca, pcca.null_object, c_nomodify + c_nodelete + c_newonopen + c_disableCC, &
								 c_noresizedw + c_nohighlightselected + c_cursorrowpointer)
save_on_close(c_socnosave)
end event

on w_duplica_mp_automatiche.create
int iCurrent
call super::create
this.dw_ext_duplica_mp_automatiche=create dw_ext_duplica_mp_automatiche
this.cb_1=create cb_1
this.mle_1=create mle_1
this.cb_2=create cb_2
this.cb_3=create cb_3
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ext_duplica_mp_automatiche
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.mle_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.cb_3
this.Control[iCurrent+6]=this.st_1
end on

on w_duplica_mp_automatiche.destroy
call super::destroy
destroy(this.dw_ext_duplica_mp_automatiche)
destroy(this.cb_1)
destroy(this.mle_1)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.st_1)
end on

type dw_ext_duplica_mp_automatiche from uo_cs_xx_dw within w_duplica_mp_automatiche
integer x = 23
integer y = 20
integer width = 2560
integer height = 400
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_ext_duplica_mp_automatiche"
boolean border = false
end type

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_modello"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "cod_modello"
			s_cs_xx.parametri.parametro_tipo_ricerca = 1
			if not isvalid(w_prodotti_ricerca) then
				window_open(w_prodotti_ricerca, 0)
			end if
			w_prodotti_ricerca.show()		
		end if
	case "des_modello"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_pos_ricerca = this.gettext()
			s_cs_xx.parametri.parametro_s_1 = "cod_modello"
			s_cs_xx.parametri.parametro_tipo_ricerca = 2
			if not isvalid(w_prodotti_ricerca) then
				window_open(w_prodotti_ricerca, 0)
			end if
			w_prodotti_ricerca.show()		
		end if
	case "cod_modello_origine"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "cod_modello_origine"
			s_cs_xx.parametri.parametro_tipo_ricerca = 1
			if not isvalid(w_prodotti_ricerca) then
				window_open(w_prodotti_ricerca, 0)
			end if
			w_prodotti_ricerca.show()		
		end if
	case "des_modello_origine"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_pos_ricerca = this.gettext()
			s_cs_xx.parametri.parametro_s_1 = "cod_modello_origine"
			s_cs_xx.parametri.parametro_tipo_ricerca = 2
			if not isvalid(w_prodotti_ricerca) then
				window_open(w_prodotti_ricerca, 0)
			end if
			w_prodotti_ricerca.show()		
		end if
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_str
	if i_colname = "cod_modello" then
		
		select des_prodotto
		into   :ls_str
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_prodotto = :i_coltext;
		if sqlca.sqlcode = 100 then
			return 1
		else
			dw_ext_duplica_mp_automatiche.setitem(1,"des_modello",ls_str)
		end if
	end if
	if i_colname = "cod_modello_origine" then
		
		
		select des_prodotto
		into   :ls_str
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_prodotto = :i_coltext;
		if sqlca.sqlcode = 100 then
			return 1
		else
			dw_ext_duplica_mp_automatiche.setitem(1,"des_modello_origine",ls_str)
		end if
	end if
end if
dw_ext_duplica_mp_automatiche.resetupdate()
end event

type cb_1 from cb_prod_ricerca within w_duplica_mp_automatiche
integer x = 2491
integer y = 120
integer width = 73
integer height = 80
integer taborder = 40
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_ext_duplica_mp_automatiche.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_modello"
end event

type mle_1 from multilineedit within w_duplica_mp_automatiche
integer x = 46
integer y = 480
integer width = 2423
integer height = 360
integer taborder = 20
boolean bringtotop = true
integer textsize = -11
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 79741120
string text = "Il modello selezionato nella finestra sottostante sarà duplicato nel modello indicato sopra; selezionando il Flag Cancella = S verranno cancellati eventuali dati già caricato nel modello di destinazione; nel caso Flag Cancella = N la procedura verificherà che non esistano alcune MP automatiche caricate per il modello selezionato sopra."
boolean border = false
alignment alignment = center!
boolean displayonly = true
end type

type cb_2 from commandbutton within w_duplica_mp_automatiche
integer x = 937
integer y = 860
integer width = 846
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui Duplicazione"
end type

event clicked;string ls_cod_modello_origine, ls_cod_modello_dest,ls_cod_mp_non_richiesta,ls_cod_gruppo_variante, ls_flag_fuori_campionario, &
       ls_flag_addizionale, ls_flag_default, ls_flag_blocco, ls_cod_tipo_det_ven, ls_flag_asta,ls_cod_verniciatura
long ll_cont , ll_max, ll_prog_mp_non_richiesta, ll_num_verniciatura
dec{4} ld_quan_utilizzo, ld_quan_tecnica, ld_lim_inf_dim_1,ld_lim_sup_dim_1,ld_lim_inf_dim_2, ld_lim_sup_dim_2, ld_altezza_comando
datetime ldt_data_blocco
  
declare cu_mp cursor for	
SELECT  cod_mp_non_richiesta,   
		  prog_mp_non_richiesta,
		  quan_utilizzo,   
		  quan_tecnica,   
		  cod_gruppo_variante,   
		  lim_inf_dim_1,   
		  lim_sup_dim_1,   
		  lim_inf_dim_2,   
		  lim_sup_dim_2,   
		  flag_fuori_campionario,   
		  flag_addizionale,   
		  flag_default,   
		  flag_blocco,   
		  data_blocco,   
		  cod_tipo_det_ven,   
		  flag_asta,   
		  altezza_comando
from    tab_mp_non_richieste
where   cod_azienda = :s_cs_xx.cod_azienda and
		  cod_modello_tenda = :ls_cod_modello_origine;
	
declare cu_verniciature cursor for  
select 	tab_mp_automatiche_mod_vern.cod_verniciatura, 
			tab_mp_automatiche_mod_vern.num_verniciatura
from    	tab_mp_automatiche_mod_vern
			join tab_modelli_verniciature on tab_mp_automatiche_mod_vern.cod_azienda = tab_modelli_verniciature.cod_azienda and
													tab_mp_automatiche_mod_vern.cod_modello = tab_modelli_verniciature.cod_modello and
													tab_mp_automatiche_mod_vern.cod_verniciatura = tab_modelli_verniciature.cod_verniciatura
where  tab_mp_automatiche_mod_vern.cod_azienda = :s_cs_xx.cod_azienda and
		  tab_mp_automatiche_mod_vern.cod_modello = :ls_cod_modello_origine and 
		  tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = :ls_cod_mp_non_richiesta and
		  tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = :ll_prog_mp_non_richiesta and
		  tab_modelli_verniciature.flag_blocco = 'N';
  
  
  
st_1.text = ""
dw_ext_duplica_mp_automatiche.accepttext()

ls_cod_modello_origine = dw_ext_duplica_mp_automatiche.getitemstring(dw_ext_duplica_mp_automatiche.getrow(),"cod_modello_origine")
ls_cod_modello_dest = dw_ext_duplica_mp_automatiche.getitemstring(dw_ext_duplica_mp_automatiche.getrow(),"cod_modello")
ll_cont = 0

select count(*)
into   :ll_cont
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :ls_cod_modello_origine;
if ll_cont = 0 or isnull(ll_cont) then
	g_mb.messagebox("Duplicazione","Indicare un modello di origine")
	return
end if

ll_cont = 0
select count(*)
into   :ll_cont
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :ls_cod_modello_dest;
if ll_cont = 0 or isnull(ll_cont) then
	g_mb.messagebox("Duplicazione","Indicare un modello di destinazione")
	return
end if


if g_mb.messagebox("Duplicazione","Sei sicuro di voler procedere alla duplicazione?", Question!, YesNo!, 2) = 1 then

	if dw_ext_duplica_mp_automatiche.getitemstring(dw_ext_duplica_mp_automatiche.getrow(),"flag_cancella") ="S" then
		if g_mb.messagebox("Duplicazione","Sei sicuro di voler cancellare tutte le MP del modello " + ls_cod_modello_dest, Question!,YesNo!,2) = 2 then return
			delete from tab_mp_automatiche_mod_vern
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_modello = :ls_cod_modello_dest;
			
			delete from tab_mp_non_richieste
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_modello_tenda = :ls_cod_modello_dest;
			
		// cancellazione records
	end if
	
	open cu_mp;	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in open cursore cu_mp.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	ll_cont = 0
	do while 1=1
		fetch cu_mp into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta, :ld_quan_utilizzo, :ld_quan_tecnica, :ls_cod_gruppo_variante, 
		                 :ld_lim_inf_dim_1, :ld_lim_sup_dim_1, :ld_lim_inf_dim_2, :ld_lim_sup_dim_2, 
							  :ls_flag_fuori_campionario, :ls_flag_addizionale, :ls_flag_default, :ls_flag_blocco, 
							  :ldt_data_blocco, :ls_cod_tipo_det_ven, :ls_flag_asta, :ld_altezza_comando;
		if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
			commit;
			st_1.text = "Duplicazione terminata; materie prime duplicate = " + string(ll_cont,"####0")
			f_scrivi_log("Duplicazione MP automatiche da " + ls_cod_modello_origine + " a " + ls_cod_modello_dest + " ESEGUITA CON SUCCESSO")
			g_mb.messagebox("Duplicazione MP","Elaborazione eseguita con successo!", Information!)
			exit
		end if
		st_1.text = "Duplicazione materia prima " + ls_cod_mp_non_richiesta
		ll_cont ++
		ll_max = 0
		select max(prog_mp_non_richiesta)
		into   :ll_max
		from   tab_mp_non_richieste  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_mp_non_richiesta = :ls_cod_mp_non_richiesta ;
		if ll_max = 0 then
			ll_max = 1
		else
			ll_max ++
		end if
		
		INSERT INTO tab_mp_non_richieste  
				( cod_azienda,   
				  cod_mp_non_richiesta,   
				  prog_mp_non_richiesta,   
				  quan_utilizzo,   
				  quan_tecnica,   
				  cod_gruppo_variante,   
				  cod_modello_tenda,   
				  lim_inf_dim_1,   
				  lim_sup_dim_1,   
				  lim_inf_dim_2,   
				  lim_sup_dim_2,   
				  flag_fuori_campionario,   
				  flag_addizionale,   
				  flag_default,   
				  flag_blocco,   
				  data_blocco,   
				  cod_tipo_det_ven,   
				  flag_asta,   
				  altezza_comando )  
		values (:s_cs_xx.cod_azienda,   
				  :ls_cod_mp_non_richiesta,   
				  :ll_max,   
				  :ld_quan_utilizzo,   
				  :ld_quan_tecnica,   
				  :ls_cod_gruppo_variante,   
				  :ls_cod_modello_dest,   
				  :ld_lim_inf_dim_1,   
				  :ld_lim_sup_dim_1,   
				  :ld_lim_inf_dim_2,   
				  :ld_lim_sup_dim_2,   
				  :ls_flag_fuori_campionario,   
				  :ls_flag_addizionale,   
				  :ls_flag_default,   
				  :ls_flag_blocco,   
				  :ldt_data_blocco,   
				  :ls_cod_tipo_det_ven,   
				  :ls_flag_asta,   
				  :ld_altezza_comando )  ;
		if sqlca.sqlcode < 0 or sqlca.sqlcode = 100 then
			g_mb.messagebox("Duplicazione","Errore durante la duplicazione. Errore nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
			rollback;
			exit
		end if
		
		
		// 30-10-2003 chieso da Valerio Centro Gibus: carico anche le verniciature se richiesto
		if dw_ext_duplica_mp_automatiche.getitemstring(dw_ext_duplica_mp_automatiche.getrow(),"flag_duplica_vernici") ="S" then
			open cu_verniciature;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Errore in open cursore cu_verniciature.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			do while 1=1
				fetch cu_verniciature into :ls_cod_verniciatura, :ll_num_verniciatura ;
				if sqlca.sqlcode <> 0 then exit
				
				INSERT INTO tab_mp_automatiche_mod_vern  
						( cod_azienda,   
						  cod_mp_non_richiesta,   
						  prog_mp_non_richiesta,   
						  cod_modello,   
						  cod_verniciatura,
						  num_verniciatura)  
				VALUES ( :s_cs_xx.cod_azienda,   
						   :ls_cod_mp_non_richiesta,   
						   :ll_max,   
						   :ls_cod_modello_dest,   
						   :ls_cod_verniciatura,
						   :ll_num_verniciatura)  ;
							
				if sqlca.sqlcode < 0 or sqlca.sqlcode = 100 then
					g_mb.messagebox("Duplicazione","Errore durante la duplicazione delle verniciature. ~r~nErrore nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
					rollback;
					exit
				end if
					
					
			loop
			close cu_verniciature;
			
		end if
		
	loop
	
end if

commit;

end event

type cb_3 from cb_prod_ricerca within w_duplica_mp_automatiche
integer x = 2491
integer y = 40
integer width = 73
integer height = 80
integer taborder = 31
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_ext_duplica_mp_automatiche.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_modello_origine"
end event

type st_1 from statictext within w_duplica_mp_automatiche
integer x = 46
integer y = 980
integer width = 2537
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
boolean focusrectangle = false
end type

