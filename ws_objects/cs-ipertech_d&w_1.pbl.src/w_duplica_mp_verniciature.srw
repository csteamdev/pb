﻿$PBExportHeader$w_duplica_mp_verniciature.srw
forward
global type w_duplica_mp_verniciature from w_cs_xx_principale
end type
type cb_ricerca from commandbutton within w_duplica_mp_verniciature
end type
type dw_selezione from uo_std_dw within w_duplica_mp_verniciature
end type
type dw_vern from uo_std_dw within w_duplica_mp_verniciature
end type
type dw_mp from uo_std_dw within w_duplica_mp_verniciature
end type
end forward

global type w_duplica_mp_verniciature from w_cs_xx_principale
integer width = 4073
integer height = 2448
string title = "Uitilu Associazione MP Verniciature"
cb_ricerca cb_ricerca
dw_selezione dw_selezione
dw_vern dw_vern
dw_mp dw_mp
end type
global w_duplica_mp_verniciature w_duplica_mp_verniciature

forward prototypes
public subroutine wf_resize (long fs_width, long fs_height)
public function integer wf_duplica (ref string fs_errore)
end prototypes

public subroutine wf_resize (long fs_width, long fs_height);long ll_larghezza_singola_dw, ll_x_dw, ll_altezza_singola_dw, ll_y_dw

 ll_y_dw = dw_selezione.y + dw_selezione.height + 30
ll_larghezza_singola_dw = (fs_width - 90) / 2
ll_altezza_singola_dw = fs_height - dw_selezione.height - dw_selezione.y - 30

dw_mp.x = 30
dw_mp.y = ll_y_dw
dw_mp.width = ll_larghezza_singola_dw
dw_mp.height = ll_altezza_singola_dw

dw_vern.x = dw_mp.x + ll_larghezza_singola_dw + 30
dw_vern.y = ll_y_dw
dw_vern.width = ll_larghezza_singola_dw
dw_vern.height = ll_altezza_singola_dw



end subroutine

public function integer wf_duplica (ref string fs_errore);string ls_cod_materia_prima, ls_cod_verniciatura,ls_cod_modello
long ll_i, ll_y, ll_prog_mp, ll_num_verniciatura

ls_cod_modello = dw_selezione.getitemstring(1, "as_cod_prodotto_finito")

for ll_i = 1 to dw_mp.rowcount()
	
	if dw_mp.getitemstring(ll_i, "flag_seleziona") = "S" then
		ls_cod_materia_prima = dw_mp.getitemstring(ll_i, "cod_mp_non_richiesta")
		ll_prog_mp = dw_mp.getitemnumber(ll_i, "prog_mp_non_richiesta")
		
		delete tab_mp_automatiche_mod_vern
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_modello = :ls_cod_modello and
		 		cod_mp_non_richiesta = :ls_cod_materia_prima and
				 prog_mp_non_richiesta = :ll_prog_mp;
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore durante la cancellazione dei dati precedenti.~r~nProdotto " + ls_cod_materia_prima + "~r~n" + sqlca.sqlerrtext
			rollback;
			return -1
		end if
		
		for ll_y = 1 to dw_vern.rowcount()
			if dw_vern.getitemstring(ll_y, "flag_seleziona") = "S" then
				ls_cod_verniciatura  = dw_vern.getitemstring(ll_y, "cod_verniciatura")
				ll_num_verniciatura = dw_vern.getitemnumber(ll_y, "num_verniciatura")
				
				INSERT INTO tab_mp_automatiche_mod_vern  
						( cod_azienda,   
						  cod_mp_non_richiesta,   
						  prog_mp_non_richiesta,   
						  cod_modello,   
						  cod_verniciatura,
						  num_verniciatura)  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ls_cod_materia_prima,
						  :ll_prog_mp,   
						  :ls_cod_modello,   
						  :ls_cod_verniciatura,
						  :ll_num_verniciatura)  ;
				if sqlca.sqlcode < 0 then
					fs_errore = "Errore durante inserimento nuove verniciature~r~MP " + ls_cod_materia_prima + "~r~n" + sqlca.sqlerrtext
					rollback;
					return -1
				end if
				
				f_log_sistema("Sovrascritto verniciature mp automatica:" + ls_cod_materia_prima + " prog.:" + string(ll_prog_mp) + " modello:" + ls_cod_modello, '1')
				
			end if
		next
	end if

next

commit;
return 0
end function

on w_duplica_mp_verniciature.create
int iCurrent
call super::create
this.cb_ricerca=create cb_ricerca
this.dw_selezione=create dw_selezione
this.dw_vern=create dw_vern
this.dw_mp=create dw_mp
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_vern
this.Control[iCurrent+4]=this.dw_mp
end on

on w_duplica_mp_verniciature.destroy
call super::destroy
destroy(this.cb_ricerca)
destroy(this.dw_selezione)
destroy(this.dw_vern)
destroy(this.dw_mp)
end on

event pc_setwindow;call super::pc_setwindow;cb_ricerca.text = "Ricerca"

set_w_options(c_NoEnablePopup + c_closenosave)

dw_selezione.insertrow(0)

dw_vern.insertrow(0)
dw_vern.settransobject(sqlca)

dw_mp.insertrow(0)
dw_mp.settransobject(sqlca)


end event

event resize;call super::resize;wf_resize(newwidth , newheight) 
end event

type cb_ricerca from commandbutton within w_duplica_mp_verniciature
integer x = 23
integer y = 16
integer width = 247
integer height = 112
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Ricerca"
end type

event clicked;if lower(text) = "ricerca" then
	dw_mp.Object.DataWindow.QueryMode = "yes"
	dw_mp.object.cod_mp_non_richiesta.TabSequence=10
	text = "Esegui Ric.."
else
	dw_mp.accepttext()
	dw_mp.Object.DataWindow.QueryMode = "no"
	dw_mp.object.cod_mp_non_richiesta.TabSequence=0
	
	string ls_cod_prodotto
	ls_cod_prodotto = dw_selezione.getitemstring(dw_selezione.getrow(),"as_cod_prodotto_finito")
	dw_mp.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)

	text = "Ricerca"
end if	


end event

type dw_selezione from uo_std_dw within w_duplica_mp_verniciature
event ue_key pbm_dwnkey
integer x = 293
integer y = 8
integer width = 3351
integer height = 140
integer taborder = 10
string dataobject = "d_duplica_mp_verniciature_selezione"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_key;choose case key
	case KeyEnter!
		accepttext()
		post event buttonclicked( 1, 1, this.object.cb_cerca)
end choose
end event

event buttonclicked;call super::buttonclicked;if isvalid(dwo) then

choose case dwo.name
	case "cb_ricerca_prodotto"
		
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		s_cs_xx.parametri.parametro_dw_1 = this
		s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_finito"	
		if not isvalid(w_prodotti_ricerca) then
			window_open(w_prodotti_ricerca, 0)
		end if
		
		w_prodotti_ricerca.show()
		
	case "cb_cerca"
		
		string ls_cod_prodotto
		
		ls_cod_prodotto = getitemstring(getrow(),"as_cod_prodotto_finito")
		
		if isnull(ls_cod_prodotto) then
			dw_mp.reset()
			dw_mp.triggerevent("ue_reset")
			dw_vern.reset()
		else
			dw_mp.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
			dw_vern.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
		end if
		postevent("pcd_modify")
		
	case "cb_esegui"
		string ls_errore
		long ll_i
		
		if wf_duplica(ref ls_errore) < 0 then
			rollback;
			g_mb.messagebox("APICE", ls_errore)
		end if
		g_mb.messagebox("APICE","Duplicazione eseguita correttamente")
		// deseleziona tutti i record
		dw_mp.object.t_flag_seleziona_tutto.text = "X"
		for ll_i = 1 to dw_mp.rowcount()
			dw_mp.setitem(ll_i, "flag_seleziona", "N")
		next
		
	end choose
	
end if
end event

type dw_vern from uo_std_dw within w_duplica_mp_verniciature
integer x = 1330
integer y = 256
integer width = 1129
integer height = 1108
integer taborder = 20
string dataobject = "d_duplica_mp_verniciature_lista_vern"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_mp from uo_std_dw within w_duplica_mp_verniciature
event ue_attiva_pulsante_esegui ( )
event ue_reset ( )
integer x = 18
integer y = 248
integer width = 1262
integer height = 1116
integer taborder = 10
string dataobject = "d_duplica_mp_verniciature_lista_mp"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_attiva_pulsante_esegui();long ll_i

for ll_i = 1 to dw_mp.rowcount()
	
	if dw_mp.getitemstring(ll_i, "flag_seleziona") = "S" then
		dw_selezione.object.cb_esegui.enabled = 'Yes'
		return
	end if	
next

dw_selezione.object.cb_esegui.enabled = 'No'

return
end event

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	
	choose case dwo.name
		case "flag_seleziona"
			
			postevent("ue_attiva_pulsante_esegui")
	end choose
end if


end event

event doubleclicked;call super::doubleclicked;long ll_i

if isvalid(dwo) then
	choose case dwo.name
		case "t_flag_seleziona_tutto"
			
			if dw_mp.object.t_flag_seleziona_tutto.text = "X" then
				dw_mp.object.t_flag_seleziona_tutto.text = "="
				for ll_i = 1 to rowcount()
					setitem(ll_i, "flag_seleziona", "S")
				next
			else
				dw_mp.object.t_flag_seleziona_tutto.text = "X"
				for ll_i = 1 to rowcount()
					setitem(ll_i, "flag_seleziona", "N")
				next
			end if
	end choose
end if


end event

