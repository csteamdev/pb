﻿$PBExportHeader$w_import_cod_raggruppo.srw
forward
global type w_import_cod_raggruppo from window
end type
type st_9 from statictext within w_import_cod_raggruppo
end type
type st_8 from statictext within w_import_cod_raggruppo
end type
type st_7 from statictext within w_import_cod_raggruppo
end type
type st_6 from statictext within w_import_cod_raggruppo
end type
type st_5 from statictext within w_import_cod_raggruppo
end type
type st_4 from statictext within w_import_cod_raggruppo
end type
type st_3 from statictext within w_import_cod_raggruppo
end type
type st_2 from statictext within w_import_cod_raggruppo
end type
type st_1 from statictext within w_import_cod_raggruppo
end type
type cb_import_txt from commandbutton within w_import_cod_raggruppo
end type
end forward

global type w_import_cod_raggruppo from window
integer width = 2313
integer height = 1344
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
cb_import_txt cb_import_txt
end type
global w_import_cod_raggruppo w_import_cod_raggruppo

on w_import_cod_raggruppo.create
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.cb_import_txt=create cb_import_txt
this.Control[]={this.st_9,&
this.st_8,&
this.st_7,&
this.st_6,&
this.st_5,&
this.st_4,&
this.st_3,&
this.st_2,&
this.st_1,&
this.cb_import_txt}
end on

on w_import_cod_raggruppo.destroy
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_import_txt)
end on

type st_9 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 324
integer width = 2149
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "2) fat_conversione_rag_mag"
boolean focusrectangle = false
end type

type st_8 from statictext within w_import_cod_raggruppo
integer x = 32
integer y = 744
integer width = 2208
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Dopo aver salvato il file TXT da Excel"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_7 from statictext within w_import_cod_raggruppo
integer x = 32
integer y = 836
integer width = 2208
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Ricordarsi di togliere la riga con le intestazioni delle colonne."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 576
integer width = 2149
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "5) cod_comodo_2"
boolean focusrectangle = false
end type

type st_5 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 492
integer width = 2153
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "4) flag_fiscale"
boolean focusrectangle = false
end type

type st_4 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 408
integer width = 2149
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "3) flag_lifo"
boolean focusrectangle = false
end type

type st_3 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 240
integer width = 2162
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "1) cod_prodotto_raggruppato"
boolean focusrectangle = false
end type

type st_2 from statictext within w_import_cod_raggruppo
integer x = 55
integer y = 152
integer width = 2162
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Questa procedura esegue l~'importazione in anagrafica prodotti dei seguenti campi :"
boolean focusrectangle = false
end type

type st_1 from statictext within w_import_cod_raggruppo
integer x = 18
integer y = 20
integer width = 2240
integer height = 92
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Procedura di importazione dati del codice raggruppamento."
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_import_txt from commandbutton within w_import_cod_raggruppo
integer x = 974
integer y = 964
integer width = 370
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;string ls_path, ls_filename, ls_str, ls_cod_prodotto, ls_cod_prodotto_raggruppo, ls_flag_lifo, ls_flag_fiscale, &
		ls_cod_comodo_2, ls_test, ls_fat_conversione_rag_mag
long 		ll_rc, ll_numfile, ll_rows, ll_pos
dec{4} 	ld_quantita_utilizzo, ld_limite_inferiore_dim_1,ld_limite_superiore_dim_1,ld_limite_inferiore_dim_2, &
			ld_limite_superiore_dim_2,ld_altezza_comando
dec{5}	ld_fat_conversione_rag_mag

ls_path = ""
ll_rc = GetFileOpenName("Importazione Codice Raggruppo", ls_path, ls_filename, "TXT", "File di testo (*.txt),*.txt" , "C:\temp")
if ll_rc < 1 then 
	g_mb.messagebox("Apice", "Operazione annullata")
	return
end if

if g_mb.messagebox("APICE","Procedo con l'elaborazione?",Question!,yesno!,2) = 2 then return

ll_numfile = fileopen(ls_path, linemode!, read!)
if ll_numfile < 1 then 
	g_mb.messagebox("Apice", "Errore in open del file")
	return
end if
	
ll_rows = 0
do while true
	ll_rc = fileread(ll_numfile, ls_str)
	if ll_rc = -100 then
		g_mb.messagebox("Apice", "Elaborazione terminata correttamente. Lette " + string(ll_rows) + " righe")
	   exit
	end if
	
	if ll_rc = -1 then
		g_mb.messagebox("Apice", "Errore durante la lettura del file di importazione")
	   exit
	end if
	ll_rows  ++
	
	// codice prodotto
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_cod_prodotto = mid(ls_str,1, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos +1)
	
	// codice prodotto RAGGRUPPO
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_cod_prodotto_raggruppo = mid(ls_str,1, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos +1)
	
	// stefanop 02/02/2010: Aggiunto nuovo codice
	// FAT conversione rag mag
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_fat_conversione_rag_mag = mid(ls_str,1, ll_pos -1)
	ld_fat_conversione_rag_mag = dec(ls_fat_conversione_rag_mag)
	
	ls_str = mid(ls_str, ll_pos +1)
	// ----
	
	// FLAG LIFO
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_flag_lifo = mid(ls_str,1, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos +1)
	
	// FLAG FISCALE
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_flag_fiscale = mid(ls_str,1, ll_pos -1)
	
	ls_str = mid(ls_str, ll_pos +1)
	
	// FLAG COD COMODO 2
	ll_pos = pos(ls_str, "~t")
	if ll_pos <= 0 then
		//Errore
	end if
	ls_cod_comodo_2 = mid(ls_str,1, ll_pos -1)
	
	// procedo con update.	
	
	if not isnull(ls_cod_prodotto_raggruppo) and len(trim(ls_cod_prodotto_raggruppo)) > 0 then
//		if ls_cod_prodotto = ls_cod_prodotto_raggruppo then continue
		if upper(ls_cod_prodotto_raggruppo) = "NO" then continue
		
		select des_prodotto
		into   :ls_test
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto_raggruppo;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Il prodotto " +ls_cod_prodotto_raggruppo+ " non esiste in anagrafica; l'importazione della riga " +string(ll_rows)+ " verrà saltata")
			continue
		end if
		
		if ls_cod_prodotto = ls_cod_prodotto_raggruppo then 
			// se i codici sono uguali modifico solamente i 2 flag e il comodo
			update	anag_prodotti
			set    		flag_lifo = :ls_flag_lifo,
					 	flag_articolo_fiscale = :ls_flag_fiscale,
					 	cod_comodo_2 = :ls_cod_comodo_2,
						fat_conversione_rag_mag = :ld_fat_conversione_rag_mag
			where  	cod_azienda = :s_cs_xx.cod_azienda and
					 	cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento prodotto "+ls_cod_prodotto+". Dettaglio~r~n" + sqlca.sqlerrtext)
				rollback;
				continue
			end if
			
		else
			
			update	anag_prodotti
			set    		cod_prodotto_raggruppato = :ls_cod_prodotto_raggruppo,
					 	flag_lifo = :ls_flag_lifo,
					 	flag_articolo_fiscale = :ls_flag_fiscale,
					 	cod_comodo_2 = :ls_cod_comodo_2,
						fat_conversione_rag_mag = :ld_fat_conversione_rag_mag
			where  	cod_azienda = :s_cs_xx.cod_azienda and
					 	cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento prodotto "+ls_cod_prodotto+". Dettaglio~r~n" + sqlca.sqlerrtext)
				rollback;
				continue
			end if
			
		end if
		
		commit;
	
	end if
loop
FileClose(ll_numfile)
	
commit;
end event

