﻿$PBExportHeader$w_importa_tessuti.srw
forward
global type w_importa_tessuti from w_cs_xx_principale
end type
type cb_importa from commandbutton within w_importa_tessuti
end type
type cb_seleziona from commandbutton within w_importa_tessuti
end type
type dw_selezione from u_dw_search within w_importa_tessuti
end type
end forward

global type w_importa_tessuti from w_cs_xx_principale
integer width = 2597
integer height = 832
string title = "Importazione file bolle tessuti"
cb_importa cb_importa
cb_seleziona cb_seleziona
dw_selezione dw_selezione
end type
global w_importa_tessuti w_importa_tessuti

type variables
transaction tessuti
end variables

forward prototypes
public function integer wf_disconnetti ()
public function integer wf_buffer (long fl_anno_bolla, long fl_num_bolla, string fs_cod_forn, decimal fd_altezza, string fs_cod_disegno, string fs_cod_colore, long fl_barcode, decimal fd_qta_netta, ref string fs_msg)
public function integer wf_connetti (ref string fs_msg)
public subroutine wf_rimpiazza (ref string fs_valore, string fs_old, string fs_new)
end prototypes

public function integer wf_disconnetti ();rollback using tessuti;
disconnect using tessuti;
destroy tessuti;

return 1
end function

public function integer wf_buffer (long fl_anno_bolla, long fl_num_bolla, string fs_cod_forn, decimal fd_altezza, string fs_cod_disegno, string fs_cod_colore, long fl_barcode, decimal fd_qta_netta, ref string fs_msg);datetime ldt_data_importazione
string ls_file

ldt_data_importazione = datetime(today(), now())
ls_file = dw_selezione.getitemstring(1, "path")

insert into js_bfr
 (		js_prog,
 		js_for,
	 	js_cdz,
		js_an,
		js_num,
		js_alt,
		js_dis,
		js_var,
		js_q,
		js_el,
		js_imp,
		js_usr,
		js_conf,
		js_usr2,
		js_fil)
values
(		:fl_barcode,
		:fs_cod_forn,
		:s_cs_xx.cod_azienda,
		:fl_anno_bolla,
		:fl_num_bolla,
		:fd_altezza,
		:fs_cod_disegno,
		:fs_cod_colore,
		:fd_qta_netta,
		'N',
		:ldt_data_importazione,
		:s_cs_xx.cod_utente,
		null,
		null,
		:ls_file)
using tessuti;

if tessuti.sqlcode<0 then
	fs_msg = "Errore in inserimento: "+tessuti.sqlerrtext
	return -1
end if

return 1
end function

public function integer wf_connetti (ref string fs_msg);integer li_risposta
string ls_valore, ls_enginetype, ls_password
n_cst_crypto lnv_crypt

//per la leggere il numero della chiave per la gestione della connessione al database tessuti usiamo il parametro
//aziendale PDT (stringa contenente un numero) che si riferisce al numero per la chiave database
//per la connessione al db dei tessuti
 select stringa
 into :ls_valore  
 from parametri_azienda
 where cod_azienda=:s_cs_xx.cod_azienda and  
       flag_parametro='S' and
       cod_parametro='PDT';

if sqlca.sqlcode<0 then
	fs_msg = "Parametro aziendale string PDT non trovato!~r~n"+&
					"Impostare il parametro con il numero del profilo con le info di connessione al db tessuti!"
	return -1
end if

if ls_valore="" or isnull(ls_valore) or not isnumber(ls_valore) then
	fs_msg = "Parametro aziendale string PDT non impostato o non numerico!~r~n"+&
					"Impostare il parametro con il numero del profilo con le info di connessione al db tessuti!"
	return -1
end if
/*
li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" + s_cs_xx.profilodefault, "profilo_"+is_profilo_tessuti, ls_valore)
if li_risposta=-1 then
	fs_msg = "Chiave 'profilo_"+is_profilo_tessuti+"' non trovata!"
	return -1
end if
*/

tessuti = create transaction

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "DBMS", tessuti.DBMS)
if li_risposta=-1 then
	fs_msg = "Chiave DBMS del 'profilo_"+ls_valore+"' non trovata!"
	destroy tessuti
	return -1
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "EngineType", ls_enginetype)
if li_risposta=-1 then
	fs_msg = "Chiave EngineType del 'profilo_"+ls_valore+"' non trovata!"
	destroy tessuti
	return -1
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "DBParm", tessuti.DBParm)
if li_risposta=-1 then
	fs_msg = "Chiave DBParm del 'profilo_"+ls_valore+"' non trovata!"
	destroy tessuti
	return -1
end if

choose case ls_enginetype
	case "SYBASE_ASA"
		//tutto è stato già valorizzato
		
	case "SYBASE_ASE"
		li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "Database", tessuti.Database)
		if li_risposta=-1 then
			fs_msg = "Chiave Database del 'profilo_"+ls_valore+"' non trovata!"
			destroy tessuti
			return -1
		end if
		
		//per la password occorre decriptare
		li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "LogPass", ls_password)
		if li_risposta=-1 then
			fs_msg = "Chiave LogPass del 'profilo_"+ls_valore+"' non trovata!"
			destroy tessuti
			return -1
		end if
		
		lnv_crypt = create n_cst_crypto
		if lnv_crypt.decryptdata(ls_password,ls_password) < 0 then
			ls_password = "prova tanto non funziona"
		end if
		destroy lnv_crypt
		
		tessuti.LogPass = ls_password
		//-------------------------------------------------------------------
		
		li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "ServerName", tessuti.ServerName)
		if li_risposta=-1 then
			fs_msg = "Chiave ServerName del 'profilo_"+ls_valore+"' non trovata!"
			destroy tessuti
			return -1
		end if
		li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + ls_valore, "LogId", tessuti.LogId)
		if li_risposta=-1 then
			fs_msg = "Chiave LogId del 'profilo_"+ls_valore+"' non trovata!"
			destroy tessuti
			return -1
		end if
		
end choose

tessuti.AutoCommit = False

/*
// Profile ptenda_tessuti
tessuti.DBMS = "ASE"//"SYC Adaptive Server Enterprise"
tessuti.Database = "sybmgmtlg"
tessuti.LogPass = "ptenda"
tessuti.ServerName = "PTENDA"//"ase.progettotenda.local"//"CS_TEAM"
tessuti.LogId = "guest"
tessuti.AutoCommit = False
tessuti.DBParm = "CHARSET='CP850',disablebind=1,OJSyntax = 'ANSI'"//"Release='12',CHARSET='CP850',disablebind=1,OJSyntax = 'ANSI'"
*/

disconnect using tessuti;
connect using tessuti;
if tessuti.sqlcode <> 0 then
	fs_msg = "Errore in connessione: "+tessuti.sqlerrtext
	destroy tessuti
	return -1
end if

return 1

end function

public subroutine wf_rimpiazza (ref string fs_valore, string fs_old, string fs_new);long start_pos=1

//rimpiazza la virgola con il punto --------PREZZO------
if not isnull(fs_valore) and fs_valore<>"" then
	// Find the first occurrence of old_str.
	start_pos = Pos(fs_valore, fs_old, start_pos)
	
	// Only enter the loop if you find old_str.
	DO WHILE start_pos > 0
		 // Replace old_str with new_str.
		 fs_valore = Replace(fs_valore, start_pos, Len(fs_old), fs_new)
		 // Find the next occurrence of old_str.
		 start_pos = Pos(fs_valore, fs_old, start_pos+Len(fs_new))
	LOOP	
else
	fs_valore="0"
end if
end subroutine

on w_importa_tessuti.create
int iCurrent
call super::create
this.cb_importa=create cb_importa
this.cb_seleziona=create cb_seleziona
this.dw_selezione=create dw_selezione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_importa
this.Control[iCurrent+2]=this.cb_seleziona
this.Control[iCurrent+3]=this.dw_selezione
end on

on w_importa_tessuti.destroy
call super::destroy
destroy(this.cb_importa)
destroy(this.cb_seleziona)
destroy(this.dw_selezione)
end on

event clicked;call super::clicked;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_importa from commandbutton within w_importa_tessuti
integer x = 1065
integer y = 576
integer width = 398
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;integer li_filenum, ll_err, ll_pos,ll_pos2
string ls_separ,ls_appoggio,ls_riga, ls_path, ls_msg
long ll_riga, ll_ret

//variabili
long ll_annobol,ll_numbol,ll_barcode
string ls_cod_disegno, ls_cod_colore,ls_cod_forn
decimal ld_altezza,ld_qtanetta

dw_selezione.accepttext()

ls_path = dw_selezione.getitemstring(1, "path")
ls_cod_forn = dw_selezione.getitemstring(1, "cod_fornitore")

if isnull(ls_cod_forn) or ls_cod_forn="" then
	g_mb.messagebox("APICE","Fornitore obbligatorio!",Exclamation!)
	 rollback using tessuti;
	return
end if

if isnull(ls_path) or ls_path="" then
	g_mb.messagebox("APICE","Selezionare il file da importare!",Exclamation!)
	 rollback using tessuti;
	return
end if

if not FileExists(ls_path) then
	g_mb.messagebox("APICE","Il file non esiste oppure è stato spostato!",Exclamation!)
	 rollback using tessuti;
	return
end if

if g_mb.messagebox("APICE","Importare il file di testo selezionato?",Question!, YesNo!, 1) = 2 then
	 rollback using tessuti;
	return
end if

setpointer(HourGlass!)

ls_separ = ","

li_filenum = fileopen(ls_path, linemode!)
if li_filenum < 0 then 
	g_mb.messagebox("APICE", "Errore in apertura del File!", StopSign!)
	 rollback using tessuti;
	return
end if

ll_riga = 0

//------------------------------
if wf_connetti(ls_msg) < 0 then
	ls_msg += " - Importazione non effettuata!"
	g_mb.messagebox("APICE", ls_msg, StopSign!)
	return
end if

do while true
	ll_err = fileread(li_filenum, ls_riga)
	if ll_err < 0 then exit
	
	ll_riga += 1	
	
	//anno bolla ---------------------------------------	
	//cerca il separetor
	ll_pos = pos(ls_riga, ls_separ)
	
	ls_appoggio = mid(ls_riga, 1,ll_pos -1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: anno bolla riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)		
		
		return
	end if
	ll_annobol = long(trim(ls_appoggio))
	//----------------------------------------------------	
	
	//numero bolla -------------------------------------	
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: numero bolla riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	ll_numbol = long(ls_appoggio)	
	//-------------------------------------------------------
	
	ll_pos = ll_pos2
	
	//salta 3 campi
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	
	//altezza  -------------------------------------
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: altezza riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	wf_rimpiazza(ls_appoggio, ".", ",")
	ld_altezza = dec(ls_appoggio)
	//-------------------------------------------------------
	
	ll_pos = ll_pos2	
	
	//cod disegno  -------------------------------------
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: codice disegno riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	
	//IL COD DISEGNO DEVE ESSERE NUMERICO MA SALVATO COME STRINGA
	//quindi se ci sono zeri li rimuovo
	ls_cod_disegno = string(long(ls_appoggio))
	//-------------------------------------------------------
	
	ll_pos = ll_pos2
	
	//cod colore  -------------------------------------	
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: codice colore riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	
	//IL COD COLORE DEVE ESSERE NUMERICO MA SALVATO COME STRINGA
	//quindi se ci sono zeri li rimuovo
	//se poi è proprio zero o vuoto metto null
	if long(ls_appoggio) = 0 or ls_appoggio="" then
		setnull(ls_cod_colore)
	else
		ls_cod_colore = string(long(ls_appoggio))
	end if
	//-------------------------------------------------------
	
	ll_pos = ll_pos2
	
	//salta 2 campi
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	
	//barcode  -------------------------------------
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		 rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: barcode riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	ll_barcode = long(ls_appoggio)
	//-------------------------------------------------------
	
	ll_pos = ll_pos2
	
	//salta 1 campo
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	ll_pos = ll_pos2
	
	//qtanetta  -------------------------------------	
	//cerca il separetor
	ll_pos2 = pos(ls_riga, ls_separ, ll_pos + 1)
	
	ls_appoggio = mid(ls_riga, ll_pos + 1,ll_pos2 - ll_pos - 1)
	
	if not isnumber(trim(ls_appoggio)) then
		rollback using tessuti;
		wf_disconnetti()
		ls_msg = "Dato non numerico! Campo previsto: quantità netta riga("+string(ll_riga)+")"
		g_mb.messagebox("APICE", "Errore in lettura del File!", StopSign!)
		
		return
	end if
	
	wf_rimpiazza(ls_appoggio, ".", ",")
	ld_qtanetta = dec(ls_appoggio)
	//-------------------------------------------------------
	
	//fai l'inserimento
	ll_ret = wf_buffer(ll_annobol, ll_numbol, ls_cod_forn, ld_altezza, ls_cod_disegno, &
				ls_cod_colore, ll_barcode, ld_qtanetta, ls_msg)
	
	if ll_ret < 0 then
		rollback using tessuti;
		wf_disconnetti()
		g_mb.messagebox("APICE", "Errore: "+ls_msg, StopSign!)
		
		return
	end if
	
loop

fileclose(li_filenum)
setpointer(Arrow!)
commit using tessuti;

wf_disconnetti()

g_mb.messagebox("APICE", "Importate "+string(ll_riga)+" righe!", Information!)
   
end event

type cb_seleziona from commandbutton within w_importa_tessuti
integer x = 2341
integer y = 144
integer width = 73
integer height = 76
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;integer li_value
string ls_nome_doc, ls_nome

li_value = GetFileOpenName("Seleziona File da importare",  &
ls_nome_doc, ls_nome,"TXT","Text Files (*.TXT), *.TXT")

if li_value <> 1 then
	g_mb.messagebox("APICE","Errore in selezione file ASCII!",Exclamation!)
	return
end if

dw_selezione.setitem(1, "path", ls_nome_doc)
end event

type dw_selezione from u_dw_search within w_importa_tessuti
integer x = 32
integer y = 16
integer width = 2510
integer height = 556
integer taborder = 70
string dataobject = "d_importa_tessuti_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

