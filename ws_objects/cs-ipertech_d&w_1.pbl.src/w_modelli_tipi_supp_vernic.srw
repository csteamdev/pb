﻿$PBExportHeader$w_modelli_tipi_supp_vernic.srw
$PBExportComments$Finestra Aggangio Modelli e verniciature per tipo supporto ( mi indica che quel tipo di supporto è valido per quel determinato modello di tenda verniciato in certo modo )
forward
global type w_modelli_tipi_supp_vernic from w_cs_xx_principale
end type
type dw_modelli_tipi_supp_vernic from uo_cs_xx_dw within w_modelli_tipi_supp_vernic
end type
end forward

global type w_modelli_tipi_supp_vernic from w_cs_xx_principale
integer width = 4091
integer height = 1940
string title = "Attivazione Supporti per Modello/Verniciatura"
dw_modelli_tipi_supp_vernic dw_modelli_tipi_supp_vernic
end type
global w_modelli_tipi_supp_vernic w_modelli_tipi_supp_vernic

on w_modelli_tipi_supp_vernic.create
int iCurrent
call super::create
this.dw_modelli_tipi_supp_vernic=create dw_modelli_tipi_supp_vernic
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_modelli_tipi_supp_vernic
end on

on w_modelli_tipi_supp_vernic.destroy
call super::destroy
destroy(this.dw_modelli_tipi_supp_vernic)
end on

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_modelli_tipi_supp_vernic,"cod_modello",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")
//
//f_PO_LoadDDDW_DW(dw_modelli_tipi_supp_vernic,"cod_verniciatura",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura')")
//
//
end event

event pc_setwindow;call super::pc_setwindow;dw_modelli_tipi_supp_vernic.set_dw_key("cod_azienda")
dw_modelli_tipi_supp_vernic.set_dw_key("cod_tipo_supporto")
dw_modelli_tipi_supp_vernic.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_modelli_tipi_supp_vernic
end event

type dw_modelli_tipi_supp_vernic from uo_cs_xx_dw within w_modelli_tipi_supp_vernic
integer x = 23
integer y = 20
integer width = 3986
integer height = 1784
string dataobject = "d_modelli_tipi_supp_vernic"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_supporto
long ll_errore

ls_cod_tipo_supporto = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_supporto")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_supporto)
if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_supporto
long l_Idx

ls_cod_tipo_supporto = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_supporto")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_supporto")) THEN
      SetItem(l_Idx, "cod_tipo_supporto", ls_cod_tipo_supporto)
   END IF
NEXT

end event

