﻿$PBExportHeader$w_modelli_verniciature.srw
$PBExportComments$Finestra Assegnazione Verniciature per Modello
forward
global type w_modelli_verniciature from w_cs_xx_principale
end type
type dw_modelli_vernicature_lista from uo_cs_xx_dw within w_modelli_verniciature
end type
end forward

global type w_modelli_verniciature from w_cs_xx_principale
integer width = 3598
integer height = 1160
string title = "Verniciature Possibili x Modello"
dw_modelli_vernicature_lista dw_modelli_vernicature_lista
end type
global w_modelli_verniciature w_modelli_verniciature

on w_modelli_verniciature.create
int iCurrent
call super::create
this.dw_modelli_vernicature_lista=create dw_modelli_vernicature_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_modelli_vernicature_lista
end on

on w_modelli_verniciature.destroy
call super::destroy
destroy(this.dw_modelli_vernicature_lista)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_modelli_vernicature_lista,"cod_verniciatura",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura')")

f_PO_LoadDDDW_DW(dw_modelli_vernicature_lista,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;dw_modelli_vernicature_lista.set_dw_key("cod_azienda")
dw_modelli_vernicature_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_modelli_vernicature_lista
end event

type dw_modelli_vernicature_lista from uo_cs_xx_dw within w_modelli_verniciature
integer x = 23
integer y = 20
integer width = 3506
integer height = 1000
string dataobject = "d_modelli_vernicature_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_modello
long ll_errore

ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_modello)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_modello
long ll_errore, l_Idx

ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_modello")) THEN
      SetItem(l_Idx, "cod_modello", ls_cod_modello)
   END IF
NEXT

end event

