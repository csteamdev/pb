﻿$PBExportHeader$w_mp_automatiche_mod_vern.srw
$PBExportComments$Finestra Abilitazione MP automatiche per Prodotto Verniciato
forward
global type w_mp_automatiche_mod_vern from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_mp_automatiche_mod_vern
end type
type dw_mp_automatiche_mod_vern from uo_cs_xx_dw within w_mp_automatiche_mod_vern
end type
type st_1 from statictext within w_mp_automatiche_mod_vern
end type
end forward

global type w_mp_automatiche_mod_vern from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3301
integer height = 1428
string title = "Attivaz. MP Aut. per Modello e Vernice"
cb_1 cb_1
dw_mp_automatiche_mod_vern dw_mp_automatiche_mod_vern
st_1 st_1
end type
global w_mp_automatiche_mod_vern w_mp_automatiche_mod_vern

on w_mp_automatiche_mod_vern.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_mp_automatiche_mod_vern=create dw_mp_automatiche_mod_vern
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_mp_automatiche_mod_vern
this.Control[iCurrent+3]=this.st_1
end on

on w_mp_automatiche_mod_vern.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_mp_automatiche_mod_vern)
destroy(this.st_1)
end on

event pc_setddlb;call super::pc_setddlb;//string ls_cod_modello

f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_modello",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")

//ls_cod_modello = dw_mp_automatiche_mod_vern.i_parentdw.getitemstring(dw_mp_automatiche_mod_vern.i_parentdw.getrow(), "cod_modello_tenda")
//
//if not isnull(ls_cod_modello) and len(ls_cod_modello) < 0 then 
//	f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
//						  "anag_prodotti","cod_prodotto","des_prodotto",&
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//						  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') " + &
//						  " and cod_prodotto in ( select cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + ls_cod_modello + "')")
//else
//	f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
//						  "anag_prodotti","cod_prodotto","des_prodotto",&
//						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//						  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') ")
//end if
end event

event pc_setwindow;call super::pc_setwindow;dw_mp_automatiche_mod_vern.set_dw_key("cod_azienda")
dw_mp_automatiche_mod_vern.set_dw_key("cod_mp_non_richiesta")
dw_mp_automatiche_mod_vern.set_dw_key("prog_mp_non_richiesta")
dw_mp_automatiche_mod_vern.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_mp_automatiche_mod_vern
end event

type cb_1 from commandbutton within w_mp_automatiche_mod_vern
integer x = 2853
integer y = 1224
integer width = 389
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Vern."
end type

event clicked;string ls_cod_mp_non_richiesta, ls_cod_modello,ls_cod_verniciatura
long ll_prog_mp_non_richiesta

ls_cod_mp_non_richiesta = dw_mp_automatiche_mod_vern.i_parentdw.getitemstring(dw_mp_automatiche_mod_vern.i_parentdw.getrow(), "cod_mp_non_richiesta")
ls_cod_modello = dw_mp_automatiche_mod_vern.i_parentdw.getitemstring(dw_mp_automatiche_mod_vern.i_parentdw.getrow(), "cod_modello_tenda")
ll_prog_mp_non_richiesta = dw_mp_automatiche_mod_vern.i_parentdw.getitemnumber(dw_mp_automatiche_mod_vern.i_parentdw.getrow(), "prog_mp_non_richiesta")

declare cu_verniciature cursor for
select cod_verniciatura
from   tab_modelli_verniciature
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_modello = :ls_cod_modello and
		 flag_blocco = 'N';

open cu_verniciature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN cursore cu_verniciature.~r~n"  + sqlca.sqlerrtext)
	rollback;
	return
end if

delete tab_mp_automatiche_mod_vern
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_mp_non_richiesta = :ls_cod_mp_non_richiesta and
		prog_mp_non_richiesta = :ll_prog_mp_non_richiesta;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione verniciature.~r~n"  + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_verniciature into :ls_cod_verniciatura;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in fetch cursore cu_verniciature.~r~n"  + sqlca.sqlerrtext)
		close cu_verniciature;
		rollback;
		return
	end if
	
	if sqlca.sqlcode = 100 then
		close cu_verniciature;
		exit
	end if
	
	INSERT INTO tab_mp_automatiche_mod_vern  
			( cod_azienda,   
			  cod_mp_non_richiesta,   
			  prog_mp_non_richiesta, 
			  cod_modello,
			  cod_verniciatura )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_mp_non_richiesta,   
			  :ll_prog_mp_non_richiesta,  
			  :ls_cod_modello,
			  :ls_cod_verniciatura )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento verniciature dal modello configuratore.~r~n"  + sqlca.sqlerrtext)
		close cu_verniciature;
		rollback;
		return
	end if
loop


commit;
end event

type dw_mp_automatiche_mod_vern from uo_cs_xx_dw within w_mp_automatiche_mod_vern
integer x = 23
integer y = 20
integer width = 3223
integer height = 1000
string dataobject = "d_mp_automatiche_mod_vern"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_mp_non_richiesta
long ll_errore, ll_prog_mp_non_richiesta

ls_cod_mp_non_richiesta = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_mp_non_richiesta")
ll_prog_mp_non_richiesta = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_mp_non_richiesta")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_mp_non_richiesta, ll_prog_mp_non_richiesta)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_mp_non_richiesta
long l_Idx, ll_prog_mp_non_richiesta

ls_cod_mp_non_richiesta = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_mp_non_richiesta")
ll_prog_mp_non_richiesta = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_mp_non_richiesta")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_mp_non_richiesta")) THEN
      SetItem(l_Idx, "cod_mp_non_richiesta", ls_cod_mp_non_richiesta)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_mp_non_richiesta")) or GetItemnumber(l_Idx, "prog_mp_non_richiesta") < 1 THEN
      SetItem(l_Idx, "prog_mp_non_richiesta", ll_prog_mp_non_richiesta)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;string ls_cod_modello
long ll_prog_mp_non_richiesta

ll_prog_mp_non_richiesta = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_mp_non_richiesta")
ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello_tenda")

SetItem(this.getrow(), "prog_mp_non_richiesta", ll_prog_mp_non_richiesta)
SetItem(this.getrow(), "cod_modello", ls_cod_modello)

if not isnull(ls_cod_modello) and len(ls_cod_modello) > 0 then 
	f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
						  "anag_prodotti","cod_prodotto","des_prodotto",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
						  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') " + &
						  " and cod_prodotto in ( select cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + ls_cod_modello + "')")
else
	f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
						  "anag_prodotti","cod_prodotto","des_prodotto",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
						  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') ")
end if

end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "cod_modello"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
			s_cs_xx.parametri.parametro_s_1 = "cod_modello"
			s_cs_xx.parametri.parametro_tipo_ricerca = 1
			if not isvalid(w_prodotti_ricerca) then
				window_open(w_prodotti_ricerca, 0)
			end if
			w_prodotti_ricerca.show()		
		end if
end choose
end event

event itemchanged;call super::itemchanged;string ls_cod_modello, ls_null

if i_extendmode then
	if i_colname = "cod_modello" then
		
		setnull(ls_null)
		
		setitem(getrow(),"cod_verniciatura", ls_null)
		
		ls_cod_modello = i_coltext
		
		if not isnull(ls_cod_modello) and len(ls_cod_modello) > 0 then 
			f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
								  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') " + &
								  " and cod_prodotto in ( select cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + ls_cod_modello + "')")
		else
			f_PO_LoadDDDW_DW(dw_mp_automatiche_mod_vern,"cod_verniciatura",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
								  s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura') ")
		end if
	end if
end if

end event

type st_1 from statictext within w_mp_automatiche_mod_vern
integer x = 23
integer y = 1040
integer width = 3223
integer height = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean enabled = false
string text = "E~' POSSIBILE ASSEGNARE MODELLI / VERNICIATURE SOLAMENTE SE L~'ABBINAMENTO FRA MODELLO / VERNICIATURA E~' GIA~' STATO ABILITATO NELLA GESTIONE FLAGS CONFIGURATORE DA BOTTONE ~'VERNICIATURE~'"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

