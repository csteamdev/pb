﻿$PBExportHeader$w_ptenda_test.srw
forward
global type w_ptenda_test from window
end type
type cb_1 from commandbutton within w_ptenda_test
end type
type sle_1 from singlelineedit within w_ptenda_test
end type
end forward

global type w_ptenda_test from window
integer width = 1138
integer height = 552
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
sle_1 sle_1
end type
global w_ptenda_test w_ptenda_test

type variables
transaction it_cgibus, it_viropa
string is_reg_chiave_root
end variables

forward prototypes
public function integer wf_connetti_cgibus ()
public function integer wf_connetti_viropa ()
public function integer wf_cancella_prodotto (string as_cod_prodotto)
end prototypes

public function integer wf_connetti_cgibus ();integer	li_errore, li_num_utenti, li_risposta, ll_ret
string		ls_parametri, ls_cod_utente, ls_password, ls_pass, ls_db, ls_cod_azienda, ls_default, &
			ls_valore,ls_placca,ls_ini_section,fs_errore, ls_xml, ls_option, ls_logpass
			
it_cgibus = create transaction

// -- Leggo i dati dal registro
ls_ini_section= "dati_aziendali\cgibus"
li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "servername", it_cgibus.ServerName)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: servername.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbms", it_cgibus.DBMS)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: dbms.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "database_intranet", it_cgibus.Database)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: database.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "logid", it_cgibus.LogId)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: logid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: logpass.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if it_cgibus.DBMS <> "ODBC" then
	
	if isnull(ls_logpass) or ls_logpass="" then		
		
		messagebox("", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", StopSign!)
		return -1	
		
	end if	
		
	n_cst_crypto luo_crypto
	luo_crypto = create n_cst_crypto
	ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
	destroy luo_crypto;
	if ll_ret < 0 then
		messagebox("", "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
					"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
					"- alcuni simboli.Modificare la password e riprovare", StopSign!)
		return -1
	end if
	
	it_cgibus.LogPass = ls_logpass
	
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "userid", it_cgibus.UserId)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: userid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbpass", it_cgibus.DBPass)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: dbpass.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbparm", it_cgibus.DBParm)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: dbparm.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "option", ls_option)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: option.", StopSign!)
	return -1
end if

it_cgibus.dbparm = it_cgibus.dbparm + ls_option

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "lock", it_cgibus.Lock)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: lock.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.2")		 

disconnect using it_cgibus;
connect using it_cgibus;
if it_cgibus.sqlcode <> 0 then
	messagebox("", "Errore durante la connessione al database~r~n" + it_cgibus.sqlerrtext, StopSign!)
	return -1
end if

return 0

end function

public function integer wf_connetti_viropa ();integer	li_errore, li_num_utenti, li_risposta, ll_ret
string		ls_parametri, ls_cod_utente, ls_password, ls_pass, ls_db, ls_cod_azienda, ls_default, &
			ls_valore,ls_placca,ls_ini_section,fs_errore, ls_xml, ls_option, ls_logpass
			
it_viropa = create transaction

// -- Leggo i dati dal registro
ls_ini_section= "dati_aziendali\viropa"
li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "servername", it_viropa.ServerName)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: servername.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbms", it_viropa.DBMS)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: dbms.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "database_intranet", it_viropa.Database)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: database.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "logid", it_viropa.LogId)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: logid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: logpass.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.1")		 

if it_viropa.DBMS <> "ODBC" then
	
	if isnull(ls_logpass) or ls_logpass="" then		
		
		messagebox("", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", StopSign!)
		return -1	
		
	end if	
		
	n_cst_crypto luo_crypto
	luo_crypto = create n_cst_crypto
	ll_ret = luo_crypto.decryptdata( ls_logpass, ls_logpass)  
	destroy luo_crypto;
	if ll_ret < 0 then
		messagebox("", "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
					"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
					"- alcuni simboli.Modificare la password e riprovare", StopSign!)
		return -1
	end if
	
	it_viropa.LogPass = ls_logpass
	
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "userid", it_viropa.UserId)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: userid.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbpass", it_viropa.DBPass)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: dbpass.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "dbparm", it_viropa.DBParm)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: dbparm.", StopSign!)
	return -1
end if

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "option", ls_option)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazione del database sul registro: option.", StopSign!)
	return -1
end if

it_viropa.dbparm = it_viropa.dbparm + ls_option

li_risposta = registryget(is_reg_chiave_root + ls_ini_section, "lock", it_viropa.Lock)
if li_risposta = -1 then
	messagebox("", "Mancano le impostazioni del database sul registro: lock.", StopSign!)
	return -1
end if

li_risposta = Registryset(is_reg_chiave_root + "profilocorrente", "appo", "1.2")		 

disconnect using it_viropa;
connect using it_viropa;
if it_viropa.sqlcode <> 0 then
	messagebox("", "Errore durante la connessione al database~r~n" + it_viropa.sqlerrtext, StopSign!)
	return -1
end if

return 0

end function

public function integer wf_cancella_prodotto (string as_cod_prodotto);string		ls_des_prodotto, ls_descrizione, ls_destinatari[]
boolean 	lb_error = false
datetime ldt_oggi, ldt_ora

ldt_oggi = datetime(today(), 00:00:00)
ldt_ora = datetime(today(), now())

// ** Controllo se ci sono record relativi al prodotto **
// Ptenda
//select 	des_prodotto
//into 		:ls_des_prodotto
//from 		anag_prodotti
//where 	cod_azienda =  :s_cs_xx.cod_azienda and
//			cod_prodotto = :as_cod_prodotto
//using 		sqlca;
//
//if sqlca.sqlcode <> 0 then
//	messagebox("", "Errore durante la ricerca del prodotto nel db : Progettotenda" + sqlca.sqlerrtext+" prodotto non presente.", StopSign!)
//	rollback using sqlca;
//	return -1
//end if
//// -----------
//
//// Centro Gibus
//select 	des_prodotto
//into 		:ls_des_prodotto
//from 		anag_prodotti
//where 	cod_azienda =  :s_cs_xx.cod_azienda and
//			cod_prodotto = :as_cod_prodotto
//using 		it_cgibus;
//
//if it_cgibus.sqlcode <> 0 then
//	messagebox("", "Errore durante la ricerca del prodotto nel db : Centro gibus" + sqlca.sqlerrtext+" prodotto non presente.", StopSign!)
//	rollback using it_cgibus;
//	return -1
//end if
//// -----------
//
//// Viropa
//select 	des_prodotto
//into 		:ls_des_prodotto
//from 		anag_prodotti
//where 	cod_azienda =  :s_cs_xx.cod_azienda and
//			cod_prodotto = :as_cod_prodotto
//using 		it_viropa;
//
//if it_viropa.sqlcode <> 0 then
//	messagebox("", "Errore durante la ricerca del prodotto nel db : Viropa" + sqlca.sqlerrtext+" prodotto non presente.", StopSign!)
//	rollback using it_viropa;
//	return -1
//end if
//// -----------

// collo bottiglia da qui
// --Il prodotto esiste su tutti i DB, posso procedere con l'eliminazione 
//delete 	anag_prodotti
//where 	cod_azienda =  :s_cs_xx.cod_azienda and
//			cod_prodotto = :as_cod_prodotto
//using 		it_cgibus;
//
//commit using it_cgibus;
// ----------------

//delete 	anag_prodotti
//where 	cod_azienda =  :s_cs_xx.cod_azienda and
//			cod_prodotto = :as_cod_prodotto
//using 		it_viropa;
//
//commit using it_viropa;
// -----------

delete 	anag_prodotti
where 	cod_azienda =  :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;

commit;

// -- Controllo errori
if lb_error then
	rollback using sqlca;
	rollback using it_cgibus;
	rollback using it_viropa;
	return -1
end if
// -----------
// a qui

//messagebox("asd", "asd")

// -- Notifico cancellazione agli uffici
ls_descrizione = "Cancellato il prodotto con codice: "+as_cod_prodotto
//if wf_invia_mail("Cancellato un prodotto a magazzino.", ls_descrizione) < 0 then
//	messagebox(gs_app_name, "Errore durante l'invio mail.", StopSign!)
//	rollback using sqlca;
//	rollback using it_viropa;
//	rollback using it_cgibus;
//	return -1
//end if
// -----------

// -- Scrivo file log
INSERT INTO log_sistema (data_registrazione, ora_registrazione, utente, messaggio, flag_tipo_log)
VALUES (:ldt_oggi, :ldt_ora, :s_cs_xx.cod_utente, :ls_descrizione, "C");
// -----------

//commit using it_cgibus;
//commit using it_viropa;

return 0
end function

on w_ptenda_test.create
this.cb_1=create cb_1
this.sle_1=create sle_1
this.Control[]={this.cb_1,&
this.sle_1}
end on

on w_ptenda_test.destroy
destroy(this.cb_1)
destroy(this.sle_1)
end on

event open;is_reg_chiave_root = "HKEY_LOCAL_MACHINE\SOFTWARE\Consulting&Software\"

wf_connetti_viropa()
wf_connetti_cgibus()
end event

type cb_1 from commandbutton within w_ptenda_test
integer x = 46
integer y = 160
integer width = 1029
integer height = 260
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cancella"
end type

event clicked;wf_cancella_prodotto(sle_1.text)
end event

type sle_1 from singlelineedit within w_ptenda_test
integer x = 46
integer y = 40
integer width = 1051
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "A033"
borderstyle borderstyle = stylelowered!
end type

