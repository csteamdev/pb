﻿$PBExportHeader$w_reparti_ptenda.srw
forward
global type w_reparti_ptenda from w_reparti
end type
type dw_folder from u_folder within w_reparti_ptenda
end type
type dw_reparti_ext from uo_cs_xx_dw within w_reparti_ptenda
end type
end forward

global type w_reparti_ptenda from w_reparti
integer width = 3319
integer height = 2564
string title = "Reparti Produzione"
dw_folder dw_folder
dw_reparti_ext dw_reparti_ext
end type
global w_reparti_ptenda w_reparti_ptenda

on w_reparti_ptenda.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_reparti_ext=create dw_reparti_ext
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_reparti_ext
end on

on w_reparti_ptenda.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_reparti_ext)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_reparti_ext.set_dw_options(sqlca,dw_reparti_lista,c_sharedata+c_scrollparent,c_default)

l_objects[1] = dw_reparti_det
dw_folder.fu_assigntab(1,"Dettagli", l_objects)

l_objects[1] = dw_reparti_ext
dw_folder.fu_assigntab(2,"Comunicazione",l_objects)

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(1)
end event

event pc_setddlb;call super::pc_setddlb;
//aggiunto
f_PO_LoadDDDW_DW(dw_reparti_det,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")			
end event

type cb_documento from w_reparti`cb_documento within w_reparti_ptenda
boolean visible = false
integer x = 2885
end type

event cb_documento::clicked;
//evento che deve sovrascrivere il padre !!!!!!!!!!!!!!!!!!!!!!!!!!
//putroppo tale finestra eredita dalla w_reparti, ma quello dello standard, non del global service
//ora che abbiamo messo le librerie del global service nel repository gibus occorre riportare
//quello che non è presente nella w_reparti di global service

dw_reparti_lista.accepttext()

if dw_reparti_lista.getrow() > 0 and not isnull(dw_reparti_lista.getrow()) then 
//	s_cs_xx.parametri.parametro_s_10 = dw_reparti_lista.getitemstring(dw_reparti_lista.getrow(), "cod_reparto")	
//	window_open_parm(w_reparti_blob, -1, dw_reparti_lista)
	s_cs_xx.parametri.parametro_s_1 = dw_reparti_lista.getitemstring(dw_reparti_lista.getrow(), "cod_reparto")	
	open(w_reparti_ole)
end if
end event

type dw_reparti_lista from w_reparti`dw_reparti_lista within w_reparti_ptenda
integer x = 32
integer width = 3227
integer height = 784
end type

event dw_reparti_lista::rowfocuschanged;call super::rowfocuschanged;if not isnull(currentrow) and currentrow > 0 then
	if getitemstring(currentrow,"flag_tipo_reparto") <> "E" then
		dw_folder.fu_selecttab(1)
	end if
end if
end event

type dw_reparti_det from w_reparti`dw_reparti_det within w_reparti_ptenda
integer x = 46
integer y = 940
integer width = 3177
integer height = 1460
end type

type dw_folder from u_folder within w_reparti_ptenda
integer x = 23
integer y = 820
integer width = 3223
integer height = 1620
integer taborder = 30
end type

event po_tabvalidate;call super::po_tabvalidate;if i_clickedtab = 2 then
	if dw_reparti_lista.getitemstring(dw_reparti_lista.getrow(),"flag_tipo_reparto") <> "E" then
		g_mb.messagebox("APICE","Non è stato selezionato un reparto esterno",exclamation!)
		i_taberror = -1
	end if
end if
end event

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_uo_dw_1 = dw_reparti_ext
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_reparti_ext from uo_cs_xx_dw within w_reparti_ptenda
integer x = 55
integer y = 952
integer width = 2066
integer height = 1316
integer taborder = 40
string dataobject = "d_reparti_ext"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_nome, ls_cognome, ls_cod_qualifica, ls_indirizzo_residenza, ls_localita_residenza, ls_frazione_residenza, ls_cap_residenza, &
		 ls_provincia_residenza, ls_telefono, ls_fax, ls_cod_operaio, ls_des_qualifica

choose case i_colname
	case "cod_operaio"
		
		ls_cod_operaio = i_coltext
	   setnull(ls_nome)
	   setnull(ls_cognome)
	   setnull(ls_cod_qualifica)
	   setnull(ls_indirizzo_residenza)
	   setnull(ls_localita_residenza)
	   setnull(ls_frazione_residenza)
	   setnull(ls_cap_residenza)
	   setnull(ls_provincia_residenza)
	   setnull(ls_telefono)
	   setnull(ls_fax)		
		
		select nome,
				 cognome,
		       cod_qualifica,
				 indirizzo_residenza,
				 localita_residenza,
				 frazione_residenza,
				 cap_residenza,
				 provincia_residenza,
				 telefono,
				 fax
		  into :ls_nome,
		  		 :ls_cognome,
		       :ls_cod_qualifica,
				 :ls_indirizzo_residenza,
				 :ls_localita_residenza,
				 :ls_frazione_residenza,
				 :ls_cap_residenza,
				 :ls_provincia_residenza,
				 :ls_telefono,
				 :ls_fax
		  from anag_operai
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_operaio = :ls_cod_operaio;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in fase di lettura dati da tabella anag_operai " + sqlca.sqlerrtext)
			return
		end if
		
		if sqlca.sqlcode = 0 then 
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "nome_responsabile", ls_nome)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "cognome_responsabile", ls_cognome)
			
			select des_qualifica
			  into :ls_des_qualifica
			  from tab_qualifiche
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_qualifica = :ls_cod_qualifica;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in fase di lettura dati da tabella anag_operai " + sqlca.sqlerrtext)
				return
			end if			
			
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "qualifica_responsabile", ls_des_qualifica)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "indirizzo", ls_indirizzo_residenza)			
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "frazione", ls_frazione_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "localita", ls_localita_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "cap", ls_cap_residenza)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "provincia", ls_provincia_residenza)						
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "telefono", ls_telefono)
			dw_reparti_det.setitem(dw_reparti_lista.getrow(), "fax", ls_fax)									
		end if	
						 
end choose
end event

event updateend;call super::updateend;
dw_reparti_ext.object.b_ricerca_cliente.enabled = false
end event

event pcd_modify;call super::pcd_modify;
dw_reparti_ext.object.b_ricerca_cliente.enabled = true
end event

event pcd_new;call super::pcd_new;
dw_reparti_ext.object.b_ricerca_cliente.enabled = true
end event

event pcd_view;call super::pcd_view;
dw_reparti_ext.object.b_ricerca_cliente.enabled = false
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_reparti_ext,"cod_cliente")
end choose
end event

