﻿$PBExportHeader$w_report_mp_automatiche.srw
$PBExportComments$Finestra Report MP Automatiche
forward
global type w_report_mp_automatiche from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_mp_automatiche
end type
type cb_1 from commandbutton within w_report_mp_automatiche
end type
end forward

global type w_report_mp_automatiche from w_cs_xx_principale
integer width = 4919
integer height = 2332
string title = "Report MP Automatiche"
dw_report dw_report
cb_1 cb_1
end type
global w_report_mp_automatiche w_report_mp_automatiche

on w_report_mp_automatiche.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.cb_1
end on

on w_report_mp_automatiche.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

//set_w_options(c_closenosave + c_autoposition + c_noresizewin)
set_w_options(c_closenosave + c_autoposition + c_resizewin)

dw_report.set_dw_options(sqlca, &
                         i_openparm, &
                         c_nomodify + &
                         c_nodelete + &
								 c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report

end event

type dw_report from uo_cs_xx_dw within w_report_mp_automatiche
integer x = 27
integer y = 124
integer width = 4800
integer height = 2076
integer taborder = 10
string dataobject = "d_report_mp_automatiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda, i_parentdw.getitemstring(i_parentdw.getrow(),"cod_modello_tenda"))

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_1 from commandbutton within w_report_mp_automatiche
integer x = 46
integer y = 20
integer width = 613
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ricerca Libera"
end type

event clicked;LONG  l_Error

l_Error = dw_report.Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

