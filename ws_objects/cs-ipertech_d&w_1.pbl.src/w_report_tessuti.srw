﻿$PBExportHeader$w_report_tessuti.srw
$PBExportComments$Finestra Report Tessuti
forward
global type w_report_tessuti from w_cs_xx_principale
end type
type cb_7 from commandbutton within w_report_tessuti
end type
type dw_report_tessuti from uo_cs_xx_dw within w_report_tessuti
end type
type cb_1 from commandbutton within w_report_tessuti
end type
type st_1 from statictext within w_report_tessuti
end type
type em_1 from editmask within w_report_tessuti
end type
type st_2 from statictext within w_report_tessuti
end type
type em_2 from editmask within w_report_tessuti
end type
type rb_1 from radiobutton within w_report_tessuti
end type
type st_3 from statictext within w_report_tessuti
end type
type em_3 from editmask within w_report_tessuti
end type
type rb_2 from radiobutton within w_report_tessuti
end type
type rb_3 from radiobutton within w_report_tessuti
end type
type cb_2 from commandbutton within w_report_tessuti
end type
type cb_3 from commandbutton within w_report_tessuti
end type
type cb_4 from commandbutton within w_report_tessuti
end type
type cb_5 from commandbutton within w_report_tessuti
end type
type cb_6 from commandbutton within w_report_tessuti
end type
end forward

global type w_report_tessuti from w_cs_xx_principale
integer width = 3474
integer height = 2132
string title = "Report Tessuti"
boolean hscrollbar = true
boolean vscrollbar = true
cb_7 cb_7
dw_report_tessuti dw_report_tessuti
cb_1 cb_1
st_1 st_1
em_1 em_1
st_2 st_2
em_2 em_2
rb_1 rb_1
st_3 st_3
em_3 em_3
rb_2 rb_2
rb_3 rb_3
cb_2 cb_2
cb_3 cb_3
cb_4 cb_4
cb_5 cb_5
cb_6 cb_6
end type
global w_report_tessuti w_report_tessuti

event pc_setwindow;call super::pc_setwindow;dw_report_tessuti.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report_tessuti.settransobject(sqlca)
dw_report_tessuti.retrieve(s_cs_xx.cod_azienda)

dw_report_tessuti.object.datawindow.print.preview="YES"
end event

on w_report_tessuti.create
int iCurrent
call super::create
this.cb_7=create cb_7
this.dw_report_tessuti=create dw_report_tessuti
this.cb_1=create cb_1
this.st_1=create st_1
this.em_1=create em_1
this.st_2=create st_2
this.em_2=create em_2
this.rb_1=create rb_1
this.st_3=create st_3
this.em_3=create em_3
this.rb_2=create rb_2
this.rb_3=create rb_3
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_4=create cb_4
this.cb_5=create cb_5
this.cb_6=create cb_6
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_7
this.Control[iCurrent+2]=this.dw_report_tessuti
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.em_1
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.em_2
this.Control[iCurrent+8]=this.rb_1
this.Control[iCurrent+9]=this.st_3
this.Control[iCurrent+10]=this.em_3
this.Control[iCurrent+11]=this.rb_2
this.Control[iCurrent+12]=this.rb_3
this.Control[iCurrent+13]=this.cb_2
this.Control[iCurrent+14]=this.cb_3
this.Control[iCurrent+15]=this.cb_4
this.Control[iCurrent+16]=this.cb_5
this.Control[iCurrent+17]=this.cb_6
end on

on w_report_tessuti.destroy
call super::destroy
destroy(this.cb_7)
destroy(this.dw_report_tessuti)
destroy(this.cb_1)
destroy(this.st_1)
destroy(this.em_1)
destroy(this.st_2)
destroy(this.em_2)
destroy(this.rb_1)
destroy(this.st_3)
destroy(this.em_3)
destroy(this.rb_2)
destroy(this.rb_3)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_4)
destroy(this.cb_5)
destroy(this.cb_6)
end on

type cb_7 from commandbutton within w_report_tessuti
integer x = 23
integer y = 248
integer width = 434
integer height = 100
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "EXPORT"
end type

event clicked;long job

dw_report_tessuti.saveas( )

end event

type dw_report_tessuti from uo_cs_xx_dw within w_report_tessuti
integer x = 23
integer y = 360
integer width = 3749
integer height = 4100
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_report_tessuti"
end type

type cb_1 from commandbutton within w_report_tessuti
integer x = 23
integer y = 20
integer width = 430
integer height = 108
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Filtro Avanzato"
end type

event clicked;string ls_null

setnull(ls_null)

if dw_report_tessuti.setfilter(ls_null) = 1 then
	dw_report_tessuti.filter()
end if

end event

type st_1 from statictext within w_report_tessuti
integer x = 503
integer y = 40
integer width = 512
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "GRUPPO TESSUTO:"
boolean focusrectangle = false
end type

type em_1 from editmask within w_report_tessuti
integer x = 1029
integer y = 20
integer width = 434
integer height = 100
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type st_2 from statictext within w_report_tessuti
integer x = 1554
integer y = 40
integer width = 512
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "COLORE TESSUTO:"
boolean focusrectangle = false
end type

type em_2 from editmask within w_report_tessuti
integer x = 2080
integer y = 20
integer width = 434
integer height = 100
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type rb_1 from radiobutton within w_report_tessuti
integer x = 503
integer y = 160
integer width = 535
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Senza Mantovava"
boolean lefttext = true
end type

type st_3 from statictext within w_report_tessuti
integer x = 2606
integer y = 40
integer width = 434
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "CODICE VELOCE:"
boolean focusrectangle = false
end type

type em_3 from editmask within w_report_tessuti
integer x = 3063
integer y = 20
integer width = 251
integer height = 100
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string displaydata = "~r"
end type

type rb_2 from radiobutton within w_report_tessuti
integer x = 1120
integer y = 160
integer width = 466
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Senza Cordolo"
boolean lefttext = true
end type

type rb_3 from radiobutton within w_report_tessuti
integer x = 1646
integer y = 160
integer width = 635
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
string text = "Senza Passamaneria"
boolean lefttext = true
end type

type cb_2 from commandbutton within w_report_tessuti
integer x = 2926
integer y = 160
integer width = 411
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Applica Filtri"
end type

event clicked;string ls_filter

ls_filter = ""
if rb_1.checked = true then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "isnull(colore_mantovana)"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

if rb_2.checked = true then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "isnull(colore_cordolo)"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

if rb_3.checked = true then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "isnull(colore_passamaneria)"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

if em_1.text <> "" and not isnull(em_1.text) then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "codice_tessuto like '"+em_1.text+"%'"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

if em_2.text <> "" and not isnull(em_2.text) then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "colore_tessuto = '"+em_2.text+"'"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

if em_3.text <> "" and not isnull(em_3.text) then
	if ls_filter <> "" then ls_filter = ls_filter + " and "
	ls_filter = "codice_veloce = '"+em_3.text+"'"
	dw_report_tessuti.setfilter(ls_filter)
	dw_report_tessuti.filter()
end if

end event

type cb_3 from commandbutton within w_report_tessuti
integer x = 2491
integer y = 160
integer width = 411
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&zzera Filtri"
end type

event clicked;string ls_null

em_1.text = ""
em_2.text = ""
em_3.text = ""
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false

dw_report_tessuti.setfilter("")
dw_report_tessuti.filter()
end event

type cb_4 from commandbutton within w_report_tessuti
integer x = 23
integer y = 140
integer width = 434
integer height = 100
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&STAMPA"
end type

event clicked;long job

job=PrintOpen() 
PrintDataWindow(job, dw_report_tessuti) 
PrintClose(job)
end event

type cb_5 from commandbutton within w_report_tessuti
integer x = 960
integer y = 260
integer width = 549
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pagina Precedente"
end type

event clicked;dw_report_tessuti.scrollpriorpage()
end event

type cb_6 from commandbutton within w_report_tessuti
integer x = 1531
integer y = 260
integer width = 549
integer height = 80
integer taborder = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pagina Successiva"
end type

event clicked;dw_report_tessuti.scrollnextpage()
end event

