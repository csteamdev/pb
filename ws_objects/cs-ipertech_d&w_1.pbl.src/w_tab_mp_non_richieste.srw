﻿$PBExportHeader$w_tab_mp_non_richieste.srw
$PBExportComments$Finestra Tabella MP Non Richieste
forward
global type w_tab_mp_non_richieste from w_cs_xx_principale
end type
type cb_associa_mod_vern from commandbutton within w_tab_mp_non_richieste
end type
type cb_import_txt from commandbutton within w_tab_mp_non_richieste
end type
type st_3 from statictext within w_tab_mp_non_richieste
end type
type st_2 from statictext within w_tab_mp_non_richieste
end type
type st_1 from statictext within w_tab_mp_non_richieste
end type
type cb_export_txt from commandbutton within w_tab_mp_non_richieste
end type
type cb_reset from commandbutton within w_tab_mp_non_richieste
end type
type cb_2 from commandbutton within w_tab_mp_non_richieste
end type
type cb_report from commandbutton within w_tab_mp_non_richieste
end type
type cb_ricerca from commandbutton within w_tab_mp_non_richieste
end type
type dw_tab_mp_non_richieste_det from uo_cs_xx_dw within w_tab_mp_non_richieste
end type
type dw_ricerca from u_dw_search within w_tab_mp_non_richieste
end type
type dw_tab_mp_non_richieste_lista from uo_cs_xx_dw within w_tab_mp_non_richieste
end type
type dw_folder_search from u_folder within w_tab_mp_non_richieste
end type
type cb_mod_vern from commandbutton within w_tab_mp_non_richieste
end type
end forward

global type w_tab_mp_non_richieste from w_cs_xx_principale
integer width = 3086
integer height = 2516
string title = "Materie Prime Automatiche"
cb_associa_mod_vern cb_associa_mod_vern
cb_import_txt cb_import_txt
st_3 st_3
st_2 st_2
st_1 st_1
cb_export_txt cb_export_txt
cb_reset cb_reset
cb_2 cb_2
cb_report cb_report
cb_ricerca cb_ricerca
dw_tab_mp_non_richieste_det dw_tab_mp_non_richieste_det
dw_ricerca dw_ricerca
dw_tab_mp_non_richieste_lista dw_tab_mp_non_richieste_lista
dw_folder_search dw_folder_search
cb_mod_vern cb_mod_vern
end type
global w_tab_mp_non_richieste w_tab_mp_non_richieste

type variables
boolean ib_new
end variables

forward prototypes
public function integer wf_verifica_dimensioni (double fd_lim_inf_dim_1, double fd_lim_sup_dim_1, double fd_lim_inf_dim_2, double fd_lim_sup_dim_2, string fs_cod_modello_tenda, string fs_cod_mp_non_richiesta, long fl_prog_mp_non_richiesta)
end prototypes

public function integer wf_verifica_dimensioni (double fd_lim_inf_dim_1, double fd_lim_sup_dim_1, double fd_lim_inf_dim_2, double fd_lim_sup_dim_2, string fs_cod_modello_tenda, string fs_cod_mp_non_richiesta, long fl_prog_mp_non_richiesta);// Funzione che verifica l'eventuale incompatibilità tra i limiti delle dimensioni passate come argomento
// e i limiti delle dimensioni dei mp_non_richieste già presenti in tab_mp_non_richieste
// Argomenti :
// 	fd_lim_inf_dim_1 : limite inferiore dimensione 1
// 	fd_lim_sup_dim_1 : limite superiore dimensione 1
// 	fd_lim_inf_dim_2 : limite inferiore dimensione 2
// 	fd_lim_sup_dim_2 : limite superiore dimensione 2
//		fs_cod_modello_tenda : modello tenda su cui gestire i limiti
// Ritorna :
//		0 : Limiti Validi
//		-1: Limiti non Validi
// Mauro 17/08/1998 (che afa ragassi!!!)

long ll_cont_1, ll_cont_2
double ld_lim_inf_dim_1, ld_lim_sup_dim_1, ld_lim_inf_dim_2, ld_lim_sup_dim_2

if (fd_lim_inf_dim_1 > fd_lim_sup_dim_1 and fd_lim_sup_dim_1 <> 0) or &
	(fd_lim_inf_dim_2 > fd_lim_sup_dim_2 and fd_lim_sup_dim_2 <> 0) then
	return -1
end if

declare cur_mp_non_richieste cursor for
  select lim_inf_dim_1,
         lim_sup_dim_1,
         lim_inf_dim_2,
         lim_sup_dim_2
    from tab_mp_non_richieste 
   where cod_azienda = :s_cs_xx.cod_azienda 
	  and cod_modello_tenda = :fs_cod_modello_tenda
	  and cod_mp_non_richiesta = :fs_cod_mp_non_richiesta
	  and prog_mp_non_richiesta <> :fl_prog_mp_non_richiesta;

open cur_mp_non_richieste;

fetch cur_mp_non_richieste into :ld_lim_inf_dim_1, :ld_lim_sup_dim_1, :ld_lim_inf_dim_2, :ld_lim_sup_dim_2;
//			(ld_lim_inf_dim_2 >= fd_lim_inf_dim_2 and ld_lim_inf_dim_2 <= fd_lim_sup_dim_2) or &
//			(ld_lim_sup_dim_2 >= fd_lim_inf_dim_2 and ld_lim_sup_dim_2 <= fd_lim_sup_dim_2) or &

//			(fd_lim_inf_dim_2 >= ld_lim_inf_dim_2 and fd_lim_inf_dim_2 <= ld_lim_sup_dim_2) or &
//			(fd_lim_sup_dim_2 >= ld_lim_inf_dim_2 and fd_lim_sup_dim_2 <= ld_lim_sup_dim_2) or &
//			(fd_lim_inf_dim_2 > fd_lim_sup_dim_2 and fd_lim_sup_dim_2 <> 0) or &


do while sqlca.sqlcode = 0
	if 	(ld_lim_inf_dim_1 >= fd_lim_inf_dim_1 and ld_lim_inf_dim_1 <= fd_lim_sup_dim_1) or &
			(ld_lim_sup_dim_1 >= fd_lim_inf_dim_1 and ld_lim_sup_dim_1 <= fd_lim_sup_dim_1) or &
			(fd_lim_inf_dim_1 >= ld_lim_inf_dim_1 and fd_lim_inf_dim_1 <= ld_lim_sup_dim_1) or &
			(fd_lim_sup_dim_1 >= ld_lim_inf_dim_1 and fd_lim_sup_dim_1 <= ld_lim_sup_dim_1) or &
			(fd_lim_inf_dim_1 > fd_lim_sup_dim_1 and fd_lim_sup_dim_1 <> 0) then
		close cur_mp_non_richieste;
		return -1	
	end if
	fetch cur_mp_non_richieste into :ld_lim_inf_dim_1, :ld_lim_sup_dim_1, :ld_lim_inf_dim_2, :ld_lim_sup_dim_2;
loop
close cur_mp_non_richieste;

return 0
end function

on w_tab_mp_non_richieste.create
int iCurrent
call super::create
this.cb_associa_mod_vern=create cb_associa_mod_vern
this.cb_import_txt=create cb_import_txt
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.cb_export_txt=create cb_export_txt
this.cb_reset=create cb_reset
this.cb_2=create cb_2
this.cb_report=create cb_report
this.cb_ricerca=create cb_ricerca
this.dw_tab_mp_non_richieste_det=create dw_tab_mp_non_richieste_det
this.dw_ricerca=create dw_ricerca
this.dw_tab_mp_non_richieste_lista=create dw_tab_mp_non_richieste_lista
this.dw_folder_search=create dw_folder_search
this.cb_mod_vern=create cb_mod_vern
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_associa_mod_vern
this.Control[iCurrent+2]=this.cb_import_txt
this.Control[iCurrent+3]=this.st_3
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_export_txt
this.Control[iCurrent+7]=this.cb_reset
this.Control[iCurrent+8]=this.cb_2
this.Control[iCurrent+9]=this.cb_report
this.Control[iCurrent+10]=this.cb_ricerca
this.Control[iCurrent+11]=this.dw_tab_mp_non_richieste_det
this.Control[iCurrent+12]=this.dw_ricerca
this.Control[iCurrent+13]=this.dw_tab_mp_non_richieste_lista
this.Control[iCurrent+14]=this.dw_folder_search
this.Control[iCurrent+15]=this.cb_mod_vern
end on

on w_tab_mp_non_richieste.destroy
call super::destroy
destroy(this.cb_associa_mod_vern)
destroy(this.cb_import_txt)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_export_txt)
destroy(this.cb_reset)
destroy(this.cb_2)
destroy(this.cb_report)
destroy(this.cb_ricerca)
destroy(this.dw_tab_mp_non_richieste_det)
destroy(this.dw_ricerca)
destroy(this.dw_tab_mp_non_richieste_lista)
destroy(this.dw_folder_search)
destroy(this.cb_mod_vern)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_tab_mp_non_richieste_lista.set_dw_key("cod_azienda")
dw_tab_mp_non_richieste_lista.set_dw_key("cod_mp_non_richiesta")
dw_tab_mp_non_richieste_lista.set_dw_key("prog_mp_non_richiesta")
dw_tab_mp_non_richieste_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_tab_mp_non_richieste_det.set_dw_options(sqlca,dw_tab_mp_non_richieste_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_mp_non_richieste_lista
ib_new = false

l_criteriacolumn[1] = "cod_gruppo_variante"
l_criteriacolumn[2] = "flag_asta"
l_criteriacolumn[3] = "altezza_comando"
l_criteriacolumn[4] = "cod_modello"
l_criteriacolumn[5] = "flag_addizionale"
l_criteriacolumn[6] = "flag_fuori_campionario"
l_criteriacolumn[7] = "flag_blocco"
l_criteriacolumn[8] = "cod_mp_non_richiesta"

l_searchtable[1] = "tab_mp_non_richieste"
l_searchtable[2] = "tab_mp_non_richieste"
l_searchtable[3] = "tab_mp_non_richieste"
l_searchtable[4] = "tab_mp_non_richieste"
l_searchtable[5] = "tab_mp_non_richieste"
l_searchtable[6] = "tab_mp_non_richieste"
l_searchtable[7] = "tab_mp_non_richieste"
l_searchtable[8] = "tab_mp_non_richieste"

l_searchcolumn[1] = "cod_gruppo_variante"
l_searchcolumn[2] = "flag_asta"
l_searchcolumn[3] = "altezza_comando"
l_searchcolumn[4] = "cod_modello_tenda"
l_searchcolumn[5] = "flag_addizionale"
l_searchcolumn[6] = "flag_fuori_campionario"
l_searchcolumn[7] = "flag_blocco"
l_searchcolumn[8] = "cod_mp_non_richiesta"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tab_mp_non_richieste_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])
l_objects[1] = dw_tab_mp_non_richieste_lista
l_objects[2] = cb_mod_vern
l_objects[3] = cb_2
l_objects[4] = cb_report
l_objects[5] = cb_associa_mod_vern
dw_folder_search.fu_assigntab(1, "L.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_tab_mp_non_richieste_lista.change_dw_current()
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_tab_mp_non_richieste_det,"cod_mp_non_richiesta",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_mp_non_richieste')")

f_PO_LoadDDDW_DW(dw_tab_mp_non_richieste_det,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_mp_non_richieste_det,"cod_modello_tenda",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")

//f_po_loaddddw_dw(dw_tab_mp_non_richieste_det, "cod_tipo_det_ven", sqlca, &
//                 "tab_tipi_det_ven", "cod_tipo_det_ven", "des_tipo_det_ven", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")



//dw_ricerca.fu_loadcode("cod_mp_non_richiesta",&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_mp_non_richieste')", "(Tutti)")

dw_ricerca.fu_loadcode("cod_gruppo_variante",&
                       "gruppi_varianti",&
							  "cod_gruppo_variante",&
							  "des_gruppo_variante",&
                       "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)")

dw_ricerca.fu_loadcode("cod_modello",&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli') order by cod_prodotto", "(Tutti)")


end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_mp_non_richieste_lista.getrow()
ib_new = false
end event

type cb_associa_mod_vern from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 328
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Utility Vern."
end type

event clicked;if dw_tab_mp_non_richieste_lista.getrow() > 0 then
	window_open(w_duplica_mp_verniciature, -1)
end if
end event

type cb_import_txt from commandbutton within w_tab_mp_non_richieste
integer x = 18
integer y = 2280
integer width = 370
integer height = 80
integer taborder = 12
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "TXT Importa"
end type

event clicked;string 	ls_path, ls_filename,ls_codice_nuovo,ls_cod_prodotto,ls_gruppo_variante,ls_modello_prodotto_finito,ls_flag_asta

long 		ll_rows, li_rc,ll_i,ll_progressivo, ll_max

dec{4} 	ld_quantita_utilizzo, ld_limite_inferiore_dim_1,ld_limite_superiore_dim_1,ld_limite_inferiore_dim_2,ld_limite_superiore_dim_2,ld_altezza_comando, &
			ld_limite_inferiore_dim_3,   ld_limite_superiore_dim_3,   ld_limite_inferiore_dim_4,   ld_limite_superiore_dim_4,   ld_quantita_commerciale, ld_quantita_tecnica

datastore lds_import
// ------------------------------------------



lds_import = CREATE datastore
lds_import.dataobject = 'd_tab_mp_non_richieste_export'
lds_import.settransobject(sqlca)

ls_path = ""
li_rc = GetFileOpenName("Importazione Variazioni", ls_path, ls_filename, "TXT", "File di testo (*.txt),*.txt" , "C:\temp")
if li_rc < 1 then 
	g_mb.messagebox("Apice", "Operazione annullata")
	return
end if

if g_mb.messagebox("APICE","Procedo con l'elaborazione?",Question!,yesno!,2) = 2 then return

li_rc = lds_import.importfile(ls_path)

choose case  li_rc 
		
	case -1
		g_mb.messagebox("Apice", "No rows or startrow value supplied is greater than the number of rows in the file")
		return
		
	case -2
		g_mb.messagebox("Apice", "File Vuoto")
		return
		
	case -3   
		g_mb.messagebox("Apice", "Invalid argument")
		return
		
	case -4   
		g_mb.messagebox("Apice", "Invalid Input: FORSE NON E' STATA ELIMINATA LA RIGA DI INTESTAZIONE.")
		return
		
	case -5
		g_mb.messagebox("Apice", "Could not open the file")
		return
		
	case -6
		g_mb.messagebox("Apice", "Could not close the file")
		return
		
	case -7   
		g_mb.messagebox("Apice", "Error reading the text")
		return
		
	case -8
		g_mb.messagebox("Apice", "Unsupported file name suffix (must be *.txt, *.csv, *.dbf or *.xml)")
		return
		
	case -10   
		g_mb.messagebox("Apice", "Unsupported dBase file format (not version 2 or 3)")
		return
		
	case -11
		g_mb.messagebox("Apice", "XML Parsing Error; XML parser libraries not found or XML not well formed")
		return
		
	case -12   
		g_mb.messagebox("Apice", "XML Template does not exist or does not match the DataWindow")
		return
		
	case -13    
		g_mb.messagebox("Apice", "Unsupported DataWindow style for import")
		return
		
	case -14    
		g_mb.messagebox("Apice", "Error resolving DataWindow nesting")
		return
		
end choose

ll_rows = lds_import.rowcount()

for ll_i = 1 to ll_rows
	ls_codice_nuovo = lds_import.getitemstring(ll_i, "codice_nuovo")
	ls_cod_prodotto = lds_import.getitemstring(ll_i, "tab_mp_non_richieste_cod_mp_non_richiesta")
	ll_progressivo  = lds_import.getitemnumber(ll_i, "progressivo")
	
	if len(trim(ls_codice_nuovo)) < 1 then setnull(ls_codice_nuovo)
	if len(trim(ls_cod_prodotto)) < 1 then setnull(ls_cod_prodotto)
	
	// nessun cambiamento, passo alla riga successiva.
	if isnull(ls_codice_nuovo) then continue	
	
	
	ld_quantita_utilizzo  = lds_import.getitemnumber(ll_i, "quantita_utilizzo")
	ls_gruppo_variante  = lds_import.getitemstring(ll_i, "gruppo_variante")
	ls_modello_prodotto_finito  = lds_import.getitemstring(ll_i, "modello_prodotto_finito")
	ld_limite_inferiore_dim_1  = lds_import.getitemnumber(ll_i, "limite_inferiore_dim_1")
	ld_limite_superiore_dim_1  = lds_import.getitemnumber(ll_i, "limite_superiore_dim_1")
	ld_limite_inferiore_dim_2  = lds_import.getitemnumber(ll_i, "limite_inferiore_dim_2")
	ld_limite_superiore_dim_2  = lds_import.getitemnumber(ll_i, "limite_superiore_dim_2")
	ls_flag_asta  = lds_import.getitemstring(ll_i, "flag_asta")
	ld_altezza_comando  = lds_import.getitemnumber(ll_i, "altezza_comando")
	ld_limite_inferiore_dim_3  = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_lim_inf_dim_3")
	ld_limite_superiore_dim_3  = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_lim_sup_dim_3")
	ld_limite_inferiore_dim_4  = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_lim_inf_dim_4")
	ld_limite_superiore_dim_4  = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_lim_sup_dim_4")
	ld_quantita_commerciale = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_quan_commerciale")
	ld_quantita_tecnica = lds_import.getitemnumber(ll_i, "tab_mp_non_richieste_quan_tecnica")
	
	// è cambiato il codice
	if not isnull(ls_codice_nuovo)  and not isnull(ls_cod_prodotto) then 
		
		select max(prog_mp_non_richiesta)
		into   :ll_max
		from   tab_mp_non_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_mp_non_richiesta = :ls_codice_nuovo;
		
		if ll_max = 0 or isnull(ll_max) then
			ll_max = 1
		else
			ll_max ++
		end if
		
		if ld_limite_inferiore_dim_1 > 99999999 	then ld_limite_inferiore_dim_1 = 99999999
		if ld_limite_superiore_dim_1 > 99999999 	then ld_limite_superiore_dim_1 = 99999999
		if ld_limite_inferiore_dim_2 > 99999999 	then ld_limite_inferiore_dim_2 = 99999999
		if ld_limite_superiore_dim_2 > 99999999 	then ld_limite_superiore_dim_2 = 99999999
		if ld_limite_inferiore_dim_3 > 99999999 	then ld_limite_inferiore_dim_3 = 99999999
		if ld_limite_superiore_dim_3 > 99999999 	then ld_limite_superiore_dim_3 = 99999999
		if ld_limite_inferiore_dim_4 > 99999999 	then ld_limite_inferiore_dim_4 = 99999999
		if ld_limite_superiore_dim_4 > 99999999 	then ld_limite_superiore_dim_4 = 99999999
		
		INSERT INTO tab_mp_non_richieste  
				( cod_azienda,   
				  cod_mp_non_richiesta,   
				  prog_mp_non_richiesta,   
				  quan_utilizzo,   
				  cod_gruppo_variante,   
				  cod_modello_tenda,   
				  lim_inf_dim_1,   
				  lim_sup_dim_1,   
				  lim_inf_dim_2,   
				  lim_sup_dim_2,   
				  flag_fuori_campionario,   
				  flag_addizionale,   
				  flag_default,   
				  flag_blocco,   
				  data_blocco,   
				  cod_tipo_det_ven,   
				  flag_asta,   
				  altezza_comando,
				  lim_inf_dim_3,   
				  lim_sup_dim_3,   
				  lim_inf_dim_4,   
				  lim_sup_dim_4,
				  quan_commerciale,
				  quan_tecnica)  
			SELECT cod_azienda,   
					:ls_codice_nuovo,   
					:ll_max,   
					:ld_quantita_utilizzo,   
					:ls_gruppo_variante,   
					:ls_modello_prodotto_finito,   
					:ld_limite_inferiore_dim_1,   
					:ld_limite_superiore_dim_1,   
					:ld_limite_inferiore_dim_2,   
					:ld_limite_superiore_dim_2,   
					flag_fuori_campionario,   
					flag_addizionale,   
					flag_default,   
					flag_blocco,   
					data_blocco,   
					cod_tipo_det_ven,   
					:ls_flag_asta,   
					:ld_altezza_comando ,
					:ld_limite_inferiore_dim_3,   
					:ld_limite_superiore_dim_3,   
					:ld_limite_inferiore_dim_4,   
					:ld_limite_superiore_dim_4,   
					:ld_quantita_commerciale,
					:ld_quantita_tecnica
			 FROM tab_mp_non_richieste  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					cod_mp_non_richiesta = :ls_cod_prodotto AND  
					prog_mp_non_richiesta = :ll_progressivo ; 

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante modifica del codice " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
			
		INSERT INTO tab_mp_automatiche_mod_vern  
			( cod_azienda,   
			  cod_mp_non_richiesta,   
			  prog_mp_non_richiesta,   
			  cod_modello,   
			  cod_verniciatura,
			  num_verniciatura)  
		SELECT cod_azienda,   
				:ls_codice_nuovo,   
				:ll_max,   
				cod_modello,   
				cod_verniciatura,
				num_verniciatura
		 FROM tab_mp_automatiche_mod_vern
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_mp_non_richiesta = :ls_cod_prodotto AND  
				prog_mp_non_richiesta = :ll_progressivo ; 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante modifica del codice " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		delete tab_mp_automatiche_mod_vern
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_mp_non_richiesta = :ls_cod_prodotto AND  
				prog_mp_non_richiesta = :ll_progressivo ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante modifica/cancellazione del codice " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		delete tab_mp_non_richieste
		WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_mp_non_richiesta = :ls_cod_prodotto AND  
				prog_mp_non_richiesta = :ll_progressivo ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante modifica/cancellazione del codice " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
	end if	
	
	// si tratta di un nuovo codice
	if not isnull(ls_codice_nuovo)  and isnull(ls_cod_prodotto) then 
		
		select max(prog_mp_non_richiesta)
		into   :ll_max
		from   tab_mp_non_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_mp_non_richiesta = :ls_codice_nuovo;
		
		if ll_max = 0 or isnull(ll_max) then
			ll_max = 1
		else
			ll_max ++
		end if
		
		if ld_limite_inferiore_dim_1 > 99999999 then ld_limite_inferiore_dim_1 = 99999999
		if ld_limite_superiore_dim_1 > 99999999 then ld_limite_superiore_dim_1 = 99999999
		if ld_limite_inferiore_dim_2 > 99999999 then ld_limite_inferiore_dim_2 = 99999999
		if ld_limite_superiore_dim_2 > 99999999 then ld_limite_superiore_dim_2 = 99999999
		if ld_limite_inferiore_dim_3 > 99999999 	then ld_limite_inferiore_dim_3 = 99999999
		if ld_limite_superiore_dim_3 > 99999999 	then ld_limite_superiore_dim_3 = 99999999
		if ld_limite_inferiore_dim_4 > 99999999 	then ld_limite_inferiore_dim_4 = 99999999
		if ld_limite_superiore_dim_4 > 99999999 	then ld_limite_superiore_dim_4 = 99999999
			
		
		INSERT INTO tab_mp_non_richieste  
				( cod_azienda,   
				  cod_mp_non_richiesta,   
				  prog_mp_non_richiesta,   
				  quan_utilizzo,   
				  cod_gruppo_variante,   
				  cod_modello_tenda,   
				  lim_inf_dim_1,   
				  lim_sup_dim_1,   
				  lim_inf_dim_2,   
				  lim_sup_dim_2,   
				  flag_asta,   
				  altezza_comando ,
				  lim_inf_dim_3,   
				  lim_sup_dim_3,   
				  lim_inf_dim_4,   
				  lim_sup_dim_4,   
				  quan_commerciale,
				  quan_tecnica
				  )  
			values(:s_cs_xx.cod_azienda,   
					:ls_codice_nuovo,   
					:ll_max,   
					:ld_quantita_utilizzo,   
					:ls_gruppo_variante,   
					:ls_modello_prodotto_finito,   
					:ld_limite_inferiore_dim_1,   
					:ld_limite_superiore_dim_1,   
					:ld_limite_inferiore_dim_2,   
					:ld_limite_superiore_dim_2,   
					:ls_flag_asta,   
					:ld_altezza_comando,
					:ld_limite_inferiore_dim_3,   
					:ld_limite_superiore_dim_3,   
					:ld_limite_inferiore_dim_4,   
					:ld_limite_superiore_dim_4,   
					:ld_quantita_commerciale,
					:ld_quantita_tecnica
					); 

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante inserimento codice nuovo " + ls_codice_nuovo + "~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
			
		
	end if 	
next
	

commit;
end event

type st_3 from statictext within w_tab_mp_non_richieste
integer x = 526
integer y = 2132
integer width = 2473
integer height = 228
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Per modificare i dati esistenti è sufficiente indicare nella colonna ~"nuovo codice~" il codice deve essere sostituito."
boolean focusrectangle = false
end type

type st_2 from statictext within w_tab_mp_non_richieste
integer x = 526
integer y = 1992
integer width = 2491
integer height = 128
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Per inserire una nuova materia prima si consiglia di fare copia/incolla di una riga, di cancellare il vecchio codice e di lasciare solo il codice nuovo; modificare poi i dati a piacimento."
boolean focusrectangle = false
end type

type st_1 from statictext within w_tab_mp_non_richieste
integer x = 411
integer y = 1916
integer width = 882
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Metodo di utilizzo del file ottenuto:"
boolean focusrectangle = false
end type

type cb_export_txt from commandbutton within w_tab_mp_non_richieste
integer x = 27
integer y = 1912
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "TXT Esporta"
end type

event clicked;string ls_path, ls_filename
long ll_rows, li_rc
datastore lds_export

lds_export = CREATE datastore
lds_export.dataobject = 'd_tab_mp_non_richieste_export'
lds_export.settransobject(sqlca)

ll_rows = lds_export.retrieve(s_cs_xx.cod_azienda)
if ll_rows > 0 then
	ls_path = ""
	ls_filename = "materie_prime_automatiche.txt"
	li_rc = GetFileSaveName ( "Salva Esportazione", ls_path, ls_filename, "TXT", "File di test (*.txt),*.txt" , "C:\temp")
	if li_rc < 1 then 
		g_mb.messagebox("Apice", "Operazione annullata")
		return
	end if
	
	li_rc = lds_export.saveas(ls_path, text!, TRUE)
	if li_rc < 1 then 
		g_mb.messagebox("Apice", "Operazione annullata")
		return
	end if
end if
end event

type cb_reset from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 40
integer width = 361
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_2 from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 132
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica"
end type

event clicked;if dw_tab_mp_non_richieste_lista.getrow() > 0 then
	window_open_parm(w_duplica_mp_automatiche, -1, dw_tab_mp_non_richieste_lista)
end if
end event

type cb_report from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 232
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;if dw_tab_mp_non_richieste_lista.getrow() > 0 then
	window_open_parm(w_report_mp_automatiche, -1, dw_tab_mp_non_richieste_lista)
end if
end event

type cb_ricerca from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 132
integer width = 361
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_tab_mp_non_richieste_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_tab_mp_non_richieste_det from uo_cs_xx_dw within w_tab_mp_non_richieste
integer x = 32
integer y = 916
integer width = 2994
integer height = 960
integer taborder = 70
string dataobject = "d_tab_mp_non_richieste_det"
borderstyle borderstyle = styleraised!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_mp_non_richieste_det,"cod_mp_non_richiesta")
end choose
end event

event pcd_delete;call super::pcd_delete;dw_tab_mp_non_richieste_det.object.b_ricerca_prodotto.visible = false
end event

event pcd_modify;call super::pcd_modify;dw_tab_mp_non_richieste_det.object.b_ricerca_prodotto.visible = true
end event

event pcd_new;call super::pcd_new;dw_tab_mp_non_richieste_det.object.b_ricerca_prodotto.visible = true
end event

event pcd_view;call super::pcd_view;dw_tab_mp_non_richieste_det.object.b_ricerca_prodotto.visible = false
end event

type dw_ricerca from u_dw_search within w_tab_mp_non_richieste
integer x = 183
integer y = 40
integer width = 2391
integer height = 824
integer taborder = 50
string dataobject = "d_ext_mp_aut_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_modello"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_modello")
	case "b_ricerca_mp"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_mp_non_richiesta")
end choose
end event

type dw_tab_mp_non_richieste_lista from uo_cs_xx_dw within w_tab_mp_non_richieste
integer x = 183
integer y = 40
integer width = 2409
integer height = 832
integer taborder = 60
string dataobject = "d_tab_mp_non_richieste_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_mp_non_richiesta
LONG  l_Idx, ll_prog_mp_non_richiesta

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
	if isnull(getitemnumber(l_Idx, "prog_mp_non_richiesta")) then
		ls_cod_mp_non_richiesta = this.getitemstring(l_Idx, "cod_mp_non_richiesta")
		ll_prog_mp_non_richiesta = 0
		
		select max(prog_mp_non_richiesta)
		 into :ll_prog_mp_non_richiesta  
		 from tab_mp_non_richieste
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_mp_non_richiesta = :ls_cod_mp_non_richiesta;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Tabella Materie Prime Non Richieste", "Errore durante il Calcolo del Progressivo Materie Prime Non Richieste")
			pcca.error = c_fatal
			return -1
		end if
		if ll_prog_mp_non_richiesta = 0 or isnull(ll_prog_mp_non_richiesta) then
			ll_prog_mp_non_richiesta = 1
		else
			ll_prog_mp_non_richiesta = ll_prog_mp_non_richiesta + 1
		end if
		this.setitem(l_Idx, "prog_mp_non_richiesta", ll_prog_mp_non_richiesta)
	end if		
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
ib_new = false
end event

event pcd_new;call super::pcd_new;ib_new = true
cb_mod_vern.enabled = false
cb_2.enabled = false

end event

event pcd_modify;call super::pcd_modify;ib_new = false
cb_mod_vern.enabled = false
cb_2.enabled = false

end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
ib_new = false
cb_mod_vern.enabled = true
cb_2.enabled = true

end event

event updatestart;call super::updatestart;string ls_cod_mp_non_richiesta
long ll_i, ll_prog_mp_non_richiesta

for ll_i = 1 to deletedcount()
	ls_cod_mp_non_richiesta = getitemstring(ll_i, "cod_mp_non_richiesta", delete!, true)
	ll_prog_mp_non_richiesta =getitemnumber(ll_i, "prog_mp_non_richiesta", delete!, true)
	
	delete tab_mp_automatiche_mod_vern
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_mp_non_richiesta = :ls_cod_mp_non_richiesta  and
			 prog_mp_non_richiesta = :ll_prog_mp_non_richiesta;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore durante la cancellazione verniciature collegate.~r~n"+sqlca.sqlerrtext)
		return 1
	end if
next
end event

type dw_folder_search from u_folder within w_tab_mp_non_richieste
integer x = 23
integer y = 20
integer width = 2999
integer height = 876
integer taborder = 30
end type

type cb_mod_vern from commandbutton within w_tab_mp_non_richieste
integer x = 2606
integer y = 40
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mod.Vern."
end type

event clicked;window_open_parm(w_mp_automatiche_mod_vern, -1, dw_tab_mp_non_richieste_lista)
end event

