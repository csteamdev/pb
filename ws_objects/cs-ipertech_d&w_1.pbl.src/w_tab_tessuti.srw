﻿$PBExportHeader$w_tab_tessuti.srw
$PBExportComments$Finestra Tabella Tessuti
forward
global type w_tab_tessuti from w_cs_xx_principale
end type
type cb_listino_teli from commandbutton within w_tab_tessuti
end type
type cb_parametri from commandbutton within w_tab_tessuti
end type
type cb_2 from commandbutton within w_tab_tessuti
end type
type cb_incolla from commandbutton within w_tab_tessuti
end type
type cb_annulla from commandbutton within w_tab_tessuti
end type
type cb_ricerca from commandbutton within w_tab_tessuti
end type
type cb_duplica_tessuti from commandbutton within w_tab_tessuti
end type
type cb_3 from commandbutton within w_tab_tessuti
end type
type dw_tab_tessuti_det from uo_cs_xx_dw within w_tab_tessuti
end type
type dw_tessuti_locale from uo_cs_xx_dw within w_tab_tessuti
end type
type dw_folder from u_folder within w_tab_tessuti
end type
type dw_tessuti_ricerca from u_dw_search within w_tab_tessuti
end type
type dw_folder_search from u_folder within w_tab_tessuti
end type
type dw_tab_tessuti_lista from uo_cs_xx_dw within w_tab_tessuti
end type
type str_record from structure within w_tab_tessuti
end type
end forward

type str_record from structure
	string		cod_tessuto
	string		cod_non_a_magazzino
	string		cod_veloce
	string		cod_gruppo_variante
	string		cod_linea_prodotto
	double		quan_disponibile
	double		quan_impegnata
	string		flag_fuori_campionario
	string		flag_addizionale
	string		flag_reparto_taglio
	double		perc_maggiorazione
	double		alt_tessuto
	string		flag_default
	string		flag_blocco
	datetime		data_blocco
	string		cod_tessuto_cliente
	string		cod_colore_tessuto_cliente
	string		cod_tessuto_mantovana
	string		cod_colore_mantovana
	string		colore_passamaneria
	string		colore_cordolo
end type

global type w_tab_tessuti from w_cs_xx_principale
integer width = 3141
integer height = 2492
string title = "Tessuti"
cb_listino_teli cb_listino_teli
cb_parametri cb_parametri
cb_2 cb_2
cb_incolla cb_incolla
cb_annulla cb_annulla
cb_ricerca cb_ricerca
cb_duplica_tessuti cb_duplica_tessuti
cb_3 cb_3
dw_tab_tessuti_det dw_tab_tessuti_det
dw_tessuti_locale dw_tessuti_locale
dw_folder dw_folder
dw_tessuti_ricerca dw_tessuti_ricerca
dw_folder_search dw_folder_search
dw_tab_tessuti_lista dw_tab_tessuti_lista
end type
global w_tab_tessuti w_tab_tessuti

type variables
str_record str_record
end variables

on w_tab_tessuti.create
int iCurrent
call super::create
this.cb_listino_teli=create cb_listino_teli
this.cb_parametri=create cb_parametri
this.cb_2=create cb_2
this.cb_incolla=create cb_incolla
this.cb_annulla=create cb_annulla
this.cb_ricerca=create cb_ricerca
this.cb_duplica_tessuti=create cb_duplica_tessuti
this.cb_3=create cb_3
this.dw_tab_tessuti_det=create dw_tab_tessuti_det
this.dw_tessuti_locale=create dw_tessuti_locale
this.dw_folder=create dw_folder
this.dw_tessuti_ricerca=create dw_tessuti_ricerca
this.dw_folder_search=create dw_folder_search
this.dw_tab_tessuti_lista=create dw_tab_tessuti_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_listino_teli
this.Control[iCurrent+2]=this.cb_parametri
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_incolla
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_ricerca
this.Control[iCurrent+7]=this.cb_duplica_tessuti
this.Control[iCurrent+8]=this.cb_3
this.Control[iCurrent+9]=this.dw_tab_tessuti_det
this.Control[iCurrent+10]=this.dw_tessuti_locale
this.Control[iCurrent+11]=this.dw_folder
this.Control[iCurrent+12]=this.dw_tessuti_ricerca
this.Control[iCurrent+13]=this.dw_folder_search
this.Control[iCurrent+14]=this.dw_tab_tessuti_lista
end on

on w_tab_tessuti.destroy
call super::destroy
destroy(this.cb_listino_teli)
destroy(this.cb_parametri)
destroy(this.cb_2)
destroy(this.cb_incolla)
destroy(this.cb_annulla)
destroy(this.cb_ricerca)
destroy(this.cb_duplica_tessuti)
destroy(this.cb_3)
destroy(this.dw_tab_tessuti_det)
destroy(this.dw_tessuti_locale)
destroy(this.dw_folder)
destroy(this.dw_tessuti_ricerca)
destroy(this.dw_folder_search)
destroy(this.dw_tab_tessuti_lista)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],lw_oggetti1[]

dw_tab_tessuti_lista.set_dw_key("cod_azienda")
dw_tab_tessuti_lista.set_dw_key("cod_tessuto")
dw_tab_tessuti_lista.set_dw_key("cod_non_a_magazzino")
dw_tab_tessuti_lista.set_dw_options(sqlca,pcca.null_object,c_default + c_noretrieveonopen,c_default)
dw_tab_tessuti_det.set_dw_options(sqlca,dw_tab_tessuti_lista,c_sharedata+c_scrollparent,c_default)
dw_tessuti_locale.set_dw_options(sqlca,dw_tab_tessuti_lista,c_scrollparent,c_default)

iuo_dw_main = dw_tab_tessuti_lista

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, dw_folder_search.c_foldertableft)
lw_oggetti[1] = dw_tab_tessuti_lista
dw_folder_search.fu_assigntab(2, "L.", lw_oggetti[])
lw_oggetti[1] = dw_tessuti_ricerca
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
dw_folder_search.fu_assigntab(1, "R.", lw_oggetti[])
dw_folder_search.fu_foldercreate(2, 2)
dw_folder_search.fu_selecttab(1)

l_criteriacolumn[1] = "cod_tessuto"
l_criteriacolumn[2] = "cod_veloce"
l_criteriacolumn[3] = "cod_colore"
l_criteriacolumn[4] = "cod_gruppo_variante"
l_criteriacolumn[5] = "altezza_tessuto"
l_criteriacolumn[6] = "flag_blocco"
l_criteriacolumn[7] = "flag_addizionale"
l_criteriacolumn[8] = "flag_fuori_campionario"
l_criteriacolumn[9] = "cod_tipo_campionario"

l_searchtable[1] = "tab_tessuti"
l_searchtable[2] = "tab_tessuti"
l_searchtable[3] = "tab_tessuti"
l_searchtable[4] = "tab_tessuti"
l_searchtable[5] = "tab_tessuti"
l_searchtable[6] = "tab_tessuti"
l_searchtable[7] = "tab_tessuti"
l_searchtable[8] = "tab_tessuti"
l_searchtable[9] = "tab_tessuti"

l_searchcolumn[1] = "cod_tessuto"
l_searchcolumn[2] = "cod_veloce"
l_searchcolumn[3] = "cod_non_a_magazzino"
l_searchcolumn[4] = "cod_gruppo_variante"
l_searchcolumn[5] = "alt_tessuto"
l_searchcolumn[6] = "flag_blocco"
l_searchcolumn[7] = "flag_addizionale"
l_searchcolumn[8] = "flag_fuori_campionario"
l_searchcolumn[9] = "cod_tipo_campionario"

dw_tessuti_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tab_tessuti_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)
							

lw_oggetti1[1] = dw_tessuti_locale
dw_folder.fu_assigntab(2, "Dati Locali", lw_oggetti1[])
lw_oggetti1[1] = dw_tab_tessuti_det
dw_folder.fu_assigntab(1, "Dati Comuni", lw_oggetti1[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)



end event

event pc_setddlb;call super::pc_setddlb;dw_tessuti_ricerca.fu_loadcode("cod_gruppo_variante", &
						"gruppi_varianti", &
						"cod_gruppo_variante", &
						"des_gruppo_variante", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)" )

dw_tessuti_ricerca.fu_loadcode("cod_tipo_campionario", &
						"tab_tipi_campionari", &
						"cod_tipo_campionario", &
						"des_tipo_campionario", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)" )


f_PO_LoadDDDW_DW(dw_tab_tessuti_det, &
                 "cod_tessuto", &
					   sqlca,&
                 "anag_prodotti", &
					  "cod_prodotto", &
					  "des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_tessuti')")

f_PO_LoadDDDW_DW(dw_tab_tessuti_det, &
 					  "cod_gruppo_variante", &
						sqlca,&
                 "gruppi_varianti", &
					  "cod_gruppo_variante",&
					  "des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tab_tessuti_det,&
 					  "cod_tessuto_cliente",&
						sqlca,&
                 "anag_prodotti",&
					  "cod_prodotto",&
					  "des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_tessuti')")

f_PO_LoadDDDW_DW(dw_tab_tessuti_det, &
                 "cod_tessuto_mantovana",&
					  sqlca,&
                 "anag_prodotti", &
					  "cod_prodotto",&
					  "des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_tessuti')")

f_PO_LoadDDDW_DW(dw_tab_tessuti_det,&
 					  "cod_tipo_campionario",&
					   sqlca,&
                 "tab_tipi_campionari", &
					  "cod_tipo_campionario",&
					  "des_tipo_campionario",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type cb_listino_teli from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 600
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Listino"
end type

event clicked;string ls_cod_tipo_campionario

ls_cod_tipo_campionario = dw_tab_tessuti_lista.getitemstring(dw_tab_tessuti_lista.getrow(), "cod_tipo_campionario")

if g_str.isnotempty(ls_cod_tipo_campionario) then

	window_open_parm(w_tipi_campionari_modelli_listino, -1, dw_tab_tessuti_lista)

else
	
	g_mb.warning("Attenzione! E' necessario indicare un campionario")
end if
end event

type cb_parametri from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 512
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Parametri"
end type

event clicked;string ls_cod_tessuto
ls_cod_tessuto = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(), "cod_tessuto")

if g_str.isnotempty(ls_cod_tessuto) then

	window_open_parm(w_tab_tessuti_parametri, -1, dw_tab_tessuti_lista)

end if
end event

type cb_2 from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 320
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Copia"
end type

event clicked;if dw_tab_tessuti_det.getrow() > 0 then
	str_record.cod_tessuto = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_tessuto")
	str_record.cod_non_a_magazzino = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_non_a_magazzino")
	str_record.cod_veloce = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_veloce")
	str_record.cod_gruppo_variante = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_gruppo_variante")
	str_record.cod_linea_prodotto = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_linea_prodotto")
	str_record.quan_disponibile = dw_tab_tessuti_det.getitemnumber(dw_tab_tessuti_det.getrow(),"quan_disponibile")
	str_record.quan_impegnata = dw_tab_tessuti_det.getitemnumber(dw_tab_tessuti_det.getrow(),"quan_impegnata")
	str_record.flag_fuori_campionario = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"flag_fuori_campionario")
	str_record.flag_addizionale = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"flag_addizionale")
	str_record.flag_reparto_taglio = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"flag_reparto_taglio")
	str_record.perc_maggiorazione = dw_tab_tessuti_det.getitemnumber(dw_tab_tessuti_det.getrow(),"perc_maggiorazione")
	str_record.alt_tessuto = dw_tab_tessuti_det.getitemnumber(dw_tab_tessuti_det.getrow(),"alt_tessuto")
	str_record.flag_default = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"flag_default")
	str_record.flag_blocco = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"flag_blocco")
	str_record.data_blocco = dw_tab_tessuti_det.getitemdatetime(dw_tab_tessuti_det.getrow(),"data_blocco")
	str_record.cod_tessuto_cliente = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_tessuto_cliente")
	str_record.cod_colore_tessuto_cliente = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_colore_tessuto_cliente")
	str_record.cod_tessuto_mantovana = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_tessuto_mantovana")
	str_record.cod_colore_mantovana = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"cod_colore_mantovana")
	str_record.colore_passamaneria = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"colore_passamaneria")
	str_record.colore_cordolo = dw_tab_tessuti_det.getitemstring(dw_tab_tessuti_det.getrow(),"colore_cordolo")
end if
end event

type cb_incolla from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 220
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Incolla"
end type

event clicked;if dw_tab_tessuti_det.getrow() > 0 then
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_tessuto",str_record.cod_tessuto)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_non_a_magazzino",str_record.cod_non_a_magazzino)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_veloce",str_record.cod_veloce)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_gruppo_variante",str_record.cod_gruppo_variante)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_linea_prodotto",str_record.cod_linea_prodotto)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"quan_disponibile",str_record.quan_disponibile)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"quan_impegnata",str_record.quan_impegnata)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"flag_fuori_campionario",str_record.flag_fuori_campionario)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"flag_addizionale",str_record.flag_addizionale)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"flag_reparto_taglio",str_record.flag_reparto_taglio)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"perc_maggiorazione",str_record.perc_maggiorazione)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"alt_tessuto",str_record.alt_tessuto)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"flag_default",str_record.flag_default)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"flag_blocco",str_record.flag_blocco)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"data_blocco",str_record.data_blocco)
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_tessuto_cliente",str_record.cod_tessuto_cliente )
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_colore_tessuto_cliente",str_record.cod_colore_tessuto_cliente )
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_tessuto_mantovana",str_record.cod_tessuto_mantovana )
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"cod_colore_mantovana",str_record.cod_colore_mantovana ) 
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"colore_passamaneria",str_record.colore_passamaneria )
	dw_tab_tessuti_det.setitem(dw_tab_tessuti_det.getrow(),"colore_cordolo",str_record.colore_cordolo )
end if
end event

type cb_annulla from commandbutton within w_tab_tessuti
integer x = 1915
integer y = 632
integer width = 361
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_tessuti_ricerca.fu_Reset()
end event

type cb_ricerca from commandbutton within w_tab_tessuti
integer x = 2304
integer y = 632
integer width = 361
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
dw_tessuti_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(2)
dw_tab_tessuti_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_duplica_tessuti from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 20
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Duplica"
end type

event clicked;window_open(w_crea_tessuti, 0)
end event

type cb_3 from commandbutton within w_tab_tessuti
integer x = 2720
integer y = 120
integer width = 366
integer height = 80
integer taborder = 81
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;window_open(w_report_tessuti, -1)
end event

type dw_tab_tessuti_det from uo_cs_xx_dw within w_tab_tessuti
integer x = 46
integer y = 876
integer width = 2642
integer height = 1468
integer taborder = 70
string dataobject = "d_tab_tessuti_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_linea_prodotto, ls_cod_linea
integer li_i, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
			if ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i = 1 to len(ls_cod_linea_prodotto)
					ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_tessuti
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_linea_prodotto like :ls_cod_linea or 
							cod_linea_prodotto = '' or
							cod_linea_prodotto is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Tessuti", "Errore nell'Estrazione Tessuto")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Tessuti", "Valore di Default già Inserito per questa Linea di Prodotti")
						return 2
					end if
				next
			else
			  select count(*)
				 into :li_count
				 from tab_tessuti
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Tessuti", "Errore nell'Estrazione Tessuto")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Tessuti", "Valore di Default già Inserito")
					return 2
				end if
			end if
		end if
	case "cod_linea_prodotto"
		this.setitem(row, "flag_default", "N")
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_cli"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_tessuti_det,"cod_tessuto_cliente")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_tessuti_det,"cod_tessuto")
end choose
end event

type dw_tessuti_locale from uo_cs_xx_dw within w_tab_tessuti
integer x = 64
integer y = 888
integer width = 2601
integer height = 1356
integer taborder = 40
string dataobject = "d_tessuti_locale"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error
string ls_cod_tessuto, ls_cod_colore_tessuto

if dw_tab_tessuti_lista.getrow() > 0 then
	ls_cod_tessuto = dw_tab_tessuti_lista.getitemstring(dw_tab_tessuti_lista.getrow(),"cod_tessuto")
	ls_cod_colore_tessuto = dw_tab_tessuti_lista.getitemstring(dw_tab_tessuti_lista.getrow(),"cod_non_a_magazzino")
	
	l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_tessuto,ls_cod_colore_tessuto)
	
	IF l_Error < 0 THEN
		PCCA.Error = c_Fatal
	END IF
end if
end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx
string ls_cod_tessuto, ls_cod_colore_tessuto

ls_cod_tessuto = dw_tab_tessuti_lista.getitemstring(dw_tab_tessuti_lista.getrow(),"cod_tessuto")
ls_cod_colore_tessuto = dw_tab_tessuti_lista.getitemstring(dw_tab_tessuti_lista.getrow(),"cod_non_a_magazzino")
	
FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
   IF IsNull(GetItemstring(l_Idx, "cod_tessuto")) THEN
      SetItem(l_Idx, "cod_tessuto", ls_cod_tessuto)
   END IF
	
   IF IsNull(GetItemstring(l_Idx, "cod_non_a_magazzino")) THEN
      SetItem(l_Idx, "cod_non_a_magazzino", ls_cod_colore_tessuto)
   END IF
	
NEXT

end event

event getfocus;call super::getfocus;
iuo_dw_main = dw_tessuti_locale
end event

event losefocus;call super::losefocus;iuo_dw_main = dw_tab_tessuti_lista
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tessuti_locale,"cod_fornitore")
end choose
end event

type dw_folder from u_folder within w_tab_tessuti
integer x = 23
integer y = 752
integer width = 2683
integer height = 1604
integer taborder = 30
end type

type dw_tessuti_ricerca from u_dw_search within w_tab_tessuti
integer x = 160
integer y = 40
integer width = 2491
integer height = 640
integer taborder = 20
string dataobject = "d_tessuti_ricerca"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case this.getcolumnname()
		case "cod_cliente"
			f_po_loaddddw_dw(this, &
								  "cod_des_cliente", &
								  sqlca, &
								  "anag_des_clienti", &
								  "cod_des_cliente", &
								  "rag_soc_1", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + data + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tessuti_ricerca,"cod_tessuto")
		
	case "b_ricerca_colore_tessuto"
		guo_ricerca.uof_ricerca_colore_tessuto(dw_tessuti_ricerca,"cod_colore")
		
		
end choose
end event

type dw_folder_search from u_folder within w_tab_tessuti
integer x = 23
integer y = 20
integer width = 2674
integer height = 720
integer taborder = 30
end type

type dw_tab_tessuti_lista from uo_cs_xx_dw within w_tab_tessuti
integer x = 160
integer y = 40
integer width = 2491
integer height = 680
integer taborder = 40
string dataobject = "d_tab_tessuti_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tessuto")) then
	cb_listino_teli.enabled = true
else
	cb_listino_teli.enabled = false
end if
dw_tab_tessuti_det.object.b_ricerca_prodotto.enabled = false
dw_tab_tessuti_det.object.b_ricerca_prodotto_cli.enabled = false
end event

event pcd_new;call super::pcd_new;cb_listino_teli.enabled = false
cb_parametri.enabled = false
dw_tab_tessuti_det.object.b_ricerca_prodotto.enabled = true
dw_tab_tessuti_det.object.b_ricerca_prodotto_cli.enabled = true

end event

event pcd_modify;call super::pcd_modify;cb_listino_teli.enabled = false
cb_parametri.enabled = false
dw_tab_tessuti_det.object.b_ricerca_prodotto.enabled = true
dw_tab_tessuti_det.object.b_ricerca_prodotto_cli.enabled = true

// stefanop: 26/09/2014
// Ogni volta che salvo devo impostare che il record è cambiato
setitem(getrow(), "flag_modificato", "S")


end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tessuto")) then
	cb_listino_teli.enabled = true
	cb_parametri.enabled = true
else
	cb_listino_teli.enabled = false
	cb_parametri.enabled = false
end if
dw_tab_tessuti_det.object.b_ricerca_prodotto.enabled = false
dw_tab_tessuti_det.object.b_ricerca_prodotto_cli.enabled =false

end event

