﻿$PBExportHeader$w_tab_tessuti_parametri.srw
forward
global type w_tab_tessuti_parametri from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_tessuti_parametri
end type
end forward

global type w_tab_tessuti_parametri from w_cs_xx_principale
integer width = 2478
integer height = 1960
string title = "Parametri"
dw_lista dw_lista
end type
global w_tab_tessuti_parametri w_tab_tessuti_parametri

type variables
private:
	string is_cod_tessuto_old, is_cod_tessuto
end variables

on w_tab_tessuti_parametri.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_tessuti_parametri.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_tessuto")
dw_lista.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)
end event

event resize;

dw_lista.resize(newwidth - 40, newheight - 40)
end event

type dw_lista from uo_cs_xx_dw within w_tab_tessuti_parametri
integer x = 18
integer y = 20
integer width = 2377
integer height = 1820
integer taborder = 10
string dataobject = "d_tab_tessuti_parametri"
boolean hscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;is_cod_tessuto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tessuto")

if g_str.isnotempty(is_cod_tessuto) then
	
	// Eseguo la retrieve solo se il codice tessuto cambia tra una riga e l'altra
	if is_cod_tessuto <> is_cod_tessuto_old then
		
		parent.title = "Parametri tessuto " + is_cod_tessuto
	
		if retrieve(s_cs_xx.cod_azienda, is_cod_tessuto) >= 0 then
			is_cod_tessuto_old = is_cod_tessuto
		else
			pcca.error = c_fatal			
		end if
		
	end if
	
else
	reset()
end if
end event

event pcd_setkey;call super::pcd_setkey;int ll_i

for ll_i = 1 to rowcount()
	
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(getitemstring(ll_i, "cod_tessuto")) then
      setitem(ll_i, "cod_tessuto", is_cod_tessuto_old)
   end if
  
next

end event

event itemchanged;call super::itemchanged;long value

choose case dwo.name
		
	case "wheel_cut_speed"
		
		if isnull(data) or data = "" then
			value = 0
		else
			value = long(data)
		end if
		
		setitem(row, "cut_speed", value)
		
		
	case "laser_enabled"
		if "S" = data then
			setitem(row, "mark_type", "0") // Laser
		else
			setitem(row, "mark_type", "2") // Pennarello
		end if
		
end choose
end event

event updateend;call super::updateend;
if rowsinserted + rowsupdated + rowsdeleted > 0 then
	
	update tab_tessuti
	set flag_modificato = 'S'
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tessuto = :is_cod_tessuto;
	
end if
end event

