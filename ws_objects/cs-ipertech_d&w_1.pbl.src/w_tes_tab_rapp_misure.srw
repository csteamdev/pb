﻿$PBExportHeader$w_tes_tab_rapp_misure.srw
$PBExportComments$Finestra Testata Tabella Rapporti Misure X Progetto Tenda
forward
global type w_tes_tab_rapp_misure from w_cs_xx_principale
end type
type dw_tes_tab_rapp_misure_lista from uo_cs_xx_dw within w_tes_tab_rapp_misure
end type
type dw_tes_tab_rapp_misure_det from uo_cs_xx_dw within w_tes_tab_rapp_misure
end type
type cb_dettaglio from commandbutton within w_tes_tab_rapp_misure
end type
end forward

global type w_tes_tab_rapp_misure from w_cs_xx_principale
int Width=1916
int Height=1305
boolean TitleBar=true
string Title="Testata Tabella Rapporti Misure"
dw_tes_tab_rapp_misure_lista dw_tes_tab_rapp_misure_lista
dw_tes_tab_rapp_misure_det dw_tes_tab_rapp_misure_det
cb_dettaglio cb_dettaglio
end type
global w_tes_tab_rapp_misure w_tes_tab_rapp_misure

type variables

end variables

on w_tes_tab_rapp_misure.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tes_tab_rapp_misure_lista=create dw_tes_tab_rapp_misure_lista
this.dw_tes_tab_rapp_misure_det=create dw_tes_tab_rapp_misure_det
this.cb_dettaglio=create cb_dettaglio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tes_tab_rapp_misure_lista
this.Control[iCurrent+2]=dw_tes_tab_rapp_misure_det
this.Control[iCurrent+3]=cb_dettaglio
end on

on w_tes_tab_rapp_misure.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tes_tab_rapp_misure_lista)
destroy(this.dw_tes_tab_rapp_misure_det)
destroy(this.cb_dettaglio)
end on

event pc_setwindow;call super::pc_setwindow;dw_tes_tab_rapp_misure_lista.set_dw_key("cod_azienda")
dw_tes_tab_rapp_misure_lista.set_dw_key("cod_tab_rapp_misure")
dw_tes_tab_rapp_misure_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tes_tab_rapp_misure_det.set_dw_options(sqlca,dw_tes_tab_rapp_misure_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tes_tab_rapp_misure_lista
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tes_tab_rapp_misure_lista.getrow()
if not(li_row > 0 and not isnull(dw_tes_tab_rapp_misure_lista.GetItemstring(li_row,"cod_tab_rapp_misure"))) then
	cb_dettaglio.enabled = true
else
	cb_dettaglio.enabled = false
end if
end event

type dw_tes_tab_rapp_misure_lista from uo_cs_xx_dw within w_tes_tab_rapp_misure
int X=14
int Y=17
int Width=1473
int Height=497
int TabOrder=10
string DataObject="d_tes_tab_rapp_misure_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_dettaglio.enabled = false
end event

event pcd_new;call super::pcd_new;cb_dettaglio.enabled = false
end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tab_rapp_misure")) then
	cb_dettaglio.enabled = true
else
	cb_dettaglio.enabled = false
end if
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tab_rapp_misure")) then
	cb_dettaglio.enabled = true
else
	cb_dettaglio.enabled = false
end if

end event

type dw_tes_tab_rapp_misure_det from uo_cs_xx_dw within w_tes_tab_rapp_misure
int X=1
int Y=521
int Width=1852
int Height=661
int TabOrder=20
boolean BringToTop=true
string DataObject="d_tes_tab_rapp_misure_det"
BorderStyle BorderStyle=StyleRaised!
end type

type cb_dettaglio from commandbutton within w_tes_tab_rapp_misure
int X=1505
int Y=21
int Width=362
int Height=81
int TabOrder=21
string Text="&Dettagli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;window_open_parm(w_det_tab_rapp_misure, -1, dw_tes_tab_rapp_misure_lista)

end event

