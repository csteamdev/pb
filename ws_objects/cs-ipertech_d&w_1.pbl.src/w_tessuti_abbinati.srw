﻿$PBExportHeader$w_tessuti_abbinati.srw
$PBExportComments$Finestra Abbinamento Cordolo e Mantonvana a Tessuto
forward
global type w_tessuti_abbinati from w_cs_xx_principale
end type
type dw_tessuti_abbinati from uo_cs_xx_dw within w_tessuti_abbinati
end type
end forward

global type w_tessuti_abbinati from w_cs_xx_principale
int Width=2478
int Height=529
boolean TitleBar=true
string Title="Cordolo e Mantovana"
dw_tessuti_abbinati dw_tessuti_abbinati
end type
global w_tessuti_abbinati w_tessuti_abbinati

event open;call super::open;dw_tessuti_abbinati.set_dw_key("cod_azienda")
dw_tessuti_abbinati.set_dw_key("cod_tessuto")
dw_tessuti_abbinati.set_dw_key("cod_non_a_magazzino")
dw_tessuti_abbinati.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_tessuti_abbinati

end event

on w_tessuti_abbinati.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tessuti_abbinati=create dw_tessuti_abbinati
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tessuti_abbinati
end on

on w_tessuti_abbinati.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tessuti_abbinati)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tessuti_abbinati,"cod_tessuto_mantovana",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( select cod_tessuto from tab_tessuti " + &
					                                                                    " where cod_azienda =  '" + s_cs_xx.cod_azienda + "' )")

end event

type dw_tessuti_abbinati from uo_cs_xx_dw within w_tessuti_abbinati
int X=23
int Y=21
int Width=2401
int Height=381
string DataObject="d_tessuti_abbinati"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tessuto, ls_cod_non_a_magazzino
long ll_errore

ls_cod_tessuto = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tessuto")
ls_cod_non_a_magazzino = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_non_a_magazzino")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tessuto, ls_cod_non_a_magazzino)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_tessuto, ls_cod_non_a_magazzino
long ll_errore, l_Idx

ls_cod_tessuto = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tessuto")
ls_cod_non_a_magazzino = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_non_a_magazzino")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_tessuto")) THEN
      SetItem(l_Idx, "cod_tessuto", ls_cod_tessuto)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_non_a_magazzino")) THEN
      SetItem(l_Idx, "cod_non_a_magazzino", ls_cod_non_a_magazzino)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;string ls_cod_tessuto

ls_cod_tessuto = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tessuto")
dw_tessuti_abbinati.setitem(dw_tessuti_abbinati.getrow(),"cod_tessuto_mantovana", ls_cod_tessuto)
end event

