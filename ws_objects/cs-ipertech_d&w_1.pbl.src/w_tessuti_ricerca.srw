﻿$PBExportHeader$w_tessuti_ricerca.srw
$PBExportComments$Finestra Modale per  Ricerca Tessuti
forward
global type w_tessuti_ricerca from w_std_principale
end type
type st_5 from statictext within w_tessuti_ricerca
end type
type st_tot from statictext within w_tessuti_ricerca
end type
type cb_ricerca from commandbutton within w_tessuti_ricerca
end type
type cb_annulla from commandbutton within w_tessuti_ricerca
end type
type cb_conferma from commandbutton within w_tessuti_ricerca
end type
type st_4 from statictext within w_tessuti_ricerca
end type
type st_3 from statictext within w_tessuti_ricerca
end type
type st_2 from statictext within w_tessuti_ricerca
end type
type st_1 from statictext within w_tessuti_ricerca
end type
type dw_tessuti_ricerca_parametri from uo_std_dw within w_tessuti_ricerca
end type
type dw_tessuti_ricerca_lista from uo_std_dw within w_tessuti_ricerca
end type
end forward

global type w_tessuti_ricerca from w_std_principale
integer width = 4814
integer height = 2308
string title = "Ricerca Tessuti e Colori Tessuti"
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
st_5 st_5
st_tot st_tot
cb_ricerca cb_ricerca
cb_annulla cb_annulla
cb_conferma cb_conferma
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
dw_tessuti_ricerca_parametri dw_tessuti_ricerca_parametri
dw_tessuti_ricerca_lista dw_tessuti_ricerca_lista
end type
global w_tessuti_ricerca w_tessuti_ricerca

forward prototypes
public function string wf_stringa_ricerca (string fs_stringa_ricerca)
end prototypes

public function string wf_stringa_ricerca (string fs_stringa_ricerca);long ll_i, ll_y

// aggiunto un carattere * alla fine della stringa
if right(trim(fs_stringa_ricerca), 1) <> "*" then
	fs_stringa_ricerca += "*"
end if

// sostituisco tutti i * con il carattere per il like
ll_i = len(fs_stringa_ricerca)
for ll_y = 1 to ll_i
	if mid(fs_stringa_ricerca, ll_y, 1) = "*" then
		fs_stringa_ricerca = replace(fs_stringa_ricerca, ll_y, 1, "%")
	end if
next

return fs_stringa_ricerca
end function

on w_tessuti_ricerca.create
int iCurrent
call super::create
this.st_5=create st_5
this.st_tot=create st_tot
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.dw_tessuti_ricerca_parametri=create dw_tessuti_ricerca_parametri
this.dw_tessuti_ricerca_lista=create dw_tessuti_ricerca_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_5
this.Control[iCurrent+2]=this.st_tot
this.Control[iCurrent+3]=this.cb_ricerca
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_conferma
this.Control[iCurrent+6]=this.st_4
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.dw_tessuti_ricerca_parametri
this.Control[iCurrent+11]=this.dw_tessuti_ricerca_lista
end on

on w_tessuti_ricerca.destroy
call super::destroy
destroy(this.st_5)
destroy(this.st_tot)
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_tessuti_ricerca_parametri)
destroy(this.dw_tessuti_ricerca_lista)
end on

event open;call super::open;dw_tessuti_ricerca_parametri.insertrow(0)
dw_tessuti_ricerca_lista.settransobject(sqlca)

dw_tessuti_ricerca_parametri.setfocus()

//sort di default per cod_tessuto e cod_non_a_magazzino
dw_tessuti_ricerca_lista.setsort("tab_tessuti_cod_tessuto asc, tab_tessuti_cod_non_a_magazzino asc")
dw_tessuti_ricerca_lista.sort()
end event

type st_5 from statictext within w_tessuti_ricerca
integer x = 3223
integer y = 188
integer width = 1568
integer height = 56
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "clic su Seleziona o doppio clic sulla riga per selezionare"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_tot from statictext within w_tessuti_ricerca
integer x = 3278
integer y = 28
integer width = 1504
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "righe trovate"
boolean focusrectangle = false
end type

type cb_ricerca from commandbutton within w_tessuti_ricerca
integer x = 3278
integer y = 100
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;dw_tessuti_ricerca_parametri.accepttext()
dw_tessuti_ricerca_parametri.postevent("ue_ricerca")
end event

type cb_annulla from commandbutton within w_tessuti_ricerca
integer x = 4398
integer y = 100
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;


setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_s_2)
	
close(parent)

end event

type cb_conferma from commandbutton within w_tessuti_ricerca
integer x = 3671
integer y = 100
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;
long			ll_row

ll_row = dw_tessuti_ricerca_lista.getrow()

if ll_row>0 then
	s_cs_xx.parametri.parametro_s_1 = dw_tessuti_ricerca_lista.getitemstring(ll_row,"tab_tessuti_cod_tessuto")
	s_cs_xx.parametri.parametro_s_2  = dw_tessuti_ricerca_lista.getitemstring(ll_row,"tab_tessuti_cod_non_a_magazzino")
	
	close(parent)
else
	g_mb.warning("Selezionare una riga!")
	return
end if
end event

type st_4 from statictext within w_tessuti_ricerca
integer x = 2423
integer y = 52
integer width = 800
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Descrizione Colore Tessuto"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_3 from statictext within w_tessuti_ricerca
integer x = 1623
integer y = 52
integer width = 800
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Codice Colore Tessuto"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_tessuti_ricerca
integer x = 823
integer y = 52
integer width = 800
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Descrizione Tessuto"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_1 from statictext within w_tessuti_ricerca
integer x = 23
integer y = 52
integer width = 800
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Codice Tessuto"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_tessuti_ricerca_parametri from uo_std_dw within w_tessuti_ricerca
event key pbm_dwnkey
event ue_ricerca ( )
integer x = 23
integer y = 132
integer width = 3200
integer height = 100
integer taborder = 30
string dataobject = "d_tessuti_ricerca_parametri"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event key;if key = keyenter! then
	accepttext()
	postevent("ue_ricerca")
end if
end event

event ue_ricerca();string					ls_cod_tessuto, ls_des_tessuto, ls_cod_colore, ls_des_colore
long					ll_ret


ls_cod_tessuto = getitemstring(getrow(),"cod_tessuto")
ls_des_tessuto = getitemstring(getrow(),"des_tessuto")
ls_cod_colore  = getitemstring(getrow(),"cod_colore")
ls_des_colore  = getitemstring(getrow(),"des_colore")

if isnull(ls_cod_tessuto) then ls_cod_tessuto = ""
if isnull(ls_des_tessuto) then ls_des_tessuto = ""
if isnull(ls_cod_colore) then ls_cod_colore = ""
if isnull(ls_des_colore) then ls_des_colore = ""

ls_cod_tessuto = wf_stringa_ricerca( ls_cod_tessuto )
ls_des_tessuto = wf_stringa_ricerca( ls_des_tessuto )
ls_cod_colore  = wf_stringa_ricerca( ls_cod_colore )
ls_des_colore  = wf_stringa_ricerca( ls_des_colore )

ll_ret = dw_tessuti_ricerca_lista.retrieve(s_cs_xx.cod_azienda, ls_cod_tessuto, ls_des_tessuto, ls_cod_colore, ls_des_colore)

st_tot.text = string(ll_ret) + " righe trovate"
end event

type dw_tessuti_ricerca_lista from uo_std_dw within w_tessuti_ricerca
integer x = 23
integer y = 272
integer width = 4763
integer height = 1936
integer taborder = 20
string dataobject = "d_tessuti_ricerca_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event rowfocuschanged;call super::rowfocuschanged;

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

event doubleclicked;call super::doubleclicked;string				ls_colonna
integer			li_ret

if right(dwo.name, 2) = "_t" then
	//doppio clic su un campo intestazione
	
	ls_colonna = dwo.name
	
	//ci sono due casi di scostamento tra nome intestazione e nome colonna
	choose case ls_colonna
		case "tab_colori_tessuti_des_colore_tessut_t"
			ls_colonna = "tab_colori_tessuti_des_colore_tessuto"
			
		case "tab_tipi_campionari_des_tipo_campion_t"
			ls_colonna = "tab_tipi_campionari_des_tipo_campionario"
			
		case else
			//prendo tutto trannegli ultimi due caratteri "_t"
			ls_colonna = left(ls_colonna, len(ls_colonna) - 2)
			
	end choose
	
	ls_colonna += " asc"
	li_ret = dw_tessuti_ricerca_lista.setsort(ls_colonna)
	li_ret = dw_tessuti_ricerca_lista.sort()
	
else

	if row>0 then
		//selezione di un dato
		s_cs_xx.parametri.parametro_s_1 = dw_tessuti_ricerca_lista.getitemstring(row, "tab_tessuti_cod_tessuto")
		s_cs_xx.parametri.parametro_s_2  = dw_tessuti_ricerca_lista.getitemstring(row, "tab_tessuti_cod_non_a_magazzino")
		close(parent)
	
	end if

end if
end event

