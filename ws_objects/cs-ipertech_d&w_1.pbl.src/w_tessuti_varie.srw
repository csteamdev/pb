﻿$PBExportHeader$w_tessuti_varie.srw
$PBExportComments$Finestra Dati Vari su Tessuti (Etichette per Veneziane/Plissè)
forward
global type w_tessuti_varie from w_cs_xx_principale
end type
type dw_tessuti_varie_lista from uo_cs_xx_dw within w_tessuti_varie
end type
type dw_tessuti_varie_det from uo_cs_xx_dw within w_tessuti_varie
end type
end forward

global type w_tessuti_varie from w_cs_xx_principale
int Width=2529
int Height=1837
boolean TitleBar=true
string Title="Comandi"
dw_tessuti_varie_lista dw_tessuti_varie_lista
dw_tessuti_varie_det dw_tessuti_varie_det
end type
global w_tessuti_varie w_tessuti_varie

type variables
boolean ib_new
end variables

on w_tessuti_varie.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tessuti_varie_lista=create dw_tessuti_varie_lista
this.dw_tessuti_varie_det=create dw_tessuti_varie_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tessuti_varie_lista
this.Control[iCurrent+2]=dw_tessuti_varie_det
end on

on w_tessuti_varie.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tessuti_varie_lista)
destroy(this.dw_tessuti_varie_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tessuti_varie_lista.set_dw_key("cod_azienda")
dw_tessuti_varie_lista.set_dw_key("cod_gruppo_tessuto")
dw_tessuti_varie_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tessuti_varie_det.set_dw_options(sqlca,dw_tessuti_varie_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tessuti_varie_lista

end event

type dw_tessuti_varie_lista from uo_cs_xx_dw within w_tessuti_varie
int X=23
int Y=21
int Width=2446
int Height=501
int TabOrder=10
string DataObject="d_tessuti_varie_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_tessuti_varie_det from uo_cs_xx_dw within w_tessuti_varie
int X=23
int Y=541
int Width=2446
int Height=1181
int TabOrder=20
string DataObject="d_tessuti_varie_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;//string ls_cod_cat_mer
//integer li_count
//long ll_cont
//
//choose case i_colname
//	case "cod_comando"
//		if len(i_coltext) > 0 and not isnull(i_coltext) then
//			select cod_cat_mer
//			into   :ls_cod_cat_mer
//			from   anag_prodotti
//			where  cod_azienda = :s_cs_xx.cod_azienda and 
//					 cod_prodotto = :i_coltext;
//			if sqlca.sqlcode <> 0 then
//				messagebox("Comandi","Prodotto inesistente")
//				return 1
//			end if
//			if isnull(ls_cod_cat_mer) then
//				messagebox("Comandi","Manca la categoria merceologica nella anagrafica del prodotto.")
//				return 1
//			end if
//			ll_cont = 0
//			select count(*)
//			into   :ll_cont
//			from   tab_cat_mer_tabelle
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//			       cod_cat_mer = :ls_cod_cat_mer and
//			       nome_tabella = 'tab_comandi';
//			if ll_cont = 0 or isnull(ll_cont) then
//				messagebox("Comandi","Questo codice non è un comando!", information!)
//				return 1
//			end if
//		end if
//
//	case "flag_default"
//		if data = "S" then
//		  select count(*)
//			 into :li_count
//			 from tab_comandi
//			where cod_azienda = :s_cs_xx.cod_azienda and
//					flag_default = 'S';
//
//			if sqlca.sqlcode = -1 then
//				messagebox("Tabella Comandi", "Errore nell'Estrazione Comandi")
//				return 2
//			end if
//			if li_count > 0 then
//				messagebox("Tabella Comandi Lastre", "Valore di Default già Inserito")
//				return 2
//			end if
//		end if
//end choose
end event

