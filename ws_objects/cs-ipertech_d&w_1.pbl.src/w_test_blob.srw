﻿$PBExportHeader$w_test_blob.srw
forward
global type w_test_blob from window
end type
type cb_5 from commandbutton within w_test_blob
end type
type st_2 from statictext within w_test_blob
end type
type st_1 from statictext within w_test_blob
end type
type cb_4 from commandbutton within w_test_blob
end type
type cb_3 from commandbutton within w_test_blob
end type
type dw_2 from datawindow within w_test_blob
end type
type cb_2 from commandbutton within w_test_blob
end type
type dw_1 from datawindow within w_test_blob
end type
type cb_1 from commandbutton within w_test_blob
end type
end forward

global type w_test_blob from window
integer width = 3406
integer height = 2200
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_5 cb_5
st_2 st_2
st_1 st_1
cb_4 cb_4
cb_3 cb_3
dw_2 dw_2
cb_2 cb_2
dw_1 dw_1
cb_1 cb_1
end type
global w_test_blob w_test_blob

on w_test_blob.create
this.cb_5=create cb_5
this.st_2=create st_2
this.st_1=create st_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.dw_2=create dw_2
this.cb_2=create cb_2
this.dw_1=create dw_1
this.cb_1=create cb_1
this.Control[]={this.cb_5,&
this.st_2,&
this.st_1,&
this.cb_4,&
this.cb_3,&
this.dw_2,&
this.cb_2,&
this.dw_1,&
this.cb_1}
end on

on w_test_blob.destroy
destroy(this.cb_5)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.dw_2)
destroy(this.cb_2)
destroy(this.dw_1)
destroy(this.cb_1)
end on

type cb_5 from commandbutton within w_test_blob
integer x = 919
integer y = 1748
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;blob lb_blob


//insert into det_ord_ven_note
//(cod_azienda, anno_registrazione, num_registrazione,prog_riga_ord_ven, cod_nota, des_nota, prog_mimetype)
//values
//('A01',2014,161062,10,'XXX','TEST CS',3);
//
//commit;
//
//GUO_FUNCTIONS.uof_file_to_blob( "C:\Users\enrico.CS\Documents\Risposta_annuncio_vigili.pdf",  ref lb_blob)

selectblob note_esterne
into :lb_blob
from det_ord_ven_note
where anno_registrazione = 2014 and num_registrazione=161062 and prog_riga_ord_ven=10 and cod_nota ='AX1';

messagebox("LEN", lenA(lb_blob))


updateblob det_ord_ven_note
set note_esterne= :lb_blob
where anno_registrazione = 2014 and num_registrazione=161062 and prog_riga_ord_ven=10 and cod_nota ='XXX';

commit;

selectblob note_esterne
into :lb_blob
from det_ord_ven_note
where anno_registrazione = 2014 and num_registrazione=161062 and prog_riga_ord_ven=10 and cod_nota ='XXX';

messagebox("LEN", lenA(lb_blob))


commit;

end event

type st_2 from statictext within w_test_blob
integer x = 2921
integer y = 1152
integer width = 402
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_1 from statictext within w_test_blob
integer x = 2921
integer y = 336
integer width = 402
integer height = 84
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_test_blob
integer x = 2907
integer y = 1004
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "BLOB"
end type

event clicked;string	ls_cod_nota
long 		ll_i, ll_anno_registrazione, ll_num_registrazione, ll_len
blob 		lb_blob


for ll_i = 1 to dw_2.rowcount()
	
	ll_anno_registrazione = dw_2.getitemnumber(ll_i,"anno_registrazione")
	ll_num_registrazione = dw_2.getitemnumber(ll_i,"num_registrazione")
	ls_cod_nota = dw_2.getitemstring(ll_i,"cod_nota")
	
	setnull(lb_blob)

	selectblob 	note_esterne
	into 			:lb_blob
	from			tes_ord_ven_note_arc
	where 		cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					cod_nota = :ls_cod_nota;

	if sqlca.sqlcode <> 0 then
		messagebox("SELECT", string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
		rollback;
		exit
	end if
	
	ll_len = LenA(lb_blob)
	
	if ll_len <= 0 or isnull(ll_len) then continue
					
	updateblob	tes_ord_ven_note
	set				note_esterne = :lb_blob
	where 			cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode <> 0 then
		messagebox("UPDATE", string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
		rollback;
		exit
	end if

	commit;
	
	dw_2.ScrollToRow (ll_i)
	dw_2.setitem(ll_i, "flag_eseguito", "S")
	st_2.text = string(ll_i) + "/" +  string(dw_2.rowcount())
	
	Yield()
	
next


dw_2.saveas()
end event

type cb_3 from commandbutton within w_test_blob
integer x = 2907
integer y = 848
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "retrieve"
end type

event clicked;long ll_row

dw_2.settransobject(sqlca)
ll_row = dw_2.retrieve(s_cs_xx.cod_azienda)

messagebox("","Caricato " + string(ll_row) + " righe")
end event

type dw_2 from datawindow within w_test_blob
integer x = 14
integer y = 840
integer width = 2853
integer height = 788
integer taborder = 10
boolean titlebar = true
string title = "TESTATA ORDINI"
string dataobject = "d_test_blob_tes_ord"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_2 from commandbutton within w_test_blob
integer x = 2917
integer y = 24
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "retrieve"
end type

event clicked;long ll_row

dw_1.settransobject(sqlca)
ll_row = dw_1.retrieve(s_cs_xx.cod_azienda)

messagebox("","Caricato " + string(ll_row) + " righe")
end event

type dw_1 from datawindow within w_test_blob
integer x = 14
integer y = 12
integer width = 2853
integer height = 788
integer taborder = 10
boolean titlebar = true
string title = "DETTAGLIO ORDINI"
string dataobject = "d_test_blob_det_ord"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_test_blob
integer x = 2917
integer y = 180
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "BLOB"
end type

event clicked;string	ls_cod_nota
long 		ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_len
blob 		lb_blob


for ll_i = 1 to dw_1.rowcount()
	
	ll_anno_registrazione = dw_1.getitemnumber(ll_i,"anno_registrazione")
	ll_num_registrazione = dw_1.getitemnumber(ll_i,"num_registrazione")
	ll_prog_riga_ord_ven = dw_1.getitemnumber(ll_i,"prog_riga_ord_ven")
	ls_cod_nota = dw_1.getitemstring(ll_i,"cod_nota")
	
	setnull(lb_blob)

	selectblob 	note_esterne
	into 			:lb_blob
	from			det_ord_ven_note_arc
	where 		cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven and
					cod_nota = :ls_cod_nota;

	if sqlca.sqlcode <> 0 then
		messagebox("SELECT", string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
		rollback;
		exit
	end if
	
	ll_len = LenA(lb_blob)
	
	if ll_len <= 0 or isnull(ll_len) then continue
					
	updateblob	det_ord_ven_note
	set				note_esterne = :lb_blob
	where 			cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_prog_riga_ord_ven and
						cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode <> 0 then
		messagebox("UPDATE", string(sqlca.sqlcode) + " " + sqlca.sqlerrtext)
		rollback;
		exit
	end if

	commit;
	
	dw_1.ScrollToRow (ll_i)
	dw_1.setitem(ll_i, "flag_eseguito", "S")
	st_1.text = string(ll_i) + "/" +  string(dw_1.rowcount())
	Yield()
	
next


dw_1.saveas()
end event

