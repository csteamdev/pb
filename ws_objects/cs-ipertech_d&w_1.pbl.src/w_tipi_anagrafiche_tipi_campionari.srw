﻿$PBExportHeader$w_tipi_anagrafiche_tipi_campionari.srw
$PBExportComments$Associazione Tipi Anagrafiche a tipi campionari
forward
global type w_tipi_anagrafiche_tipi_campionari from w_cs_xx_principale
end type
type dw_tipi_anagrafiche_tipi_campionari from uo_cs_xx_dw within w_tipi_anagrafiche_tipi_campionari
end type
end forward

global type w_tipi_anagrafiche_tipi_campionari from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3374
string title = "Associaione Tipi Anagrafiche - Tipi Campionari"
dw_tipi_anagrafiche_tipi_campionari dw_tipi_anagrafiche_tipi_campionari
end type
global w_tipi_anagrafiche_tipi_campionari w_tipi_anagrafiche_tipi_campionari

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_anagrafiche_tipi_campionari, &
                 "cod_tipo_anagrafica", &
                 sqlca, &
                 "tab_tipi_anagrafiche", &
                 "cod_tipo_anagrafica", &
                 "des_tipo_anagrafica", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_anagrafica = 'C' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_tipi_anagrafiche_tipi_campionari,&
 					  "cod_tipo_campionario",&
					   sqlca,&
                 "tab_tipi_campionari", &
					  "cod_tipo_campionario",&
					  "des_tipo_campionario",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_setwindow;call super::pc_setwindow;dw_tipi_anagrafiche_tipi_campionari.set_dw_key("cod_azienda")
dw_tipi_anagrafiche_tipi_campionari.set_dw_key("cod_tipo_anagrafica")
dw_tipi_anagrafiche_tipi_campionari.set_dw_key("cod_tipo_campionario")
dw_tipi_anagrafiche_tipi_campionari.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tipi_anagrafiche_tipi_campionari


dw_tipi_anagrafiche_tipi_campionari.ib_proteggi_chiavi=false
end event

on w_tipi_anagrafiche_tipi_campionari.create
int iCurrent
call super::create
this.dw_tipi_anagrafiche_tipi_campionari=create dw_tipi_anagrafiche_tipi_campionari
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_anagrafiche_tipi_campionari
end on

on w_tipi_anagrafiche_tipi_campionari.destroy
call super::destroy
destroy(this.dw_tipi_anagrafiche_tipi_campionari)
end on

type dw_tipi_anagrafiche_tipi_campionari from uo_cs_xx_dw within w_tipi_anagrafiche_tipi_campionari
integer x = 18
integer y = 16
integer width = 3291
integer height = 1292
integer taborder = 10
string dataobject = "d_tipi_anagrafiche_tipi_campionari"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

