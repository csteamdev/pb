﻿$PBExportHeader$w_tipi_campionari.srw
$PBExportComments$Finestra Tabella Descrizioni X Variabili X Formule X Distinta Base
forward
global type w_tipi_campionari from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_tipi_campionari
end type
type dw_tipi_campionari_lista from uo_cs_xx_dw within w_tipi_campionari
end type
type dw_tipi_campionari_det from uo_cs_xx_dw within w_tipi_campionari
end type
end forward

global type w_tipi_campionari from w_cs_xx_principale
integer width = 1925
integer height = 1376
string title = "Campionari"
cb_1 cb_1
dw_tipi_campionari_lista dw_tipi_campionari_lista
dw_tipi_campionari_det dw_tipi_campionari_det
end type
global w_tipi_campionari w_tipi_campionari

on w_tipi_campionari.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_tipi_campionari_lista=create dw_tipi_campionari_lista
this.dw_tipi_campionari_det=create dw_tipi_campionari_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_tipi_campionari_lista
this.Control[iCurrent+3]=this.dw_tipi_campionari_det
end on

on w_tipi_campionari.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_tipi_campionari_lista)
destroy(this.dw_tipi_campionari_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_campionari_lista.set_dw_key("cod_azienda")
dw_tipi_campionari_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tipi_campionari_det.set_dw_options(sqlca,dw_tipi_campionari_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tipi_campionari_lista
end event

type cb_1 from commandbutton within w_tipi_campionari
integer x = 1417
integer y = 1180
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Listino"
end type

event clicked;string ls_cod_tipo_campionario
ls_cod_tipo_campionario = dw_tipi_campionari_lista.getitemstring(dw_tipi_campionari_lista.getrow(), "cod_tipo_campionario")

if g_str.isnotempty(ls_cod_tipo_campionario) then

	window_open_parm(w_tipi_campionari_modelli_listino, -1, dw_tipi_campionari_lista)

end if
end event

type dw_tipi_campionari_lista from uo_cs_xx_dw within w_tipi_campionari
integer x = 23
integer y = 20
integer width = 1851
integer height = 500
integer taborder = 50
string dataobject = "d_tipi_campionari_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_tipi_campionari_det from uo_cs_xx_dw within w_tipi_campionari
integer x = 23
integer y = 540
integer width = 1851
integer height = 620
integer taborder = 40
string dataobject = "d_tipi_campionari_det"
boolean border = false
end type

