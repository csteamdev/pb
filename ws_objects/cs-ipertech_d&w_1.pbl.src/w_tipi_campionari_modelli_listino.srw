﻿$PBExportHeader$w_tipi_campionari_modelli_listino.srw
$PBExportComments$Finestra Tabella Descrizioni X Variabili X Formule X Distinta Base
forward
global type w_tipi_campionari_modelli_listino from w_cs_xx_principale
end type
type dw_tipi_campionari_modelli_listino from uo_cs_xx_dw within w_tipi_campionari_modelli_listino
end type
end forward

global type w_tipi_campionari_modelli_listino from w_cs_xx_principale
integer width = 3346
integer height = 2124
string title = "Listino Teli per Modello/Campionario"
dw_tipi_campionari_modelli_listino dw_tipi_campionari_modelli_listino
end type
global w_tipi_campionari_modelli_listino w_tipi_campionari_modelli_listino

on w_tipi_campionari_modelli_listino.create
int iCurrent
call super::create
this.dw_tipi_campionari_modelli_listino=create dw_tipi_campionari_modelli_listino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_campionari_modelli_listino
end on

on w_tipi_campionari_modelli_listino.destroy
call super::destroy
destroy(this.dw_tipi_campionari_modelli_listino)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_campionari_modelli_listino.set_dw_key("cod_azienda")
dw_tipi_campionari_modelli_listino.set_dw_key("cod_tipo_campionario")
dw_tipi_campionari_modelli_listino.set_dw_key("cod_modello")
dw_tipi_campionari_modelli_listino.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_tipi_campionari_modelli_listino

end event

type dw_tipi_campionari_modelli_listino from uo_cs_xx_dw within w_tipi_campionari_modelli_listino
integer x = 23
integer width = 3269
integer height = 2000
string dataobject = "d_tipi_campionari_modelli_listino"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_tipo_campionario
long ll_errore

ls_cod_tipo_campionario = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_campionario")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_campionario)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_campionario
long ll_errore, l_Idx

ls_cod_tipo_campionario = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_tipo_campionario")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_tipo_campionario")) THEN
      SetItem(l_Idx, "cod_tipo_campionario", ls_cod_tipo_campionario)
   END IF
NEXT


end event

event ue_key;call super::ue_key;if keyflags = 1 and key = keyF1! then
	
	choose case this.getcolumnname( )
		case "cod_modello"
			guo_ricerca.uof_ricerca_prodotto(dw_tipi_campionari_modelli_listino,"cod_modello")
	
		case "cod_prodotto_telo"
			guo_ricerca.uof_ricerca_prodotto(dw_tipi_campionari_modelli_listino,"cod_prodotto_telo")
	end choose
end if
end event

