﻿$PBExportHeader$w_comandi_posizioni.srw
forward
global type w_comandi_posizioni from w_cs_xx_principale
end type
type dw_comandi_posizioni_lista from uo_cs_xx_dw within w_comandi_posizioni
end type
end forward

global type w_comandi_posizioni from w_cs_xx_principale
int Width=2291
int Height=1149
boolean TitleBar=true
string Title="Posizioni Comandi"
dw_comandi_posizioni_lista dw_comandi_posizioni_lista
end type
global w_comandi_posizioni w_comandi_posizioni

on w_comandi_posizioni.create
int iCurrent
call w_cs_xx_principale::create
this.dw_comandi_posizioni_lista=create dw_comandi_posizioni_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_comandi_posizioni_lista
end on

on w_comandi_posizioni.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_comandi_posizioni_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_comandi_posizioni_lista.set_dw_key("cod_azienda")
dw_comandi_posizioni_lista.set_dw_key("cod_comando")
dw_comandi_posizioni_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_comandi_posizioni_lista
end event

type dw_comandi_posizioni_lista from uo_cs_xx_dw within w_comandi_posizioni
int X=23
int Y=21
int Width=2195
int Height=1001
string DataObject="d_comandi_posizioni_lista"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_comando
long ll_errore

ls_cod_comando = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_comando")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_comando)
if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_comando
long l_Idx

ls_cod_comando = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_comando")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_comando")) THEN
      SetItem(l_Idx, "cod_comando", ls_cod_comando)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;string ls_cod_comando
long ll_progressivo

ls_cod_comando = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_comando")
select max(progressivo)
into   :ll_progressivo
from   tab_comandi_posizioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_comando = :ls_cod_comando;
if ll_progressivo = 0 or isnull(ll_progressivo) then
	ll_progressivo = 10
else
	ll_progressivo = ll_progressivo + 10
end if

setitem(getrow(),"progressivo", ll_progressivo)


end event

