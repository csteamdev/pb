﻿$PBExportHeader$w_limiti_comando_verniciature.srw
$PBExportComments$Finestra Tabella Limiti per Comandi
forward
global type w_limiti_comando_verniciature from w_cs_xx_principale
end type
type st_nota from statictext within w_limiti_comando_verniciature
end type
type cb_1 from commandbutton within w_limiti_comando_verniciature
end type
type dw_limiti_comando_verniciatura from uo_cs_xx_dw within w_limiti_comando_verniciature
end type
end forward

global type w_limiti_comando_verniciature from w_cs_xx_principale
integer width = 1957
integer height = 1236
string title = "Verniciature per Comando"
st_nota st_nota
cb_1 cb_1
dw_limiti_comando_verniciatura dw_limiti_comando_verniciatura
end type
global w_limiti_comando_verniciature w_limiti_comando_verniciature

on w_limiti_comando_verniciature.create
int iCurrent
call super::create
this.st_nota=create st_nota
this.cb_1=create cb_1
this.dw_limiti_comando_verniciatura=create dw_limiti_comando_verniciatura
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_nota
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_limiti_comando_verniciatura
end on

on w_limiti_comando_verniciature.destroy
call super::destroy
destroy(this.st_nota)
destroy(this.cb_1)
destroy(this.dw_limiti_comando_verniciatura)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_limiti_comando_verniciatura,"cod_verniciatura",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura')")


end event

event pc_setwindow;call super::pc_setwindow;dw_limiti_comando_verniciatura.set_dw_key("cod_azienda")
dw_limiti_comando_verniciatura.set_dw_key("cod_comando")
dw_limiti_comando_verniciatura.set_dw_key("prog_limite_comando")
dw_limiti_comando_verniciatura.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_limiti_comando_verniciatura

end event

type st_nota from statictext within w_limiti_comando_verniciature
integer x = 27
integer y = 1032
integer width = 1445
integer height = 88
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Il carico verniciature si riferisce alla verniciatura 1"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_limiti_comando_verniciature
integer x = 1509
integer y = 1040
integer width = 389
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Vern."
end type

event clicked;string ls_cod_comando, ls_cod_modello,ls_cod_verniciatura
long ll_prog_limite_comando

ls_cod_comando = dw_limiti_comando_verniciatura.i_parentdw.getitemstring(dw_limiti_comando_verniciatura.i_parentdw.getrow(), "cod_comando")
ls_cod_modello = dw_limiti_comando_verniciatura.i_parentdw.getitemstring(dw_limiti_comando_verniciatura.i_parentdw.getrow(), "cod_modello_tenda")
ll_prog_limite_comando = dw_limiti_comando_verniciatura.i_parentdw.getitemnumber(dw_limiti_comando_verniciatura.i_parentdw.getrow(), "prog_limite_comando")

declare 	cu_verniciature cursor for
select 	cod_verniciatura
from   	tab_modelli_verniciature
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_modello = :ls_cod_modello and
			num_verniciatura = 1 and
		 	flag_blocco = 'N';

open cu_verniciature;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN cursore cu_verniciature.~r~n"  + sqlca.sqlerrtext)
	rollback;
	return
end if

delete from tab_limiti_comando_vern  
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_comando = :ls_cod_comando and
		prog_limite_comando = :ll_prog_limite_comando;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione verniciature.~r~n"  + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_verniciature into :ls_cod_verniciatura;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in fetch cursore cu_verniciature.~r~n"  + sqlca.sqlerrtext)
		rollback;
		return
	end if
	if sqlca.sqlcode = 100 then
		exit
	end if
	
	INSERT INTO tab_limiti_comando_vern  
			( cod_azienda,   
			  cod_comando,   
			  prog_limite_comando,   
			  cod_verniciatura )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ls_cod_comando,   
			  :ll_prog_limite_comando,   
			  :ls_cod_verniciatura )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento verniciature dal modello configuratore.~r~n"  + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop
commit;

parent.postevent("pc_retrieve")
end event

type dw_limiti_comando_verniciatura from uo_cs_xx_dw within w_limiti_comando_verniciature
integer x = 23
integer y = 20
integer width = 1874
integer height = 1000
string dataobject = "d_limiti_comando_verniciatura"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_comando
long ll_errore, ll_prog_limite_comando

ls_cod_comando = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_comando")
ll_prog_limite_comando = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_limite_comando")
ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_comando, ll_prog_limite_comando)
if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_comando
long l_Idx, ll_prog_limite_comando

ls_cod_comando = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_comando")
ll_prog_limite_comando = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_limite_comando")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_comando")) THEN
      SetItem(l_Idx, "cod_comando", ls_cod_comando)
   END IF
NEXT
FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemnumber(l_Idx, "prog_limite_comando")) or GetItemnumber(l_Idx, "prog_limite_comando") = 0 THEN
      SetItem(l_Idx, "prog_limite_comando", ll_prog_limite_comando)
   END IF
NEXT

end event

event pcd_view;call super::pcd_view;cb_1.enabled = true
end event

event pcd_modify;call super::pcd_modify;cb_1.enabled = false
end event

event pcd_new;call super::pcd_new;cb_1.enabled = false
end event

