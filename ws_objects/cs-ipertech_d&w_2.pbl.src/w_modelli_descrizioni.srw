﻿$PBExportHeader$w_modelli_descrizioni.srw
$PBExportComments$Finestra Assegnazione Verniciature per Modello
forward
global type w_modelli_descrizioni from w_cs_xx_principale
end type
type dw_modelli_descrizioni from uo_cs_xx_dw within w_modelli_descrizioni
end type
end forward

global type w_modelli_descrizioni from w_cs_xx_principale
integer width = 2935
integer height = 1144
string title = "Descrizioni per Modello"
dw_modelli_descrizioni dw_modelli_descrizioni
end type
global w_modelli_descrizioni w_modelli_descrizioni

type variables
boolean ib_mvline
end variables

forward prototypes
public function integer wf_dividi_varianti_mvline (string fs_stringa, ref string fs_left_string, ref string fs_right_string)
end prototypes

public function integer wf_dividi_varianti_mvline (string fs_stringa, ref string fs_left_string, ref string fs_right_string);// questa funzione separa l'ultima parte della descrizione per evidenziare le varianti
// esempio: se la descrizione è VERA B 45 la due parti diventano "VERA B" e "45".
long ll_len , ll_i
if len(fs_stringa) < 1 then return 0

if  ib_mvline then
	
	ll_len = len(fs_stringa)
	
	// partendo  da destra cerco il primo spazio
	for ll_i = 1 to ll_len step -1
		
		if mid(fs_stringa, ll_i,1) = " " then
			// ok trovato il punto in cui dividere la stringa
			fs_left_string = left(fs_stringa, ll_i - 1)
			fs_right_string = right(fs_stringa, len(fs_stringa) - ll_i)
			return 0
		else
			// deve essere per forza un numero
			fs_left_string = ""
			fs_right_string = ""
			return -1
		end if
	next
	
end if
end function

on w_modelli_descrizioni.create
int iCurrent
call super::create
this.dw_modelli_descrizioni=create dw_modelli_descrizioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_modelli_descrizioni
end on

on w_modelli_descrizioni.destroy
call super::destroy
destroy(this.dw_modelli_descrizioni)
end on

event pc_setwindow;call super::pc_setwindow;dw_modelli_descrizioni.set_dw_key("cod_azienda")
dw_modelli_descrizioni.set_dw_key("cod_modello")
dw_modelli_descrizioni.set_dw_options(sqlca,i_openparm,c_default,c_default)

iuo_dw_main = dw_modelli_descrizioni
end event

type dw_modelli_descrizioni from uo_cs_xx_dw within w_modelli_descrizioni
event ue_dividi_descrizione ( )
integer x = 23
integer y = 20
integer width = 2843
integer height = 1000
string dataobject = "d_modelli_descrizioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dividi_descrizione();long ll_i, ll_ret
string ls_str, ls_str_left, ls_str_right

for ll_i = 1 to rowcount()
	
	ls_str = getitemstring(ll_i, "des_modello")
	ll_ret = wf_dividi_varianti_mvline(ls_str, ls_str_left, ls_str_right)
	if ll_ret >= 0 then
		setitem(ll_i, "variante_1", ls_str_left)
		setitem(ll_i, "variante_2", ls_str_right)
	end if	
next
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_modello, ls_cod_articolo_mvline
long ll_errore, ll_i

ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_modello)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


ls_cod_articolo_mvline = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_prodotto_mvline")

if not isnull(ls_cod_articolo_mvline) and len(ls_cod_articolo_mvline) > 0 then
	ib_mvline = true
	
	for ll_i = 1 to rowcount()
		setitem(ll_i, "flag_mvline", "S")
	next
	resetupdate()
	//postevent("ue_dividi_descrizione")
	
else
	ib_mvline=false
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_modello
long ll_errore, l_Idx, ll_max

ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

FOR l_Idx = 1 TO RowCount()
	
   IF IsNull(GetItemstring(l_Idx, "cod_modello")) THEN
      SetItem(l_Idx, "cod_modello", ls_cod_modello)
   END IF
	
   IF IsNull(GetItemnumber(l_Idx, "progressivo")) or GetItemnumber(l_Idx, "progressivo") < 1 THEN

      select max(progressivo)
		into   :ll_max
		from   tab_modelli_descrizioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_modello;
				 
		if ll_max = 0 or isnull(ll_max) then
			ll_max = 1 
		else
			ll_max ++
		end if
				 
		SetItem(l_Idx, "progressivo", ll_max)
   END IF
NEXT

end event

