﻿$PBExportHeader$w_tab_cat_mer_tabelle.srw
$PBExportComments$Finestra Tabella Categorie Merceologiche - Tabelle
forward
global type w_tab_cat_mer_tabelle from w_cs_xx_principale
end type
type dw_tab_cat_mer_tabelle from uo_cs_xx_dw within w_tab_cat_mer_tabelle
end type
end forward

global type w_tab_cat_mer_tabelle from w_cs_xx_principale
int Width=2273
int Height=953
boolean TitleBar=true
string Title="Tabella Categorie Merceologiche - Tabelle"
dw_tab_cat_mer_tabelle dw_tab_cat_mer_tabelle
end type
global w_tab_cat_mer_tabelle w_tab_cat_mer_tabelle

on w_tab_cat_mer_tabelle.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_cat_mer_tabelle=create dw_tab_cat_mer_tabelle
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_cat_mer_tabelle
end on

on w_tab_cat_mer_tabelle.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_cat_mer_tabelle)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_cat_mer_tabelle.set_dw_key("cod_azienda")
dw_tab_cat_mer_tabelle.set_dw_key("cod_categoria")
dw_tab_cat_mer_tabelle.set_dw_key("nome_tabella")
dw_tab_cat_mer_tabelle.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_tab_cat_mer_tabelle
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_cat_mer_tabelle,"cod_cat_mer",sqlca,&
                 "tab_cat_mer","cod_cat_mer","des_cat_mer",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_tab_cat_mer_tabelle from uo_cs_xx_dw within w_tab_cat_mer_tabelle
int X=23
int Y=21
int Width=2181
int Height=801
int TabOrder=20
string DataObject="d_tab_cat_mer_tabelle"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

