﻿$PBExportHeader$w_tab_comandi.srw
$PBExportComments$Finestra Tabella Comandi
forward
global type w_tab_comandi from w_cs_xx_principale
end type
type dw_tab_comandi_lista from uo_cs_xx_dw within w_tab_comandi
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_comandi
end type
type cb_posizioni from commandbutton within w_tab_comandi
end type
type dw_tab_comandi_det from uo_cs_xx_dw within w_tab_comandi
end type
end forward

global type w_tab_comandi from w_cs_xx_principale
integer width = 2528
integer height = 1424
string title = "Comandi"
dw_tab_comandi_lista dw_tab_comandi_lista
cb_note_esterne cb_note_esterne
cb_posizioni cb_posizioni
dw_tab_comandi_det dw_tab_comandi_det
end type
global w_tab_comandi w_tab_comandi

type variables
boolean ib_new
end variables

on w_tab_comandi.create
int iCurrent
call super::create
this.dw_tab_comandi_lista=create dw_tab_comandi_lista
this.cb_note_esterne=create cb_note_esterne
this.cb_posizioni=create cb_posizioni
this.dw_tab_comandi_det=create dw_tab_comandi_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_comandi_lista
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.cb_posizioni
this.Control[iCurrent+4]=this.dw_tab_comandi_det
end on

on w_tab_comandi.destroy
call super::destroy
destroy(this.dw_tab_comandi_lista)
destroy(this.cb_note_esterne)
destroy(this.cb_posizioni)
destroy(this.dw_tab_comandi_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_comandi_lista.set_dw_key("cod_azienda")
dw_tab_comandi_lista.set_dw_key("cod_comando")
dw_tab_comandi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_comandi_det.set_dw_options(sqlca,dw_tab_comandi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_comandi_lista
ib_new = false
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_comandi_det,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_comandi_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_comandi_lista.GetItemstring(li_row,"cod_comando"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false
end event

event open;call super::open;dw_tab_comandi_det.object.b_ricerca_prodotto.enabled=false
end event

type dw_tab_comandi_lista from uo_cs_xx_dw within w_tab_comandi
integer x = 23
integer y = 20
integer width = 2446
integer height = 500
integer taborder = 10
string dataobject = "d_tab_comandi_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_comando")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false

end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
cb_posizioni.enabled = false
ib_new = true
dw_tab_comandi_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
cb_posizioni.enabled = false
ib_new = false
dw_tab_comandi_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_comando")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false
cb_posizioni.enabled = true
dw_tab_comandi_det.object.b_ricerca_prodotto.enabled=false
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_comandi
event clicked pbm_bnclicked
integer x = 2103
integer y = 1220
integer height = 80
integer taborder = 30
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_comando
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

ls_cod_comando= dw_tab_comandi_lista.getitemstring(dw_tab_comandi_lista.getrow(), "cod_comando")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_comandi
where      cod_azienda = :s_cs_xx.cod_azienda
and		  cod_comando = :ls_cod_comando;

if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_comandi
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  cod_comando = :ls_cod_comando;
   commit;

end if

setpointer(arrow!)
end event

type cb_posizioni from commandbutton within w_tab_comandi
integer x = 1714
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Posizioni"
end type

event clicked;if dw_tab_comandi_lista.getrow() > 0 then
	window_open_parm(w_comandi_posizioni, -1, dw_tab_comandi_lista)
end if
end event

type dw_tab_comandi_det from uo_cs_xx_dw within w_tab_comandi
integer x = 23
integer y = 540
integer width = 2450
integer height = 668
integer taborder = 20
string dataobject = "d_tab_comandi_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_cat_mer
integer li_count
long ll_cont

choose case i_colname
	case "cod_comando"
		if len(i_coltext) > 0 and not isnull(i_coltext) then
			select cod_cat_mer
			into   :ls_cod_cat_mer
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_prodotto = :i_coltext;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Comandi","Prodotto inesistente")
				return 1
			end if
			if isnull(ls_cod_cat_mer) then
				g_mb.messagebox("Comandi","Manca la categoria merceologica nella anagrafica del prodotto.")
				return 1
			end if
			ll_cont = 0
			select count(*)
			into   :ll_cont
			from   tab_cat_mer_tabelle
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cat_mer = :ls_cod_cat_mer and
			       nome_tabella = 'tab_comandi';
			if ll_cont = 0 or isnull(ll_cont) then
				g_mb.messagebox("Comandi","Questo codice non è un comando!", information!)
				return 1
			end if
		end if

	case "flag_default"
		if data = "S" then
		  select count(*)
			 into :li_count
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					flag_default = 'S';

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Tabella Comandi", "Errore nell'Estrazione Comandi")
				return 2
			end if
			if li_count > 0 then
				g_mb.messagebox("Tabella Comandi Lastre", "Valore di Default già Inserito")
				return 2
			end if
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_comandi_det,"cod_comando")
end choose
end event

