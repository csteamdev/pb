﻿$PBExportHeader$w_tab_comandi_tende_verticali.srw
$PBExportComments$Finestra Tabella Comandi Tende Verticali
forward
global type w_tab_comandi_tende_verticali from w_cs_xx_principale
end type
type dw_tab_comandi_tende_verticali_lista from uo_cs_xx_dw within w_tab_comandi_tende_verticali
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_comandi_tende_verticali
end type
type dw_tab_comandi_tende_verticali_det from uo_cs_xx_dw within w_tab_comandi_tende_verticali
end type
end forward

global type w_tab_comandi_tende_verticali from w_cs_xx_principale
int Width=2582
int Height=1185
boolean TitleBar=true
string Title="Tabella Comandi Tende Verticali"
dw_tab_comandi_tende_verticali_lista dw_tab_comandi_tende_verticali_lista
cb_note_esterne cb_note_esterne
dw_tab_comandi_tende_verticali_det dw_tab_comandi_tende_verticali_det
end type
global w_tab_comandi_tende_verticali w_tab_comandi_tende_verticali

on w_tab_comandi_tende_verticali.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_comandi_tende_verticali_lista=create dw_tab_comandi_tende_verticali_lista
this.cb_note_esterne=create cb_note_esterne
this.dw_tab_comandi_tende_verticali_det=create dw_tab_comandi_tende_verticali_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_comandi_tende_verticali_lista
this.Control[iCurrent+2]=cb_note_esterne
this.Control[iCurrent+3]=dw_tab_comandi_tende_verticali_det
end on

on w_tab_comandi_tende_verticali.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_comandi_tende_verticali_lista)
destroy(this.cb_note_esterne)
destroy(this.dw_tab_comandi_tende_verticali_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_comandi_tende_verticali_lista.set_dw_key("cod_azienda")
dw_tab_comandi_tende_verticali_lista.set_dw_key("cod_comando_tenda_verticale")
dw_tab_comandi_tende_verticali_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_comandi_tende_verticali_det.set_dw_options(sqlca,dw_tab_comandi_tende_verticali_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_comandi_tende_verticali_lista
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_comandi_tende_verticali_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_comandi_tende_verticali_lista.GetItemstring(li_row,"cod_comando_tenda_verticale"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

type dw_tab_comandi_tende_verticali_lista from uo_cs_xx_dw within w_tab_comandi_tende_verticali
int X=22
int Y=20
int Width=2113
int Height=500
int TabOrder=10
string DataObject="d_tab_comandi_tende_verticali_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_comando_tenda_verticale")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_comando_tenda_verticale")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_comandi_tende_verticali
event clicked pbm_bnclicked
int X=2148
int Y=17
int TabOrder=30
string Text="Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_comando_tenda_verticale
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

ls_cod_comando_tenda_verticale= dw_tab_comandi_tende_verticali_lista.getitemstring(dw_tab_comandi_tende_verticali_lista.getrow(), "cod_comando_tenda_verticale")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_comandi_tende_verticali
where      cod_azienda = :s_cs_xx.cod_azienda
and		  cod_comando_tenda_verticale = :ls_cod_comando_tenda_verticale;

if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_comandi_tende_verticali
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  cod_comando_tenda_verticale = :ls_cod_comando_tenda_verticale;

   commit;

end if

setpointer(arrow!)
end event

type dw_tab_comandi_tende_verticali_det from uo_cs_xx_dw within w_tab_comandi_tende_verticali
int X=22
int Y=541
int Width=2489
int Height=519
int TabOrder=20
string DataObject="d_tab_comandi_tende_verticali_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_cod_linea_prodotto, ls_cod_linea
integer li_i, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
			if ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i = 1 to len(ls_cod_linea_prodotto)
					ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_comandi_tende_verticali
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_linea_prodotto like :ls_cod_linea or 
							cod_linea_prodotto = '' or
							cod_linea_prodotto is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Colori Lastre", "Errore nell'Estrazione Comando TV")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Comandi TV", "Valore di Default già Inserito per questa Linea di Prodotti")
						return 2
					end if
				next
			else
			  select count(*)
				 into :li_count
				 from tab_comandi_tende_verticali
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Colori Lastre", "Errore nell'Estrazione Comando TV")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Comandi TV", "Valore di Default già Inserito per questa Linea di Prodotti")
					return 2
				end if
			end if
		end if
	case "cod_linea_prodotto"
		this.setitem(row, "flag_default", "N")
end choose
end event

