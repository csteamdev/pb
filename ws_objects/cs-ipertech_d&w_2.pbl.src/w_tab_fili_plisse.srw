﻿$PBExportHeader$w_tab_fili_plisse.srw
$PBExportComments$Finestra Tabella Fili X Tende Plisse
forward
global type w_tab_fili_plisse from w_cs_xx_principale
end type
type dw_tab_fili_plisse_lista from uo_cs_xx_dw within w_tab_fili_plisse
end type
type dw_tab_fili_plisse_det from uo_cs_xx_dw within w_tab_fili_plisse
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_fili_plisse
end type
end forward

global type w_tab_fili_plisse from w_cs_xx_principale
int Width=2615
int Height=1501
boolean TitleBar=true
string Title="Tabella Fili X Plisse"
dw_tab_fili_plisse_lista dw_tab_fili_plisse_lista
dw_tab_fili_plisse_det dw_tab_fili_plisse_det
cb_note_esterne cb_note_esterne
end type
global w_tab_fili_plisse w_tab_fili_plisse

type variables
boolean ib_new
end variables

forward prototypes
public function integer wf_verifica_dimensioni (double fd_lim_inf_profilo_inf, double fd_lim_sup_profilo_inf, string fs_cod_tessuto, string fs_cod_non_a_magazzino)
end prototypes

public function integer wf_verifica_dimensioni (double fd_lim_inf_profilo_inf, double fd_lim_sup_profilo_inf, string fs_cod_tessuto, string fs_cod_non_a_magazzino);// Funzione che verifica l'eventuale incompatibilità tra i limiti delle dimensioni passate come argomento
// e i limiti delle dimensioni dei fili_plisse già presenti in tab_fili_plisse
// Argomenti :
// 	fd_lim_inf_profilo_inf : limite inferiore dimensione 1
// 	fd_lim_sup_profilo_inf : limite superiore dimensione 1
//		fs_cod_tessuto : Codice del Tessuto
//		fs_cod_non_a_magazzino : Codice non A Magazzino del Tessuto
// Ritorna :
//		0 : Limiti Validi
//		-1: Limiti non Validi
// Mauro 17/08/1998 (che afa ragassi!!!)

double ld_lim_inf_profilo_inf, ld_lim_sup_profilo_inf

declare cur_fili_plisse cursor for
  select lim_inf_profilo_inf,
         lim_sup_profilo_inf
    from tab_fili_plisse 
   where cod_azienda = :s_cs_xx.cod_azienda 
	  and cod_tessuto = :fs_cod_tessuto
	  and cod_non_a_magazzino = :fs_cod_non_a_magazzino;

open cur_fili_plisse;

fetch cur_fili_plisse into :ld_lim_inf_profilo_inf, :ld_lim_sup_profilo_inf;

do while sqlca.sqlcode = 0
	if 	(ld_lim_inf_profilo_inf >= fd_lim_inf_profilo_inf and ld_lim_inf_profilo_inf <= fd_lim_sup_profilo_inf) or &
			(ld_lim_sup_profilo_inf >= fd_lim_inf_profilo_inf and ld_lim_sup_profilo_inf <= fd_lim_sup_profilo_inf) or &
			(fd_lim_inf_profilo_inf >= ld_lim_inf_profilo_inf and fd_lim_inf_profilo_inf <= ld_lim_sup_profilo_inf) or &
			(fd_lim_sup_profilo_inf >= ld_lim_inf_profilo_inf and fd_lim_sup_profilo_inf <= ld_lim_sup_profilo_inf) or &
			(fd_lim_inf_profilo_inf > fd_lim_sup_profilo_inf and fd_lim_sup_profilo_inf <> 0) then
		close cur_fili_plisse;
		return -1	
	end if
	fetch cur_fili_plisse into :ld_lim_inf_profilo_inf, :ld_lim_sup_profilo_inf;
loop
close cur_fili_plisse;

return 0
end function

on w_tab_fili_plisse.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_fili_plisse_lista=create dw_tab_fili_plisse_lista
this.dw_tab_fili_plisse_det=create dw_tab_fili_plisse_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_fili_plisse_lista
this.Control[iCurrent+2]=dw_tab_fili_plisse_det
this.Control[iCurrent+3]=cb_note_esterne
end on

on w_tab_fili_plisse.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_fili_plisse_lista)
destroy(this.dw_tab_fili_plisse_det)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_fili_plisse_lista.set_dw_key("cod_azienda")
dw_tab_fili_plisse_lista.set_dw_key("prog_filo_plisse")
dw_tab_fili_plisse_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_fili_plisse_det.set_dw_options(sqlca,dw_tab_fili_plisse_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_fili_plisse_lista
ib_new = false
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_fili_plisse_lista,"cod_tessuto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tessuto from tab_tessuti where cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "')")
					  
f_PO_LoadDDDW_DW(dw_tab_fili_plisse_det,"cod_tessuto",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tessuto from tab_tessuti where cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "')")
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_fili_plisse_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_fili_plisse_lista.GetItemnumber(li_row,"prog_filo_plisse"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false
end event

type dw_tab_fili_plisse_lista from uo_cs_xx_dw within w_tab_fili_plisse
int X=14
int Y=17
int Width=2167
int Height=497
int TabOrder=10
string DataObject="d_tab_fili_plisse_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemnumber(li_row,"prog_filo_plisse")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
ib_new = true
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
ib_new = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemnumber(li_row,"prog_filo_plisse")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_new = false
end event

event updatestart;call super::updatestart;if ib_new then
	integer li_prog_filo_plisse
	
	li_prog_filo_plisse = 0
	
	select max(prog_filo_plisse)
	 into :li_prog_filo_plisse  
	 from tab_fili_plisse
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Tabella Fili X Plisse", "Errore durante il Calcolo del Progressivo Filo X Plisse")
		pcca.error = c_fatal
		return -1
	end if
	if li_prog_filo_plisse = 0 or isnull(li_prog_filo_plisse) then
		li_prog_filo_plisse = 1
	else
		li_prog_filo_plisse = li_prog_filo_plisse + 1
	end if
	this.setitem(this.getrow(), "prog_filo_plisse", li_prog_filo_plisse)
end if
end event

type dw_tab_fili_plisse_det from uo_cs_xx_dw within w_tab_fili_plisse
int X=14
int Y=525
int Width=2542
int Height=849
int TabOrder=20
boolean BringToTop=true
string DataObject="d_tab_fili_plisse_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	double ld_lim_inf_profilo_inf, ld_lim_sup_profilo_inf
	string ls_null, ls_cod_tessuto, ls_cod_non_a_magazzino
	string ls_cod_linea_prodotto, ls_cod_linea
	integer li_i, li_count

	setnull(ls_null)
	choose case i_colname
		case "lim_inf_profilo_inf"
			ls_cod_tessuto = this.getitemstring(row, "cod_tessuto")
			ls_cod_non_a_magazzino = this.getitemstring(row, "cod_non_a_magazzino")
			ld_lim_sup_profilo_inf = this.getitemnumber(row, "lim_sup_profilo_inf")
			if wf_verifica_dimensioni(double(data), ld_lim_sup_profilo_inf, ls_cod_tessuto, &
								  			 ls_cod_non_a_magazzino) = -1 then
				g_mb.messagebox("Tabella Fili X Plisse", "Limite non Valido")
				return 2
			end if
		case "lim_sup_profilo_inf"
			ls_cod_tessuto = this.getitemstring(row, "cod_tessuto")
			ls_cod_non_a_magazzino = this.getitemstring(row, "cod_non_a_magazzino")
			ld_lim_inf_profilo_inf = this.getitemnumber(row, "lim_inf_profilo_inf")
			if wf_verifica_dimensioni(ld_lim_inf_profilo_inf, double(data), ls_cod_tessuto, &
											 ls_cod_non_a_magazzino) = -1 then
				g_mb.messagebox("Tabella Fili X Plisse", "Limite non Valido")
				return 2
			end if
		case "cod_non_a_magazzino"
			ls_cod_tessuto = this.getitemstring(row, "cod_tessuto")
			ld_lim_inf_profilo_inf = this.getitemnumber(row, "lim_inf_profilo_inf")
			ld_lim_sup_profilo_inf = this.getitemnumber(row, "lim_sup_profilo_inf")
			if wf_verifica_dimensioni(ld_lim_inf_profilo_inf, ld_lim_sup_profilo_inf, ls_cod_tessuto, &
											 data) = -1 then
				g_mb.messagebox("Tabella Fili X Plisse", "Limiti non Validi per questo Tessuto")
				return 2
			end if
			this.setitem(row, "flag_default", "N")
		case "cod_tessuto"
			this.setitem(row, "cod_non_a_magazzino", ls_null)
			this.setitem(row, "flag_default", "N")
			f_PO_LoadDDDW_DW(dw_tab_fili_plisse_det,"cod_non_a_magazzino",sqlca,&
								  "tab_tessuti","cod_non_a_magazzino","cod_non_a_magazzino",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tessuto = '" + i_coltext + "'")

		case "flag_default"
			if data = "S" then
				ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
				ls_cod_tessuto = this.getitemstring(row, "cod_tessuto")
				ls_cod_non_a_magazzino = this.getitemstring(row, "cod_non_a_magazzino")
				if ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
					for li_i = 1 to len(ls_cod_linea_prodotto)
						ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i, 1) + "%"
						
					  select count(*)
						 into :li_count
						 from tab_fili_plisse
						where cod_azienda = :s_cs_xx.cod_azienda and
								(cod_linea_prodotto like :ls_cod_linea or 
								cod_linea_prodotto = '' or
								cod_linea_prodotto is null) and
								cod_tessuto = :ls_cod_tessuto and
								cod_non_a_magazzino = :ls_cod_non_a_magazzino and
								flag_default = 'S';
		
						if sqlca.sqlcode = -1 then
							g_mb.messagebox("Tabella Fili Plisse", "Errore nell'Estrazione Fili Plisse")
							return 2
						end if
						if li_count > 0 then
							g_mb.messagebox("Tabella Fili Plisse", "Valore di Default già Inserito per questa Linea di Prodotti")
							return 2
						end if
					next
				else
				  select count(*)
					 into :li_count
					 from tab_fili_plisse
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_tessuto = :ls_cod_tessuto and
							cod_non_a_magazzino = :ls_cod_non_a_magazzino and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Fili Plisse", "Errore nell'Estrazione Fili Plisse")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Fili Plisse", "Valore di Default già Inserito")
						return 2
					end if
				end if
			end if
		case "cod_linea_prodotto"
			this.setitem(row, "flag_default", "N")
	end choose
end if
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_fili_plisse
event clicked pbm_bnclicked
int X=2204
int Y=17
int Width=353
int TabOrder=30
string Text="Docu&mento"
end type

event clicked;call super::clicked;integer li_prog_filo_plisse
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

li_prog_filo_plisse = dw_tab_fili_plisse_lista.getitemnumber(dw_tab_fili_plisse_lista.getrow(), "prog_filo_plisse")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_fili_plisse
where      cod_azienda = :s_cs_xx.cod_azienda
and		  prog_filo_plisse = :li_prog_filo_plisse;


if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_fili_plisse
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  prog_filo_plisse = :li_prog_filo_plisse;
   commit;

end if

setpointer(arrow!)
end event

