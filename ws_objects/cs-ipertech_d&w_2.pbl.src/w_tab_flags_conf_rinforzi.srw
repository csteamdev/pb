﻿$PBExportHeader$w_tab_flags_conf_rinforzi.srw
$PBExportComments$Finestra Tabella Flags X Configuratore
forward
global type w_tab_flags_conf_rinforzi from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_flags_conf_rinforzi
end type
end forward

global type w_tab_flags_conf_rinforzi from w_cs_xx_principale
integer width = 1815
integer height = 1100
string title = "Tipi Dettagli per Configuratore"
boolean center = true
dw_lista dw_lista
end type
global w_tab_flags_conf_rinforzi w_tab_flags_conf_rinforzi

type variables
private:
	string is_cod_modello
	string is_title
end variables

on w_tab_flags_conf_rinforzi.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_flags_conf_rinforzi.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;is_title = "Tabella Rinfozi Modello"
title = is_title


dw_lista.set_dw_key("id_rinforzi")

dw_lista.set_dw_options(sqlca, i_openparm,c_default,c_default)

iuo_dw_main = dw_lista
end event

type dw_lista from uo_cs_xx_dw within w_tab_flags_conf_rinforzi
integer x = 23
integer y = 20
integer width = 1737
integer height = 960
integer taborder = 10
string dataobject = "d_tab_flags_config_rinforzi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;int li_i

for li_i = 1 to RowCount()
	if IsNull(GetItemstring(li_i, "cod_azienda")) then
		SetItem(li_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if IsNull(GetItemstring(li_i, "cod_modello")) then
		SetItem(li_i, "cod_modello", is_cod_modello)
	end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

is_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

if is_cod_modello <> "" then
	parent.title = is_title + ": " + is_cod_modello
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_modello)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

