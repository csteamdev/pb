﻿$PBExportHeader$w_tab_flags_conf_tipi_det_ven.srw
$PBExportComments$Finestra Tabella Flags X Configuratore
forward
global type w_tab_flags_conf_tipi_det_ven from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_flags_conf_tipi_det_ven
end type
end forward

global type w_tab_flags_conf_tipi_det_ven from w_cs_xx_principale
integer width = 4453
integer height = 1108
string title = "Tipi Dettagli per Configuratore"
boolean center = true
dw_lista dw_lista
end type
global w_tab_flags_conf_tipi_det_ven w_tab_flags_conf_tipi_det_ven

type variables
private:
	string is_cod_modello
	string is_title
end variables

on w_tab_flags_conf_tipi_det_ven.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_flags_conf_tipi_det_ven.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_lista,"cod_tipo_ord_ven",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_lista,"cod_tipo_det_ven_prodotto",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_lista,"cod_tipo_det_ven_addizionali",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_lista,"cod_tipo_det_ven_optionals",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;is_title = "Tipi Dettagli per Configuratore"
title = is_title


dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_modello")
dw_lista.set_dw_key("cod_tipo_ord_ven")

dw_lista.set_dw_options(sqlca, i_openparm,c_default,c_default)
//dw_lista.set_dw_options(sqlca,dw_tab_flags_configuratore_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_lista
end event

type dw_lista from uo_cs_xx_dw within w_tab_flags_conf_tipi_det_ven
integer x = 23
integer y = 20
integer width = 4366
integer height = 960
integer taborder = 10
string dataobject = "d_tab_flag_conf_tipi_det_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_setkey;call super::pcd_setkey;int li_i

for li_i = 1 to RowCount()
	if IsNull(GetItemstring(li_i, "cod_azienda")) then
		SetItem(li_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if IsNull(GetItemstring(li_i, "cod_modello")) then
		SetItem(li_i, "cod_modello", is_cod_modello)
	end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

is_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

if is_cod_modello <> "" then
	parent.title = is_title + " - cod.modello: " + is_cod_modello
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, is_cod_modello)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_validate;call super::pcd_validate;int li_i

for li_i = 1 to rowcount()
	
	if isnull(getitemstring(li_i, "cod_tipo_ord_ven")) then
		g_mb.error("Selezionare un Tipo Ordine per ogni riga prima di salvare")
		PCCA.Error = c_fatal
		return -1
	end if
	
	if isnull(getitemstring(li_i, "cod_tipo_det_ven_prodotto")) then
		g_mb.error("Selezionare un Tipo Dettaglio Prodotto per ogni riga prima di salvare")
		PCCA.Error = c_fatal
		return -1
	end if
	
	if isnull(getitemstring(li_i, "cod_tipo_det_ven_addizionali")) then
		g_mb.error("Selezionare un Tipo Dettaglio Addizionali per ogni riga prima di salvare")
		PCCA.Error = c_fatal
		return -1
	end if
	
	
	if isnull(getitemstring(li_i, "cod_tipo_det_ven_optionals")) then
		g_mb.error("Selezionare un Tipo Dettaglio Optionals per ogni riga prima di salvare")
		PCCA.Error = c_fatal
		return -1
	end if
	
next
end event

