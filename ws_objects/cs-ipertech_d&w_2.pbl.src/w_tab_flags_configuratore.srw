﻿$PBExportHeader$w_tab_flags_configuratore.srw
$PBExportComments$Finestra Tabella Flags X Configuratore
forward
global type w_tab_flags_configuratore from w_cs_xx_principale
end type
type cb_rinforzi from commandbutton within w_tab_flags_configuratore
end type
type cb_tasca from commandbutton within w_tab_flags_configuratore
end type
type cb_tipi_ord_ven from commandbutton within w_tab_flags_configuratore
end type
type cb_optionals from commandbutton within w_tab_flags_configuratore
end type
type cb_descrizioni from commandbutton within w_tab_flags_configuratore
end type
type cb_verniciature from commandbutton within w_tab_flags_configuratore
end type
type dw_tab_flags_configuratore_det from uo_cs_xx_dw within w_tab_flags_configuratore
end type
type dw_tab_flags_configuratore_lista from uo_cs_xx_dw within w_tab_flags_configuratore
end type
type dw_folder from u_folder within w_tab_flags_configuratore
end type
type dw_ricerca from uo_dw_search within w_tab_flags_configuratore
end type
type dw_folder_data from u_folder within w_tab_flags_configuratore
end type
type dw_tab_flags_configuratore_dop from uo_cs_xx_dw within w_tab_flags_configuratore
end type
end forward

global type w_tab_flags_configuratore from w_cs_xx_principale
integer width = 4142
integer height = 3220
string title = "Parametri Configuratore"
cb_rinforzi cb_rinforzi
cb_tasca cb_tasca
cb_tipi_ord_ven cb_tipi_ord_ven
cb_optionals cb_optionals
cb_descrizioni cb_descrizioni
cb_verniciature cb_verniciature
dw_tab_flags_configuratore_det dw_tab_flags_configuratore_det
dw_tab_flags_configuratore_lista dw_tab_flags_configuratore_lista
dw_folder dw_folder
dw_ricerca dw_ricerca
dw_folder_data dw_folder_data
dw_tab_flags_configuratore_dop dw_tab_flags_configuratore_dop
end type
global w_tab_flags_configuratore w_tab_flags_configuratore

type variables
boolean				ib_ANC = false

string					is_sql_base = ""
end variables

forward prototypes
public function integer wf_ricerca (string as_errore)
end prototypes

public function integer wf_ricerca (string as_errore);string			ls_sql, ls_valore


dw_ricerca.accepttext()

ls_sql = is_sql_base + " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

ls_valore = dw_ricerca.getitemstring(1, "cod_modello")
if g_str.isnotempty(ls_valore) then
	ls_sql += " and cod_modello like '"+ls_valore+"'"
end if

ls_valore = dw_ricerca.getitemstring(1, "cod_gruppo_var_quan_distinta")
if g_str.isnotempty(ls_valore) then
	ls_sql += " and cod_gruppo_var_quan_distinta='"+ls_valore+"'"
end if

ls_valore = dw_ricerca.getitemstring(1, "cod_prodotto_nota")
if g_str.isnotempty(ls_valore) then
	ls_sql += " and cod_prodotto_nota like '"+ls_valore+"'"
end if

ls_sql += " order by cod_modello"

if dw_tab_flags_configuratore_lista.setsqlselect(ls_sql) < 0 then
	as_errore = "Errore in impostazione select!"
	return -1
end if

if dw_tab_flags_configuratore_lista.retrieve() < 0 then
	as_errore = "Errore in retrieve lista!"
	return -1
end if


dw_folder.fu_selecttab(2)

return 0
end function

on w_tab_flags_configuratore.create
int iCurrent
call super::create
this.cb_rinforzi=create cb_rinforzi
this.cb_tasca=create cb_tasca
this.cb_tipi_ord_ven=create cb_tipi_ord_ven
this.cb_optionals=create cb_optionals
this.cb_descrizioni=create cb_descrizioni
this.cb_verniciature=create cb_verniciature
this.dw_tab_flags_configuratore_det=create dw_tab_flags_configuratore_det
this.dw_tab_flags_configuratore_lista=create dw_tab_flags_configuratore_lista
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
this.dw_folder_data=create dw_folder_data
this.dw_tab_flags_configuratore_dop=create dw_tab_flags_configuratore_dop
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_rinforzi
this.Control[iCurrent+2]=this.cb_tasca
this.Control[iCurrent+3]=this.cb_tipi_ord_ven
this.Control[iCurrent+4]=this.cb_optionals
this.Control[iCurrent+5]=this.cb_descrizioni
this.Control[iCurrent+6]=this.cb_verniciature
this.Control[iCurrent+7]=this.dw_tab_flags_configuratore_det
this.Control[iCurrent+8]=this.dw_tab_flags_configuratore_lista
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_ricerca
this.Control[iCurrent+11]=this.dw_folder_data
this.Control[iCurrent+12]=this.dw_tab_flags_configuratore_dop
end on

on w_tab_flags_configuratore.destroy
call super::destroy
destroy(this.cb_rinforzi)
destroy(this.cb_tasca)
destroy(this.cb_tipi_ord_ven)
destroy(this.cb_optionals)
destroy(this.cb_descrizioni)
destroy(this.cb_verniciature)
destroy(this.dw_tab_flags_configuratore_det)
destroy(this.dw_tab_flags_configuratore_lista)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
destroy(this.dw_folder_data)
destroy(this.dw_tab_flags_configuratore_dop)
end on

event pc_setwindow;call super::pc_setwindow;windowobject					lw_oggetti[], lw_null[]


dw_tab_flags_configuratore_lista.set_dw_key("cod_azienda")
dw_tab_flags_configuratore_lista.set_dw_key("cod_modello")
dw_tab_flags_configuratore_lista.set_dw_options(sqlca,pcca.null_object, c_noretrieveonopen, c_default)
dw_tab_flags_configuratore_det.set_dw_options(sqlca, dw_tab_flags_configuratore_lista, c_sharedata+c_scrollparent, c_default)
dw_tab_flags_configuratore_dop.set_dw_options(sqlca, dw_tab_flags_configuratore_lista, c_sharedata+c_scrollparent, c_default)

lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "R.", lw_oggetti[])

lw_oggetti[1] = cb_verniciature
lw_oggetti[2] = cb_descrizioni
lw_oggetti[3] = cb_optionals
lw_oggetti[4] = cb_tipi_ord_ven
lw_oggetti[5] = dw_tab_flags_configuratore_lista
lw_oggetti[6] = cb_tasca
lw_oggetti[7] = cb_rinforzi
dw_folder.fu_assigntab(2, "L.", lw_oggetti[])

dw_folder.fu_folderoptions(dw_folder.c_defaultheight, dw_folder.c_foldertableft)

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

lw_oggetti = lw_null

lw_oggetti[1] = dw_tab_flags_configuratore_det
dw_folder_data.fu_assigntab(1, "Parametri Configuratore", lw_oggetti[])
lw_oggetti[1] = dw_tab_flags_configuratore_dop
dw_folder_data.fu_assigntab(2, "Certificazione DoP", lw_oggetti[])
dw_folder_data.fu_foldercreate(2, 2)
dw_folder_data.fu_selecttab(1)

iuo_dw_main = dw_tab_flags_configuratore_lista

is_sql_base = dw_tab_flags_configuratore_lista.getsqlselect()

guo_functions.uof_get_parametro_azienda("ANC", ib_ANC)
if isnull(ib_ANC) then ib_ANC = false

dw_tab_flags_configuratore_det.object.cod_prodotto_nota_t.visible = ib_ANC
dw_tab_flags_configuratore_det.object.cod_prodotto_nota.visible = ib_ANC
dw_tab_flags_configuratore_det.object.cf_cod_prodotto_nota.visible = ib_ANC
dw_tab_flags_configuratore_det.object.b_prodotto_nota.visible = ib_ANC

dw_ricerca.object.cod_prodotto_nota_t.visible = ib_ANC
dw_ricerca.object.cod_prodotto_nota.visible = ib_ANC
dw_ricerca.object.cf_cod_prodotto_nota.visible = ib_ANC
dw_ricerca.object.b_prodotto_note.visible = ib_ANC

end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ricerca,"cod_gruppo_var_quan_distinta",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	



f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_tipo_det_ven_prodotto",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_tipo_det_ven_addizionali",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_tipo_det_ven_optionals",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_gruppo_var_quan_distinta",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_quan_finito",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_vernic",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_dim_x",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_dim_y",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_dim_z",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_dim_t",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_variabile_telo",sqlca,&
                 "tab_variabili_formule","cod_variabile","'(' + nome_campo_database + ') ' + note",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_formula_dim_x",sqlca,&
                 "tab_formule_db","cod_formula","des_formula",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo = '1' ")					  
					  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_formula_dim_x",sqlca,&
                 "tab_formule_db","cod_formula","des_formula",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo = '1' ")					  
					  		  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_formula_dim_y",sqlca,&
                 "tab_formule_db","cod_formula","des_formula",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo = '1' ")					  
					  				  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_formula_dim_z",sqlca,&
                 "tab_formule_db","cod_formula","des_formula",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo = '1' ")					  
					  				  
f_PO_LoadDDDW_DW(dw_tab_flags_configuratore_det,"cod_formula_dim_t",sqlca,&
                 "tab_formule_db","cod_formula","des_formula",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo = '1' ")					  
					  
					  
					  
					  


end event

event open;call super::open;dw_tab_flags_configuratore_det.object.b_ricerca_prodotto.enabled = false
end event

type cb_rinforzi from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 540
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Rinforzi"
end type

event clicked;window_open_parm(w_tab_flags_conf_rinforzi, -1, dw_tab_flags_configuratore_lista)
end event

type cb_tasca from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 440
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tasca"
end type

event clicked;window_open_parm(w_tab_flags_conf_tipi_tasca, -1, dw_tab_flags_configuratore_lista)
end event

type cb_tipi_ord_ven from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 340
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tipi Ord.Ven"
end type

event clicked;window_open_parm(w_tab_flags_conf_tipi_det_ven, -1, dw_tab_flags_configuratore_lista)
end event

type cb_optionals from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 240
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Optionals"
end type

event clicked;window_open_parm(w_tab_optionals, -1, dw_tab_flags_configuratore_lista)
end event

type cb_descrizioni from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 140
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Descrizioni"
end type

event clicked;window_open_parm(w_modelli_descrizioni, -1, dw_tab_flags_configuratore_lista)
end event

type cb_verniciature from commandbutton within w_tab_flags_configuratore
integer x = 3543
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Verniciature"
end type

event clicked;window_open_parm(w_modelli_verniciature, -1, dw_tab_flags_configuratore_lista)
end event

type dw_tab_flags_configuratore_det from uo_cs_xx_dw within w_tab_flags_configuratore
integer x = 46
integer y = 1060
integer width = 4000
integer height = 2020
integer taborder = 30
string dataobject = "d_tab_flags_configuratore_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case i_colname
		
	case "cod_modello"
		string ls_cod_prodotto
		
		if not isnull(i_coltext) then
			select cod_prodotto
			into   :ls_cod_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :i_coltext and
					 cod_cat_mer in (	select cod_cat_mer 
											from tab_cat_mer_tabelle 
											where cod_azienda = :s_cs_xx.cod_azienda and 
													nome_tabella = 'tab_modelli');
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Questo prodotto non ha la categoria merceologica dei modelli.")
				return 1
			end if
		end if
		
end choose
end event

event getfocus;call super::getfocus;dw_tab_flags_configuratore_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_modello"
end event

event buttonclicked;call super::buttonclicked;
choose case dwo.name
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_flags_configuratore_det,"cod_modello")
		
	case "b_prodotto_nota"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_flags_configuratore_det,"cod_prodotto_nota")
		
end choose
end event

type dw_tab_flags_configuratore_lista from uo_cs_xx_dw within w_tab_flags_configuratore
integer x = 215
integer y = 12
integer width = 2903
integer height = 868
integer taborder = 10
string dataobject = "d_tab_flags_configuratore_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string				ls_errore



if wf_ricerca(ls_errore) < 0 then
	g_mb.error( ls_errore )
	return
end if

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_verniciature.enabled = false
cb_descrizioni.enabled = false
cb_optionals.enabled = false
cb_tipi_ord_ven.enabled = false
cb_tasca.enabled = false
cb_rinforzi.enabled = false
dw_tab_flags_configuratore_det.object.b_ricerca_prodotto.enabled = true

if ib_ANC then dw_tab_flags_configuratore_det.object.b_prodotto_nota.enabled = true

end event

event pcd_new;call super::pcd_new;cb_verniciature.enabled = true
cb_descrizioni.enabled = true
cb_optionals.enabled = true
cb_tipi_ord_ven.enabled = true
cb_tasca.enabled = false
cb_rinforzi.enabled = false

dw_tab_flags_configuratore_det.object.b_ricerca_prodotto.enabled = true

if ib_ANC then dw_tab_flags_configuratore_det.object.b_prodotto_nota.enabled = true

end event

event pcd_view;call super::pcd_view;cb_verniciature.enabled = true
cb_descrizioni.enabled = true
cb_optionals.enabled = true
cb_tipi_ord_ven.enabled = true
cb_tasca.enabled = true
cb_rinforzi.enabled = true

dw_tab_flags_configuratore_det.object.b_ricerca_prodotto.enabled = false


if ib_ANC then dw_tab_flags_configuratore_det.object.b_prodotto_nota.enabled = false
end event

event pcd_save;call super::pcd_save;cb_verniciature.enabled = true
cb_descrizioni.enabled = true
cb_optionals.enabled = true
cb_tipi_ord_ven.enabled = true
cb_tasca.enabled = true
cb_rinforzi.enabled = true

dw_tab_flags_configuratore_det.object.b_ricerca_prodotto.enabled = false

if ib_ANC then dw_tab_flags_configuratore_det.object.b_prodotto_nota.enabled = false

end event

event pcd_delete;call super::pcd_delete;
if ib_ANC then dw_tab_flags_configuratore_det.object.b_prodotto_nota.enabled = false
end event

type dw_folder from u_folder within w_tab_flags_configuratore
integer x = 23
integer width = 4000
integer height = 900
integer taborder = 20
boolean border = false
end type

type dw_ricerca from uo_dw_search within w_tab_flags_configuratore
integer x = 192
integer y = 32
integer width = 2697
integer height = 540
integer taborder = 40
string dataobject = "d_tab_flags_configuratore_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string			ls_null, ls_errore


if row>0 then
else
	return
end if

choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_modello")
		
	case "b_prodotto_note"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_nota")
		
	case "b_cerca"
		dw_tab_flags_configuratore_lista.change_dw_current()
		parent.triggerevent("pc_retrieve")
		
	case "b_annulla"
		setnull(ls_null)
		
		setitem(row, "cod_modello", ls_null)
		setitem(row, "cod_gruppo_var_quan_distinta", ls_null)
		setitem(row, "cod_prodotto_nota", ls_null)
		
end choose
end event

type dw_folder_data from u_folder within w_tab_flags_configuratore
integer x = 23
integer y = 920
integer width = 4069
integer height = 2180
integer taborder = 10
boolean border = false
end type

type dw_tab_flags_configuratore_dop from uo_cs_xx_dw within w_tab_flags_configuratore
integer x = 46
integer y = 1060
integer width = 4000
integer height = 2020
integer taborder = 40
string dataobject = "d_tab_flags_configuratore_dop"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_flags_configuratore_det,"cod_modello")
		
	case "b_prodotto_nota"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_flags_configuratore_det,"cod_prodotto_nota")
		
end choose
end event

event getfocus;call super::getfocus;dw_tab_flags_configuratore_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_modello"
end event

event itemchanged;call super::itemchanged;choose case i_colname
		
	case "cod_modello"
		string ls_cod_prodotto
		
		if not isnull(i_coltext) then
			select cod_prodotto
			into   :ls_cod_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :i_coltext and
					 cod_cat_mer in (	select cod_cat_mer 
											from tab_cat_mer_tabelle 
											where cod_azienda = :s_cs_xx.cod_azienda and 
													nome_tabella = 'tab_modelli');
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Questo prodotto non ha la categoria merceologica dei modelli.")
				return 1
			end if
		end if
		
end choose
end event

