﻿$PBExportHeader$w_tab_limiti_comando.srw
$PBExportComments$Finestra Tabella Limiti per Comandi
forward
global type w_tab_limiti_comando from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_tab_limiti_comando
end type
type cb_verniciature from commandbutton within w_tab_limiti_comando
end type
type cb_reset from commandbutton within w_tab_limiti_comando
end type
type dw_tab_limiti_comando_det from uo_cs_xx_dw within w_tab_limiti_comando
end type
type cb_ricerca from commandbutton within w_tab_limiti_comando
end type
type dw_ricerca from u_dw_search within w_tab_limiti_comando
end type
type dw_tab_limiti_comando_lista from uo_cs_xx_dw within w_tab_limiti_comando
end type
type dw_folder_search from u_folder within w_tab_limiti_comando
end type
end forward

global type w_tab_limiti_comando from w_cs_xx_principale
integer width = 2734
integer height = 1928
string title = "Limiti per Comando"
cb_1 cb_1
cb_verniciature cb_verniciature
cb_reset cb_reset
dw_tab_limiti_comando_det dw_tab_limiti_comando_det
cb_ricerca cb_ricerca
dw_ricerca dw_ricerca
dw_tab_limiti_comando_lista dw_tab_limiti_comando_lista
dw_folder_search dw_folder_search
end type
global w_tab_limiti_comando w_tab_limiti_comando

type variables
boolean ib_new
end variables

on w_tab_limiti_comando.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_verniciature=create cb_verniciature
this.cb_reset=create cb_reset
this.dw_tab_limiti_comando_det=create dw_tab_limiti_comando_det
this.cb_ricerca=create cb_ricerca
this.dw_ricerca=create dw_ricerca
this.dw_tab_limiti_comando_lista=create dw_tab_limiti_comando_lista
this.dw_folder_search=create dw_folder_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_verniciature
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.dw_tab_limiti_comando_det
this.Control[iCurrent+5]=this.cb_ricerca
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.dw_tab_limiti_comando_lista
this.Control[iCurrent+8]=this.dw_folder_search
end on

on w_tab_limiti_comando.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_verniciature)
destroy(this.cb_reset)
destroy(this.dw_tab_limiti_comando_det)
destroy(this.cb_ricerca)
destroy(this.dw_ricerca)
destroy(this.dw_tab_limiti_comando_lista)
destroy(this.dw_folder_search)
end on

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_tab_limiti_comando_lista.set_dw_key("cod_azienda")
dw_tab_limiti_comando_lista.set_dw_key("cod_comando")
dw_tab_limiti_comando_lista.set_dw_key("prog_limite_comando")
dw_tab_limiti_comando_lista.set_dw_options(sqlca,pcca.null_object,c_default + c_noretrieveonopen,c_default)
dw_tab_limiti_comando_det.set_dw_options(sqlca,dw_tab_limiti_comando_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_limiti_comando_lista
ib_new = false

l_criteriacolumn[1] = "cod_comando"
l_criteriacolumn[2] = "cod_modello"
l_criteriacolumn[3] = "flag_default"

l_searchtable[1] = "tab_limiti_comando"
l_searchtable[2] = "tab_limiti_comando"
l_searchtable[3] = "tab_limiti_comando"

l_searchcolumn[1] = "cod_comando"
l_searchcolumn[2] = "cod_modello_tenda"
l_searchcolumn[3] = "flag_default"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tab_limiti_comando_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_tab_limiti_comando_lista
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(2)
dw_tab_limiti_comando_lista.change_dw_current()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_limiti_comando_det,"cod_modello_tenda",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")

dw_ricerca.fu_loadcode("cod_modello",&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli') order by cod_prodotto", "(Tutti)")


end event

event pc_delete;call super::pc_delete;ib_new = false
end event

type cb_1 from commandbutton within w_tab_limiti_comando
integer x = 1879
integer y = 1724
integer width = 389
integer height = 80
integer taborder = 12
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;if dw_tab_limiti_comando_lista.getrow() > 0 then
	window_open_parm(w_duplica_limiti_comandi, -1, dw_tab_limiti_comando_lista)
end if
end event

type cb_verniciature from commandbutton within w_tab_limiti_comando
integer x = 2286
integer y = 1724
integer width = 389
integer height = 80
integer taborder = 12
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Verniciature"
end type

event clicked;if dw_tab_limiti_comando_lista.getrow() > 0 then
	window_open_parm(w_limiti_comando_verniciature, -1, dw_tab_limiti_comando_lista)
end if
end event

type cb_reset from commandbutton within w_tab_limiti_comando
integer x = 1888
integer y = 808
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type dw_tab_limiti_comando_det from uo_cs_xx_dw within w_tab_limiti_comando
integer x = 23
integer y = 936
integer width = 2651
integer height = 772
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tab_limiti_comando_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_modello_tenda, ls_flag_fuori_campionario, ls_flag_addizionale, ls_flag_default, ls_cod_comando
	ls_cod_comando = this.getitemstring(row, "cod_comando")
	
	choose case i_colname
		case "cod_comando"
			select flag_fuori_campionario,
					flag_addizionale,
					flag_default
			 into :ls_flag_fuori_campionario,
					:ls_flag_addizionale,
					:ls_flag_default
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :i_coltext;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Tabella Limiti X Comando", "Attenzione il comando selezionato non è presente nella tabella dei comandi")
				return 2
			end if
			this.setitem(this.getrow(), "flag_fuori_campionario", ls_flag_fuori_campionario)
			this.setitem(this.getrow(), "flag_addizionale", ls_flag_addizionale)
			this.setitem(this.getrow(), "flag_default", ls_flag_default)
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_limiti_comando_det,"cod_comando")

	case "b_ricerca_modello"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_limiti_comando_det,"cod_modello")
end choose
end event

type cb_ricerca from commandbutton within w_tab_limiti_comando
integer x = 2277
integer y = 808
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_tab_limiti_comando_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type dw_ricerca from u_dw_search within w_tab_limiti_comando
integer x = 160
integer y = 40
integer width = 2331
integer height = 420
integer taborder = 30
string dataobject = "d_ext_limiti_comando_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_comando")

	case "b_ricerca_modello"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_modello")
end choose
end event

type dw_tab_limiti_comando_lista from uo_cs_xx_dw within w_tab_limiti_comando
integer x = 201
integer y = 40
integer width = 2432
integer height = 852
integer taborder = 10
string dataobject = "d_tab_limiti_comando_lista"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_comando
LONG  l_Idx,li_prog_limite_comando

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
	
   IF IsNull(GetItemnumber(l_Idx, "prog_limite_comando")) THEN
		ls_cod_comando = getitemstring(l_Idx, "cod_comando")
		li_prog_limite_comando = 0
		
		select max(prog_limite_comando)
		 into :li_prog_limite_comando  
		 from tab_limiti_comando
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_comando = :ls_cod_comando;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Tabella Limiti X Comando", "Errore durante il Calcolo del Progressivo Limite X Comando")
			pcca.error = c_fatal
			return -1
		end if
		if li_prog_limite_comando = 0 or isnull(li_prog_limite_comando) then
			li_prog_limite_comando = 1
		else
			li_prog_limite_comando = li_prog_limite_comando + 1
		end if
		this.setitem(l_Idx, "prog_limite_comando", li_prog_limite_comando)
	end if
NEXT

end event

event pcd_save;call super::pcd_save;ib_new = false
end event

event pcd_new;call super::pcd_new;ib_new = true
dw_tab_limiti_comando_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_modify;call super::pcd_modify;ib_new = false
dw_tab_limiti_comando_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_view;call super::pcd_view;ib_new = false
dw_tab_limiti_comando_det.object.b_ricerca_prodotto.enabled=false
end event

event updatestart;call super::updatestart;string ls_cod_comando
long ll_i, ll_prog_limite_comando

for ll_i = 1 to deletedcount()
	ls_cod_comando = getitemstring(ll_i, "cod_comando", delete!, true)
	ll_prog_limite_comando =getitemnumber(ll_i, "prog_limite_comando", delete!, true)
	
	delete tab_limiti_comando_vern
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_comando = :ls_cod_comando  and
			 prog_limite_comando = :ll_prog_limite_comando;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore durante la cancellazione verniciature collegate.~r~n"+sqlca.sqlerrtext)
		return 1
	end if
next
end event

type dw_folder_search from u_folder within w_tab_limiti_comando
integer x = 23
integer y = 20
integer width = 2651
integer height = 904
integer taborder = 10
end type

