﻿$PBExportHeader$w_tab_optionals.srw
$PBExportComments$Finestra Tabella Optionals
forward
global type w_tab_optionals from w_cs_xx_principale
end type
type dw_tab_optionals_lista from uo_cs_xx_dw within w_tab_optionals
end type
end forward

global type w_tab_optionals from w_cs_xx_principale
integer width = 3863
integer height = 1264
string title = "Tabella Optionals"
dw_tab_optionals_lista dw_tab_optionals_lista
end type
global w_tab_optionals w_tab_optionals

on w_tab_optionals.create
int iCurrent
call super::create
this.dw_tab_optionals_lista=create dw_tab_optionals_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_optionals_lista
end on

on w_tab_optionals.destroy
call super::destroy
destroy(this.dw_tab_optionals_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_optionals_lista.ib_chiavi_protette = false

dw_tab_optionals_lista.set_dw_key("cod_azienda")
dw_tab_optionals_lista.set_dw_key("cod_optional")
dw_tab_optionals_lista.set_dw_key("cod_modello")
dw_tab_optionals_lista.set_dw_options(sqlca, i_openparm  ,c_default,c_default)

iuo_dw_main = dw_tab_optionals_lista


end event

type dw_tab_optionals_lista from uo_cs_xx_dw within w_tab_optionals
integer x = 14
integer y = 16
integer width = 3781
integer height = 1112
integer taborder = 10
string dataobject = "d_tab_optionals_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_modello
LONG  l_Error

ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

l_Error = Retrieve(s_cs_xx.cod_azienda,ls_cod_modello )

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_new;call super::pcd_new;string ls_cod_modello
ls_cod_modello = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_modello")

setitem(getrow(),"tab_optionals_cod_modello", ls_cod_modello)
end event

event ue_key;call super::ue_key;if key =  keyF1! and keyflags = 1 and getcolumnname() = "cod_optional" then
	
	change_dw_current()
	s_cs_xx.parametri.parametro_uo_dw_1 = this
	s_cs_xx.parametri.parametro_s_1 = "cod_optional"
	s_cs_xx.parametri.parametro_tipo_ricerca = 2
	s_cs_xx.parametri.parametro_pos_ricerca = ""
	if not isvalid(w_prodotti_ricerca) then
		window_open(w_prodotti_ricerca, 0)
	end if

end if
		

end event

