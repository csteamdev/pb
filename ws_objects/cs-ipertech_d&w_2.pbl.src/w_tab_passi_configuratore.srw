﻿$PBExportHeader$w_tab_passi_configuratore.srw
$PBExportComments$Finestra Tabella Passi del Configuratore
forward
global type w_tab_passi_configuratore from w_cs_xx_principale
end type
type pb_1 from picturebutton within w_tab_passi_configuratore
end type
type cb_2 from commandbutton within w_tab_passi_configuratore
end type
type cb_1 from commandbutton within w_tab_passi_configuratore
end type
type dw_tab_passi_configuratore_lista from uo_cs_xx_dw within w_tab_passi_configuratore
end type
type cb_3 from cb_prod_ricerca within w_tab_passi_configuratore
end type
type dw_tab_passi_configuratore_det from uo_cs_xx_dw within w_tab_passi_configuratore
end type
end forward

global type w_tab_passi_configuratore from w_cs_xx_principale
integer width = 3561
integer height = 1684
string title = "Passi del Configuratore"
pb_1 pb_1
cb_2 cb_2
cb_1 cb_1
dw_tab_passi_configuratore_lista dw_tab_passi_configuratore_lista
cb_3 cb_3
dw_tab_passi_configuratore_det dw_tab_passi_configuratore_det
end type
global w_tab_passi_configuratore w_tab_passi_configuratore

type variables
boolean ib_new
string is_modello=""
end variables

on w_tab_passi_configuratore.create
int iCurrent
call super::create
this.pb_1=create pb_1
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_tab_passi_configuratore_lista=create dw_tab_passi_configuratore_lista
this.cb_3=create cb_3
this.dw_tab_passi_configuratore_det=create dw_tab_passi_configuratore_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.pb_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_tab_passi_configuratore_lista
this.Control[iCurrent+5]=this.cb_3
this.Control[iCurrent+6]=this.dw_tab_passi_configuratore_det
end on

on w_tab_passi_configuratore.destroy
call super::destroy
destroy(this.pb_1)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_tab_passi_configuratore_lista)
destroy(this.cb_3)
destroy(this.dw_tab_passi_configuratore_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_passi_configuratore_lista.set_dw_key("cod_azienda")
dw_tab_passi_configuratore_lista.set_dw_key("cod_modello")
dw_tab_passi_configuratore_lista.set_dw_key("prog_passo_configuratore")
dw_tab_passi_configuratore_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_passi_configuratore_det.set_dw_options(sqlca,dw_tab_passi_configuratore_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_passi_configuratore_lista
ib_new = false
pb_1.picturename = s_cs_xx.volume + s_cs_xx.risorse + "cartella_apri.bmp"
pb_1.enabled = false
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_tab_passi_configuratore_lista,"cod_modello",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")
//
//f_PO_LoadDDDW_DW(dw_tab_passi_configuratore_det,"cod_modello",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")
end event

event pc_delete;call super::pc_delete;ib_new = false
end event

type pb_1 from picturebutton within w_tab_passi_configuratore
integer x = 2533
integer y = 1128
integer width = 101
integer height = 88
integer taborder = 12
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\CS_70\framework\RISORSE\cartella_apri.bmp"
alignment htextalign = left!
end type

event clicked;string docname, named

integer value

value = GetFileOpenName("Selezione Immagine", &
		+ docname, named, "BMP", &
		+ "Bitmap (*.BMP),*.BMP," &
		+ "Immagini WMF (*.WMF),*.WMF," &
		+ "Immagini JPG (*.JPG),*.JPG," &
		+ "Immagini GIF (*.GIF),*.GIF")

if value = 1 then
	dw_tab_passi_configuratore_det.setitem(dw_tab_passi_configuratore_det.getrow(),"path_immagine_passo", named)
end if
end event

type cb_2 from commandbutton within w_tab_passi_configuratore
integer x = 3109
integer y = 260
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sposta giù"
end type

event clicked;string ls_cod_modello
long ll_num_passo, ll_max

ll_num_passo = dw_tab_passi_configuratore_lista.getitemnumber(dw_tab_passi_configuratore_lista.getrow(),"prog_passo_configuratore")
ls_cod_modello = dw_tab_passi_configuratore_lista.getitemstring(dw_tab_passi_configuratore_lista.getrow(),"cod_modello")
ll_max = 0
select min(prog_passo_configuratore)
into   :ll_max
from   tab_passi_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore > :ll_num_passo;
if ll_max = 0 or isnull(ll_max) then
	g_mb.messagebox("APICE","Non ci sono passi successivi a questo")
	return
end if
update tab_passi_configuratore
set    prog_passo_configuratore = 9999
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = :ll_num_passo;

update tab_passi_configuratore
set    prog_passo_configuratore = :ll_num_passo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = :ll_max;

update tab_passi_configuratore
set    prog_passo_configuratore = :ll_max
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = 9999;

commit;

parent.triggerevent("pc_retrieve")

end event

type cb_1 from commandbutton within w_tab_passi_configuratore
integer x = 3109
integer y = 140
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sposta sù"
end type

event clicked;string ls_cod_modello
long ll_num_passo, ll_max

ll_num_passo = dw_tab_passi_configuratore_lista.getitemnumber(dw_tab_passi_configuratore_lista.getrow(),"prog_passo_configuratore")
ls_cod_modello = dw_tab_passi_configuratore_lista.getitemstring(dw_tab_passi_configuratore_lista.getrow(),"cod_modello")
ll_max = 0
select max(prog_passo_configuratore)
into   :ll_max
from   tab_passi_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore < :ll_num_passo;
if ll_max = 0 or isnull(ll_max) then
	g_mb.messagebox("APICE","Non ci sono passi precedenti a questo")
	return
end if
update tab_passi_configuratore
set    prog_passo_configuratore = 9999
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = :ll_num_passo;

update tab_passi_configuratore
set    prog_passo_configuratore = :ll_num_passo
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = :ll_max;

update tab_passi_configuratore
set    prog_passo_configuratore = :ll_max
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello and
       prog_passo_configuratore = 9999;

commit;

parent.triggerevent("pc_retrieve")

end event

type dw_tab_passi_configuratore_lista from uo_cs_xx_dw within w_tab_passi_configuratore
integer x = 23
integer y = 20
integer width = 3063
integer height = 600
integer taborder = 10
string dataobject = "d_tab_passi_configuratore_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;ib_new = false
is_modello = getitemstring(getrow(),"cod_modello")
end event

event pcd_new;call super::pcd_new;ib_new = true
cb_3.enabled = true
if is_modello <> "" then
	setitem(getrow(),"cod_modello",is_modello)
end if
pb_1.enabled = true
end event

event pcd_modify;call super::pcd_modify;ib_new = false
cb_3.enabled = true
pb_1.enabled = true
end event

event pcd_view;call super::pcd_view;ib_new = false
cb_3.enabled = false
pb_1.enabled = false
end event

event updatestart;call super::updatestart;if ib_new then
	string ls_cod_modello
	integer li_prog_passo_configuratore
	
	ls_cod_modello = this.getitemstring(this.getrow(), "cod_modello")
	li_prog_passo_configuratore = this.getitemnumber(this.getrow(), "prog_passo_configuratore")

	if li_prog_passo_configuratore = 0 or isnull(li_prog_passo_configuratore) then
		select max(prog_passo_configuratore)
		 into :li_prog_passo_configuratore  
		 from tab_passi_configuratore
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_modello = :ls_cod_modello;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Tabella Comandi", "Errore durante il Calcolo del Progressivo Passo")
			pcca.error = c_fatal
			return -1
		end if
		if li_prog_passo_configuratore = 0 or isnull(li_prog_passo_configuratore) then
			li_prog_passo_configuratore = 1
		else
			li_prog_passo_configuratore = li_prog_passo_configuratore + 1
		end if
		this.setitem(this.getrow(), "prog_passo_configuratore", li_prog_passo_configuratore)
	end if
end if
end event

type cb_3 from cb_prod_ricerca within w_tab_passi_configuratore
integer x = 2533
integer y = 692
integer width = 73
integer height = 72
integer taborder = 2
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_tab_passi_configuratore_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_modello"
end event

type dw_tab_passi_configuratore_det from uo_cs_xx_dw within w_tab_passi_configuratore
integer x = 23
integer y = 636
integer width = 3054
integer height = 924
integer taborder = 20
string dataobject = "d_tab_passi_configuratore_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_descrizione, ls_nome_dw, ls_mini_help
	if i_colname = "prog_passo_configuratore" then
		ls_descrizione =""
		ls_nome_dw = ""
		choose case i_coltext
			case "10"
				ls_descrizione = "TESSUTO"
				ls_nome_dw = "d_ext_tessuto"
			case "30"
				ls_descrizione = "COMANDI"
				ls_nome_dw = "d_ext_comandi"
			case "40"
				ls_descrizione = "SUPPORTI"
				ls_nome_dw = "d_ext_supporto_flags"
			case "20"
				ls_descrizione = "VERNICIATURA"
				ls_nome_dw = "d_ext_vern_lastra"
			case "60"
				ls_descrizione = "OPTIONALS"
				ls_nome_dw = "d_ext_optional"
				ls_mini_help = "SHIFT-F1 = ricerca prodotto   SHIFT up/down = inserisci riga prima/dopo  CANC = Cancella riga"
		end choose
		if ls_descrizione <> "" then
			setitem(getrow(),"des_passo_configuratore",ls_descrizione)
			setitem(getrow(),"des_visualizzata",ls_descrizione)
			setitem(getrow(),"nome_data_window",ls_nome_dw)
			if ls_nome_dw = "d_ext_optional" then 	setitem(getrow(),"mini_help",ls_mini_help)
		end if
	end if
end if
end event

