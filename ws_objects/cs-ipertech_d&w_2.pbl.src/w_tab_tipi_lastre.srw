﻿$PBExportHeader$w_tab_tipi_lastre.srw
$PBExportComments$Finestra Tabella Tipi Lastre
forward
global type w_tab_tipi_lastre from w_cs_xx_principale
end type
type dw_tab_tipi_lastre_lista from uo_cs_xx_dw within w_tab_tipi_lastre
end type
type dw_tab_tipi_lastre_det from uo_cs_xx_dw within w_tab_tipi_lastre
end type
end forward

global type w_tab_tipi_lastre from w_cs_xx_principale
integer width = 2482
integer height = 1184
string title = "Tabella Tipi Lastre"
dw_tab_tipi_lastre_lista dw_tab_tipi_lastre_lista
dw_tab_tipi_lastre_det dw_tab_tipi_lastre_det
end type
global w_tab_tipi_lastre w_tab_tipi_lastre

on w_tab_tipi_lastre.create
int iCurrent
call super::create
this.dw_tab_tipi_lastre_lista=create dw_tab_tipi_lastre_lista
this.dw_tab_tipi_lastre_det=create dw_tab_tipi_lastre_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_tipi_lastre_lista
this.Control[iCurrent+2]=this.dw_tab_tipi_lastre_det
end on

on w_tab_tipi_lastre.destroy
call super::destroy
destroy(this.dw_tab_tipi_lastre_lista)
destroy(this.dw_tab_tipi_lastre_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_lastre_lista.set_dw_key("cod_azienda")
dw_tab_tipi_lastre_lista.set_dw_key("cod_tipo_lastra")
dw_tab_tipi_lastre_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipi_lastre_det.set_dw_options(sqlca,dw_tab_tipi_lastre_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_tipi_lastre_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_tipi_lastre_det,"cod_modello",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")


end event

type dw_tab_tipi_lastre_lista from uo_cs_xx_dw within w_tab_tipi_lastre
integer x = 23
integer y = 20
integer width = 2400
integer height = 500
integer taborder = 20
string dataobject = "d_tab_tipi_lastre_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

type dw_tab_tipi_lastre_det from uo_cs_xx_dw within w_tab_tipi_lastre
integer x = 23
integer y = 540
integer width = 2400
integer height = 520
integer taborder = 10
string dataobject = "d_tab_tipi_lastre_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_linea_prodotto, ls_cod_linea
integer li_i, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
			if ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i = 1 to len(ls_cod_linea_prodotto)
					ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_tipi_lastre
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_linea_prodotto like :ls_cod_linea or 
							cod_linea_prodotto = '' or
							cod_linea_prodotto is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Tipi Lastre", "Errore nell'Estrazione Tipo di Lastra")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Tipi Lastre", "Valore di Default già Inserito per questa Linea di Prodotti")
						return 2
					end if
				next
			else
			  select count(*)
				 into :li_count
				 from tab_tipi_lastre
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Tipi Lastre", "Errore nell'Estrazione Tipo di Lastra")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Tipi Lastre", "Valore di Default già Inserito")
					return 2
				end if
			end if
		end if
	case "cod_linea_prodotto"
		this.setitem(row, "flag_default", "N")
end choose
end event

