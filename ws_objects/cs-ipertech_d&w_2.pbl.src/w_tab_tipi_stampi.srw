﻿$PBExportHeader$w_tab_tipi_stampi.srw
$PBExportComments$Finestra Tabella Tipi Stampi
forward
global type w_tab_tipi_stampi from w_cs_xx_principale
end type
type dw_tab_tipi_stampi_lista from uo_cs_xx_dw within w_tab_tipi_stampi
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_tipi_stampi
end type
type dw_tab_tipi_stampi_det from uo_cs_xx_dw within w_tab_tipi_stampi
end type
end forward

global type w_tab_tipi_stampi from w_cs_xx_principale
int Width=3201
int Height=1185
boolean TitleBar=true
string Title="Tabella Tipi Stampi"
dw_tab_tipi_stampi_lista dw_tab_tipi_stampi_lista
cb_note_esterne cb_note_esterne
dw_tab_tipi_stampi_det dw_tab_tipi_stampi_det
end type
global w_tab_tipi_stampi w_tab_tipi_stampi

on w_tab_tipi_stampi.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_tipi_stampi_lista=create dw_tab_tipi_stampi_lista
this.cb_note_esterne=create cb_note_esterne
this.dw_tab_tipi_stampi_det=create dw_tab_tipi_stampi_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_tipi_stampi_lista
this.Control[iCurrent+2]=cb_note_esterne
this.Control[iCurrent+3]=dw_tab_tipi_stampi_det
end on

on w_tab_tipi_stampi.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_tipi_stampi_lista)
destroy(this.cb_note_esterne)
destroy(this.dw_tab_tipi_stampi_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_tipi_stampi_lista.set_dw_key("cod_azienda")
dw_tab_tipi_stampi_lista.set_dw_key("cod_mat_sis_stampaggio")
dw_tab_tipi_stampi_lista.set_dw_key("cod_tipo_stampo")
dw_tab_tipi_stampi_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_tipi_stampi_det.set_dw_options(sqlca,dw_tab_tipi_stampi_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_tipi_stampi_lista
end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_tipi_stampi_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_tipi_stampi_lista.GetItemstring(li_row,"cod_mat_sis_stampaggio"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_tipi_stampi_lista,"cod_tipo_stampo",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "' and cod_classe_merceolo like '%S%')")
					  
f_PO_LoadDDDW_DW(dw_tab_tipi_stampi_det,"cod_tipo_stampo",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_verniciatura where cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "' and cod_classe_merceolo like '%S%')")
end event

type dw_tab_tipi_stampi_lista from uo_cs_xx_dw within w_tab_tipi_stampi
int X=22
int Y=20
int Width=2728
int Height=500
int TabOrder=10
string DataObject="d_tab_tipi_stampi_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_mat_sis_stampaggio")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_mat_sis_stampaggio")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_tipi_stampi
event clicked pbm_bnclicked
int X=2763
int Y=20
int TabOrder=30
string Text="Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_tipo_stampo, ls_cod_mat_sis_stampaggio
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

ls_cod_mat_sis_stampaggio= dw_tab_tipi_stampi_lista.getitemstring(dw_tab_tipi_stampi_lista.getrow(), "cod_mat_sis_stampaggio")
ls_cod_tipo_stampo= dw_tab_tipi_stampi_lista.getitemstring(dw_tab_tipi_stampi_lista.getrow(), "cod_tipo_stampo")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_tipi_stampi
where      cod_azienda = :s_cs_xx.cod_azienda
and		  cod_mat_sis_stampaggio = :ls_cod_mat_sis_stampaggio
and		  cod_tipo_stampo = :ls_cod_tipo_stampo;

if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_tipi_stampi
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  cod_mat_sis_stampaggio = :ls_cod_mat_sis_stampaggio
	and		  cod_tipo_stampo = :ls_cod_tipo_stampo;

   commit;

end if

setpointer(arrow!)
end event

type dw_tab_tipi_stampi_det from uo_cs_xx_dw within w_tab_tipi_stampi
int X=22
int Y=541
int Width=3108
int Height=519
int TabOrder=20
string DataObject="d_tab_tipi_stampi_det"
BorderStyle BorderStyle=StyleRaised!
end type

event itemchanged;call super::itemchanged;string ls_cod_classe_merceolo, ls_cod_classe
integer li_i, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_classe_merceolo = this.getitemstring(row, "cod_classe_merceolo")
			if ls_cod_classe_merceolo <> "" and not isnull(ls_cod_classe_merceolo) then
				for li_i = 1 to len(ls_cod_classe_merceolo)
					ls_cod_classe = "%" + mid(ls_cod_classe_merceolo, li_i, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_tipi_stampi
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_classe_merceolo like :ls_cod_classe or 
							cod_classe_merceolo = '' or
							cod_classe_merceolo is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Tipi di Stampi", "Errore nell'Estrazione Tipo di Stampo")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Tipi di Stampi", "Valore di Default già Inserito per questa Classe Merceologica")
						return 2
					end if
				next
			else
			  select count(*)
				 into :li_count
				 from tab_tipi_stampi
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Tipi di Stampi", "Errore nell'Estrazione Tipo di Stampo")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Tipi di Stampi", "Valore di Default già Inserito")
					return 2
				end if
			end if
		end if
	case "cod_classe_merceolo"
		this.setitem(row, "flag_default", "N")
end choose
end event

