﻿$PBExportHeader$w_tab_tipi_supporto.srw
$PBExportComments$Finestra Tabella Tipi Supporto
forward
global type w_tab_tipi_supporto from w_cs_xx_principale
end type
type dw_tab_supporti_lista from uo_cs_xx_dw within w_tab_tipi_supporto
end type
type dw_tab_supporti_det from uo_cs_xx_dw within w_tab_tipi_supporto
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_tipi_supporto
end type
type cb_1 from commandbutton within w_tab_tipi_supporto
end type
end forward

global type w_tab_tipi_supporto from w_cs_xx_principale
integer width = 2469
integer height = 1180
string title = "Tabella Tipi Supporto"
dw_tab_supporti_lista dw_tab_supporti_lista
dw_tab_supporti_det dw_tab_supporti_det
cb_note_esterne cb_note_esterne
cb_1 cb_1
end type
global w_tab_tipi_supporto w_tab_tipi_supporto

on w_tab_tipi_supporto.create
int iCurrent
call super::create
this.dw_tab_supporti_lista=create dw_tab_supporti_lista
this.dw_tab_supporti_det=create dw_tab_supporti_det
this.cb_note_esterne=create cb_note_esterne
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_supporti_lista
this.Control[iCurrent+2]=this.dw_tab_supporti_det
this.Control[iCurrent+3]=this.cb_note_esterne
this.Control[iCurrent+4]=this.cb_1
end on

on w_tab_tipi_supporto.destroy
call super::destroy
destroy(this.dw_tab_supporti_lista)
destroy(this.dw_tab_supporti_det)
destroy(this.cb_note_esterne)
destroy(this.cb_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_supporti_lista.set_dw_key("cod_azienda")
dw_tab_supporti_lista.set_dw_key("cod_tipo_supporto")
dw_tab_supporti_lista.set_dw_key("cod_non_a_magazzino")
dw_tab_supporti_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_supporti_det.set_dw_options(sqlca,dw_tab_supporti_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_supporti_lista
end event

event pc_setddlb;call super::pc_setddlb;//f_PO_LoadDDDW_DW(dw_tab_supporti_det,"cod_tipo_supporto",sqlca,&
//                 "anag_prodotti","cod_prodotto","des_prodotto",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
//					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_tipi_supporto')")

f_PO_LoadDDDW_DW(dw_tab_supporti_det,"cod_gruppo_variante",sqlca,&
                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_supporti_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_supporti_lista.GetItemstring(li_row,"cod_tipo_supporto"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if


end event

event open;call super::open;dw_tab_supporti_det.object.b_ricerca_prodotto.enabled=false
end event

type dw_tab_supporti_lista from uo_cs_xx_dw within w_tab_tipi_supporto
integer x = 14
integer y = 16
integer width = 1934
integer height = 496
integer taborder = 10
string dataobject = "d_tab_supporti_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tipo_supporto")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
dw_tab_supporti_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
dw_tab_supporti_det.object.b_ricerca_prodotto.enabled=true
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_tipo_supporto")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
dw_tab_supporti_det.object.b_ricerca_prodotto.enabled=false
end event

type dw_tab_supporti_det from uo_cs_xx_dw within w_tab_tipi_supporto
integer x = 14
integer y = 528
integer width = 2400
integer height = 528
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_tab_supporti_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_linea_prodotto, ls_cod_linea
integer li_i, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
			if ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i = 1 to len(ls_cod_linea_prodotto)
					ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_tipi_supporto
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_linea_prodotto like :ls_cod_linea or 
							cod_linea_prodotto = '' or
							cod_linea_prodotto is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito per questa Linea di Prodotti")
						return 2
					end if
				next
			else
			  select count(*)
				 into :li_count
				 from tab_tipi_supporto
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito")
					return 2
				end if
			end if
		end if
	case "cod_linea_prodotto"
		this.setitem(row, "flag_default", "N")
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_tab_supporti_det,"cod_tipo_supporto")
end choose
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_tipi_supporto
event clicked pbm_bnclicked
integer x = 1970
integer y = 16
integer width = 352
integer height = 80
integer taborder = 40
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_tipo_supporto
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

ls_cod_tipo_supporto= dw_tab_supporti_lista.getitemstring(dw_tab_supporti_lista.getrow(), "cod_tipo_supporto")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_tipi_supporto
where      cod_azienda = :s_cs_xx.cod_azienda
and		  cod_tipo_supporto = :ls_cod_tipo_supporto;

if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_tipi_supporto
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  cod_tipo_supporto = :ls_cod_tipo_supporto;
   commit;

end if

setpointer(arrow!)
end event

type cb_1 from commandbutton within w_tab_tipi_supporto
integer x = 1966
integer y = 120
integer width = 352
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Mod.Vern."
end type

event clicked;window_open_parm(w_modelli_tipi_supp_vernic, -1, dw_tab_supporti_lista)
end event

