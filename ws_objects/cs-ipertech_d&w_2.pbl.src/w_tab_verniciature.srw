﻿$PBExportHeader$w_tab_verniciature.srw
$PBExportComments$Finestra Tabella verniciatura
forward
global type w_tab_verniciature from w_cs_xx_principale
end type
type dw_tab_verniciature_lista from uo_cs_xx_dw within w_tab_verniciature
end type
type cb_note_esterne from cb_documenti_compilati within w_tab_verniciature
end type
type dw_tab_verniciature_det from uo_cs_xx_dw within w_tab_verniciature
end type
end forward

global type w_tab_verniciature from w_cs_xx_principale
integer width = 2405
integer height = 1516
string title = "Verniciature"
dw_tab_verniciature_lista dw_tab_verniciature_lista
cb_note_esterne cb_note_esterne
dw_tab_verniciature_det dw_tab_verniciature_det
end type
global w_tab_verniciature w_tab_verniciature

on w_tab_verniciature.create
int iCurrent
call super::create
this.dw_tab_verniciature_lista=create dw_tab_verniciature_lista
this.cb_note_esterne=create cb_note_esterne
this.dw_tab_verniciature_det=create dw_tab_verniciature_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_verniciature_lista
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.dw_tab_verniciature_det
end on

on w_tab_verniciature.destroy
call super::destroy
destroy(this.dw_tab_verniciature_lista)
destroy(this.cb_note_esterne)
destroy(this.dw_tab_verniciature_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_verniciature_lista.set_dw_key("cod_azienda")
dw_tab_verniciature_lista.set_dw_key("cod_verniciatura")
dw_tab_verniciature_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
dw_tab_verniciature_det.set_dw_options(sqlca,dw_tab_verniciature_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_tab_verniciature_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_tab_verniciature_det,"cod_verniciatura",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_verniciatura')")

//f_PO_LoadDDDW_DW(dw_tab_verniciature_det,"cod_gruppo_variante",sqlca,&
//                 "gruppi_varianti","cod_gruppo_variante","des_gruppo_variante",&
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event pc_delete;call super::pc_delete;integer li_row
li_row = dw_tab_verniciature_lista.getrow()
if not(li_row > 0 and not isnull(dw_tab_verniciature_lista.GetItemstring(li_row,"cod_verniciatura"))) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

type dw_tab_verniciature_lista from uo_cs_xx_dw within w_tab_verniciature
integer x = 18
integer y = 16
integer width = 2318
integer height = 496
integer taborder = 20
string dataobject = "d_tab_verniciature_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_verniciatura")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

event pcd_new;call super::pcd_new;cb_note_esterne.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_verniciatura")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
end event

type cb_note_esterne from cb_documenti_compilati within w_tab_verniciature
event clicked pbm_bnclicked
integer x = 1984
integer y = 1312
integer width = 357
integer height = 80
integer taborder = 30
string text = "Docu&mento"
end type

event clicked;call super::clicked;string ls_cod_verniciatura
string ls_cod_blob
blob lbl_null

setnull(lbl_null)

ls_cod_verniciatura= dw_tab_verniciature_lista.getitemstring(dw_tab_verniciature_lista.getrow(), "cod_verniciatura")

s_cs_xx.parametri.parametro_bl_1 = lbl_null

selectblob note_esterne
into       :s_cs_xx.parametri.parametro_bl_1
from       tab_verniciatura
where      cod_azienda = :s_cs_xx.cod_azienda
and		  cod_verniciatura = :ls_cod_verniciatura;

if sqlca.sqlcode <> 0 then
   s_cs_xx.parametri.parametro_bl_1 = lbl_null
end if

window_open(w_ole, 0)
setpointer(hourglass!)
if not isnull(s_cs_xx.parametri.parametro_bl_1) then
   updateblob tab_verniciatura
   set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
	where      cod_azienda = :s_cs_xx.cod_azienda
	and		  cod_verniciatura = :ls_cod_verniciatura;
   commit;

end if

setpointer(arrow!)
end event

event getfocus;call super::getfocus;long ll_rgb
ll_rgb = dw_tab_verniciature_det.getitemnumber(dw_tab_verniciature_det.getrow(), "valore_rgb")
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "valore_rgb"
s_cs_xx.parametri.parametro_ul_1 = ll_rgb
end event

type dw_tab_verniciature_det from uo_cs_xx_dw within w_tab_verniciature
integer x = 18
integer y = 528
integer width = 2327
integer height = 768
integer taborder = 10
string dataobject = "d_tab_verniciature_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_cod_linea_prodotto, ls_cod_linea, ls_cod_classe_merceolo, ls_cod_classe
integer li_i_linea, li_i_classe, li_count

choose case i_colname
	case "flag_default"
		if data = "S" then
			ls_cod_linea_prodotto = this.getitemstring(row, "cod_linea_prodotto")
			ls_cod_classe_merceolo = this.getitemstring(row, "cod_classe_merceolo")
			if ls_cod_classe_merceolo <> "" and not isnull(ls_cod_classe_merceolo) and &
				ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i_classe = 1 to len(ls_cod_classe_merceolo)
					ls_cod_classe = "%" + mid(ls_cod_classe_merceolo, li_i_classe, 1) + "%"
					for li_i_linea = 1 to len(ls_cod_linea_prodotto)
						ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i_linea, 1) + "%"
						
					  select count(*)
						 into :li_count
						 from tab_verniciatura
						where cod_azienda = :s_cs_xx.cod_azienda and
								(cod_linea_prodotto like :ls_cod_linea or 
								cod_linea_prodotto = '' or
								cod_linea_prodotto is null) and
								(cod_classe_merceolo like :ls_cod_classe or 
								cod_classe_merceolo = '' or
								cod_classe_merceolo is null) and
								flag_default = 'S';
		
						if sqlca.sqlcode = -1 then
							g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
							return 2
						end if
						if li_count > 0 then
							g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito per questa Linea-Classe di Prodotti")
							return 2
						end if
					next // Linea Prodotto
				next // Classe Merceolo
			elseif (ls_cod_classe_merceolo = "" or isnull(ls_cod_classe_merceolo)) and &
				ls_cod_linea_prodotto <> "" and not isnull(ls_cod_linea_prodotto) then
				for li_i_linea = 1 to len(ls_cod_linea_prodotto)
					ls_cod_linea = "%" + mid(ls_cod_linea_prodotto, li_i_linea, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_verniciatura
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_linea_prodotto like :ls_cod_linea or 
							cod_linea_prodotto = '' or
							cod_linea_prodotto is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito per questa Linea di Prodotti")
						return 2
					end if
				next // Linea Prodotto
			elseif ls_cod_classe_merceolo <> "" and not isnull(ls_cod_classe_merceolo) and &
				(ls_cod_linea_prodotto = "" or isnull(ls_cod_linea_prodotto)) then
				for li_i_classe = 1 to len(ls_cod_classe_merceolo)
					ls_cod_classe = "%" + mid(ls_cod_classe_merceolo, li_i_classe, 1) + "%"
					
				  select count(*)
					 into :li_count
					 from tab_verniciatura
					where cod_azienda = :s_cs_xx.cod_azienda and
							(cod_classe_merceolo like :ls_cod_classe or 
							cod_classe_merceolo = '' or
							cod_classe_merceolo is null) and
							flag_default = 'S';
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
						return 2
					end if
					if li_count > 0 then
						g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito per questa Merceologia di Prodotti")
						return 2
					end if
				next // Classe Merceolo
			else
			  select count(*)
				 into :li_count
				 from tab_verniciatura
				where cod_azienda = :s_cs_xx.cod_azienda and
						flag_default = 'S';

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Tabella Supporti", "Errore nell'Estrazione Supporto")
					return 2
				end if
				if li_count > 0 then
					g_mb.messagebox("Tabella Supporti", "Valore di Default già Inserito")
					return 2
				end if
			end if
		end if
	case "cod_linea_prodotto"
		this.setitem(row, "flag_default", "N")
	case "cod_classe_merceolo"
		this.setitem(row, "flag_default", "N")
end choose
end event

