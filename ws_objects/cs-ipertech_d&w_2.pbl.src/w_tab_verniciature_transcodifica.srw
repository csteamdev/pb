﻿$PBExportHeader$w_tab_verniciature_transcodifica.srw
$PBExportComments$Finestra Tabella verniciatura
forward
global type w_tab_verniciature_transcodifica from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_tab_verniciature_transcodifica
end type
end forward

global type w_tab_verniciature_transcodifica from w_cs_xx_principale
integer width = 3502
integer height = 1968
string title = "Verniciature"
dw_lista dw_lista
end type
global w_tab_verniciature_transcodifica w_tab_verniciature_transcodifica

on w_tab_verniciature_transcodifica.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_tab_verniciature_transcodifica.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;dw_lista.set_dw_key("cod_azienda")
dw_lista.set_dw_key("cod_verniciatura")
dw_lista.set_dw_options(sqlca,pcca.null_object,c_default,c_default)

iuo_dw_main = dw_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_lista,"cod_verniciatura",sqlca,&
                 "anag_prodotti","cod_prodotto","des_prodotto",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer "+&
					  																				"from tab_cat_mer_tabelle "+&
																									  "where cod_azienda = '" +s_cs_xx.cod_azienda + "' and "+&
												  						" nome_tabella = 'tab_verniciatura') and "+&
																		  "cod_prodotto in (select cod_verniciatura "+&
																		  "from tab_verniciatura "+&
																		  "where cod_azienda = '" +s_cs_xx.cod_azienda+"') " )
end event

type dw_lista from uo_cs_xx_dw within w_tab_verniciature_transcodifica
integer x = 18
integer y = 16
integer width = 3419
integer height = 1808
integer taborder = 20
string dataobject = "d_tab_verniciatura_trascodifica"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event buttonclicked;call super::buttonclicked;if row>0 then
else
	return
end if

choose case dwo.name
	case "b_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_lista, "cod_fornitore")
		
end choose
end event

event itemchanged;call super::itemchanged;string ls_temp

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_fornitore"
		if data<>"" then
			select cod_azienda
			into :ls_temp
			from anag_fornitori
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_fornitore=:data;
			
			if ls_temp="" or isnull(ls_temp) then
				g_mb.error("Il fornitore "+data+" è inesistente!")
				
				return 2
			end if
			
		end if
		
		
end choose
end event

event pcd_delete;call super::pcd_delete;object.b_fornitore.enabled = false
end event

event pcd_modify;call super::pcd_modify;object.b_fornitore.enabled = false	//infatti il campo fa parte della PK
end event

event pcd_new;call super::pcd_new;object.b_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;object.b_fornitore.enabled = false
end event

