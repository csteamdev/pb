﻿$PBExportHeader$w_tipi_unione_tessuti.srw
$PBExportComments$Finestra Tabella Tipi Lastre
forward
global type w_tipi_unione_tessuti from w_cs_xx_principale
end type
type dw_tipi_unione_tessuti from uo_cs_xx_dw within w_tipi_unione_tessuti
end type
end forward

global type w_tipi_unione_tessuti from w_cs_xx_principale
integer height = 1220
string title = "Tabella Tipi Lastre"
dw_tipi_unione_tessuti dw_tipi_unione_tessuti
end type
global w_tipi_unione_tessuti w_tipi_unione_tessuti

on w_tipi_unione_tessuti.create
int iCurrent
call super::create
this.dw_tipi_unione_tessuti=create dw_tipi_unione_tessuti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_unione_tessuti
end on

on w_tipi_unione_tessuti.destroy
call super::destroy
destroy(this.dw_tipi_unione_tessuti)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_unione_tessuti.set_dw_key("cod_azienda")
dw_tipi_unione_tessuti.set_dw_key("cod_tipo_lastra")
dw_tipi_unione_tessuti.set_dw_options(sqlca, pcca.null_object, c_default,c_default)

iuo_dw_main = dw_tipi_unione_tessuti
end event

type dw_tipi_unione_tessuti from uo_cs_xx_dw within w_tipi_unione_tessuti
integer x = 23
integer width = 1966
integer height = 1040
integer taborder = 20
string dataobject = "d_tab_tipi_unione_tessuti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

