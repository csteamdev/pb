﻿$PBExportHeader$uo_axioma.sru
forward
global type uo_axioma from nonvisualobject
end type
end forward

global type uo_axioma from nonvisualobject
end type
global uo_axioma uo_axioma

type variables
// Questa variabile booleana è molto importante perchè stabilisce se l'importazione è nuova oppure se è la modifica di una riga importata in precedenza.
boolean 	ib_nuova_configurazione=true
string 	is_log_errori
wstr_riepilogo istr_riepilogo
wstr_flags istr_flags

end variables

forward prototypes
public subroutine uof_log (string as_message)
public function integer uof_get_comp_det_tostring (string as_nome_valore_cercato, datawindow adw_varianti, ref string as_valore)
public function integer uof_get_comp_det_todecimal (string as_nome_valore_cercato, datawindow adw_varianti, ref decimal ad_valore)
public function integer uof_crea_comp_det (datawindow adw_varianti, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_gestione)
public function integer uof_exist_prodotto (string as_cod_variabile, string as_cod_prodotto)
public function integer uof_importa_riga_documento (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_cod_prodotto_finito, string as_path_file_pdf, ref datawindow adw_dettaglio, string as_tipo_gestione)
public function integer uof_get_comp_det_quantita (string as_nome_elemento_cercato, datawindow adw_varianti, ref decimal ad_valore)
public function integer uof_inserisci_optional (ref datawindow adw_varianti, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto_finito, string as_tipo_gestione)
public function long uof_inserisci_varianti_from_axioma (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_gestione, string as_cod_prodotto_finito, string as_cod_versione, ref datawindow adw_varianti, ref string as_errore)
public function long uof_inserisci_varianti_from_apice (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_gestione, string as_cod_prodotto_finito, string as_cod_versione, ref datawindow adw_varianti, ref string as_errore)
public function integer uof_calcolo_prezzo (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_riga, string as_tipo_gestione)
public function decimal uof_string_to_number (string as_string)
public function integer uof_fat_conversione (string as_cod_prodotto, decimal ad_quan_vendita, decimal ad_prezzo_vendita, string as_cod_misura_mag, string as_cod_misura_vendita_um, ref decimal ad_quan_vendita_um, ref decimal ad_prezzo_vendita_um, ref decimal ad_fat_conversione_ven, ref string as_errore)
public function integer uof_leggi_xml (string as_filename, ref string as_xml)
public function integer uof_parse_xml (string as_filename, ref datastore as_dw_det)
public function integer uof_salva_xml (long al_xml_id, ref transaction at_axioma, ref datastore adw_det)
public function long uof_crea_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, decimal ad_quantita, string as_cod_prodotto_finito, decimal ad_dim_x, decimal ad_dim_y, string as_tipo_riga, string as_tipo_gestione, string as_nota_dettaglio)
public function integer uof_get_nota_dettaglio (string as_nome_valore_cercato, datawindow adw_varianti, ref string as_valore)
public function integer uof_calcola_peso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_importa_blob (string as_path, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_gestione, ref transaction at_tran)
end prototypes

public subroutine uof_log (string as_message);if len(is_log_errori) > 0 then
	is_log_errori = is_log_errori + "~r~n" + string(today(), "dd/mm/yyyy") + "~t" + string(now(), "hh:mm:ss") + "~t" + as_message
else
	is_log_errori = string(today(), "dd/mm/yyyy") + "~t" + string(now(), "hh:mm:ss") + "~t" + as_message
end if
end subroutine

public function integer uof_get_comp_det_tostring (string as_nome_valore_cercato, datawindow adw_varianti, ref string as_valore);string ls_find
long ll_i,ll_row

ls_find = "upper(apc_gruppo_variante) = 'COMP_DET' and upper(apc_specifica_1) = '" + upper(as_nome_valore_cercato) + "'"
ll_row = adw_varianti.Find (ls_find  , 1, adw_varianti.rowcount() )

if ll_row < 0 then 
	uof_log( g_str.format("Errore nella ricerca del valore " + as_nome_valore_cercato + " nelle specifiche del dettaglio complementare" , sqlca.sqlerrtext) )
	return -2
elseif ll_row = 0 then
	uof_log( g_str.format("La variabile " + as_nome_valore_cercato + "  del dettaglio complementare non è stata valorizzata." , sqlca.sqlerrtext) )
	return -2
else
	as_valore = adw_varianti.getitemstring(ll_row, "apc_specifica_2")
	if as_valore = "**" then setnull(as_valore)
end if

return 0
end function

public function integer uof_get_comp_det_todecimal (string as_nome_valore_cercato, datawindow adw_varianti, ref decimal ad_valore);string ls_find, ls_valore
long ll_i,ll_row

ls_find = "upper(apc_gruppo_variante) = 'COMP_DET' and upper(apc_specifica_1) = '" + upper(as_nome_valore_cercato) + "'"
ll_row = adw_varianti.Find (ls_find  , 1, adw_varianti.rowcount() )

if ll_row < 0 then 
	uof_log( g_str.format("Errore nella ricerca del valore " + as_nome_valore_cercato + " nelle specifiche del dettaglio complementare" , sqlca.sqlerrtext) )
	return -2
elseif ll_row = 0 then
	uof_log( g_str.format("La variabile " + as_nome_valore_cercato + "  del dettaglio complementare non è stata valorizzata." , sqlca.sqlerrtext) )
	return -2
else
	ls_valore = adw_varianti.getitemstring(ll_row, "apc_specifica_2")
	if ls_valore = "**"   then
		// questo in Axioma vuol dire che è rimasto il default; 
		// nel caso del numerico se default restituisco il valore -1 per segnalare l'errore
		return -1
	end if
	guo_functions.uof_replace_string( ls_valore, ".", ",")
	ad_valore = dec(ls_valore)
end if

return 0
end function

public function integer uof_crea_comp_det (datawindow adw_varianti, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_gestione);string ls_flag_addizionale_tessuto, ls_cod_modello,ls_flag_tessuto_cliente,ls_cod_verniciatura_3,ls_cod_comando,ls_pos_comando,ls_cod_tipo_supporto, &
		ls_flag_addizionale_supporto, ls_flag_misura_luce_finita, ls_flag_allegati, ls_cod_tessuto, ls_colore_tessuto, ls_flag_tipo_mantovana, &
		ls_flag_fc_tessuto, ls_flag_fc_vernic, ls_des_vernice_fc, ls_cod_verniciatura,ls_flag_addizionale_vernic, ls_cod_verniciatura_2, &
		ls_flag_fc_vernic_2,ls_des_vernice_fc_2, ls_flag_addizionale_vernic_2,ls_flag_addizionale_vernic_3, ls_flag_fc_vernic_3, &
		ls_des_vernice_fc_3, ls_des_comando_1,ls_flag_addizionale_comando_1,ls_flag_fc_comando_1,ls_cod_comando_2,ls_des_comando_2,&
		ls_flag_fc_comando_2,ls_flag_addizionale_comando_2, ls_flag_fc_supporto,ls_flag_passamaneria, ls_flag_cordolo,flag_addizionale_tessuto, &
		flag_tessuto_cliente, flag_tessuto_mant_cliente,ls_cod_tessuto_mant, ls_flag_tessuto_mant_cliente, ls_cod_mant_non_a_magazzino, ls_str, &
		ls_cod_prod_finito, ls_colore_passameneria, ls_colore_cordolo, ls_cod_versione, ls_cod_addizionale[], ls_errore

long ll_i, ll_ret

dec{4} ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_alt_mantovana,ld_alt_asta, ld_quan_addizionale



ls_flag_allegati = "S"
ls_flag_passamaneria = "N"
ls_flag_cordolo = "N"
setnull(ls_colore_passameneria)
setnull(ls_colore_cordolo)

uof_get_comp_det_tostring( "cod_modello", adw_varianti, ref ls_cod_modello)
uof_exist_prodotto("cod_modello", ls_cod_modello)

uof_get_comp_det_tostring( "cod_tessuto", adw_varianti, ref ls_cod_tessuto)
uof_exist_prodotto("cod_tessuto", ls_cod_tessuto)

uof_get_comp_det_tostring( "cod_non_a_magazzino", adw_varianti, ref ls_colore_tessuto)
uof_get_comp_det_tostring( "flag_misura_luce_finita", adw_varianti, ref ls_flag_misura_luce_finita)
if isnull(ls_flag_misura_luce_finita) or len(ls_flag_misura_luce_finita) < 1 then ls_flag_misura_luce_finita = "F"

uof_get_comp_det_tostring( "flag_tipo_mantovana", adw_varianti, ref ls_flag_tipo_mantovana)
if isnull(ls_flag_tipo_mantovana) or len(ls_flag_tipo_mantovana) < 1 or ls_flag_tipo_mantovana > "F"  then ls_flag_tipo_mantovana = "A"

uof_get_comp_det_tostring( "flag_fc_tessuto", adw_varianti, ref ls_flag_fc_tessuto)
if isnull(ls_flag_fc_tessuto) or len(ls_flag_fc_tessuto) < 1 then ls_flag_fc_tessuto = "N"

uof_get_comp_det_tostring( "flag_tessuto_cliente", adw_varianti, ref ls_flag_tessuto_cliente)
if isnull(ls_flag_tessuto_cliente) or len(ls_flag_tessuto_cliente) < 1 then ls_flag_tessuto_cliente = "N"

uof_get_comp_det_tostring( "cod_verniciatura", adw_varianti, ref ls_cod_verniciatura)
uof_exist_prodotto("cod_verniciatura", ls_cod_verniciatura)

uof_get_comp_det_tostring( "flag_fc_vernic", adw_varianti, ref ls_flag_fc_vernic)
if isnull(ls_flag_fc_vernic) or len(ls_flag_fc_vernic) < 1 then ls_flag_fc_vernic = "N"

uof_get_comp_det_tostring( "des_vernice_fc", adw_varianti, ref ls_des_vernice_fc)
uof_get_comp_det_tostring( "flag_addizionale_vernic", adw_varianti, ref ls_flag_addizionale_vernic)
if isnull(ls_flag_addizionale_vernic) or len(ls_flag_addizionale_vernic) < 1 then ls_flag_addizionale_vernic = "N"

uof_get_comp_det_tostring( "cod_verniciatura_2", adw_varianti, ref ls_cod_verniciatura_2)
uof_exist_prodotto("cod_verniciatura_2", ls_cod_verniciatura_2)

uof_get_comp_det_tostring( "flag_fc_vernic_2", adw_varianti, ref ls_flag_fc_vernic_2)
if isnull(ls_flag_fc_vernic_2) or len(ls_flag_fc_vernic_2) < 1 then ls_flag_fc_vernic_2 = "N"

uof_get_comp_det_tostring( "des_vernice_fc_2", adw_varianti, ref ls_des_vernice_fc_2)
uof_get_comp_det_tostring( "flag_addizionale_vernic_2", adw_varianti, ref ls_flag_addizionale_vernic_2)
if isnull(ls_flag_addizionale_vernic_2) or len(ls_flag_addizionale_vernic_2) < 1 then ls_flag_addizionale_vernic_2 = "N"

uof_get_comp_det_tostring( "cod_verniciatura_3", adw_varianti, ref ls_cod_verniciatura_3)
uof_exist_prodotto("cod_verniciatura_3", ls_cod_verniciatura_3)

uof_get_comp_det_tostring( "flag_fc_vernic_3", adw_varianti, ref ls_flag_fc_vernic_3)
if isnull(ls_flag_fc_vernic_3) or len(ls_flag_fc_vernic_3) < 1 then ls_flag_fc_vernic_3 = "N"

uof_get_comp_det_tostring( "des_vernice_fc_3", adw_varianti, ref ls_des_vernice_fc_3)
uof_get_comp_det_tostring( "flag_addizionale_vernic_3", adw_varianti, ref ls_flag_addizionale_vernic_3)
if isnull(ls_flag_addizionale_vernic_3) or len(ls_flag_addizionale_vernic_3) < 1 then ls_flag_addizionale_vernic_3 = "N"

uof_get_comp_det_tostring( "cod_comando", adw_varianti, ref ls_cod_comando)
uof_exist_prodotto("cod_comando", ls_cod_comando)

uof_get_comp_det_tostring( "des_comando_1", adw_varianti, ref ls_des_comando_1)
uof_get_comp_det_tostring( "pos_comando", adw_varianti, ref ls_pos_comando)
uof_get_comp_det_tostring( "flag_addizionale_comando_1", adw_varianti, ref ls_flag_addizionale_comando_1)
if isnull(ls_flag_addizionale_comando_1) or len(ls_flag_addizionale_comando_1) < 1 then ls_flag_addizionale_comando_1 = "N"

uof_get_comp_det_tostring( "flag_fc_comando_1", adw_varianti, ref ls_flag_fc_comando_1)
if isnull(ls_flag_fc_comando_1) or len(ls_flag_fc_comando_1) < 1 then ls_flag_fc_comando_1 = "N"

uof_get_comp_det_tostring( "cod_comando_2", adw_varianti, ref ls_cod_comando_2)
uof_exist_prodotto("cod_comando_2", ls_cod_comando_2)

uof_get_comp_det_tostring( "des_comando_2", adw_varianti, ref ls_des_comando_2)

uof_get_comp_det_tostring( "flag_addizionale_comando_2", adw_varianti, ref ls_flag_addizionale_comando_2)
if isnull(ls_flag_addizionale_comando_2) or len(ls_flag_addizionale_comando_2) < 1 then ls_flag_addizionale_comando_2 = "N"

uof_get_comp_det_tostring( "flag_fc_comando_2", adw_varianti, ref ls_flag_fc_comando_2)
if isnull(ls_flag_fc_comando_2) or len(ls_flag_fc_comando_2) < 1 then ls_flag_fc_comando_2 = "N"

uof_get_comp_det_tostring( "cod_tipo_supporto", adw_varianti, ref ls_cod_tipo_supporto)
uof_exist_prodotto("cod_tipo_supporto", ls_cod_tipo_supporto)

uof_get_comp_det_tostring( "flag_fc_supporto", adw_varianti, ref ls_flag_fc_supporto)
uof_get_comp_det_tostring( "flag_addizionale_supporto", adw_varianti, ref ls_flag_addizionale_supporto)
if isnull(ls_flag_addizionale_supporto) or len(ls_flag_addizionale_supporto) < 1 then ls_flag_addizionale_supporto = "N"

uof_get_comp_det_tostring( "flag_addizionale_tessuto", adw_varianti, ref ls_flag_addizionale_tessuto)
if isnull(ls_flag_addizionale_tessuto) or len(ls_flag_addizionale_tessuto) < 1 then ls_flag_addizionale_tessuto = "N"

uof_get_comp_det_tostring( "cod_tessuto_mant", adw_varianti, ref ls_cod_tessuto_mant)
uof_exist_prodotto("cod_tessuto_mant", ls_cod_tessuto_mant)

uof_get_comp_det_tostring( "cod_mant_non_a_magazzino", adw_varianti, ref ls_cod_mant_non_a_magazzino)
uof_get_comp_det_tostring( "flag_tessuto_mant_cliente", adw_varianti, ref ls_flag_tessuto_mant_cliente)
if isnull(ls_flag_tessuto_mant_cliente) or len(ls_flag_tessuto_mant_cliente) < 1 then ls_flag_tessuto_mant_cliente = "N"

if uof_get_comp_det_todecimal( "alt_asta", adw_varianti, ref ld_alt_asta) = -1 then
	uof_log("La variabile alt_asta del dettaglio complmentare non è stata valorizzata")
	ld_alt_asta = 0 
end if

if uof_get_comp_det_todecimal( "dim_x", adw_varianti, ref ld_dim_x) = -1 then
	uof_log("La variabile dim_x del dettaglio complmentare non è stata valorizzata")
	ld_dim_x = 0 
end if

if uof_get_comp_det_todecimal( "dim_y", adw_varianti, ref ld_dim_y) = -1 then
	uof_log("La variabile dim_y del dettaglio complmentare non è stata valorizzata")
	ld_dim_y = 0 
end if

if uof_get_comp_det_todecimal( "dim_z", adw_varianti, ref ld_dim_z) = -1 then
	uof_log("La variabile dim_z del dettaglio complmentare non è stata valorizzata")
	ld_dim_z = 0 
end if
	
if uof_get_comp_det_todecimal( "dim_t", adw_varianti, ref ld_dim_t) = -1 then
	uof_log("La variabile dim_t del dettaglio complmentare non è stata valorizzata")
	ld_dim_t = 0 
end if
	
if uof_get_comp_det_todecimal( "alt_mantovana", adw_varianti, ref ld_alt_mantovana) = -1 then
	uof_log("La variabile alt_mantovana del dettaglio complmentare non è stata valorizzata")
	ld_alt_mantovana = 0 
end if

uo_set_default luo_set_default
luo_set_default = create uo_set_default
if luo_set_default.uof_init_codice_prodotto( ls_cod_modello,ld_dim_x, ls_colore_tessuto, ls_cod_tessuto, ls_cod_comando, ls_cod_verniciatura, ref ls_cod_prod_finito, ref ls_errore) < 0 then
	uof_log("Errore nella generazione del codice prodotto autocomposto. " + ls_errore)
end if
destroy luo_set_default




choose case as_tipo_gestione
	case "ORD_VEN"

		INSERT INTO comp_det_ord_ven  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  prog_riga_ord_ven,   
					  cod_prod_finito,   
					  cod_modello,   
					  dim_x,   
					  dim_y,   
					  dim_z,   
					  dim_t,   
					  cod_tessuto,   
					  cod_non_a_magazzino,   
					  cod_tessuto_mant,   
					  cod_mant_non_a_magazzino,   
					  flag_tipo_mantovana,   
					  alt_mantovana,   
					  flag_cordolo,   
					  flag_passamaneria,   
					  cod_verniciatura,   
					  cod_comando,   
					  pos_comando,   
					  alt_asta,   
					  cod_comando_tenda_verticale,   
					  cod_colore_lastra,   
					  cod_tipo_lastra,   
					  spes_lastra,   
					  cod_tipo_supporto,   
					  flag_cappotta_frontale,   
					  flag_rollo,   
					  flag_kit_laterale,   
					  flag_misura_luce_finita,   
					  cod_tipo_stampo,   
					  cod_mat_sis_stampaggio,   
					  rev_stampo,   
					  num_gambe,   
					  num_campate,   
					  note,
					  flag_autoblocco,   
					  num_fili_plisse,   
					  num_boccole_plisse,   
					  intervallo_punzoni_plisse,   
					  num_pieghe_plisse,   
					  prof_inf_plisse,   
					  prof_sup_plisse,   
					  colore_passamaneria,   
					  colore_cordolo,   
					  num_punzoni_plisse,   
					  des_vernice_fc,   
					  des_comando_1,   
					  cod_comando_2,   
					  des_comando_2,   
					  flag_addizionale_comando_1,   
					  flag_addizionale_comando_2,   
					  flag_addizionale_supporto,   
					  flag_addizionale_tessuto,   
					  flag_addizionale_vernic,   
					  flag_fc_comando_1,   
					  flag_fc_comando_2,   
					  flag_fc_supporto,   
					  flag_fc_tessuto,   
					  flag_fc_vernic,   
					  flag_allegati,   
					  data_ora_ar,   
					  flag_tessuto_cliente,   
					  flag_tessuto_mant_cliente,   
					  cod_verniciatura_2,   
					  flag_addizionale_vernic_2,   
					  flag_fc_vernic_2,   
					  des_vernic_fc_2,   
					  cod_verniciatura_3,   
					  flag_addizionale_vernic_3,   
					  flag_fc_vernic_3,   
					  des_vernic_fc_3 )  
		VALUES ( :s_cs_xx.cod_azienda,   
					  :al_anno_registrazione,   
					  :al_num_registrazione,   
					  :al_prog_riga,   
					  :ls_cod_prod_finito,   
					  :ls_cod_modello,   
					  :ld_dim_x,   
					  :ld_dim_y,   
					  :ld_dim_z,   
					  :ld_dim_t,   
					  :ls_cod_tessuto,   
					  :ls_colore_tessuto,   
					  :ls_cod_tessuto_mant,   
					  :ls_cod_mant_non_a_magazzino,   
					  :ls_flag_tipo_mantovana,   
					  :ld_alt_mantovana,   
					  :ls_flag_cordolo,   
					  :ls_flag_passamaneria,   
					  :ls_cod_verniciatura,   
					  :ls_cod_comando,   
					  :ls_pos_comando,   
					  :ld_alt_asta,   
					  null,   
					  null,   
					  null,   
					  '0',   
					  :ls_cod_tipo_supporto,   
					  'N',   
					  'N',   
					  'N',   
					  :ls_flag_misura_luce_finita,   
					  null,   
					  null,   
					  '0',   
					  0,   
					  0,   
					  null,   
					  'N',   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  :ls_colore_passameneria,   
					  :ls_colore_cordolo,   
					  0,   
					  :ls_des_vernice_fc,   
					  :ls_des_comando_1,   
					  :ls_cod_comando_2,   
					  :ls_des_comando_2,   
					  :ls_flag_addizionale_comando_1,   
					  :ls_flag_addizionale_comando_2,   
					  :ls_flag_addizionale_supporto,   
					  :ls_flag_addizionale_tessuto,   
					  :ls_flag_addizionale_vernic,   
					  :ls_flag_fc_comando_1,   
					  :ls_flag_fc_comando_2,   
					  :ls_flag_fc_supporto,   
					  :ls_flag_fc_tessuto,   
					  :ls_flag_fc_vernic,   
					  :ls_flag_allegati,   
					  null,   
					  :ls_flag_tessuto_cliente,   
					  :ls_flag_tessuto_mant_cliente,   
					  :ls_cod_verniciatura_2,   
					  :ls_flag_addizionale_vernic_2,   
					  :ls_flag_fc_vernic_2,   
					  :ls_des_vernice_fc_2,   
					  :ls_cod_verniciatura_3,   
					  :ls_flag_addizionale_vernic_3,   
					  :ls_flag_fc_vernic_3,   
					  :ls_des_vernice_fc_3 )  ;

			  
	case "OFF_VEN"
		
		INSERT INTO comp_det_off_ven  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  prog_riga_off_ven,   
					  cod_prod_finito,   
					  cod_modello,   
					  dim_x,   
					  dim_y,   
					  dim_z,   
					  dim_t,   
					  cod_tessuto,   
					  cod_non_a_magazzino,   
					  cod_tessuto_mant,   
					  cod_mant_non_a_magazzino,   
					  flag_tipo_mantovana,   
					  alt_mantovana,   
					  flag_cordolo,   
					  flag_passamaneria,   
					  cod_verniciatura,   
					  cod_comando,   
					  pos_comando,   
					  alt_asta,   
					  cod_comando_tenda_verticale,   
					  cod_colore_lastra,   
					  cod_tipo_lastra,   
					  spes_lastra,   
					  cod_tipo_supporto,   
					  flag_cappotta_frontale,   
					  flag_rollo,   
					  flag_kit_laterale,   
					  flag_misura_luce_finita,   
					  cod_tipo_stampo,   
					  cod_mat_sis_stampaggio,   
					  rev_stampo,   
					  num_gambe,   
					  num_campate,   
					  note,
					  flag_autoblocco,   
					  num_fili_plisse,   
					  num_boccole_plisse,   
					  intervallo_punzoni_plisse,   
					  num_pieghe_plisse,   
					  prof_inf_plisse,   
					  prof_sup_plisse,   
					  colore_passamaneria,   
					  colore_cordolo,   
					  num_punzoni_plisse,   
					  des_vernice_fc,   
					  des_comando_1,   
					  cod_comando_2,   
					  des_comando_2,   
					  flag_addizionale_comando_1,   
					  flag_addizionale_comando_2,   
					  flag_addizionale_supporto,   
					  flag_addizionale_tessuto,   
					  flag_addizionale_vernic,   
					  flag_fc_comando_1,   
					  flag_fc_comando_2,   
					  flag_fc_supporto,   
					  flag_fc_tessuto,   
					  flag_fc_vernic,   
					  flag_allegati,   
					  data_ora_ar,   
					  flag_tessuto_cliente,   
					  flag_tessuto_mant_cliente,   
					  cod_verniciatura_2,   
					  flag_addizionale_vernic_2,   
					  flag_fc_vernic_2,   
					  des_vernic_fc_2,   
					  cod_verniciatura_3,   
					  flag_addizionale_vernic_3,   
					  flag_fc_vernic_3,   
					  des_vernic_fc_3 )  
		VALUES ( :s_cs_xx.cod_azienda,   
					  :al_anno_registrazione,   
					  :al_num_registrazione,   
					  :al_prog_riga,   
					  :ls_cod_prod_finito,   
					  :ls_cod_modello,   
					  :ld_dim_x,   
					  :ld_dim_y,   
					  :ld_dim_z,   
					  :ld_dim_t,   
					  :ls_cod_tessuto,   
					  :ls_colore_tessuto,   
					  :ls_cod_tessuto_mant,   
					  :ls_cod_mant_non_a_magazzino,   
					  :ls_flag_tipo_mantovana,   
					  :ld_alt_mantovana,   
					  :ls_flag_cordolo,   
					  :ls_flag_passamaneria,   
					  :ls_cod_verniciatura,   
					  :ls_cod_comando,   
					  :ls_pos_comando,   
					  :ld_alt_asta,   
					  null,   
					  null,   
					  null,   
					  '0',   
					  :ls_cod_tipo_supporto,   
					  'N',   
					  'N',   
					  'N',   
					  :ls_flag_misura_luce_finita,   
					  null,   
					  null,   
					  '0',   
					  0,   
					  0,   
					  null,   
					  'N',   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  0,   
					  :ls_colore_passameneria,   
					  :ls_colore_cordolo,   
					  0,   
					  :ls_des_vernice_fc,   
					  :ls_des_comando_1,   
					  :ls_cod_comando_2,   
					  :ls_des_comando_2,   
					  :ls_flag_addizionale_comando_1,   
					  :ls_flag_addizionale_comando_2,   
					  :ls_flag_addizionale_supporto,   
					  :ls_flag_addizionale_tessuto,   
					  :ls_flag_addizionale_vernic,   
					  :ls_flag_fc_comando_1,   
					  :ls_flag_fc_comando_2,   
					  :ls_flag_fc_supporto,   
					  :ls_flag_fc_tessuto,   
					  :ls_flag_fc_vernic,   
					  :ls_flag_allegati,   
					  null,   
					  :ls_flag_tessuto_cliente,   
					  :ls_flag_tessuto_mant_cliente,   
					  :ls_cod_verniciatura_2,   
					  :ls_flag_addizionale_vernic_2,   
					  :ls_flag_fc_vernic_2,   
					  :ls_des_vernice_fc_2,   
					  :ls_cod_verniciatura_3,   
					  :ls_flag_addizionale_vernic_3,   
					  :ls_flag_fc_vernic_3,   
					  :ls_des_vernice_fc_3 )  ;
end choose
if sqlca.sqlcode = -1 then
	uof_log("Errore grave durante l'inserimento del dettaglio complementare; dettaglio errore " + sqlca.sqlerrtext)
	return -1
end if

// Procedo con inserimento delle addizionali
ll_i = 0
if ls_flag_addizionale_comando_1 = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_comando
end if
if ls_flag_addizionale_comando_2 = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_comando_2
end if
if ls_flag_addizionale_supporto = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_tipo_supporto
end if
if ls_flag_addizionale_tessuto = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_tessuto
end if
if ls_flag_addizionale_vernic = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_verniciatura
end if
if ls_flag_addizionale_vernic_2 = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_verniciatura_2
end if
if ls_flag_addizionale_vernic_3 = "S" then
	ll_i++
	ls_cod_addizionale[ll_i] = ls_cod_verniciatura_3
end if

for ll_i = 1 to upperbound(ls_cod_addizionale)

	select cod_versione
	into :ls_cod_versione
	from distinta_padri
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_addizionale[ll_i];
	if sqlca.sqlcode = 100 then
		setnull(ls_cod_versione)
	elseif sqlca.sqlcode < 0 then
		uof_log( g_str.format( "ERRORE in ricerca versione dell'addizionale $1. Dettaglio: $2 ", ls_cod_comando, sqlca.sqlerrtext))
	end if
	
	uof_get_comp_det_quantita( ls_cod_addizionale[ll_i], adw_varianti, ref ld_quan_addizionale)
	if isnull(ld_quan_addizionale) then ld_quan_addizionale = 0
	ll_ret = uof_crea_riga_ordine( al_anno_registrazione, al_num_registrazione, al_prog_riga, ls_cod_addizionale[ll_i], ls_cod_versione, ld_quan_addizionale, ls_cod_modello, 0,0, "A", as_tipo_gestione, "")
	
	if ll_ret < 0 then	return ll_ret
	
next

// carico la struttura istr_riepologo per futura comodità

istr_riepilogo.dimensione_1 = ld_dim_x
istr_riepilogo.dimensione_2 = ld_dim_y
istr_riepilogo.dimensione_3 = ld_dim_z
istr_riepilogo.dimensione_4 = ld_dim_t
istr_riepilogo.quan_vendita = 1
istr_riepilogo.flag_allegati = 'S'
if istr_flags.flag_luce_finita = "S" then
	istr_riepilogo.flag_misura_luce_finita = ls_flag_misura_luce_finita
else
	istr_riepilogo.flag_misura_luce_finita = "F"
end if

istr_riepilogo.cod_tessuto = ""
istr_riepilogo.cod_non_a_magazzino = ""
istr_riepilogo.cod_tessuto_mant = ""
istr_riepilogo.cod_mant_non_a_magazzino = ""
istr_riepilogo.alt_mantovana = 0
istr_riepilogo.flag_tipo_mantovana = ""
istr_riepilogo.colore_cordolo = ""
istr_riepilogo.colore_passamaneria = ""
istr_riepilogo.flag_fc_tessuto = ""

if istr_flags.flag_tessuto = "S" then
	istr_riepilogo.cod_tessuto = ls_cod_tessuto
	istr_riepilogo.cod_non_a_magazzino = ls_colore_tessuto
	istr_riepilogo.flag_addizionale_tessuto = ls_flag_addizionale_tessuto
	istr_riepilogo.flag_fc_tessuto = ls_flag_fc_tessuto
end if

if istr_flags.flag_tessuto_mantovana = "S" then
	istr_riepilogo.cod_tessuto_mant = ls_cod_tessuto_mant
	istr_riepilogo.cod_mant_non_a_magazzino = ls_cod_mant_non_a_magazzino
end if

if istr_flags.flag_altezza_mantovana = "S" then istr_riepilogo.alt_mantovana = ld_alt_mantovana

if istr_flags.flag_tipo_mantovana = "S" then 
	if not isnull(istr_riepilogo.cod_tessuto_mant) and len(istr_riepilogo.cod_tessuto_mant) > 1 then
		istr_riepilogo.flag_tipo_mantovana = ls_flag_tipo_mantovana
	else
		setnull(istr_riepilogo.flag_tipo_mantovana)
	end if
end if
if istr_flags.flag_cordolo = "S" then istr_riepilogo.colore_cordolo = ls_colore_cordolo
if istr_flags.flag_passamaneria = "S" then istr_riepilogo.colore_passamaneria = ls_colore_passameneria

istr_riepilogo.cod_comando = ""
istr_riepilogo.pos_comando =  ""
istr_riepilogo.alt_asta = 0
istr_riepilogo.des_comando_1 = ""
istr_riepilogo.des_comando_2 = ""
istr_riepilogo.flag_fc_comando_1 = ""
istr_riepilogo.flag_fc_comando_2 = ""
if istr_flags.flag_comando = "S" then 
	istr_riepilogo.cod_comando = ls_cod_comando
	istr_riepilogo.flag_addizionale_comando_1 =ls_flag_addizionale_comando_1
	istr_riepilogo.flag_fc_comando_1 = ls_flag_fc_comando_1
end if
if istr_flags.flag_pos_comando = "S" then istr_riepilogo.pos_comando = ls_pos_comando
if istr_flags.flag_alt_asta = "S" then istr_riepilogo.alt_asta = ld_alt_asta

if istr_flags.flag_secondo_comando = "S" then  
	istr_riepilogo.cod_comando_2 = ls_cod_comando_2
	istr_riepilogo.flag_addizionale_comando_2 = ls_flag_addizionale_comando_2
	istr_riepilogo.flag_fc_comando_2 = ls_flag_fc_comando_2
end if

istr_riepilogo.note = ""

istr_riepilogo.flag_autoblocco = ""
istr_riepilogo.num_fili_plisse = 0
istr_riepilogo.num_boccole_plisse = 0
istr_riepilogo.intervallo_punzoni_plisse = 0
istr_riepilogo.num_pieghe_plisse = 0
istr_riepilogo.prof_inf_plisse = 0
istr_riepilogo.prof_sup_plisse = 0

istr_riepilogo.cod_tipo_supporto = ""
istr_riepilogo.flag_cappotta_frontale = ""
istr_riepilogo.flag_rollo = ""
istr_riepilogo.flag_kit_laterale = ""
istr_riepilogo.num_gambe = 0
istr_riepilogo.num_campate = 0
istr_riepilogo.flag_fc_supporto = ""

if istr_flags.flag_supporto = "S" then 
	istr_riepilogo.cod_tipo_supporto = ls_cod_tipo_supporto
	istr_riepilogo.flag_addizionale_supporto = ls_flag_addizionale_supporto
	istr_riepilogo.flag_fc_supporto = ls_flag_fc_supporto
end if

istr_riepilogo.cod_verniciatura =  ""
istr_riepilogo.cod_colore_lastra = ""
istr_riepilogo.cod_tipo_lastra =  ""
istr_riepilogo.spes_lastra = ""
istr_riepilogo.flag_fc_vernic = ""
if istr_flags.flag_verniciatura = "S" then 
	istr_riepilogo.cod_verniciatura = ls_cod_verniciatura
	istr_riepilogo.flag_addizionale_vernic = ls_flag_addizionale_vernic
	istr_riepilogo.flag_fc_vernic = ls_flag_fc_vernic
end if
istr_riepilogo.des_vernice_fc = ls_des_vernice_fc

istr_riepilogo.cod_verniciatura_2 =  ""
istr_riepilogo.flag_fc_vernic_2 = ""
if istr_flags.flag_verniciatura_2 = "S" then 
	istr_riepilogo.cod_verniciatura_2 = ls_cod_verniciatura_2
	istr_riepilogo.flag_addizionale_vernic_2 = ls_flag_addizionale_vernic_2
	istr_riepilogo.flag_fc_vernic_2 = ls_flag_fc_vernic_2
end if
istr_riepilogo.des_vernice_fc_2 = ls_des_vernice_fc_2

istr_riepilogo.cod_verniciatura_3 =  ""
istr_riepilogo.flag_fc_vernic_3 = ""
if istr_flags.flag_verniciatura_3 = "S" then 
	istr_riepilogo.cod_verniciatura_3 = ls_cod_verniciatura_3
	istr_riepilogo.flag_addizionale_vernic_3 = ls_flag_addizionale_vernic_3
	istr_riepilogo.flag_fc_vernic_3 = ls_flag_fc_vernic_3
end if
istr_riepilogo.des_vernice_fc_3 = ls_des_vernice_fc_3

return 0
end function

public function integer uof_exist_prodotto (string as_cod_variabile, string as_cod_prodotto);string ls_str

if isnull(as_cod_prodotto) or len(as_cod_prodotto) < 0 then

	uof_log("La variabile attesa del dettaglio complementare " + as_cod_variabile + " non è stata valorizzata; la procedura prosegue ugualmente")

else

	select des_prodotto
	into :ls_str
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto;

	if sqlca.sqlcode = 0 then
		return 0
	elseif sqlca.sqlcode = 100 then
		uof_log("Variabile del dettaglio complementare " + as_cod_variabile + "; valore " + as_cod_prodotto + " non trovato in anagrafica prodotti.")
	else
		uof_log("Errore grave di sistema nella ricerca della variabile " + as_cod_variabile)
	end if

end if
end function

public function integer uof_importa_riga_documento (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_cod_prodotto_finito, string as_path_file_pdf, ref datawindow adw_dettaglio, string as_tipo_gestione);string 								ls_errore, ls_cod_versione_finito, ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], &
										ls_flag_tipo_bcl, ls_databasedocs, ls_servernamedocs

long									ll_riga,  ll_ret

uo_funzioni_1 					luo_funzioni_1
uo_produzione					luo_prod
uo_calendario_prod_new		luo_cal_prod
uo_spese_trasporto			luo_spese_trasporto

date									ldt_dtp_rep_par[], ldt_dtp_rep_arr[], ldd_vuoto[]

integer								li_ret

boolean								lb_continue_calprod

datetime							ldt_date_reparti[], ldt_vuoto[]

transaction							lt_tran




uof_log(fill("-",100))

if not isnull(al_prog_riga) and al_prog_riga > 0 then
	ib_nuova_configurazione= false
else
	ib_nuova_configurazione= true
end if

uof_get_comp_det_tostring("COD_MODELLO", adw_dettaglio, as_cod_prodotto_finito)

select 	cod_versione
into 		:ls_cod_versione_finito
from 		distinta_padri
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto_finito and
			flag_predefinita = 'S';

if sqlca.sqlcode = 100 then
	uof_log("ERRORE Fatale: il prodotto " + as_cod_prodotto_finito + " non ha alcuna distinta predefinita ")
	return -1
end if

if sqlca.sqlcode <> 0 then
	uof_log("ERRORE Fatale: si è verificato il seguente errore in lettura versione predefinita del prodotto " + as_cod_prodotto_finito + "~r~n" + sqlca.sqlerrtext)
	return -1
end if

ll_ret = uof_inserisci_varianti_from_apice(	al_anno_registrazione, &
														al_num_registrazione, &
														ref al_prog_riga, &
														as_tipo_gestione, &
														as_cod_prodotto_finito, &
														ls_cod_versione_finito, &
														ref adw_dettaglio, &
														ref ls_errore)
														
if ll_ret = -1 then
	uof_log("ERRORE Fatale - uof_inserisci_varianti_from_apice("+&
				string(al_anno_registrazione)+","+string(al_num_registrazione)+","+string(al_prog_riga)+","+as_tipo_gestione+","+as_cod_prodotto_finito+","+ls_cod_versione_finito+") "+&
				"~r~n" + sqlca.sqlerrtext)
	rollback;
	return -1
else
	if as_tipo_gestione = "ORD_VEN" then
		// Creo i reparti di produzione e le relative fasi
		luo_funzioni_1 = create uo_funzioni_1
		ll_ret = luo_funzioni_1.uof_gen_varianti_deposito_ord_ven ( 	al_anno_registrazione, &
																						al_num_registrazione, &
																						al_prog_riga, &
																						as_cod_prodotto_finito, &
																						ls_cod_versione_finito, &
																						ref ls_errore )
		if ll_ret < 0 then
			uof_log("ERRORE GRAVE in fase di creazione fasi/depositi e reparti di produzione (luo_funzioni_1.uof_gen_varianti_deposito_ord_ven). " + ls_errore)
			rollback;
			return -1
		end if
		
		
		
		//######################################################################################################
		//Donato 06/05/2013
		//allineo le date pronto dei reparti e pianifico nel calendario produzione
		//se qualcosa non va a buon fine, non blocco l'import ma lo segnalo nel log ...
		
		if as_cod_prodotto_finito<>"" and not isnull(as_cod_prodotto_finito) and ls_cod_versione_finito<>"" and not isnull(ls_cod_versione_finito) then
			
			lb_continue_calprod = true
			
			ls_dep_par[] = ls_vuoto[]
			ls_rep_par[] = ls_vuoto[]
			ls_dep_arr[] = ls_vuoto[]
			ls_rep_arr[] = ls_vuoto[]
			ldt_dtp_rep_par[] = ldd_vuoto[]
			ldt_dtp_rep_arr[] = ldd_vuoto[]
			
			luo_prod = create uo_produzione
			
			luo_prod.uof_trova_tipo(al_anno_registrazione, al_num_registrazione, ls_flag_tipo_bcl, ls_errore)
			ls_errore = ""
			
			li_ret = luo_prod.uof_cal_trasferimenti(	al_anno_registrazione, al_num_registrazione, &
																as_cod_prodotto_finito, ls_cod_versione_finito, &
																ls_dep_par[], ls_rep_par[], ldt_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldt_dtp_rep_arr[], ls_errore)
			destroy luo_prod
			
			if li_ret<0 then
				lb_continue_calprod = false					//STOP NELL'ELABORAZIONE SOLo DI QUESTA PARTE
				
				if ls_errore<>"" and not isnull(ls_errore) then
					uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
									": Il sistema non è riuscito ad assegnare le date pronto ai reparti produzione. "+&
									"Controllare dopo l'importazione lo stato reparti produzione e relative date pronto; Dettaglio: " + ls_errore)
				end if
			end if
			
			
			if lb_continue_calprod then
				//continua aggiornando la tabella varianti_det_ord_ven_prod
				ldt_date_reparti[] =  ldt_vuoto[]
				
				for li_ret=1 to upperbound(ls_rep_par[])
					
					if ls_flag_tipo_bcl="A" then
					
						update varianti_det_ord_ven_prod
						set		data_pronto = :ldt_dtp_rep_par[li_ret],
									data_arrivo = :ldt_dtp_rep_arr[li_ret],
									cod_reparto_arrivo = :ls_rep_arr[li_ret]
						where 	anno_registrazione = :al_anno_registrazione and 
									num_registrazione = :al_num_registrazione and
									prog_riga_ord_ven = :al_prog_riga and
									cod_reparto = :ls_rep_par[li_ret];
				
						if sqlca.sqlcode < 0 then
							lb_continue_calprod = false			//STOP NELL'ELABORAZIONE SOLO DI QUESTA PARTE
							
							//segnalo ma non blocco
							uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
										": Il sistema non è riuscito ad assegnare le date pronto ai reparti produzione; "+&
											"Errore in UPDATE tabella 'varianti_det_ord_ven_prod' con data_pronto: " + sqlca.sqlerrtext)
							
							exit
							
						end if
						
					else
						//tenda tecnica
						update det_ord_ven
						set		cod_reparto_sfuso = :ls_rep_par[li_ret],
								data_pronto_sfuso = :ldt_dtp_rep_par[li_ret],
								cod_reparto_arrivo = :ls_rep_arr[li_ret],
								data_arrivo = :ldt_dtp_rep_arr[li_ret]
						where 	anno_registrazione = :al_anno_registrazione and 
									num_registrazione = :al_num_registrazione and
									prog_riga_ord_ven = :al_prog_riga;
						
						if sqlca.sqlcode < 0 then
							lb_continue_calprod = false			//STOP NELL'ELABORAZIONE SOLO DI QUESTA PARTE
							
							//segnalo ma non blocco
							uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
										": Il sistema non è riuscito ad assegnare le date pronto ai reparti produzione; "+&
											"Errore in UPDATE tabella 'det_ord_ven': " + sqlca.sqlerrtext)
							
							exit
						end if
						
					end if
					
					ldt_date_reparti[li_ret] = datetime(ldt_dtp_rep_par[li_ret] ,00:00:00)
					
				next
			end if
			
			if lb_continue_calprod then
				//continua con il calendario produzione
				
				if upperbound(ls_rep_par[])>0 then
					
					luo_cal_prod = create uo_calendario_prod_new
					
					li_ret = luo_cal_prod. uof_salva_in_calendario(al_anno_registrazione, al_num_registrazione, al_prog_riga, &
																		ls_rep_par[], ldt_date_reparti[], ls_errore)
					destroy luo_cal_prod
					
					if li_ret < 0 then
						lb_continue_calprod = false			//STOP NELL'ELABORAZIONE SOLO DI QUESTA PARTE
						
						//segnalo ma non blocco
						uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
									": Il sistema non è riuscito ad assegnare le date pronto ai reparti produzione; "+&
										"Errore in pianificazione calendario produzione: " + ls_errore)
					end if
				end if
			end if
		end if
		//######################################################################################################
		
	end if
	
	//calcolo del peso netto unitario (eventuale errore non deve essere bloccante)
	if as_tipo_gestione = "ORD_VEN" then
		li_ret = uof_calcola_peso(al_anno_registrazione, al_num_registrazione, al_prog_riga, ls_errore)
		if li_ret<0 then
			uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
										": calcolo peso netto unitario: " + ls_errore)
		end if
	end if
	
	
	//calcolo spese trasporto--------------------------------------------
	// stefanop 19/02/2014
	if as_tipo_gestione = "ORD_VEN" then
		
		luo_spese_trasporto = create uo_spese_trasporto
		li_ret = luo_spese_trasporto.uof_imposta_spese_ord_ven(al_anno_registrazione, al_num_registrazione, al_prog_riga, true, ls_errore)
		destroy luo_spese_trasporto;
		
		if li_ret < 0 then
			//faccio un errore non bloccante
			uof_log("WARNING Riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga)+&
										": impostazione spese trasporto: " + ls_errore)
		end if
	end if
	
	
	// continua con il calcolo del prezzo del prodotto
	
	// Carico il foglio di produzione di Axioma
	if isnull(as_path_file_pdf) or len(as_path_file_pdf) < 1 then
		uof_log("File PDF della distinta di taglio mancante o non generato.")
	else
		ls_databasedocs = ""
		ls_servernamedocs = ""
		Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "databasedocs",ls_databasedocs)
		Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "servernamedocs",ls_servernamedocs)

		if not guo_functions.uof_create_transaction_from( sqlca, lt_tran, ls_servernamedocs, ls_databasedocs, ls_errore)  then
			uof_log("OPERAZIONE ANNULLATA: Errore nella creazione della transazione per archiviazione documenti. ~r~n" + ls_errore)
			rollback;
		else
			
			ll_ret = uof_importa_blob(as_path_file_pdf, &
												al_anno_registrazione, &
												al_num_registrazione, &
												al_prog_riga, &
												as_tipo_gestione,&
												ref lt_tran)
			if ll_ret = 0 then
				commit using lt_tran;
				commit;
			else
				rollback using lt_tran;
				rollback;
			end if
			
			disconnect using lt_tran;
			destroy lt_tran;
			
		end if
	end if
end if

uof_log("")

end function

public function integer uof_get_comp_det_quantita (string as_nome_elemento_cercato, datawindow adw_varianti, ref decimal ad_valore);string ls_find, ls_valore
long ll_i,ll_row

ls_find = " upper(apc_codice_materiale) = '" + upper(as_nome_elemento_cercato) + "'"
ll_row = adw_varianti.Find (ls_find  , 1, adw_varianti.rowcount() )

if ll_row < 0 then 
	uof_log( g_str.format("Errore nella ricerca del valore " + as_nome_elemento_cercato + " nelle specifiche del dettaglio complementare" , sqlca.sqlerrtext) )
	return -2
elseif ll_row = 0 then
	uof_log( g_str.format("Il codice materiale cercato " + as_nome_elemento_cercato + "  del dettaglio complementare non è presente." , sqlca.sqlerrtext) )
	return -2
else
	ad_valore = adw_varianti.getitemnumber(ll_row, "apc_quantita")
end if

return 0
end function

public function integer uof_inserisci_optional (ref datawindow adw_varianti, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto_finito, string as_tipo_gestione);string ls_find, ls_cod_optional[],ls_cod_tipo_det_ven, ls_cod_versione, ls_errore
long ll_i, ll_row,ll_idx,ll_ret
dec{4} ld_quan_optional[], ld_dim_1[], ld_dim_2[]
uo_conf_varianti luo_conf_varianti

ls_find = "upper(apc_gruppo_variante) = 'OPTIONAL_DET' or upper(apc_gruppo_variante) = 'OPTIONAL' "
ll_row = 0
ll_idx = 0

do while true

	ll_row = adw_varianti.Find (ls_find  , ll_row + 1, adw_varianti.rowcount() )
	
	if ll_row < 0 then 
		// errore
		uof_log( "Errore nella ricerca degli OPTIONAL fra i dettagli esportati da Axioma: nessun optional caricato." )
		return -2
	elseif ll_row = 0 then
		// fine ricerca...esco
		exit
	else
		// trovato
		ll_idx ++
		ls_cod_optional[ll_idx] = adw_varianti.getitemstring(ll_row, "apc_codice_materiale")
		ld_quan_optional[ll_idx] = adw_varianti.getitemnumber(ll_row, "apc_quantita")
		ld_dim_1[ll_idx] = uof_string_to_number( adw_varianti.getitemstring(ll_row, "apc_specifica_1") )
		if isnull(ld_dim_1[ll_idx]) then ld_dim_1[ll_idx] = 0
		ld_dim_2[ll_idx] =uof_string_to_number( adw_varianti.getitemstring(ll_row, "apc_specifica_2") )
		if isnull(ld_dim_2[ll_idx]) then ld_dim_2[ll_idx] = 0
		// procedo con insert nella riga
	end if

loop

for ll_idx = 1 to upperbound( ls_cod_optional )
	
	if ls_cod_optional[ll_idx] = "**" then continue

	select 	cod_versione
	into 		:ls_cod_versione
	from 		distinta_padri
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_optional[ll_idx] and
				flag_predefinita = 'S';
	
	if sqlca.sqlcode < 0 then
		uof_log( "Errore in ricerca distinta predefinita (uof_inserisci_optional). Dettaglio:"+sqlca.sqlerrtext )
	else
	// procedo con insert
		if sqlca.sqlcode = 100 then setnull(ls_cod_versione)
		ll_ret = uof_crea_riga_ordine(al_anno_registrazione, al_num_registrazione, al_prog_riga, ls_cod_optional[ll_idx], ls_cod_versione, ld_quan_optional[ll_idx], as_cod_prodotto_finito, ld_dim_1[ll_idx], ld_dim_2[ll_idx], "O", as_tipo_gestione, "")
		if ll_ret < 0 then return ll_ret
		
		if not isnull(ls_cod_versione) then
		
			luo_conf_varianti = create uo_conf_varianti
			
			luo_conf_varianti.il_anno_registrazione = al_anno_registrazione
			luo_conf_varianti.il_num_registrazione = al_num_registrazione
			luo_conf_varianti.il_prog_riga = ll_ret
			luo_conf_varianti.is_gestione = "ORD_VEN"
			
			
			ll_ret = luo_conf_varianti.uof_valorizza_formule( ls_cod_optional[ll_idx], ls_cod_versione, ref ls_errore)
			
			if ll_ret < 0 then
				uof_log( "Errore valorizzazione formule prodotto " + ls_cod_optional[ll_idx] + " Versione:" + ls_cod_versione + ".Errore: " + ls_errore)
				destroy luo_conf_varianti
				return 0
			end if
			
			destroy luo_conf_varianti
		end if
	end if
next

return 0
end function

public function long uof_inserisci_varianti_from_axioma (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_gestione, string as_cod_prodotto_finito, string as_cod_versione, ref datawindow adw_varianti, ref string as_errore);/*	Funzione che scorre la distinta base e inserisce la variante di tipo codice prodotto (fs_cod_prodotto_var)
 FUNZIONE CHE VALORIZZA TUTTE LE FORMULE PRESENTI DELLA DISTINTA CREANDO LA RELATIVA VARIANTE.

nome: uof_inserisci_varianti
tipo: integer
Possibili valori di ritorno
0 = tutto ok
-1 = errore con blocco della procedura
-2 = errore con salto dell'importazione della riga
*/
string  ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
		  ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_flag_materia_prima,ls_cod_versione_figlio[],&
		 ls_cod_prodotto_variante, ls_cod_versione_variante, ls_tabella_comp, ls_cod_gruppo_variante, ls_cod_misura_mag, &
		 ls_flag_materia_prima_mag
dec{4} ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo
long    ll_conteggio,ll_t,ll_num_sequenza, ll_cont_1, ll_i, ll_max_riga, ll_ret
integer li_risposta, li_count

choose case as_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"

		// se la riga ordine è a zero vuol dire  che è una nuova riga
		if al_prog_riga=0 or isnull(al_prog_riga) then
			select max(prog_riga_ord_ven)
			into :ll_max_riga
			from det_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione;
					
			if ll_max_riga=0 or isnull(ll_max_riga) then
				ll_max_riga = 10
			else
				ll_max_riga = ll_max_riga + 10
				// avanzamento righe 10 alla volta
				ll_max_riga = ( int(ll_max_riga / 10) ) * 10 
			end if
			al_prog_riga = ll_max_riga
			ll_ret = uof_crea_riga_ordine(al_anno_registrazione, al_num_registrazione, al_prog_riga, as_cod_prodotto_finito,as_cod_versione, 1,as_cod_prodotto_finito,0,0,"P" ,as_gestione,"")
			
			if ll_ret = -1 then
				return -1
			elseif ll_ret = -2 then
				return -2
			end if
			
			ll_ret = uof_crea_comp_det(adw_varianti,al_anno_registrazione, al_num_registrazione, al_prog_riga, as_gestione)
				
			if ll_ret = -1 then
				return -1
			elseif ll_ret = -2 then
				return -2
			end if
			
			ll_ret = uof_inserisci_optional( adw_varianti, al_anno_registrazione, al_num_registrazione, al_prog_riga, as_cod_prodotto_finito,as_gestione)
				
			if ll_ret = -1 then
				return -1
			elseif ll_ret = -2 then
				return -2
			end if
			
		end if
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		// se la riga ordine è a zero vuol dire  che è una nuova riga
		if al_prog_riga=0 or isnull(al_prog_riga) then
			select max(prog_riga_off_ven)
			into :ll_max_riga
			from det_off_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione;
					
			if ll_max_riga=0 or isnull(ll_max_riga) then
				ll_max_riga = 10
			else
				ll_max_riga = ll_max_riga + 10
				// avanzamento righe 10 alla volta
				ll_max_riga = ( int(ll_max_riga / 10) ) * 10 
			end if
			al_prog_riga = ll_max_riga
			
		end if
		
end choose


ll_conteggio = 1

declare cu_distinta_1 cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_materia_prima
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda  and
		  cod_prodotto_padre = :as_cod_prodotto_finito and
		  cod_versione=:as_cod_versione;

open cu_distinta_1;

do while true
	fetch cu_distinta_1 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		as_errore = "Errore in fase di fetch cursore cu_distinta_1 (uo_axioma.uof_inserisci_varianti) ~r~n" + SQLCA.SQLErrText
		close cu_distinta_1;
		return -1
	end if
	
	// carico il gruppo variante.
	select 	cod_gruppo_variante
	into 		:ls_cod_gruppo_variante
	from 		distinta_gruppi_varianti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_padre = :as_cod_prodotto_finito and
				cod_versione = :as_cod_versione and
				num_sequenza = :ll_num_sequenza and
				cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
				cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio];

	if sqlca.sqlcode = 0 then			
		// gruppo variante trovato; verifico se devo fare delle sostituzioni.
		
		ll_i = adw_varianti.find("apc_gruppo_variante ='" + ls_cod_gruppo_variante + "'", 1, adw_varianti.rowcount() )
		
		if ll_i > 0 then
//		for ll_i = 1 to adw_varianti.rowcount()
//			if adw_varianti.getitemstring(ll_i, "apc_gruppo_variante") = ls_cod_gruppo_variante then
				// verifico se su questo ramo di distinta c'è già una variante; eventualmente segnalo errore.
				
				choose case as_gestione
					case "ORD_VEN"
						select count(*)
						into   :ll_cont_1
						from   varianti_det_ord_ven
						where  cod_azienda         = :s_cs_xx.cod_azienda and
								 anno_registrazione  = :al_anno_registrazione and
								 num_registrazione   = :al_num_registrazione and
								 prog_riga_ord_ven   = :al_prog_riga and
								 cod_prodotto_padre  = :as_cod_prodotto_finito and
								 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
								 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
								 cod_versione        = :as_cod_versione and
								 num_sequenza = :ll_num_sequenza;
								 
					case "OFF_VEN"
						select count(*)
						into   :ll_cont_1
						from   varianti_det_off_ven
						where  cod_azienda         = :s_cs_xx.cod_azienda and
								 anno_registrazione  = :al_anno_registrazione and
								 num_registrazione   = :al_num_registrazione and
								 prog_riga_off_ven   = :al_prog_riga and
								 cod_prodotto_padre  = :as_cod_prodotto_finito and
								 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
								 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
								 cod_versione        = :as_cod_versione and
								 num_sequenza = :ll_num_sequenza;
								 
				end choose
				
				if ll_cont_1 > 0 then
					uof_log( g_str.format("La il gruppo variante $1 risulta già essere valorizzato; l'inserimento del prodotto $2 viene saltato.", ls_cod_gruppo_variante, ls_cod_prodotto_figlio[ll_conteggio]) )
					ll_conteggio++
					continue
				else	
					// procedo con inserimento della variante
		
					
					ls_cod_prodotto_variante = adw_varianti.getitemstring(ll_i, "apc_codice_materiale")
					
					select cod_misura_mag,
							flag_materia_prima
					into 	:ls_cod_misura_mag,
							:ls_flag_materia_prima_mag
					from 	anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto_variante;
							
					if sqlca.sqlcode = 100 then
						uof_log( g_str.format("Il prodotto $1 non esiste a magazzino; il suo inserimento nella distinta viene saltato.", ls_cod_prodotto_variante) )
						ll_conteggio++
						continue
					elseif sqlca.sqlcode < 0 then
						uof_log( g_str.format("Errore grave durante la ricerca del prodotto $1 in anagrafica prodotti. Dettaglio errore:~r~n $2", ls_cod_prodotto_variante, sqlca.sqlerrtext) )
						close cu_distinta_1;
						return -1
					end if		
					
					ld_quan_utilizzo = adw_varianti.getitemnumber(ll_i, "apc_quantita")
					
					
//					if  not isnull(ls_cod_formula_quan_utilizzo) then
//						ld_quan_utilizzo = f_calcola_formula_db(ls_cod_formula_quan_utilizzo, ls_tabella_comp, al_anno_registrazione, al_num_registrazione, al_prog_riga)
//						if ld_quan_utilizzo = -1 then
//							close cu_distinta_1;
//							rollback;
//							return -1
//						end if
//					end if
				
					ld_quan_utilizzo = round(ld_quan_utilizzo,4)
					
					if isnull(ls_cod_misura_mag) then 
						ls_cod_misura_mag = "null"
					else
						ls_cod_misura_mag = "'" + ls_cod_misura_mag + "'"
					end if
					if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
					if isnull(ld_dim_x) then ld_dim_x = 0
					if isnull(ld_dim_y) then ld_dim_y = 0
					if isnull(ld_dim_z) then ld_dim_z = 0
					if isnull(ld_dim_t) then ld_dim_t = 0
					if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
					if isnull(ls_des_estesa) then ls_des_estesa = ""
					if isnull(ls_formula_tempo) then ls_formula_tempo = ""
					if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"
					
		
					ls_sql_ins =" insert into " + ls_tabella + &
									"(cod_azienda," + &
									"anno_registrazione," + &
									"num_registrazione," + &
									ls_progressivo + "," + &
									"cod_prodotto_padre," + &
									"num_sequenza," + &
									"cod_prodotto_figlio," + &
									"cod_versione_figlio," + &
									"cod_versione," + &
									"cod_misura," + &
									"cod_prodotto," + &
									"cod_versione_variante," + &
									"quan_tecnica," + &
									"quan_utilizzo," + &
									"dim_x," + &
									"dim_y," + &
									"dim_z," + &
									"dim_t," + &
									"coef_calcolo," + &
									"flag_esclusione," + &
									"formula_tempo," + &
									"flag_materia_prima," + &
									"des_estesa)" + &
								"VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
									string(al_anno_registrazione) + ", " + &   
									string(al_num_registrazione) + ", " + &   
									string(al_prog_riga) + ", '" + &   
									as_cod_prodotto_finito + "', " + &
									string(ll_num_sequenza) + ", '" + &   
									ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
									ls_cod_versione_figlio[ll_conteggio] + "', '" + &
									as_cod_versione + "'," + &
									ls_cod_misura_mag + ", '" + &
									ls_cod_prodotto_variante + "', '" + &
									ls_cod_versione_figlio[ll_conteggio] + "', " + &
									f_double_to_string(ld_quan_tecnica) + ", " + &   
									f_double_to_string(ld_quan_utilizzo) + ", " + &
									f_double_to_string(ld_dim_x) + ", " + &   
									f_double_to_string(ld_dim_y) + ", " + &   
									f_double_to_string(ld_dim_z) + ", " + &   
									f_double_to_string(ld_dim_t) + ", " + &   
									f_double_to_string(ld_coef_calcolo) + ", '" + &   
									"N', '" + &
									ls_formula_tempo + "', '" + &
									ls_flag_materia_prima_mag + "', '" + &
									ls_des_estesa + "')" 
			
					execute immediate :ls_sql_ins;
					if sqlca.sqlcode = -1 then
						uof_log("Errore nell'inserimento della Variante (uo_conf_varianti - uof_valorizza_formule)~r~n" + sqlca.sqlerrtext)
						close cu_distinta_1;
						return -1
					end if
					
					ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante

					
				end if
				
			else
				
				uof_log( g_str.format("Il gruppo variante $1 non è stato esportato in Axioma e non verrà valorizzato nella distinta di Apice.", ls_cod_gruppo_variante) )
				ll_conteggio++
				continue
				
			end if
		//next
	
	else
		// nessun gruppo variante associato al ramo distinta: proseguo nell'esame della distinta.
		ll_conteggio++
		continue
	end if

	ll_conteggio++	

loop

close cu_distinta_1;


for ll_t = 1 to ll_conteggio -1
   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
			 cod_versione=:ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		uof_log( g_str.format("Errore grave nella SELECT da tabella distinta. ~r~nDettaglio errore: $1", sqlca.sqlerrtext) )
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_inserisci_varianti_from_axioma(al_anno_registrazione, al_num_registrazione, al_prog_riga, as_gestione, ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ref adw_varianti, ref as_errore)
		if li_risposta < 0 then
			return -1
		end if
	end if
next

return 0

end function

public function long uof_inserisci_varianti_from_apice (long al_anno_registrazione, long al_num_registrazione, ref long al_prog_riga, string as_gestione, string as_cod_prodotto_finito, string as_cod_versione, ref datawindow adw_varianti, ref string as_errore);/*	Funzione che scorre la distinta base e inserisce la variante di tipo codice prodotto (fs_cod_prodotto_var)
	Questa funzione segue le regole di Apice, cioè parte dalla comp_det_ord_ven e ricrea le varianti calcolando
	materie prime automatiche etc ...
	FUNZIONE CHE VALORIZZA TUTTE LE FORMULE PRESENTI DELLA DISTINTA CREANDO LA RELATIVA VARIANTE.

nome: uof_inserisci_varianti
tipo: integer
Possibili valori di ritorno
0 = tutto ok
-1 = errore con blocco della procedura
-2 = errore con salto dell'importazione della riga
*/
string   ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
		  ls_tabella_comp,  ls_cod_misura_mag, ls_nota_dettaglio
dec{4} ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo
long    ll_conteggio,ll_t,ll_num_sequenza, ll_cont_1, ll_i, ll_max_riga, ll_ret
integer li_risposta, li_count
uo_conf_varianti luo_conf_varianti
str_mp_automatiche astr_mp_automatiche[]

//	Carico la struttura istr_flag
select 	cod_tab_rapp_misure,
			cod_iniziale,
			flag_tessuto,
			flag_tipo_mantovana,
			flag_altezza_mantovana,
			flag_tessuto_mantovana,
			flag_cordolo,
			flag_passamaneria,
			flag_verniciatura,
			flag_verniciatura_2,
			flag_verniciatura_3,
			flag_spessore_lastra,
			flag_colore_lastra,
			flag_tipo_lastra,
			flag_comando,
			flag_pos_comando,
			flag_alt_asta,
			flag_supporto,
			flag_kit_laterale,
			flag_luce_finita,
			cod_rollo,
			flag_rollo,
			cod_cappotta_frontale,
			flag_cappotta_frontale,
			flag_num_gambe_campate,
			flag_decoro,
			flag_scritta,
			flag_plisse,
			flag_prezzo_superficie,
			flag_secondo_comando,
			flag_uso_quan_distinta,
			cod_gruppo_var_quan_distinta
	 into 	:istr_flags.cod_tab_rapp_misure,
			:istr_flags.cod_iniziale,
			:istr_flags.flag_tessuto,
			:istr_flags.flag_tipo_mantovana,
			:istr_flags.flag_altezza_mantovana,
			:istr_flags.flag_tessuto_mantovana,
			:istr_flags.flag_cordolo,
			:istr_flags.flag_passamaneria,
			:istr_flags.flag_verniciatura,
			:istr_flags.flag_verniciatura_2,
			:istr_flags.flag_verniciatura_3,
			:istr_flags.flag_spessore_lastra,
			:istr_flags.flag_colore_lastra,
			:istr_flags.flag_tipo_lastra,
			:istr_flags.flag_comando,
			:istr_flags.flag_pos_comando,
			:istr_flags.flag_alt_asta,
			:istr_flags.flag_supporto,
			:istr_flags.flag_kit_laterale,
			:istr_flags.flag_luce_finita,
			:istr_flags.cod_rollo,
			:istr_flags.flag_rollo,
			:istr_flags.cod_cappotta_frontale,
			:istr_flags.flag_cappotta_frontale,
			:istr_flags.flag_num_gambe_campate,
			:istr_flags.flag_decoro,
			:istr_flags.flag_scritta,
			:istr_flags.flag_plisse,
			:istr_flags.flag_prezzo_superficie,
			:istr_flags.flag_secondo_comando,
			:istr_flags.flag_uso_quan_distinta,
			:istr_flags.cod_gruppo_var_quan_distinta
 from 		tab_flags_configuratore
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_modello = :as_cod_prodotto_finito;

if sqlca.sqlcode = 100 then
	as_errore = "Tabella dei Flags non Completata per Questo Modello di Tenda"
	return -1
elseif sqlca.sqlcode = -1 then
	as_errore = "Errore durante l'Estrazione Tabella Flags per Questo Modello di Tenda" + sqlca.sqlerrtext
	return -1
end if




choose case as_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"

		// se la riga ordine è a zero vuol dire  che è una nuova riga
		if al_prog_riga=0 or isnull(al_prog_riga) then
			select max(prog_riga_ord_ven)
			into :ll_max_riga
			from det_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione;
					
			if ll_max_riga=0 or isnull(ll_max_riga) then
				ll_max_riga = 10
			else
				ll_max_riga = ll_max_riga + 10
				// avanzamento righe 10 alla volta
				ll_max_riga = ( int(ll_max_riga / 10) ) * 10 
			end if
			al_prog_riga = ll_max_riga
			
			// leggo nota dettaglio
			ll_ret = uof_get_nota_dettaglio( "NOTA", adw_varianti, ref ls_nota_dettaglio)
			if ll_ret < 0 then ls_nota_dettaglio = ""
			
			ll_ret = uof_crea_riga_ordine(al_anno_registrazione, al_num_registrazione, al_prog_riga, as_cod_prodotto_finito,as_cod_versione, 1,as_cod_prodotto_finito,0,0,"P" ,as_gestione, ls_nota_dettaglio)
			
			if ll_ret < 0 then return ll_ret
			
			ll_ret = uof_crea_comp_det(adw_varianti,al_anno_registrazione, al_num_registrazione, al_prog_riga, as_gestione)
				
			if ll_ret <> 0 then return ll_ret
			
			ll_ret = uof_inserisci_optional( adw_varianti, al_anno_registrazione, al_num_registrazione, al_prog_riga, as_cod_prodotto_finito,as_gestione)
				
			if ll_ret = -1 then
				return -1
			elseif ll_ret = -2 then
				return -2
			end if
			
		end if
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		// se la riga ordine è a zero vuol dire  che è una nuova riga
		if al_prog_riga=0 or isnull(al_prog_riga) then
			select max(prog_riga_off_ven)
			into :ll_max_riga
			from det_off_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione;
					
			if ll_max_riga=0 or isnull(ll_max_riga) then
				ll_max_riga = 10
			else
				ll_max_riga = ll_max_riga + 10
				// avanzamento righe 10 alla volta
				ll_max_riga = ( int(ll_max_riga / 10) ) * 10 
			end if
			al_prog_riga = ll_max_riga
			
		end if
		
end choose

luo_conf_varianti = create uo_conf_varianti

luo_conf_varianti = CREATE uo_conf_varianti
luo_conf_varianti.is_gestione = as_gestione
luo_conf_varianti.il_anno_registrazione = al_anno_registrazione
luo_conf_varianti.il_num_registrazione = al_num_registrazione
luo_conf_varianti.il_prog_riga =al_prog_riga
luo_conf_varianti.is_cod_modello = as_cod_prodotto_finito
luo_conf_varianti.is_cod_versione = as_cod_versione

luo_conf_varianti.uof_carica_mp_automatiche( as_cod_prodotto_finito, &
															istr_riepilogo.dimensione_1,  &
															istr_riepilogo.dimensione_2,  &
															istr_riepilogo.dimensione_3,  &
															istr_riepilogo.dimensione_4, &
															istr_riepilogo.alt_asta,&
															istr_riepilogo.cod_verniciatura, &
															istr_riepilogo.cod_verniciatura_2,&
															istr_riepilogo.cod_verniciatura_3,&
															ref astr_mp_automatiche[],&
															ref as_errore)
															
istr_riepilogo.mp_automatiche = astr_mp_automatiche

ll_ret = luo_conf_varianti.uof_crea_varianti(istr_riepilogo, istr_flags, ref as_errore)

if ll_ret < 0 then
	g_mb.error(as_errore)
	return -1
end if

destroy luo_conf_varianti


// Proseguo subito con il calcolo del prezzo della riga principale
ll_ret = uof_calcolo_prezzo(	al_anno_registrazione, al_num_registrazione, al_prog_riga, "P", as_gestione)
if ll_ret = -1 then
	return -1
elseif ll_ret = -2 then
	return -2
end if


return 0

end function

public function integer uof_calcolo_prezzo (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_riga, string as_tipo_gestione);string ls_cod_prodotto, ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_ord_ven, ls_cod_misura_mag,ls_cod_misura_vendita_um, ls_errore

long	ll_i

dec{4} ld_dim_x, ld_dim_y,ld_dim_x_riga, ld_dim_y_riga, ld_sconti[10],ld_provvigione_1,ld_provvigione_2,ld_prezzo_vendita, ld_fat_conversione_ven, &
		ld_quan_vendita_um, ld_prezzo_vendita_um

uo_condizioni_cliente luo_condizioni_cliente


// Carico le variabili globali necessarie per il calcolo del prezzo
s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = al_anno_registrazione
s_cs_xx.listino_db.num_documento = al_num_registrazione
s_cs_xx.listino_db.prog_riga = al_prog_riga

choose case upper(as_tipo_gestione) 
	case "ORD_VEN"
	case "OFF_VEN"
end choose

choose case upper(as_tipo_gestione)
	case "ORD_VEN"

		s_cs_xx.listino_db.tipo_gestione = "varianti_det_ord_ven"

		select cod_cliente, 
				 cod_valuta, 
				 cod_tipo_listino_prodotto, 
				 data_registrazione,
				 cod_agente_1,
				 cod_agente_2,
				 cod_tipo_ord_ven
		into  :s_cs_xx.listino_db.cod_cliente, 
				:s_cs_xx.listino_db.cod_valuta, 
				:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
				:s_cs_xx.listino_db.data_riferimento,
				:ls_cod_agente_1,
				:ls_cod_agente_2,
				:ls_cod_tipo_ord_ven
		from  tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("(uof_calcolo_prezzo) ERRORE GRAVE in ricerca testata ordine di vendita $1/$2/$3; ordine non trovato. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
			return -1
		end if
		
		select	cod_prodotto,
				cod_versione,
				quan_ordine,
				dim_x,
				dim_y
		into 	:ls_cod_prodotto,
				:s_cs_xx.listino_db.cod_versione,
				:s_cs_xx.listino_db.quan_prodotto_finito,
				:ld_dim_x_riga,
				:ld_dim_y_riga
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :al_prog_riga;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("(uof_calcolo_prezzo) ERRORE GRAVE in ricerca dettaglio ordine di vendita $1/$2/$3; riga ordine non trovato. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
			return -1
		end if
				
		choose case as_tipo_riga
			case "P" 
				select dim_x,
						dim_y
				into	:ld_dim_x,
						:ld_dim_y
				from  comp_det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						prog_riga_ord_ven = :al_prog_riga;
				if sqlca.sqlcode <> 0 then
					uof_log( g_str.format("(uof_calcolo_prezzo) ERRORE GRAVE in ricerca dettaglio complementare ordine di vendita $1/$2/$3; dettaglio complmentare ordine non trovato. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
					return -1
				end if
				
			case "A"
				ld_dim_x = 0
				ld_dim_y = 0
				
			case "O"
				// mi tengo le misure lette re dal det_ord_ven
				ld_dim_x = ld_dim_x_riga
				ld_dim_y = ld_dim_y_riga
				
		end choose	

	case "OFF_VEN"

		s_cs_xx.listino_db.tipo_gestione = "varianti_det_off_ven"

		select cod_cliente, 
				 cod_valuta, 
				 cod_tipo_listino_prodotto, 
				 data_registrazione,
				 cod_agente_1,
				 cod_agente_2
		into  	:s_cs_xx.listino_db.cod_cliente, 
				:s_cs_xx.listino_db.cod_valuta, 
				:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
				:s_cs_xx.listino_db.data_riferimento,
				:ls_cod_agente_1,
				:ls_cod_agente_2
		from  tes_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione ;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("ERRORE GRAVE in ricerca testata offerta di vendita $1/$2/$3; offerta non trovato. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
			return -1
		end if
				
		select cod_prodotto,
				cod_versione,
				quan_offerta,
				dim_x,
				dim_y
		into 	:ls_cod_prodotto,
				:s_cs_xx.listino_db.cod_versione,
				:s_cs_xx.listino_db.quan_prodotto_finito,
				:ld_dim_x_riga,
				:ld_dim_y_riga
		from det_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("ERRORE GRAVE in ricerca riga offerta di vendita $1/$2/$3; riga non trovata. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
			return -1
		end if

		choose case as_tipo_riga 
			case "P"
				select dim_x,
						dim_y
				into	:ld_dim_x,
						:ld_dim_y
				from  comp_det_off_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						prog_riga_off_ven = :al_prog_riga;
				if sqlca.sqlcode <> 0 then
					uof_log( g_str.format("ERRORE GRAVE in ricerca dettaglio complementare offerta di vendita $1/$2/$3; dettaglio complementare non trovata. $4", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
					return -1
				end if
				
			case "A"
				ld_dim_x = 0
				ld_dim_y = 0
				
			case "O"
				// mi tengo le misure lette re dal det_ord_ven
				ld_dim_x = ld_dim_x_riga
				ld_dim_y = ld_dim_y_riga
				
		end choose
end choose

luo_condizioni_cliente = create uo_condizioni_cliente
//luo_condizioni_cliente.str_parametri.ldw_oggetto = dw_inizio
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
luo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
luo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
luo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
//luo_condizioni_cliente.is_prodotti_addizionali = istr_riepilogo.cod_prodotto_addizionale
luo_condizioni_cliente.lb_flag_escludi_linee_sconto=false
luo_condizioni_cliente.wf_condizioni_cliente()

for ll_i = 1 to 10
	if ib_nuova_configurazione  then
		// si tratta di un nuovo prodotto configurato
		if upperbound(luo_condizioni_cliente.str_output.sconti) >= ll_i then
			ld_sconti[ll_i] = luo_condizioni_cliente.str_output.sconti[ll_i]
		else
			ld_sconti[ll_i] = 0
		end if
	else
		// sono in modifica, mi tengo gli sconti
		if not ib_nuova_configurazione then
			if upperbound(luo_condizioni_cliente.str_output.sconti) >= ll_i then
				ld_sconti[ll_i] = luo_condizioni_cliente.str_output.sconti[ll_i]
			else
				ld_sconti[ll_i] = 0
			end if
		end if
	end if		
next

if ib_nuova_configurazione then
	ld_provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
	ld_provvigione_2 = luo_condizioni_cliente.str_output.provvigione_2
else
	// mi tengo le vecchie provvigioni
	//ld_provvigione_1 = str_conf_prodotto.provvigione_1
	//ld_provvigione_2 = str_conf_prodotto.provvigione_2
end if
if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
if upperbound(luo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(luo_condizioni_cliente.str_output.variazioni)) then
	ld_prezzo_vendita = 0
	ld_fat_conversione_ven = 1
else
	ld_fat_conversione_ven = 1
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
end if

//  -------------calcolo del valore netto riga
destroy luo_condizioni_cliente

select cod_misura_mag,
		cod_misura_ven
into 	:ls_cod_misura_mag,
		:ls_cod_misura_vendita_um
from 	anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto;
		
uof_fat_conversione( ls_cod_prodotto, &
							s_cs_xx.listino_db.quan_prodotto_finito, &
							ld_prezzo_vendita, &
							ls_cod_misura_mag, &
							ls_cod_misura_vendita_um, &
							ref ld_quan_vendita_um,  &
							ref ld_prezzo_vendita_um, &
							ref ld_fat_conversione_ven,&
							ref  ls_errore)

// ------------------  procedo con UPDATE del record.
choose case as_tipo_gestione
	case "ORD_VEN"
		
		update 	det_ord_ven
		set 		prezzo_vendita = :ld_prezzo_vendita,
					fat_conversione_ven = :ld_fat_conversione_ven,
					provvigione_1 = :ld_provvigione_1,
					provvigione_2 = :ld_provvigione_2,
					sconto_1 = :ld_sconti[1],
					sconto_2 = :ld_sconti[2],
					sconto_3 = :ld_sconti[3],
					sconto_4 = :ld_sconti[4],
					sconto_5 = :ld_sconti[5],
					sconto_6 = :ld_sconti[6],
					sconto_7 = :ld_sconti[7],
					sconto_8 = :ld_sconti[8],
					sconto_9 = :ld_sconti[9],
					sconto_10= :ld_sconti[10],
					quantita_um = :ld_quan_vendita_um,
					prezzo_um = :ld_prezzo_vendita_um
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					prog_riga_ord_ven = :al_prog_riga;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("ERRORE in fase di aggiornamento del prezzo della riga ordine $1/$2/$3. Dettaglio: ", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
		end if
	case "OFF_VEN"
		update 	det_off_ven
		set 		prezzo_vendita = :ld_prezzo_vendita,
					fat_conversione_ven = :ld_fat_conversione_ven,
					provvigione_1 = :ld_provvigione_1,
					provvigione_2 = :ld_provvigione_2,
					sconto_1 = :ld_sconti[1],
					sconto_2 = :ld_sconti[2],
					sconto_3 = :ld_sconti[3],
					sconto_4 = :ld_sconti[4],
					sconto_5 = :ld_sconti[5],
					sconto_6 = :ld_sconti[6],
					sconto_7 = :ld_sconti[7],
					sconto_8 = :ld_sconti[8],
					sconto_9 = :ld_sconti[9],
					sconto_10= :ld_sconti[10],
					quantita_um = :ld_quan_vendita_um,
					prezzo_um = :ld_prezzo_vendita_um
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					prog_riga_off_ven = :al_prog_riga;
		if sqlca.sqlcode <> 0 then
			uof_log( g_str.format("ERRORE in fase di aggiornamento del prezzo della riga ordine $1/$2/$3. Dettaglio: ", al_anno_registrazione,al_num_registrazione,al_prog_riga, sqlca.sqlerrtext ) )
		end if
end choose

return 0
end function

public function decimal uof_string_to_number (string as_string);if as_string = "**"   then
	// questo in Axioma vuol dire che è rimasto il default; 
	// ritorno a zero.
	return 0
end if

guo_functions.uof_replace_string( as_string, ".", ",")

return dec(as_string)

end function

public function integer uof_fat_conversione (string as_cod_prodotto, decimal ad_quan_vendita, decimal ad_prezzo_vendita, string as_cod_misura_mag, string as_cod_misura_vendita_um, ref decimal ad_quan_vendita_um, ref decimal ad_prezzo_vendita_um, ref decimal ad_fat_conversione_ven, ref string as_errore);uo_gestione_conversioni luo_gestione_conversioni
dec{5} ld_precisione_prezzo_mag

select 	precisione_prezzo_mag
into   		:ld_precisione_prezzo_mag
from   	con_vendite
where  	cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore in select tabella parametri vendite. Dettaglio = " + sqlca.sqlerrtext
	return -1
end if

if not isnull(as_cod_prodotto) and len(as_cod_prodotto) > 1 then
	select 	fat_conversione_ven
	into   		:ad_fat_conversione_ven
	from   	anag_prodotti
	where  	cod_azienda = :s_cs_xx.cod_azienda and
			 	cod_prodotto = :as_cod_prodotto ;
	if sqlca.sqlcode <> 0 then
		as_errore = "uof_scrivi_optional: errore " + string(sqlca.sqlcode) + " in ricerca dell'anagrafica del prodotto  " + as_cod_prodotto
		return -1
	end if
		
	if as_cod_misura_mag <> as_cod_misura_vendita_um and ad_fat_conversione_ven <> 0 then
		ad_prezzo_vendita_um = ad_prezzo_vendita / ad_fat_conversione_ven
		luo_gestione_conversioni = CREATE uo_gestione_conversioni
		luo_gestione_conversioni.uof_arrotonda( ad_prezzo_vendita_um, ld_precisione_prezzo_mag)
		destroy luo_gestione_conversioni
		ad_quan_vendita_um = round( ad_quan_vendita * ad_fat_conversione_ven, 4)
	else
		ad_quan_vendita_um = 0 
		ad_prezzo_vendita_um = 0
	end if
end if

return 0
end function

public function integer uof_leggi_xml (string as_filename, ref string as_xml);string ls_xml,ls_buffer
long ll_handle, ll_buffer

ll_handle = fileopen(as_filename, LineMode!)
if ll_handle < 0 then
	messagebox("err", "Errore in OPEN del file " + as_filename)
	return -1
end if

ls_xml = ""
do
	ll_buffer = fileread(ll_handle, ls_buffer)
	ls_xml += ls_buffer + "~r~n"
loop while ll_buffer > 0


fileclose(ll_handle)

as_xml =  ls_xml

return 0
end function

public function integer uof_parse_xml (string as_filename, ref datastore as_dw_det);string ls_test, ls_COD_STRUTTURA, ls_COD_COLORE_STRUTTURA, ls_nome, ls_des_prodotto,ls_cod_misura_mag
int li_i, li_j, li_row
boolean lb_test
PBDOM_BUILDER lpbdom_builder
PBDOM_DOCUMENT lxml_document
PBDOM_ELEMENT lxml_elements[], lxml_root, lxml_oggetto[], ls_empty[]

try
	lpbdom_builder = create PBDOM_BUILDER
	lxml_document = lpbdom_builder.buildfromfile(as_filename)
	catch (runtimeerror ex)
	g_mb.error(ex.getmessage())
end try

// leggo nodi root
if not lxml_document.GetContent(lxml_elements) then
	messagebox("errore","Errore in acquisizione nodi file XML.")
	return -1
end if

// nodo root CONFIGURAZIONE
lxml_root = lxml_elements[3]

lxml_root.getchildelements("OGGETTO", lxml_oggetto)

for li_i = 1 to upperbound(lxml_oggetto)
	
	ls_nome = lxml_oggetto[li_i].getattribute("NOME").gettext()
	
	if ls_nome <> "COMPONENTE" then continue
	
	lxml_elements = ls_empty
	lxml_oggetto[li_i].getchildelements("ATTRIBUTO", lxml_elements)
	
	if li_i > 1 then li_row = as_dw_det.insertrow(0)
	
	for li_j = 1 to upperbound(lxml_elements)
		
		setnull(ls_test)
		
		ls_nome = lxml_elements[li_j].getattribute("NOME").gettext()
		
		ls_test    = lxml_elements[li_j].getattribute("VALORE").gettext()
		
		if li_i = 1 then
			
			if  ls_nome = "COD_STRUTTURA" then
				ls_COD_STRUTTURA = ls_test
			elseif ls_nome = "APC_COD_COLORE_STRUTTURA" then
				ls_COD_COLORE_STRUTTURA = ls_test
			end if
			
		else
			
			choose case ls_nome
				case "APC_CODICE_MATERIALE"
					as_dw_det.setitem(li_row, "APC_CODICE_MATERIALE", ls_test)
					
					select des_prodotto, cod_misura_mag
					into :ls_des_prodotto, :ls_cod_misura_mag
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_test;
					choose case sqlca.sqlcode
						case 0
							as_dw_det.setitem(li_row, "apc_des_prodotto", ls_des_prodotto)
							as_dw_det.setitem(li_row, "apc_um",ls_cod_misura_mag)
						case 100
							as_dw_det.setitem(li_row, "apc_des_prodotto", "Prodotto inesistente in anagrafica")
						case else
							as_dw_det.setitem(li_row, "apc_des_prodotto", "Errore:" + sqlca.sqlerrtext)
					end choose
					
				case "APC_QUANTITA"
					as_dw_det.setitem(li_row, "APC_QUANTITA",  lxml_elements[li_j].getattribute("VALORE").getdoublevalue())
					
				case "APC_GRUPPO_VARIANTE"
					as_dw_det.setitem(li_row, "APC_GRUPPO_VARIANTE", ls_test)
					
				case "APC_SPECIFICA_1"
					as_dw_det.setitem(li_row, "APC_SPECIFICA_1", ls_test)
					
				case "APC_SPECIFICA_2"
					as_dw_det.setitem(li_row, "APC_SPECIFICA_2", ls_test)
					
				case "APC_SPECIFICA_3"
					as_dw_det.setitem(li_row, "APC_SPECIFICA_3", ls_test)

				case "ARTS5"
					as_dw_det.setitem(li_row, "ARTS5", ls_test)
					
			end choose
			
		end if
		
	next
	
next

as_dw_det.setsort("apc_gruppo_variante, apc_specifica_1")
as_dw_det.sort()

return 0
end function

public function integer uof_salva_xml (long al_xml_id, ref transaction at_axioma, ref datastore adw_det);string ls_xml, ls_7zip, ls_zip_file, ls_file, ls_userfolder, ls_xml_ret
long ll_xml_riga_id
blob lb_blob


ls_userfolder = guo_functions.uof_get_user_temp_folder()

try
	
	 //al_xml_id = dw_lista.getitemnumber(al_row, "ax_exp_apice_xml_id")
	 ll_xml_riga_id = 0
	 
	 selectblob xml_file
	 into :lb_blob
	 from TB$XMLDB
	 where
		xmlf_id = :al_xml_id and
		xmlf_riga = :ll_xml_riga_id
	using at_axioma;
		
	if at_axioma.sqlcode = 100 then
		g_mb.error("Record non trovato.", at_axioma)
		return -1
	end if
	
	if at_axioma.sqlcode <> 0 then
		g_mb.error("Errore durante il reucpero del file blob dal database AXIOMA", at_axioma)
		g_mb.error("Errore durante il reucpero del file blob dal database AXIOMA " + at_axioma.sqlerrtext, at_axioma)
		return -1
	end if
	
	ls_file = ls_userfolder + string(al_xml_id)
	ls_zip_file = ls_file + ".zip"
	
	guo_functions.uof_blob_to_file(lb_blob, ls_zip_file)
	
	ls_7zip = s_cs_xx.volume + s_cs_xx.risorse + '7z\7za.exe e -y "' + ls_zip_file + '" -o"' + ls_userfolder + '"'
	
	run(ls_7zip, Minimized!)
	
	// aspetto 1 secondo per attendere che lo zip estragga il file
	sleep(1)
	
	uof_leggi_xml(ls_file, ref ls_xml_ret)
	uof_parse_xml(ls_file, adw_det)
	
catch(runtimeerror ex)
	g_mb.error(ex.getmessage())
finally
	
	if fileexists(ls_file) then filedelete(ls_file)
	if fileexists(ls_zip_file) then filedelete(ls_zip_file)

end try


return 0
end function

public function long uof_crea_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto, string as_cod_versione, decimal ad_quantita, string as_cod_prodotto_finito, decimal ad_dim_x, decimal ad_dim_y, string as_tipo_riga, string as_tipo_gestione, string as_nota_dettaglio);/* funzione che inserisce la riga in det_ord_ven
Valori di ritorno
0= tutto ok
-1= incontrato errore bloccante; fermare la procedura
-2= errore non bloccante; salto l'importazione della procedura

IMPORTANTE: questa funzione provvedere all'inserimento di molteplici righe, include le righe di addizionali, e optionals.
Quindi il parametro as_tipo_riga stabilisce il TIPO riga che viene caricato con i seguenti valori:
P=PRODOTTO FINITO
A=Addizionale
O=Optional
N=Nota (riga di riferimento - RIF ad esempio)
*/
string ls_cod_tipo_det_ven_prodotto,ls_des_prodotto,ls_cod_misura,ls_cod_iva,ls_cod_cliente,ls_cod_iva_esenzione,ls_flag_gen_commessa, ls_cod_tipo_det_ven_addizionali, &
		ls_cod_tipo_det_ven_optionals,ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_cod_contatto,ls_cod_tipo_off_ven
long	ll_num_riga_appartenenza,ll_max_riga_ord_ven,ll_max_riga_off_ven, ll_ret, ll_prog_riga
dec{4} ld_quan_ordine, ld_quantita_um, ld_prezzo_um
datetime ldt_data_consegna, ldt_data_esenzione_iva
uo_default_prodotto luo_default_prodotto

setnull(ls_cod_tipo_det_ven)
setnull(ls_cod_cliente)
setnull(ls_cod_contatto)

if isnull(ad_dim_x) then ad_dim_x = 0
if isnull(ad_dim_y) then ad_dim_y = 0

choose case as_tipo_gestione
	case "ORD_VEN" 
		select cod_tipo_ord_ven,
				data_consegna,
				cod_cliente
		into   :ls_cod_tipo_ord_ven,
				:ldt_data_consegna,
				:ls_cod_cliente
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione;
		if sqlca.sqlcode = 100 then
			uof_log( g_str.format("Testata ordine di vendita non trovata $1 / $2 ", al_anno_registrazione, al_num_registrazione ))
			return -2
		elseif sqlca.sqlcode < 0 then
			uof_log( g_str.format("Errore bloccante in ricerca testata ordine $1 / $2. Dettaglio errore $3 ", al_anno_registrazione, al_num_registrazione, sqlca.sqlerrtext ))
			return -1
		end if
		
		if isnull(ls_cod_tipo_ord_ven) then
			uof_log(  g_str.format("ERRORE GRAVE BLOCCANTE: nella testata ordine $1 / $2 non è specificato il TIPO ORDINE" , al_anno_registrazione, al_num_registrazione) )
			return -1
		end if
		
		select cod_tipo_det_ven_prodotto, 
				cod_tipo_det_ven_addizionali,
				cod_tipo_det_ven_optionals
		into 	:ls_cod_tipo_det_ven_prodotto, 
				:ls_cod_tipo_det_ven_addizionali,
				:ls_cod_tipo_det_ven_optionals
		from 	tab_flags_conf_tipi_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :as_cod_prodotto_finito and 
				 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		if sqlca.sqlcode = -1 then
			uof_log( "Errore in ricerca tipo dettaglio in tabella parametri del configuratore. Dettaglio " + sqlca.sqlerrtext )
			return -1
		elseif sqlca.sqlcode = 0 then
			choose case as_tipo_riga
				case "P"
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_prodotto
				case "A"
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali
				case "O"
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals
			end choose
		end if
	
case "OFF_VEN"
		select cod_tipo_off_ven,
				data_consegna,
				cod_cliente,
				cod_contatto
		into   :ls_cod_tipo_off_ven,
				:ldt_data_consegna,
				:ls_cod_cliente,
				:ls_cod_contatto
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione;
		if sqlca.sqlcode = 100 then
			uof_log( g_str.format("Testata offerta di vendita non trovata $1 / $2 ", al_anno_registrazione, al_num_registrazione ))
			return -2
		elseif sqlca.sqlcode < 0 then
			uof_log( g_str.format("Errore bloccante in ricerca testata offerta $1 / $2. Dettaglio errore $3 ", al_anno_registrazione, al_num_registrazione, sqlca.sqlerrtext ))
			return -1
		end if
		
		if isnull(ls_cod_tipo_off_ven) then
			uof_log(  g_str.format("ERRORE GRAVE BLOCCANTE: nella testata offerta $1 / $2 non è specificato il TIPO OFFERTA" , al_anno_registrazione, al_num_registrazione) )
			return -1
		end if
		
		setnull(ls_cod_tipo_det_ven)
end choose

if isnull(ls_cod_tipo_det_ven) then
	
	select cod_tipo_det_ven_prodotto, 
			cod_tipo_det_ven_addizionali,
			cod_tipo_det_ven_optionals
	into 	:ls_cod_tipo_det_ven_prodotto, 
			:ls_cod_tipo_det_ven_addizionali,
			:ls_cod_tipo_det_ven_optionals
	from 	tab_flags_configuratore
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_modello = :as_cod_prodotto_finito;
	if sqlca.sqlcode = 100 then
		uof_log(g_str.format("Attenzione; per il prodotto $1 non è stata creata la necessaria configurazione nella tabella parametri  del configuratore in Apice", as_cod_prodotto))
		return -1
	elseif sqlca.sqlcode < 0 then
		uof_log( g_str.format("ERRORE GRAVE in ricerca record in tab_flag_configuratore con prodotto $1; dettaglio errore $2",as_cod_prodotto, sqlca.sqlerrtext ))
		return -1
	end if
	
	choose case as_tipo_riga
		case "P"
			ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_prodotto
		case "A"
			ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali
		case "O"
			ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_optionals
	end choose
	
end if


ll_num_riga_appartenenza = 0

choose case upper(as_tipo_riga)
	case "P" 
		ll_num_riga_appartenenza = 0
	case "A"
		ll_num_riga_appartenenza = al_prog_riga
	case "O"
		ll_num_riga_appartenenza = al_prog_riga
end choose

		
select des_prodotto,
		cod_misura_ven,
		cod_iva
into 	:ls_des_prodotto,
		:ls_cod_misura,
		:ls_cod_iva
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :as_cod_prodotto;
if sqlca.sqlcode = 100 then
	uof_log( g_str.format("Il prodotto $1 è inesistente in anagrafica prodotti", as_cod_prodotto ))
	return -2
elseif sqlca.sqlcode < 0 then
	uof_log( g_str.format("Errore bloccante in ricerca del prodotto $1. Dettaglio errore $2 ", as_cod_prodotto, sqlca.sqlerrtext ))
	return -1
end if
		

if not isnull(ls_cod_cliente) then
	
	select cod_iva, 
			data_esenzione_iva
	into 	:ls_cod_iva_esenzione,
			:ldt_data_esenzione_iva
	from 	anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode = 100 then
		uof_log( g_str.format("Codice cliente $1 non trovato", ls_cod_cliente ))
		return -2
	elseif sqlca.sqlcode < 0 then
		uof_log( g_str.format("Errore bloccante in ricerca cliente $1 . Dettaglio errore $2 ", ls_cod_cliente, sqlca.sqlerrtext ))
		return -1
	end if
	
elseif not isnull(ls_cod_contatto) then
	
	select cod_iva, 
			data_esenzione_iva
	into 	:ls_cod_iva_esenzione,
			:ldt_data_esenzione_iva
	from 	anag_contatti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_contatto = :ls_cod_contatto;
	if sqlca.sqlcode = 100 then
		uof_log( g_str.format("Codice contatto $1 non trovato", ls_cod_contatto ))
		return -2
	elseif sqlca.sqlcode < 0 then
		uof_log( g_str.format("Errore bloccante in ricerca cliente $1 . Dettaglio errore $2 ", ls_cod_contatto, sqlca.sqlerrtext ))
		return -1
	end if
	
else
	
	uof_log( g_str.format("Errore GRAVE, l'offerta $1/$2 non è associata ad alcun cliente o contatto ", al_anno_registrazione, al_num_registrazione ))
	return -1
	
end if

if not isnull(ls_cod_iva_esenzione) then 
	ls_cod_iva = ls_cod_iva_esenzione
else
	// se l'iva è vuota, la prendo dal tipo dettaglio
	if isnull(ls_cod_iva) then
		select cod_iva
		into 	:ls_cod_iva
		from 	tab_tipi_det_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		if sqlca.sqlcode = 100 then
			uof_log( g_str.format("Codice tipo dettaglio $1 non trovato", ls_cod_tipo_det_ven ))
			return -2
		elseif sqlca.sqlcode < 0 then
			uof_log( g_str.format("Errore bloccante in ricerca tipo dettaglio di vendita $1 . Dettaglio errore $2 ", ls_cod_tipo_det_ven, sqlca.sqlerrtext ))
			return -1
		end if
	end if
end if

ls_flag_gen_commessa = "N"

luo_default_prodotto = CREATE uo_default_prodotto
if luo_default_prodotto.uof_flag_gen_commessa(as_cod_prodotto) = 0 then
	ls_flag_gen_commessa = luo_default_prodotto.is_flag_gen_commessa
else
	ls_flag_gen_commessa = "S"
end if
destroy luo_default_prodotto

ll_prog_riga = al_prog_riga


choose case as_tipo_gestione
	case "ORD_VEN"
		// calcolo il numero di riga successivo, solo per le righe di appartenenza
		if ll_num_riga_appartenenza > 0 then
			select 	max(prog_riga_ord_ven)
			into 		:ll_max_riga_ord_ven
			from 		det_ord_ven
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						num_riga_appartenenza = :ll_prog_riga;
			
			if isnull(ll_max_riga_ord_ven) or ll_max_riga_ord_ven = 0 then
				ll_max_riga_ord_ven = ll_prog_riga + 1
			else
				ll_max_riga_ord_ven++
			end if
			ll_prog_riga = ll_max_riga_ord_ven
		end if
		
		// procedo con SQL insert
		
		insert into det_ord_ven
				(cod_azienda,
				anno_registrazione,
				num_registrazione,
				prog_riga_ord_ven,
				cod_tipo_det_ven,
				cod_prodotto,
				des_prodotto,
				quan_ordine,
				prezzo_vendita,
				fat_conversione_ven,
				prezzo_um,
				quantita_um,
				cod_iva,
				data_consegna,
				cod_versione,
				cod_misura,
				flag_gen_commessa,
				flag_elaborato_rda,
				num_riga_appartenenza,
				dim_x,
				dim_y,
				nota_dettaglio)
		values (
				:s_cs_xx.cod_azienda,
				:al_anno_registrazione,
				:al_num_registrazione,
				:ll_prog_riga,
				:ls_cod_tipo_det_ven,
				:as_cod_prodotto,
				:ls_des_prodotto,
				:ad_quantita,
				0,
				1,
				0,
				0,
				:ls_cod_iva,
				:ldt_data_consegna,
				:as_cod_versione,
				:ls_cod_misura,
				:ls_flag_gen_commessa,
				'N', 
				:ll_num_riga_appartenenza,
				:ad_dim_x,
				:ad_dim_y,
				:as_nota_dettaglio);
				
		if sqlca.sqlcode < 0 then
			uof_log( g_str.format("Errore bloccante INSERT dettaglio riga ordine di vendita del prodotto $1 . Dettaglio errore $2 ", as_cod_prodotto, sqlca.sqlerrtext ))
			return -1
		else
			uof_log( g_str.format("Creazione della riga ordine $1/$2/$3 con prodotto $4", al_anno_registrazione,al_num_registrazione,ll_prog_riga, as_cod_prodotto ))
		end if
		
	case "OFF_VEN"

		// calcolo il numero di riga successivo, solo per le righe di appartenenza
		if ll_num_riga_appartenenza > 0 then
			select 	max(prog_riga_off_ven)
			into 		:ll_max_riga_ord_ven
			from 		det_off_ven
			where 	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						num_riga_appartenenza = :ll_prog_riga;
			
			if isnull(ll_max_riga_off_ven) or ll_max_riga_off_ven = 0 then
				ll_max_riga_off_ven = ll_prog_riga + 1
			else
				ll_max_riga_off_ven++
			end if
			ll_prog_riga = ll_max_riga_off_ven
		end if
		
		// procedo con SQL insert
		
		insert into det_off_ven
				(cod_azienda,
				anno_registrazione,
				num_registrazione,
				prog_riga_off_ven,
				cod_tipo_det_ven,
				cod_prodotto,
				des_prodotto,
				quan_offerta,
				prezzo_vendita,
				fat_conversione_ven,
				prezzo_um,
				quantita_um,
				cod_iva,
				data_consegna,
				cod_versione,
				cod_misura,
				flag_gen_commessa,
				num_riga_appartenenza,
				dim_x,
				dim_y,
				nota_dettaglio)
		values (
				:s_cs_xx.cod_azienda,
				:al_anno_registrazione,
				:al_num_registrazione,
				:ll_prog_riga,
				:ls_cod_tipo_det_ven,
				:as_cod_prodotto,
				:ls_des_prodotto,
				:ad_quantita,
				0,
				1,
				0,
				0,
				:ls_cod_iva,
				:ldt_data_consegna,
				:as_cod_versione,
				:ls_cod_misura,
				:ls_flag_gen_commessa,
				:ll_num_riga_appartenenza,
				:ad_dim_x,
				:ad_dim_y,
				:as_nota_dettaglio);
				
		if sqlca.sqlcode < 0 then
			uof_log( g_str.format("Errore bloccante INSERT dettaglio riga offerta di vendita del prodotto $1 . Dettaglio errore $2 ", as_cod_prodotto, sqlca.sqlerrtext ))
			return -1
		else
			uof_log( g_str.format("Creazione della riga offerta $1/$2/$3 con prodotto $4", al_anno_registrazione,al_num_registrazione,ll_prog_riga,as_cod_prodotto ))
		end if
		
end choose

// Per le addizionali prosegui subito con il calcolo del prezzo del prodotto
if as_tipo_riga = "A" then
	ll_ret = uof_calcolo_prezzo(	al_anno_registrazione, al_num_registrazione, ll_prog_riga, as_tipo_riga, as_tipo_gestione)
	if ll_ret = -1 then
		return -1
	elseif ll_ret = -2 then
		return -2
	end if
end if 

// Per optional prosegui subito con il calcolo del prezzo del prodotto
if as_tipo_riga = "O" then
	ll_ret = uof_calcolo_prezzo(	al_anno_registrazione, al_num_registrazione, ll_prog_riga, as_tipo_riga, as_tipo_gestione)
	if ll_ret = -1 then
		return -1
	elseif ll_ret = -2 then
		return -2
	end if
end if 

return ll_prog_riga
end function

public function integer uof_get_nota_dettaglio (string as_nome_valore_cercato, datawindow adw_varianti, ref string as_valore);string ls_find, ls_str
long ll_i,ll_row, ll_find

ls_find = "upper(apc_gruppo_variante) = 'NOTA' "
as_valore = ""
ll_row = 1

do while true
	
	ll_row = adw_varianti.Find (ls_find  , ll_row + 1, adw_varianti.rowcount() )	
	
	if ll_row < 0 then 
		uof_log( g_str.format("Errore nella ricerca del valore " + as_nome_valore_cercato + " nelle specifiche del dettaglio complementare" , sqlca.sqlerrtext) )
		return -2
	elseif ll_row = 0 then
		exit
	else
		ls_str = adw_varianti.getitemstring(ll_row, "apc_specifica_1")
		if ls_str <> "**" and not isnull(ls_str) and len(ls_str) > 0 then
			if len(as_valore) > 0 then
				as_valore = as_valore + "~r~n" + ls_str
			else
				as_valore = ls_str
			end if
		end if
	end if
	
loop

return 0
end function

public function integer uof_calcola_peso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);dec{5}					ld_peso_netto_unitario
uo_funzioni_1 		luo_funzioni_1
integer					li_ret
string					ls_riga_ordine
long						ll_riga_collegata


luo_funzioni_1 = create uo_funzioni_1
li_ret = luo_funzioni_1.uof_calcola_peso_riga_ordine( ai_anno_ordine, al_num_ordine, al_riga_ordine, ld_peso_netto_unitario, as_errore)
destroy luo_funzioni_1

if li_ret<0 then
	return -1
	
else
	//faccio update riga principale -----------------
	ls_riga_ordine = string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)
	
	update det_ord_ven
	set peso_netto=:ld_peso_netto_unitario
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in fase di aggiornamento peso netto unitario della riga "+ls_riga_ordine + ": " + sqlca.sqlerrtext
		return -1
	end if
	//--------------------------------------------------
end if

//righe collegate, se ce ne sono ...
declare cu_optionals cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				num_riga_appartenenza=:al_riga_ordine and
				cod_prodotto is not null
	order by prog_riga_ord_ven;

open cu_optionals;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in apertura cursore 'cu_optionals: " + sqlca.sqlerrtext
	destroy luo_funzioni_1
	rollback;
	return -1
end if

luo_funzioni_1 = create uo_funzioni_1

do while true
	fetch cu_optionals into :ll_riga_collegata;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore cu_optionals: " + sqlca.sqlerrtext
		destroy luo_funzioni_1
		close cu_optionals;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	ls_riga_ordine = string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(ll_riga_collegata)
	ld_peso_netto_unitario  = 0
	li_ret = luo_funzioni_1.uof_calcola_peso_riga_ordine(ai_anno_ordine, al_num_ordine, ll_riga_collegata, ld_peso_netto_unitario, as_errore)
	
	if li_ret<0 then
		as_errore = "Errore in fase di calcolo peso netto unitario della riga collegata "+ls_riga_ordine + ": " + as_errore
		destroy luo_funzioni_1
		close cu_optionals;
		return -1
	else
		
		//faccio update riga collegata ----------------------
		update det_ord_ven
		set peso_netto=:ld_peso_netto_unitario
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno_ordine and
					num_registrazione=:al_num_ordine and
					prog_riga_ord_ven=:ll_riga_collegata;
		
		if sqlca.sqlcode<0 then
			as_errore = "Errore in fase di aggiornamento peso netto unitario della riga "+ls_riga_ordine + ": " + sqlca.sqlerrtext
			close cu_optionals;
			destroy luo_funzioni_1
			return -1
		end if
		//--------------------------------------------------
	end if
loop

close cu_optionals;
destroy luo_funzioni_1



return 0



end function

public function integer uof_importa_blob (string as_path, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_tipo_gestione, ref transaction at_tran);string ls_file_dir, ls_file_name,ls_file_ext
long ll_prog_mimetype
blob lb_file
guo_functions.uof_get_file_info( as_path, ls_file_dir, ls_file_name, ls_file_ext)

ls_file_ext = lower(ls_file_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_file_ext;

if sqlca.sqlcode <> 0 then
	uof_log("Anomalia in importazione del FILE " + as_path + "; l'estensione non esiste nella tabella dei mimetype; la scheda dei tagli non verrà stampata importata.")
	return -2
end if

guo_functions.uof_file_to_blob(as_path, lb_file)

choose case as_tipo_gestione
	case "ORD_VEN"

		INSERT INTO det_ord_ven_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ord_ven,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :al_anno_registrazione,   
				  :al_num_registrazione,   
				  :al_prog_riga,   
				  'AX1',   
				  'Ordine di lavoro AXIOMA',   
				  null,
				  :ll_prog_mimetype) 
		using at_tran;
		if at_tran.sqlcode < 0 then
			uof_log("Errore grave nell'inseriemento delle note riga ordine relative all'importazione dell'ordine di lavoro Axioma. Dettaglio " + at_tran.sqlerrtext)
			return -2
		end if
		
		
		updateblob det_ord_ven_note
		set 		note_esterne = :lb_file
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					prog_riga_ord_ven = :al_prog_riga and
					cod_nota = 'AX1'
		using at_tran;
		if at_tran.sqlcode < 0 then
			uof_log("Errore grave nell'inseriemento delle BLOB digitale associato alle note riga ordine relative all'importazione dell'ordine di lavoro Axioma. Dettaglio " + at_tran.sqlerrtext)
			return -2
		end if
		

	case "OFF_VEN"
	
		INSERT INTO det_off_ven_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_off_ven,   
				  cod_nota,   
				  des_nota,   
				  note,
				  prog_mimetype)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :al_anno_registrazione,   
				  :al_num_registrazione,   
				  :al_prog_riga,   
				  'AX1',   
				  'Ordine di lavoro AXIOMA',   
				  null,
				  :ll_prog_mimetype)  ;
		if sqlca.sqlcode < 0 then
			uof_log("Errore grave nell'inseriemento delle note riga offerta relative all'importazione dell'ordine di lavoro Axioma. Dettaglio " + sqlca.sqlerrtext)
			return -2
		end if
		
		
		updateblob det_off_ven_note
		set 		note_esterne = :lb_file
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					prog_riga_off_ven = :al_prog_riga and
					cod_nota = 'AX1';
		if sqlca.sqlcode < 0 then
			uof_log("Errore grave nell'inseriemento delle BLOB digitale associato alle note riga ordine relative all'importazione dell'ordine di lavoro Axioma. Dettaglio " + sqlca.sqlerrtext)
			return -2
		end if
		
end choose

return 0
end function

on uo_axioma.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_axioma.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;is_log_errori = ""
end event

