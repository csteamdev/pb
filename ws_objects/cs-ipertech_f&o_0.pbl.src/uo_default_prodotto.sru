﻿$PBExportHeader$uo_default_prodotto.sru
$PBExportComments$User Object Ricerca valori default per versione e gen_commessa (personalizzata PTENDA)
forward
global type uo_default_prodotto from nonvisualobject
end type
end forward

global type uo_default_prodotto from nonvisualobject
end type
global uo_default_prodotto uo_default_prodotto

type variables
string is_cod_versione
string is_flag_gen_commessa, is_cod_tipo_politica_riordino, &
         is_flag_tipo_riordino, is_messaggio
string is_flag_presso_cg, is_flag_presso_pt
end variables

forward prototypes
public function integer uof_flag_gen_commessa (string fs_cod_prodotto)
public function integer uof_ricerca_versione (string fs_cod_prodotto)
end prototypes

public function integer uof_flag_gen_commessa (string fs_cod_prodotto);string ls_cod_fornitore, ls_stringa_cg, ls_stringa_pt

choose case uof_ricerca_versione(fs_cod_prodotto)
	case 0
		select cod_tipo_politica_riordino
		into   :is_cod_tipo_politica_riordino
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_prodotto;
		if sqlca.sqlcode = 0 then
			if not isnull(is_cod_tipo_politica_riordino) then
				select flag_tipo_riordino, cod_fornitore
				into   :is_flag_tipo_riordino, :ls_cod_fornitore
				from   tab_tipi_politiche_riordino
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_politica_riordino = :is_cod_tipo_politica_riordino;
				if sqlca.sqlcode = 0 and not isnull(is_flag_tipo_riordino) then
					choose case is_flag_tipo_riordino
						case "C"
							is_flag_presso_cg = "N"
							is_flag_presso_pt = "N"
							is_flag_gen_commessa = "S"
						case "A"
							is_flag_presso_cg = "N"
							is_flag_presso_pt = "N"
							is_flag_gen_commessa = "N"
						case "T"
							// controllo se presso centro gibus
							is_flag_presso_cg = "N"
							is_flag_presso_pt = "N"
							select stringa
							into   :ls_stringa_cg
							from   parametri_azienda
							where  cod_azienda = :s_cs_xx.cod_azienda and
							       cod_parametro = 'FCG';
							if sqlca.sqlcode = -1 then
								g_mb.messagebox("APICE","Errore in ricerca parametro FCG in parametri azienda. Dettaglio "+sqlca.sqlerrtext)
							else
								if ls_stringa_cg = ls_cod_fornitore then
									is_flag_presso_cg = "S"
								end if
							end if
							// controllo se presso centro progettotenda
							select stringa
							into   :ls_stringa_pt
							from   parametri_azienda
							where  cod_azienda = :s_cs_xx.cod_azienda and
							       cod_parametro = 'FPT';
							if sqlca.sqlcode = -1 then
								g_mb.messagebox("APICE","Errore in ricerca parametro FPT in parametri azienda. Dettaglio "+sqlca.sqlerrtext)
							else
								if ls_stringa_pt = ls_cod_fornitore then
									is_flag_presso_pt = "S"
								end if
							end if
							// sempre gen_commessa = N
							is_flag_gen_commessa = "N"
						case else
							is_flag_gen_commessa = "N"
					end choose					
				end if			
			end if
		end if
	case 1
		return 0
	case -1
		return -1
end choose
return 0

end function

public function integer uof_ricerca_versione (string fs_cod_prodotto);select cod_versione
into   :is_cod_versione
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :fs_cod_prodotto
and    flag_predefinita = 'S';
if sqlca.sqlcode = 0 then
	is_flag_presso_cg = "N"
	is_flag_presso_pt = "N"
	is_flag_gen_commessa = "S"
elseif sqlca.sqlcode = 100 then
	is_flag_gen_commessa = "N"
	is_flag_presso_cg = "N"
	is_flag_presso_pt = "N"
	return 1
else
	is_flag_gen_commessa = "N"
	is_flag_presso_cg = "N"
	is_flag_presso_pt = "N"
	is_messaggio = "Errore in ricerca versione in distinta padri " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

on uo_default_prodotto.create
TriggerEvent( this, "constructor" )
end on

on uo_default_prodotto.destroy
TriggerEvent( this, "destructor" )
end on

