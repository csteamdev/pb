﻿$PBExportHeader$uo_ottimizza_tagli.sru
forward
global type uo_ottimizza_tagli from nonvisualobject
end type
end forward

global type uo_ottimizza_tagli from nonvisualobject
end type
global uo_ottimizza_tagli uo_ottimizza_tagli

type variables
// massima lunghezza dei profili disponibili
dec{4} id_lunghezza_profili = 6500

// variabili di istanza per output
str_profili istr_profili
end variables

forward prototypes
public function integer uof_insert_soluzioni (long al_numero_asta, long al_soluzioni[], long al_numero_soluzione)
public function integer uof_ottimizza (long al_tagli[])
public function integer uof_test (long al_tagli[])
end prototypes

public function integer uof_insert_soluzioni (long al_numero_asta, long al_soluzioni[], long al_numero_soluzione);long	ll_soluzioni[], ll_i

for ll_i = 1 to al_numero_soluzione
	ll_soluzioni[ll_i] = al_soluzioni[ll_i]
next

istr_profili.astr_profili[al_numero_asta + 1].astr_tagli = ll_soluzioni

return 0

end function

public function integer uof_ottimizza (long al_tagli[]);//Commento originale del programma in C
/************************************************************
 * Questo sorgente risolve il seguente problema:                    *
 * Date molte (infinte) aste di una certa dimensione in millimetri  *
 * (MAXDIM=6500 per default) ed una lista di n valori (< MAXDIM)    *
 * che rappresentano la lungnezza di aste che si vogliono ottenere  *
 * tagliando quelle di partenza, determinare in che ordine          *
 * effettuare il taglio per minimizzare gli sprechi (uno spreco     *
 * si verifica quando avanza un pezzo di asta che non è possibile   *
 * utilizzare).                                                     *
 * La soluzione adottata (che probabilmente NON è la migliore, ma   *
 * sicuramente ci si avvicina), utilizza la programmazione dinamica.*
 * Questo problema non è altro che una variante del classico        *
 * problema dello zaino (dato uno zaino di capienza P e n oggetti   *
 * ciascuno con un peso p(o) e un valore v(o) determinare quali     *
 * oggetti mettere nello zaino per massimizzare la somma dei valori *
 * degli oggetti).                                                  *
 * Il difetto di questa implementazione è che usa uma matrice di    *
 * (MAXDIM*n) interi senza segno (unsigned long) da 4 byte, che può *
 * facilmente riempire la memoria anche per valori di n abbastanza  *
 * bassi (> 1000). (6500*1000*4=6500000*4=26000000~=24.8M )         *
*******************************************************************/
boolean	lb_trovato=false
long 	ll_matrice[1000, 0 to 6500], ll_matrice_null[1000, 0 to 6500]
long 	ll_soluzioni[], al_tagli_rimanenti[], ll_array_null[]
long	ll_numero_asta = 0
long	ll_soluzione, ll_i, ll_j, ll_k, ll_numero_soluzione, ll_soluzioni_trovate
long	ll_num_tagli

ll_num_tagli = upperbound(al_tagli)
ll_soluzioni_trovate = 0

if ll_num_tagli = 0 then
	// non ci sono tagli da fare
	return 0
end if

ll_soluzioni = ll_array_null

for ll_numero_asta = 0 to ll_num_tagli
	// azzero la matrice
	ll_matrice = ll_matrice_null
	
	// verifico che non ci siano tagli richiesti oltre la massima lunghezza dei profili
	for ll_i = al_tagli[1] to id_lunghezza_profili
		ll_matrice[1,ll_i] = al_tagli[1]
	next
	
	for ll_i = 2 to (upperbound(al_tagli) - ll_soluzioni_trovate - 1)
		if ll_i >= upperbound(al_tagli) - ll_soluzioni_trovate then exit
		for ll_j = 1 to id_lunghezza_profili
			ll_soluzione =  ll_j - al_tagli[ll_i]
			if ll_soluzione >= 0 then
				ll_matrice[ll_i, ll_j] = Max( ll_matrice[ll_i - 1, ll_soluzione] + al_tagli[ll_i], ll_matrice[ll_i - 1, ll_j])
			else
				ll_matrice[ll_i, ll_j] = ll_matrice[ll_i - 1, ll_j]
			end if
		next
	next
	
	ll_j  = id_lunghezza_profili
	
	ll_numero_soluzione = 0
	
	for ll_soluzione = (upperbound(al_tagli) - ll_soluzioni_trovate -1 ) to 2 step -1
		if upperbound(al_tagli[]) - ll_soluzioni_trovate <= 1 then exit
		if ll_matrice[ll_soluzione - 1, ll_j] <> ll_matrice[ll_soluzione, ll_j] then
			ll_numero_soluzione++
			ll_soluzioni[ll_numero_soluzione] = al_tagli[ll_soluzione]
			ll_j = ll_j - al_tagli[ll_soluzione]
		end if
	next
	
	if ll_matrice[1,ll_j] > 0 then
		ll_numero_soluzione++
		ll_soluzioni[ll_numero_soluzione] = al_tagli[1]
	end if
		
	uof_insert_soluzioni(ll_numero_asta, ll_soluzioni, ll_numero_soluzione)	
	
	ll_k = 0
	
	al_tagli_rimanenti = ll_array_null
	
	integer li_k, li_i, li_j
	long		ll_1, ll_2
	
	li_i = 1
	li_j = 1
	
	for li_k = 1 to upperbound(ll_soluzioni)
		
		ll_1 = al_tagli[li_i]
		ll_2 =ll_soluzioni[ll_numero_soluzione - li_k + 1]
		
		do while  ll_1 <> ll_2

			al_tagli[li_j] = al_tagli[li_i]
			li_i ++
			li_j ++
			
			ll_1 = al_tagli[li_i]
			ll_2 =ll_soluzioni[ll_numero_soluzione - li_k + 1]
		loop
		
		li_i++

	next
	
	do while li_i < upperbound(al_tagli)
	
		al_tagli[li_j] = al_tagli[li_i]
		li_i++
		li_j++
	
	loop
	
	ll_soluzioni_trovate += ll_numero_soluzione
	
	if ll_soluzioni_trovate >= upperbound(al_tagli) then exit
	
	
/*

	for ll_i = 1 to upperbound(al_tagli)
		lb_trovato=false
		for ll_j = 1 to upperbound(ll_soluzioni)
			if ll_soluzioni[ll_j] = al_tagli[ll_i] then 
				lb_trovato = true
				exit
			end if
		next
		
		if not(lb_trovato) then
			ll_k ++
			al_tagli_rimanenti[ll_k] = al_tagli[ll_i]
		end if
	next

	al_tagli[]  = al_tagli_rimanenti
	
	ll_num_tagli = upperbound(al_tagli)
	
	ll_i = 0
	ll_j = 0
	
	for ll_k = 1 to ll_numero_soluzione
		do
			ll_i ++
			ll_j ++
			al_tagli[ll_j] = al_tagli[ll_i]
		loop until al_tagli[ll_i] = ll_soluzioni[ll_numero_soluzione - ll_k]
		ll_i ++
	next
	
	ll_k = ll_i
	
	for ll_i = ll_k to upperbound(al_tagli)
		al_tagli[ll_j] = al_tagli[ll_i]
		ll_j ++
	next
	
	ll_num_tagli = ll_num_tagli - ll_numero_soluzione
*/

next

return 0
end function

public function integer uof_test (long al_tagli[]);return 0
end function

on uo_ottimizza_tagli.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ottimizza_tagli.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

